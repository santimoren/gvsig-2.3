/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.primitive;


public interface Circumference extends Curve {

	/**
	 * Sets the two values to define a circumference.
	 * @param center
	 * The center point of the circumference.
	 * @param radious
	 * A point that is used to calculate the radius.
	 */
	void setPoints(Point center, Point radious);

	/**
	 * Sets the two values to define a circumference.
	 * @param center
	 * The center point of the circumference.
	 * @param radious
	 * The radius of the circumference.
	 */
	void setPoints(Point center, double radious);

	/**
	 * Sets the values to define a circumference from three points.
	 * @param p1
	 * First point
	 * @param p2
	 * Second point
	 * @param p3
	 * Third point
	 */
	void setPoints(Point p1, Point p2, Point p3);

	/**
	 * Returns the center of the circumference.
	 * @return
	 * The center of the circumference.
	 */
	Point getCenter();

	/**
	 * Returns the radius of the circumference
	 * @return
	 * The radius of the circumference
	 */
	double getRadious();
}
