/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.mkmvnproject.gui;

import java.awt.Dimension;
import java.io.IOException;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.WindowInfo;

/**
 * A gvSIG window which shows the execution log messages of the create plugin
 * process.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class CreatePluginConsoleWindow extends CreatePluginConsolePanel
		implements IWindow, IWindowListener {

	private static final long serialVersionUID = -5589080107545290284L;

	private WindowInfo info;

	private Object profile = WindowInfo.EDITOR_PROFILE;

	/**
	 * Constructor.
	 * 
	 * @throws IOException
	 *             if there is an error creating the console streams
	 */
	public CreatePluginConsoleWindow() throws IOException {
		super();
		initializeWindow();
	}

	/**
	 * Constructor.
	 * 
	 * @param isDoubleBuffered
	 *            if the panel must be double buffered.
	 * @throws IOException
	 *             if there is an error creating the console streams
	 */
	public CreatePluginConsoleWindow(boolean isDoubleBuffered) throws IOException {
		super(isDoubleBuffered);
		initializeWindow();
	}

	/**
	 * Initializes the window.
	 */
	private void initializeWindow() {
		Dimension dimension = new Dimension(600, 300);
		setSize(dimension);
		int code = WindowInfo.ICONIFIABLE | WindowInfo.MAXIMIZABLE
				| WindowInfo.RESIZABLE | WindowInfo.MODELESSDIALOG;
		profile = WindowInfo.EDITOR_PROFILE;
		info = new WindowInfo(code);
		info.setTitle("Console");
		info.setMinimumSize(dimension);
	}

	public WindowInfo getWindowInfo() {
		return info;
	}

	public Object getWindowProfile() {
		return profile;
	}

	public void windowActivated() {
		// Nothing to do
	}

	public void windowClosed() {
		super.closeConsole();
	}

}
