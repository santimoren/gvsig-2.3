/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.warning.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

import org.gvsig.compat.CompatLocator;
import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.MultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl.SimpleTextSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * Warning symbol
 * 
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class WarningSymbol extends MultiShapeSymbol implements IWarningSymbol {

	public static final String WARNING_SYMBOL_PERSISTENCE_DEFINITION_NAME = "WarningSymbol";

	private static final String FIELD_DESCRIPTION = "description";
	private static final String FIELD_MESSAGE = "message";
	private static final String FIELD_EXCEPTION_TYPE = "exceptionType";

	private String message;
	private int exceptionType;
	private SimpleTextSymbol text;

	/**
	 * Empty constructor, only used in persistence.
	 */
	public WarningSymbol() {
		super();
	}

	public WarningSymbol(String message, String symbolDesc,
			int symbolDrawExceptionType) {
		super();
		this.message = message;
		this.exceptionType = symbolDrawExceptionType;
		setDescription(symbolDesc);
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Cancellable cancel) {
		try {
			drawInsideRectangle(g, g.getTransform(), geom.getShape().getBounds(), null);
		} catch (SymbolDrawingException e) {
			// IMPOSSIBLE
		}
	}

	public void setDrawExceptionType(int symbolDrawExceptionType) {
		this.exceptionType = symbolDrawExceptionType;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String getMessage() {
		return message;
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r,
			PrintAttributes properties) throws SymbolDrawingException {
		g.setClip(r);
		if (message == null) {
			message = "Symbol undrawable.\nPlease, check errors.";
		}

		String[] messageLines = CompatLocator.getStringUtils().split(message,
				"\n");
		int strokeWidth = (int) (Math.min(r.width, r.height) * .1);

		if (strokeWidth == 0) {
			strokeWidth = 1;
		}
		g.setColor(Color.red);
		g.setStroke(new BasicStroke(strokeWidth, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_ROUND));
		int width = r.width - (strokeWidth + strokeWidth);
		int height = r.height - (strokeWidth + strokeWidth);
		double radius = Math.min(width, height) * .5;
		double centerX = r.getCenterX();
		double centerY = r.getCenterY();
		Ellipse2D circle = new Ellipse2D.Double(centerX - radius, centerY
				- radius, 2 * radius, 2 * radius);
		g.draw(circle);
		g.setClip(circle);
		double aux = Math.cos(Math.PI * 0.25) * radius;
		g.drawLine((int) (centerX - aux), (int) (centerY - aux),
				(int) (centerX + aux), (int) (centerY + aux));
		int fontSize = 20;
		g.setFont(new Font("Arial", fontSize, Font.PLAIN));
		g.setColor(Color.black);
		g.setClip(null);

		if (text == null) {
			text = new SimpleTextSymbol();
			text.setAutoresizeEnabled(true);

		}

		double lineHeight = (r.getHeight() - 6) / messageLines.length;
		Rectangle textRect = new Rectangle((int) r.getMinX(),
				(int) r.getMinY() + 6, (int) r.getWidth(), (int) lineHeight);
		for (int i = 0; i < messageLines.length; i++) {
			text.setText(messageLines[i]);
			text.drawInsideRectangle(g, null, textRect, null);
			textRect.setLocation((int) r.getX(), (int) (r.getY() + r
					.getHeight()));
		}
	}

	public Object clone() throws CloneNotSupportedException {
		WarningSymbol copy = (WarningSymbol) super.clone();

		if (text != null) {
			copy.text = (SimpleTextSymbol) text.clone();
		}

		return copy;
	}

	/**
	 * @return the exceptionType
	 */
	private int getExceptionType() {
		return exceptionType;
	}

	/**
	 * @param exceptionType
	 *            the exceptionType to set
	 */
	private void setExceptionType(int exceptionType) {
		this.exceptionType = exceptionType;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setDescription(state.getString(FIELD_DESCRIPTION));
		setExceptionType(state.getInt(FIELD_EXCEPTION_TYPE));
		setMessage(state.getString(FIELD_MESSAGE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DESCRIPTION, getDescription());
		state.set(FIELD_EXCEPTION_TYPE, getExceptionType());
		state.set(FIELD_MESSAGE, getMessage());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(WARNING_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						WarningSymbol.class,
						WARNING_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						WARNING_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				// Description
				definition.addDynFieldString(FIELD_DESCRIPTION).setMandatory(true);
				// Exception type
				definition.addDynFieldInt(FIELD_EXCEPTION_TYPE).setMandatory(true);
				// Message
				definition.addDynFieldString(FIELD_MESSAGE).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
	        SymbolManager manager = MapContextLocator.getSymbolManager();
	        
	        manager.registerSymbol(IWarningSymbol.SYMBOL_NAME,
	                WarningSymbol.class);

			return Boolean.TRUE;
		}
		
	}

}