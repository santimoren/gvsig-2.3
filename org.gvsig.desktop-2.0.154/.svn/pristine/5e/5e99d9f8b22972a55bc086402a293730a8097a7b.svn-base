/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols;


/**
 * Allows to create multi layer symbols using the composition of
 * several symbols of the same type.Depending on the type of the symbols that are
 * used to compose the final symbol, the user will have MultiLayerMarkerSymbol,
 * MultiLayerLineSymbol,...
 *
 */

public interface IMultiLayerSymbol extends ISymbol {

	/**
	 * Establishes a concret symbol for a layer
	 *
	 * @param index, index of the layer
	 * @param layer, symbol to be "applied" to the layer
	 * @throws IndexOutOfBoundsException
	 */
	public abstract void setLayer(int index, ISymbol layer)
	throws IndexOutOfBoundsException;
	/**
	 * Changes the position of two layers in a multilayersymbol
	 * @param index1, index of the layer
	 * @param index2, index of the layer
	 */
	public abstract void swapLayers(int index1, int index2);
	/**
	 * Obtains  the symbol that "contains" a layer whose index is the argument of the method.
	 * @param layerIndex
	 * @return
	 */
	public abstract ISymbol getLayer(int layerIndex);
	/**
	 * Returns the number of layers
	 * @return int
	 */
	public abstract int getLayerCount();

	/**
	 * Stacks a new symbol to the symbol list. The symbol is appended to the
	 * list and, in terms of use, it will be the last symbol to be processed.
	 * @param ISymbol newLayer
	 */
	public abstract void addLayer(ISymbol newLayer);
	/**
	 * Stacks a new symbol to the symbol list. The symbol is appended to the
	 * list in the specified position.
	 * @param ISymbol newLayer
	 */
	public abstract void addLayer(ISymbol newLayer, int layerIndex)
	throws IndexOutOfBoundsException;

	/**
	 * TODO maybe push it up to ISymbol
	 * @param layer
	 * @return true if this symbol contains the removed one
	 */
	public abstract boolean removeLayer(ISymbol layer);

}
