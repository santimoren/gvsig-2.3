/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.HashSet;
import java.util.Set;

import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendChangedEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.listeners.LegendListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * <p>Manages all legend listeners of a layer, notifying them any legend change event produced.</p>
 *
 */
public class LayerChangeSupport {
        private static final Logger logger = LoggerFactory.getLogger(LayerChangeSupport.class);
	/**
	 * <p>The legend listeners of a layer.</p>
	 */
	private final Set<LegendListener> listeners = new HashSet<>();

	/**
	 * <p>Registers a <code>LegendListener</code>.</p>
	 * 
	 * @param listener the legend listener
	 */
	public void addLayerListener(LegendListener listener) {
            if( listener == null ) {
                return;
            }
            listeners.add(listener);
	}

	/**
	 * <p>Removes a registered <code>LegendListener</code>.</p>
	 *
	 * @param listener the legend listener
	 */
	public void removeLayerListener(LegendListener listener) {
		listeners.remove(listener);
	}

	/**
	 * <p>Notifies a legend change to all legend listeners registered.</p>
	 *
	 * @param e a legend event with the new legend and events that compound this one
	 */
	public void callLegendChanged(LegendChangedEvent e) {
            for (LegendListener listener : listeners) {
                try {
                    listener.legendChanged(e);
                } catch(Exception ex) {
                    logger.warn("Error calling listener '"+listener.toString()+"'.",ex);
                }
            }
	}
}
