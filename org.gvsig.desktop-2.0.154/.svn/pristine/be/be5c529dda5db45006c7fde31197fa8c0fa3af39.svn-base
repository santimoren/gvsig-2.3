
package org.gvsig.propertypage;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;


public class BasePropertiesPageDialog extends BasePropertiesPagePanelLayout implements org.gvsig.tools.swing.api.Component {
    public static final int ACTION_CANCEL = 0;
    public static final int ACTION_ACCEPT = 1;
    public static final int ACTION_APPLY = 2;

    private Object obj;
    private String groupID;
    private List<PropertiesPage> pages = null;
    private int userAction = ACTION_ACCEPT;
    
    public BasePropertiesPageDialog() {
    }

    public BasePropertiesPageDialog(Object obj, String groupID) {
        init(groupID, obj);
    }

    protected void init(String groupID, Object obj) {
        this.obj = obj;
        this.groupID = groupID;
        PropertiesPageManager manager = MapControlLocator.getPropertiesPageManager();
        this.pages = manager.getPages(this.groupID,this.obj);        
        initComponents();
    }
    
    protected void initComponents() {
        I18nManager i18nManager = ToolsLocator.getI18nManager(); 

        this.content.setLayout(new BorderLayout());
        this.content.add(createPropertiesPagesPanel(), BorderLayout.CENTER );

        this.titleLabel.setText("");
        
        this.acceptButton.setText(i18nManager.getTranslation("accept"));
        this.acceptButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                whenAccept();
            }
        });
        this.applyButton.setText(i18nManager.getTranslation("apply"));
        this.applyButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                whenApply();
            }
        });
        this.cancelButton.setText(i18nManager.getTranslation("cancel"));
        this.cancelButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                whenCancel();
            }
        });

    }

    protected Component createPropertiesPagesPanel() {
        if( this.pages.size()==1 && !useTabsAlwais() ) {
            PropertiesPage page = this.pages.get(0);
            return page.asJComponent();
        } else {
            JTabbedPane tabbedPane = new JTabbedPane();
            for( int i=0; i<this.pages.size(); i++ ) {
                PropertiesPage page = this.pages.get(i);
                tabbedPane.addTab(page.getTitle(), page.asJComponent());
            }
            return tabbedPane;
        }
    }
    
    protected boolean useTabsAlwais() {
        return false;
    }
    
    public void whenAccept() {
        for( int i=0; i<this.pages.size(); i++ ) {
            PropertiesPage page = this.pages.get(i);
            if( ! page.whenAccept() ) {
                return;
            }
        }
        this.userAction = ACTION_ACCEPT;
        this.closeDialog();
    }
    
    public void whenApply() {
        for( int i=0; i<this.pages.size(); i++ ) {
            PropertiesPage page = this.pages.get(i);
            if( ! page.whenApply() ) {
                return;
            }
        }
        this.userAction = ACTION_APPLY;
    }
    
    public void whenCancel() {
        for( int i=0; i<this.pages.size(); i++ ) {
            PropertiesPage page = this.pages.get(i);
            if( ! page.whenCancel() ) {
                return;
            }
        }
        this.userAction = ACTION_CANCEL;
        this.closeDialog();
    }
    
    public int getUserAction() {
        return this.userAction;
    }
    
    protected void closeDialog() {
        this.setVisible(false);
    }

    public JComponent asJComponent() {
        return this;
    }
    
    
}
