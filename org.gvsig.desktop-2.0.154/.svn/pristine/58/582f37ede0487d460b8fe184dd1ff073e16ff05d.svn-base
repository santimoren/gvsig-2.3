/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.gputils;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * AWT {@link PathIterator} to draw a {@link GeneralPathX} object.
 * 
 * While iterating the {@link GeneralPathX) each point coordinates are 
 * converted to int. If a segment is a PathIterator#SEG_LINETO and the 
 * integer coordinates are the same as the previous point ones, the point 
 * is ignored and the next one is looked for.
 * 
 * @author gvSIG Team
 */
public class DrawGeneralPathXIterator implements PathIterator {

    protected int typeIdx = -1;
    protected int pointIdx = 0;
    protected GeneralPathX path;
    protected AffineTransform affine;

    /**
     * This array keeps the "real" coordinates (double precision) which
     * will afterwards be rounded to int. It is final and created with
     * its maximum possible size. This does not have a performance cost
     * because only relevant indices are read/written (also when applying
     * an affine transformation)
     */
    private final double[] currentPointRealCoords = new double[] { 0, 0, 0, 0, 0, 0 };
    
    /**
     * This array keeps the rounded (int) coordinates.
     * It is final and created with
     * its maximum possible size. This does not have a performance cost
     * because only relevant indices are read/written.
     */
    private final int[] currentPointCoords = new int[] { 0, 0, 0, 0, 0, 0 };
    
    private final int[] previousPointCoords = new int[] { 0, 0 };

    private int currentType = -1;
    private int previousType = -1;

    private static final int curvesize[] = { 1, 1, 2, 3, 0 };

    private final int numTypes;
    private final int numPoints;

    /**
     * Constructs an iterator given a GeneralPathX.
     * 
     * @see GeneralPathX#getPathIterator
     */
    public DrawGeneralPathXIterator(GeneralPathX path) {
        this(path, null);
    }

    /**
     * Constructs an iterator given a GeneralPathX and an optional
     * AffineTransform.
     * 
     * @see GeneralPathX#getPathIterator
     */
    public DrawGeneralPathXIterator(GeneralPathX path, AffineTransform at) {
        this.path = path;
        this.affine = at;
        // Go to the first segment
        this.pointIdx = 0;
        this.numPoints = path.getNumCoords();
        this.typeIdx = 0;
        this.numTypes = path.getNumTypes();
        if (numTypes > 0) {
            currentType = path.getTypeAt(typeIdx);
            readCurrentSegmentCoordsAsInt();
            previousType = currentType;
            previousPointCoords[0] = currentPointCoords[0];
            previousPointCoords[1] = currentPointCoords[1];
        }
    }

    public int getWindingRule() {
        return path.getWindingRule();
    }

    public boolean isDone() {
        return typeIdx >= numTypes;
    }

    public void next() {
        do {
            typeIdx++;
            pointIdx += curvesize[currentType];
            if (typeIdx < numTypes) {
                currentType = path.getTypeAt(typeIdx);
                if (SEG_CLOSE != currentType) {
                    readCurrentSegmentCoordsAsInt();
                }
            } else {
                // We have reached past the last segment
                return;
            }
        } while (isSameSegment());

        previousType = currentType;
        previousPointCoords[0] = currentPointCoords[0];
        previousPointCoords[1] = currentPointCoords[1];
    }

    private final boolean isSameSegment() {
        return SEG_LINETO == currentType
            && (SEG_LINETO == previousType || SEG_MOVETO == previousType)
            && currentPointCoords[0] == previousPointCoords[0]
            && currentPointCoords[1] == previousPointCoords[1];
    }

    private void readCurrentSegmentCoordsAsInt() {
        int currentSize = curvesize[currentType];

        for (int i = 0; i < currentSize; i++) {
            readPoint(i * 2, pointIdx + i);
        }

        if (affine != null && !affine.isIdentity()) {
            affine.transform(currentPointRealCoords, 0, currentPointRealCoords,
                0, currentSize);
        }

        for (int i = 0; i < currentSize * 2; i++) {
            currentPointCoords[i] = (int) currentPointRealCoords[i];
        }
    }

    private void readPoint(int currentCoordsPos, int currentPointIdx) {
        Point point = path.getPointAt(currentPointIdx);
        currentPointRealCoords[currentCoordsPos] = point.getX();
        currentPointRealCoords[currentCoordsPos + 1] = point.getY();
    }

    public int currentSegment(float[] coords) {
        if (SEG_CLOSE != currentType) {
            int size = curvesize[currentType] * 2;

            for (int i = 0; i < size; i++) {
                coords[i] = currentPointCoords[i];
            }
        }
        return currentType;
    }

    public int currentSegment(double[] coords) {
        int size = curvesize[currentType] * 2;

        for (int i = 0; i < size; i++) {
            coords[i] = currentPointCoords[i];
        }
        return currentType;
    }
}
