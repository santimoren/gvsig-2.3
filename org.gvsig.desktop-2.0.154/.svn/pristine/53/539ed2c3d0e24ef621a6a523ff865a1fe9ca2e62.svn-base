/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;


import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;


/**
 * Information of the legend that allows to be represented graphically to the user
 * 
 */
public interface IClassifiedLegend extends ILegend {
	/**
	 * Obtains the descriptions of the classes that are in the classification
	 * @return DOCUMENT ME!
	 */
	String[] getDescriptions();

	/**
	 * Obtains the symbols of each class of the classification for the user.
	 *
	 * @return DOCUMENT ME!
	 */
	ISymbol[] getSymbols();

	/**
	 * Obtains the values in relation with each class of the classification
	 *
	 * @return DOCUMENT ME!
	 */
	Object[] getValues();
}
