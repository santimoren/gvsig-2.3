/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.utils.swing.objectSelection.ObjectSelectionModel;
import org.gvsig.utils.swing.objectSelection.SelectionException;

/**
 * @author Fernando Gonz�lez Cort�s
 */
public class FieldSelectionModel implements ObjectSelectionModel {

    // final static private Logger logger =
    // LoggerFactory.getLogger(FieldSelectionModel.class);

    private FeatureStore fs;
    private String msg;
    private int type = DataTypes.INVALID;
    private boolean selectAll = false;

    /**
     * Crea un nuevo FirstFieldSelectionModel.
     * 
     */
    public FieldSelectionModel(FeatureStore fs, String msg, int type) {
        this.fs = fs;
        this.msg = msg;
        this.type = type;
    }

    public FieldSelectionModel(FeatureStore fs, String msg) {
        this(fs, msg, DataTypes.UNKNOWN);
        selectAll = true;
    }

    @SuppressWarnings("unchecked")
    public Object[] getObjects() throws SelectionException {

        List<String> fields = new ArrayList<String>();
        Iterator<FeatureAttributeDescriptor> iterator = null;
        try {
            iterator = fs.getDefaultFeatureType().iterator();
        } catch (DataException e) {
            throw new SelectionException(
                "Can't create iterator for the atribute of feature type", e);
        }
        while (iterator.hasNext()) {
            FeatureAttributeDescriptor descriptor = iterator.next();
            if (type != DataTypes.INVALID) {
                if ((descriptor.getType() == type) || selectAll) {
                    fields.add(descriptor.getName());
                }
            } else {
                fields.add(descriptor.getName());
            }
        }

        return (String[]) fields.toArray(new String[fields.size()]);
    }

    /**
     * @see org.gvsig.utils.swing.objectSelection.ObjectSelectionModel#getMsg()
     */
    public String getMsg() {
        return msg;
    }
}
