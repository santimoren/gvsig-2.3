/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.panel;


import java.awt.Component;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.gvsig.i18n.Messages;
import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInterval;
import org.gvsig.timesupport.AbsoluteIntervalTypeNotRegisteredException;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.swing.impl.components.AbsoluteTemporalComponent;
import org.gvsig.timesupport.swing.impl.components.ITemporalComponent;
import org.gvsig.timesupport.swing.impl.components.RelativeTemporalComponent;
import org.gvsig.timesupport.swing.impl.rdv.TimeAdjustmentListener;
import org.gvsig.timesupport.swing.impl.rdv.TimeEvent;
import org.gvsig.timesupport.swing.impl.rdv.TimeSlider;
/**
 * Window main panel (control Relative and Absolute time instances)
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public class TimePanel extends JPanel implements TimeAdjustmentListener {
    private static final long serialVersionUID = -1711925095164453822L;
    /**
     * Time mode {Relative or absolute}
     */
    public static enum TIME_MODE {RELATIVE, ABSOLUTE};
    private TIME_MODE _mode;
    private TimeSlider slider = null;
    private ITemporalComponent jComponentStart = null;
    private ITemporalComponent jComponentEnd = null;
    private Instant startDate = null;
    private Instant endDate = null;
    private Instant startBackupForInterval = null;
    private Instant endBackupForInterval = null;
    private Interval interval = null;
    private boolean eventSynchronized = false;

    private List<Instant> instants = null;
    private int instantsPosition;
    private Instant actual = null;
    private Instant instAux = null;
    private boolean updateSlider = false;
    private static TimeSupportManager timeSupportManager = TimeSupportLocator
    .getManager();

    private TimeAdjustmentListener listener = null;

    public TimePanel()
    {
        super();
        _mode = TIME_MODE.RELATIVE;
    }

    /**
     * Intializes the panel
     * 
     * @param start
     * @param end
     */
    public void initialize(Instant start, Instant end)
    {
        
        createComponents(start, end);
        startBackupForInterval = start;
        endBackupForInterval = end;
    }
    
    /**
     * Set instants to all controls
     * @param start
     * @param end
     */
    public void setInstants(Instant start, Instant end) {
            startDate = start;
            endDate = end;

            getJComponentStart().setTimes(Messages.getText("lbl_begin_position"),
                startDate, startDate, endDate);

            getJComponentEnd().setTimes(Messages.getText("lbl_end_position"),
                endDate, startDate, endDate);


            getTimeSlider().setValues(startDate, endDate);
    }

    /**
     * Set the time Mode
     * @param mode
     * @param start
     * @param end
     */
    public void setTimeMode(TIME_MODE mode, Instant start, Instant end)
    {
    	_mode = mode;
    	initialize(start,end);
    }
    
    /**
     * Get the current TIME_MODE
     * @return
     */
    public TIME_MODE getTimeMode()
    {
    	return _mode;
    }
    
    /**
     * Set the listener to capture all events {@link TimeAdjustmentListener}
     * @param listener
     */
    public void setListener(TimeAdjustmentListener listener) {
        this.listener = listener;
    }

    /**
     * Get the listener {@link TimeAdjustmentListener}
     * @return
     */
    public TimeAdjustmentListener getListener() {
        return this.listener;
    }

    /**
     * Set if works with instants (true) or interval (false)
     * @param changeable
     */
    public void setValueChangeableSlider(boolean changeable) {
        if(changeable == true)
        { //Instant
            if(interval != null)
            {
                startBackupForInterval = startDate;
                startBackupForInterval = endDate;
                setInstants(interval.getStart(), interval.getEnd());
            }
        }else
        { //Interval
            if(interval != null)
            {
                setInstants(startBackupForInterval, endBackupForInterval);
            }
        }
        getTimeSlider().setValueChangeable(changeable);
    }

    /**
     * Set the valid instants
     * @param instants
     */
    public void setInstants(List<Instant> instants)
    {
        this.instants = instants;
        instantsPosition = 0;
        actual = null;
    }
    
    public void timeChanged(TimeEvent event) {
        // TODO Auto-generated method stub
    	if(!isUpdating())
    	{
    		setUpdating(true);
	        if (event.getSource() instanceof TimeSlider) {
	            if(getTimeSlider().getValueChangeable())
	            {
	            	if(this.instants != null && !updateSlider)
	            	{
	            	    updateSlider = true;
	            	    if(!getTimeSlider().isValueAdjusting())
	            	    {
	            	        getTimeSlider().setCurrentInstant(getApproximateInstant(getTimeSlider().getCurrentInstant()));  
	            	        getJComponentStart().setCurrentInstant(getTimeSlider().getCurrentInstant());
	            	    }
	            	    updateSlider = false;
	            	}else
	            	{
	            	    getJComponentStart().setCurrentInstant(getTimeSlider().getCurrentInstant());
	            	}
	
	            }
	
	        } else if (event.getSource() instanceof ITemporalComponent) {
	            
	            ITemporalComponent calendar = (ITemporalComponent) event.getSource();
	            if (calendar == getJComponentStart()) {
	                if (!getTimeSlider().getValueChangeable()) {
	                	
		                    try {// this try is for the daylight saving (it creates
		                        // exceptions)
		                        if(!updateSlider)
		                            getTimeSlider().setStartInstant((Instant)getJComponentStart().getTime());
		                    } catch (IllegalArgumentException e) {
		                    	e.printStackTrace();
		                    }
	                }
	            } else {
	                if (!getTimeSlider().getValueChangeable()) {
	                	   
	                		try {// this try is for the daylight saving (it creates
		                        // exceptions)
	                		    if(!updateSlider)
	                		        getTimeSlider().setEndInstant((Instant)getJComponentEnd().getTime());
		                    } catch (IllegalArgumentException e) {
		                    	e.printStackTrace();
		                    }
	                }
	            }
	            updateCalendars();
	            
	        } else {
		            try {// this try is for the daylight saving (it creates exceptions)
		                if(instants == null)
		                getTimeSlider().setCurrentInstant((Instant)
		                    getJComponentStart().getTime());
		            } catch (IllegalArgumentException e) {
		            } finally {
	        	}
	        }
	        if (getTimeSlider().getValueChangeable() && listener != null)
	            listener.timeChanged(new TimeEvent(this));
        	setUpdating(false);
    	}
    }

    public void rangeChanged(TimeEvent event) {
        // TODO Auto-generated method stub
        if (event.getSource() instanceof TimeSlider) {            
            getJComponentStart().setCurrentInstant(getTimeSlider().getStartInstant());
            getJComponentEnd().setCurrentInstant(getTimeSlider().getEndInstant());
        }
        if (!getTimeSlider().getValueChangeable() && listener != null)
            listener.rangeChanged(new TimeEvent(this));
    }

    /**
     * Update slider (intervals)
     */
    public void fireRangeInterval() {
        getTimeSlider().fireRangeChanged();
    }

    /**
     * Update slider (instants)
     */
    public void fireValueInstant() {
        getTimeSlider().fireValueChanged();
    }

    public void boundsChanged(TimeEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        getJComponentStart().setEnabled(enabled);
        getJComponentEnd().setEnabled(enabled);
        getTimeSlider().setEnabled(enabled);
    }

    public void resetValues() {
        getTimeSlider().resetAllDates();
        getJComponentStart().resetValues();
        getJComponentEnd().resetValues();
    }

    public void setEnabledEndCalendar(boolean enabled) {
        getJComponentEnd().setEnabled(enabled);
    }

    /**
     * Get the current interval
     * @return
     */
    public Interval getInterval() {

        if (datesCorrect()) {
        	if(_mode == TIME_MODE.RELATIVE)
        	{
        		return timeSupportManager.createRelativeInterval(
                (RelativeInstant)getJComponentStart().getTime(),(RelativeInstant) getJComponentEnd()
                .getTime());
        	}else
        	{
        		try {
        			AbsoluteInstant start = (AbsoluteInstant) getJComponentStart().getTime();
        			AbsoluteInstant end = (AbsoluteInstant) getJComponentEnd().getTime();
					return createAbsoluteInterval(start, end); 
				} catch (AbsoluteIntervalTypeNotRegisteredException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
        	}
        } else
            return null;

    }

    /**
     * Get the current instant
     * @return
     */
    public Instant getInstant() {

        if (datesCorrect()) {
            try {// this try is for the daylight saving (it creates exceptions)
                return (Instant) getJComponentStart().getTime();
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else
            return null;

    }

    public void setInterval(Interval interval)
    {
        this.interval = interval;
    }
    
    /*
     * PRIVATE METHODS
     */
    private void createUI() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        add((Component)getJComponentStart());
        add(getTimeSlider());
        add((Component)getJComponentEnd());
        
        getTimeSlider().setValueChangeable(false);

        getJComponentStart().setListener(this);
        getJComponentEnd().setListener(this);
        getTimeSlider().addTimeAdjustmentListener(this);
    }
    
    private void createComponents(Instant start, Instant end)
    {
    	removeAll();
    	revalidate();
    	repaint();
    	jComponentStart = null;
    	jComponentEnd = null;
    	slider = null;
    	setInstants(start, end);
    	createUI();
    	
    }

    @SuppressWarnings("unused")
    private boolean datesCorrect() {
        if (getJComponentStart().isEnabled() && getJComponentEnd().isEnabled()) {
            try {
                Instant init = (Instant) getJComponentStart().getTime();
                Instant end = (Instant) getJComponentEnd().getTime();
                return true;
            } catch (IllegalArgumentException e) {
                return false;
            }
        }
        return true;
    }

    private TimeSlider getTimeSlider() {
        if (slider == null)
        	if(_mode == TIME_MODE.RELATIVE)
        		slider = new TimeSlider(false);
        	else
        		slider = new TimeSlider(true);
        return slider;
    }

    private ITemporalComponent getJComponentStart() {
        if (jComponentStart == null)
        	if(_mode == TIME_MODE.RELATIVE)
        		jComponentStart = new RelativeTemporalComponent();
        	else
        		jComponentStart = new AbsoluteTemporalComponent();
        
        return jComponentStart;
    }

    private ITemporalComponent getJComponentEnd() {
        if (jComponentEnd == null)
        	if(_mode == TIME_MODE.RELATIVE)
        		jComponentEnd = new RelativeTemporalComponent();
        	else
        		jComponentEnd = new AbsoluteTemporalComponent();
        return jComponentEnd;
    }

    private void updateCalendars() {
        try {
            Instant start = (Instant)getJComponentStart().getTime();
            Instant end = (Instant)getJComponentEnd().getTime();
            getJComponentStart().setEndTime(end);
            getJComponentEnd().setStartTime(start);
        } catch (IllegalArgumentException e) {
        }
    }
    
    private boolean isUpdating()
    {
    	return eventSynchronized;
    }
    
    private void setUpdating(boolean value)
    {
    	eventSynchronized = value;
    }
    
    private AbsoluteInterval createAbsoluteInterval(AbsoluteInstant start, AbsoluteInstant end) throws AbsoluteIntervalTypeNotRegisteredException
    {
       long millisStart = start.toStandardDuration().getMillis();
       long millisEnd = end.toStandardDuration().getMillis();
       
       long millisInterval = millisEnd - millisStart;
       
       int years = 0, months = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0, millis = 0;
       
       long mod = millisInterval % TimeSlider.MILLIS_BY_YEAR;
       if((millisInterval / TimeSlider.MILLIS_BY_YEAR) > 0)
       {
           years = (int) (millisInterval / TimeSlider.MILLIS_BY_YEAR);
           millisInterval = mod;
       }
       
       mod = millisInterval % TimeSlider.MILLIS_BY_MONTH;
       if((millisInterval / TimeSlider.MILLIS_BY_MONTH) > 0)
       {
           months = (int) (millisInterval / TimeSlider.MILLIS_BY_MONTH);
           millisInterval = mod;
       }
       
       mod = millisInterval % TimeSlider.MILLIS_BY_WEEK;
       if((millisInterval / TimeSlider.MILLIS_BY_WEEK) > 0)
       {
           weeks = (int) (millisInterval / TimeSlider.MILLIS_BY_WEEK);
           millisInterval = mod;
       }
       
       mod = millisInterval % TimeSlider.MILLIS_BY_DAY; 
       if ((millisInterval / TimeSlider.MILLIS_BY_DAY) > 0){
           if((int)(millisInterval / TimeSlider.MILLIS_BY_DAY) > 0)
           {
               days = (int)(millisInterval / TimeSlider.MILLIS_BY_DAY);
           }
           millisInterval = mod;
       }           
      
       mod = millisInterval % TimeSlider.MILLIS_BY_HOUR;
       if ((millisInterval / TimeSlider.MILLIS_BY_HOUR) > 0){
           hours = (int)(millisInterval / TimeSlider.MILLIS_BY_HOUR);
           millisInterval = mod;
       }            
   
       mod = millisInterval % TimeSlider.MILLIS_BY_MINUTE;
       if ((millisInterval / TimeSlider.MILLIS_BY_MINUTE) > 0){
           seconds =(int)(millisInterval / TimeSlider.MILLIS_BY_MINUTE);
           millisInterval = mod;
       }
       
       mod = millisInterval % TimeSlider.MILLIS_BY_SECOND;
       if ((millisInterval / TimeSlider.MILLIS_BY_SECOND) > 0){
           seconds = (int)(millisInterval / TimeSlider.MILLIS_BY_SECOND);
           millisInterval = mod;
       } 
       
       mod = millisInterval;
       if (mod > 0){
           millis = (int) mod;
       }
       
       return timeSupportManager.createAbsoluteInterval(years, months, weeks, days, hours, minutes, seconds, millis);
       
       
    }
    
    private Instant getApproximateInstant(Instant instant)
    {
        
        int index = 0;
        
        if(instant.isBefore(instants.get(0)))
            return instants.get(0);
        
        for(Instant ins : instants)
        {
            if(ins.isEqual(instant))
                return ins;
            else if(ins.isAfter(instant))
            {
                break;
            }
            index++;
        }
        if(index == instants.size())
            return instants.get(instants.size()-1);
    
        return instants.get(index);
    }
}
