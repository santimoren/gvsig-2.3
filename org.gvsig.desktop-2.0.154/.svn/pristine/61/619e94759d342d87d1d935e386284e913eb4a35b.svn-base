/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom;

import org.cresques.ProjectionLibrary;

import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Registers the default implementation for {@link GeometryManager}.
 *
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class GeometryLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsAPI(GeometryLibrary.class);
        require(ToolsLibrary.class);
        require(ProjectionLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
    }

    protected void doPostInitialize() throws LibraryException {
        // Validate there is any implementation registered.
        if(! GeometryLocator.getInstance().exists(GeometryLocator.GEOMETRY_MANAGER_NAME)) {
            throw new ReferenceNotRegisteredException(
                GeometryLocator.GEOMETRY_MANAGER_NAME,
                GeometryLocator.getInstance());
        }
        DataTypesManager dataTypesManager = ToolsLocator.getDataTypesManager();
        dataTypesManager.addtype(DataTypes.GEOMETRY, "Geometry", "Geometry",
            Geometry.class, null);
        dataTypesManager.addtype(DataTypes.ENVELOPE, "Envelope", "Envelope",
            Envelope.class, null);

    }
}
