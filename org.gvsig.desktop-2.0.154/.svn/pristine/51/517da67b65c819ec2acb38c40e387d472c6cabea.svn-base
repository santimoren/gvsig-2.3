/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * Created on 28-oct-2004
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;

import org.gvsig.fmap.mapcontrol.MapControl;


public interface MapTool {
	public static final int EVENT_MEASURE = 1;
	public static final int EVENT_MOVE = 2;
	public static final int EVENT_POINTDOUBLECLICKED = 3;
	public static final int EVENT_POINT = 4;
	public static final int EVENT_POLYLINEFINISHED = 5;
	public static final int EVENT_POINTFIXED = 6;
	public static final int EVENT_POINTS = 7;
	public static final int EVENT_RECTANGLE = 8;
	
	public static final int TYPE_UNKNOW = 0;
	public static final int TYPE_POINT = 1;
	public static final int TYPE_RECTANGLE = 2;
	public static final int TYPE_POLYLINE = 3;
	public static final int TYPE_POLYGON = 4;
	public static final int TYPE_CIRCLE = 5;
	public static final int TYPE_MOVE = 5;
	public static final int TYPE_MOUSEMOVENENT = 7;
	public static final int TYPE_MOUSEDRAGGED = 8;
	
	public static final int ACTION_ZOOM = 20;
	public static final int ACTION_PAN = 21;
	public static final int ACTION_SELECTION = 22;
	public static final int ACTION_AREA = 23;
	public static final int ACTION_MEASURE = 24;
	
	public String getName();
	public int getType();
	public Image getImageCursor();
	public MapControl getMapControl();

}
