/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialUniqueValueLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendClearEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Vectorial legend for unique values
 * 
 * @author Vicente Caballero Navarro
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class VectorialUniqueValueLegend extends AbstractClassifiedVectorLegend implements IVectorialUniqueValueLegend {
	final static private Logger LOG = LoggerFactory.getLogger(VectorialUniqueValueLegend.class);

	public static final String VECTORIAL_UNIQUE_VALUE_LEGEND_PERSISTENCE_DEFINITION_NAME =
			"VectorialUniqueValueLegend";

	private static final String FIELD_KEYS = "keys";
	private static final String FIELD_NULL_VALUE_SYMBOL = "nullValueSymbol";
	private static final String FIELD_SELECTED_COLORS = "selectedColors";
	private static final String FIELD_SYMBOLS = "symbols";
	private static final String FIELD_USE_DEFAULT_SYMBOL = "useDefaultSymbol";

	private Map<Object, ISymbol> symbols = createSymbolMap();

	private List<Object> keys = new ArrayList<Object>();

    private ISymbol defaultSymbol;
    private int shapeType;
    private boolean useDefaultSymbol = false;
    private Color[] selectedColors=null;
    
    private ISymbol nullValueSymbol = null;
    
    public VectorialUniqueValueLegend() {
		super();
	}

    /**
     * Constructor method
     *
     * @param shapeType Type of the shape.
     */
    public VectorialUniqueValueLegend(int shapeType) {
		super();
    	setShapeType(shapeType);
    }

    public void setShapeType(int shapeType) {
    	if (this.shapeType != shapeType) {
    		if(defaultSymbol==null || defaultSymbol.getSymbolType()!=shapeType){
    			ISymbol old = defaultSymbol;
    			defaultSymbol = getSymbolManager().createSymbol(shapeType);
    			fireDefaultSymbolChangedEvent(new SymbolLegendEvent(old, defaultSymbol));
    		}
    		this.shapeType = shapeType;
    	}
    }

    public void setValueSymbolByID(int id, ISymbol symbol) {
        ISymbol old = (ISymbol)symbols.put(keys.get(id), symbol);
        fireClassifiedSymbolChangeEvent(new SymbolLegendEvent(old, symbol));
    }

    public Object[] getValues() {
        return symbols.keySet().toArray(new Object[0]);
    }

    public void addSymbol(Object key, ISymbol symbol) {
    	ISymbol resul;
    	if (key == null) {
    		resul = nullValueSymbol;
    		nullValueSymbol = symbol;
    	}
    	else {
			resul = (ISymbol) symbols.put(key, symbol);

			if (resul != null) {
				LOG.error("Error: la clave " + key + " ya exist�a. Resul = "
						+ resul);
				LOG.warn("symbol nuevo:" + symbol.getDescription()
						+ " Sviejo= " + resul.getDescription());
			} else {
				keys.add(key);
			}
    	}
    	
        fireClassifiedSymbolChangeEvent(new SymbolLegendEvent(resul, symbol));
    }

    public void clear() {
        keys.clear();
        ISymbol[] olds = (ISymbol[])symbols.values().toArray(new ISymbol[0]);
        symbols.clear();
        removeLegendListener(getZSort());
        setZSort(null);

        fireLegendClearEvent(new LegendClearEvent(olds));
    }

    public String[] getDescriptions() {
        String[] descriptions = new String[symbols.size()];
        ISymbol[] auxSym = getSymbols();

        for (int i = 0; i < descriptions.length; i++) {
			descriptions[i] = auxSym[i].getDescription();
		}

        return descriptions;
    }

     public ISymbol[] getSymbols() {
 		ISymbol[] symbolList;
		if (nullValueSymbol == null) {
			symbolList = new ISymbol[symbols.size()];
			return (ISymbol[]) symbols.values().toArray(symbolList);
		}
		else {
			symbolList = new ISymbol[symbols.size() + 1];
			symbolList[0] = nullValueSymbol;
			int i = 1;
			for (Iterator<ISymbol> iterator = symbols.values().iterator(); iterator
					.hasNext();) {
				symbolList[i] = iterator.next();
				i++;
			}
			return symbolList;
		}
    }

    public void setClassifyingFieldNames(String[] fNames) {
        // TODO: Check if need more process
        super.setClassifyingFieldNames(fNames);
    }

    /**
	 * Devuelve un s�mbolo a partir de una IFeature. OJO!! Cuando usamos un
	 * feature iterator de base de datos el �nico campo que vendr� rellenado es
	 * el de fieldID. Los dem�s vendr�n a nulos para ahorra tiempo de creaci�n.
	 *
	 * @param feat
	 *            IFeature
	 *
	 * @return S�mbolo.
     * @throws MapContextException 
	 */
    public ISymbol getSymbolByFeature(Feature feat) throws MapContextException {

    	Object val = feat.get(getClassifyingFieldNames()[0]);
//        Object val = feat.get(fieldId);
        ISymbol theSymbol = getSymbolByValue(val);

        if (theSymbol != null) {
        	return theSymbol;

        }

        if (isUseDefaultSymbol()) {
			return defaultSymbol;
		}

        return null;
    }


    public ISymbol getDefaultSymbol() {

    	if(defaultSymbol==null) {
			defaultSymbol = getSymbolManager().createSymbol(shapeType);
    		fireDefaultSymbolChangedEvent(new SymbolLegendEvent(null, defaultSymbol));
    	}
    	return defaultSymbol;
    }

	public void setDefaultSymbol(ISymbol s) {
    	ISymbol mySymbol = defaultSymbol;

    	if (s == null) {
			throw new NullPointerException("Default symbol cannot be null");
		}

    	ISymbol old = mySymbol;
    	defaultSymbol = s;
    	fireDefaultSymbolChangedEvent(new SymbolLegendEvent(old, s));
    }


    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.rendering.UniqueValueLegend#getSymbolByValue(com.hardcode.gdbms.engine.values.Value)
     */
    public ISymbol getSymbolByValue(Object key) {
    	ISymbol symbol = null; 
    	if (key == null) {
    		symbol = nullValueSymbol;
    	}
    	else {
    		symbol = (ISymbol)symbols.get(key);
    	}
    	if (symbol == null && useDefaultSymbol) {
    		symbol = getDefaultSymbol();
    	}
    	return symbol;

    }

	public Object getSymbolKey(ISymbol symbol) {
		if (symbol != null) {
			for (Iterator<Entry<Object, ISymbol>> iterator = symbols.entrySet()
					.iterator(); iterator.hasNext();) {
				Entry<Object, ISymbol> entry = iterator.next();
				if (symbol.equals(entry.getValue())) {
					return entry.getKey();
				}
			}
		}
		return null;
	}

    public int getShapeType() {
        return shapeType;
    }

    public void useDefaultSymbol(boolean b) {
        useDefaultSymbol = b;
    }

    /**
	 * Devuelve si se utiliza o no el resto de valores para representarse.
	 * @return  True si se utiliza el resto de valores.
	 */
    public boolean isUseDefaultSymbol() {
        return useDefaultSymbol;
    }

    public void delSymbol(Object key) {
        keys.remove(key);

		ISymbol removedSymbol = (ISymbol) symbols.remove(key);
		if (removedSymbol != null){
			fireClassifiedSymbolChangeEvent(new SymbolLegendEvent(removedSymbol, null));
		}
    }

	public String getClassName() {
		return getClass().getName();
	}

	public void replace(ISymbol oldSymbol, ISymbol newSymbol) {
		if (symbols.containsValue(oldSymbol)) {
			Iterator<Object> it = symbols.keySet().iterator();
			while (it.hasNext()) {
				Object key = it.next();
				if (symbols.get(key).equals(oldSymbol)) {
					fireClassifiedSymbolChangeEvent(new SymbolLegendEvent(
							(ISymbol)symbols.put(key, newSymbol), newSymbol));
				}

			}
		}
	}
	public Color[] getColorScheme() {
		return selectedColors;
	}

	public void setColorScheme(Color[] cc) {
		 this.selectedColors = cc;
	}

	public Object clone() throws CloneNotSupportedException {
		VectorialUniqueValueLegend clone =
				(VectorialUniqueValueLegend) super.clone();

		// Clone default symbol
		if (defaultSymbol != null) {
			clone.defaultSymbol = (ISymbol) defaultSymbol.clone();
		}
		// Clone keys
		clone.keys = new ArrayList<Object>();
		
        // ====================================================
		// Temporarily remove listeners to prevent
		// cascade of notifications
		LegendContentsChangedListener[] list = this.getListeners();
		removeListeners(list);
        // ====================================================

		// Clone symbols
		if (symbols != null) {
			clone.symbols = createSymbolMap();
			for (Iterator<Entry<Object, ISymbol>> iterator =
					symbols.entrySet().iterator(); iterator.hasNext();) {
				Entry<Object, ISymbol> entry = iterator.next();
				ISymbol symbolClone =
						(ISymbol) ((ISymbol) entry.getValue()).clone();
				// Map keys are of type Number or String, so being
				// immutable objects, don't clone them
				clone.addSymbol(entry.getKey(), symbolClone);
			}
		}
		
        // ====================================================
        // Restore listeners
        addListeners(list);
        // ====================================================
		return clone;
	}


    private void addListeners(LegendContentsChangedListener[] list) {
        int len = list.length;
        for (int i=0; i<len; i++) {
            addLegendListener(list[i]);
        }
    }

    private void removeListeners(LegendContentsChangedListener[] list) {
        int len = list.length;
        for (int i=0; i<len; i++) {
            removeLegendListener(list[i]);
        }
    }

    private Map<Object, ISymbol> createSymbolMap() {
		return new TreeMap<Object, ISymbol>(
				new Comparator<Object>() { 
					public int compare(Object o1, Object o2) {
						if ((o1 != null) && (o2 != null)) {
							Object v2 = o2;
							Object v1 = o1;
							if (v1 instanceof Number && v2 instanceof Number) {
								return ((Number) v1).intValue()
										- ((Number) v2).intValue();
							}
							if (v1 instanceof String && v2 instanceof String) {
								return ((String) v1).compareTo(((String) v2));
							}
						}

						return 0;
					}
				});

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent properties
		super.loadFromState(state);
		// Set own properties
		keys = new ArrayList<Object>((List) state.get(FIELD_KEYS));
		nullValueSymbol = (ISymbol) state.get(FIELD_NULL_VALUE_SYMBOL);
		selectedColors =
				(Color[]) state.getArray(FIELD_SELECTED_COLORS, Color.class);
		Map persistedSymbolMap = (Map) state.get(FIELD_SYMBOLS);
		if (persistedSymbolMap != null) {
			symbols.putAll(persistedSymbolMap);
		}
		useDefaultSymbol(state.getBoolean(FIELD_USE_DEFAULT_SYMBOL));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_KEYS, keys);
		state.set(FIELD_NULL_VALUE_SYMBOL, nullValueSymbol);
		state.set(FIELD_SELECTED_COLORS, selectedColors);
		state.set(FIELD_USE_DEFAULT_SYMBOL, isUseDefaultSymbol());
		state.set(FIELD_SYMBOLS, symbols);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(VECTORIAL_UNIQUE_VALUE_LEGEND_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						VectorialUniqueValueLegend.class,
						VECTORIAL_UNIQUE_VALUE_LEGEND_PERSISTENCE_DEFINITION_NAME,
						VECTORIAL_UNIQUE_VALUE_LEGEND_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				// Extend the Classified Vector Legend base definition
				definition.extend(manager.getDefinition(CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME));

				// Keys
				definition.addDynFieldList(FIELD_KEYS).setClassOfItems(Object.class);
				// Null interval symbol
				definition.addDynFieldObject(FIELD_NULL_VALUE_SYMBOL).setClassOfValue(ISymbol.class);
				// Selected colors
				definition.addDynFieldList(FIELD_SELECTED_COLORS).setClassOfItems(Color.class);
				// Symbols
				definition.addDynFieldMap(FIELD_SYMBOLS).setClassOfItems(ISymbol.class);
				// Use default symbol?
				definition.addDynFieldBoolean(FIELD_USE_DEFAULT_SYMBOL);
			}
			return Boolean.TRUE;
		}
		
	}

	public static class RegisterLegend implements Callable {

		public Object call() throws Exception {
	        MapContextManager manager = MapContextLocator.getMapContextManager();
	        
            manager.registerLegend(IVectorialUniqueValueLegend.LEGEND_NAME,
                    VectorialUniqueValueLegend.class);

			return Boolean.TRUE;
		}
		
	}
}