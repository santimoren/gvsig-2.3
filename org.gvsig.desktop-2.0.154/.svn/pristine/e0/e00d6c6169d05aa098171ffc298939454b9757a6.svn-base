/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import java.awt.geom.AffineTransform;


import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.GeometryLocator;

public class Draw extends GeometryOperation{
	public static final String NAME = "draw";
	public static int CODE = Integer.MIN_VALUE;

	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		DrawOperationContext doc = (DrawOperationContext)ctx;
		AffineTransform at = doc.getViewPort().getAffineTransform();		
		doc.getSymbol().draw(doc.getGraphics(), at, geom, doc.getFeature(),
				doc.getCancellable());
		return null;
	}

	public int getOperationIndex() {
		return CODE;
	}

	public static void register() {
		
		GeometryManager geoMan = GeometryLocator.getGeometryManager();
		CODE = geoMan.getGeometryOperationCode(NAME);
		geoMan.registerGeometryOperation(NAME, new Draw());
	}

}
