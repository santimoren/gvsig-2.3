/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;

/**
 * Extensi�n encargada de limpiar la selecci�n.
 *
 */
public class ClearSelectionExtension extends Extension {


    public void execute(String s) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();
        if (s.equalsIgnoreCase("selection-clear-view")) {
            MapContext mapa = document.getMapContext();
            MapControl mapCtrl = view.getMapControl();
            FLayers layers = mapa.getLayers();
            boolean refresh = clearSelectionOfView(layers);
            if (refresh) {
                mapCtrl.drawMap(false);
            }
            document.setModified(true);
        }
    }

    private boolean clearSelectionOfView(FLayers layers) {
        boolean refresh = false;

        for (int i = 0; i < layers.getLayersCount(); i++) {
            FLayer lyr = layers.getLayer(i);
            if (lyr instanceof FLayers) {
                refresh = refresh || clearSelectionOfView((FLayers) lyr);
            } else if (lyr instanceof FLyrVect) {
                FLyrVect lyrVect = (FLyrVect) lyr;
                if (lyrVect.isActive()) {
                    try {
                        FeatureStore featureStore;

                        featureStore = ((FLyrVect) lyr).getFeatureStore();
                        if (!featureStore.getFeatureSelection().isEmpty()) {
                            refresh = true;
                        }
                        featureStore.getFeatureSelection().deselectAll();
                    } catch (ReadException e) {
                        e.printStackTrace();
                    } catch (DataException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        return refresh;
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return hasVectorLayersWithSelection(mapa.getLayers());
    }

    private boolean hasVectorLayersWithSelection(FLayers layers) {
        for (int i = 0; i < layers.getLayersCount(); i++) {
            FLayer lyr = layers.getLayer(i);
            if (lyr instanceof FLayers) {
                if (hasVectorLayersWithSelection((FLayers) lyr)) {
                    return true;
                }
            } else if (lyr instanceof FLyrVect) {
                FLyrVect lyrVect = (FLyrVect) lyr;
                if (lyrVect.isActive()) {
                    if (lyrVect.isAvailable()) {
                        try {
                            if (!lyrVect.getFeatureStore().getFeatureSelection().isEmpty()) {
                                return true;
                            }
                        } catch (DataException e) {
                            e.printStackTrace();
                            NotificationManager.addWarning("Capa " + lyrVect.getName() + " sin recordset correcto", e);
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
        IconThemeHelper.registerIcon("action", "edit-clear", this);
    }
}
