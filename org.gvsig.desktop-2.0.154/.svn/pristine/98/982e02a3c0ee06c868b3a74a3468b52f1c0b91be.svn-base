/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.ring;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geomgraph.Position;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.buffer.OffsetCurveBuilder;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.BaseLine3D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3D;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Ring;


/**
 * @author fdiaz
 *
 */
public class Ring3D extends BaseLine3D implements Ring {

    /**
     *
     */
    private static final long serialVersionUID = 9116088317343114306L;

    /**
     */
    public Ring3D() {
        super(Geometry.TYPES.RING);
    }

    /**
     * @param coordinates
     */
    public Ring3D(Coordinate[] coordinates) {
        super(Geometry.TYPES.RING, coordinates);
        closePrimitive();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Ring3D(cloneCoordinates().toCoordinateArray());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3D();
        multiLine.addPrimitive(new Line3D(coordinates.toCoordinateArray()));
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2D();
        multiPolygon.addPrimitive(new Polygon3D(coordinates.toCoordinateArray()));
        return multiPolygon;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSLinearRing(coordinates);
    }

    /* (non-Javadoc)
    * @see org.gvsig.fmap.geom.Geometry#offset(double)
    */
      public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
          com.vividsolutions.jts.geom.LinearRing jtsRing = (LinearRing) getJTS();
          GeometryFactory factory = jtsRing.getFactory();
          BufferParameters bufParams = JTSUtils.getBufferParameters();

          OffsetCurveBuilder ocb = new OffsetCurveBuilder(factory.getPrecisionModel(), bufParams);

          Coordinate[] coordinates = jtsRing.getCoordinates();
          Coordinate[] coords = ocb.getRingCurve(coordinates, Position.LEFT, distance); // .getOffsetCurve(coordinates,

          return new Ring3D(coords);
      }


      /* (non-Javadoc)
       * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#reProject(org.cresques.cts.ICoordTrans)
       */
      @Override
      public void reProject(ICoordTrans ct) {
          super.reProject(ct);
          if (coordinates.size()>=2 && !isClosed()) {
              closePrimitive();
          }
      }

      @Override
      public boolean equals(Object obj) {
          boolean res = super.equals(obj);
          if(res && obj instanceof Ring3D){
              Ring3D other = (Ring3D)obj;
              if(this.getNumVertices() != other.getNumVertices()){
                  return false;
              }
              for(int i=0; i < this.getNumVertices(); i++){
                  Coordinate coordinate = this.coordinates.get(i);
                  Coordinate otherCoordinate = other.coordinates.get(i);
                  if (otherCoordinate.getOrdinate(2) != coordinate.getOrdinate(2)) {
                      return false;
                  }
              }
              return true;
          } else {
              return false;
          }
      }

}
