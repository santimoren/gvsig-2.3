/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.api;

import org.gvsig.tools.locator.AbstractLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;
/**
 * This Locator provides the entry point for the gvSIG 
 * {@link TimeSupportSwingManager}
 * @see Locator
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 */
public class TimeSupportSwingLocator extends AbstractLocator{
	
	private static final String LOCATOR_NAME = "SwingSOSDalTimeLocator";
	/**
	 * SwingInstallerManager name used by the locator to access the instance
	 */
	public static final String SWING_MANAGER_NAME = "SwingSOSDalTimeManager";
	private static final String SWING_MANAGER_DESCRIPTION = "SwingSOSDalTime of gvSIG";

	/* Singleton */
	
	private static final TimeSupportSwingLocator instance = new TimeSupportSwingLocator();
	
	public static TimeSupportSwingLocator getInstance()
	{
		return instance;
	}
	
	public String getLocatorName() {
		// TODO Auto-generated method stub
		return LOCATOR_NAME;
	}

	/**
	 * Registers the Class implementing the {@link TimeSupportSwingManager} interface.
	 *
	 * @param clazz
	 *            implementing the SwingManager interface
	 */
	@SuppressWarnings(value = { "rawtypes" })
	public static void registerSwingDalTimeManager(Class clazz) {
		if(!TimeSupportSwingManager.class.isAssignableFrom(clazz))
		{
			throw new IllegalArgumentException(clazz.getName()
					+ " must implements the SwingDalTimeManager interface");
		}
			
			getInstance().register(SWING_MANAGER_NAME, 
					SWING_MANAGER_DESCRIPTION,
					clazz);
	}
	
	/**
	 * Return a reference to {@link TimeSupportSwingManager}.
	 *
	 * @return a reference to SwingDalTimeManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static TimeSupportSwingManager getSwingManager() throws LocatorException {
		return (TimeSupportSwingManager) getInstance().get(SWING_MANAGER_NAME);
	}
	
}
