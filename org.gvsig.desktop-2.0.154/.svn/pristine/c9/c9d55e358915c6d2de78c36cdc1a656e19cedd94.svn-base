/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata;

import java.util.Set;

import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.dynobject.DynObject;

/**
 * Metadata is information or data about data {@link http
 * ://en.wikipedia.org/wiki/Metadata}.
 * <p>
 * This interface extends DynObject to add anything needed over the DynObject
 * model to be able to be used as a Metadata model.
 * </p>
 * 
 * @author gvSIG Team
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public interface Metadata extends DynObject {

	/**
	 * Returns the unique identifier of the Metadata.
	 * 
	 * @return the Metadata identifier
	 */
	public Object getMetadataID() throws MetadataException;

	/**
	 * Returns the name of the Metadata, which allows to identify the type in
	 * the Metadata registry.
	 * 
	 * @return the Metadata name
	 */
	public String getMetadataName() throws MetadataException;

	/**
	 * Returns an unsorted set of child Metadata objects.
	 * 
	 * @return an unsorted set of child Metadata objects
	 */
	public Set getMetadataChildren() throws MetadataException;

}
