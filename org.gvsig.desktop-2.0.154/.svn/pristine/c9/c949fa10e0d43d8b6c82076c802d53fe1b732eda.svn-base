/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.impl;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextDrawer;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.MapContextRuntimeException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayerFactory;
import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.impl.DefaultGraphicLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialUniqueValueLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendReader;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendWriter;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynMethodNotSupportedException;
import org.gvsig.tools.observer.Notification;
import org.gvsig.tools.observer.ObservableHelper;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the {@link MapContextManager}.
 *
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class DefaultMapContextManager implements MapContextManager {

    private static final Logger logger = LoggerFactory
            .getLogger(DefaultMapContextManager.class);

    private Class drawerClazz = DefaultMapContextDrawer.class;

    private Map legends = Collections.synchronizedMap(new HashMap());

    private Map legendReaders = Collections.synchronizedMap(new HashMap());

    private Map legendWriters = Collections.synchronizedMap(new HashMap());

    private String defaultVectorLegend;

    private ObservableHelper observableHelper = new ObservableHelper();

    private File colorTableLibraryFolder = null;

    public MapContext createMapContext() {
        MapContext mapcontext = new MapContext(new ViewPort());
        return (MapContext) notifyObservers(CREATE_MAPCONTEXT, mapcontext).getValue();
    }

    public SymbolManager getSymbolManager() {
        return MapContextLocator.getSymbolManager();
    }

    private SymbolPreferences getSymbolPreferences() {
        return getSymbolManager().getSymbolPreferences();
    }

    public String getSymbolLibraryPath() {
        return getSymbolPreferences().getSymbolLibraryPath();
    }

    public void setSymbolLibraryPath(String symbolLibraryPath) {
        getSymbolPreferences().setSymbolLibraryPath(symbolLibraryPath);
    }

    public void resetSymbolLibraryPath() {
        getSymbolPreferences().resetSymbolLibraryPath();
    }

    public Color getDefaultSymbolColor() {
        return getSymbolPreferences().getDefaultSymbolColor();
    }

    public Color getDefaultSymbolFillColor() {
        return getSymbolPreferences().getDefaultSymbolFillColor();
    }

    public Font getDefaultSymbolFont() {
        return getSymbolPreferences().getDefaultSymbolFont();
    }

    public String getSymbolFileExtension() {
        return getSymbolPreferences().getSymbolFileExtension();
    }

    public boolean isDefaultSymbolFillColorAleatory() {
        return getSymbolPreferences().isDefaultSymbolFillColorAleatory();
    }

    public void resetDefaultSymbolColor() {
        getSymbolPreferences().resetDefaultSymbolColor();
    }

    public void resetDefaultSymbolFillColor() {
        getSymbolPreferences().resetDefaultSymbolFillColor();
    }

    public void resetDefaultSymbolFillColorAleatory() {
        getSymbolPreferences().resetDefaultSymbolFillColorAleatory();
    }

    public void resetDefaultSymbolFont() {
        getSymbolPreferences().resetDefaultSymbolFont();
    }

    public void setDefaultSymbolColor(Color defaultSymbolColor) {
        getSymbolPreferences().setDefaultSymbolColor(defaultSymbolColor);
    }

    public void setDefaultSymbolFillColor(Color defaultSymbolFillColor) {
        getSymbolPreferences().setDefaultSymbolFillColor(defaultSymbolFillColor);
    }

    public void setDefaultSymbolFillColorAleatory(
            boolean defaultSymbolFillColorAleatory) {
        getSymbolPreferences().setDefaultSymbolFillColorAleatory(
                defaultSymbolFillColorAleatory);
    }

    public void setDefaultSymbolFont(Font defaultSymbolFont) {
        getSymbolPreferences().setDefaultSymbolFont(defaultSymbolFont);
    }

    public void setSymbolFileExtension(String extension) {
        getSymbolPreferences().setSymbolFileExtension(extension);
    }

    public int getDefaultCartographicSupportMeasureUnit() {
        return getSymbolPreferences().getDefaultCartographicSupportMeasureUnit();
    }

    public void setDefaultCartographicSupportMeasureUnit(
            int defaultCartographicSupportMeasureUnit) {
        getSymbolPreferences().setDefaultCartographicSupportMeasureUnit(
                defaultCartographicSupportMeasureUnit);
    }

    public int getDefaultCartographicSupportReferenceSystem() {
        return getSymbolPreferences().getDefaultCartographicSupportReferenceSystem();
    }

    public void setDefaultCartographicSupportReferenceSystem(
            int defaultCartographicSupportReferenceSystem) {
        getSymbolPreferences().setDefaultCartographicSupportReferenceSystem(
                defaultCartographicSupportReferenceSystem);
    }

    public MapContextDrawer createMapContextDrawerInstance(Class drawerClazz)
            throws MapContextException {
        return createMapContextDrawerInstance(drawerClazz, "NONE");
    }

    public MapContextDrawer createDefaultMapContextDrawerInstance()
            throws MapContextException {

        return createMapContextDrawerInstance(drawerClazz, "default");
    }

    private MapContextDrawer createMapContextDrawerInstance(Class drawerClazz,
            String name) throws RegisteredClassInstantiationException {
        try {
            MapContextDrawer drawer = (MapContextDrawer) drawerClazz.newInstance();
            notifyObservers(CREATE_MAPCONTEXT_DRAWER, drawer);
            return drawer;
        } catch (Exception ex) {
            throw new RegisteredClassInstantiationException(
                    MapContextDrawer.class, drawerClazz, name, ex);
        }
    }

    public void setDefaultMapContextDrawer(Class drawerClazz)
            throws MapContextException {

        validateMapContextDrawer(drawerClazz);
        this.drawerClazz = drawerClazz;
        notifyObservers(SET_MAPCONTEXT_DRAWER, drawerClazz);
    }

    public void validateMapContextDrawer(Class drawerClazz)
            throws MapContextException {
        if (!MapContextDrawer.class.isAssignableFrom(drawerClazz)) {
            throw new InvalidRegisteredClassException(MapContextDrawer.class,
                    drawerClazz, "UNKNOWN");
        }
    }

    public GraphicLayer createGraphicsLayer(IProjection projection) {
        DefaultGraphicLayer layer = new DefaultGraphicLayer();
        try {
            layer.initialize(projection);
            layer.setLegend((IVectorLegend) createLegend(IVectorialUniqueValueLegend.LEGEND_NAME));
        } catch (Exception e) {
            logger.error("Error initializing the graphics layer", e);
        }
        return (GraphicLayer) notifyObservers(CREATE_GRAPHICS_LAYER, layer).getValue();
    }

    public String getDefaultVectorLegend() {
        return defaultVectorLegend;
    }

    public void setDefaultVectorLegend(String defaultVectorLegend) {
        this.defaultVectorLegend = defaultVectorLegend;
    }

    public void registerLegend(String legendName, Class legendClass)
            throws MapContextRuntimeException {

        if (legendClass == null || !ILegend.class.isAssignableFrom(legendClass)) {
            throw new InvalidRegisteredClassException(ILegend.class,
                    legendClass, legendName);
        }

        legends.put(legendName, legendClass);
        notifyObservers(REGISTER_LEGEND, legendName, legendClass);
    }

    public ILegend createLegend(String legendName)
            throws MapContextRuntimeException {
        Class legendClass = (Class) legends.get(legendName);

        if (legendClass != null) {
            try {
                ILegend legend = (ILegend) legendClass.newInstance();
                return (ILegend) notifyObservers(CREATE_LEGEND, legend).getValue();
            } catch (InstantiationException e) {
                throw new RegisteredClassInstantiationException(ILegend.class,
                        legendClass, legendName, e);
            } catch (IllegalAccessException e) {
                throw new RegisteredClassInstantiationException(ILegend.class,
                        legendClass, legendName, e);
            }
        }
        return null;
    }

    public IVectorLegend createDefaultVectorLegend(int shapeType)
            throws MapContextRuntimeException {
        try {
            // Create legend
            IVectorLegend legend
                    = (IVectorLegend) createLegend(getDefaultVectorLegend());
            if (legend == null) {
                return null;
            }
            // Set legend values
            legend.setShapeType(shapeType);
            ISymbol symbol = getSymbolManager().createSymbol(shapeType);
            if (symbol == null) {
                String msg = "Can't create a legend for the shape type " + shapeType + ". The type can be incorrect or there is not registered a symbol by default for that value. If this a was obtained from the store settings, review your FeatureType have correctly configured this value.";
                throw new RuntimeException(msg);
            }
            legend.setDefaultSymbol(symbol);
            return legend;
        } catch (Exception e) {
            throw new MapContextRuntimeException(e);
        }
    }

    // =============================================================
    // Legend reading/writing
    public void registerLegendReader(String format, Class readerClass)
            throws MapContextRuntimeException {
        if (readerClass == null
                || !ILegendReader.class.isAssignableFrom(readerClass)) {
            throw new InvalidRegisteredClassException(ILegendReader.class,
                    readerClass, format);
        }

        legendReaders.put(format, readerClass);
        notifyObservers(REGISTER_LEGEND_READER, format, readerClass);
    }

    public ILegendReader createLegendReader(String format)
            throws MapContextRuntimeException {
        Class legendReaderClazz = (Class) legendReaders.get(format);

        if (legendReaderClazz != null) {
            try {
                ILegendReader reader = (ILegendReader) legendReaderClazz.newInstance();
                return (ILegendReader) notifyObservers(CREATE_LEGEND_READER, reader).getValue();
            } catch (InstantiationException e) {
                throw new RegisteredClassInstantiationException(
                        ILegendReader.class, legendReaderClazz, format, e);
            } catch (IllegalAccessException e) {
                throw new RegisteredClassInstantiationException(
                        ILegendReader.class, legendReaderClazz, format, e);
            }
        }
        return null;
    }

    public void registerLegendWriter(Class legendClass, String format,
            Class writerClass) throws MapContextRuntimeException {
        if (writerClass == null
                || !ILegendWriter.class.isAssignableFrom(writerClass)
                || legendClass == null
                || !ILegend.class.isAssignableFrom(legendClass)) {

            throw new InvalidRegisteredClassException(ILegendWriter.class,
                    writerClass, format.concat("-").concat(
                            legendClass == null ? "Null" : legendClass.getName()));
        }

        Map legendFormatWriters = (Map) legendWriters.get(format);

        synchronized (legendWriters) {
            if (legendFormatWriters == null) {
                legendFormatWriters = Collections.synchronizedMap(new HashMap());
                legendWriters.put(format, legendFormatWriters);
            }
        }
        legendFormatWriters.put(legendClass, writerClass);
        notifyObservers(REGISTER_LEGEND_WRITER, format, writerClass);
    }

    public ILegendWriter createLegendWriter(Class legendClass, String format)
            throws MapContextRuntimeException {

        if (legendClass == null || format == null) {
            return null;
        }

        Map legendFormatWriters = getLegendWritersForFormat(format);

        if (legendFormatWriters != null) {
            Class legendWriterClazz = (Class) legendFormatWriters
                    .get(legendClass);

            if (legendWriterClazz != null) {
                /*
                 * Found exact match
                 */
                try {
                    ILegendWriter writer = (ILegendWriter) legendWriterClazz.newInstance();
                    return (ILegendWriter) notifyObservers(CREATE_LEGEND_READER, writer).getValue();
                } catch (InstantiationException e) {
                    throw new RegisteredClassInstantiationException(
                            ILegendWriter.class, legendWriterClazz, format
                            .concat("-").concat(
                                    legendClass == null ? "Null" : legendClass.getName()), e);
                } catch (IllegalAccessException e) {
                    throw new RegisteredClassInstantiationException(
                            ILegendWriter.class, legendWriterClazz, format
                            .concat("-").concat(
                                    legendClass == null ? "Null" : legendClass.getName()), e);
                }
            } else {
                /*
                 * Trying to find superclass/superinterface of parameter
                 */
                try {
                    return getSuperClassLegendWriter(legendFormatWriters, legendClass);
                } catch (Exception exc) {
                    throw new MapContextRuntimeException(exc);
                }
            }
        }
        return null;
    }

    private ILegendWriter getSuperClassLegendWriter(Map clsToWtr, Class legclass)
            throws Exception {

        if (!ILegend.class.isAssignableFrom(legclass)) {
            // Class is not a legend
            return null;
        }

        Iterator kiter = clsToWtr.keySet().iterator();
        Object oitem = null;
        Class citem = null;
        while (kiter.hasNext()) {
            oitem = kiter.next();
            if (oitem instanceof Class) {
                citem = (Class) oitem;
                if (citem.isAssignableFrom(legclass)) {
                    /*
                     * Found superclass/superinterface
                     */
                    citem = (Class) clsToWtr.get(oitem);
                    return (ILegendWriter) citem.newInstance();
                }
            }
        }
        /*
         * No superclass/superinterface found
         */
        return null;
    }

    private Map getLegendWritersForFormat(String format) {
        return (Map) legendWriters.get(format);
    }

    public List getLegendReadingFormats() {
        List resp = new ArrayList();
        Iterator iter = legendReaders.keySet().iterator();
        while (iter.hasNext()) {
            resp.add(iter.next());
        }
        return resp;
    }

    public List getLegendWritingFormats() {
        List resp = new ArrayList();
        Iterator iter = legendWriters.keySet().iterator();
        while (iter.hasNext()) {
            resp.add(iter.next());
        }
        return resp;
    }
    // =============================================================

    public IMultiLayerSymbol createMultiLayerSymbol(int shapeType)
            throws MapContextRuntimeException {
        IMultiLayerSymbol symbol = getSymbolManager().createMultiLayerSymbol(shapeType);
        return (IMultiLayerSymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public IMultiLayerSymbol createMultiLayerSymbol(String symbolName)
            throws MapContextRuntimeException {
        IMultiLayerSymbol symbol = getSymbolManager().createMultiLayerSymbol(symbolName);
        return (IMultiLayerSymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public ISymbol createSymbol(int shapeType, Color color)
            throws MapContextRuntimeException {
        ISymbol symbol = getSymbolManager().createSymbol(shapeType, color);
        return (ISymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public ISymbol createSymbol(int shapeType)
            throws MapContextRuntimeException {
        ISymbol symbol = getSymbolManager().createSymbol(shapeType);
        return (ISymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public ISymbol createSymbol(String symbolName, Color color)
            throws MapContextRuntimeException {
        ISymbol symbol = getSymbolManager().createSymbol(symbolName, color);
        return (ISymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public ISymbol createSymbol(String symbolName)
            throws MapContextRuntimeException {
        ISymbol symbol = getSymbolManager().createSymbol(symbolName);
        return (ISymbol) notifyObservers(CREATE_SYMBOL, symbol).getValue();
    }

    public IWarningSymbol getWarningSymbol(String message, String symbolDesc,
            int symbolDrawExceptionType) throws MapContextRuntimeException {
        return getSymbolManager().getWarningSymbol(message, symbolDesc,
                symbolDrawExceptionType);
    }

    public ISymbol[] loadSymbols(File folder, FileFilter filter)
            throws SymbolException {
        ISymbol[] symbols = getSymbolManager().loadSymbols(folder, filter);
        return (ISymbol[]) notifyObservers(LOAD_SYMBOLS, symbols).getValue();
    }

    public ISymbol[] loadSymbols(File folder) throws SymbolException {
        ISymbol[] symbols = getSymbolManager().loadSymbols(folder);
        return (ISymbol[]) notifyObservers(LOAD_SYMBOLS, symbols).getValue();
    }

    public void registerMultiLayerSymbol(String symbolName, Class symbolClass)
            throws MapContextRuntimeException {
        getSymbolManager().registerMultiLayerSymbol(symbolName, symbolClass);
        notifyObservers(REGISTER_MULTILAYER_SYMBOL, symbolName, symbolClass);
    }

    public void registerMultiLayerSymbol(String symbolName, int[] shapeTypes,
            Class symbolClass) throws MapContextRuntimeException {
        getSymbolManager().registerMultiLayerSymbol(symbolName, shapeTypes,
                symbolClass);
        notifyObservers(REGISTER_MULTILAYER_SYMBOL, symbolName, symbolClass, shapeTypes);
    }

    public void registerSymbol(String symbolName, Class symbolClass)
            throws MapContextRuntimeException {
        getSymbolManager().registerSymbol(symbolName, symbolClass);
        notifyObservers(REGISTER_SYMBOL, symbolName, symbolClass);
    }

    public void registerSymbol(String symbolName, int[] shapeTypes,
            Class symbolClass) throws MapContextException {
        getSymbolManager().registerSymbol(symbolName, shapeTypes, symbolClass);
        notifyObservers(REGISTER_SYMBOL, symbolName, symbolClass, shapeTypes);
    }

    public void saveSymbol(ISymbol symbol, String fileName, File folder,
            boolean overwrite) throws SymbolException {
        getSymbolManager().saveSymbol(symbol, fileName, folder, overwrite);
    }

    public void saveSymbol(ISymbol symbol, String fileName, File folder)
            throws SymbolException {
        getSymbolManager().saveSymbol(symbol, fileName, folder);
    }

    public FLayer createLayer(String layerName, DataStoreParameters parameters)
            throws LoadLayerException {
        FLayer layer = LayerFactory.getInstance().createLayer(layerName, parameters);
        return (FLayer) notifyObservers(CREATE_LAYER, layer).getValue();
    }

    public FLayer createLayer(String layerName, DataStore store)
            throws LoadLayerException {
        FLayer layer = LayerFactory.getInstance().createLayer(layerName, store);
        return (FLayer) notifyObservers(CREATE_LAYER, layer).getValue();
    }

    public ILegend getLegend(DataStore dataStore) {
        ILegend legend = null;

        File file = getResourcePath(dataStore, SymbolManager.LEGEND_FILE_EXTENSION.substring(1));
        try {
            if ((file != null) && (file.exists())) {
                PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();
                FileInputStream is = new FileInputStream(file);
                legend = (ILegend) persistenceManager.getObject(is);
                is.close();
            }
        } catch (FileNotFoundException e) {
            logger.error("Legend not found", e);
        } catch (IOException e) {
            logger.error("Error reading the legend", e);
        }

        //If the legend is null, next option is to check if the store has the getLegend method
        if (legend == null) {
            try {
                legend = (IVectorLegend) dataStore.invokeDynMethod("getLegend", null);
            } catch (DynMethodNotSupportedException e) {
                logger.debug("This store {} does not provide a legend.",
                        dataStore.getName());
            } catch (DynMethodException e) {
                logger.error(
                        "Can't load the specific legend provided for the store {}.",
                        dataStore.getName(), e);
            }
        }

        //If legend is null, last step is just try to create the legend by default
        if (legend == null) {
            FeatureType featureType;
            try {
                featureType = (((FeatureStore) dataStore).getDefaultFeatureType());
                int indexGeom = featureType.getDefaultGeometryAttributeIndex();
                if (indexGeom < 0) {
                    throw new IllegalArgumentException("The layer don't has a geometry column.");
                }
                int typeShape = featureType.getAttributeDescriptor(indexGeom).getGeometryType();
                legend = createDefaultVectorLegend(typeShape);
            } catch (DataException e) {
                logger.error("Error getting the default feature type", e);
            }
        }

        return legend;
    }

    public ILabelingStrategy getLabelingStrategy(DataStore dataStore) {
        ILabelingStrategy labelingStrategy = null;

        File file = getResourcePath(dataStore, SymbolManager.LABELINGSTRATEGY_FILE_EXTENSION.substring(1));
        try {
            if ((file != null) && (file.exists())) {
                PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();
                FileInputStream is = new FileInputStream(file);
                labelingStrategy = (ILabelingStrategy) persistenceManager.getObject(is);
                is.close();
            }
        } catch (FileNotFoundException e) {
            logger.error("Label strategy not found", e);
        } catch (IOException e) {
            logger.error("Error reading the labeling strategy", e);
        }

        //If the legend is null, next option is to check if the store has the getLegend method
        if (labelingStrategy == null) {
            try {
                labelingStrategy
                        = (ILabelingStrategy) dataStore.invokeDynMethod("getLabeling",
                                null);
            } catch (DynMethodNotSupportedException e1) {
                labelingStrategy = null;
            } catch (DynMethodException e1) {
                logger.error("Can't load the specific lebeling strategy provided for the datastore {}.",
                        dataStore.getName(),
                        e1);
            }
        }

        return labelingStrategy;
    }

    private Object call(Object instance, String methodName, Class[] signature, Object[] params) {
        try {
            Method method = instance.getClass().getMethod(methodName, signature);
            if (method == null) {
                return null;
            }
            Object value = method.invoke(instance, params);
            return value;
        } catch (NoSuchMethodException ex) {
            return null;
        } catch (SecurityException ex) {
            return null;
        } catch (IllegalAccessException ex) {
            return null;
        } catch (IllegalArgumentException ex) {
            return null;
        } catch (InvocationTargetException ex) {
            return null;
        }
    }

    private File getResourcePath(DataStore dataStore, String resource) {
        //Loading the file from a store based on file
        DataServerExplorer explorer = null;
        try {
            explorer = dataStore.getExplorer();
            if (explorer == null) {
                return null;
            }
            return explorer.getResourcePath(dataStore, resource);
        } catch (Exception e) {
            logger.warn(
                    "Can't locate a specific legend provided by the explorer "
                    + explorer, e);
            return null;
        } finally {
            DisposeUtils.disposeQuietly(explorer);
        }
    }

    private Map iconLayers = new HashMap(); //  (Map<String storeProviderName, String iconName>)

    public void registerIconLayer(String storeProviderName, String iconName) {
        if (storeProviderName == null || iconName == null) {
            logger.info("registerIconLayer, storeProviderName or iconName are null");
            return;
        }
        String storeName = storeProviderName.trim().toLowerCase();
        if (storeName.length() == 0 || iconName.trim().length() == 0) {
            logger.info("registerIconLayer, invalid storeProviderName or iconName");
            return;
        }
        iconLayers.put(storeName, iconName);
        notifyObservers(REGISTER_ICON_LAYER, storeName, iconName);
    }

    public String getIconLayer(DataStore store) {
        String name = (String) iconLayers.get(store.getProviderName().trim().toLowerCase());
        if (name == null) {
            return "layer-icon";
        }
        return name;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.mapcontext.MapContextManager#getDefaultCRS()
     */
    public IProjection getDefaultCRS() {
        IProjection crs = CRSFactory.getCRS("EPSG:4326");
        return (IProjection) notifyObservers(GET_DEFAULT_CRS, crs).getValue();
    }

    public Notification notifyLoadMapContext(MapContext mapContext) {
        return this.observableHelper.notifyObservers(this, LOAD_MAPCONTEXT, mapContext);
    }

    public Notification notifyLoadLayer(FLayer layer) {
        return this.observableHelper.notifyObservers(this, LOAD_LAYER, layer);
    }

    public void addObserver(Observer o) {
        this.observableHelper.addObserver(o);
    }

    public void deleteObserver(Observer o) {
        this.observableHelper.deleteObserver(o);
    }

    public void deleteObservers() {
        this.observableHelper.deleteObservers();
    }

    protected Notification notifyObservers(String type, Object value) {
        return this.observableHelper.notifyObservers(this, type, value);
    }

    protected Notification notifyObservers(String type, Object value1, Object value2) {
        return this.observableHelper.notifyObservers(this, type, value1, value2);
    }

    protected Notification notifyObservers(String type, Object value1, Object value2, Object value3) {
        return this.observableHelper.notifyObservers(this, type, value1, value2, value3);
    }

    public File getColorTableLibraryFolder() {
        if (this.colorTableLibraryFolder == null) {
            // Provide a default value to the location for the color
            // table library.
            String colorTableLibraryPath = System.getProperty("user.home")
                    + File.separator
                    + "gvSIG"
                    + File.separator
                    + "colortable";
            this.colorTableLibraryFolder = new File(colorTableLibraryPath);
        }
        return this.colorTableLibraryFolder;
    }

    public void setColorTableLibraryFolder(File colorTableLibraryFolder) {
        this.colorTableLibraryFolder = colorTableLibraryFolder;
    }

}
