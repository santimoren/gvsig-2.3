/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.CopyParametersException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceNotification;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.PrepareResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyChangesException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyCloseException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyDisposeException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyOpenException;
import org.gvsig.fmap.dal.resource.impl.DefaultResourceNotification;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.WeakReferencingObservable;
import org.gvsig.tools.observer.impl.BaseWeakReferencingObservable;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Base implementation for Resource
 * </p>
 *
 * <p>
 * This implementation not define the {@link Resource#begin()} and
 * {@link Resource#end()}
 * </p>
 *
 * @author jmvivo
 *
 */
public abstract class AbstractResource implements ResourceProvider,
		WeakReferencingObservable {

        private static Logger logger = LoggerFactory.getLogger(AbstractResource.class);

	private DelegateWeakReferencingObservable delegateObservable;

	private List consumers;

	private long lastTimeOpen;
	private long lastTimeUsed;

	private ResourceParameters parameters;
	private ResourceParameters preparedParameters;

	private Object data;

	private int openCount;

	/**
	 * Number of times an execute is being simultaneously performed with this
	 * resource.
	 */
	private int executeCount = 0;

	protected final Object lock;

	protected Object multiResourcelock;

	protected AbstractResource(ResourceParameters parameters)
			throws InitializeException {
		consumers = new ArrayList();
		lastTimeOpen = System.currentTimeMillis();
		lastTimeUsed = lastTimeOpen;

		openCount = 0;

		preparedParameters = null;
		delegateObservable = new DelegateWeakReferencingObservable(this);
		lock = new Object();
		multiResourcelock = new Object();
		try {
			this.parameters = (ResourceParameters) parameters.getCopy();
		} catch (CopyParametersException e) {
			throw new InitializeException(e);
		}

	}

	protected final void updateLastTimeUsed() {
		lastTimeUsed = System.currentTimeMillis();
	}

	protected final void updateLastTimeOpen() {
		lastTimeOpen = System.currentTimeMillis();
		lastTimeUsed = lastTimeOpen;
	}

	public final long getLastTimeOpen() {
		return lastTimeOpen;
	}

	public final long getLastTimeUsed() {
		return lastTimeUsed;
	}

	public final ResourceParameters getParameters() {
		if (preparedParameters != null) {
			return preparedParameters;
		}
		return this.parameters;
	}

	public void prepare(ResourceParameters params)
			throws PrepareResourceException {
		ResourceNotification notification =
				new DefaultResourceNotification(this,
						ResourceNotification.PREPARE, params);
		this.delegateObservable.notifyObservers(notification);
	}

	public void prepare() throws PrepareResourceException {
		if (preparedParameters == null) {
			try {
				ResourceParameters params =
						(ResourceParameters) parameters.getCopy();
				prepare(params);
				preparedParameters = params;
			} catch (CopyParametersException e) {
				throw new PrepareResourceException(this, e);
			}
		}

	}

	public void notifyOpen() throws ResourceNotifyOpenException {
		this.notifyObserver(ResourceNotification.OPEN);
		updateLastTimeOpen();
		openCount++;
	}

	public void notifyClose() throws ResourceNotifyCloseException {
		if (openCount <= 0) {
                    try {
                        throw new IllegalStateException();
                    } catch(IllegalStateException ex) {
                        this.
                        logger.warn("notify close in a resource already closed ("+this.parameters.toString()+").", ex);
                    }
                    this.notifyObserver(ResourceNotification.CLOSE);
		} else {
                    this.notifyObserver(ResourceNotification.CLOSE);
                    openCount--;
                }
	}

	public void notifyChanges() throws ResourceNotifyChangesException {
		this.notifyObserver(ResourceNotification.CHANGED);

		Iterator it = consumers.iterator();
		while (it.hasNext()) {
			ResourceConsumer consumer =
					(ResourceConsumer) ((WeakReference) it.next()).get();
			if (consumer != null) {
				consumer.resourceChanged(this);
			} else {
				it.remove();
			}
		}
	}

	public boolean isOpen() {
		return this.openCount > 0;
	}

	public int openCount() {
		return this.openCount;
	}

	public void addObserver(Observer o) {
		this.delegateObservable.addObserver(o);
	}

	public void deleteObserver(Observer o) {
		this.delegateObservable.deleteObserver(o);
	}

	public void deleteObservers() {
		this.delegateObservable.deleteObservers();

	}

	public final void addObservers(BaseWeakReferencingObservable observers) {
		this.delegateObservable.addObservers(observers);
	}

	public final void addConsumer(ResourceConsumer consumer) {
		this.updateConsumersList();
		consumers.add(new WeakReference(consumer));
	}

	public final void removeConsumer(ResourceConsumer consumer) {
		ResourceConsumer cur;
		Iterator it = consumers.iterator();
		while (it.hasNext()) {
			cur = (ResourceConsumer) ((WeakReference) it.next()).get();
			if (cur == null || (cur == consumer)) {
				it.remove();
			}
		}

	}

	public int getConsumersCount() {
		this.updateConsumersList();
		return consumers.size();
	}

	public ResourceConsumer getConsumerAt(int i){
	    this.updateConsumersList();
	    if (consumers==null){
	        return null;
	    }else{
	        return (ResourceConsumer)consumers.get(i);
	    }
	}

	private synchronized void updateConsumersList() {
		Iterator it = consumers.iterator();
		WeakReference ref;
		while (it.hasNext()) {
			ref = (WeakReference) it.next();
			if (ref.get() == null) {
				it.remove();
			}
		}
	}

	public void closeRequest() throws ResourceException {
		if (inUse()) {
			return;
		}

		if (consumers != null) {
			Iterator it = consumers.iterator();
			while (it.hasNext()) {
				ResourceConsumer consumer =
						(ResourceConsumer) ((WeakReference) it.next()).get();
				if (consumer != null) {
					consumer.closeResourceRequested(this);
				} else {
					it.remove();
				}
			}
		}
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getData() {
		return this.data;
	}

	protected void notifyObserver(String type) {
		if (delegateObservable != null) {
			this.delegateObservable.notifyObservers(new DefaultResourceNotification(
					this, type));
		}
	}

	public void notifyDispose() throws ResourceNotifyDisposeException {
		this.notifyObserver(ResourceNotification.DISPOSE);

		if (consumers != null) {
			consumers.clear();
		}

		lastTimeOpen = 0l;
		lastTimeUsed = 0l;

		data = null;

		if (delegateObservable != null) {
			delegateObservable.deleteObservers();
		}
	}

	public final boolean inUse() {
		return executeCount > 0;
	}

	public Object execute(ResourceAction action)
			throws ResourceExecuteException {
		Object value = null;
		synchronized (lock) {
		    synchronized (multiResourcelock) {
    			executeBegins();
    			try {
    				value = performExecution(action);
    			} catch (Exception e) {
    				throw new ResourceExecuteException(this, e);
    			} finally {
    				executeEnds();
    			}
		    }
		}
		return value;
	}

	protected Object performExecution(ResourceAction action) throws Exception {
		return action.run();
	}

	protected final void executeBegins() {
		executeCount++;
	}

	protected final void executeEnds() {
		updateLastTimeUsed();
		executeCount--;
	}

	/**
	 * Returns the name of the {@link Resource}.
	 *
	 * @throws AccessResourceException
	 *             if there is an error while accessing the resource
	 */
	public abstract String getName() throws AccessResourceException;

	/**
	 * Returns the real resource represented by this object.
	 *
	 * @throws AccessResourceException
	 *             if there is an error while accessing the resource
	 */
	public abstract Object get() throws AccessResourceException;

}
