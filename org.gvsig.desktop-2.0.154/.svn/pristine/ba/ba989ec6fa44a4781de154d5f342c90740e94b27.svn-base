/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.api;

import java.awt.Component;

import org.gvsig.metadata.Metadata;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.DynFormManager;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
//import org.gvsig.tools.swing.api.dynobject.JDynObjectComponent;

/**
 *
 * The metadata editor panel.
 *
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 *
 */
public abstract class JMetadataPanel extends AbstractSimpleMetadataPanel
        implements MetadataPanel {

    private static final long serialVersionUID = 3029756241879733963L;

    public JMetadataPanel(MetadataSwingManager uiManager, Metadata metadata,
            boolean editable) {
        super(uiManager, metadata, editable);
    }

    protected JDynForm createJDynObjectComponent(DynClass dynClass,
            Metadata metadata, boolean isEditable) throws ServiceException {
        DynFormManager manager = DynFormLocator.getDynFormManager();
        JDynForm form = manager.createJDynForm(dynClass);
        if (metadata != null) {
            form.setValues(metadata);
        }
        form.setReadOnly(!isEditable);
        return form;
    }

    /**
     * Saves the current status of the GUI into the Metadata object
     *
     * @throws DynObjectValidateException
     *
     */
    public abstract void saveMetadata() throws DynObjectValidateException;

    public void save() {
        try {
            saveMetadata();
        } catch (DynObjectValidateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public DynField getDynField() {
        // TODO Auto-generated method stub
        return null;
    }

    public Component asJComponent() {
        return this;
    }

}
