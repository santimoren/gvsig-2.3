/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing.impl;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;

import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.gvsig.annotation.AnnotationCreationException;
import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationCreationServiceException;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.annotation.swing.JAnnotationCreationServicePanel;
import org.gvsig.annotation.swing.impl.wizard.AnnotationProgressWizard;
import org.gvsig.annotation.swing.impl.wizard.MainOptionsWizard;
import org.gvsig.annotation.swing.impl.wizard.OptionalOptionsWizard;
import org.gvsig.annotation.swing.impl.wizard.SelectOutputFileWizard;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.gvsig.tools.task.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation for the {@link JAnnotationCreationServicePanel}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultJAnnotationCreationServicePanel extends JAnnotationCreationServicePanel implements WizardPanel{

	private static final Logger LOG =
		LoggerFactory.getLogger(DefaultJAnnotationCreationServicePanel.class);

	private static final long serialVersionUID = 2965442763236823977L;

	private AnnotationCreationService annotationCreationService;
	private AnnotationSwingManager annotationSwingManager;

	private WizardPanelWithLogo wizardPanelWithLogo = null;

	private MainOptionsWizard mainOptionsWizard = null;
	private OptionalOptionsWizard optionalOptionsWizard = null;
	private SelectOutputFileWizard selectOutputFileWizard = null;
	private AnnotationProgressWizard annotationProgressWizard = null;
		
	private WizardListenerAdapter wizardListenerAdapter = null;

	public DefaultJAnnotationCreationServicePanel(
			DefaultAnnotationSwingManager annotationSwingManager, AnnotationCreationService annotationCreationService) throws ServiceException {

		super();

        IconThemeManager themeManager = ToolsSwingLocator.getIconThemeManager();
		wizardPanelWithLogo = new WizardPanelWithLogo(themeManager.getCurrent().get("wizard-annotation"));

		this.annotationCreationService = annotationCreationService;
		this.annotationSwingManager = annotationSwingManager;

		try {
			mainOptionsWizard = new MainOptionsWizard(this);
			optionalOptionsWizard = new OptionalOptionsWizard(this);
			selectOutputFileWizard = new SelectOutputFileWizard(this);
			annotationProgressWizard = new AnnotationProgressWizard(this);
		} catch (DataException e) {
			new AnnotationCreationServiceException("Exception creating the wizard forms", e);
		}
		

		addWizards();

		//Adding the listener
		wizardPanelWithLogo.setWizardListener(this);		

		setFinishButtonEnabled(false);

		wizardPanelWithLogo.getWizardComponents().getFinishButton().setText(Messages.getText("Comenzar"));
		
		this.setLayout(new BorderLayout());
		this.add(wizardPanelWithLogo, BorderLayout.CENTER);		
		
		this.addAncestorListener(new AncestorListener() {
    		public void ancestorAdded(AncestorEvent ancestorEvent) {
    		}

    		public void ancestorMoved(AncestorEvent ancestorEvent) {
    		}

    		public void ancestorRemoved(AncestorEvent ancestorEvent) {
    			TaskStatus taskStatus = DefaultJAnnotationCreationServicePanel.this.getAnnotationCreationService().getTaskStatus();
    			taskStatus.getManager().remove(taskStatus);
    		}
		});
	}

	private void addWizards(){
		wizardPanelWithLogo.addOptionPanel(mainOptionsWizard);	
		wizardPanelWithLogo.addOptionPanel(optionalOptionsWizard);
		wizardPanelWithLogo.addOptionPanel(selectOutputFileWizard);	
	    wizardPanelWithLogo.addOptionPanel(annotationProgressWizard);     
	}


	@Override
	public AnnotationCreationService getAnnotationCreationService() {
		return annotationCreationService;
	}

	public void setFinishButtonEnabled(boolean isEnabled){
		wizardPanelWithLogo.setFinishButtonEnabled(isEnabled);
	}
	
	public void setCancellButtonEnabled(boolean isEnabled){
		wizardPanelWithLogo.setCancelButtonEnabled(isEnabled);
	}	
	
	public String getTextValueAttribute(){
		return mainOptionsWizard.getTextValueAttribute();
	}
	
	public String getTextRotationAttribute() {
		return optionalOptionsWizard.getRotationAttribute();
	}
	
	public String getTextColorAttribute() {
		return optionalOptionsWizard.getColorAttribute();
	}
	
	public String getTextHeightAttribute() {
		return optionalOptionsWizard.getHeightAttribute();
	}
	
	public String getTextTypeAttribute(){
		return optionalOptionsWizard.getFontAttribute();
	}

	public WizardPanelActionListener getWizardPanelActionListener() {
		if ((wizardListenerAdapter == null && (getAnnotationServicePanelActionListener() != null))){
			return new WizardListenerAdapter(this);
		}
		return wizardListenerAdapter;
	}

	public void setWizardPanelActionListener(
			WizardPanelActionListener wizardActionListener) {
		// TODO Auto-generated method stub		
	}

	public String getDestinationShapeFile() {
		return selectOutputFileWizard.getSelectedFileName();
	}
	
	public void setNextButtonEnabled(boolean isEnabled){
		wizardPanelWithLogo.setNextButtonEnabled(isEnabled);
	}	
	
    /**
     * @return the annotationSwingManager
     */
    public AnnotationSwingManager getAnnotationSwingManager() {
        return annotationSwingManager;
    }

    /**
     * @param translation
     */
    public void setCancelButtonText(String translation) {
        wizardPanelWithLogo.getWizardComponents().getCancelButton().setText(translation);
    }

    public void createAnnotation() {
        this.lastWizard();
        
        Annotation annotation = new Annotation(
        		getDestinationShapeFile(), 
        		getTextValueAttribute(),
        		getTextRotationAttribute(),
        		getTextColorAttribute(),
        		getTextHeightAttribute(),
        		getTextTypeAttribute());
        annotation.start();
    }
    
    public void lastWizard() {
        this.wizardPanelWithLogo.getWizardComponents().getNextButton().getActionListeners()[0].actionPerformed(null);
    }
    
    private class Annotation extends Thread{
        private String file;
        private String textValueAttributeName;
        private String attrRotation;
        private String attrColor;
        private String attrHeight;
        private String attrType;

        /*public Annotation(String file, String textValueAttributeName) {
            super();
            this.file = file;
            this.textValueAttributeName = textValueAttributeName;
        }*/
        
        public Annotation(String file, 
        		String textValueAttributeName,
        		String attrRotation,
        		String attrColor,
        		String attrHeight,
        		String attrType) {
            super();
            this.file = file;
            this.textValueAttributeName = textValueAttributeName;
            this.attrColor = attrColor;
            this.attrHeight = attrHeight;
            this.attrRotation = attrRotation;
            this.attrType = attrType;
        }

        public synchronized void run() {
            AnnotationCreationService annotationCreationService = getAnnotationCreationService();
            annotationProgressWizard.bind(annotationCreationService.getTaskStatus());           
            try {             
                if(attrRotation != null)
                	annotationCreationService.setFontRotationAttribute(attrRotation);
                if(attrColor != null)
                	annotationCreationService.setFontColorAttribute(attrColor);
                if(attrHeight != null)
                	annotationCreationService.setFontHeigthAttribute(attrHeight);
                if(attrType != null)
                	annotationCreationService.setFontTypeAttribute(attrType);
                annotationCreationService.createAnnotationStore(file, textValueAttributeName);
            } catch (AnnotationCreationException e) {             
                LOG.error("Error creating the annotation", e);
                showError(e, annotationCreationService.getTaskStatus());              
            } catch (DataException e) {
            	 LOG.error("Error assigning properties to the annotation layer", e);
			}
        }
    }
    
    private void showError(AnnotationCreationException e, TaskStatus taskStatus){        
        String message = e.getMessage();
        if (e.getCause() != null){
            message = e.getCause().getMessage();
        }
        JOptionPane.showMessageDialog(annotationProgressWizard, message);
    }
}
