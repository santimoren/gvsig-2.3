/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.*;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.JComboBoxUnits;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.app.gui.styling.JComboBoxUnitsReferenceSystem;
import org.gvsig.app.gui.utils.FontChooser;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.gui.beans.swing.*;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.AttrInTableLabelingStrategy;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.IAttrInTableLabelingStrategy;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.utils.swing.JComboBox;

public class AttrInTableLabeling extends JPanel implements
        ILabelingStrategyPanel {

    private static final long serialVersionUID = 8229927418031917075L;

    private static final String NO_FIELD_ITEM = "-- "
            + PluginServices.getText(LabelingManager.class, "none") + " --";

    private String[] fieldNames;

    private String[] numericFieldNames;

    private String[] integerFieldNames;

    private JRadioButton rdBtnFixedHeight;

    private JRadioButton rdBtnHeightField;

    private JRadioButton rdBtnFixedColor;

    private JRadioButton rdBtnColorField;

    private JComboBox cmbTextField;

    private JComboBox cmbHeightField;

    private JComboBox cmbRotationField;

    private JComboBoxUnits cmbUnits;

    private JComboBoxUnitsReferenceSystem cmbReferenceSystem;

    private JTextField txtHeightField;

    private FLyrVect layer;

    private JCheckBox chkItalic;

    private ColorChooserPanel colorChooser;

    private JComboBox cmbColorField;

    private Font labelFont;

    private JCheckBox chkBold;

    private JComboBoxFonts cmbFontType;

    private I18nManager i18nManager = ToolsLocator.getI18nManager();

    private JPanel pnlAttributes;

    public AttrInTableLabeling() {
        labelFont = MapContextLocator.getSymbolManager().getSymbolPreferences()
                .getDefaultSymbolFont();
        initialize();
    }

    private void initialize() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        GridBagLayoutPanel panel = new GridBagLayoutPanel();
        Insets ins = new Insets(3, 0, 3, 0);

        GridBagLayoutPanel aux = new GridBagLayoutPanel();
        aux.addComponent(PluginServices.getText(this, "_Field_to_use_in_label")
                + ":", getCmbTextField(), GridBagConstraints.BOTH, ins);
        aux.addComponent(i18nManager.getTranslation("font_type_XcolonX"),
                getCmbFontType(), GridBagConstraints.BOTH, ins);
        aux.addComponent(getRdBtnHeightField(), getCmbHeightField(),
                GridBagConstraints.BOTH, ins);
        aux.addComponent(getRdBtnFixedHeight(), getTxtHeightField(),
                GridBagConstraints.BOTH, ins);
        aux.addComponent(i18nManager.getTranslation("attributes_XcolonX"),
                getPanelAttributes(), GridBagConstraints.BOTH, ins);
        aux.addComponent(PluginServices.getText(this, "rotation_height") + ":",
                getCmbRotationField(), GridBagConstraints.BOTH, ins);
        aux.addComponent(PluginServices.getText(this, "units") + ":",
                getCmbUnits(), GridBagConstraints.BOTH, ins);
        aux.addComponent(PluginServices.getText(this, "reference_system"),
                getCmbReferenceSystem(), GridBagConstraints.BOTH, ins);
        panel.add(aux);
        aux = new GridBagLayoutPanel();
        GridBagLayoutPanel colorPanel = new GridBagLayoutPanel();
        ins = new Insets(0, 3, 3, 3);
        Insets ins2 = new Insets(3, 3, 0, 3);
        colorPanel.setBorder(BorderFactory.createTitledBorder(null,
                PluginServices.getText(this, "Color")));
        colorChooser = new ColorChooserPanel(true);
        colorPanel.addComponent(getRdBtnFixedColor(), colorChooser,
                GridBagConstraints.HORIZONTAL, ins2);
        colorPanel.addComponent(getRdBtnColorField(), getCmbColorField(),
                GridBagConstraints.BOTH, ins);
        
        aux.addComponent(colorPanel);

        panel.add(new JBlank(20, 0));
        panel.add(aux);

        add(panel);

        ButtonGroup group = new ButtonGroup();
        group.add(getRdBtnFixedHeight());
        group.add(getRdBtnHeightField());

        ButtonGroup colorGroup = new ButtonGroup();
        colorGroup.add(getRdBtnFixedColor());
        colorGroup.add(getRdBtnColorField());
    }

    private JPanel getPanelAttributes() {
        if (pnlAttributes == null) {
            pnlAttributes = new JPanel(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.fill = GridBagConstraints.BOTH;
            c.anchor = GridBagConstraints.WEST;
            c.insets = new Insets(3, 3, 3, 3);
            c.weightx = 1;
            c.gridx = 0;
            c.gridy = 0;
            pnlAttributes.add(getChkBoxBold(), c);

            c.gridx = 1;
            pnlAttributes.add(getChkItalic(), c);
        }
        return pnlAttributes;
    }

    private JCheckBox getChkItalic() {
        if (chkItalic == null) {
            chkItalic = new JCheckBox(i18nManager.getTranslation("Italic"));
            chkItalic.setSelected(false);
        }
        return chkItalic;
    }

    private JCheckBox getChkBoxBold() {
        if (chkBold == null) {
            chkBold = new JCheckBox(i18nManager.getTranslation("Bold"));
            chkBold.setSelected(false);
        }
        return chkBold;
    }

    private JComboBoxFonts getCmbFontType() {
        if (cmbFontType == null) {
            cmbFontType = new JComboBoxFonts();
        }
        return cmbFontType;
    }

    private JRadioButton getRdBtnFixedHeight() {
        if (rdBtnFixedHeight == null) {
            rdBtnFixedHeight = new JRadioButton(PluginServices.getText(this,
                    "fixed_height") + ":");
            rdBtnFixedHeight.setSelected(true);
            rdBtnFixedHeight.setName("RDFIXEDHEIGHT");
        }

        return rdBtnFixedHeight;
    }

    private JRadioButton getRdBtnHeightField() {
        if (rdBtnHeightField == null) {
            rdBtnHeightField = new JRadioButton(PluginServices.getText(this,
                    "text_height_field") + ":");
            rdBtnHeightField.setSelected(false);
            rdBtnHeightField.setName("RDHEIGHTFIELD");
        }

        return rdBtnHeightField;
    }

    private JRadioButton getRdBtnFixedColor() {
        if (rdBtnFixedColor == null) {
            rdBtnFixedColor = new JRadioButton(PluginServices.getText(this,
                    "_Fixed_color") + ":");
            rdBtnFixedColor.setSelected(true);
            rdBtnFixedColor.setName("RDFIXEDCOLOR");
        }

        return rdBtnFixedColor;
    }

    private JRadioButton getRdBtnColorField() {
        if (rdBtnColorField == null) {
            rdBtnColorField = new JRadioButton(PluginServices.getText(this,
                    "color_field") + ":");
            rdBtnColorField.setSelected(false);
            rdBtnColorField.setName("RDCOLORFIELD");
        }

        return rdBtnColorField;
    }

    private JComboBoxUnits getCmbUnits() {
        if (cmbUnits == null) {
            cmbUnits = new JComboBoxUnits(true);
            cmbUnits.setName("CMBUNITS");
        }

        return cmbUnits;
    }

    private JComboBoxUnitsReferenceSystem getCmbReferenceSystem() {
        if (cmbReferenceSystem == null) {
            cmbReferenceSystem = new JComboBoxUnitsReferenceSystem();
            cmbReferenceSystem.setSelectedIndex(0);
            cmbReferenceSystem.setName("CMBREFSYST");
        }
        return cmbReferenceSystem;
    }

    private JComboBox getCmbColorField() {
        if (cmbColorField == null) {
            cmbColorField = new JComboBox();
            cmbColorField.setName("CMBCOLOR");
        }

        return cmbColorField;
    }

    private void refreshControls() {
        // When the attributes are in the table -----
        // field with the text
        refreshCmbTextField();

        // field with the rotation
        refreshCmbRotationField();

        // field with the text height or the text size
        refreshTextHeight();

        // the text size unit name
        refreshCmbUnits();

        refreshCmbRefSystem();
        // the font for the text
        refreshFont();
        // the color for the font
        refreshColorFont();
    }

    private JComboBox getCmbRotationField() {
        if (cmbRotationField == null) {
            cmbRotationField = new JComboBox();
            cmbRotationField.setPreferredSize(new Dimension(200, 20));
            cmbRotationField.setName("CMBROTATIONFIELD");
        }
        return cmbRotationField;
    }

    private JComboBox getCmbHeightField() {
        if (cmbHeightField == null) {
            cmbHeightField = new JComboBox();
            cmbHeightField.setPreferredSize(new Dimension(200, 20));
            cmbHeightField.setName("CMBHEIGHTFIELD");
        }
        return cmbHeightField;
    }

    private JComboBox getCmbTextField() {
        if (cmbTextField == null) {
            cmbTextField = new JComboBox();
            cmbTextField.setPreferredSize(new Dimension(200, 20));
            cmbTextField.setName("CMBTEXTFIELD");
        }
        return cmbTextField;
    }

    private JTextField getTxtHeightField() {
        if (txtHeightField == null) {
            txtHeightField = new JTextField(10);
            txtHeightField.setText("10");
            txtHeightField.setName("TXTHEIGHTFIELD");
        }

        return txtHeightField;
    }

    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        throw new Error("Not yet implemented!");

    }

    private ColorChooserPanel getColorChooser() {
        if (colorChooser == null) {
            colorChooser = new ColorChooserPanel(true);
        }
        return colorChooser;
    }

    public ILabelingStrategy getLabelingStrategy() {
        // user selected to define each label attributes from values
        // contained in the table for each feature row.

        double fixedSize;
        try {
            fixedSize = Double.parseDouble(getTxtHeightField().getText());
        }
        catch (Exception e) {
            fixedSize = 10;
        }
        AttrInTableLabelingStrategy strategy = new AttrInTableLabelingStrategy();
        strategy.setLayer(layer);

        if (getCmbHeightField().getItemCount() > 0
                && !rdBtnFixedHeight.isSelected()) {
            strategy.setHeightField((String) getCmbHeightField()
                    .getSelectedItem());
        }
        if (getCmbRotationField().getItemCount() > 0) {
            if (!getCmbRotationField().getSelectedItem().equals(NO_FIELD_ITEM)) {
                strategy.setRotationField((String) getCmbRotationField()
                        .getSelectedItem());
            }
            else {
                strategy.setRotationField(null);
            }
        }

        if (getCmbTextField().getItemCount() > 0) {
            strategy.setTextField((String) getCmbTextField().getSelectedItem());
        }

        strategy.setUsesFixedSize(getRdBtnFixedHeight().isSelected());
        strategy.setFixedSize(fixedSize);

        if (getCmbUnits().getItemCount() > 0) {
            strategy.setUnit(getCmbUnits().getSelectedUnitIndex());
        }
        if (getCmbReferenceSystem().getItemCount() > 0) {
            strategy.setReferenceSystem(getCmbReferenceSystem()
                    .getSelectedIndex());
        }

        strategy.setUsesFixedColor(getRdBtnFixedColor().isSelected());
        strategy.setFixedColor(getColorChooser().getColor());

        if (getCmbColorField().getItemCount() > 0
                && !rdBtnFixedColor.isSelected()) {
            strategy.setColorField((String) getCmbColorField()
                    .getSelectedItem());
        }

        strategy.setFont(labelFont);
        return strategy;
    }

    public void setModel(FLayer layer, ILabelingStrategy str) {
        this.layer = (FLyrVect) layer;
        // to allow the labeling of non-FLyrVect layers
        if (layer instanceof FLyrVect) {
            FLyrVect lv = (FLyrVect) layer;
            try {
                FeatureType featureType = lv.getFeatureStore()
                        .getDefaultFeatureType();
                fieldNames = new String[featureType.size()];
                Iterator<FeatureAttributeDescriptor> iterator = featureType
                        .iterator();
                ArrayList<String> l = new ArrayList<String>();
                ArrayList<String> lColors = new ArrayList<String>();
                String name;
                FeatureAttributeDescriptor descriptor;
                while (iterator.hasNext()) {
                    descriptor = iterator.next();

                    name = descriptor.getName();
                    fieldNames[descriptor.getIndex()] = name;
                    switch (descriptor.getType()) {
                    // case DataTypes.DECIMAL:
                    // case DataTypes.NUMERIC:
                    case DataTypes.FLOAT:
                        // case DataTypes.REAL:
                    case DataTypes.DOUBLE:
                        l.add(name);
                        break;
                    case DataTypes.INT:
                        // case DataTypes.SMALLINT:
                        // case DataTypes.TINYINT:
                    case DataTypes.LONG:
                        lColors.add(name);
                        l.add(name);
                        break;
                    }
                }
                numericFieldNames = l.toArray(new String[l.size()]);
                integerFieldNames = lColors.toArray(new String[lColors.size()]);
            }
            catch (DataException e) {
                NotificationManager.addError(PluginServices.getText(this,
                        "accessing_file_structure"), e);
            }
            refreshControls();
        }
    }

    private void refreshColorFont() {

        getCmbColorField().removeAllItems();

        boolean enabled = integerFieldNames.length > 0;
        getCmbColorField().setEnabled(enabled);
        getRdBtnColorField().setEnabled(enabled);

        if (!enabled) {
            getRdBtnFixedColor().setSelected(true);
        }

        for (int i = 0; i < integerFieldNames.length; i++) {
            getCmbColorField().addItem(integerFieldNames[i]);
        }

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            IAttrInTableLabelingStrategy aux = (IAttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            getRdBtnFixedColor().setSelected(aux.usesFixedColor());
            getRdBtnColorField().setSelected(!aux.usesFixedColor());

            String item = aux.getColorField();
            getCmbColorField().setSelectedItem(item);
            getColorChooser().setColor(aux.getFixedColor());
        }
    }

    private void refreshFont() {

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            IAttrInTableLabelingStrategy aux = (IAttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            labelFont = aux.getFont();
        }
    }

    private void refreshCmbUnits() {

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            AttrInTableLabelingStrategy aux = (AttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            getCmbUnits().setSelectedUnitIndex(aux.getUnit());
        }
    }

    private void refreshCmbRefSystem() {

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            AttrInTableLabelingStrategy aux = (AttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            getCmbReferenceSystem().setSelectedIndex(aux.getReferenceSystem());
        }
    }

    private void refreshTextHeight() {
        getCmbHeightField().removeAllItems();

        /*
         * boolean enabled = numericFieldNames.length>0; //
         * getCmbHeightField().setEnabled(enabled); //
         * getRdBtnHeightField().setEnabled(enabled); if (!enabled) {
         * getRdBtnFixedHeight().setSelected(true); }
         */

        for (int i = 0; i < numericFieldNames.length; i++) {
            getCmbHeightField().addItem(numericFieldNames[i]);
        }

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            IAttrInTableLabelingStrategy aux = (IAttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            getTxtHeightField().setText(String.valueOf(aux.getFixedSize()));
            getRdBtnFixedHeight().setSelected(aux.usesFixedSize());
            getRdBtnHeightField().setSelected(!aux.usesFixedSize());

            String item = aux.getHeightField();
            getCmbHeightField().setSelectedItem(item);

        }

    }

    private void refreshCmbRotationField() {
        getCmbRotationField().removeAllItems();
        getCmbRotationField().addItem(NO_FIELD_ITEM);
        for (int i = 0; i < numericFieldNames.length; i++) {
            getCmbRotationField().addItem(numericFieldNames[i]);
        }

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            IAttrInTableLabelingStrategy aux = (IAttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            String item = aux.getRotationField();
            getCmbRotationField().setSelectedItem(
                    item != null ? item : NO_FIELD_ITEM);
        }
    }

    private void refreshCmbTextField() {
        getCmbTextField().removeAllItems();
        for (int i = 0; i < fieldNames.length; i++) {
            getCmbTextField().addItem(fieldNames[i]);
        }

        if (layer.getLabelingStrategy() instanceof AttrInTableLabelingStrategy) {
            IAttrInTableLabelingStrategy aux = (IAttrInTableLabelingStrategy) layer
                    .getLabelingStrategy();
            String item = aux.getTextField();
            getCmbTextField().setSelectedItem(
                    item != null ? item : NO_FIELD_ITEM);
        }
    }

    public String getLabelingStrategyName() {
        return PluginServices
                .getText(this, "label_attributes_defined_in_table");
    }

    public Class getLabelingStrategyClass() {
        return AttrInTableLabelingStrategy.class;
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        // getChooseFontBut().setEnabled(enabled);
        getCmbColorField().setEnabled(enabled);
        getCmbHeightField().setEnabled(enabled);
        getCmbReferenceSystem().setEnabled(enabled);
        getCmbRotationField().setEnabled(enabled);
        getCmbTextField().setEnabled(enabled);
        getCmbUnits().setEnabled(enabled);
        getColorChooser().setEnabled(enabled);
        getRdBtnColorField().setEnabled(enabled);
        getRdBtnFixedColor().setEnabled(enabled);
        getRdBtnFixedHeight().setEnabled(enabled);
        getRdBtnHeightField().setEnabled(enabled);
        getTxtHeightField().setEnabled(enabled);
        getCmbFontType().setEnabled(enabled);
        getChkBoxBold().setEnabled(enabled);
        getChkItalic().setEnabled(enabled);
    }

    /**
     * Returns the selected style.
     *
     * @return the style.
     */
    public int getSelectedStyle() {
        int result = Font.PLAIN;
        if (this.getChkBoxBold().isSelected()) {
            result += Font.BOLD;
        }
        if (this.getChkItalic().isSelected()) {
            result += Font.ITALIC;
        }
        return result;
    }

    /**
     * Returns a Font object representing the selection in the panel.
     *
     * @return the font.
     */
    public Font getSelectedFont() {
        return new Font((String) getCmbFontType().getSelectedItem(),
                getSelectedStyle(), Integer.parseInt(getTxtHeightField()
                        .getText()));
    }
}
