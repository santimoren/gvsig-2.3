/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Behavior;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.SwingUtilities;

import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Arc;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Listeners.ToolListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * When user is working with a tool on a {@link MapControl MapControl} instance,
 * <code>Behavior</code> defines the basic ways of interacting: selecting a
 * point, a circle, a rectangle, or ...
 * </p>
 *
 * <p>
 * All events generated will be <code>MouseEvent</code>, and will depend on the
 * nature of the <i>behavior</i>, like the kind of tool for applying the
 * changes.
 * </p>
 *
 * <p>
 * <code>Behavior</code> defines the common and basic functionality for all
 * kinds of interacting ways with the <code>MapControl</code> object.
 * </p>
 *
 * @see IBehavior
 *
 * @author Luis W. Sevilla
 */
public abstract class Behavior implements IBehavior {

    public static final int BUTTON_LEFT = 1;
    public static final int BUTTON_MIDDLE = 2;
    public static final int BUTTON_RIGHT = 4;

    /**
     * Reference to the <code>MapControl</code> object that uses.
     *
     * @see #getMapControl()
     * @see #setMapControl(MapControl)
     */
    private MapControl mapControl;

    private static final Logger LOG = LoggerFactory.getLogger(Behavior.class);

    protected GeometryManager geomManager = GeometryLocator
        .getGeometryManager();

    private int mouseButton = BUTTON_LEFT;
    private boolean isMyButton = false;

    public Behavior() {

    }

    public Behavior(int mouseButton) {
        this.mouseButton = mouseButton;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#getListener()
     */
    public abstract ToolListener getListener();

    /*
     * <p>Draws as much of the associated <code>MapControl</code> image as is
     * currently available. The image
     * is drawn with its top-left corner at (0, 0) in this graphics context's
     * coordinate space. Transparent
     * pixels in the image do not affect whatever pixels are already there.</p>
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#paintComponent(java.
     * awt.Graphics)
     */
    public void paintComponent(MapControlDrawer mapControlDrawer) {
    }

    public void paintComponent(MapControlDrawer mapControlDrawer, boolean clean) {
        if (clean){
            clean(mapControlDrawer);
        }
        paintComponent(mapControlDrawer);
    }

    public void clean(MapControlDrawer mapControlDrawer){
        BufferedImage img = getMapControl().getImage();

        if (img != null) {
            mapControlDrawer.drawImage(img, 0, 0);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#setMapControl(com.iver
     * .cit.gvsig.fmap.MapControl)
     */
    public void setMapControl(MapControl mc) {
        mapControl = mc;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#getImageCursor()
     */
    public Image getImageCursor() {
        return getListener().getImageCursor();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#getMapControl()
     */
    public MapControl getMapControl() {
        return mapControl;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseClicked(java.awt
     * .event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseEntered(java.awt
     * .event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseExited(java.awt
     * .event.MouseEvent)
     */
    public void mouseExited(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mousePressed(java.awt
     * .event.MouseEvent)
     */
    public void mousePressed(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseReleased(java.awt
     * .event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseDragged(java.awt
     * .event.MouseEvent)
     */
    public void mouseDragged(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseMoved(java.awt.
     * event.MouseEvent)
     */
    public void mouseMoved(MouseEvent e) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Behavior.IBehavior#mouseWheelMoved(java
     * .awt.event.MouseWheelEvent)
     */
    public void mouseWheelMoved(MouseWheelEvent e) throws BehaviorException {
    }

    /**
     * Create point. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param x
     *            The X coordinate
     * @param y
     *            The y coordinate
     * @return
     *         The Point
     */
    protected Point createPoint(double x, double y) {
        Point point = null;
        try {
            point = (Point) geomManager.create(TYPES.POINT, SUBTYPES.GEOM2D);
            point.setX(x);
            point.setY(y);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating a point with x=" + x + ", y=" + y,
                new CreateGeometryException(TYPES.POINT, SUBTYPES.GEOM2D, e));
        }
        return point;
    }

    /**
     * Create an Arc. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param p1
     * @param p2
     * @param p3
     * @return
     *         The arc
     */
    protected Arc createArc(Point2D p1, Point2D p2, Point2D p3) {
        return createArc(createPoint(p1), createPoint(p2), createPoint(p3));
    }

    /**
     * Create an arc. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param p1
     * @param p2
     * @param p3
     * @return
     *         The arc
     */
    protected Arc createArc(Point p1, Point p2, Point p3) {
        Arc arc = null;
        try {
            arc = (Arc) geomManager.create(TYPES.ARC, SUBTYPES.GEOM2D);
            arc.setPoints(p1, p2, p3);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating and arc with p1=" + p1 + ", p2=" + p2
                + ",p3=" + p3, new CreateGeometryException(TYPES.ARC,
                SUBTYPES.GEOM2D, e));
        } catch (IllegalArgumentException ex) {
            LOG.info("Warning: unable to create arc from points: " + p1.getX()
                + " " + p1.getY() + " :: " + p2.getX() + " " + p2.getY()
                + " :: " + p3.getX() + " " + p3.getY());
            arc = null;
        }
        return arc;
    }

    /**
     * Create a curve point. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param p1
     *            The AWT point
     * @return
     *         The gvSIG point
     */
    protected Point createPoint(Point2D p1) {
        return createPoint(p1.getX(), p1.getY());
    }

    /**
     * Create an arc. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param centerX
     * @param centerY
     * @param radious
     * @param angleStart
     * @param angleExtent
     * @return The arc
     */
    protected Arc createArc(double centerX, double centerY, double radious,
        double angleStart, double angleExtent) {
        Arc arc = null;
        try {
            arc = (Arc) geomManager.create(TYPES.ARC, SUBTYPES.GEOM2D);
            arc.setPoints(createPoint(centerX, centerY), radious, angleStart,
                angleExtent);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error(
                "Error creating an arc with centerX=" + centerX + ", centerY="
                    + centerY + ", radious=" + radious + ", angleStart="
                    + angleStart + ", angleExtent=" + angleExtent,
                new CreateGeometryException(TYPES.ARC, SUBTYPES.GEOM2D, e));
        } catch (IllegalArgumentException ex) {
            LOG.info("Warning: unable to create arc from points.");
            arc = null;
        }
        return arc;
    }

    /**
     * Create an circle. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param centerX
     * @param centerY
     * @param radious
     * @return
     *         The arc
     */
    protected Circle createCircle(double centerX, double centerY, double radious) {
        Circle circle = null;
        try {
            circle = (Circle) geomManager.create(TYPES.CIRCLE, SUBTYPES.GEOM2D);
            circle.setPoints(createPoint(centerX, centerY), radious);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating circle with centerX=" + centerX
                + ", centerY=" + centerY + ", radious=" + radious,
                new CreateGeometryException(TYPES.CIRCLE, SUBTYPES.GEOM2D, e));
        }

        return circle;
    }

    /**
     * Create a lineString from a GeneralPath. If there is an
     * error return <code>null</code> and add the error
     * to the log
     *
     * @param gpx
     *            The GeneralPath
     * @return
     *         The LineString
     */
    protected Curve createLineString(GeneralPathX gpx) {
        Curve curve = null;
        try {
            curve = (Curve) geomManager.create(TYPES.CURVE, SUBTYPES.GEOM2D);
            curve.setGeneralPath(gpx);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating lineString with GeneralPathX=" + gpx,
                new CreateGeometryException(TYPES.CURVE, SUBTYPES.GEOM2D, e));
        }
        return curve;
    }

    public void resetMyButton() {
        this.isMyButton = false;
    }

    public boolean isMyButton(MouseEvent e) {
        if (e == null) {
            return this.isMyButton;
        }
        switch (this.mouseButton) {
        case BUTTON_LEFT:
        default:
            this.isMyButton = SwingUtilities.isLeftMouseButton(e);
            return this.isMyButton;
        case BUTTON_MIDDLE:
            this.isMyButton = SwingUtilities.isMiddleMouseButton(e);
            return this.isMyButton;
        case BUTTON_RIGHT:
            this.isMyButton = SwingUtilities.isRightMouseButton(e);
            return this.isMyButton;
        }
    }

    public boolean isMyButton() {
        return this.isMyButton;
    }
}
