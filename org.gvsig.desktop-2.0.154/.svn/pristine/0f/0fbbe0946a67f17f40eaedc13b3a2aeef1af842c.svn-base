/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create Parameter object to define queries}
 */
package org.gvsig.fmap.dal;

import org.gvsig.tools.persistence.Persistent;

/**
 * Defines the properties of a collection of data, as a result of a query
 * through a DataStore.
 * <p>
 * The scale parameter can be used by the DataStore as a hint about the quality
 * or resolution of the data needed to view or operate with the data returned.
 * As an example, it may use the scale to return only a representative subset of
 * the data, or maybe to return Data with less detail, like a point or a line
 * instead of a polygon.
 * </p>
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public interface DataQuery extends Persistent {

    /**
     * Sets the scale.
     *
     * @param scale
     *            the scale to set
     */
	void setScale(double scale);

	/**
	 * Returns the scale of the data to load.
	 * 
	 * @return the scale of the data to load
	 */
	double getScale();

	/**
	 * Returns the value of an query parameter.
	 *
	 * @param name
	 *            of the parameter
	 * @return the parameter value
	 */
	Object getQueryParameter(String name);

	/**
	 * Sets the value of an query parameter.
	 *
	 * @param name
	 *            of the query parameter
	 * @param value
	 *            for the query parameter
	 */
	void setQueryParameter(String name, Object value);

}