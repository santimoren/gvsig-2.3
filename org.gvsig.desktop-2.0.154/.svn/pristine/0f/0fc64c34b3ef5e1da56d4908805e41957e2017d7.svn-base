/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a JTable TableModel for a FeatureQuery}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import javax.swing.table.AbstractTableModel;

import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.i18n.Messages;

/**
 * TableModel to configure a ConfigurableFeatureTableModel.
 *
 * Allows to set Feature attributes as visible or not, and to set aliases for
 * the Feature attribute names.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class ConfigurationTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -825156698327593853L;

    private static final int VISIBILITY_COLUMN = 0;
    private static final int NAME_COLUMN = 1;
    private static final int ALIAS_COLUMN = 2;
    private static final int TYPE_COLUMN = 3;
    private static final int SIZE_COLUMN = 4;
    private static final int PRECISION_COLUMN = 5;
    private static final int PATTERN_COLUMN = 6;
    
    private static final int COLUMN_COUNT = 7;
    
    

    private ConfigurableFeatureTableModel configurable;

    public ConfigurationTableModel(ConfigurableFeatureTableModel configurable) {
        super();
        this.configurable = configurable;
    }

    public int getColumnCount() {
        // 1: visibility, 2: field name, 3: alias, 4:Type, 5:size, 6:pattern
        return COLUMN_COUNT;
    }

    public int getRowCount() {
        return configurable.getOriginalColumnCount();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
    	FeatureAttributeDescriptor fad = null;
        String name = configurable.getOriginalColumnName(rowIndex);
        switch (columnIndex) {
        case VISIBILITY_COLUMN:
            return configurable.isVisible(name);
        case NAME_COLUMN:
            return name;
        case ALIAS_COLUMN:
            return configurable.getAliasForColumn(name);
        case TYPE_COLUMN:
        	fad = configurable.internalGetFeatureDescriptorForColumn(rowIndex);
        	if( fad == null ) {
        		return null;
        	}
    		return fad.getDataType().getName();
        case SIZE_COLUMN:
        	fad = configurable.internalGetFeatureDescriptorForColumn(rowIndex);
        	if( fad == null ) {
        		return null;
        	}
    		return fad.getSize();
        case PRECISION_COLUMN:
        	fad = configurable.internalGetFeatureDescriptorForColumn(rowIndex);
        	if( fad == null ) {
        		return null;
        	}
    		return fad.getPrecision();
        case PATTERN_COLUMN:
        	return configurable.getFormattingPattern(name);
        default:
            return null;
        }
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        String name = configurable.getOriginalColumnName(rowIndex);
        switch (columnIndex) {
        case VISIBILITY_COLUMN:
            configurable.setVisible(name, Boolean.TRUE.equals(value));
            break;
        case ALIAS_COLUMN:
            configurable.setAlias(name, (String) value);
            break;
        case PATTERN_COLUMN:
        	configurable.setFormattingPattern(name, (String) value);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
        case VISIBILITY_COLUMN:
        case ALIAS_COLUMN:
        case PATTERN_COLUMN:
            return true;
        default:
            return false;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case VISIBILITY_COLUMN:
            return Boolean.class;
        case NAME_COLUMN:
        case ALIAS_COLUMN:
        case TYPE_COLUMN:
        case PATTERN_COLUMN:
            return String.class;
        case SIZE_COLUMN:
        case PRECISION_COLUMN:
            /*
             * We must use Integer.class instead of int.class
             * because int.class does not have a registered
             * renderer and its parent class is 'null', so we
             * have no renderer for it
             */
            return Integer.class;
        default:
            return Object.class;
        }

    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
        case VISIBILITY_COLUMN:
            return Messages.getText("visible");
        case NAME_COLUMN:
            return Messages.getText("name");
        case ALIAS_COLUMN:
            return Messages.getText("alias");
        case SIZE_COLUMN:
            return Messages.getText("size");
        case PRECISION_COLUMN:
            return Messages.getText("precision");
        case TYPE_COLUMN:
            return Messages.getText("type");
        case PATTERN_COLUMN:
            return Messages.getText("pattern");
        default:
            return "";
        }
    }

	public static int getVisibilityColumn() {
		return VISIBILITY_COLUMN;
	}

    /**
     * Make current changes in configuration (visible columns and aliases)
     * as definitive.
     */
	public void acceptChanges() {
		configurable.acceptChanges();
	}

    /**
     * Cancel current changes in configuration (visible columns and aliases)
     * and return to previous status.
     */
	public void cancelChanges() {
		configurable.cancelChanges();
	}
}