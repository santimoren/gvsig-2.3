/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.perpendicular;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * Gets a point that is located to a concrete distance (attribute distance)
 * of a line and is located in a perpendicular line that intersects with other 
 * point (attribute perpendicularPoint).
 * 
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class PerpendicularPoint extends GeometryOperation{
    public static final String NAME = "perpendicularPoint";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);
   
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
        throws GeometryOperationException {
      
        Point perpendicularPoint = (Point)ctx.getAttribute(PerpendicularPointOperationContext.PERPENDICULAR_POINT); 
        double dist =
            ((Double) ctx
                .getAttribute(PerpendicularPointOperationContext.DISTANCE))
                .doubleValue();
        
        try {
            Point[] p = (Point[])geom.invokeOperation(Perpendicular.CODE, ctx);
            Point unit = (Point)geom.invokeOperation(UnitVector.CODE, ctx);
            return  geomManager.createPoint(perpendicularPoint.getX() + (unit.getX() * dist),
                perpendicularPoint.getY() + (unit.getY() * dist), SUBTYPES.GEOM2D);
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryOperationException(e);
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        }
    }
   
    public int getOperationIndex() {      
        return CODE;
    }    
}
