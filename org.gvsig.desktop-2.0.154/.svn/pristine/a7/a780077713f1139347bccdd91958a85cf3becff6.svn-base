/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.ellipse;

import java.awt.geom.AffineTransform;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.UtilFunctions;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Ellipse;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public class Ellipse2D extends BaseEllipse2D implements Ellipse {

    /**
     *
     */
    private static final long serialVersionUID = 3414562338763885954L;

    /**
     * @param subtype
     */
    public Ellipse2D() {
        super(Geometry.TYPES.ELLIPSE);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        Ellipse2D clone = new Ellipse2D();
        clone.setPoints((Point)init.cloneGeometry(), (Point)end.cloneGeometry(), ydist);
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        ArrayListCoordinateSequence coordinates = getJTSCoordinates();
        return JTSUtils.createJTSPolygon(coordinates);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getRectangleCorner()
     */
    public Point getRectangleCorner() {
        return new Point2D(
            this.getAxis1Start().getX(),
            this.getAxis1Start().getY()-this.getAxis2Dist());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getRectangleHeight()
     */
    public double getRectangleHeight() {
        return this.getAxis2Dist()*2;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getRectangleWidth()
     */
    public double getRectangleWidth() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return this.getAxis1Start().distance(this.getAxis1End());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getRectangleRotation()
     */
    public AffineTransform getRectangleRotation() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return AffineTransform.getRotateInstance(getAxis1Angle(), this.getAxis1Start().getX(), this.getAxis1Start().getY());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        Ellipse clonedEllipse = (Ellipse) this.cloneGeometry();

        GeometryManager geomManager = GeometryLocator.getGeometryManager();
        Point center = new Point2D((getAxis1Start().getX()+getAxis1End().getX())/2,
            (getAxis1Start().getY()+getAxis1End().getY())/2);
        double axis1Lenght = getAxis1Start().distance(getAxis1End());

        Point clonedAxis1Start = (Point) getAxis1Start().cloneGeometry();
        Point clonedAxis1End = (Point) getAxis1End().cloneGeometry();
        double clonedYDist = this.ydist+distance;

        clonedAxis1Start.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1Start().getX(), axis1Lenght/2+distance));
        clonedAxis1Start.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1Start().getY(), axis1Lenght/2+distance));

        clonedAxis1End.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1End().getX(), axis1Lenght/2+distance));
        clonedAxis1End.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1End().getY(), axis1Lenght/2+distance));

        clonedEllipse.setPoints(clonedAxis1Start, clonedAxis1End, clonedYDist);
        return clonedEllipse;
    }
}
