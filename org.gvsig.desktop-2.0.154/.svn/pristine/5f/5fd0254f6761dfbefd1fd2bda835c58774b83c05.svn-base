/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.handler.AbstractHandler;
import org.gvsig.fmap.geom.handler.FinalHandler;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.generalpath.gputils.DrawGeometryShape;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.locator.LocatorException;


/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public abstract class OrientablePrimitive2D extends AbstractPrimitive implements OrientablePrimitive {
    private static final long serialVersionUID = -820881421374434713L;
    protected GeneralPathX gp;

    /**
     * The constructor with the GeometryType like and argument
     * is used by the {@link GeometryType}{@link #create()}
     * to create the geometry
     * @param type
     * The geometry type
     */
    public OrientablePrimitive2D(GeometryType geometryType) {
        super(geometryType, null, null);
        gp = new GeneralPathX();
    }

    public OrientablePrimitive2D(GeometryType geometryType, String id, IProjection projection, GeneralPathX gp){
        super(geometryType, id, projection);
        this.gp = gp;
    }

    /**
     * TODO método creado para dar visibilidad a gp despues de la refactorización
     * @return
     */
    public GeneralPathX getGeneralPathX() {
        return gp;
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#contains(double, double)
     */
    public boolean contains(double x, double y) {
        return gp.contains(x, y);
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#contains(double, double, double, double)
     */
    public boolean contains(double x, double y, double w, double h) {
        return gp.contains(x, y, w, h);
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#intersects(double, double, double, double)
     */
    public boolean intersects(double x, double y, double w, double h) {
        // M�s r�pido
        return gp.intersects(x, y, w, h);
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#getBounds()
     */
    public Rectangle getBounds() {
        return gp.getBounds();
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#contains(java.awt.geom.Point2D)
     */
    public boolean contains(Point2D p) {
        return gp.contains(p);
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#getBounds2D()
     */
    public Rectangle2D getBounds2D() {
        return gp.getBounds2D();
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#contains(java.awt.geom.Rectangle2D)
     */
    public boolean contains(Rectangle2D r) {
        return gp.contains(r);
    }

    /**
     * El m�todo intersects de java.awt.Shape que define la intersecci�n entre
     * una polil�nea y un Rectangle2D considera la polil�nea como un Shape
     * gen�rico y se producen errores en la selecci�n de polil�neas. Por este
     * motivo se ha modificado este m�todo intersect() de FPolyline2D para que
     * realize la intersecci�n estricta entre el Rectangle2D y la polil�nea en
     * cuesti�n. El precio es un incremento de tiempo m�ximo del 50%.
     *
     * @param r Rect�ngulo.
     *
     * @return True si intersecta con el rectangulo que se pasa como par�metro.
     */
    public boolean intersects(Rectangle2D r) {
        //return gp.intersects(r);
        // M�s exacto
        boolean bool = false;
        if (gp.intersects(r)) {
            ArrayList arrayCoords;
            int theType;
            //Use this array to store segment coordinate data
            double[] theData = new double[6];
            PathIterator theIterator;

            Point2D p1 = new Point2D.Double(r.getMinX(),r.getMinY());
            Point2D p2 = new Point2D.Double(r.getMinX(),r.getMaxY());
            Point2D p3 = new Point2D.Double(r.getMaxX(),r.getMaxY());
            Point2D p4 = new Point2D.Double(r.getMaxX(),r.getMinY());
            Line2D l1 = new Line2D.Double(p1,p2);
            Line2D l2 = new Line2D.Double(p2,p3);
            Line2D l3 = new Line2D.Double(p3,p4);
            Line2D l4 = new Line2D.Double(p4,p1);

            theIterator = this.getPathIterator(null, geomManager.getFlatness());
            arrayCoords = new ArrayList();
            while(!theIterator.isDone()) {
                theType = theIterator.currentSegment(theData);
                if (theType==PathIterator.SEG_MOVETO) {
                    arrayCoords.add(new Point2D.Double(theData[0], theData[1]));
                } else if (theType==PathIterator.SEG_LINETO) {
                    arrayCoords.add(new Point2D.Double(theData[0], theData[1]));
                    Point2D pAnt = (Point2D)arrayCoords.get(arrayCoords.size()-2);
                    Line2D l = new Line2D.Double(pAnt.getX(),pAnt.getY(),theData[0],theData[1]);
                    if (l.intersectsLine(l1.getX1(),l1.getY1(),l1.getX2(),l1.getY2())
                        || l.intersectsLine(l2.getX1(),l2.getY1(),l2.getX2(),l2.getY2())
                        || l.intersectsLine(l3.getX1(),l3.getY1(),l3.getX2(),l3.getY2())
                        || l.intersectsLine(l4.getX1(),l4.getY1(),l4.getX2(),l4.getY2())
                        || r.intersectsLine(l)) {
                        bool = true;
                    }
                } else if(theType==PathIterator.SEG_CLOSE){
                    Point2D firstPoint=(Point2D)arrayCoords.get(0);
                    Point2D pAnt = (Point2D)arrayCoords.get(arrayCoords.size()-1);
                    Line2D l = new Line2D.Double(pAnt.getX(),pAnt.getY(),firstPoint.getX(),firstPoint.getY());
                    if (l.intersectsLine(l1.getX1(),l1.getY1(),l1.getX2(),l1.getY2())
                        || l.intersectsLine(l2.getX1(),l2.getY1(),l2.getX2(),l2.getY2())
                        || l.intersectsLine(l3.getX1(),l3.getY1(),l3.getX2(),l3.getY2())
                        || l.intersectsLine(l4.getX1(),l4.getY1(),l4.getX2(),l4.getY2())
                        || r.intersectsLine(l)) {
                        bool = true;
                    }
                }else {
                    System.out.println("Not supported here");
                }
                theIterator.next();
            }
        }
        return bool;
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        return gp.getPathIterator(at);
    }

    /* (non-Javadoc)
     * @see java.awt.Shape#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return gp.getPathIterator(at, flatness);
    }

    /**
     * DOCUMENT ME!
     *
     * @param at DOCUMENT ME!
     */
    public void transform(AffineTransform at) {

        // TODO: PRUEBA. BORRAR ESTA LINEA
        // gp = FConverter.transformToInts(gp, at);

        gp.transform(at);
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.core.FShape#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        gp.reProject(ct);
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.core.FShape#getStretchingHandlers()
     */
    public Handler[] getStretchingHandlers() {
        ArrayList handlers = new ArrayList();
        PathIterator gpi = getPathIterator(null);

        double[] theData = new double[6];
        int i=0;
        while (!gpi.isDone()) {
            gpi.currentSegment(theData);
            //g.fillRect((int)(theData[0]-3),(int)(theData[1]-3),6,6);
            handlers.add(new PointHandler(i,theData[0], theData[1]));
            i++;
            gpi.next();
        }

        return (Handler[]) handlers.toArray(new Handler[0]);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int, int)
     */
    public double getCoordinateAt(int index, int dimension) {
        if (index > getNumVertices()){
            throw new ArrayIndexOutOfBoundsException();
        }
        return getVertex(index).getCoordinateAt(dimension);
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.core.FShape#getSelectHandlers()
     */
    public Handler[] getSelectHandlers() {
        Handler[] handlers = new Handler[gp.getNumCoords()];
        for (int i=0 ; i<gp.getNumCoords() ; i++){
            handlers[i] = new PointSelHandler(i, gp.getPointAt(i).getX(), gp.getPointAt(i).getY());
        }
        return handlers;
    }

    /**
     * DOCUMENT ME!
     *
     * @author Vicente Caballero Navarro
     */
    class PointHandler extends AbstractHandler implements FinalHandler{
        /**
         * Crea un nuevo PointHandler.
         *
         * @param x DOCUMENT ME!
         * @param y DOCUMENT ME!
         */
        public PointHandler(int i,double x, double y) {
            point = new Point2D.Double(x, y);
            index = i;
        }

        /**
         * DOCUMENT ME!
         *
         * @param x DOCUMENT ME!
         * @param y DOCUMENT ME!
         *
         * @return DOCUMENT ME!
         */
        public void move(double x, double y) {
            Point point = gp.getPointAt(index);
            point.setX(point.getX() + x);
            point.setY(point.getY() + y);
        }

        /**
         * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
         */
        public void set(double x, double y) {
            Point point = gp.getPointAt(index);
            point.setX(x);
            point.setY(y);
        }
    }
    /**
     * DOCUMENT ME!
     *
     * @author Vicente Caballero Navarro
     */
    class PointSelHandler extends AbstractHandler implements FinalHandler{
        /**
         * Crea un nuevo PointHandler.
         *
         * @param x DOCUMENT ME!
         * @param y DOCUMENT ME!
         */
        public PointSelHandler(int i,double x, double y) {
            point = new Point2D.Double(x, y);
            index = i;
        }

        /**
         * DOCUMENT ME!
         *
         * @param x DOCUMENT ME!
         * @param y DOCUMENT ME!
         *
         * @return DOCUMENT ME!
         */
        public void move(double x, double y) {
            Point point = gp.getPointAt(index);
            point.setX(point.getX() + x);
            point.setY(point.getY() + y);
        }

        /**
         * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
         */
        public void set(double x, double y) {
            Point point = gp.getPointAt(index);
            point.setX(x);
            point.setY(y);
        }
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.geometries.iso.GM_Object#coordinateDimension()
     */
    public int getDimension() {
        return 2;
    }

    public Envelope getEnvelope() {
        Rectangle2D r=gp.getBounds2D();
        return new Envelope2D(r.getX(),r.getY(),r.getMaxX(),r.getMaxY());
    }

    public GeneralPathX getGeneralPath() {
        return gp;
    }

    public int getNumVertices() {
        return gp.getNumCoords();
    }

    public void addVertex(Point point) {
    	if( this.getNumVertices()== 0 ) {
    		gp.moveTo(point);
    	} else {
    		gp.lineTo(point);
    	}
    }

    public void addVertex(double x, double y)  {
		try {
	    	Point point;
			point = GeometryLocator.getGeometryManager().createPoint(x, y, SUBTYPES.GEOM2D);
	    	addVertex(point);
		} catch (CreateGeometryException e) {
			throw new RuntimeException(e.getMessage());
		}
    }

    public void addVertex(double x, double y, double z) {
		try {
	    	Point point;
			point = GeometryLocator.getGeometryManager().createPoint(x, y, SUBTYPES.GEOM3D);
			point.setCoordinateAt(DIMENSIONS.Z, z);
	    	addVertex(point);
		} catch (CreateGeometryException e) {
			throw new RuntimeException(e.getMessage());
		}
    }

    public void addMoveToVertex(Point point) {
        gp.moveTo(point);
    }

    public void closePrimitive() {
        gp.closePath();
    }

    public void setVertex(int index, Point p) {
        Point point = gp.getPointAt(index);
        int min = p.getDimension() < point.getDimension()?p.getDimension():point.getDimension();
        for(int i=0; i<min; i++){
            point.setCoordinateAt(i, p.getCoordinateAt(i));
        }
    }

    public Point getVertex(int index) {
        return gp.getPointAt(index);
    }

    public void insertVertex(int index, Point p) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void removeVertex(int index) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void setCoordinateAt(int index, int dimension, double value) {
        gp.getPointAt(index).setCoordinateAt(dimension, value);
    }

    public void setGeneralPath(GeneralPathX generalPathX) {
        this.gp = generalPathX;
    }

    public void setPoints(Point startPoint, Point endPoint) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public Shape getShape() {
        return new DrawGeometryShape(this);
    }

    public Shape getShape(AffineTransform affineTransform) {
        return new DrawGeometryShape(this, affineTransform);
    }

    public void ensureCapacity(int capacity) {
        // TODO:
    }


}
