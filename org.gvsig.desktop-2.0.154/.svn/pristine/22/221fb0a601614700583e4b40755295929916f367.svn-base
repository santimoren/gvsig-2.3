/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.resource.db;

import java.sql.SQLException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.spi.AbstractNonBlockingResource;

/**
 * <p>
 * Abstract Data Base Resource implementation that allow the concurrent access.
 * </p>
 * 
 * <p>
 * Useful for Pooled Data Base Access.
 * </p>
 * 
 * @author jmvivo
 *
 */
public abstract class AbstractDBResourceNoBlocker extends AbstractNonBlockingResource {

	/**
	 * Default constructor
	 *
	 * @param parameters
	 * @throws InitializeException
	 */
	protected AbstractDBResourceNoBlocker(DBResourceParameters parameters)
			throws InitializeException {
		super(parameters);
	}

        private static class CanConnectException extends AccessResourceException {
            public CanConnectException(String resource, DataException cause) {
                super("Can't get a connection to the resource (%(resource))", cause, "_CanConnectException", 0);
                setValue("resource", resource);
            }
        }
        private static class CanGetConnectException extends AccessResourceException {
            public CanGetConnectException(String datasource, DataException cause) {
                super("Can't get the connection of the resource (%(resource))", cause, "_CanGetConnectionException", 0);
                setValue("resource", datasource);
            }
        }
    
	/**
	 * Return a connection to the data base.<br>
	 * Connect to the Data Base if is needed
	 *
	 * @return connection to the data base
	 */
	public Object get() throws AccessResourceException {
		if (!isConnected()) {
			try {
				this.connect();
			} catch (DataException e) {
				throw new CanConnectException(this.toString(), e);
			}
		}
		try {
			return getTheConnection();
		} catch (DataException e) {
			throw new CanGetConnectException(this.toString(), e);
		}
	}

	/**
	 * Return a connection to the data base.<br>
	 * Connect to the Data Base if is needed
	 *
	 *
	 * @return connection to the data base
	 * @see #get()
	 */
	public Object getConnection() throws AccessResourceException {
		return get();
	}

	/**
	 * inform if connection to the data base is established
	 *
	 * @return
	 */
	public abstract boolean isConnected();

	/**
	 * Establish connection to data base
	 *
	 * @throws DataException
	 */
	public final void connect() throws DataException {
		if (this.isConnected()) {
			return;
		}
		prepare();
		connectToDB();
	}

	/**
	 * final implementation method to Establish connection to data base<br>
	 * Called from {@link #get()}<br>
	 * <br>
	 *
	 *
	 * @throws DataException
	 */
	protected abstract void connectToDB() throws DataException;

	/**
	 * final implementation method to get a connection to data base<br>
	 * Called from {@link #connect()}<br>
	 * <br>
	 * This method is called with the connection establish
	 *
	 * @throws DataException
	 */
	protected abstract Object getTheConnection() throws DataException;

	/**
	 * Check if parameters is the same for this resource.<br>
	 * <br>
	 *
	 * <strong>Note:</strong> override this method to add checks for specify
	 * final implementation parameters
	 *
	 *
	 * @see AbstractResource#isThis(ResourceParameters)
	 */
	public boolean isThis(ResourceParameters parameters)
			throws ResourceException {
		if (!(parameters instanceof DBResourceParameters)) {
			return false;
		}
		DBResourceParameters params = (DBResourceParameters) parameters
				.getCopy();
		prepare(params);
		DBResourceParameters myParams = (DBResourceParameters) this
				.getParameters();

		if (!equals(myParams.getHost(), params.getHost())) {
			return false;
		}
		if (!equals(myParams.getPort(), params.getPort())) {
			return false;
		}
		if (!equals(myParams.getUser(), params.getUser())) {
			return false;
		}
        if (!equals(myParams.getPassword(), params.getPassword())) {
            return false;
        }
		return true;
	}

	@SuppressWarnings("unchecked")
	protected boolean equals(Comparable v1, Comparable v2) {
		if (v1 == v2) {
			return true;
		}
		if ((v1 != null) && (v2 != null)) {
			return v1.compareTo(v2) == 0;
		}
		return false;
	}


}
