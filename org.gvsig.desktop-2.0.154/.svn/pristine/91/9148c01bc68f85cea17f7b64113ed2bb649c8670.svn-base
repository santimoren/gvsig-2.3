/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl;

import java.text.MessageFormat;

import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.packageutils.StringWithAlias;
import org.gvsig.tools.packageutils.impl.DefaultStringWithAlias;

public class DefaultDependency implements Dependency {

	private String type;
	private StringWithAlias code;
	private String op;
	private Version version;

	public DefaultDependency() {
		super();
		clear();
	}

	public DefaultDependency(PackageInfo packageInfo) {
		this();
		this.code = packageInfo.getAllCodes();
		this.type = "required";
		this.op = ">=";
		this.version = packageInfo.getVersion();
	}

	public void clear() {
		this.type = "required";
		this.code = new DefaultStringWithAlias("unknow");
		this.op = "=";
		this.version = new DefaultVersion();
	}

	public Dependency parse(String dependency) {
		// Parse a string with the dependency specification
		// (required|suggested|recommended|conflict)[:] code (=|>|<|>=|<=)
		// version
		if (dependency == null) {
			this.clear();
			return this;
		}
		dependency = dependency.trim();
		if (dependency.equals("")) {
			this.clear();
			return this;
		}
		
		String s = dependency;
		/*
		 * Remove special characters, so
		 * dependency description in pom.xml is not
		 * so fragile
		 */
        s = s.replace('\n', ' ');
        s = s.replace('\t', ' ');
        s = s.replace('\r', ' ');
        s = s.replace(':', ' ');
		
        /*
         * Added loop because replaceAll is not
         * exhaustive in this case
         */
        while (s.indexOf("  ") != -1) {
            s = s.replaceAll("  ", " ");
        }
		
		String[] x = s.split(" ");
		if (x.length != 4) {
			throw new InvalidDependencyFormatException(dependency);
		}
		
		this.type = x[0];
		this.code = new DefaultStringWithAlias(x[1]);
		this.op = x[2];
		this.version.parse(x[3]);
		return this;
	}

	private class InvalidDependencyFormatException extends BaseRuntimeException {

		private static final long serialVersionUID = 2856837862117653856L;

		private static final String message = "Error parsing dependecy '%(dependency)s'";

		private static final String KEY = "_Error_parsing_dependecy_XdependecyX";

		public InvalidDependencyFormatException(String dependency) {
			super(message, null, KEY, serialVersionUID);
			setValue("dependency", dependency);
		}
	}

	public String getType() {
		return this.type;
	}

	public String getCode() {
		return this.code.toString();
	}

	public String getOp() {
		return this.op;
	}

	public Version getVersion() {
		return this.version;
	}

	public boolean match(String type, String code, Version version) {
		if (!this.type.equalsIgnoreCase(type)) {
			return false;
		}
		if (!this.code.equalsIgnoreCase(code)) {
			return false;
		}
		return version.check(this.op, this.version);
	}

	public boolean match(String type, StringWithAlias code, Version version) {
		if (!this.type.equalsIgnoreCase(type)) {
			return false;
		}
		if (!code.equalsIgnoreCase(this.code)) {
			return false;
		}
		return version.check(this.op, this.version);
	}

	@Override
	public String toString() {
		return MessageFormat.format("{0}: {1} {2} {3}", this.type, this.code.toString(),
				this.op, this.version.toString());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		DefaultDependency x = (DefaultDependency) super.clone();
		x.version = (Version) this.version.clone();
		return x;
	}

	@Override
	public boolean equals(Object obj) {
		Dependency other;
		try {
			other = (Dependency) obj;
		} catch (Exception ex) {
			return false;
		}
		if (!this.code.equalsIgnoreCase(other.getCode())) {
			return false;
		}
		if (!this.type.equalsIgnoreCase(other.getType())) {
			return false;
		}
		if (!this.op.equalsIgnoreCase(other.getOp())) {
			return false;
		}
		if (!this.version.equals(other.getVersion())) {
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
	    return version.hashCode() + code.hashCode()
	        + type.hashCode() + op.hashCode();
	}
}
