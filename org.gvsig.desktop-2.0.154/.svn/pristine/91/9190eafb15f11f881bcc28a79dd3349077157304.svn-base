/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb.vectorialdb.wizard;

import java.awt.Window;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.prepareAction.PrepareContextView;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorer;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayerFactory;
import org.gvsig.fmap.mapcontrol.MapControl;



/**
 * Driver-independent GeoDB wizard. Queries the drivers to fill GUI controls.
 * Multi-table selection available.
 *
 * @author jldominguez
 *
 */
public class WizardVectorialDB extends WizardDB {
    private static final long serialVersionUID = -5002685263220038989L;

    private static Logger logger = LoggerFactory
			.getLogger(WizardVectorialDB.class.getName());

    private static final String GEODB_WIZARD_TAB_NAME = "GeoDB";
	private DefaultViewPanel view = null;

	private PrepareContextView prepareContext;

    /**
     * This method initializes this
     *
     * @return void
     */
    protected void initialize() {
		super.initialize();
        setTabName(GEODB_WIZARD_TAB_NAME);
        setLayout(null);
        setSize(512, 478);

        IWindow iw = PluginServices.getMDIManager().getActiveWindow();

        if (iw == null) {
            return;
        }

        if ((iw instanceof DefaultViewPanel)) {
        	 view = (DefaultViewPanel) iw;
             setMapCtrl(view.getMapControl());
        }
    }
    
    
    
    protected List getTableList(DBServerExplorer explorer) throws DataException {
        return explorer.list(DBServerExplorer.MODE_GEOMETRY);
    }


	protected TablesListItem createTabeListItem(DBStoreParameters param) {
		return new TablesListItemVectorial(param, getMapCtrl(),this);
	}

	public void setSettingsPanels(TablesListItem actTable) {
		super.setSettingsPanels(actTable);
	}

	protected UserTableSettingsPanel createSettingsPanel(
			TablesListItem actTable) {
		if (actTable == null) {
			return new UserTableSettingsVectorialPanel(null, null, "",
					getMapCtrl(), true, this, null, null);
		}
		String abrev = null;
		if (getMapCtrl() != null) {
			abrev = getMapCtrl().getViewPort().getProjection().getAbrev();
		}

		return ((TablesListItemVectorial) actTable)
				.getUserTableSettingsPanel(abrev);
	}


	public DataStoreParameters[] getParameters() {
		try {
			TablesListItem[] selected = getSelectedTables();
			int count = selected.length;
			DBStoreParameters[] dbParameters = new DBStoreParameters[count];
			String strEPSG = null;
			if (getMapCtrl() != null) {
				strEPSG = getMapCtrl().getViewPort().getProjection().getAbrev();
			}

			for (int i = 0; i < count; i++) {
				TablesListItemVectorial item = (TablesListItemVectorial) selected[i];

				dbParameters[i] = getParameterForTable(item);
			}

			return dbParameters;// layerArrayToGroup(all_layers, groupName);
		} catch (Exception e) {
			logger.info("Error while creating jdbc layer: " + e.getMessage(), e);
			NotificationManager.addError(
			    "Error while loading layer: "
					+ e.getMessage(), e);
		}

		return null;
	}

	public void execute() {
		MapControl mapControl = this.getMapCtrl();
		IProjection proj = null;
		TablesListItem[] tables = getSelectedTables();


		List all_layers = new ArrayList();
		String strEPSG = mapControl.getViewPort().getProjection().getAbrev();
		LayerFactory layerFactory = LayerFactory.getInstance();
		String groupName = null;
		Envelope env = null;
		DBStoreParameters parameter;
		TablesListItem table;
		FeatureStore store, storeToAdd;

		DataManager man = DALLocator.getDataManager();

		ApplicationManager appGvSIGMan = ApplicationLocator.getManager();
		PrepareContext context = this.getPrepareDataStoreContext();


        try {

            FLayer layer;
            for (int i = 0; i < tables.length; i++) {
                table = tables[i];
                UserTableSettingsVectorialPanel userTableSettingsPanel =
                    (UserTableSettingsVectorialPanel) table
                        .getUserTableSettingsPanel();
                parameter = getParameterForTable(table);
                if (i == 0) {
                    groupName =
                        parameter.getDBName() + " (" + parameter.getHost()
                            + ")";
                }
                try {
                    parameter =
                        (DBStoreParameters) appGvSIGMan
                            .prepareOpenDataStoreParameters(parameter, context);

                } catch (Exception e2) {
                    NotificationManager.addError(e2);
                    continue;
                }

                try {
                    store = (FeatureStore) man.openStore(
                    		parameter.getDataStoreName(),
                    		parameter);
                } catch (Exception e) {
                    logger.error("While opening DB store.", e);
                    return;
                }

                try {
                    storeToAdd =
                        (FeatureStore) appGvSIGMan.pepareOpenDataSource(store,
                            context);
                } catch (Exception e) {
                    NotificationManager.addError(e);
                    store.dispose();
                    continue;
                }

                try {

                    layer =
                        layerFactory.createLayer(
                            userTableSettingsPanel.getUserLayerName(),
                            storeToAdd);
                    all_layers.add(layer);
                    if (env == null) {
                        env = layer.getFullEnvelope();
                    } else {
                        env.add(layer.getFullEnvelope());
                    }
                    storeToAdd.dispose();
                } catch (Exception e) {
                    storeToAdd.dispose();
                    NotificationManager.addError(e);
                }
            }

            MapContext mc = mapControl.getMapContext();
            FLayers root = mapControl.getMapContext().getLayers();
            if (all_layers.size() > 1) {
                FLayers group = new FLayers();// (mc,root);
                group.setMapContext(mc);
                group.setParentLayer(root);
                group.setName(groupName);

                Iterator iter = all_layers.iterator();
                while (iter.hasNext()) {
                    group.addLayer((FLayer) iter.next());
                }

                if ((group != null) && !(group.isOk())) {
                    // if the layer is not okay (it has errors) process them
                    processErrorsOfLayer(group, mapControl);
                }

                if (group != null) {
                    group.setVisible(true);
                    mapControl.getMapContext().beginAtomicEvent();
                    try {
                        // checkProjection(group, mapControl.getViewPort());
                        try {
                            mapControl.getMapContext().getLayers()
                                .addLayer(group);
                            group.dispose();
                        } catch (CancelationException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if (mapControl.getViewPort().getExtent() == null) {
                            mapControl.getViewPort().setEnvelope(env);
                        }
                    } finally {
                        mapControl.getMapContext().endAtomicEvent();
                    }
                    return;
                }
            } else
                if (all_layers.size() == 1) {
                    layer = (FLayer) all_layers.get(0);
                    if (!(layer.isOk())) {
                        // if the layer is not okay (it has errors) process them
                        processErrorsOfLayer(layer, mapControl);
                    }

                    layer.setVisible(true);
                    mapControl.getMapContext().beginAtomicEvent();
                    // checkProjection(all_layers[0], mapControl.getViewPort());
                    try {
                        try {
                            mapControl.getMapContext().getLayers()
                                .addLayer(layer);
                        } catch (CancelationException e) {
                            return;
                        }

                        if (mapControl.getViewPort().getExtent() == null) {
                            try {
                                mapControl.getViewPort().setEnvelope(
                                    layer.getFullEnvelope());
                            } catch (ReadException e) {
                                NotificationManager.addError(e);
                                return;
                            }
                        }
                    } finally {

                        mapControl.getMapContext().endAtomicEvent();
                    }
                    return;
                }
        } finally {
            // Dispose all created layers. If they have been included into
            // a FLayers object, they will have been binded there.
            for (int i = 0; i < all_layers.size(); i++) {
                FLayer layer = (FLayer) all_layers.get(i);
                layer.dispose();
            }
		}
	}

	protected PrepareContext getPrepareDataStoreContext() {
		if (this.prepareContext == null) {
			this.prepareContext = new PrepareContextView() {
				public Window getOwnerWindow() {
					return null;
				}

				public MapControl getMapControl() {
					return WizardVectorialDB.this
							.getMapCtrl();
				}
				
				public IProjection getViewProjection() {
					MapControl mapControl = getMapControl();
					if (mapControl!=null){
						return mapControl.getProjection();
					}
					return null;
				}

			};
		}
		return this.prepareContext;
	}

	protected DBStoreParameters getParameterForTable(TablesListItem table) {
		DBStoreParameters parameters = table.getParameters();

		UserTableSettingsVectorialPanel userTableSettingsPanel = (UserTableSettingsVectorialPanel) table
				.getUserTableSettingsPanel();

		Envelope _wa = userTableSettingsPanel.getWorkingArea();

		String geomField = userTableSettingsPanel.getGeoFieldName();
		String fidField = userTableSettingsPanel.getIdFieldName();
        //IF is a multiple PK, remove the {} symbols
        if (fidField.startsWith("{") && fidField.endsWith("}")) {
            fidField = fidField.substring(1, fidField.length()-1);
        }
        String[] pkFields = fidField.split(",");        
            
        String[] fields = table.getUserSelectedFieldsPanel()
                .getUserSelectedFields(pkFields, geomField);

		if (userTableSettingsPanel.isSqlActive()) {
			String whereClause = userTableSettingsPanel.getWhereClause();
			parameters.setBaseFilter(whereClause);
		} else {
			parameters.setBaseFilter("");
		}

		parameters.setFields(fields);
		parameters.setDefaultGeometryField(geomField);
		if (_wa != null) {
			parameters.setWorkingArea(_wa);
		}
		
        // ======= Setting PK from combo box
        if (pkFields != null) {
            parameters.setPkFields(pkFields);
        }
        // =========================

		IProjection proj_selected_by_user = userTableSettingsPanel.getProjection();
		parameters.setCRS(proj_selected_by_user);

		return parameters;

	}


}