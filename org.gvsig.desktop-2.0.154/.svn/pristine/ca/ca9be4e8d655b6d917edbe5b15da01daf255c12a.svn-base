/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.legend.gui.ISymbolSelector;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.gui.beans.swing.JNumberSpinner;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;


/**
 * Implements a tab to modify attributes of a mask as style,size and
 * symbol (to represent a point in the map)which can be applied to
 * symbols like simple text, simple marker,picture marker and character marker.<p>
 * <p>
 * This tab is used several times in different places in our applicattion .For
 * this reason, in order to avoid the repetition of code, this class has been
 * created (instead of treat it like a simple tab). With this solution, the user
 * only has to refer it to use it (and do not need to create a tab and fill it again
 * and so on).
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class Mask extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 101766990165772454L;
	private JButton btnHaloSymbol;
	private JRadioButton rdBtnHalo;
	private JRadioButton rdBtnNone;
	private JNumberSpinner txtHaloSize;
	private IFillSymbol fill;
	private AbstractTypeSymbolEditor owner;
	private ActionListener action = new ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent e) {
			owner.fireSymbolChangedEvent();
		};
	};
	/**
	 * Constructor method that initializes the parameters to create a tab to modify
	 * attributes of a mask for points such as style,size and symbol (to represent a point
	 * in the map).
	 * @param owner
	 */
	public Mask(AbstractTypeSymbolEditor owner) {
		super();
		setName(Messages.getText("mask"));

		this.owner = owner;

		GridBagLayoutPanel aux = new GridBagLayoutPanel();
		aux.setBorder(BorderFactory.createTitledBorder(Messages.getText("style")));
		JPanel stylePanel = new JPanel(new GridLayout(2, 1));
		stylePanel.add(getRdNone());
		stylePanel.add(getRdHalo());
		aux.addComponent(stylePanel);
		ButtonGroup group = new ButtonGroup();
		group.add(getRdNone());
		group.add(getRdHalo());

		JPanel aux2 = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
		aux2.add(new JLabel(Messages.getText("size")+":"));
		aux2.add(getTxtHaloSize());
		aux2.add(getBtnHaloSymbol());

		getRdNone().addActionListener(action);
		getRdHalo().addActionListener(action);
		getTxtHaloSize().addActionListener(action);

		add(aux);
		add(aux2);
	}
	/**
	 * Obtains the size for the text halo.This size is taken from a
	 * JNumberSpinner. If this component does not exist,
	 * a new JNumberSpinner is created to specify it.
	 * @return
	 */
	private JNumberSpinner getTxtHaloSize() {
		if (txtHaloSize == null) {
			txtHaloSize = new JNumberSpinner(0, 5, 0, Double.MAX_VALUE, 1);
		}

		return txtHaloSize;
	}

	/**
	 * Creates the button that allows the user to select the symbol that will substitute
	 * a point in the map.
	 * @return
	 */
	private JButton getBtnHaloSymbol() {
		if (btnHaloSymbol == null) {
			btnHaloSymbol = new JButton(Messages.getText("symbol"));
			btnHaloSymbol.addActionListener(new ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ISymbolSelector symSel = SymbolSelector.createSymbolSelector(
							fill, Geometry.TYPES.SURFACE);
					PluginServices.getMDIManager().addCentredWindow(symSel);
					fill = (IFillSymbol) symSel.getSelectedObject();
				};
			});
		}

		return btnHaloSymbol;
	}

	/**
	 * Determines if the halo style is selected.If the Radio button
	 * that determines this information does not exist, a new radio button
	 * is created for this purpose.
	 * @return
	 */
	private JRadioButton getRdHalo() {
		if (rdBtnHalo == null) {
			rdBtnHalo = new JRadioButton(Messages.getText("halo"));
		}

		return rdBtnHalo;
	}

	/**
	 * Determines if there will be no defined style for a point (without halo).
	 * If the Radio button that determines this information does not exist,
	 * a new radio button is created for this purpose.
	 * @return
	 */
	private JRadioButton getRdNone() {
		if (rdBtnNone == null) {
			rdBtnNone = new JRadioButton(Messages.getText("none"));
			rdBtnNone.setSelected(true);
		}

		return rdBtnNone;
	}
	/**
	 * Sets the graphical component that shows the properties of the model.
	 * @param mask
	 */
	public void setModel(IMask mask) {
		if (mask != null) {
			getTxtHaloSize().setDouble(mask.getSize());
			fill = mask.getFillSymbol();
		}
		getRdHalo().setSelected(mask != null);
	}
	/**
	 * Returns an IMask or null depending on the option
	 * that the user had decided (if he wants a mask or not)in the tab "mask" inside
	 * the panel to edit the properities of a symbol (SymbolEditor).
	 * If the user
	 * wants it, a new IMask is created.
	 * @return
	 */
	public IMask getMask() {
		if (!getRdHalo().isSelected()) return null;

		IMask mask = SymbologyLocator.getSymbologyManager().createMask();
		if (fill == null) {
			fill =
					(IFillSymbol) MapContextLocator.getSymbolManager()
					.createSymbol(IFillSymbol.SYMBOL_NAME);
		}
		mask.setFillSymbol(fill);
		mask.setSize(getTxtHaloSize().getDouble());
		return mask;
	}

}
