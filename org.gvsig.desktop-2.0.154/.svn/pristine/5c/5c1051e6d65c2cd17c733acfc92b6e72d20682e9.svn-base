/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.persistence;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.LegendLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.symbol.DummyVectorLegend;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapContextPersistenceTest extends AbstractLibraryAutoInitTestCase {

	final static private Logger logger =
			LoggerFactory.getLogger(MapContextPersistenceTest.class);

	protected void doSetUp() throws Exception {
		DummyVectorLegend.registerPersistent();
		DummyDBFeatureStore.registerPersistent();
		DummyFileFeatureStore.registerPersistent();

		MapContextLocator.getMapContextManager().registerLegend("DummyLegend",
				DummyVectorLegend.class);
		MapContextLocator.getMapContextManager().setDefaultVectorLegend(
				"DummyLegend");
	}
	
	private File getTempFile() throws Exception {

		File tempFile = File.createTempFile("persisted_mapcontext", ".xml");
		if (tempFile.exists())
			tempFile.delete();
		tempFile.createNewFile();
		
		System.out.println("TEMP FILE: " + tempFile.getAbsolutePath());
		return tempFile;
	}
	
	public void testMapContext() throws Exception {
		ViewPort vp = getViewPort();
		logger.debug("Creating mapcontext...");
		MapContext mc = new MapContext(vp);
		addDummyLayers(mc);
		doTestPersist(mc);
	}

	public void noTestFLyrs() throws Exception {
		
		int lyr_count = 3;
		// testPersist(mc);
		FLyrVect[] lyr = new FLyrVect[lyr_count];
		FeatureStore[] fs = new FeatureStore[lyr_count];
		FLayers lyrs = new FLayers();
		
		for (int i=0; i<lyr_count; i++) {
			fs[i] = new DummyFileFeatureStore("" + System.currentTimeMillis());
			lyr[i] = createLayer(fs[i]);
			lyr[i].setName("Layer " + (i+System.currentTimeMillis()));
			lyr[i].setIsLabeled(false);
			try { lyr[i].setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) {
				assertTrue("Error while creating legend: " + e.getMessage(), false);
			}
			lyrs.addLayer(lyr[i]);
			lyr[i].dispose();
		}
		
		for (int i=0; i<lyr_count; i++) {
			fs[i] = new DummyDBFeatureStore("" + System.currentTimeMillis());
			lyr[i] = createLayer(fs[i]);
			lyr[i].setName("Layer " + (10000 + i + System.currentTimeMillis()));
			lyr[i].setIsLabeled(false);
			try { lyr[i].setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) {
				assertTrue("Error while creating legend: " + e.getMessage(), false);
			}
			lyrs.addLayer(lyr[i]);
			lyr[i].dispose();
		}
		
		doTestPersist(lyrs);
	}
	
	
	
	private ViewPort getViewPort() throws LocatorException, CreateEnvelopeException {
		
		ViewPort vp = new ViewPort(CRSFactory.getCRS("EPSG:4326"));
		vp.setImageSize(new Dimension(640, 480));
		Envelope env = createEnvelope2D(0, 0, 1000000, 1000000);
		vp.setEnvelope(env);
		env = createEnvelope2D(200000, 200000, 500000, 500000);
		vp.setEnvelope(env);
		env = createEnvelope2D(300000, 300000, 300000, 300000);
		vp.setEnvelope(env);
		env = createEnvelope2D(400000, 400000, 200000, 200000);
		vp.setEnvelope(env);
		env = createEnvelope2D(440000, 440000, 100000, 100000);
		vp.setEnvelope(env);
		env = createEnvelope2D(480000, 480000, 30000, 30000);
		vp.setEnvelope(env);
		vp.setBackColor(Color.YELLOW);
		vp.setClipRect(new Rectangle2D.Double(0, 0, 10, 10));
		return vp;
	}
	
	private Envelope createEnvelope2D(double minX,double minY,double maxX, double maxY) throws LocatorException, CreateEnvelopeException {
		return GeometryLocator.getGeometryManager().createEnvelope(minX, minY, maxX, maxY, Geometry.SUBTYPES.GEOM2D);
	}

	private void addDummyLayers(MapContext mcon) throws Exception {
		
		FLayers resp = new FLayers();
		resp.setMapContext(mcon);
		resp.setName("root layer");
		
		FLayers aux = new FLayers();
		aux.setMapContext(mcon);
		aux.setName("Group");
		
		FLyrVect lyr = null;
		IVectorLegend lgn = null;
		FeatureStore fs = null;
		
		logger.debug("Adding dummy layers...");
			
		fs = new DummyFileFeatureStore("1");
		lyr = createLayer(fs);
		lyr.setName("Layer 1");
		try { lyr.setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) { }
		
		aux.addLayer(lyr);
		lyr.dispose();
		
		fs = new DummyDBFeatureStore("A");
		lyr = createLayer(fs);
		lyr.setName("Layer A");
		try { lyr.setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) { }

		aux.addLayer(lyr);
		lyr.dispose();
		
		fs = new DummyFileFeatureStore("2");
		lyr = createLayer(fs);
		lyr.setName("Layer 2");
		try { lyr.setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) { }

		aux.addLayer(lyr);
		lyr.dispose();
		
		fs = new DummyDBFeatureStore("B");
		lyr = createLayer(fs);
		lyr.setName("Layer 1");
		try { lyr.setLegend(new DummyVectorLegend(TYPES.SOLID)); } catch (LegendLayerException e) { }
		
		resp.addLayer(lyr);
		resp.addLayer(aux);
		lyr.dispose();
		aux.dispose();
		
		mcon.getLayers().addLayer(resp);
		resp.dispose();
	}

	private FLyrVect createLayer(FeatureStore fs) throws Exception {
		
		FLyrVect resp = new FLyrVect();
		resp.setDataStore(fs);
		resp.wakeUp();
		resp.load();
		return resp;
	}

	private void doTestPersist(Persistent p) throws Exception {
		
		System.out.println("Starting persistence test for class: " + p.getClass().getName());

		File perfile = getTempFile();
		FileOutputStream fos = new FileOutputStream(perfile);
		System.out.println("Getting state for class: " + p.getClass().getName());
		PersistentState pst = ToolsLocator.getPersistenceManager().getState(p);
		System.out.println("Saving state...");

		System.out.println("Saving state...");
		ToolsLocator.getPersistenceManager().saveState(pst, fos);
		System.out.println("Saving state... done.");

		fos.close();

		// ==============================================
		// ==============================================
		System.out.println("Is now persisted: " + p.getClass().getName());
		// ==============================================

		// if (true) return;

		System.out.println("Opening persistence file...");
		FileInputStream fis = new FileInputStream(perfile);

		System.out.println("Loading state from file...");
		pst = ToolsLocator.getPersistenceManager().loadState(fis);

		System.out.println("Instantiating " + p.getClass().getName()
				+ " from state, pst = " + pst);
		Object p2 = ToolsLocator.getPersistenceManager().create(pst);

		System.out.println("Comparing original and current...");
		comparePersistent(p, p2);

		System.out.println("Successful mapcontext persistence test!");
		
	}

	
	// FeatureStore, ILegend, ViewPort
	private void comparePersistent(Persistent p, Object p2) {
		
		if (p2.getClass().getName().compareTo(p.getClass().getName()) == 0) {
			
			if (p instanceof MapContext) {
				compareMapContexts((MapContext) p, (MapContext) p2);
			} else {
				if (p instanceof FLayer) {
					compareLayers((FLayer) p, (FLayer) p2);
				} else {
					if (p instanceof FeatureStore) {
						compareStore((FeatureStore) p, (FeatureStore) p2);
					} else {
						if (p instanceof ILegend) {
							compareLegend((ILegend) p, (ILegend) p2);
						} else {
							if (p instanceof ViewPort) {
								compareViewPorts((ViewPort) p, (ViewPort) p2);
							} else {
								fail("Did not compare: " + p.getClass().getName());
							}
						}
					}
				}
			}
		} else {
			fail("Comparing different classes (?)");
		}
		
	}

	private void compareMapContexts(MapContext m1, MapContext m2) {
		
		logger.debug("Getting viewports to compare...");
		ViewPort vp1 = m1.getViewPort();
		ViewPort vp2 = m2.getViewPort();
		
		compareViewPorts(vp1, vp2);

		logger.debug("Getting root flayers to compare...");
		FLayers lyr1 = m1.getLayers();
		FLayers lyr2 = m2.getLayers();
		compareLayers(lyr1, lyr2);
	}

	private void compareLayers(FLayer l1, FLayer l2) {
		
		logger.debug("Comparing layers...");

		assertTrue("Layers of diff type!", l1.getClass().getName().compareTo(l2.getClass().getName()) == 0);
		if (l1 instanceof FLayers) {
			
			logger.debug("(type FLayers)...");
			FLayers ls1 = (FLayers) l1;
			FLayers ls2 = (FLayers) l2;
			assertEquals(ls1.getLayersCount(), ls2.getLayersCount());
			int sz = ls2.getLayersCount();
			for (int i=0; i<sz; i++) {
				compareLayers(ls1.getLayer(i), ls2.getLayer(i));
			}
			
		} else {
			if (l1 instanceof FLyrVect) {
				
				logger.debug("(type FLyrVect)...");
				FLyrVect ls1 = (FLyrVect) l1;
				FLyrVect ls2 = (FLyrVect) l2;
				compareVectLayers(ls1, ls2);
				
			} else {
				
			}
		}

	}

	private void compareVectLayers(FLyrVect l1, FLyrVect l2) {
		compareLegend(l1.getLegend(), l2.getLegend());
		compareStore(l1.getFeatureStore(), l2.getFeatureStore());
	}

	private void compareStore(FeatureStore fs1,
			FeatureStore fs2) {
		
		logger.debug("Comparing stores...");
		assertTrue("Diff stores!", fs1.getName().compareTo(fs2.getName()) == 0);
	}

	private void compareLegend(ILegend l1, ILegend l2) {

		logger.debug("Comparing legends...");
		IVectorLegend vl1 = (IVectorLegend) l1;
		IVectorLegend vl2 = (IVectorLegend) l2;
		assertTrue("Diff legends!", vl1.getShapeType() == vl2.getShapeType());
		
	}

	private void compareViewPorts(ViewPort vp1, ViewPort vp2) {


		logger.debug("Comparing viewports...");
		
		assertEquals(
				vp1.getAdjustedEnvelope().getMinimum(0),
				vp2.getAdjustedEnvelope().getMinimum(0),
				0.00000000000001);
		assertEquals(
				vp1.getAdjustedEnvelope().getMinimum(1),
				vp2.getAdjustedEnvelope().getMinimum(1),
				0.00000000000001);
		assertEquals(
				vp1.getAdjustedEnvelope().getMaximum(0),
				vp2.getAdjustedEnvelope().getMaximum(0),
				0.00000000000001);
		assertEquals(
				vp1.getAdjustedEnvelope().getMaximum(1),
				vp2.getAdjustedEnvelope().getMaximum(1),
				0.00000000000001);
	}
	
}