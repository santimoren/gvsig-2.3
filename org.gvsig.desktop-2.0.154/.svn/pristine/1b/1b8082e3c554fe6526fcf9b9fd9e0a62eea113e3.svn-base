/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.operation.ensureOrientation;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * Use this function class to ensure you get real polygons or holes
 * En JTS, con bCCW = false obtienes un poligono exterior.
 * Nota: Solo se le da la vuelta (si es que lo necesita) al
 * poligono exterior. El resto, por ahora, no se tocan.
 * Si se necesita tenerlos en cuenta, habr�a que mirar
 * si est�n dentro del otro, y entonces revisar que tiene
 * un CCW contrario al exterior.
 * @param bCCW true if you want the GeneralPath in CCW order
 * @return true si se le ha dado la vuelta. (true if flipped)
 * TODO: TERMINAR ESTO!! NO EST� COMPLETO!! NO sirve para multipoligonos
 */
/**
 * @author Carlos S�nchez Peri��n <a href = "mailto:csanchez@prodevelop.es"> e-mail </a>
 */
public class EnsureOrientation extends GeometryOperation{
    public static final String NAME = "ensureOrientation";
	public static final int CODE = GeometryLocator.getGeometryManager().
    	getGeometryOperationCode(NAME);

	private Boolean bCCW;

	public int getOperationIndex() {
		return CODE;
	}

	public Object invoke(Geometry geom, GeometryOperationContext ctx)throws GeometryOperationException {


		bCCW = (Boolean) ctx.getAttribute("bCCW");
		if (bCCW==null)
			throw new GeometryOperationException(new Exception("The function Parameter hasn't been passed or is null."));

		try {
            return new Boolean(geom.ensureOrientation(bCCW.booleanValue()));
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryOperationException(e);
        }
	}

}
