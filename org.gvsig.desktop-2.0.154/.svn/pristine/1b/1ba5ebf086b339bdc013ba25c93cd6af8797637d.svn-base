/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.preferences.general;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.config.generate.PluginConfig;
import org.gvsig.andami.plugins.config.generate.SkinExtension;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.andami.ui.mdiManager.MDIManagerFactory;
import org.gvsig.utils.XMLEntity;
import org.gvsig.utils.swing.JComboBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SkinPreferences extends AbstractPreferencePage {

	private static final Logger logger = LoggerFactory.getLogger(SkinPreferences.class);
	private String id;
	private ImageIcon icon;
	private Vector listSkinsPlugins;
	private JComboBox comboBox;
	private String skinName = "org.gvsig.coreplugin.mdiManager.NewSkin";

	public SkinPreferences() {
		super();
		// TODO Auto-generated constructor stub
		id = this.getClass().getName();
		setParentID(GeneralPage.id);
		icon = PluginServices.getIconTheme().get("edit-setup-skin");
	}

	public void setChangesApplied() {
		// System.out.println("ESTOY LLAMANDO A setChangesApplied()");

	}

	public void storeValues() throws StoreException {
		// System.out.println("ESTOY LLAMANDO A storeValues()");
		PluginServices ps = PluginServices.getPluginServices("org.gvsig.coreplugin");
		XMLEntity xml = ps.getPersistentXML();
		xml.putProperty("Skin-Selected", skinName);
	}

	public String getID() {
		return id;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public JPanel getPanel() {

		if (comboBox == null) {
			comboBox = getComboBox();

			addComponent(new JLabel(PluginServices.getText(this, "skin_label")));

			addComponent(comboBox);
		}

		return this;
	}

	private JComboBox getComboBox() {
		comboBox = new JComboBox(listSkinsPlugins);

		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				String newSkinName = (String) cb.getSelectedItem();
				if (newSkinName != null)
					if (!newSkinName.equals(skinName)) {
						skinName = newSkinName;
						JOptionPane.showMessageDialog(null, PluginServices
								.getText(this, "skin_message"));
					}
			}

		});

		comboBox.setSelectedItem(skinName);
		return comboBox;
	}

	public String getTitle() {
		// TODO Auto-generated method stub
		return PluginServices.getText(this, "skin");
	}

	public void initializeDefaults() {
		// TODO Auto-generated method stub
		// System.out.println("inicialize Defaults");

	}

	public void initializeValues() {
		listSkinsPlugins = new Vector();

		HashMap pluginsConfig = Launcher.getPluginConfig();
		Iterator i = pluginsConfig.keySet().iterator();

		while (i.hasNext()) {
			String name = (String) i.next();
			PluginConfig pc = (PluginConfig) pluginsConfig.get(name);

			if (pc.getExtensions().getSkinExtension() != null) {
				SkinExtension[] se = pc.getExtensions().getSkinExtension();
				for (int j=0;j<se.length;j++){
					String extensionName = se[j].getClassName();
					listSkinsPlugins.add(extensionName );
					logger.info("Skin plugin '"+name + "', extension '"+ extensionName +".");
				}
			}
		}


		PluginServices ps = PluginServices.getPluginServices("org.gvsig.coreplugin");
		XMLEntity xml = ps.getPersistentXML();
		if (xml.contains("Skin-Selected")) {
			skinName = xml.getStringProperty("Skin-Selected");

		}



	}

	public boolean isValueChanged() {
		// TODO Auto-generated method stub
		// System.out.println("is value changed");
		return true;
	}

}
