/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.messages.MessageEvent;
import org.gvsig.andami.messages.NotificationListener;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiFrame.NewStatusBar;
import org.gvsig.tools.dynobject.DynObject;

import static org.gvsig.coreplugin.Consola.consolaFrame;

/**
 * Plugin que escucha las notificaciones que recive la aplicación y las muestra
 * en la barra de estado
 */
public class StatusBar extends Extension implements NotificationListener {

    public void initialize() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        PluginServices plugin = pluginsManager.getPlugin(this.getClass());
        
        DynObject pluginProperties = plugin.getPluginProperties();
        Boolean showNotificationsInStatusbar = (Boolean) pluginProperties.getDynValue("showNotificationsInStatusbar");
        if ( showNotificationsInStatusbar == null || showNotificationsInStatusbar.booleanValue() ) {
            NotificationManager.addNotificationListener(this);
        }
    }

    public void errorEvent(MessageEvent e) {
    	String msg = e.getMessages()[0];
        NewStatusBar statusbar = PluginServices.getMainFrame().getStatusBar();
        statusbar.message(msg, JOptionPane.ERROR_MESSAGE);
    }

    public void warningEvent(MessageEvent e) {
        String msg = e.getMessages()[0];
        NewStatusBar statusbar = PluginServices.getMainFrame().getStatusBar();
        statusbar.message(msg, JOptionPane.WARNING_MESSAGE);
    }

    public void infoEvent(MessageEvent e) {
        String msg = e.getMessages()[0];
        NewStatusBar statusbar = PluginServices.getMainFrame().getStatusBar();
        statusbar.message(msg, JOptionPane.INFORMATION_MESSAGE);
    }

    public void execute(String actionCommand) {
        // Do nothing
    }

    public boolean isEnabled() {
        return false;
    }

    public boolean isVisible() {
        return false;
    }

}
