/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.jdbc;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.store.jdbc.JDBCNewStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorer;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class ExporrtoJDBCService extends AbstractMonitorableTask implements
        ExporttoService {

    private static final Logger logger = LoggerFactory.getLogger(ExporrtoJDBCService.class);

    public static final int CHECK_NONE = 0;
    public static final int CHECK_IF_CORRUPT = 1;
    public static final int CHECK_IF_VALID = 2;

    public static final int ACTION_SET_GEOMETRY_TO_NULL = 0;
    public static final int ACTION_SKIP_FEATURE = 1;
    public static final int ACTION_ABORT = 2;

    private ExporttoServiceFinishAction exporttoServiceFinishAction = null;
    private ExporttoJDBCOptions options;
//    private Map<String,String> translationIds;

    public ExporrtoJDBCService(ExporttoJDBCOptions options) {
        this.options = options;
    }

    protected String getTranslatedIdentifier(String identifier) {
        String s = identifier;
        if (this.options.getTranslateIdentifiersToLowerCase()) {
            s = s.toLowerCase();
        }
        if (this.options.getRemoveSpacesInIdentifiers()) {
            s = StringUtils.normalizeSpace(s).replace(" ", "_");
        }
        return s;
    }

    protected void createTable(JDBCServerExplorer explorer) throws Exception {

        FeatureType targetFeatureType;
        EditableFeatureType targetEditableFeatureType;

        targetFeatureType = options.getSource().getDefaultFeatureType().getCopy();
        targetEditableFeatureType = targetFeatureType.getEditable();

        if (this.options.getTranslateIdentifiersToLowerCase()
                || this.options.getRemoveSpacesInIdentifiers()) {
            for (int i = 0; i < targetEditableFeatureType.size(); i++) {
                EditableFeatureAttributeDescriptor x = (EditableFeatureAttributeDescriptor) targetEditableFeatureType.get(i);
                x.setName(getTranslatedIdentifier(x.getName()));
            }
        }

        if (this.options.getPrimaryKey() != null) {
            EditableFeatureAttributeDescriptor pk = (EditableFeatureAttributeDescriptor) targetEditableFeatureType.get(getTranslatedIdentifier(options.getPrimaryKey()));
            if (pk == null) {
                pk = targetEditableFeatureType.add(
                        getTranslatedIdentifier(this.options.getPrimaryKey()),
                        DataTypes.LONG
                );
                pk.setIsPrimaryKey(true);
                pk.setIsAutomatic(true);
            } else {
                pk.setIsPrimaryKey(true);
            }
        }

        if (this.options.getCreateIndexInGeometryRow()) {
            EditableFeatureAttributeDescriptor x = (EditableFeatureAttributeDescriptor) targetEditableFeatureType.getDefaultGeometryAttribute();
            x.setIsIndexed(true);
        }

        // ======================================================
        // Reprojection: set SRS of geometry field to target SRS
        EditableFeatureAttributeDescriptor attrdescriptor
                = (EditableFeatureAttributeDescriptor) targetEditableFeatureType.getDefaultGeometryAttribute();
        if (attrdescriptor != null) {
            attrdescriptor.setSRS(this.options.getTargetProjection());
        }

        // ======================================
        JDBCNewStoreParameters createTableParams = (JDBCNewStoreParameters) explorer.getAddParameters();

        createTableParams.setSelectRole(this.options.getSelectRole());
        createTableParams.setInsertRole(this.options.getInsertRole());
        createTableParams.setUpdateRole(this.options.getUpdateRole());
        createTableParams.setDeleteRole(this.options.getDeleteRole());
        createTableParams.setTruncateRole(this.options.getTruncateRole());
        createTableParams.setReferenceRole(this.options.getReferenceRole());
        createTableParams.setTriggerRole(this.options.getTriggerRole());
        createTableParams.setAllRole(this.options.getAllRole());

        createTableParams.setSchema(this.options.getSchema());
        createTableParams.setPostCreatingStatement(this.options.getPostCreatingStatement());

        createTableParams.setDefaultFeatureType(targetEditableFeatureType);
        createTableParams.setTable(this.options.getTableName());
        explorer.add(explorer.getStoreName(), createTableParams, true);
    }

    private static class InvalidGeometryException extends ExporttoServiceException {

        public InvalidGeometryException(Feature feature, String checkMessage) {
            super(checkMessage, null, checkMessage, 0);
            this.feature = feature;
        }
    }

    public void export(FeatureSet featureSet) throws ExporttoServiceException {
        Geometry.ValidationStatus geometryCheck;

        DisposableIterator it = null;
        EditableFeature targetFeature = null;
        FeatureStore target = null;

        try {

            // ======================================
            // Reprojection
            FeatureAttributeDescriptor geo_att = this.options.getSource().getDefaultFeatureType().getDefaultGeometryAttribute();
            IProjection sourceProjection = null;
            ICoordTrans coord_trans = null;
            Geometry reproj_geom = null;
            IProjection targetProjection = this.options.getTargetProjection();
            if (geo_att != null) {
                sourceProjection = geo_att.getSRS();
                // this comparison is perhaps too preventive
                // we could  have two instances of same projection
                // so we would do more computations than needed
                if (sourceProjection != null && targetProjection != null && sourceProjection != targetProjection) {
                    coord_trans = sourceProjection.getCT(targetProjection);
                }
            }
            // ============================================

            DataManager dataManager = DALLocator.getDataManager();

            JDBCServerExplorer explorer = (JDBCServerExplorer) dataManager.openServerExplorer(
                    this.options.getExplorerParameters().getExplorerName(),
                    this.options.getExplorerParameters()
            );

            if (this.options.canCreatetable()) {
                logger.debug("Creating table");
                taskStatus.message("Creating table");
                this.createTable(explorer);
            }

            JDBCStoreParameters openParams = (JDBCStoreParameters) explorer.getOpenParameters();
            openParams.setSchema(this.options.getSchema());
            openParams.setTable(this.options.getTableName());
            openParams.setCRS(targetProjection);
            openParams.setDefaultGeometryField(
                    this.options.getSource().getDefaultFeatureType().getDefaultGeometryAttributeName()
            );
            target = (FeatureStore) explorer.open(openParams);

            FeatureType targetFeatureType = target.getDefaultFeatureType();
            FeatureType sourceFeatureType = featureSet.getDefaultFeatureType();

            target.edit(FeatureStore.MODE_APPEND);

            int featureCount = 1;
            taskStatus.setRangeOfValues(0, featureSet.getSize());

            int targetGeometryIndex = -1;
            int sourceGeometryIndex = -1;
            if( getGeometryColumnCount(sourceFeatureType)==1
                    && getGeometryColumnCount(targetFeatureType)==1 ) {
                // Solo si hay una columna de geometria asignaremos las geometrias
                // independientemente de como se llamen los campos.
                targetGeometryIndex = targetFeatureType.getDefaultGeometryAttributeIndex();
                sourceGeometryIndex = sourceFeatureType.getDefaultGeometryAttributeIndex();
            }

            logger.debug("Inserting rows");
            taskStatus.message("Inserting rows");
            it = featureSet.fastIterator();
            while (it.hasNext()) {
                Feature feature = (Feature) it.next();
                this.taskStatus.setCurValue(featureCount);

                targetFeature = target.createNewFeature(targetFeatureType, feature);
                for (int i = 0; i < sourceFeatureType.size(); i++) {
                    if (i == sourceGeometryIndex) {
                        // Es facil que los campos geometria no se llamen igual, asi que
                        // el campo geometria lo asignamos a capon.
                        // Esto puede ocasionar problemas cuando la tabla destino no tenga
                        // geometria o tenga mas de una.
                        targetFeature.set(targetGeometryIndex, feature.get(sourceGeometryIndex));
                    } else {
                        FeatureAttributeDescriptor x = sourceFeatureType.getAttributeDescriptor(i);
                        int targetAttributeIndex = targetFeatureType.getIndex(getTranslatedIdentifier(x.getName()));
                        if (targetAttributeIndex < 0) {
                            throw new RuntimeException("Can't locate column '" + x.getName() + "' in the target table.");
                        }
                        targetFeature.set(targetAttributeIndex, feature.get(x.getName()));
                    }
                }

                Geometry geometry = targetFeature.getDefaultGeometry();
                if (geometry != null) {
                    switch (this.options.getGeometryChecks()) {
                        case CHECK_IF_CORRUPT:
                            geometryCheck = geometry.getValidationStatus();
                            if (geometryCheck.getStatusCode() == Geometry.ValidationStatus.CURRUPTED) {
                                switch (this.options.getGeometryChecksAction()) {
                                    case ACTION_SET_GEOMETRY_TO_NULL:
                                        targetFeature.setDefaultGeometry(null);
                                        break;
                                    case ACTION_SKIP_FEATURE:
                                        continue;
                                    case ACTION_ABORT:
                                    default:
                                        throw new InvalidGeometryException(targetFeature, geometryCheck.getMessage());
                                }
                            }

                            break;

                        case CHECK_IF_VALID:
                            geometryCheck = geometry.getValidationStatus();
                            if (!geometryCheck.isValid()) {
                                Geometry g = null;
                                if (this.options.getTryToFixGeometry()) {
                                    g = geometry.makeValid();
                                    if (g != null) {
                                        targetFeature.setDefaultGeometry(g);
                                    }
                                }
                                if (g == null) {
                                    switch (this.options.getGeometryChecksAction()) {
                                        case ACTION_SET_GEOMETRY_TO_NULL:
                                            targetFeature.setDefaultGeometry(null);
                                            break;
                                        case ACTION_SKIP_FEATURE:
                                            continue;
                                        case ACTION_ABORT:
                                        default:
                                            throw new InvalidGeometryException(targetFeature, geometryCheck.getMessage());
                                    }
                                }
                            }

                            break;
                        case CHECK_NONE:
                        default:
                            break;
                    }
                    // ================================================
                    // Reprojection
                    if (geo_att != null && coord_trans != null) {
                        reproj_geom = targetFeature.getDefaultGeometry();
                        reproj_geom = reproj_geom.cloneGeometry();
                        reproj_geom.reProject(coord_trans);
                        targetFeature.setDefaultGeometry(reproj_geom);
                    }
                    // ================================================
                }

                target.insert(targetFeature);

                if (this.taskStatus.isCancellationRequested()) {
                    return;
                }
                featureCount++;
            }
            targetFeature = null;
            target.finishEditing();

            if (this.options.getUpdateTableStatistics()) {
                logger.debug("Updating statistics");
                taskStatus.message("Updating statistics");
                explorer.updateTableStatistics(openParams.tableID());
            }
            logger.debug("finish");

            if (exporttoServiceFinishAction != null) {
                exporttoServiceFinishAction.finished(
                        this.options.getTableName() + " (" + explorer.getStoreName() + ")",
                        openParams);
            }
            taskStatus.message("Exportation finished");

        } catch (Exception e) {
            logger.warn("Can't export data.", e);
            taskStatus.message("Problems exporting data");
            throw new ExporttoServiceException(e, targetFeature);

        } finally {
            if (it != null) {
                it.dispose();
            }
            featureSet.dispose();
            if (target != null) {
                target.dispose();
            }
            this.taskStatus.terminate();
            this.taskStatus.remove();
        }
    }

    private int getGeometryColumnCount(FeatureType featureType) {
        int count = 0;
        for( int i=0; i<featureType.size(); i++ ) {
            if( featureType.getAttributeDescriptor(i).getType()==DataTypes.GEOMETRY ) {
                count++;
            }
        }
        return count;
    }

    public void setFinishAction(
            ExporttoServiceFinishAction exporttoServiceFinishAction) {
        this.exporttoServiceFinishAction = exporttoServiceFinishAction;
    }

}
