/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * DOCUMENT ME!
 *
 */
public class SHPPointWriter implements SHPShapeWriter {
    private static final Logger logger = LoggerFactory.getLogger(SHPPointWriter.class);
    private int m_type;
    private Point point;

    /**
     * Crea un nuevo SHPPoint.
     *
     * @param type DOCUMENT ME!
     *
     * @throws ShapefileException DOCUMENT ME!
     */
    public SHPPointWriter(int type)  {
        m_type = type;
    }

    /**
     * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getShapeType()
     */
    public int getShapeType() {
        return m_type;
    }

    /**
     * @Deprecated Use SHPReader
     */
    @Deprecated
    public Geometry read(MappedByteBuffer buffer, int type) {
        return null;
    }

    /**
     * @see com.iver.cit.gvsig.fmap.shp.SHPShape#write(ByteBuffer, IGeometry)
     */
    public void write(ByteBuffer buffer) {
        buffer.putDouble(point.getX());
        buffer.putDouble(point.getY());
        double z;
        double m;

        if(m_type == SHP.POINT3D) { //Only POINT3D has Z
            z = point.getCoordinateAt(Geometry.DIMENSIONS.Z);
            if (Double.isNaN(z)) { // nan means not defined
                buffer.putDouble(0.0);
            } else {
                buffer.putDouble(z);
            }
        }

        if ((m_type == SHP.POINT3D) || (m_type == SHP.POINTM)){ //Both, POINT3D and POINTM, has M
            m = point.getCoordinateAt(point.getDimension()-1);
            if (Double.isNaN(m)) { // nan means not defined
                buffer.putDouble(0.0);
            } else {
                buffer.putDouble(m);
            }
        }
    }

    /**
     * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getLength(int)
     */
    public int getLength() {
        switch (m_type) {
        case SHP.POINT2D:
            return 20;
        case SHP.POINTM:
            return 28;
        case SHP.POINT3D:
            return 36;
        default:
            //Nunca deber�a pasar por aqu�
            return 20;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPShapeWriter#initialize(org.gvsig.fmap.geom.Geometry)
     */
    public void initialize(Geometry geometry) {
        point = (Point)geometry;
    }
}
