/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.DefaultPackageInfo;
import org.gvsig.installer.lib.impl.info.InstallerInfoFileReader;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatusManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class Decompress {

	private static final int OPTION_DECOMPRESS = 1;
	private static final int OPTION_READ_INSTALLINFO = 2;
	private static final int OPTION_INSTALL = 3;

	private int option = 0;

	private int BUFFER = 2048;

	private static final Logger logger = LoggerFactory
			.getLogger(Decompress.class);

	private List<PackageInfo> readedIinstallerInfos = null;
	private File outputDirectory = null;
	private List<PackageInfo> selectedInstallerInfos = null;
	private Map<PackageInfo, String> zipEntriesMap = null;
	private List<String> defaultSelectedPackets = null;

	public Decompress() {

	}

	public void decompressPlugins(InputStream is, File outputDirectory)
			throws InstallPackageServiceException {
		option = OPTION_DECOMPRESS;
		this.outputDirectory = outputDirectory;

		decompressFolderOfPlugins(is);
	}

	public void readPackageSetInstallInfos(InputStream is,
			List<PackageInfo> installerInfos,
			Map<PackageInfo, String> zipEntriesMap)
			throws InstallPackageServiceException {
		option = OPTION_READ_INSTALLINFO;
		this.readedIinstallerInfos = installerInfos;
		this.zipEntriesMap = zipEntriesMap;
		decompressFolderOfPlugins(is);
	}

	public void readPackageInstallInfo(InputStream is,
			List<PackageInfo> installerInfos,
			Map<PackageInfo, String> zipEntriesMap, String name)
			throws InstallPackageServiceException {
		option = OPTION_READ_INSTALLINFO;
		this.readedIinstallerInfos = installerInfos;
		this.zipEntriesMap = zipEntriesMap;
		decompressFolderOfPluginFromPackage(is, name);
	}

	public class InstallerPluginReadException extends
			InstallPackageServiceException {

		private static final long serialVersionUID = -6065359474790792680L;

		private static final String message = "Error reading the plugin";

		private static final String KEY = "_Error_reading_the_plugin";

		public InstallerPluginReadException(ServiceException e) {
			super(message, e, KEY, serialVersionUID);
		}

	}

	public void decompressPlugin(InputStream is, File outputDirectory)
			throws InstallPackageServiceException {
		try {
			option = OPTION_DECOMPRESS;
			this.outputDirectory = outputDirectory;
			FileUtils.forceMkdir(outputDirectory);
			logger.debug("decompress plugin to '"+outputDirectory.getAbsolutePath()+"'.");
			decompressPlugin(is);
		} catch (Exception e) {
			logger.warn("Can't decompress plugin in '"+outputDirectory+"'.", e);
			throw new InstallPackageServiceException(e);
		}
	}

	public class InstallerPluginReadErrorException extends
			InstallPackageServiceException {

		private static final long serialVersionUID = 4505298394252120334L;

		private static final String message = "Error reading the plugin '%(name)s'";

		private static final String KEY = "_Error_reading_the_plugin";

		public InstallerPluginReadErrorException(Exception e, String packageName) {
			super(message, e, KEY, serialVersionUID);
			setValue("name", packageName);
		}

	}

	public InputStream searchPlugin(InputStream is, String zipEntry)
			throws InstallPackageServiceException {
		ZipEntry entry = null;
		String name = "";

		try {
			ZipInputStream zipInputStream = new ZipInputStream(is);

			while ((entry = zipInputStream.getNextEntry()) != null) {
				name = entry.getName();
				if (entry.getName().equals(zipEntry)) {
					return zipInputStream;
				}
				zipInputStream.closeEntry();
			}
			zipInputStream.closeEntry();

			zipInputStream.close();
		} catch (Exception e) {
			throw new InstallerPluginReadErrorException(e, name);
		}
		return null;
	}

	public void installFromStream(InputStream is, PackageInfo installerInfo)
			throws InstallPackageServiceException {
		option = OPTION_INSTALL;
		this.selectedInstallerInfos = new ArrayList<PackageInfo>();
		this.selectedInstallerInfos.add(installerInfo);
		decompressFolderOfPlugins(is);
	}

	public void installFromStream(InputStream is,
			List<PackageInfo> installerInfos)
			throws InstallPackageServiceException {
		option = OPTION_INSTALL;
		this.selectedInstallerInfos = installerInfos;
		decompressFolderOfPlugins(is);
	}

	private void decompressFolderOfPlugins(InputStream is)
			throws InstallPackageServiceException {
		ZipInputStream zipInputStream = new ZipInputStream(is);
		ZipEntry entry = null;

		String name = "";

		try {
			while ((entry = zipInputStream.getNextEntry()) != null) {
				name = entry.getName();
				if (entry.getName().equals("defaultPackages")
						|| entry.getName().equals("defaultSelection")) {
					int count;
					byte data[] = new byte[BUFFER];
					ByteArrayOutputStream out = new ByteArrayOutputStream();

					while ((count = zipInputStream.read(data, 0, BUFFER)) != -1) {
						out.write(data, 0, count);
					}

					String str = out.toString().replace("\r", "");
					String lineas[] = str.split("\\n");
					List<String> defaultPackagesList = new ArrayList<String>();

					for (int i = 0; i < lineas.length; i++) {
					    
					    String trim_lineas = lineas[i].trim();
					    if (! ((trim_lineas.startsWith("#") || trim_lineas.startsWith(";")))) {
					        // not a comment
					        defaultPackagesList.add(lineas[i]);
					    }
						
					}

					defaultSelectedPackets = defaultPackagesList;
					out.flush();
				} else {
//					logger.debug("Extracting all Plugins, plugin: " + name);
					if (option == OPTION_INSTALL) {

					} else {
						if (option == OPTION_DECOMPRESS) {
							logger.info("decompress plugin " + name);
							decompressPlugin(zipInputStream);
						} else if (option == OPTION_READ_INSTALLINFO) {
							readPlugin(zipInputStream, entry.getName());
						}
					}
				}
				zipInputStream.closeEntry();
			}
			zipInputStream.close();

		} catch (Exception e) {
			throw new InstallerPluginReadErrorException(e, name);
		}
	}

	private void decompressFolderOfPluginFromPackage(InputStream is, String name)
			throws InstallPackageServiceException {

		try {
			if (option == OPTION_INSTALL) {

			} else {
				if (option == OPTION_DECOMPRESS) {
					decompressPlugin(is);
				} else {
					if (option == OPTION_READ_INSTALLINFO) {
						readPlugin(is, name);
					}
				}
			}

		} catch (Exception e) {
			throw new InstallerPluginReadErrorException(e, name);
		}
	}

	private void readPlugin(InputStream is, String zipEntryName)
			throws ZipException, IOException, InstallerInfoFileException {
		ZipEntry entry = null;
		int installerInfoNumber = zipEntriesMap.size();
		String packageInfoName = getPackageInfoFileName();
		String unixPackageInfoPath = "/".concat(packageInfoName);
		String windowsPackageInfoPath = "\\".concat(packageInfoName);
		ZipInputStream zis = new ZipInputStream(is);
		while ((entry = zis.getNextEntry()) != null) {
			logger.debug("Extracting: " + entry.getName());

			String name = entry.getName();
			// Just in case, but we are going to create them always using
			// the unix file separator
			if (name.endsWith(unixPackageInfoPath)
					|| name.endsWith(windowsPackageInfoPath)) {
				PackageInfo installerInfo = readInstallInfo(zis);
				zipEntriesMap.put(installerInfo, zipEntryName);
				readedIinstallerInfos.add(installerInfo);
			}
			zis.closeEntry();
		}
		// Don't close the stream as if it is a zip file contained
		// into another zip file, closing of the child Zip�nputStream
		// will close also the parent's.
		// zis.close();

		if (installerInfoNumber == zipEntriesMap.size()) {
			PackageInfo installerInfo = new DefaultPackageInfo();
			installerInfo.setCode(zipEntryName);
			installerInfo.setName(zipEntryName);
			zipEntriesMap.put(installerInfo, zipEntryName);
			readedIinstallerInfos.add(installerInfo);
		}
	}

	private void decompressPlugin(InputStream inputStream) throws ZipException,
			IOException, InstallerInfoFileException {

		ZipInputStream zis = null;
		ZipEntry entry = null;
		byte data[] = new byte[BUFFER];
		int count = 0;

		TaskStatusManager manager = ToolsLocator.getTaskStatusManager();
		SimpleTaskStatus taskStatus = manager
				.createDefaultSimpleTaskStatus("Uncompressing...");
		
		manager.add(taskStatus);
		int countRootFolders = 0;
		String entryName = "(header)";
		try {
			long readed = 0;
	
			// // First read all the contents size
			// zis = new ZipInputStream(inputStream);
			// while ((entry = zis.getNextEntry()) != null) {
			// count += entry.getSize();
			// }
			// // zis.close();
			// taskStatus.setRangeOfValues(0, count);
	
			// Return the stream to the initial position
			zis = new ZipInputStream(inputStream);
			while ((entry = zis.getNextEntry()) != null) {
				entryName = FilenameUtils.separatorsToSystem(entry.getName());
				taskStatus.message(entryName);
	
				File file = new File(outputDirectory, entryName);
				if (file.exists()) {
					logger.info("delete " + file.getAbsolutePath());
					FileUtils.forceDelete(file);
				}
				if (entry.isDirectory()) {
					String s = FilenameUtils.getPathNoEndSeparator(entryName);
					if( !s.contains(File.separator) ) {  
						if( ++countRootFolders > 1 ) {
							logger.warn("More than one root folder ("+entryName+") in the package; it may be an error.");
						}
					}
					logger.debug("makedirs " + file.getAbsolutePath());
					if( !file.mkdirs() ) {
                                           throw new IOException("Can't create folder '"+file.getAbsolutePath()+"'.");
                                        }
				} else {
					createParentFolder(file);
					logger.debug("extracting " + file.getAbsolutePath());
					FileOutputStream fos = new FileOutputStream(file);
					while ((count = zis.read(data, 0, BUFFER)) != -1) {
						fos.write(data, 0, count);
						readed += count;
						taskStatus.setCurValue(readed);
					}
					fos.flush();
					fos.close();
				}
			}
			zis.close();
		} catch(IOException ex) {
			logger.warn("Problems uncompresing plugin (last entry '"+entryName+"'.",ex);
			throw ex;
			
		} catch(RuntimeException ex) {
			logger.warn("Problems uncompresing plugin (last entry '"+entryName+"'.",ex);
			throw ex;
			
		} finally {
		    
		    taskStatus.remove();
		    
		}
		
        
	}

	private void createParentFolder(File file) {
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdirs();
		}
	}

	public boolean delete(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = delete(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public PackageInfo readInstallerInfo(InputStream is)
			throws InstallerInfoFileException {
		try {
			return readInstallInfo(new ZipInputStream(is));
		} catch (IOException e) {
			throw new InstallerInfoFileException("error_reading_installerinfo",
					e);
		}
	}

	private PackageInfo readInstallInfo(ZipInputStream zipInputStream)
			throws IOException, InstallerInfoFileException {
		int count;
		byte data[] = new byte[BUFFER];

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		while ((count = zipInputStream.read(data, 0, BUFFER)) != -1) {
			out.write(data, 0, count);
		}
		out.flush();

		DefaultPackageInfo installerInfo = new DefaultPackageInfo();
		InstallerInfoFileReader installerInfoFileReader = new InstallerInfoFileReader();
		installerInfoFileReader.read(installerInfo, new ByteArrayInputStream(
				out.toByteArray()));

		return installerInfo;
	}

	private String getPackageInfoFileName() {
		return InstallerProviderLocator.getProviderManager()
				.getPackageInfoFileName();
	}

	public List<String> getDefaultSelectedPackages() {
		return defaultSelectedPackets;
	}
}
