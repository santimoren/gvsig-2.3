/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.service.ServiceException;

/**
 * This class is responsible of the management of the library's swing user
 * interface. It is the swing library's main entry point, and provides all the
 * services to manage library swing components.
 * 
 * @see AnnotationWindowManager
 * @see JAnnotationCreationServicePanel
 * @author gvSIG team
 * @version $Id$
 */
public interface AnnotationSwingManager {

    /**
     * Returns the panel associated to a {@link AnnotationCreationService}.
     * 
     * @param annotationCreationService
     *            {@link AnnotationCreationService} contained on the panel
     * @return a {@link JAnnotationCreationServicePanel} with the panel of the
     *         {@link AnnotationCreationService}
     * @throws DataException 
     * @see JAnnotationCreationServicePanel
     * @see AnnotationCreationService
     */
    public JAnnotationCreationServicePanel createAnnotation(AnnotationCreationService annotationCreationService) throws ServiceException;
   
    public JAnnotationPreferencesPanel createAnnotationPreferences();
    
    /**
     * Returns the {@link AnnotationManager}.
     * 
     * @return {@link AnnotationManager}
     * @see {@link AnnotationManager}
     */
    public AnnotationManager getManager();


	public String getTranslation(String key);
	
    /**
     * Registers a new instance of a WindowManager which provides services to
     * the management of the application windows.
     * 
     * @param manager
     *            {@link LandRegistryViewerWindowManager} to register in the
     *            ScriptingUIManager.
     * @see LandRegistryViewerWindowManager
     */
    public void registerWindowManager(AnnotationWindowManager manager);

    /**
     * Returns the {@link LandRegistryViewerWindowManager}.
     * 
     * @return {@link LandRegistryViewerWindowManager}
     * @see {@link LandRegistryViewerWindowManager}
     */
    public AnnotationWindowManager getWindowManager();
}
