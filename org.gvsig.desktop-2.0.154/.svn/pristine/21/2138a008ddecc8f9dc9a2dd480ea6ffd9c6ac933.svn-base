/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {New extension for installation and update of text translations}
 */
package org.gvsig.i18n.extension;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.i18n.Messages;
import org.gvsig.i18n.impl.TranslationsConsolider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Texts localization management extension.
 *
 * @author <a href="mailto:dcervera@disid.com">David Cervera</a>
 */
public class ConsolideTranslationsExtension extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(ConsolideTranslationsExtension.class);

    private boolean executing = false;

    public synchronized void execute(String actionCommand) {
        final MainFrame main = PluginServices.getMainFrame();

        if ( "tools-devel-consolidate-translations".equalsIgnoreCase(actionCommand) ) {
            if ( this.executing ) {
                main.messageDialog("Process is running.", "Warning", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            this.executing = true;
            main.refreshControls();
            Thread process = new Thread(new Runnable() {
                public void run() {
                    try {
                        TranslationsConsolider consolider = new TranslationsConsolider();
                        consolider.consolide();
                        main.messageDialog("Consolidation of translations terminated.", "Information", JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception ex) {
                        logger.warn("There was errors consolidating translations.", ex);
                        main.messageDialog("There was errors consolidating translations.", "Warning", JOptionPane.WARNING_MESSAGE);
                    } finally {
                        executing = false;
                        main.refreshControls();
                    }
                }
            });
            process.start();
        } else if ( "tools-devel-not-translated-keys".equalsIgnoreCase(actionCommand) ) {
            PluginsManager pluginsManager = PluginsLocator.getManager();
            
            File folder = new File(pluginsManager.getApplicationI18nFolder(), "translations.all");
            
            List notTranslatedKeys = Messages.getNotTranslatedKeys();
            Properties properties = new Properties();
            for ( int i = 0; i < notTranslatedKeys.size(); i++ ) {
                String key = (String) notTranslatedKeys.get(i);
                properties.put(key, "");
            }

            FileOutputStream fos = null;
            File f = new File(folder, "nottranslated.properties");
            try {
                FileUtils.forceMkdir(folder);
                fos = new FileOutputStream(f);
                properties.store(fos, null);
            } catch (Exception ex) {
                logger.warn("Can't write properties '" + f.getAbsolutePath() + "'.", ex);
            } finally {
                IOUtils.closeQuietly(fos);
            }
        }
    }

    public void initialize() {
        // Do nothing
    }

    public boolean isEnabled() {
        return !this.executing;
    }

    public boolean isVisible() {
        return true;
    }
}
