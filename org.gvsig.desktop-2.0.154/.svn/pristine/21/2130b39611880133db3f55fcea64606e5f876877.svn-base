/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.lib.basic.impl;

import org.gvsig.metadata.exceptions.AddMetadataDefinitionException;

public class CantFindDefinitionInStreamException extends AddMetadataDefinitionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 450922621905049558L;

	public CantFindDefinitionInStreamException(String name) {
		super(
			"Can't find metadata definition %(name) in stream.", 
			"_cant_find_metadata_definition_XnameX_in_stream", 
			serialVersionUID
		);
		setValue("name",name);
	}

	public CantFindDefinitionInStreamException(String name, String found) {
		super(
				"Can't find metadata definition %(name) in stream, found %(found).", 
				"_cant_find_metadata_definition_XnameX_in_stream_found_XfoundX", 
				serialVersionUID
			);
		setValue("name",name);
		setValue("found",found);
	}
}
