/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp;

import junit.framework.TestCase;


import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Code to test bug gvsig-desktop#15642. This is based on the code provided by
 * the bug reporter (see ticket), thanks to him!!
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Bug15642Test extends TestCase {

    public void testBug15642() throws Exception {
        new DefaultLibrariesInitializer().fullInitialize();
        DataManager manager = DALLocator.getDataManager();
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        NewFeatureStoreParameters destParams =
                (NewFeatureStoreParameters) manager.createNewStoreParameters(
                    "FilesystemExplorer", "Shape");

        EditableFeatureType type = destParams.getDefaultFeatureType();
        GeometryType geometryType =
            geometryManager.getGeometryType(Geometry.TYPES.POINT,
                Geometry.SUBTYPES.GEOM2D);
        type.add("geom", org.gvsig.fmap.geom.DataTypes.GEOMETRY)
            .setGeometryType(geometryType);
        type.setDefaultGeometryAttributeName("geom");
        type.add("float", DataTypes.FLOAT).setSize(5);
        type.add("long", DataTypes.LONG).setSize(5);

        destParams.setDynValue("shpfile", "/tmp/mySHP.shp");
        destParams.setDynValue("dbffile", "/tmp/mySHP.dbf");
        destParams.setDynValue("shxfile", "/tmp/mySHP.shx");
        destParams.setDynValue("crs", "EPSG:23030");
        destParams.setDefaultFeatureType(type);

        manager.newStore("FilesystemExplorer", "Shape", destParams, true);
        FeatureStore store =
            (FeatureStore) manager.openStore("Shape", destParams);

        store.edit();
        EditableFeature feature = store.createNewFeature().getEditable();
        Geometry fmapGeom = GeometryLocator.getGeometryManager().createPoint(0,0,Geometry.SUBTYPES.GEOM2D);
        
        feature.setGeometry(feature.getType()
            .getDefaultGeometryAttributeIndex(), fmapGeom);
        feature.setFloat("float", 34.0f);
        feature.setLong("long", 34l);
        store.insert(feature);
        store.finishEditing();

        FeatureAttributeDescriptor defaultGeometryAttribute =
            store.getDefaultFeatureType().getDefaultGeometryAttribute();

        geometryType = defaultGeometryAttribute.getGeomType();
        assertEquals(Geometry.TYPES.POINT, geometryType.getType());
        assertEquals(Geometry.SUBTYPES.GEOM2D, geometryType.getSubType());

        store.dispose();
    }

}
