/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpatialCache  {
    
    private static final Logger logger = LoggerFactory.getLogger(SpatialCache.class);
    private int maxFeatures = 1000; // Por defecto, pero se puede cambiar
	private int fastNumTotalRegs=0;
	private SpatialIndex index = null;
	
	private boolean enabled = false;

	public SpatialCache() {
		try {
			this.index = GeometryLocator.getGeometryManager().createDefaultMemorySpatialIndex();
		} catch (Exception e) {
			logger.info("Can't create spatial index", e);
		}
	}
	
	public int getMaxFeatures() {
		return maxFeatures;
	}

	public void setMaxFeatures(int maxFeatures) {
		this.maxFeatures = maxFeatures;
	}

	public synchronized void insert(Envelope bounds, Geometry geom) {
		if (isEnabled() && getMaxFeatures() >= size()) {
		    this.index.insert(bounds, geom);
		}
	}

	public synchronized void query(Envelope searchEnv, Visitor visitor)
	{
		this.index.query(searchEnv, visitor);
		
	}
	public synchronized List query(Envelope searchEnv)
	{
		List result = new ArrayList();
		Iterator it = index.query(searchEnv);
		while( it.hasNext() ) {
			result.add( it.next());
		}
		return result;
	}

	public void insert(Envelope itemEnv, Object item) {
		if (isEnabled() && getMaxFeatures() >= size()) {
			this.index.insert(itemEnv, item);
			fastNumTotalRegs++;
		}
	}

	public boolean remove(Envelope itemEnv, Object item) {
		boolean resul = this.index.remove(itemEnv,item);
		if (resul)
			fastNumTotalRegs--;
		return resul;
	}

	public int size() {
		return fastNumTotalRegs;
	}

	public void clearAll() {
		index.removeAll();
		fastNumTotalRegs = 0;
	}

	public void remove(org.gvsig.fmap.geom.primitive.Envelope bounds, Geometry geom) {
        index.remove(bounds, geom);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
