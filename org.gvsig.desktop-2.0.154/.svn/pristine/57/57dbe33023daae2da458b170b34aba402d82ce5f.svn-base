package org.gvsig.app.project.documents.view.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.SingleLayerIterator;
import org.gvsig.fmap.mapcontext.layers.SpatialCache;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

public class ViewSnappingPropertiesPage extends JPanel implements PropertiesPage {

    /**
     *
     */
    private static final long serialVersionUID = 3534295787643266933L;

    private static final Logger logger =
        LoggerFactory.getLogger(ViewSnappingPropertiesPage.class);


    private JLabel jLabel = null;

    private JTextField jTxtTolerance = null;

    private JLabel jLabel1 = null;

    private JSeparator jSeparator = null;

    private JScrollPane jScrollPane = null;

    private JTable jTableSnapping = null;

    private JLabel jLabelCache = null;

    private JPanel jPanelNord = null;

    private JPanel jPanelCache = null;
    private boolean changed = false;

    private FLayers layers;

    private MapContext mapContext;

    private ViewDocument view;

    private boolean isAcceppted;

    private static MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

    private class MyRecord {
        public Boolean bSelec = new Boolean(false);

        public String layerName;

        public Integer maxFeat = new Integer(1000);
    }

    private class MyTableModel extends AbstractTableModel {
        private ArrayList records = new ArrayList();

        public MyTableModel(FLayers layers) {
            addLayer(layers);
        }

        private void addLayer(FLayer lyr) {
            if (lyr instanceof FLayers) {
                FLayers lyrGroup = (FLayers) lyr;
                for (int i = 0; i < lyrGroup.getLayersCount(); i++) {
                    FLayer lyr2 = lyrGroup.getLayer(i);
                    addLayer(lyr2);
                }
            } else {
                if (lyr instanceof FLyrVect) {
                    FLyrVect aux = (FLyrVect) lyr;
                    MyRecord rec = new MyRecord();
                    rec.layerName = lyr.getName();
                    SpatialCache spatialCache = aux.getSpatialCache();
                    rec.bSelec = new Boolean(spatialCache.isEnabled());
                    rec.maxFeat = new Integer(spatialCache.getMaxFeatures());
                    records.add(rec);
                }
            }
        }

        public int getColumnCount() {
            return 3;
        }

        public int getRowCount() {
            return records.size();
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            MyRecord rec = (MyRecord) records.get(rowIndex);
            if (columnIndex == 0)
                return rec.bSelec;
            if (columnIndex == 1)
                return rec.layerName;
            if (columnIndex == 2)
                return rec.maxFeat;
            return null;

        }

        public Class getColumnClass(int c) {
            if (c == 0)
                return Boolean.class;
            if (c == 2)
                return Integer.class;
            return String.class;
        }

        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            MyRecord rec = (MyRecord) records.get(rowIndex);
            if (columnIndex == 0)
                rec.bSelec = (Boolean) aValue;
            if (columnIndex == 2) {
                if (aValue != null)
                    rec.maxFeat = (Integer) aValue;
                else
                    rec.maxFeat = new Integer(0);
            }
            changed  =true;
            super.setValueAt(aValue, rowIndex, columnIndex);
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if (columnIndex == 0)
                return true;
            if (columnIndex == 2)
                return true;

            return false;
        }

        public String getColumnName(int column) {
            if (column == 0)
                return PluginServices.getText(this, "Selected");
            if (column == 1)
                return PluginServices.getText(this, "LayerName");
            if (column == 2)
                return PluginServices.getText(this, "MaxFeaturesEditionCache");
            return "You shouldn't reach this point";

        }
    }

    public ViewSnappingPropertiesPage(ViewDocument view) {
        this.view = view;
        initComponents();

        // addLayer(layers);
        this.mapContext = view.getMapContext();
        this.layers = mapContext.getLayers();
        MyTableModel tm = new MyTableModel(layers);
        getJTableSnapping().setModel(tm);
        getJTxtTolerance().setText(String.valueOf(mapControlManager.getTolerance()));
    }

    public boolean whenAccept() {
        isAcceppted = true;
        return whenApply();
    }

    public boolean whenApply() {
        TableModel tm = getJTableSnapping().getModel();
        ArrayList layersToSnap = new ArrayList();
        for (int i = 0; i < tm.getRowCount(); i++) {
            String layerName = (String) tm.getValueAt(i, 1);
            FLyrVect lyr = (FLyrVect) layers.getLayer(layerName);
            Boolean bUseCache = (Boolean) tm.getValueAt(i, 0);
            Integer maxFeat = (Integer) tm.getValueAt(i, 2);

            // Decidimos si vamos a habilitar el spatialCache DESPUES, justo
            // antes de renderizar.
            // Necesitamos un m�todo que explore las capas en edici�n y mire las
            // capas sobre las
            // que se necestia el cache. Aqu� lo que hacemos es a�adir las
            // seleccionadas a la
            // lista de capas asociadas al snapping de los temas activos en
            // edici�n.
            // Lo del m�ximo de features en cach�, tiene que ser para cada capa
            // distinto. Pero no
            // puedes "chafar" el que ya hay, porque puedes fastidiar a otra
            // capa en edici�n.
            // Como m�ximo, lo que podemos hacer es que si es mayor al que hay,
            // lo subimos. Si
            // se solicita uno menor, lo dejamos como est�.
            // Otra opci�n ser�a no hacer caso de esto para cada capa, y ponerlo
            // de forma global.
            // lyr.setSpatialCacheEnabled(bUseCache.booleanValue());
            lyr.setMaxFeaturesInEditionCache(maxFeat.intValue());
            if (bUseCache.booleanValue())
                layersToSnap.add(lyr);
        }
        SingleLayerIterator it = new SingleLayerIterator(layers);

        while (it.hasNext()) {
            FLayer aux = it.next();
            if (aux instanceof FLyrVect)
            {
                FLyrVect lyrVect = (FLyrVect) aux;
                // Inicializamos todas
                lyrVect.getSpatialCache().setEnabled(false);
                if (aux.isActive()) {
                    if (aux.isEditing()) {
                        // Sobre la capa en edici�n siempre se puede hacer snapping
                        // Esto no es correcto, el usuario puede preferir no hacer snapping sobre la capa en edici�n
//                        lyrVect.getSpatialCache().setEnabled(true);
                        lyrVect.getMapContext().setLayersToSnap(layersToSnap);

                    }
                }
            }
        } // while
        it.rewind();
        /*
         * Iteramos por las capas en edici�n y marcamos aquellas capas que
         * necesitan trabajar con el cache habilitado
         */
        while (it.hasNext()) {
            FLayer aux = it.next();
            if (aux.isEditing())
                if (aux instanceof FLyrVect) {
                    MapContext mx=aux.getMapContext();
                        for (int i=0; i<mx.getLayersToSnap().size(); i++)
                        {
                            FLyrVect lyrVect = (FLyrVect) mx.getLayersToSnap().get(i);
                            lyrVect.getSpatialCache().setEnabled(true);
                        }

                }

        } // while

        try{
            mapControlManager.setTolerance(Integer.parseInt(getJTxtTolerance().getText()));

        }catch (Exception e) {
            logger.error(PluginServices.getText(this, "tolerancia_incorrecta"),e);

//            throw new StoreException(PluginServices.getText(this, "tolerancia_incorrecta"),e);
        }

        /*
         * I think "mapContext.invalidate()" does not work as expected.
         * Some layers are not actually redrawn.
         */
        forceRepaint(mapContext);
        return true;
    }

    /**
     * Not very elegant method to force repaint of view.
     * @param mco
     */
    private void forceRepaint(MapContext mco) {

        ViewPort vp = mco.getViewPort();
        Envelope env = vp.getAdjustedEnvelope();
        if (env != null) {
            try {
                env = (Envelope) env.clone();
            } catch (CloneNotSupportedException e) {
                logger.error("Unable to clone envelope: ", e);
            }
            Point uc = env.getUpperCorner();
            double upperx = uc.getX();
            double inc_len = 0.000001 * env.getLength(0);
            uc.setX(upperx + inc_len);
            vp.setEnvelope(env);
        }
    }

   public boolean whenCancel() {
        isAcceppted = false;
        return true;
    }

    public String getTitle() {
        return PluginServices.getText(this, "Snapping");
        }

    public int getPriority() {
        return 900;
    }

    public JComponent asJComponent() {
        return this;
    }

    private void initComponents() {
        BorderLayout layout = new BorderLayout();
        layout.setHgap(20);

                I18nManager i18nManager = ToolsLocator.getI18nManager();
        this.setLayout(layout);

        jLabelCache = new JLabel();
        jLabelCache.setText(PluginServices.getText(this, "capas_edition_cache"));
        jLabelCache.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jLabelCache.setPreferredSize(new java.awt.Dimension(500,20));
        jLabelCache.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1 = new JLabel();
        jLabel1.setText(i18nManager.getTranslation("pixels"));
        jLabel1.setBounds(new java.awt.Rectangle(195, 8, 207, 15));
        jLabel1.setPreferredSize(new java.awt.Dimension(28, 20));
        jLabel1.setName("jLabel1");
        jLabel = new JLabel();
        jLabel.setText(i18nManager.getTranslation("snap_tolerance"));
        jLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel.setName("jLabel");
        jLabel.setBounds(new java.awt.Rectangle(15, 8, 122, 15));
        jLabel.setPreferredSize(new java.awt.Dimension(28, 20));
        jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        this.setSize(new java.awt.Dimension(502,288));
        this.setPreferredSize(this.getSize());
        this.add(getJPanelNord(), BorderLayout.NORTH);

        this.add(getJPanelCache(), BorderLayout.CENTER);

    }

    /**
     * This method initializes jPanelNord
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJPanelNord() {
        if (jPanelNord == null) {
            jPanelNord = new JPanel();
            jPanelNord.setLayout(null);
            jPanelNord
                    .setComponentOrientation(java.awt.ComponentOrientation.UNKNOWN);
            jPanelNord.setPreferredSize(new java.awt.Dimension(30, 30));
            jPanelNord.add(jLabel, null);
            jPanelNord.add(getJTxtTolerance(), null);
            jPanelNord.add(jLabel1, null);

        }
        return jPanelNord;
    }

    /**
     * This method initializes jPanelCache
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJPanelCache() {
        if (jPanelCache == null) {
            jPanelCache = new JPanel();
            jPanelCache.setLayout(new BorderLayout());
            jPanelCache.setBorder(new EmptyBorder(10, 10, 10, 10));
            jPanelCache.add(jLabelCache, java.awt.BorderLayout.NORTH);
            jPanelCache.add(getJScrollPane(), java.awt.BorderLayout.CENTER);
        }
        return jPanelCache;
    }

    /**
     * This method initializes jTxtTolerance
     *
     * @return javax.swing.JTextField
     */
    private JTextField getJTxtTolerance() {
        if (jTxtTolerance == null) {
            jTxtTolerance = new JTextField();
            jTxtTolerance.setPreferredSize(new java.awt.Dimension(28, 20));
            jTxtTolerance.setName("jTxtTolerance");
            jTxtTolerance.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
            jTxtTolerance.setText("4");
            jTxtTolerance.setBounds(new java.awt.Rectangle(142, 8, 39, 15));
            jTxtTolerance.addKeyListener(new KeyListener() {
                public void keyPressed(KeyEvent e) { changed = true; }
                public void keyReleased(KeyEvent e) { changed = true; }
                public void keyTyped(KeyEvent e){ changed = true; }
            });
        }
        return jTxtTolerance;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane() {
        if (jScrollPane == null) {
            jScrollPane = new JScrollPane();
            jScrollPane.setBorder(new LineBorder(Color.GRAY));
            jScrollPane.setPreferredSize(new java.awt.Dimension(500,419));
            jScrollPane.setViewportView(getJTableSnapping());
        }
        return jScrollPane;
    }

    /**
     * This method initializes jTableSnapping
     *
     * @return javax.swing.JTable
     */
    private JTable getJTableSnapping() {
        if (jTableSnapping == null) {
            jTableSnapping = new JTable();
            jTableSnapping.addKeyListener(new KeyListener() {
                public void keyPressed(KeyEvent e) { changed = true; }
                public void keyReleased(KeyEvent e) { changed = true; }
                public void keyTyped(KeyEvent e){ changed = true; }
            });
        }
        return jTableSnapping;
    }


    /**
     * @see org.gvsig.andami.ui.mdiManager.SingletonWindow#getWindowModel()
     */
    public Object getWindowModel() {
        return view;
    }


    public boolean isAcceppted() {
        return isAcceppted;
    }

}