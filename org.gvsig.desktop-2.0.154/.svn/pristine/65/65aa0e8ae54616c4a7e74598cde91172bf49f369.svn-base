/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public class MultiPolygon3DM extends AbstractMultiPolygon {

    /**
     *
     */
    private static final long serialVersionUID = -2391997357144095125L;

    /**
     * @param subtype
     */
    public MultiPolygon3DM() {
        super(Geometry.SUBTYPES.GEOM3DM);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPolygon#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint3DM();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Polygon3DM polygon = (Polygon3DM) iterator.next();
            MultiPoint points = polygon.toPoints();
            multiPoint.ensureCapacity(multiPoint.getPrimitivesNumber()+points.getPrimitivesNumber());
            for(int i=0; i<points.getPrimitivesNumber(); i++){
                multiPoint.addPoint(points.getPointAt(i));
            }
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPolygon#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3DM();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Polygon3DM polygon = (Polygon3DM) iterator.next();
            MultiLine lines = polygon.toLines();
            multiLine.ensureCapacity(multiLine.getPrimitivesNumber()+lines.getPrimitivesNumber());
            for(int i=0; i<lines.getPrimitivesNumber(); i++){
                multiLine.addPrimitive((Line3DM)lines.getPrimitiveAt(i));
            }
        }
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPolygon#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        return this;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        MultiPolygon3DM clone = new MultiPolygon3DM();
        return clonePrimitives(clone);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return 4;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.aggregate.AbstractMultiPrimitive#fixPrimitive(org.gvsig.fmap.geom.primitive.Primitive)
     */
    @Override
    protected Geometry fixPrimitive(Primitive primitive) {
        if(primitive instanceof Polygon3DM){
            return primitive;
        }

        if(primitive.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM){
            MultiPolygon polygons;
            try {
                polygons = primitive.toPolygons();
            } catch (GeometryException e) {
                String message = "Can't convert primitive to polygons";
                logger.warn(message);
                throw new RuntimeException(message);
            }
            return polygons;
        }

        String message = "Only 3DM primitives can be fixed to MultiPolygon3DM";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().buffer(distance));
    }
}
