/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.prepareAction;

import org.gvsig.fmap.mapcontrol.MapControl;

/**
 * Object containing the context to prepare the loading process
 * of a layer.
 * 
 * @author jmvivo
 * @see PrepareContextView_1
 */
public interface PrepareContextView extends PrepareContext {

    /**
     * You can use it to interact with the MapControl component that will
     * receive the new layer, in order to get user feedback
     * (for instance a bounding box). Check the
     * {@link PrepareContextView_v1#isMapControlAvailable()} method before
     * accessing the MapControl
     * because it may not be available (for instance when adding layers
     * to a MapContext not associated with a View).
     * 
     * For the moment, this method will return a non-null MapControl for
     * compatibility reasons, but you should still check
     * {@link PrepareContextView_v1#isMapControlAvailable()} to be sure it
     * is a valid one, as it could only be a fake MapControl.
     * 
     * It is recommended to use {@link PrepareContextView_v1#getMapContext()}
     * method when no interaction is needed with the map user interface
     * (for instance to get the active projection, visible extent, etc)
     * 
     * @return Returns the mapCtrl.
     */
	MapControl getMapControl();

}
