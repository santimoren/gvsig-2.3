/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.filters.AllFilter;
import org.gvsig.installer.swing.impl.execution.panel.filters.CategoryFilter;
import org.gvsig.installer.swing.impl.execution.panel.filters.PackageFilter;
import org.gvsig.installer.swing.impl.execution.panel.filters.TypeFilter;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class PackagePropertiesFilterPanel extends JPanel implements
		ActionListener {

	private static final long serialVersionUID = 3767011079359743742L;

	public enum PropertiesFilter {
		CATEGORIES, TYPES
	}

	private PackagesTablePanel packagesTablePanel;

	private JScrollPane filterScrollPane;
	private JList jList;
//	private PropertiesFilter optionFilter = null;
	private DefaultListModel model = null;

	private DefaultSwingInstallerManager manager;

	public PackagePropertiesFilterPanel(PackagesTablePanel packagesTablePanel) {
		this.packagesTablePanel = packagesTablePanel;
		manager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {

		model = new DefaultListModel();
		jList = new JList(model);

		filterScrollPane = new JScrollPane(jList);
		MyMouseListener mouseListener = new MyMouseListener();
		// filterScrollPane.addMouseListener(mouseListener);
		jList.addMouseListener(mouseListener);

		JButton categoriesButton = new JButton(manager.getText("_categories"));
		categoriesButton.setActionCommand("categories");
		categoriesButton.addActionListener(this);

		JButton typesButton = new JButton(manager.getText("_types"));
		typesButton.setActionCommand("types");
		typesButton.addActionListener(this);

		this.setLayout(new GridBagLayout());

		java.awt.GridBagConstraints gridBagConstraints;

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		// gridBagConstraints.gridheight = 3;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		this.add(filterScrollPane, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		this.add(categoriesButton, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		this.add(typesButton, gridBagConstraints);

	}

	public void resetPanel() {
		model.removeAllElements();
		
		// optionFilter = null;
		PackageFilter filter = null;
		packagesTablePanel.setFilter(filter);
	}

	public void actionPerformed(ActionEvent e) {

		InstallPackageService service = packagesTablePanel
				.getSelectPackagesPanel().getModel().getInstallPackageService();

		if ("categories".equals(e.getActionCommand())) {
		    
		    packagesTablePanel.resetPanel();
		    loadCategories();
			
		} else {
			if ("types".equals(e.getActionCommand())) {
//				this.optionFilter = PropertiesFilter.TYPES;
				model.removeAllElements();
				packagesTablePanel.resetPanel();

				List<String> types = service.getTypes();

				model.add(0,new AllFilter());
				if (types != null) {
					for (int i = 0; i < types.size(); i++) {
						model.add(i+1, new TypeFilter(types.get(i)));
					}
				}
			}
		}
	}

	private class MyMouseListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			int i = jList.getSelectedIndex();
			if (i >= 0) {
				ListModel listModel = jList.getModel();
				PackageFilter filter = (PackageFilter) listModel.getElementAt(i);
				packagesTablePanel.setFilter(filter);
				
//				
//				// update packets list to filter
//				if (optionFilter == PropertiesFilter.TYPES) {
//					TypeFilter typeFilter = new TypeFilter(element);
//					packagesTablePanel.setFilter(typeFilter);
//				} else if (optionFilter == PropertiesFilter.CATEGORIES) {
//					CategoryFilter categoryFilter = new CategoryFilter(element);
//					packagesTablePanel.setFilter(categoryFilter);
//				}

			}
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

	}
	
	private void loadCategories() {
	       
	    InstallPackageService service = packagesTablePanel
               .getSelectPackagesPanel().getModel().getInstallPackageService();
        model.removeAllElements();

        List<String> categories = service.getCategories();

        model.add(0,new AllFilter());
        if (categories != null) {
            for (int i = 0; i < categories.size(); i++) {
                model.add(i+1, new CategoryFilter(categories.get(i)));
            }
        }
        jList.setSelectedIndex(0);
	    
	}
	
	public void setInitialFilter() {
	    loadCategories();
	}

}
