/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import org.apache.commons.lang3.StringUtils;

import org.cresques.cts.IProjection;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindowTransform;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.app.project.documents.view.MapOverview;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.toc.actions.ShowLayerErrorsTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.gui.TOC;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.swing.api.Component;
import org.gvsig.utils.exceptionHandling.ExceptionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author 2005- Vicente Caballero
 * @author 2009- Joaquin del Cerro
 * 
 */

public abstract class AbstractViewPanel extends JPanel implements IView,
    IWindowTransform, PropertyChangeListener, Component {

	private static final Logger logger = LoggerFactory.getLogger(AbstractViewPanel.class);
	
    private static final long serialVersionUID = -259620280790490262L;

    private static final int DEFAULT_HEIGHT = 450;

    private static final int DEFAULT_WIDTH = 700;

    protected MapControl m_MapControl;
    protected MapOverview m_MapLoc;

    // store the properties of the window
    protected WindowInfo m_viewInfo = null;
    protected WindowLayout windowLayout = null;

    protected TOC m_TOC;
    protected ViewDocument modelo;
    protected ViewExceptionListener mapControlExceptionListener =
        new ViewExceptionListener();

    protected boolean isPalette = false;
    protected MapOverViewPalette movp;
    protected ViewSplitPane tempMainSplit = null;
    protected JSplitPane tempSplitToc = null;

    
    // This class is to temporarily solve a problem with jcrs 
    private class ViewProjectionContainer implements org.gvsig.andami.ProjectionContainerManager.ProjectionContainer {
		public Object getCurrentProjection() {
			return getProjection();
		}
		public void set() {
	    	org.gvsig.andami.ProjectionContainerManager.set(this);
		}
		public void unset() {
			org.gvsig.andami.ProjectionContainerManager.unset(this);
		}
    }
    private ViewProjectionContainer viewProjectionContainer = new ViewProjectionContainer();
    
	public void windowActivated() {
        // This code is to temporarily solve a problem with jcrs 
    	viewProjectionContainer.set();
    }

    public JComponent asJComponent() {
        return this;
    }

    public void toPalette() {
        // By default do nothing
    }

    public void restore() {
        // By default do nothing
    }

    protected class ViewSplitPane extends JSplitPane {

        private static final long serialVersionUID = -7506953938664812652L;
        private int lastDivider = 0;

        public ViewSplitPane(int horizontal_split) {
            super(horizontal_split);
        }

        protected void paintChildren(Graphics g) {
            if (lastDivider != lastDividerLocation) {
            	logger.debug("paintChildren = "+ this.lastDividerLocation);
                lastDivider = lastDividerLocation;
            }
            super.paintChildren(g);
        }

    }

    /**
     * Creates a new View object. Before using it, it must be initialized
     * using the <code>initialize()</code> method.
     * 
     * @see initialize()
     */
    public AbstractViewPanel() {
    	ProjectManager.getInstance().getCurrentProject().addPropertyChangeListener(this);
    }

    /**
     * Create the internal componentes and populate the window with them.
     * If the layout properties were set using the
     * <code>setWindowData(WindowData)</code> method, the window will be
     * populated according to this
     * properties.
     */
    protected void initialize() {
        // Do nothing
    }

    /**
     * This method is used to get <strong>an initial</strong> ViewInfo object
     * for this View. It is not intended to retrieve the ViewInfo object in a
     * later time. <strong>Use PluginServices.getMDIManager().getViewInfo(view)
     * to retrieve the ViewInfo object at any time after the creation of the
     * object.
     * 
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#getWindowInfo()
     */
    public WindowInfo getWindowInfo() {
        if (m_viewInfo == null) {
            m_viewInfo =
                new WindowInfo(WindowInfo.ICONIFIABLE | WindowInfo.RESIZABLE
                    | WindowInfo.MAXIMIZABLE);

            m_viewInfo.setWidth(DEFAULT_WIDTH);
            m_viewInfo.setHeight(DEFAULT_HEIGHT);
            m_viewInfo.setTitle(PluginServices.getText(this, "Vista") + ": "
                + modelo.getName());
        }
        return m_viewInfo;
    }

    /**
     * @see org.gvsig.andami.ui.mdiManager.IWindowListener#windowClosed()
     */
    public void windowClosed() {
    	viewProjectionContainer.unset();
        if (movp != null) {
            PluginServices.getMDIManager().closeWindow(movp);
        }
        if (modelo != null) {
            modelo.setWindowLayout(this.getWindowLayout());
        }
    }

    /**
     * @deprecated See {@link #getViewDocument()}
     */
    public ViewDocument getModel() {
        return modelo;
    }

    public ViewDocument getViewDocument() {
        return modelo;
    }

    public MapOverview getMapOverview() {
        return m_MapLoc;
    }

    public MapControl getMapControl() {
        return m_MapControl;
    }

    public TOC getTOC() {
        return m_TOC;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.mdiApp.ui.MDIManager.SingletonView#getModel()
     */

    public Object getWindowModel() {
        return modelo;
    }

    /**
     * This method is used to get <strong>an initial</strong> ViewInfo object
     * for this View. It is not intended to retrieve the ViewInfo object in a
     * later time. <strong>Use PluginServices.getMDIManager().getViewInfo(view)
     * to retrieve the ViewInfo object at any time after the creation of the
     * object.
     * 
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#getWindowInfo()
     */

    public boolean isPalette() {
        return isPalette;
    }

    public void repaintMap() {
        m_MapControl.drawMap(false);
    }

    public class ViewExceptionListener implements ExceptionListener {

        /**
         * @see com.iver.cit.gvsig.fmap.ExceptionListener#exceptionThrown(java.lang.Throwable)
         */
        @Override
        public void exceptionThrown(Throwable t) {
            
            NotificationManager.addError(getMessages(t), t);
        }
        
        private class ExceptionIterator implements Iterator {

            Throwable exception;

            ExceptionIterator(Throwable exception) {
                this.exception = exception;
            }

            @Override
            public boolean hasNext() {
                return this.exception != null;
            }

            @Override
            public Object next() {
                Throwable exception = this.exception;
                this.exception = exception.getCause();
                return exception;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
        
        private String getMessages(Throwable ex) {
            StringBuilder msg = new StringBuilder();
            boolean firstline = true;
            Iterator exceptions = new ExceptionIterator(ex);
            String lastmsg = "";

            while (exceptions.hasNext()) {
                Throwable ex1 = ((Throwable) exceptions.next());
                String message;
                message = ex1.getMessage();
                if ( !StringUtils.isEmpty(message) ) {
                    if( firstline ) {
                        msg.append(message);
                        msg.append("\n\n");
                        firstline = false;
                    } else {
                        if (!message.equalsIgnoreCase(lastmsg)) {
                            msg.append("- ");
                            msg.append(message.replace("\n","\n  "));
                            msg.append("\n");
                        }
                    }
                    lastmsg = message;
                }
            }
            return msg.toString();
        }

    }

    /**
     * @return
     */
    public BufferedImage getImage() {
        return m_MapControl.getImage();
    }

    public void setProjection(IProjection proj) {
        getMapControl().setProjection(proj);
    }

    public IProjection getProjection() {
        return getMapControl().getProjection();
    }

    public WindowLayout getWindowLayout() {
        if (windowLayout == null) {
            windowLayout = new WindowLayout();
        }
        windowLayout.set("MainWindow.X", Integer.toString(this.getX()));
        windowLayout.set("MainWindow.Y", Integer.toString(this.getY()));
        windowLayout.set("MainWindow.Width", Integer.toString(this.getWidth()));
        windowLayout.set("MainWindow.Height",
            Integer.toString(this.getHeight()));

        windowLayout.set("MainDivider.Location",
            Integer.toString(tempMainSplit.getDividerLocation()));
        windowLayout.set("MainDivider.X",
            Integer.toString(tempMainSplit.getX()));
        windowLayout.set("MainDivider.Y",
            Integer.toString(tempMainSplit.getY()));
        windowLayout.set("MainDivider.Width",
            Integer.toString(tempMainSplit.getWidth()));
        windowLayout.set("MainDivider.Height",
            Integer.toString(tempMainSplit.getHeight()));

        if (isPalette()) {
            windowLayout.set("GraphicLocator.isPalette", "true");
        } else {
            windowLayout.set("GraphicLocator.isPalette", "false");
            if (tempSplitToc != null) {
                windowLayout.set("TOCDivider.Location",
                    Integer.toString(tempSplitToc.getDividerLocation()));
                windowLayout.set("TOCDivider.X",
                    Integer.toString(tempSplitToc.getX()));
                windowLayout.set("TOCDivider.Y",
                    Integer.toString(tempSplitToc.getY()));
                windowLayout.set("TOCDivider.Width",
                    Integer.toString(tempSplitToc.getWidth()));
                windowLayout.set("TOCDivider.Height",
                    Integer.toString(tempSplitToc.getHeight()));
            }
        }
        if (m_TOC != null) {
            windowLayout.set("TOC.Width", Integer.toString(m_TOC.getWidth()));
            windowLayout.set("TOC.Height", Integer.toString(m_TOC.getHeight()));
        }
        if (m_MapControl != null) {
            windowLayout.set("MapControl.Width",
                Integer.toString(m_MapControl.getWidth()));
            windowLayout.set("MapControl.Height",
                Integer.toString(m_MapControl.getHeight()));
        }
        if (m_MapLoc != null) {
            windowLayout.set("GraphicLocator.Width",
                Integer.toString(m_MapLoc.getWidth()));
            windowLayout.set("GraphicLocator.Height",
                Integer.toString(m_MapLoc.getHeight()));
        }

        return windowLayout;
    }

    public void setWindowLayout(WindowLayout data) {
        windowLayout = data;
    }
    
	public void propertyChange(PropertyChangeEvent evt) {
		// Detect when the related document has been removed
		// and close or dispose whatever is needed
		if ("delDocument".equals(evt.getPropertyName())) {
			if (evt.getOldValue() != null
					&& evt.getOldValue() instanceof ViewDocument) {
				ViewDocument viewDocument = (ViewDocument) evt.getOldValue();
				ViewDocument myViewDocument = getViewDocument();
				if (myViewDocument != null
						&& myViewDocument.equals(viewDocument)) {
					m_MapControl.dispose();
					m_MapLoc.dispose();
				}
			}
		}
	}
}
