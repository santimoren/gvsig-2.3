/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: IFLayerStatus.java 20989 2008-05-28 11:05:57Z jmvivo $
* $Log$
* Revision 1.1  2006-09-21 17:23:39  azabala
* First version in cvs
*
*
*/
package org.gvsig.fmap.mapcontext.layers;

import org.gvsig.tools.persistence.Persistent;

/**
 * <p>According to the particular constitution of a layer, it can be in a set of states,
 *  some of them simultaneously, that altogether represent its <i>status</i>.</p>
 *   
 * <p><code>IFLayerStatus</code> is a "marked" interface (we don't define any methods), that represents
 *  the common status of all kind of layers.</p>
 * 
 * @author azabala
 */
public interface IFLayerStatus extends Persistent {
}

