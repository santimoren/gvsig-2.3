package org.gvsig.app.project.documents.view.legend.gui;

import com.jeta.forms.components.border.TitledBorderLabel;
import com.jeta.open.i18n.I18NUtils;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GeneralLayerPropertiesPageView extends JPanel
{
   JLabel lblName = new JLabel();
   TitledBorderLabel lblRangeOfScales = new TitledBorderLabel();
   JRadioButton rdoShowAlways = new JRadioButton();
   ButtonGroup buttongroup1 = new ButtonGroup();
   JRadioButton rdoShowConditionedToTheScale = new JRadioButton();
   TitledBorderLabel lblProperties = new TitledBorderLabel();
   JTextArea txtProperties = new JTextArea();
   JLabel lblIsLessThan = new JLabel();
   JLabel lblIsGreaterThan = new JLabel();
   JLabel lblFullScale = new JLabel();
   JLabel lblMinumunScale = new JLabel();
   JTextField txtIsLessThan = new JTextField();
   JTextField txtIsGreaterThan = new JTextField();
   JTextField txtName = new JTextField();
   TitledBorderLabel lblDataSource = new TitledBorderLabel();
   JLabel lblSourceType = new JLabel();
   JLabel lblSource = new JLabel();
   JTextField txtSourceType = new JTextField();
   JTextField txtSource = new JTextField();

   /**
    * Default constructor
    */
   public GeneralLayerPropertiesPageView()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblName.setName("lblName");
      lblName.setText("Name:");
      jpanel1.add(lblName,cc.xywh(2,2,2,1));

      lblRangeOfScales.setName("lblRangeOfScales");
      lblRangeOfScales.setText("Range of scales");
      jpanel1.add(lblRangeOfScales,cc.xywh(2,5,2,1));

      rdoShowAlways.setActionCommand("Show always");
      rdoShowAlways.setName("rdoShowAlways");
      rdoShowAlways.setText("Show always");
      buttongroup1.add(rdoShowAlways);
      jpanel1.add(rdoShowAlways,cc.xywh(2,6,2,1));

      rdoShowConditionedToTheScale.setActionCommand("Do not show the layer when the scale:");
      rdoShowConditionedToTheScale.setName("rdoShowConditionedToTheScale");
      rdoShowConditionedToTheScale.setText("Do not show the layer when the scale:");
      buttongroup1.add(rdoShowConditionedToTheScale);
      jpanel1.add(rdoShowConditionedToTheScale,cc.xywh(2,8,2,1));

      lblProperties.setName("lblProperties");
      lblProperties.setText("Properties");
      jpanel1.add(lblProperties,cc.xywh(2,14,2,1));

      txtProperties.setEditable(false);
      txtProperties.setName("txtProperties");
      JScrollPane jscrollpane1 = new JScrollPane();
      jscrollpane1.setViewportView(txtProperties);
      jscrollpane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane1,cc.xywh(2,15,2,1));

      jpanel1.add(createPanel1(),cc.xywh(2,9,2,1));
      txtName.setName("txtName");
      jpanel1.add(txtName,cc.xywh(2,3,2,1));

      lblDataSource.setName("lblDataSource");
      lblDataSource.setText("Data source");
      jpanel1.add(lblDataSource,cc.xywh(2,11,2,1));

      jpanel1.add(createPanel2(),cc.xywh(2,12,2,1));
      addFillComponents(jpanel1,new int[]{ 1,2,3,4 },new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 });
      return jpanel1;
   }

   public JPanel createPanel1()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblIsLessThan.setName("lblIsLessThan");
      lblIsLessThan.setText("Is less than");
      jpanel1.add(lblIsLessThan,cc.xy(2,1));

      lblIsGreaterThan.setName("lblIsGreaterThan");
      lblIsGreaterThan.setText("Is greater than");
      jpanel1.add(lblIsGreaterThan,cc.xy(2,3));

      lblFullScale.setName("lblFullScale");
      lblFullScale.setText("(full scale)");
      jpanel1.add(lblFullScale,cc.xy(7,1));

      lblMinumunScale.setName("lblMinumunScale");
      lblMinumunScale.setText("(minimum scale)");
      jpanel1.add(lblMinumunScale,cc.xy(7,3));

      txtIsLessThan.setName("txtIsLessThan");
      jpanel1.add(txtIsLessThan,cc.xy(5,1));

      txtIsGreaterThan.setName("txtIsGreaterThan");
      jpanel1.add(txtIsGreaterThan,cc.xy(5,3));

      JLabel jlabel1 = new JLabel();
      jlabel1.setText("1:");
      jpanel1.add(jlabel1,cc.xy(4,1));

      JLabel jlabel2 = new JLabel();
      jlabel2.setText("1:");
      jpanel1.add(jlabel2,cc.xy(4,3));

      addFillComponents(jpanel1,new int[]{ 1,3,6 },new int[]{ 1,2,3 });
      return jpanel1;
   }

   public JPanel createPanel2()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0)","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblSourceType.setName("lblSourceType");
      lblSourceType.setText("Source type");
      jpanel1.add(lblSourceType,cc.xy(1,1));

      lblSource.setName("lblSource");
      lblSource.setText("Source");
      jpanel1.add(lblSource,cc.xy(1,3));

      txtSourceType.setBackground(new Color(236,233,216));
      txtSourceType.setEditable(false);
      txtSourceType.setName("txtSourceType");
      jpanel1.add(txtSourceType,cc.xy(3,1));

      txtSource.setBackground(new Color(236,233,216));
      txtSource.setEditable(false);
      txtSource.setName("txtSource");
      jpanel1.add(txtSource,cc.xy(3,3));

      addFillComponents(jpanel1,new int[]{ 2 },new int[]{ 2 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
