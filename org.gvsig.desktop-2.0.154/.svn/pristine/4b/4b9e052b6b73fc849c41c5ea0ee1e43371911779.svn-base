/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.exception;

import org.gvsig.fmap.dal.feature.FeatureIndex;

/**
 * Exception thrown when a {@link FeatureIndex} is used but its state is not
 * valid.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class InvalidFeatureIndexException extends FeatureIndexException {
    
    private static final long serialVersionUID = 3857591701014208141L;
    private static final String MESSAGE_KEY = "_InvalidFeatureIndexException";
    private static final String FORMAT_STRING = 
        "The feature index is not valid in its current state and can't be used";
    
    /**
     * Creates a new {@link InvalidFeatureIndexException}.
     */
    public InvalidFeatureIndexException() {
        super(FORMAT_STRING, MESSAGE_KEY, serialVersionUID);
    }

}
