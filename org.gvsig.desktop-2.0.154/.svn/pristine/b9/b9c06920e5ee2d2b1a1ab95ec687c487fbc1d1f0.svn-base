/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.tools.exception.BaseException;

/**
 * Extensi�n de zoom a lo seleccionado teniendo como ventana activa una tabla.
 * 
 * @author Vicente Caballero Navarro
 */
public class ZoomToSelectExtension extends Extension {

    public void execute(String s) {
    	if( "view-navigation-zoom-to-selection-table".equalsIgnoreCase(s) ) {
	        org.gvsig.andami.ui.mdiManager.IWindow f =
	            PluginServices.getMDIManager().getActiveWindow();
	        MapContext mapa = null;
	        Envelope selectedExtent = null;
	        if (f instanceof FeatureTableDocumentPanel) {
	            FeatureTableDocumentPanel table = (FeatureTableDocumentPanel) f;
	            mapa = (table.getModel().getAssociatedLayer()).getMapContext();
	        }
	
	        try {
	            selectedExtent = mapa.getSelectionBounds();
	            mapa.getViewPort().setEnvelope(selectedExtent);
	        } catch (BaseException e) {
	            NotificationManager.addError(e);
	        }
    	}
    }

    public boolean isVisible() {
        org.gvsig.andami.ui.mdiManager.IWindow window =
            PluginServices.getMDIManager().getActiveWindow();
        if (window instanceof FeatureTableDocumentPanel) {
            FeatureTableDocumentPanel featureTableDocumentPanel =
                (FeatureTableDocumentPanel) window;
            IWindow[] windows = PluginServices.getMDIManager().getAllWindows();
            for (int i = 0; i < windows.length; i++) {
                if (windows[i] instanceof DefaultViewPanel) {
                    if (featureTableDocumentPanel.getModel()
                        .getAssociatedLayer() != null) {
                        if (((DefaultViewPanel) windows[i])
                            .getMapControl()
                            .getMapContext()
                            .equals(
                                (featureTableDocumentPanel.getModel()
                                    .getAssociatedLayer()).getMapContext())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isEnabled() {
        org.gvsig.andami.ui.mdiManager.IWindow f =
            PluginServices.getMDIManager().getActiveWindow();
        if (f == null) {
            return false;
        }
        if (f instanceof FeatureTableDocumentPanel) {
            FeatureTableDocumentPanel t = (FeatureTableDocumentPanel) f;
            IWindow[] windows = PluginServices.getMDIManager().getAllWindows();
            for (int i = 0; i < windows.length; i++) {
                if (windows[i] instanceof DefaultViewPanel) {
                    if (((DefaultViewPanel) windows[i])
                        .getMapControl()
                        .getMapContext()
                        .equals(
                            (t.getModel().getAssociatedLayer()).getMapContext())) {
                        try {
                            if (!t.getModel().getStore().getFeatureSelection()
                                .isEmpty()) {
                                return true;
                            }
                        } catch (DataException e) {
                            NotificationManager.addError(e);
                            return false;
                        }
                    }
                }
            }

        }
        return false;
    }

    public void initialize() {
        registerIcons();
    }

    private void registerIcons() {
    	IconThemeHelper.registerIcon("action", "view-navigation-zoom-to-selection", this);
    }
}
