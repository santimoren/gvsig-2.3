package org.gvsig.labeling.lang;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.evaluator.EvaluatorWithDescriptions;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentContext;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.persistence.spi.PersistentContextServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LabelClassUtils {

    private static Logger logger =
            LoggerFactory.getLogger(LabelClassUtils.class);

	private static PersistenceManager persman =
			ToolsLocator.getPersistenceManager();

	public static boolean isUseSqlQuery(ILabelClass lbl) {
		String str = lbl.getSQLQuery();
		return str != null && str.trim().length() > 0;
	}
	
	
	public static Object clone(Persistent persi) {
		
		PersistentContext context = null;
		Object resp = null;
		PersistentState state = null;
        try {
			state = persman.getState(persi, true);
		} catch (PersistenceException e) {
			logger.error("While cloning ILabelClass", e);
			return persi;
		}
        context = state.getContext();
        if (context instanceof PersistentContextServices) {
            /*
             * Ensure that previous instances are not used,
             * so objects are recreated
             */
            ((PersistentContextServices) context).clear();
        }
        try {
			resp = persman.create(state);
		} catch (Exception e) {
			logger.error("While cloning ILabelClass", e);
			return persi;
		}
        return resp;
	}
	
	public static Object evaluate(String expr, EvaluatorData data) {
		
		Evaluator ev = EvaluatorCreator.getEvaluator(expr);
        Object resp_obj = null;
        try {
            resp_obj = ev.evaluate(data);
        } catch (EvaluatorException e) {
            /*
             * Parse exception or field not found or
             * divison by zero, etc. Malformed expression, not valid
             */
            return null;
        } catch (Exception e) {
            /*
             * All other exceptions. Should not happen often.
             * We assume a strange exception (NPE, etc) and decide it's not a valid expression
             */
            return null;
        }	
        return resp_obj;
	}
	
	
	public static boolean validExpression(
			String str, FeatureStore sto, boolean must_be_boolean) {
		
    	Feature dummyfeat = null;;
		try {
			dummyfeat = sto.createNewFeature(true);
		} catch (DataException e1) {
			logger.error("While getting dummy feature in labeling expression.", e1);
			return false;
		}
		
		
        EvaluatorWithDescriptions evde = EvaluatorCreator.getEvaluator(str);
        Object resp_obj = null;
        try {
            /*
             * Try evaluate feature with default values
             */
            resp_obj = evde.evaluate(dummyfeat.getEvaluatorData());
        } catch (EvaluatorException e) {
            /*
             * Parse exception or field not found or
             * divison by zero, etc.
             * Malformed expression, not valid
             */
            return false;
        } catch (Exception e) {
            /*
             * All other exceptions.
             * Should not happen often.
             * We assume a strange
             * exception (NPE, etc) and decide it's not a valid
             * expression
             *  
             */
            return false;
        }

        if (must_be_boolean) {
        	return resp_obj.getClass() == Boolean.class;
        } else {
        	return true;
        }
		
	}
	

}
