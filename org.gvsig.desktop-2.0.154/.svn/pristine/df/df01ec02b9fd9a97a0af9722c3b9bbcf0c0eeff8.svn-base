/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JOptionPane;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.filter.ExpressionListener;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.gui.AndamiWizard;
import org.gvsig.app.project.documents.gui.ObjectSelectionStep;
import org.gvsig.app.project.documents.table.FieldSelectionModel;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.TableSelectionModel;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.utils.swing.objectSelection.SelectionException;
import org.gvsig.utils.swing.wizard.WizardControl;
import org.gvsig.utils.swing.wizard.WizardEvent;
import org.gvsig.utils.swing.wizard.WizardListener;

/**
 * Extensi�n que controla las operaciones realizadas sobre las tablas.
 * 
 * @author Fernando Gonz�lez Cort�s
 */
public class TableOperations extends Extension implements ExpressionListener {

    private FeatureStore featureStore = null;

    public void execute(String actionCommand) {
        
        I18nManager i18n = ToolsLocator.getI18nManager();
        final Project project =
            ProjectManager.getInstance().getCurrentProject();
        List<Document> tableDcouments =
            project.getDocuments(TableManager.TYPENAME);
        TableDocument[] pts =
            tableDcouments
                .toArray(new TableDocument[tableDcouments.size()]);

        if ("table-create-link".equals(actionCommand)) {
            try {
                final ObjectSelectionStep sourceTable =
                    new ObjectSelectionStep();
                sourceTable.setModel(new TableSelectionModel(pts,
                    PluginServices.getText(this, "seleccione_tabla_origen")));

                final ObjectSelectionStep targetTable =
                    new ObjectSelectionStep();
                targetTable
                    .setModel(new TableSelectionModel(pts, PluginServices
                        .getText(this, "seleccione_tabla_a_enlazar")));

                final ObjectSelectionStep firstTableField =
                    new ObjectSelectionStep();
                final ObjectSelectionStep secondTableField =
                    new ObjectSelectionStep();
                final AndamiWizard wiz =
                    new AndamiWizard(PluginServices.getText(this, "back"),
                        PluginServices.getText(this, "next"),
                        PluginServices.getText(this, "finish"),
                        PluginServices.getText(this, "cancel"));
                wiz.getWindowInfo().setTitle(i18n.getTranslation("_Create_link_between_tables"));
                wiz.setSize(new Dimension(450, 200));
                wiz.addStep(sourceTable);
                wiz.addStep(firstTableField);
                wiz.addStep(targetTable);
                wiz.addStep(secondTableField);

                wiz.addWizardListener(new WizardListener() {

                    public void cancel(WizardEvent w) {
                        PluginServices.getMDIManager().closeWindow(wiz);
                    }

                    public void finished(WizardEvent w) {
                        PluginServices.getMDIManager().closeWindow(wiz);

                        TableDocument sourceProjectTable =
                            (TableDocument) sourceTable.getSelected();

                        TableDocument targetProjectTable =
                            (TableDocument) targetTable.getSelected();
                        FeatureStore sds2 = targetProjectTable.getStore();

                        String field1 = (String) firstTableField.getSelected();
                        String field2 = (String) secondTableField.getSelected();
                        sourceProjectTable.addLinkTable(
                            targetProjectTable.getName(), field1,
                            field2);

                    }

                    public void next(WizardEvent w) {
                        WizardControl wiz = w.wizard;
                        wiz.enableBack(true);
                        wiz.enableNext(((ObjectSelectionStep) wiz
                            .getCurrentStep()).getSelectedItem() != null);

                        if (w.currentStep == 1) {
                            TableDocument pt =
                                (TableDocument) sourceTable.getSelected();

                            try {
                                firstTableField
                                    .setModel(new FieldSelectionModel(pt
                                        .getStore(), PluginServices.getText(
                                        this, "seleccione_campo_enlace")));
                            } catch (SelectionException e) {
                                NotificationManager.addError(
                                    "Error obteniendo los campos de la tabla",
                                    e);
                            }
                        } else
                            if (w.currentStep == 3) {
                                try {
                                    // tabla
                                    TableDocument pt =
                                        (TableDocument) sourceTable
                                            .getSelected();

                                    // �ndice del campo
                                    FeatureStore fs = pt.getStore();
                                    String fieldName =
                                        (String) firstTableField.getSelected();
                                    int type =
                                        ((FeatureAttributeDescriptor) fs
                                            .getDefaultFeatureType().get(
                                                fieldName)).getType();

                                    secondTableField
                                        .setModel(new FieldSelectionModel(
                                            ((TableDocument) targetTable
                                                .getSelected()).getStore(),
                                            PluginServices.getText(this,
                                                "seleccione_campo_enlace"),
                                            type));
                                } catch (SelectionException e) {
                                    NotificationManager
                                        .addError(
                                            "Error obteniendo los campos de la tabla",
                                            e);
                                } catch (DataException e) {
                                    NotificationManager
                                        .addError(
                                            "Error obteniendo los campos de la tabla",
                                            e);
                                }
                            }
                    }

                    public void back(WizardEvent w) {
                        WizardControl wiz = w.wizard;
                        wiz.enableBack(true);
                        wiz.enableNext(((ObjectSelectionStep) wiz
                            .getCurrentStep()).getSelectedItem() != null);
                    }
                });
                project.setModified(true);
                PluginServices.getMDIManager().addWindow(wiz);
            } catch (SelectionException e) {
                NotificationManager.addError("Error abriendo el asistente", e);
            }
        }
    }

    /**
     * @see org.gvsig.app.gui.filter.ExpressionListener#newSet(java.lang.String)
     */
    public void newSet(String expression) throws DataException {
        // By Pablo: if no filter expression -> no element selected
        if (!this.filterExpressionFromWhereIsEmpty(expression)) {
            FeatureSet set = null;
            try {
                set = doSet(expression);

                if (set == null) {
                    throw new RuntimeException("Not a 'where' clause?");
                }
                FeatureSelection newSel = featureStore.createFeatureSelection();
                newSel.select(set);
                featureStore.setSelection(newSel);
            } catch (Exception e) {
                
                JOptionPane.showMessageDialog(
                    ApplicationLocator.getManager().getRootComponent(),
                    Messages.getText("_Invalid_expression") + ":\n"
                        + SelectByAttributesExtension.getLastMessage(e),
                    Messages.getText("_Invalid_expression"),
                    JOptionPane.ERROR_MESSAGE);
                
                
            } finally {
                if (set != null) {
                    set.dispose();
                }
            }
        } else {
            // By Pablo: if no expression -> no element selected
            featureStore.getFeatureSelection().deselectAll();
        }
    }

    /**
     * @see org.gvsig.app.gui.filter.ExpressionListener#newSet(java.lang.String)
     */
    private FeatureSet doSet(String expression) throws DataException {
        FeatureQuery query = featureStore.createFeatureQuery();
        query
            .setFilter(DALLocator.getDataManager().createExpresion(expression));
        return featureStore.getFeatureSet(query);
    }

    /**
     * @see org.gvsig.app.gui.filter.ExpressionListener#addToSet(java.lang.String)
     */
    public void addToSet(String expression) throws DataException {
        // By Pablo: if no filter expression -> don't add more elements to set
        if (!this.filterExpressionFromWhereIsEmpty(expression)) {
            FeatureSet set = null;
            try {
                set = doSet(expression);

                featureStore.getFeatureSelection().select(set);
            } finally {
                if (set != null) {
                    set.dispose();
                }
            }
        }
    }

    /**
     * @see org.gvsig.app.gui.filter.ExpressionListener#fromSet(java.lang.String)
     */
    public void fromSet(String expression) throws DataException {
        // By Pablo: if no filter expression -> no element selected
        if (!this.filterExpressionFromWhereIsEmpty(expression)) {

            FeatureSet set = null;
            DisposableIterator iterator = null;
            try {
                set = doSet(expression);

                if (set == null) {
                    throw new RuntimeException("Not a 'where' clause?");
                }
                FeatureSelection oldSelection =
                    featureStore.getFeatureSelection();

                FeatureSelection newSelection =
                    featureStore.createFeatureSelection();
                iterator = set.fastIterator();
                while (iterator.hasNext()) {
                    Feature feature = (Feature) iterator.next();
                    if (oldSelection.isSelected(feature)) {
                        newSelection.select(feature);
                    }
                }
                featureStore.setSelection(newSelection);
            } finally {
                DisposeUtils.dispose(iterator);
                DisposeUtils.dispose(set);
            }
        } else {
            // By Pablo: if no expression -> no element selected
            // featureStore.setSelection(featureStore.createSelection());
            featureStore.getFeatureSelection().deselectAll();
        }
    }

    /**
     * Returns true if the WHERE subconsultation of the filterExpression is
     * empty ("")
     * 
     * @author Pablo Piqueras Bartolom� (p_queras@hotmail.com)
     * @param expression
     *            An string
     * @return A boolean value
     */
    private boolean filterExpressionFromWhereIsEmpty(String expression) {
        String subExpression = expression.trim();
        int pos;

        // Remove last ';' if exists
        if (subExpression.charAt(subExpression.length() - 1) == ';') {
            subExpression =
                subExpression.substring(0, subExpression.length() - 1).trim();
        }

        // If there is no 'where' clause
        if ((pos = subExpression.indexOf("where")) == -1) {
            return false;
        }

        // If there is no subexpression in the WHERE clause -> true
        subExpression =
            subExpression.substring(pos + 5, subExpression.length()).trim(); // +
                                                                             // 5
                                                                             // is
                                                                             // the
                                                                             // length
                                                                             // of
                                                                             // 'where'
        if (subExpression.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @see com.iver.mdiApp.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        }

        if (v instanceof FeatureTableDocumentPanel) {
            return true;
        } /*
           * else {
           * if (v instanceof com.iver.cit.gvsig.gui.View) {
           * com.iver.cit.gvsig.gui.View view = (com.iver.cit.gvsig.gui.View) v;
           * ProjectView pv = view.getModel();
           * FLayer[] seleccionadas = pv.getMapContext().getLayers()
           * .getActives();
           * 
           * if (seleccionadas.length == 1) {
           * if (seleccionadas[0] instanceof AlphanumericData) {
           * return true;
           * }
           * }
           * }
           */
        return false;
        // }
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    	IconThemeHelper.registerIcon("action", "table-create-link", this);

    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /**
     * Ensure that field name only has 'safe' characters
     * (no spaces, special characters, etc).
     */
    public String sanitizeFieldName(String fieldName) {
        return fieldName.replaceAll("\\W", "_"); // replace any non-word
                                                 // character by an underscore
    }

}
