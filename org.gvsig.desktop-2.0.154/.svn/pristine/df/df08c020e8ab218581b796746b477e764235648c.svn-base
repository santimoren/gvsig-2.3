/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.jts;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.operation.valid.TopologyValidationError;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultValidationStatus implements Geometry.ValidationStatus {

    private static final Logger logger = LoggerFactory.getLogger(DefaultValidationStatus.class);

    private int statusCode = VALID;
    private Point problemLocation = null;
    private String message = null;

    public DefaultValidationStatus() {

    }

    public DefaultValidationStatus(int code, String message) {
        this.statusCode = code;
        this.message = message;
    }

    public void setMesage(String message) {
        this.message = message;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setValidationError(TopologyValidationError validationError) {
        if( validationError == null ) {
            statusCode = VALID;
            message = null;
            problemLocation = null;
            return;
        }
        this.message = validationError.getMessage();
        Coordinate coord = validationError.getCoordinate();
        try {
            GeometryManager geomManager = GeometryLocator.getGeometryManager();
            if ( Double.isNaN(coord.z) ) {
                this.problemLocation = geomManager.createPoint(coord.x, coord.y, Geometry.SUBTYPES.GEOM2D);
            } else {
                this.problemLocation = geomManager.createPoint(coord.x, coord.y, Geometry.SUBTYPES.GEOM3D);
                this.problemLocation.setCoordinateAt(2, coord.z);
            }
        } catch (CreateGeometryException ex) {
            String coordstr;
            try {
                coordstr = coord.toString();
            } catch (Exception ex1) {
                coordstr = "unknow";
            }
            logger.warn("Can't create a point from jts Coordinate (" + coordstr + ").", ex);
            this.problemLocation = null;
        }
        switch (validationError.getErrorType()) {
        case TopologyValidationError.DISCONNECTED_INTERIOR:
            this.statusCode = DISCONNECTED_INTERIOR;
            break;
        case TopologyValidationError.DUPLICATE_RINGS:
            this.statusCode = DUPLICATE_RINGS;
            break;
        case TopologyValidationError.HOLE_OUTSIDE_SHELL:
            this.statusCode = HOLE_OUTSIDE_SHELL;
            break;
        case TopologyValidationError.INVALID_COORDINATE:
            this.statusCode = INVALID_COORDINATE;
            break;
        case TopologyValidationError.NESTED_HOLES:
            this.statusCode = NESTED_HOLES;
            break;
        case TopologyValidationError.NESTED_SHELLS:
            this.statusCode = NESTED_SHELLS;
            break;
        case TopologyValidationError.RING_NOT_CLOSED:
            this.statusCode = RING_NOT_CLOSED;
            break;
        case TopologyValidationError.RING_SELF_INTERSECTION:
            this.statusCode = RING_SELF_INTERSECTION;
            break;
        case TopologyValidationError.SELF_INTERSECTION:
            this.statusCode = SELF_INTERSECTION;
            break;
        case TopologyValidationError.TOO_FEW_POINTS:
            this.statusCode = TOO_FEW_POINTS;
            break;
        default:
            this.statusCode = UNKNOW;
            break;
        }
    }

    public boolean isValid() {
        return statusCode == VALID;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public Point getProblemLocation() {
        return this.problemLocation;
    }

    public String getMessage() {
        return this.message;
    }

}
