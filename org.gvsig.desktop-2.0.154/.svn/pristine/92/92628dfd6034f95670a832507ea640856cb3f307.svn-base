/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {Create Library class to initialize the MapControls library}
 */
package org.gvsig.fmap.mapcontrol;


import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.mapcontext.MapContextLibrary;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Initialization of the MapControls library.
 * 
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class MapControlLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsAPI(MapControlLibrary.class);
        require(MapContextLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
		I18nManager i18nManager = ToolsLocator.getI18nManager();
		i18nManager.addResourceFamily(
				"org.gvsig.fmap.mapcontrol.i18n.text",
				MapControlLibrary.class.getClassLoader(),
				MapControlLibrary.class.getClass().getName()
                );
	}
	
	@Override
	protected void doPostInitialize() throws LibraryException {
	   	IconThemeHelper.registerIcon("cursor", "cursor-crux", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-info-by-point",this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-pan", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-query-area", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-select-by-point", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-select-by-polygon", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-zoom-in", this);
	   	IconThemeHelper.registerIcon("cursor", "cursor-zoom-out", this);

	   	IconThemeHelper.registerIcon("layer", "layer-icon", this);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-group", this);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-vectorial", this);
	   	
	}
}