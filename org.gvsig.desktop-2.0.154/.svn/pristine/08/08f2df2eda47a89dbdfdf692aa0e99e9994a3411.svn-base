/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class DefaultColorTablePainter implements ColorTablePainter {
	private class ColorItem {
		Color color = null;
		double value =0;
		
		ColorItem(double value, Color color) {
			this.color = color;
			this.value = value;
		}
		
		Color getColor() {
			return color;
		}
		
		double getValue() {
			return value;
		}
	}
	
	List<ColorItem> colorItems = null;
	
	DefaultColorTablePainter() {
		this.colorItems = new ArrayList<ColorItem>();
		int n = 256;
		for( int i=0; i<n; i++) {
                Color color = Color.getHSBColor((float) i / (float) n, 0.85f, 1.0f);
                this.colorItems.add( new ColorItem(i,color));
        }

	}
	
	public Color[] getColors() {
		Color[] colors = new Color[colorItems.size()];
		for (int i = 0; i < colors.length; i++) {
			colors[i] = ((ColorItem) colorItems.get(i)).getColor();
		}
		return colors;	
	}

	public String getTableName() {
		return "Default colot table";
	}

	public void paint(Graphics2D g, boolean isSelected) {
		Rectangle area = g.getClipBounds();
		area.y=0;
		area.x=0;
		int x1 = area.x;
		int x2 = area.x + area.width - 1;

		if( this.colorItems.size() > 0 ) {
			double min = colorItems.get(0).getValue();
            double max = colorItems.get(colorItems.size() - 1).getValue();
            for (int i = area.x; i < (area.x + area.width - 1); i++) {
				double pos = min + (((max - min) * (i - area.x)) / (area.width - 2));
				g.setColor(this.colorItems.get((int) pos).getColor());
				g.drawLine(i, area.y, i, area.y + area.height);
			}
		} else {
			g.setColor(new Color(224, 224, 224));
			g.fillRect(x1, area.y, x2 - x1, area.height - 1);
		}
		if (isSelected) {
			g.setColor(Color.black);
		} else {
			g.setColor(new Color(96, 96, 96));
		}
		g.drawRect(x1, area.y, x2 - x1, area.height - 1);
	}

}
