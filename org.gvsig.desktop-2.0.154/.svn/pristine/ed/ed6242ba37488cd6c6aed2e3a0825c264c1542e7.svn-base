/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ProviderSelectionPanel extends AbstractExporttoPanel {

    private static final long serialVersionUID = -5887438468358948411L;

    protected JList exporttoProviderList = null;
    private JScrollPane scrollPane = null;
    private JScrollPane descriptionScrollPane;
    protected JTextArea descriptionText;
    private int[] providerTypes;

    public ProviderSelectionPanel(
        DefaultJExporttoServicePanel exporttoServicePanel, int[] providerTypes) {
        super(exporttoServicePanel);
        this.providerTypes = providerTypes;
        initializeComponents();
    }

    private void initializeComponents() {
        GridBagConstraints gridBagConstraints;
        this.setLayout(new GridBagLayout());

        // Create the list
        exporttoProviderList = new JList();
        exporttoProviderList.setModel(new ExporterSelectionListModel(
            providerTypes));

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(exporttoProviderList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 5, 2);
        add(scrollPane, gridBagConstraints);

        // Create the description area
        descriptionScrollPane = new javax.swing.JScrollPane();
        descriptionText = new javax.swing.JTextArea();

        descriptionText.setColumns(20);
        descriptionText.setEditable(false);
        descriptionText.setRows(5);
        descriptionText.setLineWrap(true);
        descriptionText.setBorder(null);
        descriptionScrollPane.setBorder(null);
        descriptionScrollPane.setViewportView(descriptionText);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        add(descriptionScrollPane, gridBagConstraints);
    }

    protected ExporttoSwingProviderFactory getSelectedProvider() {
        return (ExporttoSwingProviderFactory) exporttoProviderList
            .getSelectedValue();
    }
}
