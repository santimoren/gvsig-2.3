/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.renderers;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageOsAndArchitecture;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class OsAndArchitectureCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 3767874502044161245L;

	public OsAndArchitectureCellRenderer() {
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		String tooltip;
		Icon icon;
		URL resource;
		JLabel check = new JLabel();
		PackageOsAndArchitecture osArch = (PackageOsAndArchitecture) value;

		if( InstallerManager.OS.ALL.equalsIgnoreCase(osArch.osfamily) ) {
			return check;
		}  
                
                String oslabel = osArch.osfamily;
		if( InstallerManager.OS.LINUX.equalsIgnoreCase(osArch.osfamily) ) {
                    oslabel = "Linux";
                    if( !StringUtils.isEmpty(osArch.osname) ) {
                        oslabel = oslabel + " " + osArch.osname;
                        if( !StringUtils.isEmpty(osArch.osversion) ) {
                            oslabel = oslabel + " " + osArch.osversion;
                        }
                    }
		} else if( InstallerManager.OS.DARWIN.equalsIgnoreCase(osArch.osfamily) ) {
                    oslabel = "Darwin";
                    if( !StringUtils.isEmpty(osArch.osname) ) {
                        oslabel = oslabel + " " + osArch.osname;
                        if( !StringUtils.isEmpty(osArch.osversion) ) {
                            oslabel = oslabel + " " + osArch.osversion;
                        }
                    }

                } else if( InstallerManager.OS.WINDOWS.equalsIgnoreCase(osArch.osfamily) ) {
                    oslabel = "Windows";
		}
                
                if( InstallerManager.ARCH.ALL.equalsIgnoreCase(osArch.arch) ) {
                    tooltip = oslabel;
                } else if( InstallerManager.ARCH.X86.equalsIgnoreCase(osArch.arch) ) {
                    tooltip = oslabel + " 32 bits";
                } else if( InstallerManager.ARCH.X86_64.equalsIgnoreCase(osArch.arch) ) {
                    tooltip = oslabel + " 64 bits";
                } else {
                    tooltip = oslabel + " (" + osArch.arch +")";
                }

                try {
                    String s;
                    if( StringUtils.isEmpty(osArch.osname) ) {
                        s = "/images/platform_14x14-"+osArch.osfamily+"-"+osArch.arch+".png";
                    } else {
                        s = "/images/platform_14x14-"+osArch.osfamily+"_"+osArch.osname+"-"+osArch.arch+".png";
                    }
			resource = this.getClass().getResource(s);
		} catch( Exception ex) {
			resource = null;
		}
		if( resource == null ) {
			resource = this.getClass().getResource("/images/platform_14x14-unknow-unknow.png");
		}
		icon = new ImageIcon(resource);
		check.setIcon(icon);
		check.setToolTipText(tooltip);
		return check;
	}

}
