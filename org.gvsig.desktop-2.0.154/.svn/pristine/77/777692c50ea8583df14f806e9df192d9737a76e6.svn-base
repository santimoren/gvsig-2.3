/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.operation.fromwkt;

import com.vividsolutions.jts.io.ParseException;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.fromwkt.FromWKTGeometryOperationContext;

/**
 * @author jmvivo
 *
 */
public class FromWKT extends GeometryOperation {

    public static final String NAME = GeometryManager.OPERATIONS.FROMWKT;
    public static final int CODE = GeometryLocator.getGeometryManager().
            getGeometryOperationCode(NAME);

//    private static final WKTParser parser = new WKTParser();
    private static final EWKTParser parser = new EWKTParser();
    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#getOperationIndex()
     */

    @Override
    public int getOperationIndex() {
        return CODE;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    @Override
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
            throws GeometryOperationException {
        try {
            return parser.read((String) ctx.getAttribute(FromWKTGeometryOperationContext.TEXT));
        } catch (ParseException e) {
            int type = geom==null? Geometry.TYPES.NULL : geom.getType();
            throw new GeometryOperationException(type, CODE, e);
          }
    }

}
