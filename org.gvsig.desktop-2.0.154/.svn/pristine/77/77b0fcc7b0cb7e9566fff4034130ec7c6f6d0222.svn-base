/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.installer.swing.api.JProgressPanel;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.task.JTaskStatus;
import org.gvsig.tools.task.TaskStatus;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultProgressPanel extends JProgressPanel {

	protected static Logger logger = LoggerFactory
			.getLogger(DefaultProgressPanel.class);
	/**
     * 
     */
	private static final long serialVersionUID = 3961459135479121712L;
	final private JTaskStatus progressBar;

	public DefaultProgressPanel() {
		super();
		progressBar = ToolsSwingLocator.getTaskStatusSwingManager()
				.createJTaskStatus();

		initComponents();
	}

	@Override
	public void bind(TaskStatus taskStatus) {
		this.progressBar.bind(taskStatus);
	}

	protected void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		setLayout(new GridBagLayout());
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(progressBar, gridBagConstraints);

	}

	public void showErrorMessage(String msg, Throwable ex) {
		logger.error(msg, ex);
                
	}
}
