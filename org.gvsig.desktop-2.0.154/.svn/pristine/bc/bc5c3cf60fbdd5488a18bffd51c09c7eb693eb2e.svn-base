/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols.impl;

/*
 * Based on portions of code from www.ColorBrewer.org
 *
 * Colors from www.ColorBrewer.org by Cynthia A. Brewer,
 * Geography, Pennsylvania State University.
 * Using groups of 4 colors from the 9 Diverging Schemes
 * 4 * 9 = 36 colors
 */


import java.awt.Color;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.MapContextRuntimeException;
import org.gvsig.fmap.mapcontext.impl.InvalidRegisteredClassException;
import org.gvsig.fmap.mapcontext.impl.RegisteredClassInstantiationException;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol_v2;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.CancellableTask;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatusManager;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * Default {@link SymbolManager} implementation.
 *
 * @author gvSIG team
 */
public class DefaultSymbolManager implements SymbolManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSymbolManager.class);

    private SymbolPreferences symbolPreferences
            = new DefaultSymbolPreferences();

    private Map symbolsByName = Collections.synchronizedMap(new HashMap());

    private Map symbolsByShapeType = Collections.synchronizedMap(new HashMap());

    private Map multiLineSymbolsByName
            = Collections.synchronizedMap(new HashMap());

    private Map multiLineSymbolsByShapeType
            = Collections.synchronizedMap(new HashMap());

    private IWarningSymbol warningSymbol;

    private Object warningSymbolLock = new Object();

    public ISymbol[] loadSymbols(File folder) throws SymbolException {
        return loadSymbols(folder, null);
    }

    public ISymbol[] loadSymbols(File folder, FileFilter fileFilter)
            throws SymbolException {
        // TODO: add symbol caching

        if (folder.exists()) {

            File[] symbolFiles = null;
            if (fileFilter == null) {
                symbolFiles = folder.listFiles();
            } else {
                symbolFiles = folder.listFiles(fileFilter);
            }

            if (symbolFiles != null) {
                TaskStatusManager tm = ToolsLocator.getTaskStatusManager();
                SimpleTaskStatus status = tm.createDefaultSimpleTaskStatus("Load symbols");
                status.setRangeOfValues(0, symbolFiles.length);
                status.setAutoremove(true);
                status.add();

                /*
                 * Sorting by file name before loading.
                 * The problem here is that some
                 * descriptions can be empty, so sorting using description
                 * can have strange behavior. Sorting by file name is not
                 * very elegant, though.
                 */
                status.message("sorting symbols");
                Arrays.sort(symbolFiles, new Comparator() {
                    public int compare(Object o1, Object o2) {
                        File f1 = (File) o1;
                        File f2 = (File) o2;
                        return f1.getName().compareTo(f2.getName());
                    }
                });

                ISymbol[] symbols = null;
                try {
                    symbols = new ISymbol[symbolFiles.length];
                    for (int i = 0; i < symbolFiles.length; i++) {
                        status.setCurValue(i);
                        File symbolFile = symbolFiles[i];
                        try {
                            symbols[i] = loadSymbol(symbolFile);
                        } catch(Throwable th) {
                            logger.warn("Can't load symbol '"+symbolFile.getAbsolutePath()+"'.",th);
                        }
                    }

                } finally {
                    status.terminate();
                }
                return symbols;
            }
        }

        return null;
    }

    public class SymbolsLoaderTask extends AbstractMonitorableTask {

        private File folder;
        private FileFilter filter;
        private Visitor visitor;

        public SymbolsLoaderTask(File folder, FileFilter filter, Visitor visitor) {
            super("Load symbols", true);
            this.folder = folder;
            this.filter = filter;
            this.visitor = visitor;
            if (folder == null) {
                throw new IllegalArgumentException("folder is null");
            }
            if (visitor == null) {
                throw new IllegalArgumentException("visitor is null");
            }
        }

        private File[] getFiles() {
            if (filter == null) {
                return folder.listFiles();
            } else {
                return folder.listFiles(filter);
            }
        }

        private String getVisitorName() {
            String s = visitor.toString() + "/" + visitor.getClass().getName();
            return s;
        }

        private void visit(final File file, final ISymbol symbol) throws VisitCanceledException {
            try {
                visitor.visit(symbol);
            } catch (BaseException e) {
                logger.warn("Can't call visit '" + getVisitorName() + "' to offer the symbol '" + file.getAbsolutePath() + "'.", e);
            }
        }

        public void run() {
            // TODO: add symbol caching
            try {
                logger.info("[SymbolsLoaderTask" + this.getId() + "] process initited.");
                if (folder.exists()) {
                    taskStatus.setAutoremove(true);
                    taskStatus.message("preparing");
                    File[] symbolFiles = getFiles();

                    if (symbolFiles != null) {
                        taskStatus.setRangeOfValues(0, symbolFiles.length);

                        /*
                         * Sorting by file name before loading. The problem here
                         * is that some descriptions can be empty, so sorting
                         * using description can have strange behavior. Sorting
                         * by file name is not very elegant, though.
                         */
                        taskStatus.message("sorting");
                        Arrays.sort(symbolFiles, new Comparator() {
                            public int compare(Object o1, Object o2) {
                                File f1 = (File) o1;
                                File f2 = (File) o2;
                                return f1.getName().compareTo(f2.getName());
                            }
                        });

                        taskStatus.message("loading");
                        for (int i = 0; i < symbolFiles.length; i++) {
                            taskStatus.setCurValue(i);
                            if (taskStatus.isCancellationRequested()) {
                                logger.info("[SymbolsLoaderTask" + this.getId() + "] process canceled.");
                                break;
                            }
                            ISymbol symbol = null;
                            try {
                                symbol = loadSymbol(symbolFiles[i]);
                                visit(symbolFiles[i], symbol);
                            } catch (VisitCanceledException e) {
                                break;
                            } catch (Throwable e) {
                                logger.warn("Can't load symbol '"
                                        + symbolFiles[i].getAbsolutePath()
                                        + "'.", e);
                            }
                        }
                        taskStatus.message("");

                    }
                }

            } finally {
                taskStatus.terminate();
            }
            logger.info("[SymbolsLoaderTask" + this.getId() + "] process terminated.");

        }
    }

    public CancellableTask loadSymbols(File folder, FileFilter filter, Visitor visitor) {
        SymbolsLoaderTask task = new SymbolsLoaderTask(folder, filter, visitor);
        task.start();
        return task;
    }

    public void saveSymbol(ISymbol symbol, String fileName, File folder)
            throws SymbolException {
        saveSymbol(symbol, fileName, folder, false);

    }

    public void saveSymbol(ISymbol symbol, String fileName, File folder,
            boolean overwrite) throws SymbolException {
        // TODO: add symbol caching

        PersistenceManager persistenceManager = ToolsLocator
                .getPersistenceManager();

        File symbolFile = new File(folder, fileName);
        if (!overwrite && symbolFile.exists()) {
            throw new SymbolFileAlreadyExistsException(symbolFile);
        }

        try {
            FileOutputStream fos = new FileOutputStream(symbolFile);

            persistenceManager.saveState(persistenceManager.getState(symbol),
                    fos);

            fos.flush();
            fos.close();
        } catch (PersistenceException e) {
            throw new SaveSymbolException(e);
        } catch (IOException e) {
            throw new SaveSymbolException(e);
        }
        if (symbol instanceof ISymbol_v2) {
            ISymbol_v2 symbolv2 = (ISymbol_v2) symbol;
            if (StringUtils.isBlank(symbolv2.getID())) {
                symbolv2.setID(FilenameUtils.getBaseName(fileName));
            }
        }
    }

    /**
     * Loads a persisted symbol from the given file.
     */
    private ISymbol loadSymbol(File file) throws SymbolException {
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);

                PersistenceManager persistenceManager = ToolsLocator
                        .getPersistenceManager();

                PersistentState state = persistenceManager.loadState(fis);
                ISymbol symbol = (ISymbol) persistenceManager.create(state);

                fis.close();
                if (symbol instanceof ISymbol_v2) {
                    ISymbol_v2 symbolv2 = (ISymbol_v2) symbol;
                    symbolv2.setID(FilenameUtils.getBaseName(file.getName()));
                }
                return symbol;
            } catch (PersistenceException e) {
                throw new LoadSymbolException(e);
            } catch (IOException e) {
                throw new LoadSymbolException(e);
            }
        }

        return null;
    }

    public SymbolPreferences getSymbolPreferences() {
        return symbolPreferences;
    }

    public ISymbol createSymbol(String symbolName)
            throws MapContextRuntimeException {
        return createSymbol(symbolName, (Class) symbolsByName.get(symbolName),
                ISymbol.class);
    }

    public ISymbol createSymbol(int shapeType)
            throws MapContextRuntimeException {
        String symbolName = getSymbolName(symbolsByShapeType, shapeType);

        return symbolName == null ? null : createSymbol(symbolName);
    }

    public ISymbol createSymbol(String symbolName, Color color)
            throws MapContextRuntimeException {
        ISymbol symbol = createSymbol(symbolName);

        if (symbol != null) {
            symbol.setColor(color);
        }

        return symbol;
    }

    public ISymbol createSymbol(int shapeType, Color color)
            throws MapContextRuntimeException {
        String symbolName = getSymbolName(symbolsByShapeType, shapeType);

        return symbolName == null ? null : createSymbol(symbolName, color);
    }

    public IMultiLayerSymbol createMultiLayerSymbol(String symbolName)
            throws MapContextRuntimeException {
        return (IMultiLayerSymbol) createSymbol(symbolName,
                (Class) multiLineSymbolsByName.get(symbolName),
                IMultiLayerSymbol.class);
    }

    public IMultiLayerSymbol createMultiLayerSymbol(int shapeType)
            throws MapContextRuntimeException {
        String symbolName = getSymbolName(multiLineSymbolsByShapeType, shapeType);

        return symbolName == null ? null : createMultiLayerSymbol(symbolName);
    }

    /**
     * @param shapeType
     * @return
     */
    private String getSymbolName(Map symbols, int shapeType) {
        String symbolName
                = (String) symbols.get(new Integer(shapeType));
        if (symbolName == null) {
            GeometryManager geomManager = GeometryLocator.getGeometryManager();
            GeometryType shapeGeometryType;
            try {
                shapeGeometryType = geomManager.getGeometryType(shapeType, Geometry.SUBTYPES.UNKNOWN);
                Iterator it = symbols.keySet().iterator();
                while (it.hasNext()) {
                    Integer geomType = (Integer) it.next();
                    GeometryType geomGeometryType =
                        geomManager.getGeometryType(geomType.intValue(), Geometry.SUBTYPES.UNKNOWN);
                    if (shapeGeometryType.isSubTypeOf(geomGeometryType)) {
                        symbolName = (String) symbols.get(geomType);
                        break;
                    }
                }
            } catch (GeometryTypeNotSupportedException e) {
                // do nothing
            } catch (GeometryTypeNotValidException e) {
                // do nothing
            }
        }
        return symbolName;
    }

    public void registerSymbol(String symbolName, Class symbolClass)
            throws MapContextRuntimeException {
        if (symbolClass == null || !ISymbol.class.isAssignableFrom(symbolClass)) {
            throw new InvalidRegisteredClassException(ISymbol.class,
                    symbolClass, symbolName);
        }
        symbolsByName.put(symbolName, symbolClass);
    }

    public void registerSymbol(String symbolName, int[] shapeTypes,
            Class symbolClass) throws MapContextRuntimeException {
        registerSymbol(symbolName, symbolClass);
        if (shapeTypes != null) {
            for (int i = 0; i < shapeTypes.length; i++) {
                symbolsByShapeType.put(new Integer(shapeTypes[i]), symbolName);
            }
        }
    }

    public void registerMultiLayerSymbol(String symbolName, Class symbolClass)
            throws MapContextRuntimeException {
        if (symbolClass == null
                || !IMultiLayerSymbol.class.isAssignableFrom(symbolClass)) {
            throw new InvalidRegisteredClassException(IMultiLayerSymbol.class,
                    symbolClass, symbolName);
        }

        multiLineSymbolsByName.put(symbolName, symbolClass);
    }

    public void registerMultiLayerSymbol(String symbolName, int[] shapeTypes,
            Class symbolClass) throws MapContextRuntimeException {
        registerMultiLayerSymbol(symbolName, symbolClass);
        if (shapeTypes != null) {
            for (int i = 0; i < shapeTypes.length; i++) {
                multiLineSymbolsByShapeType.put(new Integer(shapeTypes[i]),
                        symbolName);
            }
        }
    }

    public IWarningSymbol getWarningSymbol(String message, String symbolDesc,
            int symbolDrawExceptionType) throws MapContextRuntimeException {
        synchronized (warningSymbolLock) {
            if (warningSymbol == null) {
                warningSymbol = (IWarningSymbol) createSymbol("warning");
            }
        }

        // TODO: set those values as parameter values in the draw method.
        warningSymbol.setDescription(symbolDesc);
        warningSymbol.setMessage(message);
        warningSymbol.setDrawExceptionType(symbolDrawExceptionType);

        return warningSymbol;
    }

    private ISymbol createSymbol(String symbolName, Class symbolClass,
            Class expectedType) throws MapContextRuntimeException {
        ISymbol symbol;
        try {
            symbol
                    = (ISymbol) (symbolClass == null ? null
                    : symbolClass.newInstance());
        } catch (InstantiationException e) {
            throw new RegisteredClassInstantiationException(expectedType,
                    symbolClass, symbolName, e);
        } catch (IllegalAccessException e) {
            throw new RegisteredClassInstantiationException(expectedType,
                    symbolClass, symbolName, e);
        }

        Color the_color = null;

        if (getSymbolPreferences().isDefaultSymbolFillColorAleatory()) {

            the_color = getRandomBrewerBasedColor();

        } else {
            // not random
            the_color = getSymbolPreferences().getDefaultSymbolFillColor();
        }
        symbol.setColor(the_color);
        // Perform this initialization into the Symbol implementation
        // if (symbol instanceof CartographicSupport) {
        // CartographicSupport cs = (CartographicSupport) symbol;
        // cs.setUnit(getDefaultCartographicSupportMeasureUnit());
        // cs
        // .setReferenceSystem(getDefaultCartographicSupportReferenceSystem();
        // }

        return symbol;
    }

    public void setSymbolPreferences(SymbolPreferences symbolPreferences) {
        this.symbolPreferences = symbolPreferences;
    }

    public List getSymbolLibraryNames() {
        File rootfolder = new File(this.getSymbolPreferences().getSymbolLibraryPath());
        Collection libraries = FileUtils.listFiles(rootfolder, FileFilterUtils.directoryFileFilter(), null);
        List l = new ArrayList();
        l.addAll(libraries);
        return l;
    }

    public ISymbol getSymbol(String libraryName, String symbolID) throws SymbolException {
        Collection symbols = null;
        File rootfolder = null;
        try {
            rootfolder = new File(this.getSymbolPreferences().getSymbolLibraryPath());
            symbols = FileUtils.listFiles(rootfolder,
                    FileFilterUtils.nameFileFilter(symbolID + getSymbolPreferences().getSymbolFileExtension()),
                    FileFilterUtils.trueFileFilter()
            );
        } catch(Exception ex) {
            logger.warn("Can't get symbol from symbol library (library:'"+libraryName+"', symbol:'"+symbolID+"', symbolLibraryPath'"+rootfolder+"')", ex);
        }
        if (symbols == null) {
            return null;
        }
        if (symbols.isEmpty()) {
            return null;
        }

        File f = null;
        try {
            f = (File) symbols.iterator().next();
            ISymbol symbol = loadSymbol(f);
            return symbol;
        } catch(Exception ex) {
            String fname = ((f==null)?"Null":f.getAbsolutePath());
            logger.warn("Can't load symbol from symbol library (library:'"+libraryName+"', symbol:'"+symbolID+"', symbolLibraryPath'"+rootfolder+"', symbolFile:'"+fname+"')", ex);
        }
        return null;
    }

    /**
     *
     * @param col
     * @return color 1/3 closer to black
     */
    public static Color darker(Color col) {
        return new Color(
                (2 * col.getRed()) / 3,
                (2 * col.getGreen()) / 3,
                (2 * col.getBlue()) / 3,
                col.getAlpha());
    }

    /**
     *
     * @param col
     * @return color 1/3 closer to white
     */
    public static Color lighter(Color col) {
        Color resp = invert(col);
        resp = darker(resp);
        return invert(resp);
    }

    /**
     *
     * @param col
     * @return inverted color (inverts each band, same alpha)
     */
    public static Color invert(Color col) {
        return new Color(
                255 - col.getRed(),
                255 - col.getGreen(),
                255 - col.getBlue(),
                col.getAlpha());
    }

    private static Color getRandomBrewerBasedColor() {

        int ind = rnd.nextInt(BREWER_COLOR.length);
        Color resp = BREWER_COLOR[ind];
        ind = rnd.nextInt(100);
        if (ind > 66) {
            resp = darker(resp);
        } else {
            if (ind > 33) {
                resp = lighter(resp);
            }
            // ...else resp remains the same
        }

        // finally add some dark noise
        resp = addDarkNoise(resp);
        return resp;
    }

    private static Color addDarkNoise(Color c) {
        int r = Math.max(0, c.getRed() - rnd.nextInt(30));
        int g = Math.max(0, c.getGreen() - rnd.nextInt(30));
        int b = Math.max(0, c.getBlue() - rnd.nextInt(30));
        return new Color(r, g, b, c.getAlpha());
    }

    private static Color[] BREWER_COLOR = new Color[36];
    private static final Random rnd = new Random();

    static {
        /**
         * Colors from www.ColorBrewer.org by Cynthia A. Brewer, Geography,
         * Pennsylvania State University.
         *
         * Using groups of 4 colors from the 9 Diverging Schemes 4 * 9 = 36
         * colors
         */
        BREWER_COLOR[0] = new Color(230, 97, 1);
        BREWER_COLOR[1] = new Color(253, 184, 99);
        BREWER_COLOR[2] = new Color(178, 171, 210);
        BREWER_COLOR[3] = new Color(94, 60, 153);
        BREWER_COLOR[4] = new Color(166, 97, 26);
        BREWER_COLOR[5] = new Color(223, 194, 125);
        BREWER_COLOR[6] = new Color(128, 205, 193);
        BREWER_COLOR[7] = new Color(1, 133, 113);
        BREWER_COLOR[8] = new Color(123, 50, 148);
        BREWER_COLOR[9] = new Color(194, 165, 207);
        BREWER_COLOR[10] = new Color(166, 219, 160);
        BREWER_COLOR[11] = new Color(0, 136, 55);
        BREWER_COLOR[12] = new Color(208, 28, 139);
        BREWER_COLOR[13] = new Color(241, 182, 218);
        BREWER_COLOR[14] = new Color(184, 225, 134);
        BREWER_COLOR[15] = new Color(77, 172, 38);
        BREWER_COLOR[16] = new Color(202, 0, 32);
        BREWER_COLOR[17] = new Color(244, 165, 130);
        BREWER_COLOR[18] = new Color(146, 197, 222);
        BREWER_COLOR[19] = new Color(5, 113, 176);
        BREWER_COLOR[20] = new Color(202, 0, 32);
        BREWER_COLOR[21] = new Color(244, 165, 130);
        BREWER_COLOR[22] = new Color(186, 186, 186);
        BREWER_COLOR[23] = new Color(64, 64, 64);
        BREWER_COLOR[24] = new Color(215, 25, 28);
        BREWER_COLOR[25] = new Color(253, 174, 97);
        BREWER_COLOR[26] = new Color(171, 217, 233);
        BREWER_COLOR[27] = new Color(44, 123, 182);
        BREWER_COLOR[28] = new Color(215, 25, 28);
        BREWER_COLOR[29] = new Color(253, 174, 97);
        BREWER_COLOR[30] = new Color(171, 221, 164);
        BREWER_COLOR[31] = new Color(43, 131, 186);
        BREWER_COLOR[32] = new Color(215, 25, 28);
        BREWER_COLOR[33] = new Color(253, 174, 97);
        BREWER_COLOR[34] = new Color(166, 217, 106);
        BREWER_COLOR[35] = new Color(26, 150, 65);
    }

}
