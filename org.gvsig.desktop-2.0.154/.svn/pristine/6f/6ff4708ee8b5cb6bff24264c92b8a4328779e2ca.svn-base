<?xml version="1.0" encoding="ISO-8859-1"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <artifactId>org.gvsig.labeling.app.mainplugin</artifactId>
  <packaging>jar</packaging>
  <name>${project.artifactId}</name>
  <description>
  This plugin provides extended labeling options for vector layers, such as labeling selected features only,
  label position with regard to the geometry, etc.
  </description>

  <parent>
    <groupId>org.gvsig</groupId>
    <artifactId>org.gvsig.labeling.app</artifactId>
    <version>2.0.154</version>
  </parent>

  <dependencies>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.app.mainplugin</artifactId>
          <scope>compile</scope>
      </dependency>
  
      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.symbology.swing.api</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.symbology.lib.impl</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.andami</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.fmap.mapcontext.api</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.fmap.dal.api</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.fmap.dal.file.shp</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.i18n</artifactId>
          <scope>compile</scope>
      </dependency>
      
      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.ui</artifactId>
          <scope>compile</scope>
      </dependency>
      
      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.fmap.geometry.api</artifactId>
          <scope>compile</scope>
      </dependency>

      <dependency>
          <groupId>org.gvsig</groupId>
          <artifactId>org.gvsig.fmap.control</artifactId>
          <scope>compile</scope>
      </dependency>
      
      <dependency>
         <groupId>org.slf4j</groupId>
         <artifactId>slf4j-api</artifactId>
         <scope>compile</scope>
      </dependency>
    
	  <dependency>
          <groupId>org.gvsig</groupId>
		  <artifactId>org.gvsig.tools.evaluator.sqljep</artifactId>
          <scope>compile</scope>
	  </dependency>
	  
        <dependency>
            <groupId>org.gvsig</groupId>
            <artifactId>org.gvsig.tools.lib</artifactId>
            <scope>compile</scope>
        </dependency>
      
		<dependency>
			<groupId>org.gvsig</groupId>
			<artifactId>org.gvsig.projection.api</artifactId>
			<scope>compile</scope>
		</dependency>
    <!-- Runtime dependencies -->


  </dependencies>

  <properties>
      <gvsig.package.info.name>Labeling: advanced options</gvsig.package.info.name>
      <gvsig.package.info.dependencies>required: org.gvsig.app.mainplugin -ge 2</gvsig.package.info.dependencies>
      <gvsig.package.info.categories>Vector,Labeling</gvsig.package.info.categories>
	  <gvsig.package.info.official>true</gvsig.package.info.official>
	  <gvsig.package.info.poolURL>http://devel.gvsig.org/download/projects/gvsig-labeling/pool</gvsig.package.info.poolURL>
  </properties>  
</project>
