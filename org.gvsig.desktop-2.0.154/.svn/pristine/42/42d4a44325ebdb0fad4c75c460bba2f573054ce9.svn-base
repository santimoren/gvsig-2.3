/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.fmap.dal.store.memory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MemoryStoreProviderTest extends AbstractLibraryAutoInitTestCase {
	protected Logger logger = LoggerFactory.getLogger(getClass());;
	protected static DataManager dataManager = null;
	protected static GeometryManager geomManager = null;

	protected static final String ID = "id";
	protected static final String GEOM = "geom";
	protected static final String IDSYM = "idsym";
	protected static final String LABEL = "label";
	protected static final String FEATUREID = "featureid";

	protected void doSetUp() throws Exception {
		dataManager = DALLocator.getDataManager();
		geomManager = GeometryLocator.getGeometryManager();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseTestFeatureStore#getDefaultDataStoreParameters()
	 */
	public DataStoreParameters getDefaultDataStoreParameters()
	throws DataException {
		MemoryStoreParameters memoryParameters = null;

		memoryParameters = (MemoryStoreParameters) dataManager
		.createStoreParameters(MemoryStoreProvider.NAME);
		return memoryParameters;
	}

	public void testInitializeStore() throws Exception {
		FeatureStore store = (FeatureStore) dataManager.createStore(this
				.getDefaultDataStoreParameters());

		assertNotNull(store.getMetadataID());
		assertNotNull(store.getName());
		assertEquals(store.getEnvelope(), store.getDynValue("Envelope"));
		assertTrue(store.getFeatureCount() == 0);
		if (store.isLocksSupported()) {
			assertNotNull(store.getLocks());
		} else {
			assertNull(store.getLocks());
		}
		store.dispose();
	}

	public void testAddFeatureType() throws DataException, ValidateDataParametersException{
		FeatureStore store = createEditableFeatureStore();
		FeatureAttributeDescriptor attributeDescriptor = store.getDefaultFeatureType().getAttributeDescriptor(ID);
		assertNotNull(attributeDescriptor);
		assertEquals(attributeDescriptor.getType(), DataTypes.OBJECT);

		store.dispose();
	}

	protected FeatureStore createEditableFeatureStore() throws ValidateDataParametersException, InitializeException, ProviderNotRegisteredException, DataException{
		FeatureStore store = (FeatureStore) dataManager.createStore(this
				.getDefaultDataStoreParameters());

		store.edit();

		EditableFeatureType editableFeatureType = store.getDefaultFeatureType().getEditable();

		EditableFeatureAttributeDescriptor idDescriptor = editableFeatureType.add(ID, DataTypes.OBJECT).setObjectClass(Object.class);
		idDescriptor.setIsPrimaryKey(true);
		EditableFeatureAttributeDescriptor geometryDescriptor = 
			editableFeatureType.add(GEOM, DataTypes.GEOMETRY);
		geometryDescriptor.setGeometryType(TYPES.GEOMETRY);
		editableFeatureType.setDefaultGeometryAttributeName(GEOM);

		editableFeatureType.add(IDSYM, DataTypes.OBJECT).setObjectClass(Object.class);
		editableFeatureType.add(LABEL, DataTypes.STRING);
		EditableFeatureAttributeDescriptor featureIdDescriptor = 
			editableFeatureType.add(FEATUREID, DataTypes.OBJECT).setObjectClass(Object.class);		
		
		//editableFeatureType.setHasOID(true);

		store.update(editableFeatureType);	
		store.finishEditing();
		return store;
	}

	public void testAddFeature() throws DataException, ValidateDataParametersException, CreateGeometryException{
		FeatureStore store = createEditableFeatureStore();

		store.edit();

		EditableFeature feature = store.createNewFeature().getEditable();
		feature.set(ID, "0");
		feature.set(GEOM, geomManager.createPoint(0, 0, SUBTYPES.GEOM2D));

		store.insert(feature);

		Envelope envelope = store.getEnvelope();
		assertNotNull(envelope);

		store.finishEditing();

		FeatureSet featureSet = store.getFeatureSet();
		DisposableIterator it = featureSet.fastIterator();
		while (it.hasNext()){
			Feature feature2 = (Feature)it.next();
			assertEquals(feature.getType().get(0), feature2.getType().get(0));
			assertEquals(feature.get(0), feature2.get(0));

			assertEquals(feature.getType().get(1), feature2.getType().get(1));
			assertEquals(feature.get(1), feature2.get(1));
		}	

		envelope = store.getEnvelope();
		assertNotNull(envelope);

		store.dispose();
	}

	public void testAddFeatures() throws DataException, ValidateDataParametersException, CreateGeometryException{
		FeatureStore store = createEditableFeatureStore();

		store.edit();

		EditableFeature feature = store.createNewFeature().getEditable();
		feature.set(ID, "0");
		feature.set(GEOM, geomManager.createPoint(0, 0, SUBTYPES.GEOM2D));

		// One
		store.insert(feature);

		EditableFeature feature2 = store.createNewFeature().getEditable();
		feature2.set(ID, "1");
		Curve curve =  geomManager.createCurve(Geometry.SUBTYPES.GEOM2D);
		curve.addVertex(0,0);
		curve.addVertex(1,0);
		curve.addVertex(1,1);
		curve.addVertex(0,1);
		curve.addVertex(0,0);
		feature2.set(GEOM, curve);

		// Two
		store.insert(feature2);

		EditableFeature feature3 = store.createNewFeature().getEditable();
		feature3.set(ID, "2");
		Surface surface =  geomManager.createSurface(Geometry.SUBTYPES.GEOM2D);
		surface.addVertex(0,0);
		surface.addVertex(1,0);
		surface.addVertex(1,1);
		surface.addVertex(0,1);
		surface.addVertex(0,0);
		feature3.set(GEOM, surface);

		//Three
		store.insert(feature3);

		Envelope envelope = store.getEnvelope();
		assertNotNull(envelope);

		store.finishEditing();

		FeatureSet featureSet = store.getFeatureSet();
		DisposableIterator it = featureSet.fastIterator();

		//Count the features in the set
		int i=0;
		while (it.hasNext()){
			Feature feat = (Feature)it.next();
			i++;
		}	

		//Are all three inside?
		assertEquals(3,i);

		envelope = store.getEnvelope();
		assertNotNull(envelope);

		// Editing other time
		store.edit();
		EditableFeature feature4 = store.createNewFeature().getEditable();
		feature4.set(ID, "4");
		surface =  geomManager.createSurface(Geometry.SUBTYPES.GEOM2D);
		surface.addVertex(0,0);
		surface.addVertex(1,0);
		surface.addVertex(1,1);
		surface.addVertex(0,1);
		surface.addVertex(0,0);
		feature4.set(GEOM, surface);

		//four
		store.insert(feature4);

		store.finishEditing();
		
		//Are all four inside?
		assertEquals(4,store.getFeatureCount());

		store.dispose();
	}

	public void testRemoveFeatures() throws DataException, ValidateDataParametersException, CreateGeometryException{
		FeatureStore store = createEditableFeatureStore();

		store.edit();

		EditableFeature feature = store.createNewFeature().getEditable();
		feature.set(ID, "0");
		feature.set(GEOM, geomManager.createPoint(0, 0, SUBTYPES.GEOM2D));

		// One
		store.insert(feature);

		EditableFeature feature2 = store.createNewFeature().getEditable();
		feature2.set(ID, "1");
		Curve curve =  geomManager.createCurve(Geometry.SUBTYPES.GEOM2D);
		curve.addVertex(0,0);
		curve.addVertex(1,0);
		curve.addVertex(1,1);
		curve.addVertex(0,1);
		curve.addVertex(0,0);
		feature2.set(GEOM, curve);

		// Two
		store.insert(feature2);

		EditableFeature feature3 = store.createNewFeature().getEditable();
		feature3.set(ID, "2");
		Surface surface =  geomManager.createSurface(Geometry.SUBTYPES.GEOM2D);
		surface.addVertex(0,0);
		surface.addVertex(1,0);
		surface.addVertex(1,1);
		surface.addVertex(0,1);
		surface.addVertex(0,0);
		feature3.set(GEOM, surface);

		//Three
		store.insert(feature3);

		Envelope envelope = store.getEnvelope();
		assertNotNull(envelope);

		store.finishEditing();

		// Editing other time and remove the second one
		store.edit();
			

		FeatureSet featureSet = store.getFeatureSet();
		DisposableIterator it = featureSet.fastIterator();

		//Count the features in the set
		int i=1;
		Feature addedFeature2 = null;
		while (it.hasNext()){
			Feature feat = (Feature)it.next();
			if (i == 2){
				addedFeature2 = feat;
			}			
			i++;
		}	
		
		store.delete(addedFeature2);

		store.finishEditing();
		//Are two inside?
		assertEquals(2,store.getFeatureCount());

		store.dispose();
	}
	
	public void testUpdateFeatures() throws DataException, ValidateDataParametersException, CreateGeometryException{
		FeatureStore store = createEditableFeatureStore();

		store.edit();

		EditableFeature feature = store.createNewFeature().getEditable();
		feature.set(ID, "0");
		feature.set(GEOM, geomManager.createPoint(0, 0, SUBTYPES.GEOM2D));
		feature.set(LABEL, "Hi");		
		store.insert(feature);
		store.finishEditing();

		// Editing other time and update 
		store.edit();
			
		FeatureSet featureSet = store.getFeatureSet();
		DisposableIterator it = featureSet.fastIterator();
		
		EditableFeature editableFeature = null;
		while (it.hasNext()){			
			Feature feat = (Feature)it.next();
			editableFeature = feat.getEditable();
			editableFeature.set(LABEL, "Bye");			
		}
		
		store.update(editableFeature);
		
		store.finishEditing();
		
		//Check the value
		featureSet = store.getFeatureSet();
		it = featureSet.fastIterator();

		while (it.hasNext()){
			Feature feat = (Feature)it.next();
			assertEquals("Bye", feat.get(LABEL));
		}	
		
		store.dispose();
	}
}

