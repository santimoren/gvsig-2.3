/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.swing.impl;

import javax.swing.ComboBoxModel;
import org.gvsig.featureform.swing.CreateJFeatureFormException;
import org.gvsig.featureform.swing.JFeatureForm;

import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.featureform.swing.impl.DefaultJFeatureForm;
import org.gvsig.featureform.swing.impl.DefaultJFeaturesForm;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.DataSwingManager;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.JFeatureTable;
import org.gvsig.fmap.dal.swing.impl.featuretable.FeatureTablePanel;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.DefaultFeatureTableModel;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.EmptyFeatureTableModel;
import org.gvsig.fmap.dal.swing.impl.jdbc.DefaultJDBCConnectionPanel;
import org.gvsig.fmap.dal.swing.impl.queryfilter.DefaultQueryFilterExpresion;
import org.gvsig.fmap.dal.swing.jdbc.JDBCConnectionPanel;
import org.gvsig.fmap.dal.swing.queryfilter.QueryFilterExpresion;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;


/**
 * @author fdiaz
 *
 */
public class DefaultDataSwingManager implements DataSwingManager {

    @Override
    public JFeaturesForm createJFeaturesForm(FeatureStore store) throws CreateJFeatureFormException {
        try {
            FeatureType featureType = store.getDefaultFeatureType();
            FeatureTypeDefinitionsManager featureTypeDefinitionsManager = DALLocator.getFeatureTypeDefinitionsManager();
            if( featureTypeDefinitionsManager.contains(store,featureType) ) {
                // Force reload next call to get.
                featureTypeDefinitionsManager.remove(store,featureType);
            }
            DynClass dynClass = featureTypeDefinitionsManager.get(store,featureType);
            DynFormLocator.getDynFormManager().removeDefinition(dynClass.getName());
            
            DefaultJFeaturesForm form = new DefaultJFeaturesForm();
            form.bind(store, dynClass);
            return form;
            
        } catch (Exception ex) {
            throw new CreateJFeatureFormException(ex);
        }
    }

    @Override
    public JDBCConnectionPanel createJDBCConnectionPanel() {
        JDBCConnectionPanel x = new DefaultJDBCConnectionPanel();
        return x;
    }

    @Override
    public QueryFilterExpresion createQueryFilterExpresion(FeatureStore store) {
        return new DefaultQueryFilterExpresion(store);
    }

    @Override
    public JFeatureForm createJFeatureForm(FeatureStore store) throws CreateJFeatureFormException {
        JFeatureForm form = new DefaultJFeatureForm();
        form.setStore(store);
        return form;
    }

    @Override
    public JFeatureForm createJFeatureForm(Feature feature) throws CreateJFeatureFormException {
        JFeatureForm form = new DefaultJFeatureForm();
        form.setFeature(feature);
        return form;
    }

    public JFeatureTable createJFeatureTable(FeatureTableModel model) {
        return this.createJFeatureTable(model, true);
    }
    
    public JFeatureTable createJFeatureTable(FeatureTableModel model, boolean isDoubleBuffered) {
        return new FeatureTablePanel(model, isDoubleBuffered);
    }
    
    public FeatureTableModel createEmptyFeatureTableModel(DynStruct struct) {
        return new EmptyFeatureTableModel(struct);
    }
    
    public FeatureTableModel createFeatureTableModel(FeatureStore featureStore,
            FeatureQuery featureQuery) {
        try {
            FeaturePagingHelper pager = DALLocator.getDataManager().createFeaturePagingHelper(
                    featureStore, featureQuery,
                    FeaturePagingHelper.DEFAULT_PAGE_SIZE
            );
            return this.createFeatureTableModel(pager);
        } catch (BaseException ex) {
            throw new RuntimeException("Can't create a FeatureTableModel.", ex);
        }
    }
    
    public FeatureTableModel createFeatureTableModel(FeaturePagingHelper featurePager) {
        return new DefaultFeatureTableModel(featurePager);
    }
}
