/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.wizard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.panel.DefaultOutputPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectOutputFileWizard extends DefaultOutputPanel implements
		OptionPanel {

	/**
     * 
     */
	private static final long serialVersionUID = 555020268506822671L;
	private DefaultMakePluginPackageWizard installerCreationWizard;
	private static final Logger logger = LoggerFactory
			.getLogger(SelectOutputFileWizard.class);

	public SelectOutputFileWizard(
			DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_output_options");
	}

	public void lastPanel() {

	}

	public void nextPanel() throws NotContinueWizardException {
		try {
			File file = getPackageFile();
			File filePath = new File(file.getParent());
			if (!filePath.exists()) {
				JOptionPane
						.showMessageDialog(
								installerCreationWizard,
								swingInstallerManager
										.getText("_the_package_file_folder_does_not_exist")
										+ ": " + filePath);
				throw new NotContinueWizardException("", null, false);
			}

			if (isCreatePackageIndex()) {
				URL url = getDownloadURL();
				if (url == null) {
					throw new NotContinueWizardException("", null, false);
				}
				installerCreationWizard.setDownloadURL(url);
				File indexFile = getPackageIndexFile();
				File indexFilePath = new File(indexFile.getParent());
				if (!indexFilePath.exists()) {
					JOptionPane
							.showMessageDialog(
									installerCreationWizard,
									swingInstallerManager
											.getText("_the_package_index_file_folder_does_not_exist")
											+ ": " + indexFilePath);
					throw new NotContinueWizardException("", null, false);
				}
				installerCreationWizard
						.setIndexOutputStream(new FileOutputStream(indexFile));
			}
			installerCreationWizard.setOutputStream(new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			logger.info(swingInstallerManager
					.getText("Create output file exception"), e);
			JOptionPane.showMessageDialog(installerCreationWizard,
					swingInstallerManager
							.getText("_create_output_file_exception"));
		}
	}

	public void updatePanel() {
		setPackageFile(getDefaultPackageBundleFile());
		setPackageIndexFile(getDefaultPackageIndexBundleFile());
	}

	private File getDefaultPackageBundleFile() {
		File installsFolder = installerCreationWizard.getInstallFolder();
		PackageInfo info = installerCreationWizard.getSelectedPackageInfo();
		String fileName = InstallerLocator.getInstallerManager()
				.getPackageFileName(info);
		return new File(installsFolder, fileName);
	}

	private File getDefaultPackageIndexBundleFile() {
		File installsFolder = installerCreationWizard.getInstallFolder();
		PackageInfo info = installerCreationWizard.getSelectedPackageInfo();
		String fileName = InstallerLocator.getInstallerManager()
				.getPackageIndexFileName(info);
		return new File(installsFolder, fileName);
	}
	
	@Override
	protected PackageInfo getPackageInfo() {
		PackageInfo info = installerCreationWizard.getSelectedPackageInfo();
		return info;
	}
}
