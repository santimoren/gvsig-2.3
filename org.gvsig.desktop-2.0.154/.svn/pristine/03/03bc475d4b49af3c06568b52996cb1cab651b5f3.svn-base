/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.extension;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.timesupport.AbsoluteIntervalTypeNotRegisteredException;
import org.gvsig.timesupport.swing.api.TimeSupportSwingException;
import org.gvsig.timesupport.swing.api.TimeSupportSwingLocator;
import org.gvsig.timesupport.swing.api.TimeSupportSwingManager;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanel;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Andami extension to show TimeSupport in the application.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class TimeSupportExtension extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(TimeSupportExtension.class);

    public void initialize() {
        // Do nothing
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
    }

    public void execute(String actionCommand) {
        try {
            showTimeSupport();
        } catch (Exception e) {
            logger.warn("Can't show time support dialog",e);
        }
    }

    public void showTimeSupport() throws CloneNotSupportedException, LocatorException, TimeSupportSwingException, AbsoluteIntervalTypeNotRegisteredException {

        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument viewDocument = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if( viewDocument == null ) {
            return;
        }
        
        TimeSupportSwingManager swingManager = TimeSupportSwingLocator.getSwingManager();
        
        MapContext mapContext = viewDocument.getMapContext();
        MapContextToInterval mapContextToInterval = new MapContextToInterval(mapContext);
        
        TimeSelectorPanel timeSelectorPanel = swingManager.createTimeSelectorPanel(mapContextToInterval.getInterval());
        timeSelectorPanel.setInstants(mapContextToInterval.getTimes());
        timeSelectorPanel.addObserver(new TimeSelectorObserver(viewDocument));
        
        mapContext.getLayers().addLayerCollectionListener(new MapContextListener(mapContextToInterval, timeSelectorPanel));

        WindowManager windowManager = ToolsSwingLocator.getWindowManager();
        windowManager.showWindow(timeSelectorPanel, viewDocument.getName(), WindowManager.MODE.TOOL);
     }

    public boolean isEnabled() {
        
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument viewDocument = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if( viewDocument == null ) {
            return false;
        }
        final MutableBoolean enabled = new MutableBoolean(false);
        
        MapContext mapContext = viewDocument.getMapContext();        
        try {
            mapContext.getLayers().accept(new Visitor() {                
                public void visit(Object layer) throws VisitCanceledException, BaseException {
                    if (layer instanceof SingleLayer){
                        DataStore dataStore = ((SingleLayer)layer).getDataStore();
                        if (dataStore.getInterval() != null){           
                            enabled.setValue(true);
                            throw new VisitCanceledException();
                        }
                    }                    
                }
            });
        } catch (Exception ex) {
            logger.warn("Unable to determine if there are layers with temporal information.",ex);
        }

        return enabled.getValue();
    }

    public boolean isVisible() {
        return true;
    }

}
