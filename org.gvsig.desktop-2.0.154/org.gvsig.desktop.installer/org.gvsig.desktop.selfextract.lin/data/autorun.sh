#!/bin/bash

PATH=$PATH:$PWD

if [ "$1" == "--message" ] ; then
  shift
  tput setf 1
  tput setb 7
  #tput bold
  tput clear
  echo "
  
  $@"
  echo -n "
                        "
  tput smso
  echo -n "  Aceptar  "
  tput rmso
  echo -n "
  
Pulse ENTER to continue..."
  read n
  exit 0
fi

message() {
  echo "$1"
  if type zenity >/dev/null 2>/dev/null ; then
    zenity --info --text="$1"
  else
    xterm -title "Information" -geometry 60x10+200+200 -e $0 --message "$1"
  fi
}

if [ -x "${JAVA_HOME}/bin/java" ] ; then
  JAVA="${JAVA_HOME}/bin/java"
else
  if ! type java >/dev/null ; then
    message "
  Can't install gvSIG desktop
  
  Can't locate a Java Runtime Environment (JRE)
  Install it, configure it or set JAVA_HOME and retry."
    exit 1
  fi
  JAVA="java"
fi

INSTALLER=$(echo gvSIG-desktop-*.jar)
if [ "${INSTALLER/*-x86_64-*/64}" == "64" ] ; then
  ARCH="64"
else
  ARCH="32"
fi

"$JAVA" -d${ARCH} -version >/dev/null 2>/dev/null 
if [ "$?" != "0" ] ; then
    message "
  Can't install gvSIG desktop
  
  gvSIG requires a ${ARCH}-bit JRE
  Install it, configure it or set JAVA_HOME and retry."
    exit 1
fi

exec "${JAVA}" -d${ARCH} -jar "${INSTALLER}"
