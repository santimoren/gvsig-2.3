
import WebDAVClient
import java.io.File

public class Helper {

  public ant = null

  private projectBaseFile = null
  private packageinfo = null
  private project = null
  private log = null

  public Helper(logger,project) {
    this.project = project
    log = logger
    //log.info("Helper(...)")

    ant = new AntBuilder()
  }

  def copyDirToDir(source, target) {
    //log.info("copyDirToDir "+source+","+target)
    if( source instanceof File ) {
      source = source.getAbsolutePath()
    }
    if( target instanceof File ) {
      target = target.getAbsolutePath()
    }
    if( !target.endsWith("/") ) {
      target += "/"
    }
    ant.copy( todir: target ) {
      fileset( dir:source )
    }
  }

  def moveDirToDir(source, target) {
    //log.info("moveDirToDir "+source+","+target)
    if( source instanceof File ) {
      source = source.getAbsolutePath()
    }
    if( target instanceof File ) {
      target = target.getAbsolutePath()
    }
    if( !target.endsWith("/") ) {
      target += "/"
    }
    ant.move( todir: target ) {
      fileset( dir: source )
    }
  }

  def copyFileToDir(source, target) {
    //log.info("copyFileToDir "+source+","+target)
    if( source instanceof File ) {
      source = source.getAbsolutePath()
    }
    if( target instanceof File ) {
      target = target.getAbsolutePath()
    }
    if( !target.endsWith("/") ) {
      target += "/"
    }
    ant.copy(file:source, todir:target)
  }

  def copyFileToFile(source, target) {
    //log.info("copyFileToFile "+source+","+target)
    if( source instanceof File ) {
      source = source.getAbsolutePath()
    }
    if( target instanceof File ) {
      target = target.getAbsolutePath()
    }
    ant.copy(file:source, tofile:target)
  }

  def getProjectBaseFolder(Object[] args ) {
    if( projectBaseFile == null ) {
      projectBaseFile = project.basedir
      def f = new File(projectBaseFile,"/src/main/scripts/Helper.groovy")
      if( !f.exists() ) {
        projectBaseFile = projectBaseFile.getParentFile()
      }
    }
    def f = projectBaseFile
    args.each {
      f = new File(f,it)
    }
    return f
  }

  def getProjectTargetFolder(Object[] args) {
    return getProjectBaseFolder("target", *args)
  }

  def getProductFolderPath() {
      return project.properties["gvsig.product.folder.path"]
  }

  def getInstallationFolder(Object[] args) {
    return getProjectBaseFolder("target", "product", "gvsig-desktop", *args)
  }

  def getVersionWithBuildNumber() {
      def ver = getPackageinfo().getProperty("version")
      if( ver.contains("-") ) {
	return ver
      } else {
	return ver +"-"+getPackageinfo().getProperty("buildNumber")
      }
  }

  def getVersionWithoutBuildNumber() {
      def ver = getPackageinfo().getProperty("version")
      if( ver.contains("-") ) {
	return ver.replaceFirst("-[0-9]*\$","")
      } else {
	return ver
      }
  }

  def createLauncher(source,target) {
      //log.info("Creating launcher ("+source+")...")
      source = getProjectBasePath() + source
      target = getProjectBasePath() + target
      s = new File(source).text
      s = s.replaceAll("@@gvsig.product.folder.path@@", getInstallationFolder().getAbsolutePath())
      s = s.replaceAll("@@org.gvsig.andami.updater.jar@@", getAndamiUpdaterJarName())
      s = s.replaceAll("@@version@@", getVersionWithBuildNumber().replace("-","."))
      new File(target).write(s)
      log.info("Created "+target)
      ant.exec( command:"launch4jc "+ target )
      log.info("Created launcher ("+target+").")
  }

  def getPackageinfo() {
    if( packageinfo == null ) {
      File file = getInstallationFolder("gvSIG","extensiones","org.gvsig.app.mainplugin","package.info")
      log.info("read package.info from "+file.getAbsolutePath())
      def p = new Properties()
      p.load(file.newDataInputStream())
      packageinfo = p
      project.properties["package.info.version"] = getVersionWithBuildNumber()
      project.properties["package.info.buildNumber"] = p.getProperty("buildNumber")
      project.properties["package.info.state"] = p.getProperty("state")
    }
    return packageinfo
  }

  def readPackageInfo() {
    getPackageinfo()
  }

  def getWebDAVClient(log) {
	  def user
	  def password
	  if( project.properties["username"] == null ) {
	      print("Enter user name: ");
	      user = System.console().readLine().toString()
	      if( user != "" ) {
		  project.properties.setProperty("username",user);
	      }
	  }
	  if( project.properties["password"] == null ) {
	      print("Enter password for user '" + project.properties["username"] + "': ");
	      password = System.console().readPassword().toString()
	      if( password != "" ) {
		  project.properties.setProperty("password",password);
	      }
	  }
	  def session = new WebDAVClient(log)
	  if( project.properties["username"] == null || project.properties["password"] == null ) {
	      log.info("[WEBDAV] creating non authenticated session.");
	      log.info("[WEBDAV] Use -Dusername=USER -Dpassword=PASSWORD to create a authenticated session.");
	      session.login();
	  } else {
	      session.login(project.properties["username"], project.properties["password"]);
	  }
	  return session
  }

  def getAddOnRepoURL() {
	  return "https://downloads.gvsig.org/download/gvsig-desktop-testing" +
	      "/dists/" +
	      getVersionWithoutBuildNumber() +
	      "/builds/" +
	      getPackageinfo().getProperty("buildNumber")
  }

  def getInstallerName(suffix) {
	  return "gvSIG-desktop-" +
	    getVersionWithBuildNumber() +
	    "-" +
	    getPackageinfo().getProperty("state") +
	    suffix
  }


  def download_jre() {
      jre_pkg_name = "gvSIG-desktop-2.1.0-jre_7_windows_i586-1.7.72-0-devel-win-x86-j1_7.gvspkg"
      url = "http://downloads.gvsig.org/download/gvsig-desktop/pool/jre_7_windows_i586/" + jre_pkg_name
      target = new File(getProjectTargetFolder() , jre_pkg_name).getAbsolutePath()
      jre_path = System.getenv()["jre_gvspkg_path"]
      if(! new File(target).exists() ) {
	if( jre_path!=null ) {
	  if( jre_path.endsWith(".gvspkg") ) {
	    source = jre_path
	  } else {
	    source = jre_path + "/" + jre_pkg_name
	  }
	  if (source.startsWith("~" + File.separator)) {
	      source = System.getenv().get("HOME") + source.substring(1);
	  }
	  if( ! new File(source).exists() ) {
	    log.info("jre specified in jre_path variable not found - "+source)
	    log.info("Downloading windows jre plugin")
	    ant.get(src: url, dest: target)
	  } else {
	    log.info("Copy windows jre plugin")
	    copyFileToDir(source, getProjectTargetFolder())

	  }
	} else {
	  log.info("Downloading windows jre plugin")
	  ant.get(src: url, dest: target)
	}
      } else {
	log.info("Skip download of jre, it exist in local filesystem.")
      }
  }

  def getAndamiUpdaterJarName() {
    def org_gvsig_andami_updater_jar = new FileNameFinder().getFileNames(
      getInstallationFolder("lib").getAbsolutePath(),
      'org.gvsig.andami.updater-*.jar'
    )[0]
    if( org_gvsig_andami_updater_jar == null ) {
      log.error("Can't find andami updater jar")
      assert false
    }
    org_gvsig_andami_updater_jar = new File(org_gvsig_andami_updater_jar).getName()
    return org_gvsig_andami_updater_jar
  }

  def copy_common_install_files(targetdir) {
    copyFileToDir(getProjectBaseFolder("src","main","izpack","addonmanager.html"),targetdir)
    copyFileToDir(getProjectBaseFolder("src","main","izpack","readme.html"),targetdir)
    copyFileToDir(getProjectBaseFolder("target","product","gvsig-desktop","LICENSE.txt"),targetdir)
  }

  def copy_extractpackages_jar(targetdir) {
    def org_gvsig_desktop_installer_izpack_extractpackages_jar = new FileNameFinder().getFileNames(
      getProjectBaseFolder("org.gvsig.desktop.installer.izpack.extractpackages","target").getAbsolutePath(),
      "org.gvsig.desktop.installer.izpack.extractpackages-*.jar",
      "*-sources.jar"
    )[0]

    copyFileToFile(
      org_gvsig_desktop_installer_izpack_extractpackages_jar,
      new File(targetdir, "org.gvsig.desktop.installer.izpack.extractpackages.jar")
    )

  }

  def copy_copyjre_jar(targetdir) {
    def org_gvsig_desktop_installer_izpack_copyjre_jar = new FileNameFinder().getFileNames(
      getProjectBaseFolder("org.gvsig.desktop.installer.izpack.copyjre","target").getAbsolutePath(),
      "org.gvsig.desktop.installer.izpack.copyjre-*.jar",
      "*-sources.jar"
    )[0]

    copyFileToFile(
      org_gvsig_desktop_installer_izpack_copyjre_jar,
      new File(targetdir, "org.gvsig.desktop.installer.izpack.copyjre.jar")
    )

  }

  def expand(s) {

    def t = "target/portable/gvSIG-desktop-"+this.getVersionWithoutBuildNumber()
    def install_path = new File(project.basedir, t).getAbsolutePath()
    s = s.replace("\${product.folder}",this.getInstallationFolder().getAbsolutePath())
    s = s.replace("\${INSTALL_PATH}",install_path)
    s = s.replace("\$INSTALL_PATH",install_path)
    return s
  }

  def prepare_portable() {

    def t = "target/portable/gvSIG-desktop-"+this.getVersionWithoutBuildNumber()
    def install_path = new File(project.basedir, t).getAbsoluteFile()
    install_path.mkdirs()

    def target = new File(project.basedir, "target")
    def installfile = new File(target,"install.xml")
    def install = new XmlSlurper().parse(installfile)
    install.packs.each {
      it.pack.each {
	it.file.each {
	  def src = new File(expand(it.@src.text()))
	  if( !src.isAbsolute() ) {
	    src = new File(target,src.getPath())
	  }
	  def targetdir = expand(it.@targetdir.text())
	  this.copyFileToDir(src,targetdir)
	}
	it.fileset.each {
	  def src = new File(expand(it.@dir.text()))
	  if( !src.isAbsolute() ) {
	    src = new File(target,src.getPath())
	  }
	  def targetdir = expand(it.@targetdir.text())
	  this.copyDirToDir(src,targetdir)
	}
	it.executable.each {
	  def targetfile = new File(expand(it.@targetfile.text()))
	  targetfile.setExecutable(true)
	}
      }
    }

    this.copyDirToDir(
      this.getInstallationFolder("gvSIG/extensiones/org.gvsig.coreplugin.app.mainplugin"),
      new File(install_path,"gvSIG/extensiones/org.gvsig.coreplugin.app.mainplugin")
    )
    this.copyDirToDir(
      this.getInstallationFolder("gvSIG/extensiones/org.gvsig.installer.app.mainplugin"),
      new File(install_path,"gvSIG/extensiones/org.gvsig.installer.app.mainplugin")
    )
  }

  def pack_portable() {

    def t = "target/portable/gvSIG-desktop-"+this.getVersionWithoutBuildNumber()
    def install_path = new File(project.basedir, t).getAbsoluteFile()
    def target = new File(project.basedir, "target")

    def portablezip = new File(target,this.getInstallerName(
      "-" +
      project.properties["installer_os"] +
      "-" +
      project.properties["installer_architecture"] +
      "-online.zip"
    ))

    this.ant.zip(
      destfile: portablezip,
      basedir: install_path
    )
  }

}
