
import java.security.SecureRandom;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager
import java.security.cert.X509Certificate
import java.security.cert.CertificateException

import org.apache.http.conn.ssl.SSLSocketFactory;

import com.github.sardine.impl.SardineImpl;

public class X509TrustedManager implements X509TrustManager,TrustManager {
    public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
    }

    public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
    }

    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }
}

public class SardineTrusted extends SardineImpl {

  SSLSocketFactory socketFactory = null;

  public SardineTrusted(String username, String password) {
    super(username,password);
  }

  protected SSLSocketFactory createDefaultSecureSocketFactory() {
    if( socketFactory == null ) {
      SSLContext sslContext = SSLContext.getInstance("SSL");
      def trustManagers = new X509TrustedManager[1];
      X509TrustedManager trustManager = new X509TrustedManager()
      trustManagers[0] = trustManager
      sslContext.init(null, trustManagers, new SecureRandom());
      socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }
    return socketFactory;
  }

}

public class WebDAVClient {

    def log;

    private String user;
    private String password;
    private Object sardine;

    public WebDAVClient(logger) {
    	this.log = logger;
    }

    public void login(String user, String password) {
        log.info("[WEBDAV] login as '"+user+"'.");
        this.user = user;
        this.password = password;

        this.sardine = new SardineTrusted(this.user,this.password)
        this.sardine.enablePreemptiveAuthentication("devel.gvsig.net");
        this.sardine.enablePreemptiveAuthentication("downloads.gvsig.net");
        this.sardine.enablePreemptiveAuthentication("devel.gvsig.org");
        this.sardine.enablePreemptiveAuthentication("downloads.gvsig.org");
    }

    public void login() {
        log.info("[WEBDAV] login as guest");
        this.sardine = new SardineTrusted();
    }

    public boolean exists(String url) throws Exception {
        return sardine.exists(url);
    }

    public void makedirs(String url) throws Exception {
        log.info("[WEBDAV] makedirs '"+url+"'.");
        URL u = new URL(url);
        String[] x = u.getPath().split("/");
        String path = "";
        for( int i=1; i<x.length; i++ ) {
          path = path + "/" + x[i];
          URL t = new URL(u,path);
          mkdir(t.toString());
        }
    }

    public void mkdir(String url) throws Exception {
        if( ! exists(url) ) {
            log.info("[WEBDAV] mkdir '"+url+"'.");
            sardine.createDirectory(url);
        }
    }

    public void put(String source, String target) throws Exception {
        log.info("[WEBDAV] put '" + source + "' to '" + target + "'...");
        InputStream fis = new FileInputStream(new File(source));
        sardine.put(target, fis);
        log.info("[WEBDAV] put ok.");
    }

    public List list(String url) throws Exception {
        List resources = sardine.list(url);
        return resources;
    }
}
