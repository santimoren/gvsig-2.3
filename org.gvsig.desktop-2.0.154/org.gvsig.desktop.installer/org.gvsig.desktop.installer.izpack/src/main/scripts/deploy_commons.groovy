
import Helper

def deployInstallers(helper) {

        WebDAVClient session = helper.getWebDAVClient(log);
        targetfolder = helper.getAddOnRepoURL()
        sourcefolder  = new File(project.basedir, "target").getAbsolutePath()

        session.makedirs(targetfolder);
        session.makedirs(targetfolder+"/misc");

        fname = helper.getInstallerName("-all-all-pool.zip")
        session.put(
	  sourcefolder + "/" + fname,
	  targetfolder+"/misc/" + fname
	)
        fname = helper.getInstallerName("-all-all.gvspks")
        session.put(
	  sourcefolder + "/" + fname,
	  targetfolder+"/misc/" + fname
	);
}


def main() {
    if( project.properties["deploy-installers"] != null ) {
	def helper = new Helper(log,project)
        deployInstallers(helper)
    }
}

main()

