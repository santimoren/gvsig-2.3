
import Helper

def main() {
    def helper = new Helper(log,project)

    log.info("Base path=" + helper.getProjectBaseFolder().getAbsolutePath())

    helper.getInstallationFolder().mkdirs()
    helper.getProjectTargetFolder("pool.d","pool").mkdirs()

    log.info("Getting a local copy of product folder...")
    helper.copyDirToDir(
      helper.getProductFolderPath(),
      helper.getInstallationFolder()
    )

    log.info("Populate the pool folder...")
    helper.moveDirToDir(
      helper.getInstallationFolder("install"),
      helper.getProjectTargetFolder("pool.d", "pool" )
    )
    helper.getInstallationFolder("install").mkdirs()
    helper.copyFileToDir(
      helper.getProjectTargetFolder("pool.d", "pool", "README.txt" ),
      helper.getInstallationFolder("install")
    )

    log.info("Preparing basic package set (gvspks)...")
    gvspks_folder = helper.getProjectTargetFolder("gvspks")
    gvspks_folder.mkdirs()
    filenames = new FileNameFinder().getFileNames( helper.getProjectTargetFolder("pool.d").getAbsolutePath(),"**/*.gvspkg")
    for( filename in filenames ) {
      helper.copyFileToDir(filename, gvspks_folder)
    }
    helper.copyFileToDir(
      helper.getProjectBaseFolder("src","main","config","defaultPackages"),
      gvspks_folder
    )

    log.info("Building pool zip...")
    source  = helper.getProjectTargetFolder("pool.d")
    target  = new File(helper.getProjectTargetFolder(),helper.getInstallerName("-all-all-pool.zip"))
    helper.ant.zip(destfile: target.getAbsolutePath(), basedir: source.getAbsolutePath())

    log.info("Building basic package-set (gvspks)...")
    source  = helper.getProjectTargetFolder("gvspks")
    target  = new File(helper.getProjectTargetFolder(),helper.getInstallerName("-all-all.gvspks"))
    helper.ant.zip(destfile: target.getAbsolutePath(), basedir: source.getAbsolutePath())

    log.info("Found andami updater :"+helper.getAndamiUpdaterJarName())

}

main()

