

import Helper


def deployInstallers(helper) {

  WebDAVClient session = helper.getWebDAVClient(log);
  targetfolder = helper.getAddOnRepoURL()
  sourcefolder  = new File(project.basedir, "target").getAbsolutePath()

  session.makedirs(targetfolder);

  fname = helper.getInstallerName("-"+project.properties["installer_os"]+"-"+project.properties["installer_architecture"]+"-online.jar")
  session.put(
    sourcefolder + "/" + fname,
    targetfolder+"/" + fname
  )
  fname = helper.getInstallerName("-"+project.properties["installer_os"]+"-"+project.properties["installer_architecture"]+"-online.zip")
  session.put(
    sourcefolder + "/" + fname,
    targetfolder+"/" + fname
  )
}


def main() {
  if( project.properties["deploy-installers"] != null ) {
    def helper = new Helper(log,project)

    deployInstallers(helper)
  }

}

main()

