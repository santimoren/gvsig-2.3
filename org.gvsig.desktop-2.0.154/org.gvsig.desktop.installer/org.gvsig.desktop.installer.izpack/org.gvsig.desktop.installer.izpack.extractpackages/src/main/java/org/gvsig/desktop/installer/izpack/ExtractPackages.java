 
package org.gvsig.desktop.installer.izpack;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import com.izforge.izpack.core.resource.ResourceManager;
import com.izforge.izpack.panels.process.AbstractUIProcessHandler;
 
public class ExtractPackages {

  private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
  private static final int EOF = -1;

  public boolean run(AbstractUIProcessHandler handler, String[] args) {
    ResourceManager rm = new ResourceManager();
    URL packagesurl = null;
    try {
      packagesurl = rm.getURL("/package.gvspks");
    } catch(Exception ex) {
    }
    if( packagesurl == null ) {
      handler.logOutput("package.gvspks not available in the instalation.", false);
      return true;
    }
    handler.logOutput("Extracting packages (This may take a while)...", false);

    File installation_folder = new File(args[0]);
    File install_folder = new File(installation_folder,"install");
    File packages = new File(install_folder,"package.gvspks");
    
    try {
      install_folder.mkdir();
      InputStream packages_fis = rm.getInputStream("/package.gvspks");
      OutputStream packages_fos = new FileOutputStream(packages);
      copy(packages_fis, packages_fos, new byte[DEFAULT_BUFFER_SIZE]);
  
    } catch(Exception ex) {
      handler.logOutput("Can't extract packages: " + ex.toString(), false);
      return false;
    }
    
    handler.logOutput("Packages extracted.", false);
    return true;
  }
  
  public long copy(final InputStream input, final OutputStream output, final byte[] buffer)
	  throws IOException {
      long count = 0;
      int n = 0;
      while (EOF != (n = input.read(buffer))) {
	  output.write(buffer, 0, n);
	  count += n;
      }
      return count;
  }
  
}
