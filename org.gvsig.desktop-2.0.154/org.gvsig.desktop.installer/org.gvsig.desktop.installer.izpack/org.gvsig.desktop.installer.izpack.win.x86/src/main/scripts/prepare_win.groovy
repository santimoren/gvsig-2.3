
import Helper

def main() {
    def helper = new Helper(log,project)

    log.info("Base path=" + helper.getProjectBaseFolder().getAbsolutePath())

    helper.readPackageInfo()

    target = new File(project.basedir, "target")

    log.info("Copy common install files")
    helper.copy_common_install_files(target)

    log.info("Copy org.gvsig.desktop.installer.izpack.extractpackages.jar")
    helper.copy_extractpackages_jar(target)

    log.info("Copy org.gvsig.desktop.installer.izpack.copyjre.jar")
    helper.copy_copyjre_jar(target)

    log.info("Download busybox.exe")
    helper.ant.get(
      src: "http://downloads.gvsig.org/download/gvsig-desktop/runtimes/winutils/x86/busybox.exe",
      dest: new File(target,"busybox.exe").getAbsolutePath()
    )

    log.info("Download cygcheck.exe ")
    helper.ant.get(
      src: "http://downloads.gvsig.org/download/gvsig-desktop/runtimes/winutils/x86/cygcheck.exe",
      dest: new File(target,"cygcheck.exe").getAbsolutePath()
    )
    helper.prepare_portable()
    helper.pack_portable()

}

main()

