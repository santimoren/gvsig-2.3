
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "libtar.h"
#include <errno.h>
#include <dirent.h>


int remove_directory(const char *path) {
  DIR *d = opendir(path);
  size_t path_len = strlen(path);
  int r = -1;

  if (d != NULL ) {
    struct dirent *p;
    r = 0;
    while (!r && (p=readdir(d))) {
      int r2 = -1;
      char *buf;
      size_t len;

      /* Skip the names "." and ".." as we don't want to recurse on them. */
      if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
          continue;
      }
      len = path_len + strlen(p->d_name) + 2;
      buf = malloc(len);
      if (buf) {
        struct stat statbuf;
        snprintf(buf, len, "%s/%s", path, p->d_name);
        if (!stat(buf, &statbuf)) {
          if (S_ISDIR(statbuf.st_mode)) {
              r2 = remove_directory(buf);
          } else {
              r2 = unlink(buf);
          }
        }
        free(buf);
      }
      r = r2;
    }
    closedir(d);
  }
  if (!r) {
    r = rmdir(path);
  }
  return r;
}


char *getMark() {
  static char buffer[100];

  strcpy(buffer, ";!@self");
  strcat(buffer,"extract@!\n");
  return buffer;
}

int findMark(int fd) {
  static char buffer[1024*50];

  int x=lseek(fd,0,SEEK_SET);
  if( x<0 ) {
    printf("seek 0 error\n");
    return -1;
  }
  x=read(fd,buffer,1024*50);
  if( x<0 ) {
    printf("read error\n");
    return -1;
  }
  char *mark = getMark();
  int markSize = strlen(mark);
  char *p;
  for( p=buffer ; p<buffer+x; p++ ) {
    if( strncmp(p,mark,markSize)==0 ) {
      int n = p-buffer + markSize;
      printf("found %d\n", n);
      return n;
    }
  }
  printf("not found\n");
  return -1;
}

void extract(char *source) {
  static char path[1024];
  static char folder[1024];
  TAR *tar;
  int start,x;


  x = tar_open(&tar, source, 0, O_RDONLY, 0, 0);
  if( x <0 ) {
    fprintf(stderr,"Can 't extract from '%s', don't create tar object.\n", source);
    perror(NULL);
    exit(1);
  }

  int fdi = tar_fd(tar);
  start = findMark(fdi);
  if( start <0 ) {
    fprintf(stderr,"Can 't extract from '%s', don't find position of tar start.\n", source);
    perror(NULL);
    exit(3);
  }
  x = lseek(fdi,start,SEEK_SET);
  if( x <0 ) {
    fprintf(stderr,"Can 't extract from '%s', don't seek at position of tar start.\n", source);
    perror(NULL);
    exit(4);
  }

  sprintf(folder,"/tmp/selfextract-%d", getpid());
  printf("Extracting to %s...\n", folder);
  x = tar_extract_all(tar, folder);
  if( x <0 ) {
    fprintf(stderr,"Can 't extract from '%s', output folder '%s'.\n", source, folder);
    perror(NULL);
    exit(5);
  }
  tar_close(tar);

  sprintf(path,"%s/autorun.sh", folder);
  if( access(path,R_OK)<0 ) {
    fprintf(stderr,"Can 't extract from '%s', don't access to '%s'.\n", source, path);
    exit(6);
  }
  if( chdir(folder)<0 ) {
    fprintf(stderr,"Can 't extract from '%s', don't change current folder to '%s'.\n", source, folder);
    exit(7);
  }
  printf("Executing autorun.sh...\n");
  x = system(path);
  if( x <0 ) {
    fprintf(stderr,"Can 't extract from '%s', execute '%s' fail.\n", source, path);
    perror(NULL);
    exit(8);
  }
  printf("Removing %s...\n", folder);
  remove_directory(folder);
  printf("Finish\n");
  exit(0);
}

void create(char *source,char *target, char *tarfile) {
  static char buffer[1024];
  int size;
  int n,x;

  int fdo = creat(target,S_IWUSR|S_IXUSR|S_IRUSR|S_IRGRP|S_IWGRP|S_IXGRP|S_IROTH|S_IXOTH);
  if( fdo <0 ) {
    fprintf(stderr,"Can't create '%s'.\n", target);
    perror(NULL);
    exit(20);
  }
  int fdi = open(source,O_RDONLY);
  if( fdo <0 ) {
    fprintf(stderr,"Can't create '%s', don't open header file '%s'.\n", target, source);
    perror(NULL);
    exit(21);
  }

  while( (n=read(fdi,buffer,1024)) != 0 ) {
    if( n<0 ) {
      fprintf(stderr,"Can't create '%s', don't read file '%s'.\n", target, source);
      perror(NULL);
      exit(24);
    }
    x=write(fdo,buffer,n);
    if( x!=n ) {
      fprintf(stderr,"Can't create '%s', don't add '%s' to end of file.\n", target, source);
      perror(NULL);
      exit(25);
    }
  }
  close(fdi);

  char *mark = getMark();
  x=write(fdo,mark,strlen(mark));
  if( x!=strlen(mark) ) {
    fprintf(stderr,"Can't create '%s', magic to end of file.\n", target);
    perror(NULL);
    exit(26);
  }

  fdi = open(tarfile,O_RDONLY);
  if( fdi<0 ) {
    fprintf(stderr,"Can't create '%s', don't open file '%s'.\n", target, tarfile);
    perror(NULL);
    exit(27);
  }
  while( (n=read(fdi,buffer,1024)) != 0 ) {
    if( n<0 ) {
      fprintf(stderr,"Can't create '%s', don't read file '%s'.\n", target, tarfile);
      perror(NULL);
      exit(28);
    }
    x=write(fdo,buffer,n);
    if( x!=n ) {
      fprintf(stderr,"Can't create '%s', don't add '%s' to end of file.\n", target, tarfile);
      perror(NULL);
      exit(29);
    }
  }
  close(fdi);

  close(fdo);
  exit(0);
}

int main(int argc, char*argv[]) {

  if( argc==2 && ( strcmp(argv[1], "--help")==0 || strcmp(argv[1], "--h")==0 || strcmp(argv[1], "-?")==0 ) ) {
    fprintf(stderr,"Usage:\n%s --create target tarfile\n    Create a selfextracting target file with the tarfile indicated.\n%s --help|-h|-?\n    Show this usage\n%s\n    Extract in /tmp/selfextract-PID the tar file and execute the autorun.sh file\n", argv[0], argv[0], argv[0]);
    exit(-1);
  }
  if( argc==4 && strcmp(argv[1], "--create")==0 ) {
    create(argv[0], argv[2],argv[3]);
    return 0;
  }
  extract(argv[0]);
}

