proc main { } {
        global argc
        global argv

        if {$argc < 1} {
                help
                return 1
        }
        if {$::installkit::wrapped} {
                puts "Script embebido en el binario"
                # Cuando se da esta condicion he palmado el argv de 0,
                # y no se de donde sacarlo., con lo que se perdio el command
                puts "argv: $argv"
                puts "tcl_argv: $::tcl_argv"
        }

        set cmd [lindex $argv 0]
        # puts "command: $cmd"
        #puts "argv: $argv"
        #puts "tcl_argv: $::tcl_argv"
        switch $cmd {
                "help" {
                        help
                }
                "ls" {
                        set install_file  [ lindex $argv 1 ]
                        ls $install_file
                }
                "addmain" {
                        set install_file  [ lindex $argv 1 ]
                        set main_file  [ lindex $argv 2 ]
                        addmain $install_file $main_file
                }
                "addpks" {
                        set install_file  [ lindex $argv 1 ]
                        set package_set  [ lindex $argv 2 ]
                        addpks $install_file $package_set
                }
                "addjrelin" {
                        set install_file  [ lindex $argv 1 ]
                        set jre_installer  [ lindex $argv 2 ]
                        addjrelin $install_file $jre_installer
                }
                "addjrewin" {
                        set install_file  [ lindex $argv 1 ]
                        set jre_installer  [ lindex $argv 2 ]
                        addjrewin $install_file $jre_installer
                }
                "extractpks" {
                        set install_file  [ lindex $argv 1 ]
                        set folder  [ lindex $argv 2 ]
                        extractpks $install_file $folder
                }
                "extractjrelin" {
                        set install_file  [ lindex $argv 1 ]
                        set folder  [ lindex $argv 2 ]
                        extractjrelin $install_file $folder
                }
               "extractjrewin" {
                        set install_file  [ lindex $argv 1 ]
                        set folder  [ lindex $argv 2 ]
                        extractjrewin $install_file $folder
                }
                default {
                        puts "unknow command $cmd"
                        help
                }
        }
}


proc help {} {
        global argv0
        puts "Usage: [ file tail $argv0 ] command args"
        puts "Command are:"
        ls_help
        addpks_help
        addmain_help
        addjrelin_help
        addjrewin_help
        extractpks_help
        extractjrelin_help
        extractjrewin_help
}

proc addmain_help {} {
        puts ""
        puts "addmain installkit main-file"
        puts "  Add the main file to the installkit"
        puts "  If main.tcl already in installkit it is replaced"
}

proc addmain { installkit_file main_file } {
	addfile $install_file $main_file "main.tcl" "Main"
}

proc addpks_help {} {
        puts ""
        puts "addpks gvsig-install-program package-set"
        puts "  Add the specified package set to the installation program"
        puts "  If package.gvspks was already previously added, it will be replaced"
}

proc addpks { install_file package_set } {
	addfile $install_file $package_set "package.gvspks" "Package set"
}

proc addjrelin_help {} {
        puts ""
        puts "addjrelin gvsig-install-program jre-installer"
        puts "  Add the specified jre linux installer to the installation program"
        puts "  If the jre installer was already previously added, it will be replaced"
}

proc addjrelin { install_file jre_installer } {
	addfile $install_file $jre_installer "jre-installer-lin.bin" "JRE installer"
}

proc addjrewin_help {} {
        puts ""
        puts "addjrewin gvsig-install-program jre-installer"
        puts "  Add the specified jre windows installer to the installation program"
        puts "  If the jre installer was already previously added, it will be replaced"
}

proc addjrewin { install_file jre_installer } {
	addfile $install_file $jre_installer "jre-installer-win.exe" "JRE installer"
}

proc addfile { install_file file_to_add file_name file_description } {
        if { [file writable $install_file] != 1 } {
                puts "Write access unavailable to the file $install_file"
                return
        }
        if { [file isfile $file_to_add] !=1 } {
                puts "The file to add is not a file: $file_to_add"
                return
        }
        set fp [ ::miniarc::open crap "$install_file" a ]
        ::miniarc::addfile $fp "$file_to_add"  -name "$file_name"
        ::miniarc::close $fp
        puts "$file_description added to installation program."
}

proc ls_help {} {
        puts ""
        puts "ls gvsig-install-program "
        puts "  list the files attached to the installkit or installation program especified"
}

proc ls { install_file } {
        if { [file isfile $install_file] != 1 } {
                puts "Can't access to file $install_file"
                return
        }
        set root /installkitunpackvfs
        crapvfs::mount $install_file $root

        set dirs [glob -nocomplain -type d $root/*]
        if { [llength $dirs] > 0 } {
            puts "Directories:"
            foreach d [lsort $dirs] {
                puts "    $d"
            }
        } else {
            puts "(no subdirectories)"
        }

        set files [glob -nocomplain -type f $root/*]
        if { [llength $files] > 0 } {
            puts "Files:"
            foreach f [lsort $files] {
                puts "    [file size $f] - $f"
            }
        } else {
            puts "(no files)"
        }

        crapvfs::unmount $root
}


proc extractpks_help {} {
        puts ""
        puts "extractpks gvsig-install-program folder"
        puts "  Extract the package set from the installation program to the especified folder"
        puts "  If the installation program has not a package set do nothing."
}

proc extractpks { install_file folder } {
        if { [file isfile $install_file] != 1 } {
                puts "Can't access to file $install_file"
                return
        }
        if { [file isdirectory $folder] !=1 } {
                puts "Can't access to folder $folder"
                return
        }
        set root /installkitunpackvfs
        set already_mounted true
        if { [file isdirectory $root] !=1 } {
                crapvfs::mount $install_file $root
                set already_mounted false
        }

        if { [file isfile "$root/package.gvspks"] == 1 } {
                file copy "$root/package.gvspks" "$folder/package.gvspks"
        }

        if { $already_mounted == false } {
                crapvfs::unmount $root
        }
        puts "Package set extracted to folder $folder"
}

proc extractjrelin_help {} {
        puts ""
        puts "extractjrelin gvsig-install-program folder"
        puts "  Extract the jre linux installer from the installation program to the especified folder"
        puts "  If the installation program has not a jre installer do nothing."
}

proc extractjrelin { install_file folder } {
        if { [file isfile $install_file] != 1 } {
                puts "Can't access to file $install_file"
                return
        }
        if { [file isdirectory $folder] !=1 } {
                puts "Can't access to folder $folder"
                return
        }
        set root /installkitunpackvfs
        set already_mounted true
        if { [file isdirectory $root] !=1 } {
                crapvfs::mount $install_file $root
                set already_mounted false
        }

        if { [file isfile "$root/jre-installer-lin.bin"] == 1 } {
                file copy "$root/jre-installer-lin.bin" "$folder/jre-installer-lin.bin"
                puts "JRE installer extracted to folder $folder"
        } else {
            puts "JRE installer (jre-installer-lin.bin) not available to be extracted"
        }

        if { $already_mounted == false } {
                crapvfs::unmount $root
        }
}

proc extractjrewin_help {} {
        puts ""
        puts "extractjrewin gvsig-install-program folder"
        puts "  Extract the jre windows installer from the installation program to the especified folder"
        puts "  If the installation program has not a jre installer do nothing."
}

proc extractjrewin { install_file folder } {
        if { [file isfile $install_file] != 1 } {
                puts "Can't access to file $install_file"
                return
        }
        if { [file isdirectory $folder] !=1 } {
                puts "Can't access to folder $folder"
                return
        }
        set root /installkitunpackvfs
        set already_mounted true
        if { [file isdirectory $root] !=1 } {
                crapvfs::mount $install_file $root
                set already_mounted false
        }

        if { [file isfile "$root/jre-installer-win.exe"] == 1 } {
                file copy "$root/jre-installer-win.exe" "$folder/jre-installer-win.exe"
            puts "JRE installer extracted to folder $folder"
        } else {
            puts "JRE installer (jre-installer-win.exe) not available to be extracted"
        }

        if { $already_mounted == false } {
                crapvfs::unmount $root
        }
}

main
