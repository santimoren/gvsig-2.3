ApplicationReadmeText
"<%AppName%> Readme"

Back
"Back"

Browse
"Browse..."

BuildFileText
"Building <%Tail <%FileBeingInstalled%>%>..."

BuildUninstallText
"Building uninstall..."

Cancel
"Cancel"

CleanupInstallText
"Cleaning up install..."

Close
"Close"

ConsolePauseQuitText
"-- Press space to continue or 'q' to quit --"

ConsolePauseText
"-- Press space to continue --"

ConsoleSelectDestinationText
"Where do you want to install <%AppName%>?"

CreateDesktopShortcutText
"Create Desktop Shortcut"

CreateQuickLaunchShortcutText
"Create Quick Launch Shortcut"

CustomInstallDescription
"Select custom components for installation."

DirectoryNameErrorText
"A directory name cannot contain any of the following characters:
        \\ / : * ? \" < > |"

DirectoryNotEmptyText
"<%Dir%> is not empty."

DirectoryPermissionText
"You do not have permission to write to <%Dir%>."

DisplayName
"<%AppName%>"

DownloadingFilesText
"Downloading files..."

ErrorTitle
"Install Error"

ExitText
"Are you sure you want to exit this setup?"

ExitTitle
"Exit Setup"

FileBeingInstalledText
"Copying <%FileBeingInstalled%>"

FileBeingUninstalledText
"Removing <%FileBeingUninstalled%>"

FileOverwriteText
"The file <%FileBeingInstalled%> already exists.  Do you want to overwrite it?"

Finish
"Finish"

GroupBeingInstalledText
"Installing <%GroupBeingInstalled%>..."

GroupBeingUninstalledText
"Removing <%GroupBeingUninstalled%>"

Help
"Help"

InsertDiscText
"Insert the disc labeled <%RequiredDiscName%> and then click OK."

InstallApplicationText
"Install <%AppName%>"

InstallationCompleteText
"Installation complete."

InstallCompleteText
"File installation complete..."

InstallingApplicationText
"Installing <%AppName%>..."

InstallErrorText
"An error has occurred during installation."

InstallPrepareText
"Preparing to install..."

InstallStartupText
"This will install <%AppName%> on your computer.  Continue?"

InstallTitleText
"<%AppName%> Setup"

LaunchApplicationText
"Launch <%AppName%>"

Next
"Next"

No
"No"

OK
"OK"

PasswordIncorrectText
"Password is incorrect."

ProgramFilesDescription
"Common Program Files"

PromptForAdministratorText
"This program requires administrator privileges.  Please enter your password."

PromptForDirectoryMessage
"Please choose a directory, then select OK."

PromptForDirectoryNewFolderText
"Make New Folder"

PromptForDirectoryTitle
"Browse for Folder"

PromptForRootText
"Please enter your root password."

RequireAdministratorText
"This program requires administrator privileges to install."

RequireRootText
"You must be root to run this installer."

RequireRootTitleText
"Root Required"

RequireRootUninstallText
"You must be root to uninstall this application."

SelectLanguageText
"Please select the installation language"

SeparatorText
"<%AppName%> <%Version%>"

String.database.entries
"database entries"

String.directories
"directories"

String.environment.variables
"environment variables"

String.files
"files"

String.package.entries
"package entries"

String.registry.entries
"registry entries"

String.shortcuts
"shortcuts"

TypicalInstallDescription
"A typical installation."

UninstallApplicationText
"Uninstall <%AppName%>"

UninstallPrepareText
"Preparing to uninstall..."

UpdateRegistryText
"Updating Windows Registry..."

UninstallingApplicationText
"Uninstalling <%AppName%>..."

UninstallCompleteText
"Uninstall complete."

UninstallStartupText
"This will completely remove <%AppName%> from your system.  Are you sure\
 you want to do this?"

UninstallLeftoverText
"Some components could not be removed.  Do you wish to delete them anyway\
 (not recommended)?"

UninstallPrepareText
"Preparing to uninstall..."

UninstallTitleText
"Uninstall <%AppName%>"

UnpackingFilesText
"Unpacking files..."

VersionHelpText
"This program will install <%AppName%> version <%Version%>."

ViewReadmeText
"View Readme"

Yes
"Yes"

ConsolePause {
    Message
    "<%ConsolePauseText%>"
}

LocateJavaRuntime {
    PromptMessage
    "Please tell us where your Java Runtime is located"

    StatusMessage
    "Locating Java Runtime..."
}

TextWindow {
    Title
    "Window Title"

    Caption
    "Caption"

    Message
    "Message goes here."

    CloseButton
    "<%Close%>"
}

UnzipFile {
    StatusText
    "<%FileBeingInstalledText%>"
}
ChooseComponents {
    Title
    "Select Components"

    Subtitle
    "Select the components setup will install."

    Caption
    "Select the components you want to install and deselect the components\
     you do not want to install."

    Message
    "<%SpaceRequiredText%> of space required"

    DescriptionLabel
    "Description"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

CopyFiles {
    Title
    "Installing"

    Subtitle
    "Installing <%AppName%>"

    Caption
    "Please wait while Setup installs <%AppName%> on your computer."

    Message
    ""

    FileLabel
    "<%Status%>"

    ProgressValue
    "<%InstallPercentComplete%>"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

CustomBlankPane1 {
    Caption
    "Caption goes here."

    Message
    "Message goes here."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

CustomBlankPane2 {
    Title
    "Title"

    Subtitle
    "Subtitle"

    Caption
    "Caption"

    Message
    "Message"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

CustomTextPane1 {
    Title
    "Title"

    Subtitle
    "Subtitle"

    Caption
    "Caption"

    Message
    "Message"

    Text
    "Put your custom text here."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

License {
    Title
    "License Agreement"

    Subtitle
    "Please read the following license agreement carefully."

    Text
    "Put your license agreement here."

    AcceptRadiobutton
    "I accept the terms of the license agreement."

    DeclineRadiobutton
    "I do not accept the terms of the license agreement."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

Readme {
    Title
    "Readme Information"

    Subtitle
    "Readme Information for <%AppName%>"

    Caption
    ""

    Message
    ""

    Text
    "Put your readme information here."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

SelectDestination {
    Title
    "Choose Destination Location"

    Subtitle
    "Where should <%AppName%> be installed?"

    Caption
    "Setup will install <%AppName%> in the following folder.\n\nTo install\
     to this folder, click Next.  To install to a different folder, click\
     Browse and select another folder."

    Message
    ""

    BrowseText
    "To continue, click Next.  If you would like to select a different\
     folder, click Browse."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"

    BrowseButton
    "<%Browse%>"

    DestinationLabel
    "Destination Folder"
}

SelectProgramFolder {
    Title
    "Select Program Folder"

    Subtitle
    "Please select a program folder."

    Caption
    "Setup will add the program icons to the Program Folder listed below. \
     You may type a new folder name or select from the existing folders list. \
     Click Next to continue."

    Message
    ""

    ProgramFolderLabel
    "Program Folder:"

    FolderListLabel
    "Exising Folders:"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"

    AllUsersCheckbutton
    "Create program folder for all users"
}

SetupComplete {
    Caption
    "InstallJammer Wizard Complete"

    Message
    "The InstallJammer Wizard has successfully installed <%AppName%>. \
     Click Finish to exit the wizard."

    NextButton
    "<%Finish%>"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

SetupType {
    Title
    "Setup Type"

    Subtitle
    "Select the setup type that best suits your needs."

    Caption
    "Click the type of Setup you prefer."

    Message
    ""

    DescriptionLabel
    "Description"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

StartCopyingFiles {
    Title
    "Start Copying Files"

    Subtitle
    "Review settings before copying files"

    Caption
    "Setup has enough information to start copying the program files.  If\
     you want to review or change any settings, click Back.  If you are\
     satisfied with the settings, click Next to begin copying files."

    Message
    ""

    Text
    "Install Directory:
            <%InstallDir%>

    Setup Type:
            <%InstallType%>"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

Uninstall {
    Title
    "Uninstalling"

    Subtitle
    "Uninstalling <%AppName%>"

    Caption
    "<%GroupBeingUninstalledText%>"

    Message
    ""

    FileValue
    "<%Status%>"

    ProgressValue
    "<%UninstallPercentComplete%>"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

UninstallComplete {
    Caption
    "Uninstall Complete"

    Message
    "<%AppName%> has been removed from your system."

    NextButton
    "<%Finish%>"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

UninstallDetails {
    Title
    "Uninstall Details"

    Subtitle
    "Errors occurred during removal"

    Caption
    "The following errors occurred during removal:"

    Message
    ""

    Text
    "<%Errors%>"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}


UserInformation {
    Title
    "Customer Information"

    Subtitle
    "Please enter your information."

    Caption
    "Please enter your name and the name of the company for which you work."

    Message
    ""

    UserNameLabel
    "User Name:"

    CompanyLabel
    "Company:"

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}


Welcome {
    Caption
    "Welcome to the InstallJammer Wizard for <%AppName%>"

    Message
    "This will install <%AppName%> version <%Version%> on your computer. \
    \n\nIt is recommended that you close all other applications before\
    continuing.\n\nClick Next to continue or Cancel to exit Setup."

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

InstallPassword {
    Title
    "Install Password"

    Subtitle
    "This installation requires a password to continue."

    Caption
    "Please enter a password."

    Message
    ""

    NextButton
    "<%Next%> >"

    BackButton
    "< <%Back%>"

    CancelButton
    "<%Cancel%>"
}

54A9E67A-F932-CDD4-6F9A-8842AAE31ACC,Message
"Click Next to open the gvSIG add-ons installer"

54A9E67A-F932-CDD4-6F9A-8842AAE31ACC,Text
"Now you have installed a basic gvSIG version.\n\nNext you will be able to install some additional add-ons to add or improve the available gvSIG functionalities.\n\nThose add-ons installation will be able to be made from three different sources:\n\n- Standard installation: select this option if the downloaded gvSIG installable file contains add-ons to install.\n\n- Installation from file: if you have also downloaded installation add-ons or add-on sets (files with extension .gvspkg or .gvspks).\n\n- Installation from URL: select this option if you are connected to Internet. The list of all currently available gvSIG add-ons will be downloaded from the link which appears in this option."

54A9E67A-F932-CDD4-6F9A-8842AAE31ACC,Caption
"Information"

54A9E67A-F932-CDD4-6F9A-8842AAE31ACC,Title
"Add-ons installer"

54A9E67A-F932-CDD4-6F9A-8842AAE31ACC,Subtitle
""

6CF34479-3460-FEC4-2ABA-2DA49D92D2C5,Caption
{Please, select a Java installation to use:}

6CF34479-3460-FEC4-2ABA-2DA49D92D2C5,Message
{}

6CF34479-3460-FEC4-2ABA-2DA49D92D2C5,Subtitle
{gvSIG needs a valid Java installation version 1.5 or newer to run}

6CF34479-3460-FEC4-2ABA-2DA49D92D2C5,Title
{Java installation}

7EAAED24-06DF-C67A-3AAE-8AC0DACAFF60,Caption
{Please wait while Setup downloads a Java Runtime suitable for your computer}

7EAAED24-06DF-C67A-3AAE-8AC0DACAFF60,Message
{}

7EAAED24-06DF-C67A-3AAE-8AC0DACAFF60,Subtitle
{Downloading JRE}

7EAAED24-06DF-C67A-3AAE-8AC0DACAFF60,Title
{Installing}

4BC275F9-FDA8-3886-3418-DF6EADACC78E,Caption
{Please wait while Setup downloads a Java Runtime suitable for your computer}

4BC275F9-FDA8-3886-3418-DF6EADACC78E,Message
{}

4BC275F9-FDA8-3886-3418-DF6EADACC78E,Subtitle
{Downloading JRE}

4BC275F9-FDA8-3886-3418-DF6EADACC78E,Title
{Installing}

BCA2EBA3-8663-9B43-6783-EB398AADE2F5,Caption
{ }

BCA2EBA3-8663-9B43-6783-EB398AADE2F5,Message
{Wait until the JRE installation process finishes}

BCA2EBA3-8663-9B43-6783-EB398AADE2F5,Subtitle
{ }

BCA2EBA3-8663-9B43-6783-EB398AADE2F5,Text
{ }

BCA2EBA3-8663-9B43-6783-EB398AADE2F5,Title
{Installation of a new Java Runtime Environment}

A8D2C5F5-091A-A2C4-3D33-C1DB3B4B7221,Message
{Please wait until the JRE installation process finishes}

A8D2C5F5-091A-A2C4-3D33-C1DB3B4B7221,Subtitle
{ }

A8D2C5F5-091A-A2C4-3D33-C1DB3B4B7221,Text
{Running Java Runtime Environment installer...}

A8D2C5F5-091A-A2C4-3D33-C1DB3B4B7221,Title
{Installation of a new Java Runtime Environment}

