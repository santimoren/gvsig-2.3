gvSIG Desktop v2.2.0.

 - (c) gvSIG Association
 - Software under GNU/GPL license (http://www.fsf.org/licensing/licenses/gpl.html)
 - Contact: info@gvsig.com


Application Requirements:

 System
  - At least: Intel/AMD - 512 MB RAM.
  - Operative Systems: platforms Windows and Linux. Tested in Windows XP, Linux Ubuntu/Debian.

 Installed software (available at http://www.oracle.com)
  - Java VM 1.6.0 or higher (Java 1.7.0 in Windows 8).

Further information and support:

 - Official web site and Professional support: http://www.gvsig.com

 - Mailing lists: http://www.gvsig.com/en/community/mailing-lists
