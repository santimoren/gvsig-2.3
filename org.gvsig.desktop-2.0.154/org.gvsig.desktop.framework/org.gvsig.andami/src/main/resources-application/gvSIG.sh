#!/bin/bash
#
# gvSIG. Desktop Geographic Information System.
#
# Copyright (C) 2007-2013 gvSIG Association.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.
#
# For any additional information, do not hesitate to contact us
# at info AT gvsig.com, or visit our website www.gvsig.com.
#

# gvSIG.sh

#
# Notes:
# - find command in busybox (win32) don't support "-exec"
# - trap command in busybox (win32) don't support "trap comando ERR"
# - busybox (win32) don't support statement "local"
# - Careful with loop "while", changes in variables within the loop are
#   lost when you leave this.  Use "for" loops to avoid this.
# - busybox (win32) don't support typeset
#

#set -x
set -e

onerror() {
  set +x
  echo "Error executing the command:"
  echo ">>> $BASH_COMMAND <<<"
  echo "At line $BASH_LINENO"
  exit 1
}
#trap onerror ERR

####################
# Identify OS and Architecture  #
####################

UNAME=$(uname -p -o | tr '[:upper:]' '[:lower:]')
case "$UNAME" in
*darwin*)
    OS="darwin"
    ARG_D32=""
    CPSEP=":"
    ;;
*win*)
    OS="win"
    ARG_D32=""
    CPSEP=";"
    ;;
*lin*)
    OS="lin"
    # Only linux support -d32 arg
    ARG_D32="-d32"
    CPSEP=":"
    ;;
*)
    OS="unknown"
    ARG_D32=""
    CPSEP=":"
    ;;
esac
case "$UNAME" in
*x86_64*)
    ARCH="x86_64"
    ;;
*amd64*)
    ARCH="x86_64"
    ;;
*)
    ARCH="x86"
    ;;
esac

if [ "${OS}" == "win" ] ; then
  xmkdir() {
    if [ "$1" == "-p" ] ; then
      if [ "${2:1:2}" == ":/" ] ; then
	cd "${2:0:3}"
	if mkdir -p "${2:3}" ; then
	  cd - >/dev/null
	else
	  cd - >/dev/null
	  false
	fi
      else
	mkdir -p "$2"
      fi
    else
      mkdir "$1"
    fi
  }
else
  xmkdir() {
    if [ "$1" == "-p" ] ; then
      mkdir -p "$2"
    else
      mkdir "$1"
    fi
  }
fi

########################

getJavaArchitecture() {
	if [ "$ARCH" = "x86" ] ; then
                # Si la arquitectura del SO es x86 decimos
                # que la de java es x86
		echo "x86"
	else
                # si la arquitectura es x86_64, comprobamos
                # la arquitectura de la java que nos pasan
		if "$1" -version 2>&1 | egrep -i "64-Bit" >/dev/null ; then
			echo "x86_64"
		else
			echo "x86"
		fi
	fi
}

#################################
# Set install and home folders  #
#################################

# Go into the gvSIG installation folder, just in case
x=$(dirname "$0")
cd "$x"

if [ "$GVSIG_APPLICATION_NAME" = "" ] ; then
  GVSIG_APPLICATION_NAME=gvSIG
fi
export GVSIG_APPLICATION_NAME

# gvSIG installation folder
export GVSIG_INSTALL_FOLDER="$PWD"

# gvSIG home folder
if [ "$OS" = "win" ] ; then
  # https://redmine.gvsig.net/redmine/issues/4033
  export GVSIG_HOME_FOLDER="$USERPROFILE/$GVSIG_APPLICATION_NAME"
else
  export GVSIG_HOME_FOLDER="$HOME/$GVSIG_APPLICATION_NAME"
fi

####################
# Load config file #
####################

if [ -f "$GVSIG_INSTALL_FOLDER/gvSIG.config" ] ; then
  . "$GVSIG_INSTALL_FOLDER/gvSIG.config"
fi
if [ -f "$GVSIG_HOME_FOLDER/gvSIG.config" ] ; then
  . "$GVSIG_HOME_FOLDER/gvSIG.config"
fi

if [ ! -d "$GVSIG_HOME_FOLDER" ] ; then
  xmkdir -p "$GVSIG_HOME_FOLDER"
fi

#################
# Logger config #
#################

LOGGER_FILE="$GVSIG_HOME_FOLDER/${GVSIG_APPLICATION_NAME}-launcher.log"
echo > "$LOGGER_FILE"


logger () {
  # level (INFO/WARNIG) message
  LOGGER_LEVEL="$1"
  shift
  echo "$LOGGER_LEVEL launch $@"
  echo "$LOGGER_LEVEL launch $@" >> "$LOGGER_FILE"
}

logger_info () {
  logger "INFO" "$@"
}

logger_warn () {
  logger "WARNING" "$@"
}

logger_cmd() {
  logger_info "$@"
  eval $@ 2>&1 | tee -a  "$LOGGER_FILE"
}

logger_info "gvSIG Installation folder: $GVSIG_INSTALL_FOLDER"
logger_info "gvSIG home folder: $GVSIG_HOME_FOLDER"

##########################################################
# Search in gvSIG/extensiones the architecture of plugins
# installeds and set as PREFERED_ARCHITECTURE
# Note: Busybox in windows don't support -exec in find
##########################################################

setPreferedArchitecture() {
  pf_foundArch=""
  pf_fname=""
  cd "$GVSIG_INSTALL_FOLDER"
  find . -name package.info | while read pf_fname
  do
    cat "$pf_fname"
  done | grep "architecture=[^a]" | sort | head -n 1 | read pf_foundArch || true
  # El comando "read x" de la linea anterior puede fallar si
  # no hay ningun plugin depdendiente de la arquitectura, por eso
  # el "|| true" del final.
  if [ "X${pf_foundArch}X" != "XX" ] ; then
	  eval $pf_foundArch
	  PREFERED_ARCHITECTURE="$architecture"
  else
	  PREFERED_ARCHITECTURE="$ARCH"
  fi
  cd - >/dev/null
}

setPreferedArchitecture

##########################################################

export JAVA_HOME
export JAVA
export FORCE_JAVA_32BITS

FORCE_JAVA_32BITS=""
JAVA_HOME_OTHER_ARCHITECTURE=""

# PRIVATE JAVA HOMEs
if [ -d "$GVSIG_INSTALL_FOLDER/gvSIG/extensiones/jre" ] ; then
    PJH1="$GVSIG_INSTALL_FOLDER/gvSIG/extensiones/jre"
else
    PJH1=
fi
#
# El ls|head casca en win10 64 bits. Parece que hay algun problema con los
# pipes y el find y el ls.
if [ "${OS}" == "win" ] ; then
  PJH2=""
  PJH3=""
  PJH4=""
  PJH5=""
else
  PJH2=$(ls -dt "$GVSIG_HOME_FOLDER/jre/"*1.8* 2>/dev/null | head -1)
  PJH3=$(ls -dt "$GVSIG_HOME_FOLDER/jre/"*1.7* 2>/dev/null | head -1)
  PJH4=$(ls -dt "$GVSIG_HOME_FOLDER/jre/"*1.6* 2>/dev/null | head -1)
  PJH5=$(ls -dt "$GVSIG_HOME_FOLDER/jre/"*1.5* 2>/dev/null | head -1)
fi
if [ -d /usr/lib/jvm ] ; then
    PJH6=$(find /usr/lib/jvm -maxdepth 1 ! -name "jvm" -name "[a-zA-Z]*" ! -type l -print)
else
    PJH6=
fi

if [ -f "$PJH1/bin/java" ] ; then
    if [ ! -x "$PJH1/bin/java" ] ; then
	chmod a+x "$PJH1/bin/java"
    fi
fi

#
#
# Try to use java from:
# 1. Java in gvSIG/extensiones/jre (jre plugin)
# 2. Java specified in JAVA_HOME variable
# 3. Java in the HOME/gvsig/jre folder (old gvsig versions optionally create during installation process)
# 4. Java installed in the system (/usr/lib/jvm)
# 5. Java in the system path.
#
for JAVA_HOME_TO_USE in "$PJH1" "$JAVA_HOME" "$PJH2" "$PJH3" "$PJH4" "$PJH5" $PJH6 ""
do
  if [ "$JAVA_HOME_TO_USE" = "" ] ; then
	  continue
  fi
  logger_info "Located possible jre at: ${JAVA_HOME_TO_USE}"
done

for JAVA_HOME_TO_USE in "$JAVA_HOME" "$PJH1" "$PJH2" "$PJH3" "$PJH4" "$PJH5" $PJH6 ""
do
        if [ "$JAVA_HOME_TO_USE" = "" ] ; then
                continue
        fi
	logger_info "Checking JAVA_HOME ${JAVA_HOME_TO_USE}..."
        if [ ! -d "$JAVA_HOME_TO_USE" ] ; then
                logger_warn "Folder not found ($JAVA_HOME_TO_USE), skip"
                continue
        fi
        if [ ! -x "$JAVA_HOME_TO_USE/bin/java" ] ; then
                logger_warn "Not a valid JAVA_HOME ($JAVA_HOME_TO_USE), bin/java not found, skip"
                continue
        fi
	JAVA_ARCH=$(getJavaArchitecture "$JAVA_HOME_TO_USE/bin/java")
	if [ "$JAVA_ARCH" != "$PREFERED_ARCHITECTURE" ] ; then
                JAVA_HOME_OTHER_ARCHITECTURE="$JAVA_HOME_TO_USE"
                logger_warn "Prefered architecture is $PREFERED_ARCHITECTURE, java $JAVA_ARCH found in $JAVA_HOME_TO_USE, skip"
                continue
        fi
        break
done

if [ "$JAVA_HOME_TO_USE" = "" ] ; then
        # Try to use the java in the system path
        if ! type java >/dev/null ; then
                # java not found in the system path
                if [ "$JAVA_HOME_OTHER_ARCHITECTURE" != "" ] ; then
                        JAVA_HOME="$JAVA_HOME_OTHER_ARCHITECTURE"
                        JAVA="$JAVA_HOME/bin/java"
                        logger_warn "java $PREFERED_ARCHITECTURE not found in the system path, using $JAVA_HOME_OTHER_ARCHITECTURE"
                else
                        logger_warn "java not found in the system"
                        exit 1
                fi
        else
		if [ "$(getJavaArchitecture $(type -p java))" != "$PREFERED_ARCHITECTURE" ] ; then
                      logger_warn "java $PREFERED_ARCHITECTURE not found, using $(type -p java)"
                fi
                unset JAVA_HOME
                JAVA="java"
        fi
else
        JAVA_HOME="$JAVA_HOME_TO_USE"
        JAVA="$JAVA_HOME/bin/java"
fi

if [ "$PREFERED_ARCHITECTURE" = "x86" ] ; then
	FORCE_JAVA_32BITS="$ARG_D32"
else
	FORCE_JAVA_32BITS=""

fi
logger_info "Using java " $(type -p "$JAVA")

###################################################################
# gvSIG native libraries location
###################################################################

export GVSIG_NATIVE_LIBS=""
export LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH
export PATH
export GVSIG_PROPERTIES=""

add_library_path() {
  #logger_info "add_library_path $1"
  GVSIG_NATIVE_LIBS="$1${CPSEP}$GVSIG_NATIVE_LIBS"

  # On Linux family systems
  LD_LIBRARY_PATH="$1${CPSEP}$LD_LIBRARY_PATH"
  # On Darwin family systems
  DYLD_LIBRARY_PATH="$1${CPSEP}$DYLD_LIBRARY_PATH"
  # On Windows family systems
  PATH="$1${CPSEP}$PATH"
}

list_shared_library_dependencies() {
  logger_info "Checking shared library dependencies for $1"
  case "$OS" in
  "win")
        logger_cmd ./cygcheck.exe "\"$1\""
        ;;
  "lin")
        logger_cmd ldd "\"$1\""
        ;;
  "darwin")
        logger_cmd otool -L "\"$1\""
        ;;
  esac || logger_warn "Problens checking shared library $1"  || true
}

add_classpath() {
  GVSIG_CLASSPATH="$GVSIG_CLASSPATH${CPSEP}$1"
}

add_property() {
  value="\"$2\""
  GVSIG_PROPERTIES="$GVSIG_PROPERTIES -D$1=$value"
}


###################################################################
# gvSIG java libraries location
###################################################################

# Load gvSIG Andami jars and dependencies for the classpath
for i in "$GVSIG_INSTALL_FOLDER/lib/"*.jar ; do
  if [ "$i" != "$GVSIG_INSTALL_FOLDER/lib/*.jar" -a "$i" != "" ]; then
    add_classpath "$i"
  fi
done
for i in "$GVSIG_INSTALL_FOLDER/lib/"*.zip ; do
  if [ "$i" != "$GVSIG_INSTALL_FOLDER/lib/*.zip" -a "$i" != "" ]; then
    add_classpath "$i"
  fi
done

# gvSIG Andami launcher

if [ "$GVSIG_LAUNCHER" = "" ] ; then
  GVSIG_LAUNCHER=org.gvsig.andamiupdater.Updater
fi
if [ "$GVSIG_PARAMS" = "" ] ; then
  GVSIG_PARAMS="$GVSIG_APPLICATION_NAME gvSIG/extensiones $@"
fi

export GVSIG_LAUNCHER
export GVSIG_PARAMS

########################
# Memory configuration #
########################

# Initial gvSIG memory (M=Megabytes, G=Gigabytes)
export GVSIG_INITIAL_MEM=${GVSIG_INITIAL_MEM:-256M}
# Maximum gvSIG memory (M=Megabytes, G=Gigabytes)
export GVSIG_MAX_MEM=${GVSIG_MAX_MEM:-1024M}
# Maximum permanent memory size: needed to load classes and statics
export GVSIG_MAX_PERM_SIZE=${GVSIG_MAX_PERM_SIZE:-128M}

#############################
# Execute plugin's autorun  #
#############################

for PLUGIN_FOLDER in "$GVSIG_INSTALL_FOLDER/gvSIG/extensiones/"*
do
  if [ -f "$PLUGIN_FOLDER/autorun.sh" ] ; then
    logger_info "Running autorun from plugin " $(basename "$PLUGIN_FOLDER")
    . "$PLUGIN_FOLDER/autorun.sh"
    cd "$GVSIG_INSTALL_FOLDER"
  fi
done

if [ "$OS" = "win" ] ; then
	add_library_path "$GVSIG_INSTALL_FOLDER"
fi

logger_info "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"

#####################
# Java debug mode ? #
#####################

ARGS="$@" # Can't use ${@/ , use ${ARGS/ instead
if [ "${ARGS/*--pause*/--pause}" == "--pause" ] ; then
    DEBUG_PAUSE="y"
else
    DEBUG_PAUSE="n"
fi

if [ "${ARGS/*--debug*/--debug}" == "--debug" ] ; then
  DEBUG_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=8765,server=y,suspend=$DEBUG_PAUSE"
else
  DEBUG_OPTIONS=""
fi

################
# Launch gvSIG #
################

# For Java parameters documentation and more parameters look at:
# http://download.oracle.com/javase/6/docs/technotes/tools/windows/java.html
# http://www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html
CMD="\"${JAVA}\" \
  $FORCE_JAVA_32BITS \
  -Xms${GVSIG_INITIAL_MEM} \
  -Xmx${GVSIG_MAX_MEM} \
  -XX:MaxPermSize=${GVSIG_MAX_PERM_SIZE} \
  ${DEBUG_OPTIONS} \
  -Djava.library.path=\"${GVSIG_NATIVE_LIBS}\" \
  ${GVSIG_PROPERTIES} \
  -cp \"${GVSIG_CLASSPATH}\" \
  ${GVSIG_JAVA_PARAMS} \
  ${GVSIG_LAUNCHER} ${GVSIG_PARAMS}"

logger_info Launching gvSIG: "${CMD}"
eval "$CMD"
