/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.Component;
import java.io.File;
import javax.swing.filechooser.FileFilter;

public interface ThreadSafeDialogs {
	
	/**
	 * Create a message dialog with an ok button.
	 * 
	 * If message or title start whit a "_" then try to translate.
	 *
	 * This method is thread safe to use.
	 * 
	 * @param message to present in the dialog
	 * @param title title of the dialog
	 * @param messageType type of icon to use.
	 * 
	 * @see javax.swing.JOptionPane#showMessageDialog(Component, Object, String, int)
	 */
	public void messageDialog(String message, String title, int messageType);
	
	/**
	 * Create a message dialog with ok button.
	 * 
	 * If message or title start whit a "_" then try to translate.
	 * if messageArgs not is null the message can be formated whit MessageFormat.
	 * 
	 * This method is thread safe to use.
	 * 
	 * @param message to present in the dialog
	 * @param messageArgs string array of arguments used to format the message 
	 * @param title title of the dialog
	 * @param messageType type of icon to use.
	 * 
	 * @see javax.swing.JOptionPane#showMessageDialog(Component, Object, String, int)
	 */
	public void messageDialog(String message, String messageArgs[], String title, int messageType);
	
	/**
	 * Show a JOptionPane confirm dialog.
	 * 
	 * if this method is invoked out of the event dispatch thread of swing, 
	 * the dialog is presented in the thread of swing and the calling thread
	 * wait to close to continue.
	 * 
	 * @param message
	 * @param title
	 * @param optionType
	 * @param messageType
	 * @return
	 * 
	 * @see javax.swing.JOptionPane#showConfirmDialog(Component, Object, String, int, int)
	 * 
	 */
	public int confirmDialog(String message, String title, int optionType, int messageType) ;

	/**
	 * Show a JOptionPane input dialog.
	 * 
	 * if this method is invoked out of the event dispatch thread of swing, 
	 * the dialog is presented in the thread of swing and the calling thread
	 * wait to close to continue.
	 * 
	 * @param message
	 * @param title
	 * @param optionType 
         *           an integer designating the options available on the dialog: 
         *           YES_NO_OPTION, YES_NO_CANCEL_OPTION, or OK_CANCEL_OPTION
	 * @param messageType
         *           an integer designating the kind of message this is; 
         *           primarily used to determine the icon from the pluggable 
         *           Look and Feel: ERROR_MESSAGE, INFORMATION_MESSAGE, 
         *           WARNING_MESSAGE, QUESTION_MESSAGE, or PLAIN_MESSAGE
	 * @return
	 * 
	 * @see javax.swing.JOptionPane#showInputDialog(Component, Object, String, int)
	 * 
	 */
	public String inputDialog(String message, String title, int messageType, String initialValue) ;
	
	/**
	 * Show a JOptionPane input dialog.
	 * 
	 * if this method is invoked out of the event dispatch thread of swing, 
	 * the dialog is presented in the thread of swing and the calling thread
	 * wait to close to continue.
	 * 
	 * @param message
	 * @param title
	 * @param optionType
	 * @param messageType
	 * @return
	 * 
	 * @see javax.swing.JOptionPane#showInputDialog(Component, Object, String, int)
	 * 
	 */
	public String inputDialog(String message, String title);
	
	/**
	 * Create a window with the contents and title specified as parameters
	 * and show as a dialog windows of gvSIG.
	 * 
	 * if this method is invoked out of the event dispatch thread of swing, 
	 * the dialog is presented in the thread of swing and the calling thread
	 * wait to close to continue. in this case the dialog is not presented as modal to
	 * other components of the gui.
	 * 
	 * @param contents 
	 * @param title
	 */
	public void showDialog(final Component contents, final String title);
	
	/**
	 * Creates an return a new instance of the component specified 
	 * 
	 * This method ensure that the component is created in the event dispatch thread of swing. 
	 * 
	 * @param theClass of component to be created
	 * @param parameters passed to the constructor of the component
	 * 
	 * @return the instance of the component created
	 */
	public Component createComponent(final Class<? extends Component> theClass,  final Object ... parameters);

	/**
	 * Creates an return a new instance of the component specified 
	 * 
	 * This method ensure that the component is created in the event dispatch thread of swing. 
	 * 
	 * @param theClass of component to be created
	 * @param parameters passed to the constructor of the component
	 * 
	 * @return the instance of the component created
	 */
	public Component createComponentWithParams(final Class<? extends Component> theClass,  final Object[] parameters);
	
	/**
	 * Creates and show a JFileChooser dialog.
	 * 
	 * This method return an array whit the selected files. If multiselection is
	 * not allowed, an array with one element is returned. 
	 * 
	 * If the user select the cancel button, null is returned.
	 * 
	 * if this method is invoked out of the event dispatch thread of swing, 
	 * the dialog is presented in the thread of swing and the calling thread
	 * wait to close to continue.
	 * 
	 * @param title, title of dialog
	 * @param type of the dialog, JFileChooser.SAVE_DIALOG or JFileChooser.OPEN_DIALOG
	 * @param selectionMode of the dialog, values are: JFileChooser.FILES_ONLY, JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES
	 * @param multiselection, true if multiselection is allowed
	 * @param initialPath, to show in the dialog
	 * @param filter used to filter the files show in the dialog.
	 * @param fileHidingEnabled, if true hidden files are show
	 * @return an array whit the files selecteds or null.
	 */
	public File[] showChooserDialog(
			final String title,
			final int type,
			final int selectionMode,
			final boolean multiselection, 
			final File initialPath,
			final FileFilter filter,
			final boolean fileHidingEnabled
			) ;

	/**
	 * Creates and show a JFileChooser dialog for folder selection.
	 *
	 * This is an utility method that wrap a showChooserDialog call.
	 *  
 	 * @param title, title of dialog
	 * @param initialPath, to show in the dialog
	 * @return an array whit the files selecteds or null.
	 * 
	 * @see #showChooserDialog(String, int, int, boolean, File, FileFilter, boolean)
	 */
	public File[] showOpenDirectoryDialog(String title, File initialPath) ;
	
	/**
	 * Creates and show a JFileChooser dialog for selection a file for open.
	 *
	 * This is an utility method that wrap a {@link #showChooserDialog(String, int, int, boolean, File, FileFilter, boolean)}  call.
	 *  
 	 * @param title, title of dialog
	 * @param initialPath, to show in the dialog
	 * @return an array whit the files selecteds or null.
	 * 
	 * @see #showChooserDialog(String, int, int, boolean, File, FileFilter, boolean)
	 */
	public File[] showOpenFileDialog(String title, File initialPath) ;
	
	/**
	 * Creates and show a JFileChooser dialog for selection a file for save.
	 *
	 * This is an utility method that wrap a {@link #showChooserDialog(String, int, int, boolean, File, FileFilter, boolean)}  call.
	 *  
 	 * @param title, title of dialog
	 * @param initialPath, to show in the dialog
	 * @return an array whit the files selecteds or null.
	 * 
	 * @see #showChooserDialog(String, int, int, boolean, File, FileFilter, boolean)
	 */
	public File[] showSaveFileDialog(String title, File initialPath) ;
	
}
