/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import org.gvsig.andami.actioninfo.impl.DefaultActionInfoManager;
import org.gvsig.andami.firewall.DefaultFirewallConfiguration;
import org.gvsig.andami.impl.DefaultLocaleManager;
import org.gvsig.andami.impl.DefaultPluginsManager;
import org.gvsig.andami.installer.translations.TranslationsInstallerFactory.RegisterTranslationsInstaller;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.installer.lib.api.InstallerLibrary;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.util.Caller;
import org.gvsig.tools.util.impl.DefaultCaller;


public class PluginsLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsAPI(PluginsLibrary.class);
        require(InstallerLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        PluginsLocator.registerDefaultManager(DefaultPluginsManager.class);
        PluginsLocator.registerActionInfoManager(DefaultActionInfoManager.class);
        PluginsLocator.registerLocaleManager(DefaultLocaleManager.class);
        ToolsLocator.registerDefaultFirewallManager(DefaultFirewallConfiguration.class);

        // Force initialize of the LocaleManager
        PluginsLocator.getLocaleManager();
    }

    protected void doPostInitialize() throws LibraryException {
        Caller caller = new DefaultCaller();

        caller.add(new WindowInfo.RegisterPersistence() );
        caller.add(new RegisterTranslationsInstaller() );
        if( !caller.call() ) {
            throw new LibraryException(PluginsLibrary.class, caller.getExceptions());
        }


    }

}
