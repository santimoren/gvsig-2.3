/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.theme;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.tools.util.XMLUtils;
import org.kxml2.io.KXmlParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * Personalization of the program according to the file xml with the necessary
 * information.
 *
 * @author Vicente Caballero Navarro
 */
public class Theme {

    private static final Logger logger = LoggerFactory.getLogger(Theme.class);

    public static final String CENTERED = "CENTERED";
    public static final String EXPAND = "EXPAND";
    public static final String MOSAIC = "MOSAIC";

    private static final String ANDAMI_PROPERTIES = "AndamiProperties";
    private static final String APPLICATION_IMAGES = "ApplicationImages";
    private static final String SPLASH_IMAGES = "SplashImages";
    private static final String SPLASH = "Splash";
    private static final String PATH = "path";
    private static final String TIMER = "timer";
    private static final String ICON = "Icon";
    private static final String APPLICATION_NAME = "ApplicationName";
    private static final String PRIORITY = "priority";
    private static final String VALUE = "value";
    private static final String BACKGROUND_IMAGE = "BackgroundImage";
    private static final String BACKGROUND_COLOR = "BackgroundColor";
    private static final String WALLPAPER_TYPE = "WallpaperType";

    private static final String VERSION = "version";
    private static final String FONTPOSITIONX = "x";
    private static final String FONTPOSITIONY = "y";

    private static final String FONTSIZE = "fontsize";
    private static final String FONTCOLOR = "color";

    private ArrayList<String> images = new ArrayList<String>();
    private String icon;
    private ArrayList<String> timers = new ArrayList<String>();
    private ArrayList<String> versions = new ArrayList<String>();
    private ArrayList<String> fontColors = new ArrayList<String>();
    private ArrayList<String> fontSizes = new ArrayList<String>();
    private ArrayList<String> fontpositionsX = new ArrayList<String>();
    private ArrayList<String> fontpositionsY = new ArrayList<String>();

    private String name = null;
    private String backgroundimage = null;
    private Color backgroundColor = null;
    private String wallpaperType = CENTERED;
    private int priority = 0;

    /**
     * Spurce file of the theme *
     */
    private File source = null;
    private StrSubstitutor strSubstitutor = null;

    public Theme() {
    }

    public Theme(Map vars) {
        this();
        this.strSubstitutor = new StrSubstitutor(vars);
    }

    public File getSource() {
        return source;
    }

    /**
     * Read the file xml with the personalization.
     *
     * @param file xml
     */
    public void readTheme(File file) {
        FileInputStream fis = null;
        try {
            String encoding = XMLUtils.getEncoding(file, "UTF-8");

            fis = new FileInputStream(file);
            KXmlParser parser = new KXmlParser();
            parser.setInput(fis, encoding);

            int tag = parser.nextTag();

            if ( parser.getEventType() != XmlPullParser.END_DOCUMENT ) {
                parser.require(XmlPullParser.START_TAG, null, ANDAMI_PROPERTIES);

                while ( tag != XmlPullParser.END_DOCUMENT ) {
                    //parser.next();
                    switch (tag) {
                    case XmlPullParser.START_TAG:
                        if ( parser.getName().compareTo(ANDAMI_PROPERTIES) == 0 ) {
                            parser.nextTag();
                            if ( parser.getName().compareTo(APPLICATION_IMAGES) == 0 ) {
                                int splashtag = parser.nextTag();
                                if ( parser.getName().compareTo(SPLASH_IMAGES) == 0 ) {
                                    splashtag = parser.nextTag();
                                    boolean endSplash = false;

                                    // parser.require(KXmlParser.START_TAG,null, SPLASH);
                                    while ( (splashtag != XmlPullParser.END_DOCUMENT)
                                            && !endSplash ) {
                                        if ( splashtag == XmlPullParser.END_TAG ) {
                                            splashtag = parser.nextTag();
                                            continue;
                                        }

                                        if ( parser.getName().compareTo(SPLASH) == 0 ) {
                                            images.add(fixPath(file, parser.getAttributeValue("", PATH)));
                                            timers.add(parser.getAttributeValue("", TIMER));
                                            versions.add(expand(parser.getAttributeValue("", VERSION)));
                                            fontpositionsX.add(parser.getAttributeValue("", FONTPOSITIONX));
                                            fontpositionsY.add(parser.getAttributeValue("", FONTPOSITIONY));
                                            fontSizes.add(parser.getAttributeValue("", FONTSIZE));
                                            fontColors.add(parser.getAttributeValue("", FONTCOLOR));

                                            splashtag = parser.nextTag();
                                        } else {
                                            endSplash = true;
                                        }
                                    }
                                }
                                int tagOptions = XmlPullParser.START_TAG;
                                while ( (tagOptions != XmlPullParser.END_TAG) ) {
                                    if ( parser.getName().compareTo(BACKGROUND_IMAGE) == 0 ) {
                                        backgroundimage = fixPath(file, parser.getAttributeValue("", PATH));
                                        tag = parser.next();
                                    } else if ( parser.getName().compareTo(BACKGROUND_COLOR) == 0 ) {
                                        backgroundColor = parseColor(parser.getAttributeValue("", "color"));
                                        tag = parser.next();
                                    } else if ( parser.getName().compareTo(ICON) == 0 ) {
                                        icon = fixPath(file, parser.getAttributeValue("", PATH));
                                        tag = parser.next();
                                    } else if ( parser.getName().compareTo(WALLPAPER_TYPE) == 0 ) {
                                        wallpaperType = parser.getAttributeValue("", VALUE);
                                        tag = parser.next();
                                    }
                                    tagOptions = parser.nextTag();

                                }
                            }
                        }
                        if ( parser.getName().compareTo(APPLICATION_NAME) == 0 ) {
                            name = expand(parser.getAttributeValue("", VALUE));
                            tag = parser.next();
                        }
                        if ( parser.getName().compareTo(PRIORITY) == 0 ) {
                            String s = parser.getAttributeValue("", VALUE);
                            try {
                                this.priority = Integer.parseInt(s);
                            } catch (Exception ex) {
                                this.priority = 0;
                            }
                            tag = parser.next();
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        // System.out.println("[TEXT]["+kxmlParser.getText()+"]");
                        break;
                    }
                    tag = parser.next();
                }
            }

            parser.require(XmlPullParser.END_DOCUMENT, null, null);
            this.source = file;

        } catch (FileNotFoundException ex) {
            logger.warn("Can't open theme file '" + file.getAbsolutePath() + "'.", ex);

        } catch (XmlPullParserException ex) {
            logger.warn("Can't parse theme file '" + file.getAbsolutePath() + "'.", ex);

        } catch (IOException ex) {
            logger.warn("Can't read theme file '" + file.getAbsolutePath() + "'.", ex);

        } finally {
            IOUtils.closeQuietly(fis);
        }
    }

    private String fixPath(File theme, String path) {
        path = path.replace("$GVSIG_INSTALL", Launcher.getApplicationDirectory());
        path = path.replace("$GVSIG_HOME", Launcher.getAppHomeDir());
        path = path.replace("$GVSIG_PLUGINS", Launcher.getAndamiConfig().getPluginsDirectory());
        File parent = theme.getParentFile().getAbsoluteFile();
        File fpath = new File(path);
        if ( !fpath.isAbsolute() ) {
            fpath = new File(parent, path);
        }
        return fpath.getAbsolutePath();
    }

    /**
     * Returns image to the splashwindow.
     *
     * @return ImageIcon[]
     */
    public ImageIcon[] getSplashImages() {
        ImageIcon[] imgs = new ImageIcon[images.size()];

        for ( int i = 0; i < images.size(); i++ ) {
            imgs[i] = new ImageIcon(images.get(i));
        }

        return imgs;
    }

    public String getTypeDesktop() {
        return wallpaperType;
    }

    /**
     * Return the icon.
     *
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        if ( icon == null ) {
            return null;
        }

        try {
            return new ImageIcon(icon);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return the backgroundimage.
     *
     * @return ImageIcon
     */
    public ImageIcon getBackgroundImage() {
        if ( backgroundimage == null ) {
            return null;
        }

        try {
            return new ImageIcon(backgroundimage);
        } catch (Exception e) {
            return null;
        }
    }

    public Color getBackgroundColor() {
        if ( this.backgroundColor == null ) {
            this.backgroundColor = (Color) UIManager.get("Desktop.background");
        }
        return this.backgroundColor;
    }

    /**
     * Return the time of the splash images.
     *
     * @return long[]
     */
    public int[] getTimers() {
        int[] tms = new int[timers.size()];

        for ( int i = 0; i < tms.length; i++ ) {
            tms[i] = Integer.parseInt(timers.get(i));
        }

        return tms;
    }

    /**
     * Return the name of program.
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Return the text to overwrite the images.
     *
     * @return String[]
     */
    public String[] getVersions() {
        return versions.toArray(new String[0]);
    }

    /**
     * Return the position of text to overwrite the images.
     *
     * @return Point[]
     */
    public Point[] getPositions() {
        Point[] points = new Point[fontpositionsX.size()];
        for ( int i = 0; i < points.length; i++ ) {
            try {
                points[i] = new Point(Integer.valueOf(fontpositionsX.get(i)), Integer.valueOf(fontpositionsY.get(i)));
            } catch (NumberFormatException e) {
                NotificationManager.addInfo(PluginServices.getText(this, "incorrect_position"), e);
            }
        }
        return points;
    }

    /**
     * Return the font size text to overwrite the images.
     *
     * @return int[]
     */
    public int[] getFontSizes() {
        int[] sizes = new int[fontSizes.size()];
        for ( int i = 0; i < sizes.length; i++ ) {
            try {
                sizes[i] = Integer.valueOf(fontSizes.get(i));
            } catch (NumberFormatException e) {
                NotificationManager.addInfo(PluginServices.getText(this, "incorrect_size"), e);
            }
        }
        return sizes;
    }

    /**
     * Return the font color of text to overwrite the images.
     *
     * @return Color[]
     */
    public Color[] getFontColors() {
        Color[] colors = new Color[fontColors.size()];
        for ( int i = 0; i < colors.length; i++ ) {
            try {
                String s = fontColors.get(i);
                colors[i] = parseColor(s);
            } catch (Exception e) {
                NotificationManager.addInfo(PluginServices.getText(this, "incorrect_color"), e);
            }
        }
        return colors;
    }

    private Color parseColor(String s) {
        if ( StringUtils.isEmpty(s) ) {
            return null;
        }
        String[] rgb = s.split(",");
        Color color = new Color(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
        return color;
    }

    private String expand(String value) {
        if ( this.strSubstitutor == null ) {
            return value;
        }
        return strSubstitutor.replace(value);
    }

    public int getPriority() {
        return this.priority;
    }
}
