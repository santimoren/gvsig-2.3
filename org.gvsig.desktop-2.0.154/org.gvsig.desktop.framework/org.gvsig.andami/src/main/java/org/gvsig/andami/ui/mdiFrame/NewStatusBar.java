/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.plugins.config.generate.Label;
import org.gvsig.gui.beans.controls.IControl;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.task.JTasksStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class contains the status bar. It contains the graphical component, and
 * the methods to manage it.
 * </p>
 * 
 * <p>
 * The status bar is divided in several areas. At the very left, there is an
 * icon and the main status text. There are three icons to show: Info, Warning
 * and Error icons. They can be set together with the main status text using the
 * methods <code>setInfoText()</code>, <code>setWarningText()</code> and
 * <code>setErrorText()</code> (and also with <code>setInfoTextTemporal()</code>
 * , etc). Then, there is a right area which contains labels and other controls.
 * Labels are set in the config.xml files and are visible or not depending on
 * the currently selected Andami window. Controls are associated to extensions,
 * and are enabled/disabled and visible/hidden depending on the associated
 * extension.
 * </p>
 * 
 */
public class NewStatusBar extends JPanel {

    private static final long serialVersionUID = 5575549032728844632L;
    
    private static Logger logger = LoggerFactory.getLogger(NewStatusBar.class);
    
    private static final int INFO = 0;
    private static final int WARNING = 1;
    private static final int ERROR = 2;
    private JLabel lblIcon = null;
    private JLabel lblTexto = null;
    private boolean contenidoTemporal;
    private String textoAnterior;
    private int estadoAnterior;
    private int estado;
    private JProgressBar progressBar = null;
    private Map<String, JLabel> idLabel = new HashMap<String, JLabel>();
    private int[] widthlabels = null;
    private Map<String, Component> controls = new HashMap<String, Component>();
    private JPanel controlContainer;
    private JTasksStatus tasksStatus = null;
	private Timer messageTimer;

	private String textoCompleto;

	
    /**
     * This is the default constructor
     */
    public NewStatusBar() {
        super();
        initialize();
    }

	private ImageIcon getImageIcon(String iconName) {
//		return PluginServices.getIconTheme().get(iconName);
		return IconThemeHelper.getImageIcon(iconName);
	}
	

    /**
     * This method initializes the status bar. It creates the required
     * containers and sets the layout.
     */
    private void initialize() {
        LayoutManager mainLayout = new BorderLayout();
        this.setLayout(mainLayout);

        JPanel panelRight = new JPanel(new FlowLayout(FlowLayout.RIGHT, 1, 0));

        controlContainer = new JPanel(new FlowLayout(FlowLayout.RIGHT, 1, 0));
        panelRight.add(controlContainer);
        panelRight.add(getTasksStatus());

        lblIcon = new JLabel();
        lblTexto = new JLabel();
        lblTexto.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        lblTexto.setHorizontalAlignment(SwingConstants.LEFT);
        lblTexto.setHorizontalTextPosition(SwingConstants.LEFT);
        lblTexto.setVerticalAlignment(SwingConstants.CENTER);
        lblTexto.setVerticalTextPosition(SwingConstants.CENTER);
        lblIcon.setText("");
        lblTexto.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblTexto.setText(Messages.getString("StatusBar.Aplicacion_iniciada"));

        JPanel panelLeft = new JPanel();
        panelLeft.setLayout(new FlowLayout(FlowLayout.LEFT, 1, 0));
        panelLeft.add(lblIcon);
        panelLeft.add(lblTexto);
        panelLeft.add(getProgressBar());

        this.add(panelLeft, BorderLayout.CENTER);
        this.add(panelRight, BorderLayout.EAST);

        buildMesagePopupMenu();
        
        messageTimer = new Timer(30000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					clearMessage();
				} catch( Throwable ex) {
					logger.info("Can't clear message", ex);
				}
			}
		});
    }
    
    public void clearMessage() {
    	this.textoCompleto = "";
    	lblTexto.setText("");
    	lblIcon.setIcon(null);
    	estado = INFO;
    }

    
    public void message(final String msg, final int messageTyoe) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	message(msg,messageTyoe);
                }
            });
            return;
        }
        messageTimer.stop();
        if( StringUtils.isBlank(msg) ) {
            this.clearMessage();
            return;
        }
    	switch (messageTyoe) {
		case JOptionPane.ERROR_MESSAGE:
			setErrorText(msg);
			break;
		case JOptionPane.WARNING_MESSAGE:
			setWarningText(msg);
			break;
		default:
		case JOptionPane.INFORMATION_MESSAGE:
			setInfoText(msg);
			break;
		}
    	this.doLayout();
    	this.repaint();
    	messageTimer.start();
	}

    private void buildMesagePopupMenu() {
    	final JPopupMenu menu = new JPopupMenu();
    	
    	JMenuItem item = new JMenuItem("View details");
    	item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switch (estado) {
				default:
				case INFO:
					JOptionPane.showMessageDialog(lblTexto, getStatusText(), "", JOptionPane.INFORMATION_MESSAGE);
					break;
				case WARNING:
					JOptionPane.showMessageDialog(lblTexto, getStatusText(), "", JOptionPane.WARNING_MESSAGE);
					break;
				case ERROR:
					JOptionPane.showMessageDialog(lblTexto, getStatusText(), "", JOptionPane.ERROR_MESSAGE);
					break;
				}
			}
		});
    	menu.add(item);
    	item = new JMenuItem("Clear");
    	item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearMessage();
			}
		});
    	menu.add(item);
    	menu.addSeparator();
    	item = new JMenuItem("Cancel");
    	menu.add(item);
    	
    	this.lblTexto.addMouseListener( new MouseListener() {
			public void mouseReleased(MouseEvent e) {
//				if( e.isPopupTrigger() ) {
//					if( getStatusText().length() > 0 || estado!=INFO ) {
//						menu.show(lblTexto, e.getX(), e.getY());
//					}
//				}
			}
			public void mousePressed(MouseEvent e) {
				if( e.isPopupTrigger() || e.getClickCount()==1 ) {
					if( getStatusText().length() > 0 || estado!=INFO ) {
						menu.show(lblTexto, e.getX(), e.getY());
					}
				}
			}
			public void mouseExited(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
			}
			public void mouseClicked(MouseEvent e) {
			}
		});
    }
    
    /**
     * Gets the status bar main text.
     * 
     * @return The status bar main text.
     * @see #setInfoText(String)
     * @see #setWarningText(String)
     * @see #setErrorText(String)
     * @see #setInfoTextTemporal(String)
     * @see #setWarningTextTemporal(String)
     * @see #setErrorTextTemporal(String)
     */
    public String getStatusText() {
        return textoCompleto; //lblTexto.getText();
    }

    /**
     * Restores the previous contents in the status bar main text,
     * after the {@link #setInfoTextTemporal(String)},
     * {@link #setWarningTextTemporal(String)} or
     * {@link #setErrorTextTemporal(String)} have been called.
     * 
     * @see #setInfoTextTemporal(String)
     * @see #setWarningTextTemporal(String)
     * @see #setErrorTextTemporal(String)
     */
    public void restaurarTexto() {
        contenidoTemporal = false;

        if (estadoAnterior == -1) {
            return;
        }

        switch (estadoAnterior) {
        case INFO:
            setInfoText(textoAnterior);

            break;

        case WARNING:
            setWarningText(textoAnterior);

            break;

        case ERROR:
            setErrorText(textoAnterior);

            break;
        }

        estadoAnterior = -1;
        textoAnterior = null;
    }

    /**
     * Sets a temporary information message in the status bar, and changes the
     * icon to an Info icon. The previous text and icon can be restored using
     * the {@link #restaurarTexto()} method.
     * 
     * @param texto
     *            The text to set
     * @see #restaurarTexto()
     */
    public void setInfoTextTemporal(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setInfoTextTemporal(texto);
                }
            });
            return;
        }
        if (!contenidoTemporal) {
            textoAnterior = getStatusText();
            estadoAnterior = this.estado;
            contenidoTemporal = true;
        }

        this.estado = INFO;
        lblIcon.setIcon(getImageIcon("statusbar-info"));
        lblTexto.setText(texto);
    }

    /**
     * Sets a temporary warning message in the status bar, and changes the
     * icon to a Warning icon. The previous text and icon can be restored using
     * the {@link #restaurarTexto()} method.
     * 
     * @param texto
     *            The text to set
     * @see #restaurarTexto()
     */
    public void setWarningTextTemporal(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setWarningTextTemporal(texto);
                }
            });
            return;
        }
      	
        if (!contenidoTemporal) {
            estadoAnterior = this.estado;
            textoAnterior = getStatusText();
            contenidoTemporal = true;
        }
        this.estado = WARNING;
        lblIcon.setIcon(getImageIcon("statusbar-warning"));
        lblTexto.setText(texto);
    }

    /**
     * Sets a temporary error message in the status bar, and changes the
     * icon to an Error icon. The previous text and icon can be restored using
     * the {@link #restaurarTexto()} method.
     * 
     * @param texto
     *            The text to set
     * @see #restaurarTexto()
     */
    public void setErrorTextTemporal(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setErrorTextTemporal(texto);
                }
            });
            return;
        }
       if (!contenidoTemporal) {
            estadoAnterior = this.estado;
            textoAnterior = getStatusText();
            contenidoTemporal = true;
        }

        this.estado = ERROR;
        lblIcon.setIcon(getImageIcon("statusbar-error"));
        lblTexto.setText(texto);
    }

    /**
     * Sets a permanent info message in the status bar, and changes the
     * permanent icon to an Info icon. If there is a temporary message showing
     * at the moment, the message set now is not shown until
     * the {@link #restaurarTexto()} method is called.
     * 
     * @param texto
     *            The permanent info message to set
     * @see #restaurarTexto()
     */
    public void setInfoText(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setInfoText(texto);
                }
            });
            return;
        }
        if (contenidoTemporal) {
            textoAnterior = texto;
            estadoAnterior = INFO;
        } else {
        	this.textoCompleto = texto; 
            lblTexto.setText(getFirstTextLine(texto));
            lblIcon.setIcon(getImageIcon("statusbar-info"));
            estado = INFO;
        }
    }
    
    private String getFirstTextLine(String text) {
        
        if (text == null) {
            return null;
        }
        
    	int n = text.indexOf("\n");
    	if( n == -1 ) {
    		return text;
    	}
    	return text.substring(0,n) + "...";
    }

    /**
     * Sets a permanent warning message in the status bar, and changes the
     * permanent icon to a Warning icon. If there is a temporary message showing
     * at the moment, the message set now is not shown until
     * the {@link #restaurarTexto()} method is called.
     * 
     * @param texto
     *            The permanent warning message to set
     * @see #restaurarTexto()
     */
    public void setWarningText(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setWarningText(texto);
                }
            });
            return;
        }
        
        if (contenidoTemporal) {
            textoAnterior = texto;
            estadoAnterior = WARNING;
        } else {
        	this.textoCompleto = texto; 
            lblTexto.setText(getFirstTextLine(texto));
            lblIcon.setIcon(getImageIcon("statusbar-warning"));
            estado = WARNING;
        }
    }

    /**
     * Sets a permanent error message in the status bar, and changes the
     * permanent icon to an Error icon. If there is a temporary message showing
     * at the moment, the message set now is not shown until
     * the {@link #restaurarTexto()} method is called.
     * 
     * @param texto
     *            The permanent info message to set
     * @see #restaurarTexto()
     */
    public void setErrorText(final String texto) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setErrorText(texto);
                }
            });
            return;
        }

        if (contenidoTemporal) {
            textoAnterior = texto;
            estadoAnterior = ERROR;
        } else {
        	this.textoCompleto = texto; 
            lblTexto.setText(getFirstTextLine(texto));
            lblIcon.setIcon(getImageIcon("statusbar-error"));
            estado = ERROR;
        }
    }

    /**
     * If <code>p</code> is a value between 0 and 99, it shows a progress bar
     * in the left area of the status bar, and sets the specified progress.
     * If <code>p</code> is bigger than 99, it hides the progress bar.
     * 
     * @param p
     *            The progress to set in the progress bar. If it is bigger
     *            than 99, the task will be considered to be finished, and the
     *            progress bar will be hidden.
     * 
     * @deprecated use instead TaskStatus and TaskStatusManager
     */
    public void setProgress(int p) {
        if (p < 100) {
            getProgressBar().setValue(p);
            getProgressBar().setVisible(true);
        } else {
            getProgressBar().setVisible(false);
        }

        getProgressBar().repaint();
    }

    /**
     * Sets a label-set to be shown in the status bar. This method it is not
     * intended to be used directly, because the set will be overwritten when
     * the selected
     * window changes. Use {@link MainFrame#setStatusBarLabels(Class, Label[])}
     * to permanently associate a label set with a window.
     * 
     * @param labels
     *            The labels to set.
     * @see MainFrame#setStatusBarLabels(Class, Label[])
     */
    public void setLabelSet(Label[] labels) {
        removeAllLabels();
        idLabel.clear();

        for (int i = 0; i < labels.length; i++) {
            // Set an initial text so the getPreferredSize works as expected
            JLabel lbl = new JLabel();
            lbl.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            lbl.setName(labels[i].getId());
            controlContainer.add(lbl);

            /*
             * if (i != labels.length - 1){
             * this.add(new JSeparator(JSeparator.VERTICAL));
             * }
             */
            idLabel.put(labels[i].getId(), lbl);
        }

        JLabel[] configlabels =
            (JLabel[]) idLabel.values().toArray(new JLabel[0]);
        widthlabels = new int[configlabels.length];

        for (int i = 0; i < labels.length; i++) {
            widthlabels[i] = configlabels[i].getWidth();
        }

        this.validate();
    }

    /**
     * Hides the empty labels and adjust the space in the bar.
     */
    public void ajustar() {
        if (widthlabels == null) {
            return;
        }

        JLabel[] labels = (JLabel[]) idLabel.values().toArray(new JLabel[0]);

        /*
         * double total = 1;
         * for (int i = 0; i < widthlabels.length; i++) {
         * if (labels[i].getText().compareTo("") != 0) {
         * total += widthlabels[i];
         * }
         * }
         * double p = (ws - lblTexto.getWidth() - 20) / total;
         */
        for (int i = 0; i < labels.length; i++) {
            JLabel label = (JLabel) labels[i];

            if (label.getText().compareTo("") != 0) {
                label.setVisible(true);
            } else {
                label.setVisible(false);
            }
        }
        validate();
    }

    /**
     * Removes all the labels from the status bar. It does not remove the
     * controls.
     */
    private void removeAllLabels() {
        Component[] controlArray = controlContainer.getComponents();

        for (int i = 0; i < controlArray.length; i++) {
            if ((controlArray[i] != lblIcon) && (controlArray[i] != lblTexto)
                && !(controlArray[i] instanceof IControl)
                && (controlArray[i] instanceof JLabel)) {
                controlContainer.remove(controlArray[i]);
            }
        }
    }

    /**
     * Sets the text of the provided label.
     * 
     * @param id
     *            The ID of the label to modify. It is defined in the
     *            config.xml file
     * @param msg
     *            The message to show in the label
     */
    public void setMessage(final String id, final String msg) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	setMessage(id, msg);
                }
            });
            return;
        }

        JLabel lbl = (JLabel) idLabel.get(id);

        if (lbl != null) {
        	Dimension originalPreferredSize = lbl.getPreferredSize();
            lbl.setText(msg);
            // Set preferred size to null just in case it has been set
            // previously, as we want it to be calculated from the text size.
            lbl.setPreferredSize(null);
            // Allow only to increase label width, to avoid too much ugly 
            // width changes in the status bar components.
            if (lbl.getPreferredSize().width < originalPreferredSize.width) {
            	lbl.setPreferredSize(originalPreferredSize);
            }
        } else {
            // try with controls
            try {
                IControl control = (IControl) controls.get(id);
                if (control != null)
                    control.setValue(msg);
            } catch (ClassCastException ex) {
                logger.debug("no label called " + id);
            }
        }

        ajustar();
    }

    /**
     * Sets the control identified by 'id' with the provided value.
     * 
     * @param id
     *            The ID of the control to modify
     * @param value
     *            The value to set in the control
     */
    public void setControlValue(String id, String value) {
        IControl control = (IControl) controls.get(id);
        if (control != null) {
            control.setValue(value);
        } else {
            logger.debug("NewStatusBar -- no control called " + id);
        }
    }

    /**
     * This method initializes the progressBar and gets it.
     * 
     * @return javax.swing.JProgressBar
     */
    private JProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = new JProgressBar();
            // progressBar.setPreferredSize(new java.awt.Dimension(100, 14));
            progressBar.setVisible(false);
            progressBar.setMinimum(0);
            progressBar.setMaximum(100);
            progressBar.setValue(50);
        }

        return progressBar;
    }

    private JTasksStatus getTasksStatus() {
        if (tasksStatus == null) {
            tasksStatus =
                ToolsSwingLocator.getTaskStatusSwingManager()
                    .createJTasksStatus();
            tasksStatus.setVisible(true);
        }
        return tasksStatus;
    }

    /**
     * Sets the width of the main message label.
     * 
     * @param d
     *            The width ob the main label
     * @deprecated
     */
    public void setFixedLabelWidth(double d) {
        lblTexto.setPreferredSize(new Dimension((int) d, lblTexto.getHeight()));
    }

    /**
     * Adds a control to the status bar
     * 
     * @param id
     *            The ID of the control, useful to later retrive it or set its
     *            value
     * @param control
     *            The control to add
     */
    public void addControl(String id, Component control) {
        if (!controls.containsKey(control.getName())) {
            controls.put(control.getName(), control);
            controlContainer.add(control);
        } else {
            logger.debug("NewStatusBar.addControl -- control 'id' already exists" + id);
        }
    }

    /**
     * Remove a control from the status bar
     * 
     * @param id
     *            The ID of the control to get
     */
    public Component removeControl(String id) {
        try {
            Component component = (Component) controls.get(id);
            controlContainer.remove(component);
            controls.remove(id);
            return component;
        } catch (ClassCastException ex) {
            logger.debug("NewStatusBar.removeControl -- control " + id
                + "doesn't exist");
        }
        return null;
    }

    /**
     * Gets a control from the status bar
     * 
     * @param id
     *            The ID of the control to get
     */
    public Component getControl(String id) {
        return (Component) controls.get(id);
    }
} // @jve:decl-index=0:visual-constraint="10,10"
