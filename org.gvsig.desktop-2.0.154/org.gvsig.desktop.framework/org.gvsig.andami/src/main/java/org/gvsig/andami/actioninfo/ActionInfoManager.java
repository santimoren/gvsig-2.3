/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.actioninfo;

import java.util.Iterator;

import org.gvsig.andami.plugins.IExtension;

public interface ActionInfoManager {

	/**
	 * Create a new action and return.
	 *
	 * @param extension, class that execute the action, extends IExtension
	 * @param name, name of the action
	 * @param text, text to use to represent the action.
	 * @param command, command used to execute the action
	 * @param icon, icon used to represent the action
	 * @param accelerator, the key which invokes the action
	 * @param position, position of the action when it is represented in the user interface
	 * @param tip, tooltip to associate to the action
	 * @return
	 */
    public ActionInfo createAction(Class<? extends IExtension> extension, String name, String text, String command, String icon, String accelerator, long position, String tip);

    public ActionInfo createAction(IExtension extension, String name, String text, String command, String icon, String accelerator, long position, String tip);

    /**
     * Register the action in the actions of the system, and return the action
     * that is registered.
     *
     * If an action with the same name is in the system then actions are merged
     * and the merged action is returned and remain in the system.
     *
     * @param action
     */
    public ActionInfo registerAction(ActionInfo action);

    /**
     * Retrieve an action by name
     *
     * @param name
     * @return the action associated to this name
     */
    public ActionInfo getAction(String name);

    /**
     * Return an iterator over all registered actions.
     *
     * @return iterator over al acctions
     */
    public Iterator<ActionInfo> getActions();

    public ActionInfoStatusCache createActionStatusCache();

    /**
     * Redirect the action source to the target.
     *
     * @param sourceName of action
     * @param targetName of action
     */
    public void redirect(String sourceName, String targetName);

    public void execute(String actionName, Object[] parameters);

    public ActionInfo getTranslated(ActionInfo actionInfo);
}
