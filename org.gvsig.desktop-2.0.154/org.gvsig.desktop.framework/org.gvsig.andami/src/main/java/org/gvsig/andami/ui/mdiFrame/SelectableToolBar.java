/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.Component;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import org.apache.commons.lang3.StringUtils;


/**
 * Caja de herramientas seleccionables
 */
public class SelectableToolBar extends JToolBar {
	private static final long serialVersionUID = 1L;
	/**
     * Andami visibility: determines whether this toolbar should
     * be shown by Andami. If it's false, the toolbar will be
     * hidden even if its associated extension is visible.
     */
    private boolean _visible;

    /**
     * Creates a new SelectableToolBar object.
     *
     * @param titulo T�tulo de la barra
     */
    public SelectableToolBar(String titulo) {
        super(titulo);
    }

    /**
     * A�ade un boton a la caja
     *
     * @param btn bot�n a a�adir.
     */
    public void addButton(ButtonGroup group, JToggleButton btn) {
    	group.add(btn);
        add(btn);
    }

    /**
     * Selects a toggleButton in this toolBar by ActionCommand
     * @param actionCommand
     */
    public void setSelectedTool(String actionCommand){
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof JToggleButton)
            {
                JToggleButton toggleButton = (JToggleButton) getComponent(i);
                String aux = toggleButton.getActionCommand();
                if (aux != null)
                  if (aux.equals(actionCommand)){
                        toggleButton.setSelected(true);
                      }
            }
        }

    }
    
    /**
     * Sets whether or not this toolbar should
     * be shown by Andami. If it's false, the toolbar will be
     * hidden even if its associated extension is visible.
     * 
     * @param visible
     */
    public void setAndamiVisibility(boolean visible) {
    	_visible = visible;
    }
    
    /**
     * Gets whether this toolbar should
     * be shown by Andami. If it's false, the toolbar will be
     * hidden even if its associated extension is visible.
     * 
     * @return <code>true</code> if the toolbar should be shown
     * when it has some visible button, <code>false</code> if
     * the toolbar should be hidden even it it contains some
     * buttons that should be visible according to its associated
     * extension.
     */
    public boolean getAndamiVisibility() {
    	return _visible;
    }
    
    public Component findComponent(String name) {
        if( StringUtils.isBlank(name) ) {
            return null;
        }
        Object lock = this.getTreeLock();
        synchronized(lock) {
            for( int i=0; i<this.getComponentCount(); i++ ) {
                Component c1= this.getComponent(i);
                if( name.equalsIgnoreCase(c1.getName()) ) {
                    return c1;
                }
            }
        }
        return null;
    }
}
