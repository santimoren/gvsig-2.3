/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.messages.NotificationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class AndamiEventQueue extends EventQueue {
	private static Logger logger = LoggerFactory.getLogger(AndamiEventQueue.class);
	
	protected void dispatchEvent(AWTEvent event){
		try{
			super.dispatchEvent(event);
		} catch(RuntimeException e){
		    
		    if (canShowWindow(e, event)) {
		        NotificationManager.addError(e);//Messages.getString("PluginServices.Bug en el c�digo"), e);
		    } else {
                try {
                    PluginServices.getMainFrame().getStatusBar().message(e.getMessage(), JOptionPane.ERROR_MESSAGE);
                } catch(Throwable ex) {
                    // Ignora cualquier error que se produzca intentando mostrar el mensaje de error y nos
                    // conformaremos con que este en el log.
                }
                logger.info("Error dispaching event",e);
			}
			
		} catch (Error e){
			NotificationManager.addError(
			    Messages.getString("Error de la applicacion.  \nEs conveniente que salga de la aplicaci�n\n\n"+e.getLocalizedMessage()), e);
		}
	}
	
	private boolean canShowWindow(Throwable th, AWTEvent event) {
	    
	    try {
	        if( event instanceof MouseEvent ) {
	            MouseEvent me = (MouseEvent)event;
	            if( me.getButton() == 0 && me.getClickCount()==0 ) {
	                // Intentamos que los errores que se produzcan con los eventos de movimiento del raton
                    // no provoquen la aparicion de la ventana de errores ya que llega a bloquear al usuario.
                    // Intentaremos sacarlos en la barra de estado si podemos.
	                return false;
	            }
	        }
	        StackTraceElement[] stack_ee = th.getStackTrace();
	        for (int i=0; i<stack_ee.length; i++) {
	            StackTraceElement se = stack_ee[i];
	            if (se.getClassName().startsWith("org.gvsig.fmap.mapcontrol.MapControl")) {
	                return false;
	            }
	        }
	    } catch (Throwable t) {
	        
	    }
        return true;
	    
	}
}
