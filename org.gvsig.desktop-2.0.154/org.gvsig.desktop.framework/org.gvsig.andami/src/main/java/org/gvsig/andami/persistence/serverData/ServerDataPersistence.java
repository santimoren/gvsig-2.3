/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.persistence.serverData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.utils.swing.jcomboServer.ServerData;

/**
 * This class is used to save a list of servers (using the Andami persistence
 * model) to the plugins-persistence.xml file. It has methods to create a set of
 * ServerData objects from an xml file. It can also save a set of ServerData
 * objects in an xml file.
 *
 * @see es.gva.cit.catalogClient.utils.comboserver.ServerData
 * @author Jorge Piera Llodra (piera_jor@gva.es)
 */
public class ServerDataPersistence implements Persistent, Comparable<ServerDataPersistence> {

    public static final String PERSISTENT_NAME = "ServerDataList_Persistent";
    public static final String PERSISTENT_DESCRIPTION = "ServerDataList Persistent";

    private String serviceType = null;
    private List<ServerData> serverList = null;

    /**
     * Constructor
     *
     * @param sevice Type Service type to load
     */
    public ServerDataPersistence(String serviceType) {
        this.serviceType = serviceType;
        this.serverList = new ArrayList<ServerData>();
    }

    public ServerDataPersistence() {
        this.serverList = new ArrayList<ServerData>();
        this.serviceType = "UNKNOW";
    }

    /**
     * This method adds a ServerData using the Andami persistence model. If the
     * server exists just actualizes the type and subtype fileds and changes the
     * last access value to the current time.
     *
     * @param server ServerData
     */
    public void addServerData(ServerData server) {
        String address = server.getServerAddress();
        for (int i = 0; i < serverList.size(); i++) {
            ServerData sd = serverList.get(i);
            if (sd.getServerAddress().equals(address)) {
                // Already exist, update  it
                serverList.set(i, server);
                return;
            }
        }
        // Add the new server
        serverList.add(server);
    }

    /**
     * Returns true if exists a server in this list
     *
     * @param address
     * @return
     */
    public boolean existsServer(String address) {
        return contains(address);
    }

    /**
     * This method returns an array of ServerData objects that have been saved
     * using the gvsig tools persistence model.
     *
     * @return ServerData[]
     */
    public ServerData[] getArrayOfServerData() {
        return (ServerData[]) serverList.toArray(new ServerData[serverList.size()]);
    }

    public List<ServerData> getServerData() {
        return serverList;
    }

    public void setArrayOfServerData(ServerData[] servers) {
        if (servers != null) {
            serverList = Arrays.asList(servers);
        }
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("serverData", getArrayOfServerData());
        state.set("serviceType", serviceType);
    }

    public void loadFromState(PersistentState state)
            throws PersistenceException {
        if (state.get("serverData") instanceof List) {
            serverList = (List<ServerData>) state.get("serverData");
        }
        serviceType = state.getString("serviceType");
    }

    public static void registerPersistence() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynStruct definition = manager.getDefinition(PERSISTENT_NAME);
        if (definition == null) {
            definition = manager.addDefinition(
                    ServerDataPersistence.class,
                    PERSISTENT_NAME,
                    PERSISTENT_DESCRIPTION,
                    null,
                    null
            );
        }

        definition.addDynFieldList("serverData").setClassOfItems(ServerData.class).setMandatory(false);
        definition.addDynFieldString("serviceType").setMandatory(true);
    }

    public int compareTo(ServerDataPersistence a) {
        if (a.getServiceType().equals(getServiceType())) {
            if (getServerData().size() != a.getServerData().size()) {
                return -1;
            }
            for (int i = 0; i < getServerData().size(); i++) {
                ServerData serverData = getServerData().get(i);
                if (!a.existsServer(serverData.getServerAddress())) {
                    return -1;
                }
            }
        }
        return 0;
    }

    public boolean isEmpty() {
        if (serverList == null) {
            return true;
        }
        return serverList.isEmpty();
    }

    public void remove(String serverAddress) {
        if (StringUtils.isBlank(serverAddress)) {
            return;
        }
        Iterator<ServerData> it = getServerData().iterator();
        while (it.hasNext()) {
            ServerData x = it.next();
            if (serverAddress.equalsIgnoreCase(x.getServerAddress())) {
                it.remove();
                return;
            }
        }
    }

    public void add(String server, String protocol) {
        if (StringUtils.isBlank(server)) {
            return;
        }
        this.add(new ServerData(server, protocol));
    }

    public void add(ServerData server) {
        String address = server.getServerAddress().trim();
        for (int i = 0; i < getServerData().size(); i++) {
            ServerData sd = getServerData().get(i);
            if (sd.getServerAddress().trim().equals(address)) {
                getServerData().set(i, server);
                return;
            }
        }
        getServerData().add(server);
    }

    public boolean contains(String serverAddress) {
        if (StringUtils.isBlank(serverAddress)) {
            return false;
        }
        serverAddress = serverAddress.trim();
        for (int i = 0; i < getServerData().size(); i++) {
            String address = getServerData().get(i).getServerAddress().trim();
            if (serverAddress.equalsIgnoreCase(address)) {
                return true;
            }
        }
        return false;
    }

}
