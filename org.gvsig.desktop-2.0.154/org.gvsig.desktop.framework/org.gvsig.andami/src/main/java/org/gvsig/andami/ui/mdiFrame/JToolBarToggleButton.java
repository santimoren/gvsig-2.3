/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import javax.swing.Action;
import javax.swing.Icon;
import org.gvsig.andami.ui.mdiFrame.TranslatableButtonHelper.TranslatableButton;

public class JToolBarToggleButton extends javax.swing.JToggleButton implements EnableTextSupport, TranslatableButton {

    private TranslatableButtonHelper i18nHelper = new TranslatableButtonHelper();
    private String enableText;
    private String toolTip;

    public JToolBarToggleButton() {
        super();
    }

    public JToolBarToggleButton(String text) {
        super(text);
    }

    public JToolBarToggleButton(String text, boolean selected) {
        super(text, selected);
    }

    public JToolBarToggleButton(String text, Icon icon) {
        super(text, icon);
    }

    public JToolBarToggleButton(String text, Icon icon, boolean selected) {
        super(text, icon, selected);
    }

    public JToolBarToggleButton(Action a) {
        super(a);
    }

    public JToolBarToggleButton(Icon icon) {
        super(icon);
    }

    public JToolBarToggleButton(Icon icon, boolean selected) {
        super(icon, selected);
    }

    /**
     * @return Returns the enableText.
     */
    public String getEnableText() {
        return enableText;
    }

    /**
     * @param enableText The enableText to set.
     */
    public void setEnableText(String enableText) {
        this.enableText = enableText;
    }

    public void setEnabled(boolean aFlag) {
        super.setEnabled(aFlag);
        if ( aFlag ) {
            setToolTipText(toolTip);
        } else {
            setToolTipText(enableText);
        }
    }

    public void setToolTip(String text) {
        toolTip = text;
    }

    public void setToolTipKey(String key) {
        this.i18nHelper.setTooltip(key);
    }

    public void setTextKey(String key) {
        this.i18nHelper.setText(key);
    }

    public void translate() {
        if ( this.i18nHelper.needTranslation() ) {
            this.i18nHelper.translate();
            this.setText(this.i18nHelper.getText());
            this.setToolTip(this.i18nHelper.getTooltip());
        }
    }

}
