/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: PopupMenu.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PopupMenu.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class PopupMenu implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _name
     */
    private java.lang.String _name;

    /**
     * Field _menuList
     */
    private java.util.Vector _menuList;


      //----------------/
     //- Constructors -/
    //----------------/

    public PopupMenu() {
        super();
        _menuList = new Vector();
    } //-- com.iver.andami.plugins.config.generate.PopupMenu()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addMenu
     * 
     * @param vMenu
     */
    public void addMenu(org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _menuList.addElement(vMenu);
    } //-- void addMenu(com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method addMenu
     * 
     * @param index
     * @param vMenu
     */
    public void addMenu(int index, org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _menuList.insertElementAt(vMenu, index);
    } //-- void addMenu(int, com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method enumerateMenu
     */
    public java.util.Enumeration enumerateMenu()
    {
        return _menuList.elements();
    } //-- java.util.Enumeration enumerateMenu() 

    /**
     * Method getMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Menu getMenu(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _menuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.Menu) _menuList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Menu getMenu(int) 

    /**
     * Method getMenu
     */
    public org.gvsig.andami.plugins.config.generate.Menu[] getMenu()
    {
        int size = _menuList.size();
        org.gvsig.andami.plugins.config.generate.Menu[] mArray = new org.gvsig.andami.plugins.config.generate.Menu[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.Menu) _menuList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.Menu[] getMenu() 

    /**
     * Method getMenuCount
     */
    public int getMenuCount()
    {
        return _menuList.size();
    } //-- int getMenuCount() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllMenu
     */
    public void removeAllMenu()
    {
        _menuList.removeAllElements();
    } //-- void removeAllMenu() 

    /**
     * Method removeMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Menu removeMenu(int index)
    {
        java.lang.Object obj = _menuList.elementAt(index);
        _menuList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.Menu) obj;
    } //-- com.iver.andami.plugins.config.generate.Menu removeMenu(int) 

    /**
     * Method setMenu
     * 
     * @param index
     * @param vMenu
     */
    public void setMenu(int index, org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _menuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _menuList.setElementAt(vMenu, index);
    } //-- void setMenu(int, com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method setMenu
     * 
     * @param menuArray
     */
    public void setMenu(org.gvsig.andami.plugins.config.generate.Menu[] menuArray)
    {
        //-- copy array
        _menuList.removeAllElements();
        for (int i = 0; i < menuArray.length; i++) {
            _menuList.addElement(menuArray[i]);
        }
    } //-- void setMenu(com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.PopupMenu) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.PopupMenu.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
