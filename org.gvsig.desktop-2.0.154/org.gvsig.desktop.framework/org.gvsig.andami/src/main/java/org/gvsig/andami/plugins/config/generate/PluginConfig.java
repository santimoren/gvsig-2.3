/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: PluginConfig.java 40403 2013-06-13 06:24:04Z jjdelcerro $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.File;
import java.util.Vector;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class PluginConfig.
 * 
 * @version $Revision: 40403 $ $Date: 2013-06-13 08:24:04 +0200 (jue, 13 jun 2013) $
 */
public class PluginConfig implements java.io.Serializable {

	private static final Logger logger = LoggerFactory.getLogger(PluginConfig.class);

      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _updateUrl
     */
    private java.lang.String _updateUrl;

    /**
     * Field _icon
     */
    private org.gvsig.andami.plugins.config.generate.Icon _icon;

    /**
     * Field _dependsList
     */
    private java.util.Vector _dependsList;

    /**
     * Field _alternativeNameList
     */
    private java.util.Vector _alternativeNamesList;

    /**
     * Field _resourceBundle
     */
    private org.gvsig.andami.plugins.config.generate.ResourceBundle _resourceBundle;

    /**
     * Field _labelSetList
     */
    private java.util.Vector _labelSetList;

    /**
     * Field _libraries
     */
    private org.gvsig.andami.plugins.config.generate.Libraries _libraries;

    /**
     * Field _popupMenus
     */
    private org.gvsig.andami.plugins.config.generate.PopupMenus _popupMenus;

    /**
     * Field _extensions
     */
    private org.gvsig.andami.plugins.config.generate.Extensions _extensions;


      //----------------/
     //- Constructors -/
    //----------------/

    public PluginConfig() {
        super();
        _dependsList = new Vector();
        _alternativeNamesList = new Vector();
        _labelSetList = new Vector();
    } //-- com.iver.andami.plugins.config.generate.PluginConfig()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addDepends
     * 
     * @param vDepends
     */
    public void addDepends(org.gvsig.andami.plugins.config.generate.Depends vDepends)
        throws java.lang.IndexOutOfBoundsException
    {
        _dependsList.addElement(vDepends);
    	if( "org.gvsig.app".equals(vDepends.getPluginName())  ) {
    		logger.info("Deprecated dependency 'org.gvsig.app' add optional depdency with 'org.gvsig.shp.app.mainplugin' to ensure compatibility with gvSIG 2.0.");
    		org.gvsig.andami.plugins.config.generate.Depends dep2 = new org.gvsig.andami.plugins.config.generate.Depends();
    		dep2.setOptional(true);
    		dep2.setPluginName("org.gvsig.shp.app.mainplugin");
            _dependsList.addElement(dep2);
    	}
    } //-- void addDepends(com.iver.andami.plugins.config.generate.Depends) 

    /**
     * Method addDepends
     * 
     * @param index
     * @param vDepends
     */
    public void addDepends(int index, org.gvsig.andami.plugins.config.generate.Depends vDepends)
        throws java.lang.IndexOutOfBoundsException
    {
        _dependsList.insertElementAt(vDepends, index);
    } //-- void addDepends(int, com.iver.andami.plugins.config.generate.Depends) 

    /**
     * Method addAlternativeName
     * 
     * @param vAlternativeName
     */
    public void addAlternativeNames(org.gvsig.andami.plugins.config.generate.AlternativeNames vAlternativeName)
        throws java.lang.IndexOutOfBoundsException
    {
        _alternativeNamesList.addElement(vAlternativeName);
    } //-- void addAlternativeName(com.iver.andami.plugins.config.generate.AlternativeName) 

    /**
     * Method addAlternativeName
     * 
     * @param index
     * @param vAlternativeName
     */
    public void addAlternativeNames(int index, org.gvsig.andami.plugins.config.generate.AlternativeNames vAlternativeName)
        throws java.lang.IndexOutOfBoundsException
    {
        _alternativeNamesList.insertElementAt(vAlternativeName, index);
    } //-- void addDepends(int, com.iver.andami.plugins.config.generate.Depends) 
    
    /**
     * Method addLabelSet
     * 
     * @param vLabelSet
     */
    public void addLabelSet(org.gvsig.andami.plugins.config.generate.LabelSet vLabelSet)
        throws java.lang.IndexOutOfBoundsException
    {
        _labelSetList.addElement(vLabelSet);
    } //-- void addLabelSet(com.iver.andami.plugins.config.generate.LabelSet) 

    /**
     * Method addLabelSet
     * 
     * @param index
     * @param vLabelSet
     */
    public void addLabelSet(int index, org.gvsig.andami.plugins.config.generate.LabelSet vLabelSet)
        throws java.lang.IndexOutOfBoundsException
    {
        _labelSetList.insertElementAt(vLabelSet, index);
    } //-- void addLabelSet(int, com.iver.andami.plugins.config.generate.LabelSet) 

    /**
     * Method enumerateDepends
     */
    public java.util.Enumeration enumerateDepends()
    {
        return _dependsList.elements();
    } //-- java.util.Enumeration enumerateDepends() 

    /**
     * Method enumerateAlternativeNames
     */
    public java.util.Enumeration enumerateAlternativeNames()
    {
        return _alternativeNamesList.elements();
    } //-- java.util.Enumeration enumerateAlternativeNames() 

    /**
     * Method enumerateLabelSet
     */
    public java.util.Enumeration enumerateLabelSet()
    {
        return _labelSetList.elements();
    } //-- java.util.Enumeration enumerateLabelSet() 

    /**
     * Method getDepends
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Depends getDepends(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _dependsList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.Depends) _dependsList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Depends getDepends(int) 

    /**
     * Method getDepends
     */
    public org.gvsig.andami.plugins.config.generate.Depends[] getDepends()
    {
        int size = _dependsList.size();
        org.gvsig.andami.plugins.config.generate.Depends[] mArray = new org.gvsig.andami.plugins.config.generate.Depends[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.Depends) _dependsList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.Depends[] getDepends() 

    /**
     * Method getDependsCount
     */
    public int getDependsCount()
    {
        return _dependsList.size();
    } //-- int getDependsCount() 

    /**
     * Method getAlternativeNames
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.AlternativeNames getAlternativeNames(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _alternativeNamesList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.AlternativeNames) _alternativeNamesList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Depends getAlternativeNames(int) 

    /**
     * Method getDepends
     */
    public org.gvsig.andami.plugins.config.generate.AlternativeNames[] getAlternativeNames()
    {
        int size = _alternativeNamesList.size();
        org.gvsig.andami.plugins.config.generate.AlternativeNames[] mArray = new org.gvsig.andami.plugins.config.generate.AlternativeNames[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.AlternativeNames) _alternativeNamesList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.AlternativeNames[] getAlternativeNames() 

    /**
     * Method getAlternativeNamesCount
     */
    public int getAlternativeNamesCount()
    {
        return _alternativeNamesList.size();
    } //-- int getAlternativeNamesCount() 

    
    /**s
     * Returns the value of field 'extensions'.
     * 
     * @return the value of field 'extensions'.
     */
    public org.gvsig.andami.plugins.config.generate.Extensions getExtensions()
    {
        return this._extensions;
    } //-- com.iver.andami.plugins.config.generate.Extensions getExtensions() 

    /**
     * Returns the value of field 'icon'.
     * 
     * @return the value of field 'icon'.
     */
    public org.gvsig.andami.plugins.config.generate.Icon getIcon()
    {
        return this._icon;
    } //-- com.iver.andami.plugins.config.generate.Icon getIcon() 

    /**
     * Method getLabelSet
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.LabelSet getLabelSet(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _labelSetList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.LabelSet) _labelSetList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.LabelSet getLabelSet(int) 

    /**
     * Method getLabelSet
     */
    public org.gvsig.andami.plugins.config.generate.LabelSet[] getLabelSet()
    {
        int size = _labelSetList.size();
        org.gvsig.andami.plugins.config.generate.LabelSet[] mArray = new org.gvsig.andami.plugins.config.generate.LabelSet[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.LabelSet) _labelSetList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.LabelSet[] getLabelSet() 

    /**
     * Method getLabelSetCount
     */
    public int getLabelSetCount()
    {
        return _labelSetList.size();
    } //-- int getLabelSetCount() 

    /**
     * Returns the value of field 'libraries'.
     * 
     * @return the value of field 'libraries'.
     */
    public org.gvsig.andami.plugins.config.generate.Libraries getLibraries()
    {
        return this._libraries;
    } //-- com.iver.andami.plugins.config.generate.Libraries getLibraries() 

    /**
     * Returns the value of field 'popupMenus'.
     * 
     * @return the value of field 'popupMenus'.
     */
    public org.gvsig.andami.plugins.config.generate.PopupMenus getPopupMenus()
    {
        return this._popupMenus;
    } //-- com.iver.andami.plugins.config.generate.PopupMenus getPopupMenus() 

    /**
     * Returns the value of field 'resourceBundle'.
     * 
     * @return the value of field 'resourceBundle'.
     */
    public org.gvsig.andami.plugins.config.generate.ResourceBundle getResourceBundle()
    {
        return this._resourceBundle;
    } //-- com.iver.andami.plugins.config.generate.ResourceBundle getResourceBundle() 

    /**
     * Returns the value of field 'updateUrl'.
     * 
     * @return the value of field 'updateUrl'.
     */
    public java.lang.String getUpdateUrl()
    {
        return this._updateUrl;
    } //-- java.lang.String getUpdateUrl() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllDepends
     */
    public void removeAllDepends()
    {
        _dependsList.removeAllElements();
    } //-- void removeAllDepends() 

    /**
     * Method removeAllAlternativeNames
     */
    public void removeAllAlternativeNames()
    {
        _alternativeNamesList.removeAllElements();
    } //-- void removeAllAlternativeNames() 

    /**
     * Method removeAllLabelSet
     */
    public void removeAllLabelSet()
    {
        _labelSetList.removeAllElements();
    } //-- void removeAllLabelSet() 

    /**
     * Method removeDepends
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Depends removeDepends(int index)
    {
        java.lang.Object obj = _dependsList.elementAt(index);
        _dependsList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.Depends) obj;
    } //-- com.iver.andami.plugins.config.generate.Depends removeDepends(int) 

    /**
     * Method removeAlternativeNames
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.AlternativeNames removeAlternativeNames(int index)
    {
        java.lang.Object obj = _alternativeNamesList.elementAt(index);
        _alternativeNamesList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.AlternativeNames) obj;
    } //-- com.iver.andami.plugins.config.generate.Depends removeAlternativeNames(int) 

    /**
     * Method removeLabelSet
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.LabelSet removeLabelSet(int index)
    {
        java.lang.Object obj = _labelSetList.elementAt(index);
        _labelSetList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.LabelSet) obj;
    } //-- com.iver.andami.plugins.config.generate.LabelSet removeLabelSet(int) 

    /**
     * Method setDepends
     * 
     * @param index
     * @param vDepends
     */
    public void setDepends(int index, org.gvsig.andami.plugins.config.generate.Depends vDepends)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _dependsList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _dependsList.setElementAt(vDepends, index);
    } //-- void setDepends(int, com.iver.andami.plugins.config.generate.Depends) 

    /**
     * Method setDepends
     * 
     * @param dependsArray
     */
    public void setDepends(org.gvsig.andami.plugins.config.generate.Depends[] dependsArray)
    {
        //-- copy array
        _dependsList.removeAllElements();
        for (int i = 0; i < dependsArray.length; i++) {
            _dependsList.addElement(dependsArray[i]);
        }
    } //-- void setDepends(com.iver.andami.plugins.config.generate.Depends) 
    
    /**
     * Method setAlternativeNames
     * 
     * @param index
     * @param vAlternativeNames
     */
    public void setAlternativeNames(int index, org.gvsig.andami.plugins.config.generate.AlternativeNames vAlternativeNames)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _alternativeNamesList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _alternativeNamesList.setElementAt(vAlternativeNames, index);
    } //-- void setAlternativeNames(int, com.iver.andami.plugins.config.generate.AlternativeNames) 

    /**
     * Method setAlternativeNames
     * 
     * @param AlternativeNamesArray
     */
    public void setAlternativeNames(org.gvsig.andami.plugins.config.generate.AlternativeNames[] alternativeNamesArray)
    {
        //-- copy array
        _alternativeNamesList.removeAllElements();
        for (int i = 0; i < alternativeNamesArray.length; i++) {
            _alternativeNamesList.addElement(alternativeNamesArray[i]);
        }
    } //-- void setAlternativeNames(com.iver.andami.plugins.config.generate.AlternativeNames) 
    
    /**
     * Sets the value of field 'extensions'.
     * 
     * @param extensions the value of field 'extensions'.
     */
    public void setExtensions(org.gvsig.andami.plugins.config.generate.Extensions extensions)
    {
        this._extensions = extensions;
    } //-- void setExtensions(com.iver.andami.plugins.config.generate.Extensions) 

    /**
     * Sets the value of field 'icon'.
     * 
     * @param icon the value of field 'icon'.
     */
    public void setIcon(org.gvsig.andami.plugins.config.generate.Icon icon)
    {
        this._icon = icon;
    } //-- void setIcon(com.iver.andami.plugins.config.generate.Icon) 

    /**
     * Method setLabelSet
     * 
     * @param index
     * @param vLabelSet
     */
    public void setLabelSet(int index, org.gvsig.andami.plugins.config.generate.LabelSet vLabelSet)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _labelSetList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _labelSetList.setElementAt(vLabelSet, index);
    } //-- void setLabelSet(int, com.iver.andami.plugins.config.generate.LabelSet) 

    /**
     * Method setLabelSet
     * 
     * @param labelSetArray
     */
    public void setLabelSet(org.gvsig.andami.plugins.config.generate.LabelSet[] labelSetArray)
    {
        //-- copy array
        _labelSetList.removeAllElements();
        for (int i = 0; i < labelSetArray.length; i++) {
            _labelSetList.addElement(labelSetArray[i]);
        }
    } //-- void setLabelSet(com.iver.andami.plugins.config.generate.LabelSet) 

    /**
     * Sets the value of field 'libraries'.
     * 
     * @param libraries the value of field 'libraries'.
     */
    public void setLibraries(org.gvsig.andami.plugins.config.generate.Libraries libraries)
    {
        this._libraries = libraries;
    } //-- void setLibraries(com.iver.andami.plugins.config.generate.Libraries) 

    /**
     * Sets the value of field 'popupMenus'.
     * 
     * @param popupMenus the value of field 'popupMenus'.
     */
    public void setPopupMenus(org.gvsig.andami.plugins.config.generate.PopupMenus popupMenus)
    {
        this._popupMenus = popupMenus;
    } //-- void setPopupMenus(com.iver.andami.plugins.config.generate.PopupMenus) 

    /**
     * Sets the value of field 'resourceBundle'.
     * 
     * @param resourceBundle the value of field 'resourceBundle'.
     */
    public void setResourceBundle(org.gvsig.andami.plugins.config.generate.ResourceBundle resourceBundle)
    {
        this._resourceBundle = resourceBundle;
    } //-- void setResourceBundle(com.iver.andami.plugins.config.generate.ResourceBundle) 

    /**
     * Sets the value of field 'updateUrl'.
     * 
     * @param updateUrl the value of field 'updateUrl'.
     */
    public void setUpdateUrl(java.lang.String updateUrl)
    {
        this._updateUrl = updateUrl;
    } //-- void setUpdateUrl(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.PluginConfig) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.PluginConfig.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

    
    private String pluginName = null;
    private File pluginFolder = null;
    
    public String getPluginName() {
        return this.pluginName;
    }
    
    public void setPluginName(String name) {
        this.pluginName = name;
    }
    
    public File getPluginFolder() {
        return this.pluginFolder;
    }
    
    public void setPluginFolder(File folder) {
        this.pluginFolder = folder;
    }
}
