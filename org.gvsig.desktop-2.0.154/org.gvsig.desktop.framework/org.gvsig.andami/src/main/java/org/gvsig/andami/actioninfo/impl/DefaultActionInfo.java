/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.actioninfo.impl;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

import org.apache.commons.io.FilenameUtils;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.plugins.ExclusiveUIExtension;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.ExtensionHelper;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.ui.mdiFrame.KeyMapping;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.identitymanagement.SimpleIdentity;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultActionInfo extends AbstractAction implements ActionInfo {

    /**
     *
     */
    private static final long serialVersionUID = 1620939552263334110L;

    private static Logger logger = LoggerFactory
            .getLogger(DefaultActionInfo.class);

    private Class<? extends IExtension> extensionClass;
    private IExtension extension;
    private String name;
    private String text;
    private String command;
    private String iconName;
    private String accelerator;
    private long position;
    private String tip;
    private List<ActionInfo> redirections;
    private boolean active;

    private Boolean previousEnabled = null;
    private SimpleIdentityManager identityManager;

    
    DefaultActionInfo(IExtension extension, String name,
            String text, String command, String icon, String accelerator,
            long position, String tip) {
        this.extensionClass = extension.getClass();
        this.extension = extension;
        this.name = name;
        this.text = emptyToNull(text);
        this.command = emptyToNull(command);
        this.iconName = emptyToNull(icon);
        this.accelerator = emptyToNull(accelerator);
        this.position = position;
        this.tip = emptyToNull(tip);
        this.redirections = null;
        this.active = true;

        fixIcon();
    }
    
    DefaultActionInfo(Class<? extends IExtension> extensionClass, String name,
            String text, String command, String icon, String accelerator,
            long position, String tip) {
        this.extensionClass = extensionClass;
        this.name = name;
        this.text = emptyToNull(text);
        this.command = emptyToNull(command);
        this.iconName = emptyToNull(icon);
        this.accelerator = emptyToNull(accelerator);
        this.position = position;
        this.tip = emptyToNull(tip);
        this.redirections = null;
        this.active = true;

        fixIcon();
    }

    public Object clone() throws CloneNotSupportedException {
        DefaultActionInfo other = (DefaultActionInfo) super.clone();
        if (other.redirections != null) {
            other.redirections = new ArrayList<ActionInfo>();
            other.redirections.addAll(this.redirections);
        }
        return other;
    }

    private void fixIcon() {
        if (iconName != null && (iconName.contains("/") || iconName.contains("."))) {
            // it's a file path
            String name = FilenameUtils.getBaseName(iconName);
            IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getDefault();
            URL resource = null;
            try {
                resource = this.extensionClass.getClassLoader().getResource(iconName);
            } catch (Exception e) {
                return;
            }
            if (resource == null) {
                return;
            }
            iconTheme.registerDefault(this.getPluginName(), "broken", name, null, resource);
            logger.info("Plugin " + this.getPluginName() + " contains icons out of icon theme (" + iconName + ")");
            iconName = name;
        }
    }

    private String emptyToNull(String s) {
        if (s == null) {
            return null;
        }
        return s.trim().length() < 0 ? null : s;
    }

    public Collection<ActionInfo> getRedirections() {
        if (this.redirections == null) {
            this.redirections = new ArrayList<ActionInfo>();
        }
        return this.redirections;
    }

    public void merge(ActionInfo other) {
        if (this.extensionClass == null) {
            this.extensionClass = other.getExtension().getClass();
            this.extension = other.getExtension();
        }
        if (this.text == null) {
            this.text = other.getLabel();
        }
        if (this.command == null) {
            this.command = other.getCommand();
        }
        if (this.iconName == null) {
            this.iconName = other.getIconName();
        }
        if (this.accelerator == null) {
            this.accelerator = other.getAccelerator();
        }
        if (this.position < 1) {
            this.position = other.getPosition();
        }
        if (this.tip == null) {
            this.tip = other.getTooltip();
        }

    }

    public PluginServices getPlugin() {
        if( this.extension instanceof Extension ) {
            return ((Extension)this.extension).getPlugin();
        }
        PluginServices plugin = PluginsLocator.getManager().getPlugin(
                this.extensionClass);
        return plugin;
    }

    public String getPluginName() {
        if (this.getPlugin() == null) {
            return null;
        }
        return this.getPlugin().getPluginName();
    }

    public IExtension getExtension() {
        if (this.extension == null) {
            this.extension = PluginsLocator.getManager().getExtension(
                    this.extensionClass);
        }
        return this.extension;
    }

    public String getExtensionName() {
        if (this.extensionClass == null) {
            return null;
        }
        return this.extensionClass.getName();
    }

    private SimpleIdentityManager getIdentityManager() {
        if( this.identityManager == null ) {
            this.identityManager = ToolsLocator.getIdentityManager();
        }
        return this.identityManager;
    }

    private SimpleIdentity getCurrentUser() {
        return this.getIdentityManager().getCurrentIdentity();
    }

    public boolean isVisible() {
        if( !this.getCurrentUser().isAuthorized(this.getName()) ) {
            return false;
        }
        if (!this.isActive()) {
            logger.info("isVisible(), action {} not active", this.getName());
            return false;
        }
        ActionInfo redirection = this.getRedirection();
        if (redirection != null) {
            return redirection.isVisible();
        }

        ExclusiveUIExtension eui = PluginsLocator.getManager()
                .getExclusiveUIExtension();
        if (eui == null) {
            return ExtensionHelper.isVisible(this.getExtension(), this.command);
        }
        return eui.isVisible(this.getExtension());
    }

    public boolean isEnabled() {
        boolean value;
        if( !this.getCurrentUser().isAuthorized(this.getName()) ) {
            return false;
        }
        if (!this.isActive()) {
            logger.info("isEnabled(), action {} not active", this.getName());
            value = false;
        } else {
            ActionInfo redirection = this.getRedirection();
            if (redirection != null) {
                value = true;
            } else {
                ExclusiveUIExtension eui = PluginsLocator.getManager()
                        .getExclusiveUIExtension();
                if (eui == null) {
                    value = ExtensionHelper.isEnabled(this.getExtension(), this.command);
                } else {
                    value = eui.isEnabled(this.getExtension());
                }
            }
        }
        if (this.previousEnabled == null || this.previousEnabled.booleanValue() != value) {
            this.setEnabled(value); // force call listeners
        }
        return value;
    }

    private ActionInfo getRedirection() {
        if (this.redirections == null) {
            return null;
        }
        for (int n = this.redirections.size() - 1; n >= 0; n--) {
            ActionInfo redirection = this.redirections.get(n);
            if (redirection.isEnabled()) {
                return redirection;
            }
        }
        return null;
    }

    public void execute() {
//		if (!this.isActive()) {
//			logger.info("execute(), action {} not active",  this.getName());
//			return;
//		}
        if( !this.getCurrentUser().isAuthorized(this.getName()) ) {
            logger.warn("Current user '"+this.getCurrentUser().getID()+"' not authorized to execute this action '"+this.getName()+"'.");
            return ;
        }
        ActionInfo redirection = this.getRedirection();
        if (redirection != null) {
            logger.info("{}.execute('{}') redirected", this.getPluginName()
                    + ":" + this.getExtensionName(), this.getCommand());
            redirection.execute();
            return;
        }
        logger.info("{}.execute('{}')",
                this.getPluginName() + ":" + this.getExtensionName(),
                this.getCommand());
        this.getExtension().execute(this.command);
    }

    public void execute(Object[] args) {
//		if (!this.isActive()) {
//			logger.info("execute(args), action {} not active", this.getName());
//			return;
//		}
        if( !this.getCurrentUser().isAuthorized(this.getName()) ) {
            logger.warn("Current user '"+this.getCurrentUser().getID()+"' not authorized to execute this action '"+this.getName()+"'.");
            return ;
        }
        ActionInfo redirection = this.getRedirection();
        if (redirection != null) {
            logger.info("{}.execute('{}', args) redirected", this.getPluginName()
                    + ":" + this.getExtensionName(), this.getCommand());
            redirection.execute(args);
            return;
        }
        logger.info("{}.execute('{}', Object[] args)",
                this.getPluginName() + ":" + this.getExtensionName(),
                this.getCommand());
        ExtensionHelper.execute(this.getExtension(), this.command, args);
    }

    public void execute(Object arg) {
        if (arg instanceof Object[]) {
            execute((Object[]) arg);
            return;
        }
        execute(new Object[]{arg});
    }

    public void execute(Map args) {
        if( !this.getCurrentUser().isAuthorized(this.getName()) ) {
            logger.warn("Current user '"+this.getCurrentUser().getID()+"' not authorized to execute this action '"+this.getName()+"'.");
            return ;
        }
        logger.info("{}.execute('{}', Map args)",
                this.getPluginName() + ":" + this.getExtensionName(),
                this.getCommand());
        ExtensionHelper.execute(this.getExtension(), this.command, new Object[]{args});
    }

    public void actionPerformed(ActionEvent arg0) {
        this.execute();
    }

    public String getName() {
        return this.name;
    }

    public String getLabel() {
        return this.text;
    }

    public String getCommand() {
        return this.command;
    }

    public String getIconName() {
        return this.iconName;
    }

    public ImageIcon getIcon() {
        IconTheme iconTheme = PluginServices.getIconTheme();
        return iconTheme.get(this.iconName);
    }

    public String getAccelerator() {
        return this.accelerator;
    }

    public KeyStroke getKeyStroke() {
        if (emptyToNull(this.accelerator) == null) {
            return null;
        }
        return KeyMapping.getKeyStroke(this.accelerator);
    }

    public long getPosition() {
        return this.position;
    }

    public String getTooltip() {
        return this.tip;
    }

    public Object getValue(String key) {
        if (Action.ACTION_COMMAND_KEY.equalsIgnoreCase(key)) {
            return this.command;
        }
        if (Action.LONG_DESCRIPTION.equalsIgnoreCase(key)) {
            return this.tip;
        }
        if (Action.NAME.equalsIgnoreCase(key)) {
            return this.name;
        }
        if (Action.SMALL_ICON.equalsIgnoreCase(key)) {
            return this.getIcon();
        }
        if (Action.ACTION_COMMAND_KEY.equalsIgnoreCase(key)) {
            return this.command;
        }
        if (Action.ACCELERATOR_KEY.equalsIgnoreCase(key)) {
            return this.getKeyStroke();
        }
        if (Action.LARGE_ICON_KEY.equalsIgnoreCase(key)) {
            return this.getIcon();
        }
        if (ActionInfo.ICON_NAME.equalsIgnoreCase(key)) {
            return this.iconName;
        }
        if (Action.SHORT_DESCRIPTION.equalsIgnoreCase(key)) {
            return this.getLabel();
        }
        if (ActionInfo.TOOLTIP.equalsIgnoreCase(key)) {
            return this.getTooltip();
        }
        if (ActionInfo.POSITION.equalsIgnoreCase(key)) {
            return this.position;
        }
        if (ActionInfo.REDIRECTIONS.equalsIgnoreCase(key)) {
            return this.redirections;
        }
        if (ActionInfo.REDIRECTION.equalsIgnoreCase(key)) {
            return this.getRedirection();
        }
        if (ActionInfo.EXTENSION.equalsIgnoreCase(key)) {
            return this.getExtension();
        }
        if (ActionInfo.EXTENSION_NAME.equalsIgnoreCase(key)) {
            return this.getExtensionName();
        }
        if (ActionInfo.PLUGIN.equalsIgnoreCase(key)) {
            return this.getPlugin();
        }
        if (ActionInfo.PLUGIN_NAME.equalsIgnoreCase(key)) {
            return this.getPluginName();
        }
        if (ActionInfo.VISIBLE.equalsIgnoreCase(key)) {
            return this.isVisible();
        }
        if (ActionInfo.ACTIVE.equalsIgnoreCase(key)) {
            return this.active;
        }
        if (ActionInfo.ACCELERATOR.equalsIgnoreCase(key)) {
            return this.accelerator;
        }
        return super.getValue(key);
    }

    public void putValue(String key, Object newValue) {
        super.putValue(key, newValue);
        // This class is immutable, only "active" can be changed
        if (ActionInfo.ACTIVE.equalsIgnoreCase(key)) {
            this.setActive(this.active);
        }
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("ActionInfo {");
        buffer.append("name='").append(this.name).append("', ");
        buffer.append("active='").append(this.active).append("', ");
        buffer.append("label='").append(this.text).append("', ");
        buffer.append("tooltip='").append(this.tip).append("', ");
        buffer.append("actionCommand='").append(this.command).append("', ");
        buffer.append("position='").append(this.position).append("', ");
        buffer.append("icon='").append(this.iconName).append("', ");
        buffer.append("extension='").append(this.getExtensionName())
                .append("', ");
        if (this.redirections != null) {
            buffer.append("redirection=(");
            for (ActionInfo redirection : this.redirections) {
                buffer.append(redirection.getName());
                buffer.append(" ");
            }
            buffer.append("), ");
        } else {
            buffer.append("redirections=( ), ");
        }
        buffer.append("accelerator='").append(this.accelerator);
        buffer.append("' }");
        return buffer.toString();
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        logger.info("setActive({})", active);
        this.active = active;
    }

}
