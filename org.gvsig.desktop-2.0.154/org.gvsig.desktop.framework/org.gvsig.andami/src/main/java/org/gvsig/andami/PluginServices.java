/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import java.awt.Component;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.ExclusiveUIExtension;
import org.gvsig.andami.plugins.ExtensionDecorator;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.PluginClassLoader;
import org.gvsig.andami.preferences.DlgPreferences;
import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.AddDefinitionException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.gvsig.utils.XMLEntity;
import org.gvsig.utils.swing.threads.IMonitorableTask;
import org.gvsig.utils.swing.threads.IProgressMonitorIF;
import org.gvsig.utils.swing.threads.TaskMonitorTimerListener;
import org.gvsig.utils.swing.threads.UndefinedProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides services to Plugins. Each plugin has an associated PluginServices
 * object, which provides specific services. Main of them: translations,
 * logging, access to plugin's resources, background tasks, clipboard access
 * and data persistence.
 * 
 * @author Fernando Gonz�lez Cort�s
 */
public class PluginServices {

    private static Logger logger =
        LoggerFactory.getLogger(PluginServices.class);

    private static String[] arguments;

    private static ExclusiveUIExtension exclusiveUIExtension = null;

    private PluginClassLoader loader;

    private XMLEntity persistentXML;

    private DynObject pluginPersistence = null;

    private String[] alternativeNames = null;
    /**
     * Creates a new PluginServices objetct.
     * 
     * @param loader
     *            The Plugin's ClassLoader.
     */
    public PluginServices(PluginClassLoader loader) {
        this.loader = loader;
    }

    public PluginServices(PluginClassLoader loader, String[] alternativeNames) {
    	this(loader);
        this.alternativeNames = alternativeNames;
    }

    public String[] getAlternativeNames() {
    	return this.alternativeNames;
    }
    
    /**
     * Returns the message in the current's locale language
     * corresponding to the provided translation key.
     * The key-message pairs are obtained from the plugin's
     * translation files (text_xx.properties files).
     * 
     * @param key
     *            Translation key whose associated message is to be obtained
     * 
     * @return The message associated with the provided key, in one of the
     *         current locale languages, or the key if the translation is not
     *         found.
     * @deprecated use I18NManager
     */
    public String getText(String key) {
        if (key == null)
            return null;
        String translation = org.gvsig.i18n.Messages.getText(key, false);
        if (translation != null)
            return translation;
        else {
            logger.debug("Can't find translation for ''{}'' in plugin ''{}''.",
                key,
                getPluginName());
            return key;
        }
    }

    /**
     * Gets the plugin's classloader.
     * 
     * @return Returns the loader.
     */
    public PluginClassLoader getClassLoader() {
        return loader;
    }

    /**
     * Gets the plugin's name
     * 
     * @return The plugin's name
     */
    public String getPluginName() {
        return loader.getPluginName();
    }

    /**
     * Gets a reference to the PluginServices object associated with the
     * plugin containing the provided class.
     * 
     * Obtienen una referencia al PluginServices del plugin cuyo nombre se pasa
     * como par�metro
     * 
     * @param pluginClassInstance
     *            An instance of a class. This class is contained in a plugin,
     *            whose
     *            services are to be obtained
     * 
     * @return The PluginServices object associated to the containing plugin
     * 
     * @throws RuntimeException
     *             If the parameter was not loaded from a plugin
     */
    public static PluginServices getPluginServices(Object pluginClassInstance) {
        try {
        	PluginClassLoader loader;
    		if( pluginClassInstance instanceof Class ) {
    			loader = (PluginClassLoader) ((Class) pluginClassInstance).getClassLoader();
    		} else {
    			loader = (PluginClassLoader) pluginClassInstance.getClass().getClassLoader();
    		}
            return Launcher.getPluginServices(loader.getPluginName());
        } catch (ClassCastException e) {
            /*
             * throw new RuntimeException( "Parameter is not a plugin class
             * instance");
             */
            return null;
        }
    }

    /**
     * Gets a reference to the PluginServices object associated with the
     * provided plugin.
     * 
     * @param pluginName
     *            Plugin's name whose services are going to be used
     * 
     * @return The PluginServices object associated with the provided plugin.
     */
    public static PluginServices getPluginServices(String pluginName) {
        return Launcher.getPluginServices(pluginName);
    }

    /**
     * Gets the window manager (MDIManager).
     * 
     * @return A reference to the window manager (MDIManager).
     */
    public static MDIManager getMDIManager() {
        return Launcher.getFrame().getMDIManager();
    }

    /**
     * Gets the application's main frame.
     * 
     * @return A reference to the application's main window
     */
    public static MainFrame getMainFrame() {
        return Launcher.getFrame();
    }

    public static void registerKeyStroke(KeyStroke key, KeyEventDispatcher a) {
        GlobalKeyEventDispatcher.getInstance().registerKeyStroke(key, a);
    }

    public static void unRegisterKeyStroke(KeyStroke key) {
        GlobalKeyEventDispatcher.getInstance().removeKeyStrokeBinding(key);
    }

    /**
     * Gets the instance of the extension whose class is provided.
     * 
     * @param extensionClass
     *            Class of the extension whose instance is to be returned
     * 
     * @return The instance of the extension, or null if the instance does
     *         not exist.Instancia de la extensi�n o null en caso de que no haya
     *         una
     */
    public static IExtension getExtension(Class extensionClass) {
        ExtensionDecorator extAux =
            (ExtensionDecorator) Launcher.getClassesExtensions()
            .get(extensionClass);
        try {
            return extAux.getExtension();
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Gets a reference to the Extension Decorator which adds some extra options
     * to the basic extension interface.
     * 
     * @param extensionClass
     *            The class of the extension whose decorator is to be returned
     * @return The ExtensionDecorator associated with the provided extension,
     *         or null if the extension does not exist.
     */
    public static ExtensionDecorator getDecoratedExtension(Class extensionClass) {
        return (ExtensionDecorator) Launcher.getClassesExtensions()
        .get(extensionClass);
    }

    /**
     * Returns an array containing references to all the loaded extensions.
     * 
     * @return ExtensionDecorator[] An array of ExtensionDecorators (each
     *         Decorator contains one extension).
     */
    public static ExtensionDecorator[] getDecoratedExtensions() {
        HashMap map = Launcher.getClassesExtensions();
        ExtensionDecorator[] extensions =
            (ExtensionDecorator[]) map.values()
            .toArray(new ExtensionDecorator[0]);
        return extensions;
    }

    /**
     * Gets an iterator with all the loaded Extensions.
     * 
     * @return Iterator over the decorated extensions (each member of
     *         the iterator is an ExtensionDecorator, which in turn contains
     *         one IExtension object).
     */
    public static Iterator getExtensions() {
        return Launcher.getClassesExtensions().values().iterator();
    }

    /**
     * Returns the message in the current's locale language
     * corresponding to the provided translation key.
     * The key-message pairs are obtained from the plugin's
     * translation files (text_xx.properties files).
     * 
     * @param pluginObject
     *            Any object which was loaded from a plugin
     * 
     * @param key
     *            Translation key whose associated message is to be obtained
     * 
     * @return The message associated with the provided key, in one of the
     *         current locale languages, or the key if the translation is not
     *         found.
     * @deprecated use I18NManager
     */
    public static String getText(Object pluginObject, String key) {
        if (key == null)
            return null;
        String translation = org.gvsig.i18n.Messages.getText(key, false);
        if (translation != null)
            return translation;
        else {
            logger.debug("Can't find translation for ''{}''.", key);
            return key;
        }
    }

    /**
     * Sets the XML data which should be saved on disk for this plugin. This
     * data can be retrieved on following sessions.
     * 
     * @param The
     *            XMLEntity object containing the data to be persisted.
     * 
     * @see PluginServices.getPersistentXML()
     * @see XMLEntity
     */
    public void setPersistentXML(XMLEntity entity) {
        persistentXML = entity;
    }

    /**
     * Gets the XML data which was saved on previous sessions for this
     * plugins.
     * 
     * @return An XMLEntity object containing the persisted data
     */
    public XMLEntity getPersistentXML() {
        if (persistentXML == null) {
            persistentXML = new XMLEntity();
        }
        return persistentXML;
    }

    /**
     * A�ade un listener a un popup menu registrado en el config.xml de alg�n
     * plugin
     * 
     * @param name
     *            Nombre del men� contextual
     * @param c
     *            Componente que desplegar� el men� cuando se haga click con el
     *            bot�n derecho
     * @param listener
     *            Listener que se ejecutar� cuando se seleccione cualquier
     *            entrada del men�
     * 
     * @throws RuntimeException
     *             Si la interfaz no est� preparada todav�a. S�lo puede darse
     *             durante el arranque
     */
    public void addPopupMenuListener(String name,
        Component c,
        ActionListener listener) {
        MDIFrame frame = Launcher.getFrame();

        if (frame == null) {
            throw new RuntimeException("MDIFrame not loaded yet");
        }

        frame.addPopupMenuListener(name, c, listener, loader);
    }

    /**
     * Gets the plugin's root directory.
     * 
     * @return A File pointing to the plugin's root directory.
     */
    public File getPluginDirectory() {
        return Launcher.getPluginFolder(this.getPluginName());
    }

    /**
     * Runs a background task. The events produced on the main frame will
     * be inhibited.
     * 
     * @param r
     *            The task to run.
     * 
     * @return The Thread on which the task is executed.
     */
    public static Thread backgroundExecution(Runnable r) {
        Thread t = new Thread(new RunnableDecorator(r));
        t.start();

        return t;
    }

    /**
     * Runs a backbround task. This task may be monitored and canceled, and
     * does not inhibit any event.
     * 
     * @param task
     *            The task to run.
     */
    public static void cancelableBackgroundExecution(final IMonitorableTask task) {
        final org.gvsig.utils.swing.threads.SwingWorker worker =
            new org.gvsig.utils.swing.threads.SwingWorker() {

            public Object construct() {
                try {
                    task.run();
                    return task;
                } catch (Exception e) {
                    NotificationManager.addError(null, e);
                }
                return null;
            }

            /**
             * Called on the event dispatching thread (not on the worker
             * thread)
             * after the <code>construct</code> method has returned.
             */
            public void finished() {
                task.finished();
            }
        };

        Component mainFrame = (Component) PluginServices.getMainFrame();

        IProgressMonitorIF progressMonitor = null;
        String title = getText(null, "PluginServices.Procesando");
        progressMonitor =
            new UndefinedProgressMonitor((Frame) mainFrame, title);
        progressMonitor.setIndeterminated(!task.isDefined());
        progressMonitor.setInitialStep(task.getInitialStep());
        progressMonitor.setLastStep(task.getFinishStep());
        progressMonitor.setCurrentStep(task.getCurrentStep());
        progressMonitor.setMainTitleLabel(task.getStatusMessage());
        progressMonitor.setNote(task.getNote());
        progressMonitor.open();
        int delay = 500;
        TaskMonitorTimerListener timerListener =
            new TaskMonitorTimerListener(progressMonitor, task);
        Timer timer = new Timer(delay, timerListener);
        timerListener.setTimer(timer);
        timer.start();

        worker.start();

    }

    /**
     * Closes the application. Cleanly exits from the application:
     * terminates all the extensions, then exits.
     * 
     */
    public static void closeApplication() {
        Launcher.closeApplication();
    }

    /**
     * DOCUMENT ME!
     * 
     * @author Fernando Gonz�lez Cort�s
     */
    private static class RunnableDecorator implements Runnable {

        private Runnable r;

        /**
         * Crea un nuevo RunnableDecorator.
         * 
         * @param r
         *            DOCUMENT ME!
         */
        public RunnableDecorator(Runnable r) {
            this.r = r;
        }

        /**
         * @see java.lang.Runnable#run()
         */
        public void run() {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {

                    public void run() {
                        try {
                            r.run();
                        } catch (RuntimeException e) {
                            NotificationManager.addError(Messages.getText("PluginServices.Bug_en_el_codigo"),
                                e);
                        } catch (Error e) {
                            NotificationManager.addError(Messages.getText("PluginServices.Error_grave_de_la_aplicaci�n"),
                                e);
                        }
                    }
                });
            } catch (InterruptedException e) {
                NotificationManager.addWarning(Messages.getText("PluginServices.No_se_pudo_poner_el_reloj_de_arena"),
                    e);
            } catch (InvocationTargetException e) {
                NotificationManager.addWarning(Messages.getText("PluginServices.No_se_pudo_poner_el_reloj_de_arena"),
                    e);
            }
        }
    }

    /**
     * Gets an array containing the application's startup arguments. <br>
     * <br>
     * Usually: <code>appName plugins-directory [locale] [other args]</code>
     * 
     * @return the original arguments that Andami received. (app-name
     *         plugins-directory, locale, etc)
     */
    public static String[] getArguments() {
        return arguments;
    }

    /**
     * Replaces the original Andami arguments with the provided arguments.
     * 
     * @param arguments
     *            An array of String, each element of the array
     *            represents one
     *            argument.
     */
    public static void setArguments(String[] arguments) {
        PluginServices.arguments = arguments;
    }

    /**
     * Returns the value of a command line named argument. <br>
     * <br>
     * The argument format is: <br>
     * -{argumentName}={argumentValue} <br>
     * <br>
     * example: <br>
     * ' -language=en '
     * 
     * @return String The value of the argument
     */
    public static String getArgumentByName(String name) {
        for (int i = 2; i < PluginServices.arguments.length; i++) {
        	String arg = PluginServices.arguments[i];
        	if( arg != null ) {
	            int index = arg.indexOf(name + "=");
	            if (index != -1)
	                return arg.substring(index
	                    + name.length() + 1);
        	}
        }
        return null;
    }

    /**
     * Gets the logger. The logger is used to register important
     * events (errors, etc), which are stored on a file which
     * can be checked later.
     * 
     * @return A Logger object.
     * @see Logger object from the Log4j library.
     * 
     */
    public static Logger getLogger() {
        return logger;
    }

    public static DlgPreferences getDlgPreferences() {
        return DlgPreferences.getInstance();
    }

    /**
     * Stores the provided text data on the clipboard.
     * 
     * @param data
     *            An String containing the data to be stored
     *            on the clipboard.
     */
    public static void putInClipboard(String data) {
        StringSelection ss = new StringSelection(data);

        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, ss);
    }

    /**
     * Gets text data from the Clipboard, if available.
     * 
     * @return An String containing the clipboard's data, or <code>null</code>
     *         if the data was not available.
     */
    public static String getFromClipboard() {

        try {
            return (String) Toolkit.getDefaultToolkit()
            .getSystemClipboard()
            .getContents(null)
            .getTransferData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException e) {
            return null;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return null;
        }
    }

    /**
     * Gets the ExclusiveUIExtension, an special extension which
     * will take
     * control over the UI and will decide which extensions may be
     * enable/disabled or visible/hidden.
     * 
     * @return If an ExclusiveUIExtension was set, return this extension.
     *         Otherwise, return <code>null</code>.
     * 
     * @see org.gvsig.andami.Launcher#initializeExclusiveUIExtension()
     * @see org.gvsig.andami.plugins.IExtension#isEnabled(IExtension extension)
     * @see org.gvsig.andami.plugins.IExtension#isVisible(IExtension extension)
     */
    public static ExclusiveUIExtension getExclusiveUIExtension() {
        return PluginServices.exclusiveUIExtension;
    }

    /**
     * Sets the ExclusiveUIExtension, an special extension which
     * will take
     * control over the UI and will decide which extensions may be
     * enable/disabled or visible/hidden. <br>
     * <br>
     * The ExclusiveUIExtension is normally set by the following
     * Andami startup argument: <br>
     * <br>
     * <code>ExclusiveUIExtension=ExtensionName</code>
     * 
     * @see org.gvsig.andami.Launcher#initializeExclusiveUIExtension()
     * @see org.gvsig.andami.plugins.IExtension#isEnabled(IExtension extension)
     * @see org.gvsig.andami.plugins.IExtension#isVisible(IExtension extension)
     */
    public static void setExclusiveUIExtension(ExclusiveUIExtension extension) {
        PluginServices.exclusiveUIExtension = extension;
    }

    public static void addLoaders(ArrayList classLoaders) {
        PluginClassLoader.addLoaders(classLoaders);
    }

    /**
     * @deprecated use ToolsSwingLocator.getIconThemeManager()
     */
    public static IconThemeManager getIconThemeManager() {
    	return ToolsSwingLocator.getIconThemeManager();
    }

    /**
     * @deprecated use  ToolsSwingLocator.getIconThemeManager().getCurrent()
     */
    public static IconTheme getIconTheme() {
    	return getIconThemeManager().getCurrent(); 
    }

    /**
     * Try to detect if the application is running in a development
     * environment. <br>
     * This look for <b>.project</b> and <b>.classpath</b> files in the starts
     * path of the application.
     * 
     * @return true if <b>.project</b> and <b>.classpath</b> are in the
     *         development path
     */
    public static boolean runningInDevelopment() {
        String andamiPath;
        Properties props = System.getProperties();

        try {
            try {
                andamiPath =
                    (new File(Launcher.class.getResource(".").getFile()
                        + File.separator + ".." + File.separator + ".."
                        + File.separator + ".." + File.separator + "..")).getCanonicalPath();
            } catch (IOException e) {
                andamiPath =
                    (new File(Launcher.class.getResource(".").getFile()
                        + File.separator + ".." + File.separator + ".."
                        + File.separator + ".." + File.separator + "..")).getAbsolutePath();
            }
        } catch (Exception e1) {
            andamiPath = (String) props.get("user.dir");
        }

        File andamiJar = new File(andamiPath + File.separator + "andami.jar");
        if (!andamiJar.exists())
            return false;
        File projectFile = new File(andamiPath + File.separator + ".project");
        File classpathFile =
            new File(andamiPath + File.separator + ".classpath");
        return projectFile.exists() && classpathFile.exists();

    }

    public PluginsManager getManager() {
        return PluginsLocator.getManager();
    }

    private String[] getAllPluginNames() {
    	String[] names = new String[this.alternativeNames.length+1];
    	names[0] = this.getPluginName();
    	for( int n=0; n<this.alternativeNames.length; n++ ) {
        	names[n+1] = this.alternativeNames[n];
    	}
    	return names;
    }
    
    public DynObject getPluginProperties() {
        if (this.pluginPersistence == null) {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            DynStruct dynStruct = manager.getDynObjectDefinition(getPluginName());
            if ( dynStruct == null) {
                File persistenceDefinitionFile =
                    new File(this.getPluginDirectory(), "plugin-persistence.def");
                String[] names = getAllPluginNames(); 
                for( int i=0; i<names.length ; i++ ) {
	                try {
	                    dynStruct = manager.addDefinition(DynObject.class,
	                        names[i],
	                        new FileInputStream(persistenceDefinitionFile),
	                        this.getClassLoader(),
	                        null,
	                        null);
	                    break;
	                } catch (FileNotFoundException e) {
	                    throw new PluginPersistenceNeedDefinitionException(this.getPluginName(), e);
	                } catch (AddDefinitionException e) {
	                	if( i+1 >= names.length ) { // Si ya no hay mas nombres peta
	                		throw new PluginPersistenceAddDefinitionException(this.getPluginName(), e);
	                	}
	                }
                }
            }

            File persistenceFile = getPluginPersistenceFile();
            if (persistenceFile.exists()) {
                PersistentState state;

                try {
                    state = manager.loadState(new FileInputStream(persistenceFile));
                    pluginPersistence = (DynObject) manager.create(state);
                } catch (Exception e) {
                	
                    logger.info("Can't retrieve persistent values from plugin "+
                    getPluginName(), e);
                    showMessageDialogDelayed(Messages.getText("_Unable_to_read_persistence_for_plugin")
                                    + ": " + getPluginName() + "\n" +
                                    Messages.getText("_Perhaps_saved_data_refers_to_missing_plugin"),
                                    Messages.getText("_Persistence"),
                                    JOptionPane.WARNING_MESSAGE);
                }
            }
            if (this.pluginPersistence == null) {
                logger.info("Creating default values for plugin persistence ("+this.getPluginName()+").");
                this.pluginPersistence =
                    ToolsLocator.getDynObjectManager()
                    .createDynObject(dynStruct); 
            }
        }
        return pluginPersistence;
    }

    private void showMessageDialogDelayed(final String msg, final String title, final int type) {
        PluginsManager pluginManger = PluginsLocator.getManager();
        pluginManger.addStartupTask("Persistence_"+getPluginName(), new Runnable() {
           public void run() {
                JOptionPane.showMessageDialog(
                                (Component) PluginServices.getMainFrame(),
                                msg,
                                title,
                                type);
                    }
        }, true, 100);
    }
    
    public void savePluginProperties() {
        if (this.pluginPersistence == null) {
            return;
        }
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
		File persistenceFile = getPluginPersistenceFile();
        PersistentState state;
        FileOutputStream fos;
        try {
            state = manager.getState(pluginPersistence);
            manager.saveState(state, new FileOutputStream(persistenceFile));
        } catch (Exception e) {
            throw new PluginSaveDataException(this.getPluginName(), e);
        }


    }

	/**
	 * Returns the plugin persistence file.
	 * 
	 * @return the plugin persistence file
	 */
	private File getPluginPersistenceFile() {
		return new File(getPluginHomeFolder(), "plugin-persistence.dat");
	}

	/**
	 * Returns the folder where the plugin stores its resources. That folder
	 * will be usually a subfolder into the application folder in the user home
	 * folder.
	 * 
	 * @return the folder where the plugin stores its resources
	 */
	public File getPluginHomeFolder() {
		File persistenceFolder = new File(Launcher.getAppHomeDir()
				+ File.separator + "plugins" + File.separator
				+ this.getPluginName());

		if (!persistenceFolder.exists()) {
			persistenceFolder.mkdirs();
		}

		return persistenceFolder;
	}

    public class PluginPersistenceNeedDefinitionException extends
    BaseRuntimeException {

        /**
         * 
         */
        private static final long serialVersionUID = -2036279527440882712L;

        PluginPersistenceNeedDefinitionException(String name, Throwable cause) {
            super("Can't load persistence definition of plugin %(name).",
                "_cant_load_persistence_definition_of_plugin_XnameX",
                serialVersionUID);
            initCause(cause);
            setValue("name", name);
        }
    }

    public class PluginPersistenceAddDefinitionException extends
    BaseRuntimeException {

        /**
         * 
         */
        private static final long serialVersionUID = 2227722796640780361L;

        PluginPersistenceAddDefinitionException(String name, Throwable cause) {
            super("Can't add persistence definition of plugin %(name).",
                "_cant_add_persistence_definition_of_plugin_XnameX",
                serialVersionUID);
            this.initCause(cause);
            setValue("name", name);
        }
    }

    public class PluginLoadDataException extends
    BaseRuntimeException {

        /**
         * 
         */
        private static final long serialVersionUID = 1168749231143949111L;

        PluginLoadDataException(String name) {
            super("Can't load data of plugin %(name).",
                "_cant_load_data_of_plugin_XnameX",
                serialVersionUID);
            setValue("name", name);
        }
    }

    public class PluginSaveDataException extends
    BaseRuntimeException {

        /**
         * 
         */
        private static final long serialVersionUID = 4893241183911774542L;
        private final static String MESSAGE_FORMAT = "Can't save data of plugin %(name).";
        private final static String MESSAGE_KEY = "_cant_save_data_of_plugin_XnameX";

        PluginSaveDataException(String name) {
            super(MESSAGE_FORMAT,
                MESSAGE_KEY,
                serialVersionUID);
            setValue("name", name);
        }

        public PluginSaveDataException(String name, Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
            setValue("name", name);

        }
    }
    
    @Override
    public String toString() {
        return super.toString()+" "+this.getPluginName();
    }
    
    public void addDependencyWithPlugin(PluginServices otherPlugin) {
        this.getClassLoader().addPluginClassLoader(otherPlugin.getClassLoader());
    }
}
