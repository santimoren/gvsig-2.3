/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import javax.swing.KeyStroke;

public class GlobalKeyEventDispatcher implements KeyEventDispatcher {
	private static GlobalKeyEventDispatcher globalKeyDispatcher = null;
	private static Hashtable registeredKeys = new Hashtable();
	static GlobalKeyEventDispatcher getInstance()
	{
		if (globalKeyDispatcher == null)
			globalKeyDispatcher = new GlobalKeyEventDispatcher();
		return globalKeyDispatcher;
	}

	public void registerKeyStroke(KeyStroke key, KeyEventDispatcher disp)
		throws RuntimeException
	{
		if (registeredKeys.containsKey(key))
		{
			KeyEventDispatcher a = (KeyEventDispatcher) registeredKeys.get(key);
			throw new RuntimeException("Error: La tecla " + key +
					" ya est� asignada al action" + a);
		}
		registeredKeys.put(key, disp);
	}

    public void removeKeyStrokeBinding(KeyStroke key) {
    	registeredKeys.remove(key);
    }

    public void removeAll() {
    	registeredKeys.clear();
    }

    public KeyEventDispatcher getListenerAssignedTo(KeyStroke key)
    {
    	return (KeyEventDispatcher) registeredKeys.get(key);
    }


	private GlobalKeyEventDispatcher()
	{

	}
	public boolean dispatchKeyEvent(KeyEvent e) {
		KeyStroke key = KeyStroke.getKeyStroke(e.getKeyCode(), e.getModifiers());
		KeyEventDispatcher a = getListenerAssignedTo(key);

		if (a!=null)
		{
			//System.err.println("DEBUG: KeyEvent " + e +
			//		".Antes de ejecutar action=" + a);
			return a.dispatchKeyEvent(e);
		}
		return false;
	}

}


