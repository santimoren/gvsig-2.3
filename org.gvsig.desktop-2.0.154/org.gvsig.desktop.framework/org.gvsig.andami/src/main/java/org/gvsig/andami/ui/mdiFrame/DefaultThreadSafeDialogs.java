/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.lang.reflect.Constructor;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.tools.task.CancellableTask;
import org.gvsig.tools.task.RunnableWithParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread safe functions for showing dialogs
 * 
 * @author jjdelcerro
 * 
 */
public class DefaultThreadSafeDialogs implements ThreadSafeDialogs {

	private static Logger logger = LoggerFactory
			.getLogger(DefaultThreadSafeDialogs.class);
	private Component rootComponent;
	private NewStatusBar statusbar;

	public DefaultThreadSafeDialogs() {
            this(null, null);
	}

	public DefaultThreadSafeDialogs(Component rootComponent) {
            this(rootComponent,null);
	}

	public DefaultThreadSafeDialogs(Component rootComponent,
			NewStatusBar statusbar) {
		this.statusbar = statusbar;
		this.rootComponent = rootComponent;
	}

	private Component getRootComponent() {
		if (this.rootComponent == null) {
			try {
				this.rootComponent = (MDIFrame) PluginServices.getMainFrame();
			} catch (Throwable t) {
				// Ignore and return null
			}
		}
		return this.rootComponent;
	}

	private NewStatusBar getStatusbar() {
		if (this.statusbar == null) {
			this.statusbar = PluginServices.getMainFrame().getStatusBar();
		}
		return this.statusbar;
	}

	private void message(String message, int messageType) {
		this.getStatusbar().message(message, messageType);
	}

	private String translate(String message) {
		return translate(message, null);
	}

	private String translate(String message, String[] args) {
		String msg = message;
		if (msg == null) {
			msg = "";
		}
		if (msg.startsWith("_")) {
			msg = org.gvsig.i18n.Messages.getText(msg, args);
			if (msg == null) {
				msg = "_" + message.replace("_", " ");
			}
		}
		return msg;
	}

	public int confirmDialog(final String message, final String title, final int optionType,
			final int messageType) {
		RunnableWithParameters runnable = new RunnableWithParameters() {
			public void run() {
				this.returnValue = JOptionPane.showConfirmDialog(
						getRootComponent(), message,title, optionType, messageType);
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (Exception e) {
				logger.info("Can't show input dialog '" + message + "'.", e);
			}
		}
		return (Integer) runnable.getReturnValue();
	}

	public String inputDialog(final String message, final String title, final int messageType,
			final String initialValue) {
		// inputDialog dlg = new inputDialog();
		// return dlg.show(translate(message), translate(title), messageType,
		// initialValue);
		//
		RunnableWithParameters runnable = new RunnableWithParameters() {
			public void run() {
				this.returnValue = JOptionPane.showInputDialog(
						getRootComponent(), translate(message),
						translate(title),
						messageType, null, null,
						initialValue);
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (Exception e) {
				logger.info("Can't show input dialog '" + message + "'.", e);
			}
		}
		return (String) runnable.getReturnValue();
	}

	public String inputDialog(final String message, final String title) {
		RunnableWithParameters runnable = new RunnableWithParameters() {
			public void run() {
				this.returnValue = JOptionPane.showInputDialog(
						getRootComponent(), (String) translate(message),
						translate(title),
						JOptionPane.QUESTION_MESSAGE, null, null, null);
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (Exception e) {
				logger.info("Can't show input dialog '" + message + "'.", e);
			}
		}
		return (String) runnable.getReturnValue();
	}

	public void messageDialog(String message, String title, int messageType) {
		messageDialog(message, null, title, messageType);
	}

	public void messageDialog(final String message, final String messageArgs[],
			final String title, final int messageType) {
		if (!SwingUtilities.isEventDispatchThread()) {
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						messageDialog(message, messageArgs, title, messageType);
					}
				});
			} catch (Exception e) {
				logger.info("Can't show message dialog '" + message
						+ "'. redirect to status bar", e);
				this.message(message, messageType);
			}
			return;
		}

		if (message == null) {
			logger.info("message if null, message dialog not show.");
			return;
		}
		JOptionPane.showMessageDialog(getRootComponent(),
				translate(message, messageArgs), translate(title), messageType);
	}

	public void showDialog(final Component contents, final String title) {
		if (SwingUtilities.isEventDispatchThread()) {
			final DialogWindow window = new DialogWindow();
			window.setDialogFlag();
			window.setContents(contents, title);
			MDIManager manager = PluginServices.getMDIManager();
			manager.addWindow(window, GridBagConstraints.CENTER);
		} else {
			final DialogWindow window = new DialogWindow();
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					window.setContents(contents, title);
					MDIManager manager = PluginServices.getMDIManager();
					manager.addWindow(window, GridBagConstraints.CENTER);
				}
			});
			try {
				synchronized (window) {
					if (contents instanceof CancellableTask) {
						while( contents.isVisible()  ) {
							if( ((CancellableTask)contents).isCancellationRequested() ) {
								SwingUtilities.invokeLater(new Runnable() {
									public void run() {
										contents.setVisible(false);
									}
								});
							}
							window.wait(10000);
						}
					} else {
						while( contents.isVisible() ) {
							window.wait();
						}
					}
				}
			} catch (InterruptedException e) {
				logger.info("showDialog can wait to close dialog.", e);
			}
		}
	}

	class DialogWindow extends JPanel implements IWindow, ComponentListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6283975319620714565L;
		protected WindowInfo windowInfo;
		protected Object profile;
		protected Component contents;
		private int code = 0;

		public DialogWindow() {
			code = WindowInfo.RESIZABLE | WindowInfo.MAXIMIZABLE
					| WindowInfo.ICONIFIABLE;
			profile = WindowInfo.DIALOG_PROFILE;
		}

		public void setDialogFlag() {
			code |= WindowInfo.MODALDIALOG;
		}

		public void setContents(Component contents, String title) {
			this.windowInfo = new WindowInfo(code);
			this.windowInfo.setTitle(title);

			this.contents = contents;

			Dimension size = this.contents.getPreferredSize();
			this.windowInfo.setHeight(size.height);
			this.windowInfo.setWidth(size.width);

			this.setLayout(new BorderLayout());
			this.add(this.contents, BorderLayout.CENTER);

			this.contents.addComponentListener(this);
		}

		public WindowInfo getWindowInfo() {
			return this.windowInfo;
		}

		public Object getWindowProfile() {
			return this.profile;
		}

		public void componentHidden(ComponentEvent arg0) {
			// Close window when hide contents panel.
			MDIManager manager = PluginServices.getMDIManager();
			manager.closeWindow(this);
			if ((code & WindowInfo.MODALDIALOG) == 0) {
				synchronized (this) {
					this.notifyAll();
				}
			}
		}

		public void componentMoved(ComponentEvent arg0) {
			// Do nothing
		}

		public void componentResized(ComponentEvent arg0) {
			// Do nothing
		}

		public void componentShown(ComponentEvent arg0) {
			// Do nothing
		}

	}

	public Component createComponent(final Class<? extends Component> theClass,
			final Object... parameters) {
		return createComponentWithParams(theClass, parameters);
	}

	public Component createComponentWithParams(
			final Class<? extends Component> theClass, final Object[] parameters) {
		final Class<?>[] parameterTypes = new Class<?>[parameters.length];
		for (int i = 0; i < parameters.length; i++) {
			parameterTypes[i] = parameters[i].getClass();
		}
		final Component component;
		final Constructor<?> constructor;
		try {
			constructor = theClass.getConstructor(parameterTypes);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		if (SwingUtilities.isEventDispatchThread()) {
			try {
				component = (Component) constructor.newInstance(parameters);
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			try {
				RunnableWithParameters runnable = new RunnableWithParameters(parameters) {
					public void run() {
						Constructor<?> cons = constructor;
						try {
							this.returnValue = cons.newInstance(parameters.toArray());
						} catch (Exception e) {
							String msg ="Can't create instance of components, constructor="+cons.toString()+", parameters="+this.parameters.toString()+"."; 
							logger.info(msg,e);
							throw new IllegalArgumentException(e);
						}
					}
				};
				SwingUtilities.invokeAndWait(runnable);
				component = (Component) runnable.getReturnValue();
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
		}
		return component;
	}

	public File[] showChooserDialog(
			final String title,
			final int type, // SAVE_DIALOG / OPEN_DIALOG
			final int selectionMode, //    JFileChooser.FILES_ONLY, JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES
			final boolean multiselection, 
			final File initialPath,
			final FileFilter filter,
			final boolean fileHidingEnabled
			) {
		RunnableWithParameters runnable = new RunnableWithParameters() {
			public void run() {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle(title);
				fc.setDialogType(type);
				fc.setFileSelectionMode(selectionMode);
				fc.setMultiSelectionEnabled(multiselection);
				fc.setCurrentDirectory(initialPath);
				fc.setFileFilter(filter);
				fc.setFileHidingEnabled(fileHidingEnabled);
				int r = JFileChooser.CANCEL_OPTION;
				switch(type) {
				case JFileChooser.SAVE_DIALOG:
					r = fc.showSaveDialog(getRootComponent());
					break;
				case JFileChooser.OPEN_DIALOG:
				default:
					r = fc.showOpenDialog(getRootComponent());
					break;
				}
				if( r != JFileChooser.APPROVE_OPTION ) {
					this.returnValue = null;
					return;
				}
				if( fc.isMultiSelectionEnabled() ) {
					this.returnValue = fc.getSelectedFiles();
				} else {
					this.returnValue = new File[] { fc.getSelectedFile() };
				}
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(runnable);
			} catch (Exception e) {
				logger.info("Can't show chooser dialog '" + title + "'.", e);
			}
		}
		return (File[]) runnable.getReturnValue();
	}
	
	public File[] showOpenDirectoryDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.DIRECTORIES_ONLY, false, initialPath, null, false);
	}

	
	public File[] showOpenFileDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
	}

	
	public File[] showSaveFileDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.SAVE_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
	}

}
