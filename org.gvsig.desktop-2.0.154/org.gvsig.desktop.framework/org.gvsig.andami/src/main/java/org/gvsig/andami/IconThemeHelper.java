/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IconThemeHelper {

	private static Logger logger = LoggerFactory.getLogger(IconThemeHelper.class);
	
	@SuppressWarnings("rawtypes")
	public static void registerIcon(String group, String name, Object obj) {
		String resourceName;
		String provider;
		ClassLoader loader;
		IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getCurrent();
		if( group == null || group.trim().length()==0 ) {
			resourceName = "images/"+name+".png";
		} else {
			resourceName = "images/"+group+"/"+name+".png";
		}
		if( obj instanceof Class ) {
			loader = ((Class) obj).getClassLoader();
			provider = ((Class) obj).getName();
		} else {
			loader = obj.getClass().getClassLoader();
			provider = obj.getClass().getName();
		}
		PluginServices plugin = PluginServices.getPluginServices(obj);
		if( plugin != null ) {
			provider = plugin.getPluginName(); 
		}

		URL resource = null;
		try {
			resource = loader.getResource(resourceName);
			if(resource == null) {
				if( group == null || group.trim().length() == 0) {
					resourceName = "images/" + name + ".gif";
				} else {
					resourceName = "images/" + group + "/" + name + ".gif";
				}
				resource = loader.getResource(resourceName);
			}
			iconTheme.registerDefault(provider, group, name, null, resource);
		} catch( Throwable e) {
			logger.info("Can't register icon '" + name + "'.",e);
		}

	}

	public static ImageIcon getImageIcon(String iconName) {
		return PluginServices.getIconTheme().get(iconName);
	}
	
	public static Image getImage(String iconName) {
		return PluginServices.getIconTheme().get(iconName).getImage();
	}
}
