/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiManager;

import java.awt.GridBagConstraints;
import java.beans.PropertyVetoException;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;



/**
 * <p>
 * This interface acts as window manager. It is the place to create
 * new windows, close existing ones or modify their properties
 * (size, position, etc).
 * </p>
 * <p>
 * Any class extending from JPanel and implementing the {@link IWindow}
 * interface may become an Andami window. Andami will create a real
 * window (normally a JInternalFrame) and put the JPanel inside the
 * real window. In order to differentiate the contents (panels
 * extending JPanel and implementing IWindow) from the real window
 * (normally JInternalFrames or JDialogs), we will use
 * <i>window</i> to refer to the contents and <i>frame</i>
 * to refer to the real window.
 * </p>
 * <p>
 * This class is implemented by the Andami Skin (currently libCorePlugin),
 * which will decide the final implementation of frames. A different frame
 * implementation could be used by switching the Skin.
 * </p>
 *
 * @see IWindow
 * @see WindowInfo
 * @see SingletonWindow
 *
 */
public interface MDIManager {

    /**
     * Initializes the MDIFrame. It must be called before starting
     * to use it. It receives the application's main frame
     * (MDIFrame) as parameter.
     *
     * @param f Application's main frame.
     */
    public void init(MDIFrame f);

    /**
     * <p>
     * Creates a new frame with the provided contents, and shows this
     * new window. The new frame's properties are set according to
     * the WindowInfo object from IWindow's <code>getWindowInfo()</code>
     * method.
     * The new frame is disposed when closed.
     * </p>
     * <p>
     * If the provided IWindow also implements SingletonWindow, and
     * another SingletonWindow already exists and uses the same
     * model, this later window will be sent to the foreground
     * and no new window will be created.
     * </p>
     *
     * @param p Panel with the contents of the new window.
     *
     * @return Returns the added IWindow, or in case it is a
     * SingletonWindow and there is another SingletonWindow with
     * the same model that it is already shown, returns this
     * later SingletonWindow.
     */
    public IWindow addWindow(IWindow p) throws SingletonDialogAlreadyShownException;


    /*
     * Constants used by the method showWindow
     */
    public final MODE WINDOW = WindowManager.MODE.WINDOW;
    public final MODE TOOL = WindowManager.MODE.TOOL;
    public final MODE DIALOG = WindowManager.MODE.DIALOG;

    /**
     * Useful method to simplify the presentation of a window.
     * For more precise control over the behavior of the window
     * use addWindow
     *
     * This methos
     * @param panel to show as a window
     * @param title title of the window
     * @param mode type of the window to create
     */
    public void showWindow(JPanel panel, String title, MODE mode);

    /**
     * Return the window associated to the SingletonWindow class and the model
     * specified. If not exists any singleton window associated to this
     * null is returned.
     *
     * @param windowClass, the class that implement SingletonWindow
     * @param model, the model associated to the SingletonWindow
     * @return the requested window or null.
     */
    public SingletonWindow getSingletonWindow(Class windowClass, Object model) ;

    /**
     * <p>
     * Creates a new frame with the provided contents, and shows this
     * new window. The new frame will be centered, regardless the
     * position specified in the WindowInfo object from IWindow's
     * <code>getWindowInfo()</code> method.
     * The new frame is disposed when closed.
     * </p>
     * <p>
     * If the provided IWindow also implements SingletonWindow, and
     * another SingletonWindow already exists and uses the same
     * model, this later window will be sent to the foreground
     * and no new window will be created.
     * </p>
     *
     * @param p Panel with the contents of the new window.
     *
     * @return Returns the added IWindow, or in case it is a
     * SingletonWindow and there is another SingletonWindow with
     * the same model that it is already shown, returns this
     * later SingletonWindow.
     *
     * @author Pablo Piqueras Bartolomé
     */
    public IWindow addCentredWindow(IWindow p) throws SingletonDialogAlreadyShownException;

    public static final int ALIGN_FIRST_LINE_START = GridBagConstraints.FIRST_LINE_START;
    public static final int ALIGN_PAGE_START = GridBagConstraints.PAGE_START;
    public static final int ALIGN_FIRST_LINE_END = GridBagConstraints.FIRST_LINE_END;
    public static final int ALIGN_FIRST_LINE_END_CASCADE = GridBagConstraints.FIRST_LINE_END + GridBagConstraints.BELOW_BASELINE;
    public static final int ALIGN_LINE_START = GridBagConstraints.LINE_START;
    public static final int ALIGN_LINE_END = GridBagConstraints.LINE_END;
    public static final int ALIGN_LAST_LINE_START = GridBagConstraints.LAST_LINE_START;
    public static final int ALIGN_PAGE_END = GridBagConstraints.PAGE_END;
    public static final int ALIGN_LAST_LINE_END = GridBagConstraints.LAST_LINE_END;
    public static final int ALIGN_CENTER = GridBagConstraints.CENTER;

    public IWindow addWindow(IWindow p, int align) throws SingletonDialogAlreadyShownException;

    /**
     * <p>
     * Returns the currently active window, excluding the modal windows and the
     * PALETTE windows. If the currently active window is modal or PALETTE type,
     * the previous non-modal and non-PALETTE active window is returned.
     * </p>
     * <p>
     * Modal windows and PALETTE windows are considered to be auxiliary windows,
     * that is the reason why they are not returned.
     * </p>
     *
     * @return A reference to the active window, or null if there is no
     * active window
     */
    public IWindow getActiveWindow();

    /**
     * <p>
     * Returns the currently focused window, excluding the modal windows.
     * If the currently focused window is modal,
     * the previous non-modal focused window is returned.
     * </p>
     *
     * @return A reference to the focused window, or null if there is no
     * focused window
     */
    public IWindow getFocusWindow();

    public void moveToFrom(IWindow win);

    /**
     * Gets all the open windows. Minimized and maximized windows are
     * included. The application's main frame is excluded; it can be
     * accessed using <code>PluginServices.getMainFrame()</code>.
     *
     * @return An IWindow array containing all the open windows.
     */
    public IWindow[] getAllWindows();

    /**
     * Gets all the open windows (as {@link #getAllWindows()}),
     * but in this method the windows are returned in the same
     * deepness order that they have in the application.
     *
     * @return   An ordered array containing all the panels in the application.
     * The first element of the array is the topmost (foreground) window in the
     * application. The last element is the bottom (background) window.
     */
    public IWindow[] getOrderedWindows();

    /**
     * Close the SingletonWindow whose class and model are provided as
     * parameters.
     *
     * @param viewClass Class of the window which is to be closed
     * @param model Model of the window which is to be closed
     *
     * @return true if there is an open window whose class and model
     *         match the provided parameteres, false otherwise.
     */
    public boolean closeSingletonWindow(Class viewClass, Object model);

    /**
     * Close the SingletonWindow whose model is provided as parameter.
     *
     * @param model Model of the window which is to be closed
     *
     * @return true if there is an open window whose model matchs
     *         the provided one, false otherwise.
     */
    public boolean closeSingletonWindow(Object model);

    /**
     * Close the provided window.
     * If the window is openend with the WindowManager, use setVisible(false) in
     * the JPanel to close it.
     *
     * @param p window to be closed
     */
    public void closeWindow(IWindow p);

    /**
     * Close all the currently open windows
     */
    public void closeAllWindows();

    /**
     * Gets the WindowInfo object associated with the provided window.
     *
     * @param v window whose information is to be retrieved
     *
     * @return WindowInfo The WindowInfo object containing the information
     * about the provided window
     *
     * @see WindowInfo
     */
    public WindowInfo getWindowInfo(IWindow v);

    /**
     * Shows the wait cursor and blocks all the events from main window until
     * {@link #restoreCursor()} is called.
     */
    public void setWaitCursor();

    /**
     * Sets the normal cursor and unblocks events from main window.
     *
     * @see #setWaitCursor()
     */
    public void restoreCursor();

    /**
     * Maximizes or restores the provided window
     *
     * @param v The window to be maximized or restored
     * @param bMaximum If true, the window will be maximized,
     *  if false, it will be restored
     * @throws PropertyVetoException
     */
    public void setMaximum(IWindow v, boolean bMaximum) throws PropertyVetoException;

    /**
     * Updates the window properties (size, location, etc) according to the
     * provided WindowInfo object.
     *
     * @param v The window whose properties are to be changed
     * @param vi The WindowInfo object containing the new properties to be set
     */
    public void changeWindowInfo(IWindow v, WindowInfo vi);

    /**
     * Forces a window to be repainted. Normally, this is not necessary,
     * as windows are refreshed when necessary.
     *
     * @param win The window to be refreshed.
     */
    public void refresh(IWindow win);

    /**
     * Sets the provided image as background image in the main window. The image
     * will be centered, set in mosaic or expanded to fill the full window,
     * depending on the <code>typeDesktop</code> argument.
     *
     * @param image The image to be set as background image
     * @param typeDesktop Decides whether the image should be centered, set
     * in mosaic or expanded. Accepted values are: Theme.CENTERED,
     * Theme.MOSAIC and Theme.EXPAND.
     */
    public void setBackgroundImage(ImageIcon image, String typeDesktop);

    public void setLocale(Locale locale);

    /**
     * Change the position of the speficied window.
     *
     * @param window
     * @param x
     * @param y
     */
    public void move(IWindow window, int x, int y);

    /**
     * Gets the associated IWindow to the panel shown with showWindow method.
     * Also panel shown with the tools WindowManager.
     *
     * @param panel
     * @return
     */
    public IWindow getWindow(JPanel panel);
}
