/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.gvsig.andami.messages.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Class loader which loads the classes requested by the plugins. It first tries
 * to search in the classpath, then it requests the class to the parent
 * classloader, then it searches in the owns plugins' library dir, and if all
 * these methods fail, it tries to load the class from any of the depended
 * plugins. Finally, if this also fails, the other classloaders provided in the
 * <code>addLoaders</code> method are requested to load the class.</p>
 *
 * <p>
 * The class loader can also be used to load resources from the plugin's
 * directory by using the <code>getResource()</code> method.</p>
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class PluginClassLoader extends URLClassLoader {

    /**
     * DOCUMENT ME!
     */
    private static Logger logger = LoggerFactory.getLogger(PluginClassLoader.class.getName());


    private Map<String, ZipFile> clasesJar = new HashMap<>();


    private File baseDir;
    private List<PluginClassLoader> pluginLoaders;
    private static List<ClassLoader> otherLoaders = new ArrayList<>();
    private boolean isOtherLoader = false;

    /**
     * Creates a new PluginClassLoader object.
     *
     * @param jars Array with the search paths where classes will be searched
     * @param baseDir Base directory for this plugin. This is the directory
     * which will be used as basedir in the <code>getResources</code> method.
     * @param cl The parent classloader of this classloader. It will be used to
     * search classes before trying to search in the plugin's directory
     * @param pluginLoaders The classloaders of the depended plugins.
     *
     * @throws IOException
     */
    public PluginClassLoader(URL[] jars, String baseDir, ClassLoader cl,
            PluginClassLoader[] pluginLoaders) throws IOException {
        this(jars, baseDir, cl, Arrays.asList(pluginLoaders));
    }

    public PluginClassLoader(URL[] jars, String baseDir, ClassLoader cl,
            List<PluginClassLoader> pluginLoaders) throws IOException {
        super(jars, cl);
        this.baseDir = new File(new File(baseDir).getAbsolutePath());
        try {
            logger.debug("Creating PluginClassLoader for {}.", this.getPluginName());
            this.pluginLoaders = new ArrayList();
            this.pluginLoaders.addAll(pluginLoaders);

            if (jars == null || jars.length < 1) {
                debugDump();
                return;
            }
            ZipFile[] jarFiles = new ZipFile[jars.length];

            for (int i = 0; i < jars.length; i++) {
                try {
                    logger.debug("Classloader {}, loading jar {}", this.getPluginName(), jars[i].getPath());
                    jarFiles[i] = new ZipFile(jars[i].getPath());

                    Enumeration<? extends ZipEntry> entradas = jarFiles[i].entries();

                    while (entradas.hasMoreElements()) {
                        ZipEntry file = (ZipEntry) entradas.nextElement();
                        String fileName = file.getName();

                        if (!fileName.toLowerCase().endsWith(".class")) { //$NON-NLS-1$

                            continue;
                        }

                        fileName = fileName.substring(0, fileName.length() - 6)
                                .replace('/', '.');

                        if (clasesJar.get(fileName) != null) {
                            logger.warn("Classloader {}, duplicated class {} in {} and {}",
                                    new Object[]{
                                        this.getPluginName(),
                                        fileName,
                                        jarFiles[i].getName(),
                                        clasesJar.get(fileName).getName()
                                    }
                            );
                        } else {
                            clasesJar.put(fileName, jarFiles[i]);
                        }
                    }
                } catch (ZipException e) {
                    throw new IOException(e.getMessage() + " Jar: "
                            + jars[i].getPath() + ": " + jarFiles[i]);
                } catch (IOException e) {
                    throw e;
                }
            }
        } finally {
            debugDump();
        }
    }

    private void debugDump() {
        if (!logger.isDebugEnabled()) {
            return;
        }
        logger.debug(this.toString());
        logger.debug("  baseDir: " + this.baseDir);
        logger.debug("  parent: " + this.getParent());
        logger.debug("  depends:");
        for (PluginClassLoader pluginLoader : this.pluginLoaders) {
            logger.debug("    {}", pluginLoader.toString());
        }
        logger.debug("  urls:");
        URL[] urls = this.getURLs();
        for (URL url : urls) {
            logger.debug("    " + url.toString());
        }
//        logger.debug("  classes:");
//        Iterator<Map.Entry<String, ZipFile>> it = this.clasesJar.entrySet().iterator();
//        while (it.hasNext()) {
//            Entry<String, ZipFile> entry = it.next();
//            logger.debug("    " + entry.getKey() + "(" + entry.getValue().getName() + ")");
//        }
    }

    protected Class singleLoadClass(String name) throws ClassNotFoundException {
        Class<?> c;
        Set<String> pluginsVisiteds = new HashSet<>();
        c = this.singleLoadClass(pluginsVisiteds, name);
        return c;
    }

    /**
     * Carga la clase
     *
     * @param name Nombre de la clase
     * @param resolve Si se ha de resolver la clase o no
     *
     * @return Clase cargada
     *
     * @throws ClassNotFoundException Si no se pudo encontrar la clase
     */
    @Override
    protected Class loadClass(String name, boolean resolve)
            throws ClassNotFoundException {
        Class<?> c = null;

        // Intentamos cargar con el URLClassLoader
        try {
            if (!isOtherLoader) {
                c = super.loadClass(name, resolve);
                logger.debug("Classloader {}, found class {}.", this.getPluginName(), name);
            }
        } catch (ClassNotFoundException e1) {
            try {
                c = singleLoadClass(name);
            } catch (ClassNotFoundException e2) {
                try {
                    isOtherLoader = true;
                    c = loadOtherClass(name);
                } catch (ClassNotFoundException e3) {
                    throw new ClassNotFoundException("Class " + name
                            + " not found through the plugin " + baseDir, e3);
                } finally {
                    isOtherLoader = false;
                }
            }
        }
        if (c == null) {
            throw new ClassNotFoundException(Messages.getString(
                    "PluginClassLoader.Error_reading_file") + name);
        }
        if (resolve) {
            resolveClass(c);
        }
        return c;
    }

    private Class<?> loadOtherClass(String name)
            throws ClassNotFoundException {
        ClassLoader[] ocls = (ClassLoader[]) otherLoaders.toArray(new ClassLoader[0]);
        Class<?> c;
        for (ClassLoader ocl : ocls) {
            c = ocl.loadClass(name);
            if (c != null) {
                logger.debug("Classloader {}, found class {} in classloader {}", 
                        new Object[]{this, getPluginName(), name, ocl.toString()}
                );
                return c;
            }
        }
        throw new ClassNotFoundException(name);
    }

    /**
     * obtiene el array de bytes de la clase
     *
     * @param classFile Entrada dentro del jar contiene los bytecodes de la
     * clase (el .class)
     * @param is InputStream para leer la entrada del jar
     *
     * @return Bytes de la clase
     *
     * @throws IOException Si no se puede obtener el .class del jar
     */
    private byte[] loadClassData(ZipEntry classFile, InputStream is)
            throws IOException {
        // Get size of class file
        int size = (int) classFile.getSize();

        // Reserve space to read
        byte[] buff = new byte[size];

        // Get stream to read from
        DataInputStream dis = new DataInputStream(is);

        // Read in data
        dis.readFully(buff);

        // close stream
        dis.close();

        // return data
        return buff;
    }

    /**
     * Gets the bytes of a File
     *
     * @param file File
     *
     * @return bytes of file
     *
     * @throws IOException If the operation fails
     */
    private byte[] loadClassData(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        while ((offset < bytes.length)
                && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {
            offset += numRead;
        }

        // Close the input stream and return bytes
        is.close();

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "
                    + file.getName());
        }

        return bytes;
    }

    /**
     * Gets the requested resource. If the path is relative, its base directory
     * will be the one provided in the PluginClassLoader's constructor. If the
     * resource is not found, search in dependents plugins, otherwise the parent
     * classloader will be invoked to try to get it. If it is not found, it will
     * return null.
     *
     * @param res An absolute or relative path to the requested resource.
     *
     * @return Resource's URL if it was found, nul otherwise.
     */
    @Override
    public URL getResource(String res) {
        URL ret ;

        Set<String> pluginsVisiteds = new HashSet<>();
        ret = this.getResource(pluginsVisiteds, res);
        return ret;
    }

    /**
     * Gets the requested resource. If the path is relative, its base directory
     * will be the one provided in the PluginClassLoader's constructor. If the
     * resource is not found, the parent classloader will be invoked to try to
     * get it. If it is not found, it will return null.
     *
     * @param res An absolute or relative path to the requested resource.
     *
     * @return Resource's URL if it was found, nul otherwise.
     */
    private URL getResource(File base, List<String> res) {
        File[] files = base.listFiles();

        String parte = res.get(0);

        for (File file : files) {
            if (file.getName().compareTo(parte) == 0) {
                if (res.size() == 1) {
                    try {
                        return new URL("file:" + file.toString());
                    }catch (MalformedURLException e) {
                        return null;
                    }
                } else {
                    return getResource(file, res.subList(1, res.size()));
                }
            }
        }

        return null;
    }
    
    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
    	HashSet visitedPlugins = new HashSet();
    	return getResources(name, visitedPlugins);
    }
    
    protected Enumeration<URL> getResources(String name, HashSet<PluginClassLoader> visitedPlugins) throws IOException {        
    	List<URL> resources = new ArrayList<>();
    	Enumeration<URL> aux = super.getResources(name);
    	while(aux.hasMoreElements()){
    		URL url = aux.nextElement();
    		resources.add(url);
    	}
        visitedPlugins.add(this);
    	for(PluginClassLoader loader: this.pluginLoaders){
    		if (!visitedPlugins.contains(loader)) {
	    		aux = loader.getResources(name, visitedPlugins);
	        	while(aux.hasMoreElements()){
	        		URL url = aux.nextElement();
	        		resources.add(url);
	        	}
    		}
    	}
    	return Collections.enumeration(resources);
    }

    /**
     * Returns the name of the plugin (the name of the directory containing the
     * plugin).
     *
     * @return An String containing the plugin's name.
     */
    public String getPluginName() {
        return baseDir.getName();
    }

    /*
     * @see java.security.SecureClassLoader#getPermissions(java.security.CodeSource)
     */
    @Override
    protected PermissionCollection getPermissions(CodeSource codesource) {
        PermissionCollection perms = super.getPermissions(codesource);
        perms.add(new AllPermission());

        return perms;
    }

    /**
     * Gets the plugin's base Iterator<Map.Entry<Integer, Integer>>dir, the
     * directory which will be used to search resources.
     *
     * @return Returns the baseDir.
     */
    public String getBaseDir() {
        return baseDir.getAbsolutePath();
    }

    /**
     * Adds other classloader to use when all the normal methods fail.
     *
     * @param classLoaders An ArrayList of ClassLoaders which will be used to
     * load classes when all the normal methods fail.
     */
    public static void addLoaders(ArrayList classLoaders) {
        otherLoaders.addAll(classLoaders);
    }

    @Override
    public String toString() {
        return super.toString() + " (" + getPluginName() + ")";
    }

    public void addPluginClassLoader(PluginClassLoader pluginClassLoader) {
        if (!this.pluginLoaders.contains(pluginClassLoader)) {
            this.pluginLoaders.add(pluginClassLoader);
        }
    }

    private Class singleLoadClass(Set<String> pluginsVisiteds, String name) throws ClassNotFoundException {

        if (pluginsVisiteds.contains(this.getPluginName())) {
            return null;
        }
        pluginsVisiteds.add(this.getPluginName());

        // Buscamos en las clases de las librer�as del plugin
        Class<?> c = findLoadedClass(name);

        if (c != null) {
            return c;
        }

        logger.debug("Classloader {}, searching class '{}'",
                new Object[]{this.getPluginName(), name}
        );
        try {
            ZipFile jar = (ZipFile) clasesJar.get(name);

            //No esta en ningun jar
            if (jar == null) {
                //Buscamos en el directorio de clases
                String classFileName = baseDir + "/classes/"
                        + name.replace('.', '/') + ".class";
                File f = new File(classFileName);
                if (f.exists()) {
                    byte[] data = loadClassData(f);
                    c = defineClass(name, data, 0, data.length);
                    logger.debug("Classloader {}/classes-folder, found class {}",
                            new Object[]{this.getPluginName(), name}
                    );

                } else {
                    for (PluginClassLoader pluginLoader1 : pluginLoaders) {
                        c = null;
                        PluginClassLoader pluginLoader = pluginLoader1;
                        if (pluginLoader != null) {
                            try {
                                c = pluginLoader.singleLoadClass(pluginsVisiteds, name);
                            } catch (ClassNotFoundException e) {
                                // Si no la encontramos en el primer plugin, capturamos la exceptcion
                                // porque es probable que la encontremos en el resto de plugins.
                            }
                            if (c != null) {
                                logger.debug("Classloader {}, found class {} in plugin {}",
                                        new Object[]{
                                            this.getPluginName(),
                                            name,
                                            pluginLoader.getPluginName()
                                        }
                                );
                                return c;
                            }
                        }
                    }
                    
                    
                }
            } else {
                logger.debug("Classloader {}, found class {} in jar {}",
                        new Object[]{
                            this.getPluginName(),
                            name,
                            jar.getName()
                        }
                );
                String fileName = name.replace('.', '/') + ".class";
                ZipEntry classFile = jar.getEntry(fileName);
                byte[] data = loadClassData(classFile,
                        jar.getInputStream(classFile));

                c = defineClass(name, data, 0, data.length);
                if (c == null) {
                    logger.debug("Classloader {}, can't load class {} from jar {}",
                            new Object[]{
                                this.getPluginName(),
                                name,
                                jar.getName()
                            }
                    );
                }

            }

            if (c == null) {
                logger.debug("Classloader {}, class not found {}",
                        new Object[]{
                            this.getPluginName(),
                            name
                        }
                );
                debugDump();
                throw new ClassNotFoundException(name);
            }

            return c;
        } catch (IOException e) {
            logger.debug("Classloader {}, error loading class {}",
                    new Object[]{
                        this.getPluginName(),
                        name
                    },
                    e
            );
            throw new ClassNotFoundException(Messages.getString(
                    "PluginClassLoader.Error_reading_file") + name);
        }
    }

    /**
     * Este metodo busca en este class loader y en el de los plugins de los que
     * depende. Se cerciora de que no se queda bucleado cuando hay una relacion
     * recursiva entre los plugins.
     *
     * @param pluginsVisiteds, set con los plugins que va visitando para evitar
     * que se quede bucleado cuando hay referencia ciclicas entre plugins.
     * @param res
     *
     * @return
     */
    private URL getResource(Set<String> pluginsVisiteds, String res) {
        URL ret = null;

        if (pluginsVisiteds.contains(this.getPluginName())) {
            return null;
        }
        pluginsVisiteds.add(this.getPluginName());

        //
        // Primero buscamos en el directorio del plugin.
        try {
            logger.debug("Classloader {}, searching resource '{}'", this.getPluginName(), res);
            List<String> resource = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(res, "\\/");
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                resource.add(token);
            }
            ret = getResource(baseDir, resource);
            if (ret != null) {
                return ret;
            }
        } catch (Exception e) {
            logger.warn("Classloader {}, Error getting resource '{}'.",
                    new Object[]{
                        this.getPluginName(), res
                    },
                    e
            );
        }

        logger.debug("Classloader {}, searching in depends pluginLoaders", this.getPluginName());
        for (PluginClassLoader pluginClassLoader : this.pluginLoaders) {
            if (pluginClassLoader != null) {
                try {
                    pluginsVisiteds.add(pluginClassLoader.getPluginName());
                    ret = pluginClassLoader.getResource(pluginsVisiteds, res);
                    if (ret != null) {
                        logger.debug("Classloader {}, Found resource '{}' in plugin '{}'.",
                                new Object[]{
                                    this.getPluginName(), res, pluginClassLoader.getPluginName(),});
                        return ret;
                    }
                } catch (Exception e) {
                    // Ignore, try in next classloader
                }
            }
        }

        if (ret == null) {
            //
            // Por ultimo en el class loader padre, se supone que es el del sistema.
            try {
                ret = super.getResource(res);
            } catch (Exception e) {
                logger.warn("Classloader {}, error getting resource '{}' in parent classloader'",
                        new Object[]{this.getPluginName(), res},
                        e
                );
            }
            if (ret == null) {
                logger.debug("Classloader {}, Resource '{}' not found.",
                        new Object[]{this.getPluginName(), res}
                );
            }
        }
        return ret;
    }

}
