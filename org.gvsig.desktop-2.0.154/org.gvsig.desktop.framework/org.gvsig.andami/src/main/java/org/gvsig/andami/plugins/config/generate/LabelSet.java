/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: LabelSet.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class LabelSet.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class LabelSet implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _className
     */
    private java.lang.String _className;

    /**
     * Field _labelList
     */
    private java.util.Vector _labelList;


      //----------------/
     //- Constructors -/
    //----------------/

    public LabelSet() {
        super();
        _labelList = new Vector();
    } //-- com.iver.andami.plugins.config.generate.LabelSet()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addLabel
     * 
     * @param vLabel
     */
    public void addLabel(org.gvsig.andami.plugins.config.generate.Label vLabel)
        throws java.lang.IndexOutOfBoundsException
    {
        _labelList.addElement(vLabel);
    } //-- void addLabel(com.iver.andami.plugins.config.generate.Label) 

    /**
     * Method addLabel
     * 
     * @param index
     * @param vLabel
     */
    public void addLabel(int index, org.gvsig.andami.plugins.config.generate.Label vLabel)
        throws java.lang.IndexOutOfBoundsException
    {
        _labelList.insertElementAt(vLabel, index);
    } //-- void addLabel(int, com.iver.andami.plugins.config.generate.Label) 

    /**
     * Method enumerateLabel
     */
    public java.util.Enumeration enumerateLabel()
    {
        return _labelList.elements();
    } //-- java.util.Enumeration enumerateLabel() 

    /**
     * Returns the value of field 'className'.
     * 
     * @return the value of field 'className'.
     */
    public java.lang.String getClassName()
    {
        return this._className;
    } //-- java.lang.String getClassName() 

    /**
     * Method getLabel
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Label getLabel(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _labelList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.Label) _labelList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Label getLabel(int) 

    /**
     * Method getLabel
     */
    public org.gvsig.andami.plugins.config.generate.Label[] getLabel()
    {
        int size = _labelList.size();
        org.gvsig.andami.plugins.config.generate.Label[] mArray = new org.gvsig.andami.plugins.config.generate.Label[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.Label) _labelList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.Label[] getLabel() 

    /**
     * Method getLabelCount
     */
    public int getLabelCount()
    {
        return _labelList.size();
    } //-- int getLabelCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllLabel
     */
    public void removeAllLabel()
    {
        _labelList.removeAllElements();
    } //-- void removeAllLabel() 

    /**
     * Method removeLabel
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Label removeLabel(int index)
    {
        java.lang.Object obj = _labelList.elementAt(index);
        _labelList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.Label) obj;
    } //-- com.iver.andami.plugins.config.generate.Label removeLabel(int) 

    /**
     * Sets the value of field 'className'.
     * 
     * @param className the value of field 'className'.
     */
    public void setClassName(java.lang.String className)
    {
        this._className = className;
    } //-- void setClassName(java.lang.String) 

    /**
     * Method setLabel
     * 
     * @param index
     * @param vLabel
     */
    public void setLabel(int index, org.gvsig.andami.plugins.config.generate.Label vLabel)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _labelList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _labelList.setElementAt(vLabel, index);
    } //-- void setLabel(int, com.iver.andami.plugins.config.generate.Label) 

    /**
     * Method setLabel
     * 
     * @param labelArray
     */
    public void setLabel(org.gvsig.andami.plugins.config.generate.Label[] labelArray)
    {
        //-- copy array
        _labelList.removeAllElements();
        for (int i = 0; i < labelArray.length; i++) {
            _labelList.addElement(labelArray[i]);
        }
    } //-- void setLabel(com.iver.andami.plugins.config.generate.Label) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.LabelSet) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.LabelSet.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
