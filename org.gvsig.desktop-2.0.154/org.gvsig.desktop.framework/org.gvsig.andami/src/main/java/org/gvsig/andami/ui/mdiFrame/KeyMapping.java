/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.KeyStroke;

import org.gvsig.andami.messages.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Clase que sirve para obtener los codigos de las teclas de la manera que las
 * requieren los JMenu a partir de un caracter que representa dicha tecla
 *
 */
public class KeyMapping {
	private static Logger logger = LoggerFactory.getLogger(KeyMapping.class);
	
    /** Asocia caracteres con KeyEvents */
    private static Map<String,Integer> keys = new HashMap<String, Integer>();

    static {
        keys.put("a", new Integer(KeyEvent.VK_A));
        keys.put("b", new Integer(KeyEvent.VK_B));
        keys.put("c", new Integer(KeyEvent.VK_C));
        keys.put("d", new Integer(KeyEvent.VK_D));
        keys.put("e", new Integer(KeyEvent.VK_E));
        keys.put("f", new Integer(KeyEvent.VK_F));
        keys.put("g", new Integer(KeyEvent.VK_G));
        keys.put("h", new Integer(KeyEvent.VK_H));
        keys.put("i", new Integer(KeyEvent.VK_I));
        keys.put("j", new Integer(KeyEvent.VK_J));
        keys.put("k", new Integer(KeyEvent.VK_K));
        keys.put("l", new Integer(KeyEvent.VK_L));
        keys.put("m", new Integer(KeyEvent.VK_M));
        keys.put("n", new Integer(KeyEvent.VK_N));
        keys.put("o", new Integer(KeyEvent.VK_O));
        keys.put("p", new Integer(KeyEvent.VK_P));
        keys.put("q", new Integer(KeyEvent.VK_Q));
        keys.put("r", new Integer(KeyEvent.VK_R));
        keys.put("s", new Integer(KeyEvent.VK_S));
        keys.put("t", new Integer(KeyEvent.VK_T));
        keys.put("u", new Integer(KeyEvent.VK_U));
        keys.put("v", new Integer(KeyEvent.VK_V));
        keys.put("w", new Integer(KeyEvent.VK_W));
        keys.put("x", new Integer(KeyEvent.VK_X));
        keys.put("y", new Integer(KeyEvent.VK_Y));
        keys.put("z", new Integer(KeyEvent.VK_Z));
        keys.put("0", new Integer(KeyEvent.VK_0));
        keys.put("1", new Integer(KeyEvent.VK_1));
        keys.put("2", new Integer(KeyEvent.VK_2));
        keys.put("3", new Integer(KeyEvent.VK_3));
        keys.put("4", new Integer(KeyEvent.VK_4));
        keys.put("5", new Integer(KeyEvent.VK_5));
        keys.put("6", new Integer(KeyEvent.VK_6));
        keys.put("7", new Integer(KeyEvent.VK_7));
        keys.put("8", new Integer(KeyEvent.VK_8));
        keys.put("9", new Integer(KeyEvent.VK_9));
        keys.put("+", new Integer(KeyEvent.VK_PLUS));
        keys.put("-", new Integer(KeyEvent.VK_MINUS));       
        keys.put("f1", new Integer(KeyEvent.VK_F1));       
        keys.put("f2", new Integer(KeyEvent.VK_F2));       
        keys.put("f3", new Integer(KeyEvent.VK_F3));       
        keys.put("f4", new Integer(KeyEvent.VK_F4));       
        keys.put("f5", new Integer(KeyEvent.VK_F5));       
        keys.put("f6", new Integer(KeyEvent.VK_F6));       
        keys.put("f7", new Integer(KeyEvent.VK_F7));       
        keys.put("f8", new Integer(KeyEvent.VK_F8));       
        keys.put("f9", new Integer(KeyEvent.VK_F9));       
        keys.put("f10", new Integer(KeyEvent.VK_F10));       
        keys.put("f11", new Integer(KeyEvent.VK_F11));       
        keys.put("f12", new Integer(KeyEvent.VK_F12));       
        keys.put("delete", new Integer(KeyEvent.VK_DELETE));       
    }

    /**
     * Obtiene dado un caracter el entero correspondiente al codigo que tiene
     * la tecla que produce dicho caracter
     *
     * @param a caracter
     *
     * @return Codigo de la tecla asociada
     *
     * @throws RuntimeException Si el caracter no tiene una tecla asociada
     */
    public static int getKey(char a) {
        Integer ret = (Integer) keys.get(String.valueOf(a));
        if (ret == null) {
            throw new RuntimeException(Messages.getString(
                    "KeyMapping.Caracter_no_valido") + a); //$NON-NLS-1$
        }
        return ret.intValue();
    }
    
    public static KeyStroke getKeyStroke(String accelerator) {
    	if( accelerator == null ) {
    		return null;
    	}
    	accelerator = accelerator.trim();
    	if( accelerator.length() < 1 ) {
    		return null;
    	}
    	String[] parts = accelerator.toLowerCase().split("[+]");
    	int modifiers = 0;
    	int key = 0;
    	for( int i=0; i<parts.length ; i++) {
    		String part = parts[i];
    		if( "ctrl".equals(part) ) {
    			modifiers = modifiers | InputEvent.CTRL_MASK;
    		} else if( "shift".equals(part) ) {
    			modifiers = modifiers | InputEvent.SHIFT_MASK;
    		} else if( "meta".equals(part) ) {
    			modifiers = modifiers | InputEvent.META_MASK;
    		} else if( "alt".equals(part) ) {
    			modifiers = modifiers | InputEvent.ALT_MASK;
    		} else if( "".equals(part) ) { // +
    			key = KeyEvent.VK_PLUS;
    		} else {
    			Integer x = (Integer) keys.get(part);
    			if( x != null ) {
    				key = x.intValue();
    			} else {
    				try {
    					throw new IllegalArgumentException();
    				} catch (IllegalArgumentException ex) {
    					logger.info("getKeyStroke('"+accelerator+"') malformed accelerator.",ex);
    				}
    				return null;
    			}
    		}
    	}
    	if( key == 0 ) {
    		return null;
    	}
    	/*
    	 * We are not adding default modifier, this means
    	 * short cuts must come with modifiers explicitly
    	 * except special keys like F3, F7, etc
    	 */
    	/*
    	if( modifiers == 0 ) {
    		modifiers = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
    	}
    	*/
    	return KeyStroke.getKeyStroke(key,modifiers);
    }
}
