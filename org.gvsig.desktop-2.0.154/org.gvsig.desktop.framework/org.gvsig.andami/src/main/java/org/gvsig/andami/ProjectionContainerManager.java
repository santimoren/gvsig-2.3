package org.gvsig.andami;


/**
 * Don't use this class, it will be removed in nexts versions.
 * 
 * This is a patch to resolve access to projection of view in jcrs
 * plugin and will be removed in nexts versions.
 *  
 * @deprecated 
 */
public class ProjectionContainerManager {
	/**
	 * @deprecated 
	 */
	public interface ProjectionContainer {
		Object getCurrentProjection();
	}
	private static ProjectionContainer currentProjectionContainer = null;
	
	/**
	 * @deprecated 
	 */
	public static void set(ProjectionContainer container) {
		currentProjectionContainer = container;
	}

	/**
	 * @deprecated 
	 */
	public static void unset(ProjectionContainer container) {
		if( container == currentProjectionContainer ) {
			currentProjectionContainer = null;
		}
	}
		
	/**
	 * @deprecated 
	 */
	public static Object getCurrentProjection() {
                if( currentProjectionContainer==null ) {
                    return null;
                } 
		return currentProjectionContainer.getCurrentProjection();
	}

}
