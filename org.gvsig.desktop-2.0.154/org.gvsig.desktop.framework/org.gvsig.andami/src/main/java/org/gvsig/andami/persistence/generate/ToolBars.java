/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: ToolBars.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.persistence.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.util.Vector;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ToolBars.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class ToolBars implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _toolBarList
     */
    private java.util.Vector _toolBarList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ToolBars() {
        super();
        _toolBarList = new Vector();
    } //-- com.iver.andami.persistence.generate.ToolBars()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addToolBar
     * 
     * @param vToolBar
     */
    public void addToolBar(org.gvsig.andami.persistence.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        _toolBarList.addElement(vToolBar);
    } //-- void addToolBar(com.iver.andami.persistence.generate.ToolBar) 

    /**
     * Method addToolBar
     * 
     * @param index
     * @param vToolBar
     */
    public void addToolBar(int index, org.gvsig.andami.persistence.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        _toolBarList.insertElementAt(vToolBar, index);
    } //-- void addToolBar(int, com.iver.andami.persistence.generate.ToolBar) 

    /**
     * Method enumerateToolBar
     */
    public java.util.Enumeration enumerateToolBar()
    {
        return _toolBarList.elements();
    } //-- java.util.Enumeration enumerateToolBar() 

    /**
     * Method getToolBar
     * 
     * @param index
     */
    public org.gvsig.andami.persistence.generate.ToolBar getToolBar(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _toolBarList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.persistence.generate.ToolBar) _toolBarList.elementAt(index);
    } //-- com.iver.andami.persistence.generate.ToolBar getToolBar(int) 

    /**
     * Method getToolBar
     */
    public org.gvsig.andami.persistence.generate.ToolBar[] getToolBar()
    {
        int size = _toolBarList.size();
        org.gvsig.andami.persistence.generate.ToolBar[] mArray = new org.gvsig.andami.persistence.generate.ToolBar[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.persistence.generate.ToolBar) _toolBarList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.persistence.generate.ToolBar[] getToolBar() 

    /**
     * Method getToolBarCount
     */
    public int getToolBarCount()
    {
        return _toolBarList.size();
    } //-- int getToolBarCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllToolBar
     */
    public void removeAllToolBar()
    {
        _toolBarList.removeAllElements();
    } //-- void removeAllToolBar() 

    /**
     * Method removeToolBar
     * 
     * @param index
     */
    public org.gvsig.andami.persistence.generate.ToolBar removeToolBar(int index)
    {
        java.lang.Object obj = _toolBarList.elementAt(index);
        _toolBarList.removeElementAt(index);
        return (org.gvsig.andami.persistence.generate.ToolBar) obj;
    } //-- com.iver.andami.persistence.generate.ToolBar removeToolBar(int) 

    /**
     * Method setToolBar
     * 
     * @param index
     * @param vToolBar
     */
    public void setToolBar(int index, org.gvsig.andami.persistence.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _toolBarList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _toolBarList.setElementAt(vToolBar, index);
    } //-- void setToolBar(int, com.iver.andami.persistence.generate.ToolBar) 

    /**
     * Method setToolBar
     * 
     * @param toolBarArray
     */
    public void setToolBar(org.gvsig.andami.persistence.generate.ToolBar[] toolBarArray)
    {
        //-- copy array
        _toolBarList.removeAllElements();
        for (int i = 0; i < toolBarArray.length; i++) {
            _toolBarList.addElement(toolBarArray[i]);
        }
    } //-- void setToolBar(com.iver.andami.persistence.generate.ToolBar) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.persistence.generate.ToolBars) Unmarshaller.unmarshal(org.gvsig.andami.persistence.generate.ToolBars.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
