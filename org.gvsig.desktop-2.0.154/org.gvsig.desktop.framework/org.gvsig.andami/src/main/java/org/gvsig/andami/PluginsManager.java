/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import java.io.File;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import static org.gvsig.andami.Launcher.appName;

import org.gvsig.andami.firewall.FirewallConfiguration;
import org.gvsig.andami.impl.UnsavedDataException;
import org.gvsig.andami.plugins.ExclusiveUIExtension;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;

public interface PluginsManager {

    public String getApplicationName();

    /**
     * Return the associated pluginServices to the extension class passed as
     * parameter.
     *
     * @param extension
     * @return plugin services
     */
    public PluginServices getPlugin(Class<? extends IExtension> extension);

    /**
     * Return the associated pluginServices to the object passed as parameter.
     *
     * @param object
     * @return plugin services
     */
    public PluginServices getPlugin(Object obj);

    /**
     * Return the associated pluginServices to the extension plugin.
     *
     * @param pluginName, name of the plugin
     * @return plugin services
     */
    public PluginServices getPlugin(String pluginName);

    /**
     * Get the package info associated to the plugin of the extension.
     *
     * @param extension extension of the plugin
     * @return plugin package info
     */
    public PackageInfo getPackageInfo(Class<? extends IExtension> extension);

    /**
     * Get the package info associated to the plugin with the specified name.
     *
     * @param pluginName, name of the plugin
     * @return plugin package info
     */
    public PackageInfo getPackageInfo(String pluginName);

    /**
     * Get the package info associated to the application.
     *
     * @return application package info
     */
    public PackageInfo getPackageInfo();

    /**
     * Return the list of plugins loaded in the application
     *
     * @return list of plugins
     */
    public List<PluginServices> getPlugins();

    /**
     * Gets the instance of the extension whose class is provided.
     *
     * @param extension, class of the extension to retrieve
     *
     * @return The instance of the extension, or null if the instance does
     * not exist.
     */
    public IExtension getExtension(Class<? extends IExtension> extension);

    /**
     * Return an iterator over al extensions loaded in the application.
     *
     * @return iterator over extensions
     */
    public Iterator<IExtension> getExtensions();

    /**
     * Set an extension to control the visivility of all the extensions
     * of the application.
     *
     * @param extension
     */
    public void setExclusiveUIExtension(ExclusiveUIExtension extension);

    /**
     * Return the ExclusiveUIExtension installed in the application or
     * null.
     *
     * @return
     */
    public ExclusiveUIExtension getExclusiveUIExtension();

    /**
     * @deprecated use {@link #translate(String)}
     * @see {@link #translate(String)}
     */
    public String getText(Object obj, String msg);

    /**
     * Translate the message key to the current language used
     * in the application.
     *
     * @param msg key to translate
     * @return message traslated or the key
     */
    public String translate(String msg);

    /**
     * Returns the main application folder, where the application is installed.
     *
     * @return the main application folder
     */
    public File getApplicationFolder();

    /**
     * Returns the default folder with the installable package bundles.
     *
     * @return the default folder with the installable package bundles
     */
    public File getInstallFolder();

    /**
     * Returns the application folder in the home of user.
     *
     * @return the main application folder
     */
    public File getApplicationHomeFolder();


    /**
     * Returns the default folder where the internationalization files are installed.
     *
     * @return the default folder where the internationalization files are installed.
     */
    public File getApplicationI18nFolder();

    /**
     * Returns the plugins folder.
     *
     * @return the plugins folder
     * @deprecated use {@link #getPluginsFolders()}
     */
    public File getPluginsFolder();

    /**
     * Returns a list of folder on the plugins are instaleds.
     *
     * @return list of folders File of the plugins.
     */
    public List<File> getPluginsFolders();

    /**
     * @deprecated @see {@link #getPluginsFolders()}
     */
    public File getPluginsDirectory();

    /**
     * This method allows plugins register task to do before initializacion of
     * all gvSIG plugins.
     *
     * Task with higher priority value are executed after.
     *
     * @param name of the task
     * @param task runnable with the code of the task
     * @param in_event_thread, true if the task shoul run in the AWT event thread.
     * @param priority of the task.
     */
    public void addStartupTask(String name, Runnable task, boolean in_event_thread, int priority);

    /**
     * This method allows plugins register task to do after close the application.
     *
     * Task with higher priority value are executed after.
     *
     * @param name of the task
     * @param task runnable with the code of the task
     * @param in_event_thread, true if the task shoul run in the AWT event thread.
     * @param priority of the task.
     */
    public void addShutdownTask(String name, Runnable task, boolean in_event_thread, int priority);

    /**
     * This method executes registered task to run before the initialization of
     * all plugins.
     * The tasks are sorted according to the priority and executed in a separated
     * thread.
     *
     */
    public void executeStartupTasks();

    /**
     * This method executes registered task to run after the close of the application.
     * The tasks are sorted according to the priority and executed in a separated
     * thread.
     *
     */
    public void executeShutdownTasks();


    public FirewallConfiguration getFirewallConfiguration();

    public Version getApplicationVersion();

    /**
     * Return a list of unsaved data
     *
     * @return
     */
    public List<IUnsavedData> getUnsavedData();

    /**
     * Save the provided unsaved data as parameter
     *
     * @param unsavedData
     * @throws UnsavedDataException
     */
    public void saveUnsavedData(List<IUnsavedData> unsavedData) throws UnsavedDataException;

    /**
     * Gets the temporary folder of the plugins framework.
     *
     * @return the temp folder
     */
    public File getTempFolder();

    /**
     * Get the file name in the temporary folder of the plugins framework.
     * @param name
     * @return
     */
    public File getTempFile(String name);

    /**
     * Create a unique file name in the temporary folder of the plugins framework.
     *
     * @param name base name used for create the unique file name
     * @param sufix to add the the unique file name
     * @return
     */
    public File getTempFile(String name, String sufix);

    /**
     * Browses to a given URI
     * @param uri
     * @return
     */
    public boolean desktopBrowse(URI uri);

    /**
     * Open a given file
     * @param file
     * @return
     */
    public boolean desktopOpen(File file);


}
