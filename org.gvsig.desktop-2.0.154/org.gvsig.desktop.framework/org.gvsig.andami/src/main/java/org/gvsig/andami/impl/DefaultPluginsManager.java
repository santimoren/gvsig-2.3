/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.impl;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.commons.io.FileUtils;
import org.jfree.util.Log;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.Utilities;
import org.gvsig.andami.config.generate.AndamiConfig;
import org.gvsig.andami.config.generate.Plugin;
import org.gvsig.andami.firewall.DefaultFirewallConfiguration;
import org.gvsig.andami.firewall.FirewallConfiguration;
import org.gvsig.andami.plugins.ExclusiveUIExtension;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.PluginClassLoader;
import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.packageutils.PackageManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPluginsManager implements PluginsManager {

    private class Task implements Comparable, Runnable {

        private String type = "";
        private Runnable task = null;
        private boolean in_event_thread = false;
        private int priority = 0;
        private String name = null;

        public Task(String type, String name, Runnable task, boolean in_event_thread, int priority) {
            this.type = type;
            this.in_event_thread = in_event_thread;
            this.task = task;
            this.priority = priority;
            this.name = name;
        }

        public int compareTo(Object t) {
            return this.priority - ((Task) t).priority;
        }

        public boolean equals(Object t) {
            return this.compareTo(t) == 0;
        }

        public void run() {
            if ( this.in_event_thread ) {
                if ( !SwingUtilities.isEventDispatchThread() ) {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                Task.this.run();
                            }
                        });
                    } catch (InterruptedException ex) {
                        // Do nothing
                    } catch (InvocationTargetException ex) {
                        logger.warn("Errors in execution of " + type + " task '" + name + "'.", ex);

                    }
                    return;
                }
            }
            logger.info("Running " + type + " task '" + name + "' (priority " + priority + ").");
            try {
                task.run();
                logger.info("Terminated " + type + " task '" + name + "'.");
            } catch (Throwable ex) {
                // Catch Exceptions and Errors (class not found)
                logger.warn("Errors in execution of " + type + " task '" + name + "'.", ex);
            }
        }

    }

    private static Logger logger
            = LoggerFactory.getLogger(DefaultPluginsManager.class);

    private List<File> pluginsFolders = null;
    private List<Task> startupTasks = new ArrayList<Task>();
    private List<Task> shutdownTasks = new ArrayList<Task>();

    public ExclusiveUIExtension getExclusiveUIExtension() {
        return PluginServices.getExclusiveUIExtension();
    }

    public IExtension getExtension(Class<? extends IExtension> extension) {
        return PluginServices.getExtension(extension);
    }

    @SuppressWarnings("unchecked")
    public Iterator<IExtension> getExtensions() {
        return PluginServices.getExtensions();
    }

    /**
     * Return the associated pluginServices to the extension class passed as
     * parameter.
     *
     */
    public PluginServices getPlugin(Class<? extends IExtension> extension) {
        String pluginName = ((PluginClassLoader) extension.getClassLoader()).getPluginName();
        return this.getPlugin(pluginName);
    }

    public PluginServices getPlugin(Object obj) {
        if ( obj instanceof Extension ) {
            return ((Extension)obj).getPlugin();
        }
        if ( obj instanceof IExtension ) {
            Class<? extends IExtension> klass = (Class<? extends IExtension>) obj.getClass();
            return this.getPlugin(klass);
        }
        PluginClassLoader loader = (PluginClassLoader) obj.getClass().getClassLoader();
        String pluginName = loader.getPluginName();
        return this.getPlugin(pluginName);
    }

    public PluginServices getPlugin(String pluginName) {
        return Launcher.getPluginServices(pluginName);
    }

    public PackageInfo getPackageInfo(Class<? extends IExtension> extension) {
        PackageManager pkgmgr = ToolsLocator.getPackageManager();
        File pinfo_file = new File(this.getPlugin(extension).getPluginDirectory(), "package.info");

        PackageInfo packageInfo = null;
        try {
            packageInfo = pkgmgr.createPackageInfo(pinfo_file);
        } catch (Exception e) {
            logger.info("Error while reading package info file from "
                    + pinfo_file.toString(), e);
        }
        return packageInfo;
    }

    public PackageInfo getPackageInfo(String pluginName) {
        PackageManager pkgmgr = ToolsLocator.getPackageManager();
        File pinfo_file = new File(this.getPlugin(pluginName)
                .getPluginDirectory(), "package.info");

        PackageInfo packageInfo = null;
        try {
            packageInfo = pkgmgr.createPackageInfo(pinfo_file);
        } catch (Exception e) {
            logger.info("Error while reading package info file from "
                    + pinfo_file.toString(), e);
        }
        return packageInfo;
    }

    public PackageInfo getPackageInfo() {
        PackageManager pkgmgr = ToolsLocator.getPackageManager();
        File pinfo_file = new File(
                this.getApplicationFolder(), "package.info");
        PackageInfo packageInfo = null;
        try {
            packageInfo = pkgmgr.createPackageInfo(pinfo_file);
        } catch (Exception e) {
            logger.info("Error while reading package info file from "
                    + pinfo_file.toString(), e);
        }
        return packageInfo;
    }

    @SuppressWarnings("unchecked")
    public List<PluginServices> getPlugins() {
        List<PluginServices> pluginServices = new ArrayList<PluginServices>();

        AndamiConfig config = Launcher.getAndamiConfig();
        Enumeration<Plugin> plugins = config.enumeratePlugin();
        while ( plugins.hasMoreElements() ) {
            Plugin plugin = plugins.nextElement();
            pluginServices.add(PluginServices.getPluginServices(plugin.getName()));
        }
        return pluginServices;
    }

    public void setExclusiveUIExtension(ExclusiveUIExtension extension) {
        PluginServices.setExclusiveUIExtension(extension);
    }

    public String getText(Object obj, String msg) {
        return PluginServices.getText(obj, msg);
    }

    public String translate(String msg) {
        return org.gvsig.i18n.Messages.translate(msg);
    }

    public File getApplicationFolder() {
        return Launcher.getApplicationFolder();
    }

    /**
     * @deprecated use {@link #getPluginsFolders()}
     */
    public File getPluginsDirectory() {
        return getPluginsFolder();
    }

    /**
     * @deprecated use {@link #getPluginsFolders()}
     */
    public File getPluginsFolder() {
        List<File> l = this.getPluginsFolders();
        if ( l == null || l.size() < 1 ) {
            return null;
        }
        return l.get(0);
    }

    public List<File> getPluginsFolders() {
        if ( this.pluginsFolders != null ) {
            return this.pluginsFolders;
        }
        File folder;
        String folderPath = "gvSIG/extensiones";
        if ( !(Launcher.getAndamiConfig() == null || Launcher.getAndamiConfig().getPluginsDirectory() == null) ) {
            folderPath = Launcher.getAndamiConfig().getPluginsDirectory();
        }

        this.pluginsFolders = new ArrayList<File>();

        folder = new File(this.getApplicationFolder(), folderPath);
        if( !folder.exists() ) {
            try {
                FileUtils.forceMkdir(folder);
            } catch (IOException ex) {
                logger.warn("The plugins folder '"+folder.getAbsolutePath()+"' don't exist and can't create.",ex);
            }
        }
        this.pluginsFolders.add(folder);

        folder = new File(this.getApplicationHomeFolder(), "installation");
        folder = new File(folder, folderPath);
        if( !folder.exists() ) {
            try {
                FileUtils.forceMkdir(folder);
            } catch (IOException ex) {
                logger.warn("The plugins folder '"+folder.getAbsolutePath()+"' don't exist and can't create.",ex);
            }
        }
        this.pluginsFolders.add(folder);

        return this.pluginsFolders;
    }

    public File getInstallFolder() {
        return new File(getApplicationFolder(), "install");
    }

    public File getApplicationHomeFolder() {
        return Launcher.getApplicationHomeFolder();
    }

    public void addStartupTask(String name, Runnable task, boolean in_event_thread, int priority) {
        this.startupTasks.add(new Task("startup", name, task, in_event_thread, priority));
    }

    public void addShutdownTask(String name, Runnable task, boolean in_event_thread, int priority) {
        this.shutdownTasks.add(new Task("shutdown", name, task, in_event_thread, priority));
    }

    public void executeStartupTasks() {
        logger.info("Executing startup tasks.");
        Thread th = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(10);
                } catch (Exception exc) {
                    // Ignore error
                }
                Collections.sort(startupTasks);
                for ( int i = startupTasks.size() - 1; i >= 0; i-- ) {
                    Task task = startupTasks.get(i);
                    task.run();
                }
            }
        });
        th.start();
    }

    public void executeShutdownTasks() {
        logger.info("Executing shutdown tasks.");
        Collections.sort(shutdownTasks);
        for ( int i = shutdownTasks.size() - 1; i >= 0; i-- ) {
            Task task = shutdownTasks.get(i);
            task.run();
        }
    }

    public File getApplicationI18nFolder() {
        return new File(this.getApplicationFolder(), "i18n");
    }

    public FirewallConfiguration getFirewallConfiguration() {
    	return (FirewallConfiguration) ToolsLocator.getFirewallManager();
    }

    public Version getApplicationVersion() {
        PackageInfo pinfo = this.getPackageInfo();
        Version version = pinfo.getVersion();
        return version;
    }

    public List<IUnsavedData> getUnsavedData() {
        List<IUnsavedData> unsavedDatas = new ArrayList<IUnsavedData>();
        Iterator<IExtension> extensions = getExtensions();
        while(extensions.hasNext()){
            IExtension extension = extensions.next();
            IExtensionStatus status = extension.getStatus();
            if(status != null && status.hasUnsavedData()){
              IUnsavedData[] unsavedData = status.getUnsavedData();
              if (unsavedData != null){
                  unsavedDatas.addAll(Arrays.asList(unsavedData));
              }
            }
        }
        return unsavedDatas;
    }

    public void saveUnsavedData(List<IUnsavedData> unsavedData) throws UnsavedDataException {
        List<IUnsavedData> errors = new ArrayList<IUnsavedData>();

        for (Iterator iterator = unsavedData.iterator(); iterator.hasNext();) {
            IUnsavedData itemUnsavedData = (IUnsavedData) iterator.next();
            try {
                itemUnsavedData.saveData();
            } catch (Exception e) {
                errors.add(itemUnsavedData);
                logger.warn("Can't save "+itemUnsavedData.getResourceName());
            }
        }
        if(!errors.isEmpty()){
            throw new UnsavedDataException(errors);
        }
    }

    @Override
    public String getApplicationName() {
        return Launcher.getApplicationName();
    }

    @Override
    public File getTempFolder() {
        return new File(Utilities.TEMPDIRECTORYPATH);
    }

    @Override
    public File getTempFile(String name) {
        return new File(Utilities.TEMPDIRECTORYPATH, name);
    }

    @Override
    public File getTempFile(String name, String sufix) {
        File tempFolder = new File(Utilities.TEMPDIRECTORYPATH);
        if( !tempFolder.exists() ) {
            try {
                FileUtils.forceMkdir(tempFolder);
            } catch(Throwable th) {
                throw new RuntimeException(th);
            }
        }
        long t = new Date().getTime();
        String fname = String.format("%s-%x%x%s", name,t,sufix);
        return new File(tempFolder,fname);
    }

    @Override
    public boolean desktopBrowse(URI uri){
        return DesktopApi.browse(uri);
    }

    @Override
    public boolean desktopOpen(File file){
        return DesktopApi.open(file);
    }
}
