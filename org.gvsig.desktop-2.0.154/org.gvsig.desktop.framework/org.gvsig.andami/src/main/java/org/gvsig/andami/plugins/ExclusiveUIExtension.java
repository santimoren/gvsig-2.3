/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins;

/**
 * Extensions implementing this interface are able to take control
 * over the user interface, by deciding which of the other extensions
 * will be enabled/disabled or visible/hidden.
 * 
 * Besides implementing this interface, the extension needs to
 * be set as ExclusiveUIExtension during Andami startup process.
 * This is performed by providing a command line argument to
 * Andami:
 * <br>
 * <code>ExclusiveUIExtension=ExtensionName</code>
 *
 * @author Cesar Martinez Izquierdo <cesar.martinez@iver.es>
 */
public interface ExclusiveUIExtension extends IExtension {
	
    /**
     * This method is used when this extension is installed as
     * ExclusiveUIExtension. This extension will be asked for
     * each installed extension to determine which of them
     * should be enabled.
     * 
     * @return true if the provided extension should be enabled,
     * false if it should be disabled.
     */
    public boolean isEnabled(IExtension extension);

    /**
     * This method is used when this extension is installed as
     * ExclusiveUIExtension. This extension will be asked for
     * each installed extension to determine which of them
     * should be visible.
     * 
     * @return true if the provided extension should be visible,
     * false if it should be hidden.
     */
    public boolean isVisible(IExtension extension);
}
