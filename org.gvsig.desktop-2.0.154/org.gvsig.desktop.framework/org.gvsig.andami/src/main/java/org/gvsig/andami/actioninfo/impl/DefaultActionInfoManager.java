/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.actioninfo.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.actioninfo.ActionInfoStatusCache;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultActionInfoManager implements ActionInfoManager {

    private static Logger logger = LoggerFactory.getLogger(DefaultActionInfoManager.class);

    private Map<String, ActionInfo> actions = new HashMap<String, ActionInfo>();
    private int anonymousCounter = 1;

    public ActionInfo createAction(Class<? extends IExtension> extension, String name, String text, String command, String icon, String accelerator, long position, String tip) {
        name = emptyToNull(name);
        String actionName = name;
        if (actionName == null) {
            actionName = String.format("anonymous__%04d", this.anonymousCounter++);
        }
        ActionInfo action = new DefaultActionInfo(extension, actionName, text, command, icon, accelerator, position, tip);
        ActionInfo previous = this.getAction(action.getName());
        if (previous != null) {
            ((DefaultActionInfo) action).merge(previous);
        }
        if (name == null) {
            logger.info("createAction: name of action is null/empty, rename to '" + actionName + "' (" + action.toString() + ").");
        }
        if (action.getLabel() == null && action.getIconName() == null) {
            logger.info("createAction(name='" + name + "'): text and icon of action is null");
        }
        return action;
    }

    public ActionInfo createAction(IExtension extension, String name, String text, String command, String icon, String accelerator, long position, String tip) {
        name = emptyToNull(name);
        String actionName = name;
        if (actionName == null) {
            actionName = String.format("anonymous__%04d", this.anonymousCounter++);
        }
        ActionInfo action = new DefaultActionInfo(extension, actionName, text, command, icon, accelerator, position, tip);
        ActionInfo previous = this.getAction(action.getName());
        if (previous != null) {
            ((DefaultActionInfo) action).merge(previous);
        }
        if (name == null) {
            logger.info("createAction: name of action is null/empty, rename to '" + actionName + "' (" + action.toString() + ").");
        }
        if (action.getLabel() == null && action.getIconName() == null) {
            logger.info("createAction(name='" + name + "'): text and icon of action is null");
        }
        return action;
    }

    private String emptyToNull(String s) {
        if (s == null) {
            return null;
        }
        return "".equals(s.trim()) ? null : s;
    }

    public ActionInfo registerAction(ActionInfo action) {
        if (action == null) {
    		// Avisamos en el log de que se intenta registrar una accion null, intentado
            // sacar el stack para que se pueda ver quien lo esta haciendo, pero no
            // provocamos un error, solo retornamos null.
            try {
                throw new IllegalArgumentException();
            } catch (IllegalArgumentException e) {
                logger.info("registerAction(null).", e);
            }
            return null;
        }
        ActionInfo previous = this.getAction(action.getName());
        if (previous != null) {
            ((DefaultActionInfo) previous).merge(action);
            return previous;
        } else {
            this.actions.put(action.getName(), action);
            SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
            identityManager.registerAction(action.getName());
            return action;
        }
    }

    public ActionInfo getAction(String name) {
        if (name == null || "".equals(name)) {
            try {
                throw new IllegalArgumentException();
            } catch (IllegalArgumentException e) {
                logger.info("getAction(null/empty) return null.", e);
                return null;
            }
        }
        return this.actions.get(name);
    }

    public Iterator<ActionInfo> getActions() {
        List<ActionInfo> actions = new ArrayList<ActionInfo>();
        actions.addAll(this.actions.values());
        Collections.sort(actions, new Comparator<ActionInfo>() {
            public int compare(ActionInfo arg0, ActionInfo arg1) {
                String s0 = String.format("%s/%012d", arg0.getPluginName(), arg0.getPosition());
                String s1 = String.format("%s/%012d", arg1.getPluginName(), arg1.getPosition());
                return s0.compareTo(s1);
            }
        ;
        });
    	return actions.iterator();
    }

    public ActionInfoStatusCache createActionStatusCache() {
        return new DefaultActionInfoStatusCache();
    }

    public void redirect(String sourceName, String targetName) {
        ActionInfo source = this.getAction(sourceName);
        if (source == null) {
            throw new IllegalArgumentException("Can't locate source action '" + sourceName + "'.");
        }
        ActionInfo target = this.getAction(targetName);
        if (target == null) {
            throw new IllegalArgumentException("Can't locate target action '" + targetName + "'.");
        }
        source.getRedirections().add(target);
    }

    public void execute(String actionName, Object[] parameters) {
        ActionInfo action = this.actions.get(actionName);
        if (action == null) {
            return;
        }
        action.execute(parameters);
    }

    public ActionInfo getTranslated(ActionInfo actionInfo) {
        return new TranslatedActionInfo(actionInfo);
    }

}
