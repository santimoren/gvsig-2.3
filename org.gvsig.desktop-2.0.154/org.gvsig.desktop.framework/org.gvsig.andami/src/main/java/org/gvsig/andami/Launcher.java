/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.channels.FileChannel;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Policy;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.ClassLoaderObjectInputStream;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.config.generate.Andami;
import org.gvsig.andami.config.generate.AndamiConfig;
import org.gvsig.andami.config.generate.Plugin;
import org.gvsig.andami.impl.UnsavedDataException;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.persistence.serverData.ServerDataPersistence;
import org.gvsig.andami.plugins.ExclusiveUIExtension;
import org.gvsig.andami.plugins.ExtensionDecorator;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.PluginClassLoader;
import org.gvsig.andami.plugins.config.generate.Action;
import org.gvsig.andami.plugins.config.generate.ActionTool;
import org.gvsig.andami.plugins.config.generate.AlternativeNames;
import org.gvsig.andami.plugins.config.generate.ComboButton;
import org.gvsig.andami.plugins.config.generate.ComboButtonElement;
import org.gvsig.andami.plugins.config.generate.ComboScale;
import org.gvsig.andami.plugins.config.generate.Depends;
import org.gvsig.andami.plugins.config.generate.Extension;
import org.gvsig.andami.plugins.config.generate.Extensions;
import org.gvsig.andami.plugins.config.generate.LabelSet;
import org.gvsig.andami.plugins.config.generate.Menu;
import org.gvsig.andami.plugins.config.generate.PluginConfig;
import org.gvsig.andami.plugins.config.generate.PopupMenu;
import org.gvsig.andami.plugins.config.generate.PopupMenus;
import org.gvsig.andami.plugins.config.generate.SelectableTool;
import org.gvsig.andami.plugins.config.generate.SkinExtension;
import org.gvsig.andami.plugins.config.generate.SkinExtensionType;
import org.gvsig.andami.plugins.config.generate.ToolBar;
import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.andami.ui.AndamiEventQueue;
import org.gvsig.andami.ui.DisablePluginsConflictingLayoutPanel;
import org.gvsig.andami.ui.MDIManagerLoadException;
import org.gvsig.andami.ui.ToolsWindowManager;
import org.gvsig.andami.ui.fonts.FontUtils;
import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.andami.ui.mdiManager.MDIManagerFactory;
import org.gvsig.andami.ui.splash.MultiSplashWindow;
import org.gvsig.andami.ui.theme.Theme;
import org.gvsig.andami.ui.wizard.UnsavedDataPanel;
import org.gvsig.andami.ui.wizard.UnsavedDataPanel.UnsavedDataPanelListener;
import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.api.wizard.InstallerWizardActionListener;
import org.gvsig.installer.swing.api.wizard.InstallerWizardPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.exception.ListBaseException;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.gvsig.tools.util.FolderSet;
import org.gvsig.tools.util.FolderSet.FolderEntry;
import org.gvsig.utils.DateTime;
import org.gvsig.utils.DefaultListModel;
import org.gvsig.utils.XMLEntity;
import org.gvsig.utils.xml.XMLEncodingUtils;
import org.gvsig.utils.xmlEntity.generate.XmlTag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Andami's launching class. This is the class used to create the Andami's
 * plugin environment.<br>
 * </p>
 *
 * <p>
 * <b>Syntax:</b> <br>
 * java [-Xmx512M (for 512MB of RAM)] [-classpath={a colon-separated(unix) or
 * semicolon-separated(windows) list of files containg base library of classes}]
 * [-Djava.library.path=PATH_TO_NATIVE_LIBRARIES]
 * PATH_TO_APPLICATION_HOME_DIRECTORY PATH_TO_APPLICATION_PLUGINS_DIRECTORY
 * [{list of additional custom application arguments separated by spaces}]
 * </p>
 *
 *
 * @author $author$
 * @version $Revision: 40305 $
 */
public class Launcher {

    public static abstract class MapWithAlias<Item> extends HashMap<String, Item> {

        private HashMap<String, String> aliases = new HashMap<String, String>();

        public abstract String[] getAliases(Item item);

        public boolean isAlias(String key) {
            return aliases.get(key) != null;
        }

        public String getMainKey(String key) {
            Item item = super.get(key);
            if (item != null) {
                return key;
            }
            String alias = aliases.get(key);
            if (alias != null) {
                return alias;
            }
            return null;
        }

        public Item get(Object key) {
            Item item = super.get(key);
            if (item != null) {
                return item;
            }
            String alias = aliases.get(key);
            if (alias != null) {
                return super.get(alias);
            }
            return null;
        }

        public boolean containsKey(Object key) {
            boolean contains = super.containsKey(key);
            if (contains) {
                return true;
            }
            String alias = aliases.get(key);
            return super.containsKey(alias);
        }

        public Item put(String key, Item value) {
            super.put(key, value);
            String[] aliases = getAliases(value);
            if (aliases == null) {
                return value;
            }
            for (int n = 0; n < aliases.length; n++) {
                this.aliases.put(aliases[n].trim(), key);
            }
            return value;
        }

        public void putAll(Map<? extends String, ? extends Item> m) {
            Iterator<?> it = m.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Item> x = (Map.Entry<String, Item>) it.next();
                this.put(x.getKey(), x.getValue());
            }
        }

        public Item remove(Object key) {
            Item item = super.get(key);
            if (item == null) {
                String alias = aliases.get(key);
                if (alias == null) {
                    return null;
                }
                item = super.get(alias);
                super.remove(alias);
            } else {
                super.remove(key);
            }
            String[] aliases = getAliases(item);
            if (aliases == null) {
                return item;
            }
            // Rebuild the alias list
            this.aliases = new HashMap<String, String>();
            Iterator<java.util.Map.Entry<String, Item>> it = this.entrySet().iterator();
            while (it.hasNext()) {
                java.util.Map.Entry<String, Item> entry = it.next();
                aliases = getAliases(entry.getValue());
                if (aliases == null) {
                    continue;
                }
                for (int n = 0; n < aliases.length; n++) {
                    this.aliases.put(aliases[n].trim(), entry.getKey());
                }
            }

            return item;
        }

    }

    public static class PluginsConfig extends MapWithAlias<org.gvsig.andami.plugins.config.generate.PluginConfig> {

        public String[] getAliases(
                org.gvsig.andami.plugins.config.generate.PluginConfig item) {
            return getAlternativeNames(item);
        }

        static String[] getAlternativeNames(org.gvsig.andami.plugins.config.generate.PluginConfig item) {
            AlternativeNames[] x = item.getAlternativeNames();
            if (x == null) {
                return null;
            }
            String[] r = new String[x.length];
            for (int i = 0; i < x.length; i++) {
                r[i] = x[i].getName();
            }
            return r;
        }

    }

    public static class PluginsServices extends MapWithAlias<org.gvsig.andami.PluginServices> {

        public String[] getAliases(org.gvsig.andami.PluginServices item) {
            return item.getAlternativeNames();
        }
    }

    protected static Logger logger = LoggerFactory.getLogger(Launcher.class
            .getName());
    protected static Preferences prefs = Preferences.userRoot().node(
            "gvsig.connection");
    protected static AndamiConfig andamiConfig;
    protected static MultiSplashWindow splashWindow;
    protected static String appName;
    protected static Locale locale;
    protected static PluginsConfig pluginsConfig = new PluginsConfig();
    protected static PluginsServices pluginsServices = new PluginsServices();
    protected static MDIFrame frame;
    protected static HashMap<Class<? extends IExtension>, ExtensionDecorator> classesExtensions = new HashMap<Class<? extends IExtension>, ExtensionDecorator>();
    protected static String andamiConfigPath;
    protected static final String nonWinDefaultLookAndFeel = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";

    protected static List<String> pluginsOrdered = new ArrayList<String>();
    protected static List<IExtension> extensions = new ArrayList<IExtension>();
    protected static String appHomeDir = null;
    // it seems castor uses this encoding
    protected static final String CASTORENCODING = "UTF8";

    protected static ListBaseException launcherrors = null;

    protected static Theme theme = null;

    private List<String> deprecatedPluginNames = null;

    private static final class ProxyAuth extends Authenticator {

        private PasswordAuthentication auth;

        private ProxyAuth(String user, String pass) {
            auth = new PasswordAuthentication(user, pass.toCharArray());
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return auth;
        }
    }

    private static Launcher launcherInstance;

    public static Launcher getInstance() {
        if (launcherInstance == null) {
            launcherInstance = new Launcher();
        }
        return launcherInstance;
    }

    public static void main(String[] args) throws Exception {
        Launcher launcher = getInstance();
        boolean install = false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("--install")) {
                install = true;
            }
        }

        try {
            if (install) {
                launcher.doInstall(args);
            } else {
                launcher.doMain(args);
            }
        } catch (Exception e) {
            logger.error("excepci�n al arrancar", e);
            System.exit(-1);
        }
    }

    protected void downloadExtensions(String extDir) {
        // do nothing
    }

    public static class LaunchException extends ListBaseException {

        private static final long serialVersionUID = 4541192746962684705L;

        public LaunchException() {
            super("Errors in initialization of application.",
                    "_errors_in_initialization_of_application",
                    serialVersionUID);
        }

    }

    protected void addError(Throwable ex) {
        if (launcherrors == null) {
            launcherrors = new LaunchException();
        }
        launcherrors.add(ex);
    }

    protected void addError(String msg, Throwable cause) {
        logger.error(msg, cause);
        this.addError(new RuntimeException(msg, cause));
    }

    protected void addError(String msg) {
        this.addError(msg, null);
    }

    private String translate(String msg) {
        return PluginServices.getText(Launcher.class, msg);
    }

    private List<String> getDeprecatedPluginNames() {
        if (deprecatedPluginNames == null) {
            String[] ss = new String[]{
                "org.gvsig.app",
                "org.gvsig.coreplugin",
                "org.gvsig.editing",
                "org.gvsig.installer.app.extension",
                "org.gvsig.exportto.app.extension"
            };
            deprecatedPluginNames = Arrays.asList(ss);
        }
        return deprecatedPluginNames;
    }

    public void doMain(String[] args) throws Exception {

        if (args.length < 1) {
            System.err.println("Usage: Launcher appName plugins-directory [language=locale]");
            System.err.println("No arguments specified.");
            System.err.println("Use default arguments 'gvSIG gvSIG/extensiones'");
            args = new String[]{"gvSIG", "gvSIG/extensiones"};
        }

        initializeApp(args, null);

		// Solucionamos el problema de permisos que se produc�do con Java
        // Web Start con este codigo.
        // System.setSecurityManager(null);
        Policy.setPolicy(new Policy() {

            public PermissionCollection getPermissions(CodeSource codesource) {
                Permissions perms = new Permissions();
                perms.add(new AllPermission());
                return (perms);
            }

            public void refresh() {
            }
        });

        new DefaultLibrariesInitializer().fullInitialize(true);
        InstallerLocator.getInstallerManager().setDownloadBaseURL(
                new URL("http://downloads.gvsig.org/download/gvsig-desktop/"));

        try {
            initIconThemes();
        } catch (Exception ex) {
            this.addError("Can't initialize icon theme", ex);
        }
        // Registramos los iconos base
        try {
            registerIcons();
        } catch (Exception ex) {
            this.addError("Can't register icons", ex);
        }

        // Obtener la personalizaci�n de la aplicacion.
        try {
            logger.info("Initialize andami theme");
            theme = getTheme(andamiConfig.getPluginsDirectory());
        } catch (Exception ex) {
            this.addError("Can't get personalized theme for the application",
                    ex);
        }
        UIManager.put("Desktop.background", theme.getBackgroundColor());

        // Mostrar la ventana de inicio
        Frame f = new Frame();
        splashWindow = new MultiSplashWindow(f, theme, 27);

        // Ponemos los datos del proxy
        splashWindow.process(translate("SplashWindow.configuring_proxy"));
        logger.info("Configute http proxy");
        configureProxy();

        // Buscar actualizaciones de los plugins
        splashWindow.process(translate("SplashWindow.looking_for_updates"));
        try {
//			this.downloadExtensions(andamiConfig.getPluginsDirectory());
        } catch (Exception ex) {
            this.addError("Can't downloads plugins", ex);
        }

        splashWindow.process(translate("SplashWindow.initialize_install_manager"));
        initializeInstallerManager();

        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        PackageInfo[] installedPackages = null;
        try {
            installedPackages = installerManager.getInstalledPackages();
        } catch (MakePluginPackageServiceException e) {
            // Do nothing, ignore errors
        }
        logger.info("Dump system information");
        logger_info(getInformation(installedPackages));
        saveEnvironInformation(installedPackages);

        // Se leen los config.xml de los plugins
        splashWindow.process(translate("SplashWindow.load_plugins_configuration"));
        try {
            logger.info("Load plugins information");
            this.loadPluginConfigs();
            if (pluginsConfig.isEmpty()) {
                logger.warn("No valid plugin was found.");
                System.exit(-1);
            }
        } catch (Throwable ex) {
            this.addError("Can't load plugins", ex);
        }

        splashWindow.process(translate("SplashWindow.check_incompatible_plugins"));
        fixIncompatiblePlugins(installedPackages);

        // Se configura el classloader del plugin
        splashWindow.process(translate("SplashWindow.setup_plugins_configuration"));
        try {
            logger.info("Configure plugins class loader");
            this.loadPluginServices();
        } catch (Throwable ex) {
            logger.warn("Can't initialize plugin's classloaders  ", ex);
        }
        try {
            registerActions();
        } catch (Throwable ex) {
            logger.warn("Can't register actions of plugins", ex);
        }

        initializeIdentityManagement(new File(andamiConfig.getPluginsDirectory()).getAbsoluteFile());

        // Initialize libraries
        splashWindow.process(translate("SplashWindow.initialize_plugins_libraries"));
        initializeLibraries();

        // Se carga un Skin si alguno ide los plugins trae informacion para ello
        splashWindow.process(translate("SplashWindow.looking_for_a_skin"));
        logger.info("Initialize skin");
        skinPlugin(null);

        // Se configura la cola de eventos
        splashWindow.process(translate("setting_up_event_queue"));
        EventQueue waitQueue = new AndamiEventQueue();
        Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitQueue);

        // Se configura la internacionalizacion del plugin
        splashWindow.process(translate("SplashWindow.starting_plugin_internationalization_system"));
        pluginsMessages();

        // Se modifica el andami-config con los plugins nuevos
        splashWindow.process(translate("SplashWindow.update_framework_configuration"));
        updateAndamiConfig();

        frame = MDIFrame.getInstance();
        // Se configura el nombre e icono de la aplicacion
        splashWindow.process(translate("SplashWindow.setting_up_applications_name_and_icons"));
        frameIcon(theme);

        // Se prepara el MainFrame para albergar las extensiones
        splashWindow.process(translate("SplashWindow.preparing_workbench"));
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                frame.init();
            }
        });
        ToolsSwingLocator.registerWindowManager(ToolsWindowManager.class);

        // Leer el fichero de persistencia de los plugins
        splashWindow.process(translate("SplashWindow.loading_plugin_settings"));
        loadPluginsPersistence();

		// Se instalan los controles del skin
        // Se inicializan todas las extensiones de todos los plugins
        splashWindow.process(translate("SplashWindow.initializing_extensions"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                initializeExtensions();
            }
        });

        // Se inicializan la extension exclusiva
        splashWindow.process(translate("SplashWindow.setting_up_master_extension"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                initializeExclusiveUIExtension();
            }
        });
        frame.setClassesExtensions(classesExtensions);

        // Se instalan los controles de las extensiones de los plugins
        message(translate("SplashWindow.installing_extensions_controls"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                installPluginsControls();
            }
        });

        // Se instalan los menus de las extensiones de los plugins
        message(translate("SplashWindow.installing_extensions_menus"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                installPluginsMenus();
            }
        });

        message(translate("SplashWindow.initializing_server_data_persistence"));
        ServerDataPersistence.registerPersistence();

        // Se instalan las etiquetas de las extensiones de los plugins
        message(translate("SplashWindow.installing_extensions_labels"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                installPluginsLabels();
            }
        });

        // Se muestra el frame principal
        message(translate("creating_main_window"));
        frame.setVisible(true);
        frame.setCursor(Cursor.WAIT_CURSOR);

		// Definimos un KeyEventDispatcher global para que las extensiones
        // puedan registrar sus "teclas rapidas".
        message(translate("SplashWindow.initializing_accelerator_keys"));
        GlobalKeyEventDispatcher keyDispatcher = GlobalKeyEventDispatcher.getInstance();
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyDispatcher);

        message(translate("SplashWindow.enable_controls"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                try {
                    frame.enableControls();
                } catch (Throwable th) {
                    logger.warn("Problems enabling controls", th);
                }
            }
        });

        // Se ejecuta el postInitialize
        message(translate("SplashWindow.post_initializing_extensions"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                postInitializeExtensions();
            }
        });

        message(translate("SplashWindow.enable_controls"));
        SwingUtilities.invokeAndWait(new Runnable() {
            public void run() {
                try {
                    frame.enableControls();
                    message(translate("StatusBar.Aplicacion_iniciada"));
                } catch (Throwable th) {
                    logger.warn("Problems enabling controls", th);
                }
            }
        });

        splashWindow.close();

        frame.setCursor(Cursor.DEFAULT_CURSOR);

        if (launcherrors != null) {
            NotificationManager.addError(launcherrors);
        }
        org.apache.log4j.Logger.getRootLogger().addAppender(
                new NotificationAppender());

        /*
         * Executes additional tasks required by plugins
         */
        PluginsLocator.getManager().executeStartupTasks();

    }

    private void initializeInstallerManager() {
        PluginsManager pluginmgr = PluginsLocator.getManager();
        InstallerManager installerManager = InstallerLocator.getInstallerManager();

            //
        // Configure repository of plugins
        //
        List<File> folders = pluginmgr.getPluginsFolders();
        for (File folder : folders) {
            installerManager.addLocalAddonRepository(folder, "plugin");
        }
        installerManager.setDefaultLocalAddonRepository(folders.get(0), "plugin");

            //
        // Configure repository of iconsets
        //
        IconThemeManager iconManager = ToolsSwingLocator.getIconThemeManager();
        FolderSet fset = iconManager.getRepository();
        Iterator<FolderSet.FolderEntry> it = fset.iterator();
        boolean first = true;
        while (it.hasNext()) {
            FolderEntry entry = it.next();
            installerManager.addLocalAddonRepository(entry.getFolder(), "iconset");
            if (first) {
                first = false;
                installerManager.setDefaultLocalAddonRepository(entry.getFolder(), "iconset");
            }
        }

    }

    private void message(final String msg) {
        if (!SwingUtilities.isEventDispatchThread()) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        message(msg);
                    }
                });
            } catch (Exception e) {
                logger.info(msg);
                logger.warn("Error showing message.", e);
            }
            return;
        }
        if (splashWindow.isVisible()) {
            splashWindow.process(msg);
        }
        if (frame.isVisible()) {
            frame.message(msg, JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void initializeLibraries() {
        List<ClassLoader> classLoaders = new ArrayList<ClassLoader>(
                pluginsOrdered.size() + 1);
        classLoaders.add(getClass().getClassLoader());
        Iterator<String> iter = pluginsOrdered.iterator();

        logger.info("Initializing plugins libraries: ");
        while (iter.hasNext()) {
            String pName = (String) iter.next();
            PluginServices ps = (PluginServices) pluginsServices.get(pName);
            logger.info("Initializing plugin libraries (" + pName + ")");
            classLoaders.add(ps.getClassLoader());
        }

		// Create the libraries initializer and
        // initialize the plugin libraries
        new DefaultLibrariesInitializer(classLoaders
                .toArray(new ClassLoader[classLoaders.size()]))
                .fullInitialize(true);

        // Remove them all, we don't need them anymore
        classLoaders.clear();
        classLoaders = null;
    }

    /**
     * @param args
     * @throws IOException
     * @throws ConfigurationException
     */
    private void initializeApp(String[] args, String applicationClasifier) throws IOException, ConfigurationException {
        if (args.length < 1) {
            appName = "gvSIG"; // Nombre de aplicacion por defecto es "gvSIG"
        } else {
            appName = args[0];
        }
        getOrCreateConfigFolder();
        configureLogging(appName, applicationClasifier);
        if (!validJVM()) {
            logger.error("Not a valid JRE. Exit application.");
            System.exit(-1);
        }
        // Clean temporal files
        Utilities.cleanUpTempFiles();

        if (args.length < 2) {
            loadAndamiConfig("gvSIG/extensiones"); // Valor por defecto
        } else {
            loadAndamiConfig(args[1]);
        }

		// Hacemos visibles los argumentos como una propiedad est�tica
        // de plugin services para quien lo quiera usar (por ejemplo, para
        // cargar un proyecto por l�nea de comandos)
        PluginServices.setArguments(args);

        configureLocales(args);

        logger.info("Configure LookAndFeel");
        configureLookAndFeel();
    }

    /**
     *
     */
    private void configureLookAndFeel() {
        // Se pone el lookAndFeel
        try {
            String lookAndFeel = getAndamiConfig().getLookAndFeel();
            if (lookAndFeel == null) {
                lookAndFeel = getDefaultLookAndFeel();
            }
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {
            logger.warn(Messages.getString("Launcher.look_and_feel"), e);
        }
        FontUtils.initFonts();
    }

    /**
     * @param args
     * @throws ConfigurationException
     */
    private void loadAndamiConfig(String pluginFolder)
            throws ConfigurationException {
        andamiConfigPath = appHomeDir + File.separator + "andami-config.xml";
        andamiConfigFromXML(andamiConfigPath);
        andamiConfig.setPluginsDirectory(pluginFolder);
    }

    /**
     *
     */
    private void getOrCreateConfigFolder() {
        // Create application configuration folder
        appHomeDir = System.getProperty(appName + ".home");
        if (appHomeDir == null) {
            appHomeDir = System.getProperty("user.home");
        }

        appHomeDir += File.separator + appName;
        File parent = new File(appHomeDir);
        parent.mkdirs();
    }

    /**
     * @param args
     * @throws IOException
     */
    private void configureLogging(String appName, String applicationClasifier) throws IOException {
        // Configurar el log4j

        String pathname;
        if (StringUtils.isBlank(applicationClasifier)) {
            pathname = appHomeDir + File.separator + appName + ".log";
        } else {
            pathname = appHomeDir + File.separator + appName + "-" + applicationClasifier + ".log";
        }
        URL config = Launcher.class.getClassLoader().getResource("log4j.properties");
        if (config == null) {
            config = Launcher.class.getClassLoader().getResource("default-log4j/log4j.properties");
        }
        PropertyConfigurator.configure(config);
        PatternLayout l = new PatternLayout("%p %t %C - %m%n");
        RollingFileAppender fa = new RollingFileAppender(l, pathname, false);
        fa.setMaxFileSize("512KB");
        fa.setMaxBackupIndex(3);
        org.apache.log4j.Logger.getRootLogger().addAppender(fa);
        logger.info("Loadded log4j.properties from " + config.toString());
        if (StringUtils.isBlank(applicationClasifier)) {
            logger.info("Application " + appName);
        } else {
            logger.info("Application " + appName + "-" + applicationClasifier);
        }
    }
    
    public static String getApplicationName() {
        return appName;
    }

    private class NotificationAppender extends AppenderSkeleton {

        @Override
        protected void append(LoggingEvent event) {
            if (event.getLevel() == org.apache.log4j.Level.ERROR
                    || event.getLevel() == org.apache.log4j.Level.FATAL) {

                Throwable th = null;
                ThrowableInformation thi = event.getThrowableInformation();
                if (thi != null) {
                    th = thi.getThrowable();
                }
                NotificationManager.dispatchError(event.getRenderedMessage(), th);
                return;
            }
			// if (event.getLevel() == org.apache.log4j.Level.WARN) {
            // NotificationManager.dispatchWarning(event.getRenderedMessage(),
            // null);
            // return;
            // }
        }

        @Override
        public void close() {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean requiresLayout() {
            // TODO Auto-generated method stub
            return false;
        }

    }

    /**
     * Return the directory applicaction is installed.
     */
    public static String getApplicationDirectory() {
        return getApplicationFolder().getAbsolutePath();
    }

    public static File getApplicationFolder() {
        // TODO: check if there is a better way to handle this
        return new File(System.getProperty("user.dir"));
    }

    private void registerIcons() {
        IconTheme theme = PluginServices.getIconTheme();
        ClassLoader loader = Launcher.class.getClassLoader();

        String[][] icons = {
            // MultiSplashWindow
            {"main", "splash-default"},
            // NewStatusBar
            {"main", "statusbar-info"},
            {"main", "statusbar-warning"},
            {"main", "statusbar-error"}
        };
        for (int i = 0; i < icons.length; i++) {
            try {
                IconThemeHelper.registerIcon(icons[i][0], icons[i][1], Launcher.class);
            } catch (Exception e) {
                logger.info("Can't register icon '" + icons[i][0] + "' (" + icons[i][1] + ").");
            }
        }
        theme.setDefaultIcon(loader.getResource("images/main/default-icon.png"));
    }

    private Properties loadProperties(File f) {
        FileInputStream fin = null;
        Properties p = null;
        try {
            fin = new FileInputStream(f);
            p = new Properties();
            p.load(fin);
        } catch (IOException ex) {
            // Do nothing
        }
        return p;
    }

    /**
     * Obtiene la personalizaci�n de los iconos, splash, fondo y el nombre de
     * la aplicaci�n.
     *
     * @return Theme
     */
    private Theme getTheme(String pluginsDirectory) {
        File infoFile = new File(Launcher.getApplicationFolder(), "package.info");
        File themeFile = null;
        List<Theme> themes = new ArrayList<Theme>();

        // Try to get theme from args
        String name = PluginServices.getArgumentByName("andamiTheme");
        if (name != null) {
            themeFile = new File(name);
            logger.info("search andami-theme in {}", themeFile.getAbsolutePath());
            if (themeFile.exists()) {
                Theme theme = new Theme(loadProperties(infoFile));
                theme.readTheme(themeFile);
                logger.info("andami-theme found in {}", themeFile.getAbsolutePath());
                return theme;
            }
        }

        // Try to get theme from a plugin
        File pluginsDir = new File(pluginsDirectory);
        if (!pluginsDir.isAbsolute()) {
            pluginsDir = new File(getApplicationFolder(), pluginsDirectory);
        }
        if (pluginsDir.exists()) {
            logger.info("search andami-theme in plugins folder '" + pluginsDir.getAbsolutePath() + "'.");
            File[] pluginDirs = pluginsDir.listFiles();
            if (pluginDirs.length > 0) {
                for (int i = 0; i < pluginDirs.length; i++) {
                    File pluginThemeFile = new File(pluginDirs[i],
                            "theme" + File.separator + "andami-theme.xml");
                    if (pluginThemeFile.exists()) {
                        Theme theme = new Theme(loadProperties(infoFile));
                        theme.readTheme(pluginThemeFile);
                        themes.add(theme);
                    }
                }
            }
        }

        // Try to get theme from dir gvSIG in user home
        themeFile = new File(getAppHomeDir(), "theme" + File.separator
                + "andami-theme.xml");
        logger.info("search andami-theme in user's home {}", themeFile
                .getAbsolutePath());
        if (themeFile.exists()) {
            Theme theme = new Theme(loadProperties(infoFile));
            theme.readTheme(themeFile);
            themes.add(theme);
        }

        // Try to get theme from the instalation dir of gvSIG.
        themeFile = new File(getApplicationDirectory(), "theme"
                + File.separator + "andami-theme.xml");
        logger.info("search andami-theme in installation folder {}", themeFile
                .getAbsolutePath());
        if (themeFile.exists()) {
            Theme theme = new Theme(loadProperties(infoFile));
            theme.readTheme(themeFile);
            themes.add(theme);
        }

        Collections.sort(themes, new Comparator<Theme>() {
            public int compare(Theme t1, Theme t2) {
                return t2.getPriority() - t1.getPriority();
            }
        });
        if (logger.isInfoEnabled()) {
            logger.info("Found andami-themes in:");
            for (Theme theme : themes) {
                logger.info(" - " + theme.getPriority() + ", " + theme.getSource().getAbsolutePath());
            }
        }
        Theme theme = themes.get(0);
        logger.info("Using theme '" + theme.getSource() + "'.");
        return theme;
    }

    /**
     * Establece los datos que tengamos guardados respecto de la configuracion
     * del proxy.
     */
    private void configureProxy() {
        String host = prefs.get("firewall.http.host", "");
        String port = prefs.get("firewall.http.port", "");

        System.getProperties().put("http.proxyHost", host);
        System.getProperties().put("http.proxyPort", port);

        // Ponemos el usuario y clave del proxy, si existe
        String proxyUser = prefs.get("firewall.http.user", null);
        String proxyPassword = prefs.get("firewall.http.password", null);
        if (proxyUser != null) {
            System.getProperties().put("http.proxyUserName", proxyUser);
            System.getProperties().put("http.proxyPassword", proxyPassword);

            Authenticator.setDefault(new ProxyAuth(proxyUser, proxyPassword));
        } else {
            Authenticator.setDefault(new ProxyAuth("", ""));
        }
    }

    /**
     * Recupera la geometr�a (tama�o, posic�n y estado) de la ventana
     * principal de Andami. TODO Pendiente de ver como se asigna un
     * pluginServices para el launcher.
     *
     * @author LWS
     */
    private void restoreMDIStatus(XMLEntity xml) {
        if (xml == null) {
            xml = new XMLEntity();
        }
        // ====================================
        // restore frame size
        Dimension sz = new Dimension(
                MainFrame.MAIN_FRAME_SIZE_DEFAULT[0],
                MainFrame.MAIN_FRAME_SIZE_DEFAULT[1]);
        if (xml.contains(MainFrame.MAIN_FRAME_SIZE)) {
            int[] wh = xml.getIntArrayProperty(MainFrame.MAIN_FRAME_SIZE);
            sz = new Dimension(wh[0], wh[1]);
        }
        frame.setSize(sz);
        // ==========================================
        // restore frame location
        Point pos = new Point(
                MainFrame.MAIN_FRAME_POS_DEFAULT[0],
                MainFrame.MAIN_FRAME_POS_DEFAULT[1]);
        if (xml.contains(MainFrame.MAIN_FRAME_POS)) {
            int[] xy = xml.getIntArrayProperty(MainFrame.MAIN_FRAME_POS);
            pos = new Point(xy[0], xy[1]);
        }
        frame.setLocation(pos);
        // =============================================
        // restore frame state (Maximized, minimized, etc);
        int state = MainFrame.MAIN_FRAME_EXT_STATE_DEFAULT;
        if (xml.contains(MainFrame.MAIN_FRAME_EXT_STATE)) {
            state = xml.getIntProperty(MainFrame.MAIN_FRAME_EXT_STATE);
        }
        frame.setExtendedState(state);
    }

    private XMLEntity saveMDIStatus() {
        XMLEntity xml = new XMLEntity();
        // save frame size
        int[] wh = new int[2];
        wh[0] = frame.getWidth();
        wh[1] = frame.getHeight();
        xml.putProperty(MainFrame.MAIN_FRAME_SIZE, wh);
        // save frame location
        int[] xy = new int[2];
        xy[0] = frame.getX();
        xy[1] = frame.getY();
        xml.putProperty(MainFrame.MAIN_FRAME_POS, xy);
        // save frame status
        xml.putProperty(MainFrame.MAIN_FRAME_EXT_STATE,
                frame.getExtendedState());
        return xml;
    }

    private boolean validJVM() {
        if (!SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_1_4)) {
            logger.warn("gvSIG requires Java version 1.4 or higher.");
            logger.warn("The Java HOME is '" + SystemUtils.getJavaHome().getAbsolutePath() + "'.");
            return false;
        }
        if (SystemUtils.isJavaAwtHeadless()) {
            logger.warn("The java used by gvSIG does not contain the libraries for access to the graphical interface (AWT-headless)");
            logger.warn("The Java HOME is '" + SystemUtils.getJavaHome().getAbsolutePath() + "'.");
            return false;
        }
        return true;
    }

    private void loadPluginsPersistence() throws ConfigurationException {
        XMLEntity entity = persistenceFromXML();

        for (int i = 0; i < entity.getChildrenCount(); i++) {
            XMLEntity plugin = entity.getChild(i);
            String pName = plugin
                    .getStringProperty("com.iver.andami.pluginName");

            if (pName.compareToIgnoreCase("com.iver.cit.gvsig") == 0) {
                pName = "org.gvsig.app";
            }
            if (pluginsServices.get(pName) != null) {
                ((PluginServices) pluginsServices.get(pName))
                        .setPersistentXML(plugin);
            } else {
                if (pName.startsWith("Andami.Launcher")) {
                    restoreMDIStatus(plugin);
                }
            }
        }
    }

    /**
     * Salva la persistencia de los plugins.
     *
     * @author LWS
     */
    private void savePluginPersistence() {
        Iterator<String> i = pluginsConfig.keySet().iterator();

        XMLEntity entity = new XMLEntity();

        while (i.hasNext()) {
            String pName = i.next();
            PluginServices ps = (PluginServices) pluginsServices.get(pName);
            XMLEntity ent = ps.getPersistentXML();

            if (ent != null) {
                ent.putProperty("com.iver.andami.pluginName", pName);
                entity.addChild(ent);
            }
        }
        XMLEntity ent = saveMDIStatus();
        if (ent != null) {
            ent.putProperty("com.iver.andami.pluginName", "Andami.Launcher");
            entity.addChild(ent);
        }
        try {
            persistenceToXML(entity);
        } catch (ConfigurationException e1) {
            this
                    .addError(
                            Messages
                            .getString("Launcher.Se_produjo_un_error_guardando_la_configuracion_de_los_plugins"),
                            e1);
        }
    }

    private void installPluginsLabels() {
        Iterator<String> i = pluginsConfig.keySet().iterator();

        while (i.hasNext()) {
            String name = i.next();
            PluginConfig pc = pluginsConfig.get(name);
            PluginServices ps = (PluginServices) pluginsServices.get(name);

            LabelSet[] ls = pc.getLabelSet();

            for (int j = 0; j < ls.length; j++) {
                PluginClassLoader loader = ps.getClassLoader();

                try {
                    Class clase = loader.loadClass(ls[j].getClassName());
                    frame.setStatusBarLabels(clase, ls[j].getLabel());
                } catch (Throwable e) {
                    this.addError(
                            Messages.getString("Launcher.labelset_class"), e);
                }
            }
        }
    }

    private String configureSkin(XMLEntity xml, String defaultSkin) {
        if (defaultSkin == null) {
            for (int i = 0; i < xml.getChildrenCount(); i++) {
                if (xml.getChild(i).contains("Skin-Selected")) {
                    String className = xml.getChild(i).getStringProperty(
                            "Skin-Selected");
                    return className;
                }
            }
        }
        // return "com.iver.core.mdiManager.NewSkin";
        return defaultSkin;
    }

    private void fixSkin(SkinExtension skinExtension,
            PluginClassLoader pluginClassLoader) throws MDIManagerLoadException {
        // now insert the skin selected.
        MDIManagerFactory.setSkinExtension(skinExtension, pluginClassLoader);
		// MDIManagerFactory.setSkinExtension(se,
        // ps.getClassLoader());

        Class<? extends IExtension> skinClass;

        try {
            skinClass = (Class<? extends IExtension>) pluginClassLoader
                    .loadClass(skinExtension.getClassName());

            IExtension skinInstance = skinClass.newInstance();
            ExtensionDecorator newExtensionDecorator = new ExtensionDecorator(
                    skinInstance, ExtensionDecorator.INACTIVE);
            classesExtensions.put(skinClass, newExtensionDecorator);
        } catch (ClassNotFoundException e) {
            logger.error(Messages
                    .getString("Launcher.No_se_encontro_la_clase_mdi_manager"),
                    e);
            throw new MDIManagerLoadException(e);
        } catch (InstantiationException e) {
            logger
                    .error(
                            Messages
                            .getString("Launcher.No_se_pudo_instanciar_la_clase_mdi_manager"),
                            e);
            throw new MDIManagerLoadException(e);
        } catch (IllegalAccessException e) {
            logger
                    .error(
                            Messages
                            .getString("Launcher.No_se_pudo_acceder_a_la_clase_mdi_manager"),
                            e);
            throw new MDIManagerLoadException(e);
        }

    }

    /**
     * DOCUMENT ME!
     *
     * @throws MDIManagerLoadException
     */
    private void skinPlugin(String defaultSkin) throws MDIManagerLoadException {
        XMLEntity entity = null;
        try {
            entity = persistenceFromXML();
        } catch (ConfigurationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Iterator<String> i = pluginsConfig.keySet().iterator();

        SkinExtension skinExtension = null;
        PluginClassLoader pluginClassLoader = null;
        List<SkinExtension> skinExtensions = new ArrayList<SkinExtension>();
        while (i.hasNext()) {
            String name = i.next();
            PluginConfig pc = pluginsConfig.get(name);
            PluginServices ps = pluginsServices.get(name);

            if (pc.getExtensions().getSkinExtension() != null) {
				// if (MDIManagerFactory.getSkinExtension() != null) {
                // logger.warn(Messages.getString(
                // "Launcher.Dos_skin_extension"));
                // }

                SkinExtension[] se = pc.getExtensions().getSkinExtension();
                for (int numExten = 0; numExten < se.length; numExten++) {
                    skinExtensions.add(se[numExten]);
                }
                for (int j = 0; j < se.length; j++) {
                    String configuredSkin = this.configureSkin(entity,
                            defaultSkin);
                    if ((configuredSkin != null)
                            && configuredSkin.equals(se[j].getClassName())) {
                        skinExtension = se[j];
                        pluginClassLoader = ps.getClassLoader();
                    }
                }
            }
        }

        if ((skinExtension != null) && (pluginClassLoader != null)) {
            // configured skin was found
            fixSkin(skinExtension, pluginClassLoader);
        } else {
            if (skinExtensions.contains("com.iver.core.mdiManager.NewSkin")) {
                // try first NewSkin (from CorePlugin)
                skinPlugin("com.iver.core.mdiManager.NewSkin");
            } else if (skinExtensions.size() > 0) {
                // try to load the first skin found
                SkinExtension se = (SkinExtension) skinExtensions.get(0);
                skinPlugin((String) se.getClassName());
            } else {
                throw new MDIManagerLoadException("No Skin-Extension installed");
            }
        }

    }

    private static void frameIcon(Theme theme) {
        Iterator<String> i = pluginsConfig.keySet().iterator();

        while (i.hasNext()) {
            String pName = i.next();
            PluginConfig pc = pluginsConfig.get(pName);
            if (pc.getIcon() != null) {
                if (theme.getIcon() != null) {
                    frame.setIconImage(theme.getIcon().getImage());
                } else {

                    ImageIcon icon = PluginServices.getIconTheme().get(
                            pc.getIcon().getSrc());
                    frame.setIconImage(icon.getImage());

                }
                if (theme.getName() != null) {
                    frame.setTitlePrefix(theme.getName());
                } else {
                    frame.setTitlePrefix(pc.getIcon().getText());
                }
                if (theme.getBackgroundImage() != null) {

                    PluginServices.getMDIManager().setBackgroundImage(
                            theme.getBackgroundImage(), theme.getTypeDesktop());
                }
            }
        }
    }

    private void initializeExtensions() {

        List<ClassLoader> classLoaders = new ArrayList<ClassLoader>(
                pluginsOrdered.size());
        classLoaders.add(getClass().getClassLoader());
        Iterator<String> iter = pluginsOrdered.iterator();

		// logger.debug("Initializing plugins libraries: ");
        // while (iter.hasNext()) {
        // String pName = (String) iter.next();
        // PluginServices ps = (PluginServices) pluginsServices.get(pName);
        // classLoaders.add(ps.getClassLoader());
        // }
        //
        // // Create the libraries initializer and
        // // initialize the plugin libraries
        // new DefaultLibrariesInitializer(
        // classLoaders.toArray(new ClassLoader[classLoaders.size()]))
        // .fullInitialize();
        //
        // // Remove them all, we don't need them anymore
        // classLoaders.clear();
        // classLoaders = null;
        logger.info("Initializing plugins: ");
        // iter = pluginsOrdered.iterator();
        while (iter.hasNext()) {
            String pName = (String) iter.next();
            logger.info("Initializing plugin " + pName);
            PluginConfig pc = (PluginConfig) pluginsConfig.get(pName);
            PluginServices ps = (PluginServices) pluginsServices.get(pName);

            Extension[] exts = pc.getExtensions().getExtension();

            TreeSet<Extension> orderedExtensions = new TreeSet<Extension>(
                    new ExtensionComparator());

            for (int j = 0; j < exts.length; j++) {
                if (!exts[j].getActive()) {
                    continue;
                }

                if (orderedExtensions.contains(exts[j])) {
                    logger.warn("Two extensions with the same priority ("
                            + exts[j].getClassName() + ")");
                }

                orderedExtensions.add(exts[j]);
            }

            Iterator<Extension> e = orderedExtensions.iterator();

            logger.info("Initializing extensions of plugin " + pName + ": ");
            while (e.hasNext()) {
                Extension extension = e.next();
                org.gvsig.andami.plugins.IExtension extensionInstance;

                try {
                    logger.info("Initializing " + extension.getClassName()
                            + "...");
                    message(extension.getClassName() + "...");
                    Class<? extends IExtension> extensionClass = (Class<? extends IExtension>) ps
                            .getClassLoader().loadClass(
                                    extension.getClassName());
                    extensionInstance = extensionClass.newInstance();
                    if (extensionInstance instanceof org.gvsig.andami.plugins.Extension) {
                        ((org.gvsig.andami.plugins.Extension) extensionInstance).setPlugin(ps);
                    } else {
                        logger.warn("The extension " + extensionClass.getName() + " don't extends of Extension.");
                    }

					// CON DECORATOR
                    // ANTES: classesExtensions.put(extensionClass,
                    // extensionInstance);
                    // AHORA: CREAMOS UNA ExtensionDecorator y asignamos esta
                    // instancia para
                    // poder ampliar con nuevas propiedades (AlwaysVisible, por
                    // ejemplo)
                    // Para crear la nueva clase ExtensionDecorator, le pasamos
                    // como par�metro
                    // la extensi�n original que acabamos de crear
                    // 0-> Inactivo, controla la extension
                    // 1-> Siempre visible
                    // 2-> Invisible
                    ExtensionDecorator newExtensionDecorator = new ExtensionDecorator(
                            extensionInstance, ExtensionDecorator.INACTIVE);
                    classesExtensions
                            .put(extensionClass, newExtensionDecorator);

                    extensionInstance.initialize();
                    extensions.add(extensionInstance);

                } catch (NoClassDefFoundError e1) {
                    this.addError("Can't find class extension ("
                            + extension.getClassName() + ")", e1);
                } catch (Throwable e1) {
                    this.addError("Can't initialize extension '"
                            + extension.getClassName() + "'.", e1);
                }
            }
        }
    }

    private void postInitializeExtensions() {
        logger.info("PostInitializing extensions: ");

        for (int i = 0; i < extensions.size(); i++) {
            org.gvsig.andami.plugins.IExtension extensionInstance = (org.gvsig.andami.plugins.IExtension) extensions
                    .get(i);
            String name = extensionInstance.getClass().getName();
            logger.info("PostInitializing " + name + "...");
            message(name + "...");
            try {
                extensionInstance.postInitialize();
            } catch (Throwable ex) {
                this.addError("postInitialize of extension '"
                        + extensionInstance.getClass().getName() + "' failed",
                        ex);
            }
        }
    }

    private void registerActionOfExtensions(ActionInfoManager actionManager,
            Enumeration<SkinExtensionType> extensiones, ClassLoader loader) {
        ActionInfo actionInfo;
        while (extensiones.hasMoreElements()) {
            SkinExtensionType extension = extensiones.nextElement();
            Class<? extends IExtension> classExtension;
            try {
                classExtension = (Class<? extends IExtension>) loader
                        .loadClass(extension.getClassName());

                Enumeration<Action> actions = extension.enumerateAction();
                while (actions.hasMoreElements()) {
                    Action action = actions.nextElement();
                    if (action.getName() == null) {
                        logger.info("invalid action name (null) in " + extension.getClassName() + " of " + loader.toString());
                    } else {
                        actionInfo = actionManager.createAction(
                                classExtension, action.getName(),
                                action.getLabel(), action.getActionCommand(),
                                action.getIcon(), action.getAccelerator(), action.getPosition(),
                                action.getTooltip());
                        actionManager.registerAction(actionInfo);
                        if (action.getPosition() < 100000000) {
                            logger.info("Invalid position in action (" + actionInfo.toString() + ").");
                            action.setPosition(action.getPosition() + 1000000000);
                        }
                    }
                }

                Enumeration<Menu> menus = extension.enumerateMenu();
                while (menus.hasMoreElements()) {
                    Menu menu = menus.nextElement();
                    if (!menu.getIs_separator()) {
                        actionInfo = actionManager.createAction(
                                classExtension, menu.getName(), menu.getText(),
                                menu.getActionCommand(), menu.getIcon(),
                                menu.getKey(), menu.getPosition(),
                                menu.getTooltip());
                        actionInfo = actionManager.registerAction(actionInfo);
                        if (actionInfo != null) {
                            menu.setActionCommand(actionInfo.getCommand());
                            menu.setTooltip(actionInfo.getTooltip());
                            menu.setIcon(actionInfo.getIconName());
                            menu.setPosition(actionInfo.getPosition());
                            menu.setKey(actionInfo.getAccelerator());
                            menu.setName(actionInfo.getName());
                        }
                    }
                    if (menu.getPosition() < 100000000) {
                        logger.info("Invalid position in menu (" + menu.getText() + ").");
                        menu.setPosition(menu.getPosition() + 1000000000);
                    }

                }
                Enumeration<ToolBar> toolBars = extension.enumerateToolBar();
                while (toolBars.hasMoreElements()) {
                    ToolBar toolBar = toolBars.nextElement();

                    Enumeration<ActionTool> actionTools = toolBar
                            .enumerateActionTool();
                    while (actionTools.hasMoreElements()) {
                        ActionTool actionTool = actionTools.nextElement();
                        actionInfo = actionManager.createAction(
                                classExtension, actionTool.getName(),
                                actionTool.getText(),
                                actionTool.getActionCommand(),
                                actionTool.getIcon(),
                                null,
                                actionTool.getPosition(),
                                actionTool.getTooltip());
                        actionInfo = actionManager.registerAction(actionInfo);
                        if (actionInfo != null) {
                            actionTool.setActionCommand(actionInfo.getCommand());
                            actionTool.setTooltip(actionInfo.getTooltip());
                            actionTool.setIcon(actionInfo.getIconName());
                            actionTool.setPosition(actionInfo.getPosition());
                            actionTool.setName(actionInfo.getName());
                        }
                    }

                    Enumeration<SelectableTool> selectableTool = toolBar
                            .enumerateSelectableTool();
                    while (selectableTool.hasMoreElements()) {
                        SelectableTool actionTool = selectableTool
                                .nextElement();
                        actionInfo = actionManager.createAction(
                                classExtension, actionTool.getName(),
                                actionTool.getText(),
                                actionTool.getActionCommand(),
                                actionTool.getIcon(),
                                null,
                                actionTool.getPosition(),
                                actionTool.getTooltip());
                        actionInfo = actionManager.registerAction(actionInfo);
                        if (actionInfo != null) {
                            actionTool.setActionCommand(actionInfo.getCommand());
                            actionTool.setTooltip(actionInfo.getTooltip());
                            actionTool.setIcon(actionInfo.getIconName());
                            actionTool.setPosition(actionInfo.getPosition());
                            actionTool.setName(actionInfo.getName());
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                logger.warn(
                        "Can't register actions of extension '"
                        + extension.getClassName() + "'", e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void registerActions() {
        logger.info("registerActions");

        ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
        Iterator<String> it = pluginsConfig.keySet().iterator();

        while (it.hasNext()) {
            String pluginName = it.next();
            PluginConfig pluginConfig = pluginsConfig.get(pluginName);
            PluginServices pluginService = pluginsServices.get(pluginName);
            PluginClassLoader loader = pluginService.getClassLoader();

            logger.info("registerActions of plugin '" + pluginName + "'.");

            Extensions extensionConfig = pluginConfig.getExtensions();

            Enumeration<SkinExtensionType> extensiones = extensionConfig.enumerateExtension();
            registerActionOfExtensions(actionManager, extensiones, loader);

            Enumeration<SkinExtensionType> skinSxtensiones = extensionConfig.enumerateSkinExtension();
            registerActionOfExtensions(actionManager, skinSxtensiones, loader);

            PopupMenus pluginPopupMenus = pluginConfig.getPopupMenus();
            if (pluginPopupMenus != null) {
                PopupMenu[] menus1 = pluginPopupMenus.getPopupMenu();
                for (int j = 0; j < menus1.length; j++) {
                    PopupMenu popupMenu = menus1[j];
                    Enumeration<Menu> menus2 = popupMenu.enumerateMenu();
                    while (menus2.hasMoreElements()) {
                        Menu menu = menus2.nextElement();
                        if (!menu.getIs_separator()) {
                            if (menu.getName() == null) {
                                logger.info("Null name for popmenu '" + menu.getText() + "' in plugin " + pluginService.getPluginName());
                            } else {
                                ActionInfo actionInfo = actionManager.getAction(menu.getName());
                                if (actionInfo != null) {
                                    menu.setActionCommand(actionInfo.getCommand());
                                    menu.setTooltip(actionInfo.getTooltip());
                                    menu.setIcon(actionInfo.getIconName());
                                    menu.setPosition(actionInfo.getPosition());
                                    menu.setText(actionInfo.getLabel());
                                    menu.setKey(actionInfo.getAccelerator());
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    private TreeSet<SortableMenu> getOrderedMenus() {

        TreeSet<SortableMenu> orderedMenus = new TreeSet<SortableMenu>(
                new MenuComparator());

        Iterator<String> i = pluginsConfig.keySet().iterator();

        while (i.hasNext()) {
            String pName = i.next();
            try {
                PluginServices ps = pluginsServices.get(pName);
                PluginConfig pc = pluginsConfig.get(pName);

                Extension[] exts = pc.getExtensions().getExtension();

                for (int j = 0; j < exts.length; j++) {
                    if (!exts[j].getActive()) {
                        continue;
                    }

                    Menu[] menus = exts[j].getMenu();

                    for (int k = 0; k < menus.length; k++) {
                        SortableMenu sm = new SortableMenu(ps.getClassLoader(),
                                exts[j], menus[k]);

                        if (orderedMenus.contains(sm)) {
                            this
                                    .addError(Messages
                                            .getString("Launcher.Two_menus_with_the_same_position")
                                            + " - "
                                            + menus[k].getText()
                                            + " - " + exts[j].getClassName());
                        }

                        orderedMenus.add(sm);
                    }
                }

                // Se instalan las extensiones de MDI
                SkinExtension[] skinExts = pc.getExtensions()
                        .getSkinExtension();
                for (int j = 0; j < skinExts.length; j++) {

                    if (skinExts[j] != null) {
                        Menu[] menu = skinExts[j].getMenu();

                        for (int k = 0; k < menu.length; k++) {
                            SortableMenu sm = new SortableMenu(ps
                                    .getClassLoader(), skinExts[j], menu[k]);

                            if (orderedMenus.contains(sm)) {
                                this
                                        .addError(Messages
                                                .getString("Launcher.Two_menus_with_the_same_position")
                                                + skinExts[j].getClassName());
                            }

                            orderedMenus.add(sm);
                        }
                    }
                }

            } catch (Throwable e) {
                addError("Error initializing menus of plugin '" + pName + "'",
                        e);
            }

        }

        return orderedMenus;
    }

    private void installPluginsMenus() {
        logger.info("installPluginsMenus");

        TreeSet<SortableMenu> orderedMenus = getOrderedMenus();

        // Se itera por los menus ordenados
        Iterator<SortableMenu> e = orderedMenus.iterator();

        // Se ordenan los menues
        while (e.hasNext()) {
            try {
                SortableMenu sm = e.next();
                logger.debug(sm.menu.getPosition() + ":" + sm.menu.getText() + ":" + sm.loader.getPluginName() + ":" + sm.extension.getClassName());
                frame.addMenu(sm.loader, sm.extension, sm.menu);
            } catch (Throwable ex) {
                this.addError(
                        Messages.getString("Launcher.No_se_encontro_la_clase_de_la_extension"),
                        ex
                );
            }
        }
    }

    public class PluginMenuItem {

        private Menu menu;
        private PluginClassLoader loader;
        private SkinExtensionType extension;

        PluginMenuItem(PluginClassLoader loader,
                SkinExtensionType extension, Menu menu) {
            this.menu = menu;
            this.loader = loader;
            this.extension = extension;
        }

        public PluginServices getPlugin() {
            String pluginName = loader.getPluginName();
            return PluginServices.getPluginServices(pluginName);
        }

        public String getExtensionName() {
            return this.extension.getClassName();
        }

        public IExtension getExtension() {
            Class<?> extensionClass;
            try {
                extensionClass = loader.loadClass(this.extension.getClassName());
            } catch (ClassNotFoundException e) {
                return null;
            }
            return PluginServices.getExtension(extensionClass);
        }

        public String getText() {
            return this.menu.getText();
        }

        public long getPosition() {
            return this.menu.getPosition();
        }

        public String getName() {
            return this.menu.getName();
        }

        public boolean isParent() {
            return menu.getIs_separator();
        }

        public String getPluginName() {
            return this.loader.getPluginName();
        }

        public ActionInfo getAction() {
            ActionInfoManager manager = PluginsLocator.getActionInfoManager();
            return manager.getAction(this.menu.getName());
        }
    }

    public List<PluginMenuItem> getPluginMenuItems() {
        List<PluginMenuItem> menuItems = new ArrayList<Launcher.PluginMenuItem>();

        TreeSet<SortableMenu> orderedMenus = getOrderedMenus();
        Iterator<SortableMenu> e = orderedMenus.iterator();
        while (e.hasNext()) {
            SortableMenu sm = e.next();
            PluginMenuItem item = new PluginMenuItem(sm.loader, sm.extension, sm.menu);
            menuItems.add(item);
        }
        return menuItems;
    }

    /**
     * Installs the menus, toolbars, actiontools, selectable toolbars and
     * combos. The order in which they are shown is determined here.
     */
    private void installPluginsControls() {
        logger.info("installPluginsControls (toolbars)");

        Iterator<String> i = pluginsConfig.keySet().iterator();

        Map<Extension, PluginServices> extensionPluginServices = new HashMap<Extension, PluginServices>();
        Map<Extension, PluginConfig> extensionPluginConfig = new HashMap<Extension, PluginConfig>();
        Set<Extension> orderedExtensions = new TreeSet<Extension>(
                new ExtensionComparator());

		// First of all, sort the extensions.
        // We need to iterate on the plugins, and iterate on each plugin's
        // extensions
        // (each plugin may contain one or more extensions)
        while (i.hasNext()) { // iterate on the plugins
            String pName = i.next();
            try {
                PluginConfig pc = pluginsConfig.get(pName);
                PluginServices ps = pluginsServices.get(pName);

                Extension[] exts = pc.getExtensions().getExtension();

                for (int j = 0; j < exts.length; j++) { // iterate on the
                    // extensions
                    String cname = "unknow";
                    try {
                        cname = exts[j].getClassName();
                        if (exts[j].getActive()
                                && !cname.equals(LibraryExtension.class
                                        .getName())) {
                            if (orderedExtensions.contains(exts[j])) {
                                this
                                        .addError(Messages
                                                .getString("Launcher.Two_extensions_with_the_same_priority")
                                                + cname);
                            }

                            orderedExtensions.add(exts[j]);
                            extensionPluginServices.put(exts[j], ps);
                            extensionPluginConfig.put(exts[j], pc);
                        }
                    } catch (Throwable e) {
                        addError("Error initializing controls of plugin '"
                                + pName + "' extension '" + cname + "'", e);
                    }
                }
            } catch (Throwable e) {
                addError("Error initializing controls of plugin '" + pName
                        + "'", e);
            }
        }

        TreeSet<SortableTool> orderedTools = new TreeSet<SortableTool>(
                new ToolComparator());
        Iterator<Extension> e = orderedExtensions.iterator();

		// sort the toolbars and tools from 'normal' extensions (actiontools,
        // selectabletools)
        // and load the combo-scales and combo-buttons for the status bar
        while (e.hasNext()) {
            Extension ext = e.next();
            String extName = "unknow";
            try {
                extName = ext.getClassName();
                ToolBar[] toolbars = ext.getToolBar();

                // get tools from toolbars
                for (int k = 0; k < toolbars.length; k++) {
                    ActionTool[] tools = toolbars[k].getActionTool();

                    for (int t = 0; t < tools.length; t++) {
                        SortableTool sm = new SortableTool(
                                (extensionPluginServices.get(ext))
                                .getClassLoader(), ext, toolbars[k],
                                tools[t]);
                        orderedTools.add(sm);
                    }

                    SelectableTool[] sTools = toolbars[k].getSelectableTool();

                    for (int t = 0; t < sTools.length; t++) {
                        SortableTool sm = new SortableTool(
                                (extensionPluginServices.get(ext))
                                .getClassLoader(), ext, toolbars[k],
                                sTools[t]);
                        orderedTools.add(sm);
                    }
                }

                // get controls for statusBar
                PluginServices ps = extensionPluginServices.get(ext);
                PluginClassLoader loader = ps.getClassLoader();

                // ArrayList componentList = new ArrayList();
                ComboScale[] comboScaleArray = ext.getComboScale();
                for (int k = 0; k < comboScaleArray.length; k++) {
                    org.gvsig.gui.beans.controls.comboscale.ComboScale combo = new org.gvsig.gui.beans.controls.comboscale.ComboScale();
                    String label = comboScaleArray[k].getLabel();
                    if (label != null) {
                        combo.setLabel(label);
                    }
                    String name = comboScaleArray[k].getName();
                    if (name != null) {
                        combo.setName(name);
                    }
                    String[] elementsString = ((String) comboScaleArray[k]
                            .getElements()).split(";");
                    long[] elements = new long[elementsString.length];
                    for (int currentElem = 0; currentElem < elementsString.length; currentElem++) {
                        try {
                            elements[currentElem] = Long
                                    .parseLong(elementsString[currentElem]);
                        } catch (NumberFormatException nfex1) {
                            this
                                    .addError(ext.getClassName()
                                            + " -- "
                                            + Messages
                                            .getString("error_parsing_comboscale_elements"));
                            elements[currentElem] = 0;
                        }
                    }
                    combo.setItems(elements);
                    try {
                        long value = Long.parseLong((String) comboScaleArray[k]
                                .getValue());
                        combo.setScale(value);
                    } catch (NumberFormatException nfex2) {
                        this
                                .addError(ext.getClassName()
                                        + " -- "
                                        + Messages
                                        .getString("error_parsing_comboscale_value"));
                    }
                    try {
                        frame.addStatusBarControl(loader.loadClass(ext
                                .getClassName()), combo);
                    } catch (ClassNotFoundException e1) {
                        this
                                .addError(
                                        Messages
                                        .getString("Launcher.error_getting_class_loader_for_status_bar_control"),
                                        e1);
                    }
                }

                ComboButton[] comboButtonArray = ext.getComboButton();
                for (int k = 0; k < comboButtonArray.length; k++) {
                    ComboButtonElement[] elementList = comboButtonArray[k]
                            .getComboButtonElement();
                    org.gvsig.gui.beans.controls.combobutton.ComboButton combo = new org.gvsig.gui.beans.controls.combobutton.ComboButton();
                    String name = comboButtonArray[k].getName();
                    if (name != null) {
                        combo.setName(name);
                    }
                    for (int currentElement = 0; currentElement < elementList.length; currentElement++) {
                        ComboButtonElement element = elementList[currentElement];
                        ImageIcon icon;
                        URL iconLocation = loader
                                .getResource(element.getIcon());
                        if (iconLocation == null) {
                            this.addError(Messages.getString("Icon_not_found_")
                                    + element.getIcon());
                        } else {
                            icon = new ImageIcon(iconLocation);
                            JButton button = new JButton(icon);
                            combo.addButton(button);
                            button.setActionCommand(element.getActionCommand());
                        }
                    }
                    try {
                        frame.addStatusBarControl(loader.loadClass(ext
                                .getClassName()), combo);
                    } catch (ClassNotFoundException e1) {
                        this
                                .addError(
                                        Messages
                                        .getString("Launcher.error_getting_class_loader_for_status_bar_control"),
                                        e1);
                    }
                }
            } catch (Throwable e2) {
                addError(
                        "Error initializing tools and status bars of extension '"
                        + extName + "'", e2);
            }
        }

		// Add the tools from MDI extensions to the ordered tool-list, so that
        // we get a sorted list containing all the tools
        i = pluginsConfig.keySet().iterator();
        while (i.hasNext()) {
            String pName = (String) i.next();
            try {
                PluginConfig pc = (PluginConfig) pluginsConfig.get(pName);
                PluginServices ps = (PluginServices) pluginsServices.get(pName);

                SkinExtension[] skinExts = pc.getExtensions()
                        .getSkinExtension();
                for (int j = 0; j < skinExts.length; j++) {

                    if (skinExts[j] != null) {
                        ToolBar[] toolbars = skinExts[j].getToolBar();

                        for (int k = 0; k < toolbars.length; k++) {
                            ActionTool[] tools = toolbars[k].getActionTool();

                            for (int t = 0; t < tools.length; t++) {
                                SortableTool stb = new SortableTool(ps
                                        .getClassLoader(), skinExts[j],
                                        toolbars[k], tools[t]);
                                orderedTools.add(stb);
                            }

                            SelectableTool[] sTools = toolbars[k]
                                    .getSelectableTool();

                            for (int t = 0; t < sTools.length; t++) {
                                SortableTool stb = new SortableTool(ps
                                        .getClassLoader(), skinExts[j],
                                        toolbars[k], sTools[t]);
                                orderedTools.add(stb);
                            }
                        }
                    }
                }
                // Install popup menus
                PopupMenus pus = pc.getPopupMenus();
                if (pus != null) {
                    PopupMenu[] menus = pus.getPopupMenu();
                    for (int j = 0; j < menus.length; j++) {
                        String menuName = "(unknow)";
                        try {
                            menuName = menus[j].getName();
                            frame.addPopupMenu(ps.getClassLoader(), menus[j]);
                        } catch (Throwable ex) {
                            addError("Error adding popup menu' " + menuName + "' in plugin '" + pName + "'.");
                        }
                    }
                }
            } catch (Throwable e3) {
                addError("Error initializing skins of the plugin '" + pName
                        + "'", e3);
            }
        }

		// loop on the ordered extension list, to add them to the interface in
        // an ordered way
        Iterator<SortableTool> t = orderedTools.iterator();
        while (t.hasNext()) {
            SortableTool stb = t.next();
            try {
                if (stb.actiontool != null) {
                    frame.addTool(stb.loader, stb.extension, stb.toolbar,
                            stb.actiontool);
                } else {
                    frame.addTool(stb.loader, stb.extension, stb.toolbar,
                            stb.selectabletool);
                }
            } catch (ClassNotFoundException ex) {
                this
                        .addError(
                                Messages
                                .getString("Launcher.No_se_encontro_la_clase_de_la_extension"),
                                ex);
            } catch (Throwable e2) {
                addError("Error adding tools to the interface of extension '"
                        + stb.extension.getClassName() + "'", e2);
            }
        }
    }

    /**
     * Adds new plugins to the the andami-config file.
     */
    private void updateAndamiConfig() {
        Set<String> olds = new HashSet<String>();

        Plugin[] plugins = andamiConfig.getPlugin();

        for (int i = 0; i < plugins.length; i++) {
            olds.add(plugins[i].getName());
        }

        Iterator<PluginServices> i = pluginsServices.values().iterator();

        while (i.hasNext()) {
            PluginServices ps = i.next();

            if (!olds.contains(ps.getPluginName())) {
                Plugin p = new Plugin();
                p.setName(ps.getPluginName());
                p.setUpdate(false);

                andamiConfig.addPlugin(p);
            }
        }
    }

    private URL[] getPluginClasspathURLs(PluginConfig pluginConfig) {
        URL[] urls = null;

        String libfolderPath = pluginConfig.getLibraries().getLibraryDir();
        File libFolderFile = new File(pluginConfig.getPluginFolder(), libfolderPath);

        File[] files = libFolderFile.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return (pathname.getName().toUpperCase().endsWith(".JAR")
                        || pathname.getName().toUpperCase().endsWith(".ZIP"));
            }
        });
        if (files == null) {
            urls = new URL[0];
        } else {
            urls = new URL[files.length];
            for (int j = 0; j < files.length; j++) {
                try {
                    urls[j] = new URL("file:" + files[j]);
                } catch (MalformedURLException e) {
                    logger.warn("Can't add file '" + files[j] + "' to the classpath of plugin '" + pluginConfig.getPluginName() + "'.");
                }
            }
        }
        return urls;
    }

    private static class OrderedPlugins extends ArrayList<String> {
        
        private List<String> problems = new ArrayList<String>();
        private int retries = 0;
        private PluginsConfig pluginsConfig = null;
        private List<String> deprcatedPluginNames = null;
        
        OrderedPlugins(PluginsConfig pluginsConfig, List<String>deprcatedPluginNames) {
            super();
            this.pluginsConfig = pluginsConfig;
            this.deprcatedPluginNames = deprcatedPluginNames;
            List<String> pluginNames = new ArrayList<String>();
            for (String pluginName : pluginsConfig.keySet()) {
                pluginNames.add(pluginName);
            }
            Collections.sort(pluginNames);
            for (String pluginName : pluginNames) {
                this.add(pluginName);
            }
        }
        
        public List<String> getProblems() {
            return this.problems;
        }
 
        private void addProblem(String msg) {
           this.problems.add(msg);
        }
    
        public boolean add(String pluginName) {
            return this.add(pluginName,new ArrayList<String>());
        }
        
        private boolean add(String pluginName, List<String>processing) {
            pluginName = this.pluginsConfig.getMainKey(pluginName);
            if( this.contains(pluginName) ) {
                return true;
            }
            if( processing.contains(pluginName) ) {
                this.addProblem("Dependencias ciclicas procesando '"+pluginName+"'.");
                return true;
            }
            PluginConfig pluginConfig = this.pluginsConfig.get(pluginName);
            Depends[] dependencies = pluginConfig.getDepends();
            if( dependencies.length==0 ) {
               super.add(pluginName);
               return true;
            }
            if( this.retries > 100 ) {
               this.addProblem("Posible dependencias ciclicas procesando '"+pluginName+"'." );
               return false;
            }
            processing.add(pluginName);
            boolean dependenciesAvailables = true;
            for (Depends dependency : dependencies) {
                String dependencyName = dependency.getPluginName();
                if (deprcatedPluginNames.contains(dependencyName)) {
                    this.addProblem("Plugin '" + pluginName + "' use a deprecated plugin name '" + dependencyName + "' as dependency. Must use '" + pluginsConfig.getMainKey(dependencyName) + "'.");
                }                
                PluginConfig dependencyConfig = this.pluginsConfig.get(dependencyName);
                if( dependencyConfig == null) {
                  if( dependency.getOptional() ) {
                    this.addProblem("Plugin '" + pluginName + "', declare an optional dependency '" + dependencyName + "' that not found.");
                    continue;
                  }
                  dependenciesAvailables = false;
                  this.addProblem("Plugin '"+pluginName+"' declara a dependency '"+dependencyName+"' that not found.");
                  continue;
                }
                this.retries++;
                if( ! (this.add(dependencyName, processing)) ) {
                  dependenciesAvailables = false;
                }
                this.retries--;
            }
            if( dependenciesAvailables ) {
                super.add(pluginName);
            } else {
              this.addProblem("Plugin '"+pluginName+"' no disponible, alguna dependencia no resuelta.");
              return false;
            }
            return true;
        }      
    }
            
    private void loadPluginServices() {
        OrderedPlugins orderedPlugins = new OrderedPlugins(pluginsConfig, getDeprecatedPluginNames());
        
        if( !orderedPlugins.getProblems().isEmpty() ) {
            for (String problem : orderedPlugins.getProblems()) {
                logger.warn(problem);
            }
        }

        for (String pluginName : orderedPlugins) {
            PluginConfig config = pluginsConfig.get(pluginName);

            URL[] urls = getPluginClasspathURLs(config);
            
            List<PluginClassLoader> loaders = new ArrayList<PluginClassLoader>();
            for (Depends dependency : config.getDepends()) {
                String dependencyName = dependency.getPluginName();
                PluginServices dependencyPluginService = pluginsServices.get(dependencyName);
                if( dependencyPluginService == null ) {
                    if( dependency.getOptional() ) {
                        logger.info("Procesing plugin '"+pluginName+", optional dependency not found ("+dependencyName+").");
                    } else {
                        logger.warn("Procesing plugin '"+pluginName+", dependency not found ("+dependencyName+").");
                    }
                    continue;
                }
                loaders.add(dependencyPluginService.getClassLoader());
            }
            try {
                PluginClassLoader loader = new PluginClassLoader(
                        urls,
                        config.getPluginFolder().getAbsolutePath(),
                        Launcher.class.getClassLoader(),
                        loaders
                );
                PluginServices ps = new PluginServices(
                        loader,
                        PluginsConfig.getAlternativeNames(config)
                );
                logger.info("Plugin '" + pluginName + "' created");
                pluginsServices.put(ps.getPluginName(), ps);
                pluginsOrdered.add(pluginName);
            } catch (IOException ex) {
                logger.warn("Can't create PluginServices for '" + pluginName + "'.", ex);
            }
        }
        
        // Se eliminan los plugins que no fueron instalados
        List<String> pluginsToRemove = new ArrayList<String>();
        for (String pluginName : pluginsConfig.keySet()) {
            PluginServices pluginService = pluginsServices.get(pluginName);
            if( pluginService == null ) {
                pluginsToRemove.add(pluginName);
            } 
        }
        for (String pluginName : pluginsToRemove) {
            logger.warn("Removed plugin "+pluginName+".");
            pluginsConfig.remove(pluginName);
       }
    }
 
    /*
    private void dumpPluginsDependencyInformation() {
        logger.info("Plugin dependency information");
        Iterator<String> i = pluginsConfig.keySet().iterator();
        while (i.hasNext()) {
            String pluginName = i.next();
            PluginConfig config = (PluginConfig) pluginsConfig.get(pluginName);
            logger.info("  Plugin " + pluginName);
            Depends[] dependencies = config.getDepends();
            for (int j = 0; j < dependencies.length; j++) {
                Depends dependency = dependencies[j];
                String dependencyName = dependency.getPluginName();
                logger.info("    Dependency " + dependencyName);
            }
        }
    }
    */
    private void pluginsMessages() {
        Iterator<String> iterator = pluginsOrdered.iterator();
        PluginConfig config;
        PluginServices ps;

        while (iterator.hasNext()) {
            String pluginName = iterator.next();
            config = pluginsConfig.get(pluginName);
            ps = pluginsServices.get(pluginName);

            if ((config.getResourceBundle() != null)
                    && !config.getResourceBundle().getName().equals("")) {
                // add the locale files associated with the plugin
                org.gvsig.i18n.Messages.addResourceFamily(config
                        .getResourceBundle().getName(), ps.getClassLoader(),
                        pluginName);
                org.gvsig.i18n.Messages.addResourceFamily("i18n." + config
                        .getResourceBundle().getName(), ps.getClassLoader(),
                        pluginName);
            }
        }
    }

    static public PluginServices getPluginServices(String name) {
        return (PluginServices) pluginsServices.get(name);
    }

    static String getPluginsDir() {
        return andamiConfig.getPluginsDirectory();
    }

    static File getPluginFolder(String pluginName) {
        return pluginsConfig.get(pluginName).getPluginFolder();
    }

    static void setPluginsDir(String s) {
        andamiConfig.setPluginsDirectory(s);
    }

    static MDIFrame getMDIFrame() {
        return frame;
    }

    private PluginsConfig loadPluginConfigs() {
        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        List<File> repositoriesFolders = installerManager.getLocalAddonRepositories("plugin");
        for (File repositoryFolder : repositoriesFolders) {
            logger.info("Loading plugins configuration from repository folder " + repositoryFolder.getAbsolutePath() + ".");

            if (!repositoryFolder.exists()) {
                logger.warn("Plugins repository folder not found '" + repositoryFolder.getAbsolutePath() + "'.");
                continue;
            }

            File[] pluginsFolders = repositoryFolder.listFiles();
            if (pluginsFolders.length == 0) {
                logger.info("Plugins repository folder is empty '" + repositoryFolder.getAbsolutePath() + "'.");
                continue;
            }

            for (int i = 0; i < pluginsFolders.length; i++) {
                File pluginFolder = pluginsFolders[i];
                if (!pluginFolder.isDirectory()) {
                    continue;
                }
                String pluginName = pluginFolder.getName();
                File pluginConfigFile = new File(pluginFolder, "config.xml");
                if (!pluginConfigFile.exists()) {
                    logger.info("Plugin '" + pluginName + "' not has a config.xml file (" + pluginConfigFile.getAbsolutePath() + ".");
                    continue;
                }
                try {
                    FileInputStream is = new FileInputStream(pluginConfigFile);
                    Reader xml = org.gvsig.utils.xml.XMLEncodingUtils.getReader(is);
                    if (xml == null) {
                        // the encoding was not correctly detected, use system default
                        xml = new FileReader(pluginConfigFile);
                    } else {
                        // use a buffered reader to improve performance
                        xml = new BufferedReader(xml);
                    }
                    PluginConfig pluginConfig = (PluginConfig) PluginConfig.unmarshal(xml);
                    pluginConfig.setPluginName(pluginName);
                    pluginConfig.setPluginFolder(pluginFolder);
                    pluginsConfig.put(pluginName, pluginConfig);

                } catch (FileNotFoundException e) {
                    logger.info("Can't read plugin config file from plugin '" + pluginName + "' ('" + pluginConfigFile.getAbsolutePath() + ").");

                } catch (MarshalException e) {
                    logger.warn("Can't load plugin the config file from plugin '" + pluginName + "' is incorect. " + e.getMessage() + " ('" + pluginConfigFile.getAbsolutePath() + ").", e);

                } catch (ValidationException e) {
                    logger.warn("Can't load plugin the config file from plugin '" + pluginName + "' is invalid. " + e.getMessage() + " ('" + pluginConfigFile.getAbsolutePath() + ").", e);

                }
            }
        }
        return pluginsConfig;
    }

    private static Locale getLocale(String language, String country,
            String variant) {
        if (variant != null) {
            return new Locale(language, country, variant);
        } else if (country != null) {
            return new Locale(language, country);
        } else if (language != null) {
            return new Locale(language);
        } else {
            return new Locale("es");
        }
    }

    private static void andamiConfigToXML(String file) throws IOException,
            MarshalException, ValidationException {
		// write on a temporary file in order to not destroy current file if
        // there is some problem while marshaling
        File tmpFile = new File(file + "-"
                + DateTime.getCurrentDate().getTime());
        File xml = new File(file);
        File parent = xml.getParentFile();
        parent.mkdirs();

        BufferedOutputStream os = new BufferedOutputStream(
                new FileOutputStream(tmpFile));
        OutputStreamWriter writer = new OutputStreamWriter(os, CASTORENCODING);
        andamiConfig.marshal(writer);
        writer.close();

		// if marshaling process finished correctly, move the file to the
        // correct one
        xml.delete();
        if (!tmpFile.renameTo(xml)) {
            // if rename was not succesful, try copying it
            FileChannel sourceChannel = new FileInputStream(tmpFile)
                    .getChannel();
            FileChannel destinationChannel = new FileOutputStream(xml)
                    .getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
            sourceChannel.close();
            destinationChannel.close();
        }
    }

    private static void andamiConfigFromXML(String file)
            throws ConfigurationException {
        File xml = new File(file);

        InputStreamReader reader = null;
        try {
            // Se lee la configuraci�n
            reader = XMLEncodingUtils.getReader(xml);
            andamiConfig = (AndamiConfig) AndamiConfig.unmarshal(reader);
        } catch (FileNotFoundException e) {
            // Si no existe se ponen los valores por defecto
            andamiConfig = getDefaultAndamiConfig();
        } catch (MarshalException e) {
            // try to close the stream, maybe it remains open
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
			// if there was a problem reading the file, backup it and create a
            // new one with default values
            String backupFile = file + "-"
                    + DateTime.getCurrentDate().getTime();
            NotificationManager
                    .addError(
                            Messages
                            .getString("Error_reading_andami_config_New_file_created_A_backup_was_made_on_")
                            + backupFile, new ConfigurationException(e));
            xml.renameTo(new File(backupFile));
            andamiConfig = getDefaultAndamiConfig();
        } catch (ValidationException e) {
            throw new ConfigurationException(e);
        }
    }

    private static AndamiConfig getDefaultAndamiConfig() {
        AndamiConfig andamiConfig = new AndamiConfig();

        Andami andami = new Andami();
        andami.setUpdate(true);
        andamiConfig.setAndami(andami);
        andamiConfig.setLocaleCountry(Locale.getDefault().getCountry());
        andamiConfig.setLocaleLanguage(Locale.getDefault().getLanguage());
        andamiConfig.setLocaleVariant(Locale.getDefault().getVariant());

        if (System.getProperty("javawebstart.version") != null) // Es java web
        // start)
        {
            andamiConfig
                    .setPluginsDirectory(new File(appHomeDir, "extensiones")
                            .getAbsolutePath());
        } else {
            andamiConfig.setPluginsDirectory(new File(appName, "extensiones")
                    .getAbsolutePath());
        }

        andamiConfig.setPlugin(new Plugin[0]);
        return andamiConfig;
    }

    private static XMLEntity persistenceFromXML() throws ConfigurationException {
        File xml = getPluginsPersistenceFile(true);

        if (xml.exists()) {
            InputStreamReader reader = null;

            try {
                reader = XMLEncodingUtils.getReader(xml);
                XmlTag tag = (XmlTag) XmlTag.unmarshal(reader);
                return new XMLEntity(tag);
            } catch (FileNotFoundException e) {
                throw new ConfigurationException(e);
            } catch (MarshalException e) {

				// try to reopen with default encoding (for backward
                // compatibility)
                try {
                    reader = new FileReader(xml);
                    XmlTag tag = (XmlTag) XmlTag.unmarshal(reader);
                    return new XMLEntity(tag);

                } catch (MarshalException ex) {
                    // try to close the stream, maybe it remains open
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e1) {
                        }
                    }
                    // backup the old file
                    String backupFile = getPluginsPersistenceFile(true)
                            .getPath()
                            + "-" + DateTime.getCurrentDate().getTime();
                    NotificationManager
                            .addError(
                                    Messages
                                    .getString("Error_reading_plugin_persinstence_New_file_created_A_backup_was_made_on_")
                                    + backupFile,
                                    new ConfigurationException(e));
                    xml.renameTo(new File(backupFile));
                    // create a new, empty configuration
                    return new XMLEntity();
                } catch (FileNotFoundException ex) {
                    return new XMLEntity();
                } catch (ValidationException ex) {
                    throw new ConfigurationException(e);
                }
            } catch (ValidationException e) {
                throw new ConfigurationException(e);
            }
        } else {
            return new XMLEntity();
        }
    }

    private static File getPluginsPersistenceFile(boolean read) {
        if (read) {
            File pluginsPersistenceFile = new File(getAppHomeDir(),
                    "plugins-persistence-2_0.xml");
            if (pluginsPersistenceFile.exists()) {
                return pluginsPersistenceFile;
            }
            pluginsPersistenceFile = new File(getAppHomeDir(),
                    "plugins-persistence.xml");
            if (pluginsPersistenceFile.exists()) {
                return pluginsPersistenceFile;
            }
        }
        return new File(getAppHomeDir(), "plugins-persistence-2_0.xml");

    }

    private static void persistenceToXML(XMLEntity entity)
            throws ConfigurationException {
		// write on a temporary file in order to not destroy current file if
        // there is some problem while marshaling
        File tmpFile = new File(getPluginsPersistenceFile(false).getPath()
                + "-" + DateTime.getCurrentDate().getTime());

        File xml = getPluginsPersistenceFile(false);
        OutputStreamWriter writer = null;

        try {
            writer = new OutputStreamWriter(new FileOutputStream(tmpFile),
                    CASTORENCODING);
            entity.getXmlTag().marshal(writer);
            writer.close();

			// if marshaling process finished correctly, move the file to the
            // correct one
            xml.delete();
            if (!tmpFile.renameTo(xml)) {
                // if rename was not succesful, try copying it
                FileChannel sourceChannel = new FileInputStream(tmpFile)
                        .getChannel();
                FileChannel destinationChannel = new FileOutputStream(xml)
                        .getChannel();
                sourceChannel.transferTo(0, sourceChannel.size(),
                        destinationChannel);
                sourceChannel.close();
                destinationChannel.close();

            }
        } catch (FileNotFoundException e) {
            throw new ConfigurationException(e);
        } catch (MarshalException e) {
            // try to close the stream, maybe it remains open
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e1) {
                }
            }
        } catch (ValidationException e) {
            throw new ConfigurationException(e);
        } catch (IOException e) {
            throw new ConfigurationException(e);
        }
    }

    static MDIFrame getFrame() {
        return frame;
    }

    /**
     * Gracefully closes the application. It shows dialogs to save data, finish
     * processes, etc, then it terminates the extensions, removes temporal files
     * and finally exits.
     */
    public synchronized static void closeApplication() {
        TerminationProcess terminationProcess = (new Launcher()).new TerminationProcess();
        terminationProcess.run();
    }

    static HashMap getClassesExtensions() {
        return classesExtensions;
    }

    private static Extensions[] getExtensions() {
        List<Extensions> array = new ArrayList<Extensions>();
        Iterator<PluginConfig> iter = pluginsConfig.values().iterator();

        while (iter.hasNext()) {
            array.add(iter.next().getExtensions());
        }

        return array.toArray(new Extensions[array.size()]);
    }

    public static Iterator getExtensionIterator() {
        return extensions.iterator();
    }

    public static HashMap getPluginConfig() {
        return pluginsConfig;
    }

    public static Extension getExtension(String s) {
        Extensions[] exts = getExtensions();

        for (int i = 0; i < exts.length; i++) {
            for (int j = 0; j < exts[i].getExtensionCount(); j++) {
                if (exts[i].getExtension(j).getClassName().equals(s)) {
                    return exts[i].getExtension(j);
                }
            }
        }

        return null;
    }

    public static AndamiConfig getAndamiConfig() {
        return andamiConfig;
    }

    private static class ExtensionComparator implements Comparator {

        public int compare(Object o1, Object o2) {
            Extension e1 = (Extension) o1;
            Extension e2 = (Extension) o2;

            if (!e1.hasPriority() && !e2.hasPriority()) {
                return -1;
            }

            if (e1.hasPriority() && !e2.hasPriority()) {
                return Integer.MIN_VALUE;
            }

            if (e2.hasPriority() && !e1.hasPriority()) {
                return Integer.MAX_VALUE;
            }

            if (e1.getPriority() != e2.getPriority()) {
                return e2.getPriority() - e1.getPriority();
            } else {
                return (e2.toString().compareTo(e1.toString()));
            }
        }
    }

    private static class MenuComparator implements Comparator<SortableMenu> {

        private static ExtensionComparator extComp = new ExtensionComparator();

        public int compare(SortableMenu e1, SortableMenu e2) {

            if (!e1.menu.hasPosition() && !e2.menu.hasPosition()) {
                if (e1.extension instanceof SkinExtensionType) {
                    return 1;
                } else if (e2.extension instanceof SkinExtensionType) {
                    return -1;
                } else {
                    return extComp.compare(e1.extension, e2.extension);
                }
            }

            if (e1.menu.hasPosition() && !e2.menu.hasPosition()) {
                return Integer.MIN_VALUE;
            }

            if (e2.menu.hasPosition() && !e1.menu.hasPosition()) {
                return Integer.MAX_VALUE;
            }

            if (e1.menu.getPosition() == e2.menu.getPosition()) {
				// we don't return 0 unless both objects are the same, otherwise
                // the objects get overwritten in the treemap
                return (e1.toString().compareTo(e2.toString()));
            }
            if (e1.menu.getPosition() > e2.menu.getPosition()) {
                return Integer.MAX_VALUE;
            }
            return Integer.MIN_VALUE;

        }
    }

    private static class SortableMenu {

        public PluginClassLoader loader;
        public Menu menu;
        public SkinExtensionType extension;

        public SortableMenu(PluginClassLoader loader,
                SkinExtensionType skinExt, Menu menu2) {
            extension = skinExt;
            menu = menu2;
            this.loader = loader;
        }

    }

    private static class SortableTool {

        public PluginClassLoader loader;
        public ToolBar toolbar;
        public ActionTool actiontool;
        public SelectableTool selectabletool;
        public SkinExtensionType extension;

        public SortableTool(PluginClassLoader loader,
                SkinExtensionType skinExt, ToolBar toolbar2,
                ActionTool actiontool2) {
            extension = skinExt;
            toolbar = toolbar2;
            actiontool = actiontool2;
            this.loader = loader;
        }

        public SortableTool(PluginClassLoader loader,
                SkinExtensionType skinExt, ToolBar toolbar2,
                SelectableTool selectabletool2) {
            extension = skinExt;
            toolbar = toolbar2;
            selectabletool = selectabletool2;
            this.loader = loader;
        }
    }

    private static class ToolBarComparator implements Comparator<SortableTool> {

        private static ExtensionComparator extComp = new ExtensionComparator();

        public int compare(SortableTool e1, SortableTool e2) {

			// if the toolbars have the same name, they are considered to be
            // the same toolbar, so we don't need to do further comparing
            if (e1.toolbar.getName().equals(e2.toolbar.getName())) {
                return 0;
            }

            if (!e1.toolbar.hasPosition() && !e2.toolbar.hasPosition()) {
                if (e1.extension instanceof SkinExtensionType) {
                    return 1;
                } else if (e2.extension instanceof SkinExtensionType) {
                    return -1;
                } else {
                    return extComp.compare(e1.extension, e2.extension);
                }
            }

            if (e1.toolbar.hasPosition() && !e2.toolbar.hasPosition()) {
                return Integer.MIN_VALUE;
            }

            if (e2.toolbar.hasPosition() && !e1.toolbar.hasPosition()) {
                return Integer.MAX_VALUE;
            }
            if (e1.toolbar.getPosition() != e2.toolbar.getPosition()) {
                return e1.toolbar.getPosition() - e2.toolbar.getPosition();
            }

            if (e1.toolbar.getActionTool().equals(e2.toolbar.getActionTool())
                    && e1.toolbar.getSelectableTool().equals(
                            e2.toolbar.getSelectableTool())) {
                return 0;
            }
            return (e1.toolbar.toString().compareTo(e2.toolbar.toString()));
        }
    }

    /**
     * <p>
     * This class is used to compare tools (selectabletool and actiontool),
     * using the "position" attribute.
     * </p>
     * <p>
     * The ordering criteria are:
     * </p>
     * <ul>
     * <li>If the tools are placed in different toolbars, they use the toolbars'
     * order. (using the ToolBarComparator).</li>
     * <li></li>
     * <li>If any of the tools has not 'position' attribute, the tool which
     * <strong>has</strong> the attribute will be placed first.</li>
     * <li>If both tools have the same position (or they don't have a 'position'
     * attribute), the priority of the extensions where the tool is
     * defined.</li>
     * </ul>
     *
     * @author cesar
     * @version $Revision: 40305 $
     */
    private static class ToolComparator implements Comparator<SortableTool> {

        private static ToolBarComparator toolBarComp = new ToolBarComparator();

        public int compare(SortableTool e1, SortableTool e2) {
            // compare the toolbars which contain the tools
            long result = toolBarComp.compare(e1, e2);
            if (result != 0) { // if the toolbars are different, use their order
                return result > 0 ? 1 : -1;
            }
            // otherwise, compare the tools
            long e1Position = -1, e2Position = -1;

            if (e1.actiontool != null) {
                if (e1.actiontool.hasPosition()) {
                    e1Position = e1.actiontool.getPosition();
                }
            } else if (e1.selectabletool != null) {
                if (e1.selectabletool.hasPosition()) {
                    e1Position = e1.selectabletool.getPosition();
                }
            }

            if (e2.actiontool != null) {
                if (e2.actiontool.hasPosition()) {
                    e2Position = e2.actiontool.getPosition();
                }
            } else if (e2.selectabletool != null) {
                if (e2.selectabletool.hasPosition()) {
                    e2Position = e2.selectabletool.getPosition();
                }
            }

            if ((e1Position == -1) && (e2Position != -1)) {
                return 1;
            }
            if ((e1Position != -1) && (e2Position == -1)) {
                return -1;
            }
            if ((e1Position != -1) && (e2Position != -1)) {
                result = e1Position - e2Position;
				// we don't return 0 unless both objects are the same, otherwise
                // the objects get overwritten in the treemap
                if (result != 0) {
                    return result > 0 ? 1 : -1;
                }
            }
            return e1.toString().compareTo(e2.toString());
        }
    }

    public static String getDefaultLookAndFeel() {
        String osName = (String) System.getProperty("os.name");

        if ((osName.length() > 4)
                && osName.substring(0, 5).toLowerCase().equals("linux")) {
            return nonWinDefaultLookAndFeel;
        }
        if (osName.toLowerCase().startsWith("mac os x")) {
            return "ch.randelshofer.quaqua.QuaquaLookAndFeel";
        }

        return UIManager.getSystemLookAndFeelClassName();
    }

    /**
     * Gets the ISO 839 two-characters-long language code matching the provided
     * language code (which may be an ISO 839-2/T three-characters-long code or
     * an ISO 839-1 two-characters-long code).
     *
     * If the provided parameter is already two characters long, it returns the
     * parameter without any modification.
     *
     * @param langCode A language code representing either an ISO 839-2/T
     * language code or an ISO 839-1 code.
     * @return A two-characters-long code specifying an ISO 839 language code.
     */
    private static String normalizeLanguageCode(String langCode) {
        final String fileName = "iso_639.tab";
        if (langCode.length() == 2) {
            return langCode;
        } else if (langCode.length() == 3) {
            if (langCode.equals("va") || langCode.equals("val")) { // special
                // case
                // for
                // Valencian
                return "ca";
            }
            URL isoCodes = Launcher.class.getClassLoader()
                    .getResource(fileName);
            if (isoCodes != null) {
                try {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(isoCodes.openStream(),
                                    "ISO-8859-1"));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        String[] language = line.split("\t");
                        if (language[0].equals(langCode)) {
							// the three
                            // characters code
                            return language[2]; // third column i the two
                            // characters code
                        }
                    }
                } catch (IOException ex) {
                    logger.error(Messages
                            .getString("Error_reading_isocodes_file"), ex);
                    return "es";
                }
            } else {
                logger.error(Messages.getString("Error_reading_isocodes_file"));
                return "es";
            }
        }
        return "es";
    }

    /**
     * Configures the locales (languages and local resources) to be used by the
     * application.
     *
     * First it tries to get the locale from the command line parameters, then
     * the andami-config file is checked.
     *
     * The locale name is normalized to get a two characters language code as
     * defined by ISO-639-1 (although ISO-639-2/T three characters codes are
     * also accepted from the command line or the configuration file).
     *
     * Finally, the gvsig-i18n library and the default locales for Java and
     * Swing are configured.
     *
     */
    private static void configureLocales(String[] args) {
        // Configurar el locale
        String localeStr = null;

        localeStr = PluginServices.getArgumentByName("language");
        if (localeStr == null) {
            localeStr = andamiConfig.getLocaleLanguage();
        }
        localeStr = normalizeLanguageCode(localeStr);
        locale = getLocale(
                localeStr,
                andamiConfig.getLocaleCountry(),
                andamiConfig.getLocaleVariant()
        );
        org.gvsig.i18n.Messages.setCurrentLocale(locale);
        JComponent.setDefaultLocale(locale);
        /*
         org.gvsig.i18n.Messages.addLocale(locale);
         // add english and spanish as fallback languages
         if ( localeStr.equals("es") || localeStr.equals("ca")
         || localeStr.equals("gl") || localeStr.equals("eu")
         || localeStr.equals("va") ) {
         // prefer Spanish for languages spoken in Spain
         org.gvsig.i18n.Messages.addLocale(new Locale("es"));
         org.gvsig.i18n.Messages.addLocale(new Locale("en"));
         } else {
         // prefer English for the rest
         org.gvsig.i18n.Messages.addLocale(new Locale("en"));
         org.gvsig.i18n.Messages.addLocale(new Locale("es"));
         }
         */
        // Create classloader for the i18n resources in the
        // andami and user i18n folder. Those values will have
        // precedence over any other values added afterwards
        File appI18nFolder = new File(getApplicationFolder(), "i18n");
        File userI18nFolder = new File(getAppHomeDir(), "i18n");
        if (!userI18nFolder.exists()) {
            try {
                FileUtils.forceMkdir(userI18nFolder);
            } catch (IOException e) {
                logger.info("Can't create i18n folder in gvSIG home (" + userI18nFolder + ").", e);
            }
        }
//        logger.info("Loading i18n resources from the application and user "
//                + "folders: {}, {}", appI18nFolder, userI18nFolder);

        URL[] i18nURLs = getURLsFromI18nFolders(userI18nFolder, appI18nFolder);
        ClassLoader i18nClassLoader = URLClassLoader.newInstance(i18nURLs, null);
        logger.info("loading resources from classloader " + i18nClassLoader.toString() + ".");
        org.gvsig.i18n.Messages.addResourceFamily("text", i18nClassLoader,
                "Andami Launcher");

        // Finally load the andami own i18n resources
        org.gvsig.i18n.Messages.addResourceFamily("org.gvsig.andami.text",
                "Andami Launcher");
    }

    private static URL[] getURLsFromI18nFolders(File userFolder, File appFolder) {
        List<URL> urls = new ArrayList<URL>();
        File[] files = new File[]{userFolder, appFolder};
        for (int n1 = 0; n1 < files.length; n1++) {
            File folder = files[n1];
//                try {
//                    urls.add(folder.toURI().toURL());
//                } catch (MalformedURLException ex) {
//                    logger.warn("Can't convert file to url (file="+userFolder.getAbsolutePath()+").", ex);
//                    return null;
//                }
            File[] subFiles = folder.listFiles();
            for (int n2 = 0; n2 < subFiles.length; n2++) {
                File subFolder = subFiles[n2];
                if ("andami".equalsIgnoreCase(subFolder.getName())) {
                    // Skip andami and add the last.
                    continue;
                }
                if (subFolder.isDirectory()) {
                    try {
                        urls.add(subFolder.toURI().toURL());
                    } catch (MalformedURLException ex) {
                        logger.warn("Can't convert file to url (file=" + subFolder.getAbsolutePath() + ").", ex);
                        return null;
                    }
                }
            }
        }
        File folder = new File(appFolder, "andami");
        try {
            urls.add(folder.toURI().toURL());
        } catch (MalformedURLException ex) {
            logger.warn("Can't convert file to url (file=" + folder.getAbsolutePath() + ").", ex);
            return null;
        }
        return urls.toArray(new URL[urls.size()]);
    }

    /**
     * Gets Home Directory location of the application into users home folder.
     *
     * May be set from outside the aplication by means of
     * -DgvSIG.home=C:/data/gvSIG, where gvSIG its the name of the application
     *
     * @return
     */
    public static String getAppHomeDir() {
        return appHomeDir;
    }

    public static File getApplicationHomeFolder() {
        return new File(getAppHomeDir());
    }

    /**
     * Sets Home Directory location of the application. May be set from outside
     * the aplication by means of -DgvSIG.home=C:/data/gvSIG, where gvSIG its
     * the name of the application
     *
     * @param appHomeDir
     */
    public static void setAppHomeDir(String appHomeDir) {
        Launcher.appHomeDir = appHomeDir;
    }

    /**
     * Initialize the extesion that have to take the control of the state of
     * action controls of the UI of all extensions. <br>
     * <br>
     * For use this option you have to add an argument to the command line like
     * this: <br>
     * <br>
     * -exclusiveUI={pathToExtensionClass} <br>
     *
     * @see org.gvsig.andami.plugins.IExtension#isEnabled(IExtension extension)
     * @see org.gvsig.andami.plugins.IExtension#isVisible(IExtension extension)
     */
    private static void initializeExclusiveUIExtension() {
        String name = PluginServices.getArgumentByName("exclusiveUI");
        if (name == null) {
            return;
        }

        Iterator<Class<? extends IExtension>> iter = classesExtensions.keySet()
                .iterator();
        int charIndex;
        Class<? extends IExtension> key;
        while (iter.hasNext()) {
            key = iter.next();
            charIndex = key.getName().indexOf(name);
            // System.out.println("key='"+key.getName()+"' name='"+name+"' charIndex="+charIndex);
            if (charIndex == 0) {
                IExtension ext = classesExtensions.get(key);
                if (ext instanceof ExtensionDecorator) {
                    ext = ((ExtensionDecorator) ext).getExtension();
                }
                if (ext instanceof ExclusiveUIExtension) {
                    PluginServices
                            .setExclusiveUIExtension((ExclusiveUIExtension) ext);
                }
                break;
            }
        }

        logger
                .error(Messages
                        .getString("No_se_encontro_la_extension_especificada_en_el_parametro_exclusiveUI")
                        + " '" + name + "'");
    }

    public static void initIconThemes() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        IconThemeManager iconManager = ToolsSwingLocator.getIconThemeManager();

        File f = new File(pluginsManager.getApplicationFolder(), "icon-theme");
        if (!f.exists()) {
            try {
                f.mkdir();
            } catch (Exception ex) {
                // Do nothing
            }
        }
        iconManager.getRepository().add(f, "_Global");

        f = new File(pluginsManager.getApplicationHomeFolder(), "icon-theme");
        if (!f.exists()) {
            try {
                f.mkdir();
            } catch (Exception ex) {
                // Do nothing
            }
        }
        iconManager.getRepository().add(f, "_User");

        Preferences prefs = Preferences.userRoot().node("gvsig.icontheme");
        String defaultThemeID = prefs.get("default-theme", null);
        if (defaultThemeID != null) {
            IconTheme iconTheme = iconManager.get(defaultThemeID);
            if (iconTheme != null) {
                iconManager.setCurrent(iconTheme);
            }
        }
    }

    public static void manageUnsavedData(String prompt) throws Exception {
        final UnsavedDataPanel panel = new UnsavedDataPanel(prompt);
        final List<IUnsavedData> unsavedData = PluginsLocator.getManager().getUnsavedData();
        panel.setUnsavedData(unsavedData);

        panel.addActionListener(panel.new UnsavedDataPanelListener() {

            public void cancel(UnsavedDataPanel panel) {
                panel.setVisible(false);
            }

            public void discard(UnsavedDataPanel panel) {
                panel.setVisible(false);
            }

            public void accept(UnsavedDataPanel panel) {
                panel.setVisible(false);

                try {
                    PluginsLocator.getManager().saveUnsavedData(panel.getSelectedsUnsavedDataList());
                } catch (UnsavedDataException e) {
                    StringBuilder msg = new StringBuilder();
                    msg.append(PluginServices.getText(this, "The_following_resources_could_not_be_saved"));
                    msg.append("\n");
                    for (Iterator iterator = e.getUnsavedData().iterator(); iterator.hasNext();) {
                        IUnsavedData unsavedData = (IUnsavedData) iterator.next();
                        msg.append(unsavedData.getResourceName());
                        msg.append(" -- ");
                        msg.append(unsavedData.getDescription());
                        msg.append("\n");
                    }
                    JOptionPane.showMessageDialog(panel, msg, PluginServices.getText(this, "Resources_was_not_saved"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        //TODO: mostrar panel WindowManager
        ToolsSwingLocator.getWindowManager().showWindow(
                panel,
                PluginServices.getText(panel, "Resource_was_not_saved"),
                MODE.DIALOG);

    }

    /**
     * Manages Andami termination process
     *
     * @author Cesar Martinez Izquierdo <cesar.martinez@iver.es>
     */
    public class TerminationProcess {

        private boolean proceed = false;
        private UnsavedDataPanel panel = null;

        public void run() {
            try {
                int exit = manageUnsavedData();
                if ((exit == JOptionPane.NO_OPTION)
                        || (exit == JOptionPane.CLOSED_OPTION)) {
                    // the user doesn't want to exit
                    return;
                }
                closeAndami();
            } catch (Exception e) {
                logger.warn("It is not possible to close the application", e);
				// It is not possible to close the application.
                // this exception has been registered before
            }
        }

        /**
         * Finishes the application without asking user if want or not to save
         * unsaved data.
         */
        public void closeAndami() {
            PluginsManager pluginsManager = PluginsLocator.getManager();
            pluginsManager.executeShutdownTasks();

            try {
                saveAndamiConfig();
            } catch (Exception ex) {
                logger.error(
                        "There was an error exiting application, can't save andami-config.xml",
                        ex
                );
            }

            try {
                // Persistencia de los plugins
                savePluginPersistence();
                savePluginsProperties();
            } catch (Exception ex) {
                logger.error(
                        "There was an error exiting application, can't save plugins properties",
                        ex
                );
            }

            // Finalize all the extensions
            finalizeExtensions();

            try {
                // Clean any temp data created
                Utilities.cleanUpTempFiles();
            } catch (Exception ex) {
                logger.error(
                        "There was an error exiting application, can't remove temporary files",
                        ex
                );
            }

            logger.info("Quiting application.");

            // Para la depuraci�n de memory leaks
            System.gc();

            System.exit(0);
        }

        public void saveAndamiConfig() {
            // Configuraci�n de Andami
            try {
                andamiConfigToXML(andamiConfigPath);
            } catch (MarshalException e) {
                logger
                        .error(
                                Messages
                                .getString("Launcher.No_se_pudo_guardar_la_configuracion_de_andami"),
                                e);
            } catch (ValidationException e) {
                logger
                        .error(
                                Messages
                                .getString("Launcher.No_se_pudo_guardar_la_configuracion_de_andami"),
                                e);
            } catch (IOException e) {
                logger
                        .error(
                                Messages
                                .getString("Launcher.No_se_pudo_guardar_la_configuracion_de_andami"),
                                e);
            }
        }

        private void savePluginsProperties() {
            PluginsManager manager = PluginsLocator.getManager();
            List<PluginServices> plugins = manager.getPlugins();
            for (PluginServices plugin : plugins) {
                if (plugin != null) {
                    plugin.savePluginProperties();
                }
            }
        }

        /**
         * Exectutes the terminate method for all the extensions, in the reverse
         * order they were initialized
         *
         */
        private void finalizeExtensions() {
            for (int i = extensions.size() - 1; i >= 0; i--) {
                org.gvsig.andami.plugins.IExtension extensionInstance = (org.gvsig.andami.plugins.IExtension) extensions
                        .get(i);
                String extensionName = "(unknow)";
                try {
                    extensionName = extensionInstance.getClass().getName();
                    extensionInstance.terminate();
                } catch (Exception ex) {
                    logger.error(MessageFormat.format(
                            "There was an error extension ending {0}",
                            extensionName), ex);
                }
            }
        }

        private IUnsavedData[] getUnsavedData() throws Exception {
            List<IUnsavedData> unsavedDataList = new ArrayList<IUnsavedData>();
            IExtension exclusiveExtension = PluginServices
                    .getExclusiveUIExtension();

            for (int i = extensions.size() - 1; i >= 0; i--) {
                org.gvsig.andami.plugins.IExtension extensionInstance = (org.gvsig.andami.plugins.IExtension) extensions
                        .get(i);
                IExtensionStatus status = null;
                if (exclusiveExtension != null) {
                    status = exclusiveExtension.getStatus(extensionInstance);
                } else {
                    status = extensionInstance.getStatus();
                }
                if (status != null) {
                    try {
                        if (status.hasUnsavedData()) {
                            IUnsavedData[] array = status.getUnsavedData();
                            for (int element = 0; element < array.length; element++) {
                                unsavedDataList.add(array[element]);
                            }
                        }
                    } catch (Exception e) {
                        logger.info("Error calling the hasUnsavedData method",
                                new Exception());
                        int option = JOptionPane
                                .showConfirmDialog(
                                        frame,
                                        Messages
                                        .getString("error_getting_unsaved_data"),
                                        Messages.getString("MDIFrame.salir"),
                                        JOptionPane.YES_NO_OPTION);
                        if (option == JOptionPane.NO_OPTION) {
                            throw e;
                        }
                    }
                }
            }
            return unsavedDataList.toArray(new IUnsavedData[unsavedDataList
                    .size()]);
        }

        public UnsavedDataPanel getUnsavedDataPanel() {
            if (panel == null) {
                panel = new UnsavedDataPanel(new IUnsavedData[0]);
            }
            return panel;
        }

        /**
         * Checks if the extensions have some unsaved data, and shows a dialog
         * to allow saving it. This dialog also allows to don't exit Andami.
         *
         * @return true if the user confirmed he wishes to exit, false otherwise
         * @throws Exception
         */
        public int manageUnsavedData() throws Exception {
            IUnsavedData[] unsavedData = getUnsavedData();

            // there was no unsaved data
            if (unsavedData.length == 0) {
                int option = JOptionPane
                        .showConfirmDialog(frame, Messages
                                .getString("MDIFrame.quiere_salir"), Messages
                                .getString("MDIFrame.salir"),
                                JOptionPane.YES_NO_OPTION);
                return option;
            }

            UnsavedDataPanel panel = getUnsavedDataPanel();
            panel.setUnsavedDataArray(unsavedData);

            panel.addActionListener(panel.new UnsavedDataPanelListener() {

                public void cancel(UnsavedDataPanel panel) {
                    proceed(false);
                    PluginServices.getMDIManager().closeWindow(panel);

                }

                public void discard(UnsavedDataPanel panel) {
                    proceed(true);
                    PluginServices.getMDIManager().closeWindow(panel);

                }

                public void accept(UnsavedDataPanel panel) {
                    IUnsavedData[] unsavedDataArray = panel
                            .getSelectedsUnsavedData();
                    boolean saved;
                    for (int i = 0; i < unsavedDataArray.length; i++) {
                        try {
                            saved = unsavedDataArray[i].saveData();
                        } catch (Exception ex) {
                            PluginServices.getLogger().error(
                                    "Error saving"
                                    + unsavedDataArray[i]
                                    .getResourceName(), ex);
                            saved = false;
                        }
                        if (!saved) {
                            JOptionPane
                                    .showMessageDialog(
                                            panel,
                                            PluginServices
                                            .getText(this,
                                                    "The_following_resource_could_not_be_saved_")
                                            + "\n"
                                            + unsavedDataArray[i]
                                            .getResourceName()
                                            + " -- "
                                            + unsavedDataArray[i]
                                            .getDescription(),
                                            PluginServices.getText(this,
                                                    "unsaved_resources"),
                                            JOptionPane.ERROR_MESSAGE);

                            try {
                                unsavedDataArray = getUnsavedData();
                            } catch (Exception e) {
                                // This exception has been registered before
                            }
                            panel.setUnsavedDataArray(unsavedDataArray);
                            return;
                        }
                    }
                    proceed(true);
                    PluginServices.getMDIManager().closeWindow(panel);
                }
            });

            PluginServices.getMDIManager().addWindow(panel);
            if (proceed) {
                return JOptionPane.YES_OPTION;
            } else {
                return JOptionPane.NO_OPTION;
            }
        }

        private void proceed(boolean proceed) {
            this.proceed = proceed;
        }

    }

    public static TerminationProcess getTerminationProcess() {
        return (new Launcher()).new TerminationProcess();
    }

    private PackageInfo getPackageInfo(String pluginsFolder) {
        try {
            PluginsManager pm = PluginsLocator.getManager();
            return pm.getPackageInfo();
        } catch (Exception e) {
            logger.info("Can't locate PackageInfo from plugin org.gvsig.app", e);
            return null;
        }
    }

    /**
     * Launch the gvSIG package installer.
     *
     * @throws Exception if there is any error
     */
    private void doInstall(String[] args) throws Exception {
        String installURL = null;
        String installURLFile = null;
        String gvSIGVersion = null;
        String[] myArgs = new String[3];
        PackageInfo packageInfo = null;

        Options options = new Options();
        options.addOption("i", "install", false, "install");
        options.addOption("u", "installURL", true, "installURL");
        options.addOption("f", "installURLFile", true, "installURLFile");
        options.addOption("v", "installVersion", true, "installVersion");
        options.addOption("A", "applicationName", true, "application name, default is gvSIG");
        options.addOption("P", "pluginsFolder", true, "pluginsFolder");
        options.addOption("l", "language", true, "language");

        // This options are managed by the gvSIG.sh but need to be declared here to avoid
        // errors.
        options.addOption(null, "debug", false, "Activate the JVM remote debug");
        options.addOption(null, "pause", false, "Pause when activate JVM debug");

        /*
         * Los parametros que deberian pasarse en el instalador oficial de gvSIG serian:
         *
         * --install
         * --applicationName=gvSIG
         * --language=es
         * --pluginsFolder=gvSIG/extensiones
         *
         * Opcionales (casi mejor que dejar los de por defecto y no pasarselos):
         * --installVersion=2.0.0
         * --installURL=http://downloads.gvsig.org/download/gvsig-desktop/dists
         * --installURLFile=gvSIG/extensiones/org.gvsig.installer.app.extension/defaultDownloadsURLs
         *
         */
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;
        try {
            line = parser.parse(options, args);
            boolean hasAllMandatoryOptions = true;
            if (!line.hasOption("install")) {
                hasAllMandatoryOptions = false;
            }

            if (line.hasOption("installVersion")) {
                gvSIGVersion = line.getOptionValue("installVersion");
            }
            if (line.hasOption("applicationName")) {
                myArgs[0] = line.getOptionValue("applicationName");
            } else {
                myArgs[0] = "gvSIG";
            }
            if (line.hasOption("pluginsFolder")) {
                myArgs[1] = line.getOptionValue("pluginsFolder");
            } else {
                myArgs[1] = "gvSIG/extensiones";
            }
            if (line.hasOption("language")) {
                myArgs[2] = "language=" + line.getOptionValue("language");
            } else {
                // prevent null
                myArgs[2] = Locale.getDefault().toString();
            }

            if (line.hasOption("installURL")) {
                installURL = line.getOptionValue("installURL");
            } else {
                installURL = "http://downloads.gvsig.org/download/gvsig-desktop/";
            }

            if (line.hasOption("installURLFile")) {
                installURLFile = line.getOptionValue("installURLFile");
            } else {
                installURLFile = "gvsig-installer-urls.config";
            }

            if (!hasAllMandatoryOptions) {
                System.err.println(Messages.get("usage") + ": Launcher "
                        + "--install "
                        + "[--applicationName=appName] "
                        + "[--pluginsFolder=plugins-directory] "
                        + "[--installURLFile=File] "
                        + "[--installURL=URL] "
                        + "[--language=locale]"
                );
                return;
            }
        } catch (ParseException exp) {
            System.err.println("Unexpected exception:" + exp.getMessage());
            System.exit(-1);
        }

        initializeApp(myArgs, "installer");

        new DefaultLibrariesInitializer().fullInitialize(true);

        initializeInstallerManager();

        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        
//        try {
//            logger.info("Loading plugins configurations");
//            this.loadPluginConfigs();
//        } catch (Throwable ex) {
//            logger.warn("Can't load plugins configurations", ex);
//        }
//
//        try {
//            logger.info("Loading plugins");
//            this.loadPluginServices();
//        } catch (Throwable ex) {
//            logger.warn("Can't load plugins", ex);
//        }

        AndamiConfig config = getAndamiConfig();

        initializeIdentityManagement(new File(config.getPluginsDirectory()).getAbsoluteFile());

        initializeLibraries();

        packageInfo = getPackageInfo(myArgs[1]);

        // set the gvSIG version to the install manager, to compose the download URL
        if (packageInfo != null) {
            installerManager.setVersion(packageInfo.getVersion());
        } else {
            installerManager.setVersion(gvSIGVersion);
        }
        if (!installURL.contains(";")
                && !installURL.endsWith(InstallerManager.PACKAGE_EXTENSION)
                && !installURL.endsWith(InstallerManager.PACKAGE_INDEX_EXTENSION)) {
            if (packageInfo != null && (packageInfo.getState().startsWith(InstallerManager.STATE.BETA)
                    || packageInfo.getState().startsWith(InstallerManager.STATE.RC)
                    || packageInfo.getState().equalsIgnoreCase(InstallerManager.STATE.FINAL))) {
                installURL = installURL + "dists/<%Version%>/builds/<%Build%>/packages.gvspki";
            }
        }
        // Configure default index download URL
        SwingInstallerLocator.getSwingInstallerManager().setDefaultDownloadURL(installURL);

        SwingInstallerLocator.getSwingInstallerManager().setDefaultDownloadURL(new File(installURLFile));

        // Launch installer
        PluginsManager manager = PluginsLocator.getManager();

        AbstractInstallPackageWizard installPackageWizard = SwingInstallerLocator
                .getSwingInstallerManager().createInstallPackageWizard(
                        manager.getApplicationFolder(),
                        manager.getInstallFolder());
        installPackageWizard.setWizardActionListener(new InstallerWizardActionListener() {
            public void finish(InstallerWizardPanel installerWizard) {
                logger.info("Finish installation.");
                System.exit(0);
            }

            public void cancel(InstallerWizardPanel installerWizard) {
                logger.info("Cancel installation.");
                System.exit(0);
            }
        });

        // the wizard will show the Typical or Advanced mode option.
        installPackageWizard.setAskTypicalOrCustom(true);
        // default packages will be selected.
        installPackageWizard.setSelectDefaultPackages(true);

        JFrame frame = new JFrame(Messages.get("gvsig_package_installer"));

        frame.addWindowListener(new WindowListener() {
            public void windowOpened(WindowEvent we) {
                logger.info("Open window installation.");
            }

            public void windowClosing(WindowEvent we) {
                logger.info("Closing installation.");
                System.exit(0);
            }

            public void windowClosed(WindowEvent we) {
                logger.info("Close installation.");
                System.exit(0);
            }

            public void windowIconified(WindowEvent we) {
            }

            public void windowDeiconified(WindowEvent we) {
            }

            public void windowActivated(WindowEvent we) {
                logger.info("Activate window installation.");
            }

            public void windowDeactivated(WindowEvent we) {
                logger.info("Deactivate window installation.");
            }
        });
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().add(installPackageWizard, BorderLayout.CENTER);

        URL iconURL = getClass().getResource("/images/main/install-icon.png");
        if( iconURL!=null ) {
            ImageIcon icon = new ImageIcon(iconURL);
            frame.setIconImage(icon.getImage());
        }
        frame.pack();
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }

    public static String getInformation() {
        return getInformation(null);
    }

    private static final int INFO_OS_NAME = 0;
    private static final int INFO_OS_ARCH = 1;
    private static final int INFO_OS_VERSION = 2;
    private static final int INFO_OS_ADITIONAL = 3;
    private static final int INFO_JRE_VENDOR = 4;
    private static final int INFO_JRE_VERSION = 5;
    private static final int INFO_JRE_HOME = 6;
    private static final int INFO_PROXY_HOST = 7;
    private static final int INFO_PROXY_PORT = 8;
    private static final int INFO_PROXY_USER = 9;
    private static final int INFO_PROXY_PASSWORD = 10;
    private static final int INFO_APP_LOCALE = 11;
    private static final int INFO_APP_FOLDER = 12;
    private static final int INFO_APP_HOME = 13;
    private static final int INFO_APP_INSTALL_FOLDER = 14;
    private static final int INFO_APP_PLUGINS_FOLDER = 15;
    private static final int INFO_APP_THEME = 16;
    private static final int INFO_APP_SKIN = 17;
    private static final int INFO_PACKAGES = 18;

    public static String getInformation(PackageInfo[] pkgs) {

        String template = "OS\n"
                + "    name    : {" + INFO_OS_NAME + "}\n"
                + "    arch    : {" + INFO_OS_ARCH + "}\n"
                + "    version : {" + INFO_OS_VERSION + "} \n"
                + "{" + INFO_OS_ADITIONAL + "}"
                + "JRE\n"
                + "    vendor  : {" + INFO_JRE_VENDOR + "}\n"
                + "    version : {" + INFO_JRE_VERSION + "}\n"
                + "    home    : {" + INFO_JRE_HOME + "}\n"
                + "HTTP Proxy\n"
                + "    http.proxyHost     : {" + INFO_PROXY_HOST + "}\n"
                + "    http.proxyPort     : {" + INFO_PROXY_PORT + "}\n"
                + "    http.proxyUserName : {" + INFO_PROXY_USER + "}\n"
                + "    http.proxyPassword : {" + INFO_PROXY_PASSWORD + "}\n"
                + "Application\n"
                + "    locale language         : {" + INFO_APP_LOCALE + "}\n"
                + "    application forlder     : {" + INFO_APP_FOLDER + "}\n"
                + "    application home forlder: {" + INFO_APP_HOME + "}\n"
                + "    install forlder         : {" + INFO_APP_INSTALL_FOLDER + "}\n"
                + "    plugins forlder         : {" + INFO_APP_PLUGINS_FOLDER + "}\n"
                + "    theme                   : {" + INFO_APP_THEME + "}\n"
                + "    Skin                    : {" + INFO_APP_SKIN + "}\n"
                + "Installed packages\n"
                + "{" + INFO_PACKAGES + "}";

        String values[] = new String[INFO_PACKAGES + 1];

        PluginsManager pluginmgr = PluginsLocator.getManager();
        LocaleManager localemgr = PluginsLocator.getLocaleManager();

        Properties props = System.getProperties();

        // OS information
        values[INFO_OS_NAME] = props.getProperty("os.name");
        values[INFO_OS_ARCH] = props.getProperty("os.arch");
        values[INFO_OS_VERSION] = props.getProperty("os.version");

        if (values[INFO_OS_NAME].startsWith("Linux")) {
            try {
                StringWriter writer = new StringWriter();

                String[] command = {"lsb_release", "-a"};
                Process p = Runtime.getRuntime().exec(command);
                InputStream is = p.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.write("    " + line + "\n");
                }
                values[INFO_OS_ADITIONAL] = writer.toString();
            } catch (Exception ex) {
                logger.warn("Can't get detailled os information (lsb_release -a).", ex);
            }
        }

        values[INFO_JRE_VENDOR] = props.getProperty("java.vendor");
        values[INFO_JRE_VERSION] = props.getProperty("java.version");
        values[INFO_JRE_HOME] = props.getProperty("java.home");
        values[INFO_PROXY_HOST] = props.getProperty("http.proxyHost");
        values[INFO_PROXY_PORT] = props.getProperty("http.proxyPort");
        values[INFO_PROXY_USER] = props.getProperty("http.proxyUserName");

        if (props.get("http.proxyPassword") == null) {
            values[INFO_PROXY_PASSWORD] = "(null)";
        } else {
            values[INFO_PROXY_PASSWORD] = "***********";
        }

        try {
            values[INFO_APP_SKIN] = MDIManagerFactory.getSkinExtension().getClassName();
        } catch (Throwable e) {
            values[INFO_APP_SKIN] = "(unknow)";
        }
        values[INFO_APP_LOCALE] = localemgr.getCurrentLocale().toString();
        values[INFO_APP_FOLDER] = pluginmgr.getApplicationFolder().getAbsolutePath();
        values[INFO_APP_HOME] = pluginmgr.getApplicationHomeFolder().getAbsolutePath();
        values[INFO_APP_INSTALL_FOLDER] = pluginmgr.getInstallFolder().getAbsolutePath();
        values[INFO_APP_PLUGINS_FOLDER] = StringUtils.join(pluginmgr.getPluginsFolders());
        values[INFO_APP_THEME] = Launcher.theme.getSource().getAbsolutePath();

        try {
            if (pkgs == null) {
                InstallerManager installmgr = InstallerLocator.getInstallerManager();
                pkgs = installmgr.getInstalledPackages();
            }
            StringWriter writer = new StringWriter();
            for (int i = 0; i < pkgs.length; i++) {
                writer.write("    ");
                writer.write(pkgs[i].toStringCompact());
                writer.write("\n");
            }
            values[INFO_PACKAGES] = writer.toString();

        } catch (Throwable e) {
            logger.warn("Can't get installed package information.", e);
        }

        String s = MessageFormat.format(template, values);
        return s;
    }

    private void logger_info(String msg) {
        String info[] = msg.split("\n");
        for (int i = 0; i < info.length; i++) {
            logger.info(info[i]);
        }
    }

    private void saveEnvironInformation(PackageInfo[] pkgs) {
        PluginsManager manager = PluginsLocator.getManager();
        File fout = new File(manager.getApplicationHomeFolder(), "gvSIG-environ.info");
        try {
            FileUtils.write(fout, getInformation(pkgs));
        } catch (IOException e) {
            logger.info("Can't create '" + fout.getAbsolutePath() + "'");
        }
    }

    private void fixIncompatiblePlugins(PackageInfo[] installedPackages) {
        final Set<String> incompatiblePlugins = new HashSet<String>();

        // Add installed packages to a Map to optimize searchs
        final Map<String, PackageInfo> packages = new HashMap<String, PackageInfo>();
        for (int i = 0; i < installedPackages.length; i++) {
            packages.put(installedPackages[i].getCode(), installedPackages[i]);
        }
        Iterator<Entry<String, PluginConfig>> it = pluginsConfig.entrySet().iterator();
        while (it.hasNext()) {
            List<String> pluginNames = new ArrayList<String>();
            Entry<String, PluginConfig> entry = it.next();
            PluginConfig pluginConfig = entry.getValue();
            pluginNames.add(entry.getKey());

			// Locate the package for this plugin.
            // Be care whith alias
            String[] aliases = pluginsConfig.getAliases(pluginConfig);
            if (aliases != null) {
                for (int i = 0; i < aliases.length; i++) {
                    pluginNames.add(aliases[i]);
                }
            }
            PackageInfo pkg = null;
            for (int n = 0; n < pluginNames.size(); n++) {
                pkg = packages.get(pluginNames.get(n));
                if (pkg != null) {
                    break;
                }
            }

            // If package is found verify dependencies
            if (pkg != null) {
                Dependencies dependencies = pkg.getDependencies();
                for (int i = 0; i < dependencies.size(); i++) {
                    Dependency dependency = (Dependency) dependencies.get(i);
                    if (Dependency.CONFLICT.equalsIgnoreCase(dependency.getType())) {
                        String code = dependency.getCode();
                        if (pluginsConfig.get(code) != null) {
                            incompatiblePlugins.add(pkg.getCode());
                            incompatiblePlugins.add(code);
                        }
                    }
                }
            }
        }
        if (incompatiblePlugins.isEmpty()) {
            return;
        }
        splashWindow.toBack();
        DisablePluginsConflictingDialog dlg = new DisablePluginsConflictingDialog(packages, incompatiblePlugins);
        dlg.setVisible(true);
        splashWindow.toFront();
        switch (dlg.getAction()) {
            case DisablePluginsConflictingDialog.CLOSE:
                System.exit(0);
                break;
            case DisablePluginsConflictingDialog.CONTINUE:
                break;
        }
        List<String> pluginsToDissable = dlg.getPluginNamesToDisable();
        if (pluginsToDissable == null) {
            return;
        }

        Iterator<String> it2 = pluginsToDissable.iterator();
        while (it2.hasNext()) {
            String pluginName = it2.next();
            logger.info("Dissabling plugin '" + pluginName + "' by user action.");
            pluginsConfig.remove(pluginName);
        }
    }

    private class DisablePluginsConflictingDialog extends JDialog {

        public static final int CONTINUE = 0;
        public static final int CLOSE = 1;

        private DisablePluginsConflictingLayoutPanel contents;
        private int action = 0;
        private List<Item> incompatiblePlugins = null;
        private Map<String, PackageInfo> packages;

        private class Item {

            private String code;
            private PackageInfo pkg;

            public Item(String code, PackageInfo pkg) {
                this.code = code;
                this.pkg = pkg;
            }

            public String toString() {
                if (this.pkg == null) {
                    return code;
                }
                return this.pkg.getName() + " (" + this.pkg.getCode() + ")";
            }

            public String getCode() {
                if (pkg == null) {
                    return code;
                }
                return pkg.getCode();
            }
        }

        DisablePluginsConflictingDialog(Map<String, PackageInfo> packages, Set<String> incompatiblePlugins) {
            super((Frame) null, "", true);
            this.setTitle(translate("_Conflicting_plugins"));

            this.packages = packages;

            this.incompatiblePlugins = new ArrayList<Item>();
            Item item = null;
            Iterator<String> it = incompatiblePlugins.iterator();
            while (it.hasNext()) {
                String code = it.next();
                item = new Item(code, packages.get(code));
                this.incompatiblePlugins.add(item);
                logger.info("Found plugin '" + item.getCode() + "' incopatibles with each other.");
            }
            initComponents();
        }

        private void initComponents() {
            this.contents = new DisablePluginsConflictingLayoutPanel();

            doTranslations();

            this.contents.buttonClose.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    doClose();
                }
            });
            this.contents.buttonContinue.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    doContinue();
                }
            });
            this.contents.pluginList.setModel(new DefaultListModel(this.incompatiblePlugins));
            ListSelectionModel sm = this.contents.pluginList.getSelectionModel();
            sm.setSelectionMode(sm.MULTIPLE_INTERVAL_SELECTION);
            this.setContentPane(this.contents);
            this.pack();

            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screenSize.width / 2) - (this.getWidth() / 2),
                    (screenSize.height / 2) - (this.getHeight() / 2));
        }

        private void doTranslations() {
            DisablePluginsConflictingLayoutPanel c = this.contents;
            c.lblConflict.setText(translate("_Some_of_plugins_installed_conflict_with_each_other"));
            c.lblSelectPluginToDisable.setText(translate("_Select_the_plugins_that_you_want_to_disable_and_click_the_continue_button"));
            c.lblClickContinue.setText(translate("_You_can_click_on_continue_button_directly_if_you_dont_want_to_disable_any_plugins"));
            c.lblClickClose.setText(translate("_Or_click_the_close_button_to_close_the_application"));
            c.buttonClose.setText(translate("_Close"));
            c.buttonContinue.setText(translate("_Continue"));
        }

        private String translate(String msg) {
            return PluginServices.getText(this, msg);
        }

        private void doClose() {
            this.action = CLOSE;
            this.setVisible(false);
        }

        private void doContinue() {
            this.action = CONTINUE;
            this.setVisible(false);
        }

        public int getAction() {
            return this.action;
        }

        public List<String> getPluginNamesToDisable() {
            if (this.action == CLOSE) {
                return null;
            }
            Object[] selecteds = null;
            selecteds = (Object[]) this.contents.pluginList.getSelectedValues();
            if (selecteds == null || selecteds.length < 1) {
                return null;
            }
            List<String> values = new ArrayList<String>();
            for (int i = 0; i < selecteds.length; i++) {
                values.add(((Item) selecteds[i]).getCode());
            }
            return values;
        }
    }

    private void initializeIdentityManagement(File pluginsFolder) {
        File identityManagementConfigFile = null;
        PluginServices plugin = null;
        Iterator<Entry<String, PluginConfig>> it = pluginsConfig.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, PluginConfig> entry = it.next();
            File pluginFolder = new File(pluginsFolder, entry.getKey());
            File f = new File(pluginFolder, "identity-management.ini");
            if (f.exists()) {
                if (identityManagementConfigFile != null) {
                    logger.warn("Too many identity-managemnt plugins. Disable all.");
                } else {
                    identityManagementConfigFile = f;
                    plugin = PluginServices.getPluginServices(entry.getKey());
                }
            }
        }
        if(plugin != null){
        	new DefaultLibrariesInitializer(plugin.getClassLoader()).fullInitialize(true);
        }
        
        if (identityManagementConfigFile == null || plugin == null) {
            return;
        }
        if (!identityManagementConfigFile.canRead()) {
            return;
        }
        PropertiesConfiguration identityManagementConfig = null;
        try {
            identityManagementConfig = new PropertiesConfiguration(identityManagementConfigFile);
        } catch (Exception ex) {
            logger.warn("Can't open identity management config file '" + identityManagementConfigFile.getAbsolutePath() + "'.", ex);
            return;
        }
        String identityManagerClassName = identityManagementConfig.getString("IdentityManager", null);
        String identityManagementInitializerClassName = identityManagementConfig.getString("IdentityManagementInitializer", null);
        try {
            if (identityManagerClassName != null) {
                Class identityManagerClass = plugin.getClassLoader().loadClass(identityManagerClassName);
                ToolsLocator.registerIdentityManager(identityManagerClass);
            } else {
                logger.info("Entry IdentityManager not found in identity management config file '" + identityManagementConfigFile.getAbsolutePath() + "'.");
            }

            if (identityManagementInitializerClassName != null) {
                Class identityManagerInitializerClass = plugin.getClassLoader().loadClass(identityManagementInitializerClassName);
                Runnable identityManagerInitializer = (Runnable) identityManagerInitializerClass.newInstance();
                identityManagerInitializer.run();
            } else {
                logger.info("Entry IdentityManagementInitializer not found in identity management config file '" + identityManagementConfigFile.getAbsolutePath() + "'.");
            }

        } catch (Exception ex) {
            logger.warn("Can't initialize the identity manager from '" + identityManagementConfigFile.getAbsolutePath() + ".", ex);
            return;
        }
        logger.info("Loaded an identity manager from plugin '" + plugin.getPluginName() + ".");
    }

}
