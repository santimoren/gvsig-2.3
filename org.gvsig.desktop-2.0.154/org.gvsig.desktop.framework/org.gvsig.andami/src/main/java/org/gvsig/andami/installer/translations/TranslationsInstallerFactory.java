/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.installer.translations;

import java.io.File;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.service.spi.AbstractProviderFactory;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderManager;
import org.gvsig.tools.service.spi.ProviderServices;
import org.gvsig.tools.util.Callable;

public class TranslationsInstallerFactory extends
        AbstractProviderFactory {

    public static final String PROVIDER_NAME = "translations";
    public static final String PROVIDER_DESCRIPTION = "";

    private DynClass dynclass;

    @Override
    protected DynClass createParametersDynClass() {
        if ( dynclass == null ) {
            initialize();
        }
        return dynclass;
    }

    @Override
    protected Provider doCreate(DynObject parameters, ProviderServices services) {
        return new TranslationsInstaller(services);
    }

    public void initialize() {
        if ( dynclass == null ) {
            DynObjectManager dynObjectManager = ToolsLocator
                    .getDynObjectManager();
            dynclass = dynObjectManager.createDynClass(PROVIDER_NAME,
                    PROVIDER_DESCRIPTION);
            dynObjectManager.add(dynclass);
        }
    }

    public static class RegisterTranslationsInstaller implements Callable {

        public Object call() throws Exception {
            InstallerManager installerManager = InstallerLocator.getInstallerManager();
            ProviderManager providerManager = InstallerProviderLocator.getProviderManager();
            PluginsManager pluginsManager = PluginsLocator.getManager();
            
            providerManager.addProviderFactory(new TranslationsInstallerFactory());
            
            File f = new File(pluginsManager.getApplicationFolder(),"i18n");
            installerManager.setDefaultLocalAddonRepository(f,PROVIDER_NAME);
            
            return Boolean.TRUE;
        }
    }
}
