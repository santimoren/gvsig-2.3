/*
 /**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.util.Locale;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;

public class TranslatableButtonHelper {

    public interface TranslatableButton {

        public void setTextKey(String key);

        public void setToolTipKey(String key);

        public void translate();
    }

    private String keyText = null;
    private String keyToolTip = null;
    private String text = null;
    private String tooltip = null;

    private Locale locale = null;
    private static PluginsManager pluginsManager = null;

    public void setText(String keyText) {
        this.keyText = keyText;
    }

    public void setTooltip(String keyTooltip) {
        this.keyToolTip = keyTooltip;
    }

    public String getText() {
        return this.text;
    }

    public String getTooltip() {
        return this.tooltip;
    }

    private PluginsManager getPluginsManager() {
        if ( pluginsManager == null ) {
            pluginsManager = PluginsLocator.getManager();
        }
        return pluginsManager;
    }

    public void translate() {
        if ( this.keyText != null ) {
            this.text = getPluginsManager().translate(this.keyText);
        }
        if ( this.keyToolTip != null ) {
            this.tooltip = getPluginsManager().translate(this.keyToolTip);
        }
        locale = Locale.getDefault();
    }

    public boolean needTranslation() {
        if ( this.locale == null ) {
            return true;
        }
        if ( this.locale.equals(Locale.getDefault()) ) {
            return false;
        }
        return true;
    }
}
