/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtensionHelper {

	private static Logger logger = LoggerFactory.getLogger(ExtensionHelper.class);
	
	public static boolean canQueryByAction(IExtension extension) {
		if( extension instanceof IExtensionQueryByAction ) {
			return ((IExtensionQueryByAction)extension).canQueryByAction();
		}
		return false;
	}
	
	public static boolean isVisible(IExtension extension, String action) {
		if( extension instanceof IExtensionQueryByAction ) {
			return ((IExtensionQueryByAction) extension).isVisible(action);
		}
		return extension.isVisible();
	}

	public static boolean isEnabled(IExtension extension, String action) {
		if( extension instanceof IExtensionQueryByAction ) {
			return ((IExtensionQueryByAction) extension).isEnabled(action);
		}
		return extension.isEnabled();
	}
	
	public static void execute(IExtension extension, String command, Object[] args) {
		if( extension instanceof IExtensionExecuteWithArgs ) {
			((IExtensionExecuteWithArgs) extension).execute(command, args);
			return;
		}
		if( args!= null && args.length>0 ) {
			logger.info("calling execute with args in a extension that not support ("+extension.getClass().getSimpleName()+").");
		}
		extension.execute(command);
	}	
	
}
