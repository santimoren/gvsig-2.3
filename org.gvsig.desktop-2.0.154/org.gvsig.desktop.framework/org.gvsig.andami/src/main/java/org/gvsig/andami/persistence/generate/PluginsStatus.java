/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: PluginsStatus.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.persistence.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class PluginsStatus.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class PluginsStatus implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _plugin
     */
    private org.gvsig.andami.persistence.generate.Plugin _plugin;

    /**
     * Field _toolBars
     */
    private org.gvsig.andami.persistence.generate.ToolBars _toolBars;


      //----------------/
     //- Constructors -/
    //----------------/

    public PluginsStatus() {
        super();
    } //-- com.iver.andami.persistence.generate.PluginsStatus()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'plugin'.
     * 
     * @return the value of field 'plugin'.
     */
    public org.gvsig.andami.persistence.generate.Plugin getPlugin()
    {
        return this._plugin;
    } //-- com.iver.andami.persistence.generate.Plugin getPlugin() 

    /**
     * Returns the value of field 'toolBars'.
     * 
     * @return the value of field 'toolBars'.
     */
    public org.gvsig.andami.persistence.generate.ToolBars getToolBars()
    {
        return this._toolBars;
    } //-- com.iver.andami.persistence.generate.ToolBars getToolBars() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'plugin'.
     * 
     * @param plugin the value of field 'plugin'.
     */
    public void setPlugin(org.gvsig.andami.persistence.generate.Plugin plugin)
    {
        this._plugin = plugin;
    } //-- void setPlugin(com.iver.andami.persistence.generate.Plugin) 

    /**
     * Sets the value of field 'toolBars'.
     * 
     * @param toolBars the value of field 'toolBars'.
     */
    public void setToolBars(org.gvsig.andami.persistence.generate.ToolBars toolBars)
    {
        this._toolBars = toolBars;
    } //-- void setToolBars(com.iver.andami.persistence.generate.ToolBars) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.persistence.generate.PluginsStatus) Unmarshaller.unmarshal(org.gvsig.andami.persistence.generate.PluginsStatus.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
