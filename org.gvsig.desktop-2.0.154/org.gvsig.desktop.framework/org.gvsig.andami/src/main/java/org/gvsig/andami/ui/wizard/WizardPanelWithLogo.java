/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import jwizardcomponent.CancelAction;
import jwizardcomponent.DefaultJWizardComponents;
import jwizardcomponent.FinishAction;
import jwizardcomponent.common.SimpleButtonPanel;

public class WizardPanelWithLogo extends JPanel {
	  DefaultJWizardComponents wizardComponents;

	  JPanel buttonPanel;
	  JLabel statusLabel = new JLabel();

	  ImageIcon logo;

	  public WizardPanelWithLogo(ImageIcon logo) {
	    this.logo = logo;
	    wizardComponents = new DefaultJWizardComponents();
	    init();
	  }

	  private void init() {
	    

	    JPanel logoPanel = new JPanel();

	    String fileString;
	    if (logo.toString().indexOf("file:") < 0 &&
	        logo.toString().indexOf("http:") < 0) {
	      fileString = "file:///" +System.getProperty("user.dir") +"/"
	                        +logo.toString();
	      fileString = fileString.replaceAll("\\\\", "/");
	    } else {
	      fileString = logo.toString();
	    }
	    logoPanel.add(new JLabel(logo));
	    logoPanel.setBackground(Color.WHITE);
	    this.setLayout(new BorderLayout());
	    this.add(logoPanel, BorderLayout.WEST);
	    this.add(wizardComponents.getWizardPanelsContainer(),
	    							BorderLayout.CENTER);

	    JPanel auxPanel = new JPanel(new BorderLayout());
	    auxPanel.add(new JSeparator(), BorderLayout.NORTH);

	    buttonPanel = new SimpleButtonPanel(wizardComponents);
	    auxPanel.add(buttonPanel);
	    this.add(auxPanel, BorderLayout.SOUTH);
	    

	    wizardComponents.setFinishAction(new FinishAction(wizardComponents) {
	      public void performAction() {
	        // dispose();
	      }
	    });
	    wizardComponents.setCancelAction(new CancelAction(wizardComponents) {
	      public void performAction() {
	        // dispose();
	      }
	    });
	  }

	  public DefaultJWizardComponents getWizardComponents(){
	    return wizardComponents;
	  }

	  public void setWizardComponents(DefaultJWizardComponents aWizardComponents){
	    wizardComponents = aWizardComponents;
	  }

	  public void show() {
	    wizardComponents.updateComponents();
	    super.setVisible(true);
	  }

}
