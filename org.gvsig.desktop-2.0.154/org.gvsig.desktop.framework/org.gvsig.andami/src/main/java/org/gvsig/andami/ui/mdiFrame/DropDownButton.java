package org.gvsig.andami.ui.mdiFrame;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicArrowButton;
import org.apache.commons.lang3.BooleanUtils;

public class DropDownButton extends JButton {

    private final List<Action> actions = new ArrayList<Action>();
    private Action currentAction = null;
    private final JButton mainButton;
    private final JButton dropDownButton;
    private JPopupMenu dropDownMenu;
    private boolean showText = false;

    public DropDownButton() {
        super();
        this.mainButton = new JButton();
        this.dropDownMenu = null;

        this.dropDownButton = new BasicArrowButton(SwingConstants.SOUTH);
        dropDownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                doToggleDropDownMenu();
            }
        });

        this.setBorder(BorderFactory.createEmptyBorder());
        this.dropDownButton.setBorder(BorderFactory.createEmptyBorder());
        this.mainButton.setBorder(BorderFactory.createEmptyBorder());
        this.setLayout(new BorderLayout());
        this.add(mainButton, BorderLayout.CENTER);
        this.add(dropDownButton, BorderLayout.EAST);

        this.mainButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (currentAction != null) {
                    currentAction.actionPerformed(ae);
                }
            }
        });

    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
    }

    public void add(final Action action) {
        this.actions.add(action);
        if (this.actions.size() == 1) {
            setMainButton(action);
        }
    }

    private void setMainButton(Action action){
        mainButton.setIcon((Icon) action.getValue(Action.SMALL_ICON));
        if (showText) {
            mainButton.setText((String) action.getValue(Action.SHORT_DESCRIPTION));
        } else {
            mainButton.setText("");
        }
        currentAction = action;
    }

    public void updateMainButton(){
        for (Action action : actions) {
            if (action.isEnabled()){
                setMainButton(action);
                return;
            }
        }
    }

    public boolean isAllHiden() {
        for (Action action : actions) {
            Boolean isVisible = (Boolean) action.getValue("VISIBLE");
            if( isVisible==null ) {
                return false;
            }
            if( isVisible.booleanValue() ) {
                return false;
            }
        }
        return true;
    }


    public boolean isAllDisabled() {
        this.mainButton.setEnabled(this.currentAction.isEnabled());
        for (Action action : actions) {
            if( action.isEnabled() ) {
                return false;
            }
        }
        return true;
    }

    private void createDropDownMenu() {
        this.dropDownMenu = null;
        for (Action action : actions) {
            Boolean isVisible = (Boolean) action.getValue("VISIBLE");
            if( !BooleanUtils.isTrue(isVisible) ) {
                continue;
            }

            JMenuItem mi = new JMenuItem(
                    (String) action.getValue(Action.SHORT_DESCRIPTION),
                    (Icon) action.getValue(Action.SMALL_ICON)
            );
            mi.setToolTipText((String) action.getValue(Action.SHORT_DESCRIPTION));
            mi.addActionListener(new MyItemActionListener(action));
            mi.setEnabled(action.isEnabled());
            if( this.dropDownMenu==null ) {
                this.dropDownMenu = new JPopupMenu();
                this.dropDownMenu.addFocusListener(new FocusListener() {
                    public void focusGained(FocusEvent fe) {
                    }
                    public void focusLost(FocusEvent fe) {
                        if( dropDownMenu!=null ) {
                            dropDownMenu.setVisible(false);
                        }
                    }
                });
            }
            this.dropDownMenu.add(mi);
        }
    }

    private class MyItemActionListener implements ActionListener {
        private Action action = null;
        MyItemActionListener(Action action) {
            this.action = action;
        }

        public void actionPerformed(ActionEvent ae) {
            mainButton.setIcon((Icon) action.getValue(Action.SMALL_ICON));
            if (showText) {
                mainButton.setText((String) action.getValue(Action.SHORT_DESCRIPTION));
            } else {
                mainButton.setText("");
            }
            currentAction = action;
            dropDownMenu.setVisible(false);
            mainButton.setEnabled(true);
            action.actionPerformed(ae);
        }
    }

    private void doToggleDropDownMenu() {
        if (dropDownMenu==null || !dropDownMenu.isVisible()) {
            createDropDownMenu();
            dropDownMenu.show(this, 0,this.getHeight());
        } else {
            dropDownMenu.setVisible(false);
        }
    }

    public void setShowText(boolean showText) {
        this.showText = showText;
    }

    public boolean isShowText() {
        return showText;
    }

}
