/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins;

import java.util.Map;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extensions are the way in which plugins extend Andami. Extensions
 * can add controls to user interface (GUI) and execute some code
 * when controls are activated. Every class implementing
 * {@link IExtension} is an extension, but directly implementing
 * that interface is discouraged. The preferred way to create
 * an extension is extending this absctract class.
 *
 * @see IExtension
 *
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public abstract class Extension implements IExtension, IExtensionQueryByAction, IExtensionExecuteWithArgs  {

	private static Logger logger = LoggerFactory.getLogger(Extension.class);

	private PluginServices plugin = null;

	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.IExtension#terminate()
	 */
	public void terminate(){

	}
	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.IExtension#postInitialize()
	 */
	public void postInitialize(){

	}

    /*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.IExtension#getStatus()
	 */
    public IExtensionStatus getStatus() {
    	return null;
    }

	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.IExtension#getStatus(IExtension extension)
	 */
	public IExtensionStatus getStatus(IExtension extension) {
		return extension.getStatus();
	}

	public String getText(String msg) {
	    return PluginsLocator.getManager().getText(this, msg);
	}

	public void setPlugin(PluginServices plugin) {
		if (this.plugin != null){
			throw new IllegalStateException();
		}
		this.plugin = plugin;
	}

	public PluginServices getPlugin() {
		if (this.plugin == null){
			return PluginsLocator.getManager().getPlugin(this.getClass());
		}
		return this.plugin;
	}

	public boolean isEnabled(String action) {
		return isEnabled();
	}

	public boolean isVisible(String action) {
		return isVisible();
	}

	public boolean canQueryByAction() {
		return false;
	}

	public void execute(String command, Object[] args) {
		if( args!= null && args.length>0 ) {
			logger.info("calling execute with args in a extension that not support ("+this.getClass().getSimpleName()+").");
		}
		execute(command);
	}

}

