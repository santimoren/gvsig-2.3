package org.gvsig.andami;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.Set;

public interface LocaleManager {

    public static final Locale SPANISH = new Locale("es");

    public static final Locale ENGLISH = new Locale("en");

    public Set<Locale> getDefaultLocales();

    public Set<Locale> getInstalledLocales();

    public Locale getDefaultSystemLocale();

    public Locale getCurrentLocale();

    public void setCurrentLocale(Locale locale);

    public boolean installLocale(Locale locale);

    public boolean uninstallLocale(Locale locale);

    public boolean installLocales(URL localesFile);

    public File getResourcesFolder();

    public Locale[] getLocaleAlternatives(Locale locale);

    public String getLanguageDisplayName(Locale locale);

    public String getLocaleDisplayName(Locale locale);

    public Locale getNearestLocale(Locale locale);
}
