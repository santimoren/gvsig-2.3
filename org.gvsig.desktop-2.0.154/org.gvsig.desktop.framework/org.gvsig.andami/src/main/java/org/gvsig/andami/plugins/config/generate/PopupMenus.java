/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: PopupMenus.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PopupMenus.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class PopupMenus implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _popupMenuList
     */
    private java.util.Vector _popupMenuList;


      //----------------/
     //- Constructors -/
    //----------------/

    public PopupMenus() {
        super();
        _popupMenuList = new Vector();
    } //-- com.iver.andami.plugins.config.generate.PopupMenus()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addPopupMenu
     * 
     * @param vPopupMenu
     */
    public void addPopupMenu(org.gvsig.andami.plugins.config.generate.PopupMenu vPopupMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _popupMenuList.addElement(vPopupMenu);
    } //-- void addPopupMenu(com.iver.andami.plugins.config.generate.PopupMenu) 

    /**
     * Method addPopupMenu
     * 
     * @param index
     * @param vPopupMenu
     */
    public void addPopupMenu(int index, org.gvsig.andami.plugins.config.generate.PopupMenu vPopupMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _popupMenuList.insertElementAt(vPopupMenu, index);
    } //-- void addPopupMenu(int, com.iver.andami.plugins.config.generate.PopupMenu) 

    /**
     * Method enumeratePopupMenu
     */
    public java.util.Enumeration enumeratePopupMenu()
    {
        return _popupMenuList.elements();
    } //-- java.util.Enumeration enumeratePopupMenu() 

    /**
     * Method getPopupMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.PopupMenu getPopupMenu(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _popupMenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.PopupMenu) _popupMenuList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.PopupMenu getPopupMenu(int) 

    /**
     * Method getPopupMenu
     */
    public org.gvsig.andami.plugins.config.generate.PopupMenu[] getPopupMenu()
    {
        int size = _popupMenuList.size();
        org.gvsig.andami.plugins.config.generate.PopupMenu[] mArray = new org.gvsig.andami.plugins.config.generate.PopupMenu[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.PopupMenu) _popupMenuList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.PopupMenu[] getPopupMenu() 

    /**
     * Method getPopupMenuCount
     */
    public int getPopupMenuCount()
    {
        return _popupMenuList.size();
    } //-- int getPopupMenuCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllPopupMenu
     */
    public void removeAllPopupMenu()
    {
        _popupMenuList.removeAllElements();
    } //-- void removeAllPopupMenu() 

    /**
     * Method removePopupMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.PopupMenu removePopupMenu(int index)
    {
        java.lang.Object obj = _popupMenuList.elementAt(index);
        _popupMenuList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.PopupMenu) obj;
    } //-- com.iver.andami.plugins.config.generate.PopupMenu removePopupMenu(int) 

    /**
     * Method setPopupMenu
     * 
     * @param index
     * @param vPopupMenu
     */
    public void setPopupMenu(int index, org.gvsig.andami.plugins.config.generate.PopupMenu vPopupMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _popupMenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _popupMenuList.setElementAt(vPopupMenu, index);
    } //-- void setPopupMenu(int, com.iver.andami.plugins.config.generate.PopupMenu) 

    /**
     * Method setPopupMenu
     * 
     * @param popupMenuArray
     */
    public void setPopupMenu(org.gvsig.andami.plugins.config.generate.PopupMenu[] popupMenuArray)
    {
        //-- copy array
        _popupMenuList.removeAllElements();
        for (int i = 0; i < popupMenuArray.length; i++) {
            _popupMenuList.addElement(popupMenuArray[i]);
        }
    } //-- void setPopupMenu(com.iver.andami.plugins.config.generate.PopupMenu) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.PopupMenus) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.PopupMenus.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
