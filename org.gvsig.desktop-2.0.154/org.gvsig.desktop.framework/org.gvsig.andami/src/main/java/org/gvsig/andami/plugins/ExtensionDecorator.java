/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins;

import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class extends the functionality of Extension class to let the programmer
 * set an extension visible or not on-the-fly.
 *
 * @autor Jaume Dominguez Faus - jaume.dominguez@iver.es
 */
public class ExtensionDecorator implements HiddableExtension{
    private static Logger logger = LoggerFactory.getLogger(ExtensionDecorator.class);
	public static final int INACTIVE = 0;
	public static final int ALWAYS_VISIBLE = 1;
	public static final int ALWAYS_INVISIBLE = 2;
	int  alwaysVisible;
	IExtension extension;

	public ExtensionDecorator(IExtension e, int visibilityControl){
		setExtension(e);
		setVisibility(visibilityControl);
	}

	public void setExtension(IExtension e) {
		this.extension = e;
	}

	public void setVisibility(int state){
		this.alwaysVisible = state;
	}

	public int getVisibility(){
		return alwaysVisible;
	}

	public IExtension getExtension(){
		return extension;
	}

	public void initialize() {
		extension.initialize();
	}

	public void terminate(){
		//TODO
	}

	public void execute(String actionCommand) {
        logger.info("{}.execute('{}')", extension.getClass().getSimpleName(), actionCommand);
		extension.execute(actionCommand);
	}

	public boolean isEnabled() {
		return extension.isEnabled();
	}

	public boolean isVisible() {
		if (alwaysVisible == INACTIVE)
			return extension.isVisible();
		else if (alwaysVisible == ALWAYS_VISIBLE) return true;
		else return false;
	}

	public void postInitialize() {
		// TODO
	}

	/* (non-Javadoc)
	 * @see com.iver.andami.plugins.IExtension#getStatus()
	 */
	public IExtensionStatus getStatus() {
		return extension.getStatus();
	}
	public IExtensionStatus getStatus(IExtension extension) {
		return this.extension.getStatus(extension);
	}
}
