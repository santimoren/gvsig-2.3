/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.actioninfo;

import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

import org.gvsig.tools.lang.Cloneable;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.IExtension;

/**
 * Esta entidad representa a una accion dentro de gvSIG, que podra ser usada
 * por menus, botones de la toolbar, menus contextuales, o desde cualquier
 * otro elemento de gvSIG que precise invocar a una accion.
 * 
 * La accion esta compuesta por una definicion de cual es su etiqueta, su
 * icono, teclas aceleradoras, su tip, o su posicion respecto a otras acciones, 
 * junto con una extension (IExtension) que aporta el codigo de la accion.
 * 
 *  Una accion responde principalmente a cuatro operaciones:
 *  
 *  - isActive, que nos indica si la accion esta activa, es decir, si 
 *    debera presentarse al usuario o no. Si una extension no esta activa,
 *    los metodos isVisible y isEnabled retornaran siempre false, sin invocar
 *    al codigo de la extension asociada a la accion.
 *    
 *  - isVisible, que nos indica si la accion debera presentarse o no en el
 *    interface de usuario. De forma general podemos asumir que cuando se
 *    invoque a este metodo se delegara en el metodo isVisible de la extension
 *    asociada a la accion.
 *    
 *  - isEnabled, que nos indica si la accion debera estar habilitada o no en 
 *    el interface de usuario. De forma general podemos asumir que cuando se
 *    invoque a este metodo se delegara en el metodo isEnabled de la extension
 *    asociada a la accion.
 *    
 *  - execute, que provocara que se ejecute el codigo asociado a la accion.  
 *    De forma general podemos asumir que cuando se invoque a estos metodo se 
 *    delegara en el metodo execute de la extension asociada a esta accion, pasandole
 *    el "command" de la accion como parametro.
 *    
 * Ademas de la definicion de la accion, esta tambien puede disponer de una coleccion
 * de acciones hacia las que puede ser redirigida. Esto es, en un momento dado
 * nos puede interesar que cuando sea invocada la accion "A", en lugar de ejecutarse
 * las operaciones de esta, se ejecuten las operaciones de otra accion "B". Esto nos
 * permite atrapar la ejecucion de una accion independientemente de desde donde se este 
 * invocando. Cuando una accion tenga asignadas otras acciones a las que redirigir
 * su ejecucion, solo se redigira a una de ellas, la primera en la coleccion de acciones a
 * redirigir que responda "true" a su metodo isEnabled, y en caso de que no responda true
 * ninguna se ejecutara el codigo de la accion original. El orden de la coleccion de
 * acciones a las que redirigir de una accion sera el orden inverso en el que se han
 * ido registrandose las redirecciones. Asi primero se intentara con la ultima redireccion
 * asignada, luego con la anterior, y asi sucesivamente se recorreran todas las redireccion
 * hasta que una responda "true" a su isEnabled, ejecutandose entonces el codigo de esta.
 * 
 * 
 * @author jjdelcerro
 *
 */
public interface ActionInfo extends Action, ActionListener, Cloneable {
	
	public static final String ACCELERATOR = "ACCELERATOR";
	public static final String ACTIVE = "ACTIVE"; 
	public static final String VISIBLE = "VISIBLE";
	public static final String PLUGIN_NAME = "PLUGIN_NAME";
	public static final String PLUGIN = "PLUGIN";
	public static final String EXTENSION_NAME = "EXTENSION_NAME";
	public static final String EXTENSION = "EXTENSION";
	public static final String REDIRECTIONS = "REDIRECTIONS";
	public static final String REDIRECTION = "REDIRECTION";
	public static final String POSITION = "POSITION";
	public static final String TOOLTIP = "TOOLTIP";
	public static final String ICON_NAME = "ICON_NAME";

	/**
	 * returns the plugin of the extension asociated to the action.
	 * 
	 * @return pluginServices asiciated to the action.
	 */
	public PluginServices getPlugin();
	
	/**
	 * returns the extension asociated to the action. The action 
	 * delegates the methods isEnabled, isVisible and execute in this
	 * extension when these are invoked.
	 * 
	 * @return IExtension asociated to the action
	 */
	public IExtension getExtension();
	
	/**
	 * Return the plugin name of the plugin asociated to the action.
	 * This is a utility method checkins null values. is equivalent to:
	 * 
	 *   action.getPlugin().getPluginName()
	 * 
	 * 
	 * @return plugin name
	 * @see #getPlugin()
	 */
	public String getPluginName();
	
	/**
	 * Returns the extension name of the extension asociated to this action.
	 * This a utility method that check null values. Is equivalent to:
	 * 
	 *   action.getExtension().getClass().getName()
	 *   
	 * @return extension name.
	 * @see #getExtension()
	 */
	public String getExtensionName();
	
	/**
	 * Returns the name of the action. This name is usaed to retrieve the 
	 * action thwros the manager.
	 * 
	 * @return action name
	 */
	public String getName();
	
	/**
	 * Return a label asociated to the action. This label can be used
	 * in buttons, o menus.
	 * 
	 * @return label of action
	 */
	public String getLabel();
	
	/**
	 * Returns the command used for invoking the execute method of
	 * the extension asociated to this action.
	 * 
	 * @return command of action
	 */
	public String getCommand();
	
	/**
	 * Return an icon asociated to the action. This icon can be used
	 * in buttons, o menus.
	 * 
	 * @return ImageIcon asociated tho the action
	 */
	public ImageIcon getIcon();
	
	/**
	 * Returns the name of icon asociated to the action. This name is
	 * used to retrive the icon from the current icon theme of the application.
	 *   
	 * @return icon name.
	 */
	public String getIconName();
	
	/**
	 * returns a representation human readable of the accelerator to be used 
	 * associated to the action.
	 * 
	 * @return String representing the accelerator
	 */
	public String getAccelerator();
	
	/**
	 * returns the KeyStroke which represents the accelerator of this
	 * action.
	 * 
	 * @return keystroke asociated to this action
	 * @see #getAccelerator()
	 */
	public KeyStroke getKeyStroke();
	
	/**
	 * Return a string that represents a tip asociated whit the action,
	 * usually used as tooltip in buttons or menus.
	 * 
	 * @return the tip of the action
	 */
	public String getTooltip();
	
	/**
	 * Return the position absolute of the action referred to all actions.
	 * 
	 * @return the position of the action
	 */
	public long getPosition();
	
	/**
	 * retrurn if the action can be visible in the user interface or not.
	 * This method call the isVisible of the extension asociated to the action,
	 * unless the action is inactive.
	 * If has a ExclusiveUIExtension set, then this is invoqued instead of the
	 * the isVisible of the extension.
	 *  
	 * @return if the action if visible for the user.
	 */
	public boolean isVisible();
	
	/**
	 * retrurn if the action is enables.
	 * This method call the isEnabled of the extension asociated to the action,
	 * unless the action is inactive.
	 * This method is used to determine whether it is possible to redirect 
	 * to this action or not.
	 * If has a ExclusiveUIExtension set, then this is invoqued instead of the
	 * the isEnabled of the extension.
	 *  
	 * @return if the action if visible for the user.
	 */
	public boolean isEnabled();
	
	/**
	 * Execute the code asociated to the action.
	 * This method call the execute method of the asociated exetnsion using the
	 * command of action as argument.
	 * If the action is redirected try to call to the redirected actions.
	 *  
	 */
	public void execute();

	/**
	 * Execute the code asociated to the action.
	 * Pass the args to the execute of the asociated extension.
	 *
	 *  @see #execute()
	 */
	public void execute(Object[] args);
	public void execute(Map args);

	/**
	 * Execute the code asociated to the action.
	 * Pass the args to the execute of the asociated extension.
	 *
	 *  @see #execute()
	 */
	public void execute(Object arg);
	
	/**
	 * Return true is the action is active. When an action is active the methods
	 * isEnable and isVisible call the methods of the extension asociated to this.
	 * When is inactive always return false.
	 * 
	 * @return if the action is active
	 */
	public boolean isActive();
	/**
	 * Set the active state of an ActionInfo.
	 * When the active state is set to false, isEnabled, and isVisible
	 * returns false.
	 * 
	 * @param active
	 */
	public void setActive(boolean active);
	
	/**
	 * 
	 * An action can redirect the execution of the execute, isVisible and isEnabled methods
	 * to other action. Using this method is can be query and set this redirections.
	 * The redirect will be established only if the method isEnabled of target action
	 * returns true. Otherwise execute methods of initial action.
	 * 
	 * @return the redirections established for this action
	 */
	public Collection<ActionInfo> getRedirections();
	
}
