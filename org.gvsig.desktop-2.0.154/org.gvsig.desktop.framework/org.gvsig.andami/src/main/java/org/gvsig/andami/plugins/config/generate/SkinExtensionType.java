/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: SkinExtensionType.java 38564 2012-07-16 11:19:13Z jjdelcerro $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class SkinExtensionType.
 * 
 * @version $Revision: 38564 $ $Date: 2012-07-16 13:19:13 +0200 (lun, 16 jul 2012) $
 */
public class SkinExtensionType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _className
     */
    private java.lang.String _className;

    /**
     * Field _menuList
     */
    private java.util.Vector _menuList;

    /**
     * Field _actionList
     */
    private java.util.Vector _actionList;

    /**
     * Field _toolBarList
     */
    private java.util.Vector _toolBarList;

    /**
     * Field _comboButtonList
     */
    private java.util.Vector _comboButtonList;

    /**
     * Field _comboScaleList
     */
    private java.util.Vector _comboScaleList;


      //----------------/
     //- Constructors -/
    //----------------/

    public SkinExtensionType() {
        super();
        _menuList = new Vector();
        _actionList = new Vector();
        _toolBarList = new Vector();
        _comboButtonList = new Vector();
        _comboScaleList = new Vector();
    } //-- com.iver.andami.plugins.config.generate.SkinExtensionType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addComboButton
     * 
     * @param vComboButton
     */
    public void addComboButton(org.gvsig.andami.plugins.config.generate.ComboButton vComboButton)
        throws java.lang.IndexOutOfBoundsException
    {
        _comboButtonList.addElement(vComboButton);
    } //-- void addComboButton(com.iver.andami.plugins.config.generate.ComboButton) 

    /**
     * Method addComboButton
     * 
     * @param index
     * @param vComboButton
     */
    public void addComboButton(int index, org.gvsig.andami.plugins.config.generate.ComboButton vComboButton)
        throws java.lang.IndexOutOfBoundsException
    {
        _comboButtonList.insertElementAt(vComboButton, index);
    } //-- void addComboButton(int, com.iver.andami.plugins.config.generate.ComboButton) 

    /**
     * Method addComboScale
     * 
     * @param vComboScale
     */
    public void addComboScale(org.gvsig.andami.plugins.config.generate.ComboScale vComboScale)
        throws java.lang.IndexOutOfBoundsException
    {
        _comboScaleList.addElement(vComboScale);
    } //-- void addComboScale(com.iver.andami.plugins.config.generate.ComboScale) 

    /**
     * Method addComboScale
     * 
     * @param index
     * @param vComboScale
     */
    public void addComboScale(int index, org.gvsig.andami.plugins.config.generate.ComboScale vComboScale)
        throws java.lang.IndexOutOfBoundsException
    {
        _comboScaleList.insertElementAt(vComboScale, index);
    } //-- void addComboScale(int, com.iver.andami.plugins.config.generate.ComboScale) 

    /**
     * Method addMenu
     * 
     * @param vMenu
     */
    public void addMenu(org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _menuList.addElement(vMenu);
    } //-- void addMenu(com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method addMenu
     * 
     * @param index
     * @param vMenu
     */
    public void addMenu(int index, org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _menuList.insertElementAt(vMenu, index);
    } //-- void addMenu(int, com.iver.andami.plugins.config.generate.Menu) 


    /**
     * Method addAction
     * 
     * @param vAction
     */
    public void addAction(org.gvsig.andami.plugins.config.generate.Action vAction)
        throws java.lang.IndexOutOfBoundsException
    {
        _actionList.addElement(vAction);
    } //-- void addAction(com.iver.andami.plugins.config.generate.Action) 

    /**
     * Method addAction
     * 
     * @param index
     * @param vAction
     */
    public void addAction(int index, org.gvsig.andami.plugins.config.generate.Action vAction)
        throws java.lang.IndexOutOfBoundsException
    {
        _actionList.insertElementAt(vAction, index);
    } //-- void addAction(int, com.iver.andami.plugins.config.generate.Action) 

    /**
     * Method addToolBar
     * 
     * @param vToolBar
     */
    public void addToolBar(org.gvsig.andami.plugins.config.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        _toolBarList.addElement(vToolBar);
    } //-- void addToolBar(com.iver.andami.plugins.config.generate.ToolBar) 

    /**
     * Method addToolBar
     * 
     * @param index
     * @param vToolBar
     */
    public void addToolBar(int index, org.gvsig.andami.plugins.config.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        _toolBarList.insertElementAt(vToolBar, index);
    } //-- void addToolBar(int, com.iver.andami.plugins.config.generate.ToolBar) 

    /**
     * Method enumerateComboButton
     */
    public java.util.Enumeration enumerateComboButton()
    {
        return _comboButtonList.elements();
    } //-- java.util.Enumeration enumerateComboButton() 

    /**
     * Method enumerateComboScale
     */
    public java.util.Enumeration enumerateComboScale()
    {
        return _comboScaleList.elements();
    } //-- java.util.Enumeration enumerateComboScale() 

    /**
     * Method enumerateMenu
     */
    public java.util.Enumeration enumerateMenu()
    {
        return _menuList.elements();
    } //-- java.util.Enumeration enumerateMenu() 

    /**
     * Method enumerateAction
     */
    public java.util.Enumeration enumerateAction()
    {
        return _actionList.elements();
    } //-- java.util.Enumeration enumerateAction() 

    /**
     * Method enumerateToolBar
     */
    public java.util.Enumeration enumerateToolBar()
    {
        return _toolBarList.elements();
    } //-- java.util.Enumeration enumerateToolBar() 

    /**
     * Returns the value of field 'className'.
     * 
     * @return the value of field 'className'.
     */
    public java.lang.String getClassName()
    {
        return this._className;
    } //-- java.lang.String getClassName() 

    /**
     * Method getComboButton
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ComboButton getComboButton(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _comboButtonList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.ComboButton) _comboButtonList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.ComboButton getComboButton(int) 

    /**
     * Method getComboButton
     */
    public org.gvsig.andami.plugins.config.generate.ComboButton[] getComboButton()
    {
        int size = _comboButtonList.size();
        org.gvsig.andami.plugins.config.generate.ComboButton[] mArray = new org.gvsig.andami.plugins.config.generate.ComboButton[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.ComboButton) _comboButtonList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.ComboButton[] getComboButton() 

    /**
     * Method getComboButtonCount
     */
    public int getComboButtonCount()
    {
        return _comboButtonList.size();
    } //-- int getComboButtonCount() 

    /**
     * Method getComboScale
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ComboScale getComboScale(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _comboScaleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.ComboScale) _comboScaleList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.ComboScale getComboScale(int) 

    /**
     * Method getComboScale
     */
    public org.gvsig.andami.plugins.config.generate.ComboScale[] getComboScale()
    {
        int size = _comboScaleList.size();
        org.gvsig.andami.plugins.config.generate.ComboScale[] mArray = new org.gvsig.andami.plugins.config.generate.ComboScale[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.ComboScale) _comboScaleList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.ComboScale[] getComboScale() 

    /**
     * Method getComboScaleCount
     */
    public int getComboScaleCount()
    {
        return _comboScaleList.size();
    } //-- int getComboScaleCount() 

    /**
     * Method getMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Menu getMenu(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _menuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.Menu) _menuList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Menu getMenu(int) 

    /**
     * Method getMenu
     */
    public org.gvsig.andami.plugins.config.generate.Menu[] getMenu()
    {
        int size = _menuList.size();
        org.gvsig.andami.plugins.config.generate.Menu[] mArray = new org.gvsig.andami.plugins.config.generate.Menu[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.Menu) _menuList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.Menu[] getMenu() 
    

    /**
     * Method getMenuCount
     */
    public int getMenuCount()
    {
        return _menuList.size();
    } //-- int getMenuCount() 

    /**
     * Method getAction
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Action getAction(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _actionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.Action) _actionList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.Action getAction(int) 

    /**
     * Method getAction
     */
    public org.gvsig.andami.plugins.config.generate.Action[] getAction()
    {
        int size = _actionList.size();
        org.gvsig.andami.plugins.config.generate.Action[] mArray = new org.gvsig.andami.plugins.config.generate.Action[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.Action) _actionList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.Action[] getAction() 

    /**
     * Method getActionCount
     */
    public int getActionCount()
    {
        return _actionList.size();
    } //-- int getActionCount() 

    /**
     * Method getToolBar
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ToolBar getToolBar(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _toolBarList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (org.gvsig.andami.plugins.config.generate.ToolBar) _toolBarList.elementAt(index);
    } //-- com.iver.andami.plugins.config.generate.ToolBar getToolBar(int) 

    /**
     * Method getToolBar
     */
    public org.gvsig.andami.plugins.config.generate.ToolBar[] getToolBar()
    {
        int size = _toolBarList.size();
        org.gvsig.andami.plugins.config.generate.ToolBar[] mArray = new org.gvsig.andami.plugins.config.generate.ToolBar[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (org.gvsig.andami.plugins.config.generate.ToolBar) _toolBarList.elementAt(index);
        }
        return mArray;
    } //-- com.iver.andami.plugins.config.generate.ToolBar[] getToolBar() 

    /**
     * Method getToolBarCount
     */
    public int getToolBarCount()
    {
        return _toolBarList.size();
    } //-- int getToolBarCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllComboButton
     */
    public void removeAllComboButton()
    {
        _comboButtonList.removeAllElements();
    } //-- void removeAllComboButton() 

    /**
     * Method removeAllComboScale
     */
    public void removeAllComboScale()
    {
        _comboScaleList.removeAllElements();
    } //-- void removeAllComboScale() 

    /**
     * Method removeAllMenu
     */
    public void removeAllMenu()
    {
        _menuList.removeAllElements();
    } //-- void removeAllMenu() 


    /**
     * Method removeAllAction
     */
    public void removeAllAction()
    {
        _actionList.removeAllElements();
    } //-- void removeAllAction() 

    /**
     * Method removeAllToolBar
     */
    public void removeAllToolBar()
    {
        _toolBarList.removeAllElements();
    } //-- void removeAllToolBar() 

    /**
     * Method removeComboButton
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ComboButton removeComboButton(int index)
    {
        java.lang.Object obj = _comboButtonList.elementAt(index);
        _comboButtonList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.ComboButton) obj;
    } //-- com.iver.andami.plugins.config.generate.ComboButton removeComboButton(int) 

    /**
     * Method removeComboScale
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ComboScale removeComboScale(int index)
    {
        java.lang.Object obj = _comboScaleList.elementAt(index);
        _comboScaleList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.ComboScale) obj;
    } //-- com.iver.andami.plugins.config.generate.ComboScale removeComboScale(int) 

    /**
     * Method removeMenu
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.Menu removeMenu(int index)
    {
        java.lang.Object obj = _menuList.elementAt(index);
        _menuList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.Menu) obj;
    } //-- com.iver.andami.plugins.config.generate.Menu removeMenu(int) 

    /**
     * Method removeToolBar
     * 
     * @param index
     */
    public org.gvsig.andami.plugins.config.generate.ToolBar removeToolBar(int index)
    {
        java.lang.Object obj = _toolBarList.elementAt(index);
        _toolBarList.removeElementAt(index);
        return (org.gvsig.andami.plugins.config.generate.ToolBar) obj;
    } //-- com.iver.andami.plugins.config.generate.ToolBar removeToolBar(int) 

    /**
     * Sets the value of field 'className'.
     * 
     * @param className the value of field 'className'.
     */
    public void setClassName(java.lang.String className)
    {
        this._className = className;
    } //-- void setClassName(java.lang.String) 

    /**
     * Method setComboButton
     * 
     * @param index
     * @param vComboButton
     */
    public void setComboButton(int index, org.gvsig.andami.plugins.config.generate.ComboButton vComboButton)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _comboButtonList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _comboButtonList.setElementAt(vComboButton, index);
    } //-- void setComboButton(int, com.iver.andami.plugins.config.generate.ComboButton) 

    /**
     * Method setComboButton
     * 
     * @param comboButtonArray
     */
    public void setComboButton(org.gvsig.andami.plugins.config.generate.ComboButton[] comboButtonArray)
    {
        //-- copy array
        _comboButtonList.removeAllElements();
        for (int i = 0; i < comboButtonArray.length; i++) {
            _comboButtonList.addElement(comboButtonArray[i]);
        }
    } //-- void setComboButton(com.iver.andami.plugins.config.generate.ComboButton) 

    /**
     * Method setComboScale
     * 
     * @param index
     * @param vComboScale
     */
    public void setComboScale(int index, org.gvsig.andami.plugins.config.generate.ComboScale vComboScale)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _comboScaleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _comboScaleList.setElementAt(vComboScale, index);
    } //-- void setComboScale(int, com.iver.andami.plugins.config.generate.ComboScale) 

    /**
     * Method setComboScale
     * 
     * @param comboScaleArray
     */
    public void setComboScale(org.gvsig.andami.plugins.config.generate.ComboScale[] comboScaleArray)
    {
        //-- copy array
        _comboScaleList.removeAllElements();
        for (int i = 0; i < comboScaleArray.length; i++) {
            _comboScaleList.addElement(comboScaleArray[i]);
        }
    } //-- void setComboScale(com.iver.andami.plugins.config.generate.ComboScale) 

    /**
     * Method setMenu
     * 
     * @param index
     * @param vMenu
     */
    public void setMenu(int index, org.gvsig.andami.plugins.config.generate.Menu vMenu)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _menuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _menuList.setElementAt(vMenu, index);
    } //-- void setMenu(int, com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method setMenu
     * 
     * @param menuArray
     */
    public void setMenu(org.gvsig.andami.plugins.config.generate.Menu[] menuArray)
    {
        //-- copy array
        _menuList.removeAllElements();
        for (int i = 0; i < menuArray.length; i++) {
            _menuList.addElement(menuArray[i]);
        }
    } //-- void setMenu(com.iver.andami.plugins.config.generate.Menu) 

    /**
     * Method setToolBar
     * 
     * @param index
     * @param vToolBar
     */
    public void setToolBar(int index, org.gvsig.andami.plugins.config.generate.ToolBar vToolBar)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _toolBarList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _toolBarList.setElementAt(vToolBar, index);
    } //-- void setToolBar(int, com.iver.andami.plugins.config.generate.ToolBar) 

    /**
     * Method setToolBar
     * 
     * @param toolBarArray
     */
    public void setToolBar(org.gvsig.andami.plugins.config.generate.ToolBar[] toolBarArray)
    {
        //-- copy array
        _toolBarList.removeAllElements();
        for (int i = 0; i < toolBarArray.length; i++) {
            _toolBarList.addElement(toolBarArray[i]);
        }
    } //-- void setToolBar(com.iver.andami.plugins.config.generate.ToolBar) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.SkinExtensionType) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.SkinExtensionType.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
