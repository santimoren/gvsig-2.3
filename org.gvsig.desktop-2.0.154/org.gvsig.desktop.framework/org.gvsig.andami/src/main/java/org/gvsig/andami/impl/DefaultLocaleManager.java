package org.gvsig.andami.impl;

import java.io.File;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.LocaleManager;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.config.generate.AndamiConfig;
import org.gvsig.andami.installer.translations.TranslationsInstallerFactory;
import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.utils.XMLEntity;

public class DefaultLocaleManager implements LocaleManager {

    private static final Logger logger
            = LoggerFactory.getLogger(DefaultLocaleManager.class);

    private static final String LOCALE_CODE_VALENCIANO = "vl";

    private static final String I18N_EXTENSION = "org.gvsig.i18n.extension";
    private static final String INSTALLED_TRANSLATIONS_HOME_FOLDER = "i18n";
    private static final String VARIANT = "variant";
    private static final String COUNTRY = "country";
    private static final String LANGUAGE = "language";
    private static final String REGISTERED_LOCALES_PERSISTENCE = "RegisteredLocales";

    private static final String LOCALE_CONFIG_FILENAME = "locale.config";

    private final Locale[] hardcoredLocales = new Locale[]{
        // Default supported locales
        SPANISH, // Spanish
        ENGLISH, // English
        new Locale("en", "US"), // English US
        new Locale("ca"), // Catalan
        new Locale("gl"), // Galician
        new Locale("eu"), // Basque
        new Locale("de"), // German
        new Locale("cs"), // Czech
        new Locale("fr"), // French
        new Locale("it"), // Italian
        new Locale("pl"), // Polish
        new Locale("pt"), // Portuguese
        new Locale("pt", "BR"), // Portuguese Brazil
        new Locale("ro"), // Romanian
        new Locale("zh"), // Chinese
        new Locale("ru"), // Russian
        new Locale("el"), // Greek
        new Locale("ro"), // Romanian
        new Locale("pl"), // Polish
        new Locale("tr"), // Turkish
        new Locale("sr"), // Serbio
        new Locale("sw") // Swahili
    };

    private Set installedLocales = null;
    private Set defaultLocales = null;

    private boolean storedInstalledLocales = false;

    public DefaultLocaleManager() {
        Locale currentLocale = org.gvsig.i18n.Messages.getCurrentLocale();
        this.setCurrentLocale(currentLocale);
    }

    public Locale getCurrentLocale() {
        return org.gvsig.i18n.Messages.getCurrentLocale();
    }

    public void setCurrentLocale(final Locale locale) {
        Locale nearestLocale = getNearestLocale(locale);
        if(nearestLocale == null){
            nearestLocale = this.getLocaleAlternatives(locale)[0];
        }
        org.gvsig.i18n.Messages.setCurrentLocale(nearestLocale, this.getLocaleAlternatives(nearestLocale));

        String localeStr = nearestLocale.getLanguage();
        if (localeStr.equalsIgnoreCase(LOCALE_CODE_VALENCIANO)) {
            Locale.setDefault(new Locale("ca"));
        } else {
            Locale.setDefault(nearestLocale);
        }

        AndamiConfig config = Launcher.getAndamiConfig();
        config.setLocaleLanguage(nearestLocale.getLanguage());
        config.setLocaleCountry(nearestLocale.getCountry());
        config.setLocaleVariant(nearestLocale.getVariant());

        setCurrentLocaleUI(nearestLocale);
    }

    public Locale getDefaultSystemLocale() {
        String language, region, country, variant;
        language = getSystemProperty("user.language", "en");
        // for compatibility, check for old user.region property
        region = getSystemProperty("user.region", null);

        if (region != null) {
            // region can be of form country, country_variant, or _variant
            int i = region.indexOf('_');
            if (i >= 0) {
                country = region.substring(0, i);
                variant = region.substring(i + 1);
            } else {
                country = region;
                variant = "";
            }
        } else {
            country = getSystemProperty("user.country", "");
            variant = getSystemProperty("user.variant", "");
        }
        return new Locale(language, country, variant);
    }

    private Set<Locale> loadLocalesFromConfing(File localesFile)  {
        PropertiesConfiguration config = null;
        if (!localesFile.canRead()) {
            return null;
        }
        try {
            config = new PropertiesConfiguration(localesFile);
        } catch (Exception ex) {
            logger.warn("Can't open locale configuration '" + localesFile + "'.", ex);
            return null;
        }

        Set<Locale> locales = new HashSet<Locale>();
        List localeCodes = config.getList("locale");
        for (Object localeCode : localeCodes) {
            try {
                Locale locale = LocaleUtils.toLocale((String) localeCode);
                locales.add(locale);
            } catch(IllegalArgumentException ex) {
                logger.warn("Can't load locale '"+localeCode+"' from config '"+localesFile.getAbsolutePath()+"'.",ex);
            }
        }
        return locales;
    }

    public Set<Locale> getDefaultLocales() {
        if (this.defaultLocales == null) {
            PluginsManager pluginsManager = PluginsLocator.getManager();
            File localesFile = new File(pluginsManager.getApplicationI18nFolder(), LOCALE_CONFIG_FILENAME);

            Set<Locale> locales = loadLocalesFromConfing(localesFile);
            defaultLocales = new HashSet<Locale>();
            if( locales == null ) {
                for (int i = 0; i < hardcoredLocales.length; i++) {
                    defaultLocales.add(hardcoredLocales[i]);
                }
            } else {
                defaultLocales.addAll(locales);
            }
        }
        return this.defaultLocales;
    }

    public boolean installLocales(URL localesFile) {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        PropertiesConfiguration config = null;
        try {
            config = new PropertiesConfiguration(localesFile);
        } catch (Exception ex) {
            logger.warn("Can't open configuration '" + localesFile + "'.", ex);
        }
        if (config == null) {
            return false;
        }
        List localeCodes = config.getList("locale");
        for (Object localeCode : localeCodes) {
            Locale locale = LocaleUtils.toLocale((String) localeCode);
            if (!getInstalledLocales().contains(locale)) {
                this.installedLocales.add(locale);
            }
        }
        storeInstalledLocales();
        return true;
    }

    public Set<Locale> getInstalledLocales() {
        if (installedLocales == null) {
            installedLocales = new TreeSet<Locale>(new Comparator<Locale>() {
                public int compare(Locale t0, Locale t1) {
                    if (t1 == null || t0 == null) {
                        return 0;
                    }
                    // return getLanguageDisplayName(t0).compareToIgnoreCase(getLanguageDisplayName(t1));
                    return t0.toString().compareToIgnoreCase(t1.toString());
                }
            });

            XMLEntity child = getRegisteredLocalesPersistence();

            // If the list of registered locales is not already persisted,
            // this should be the first time gvSIG is run with the I18nPlugin
            // so we will take the list of default locales
            if (child == null) {
                installedLocales.addAll(getDefaultLocales());
            } else {
                XMLEntity localesEntity = child;
                for (int i = 0; i < localesEntity.getChildrenCount(); i++) {
                    Locale locale = null;
                    XMLEntity localeEntity = localesEntity.getChild(i);
                    String language = localeEntity.getStringProperty(LANGUAGE);
                    String country = localeEntity.getStringProperty(COUNTRY);
                    String variant = localeEntity.getStringProperty(VARIANT);
                    locale = new Locale(language, country, variant);
                    installedLocales.add(locale);
                }
            }
            Set<Locale> lfp = getAllLocalesFromPackages();
            installedLocales.addAll(lfp);
        }
        storeInstalledLocales();
        return Collections.unmodifiableSet(installedLocales);
    }

    public boolean installLocale(Locale locale) {
        // Add the locale to the list of installed ones, if it
        // is new, otherwise, update the texts.
        if (!getInstalledLocales().contains(locale)) {
            getInstalledLocales().add(locale);
            storeInstalledLocales();
        }
        return true;
    }

    public boolean uninstallLocale(Locale locale) {
        if (!getInstalledLocales().remove(locale)) {
            return false;
        }
        storeInstalledLocales();
        return true;
    }

    public String getLanguageDisplayName(Locale locale) {

        String displayName;

        if ("eu".equals(locale.getLanguage())
                && "vascuence".equals(locale.getDisplayLanguage())) {
            // Correction for the Basque language display name,
            // show it in Basque, not in Spanish
            displayName = "Euskera";
        } else if (LOCALE_CODE_VALENCIANO.equals(locale.getLanguage())) {
            // Patch for Valencian/Catalan
            displayName = "Valenci�";
        } else {
            displayName = locale.getDisplayLanguage(locale);
        }

        return StringUtils.capitalize(displayName);
    }

    public String getLocaleDisplayName(Locale locale) {
        String displayName = this.getLanguageDisplayName(locale);

        if( !StringUtils.isBlank(locale.getCountry()) ) {
            if( !StringUtils.isBlank(locale.getVariant()) ) {
                displayName = displayName + " (" +
                        StringUtils.capitalize(locale.getDisplayCountry(locale)) + "/" +
                        locale.getDisplayVariant(locale) + ")";
            } else {
                displayName = displayName + " ("+locale.getDisplayCountry(locale)+")";
            }
        }

        return displayName;
    }

    public Locale[] getLocaleAlternatives(Locale locale) {
        Locale alternatives[] = null;

        String localeStr = locale.getLanguage();
        if (localeStr.equals("es")
                || localeStr.equals("ca")
                || localeStr.equals("gl")
                || localeStr.equals("eu")
                || localeStr.equals(LOCALE_CODE_VALENCIANO)) {
            alternatives = new Locale[2];
            alternatives[0] = new Locale("es");
            alternatives[1] = new Locale("en");
        } else {
            // prefer English for the rest
            alternatives = new Locale[2];
            alternatives[0] = new Locale("en");
            alternatives[1] = new Locale("es");
        }
        return alternatives;
    }

    private void setCurrentLocaleUI(final Locale locale) {
        if (!SwingUtilities.isEventDispatchThread()) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        setCurrentLocaleUI(locale);
                    }
                });
            } catch (Exception ex) {
                // Ignore
            }
        }
        try {
            JComponent.setDefaultLocale(locale);
        } catch (Exception ex) {
            logger.warn("Problems setting locale to JComponent.", ex);
        }
        if (MDIFrame.isInitialized()) {
            try {
                MDIFrame.getInstance().setLocale(locale);
            } catch (Exception ex) {
                logger.warn("Problems settings locale to MDIFrame.", ex);
            }
        }
    }

    public File getResourcesFolder() {
        File i18nFolder = new File(
                PluginsLocator.getManager().getApplicationHomeFolder(),
                INSTALLED_TRANSLATIONS_HOME_FOLDER
        );
        if (!i18nFolder.exists()) {
            i18nFolder.mkdirs();
        }
        return i18nFolder;
    }

    /**
     * Returns the child XMLEntity with the RegisteredLocales.
     */
    private XMLEntity getRegisteredLocalesPersistence() {
        XMLEntity entity = getI18nPersistence();
        if (entity == null) {
            return null;
        }
        XMLEntity child = null;
        for (int i = entity.getChildrenCount() - 1; i >= 0; i--) {
            XMLEntity tmpchild = entity.getChild(i);
            if (tmpchild.getName().equals(REGISTERED_LOCALES_PERSISTENCE)) {
                child = tmpchild;
                break;
            }
        }
        return child;
    }

    /**
     * Stores the list of installed locales into the plugin persistence.
     */
    private void storeInstalledLocales() {
        if(this.storedInstalledLocales ){
            return;
        }
        XMLEntity i18nPersistence = getI18nPersistence();
        if(i18nPersistence == null){
            return;
        }
        XMLEntity localesEntity = getRegisteredLocalesPersistence();

        // Remove the previous list of registered languages
        if (localesEntity != null) {
            for (int i = i18nPersistence.getChildrenCount() - 1; i >= 0; i--) {
                XMLEntity child = i18nPersistence.getChild(i);
                if (child.getName().equals(REGISTERED_LOCALES_PERSISTENCE)) {
                    i18nPersistence.removeChild(i);
                    break;
                }
            }
        }

        // Create the new persistence for the registered languages
        localesEntity = new XMLEntity();

        localesEntity.setName(REGISTERED_LOCALES_PERSISTENCE);

        Set<Locale> locales = this.installedLocales; //getInstalledLocales();
        for (Iterator iterator = locales.iterator(); iterator.hasNext();) {
            Locale locale = (Locale) iterator.next();
            XMLEntity localeEntity = new XMLEntity();
            localeEntity.setName(locale.getDisplayName());
            localeEntity.putProperty(LANGUAGE, locale.getLanguage());
            localeEntity.putProperty(COUNTRY, locale.getCountry());
            localeEntity.putProperty(VARIANT, locale.getVariant());

            localesEntity.addChild(localeEntity);
        }

        getI18nPersistence().addChild(localesEntity);
        this.storedInstalledLocales = true;
    }

    /**
     * Returns the I18n Plugin persistence.
     */
    private XMLEntity getI18nPersistence() {
        try {
            XMLEntity entity = PluginServices.getPluginServices(I18N_EXTENSION).getPersistentXML();
            return entity;
        } catch (Throwable t) {
            // logger.warn("Can't get I18nPersistence",t);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private String getSystemProperty(final String property, String defaultValue) {
        String value
                = (String) AccessController.doPrivileged(new PrivilegedAction() {
                    public Object run() {
                        return System.getProperty(property);
                    }
                });
        return value == null ? defaultValue : value;
    }

    private Set<Locale> getAllLocalesFromPackages() {
        Set<Locale> locales = new HashSet<Locale>();
        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        List<File> folders = installerManager.getAddonFolders(TranslationsInstallerFactory.PROVIDER_NAME);
        for( File folder : folders ) {
            File file = new File(folder,LOCALE_CONFIG_FILENAME);
            Set<Locale> l = this.loadLocalesFromConfing(file);
            locales.addAll(l);
        }
        return locales;
    }

    public Locale getNearestLocale(Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        String variant = locale.getVariant();
        Locale nearestLocale;
        if (language != null && !language.isEmpty()) {
            if (country != null && !country.isEmpty()) {
                if(variant != null && !variant.isEmpty()){
                    nearestLocale = new Locale(language, country, variant);
                    if (getInstalledLocales().contains(nearestLocale)) {
                        return nearestLocale;
                    }
                }
                nearestLocale = new Locale(language, country);
                if (getInstalledLocales().contains(nearestLocale)) {
                    return nearestLocale;
                }
            }
            nearestLocale = new Locale(language);
            if (getInstalledLocales().contains(nearestLocale)) {
                return nearestLocale;
            }
        }
        return null;
    }
}
