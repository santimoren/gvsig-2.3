/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.tools.swing.api.windowmanager.Dialog;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;
import org.gvsig.tools.swing.api.windowmanager.WindowManager_v2;
import org.gvsig.tools.swing.impl.windowmanager.DefaultDialog;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class ToolsWindowManager implements WindowManager_v2 {

    public void showWindow(JComponent contents, String title, MODE mode) {
        int align;
        switch(mode) {
        case DIALOG:
            align = GridBagConstraints.CENTER;
            break;
        case TOOL:
            align = GridBagConstraints.FIRST_LINE_END;
            break;
        case WINDOW:
        default:
            align = GridBagConstraints.FIRST_LINE_START;
            break;
        }
        showWindow(contents, title, mode, align, null);
    }

    public void showWindow(final JComponent contents, final String title, final MODE mode, final int align) {
        this.showWindow(contents, title, mode, align, null);
    }
    
    public void showWindow(final JComponent contents, final String title, final MODE mode, final int align, final Image icon ) {
        if( !SwingUtilities.isEventDispatchThread() ) {
            switch(mode) {
            case DIALOG:
                {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                showWindow(contents, title, mode, align, null);
                            }
                        });
                    } catch (InterruptedException ex) {
                    } catch (InvocationTargetException ex) {
                    }
                }
                break;
            case TOOL:
            case WINDOW:
            default:
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        showWindow(contents, title, mode, align,null);
                    }
                });
                break;
            }
            return;
        }
        MDIManager manager = PluginServices.getMDIManager();
        IWindow window = new Window(contents, title, mode);
        manager.addWindow(window,align);
    }

    public class Window extends JPanel implements IWindow, ComponentListener {

        /**
         *
         */
        private static final long serialVersionUID = -6688594664366192449L;
        protected WindowInfo windowInfo;
        protected Object profile;
        protected JComponent contents;

        public Window(JComponent contents, String title, MODE mode) {
            int code = WindowInfo.RESIZABLE | WindowInfo.MAXIMIZABLE | WindowInfo.ICONIFIABLE;
            switch(mode) {
            case DIALOG:
                code |= WindowInfo.MODALDIALOG;
                profile = WindowInfo.DIALOG_PROFILE;
                break;
            case TOOL:
                code |= WindowInfo.MODELESSDIALOG | WindowInfo.PALETTE;
                profile = WindowInfo.TOOL_PROFILE;
                break;
            case WINDOW:
            default:
                profile = WindowInfo.EDITOR_PROFILE;
                break;
            }

            this.contents = contents;

            this.windowInfo = new WindowInfo(code);
            this.windowInfo.setTitle(title);
            this.windowInfo.setNeedPack(true);

            Dimension size = this.contents.getPreferredSize();
            this.windowInfo.setHeight(size.height);
            this.windowInfo.setWidth(size.width);

            this.setLayout(new BorderLayout());
            this.add(this.contents,BorderLayout.CENTER);

            this.contents.addComponentListener(this);
        }

        public void fireClosingWindow() {
            this.contents.removeComponentListener(this);
            ComponentListener[] l = this.contents.getComponentListeners();
            for( int i=0; i<l.length; i++) {
                l[i].componentHidden(new ComponentEvent(this, i));
            }
        }

        public WindowInfo getWindowInfo() {
            return this.windowInfo;
        }

        public Object getWindowProfile() {
            return this.profile;
        }

        public void componentHidden(ComponentEvent arg0) {
            // Close window when hide contents panel.
            MDIManager manager = PluginServices.getMDIManager();
            manager.closeWindow(this);
        }

        public void componentMoved(ComponentEvent arg0) {
            // Do nothing
        }

        public void componentResized(ComponentEvent arg0) {
            // Do nothing
        }

        public void componentShown(ComponentEvent arg0) {
            // Do nothing
        }

        public JComponent getContents() {
            return this.contents;
        }

    }
    
    
    @Override
    public Dialog createDialog(JComponent component, String title, String header, int buttons) {
        DefaultDialog dialog = new DefaultDialog(component, title, header, buttons);
        return dialog;
    }

    @Override
    public void showWindow(JComponent contents, String title, MODE mode, Image icon) {
        int align;
        switch(mode) {
        case DIALOG:
            align = GridBagConstraints.CENTER;
            break;
        case TOOL:
            align = GridBagConstraints.FIRST_LINE_END;
            break;
        case WINDOW:
        default:
            align = GridBagConstraints.FIRST_LINE_START;
            break;
        }
        showWindow(contents, title, mode, align, icon);
    }

}
