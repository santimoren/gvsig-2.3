/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.actioninfo.impl;

import java.util.HashMap;
import java.util.Map;

import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoStatusCache;
import org.gvsig.andami.plugins.ExtensionHelper;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.identitymanagement.SimpleIdentity;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;

public class DefaultActionInfoStatusCache implements ActionInfoStatusCache {

        private SimpleIdentityManager identityManager;

        private class Status {
		Boolean isEnabled = null;
		Boolean isVisible = null;
	}
	
	private Map<IExtension,Status> extensions = null;
	private Map<ActionInfo,Status> actions = null;
	
	public DefaultActionInfoStatusCache() {
		this.clear();
	}

	public void clear() {
		extensions = new HashMap<IExtension, Status>();
		actions = new HashMap<ActionInfo, Status>();
	}
	
        private SimpleIdentityManager getIdentityManager() {
            if( this.identityManager == null ) {
                this.identityManager = ToolsLocator.getIdentityManager();
            }
            return this.identityManager;
        }

        private SimpleIdentity getCurrentUser() {
            return this.getIdentityManager().getCurrentIdentity();
        }

	public boolean isEnabled(ActionInfo action) {
                if( !this.getCurrentUser().isAuthorized(action.getName()) ) {
                    return false;
                }
                Status status; 
		IExtension extension = action.getExtension();
		if( extension == null ) {
			return false;
		}
		if( ExtensionHelper.canQueryByAction(extension) ) {
			status = actions.get(action);
			if( status == null ) {
				status = new Status();
				actions.put(action, status);
			}
			if( status.isEnabled == null ) {
				status.isEnabled = new Boolean( action.isEnabled() );
			}
		} else {
			status = extensions.get(extension);
			if( status == null ) {
				status = new Status();
				extensions.put(extension, status);
			}
			if( status.isEnabled == null ) {
				status.isEnabled = new Boolean( extension.isEnabled() );
			}
		}
		return status.isEnabled.booleanValue();
	}

	public boolean isVisible(ActionInfo action) {
                if( !this.getCurrentUser().isAuthorized(action.getName()) ) {
                    return false;
                }
                Status status; 
		IExtension extension = action.getExtension();
		if( extension == null ) {
			return false;
		}
		if( ExtensionHelper.canQueryByAction(extension) ) {
			status = actions.get(action);
			if( status == null ) {
				status = new Status();
				actions.put(action, status);
			}
			if( status.isVisible == null ) {
				status.isVisible = new Boolean( action.isVisible() );
			}
		} else {
			status = extensions.get(extension);
			if( status == null ) {
				status = new Status();
				extensions.put(extension, status);
			}
			if( status.isVisible == null ) {
				status.isVisible = new Boolean( extension.isVisible() );
			}
		}
		return status.isVisible.booleanValue();
	}

}
