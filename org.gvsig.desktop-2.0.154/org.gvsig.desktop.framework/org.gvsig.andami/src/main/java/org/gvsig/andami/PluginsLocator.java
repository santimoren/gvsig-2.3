/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami;

import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.tools.locator.AbstractLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;


public class PluginsLocator extends AbstractLocator {

    private static final String LOCATOR_NAME = "PluginsLocator";

    /**
     * PluginsManager name used by the locator to access the instance
     */
    public static final String PLUGINS_MANAGER_NAME = "PluginsManager";
    public static final String PLUGINS_ACTIONINFO_MANAGER_NAME = "PluginsActionInfoManager";
    public static final String PLUGINS_LOCALE_MANAGER_NAME = "LocaleManager";

    private static final String PLUGINS_MANAGER_DESCRIPTION = "PluginsManager of Andami framework";
    private static final String PLUGINS_ACTIONINFO_MANAGER_DESCRIPTION = "PluginsActionInfoManager";
    private static final String PLUGINS_LOCALE_MANAGER_DESCRIPTION = "LocaleManager";

    /**
     * Unique instance.
     */
    private static final PluginsLocator instance = new PluginsLocator();

    /**
     * Return the singleton instance.
     *
     * @return the singleton instance
     */
    public static PluginsLocator getInstance() {
        return instance;
    }

    /**
     * Returns the Locator name.
     *
     * @return String containing the locator name.
     */
    public String getLocatorName() {
        return LOCATOR_NAME;
    }

    /**
     * Return a reference to DataManager.
     *
     * @return a reference to DataManager
     * @throws LocatorException
     *             if there is no access to the class or the class cannot be
     *             instantiated
     * @see Locator#get(String)
     */
    public static PluginsManager getManager() throws LocatorException {
        return (PluginsManager) getInstance().get(PLUGINS_MANAGER_NAME);
    }
    
    /**
     * Registers the Class implementing the DataManager interface.
     *
     * @param clazz
     *            implementing the DataManager interface
     */
    public static void registerManager(Class clazz) {
        getInstance().register(PLUGINS_MANAGER_NAME, PLUGINS_MANAGER_DESCRIPTION,
                clazz);
    }

    /**
     * Registers a class as the default DataManager.
     *
     * @param clazz
     *            implementing the DataManager interface
     */
    public static void registerDefaultManager(Class clazz) {
        getInstance().registerDefault(PLUGINS_MANAGER_NAME, PLUGINS_MANAGER_DESCRIPTION,
                clazz);
    }


    public static ActionInfoManager getActionInfoManager() throws LocatorException {
        return (ActionInfoManager) getInstance().get(PLUGINS_ACTIONINFO_MANAGER_NAME);
    }

    public static void registerActionInfoManager(Class clazz) {
        getInstance().register(PLUGINS_ACTIONINFO_MANAGER_NAME, PLUGINS_ACTIONINFO_MANAGER_DESCRIPTION,
                clazz);
    }

    public static MainFrame getMainFrame() {
        return Launcher.getFrame();
    }
    
    public static LocaleManager getLocaleManager() throws LocatorException {
        return (LocaleManager) getInstance().get(PLUGINS_LOCALE_MANAGER_NAME);
    }

    public static void registerLocaleManager(Class clazz) {
        getInstance().register(PLUGINS_LOCALE_MANAGER_NAME, PLUGINS_LOCALE_MANAGER_DESCRIPTION,
                clazz);
    }

}
