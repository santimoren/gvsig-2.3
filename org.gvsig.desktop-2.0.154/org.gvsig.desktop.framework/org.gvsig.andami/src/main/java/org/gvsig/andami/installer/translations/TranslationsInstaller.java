/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.andami.installer.translations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.gvsig.andami.LocaleManager;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;
import org.gvsig.installer.lib.spi.execution.InstallPackageProvider;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatusManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TranslationsInstaller extends AbstractProvider implements InstallPackageProvider {

    private static Logger logger = LoggerFactory.getLogger(TranslationsInstaller.class);
    
    private int BUFFER = 2048;
    
    public TranslationsInstaller(ProviderServices providerServices) {
            super(providerServices);
    }

    public void install(File applicationFolder, InputStream inputStream, PackageInfo packageInfo) throws InstallPackageServiceException {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        File i18nFolder = pluginsManager.getApplicationI18nFolder();
        
	logger.info("Installing package '"+packageInfo.getCode()+"' in '"+i18nFolder.getAbsolutePath()+"'.");
        try {
            if (!i18nFolder.exists()) {
		logger.warn("Can install package '"+packageInfo.getCode()+"', install folder '"+i18nFolder+"' does not exists.");
		throw new TranslationsIntallerFolderNotFoundException();
            }
            decompress(inputStream, i18nFolder);
            installLocales(i18nFolder, packageInfo);
        } catch (Exception e) {
            try {
                logger.warn("Can install package '"+packageInfo.getCode()+"'.", e);
                // if there is an exception, installLater is called
                installLater(applicationFolder, inputStream, packageInfo);
            } catch (IOException e1) {
                logger.warn("Can install package '"+packageInfo.getCode()+"'.", e1);
                throw new InstallPackageServiceException(e1);
            }
        }

    }

    private void decompress(InputStream inputStream, File folder) 
        throws ZipException, IOException, InstallerInfoFileException {

        ZipInputStream zis = null;
        ZipEntry entry = null;
        byte data[] = new byte[BUFFER];
        int count = 0;

        TaskStatusManager manager = ToolsLocator.getTaskStatusManager();
        SimpleTaskStatus taskStatus = manager.createDefaultSimpleTaskStatus("Uncompressing...");

        manager.add(taskStatus);
        String entryName = "(header)";
        try {
            long readed = 0;
            zis = new ZipInputStream(inputStream);
            while ((entry = zis.getNextEntry()) != null) {
                entryName = FilenameUtils.separatorsToSystem(entry.getName());
                taskStatus.message(entryName);

                File file = new File(folder, entryName);
                if (entry.isDirectory()) {
        		file.mkdirs();                    
                } else {
                    if (file.exists()) {
                         FileUtils.forceDelete(file);
                    }
                    if( !file.getParentFile().exists() ) {
                        FileUtils.forceMkdir(file.getParentFile());
                    }
                    logger.debug("extracting " + file.getAbsolutePath());
                    FileOutputStream fos = new FileOutputStream(file);
                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                        fos.write(data, 0, count);
                        readed += count;
                        taskStatus.setCurValue(readed);
                    }
                    fos.flush();
                    fos.close();
                }
            }
            zis.close();
        } catch(IOException ex) {
                logger.warn("Problems uncompresing 'translations' (last entry '"+entryName+"'.",ex);
                throw ex;

        } catch(RuntimeException ex) {
                logger.warn("Problems uncompresing 'translations' (last entry '"+entryName+"'.",ex);
                throw ex;

        } finally {
            taskStatus.remove();

        }
    }

    
    public void installLater(File applicationDirectory, InputStream inputStream, PackageInfo packageInfo) throws InstallPackageServiceException, IOException {
        logger.warn("installLater is not implementes.");
    }

    private void installLocales(File i18nFolder, PackageInfo packageInfo) throws MalformedURLException {
        LocaleManager localeManager = PluginsLocator.getLocaleManager();
        
        File folder = new File(i18nFolder, packageInfo.getCode());
        File localeConfig = new File(folder,"locale.config");
        localeManager.installLocales(localeConfig.toURI().toURL());
    }
    
    
    public class TranslationsIntallerFolderNotFoundException extends
        InstallPackageServiceException {

        private static final long serialVersionUID = 4416143986837955881L;

        private static final String message = "Translations folder not found";

        private static final String KEY = "translations_folder_not_found";

        public TranslationsIntallerFolderNotFoundException() {
                super(message, KEY, serialVersionUID);
        }

    }
}
