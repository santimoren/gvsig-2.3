/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.help;

import javax.help.HelpSet;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.WindowInfo;



public class AndamiHelpPanel extends HelpPanel implements IWindow {

	private static final long serialVersionUID = 2683827167020046672L;

	private WindowInfo info = null ;

	public AndamiHelpPanel(HelpSet hs){
		super(hs);
	}

	public AndamiHelpPanel(HelpSet hs, String id){
		super(hs,id);
	}

	public void showWindow() {
		MDIManager mdim = PluginServices.getMDIManager();
		mdim.addWindow((IWindow) this);
	}

	public WindowInfo getWindowInfo() {
		if( info == null ) {
			info = new WindowInfo( WindowInfo.RESIZABLE |
	                WindowInfo.MAXIMIZABLE | WindowInfo.MODELESSDIALOG);
			info.setHeight(HEIGHT);
			info.setWidth(WIDTH);
			info.setTitle(getTitle());
		}
		return info;
	}

	public Object getWindowProfile() {
		return WindowInfo.TOOL_PROFILE;
	}

}
