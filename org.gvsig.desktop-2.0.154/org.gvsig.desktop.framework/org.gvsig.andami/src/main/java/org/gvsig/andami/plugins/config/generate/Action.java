/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id: Menu.java 29593 2009-06-29 15:54:31Z jpiera $
 */

package org.gvsig.andami.plugins.config.generate;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Action.
 * 
 * @version $Revision: 29593 $ $Date: 2009-06-29 17:54:31 +0200 (lun, 29 jun 2009) $
 */
public class Action implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/
    /**
     * Field _name
     */
    private java.lang.String _name;
    
    /**
     * Field _accelerator
     */
    private java.lang.String _accelerator;
    
    /**
     * Field _actionCommand
     */
    private java.lang.String _actionCommand;

    /**
     * Field _icon
     */
    private java.lang.String _icon;

    /**
     * Field _tooltip
     */
    private java.lang.String _tooltip;

    /**
     * Field _label
     */
    private java.lang.String _label;

    /**
     * Field _position
     */
    private long _position;

    /**
     * keeps track of state for field: _position
     */
    private boolean _has_position;


      //----------------/
     //- Constructors -/
    //----------------/

    public Action() {
        super();
    } //-- com.iver.andami.plugins.config.generate.Menu()


      //-----------/
     //- Methods -/
    //-----------/
    /**
     * Method deletePosition
     */
    public void deletePosition()
    {
        this._has_position= false;
    } //-- void deletePosition() 

    /**
     * Returns the value of field 'actionCommand'.
     * 
     * @return the value of field 'actionCommand'.
     */
    public java.lang.String getActionCommand()
    {
        return this._actionCommand;
    } //-- java.lang.String getActionCommand() 

    /**
     * Returns the value of field 'accelerator'.
     * 
     * @return the value of field 'accelerator'.
     */
    public java.lang.String getAccelerator()
    {
        return this._accelerator;
    } //-- java.lang.String getAccelerator() 

    /**
     * Returns the value of field 'icon'.
     * 
     * @return the value of field 'icon'.
     */
    public java.lang.String getIcon()
    {
        return this._icon;
    } //-- java.lang.String getIcon() 

    /**
     * Returns the value of field 'position'.
     * 
     * @return the value of field 'position'.
     */
    public long getPosition()
    {
        return this._position;
    } //-- int getPosition() 

    /**
     * Returns the value of field 'label'.
     * 
     * @return the value of field 'label'.
     */
    public java.lang.String getLabel()
    {
        return this._label;
    } //-- java.lang.String getLabel() 

    /**
     * Returns the value of field 'tooltip'.
     * 
     * @return the value of field 'tooltip'.
     */
    public java.lang.String getTooltip()
    {
        return this._tooltip;
    } //-- java.lang.String getTooltip() 

    /**
     * Method hasPosition
     */
    public boolean hasPosition()
    {
        return this._has_position;
    } //-- boolean hasPosition() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'actionCommand'.
     * 
     * @param actionCommand the value of field 'actionCommand'.
     */
    public void setActionCommand(java.lang.String actionCommand)
    {
        this._actionCommand = actionCommand;
    } //-- void setActionCommand(java.lang.String) 

    /**
     * Sets the value of field 'accelerator'.
     * 
     * @param accelerator the value of field 'accelerator'.
     */
    public void setAccelerator(java.lang.String accelerator)
    {
        this._accelerator = accelerator;
    } //-- void setAccelerator(java.lang.String) 

    /**
     * Sets the value of field 'icon'.
     * 
     * @param icon the value of field 'icon'.
     */
    public void setIcon(java.lang.String icon)
    {
        this._icon = icon;
    } //-- void setIcon(java.lang.String) 


    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Sets the value of field 'position'.
     * 
     * @param position the value of field 'position'.
     */
    public void setPosition(long position)
    {
    	if( position < 100000000 ) {
    		this._position = position + 1000000000; 
    	} else {
    		this._position = position;
    	}
        this._has_position = true;
    } //-- void setPosition(int) 

    /**
     * Sets the value of field 'label'.
     * 
     * @param text the value of field 'label'.
     */
    public void setLabel(java.lang.String label)
    {
        this._label = label;
    } //-- void setLabel(java.lang.String) 

    /**
     * Sets the value of field 'tooltip'.
     * 
     * @param tooltip the value of field 'tooltip'.
     */
    public void setTooltip(java.lang.String tooltip)
    {
        this._tooltip = tooltip;
    } //-- void setTooltip(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (org.gvsig.andami.plugins.config.generate.Action) Unmarshaller.unmarshal(org.gvsig.andami.plugins.config.generate.Action.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 
    
    public String toString() {
    	StringBuffer buffer = new StringBuffer();
    	buffer.append("Action {");
    	buffer.append("name='").append(this._name).append("', ");
    	buffer.append("label='").append(this._label).append("', ");
    	buffer.append("tooltip='").append(this._tooltip).append("', ");
    	buffer.append("actionCommand='").append(this._actionCommand).append("', ");
    	buffer.append("position='").append(this._position).append("', ");
    	buffer.append("icon='").append(this._icon).append("', ");
    	buffer.append("accelerator='").append(this._accelerator).append("' }");
    	return buffer.toString();
	}
}
