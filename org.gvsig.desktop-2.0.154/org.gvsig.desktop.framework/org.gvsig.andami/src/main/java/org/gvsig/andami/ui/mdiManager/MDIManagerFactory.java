/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiManager;

import org.apache.log4j.Logger;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.plugins.PluginClassLoader;
import org.gvsig.andami.plugins.config.generate.SkinExtension;



/**
 * Se encarga de la creaci�n de la clase Skin.
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class MDIManagerFactory {
    private static SkinExtension skinExtension = null;
    private static PluginClassLoader loader = null;
    private static MDIManager mdiManager = null;
    private static Logger logger = Logger.getLogger(MDIManagerFactory.class.getName());

    public static void setSkinExtension(SkinExtension extension,
        PluginClassLoader loader) {
        MDIManagerFactory.loader = loader;
        MDIManagerFactory.skinExtension = extension;
    }

    /**
     * Obtiene una referencia al Skin cargado. El skin cargado es un singleton, se
     * devuelve la misma instancia a todas las invocaciones de �ste m�todo
     *
     * @return referencia al skin cargado
     */
    public static MDIManager createManager() {
    	if (mdiManager != null) return mdiManager;
    	
        if (skinExtension == null) {
        	throw new NoSkinExtensionException(Messages.getString("MDIManagerFactory.No_skin_extension_in_the_plugins"));
        } else {
            try {
                mdiManager = (MDIManager) loader.loadClass(skinExtension.getClassName())
                                          .newInstance();
                return mdiManager;
            } catch (InstantiationException e) {
                logger.error(Messages.getString("Launcher.Error_instanciando_la_extension"), e);

                //                return new NewSkin();
            } catch (IllegalAccessException e) {
                logger.error(Messages.getString("Launcher.Error_instanciando_la_extension"), e);

                //                return new NewSkin();
            } catch (ClassNotFoundException e) {
                logger.error(Messages.getString("Launcher.No_se_encontro_la_clase_de_la_extension"), e);

                //                return new NewSkin();
            }
        }
        
        return null;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the skinExtension.
     */
    public static SkinExtension getSkinExtension() {
        return skinExtension;
    }
}
