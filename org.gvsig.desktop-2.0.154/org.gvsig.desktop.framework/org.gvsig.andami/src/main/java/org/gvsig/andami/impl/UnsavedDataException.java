package org.gvsig.andami.impl;

import java.util.List;

import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.tools.exception.BaseException;

public class UnsavedDataException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = 7764186865062216316L;
    private final static String MESSAGE_FORMAT = "Has not been able to save the following data: %(errors)\n";
    private final static String MESSAGE_KEY = "_UnsavedDataException";
    private List<IUnsavedData> errors;

    public UnsavedDataException(List<IUnsavedData> errors) {
        super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
        this.errors = errors;

        StringBuilder errorsString = new StringBuilder();
        for (IUnsavedData error : errors) {
            errorsString.append(" - ");
            errorsString.append(error.getResourceName());
            errorsString.append(" -- ");
            errorsString.append(error.getDescription());
            errorsString.append("\n");
        }
        setValue("errors", errorsString);
    }

    public List<IUnsavedData> getUnsavedData(){
        return errors;
    }
}
