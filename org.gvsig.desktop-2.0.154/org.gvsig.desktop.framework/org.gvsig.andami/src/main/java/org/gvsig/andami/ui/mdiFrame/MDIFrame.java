/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.ui.mdiFrame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.actioninfo.ActionInfoStatusCache;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.plugins.ExtensionDecorator;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.PluginClassLoader;
import org.gvsig.andami.plugins.config.generate.ActionTool;
import org.gvsig.andami.plugins.config.generate.Label;
import org.gvsig.andami.plugins.config.generate.Menu;
import org.gvsig.andami.plugins.config.generate.PopupMenu;
import org.gvsig.andami.plugins.config.generate.SelectableTool;
import org.gvsig.andami.plugins.config.generate.SkinExtensionType;
import org.gvsig.andami.plugins.config.generate.ToolBar;
import org.gvsig.andami.ui.mdiFrame.TranslatableButtonHelper.TranslatableButton;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.MDIManagerFactory;
import org.gvsig.gui.beans.controls.IControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main application window.
 *
 * Use Launcher.getMDIFrame to get the instance of this class.
 *
 * @version $Revision: 39484 $
 */
@SuppressWarnings("unchecked")
public class MDIFrame extends JFrame implements ComponentListener,
        ContainerListener, ActionListener, MainFrame {

    private static final long serialVersionUID = -2472484309160847654L;

    private static Logger logger = LoggerFactory.getLogger(MDIFrame.class);

    private static MDIFrame instance = null;

    private MDIManager mdiManager = MDIManagerFactory.createManager();

    private boolean refreshingControls = false;

    /**
     * Elementos de la aplicaciï¿½n
     */
    private JMenuBar menuBar = new JMenuBar();

    /**
     * Panel which contains the toolbars
     */
    private JPanel toolBars = new JPanel();

    /**
     * Status bar
     */
    private NewStatusBar bEstado = null;

    /**
     * Asocia los nombres con las barras de herramientas
     */
    private HashMap toolBarMap = new HashMap();

    /**
     * Almacena los grupos de selectableTools
     */
    private HashMap buttonGroupMap = new HashMap();
    /**
     * Stores the initially selected tools.
     * It contains pairs (String groupName, JToolBarToggleButton button)
     */
    private HashMap initialSelectedTools = new HashMap();

    /**
     * Stores the actionCommand of the selected tool, for each group.
     * It contains pairs (String groupName, JToolBarToggleButton button)
     */
    private Map selectedTool = null;
    // this should be the same value defined at plugin-config.xsd
    private String defaultGroup = "unico";

    /**
     * Asocia los nombres con los popupMenus
     */
    private HashMap popupMap = new HashMap();

    /**
     * Asocia controles con la clase de la extension asociada
     */
    private List<JComponent> controls = new ArrayList<JComponent>();

    /**
     * Asocia la informaciï¿½n sobre las etiquetas que van en la status bar con
     * cada extension
     */
    private HashMap classLabels = new HashMap();

    // private HashMap classControls = new HashMap();
    /**
     * ProgressListeners (ver interfaz com.iver.mdiApp.ui.ProgressListener)
     */
    private ArrayList progressListeners = new ArrayList();

    /**
     * Timer para invocar los enventos de la interfaz anterior
     */
    private Timer progressTimer = null;

    /**
     * Tabla hash que asocia las clases con las extensiones
     */
    // private Map classesExtensions = new HashMap<Class<? extends IExtension>, ExtensionDecorator>();
    /**
     * ï¿½ltima clase que activï¿½ etiquetas
     */
    private Class lastLabelClass;

    /**
     * Instancia que pone los tooltip en la barra de estado
     */
    private TooltipListener tooltipListener = new TooltipListener();

    private HashMap infoCodedMenus = new HashMap();

    private String titlePrefix;

    private static final String noIcon = "no-icon";

    private Map<JComponent,IExtension> control2extensions = new HashMap<JComponent, IExtension>();

    private MDIFrame() {

    }

    public static boolean isInitialized() {
        return instance!=null ;
    }

    public static MDIFrame getInstance() {
        if ( instance == null ) {
            instance = new MDIFrame();
        }
        return instance;
    }

    /**
     * Makes some initialization tasks.
     *
     * @throws RuntimeException
     */
    public void init() {
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        if ( !SwingUtilities.isEventDispatchThread() ) {
            throw new RuntimeException("Not Event Dispatch Thread");
        }

        // Se añaden los listeners del JFrame
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                Launcher.closeApplication();
            }
        });
        this.addComponentListener(this);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        // Se configura la barra de menu
        setJMenuBar(menuBar);

        // Se configura el layout del JFrame principal
        this.getContentPane().setLayout(new BorderLayout());

        /*
         * Se configura y se añade el JPanel de las barras de
         * herramientas
         */
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        layout.setHgap(0);
        layout.setVgap(0);
        toolBars.setLayout(layout);
        getContentPane().add(toolBars, BorderLayout.PAGE_START);

        // Add the status bar of the application
        bEstado = new NewStatusBar();
        bEstado.message(Messages.getString("StatusBar.Aplicacion_iniciada"), JOptionPane.INFORMATION_MESSAGE);
        getContentPane().add(bEstado, BorderLayout.SOUTH);

        this.toolBars.addContainerListener(this);


        /*
         * Setting default values. Persistence is read
         * afterwards
         */
        setSize(
                MainFrame.MAIN_FRAME_SIZE_DEFAULT[0],
                MainFrame.MAIN_FRAME_SIZE_DEFAULT[1]);
        setLocation(
                MainFrame.MAIN_FRAME_POS_DEFAULT[0],
                MainFrame.MAIN_FRAME_POS_DEFAULT[1]);
        setExtendedState(
                MainFrame.MAIN_FRAME_EXT_STATE_DEFAULT);

        mdiManager.init(this);
    }

    @Override
    public void setTitle(final String title) {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    setTitle(title);
                }
            });
            return;
        }
        super.setTitle(titlePrefix + " : " + title);
    }

    private SelectableToolBar getToolBar(final String toolBarName) {
        SelectableToolBar jtb = (SelectableToolBar) toolBarMap.get(toolBarName);
        if ( jtb == null ) {
            jtb = new SelectableToolBar(toolBarName);
            jtb.setRollover(true);
            jtb.setAndamiVisibility(true);
            toolBarMap.put(toolBarName, jtb);
            toolBars.add(jtb);
        }
        return jtb;
    }

    public void addTool(final PluginClassLoader loader, final SkinExtensionType ext,
            final ToolBar toolBar, final SelectableTool selectableTool)
            throws ClassNotFoundException {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        try {
                            addTool(loader, ext, toolBar, selectableTool);
                        } catch (ClassNotFoundException ex) {
                            logger.warn("¿¿¿ Eehh????", ex);
                        }
                    }
                });
            } catch (Exception ex) {
                // Do nothing
            }
            return;
        }
        String name = toolBar.getName();
        SelectableToolBar jtb = getToolBar(name);
        if( selectableTool.getDropDownGroup()!=null ) {
            ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
            ActionInfo action = actionManager.getAction(selectableTool.getName());
            if( action!=null ) {
                DropDownButton dropDownButton = (DropDownButton) jtb.findComponent(selectableTool.getDropDownGroup());
                if( dropDownButton==null ) {
                    dropDownButton = new DropDownButton();
                    dropDownButton.setName(selectableTool.getDropDownGroup());
                    jtb.add(dropDownButton);
                    addControl(dropDownButton);
                }
                dropDownButton.add(actionManager.getTranslated(action));
            }
            return;
        }

        I18nManager i18nManager = ToolsLocator.getI18nManager();

        ImageIcon image = PluginServices.getIconTheme().get(selectableTool.getIcon());
        if ( image != null ) {
            logger.warn("Unable to find icon '" + selectableTool.getIcon() + "'.");
            image = PluginServices.getIconTheme().get(noIcon);
        }

        ToggleButtonModel buttonModel = new ToggleButtonModel();
        JToolBarToggleButton btn = new JToolBarToggleButton(selectableTool.getText(), image);
        btn.setModel(buttonModel);
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.addMouseListener(tooltipListener);
        btn.addActionListener(this);
        btn.setFocusable(false);
        btn.setActionCommand(selectableTool.getActionCommand());
        btn.setToolTipText(selectableTool.getTooltip());
        btn.setEnabled(false);
        btn.setVisible(false);

        ButtonGroup group = (ButtonGroup) buttonGroupMap.get(selectableTool.getGroup());
        if ( group == null ) {
            group = new ButtonGroup();
            buttonGroupMap.put(selectableTool.getGroup(), group);
        }

        jtb.addButton(group, btn);

        buttonModel.setGroupName(selectableTool.getGroup());
        if ( selectableTool.getIsDefault() ) {
            btn.setSelected(true);
            initialSelectedTools.put(selectableTool.getGroup(),
                    btn.getActionCommand());
        }

        addControl(btn);

        if ( selectableTool.getName() != null ) {
            btn.setName(selectableTool.getName());
        }

        if ( selectableTool.getTooltip() != null ) {
            btn.setToolTip(i18nManager.getTranslation(selectableTool.getTooltip()));
            btn.setToolTipKey(selectableTool.getTooltip());
        }

        if ( selectableTool.getEnableText() != null ) {
            btn.setEnableText(i18nManager.getTranslation(selectableTool.getEnableText()));
        }

        if ( selectableTool.getLast() == true ) {
            jtb.addSeparator();
        }
    }

    public void addTool(final PluginClassLoader loader, final SkinExtensionType ext,
            final ToolBar toolBar, final ActionTool actionTool) throws ClassNotFoundException {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        try {
                            addTool(loader, ext, toolBar, actionTool);
                        } catch (ClassNotFoundException ex) {
                            logger.warn("¿¿¿ Eehh????", ex);
                        }
                    }
                });
            } catch (Exception ex) {
                // Do nothing
            }
            return;
        }

        String name = toolBar.getName();
        SelectableToolBar jtb = getToolBar(name);
        if( actionTool.getDropDownGroup()!=null ) {
            ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
            ActionInfo action = actionManager.getAction(actionTool.getName());
            if( action!=null ) {
                DropDownButton dropDownButton = (DropDownButton) jtb.findComponent(actionTool.getDropDownGroup());
                if( dropDownButton==null ) {
                    dropDownButton = new DropDownButton();
                    dropDownButton.setName(actionTool.getDropDownGroup());
                    jtb.add(dropDownButton);
                    addControl(dropDownButton);
                }
                dropDownButton.add(actionManager.getTranslated(action));
            }
            return;
        }

        I18nManager i18nManager = ToolsLocator.getI18nManager();

        ImageIcon image = IconThemeHelper.getImageIcon(actionTool.getIcon());
        if ( image != null ) {
            logger.warn("Unable to find icon '" + actionTool.getIcon() + "'.");
            image =  PluginServices.getIconTheme().get(noIcon);
        }

        JToolBarButton btn = new JToolBarButton(actionTool.getText(), image);
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.addMouseListener(tooltipListener);
        btn.addActionListener(this);
        btn.setFocusable(false);
        btn.setActionCommand(actionTool.getActionCommand());
        btn.setEnabled(false);
        btn.setVisible(false);

        jtb.add(btn);

        addControl(btn);

        if ( actionTool.getName() != null ) {
            btn.setName(actionTool.getName());
        }

        if ( actionTool.getTooltip() != null ) {
            btn.setToolTip(i18nManager.getTranslation(actionTool.getTooltip()));
            btn.setToolTipKey(actionTool.getTooltip());
        }

        if ( actionTool.getEnableText() != null ) {
            btn.setEnableText(i18nManager.getTranslation(actionTool.getEnableText()));
        }

        if ( actionTool.getLast() == true ) {
            jtb.addSeparator();
        }
    }

    public void addTool(final ActionInfo action, final String toolBarName, final String dropDownName) {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    addTool(action, toolBarName,dropDownName);
                }
            });
            return;
        }
        SelectableToolBar jtb = getToolBar(toolBarName);
        DropDownButton dropDownButton = (DropDownButton) jtb.findComponent(dropDownName);
        if( dropDownButton == null ) {
            dropDownButton = new DropDownButton();
            jtb.add(dropDownButton);
        }
        ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
        dropDownButton.add(actionManager.getTranslated(action));
    }
    public void addTool(final ActionInfo action, final String toolBarName) {
        addTool(action, toolBarName, false);
    }

    public void addTool(final ActionInfo action, final String toolBarName, final boolean useText) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    addTool(action, toolBarName, useText);
                }
            });
            return;
        }
        JToolBarButton btn = new JToolBarButton(action.getIcon());
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.addMouseListener(tooltipListener);
        btn.addActionListener(this);
        btn.setFocusable(false);
        btn.setActionCommand(action.getCommand());
        btn.setEnabled(false);
        btn.setVisible(false);
        btn.setName(action.getName());
        if( useText ) {
            btn.setText(action.getLabel());
        }
        if ( action.getTooltip() != null ) {
            btn.setToolTip(i18nManager.getTranslation(action.getTooltip()));
            btn.setToolTipKey(action.getTooltip());
        }

        SelectableToolBar jtb = getToolBar(toolBarName);
        jtb.add(btn);

        addControl(btn);

    }

    public void addSelectableTool(final ActionInfo action, final String toolBarName, final String groupName, final boolean useText) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    addSelectableTool(action, toolBarName,groupName,useText);
                }
            });
            return;
        }

        ButtonGroup group = (ButtonGroup) buttonGroupMap.get(groupName);
        if ( group == null ) {
            group = new ButtonGroup();
            buttonGroupMap.put(groupName, group);
        }
        ToggleButtonModel buttonModel = new ToggleButtonModel();
        buttonModel.setGroupName(groupName);

        JToolBarToggleButton btn = new JToolBarToggleButton(action.getIcon());
        btn.setModel(buttonModel);
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.addMouseListener(tooltipListener);
        btn.addActionListener(this);
        btn.setFocusable(false);
        btn.setActionCommand(action.getCommand());
        btn.setEnabled(false);
        btn.setVisible(false);
        btn.setName(action.getName());
        if( useText ) {
            btn.setText(action.getLabel());
        }
        if ( action.getTooltip() != null ) {
            btn.setToolTip(i18nManager.getTranslation(action.getTooltip()));
            btn.setToolTipKey(action.getTooltip());
        }

        SelectableToolBar jtb = getToolBar(toolBarName);
        jtb.addButton(group, btn);

        addControl(btn);

    }

    /**
     * Creates the needed menu structure to add the menu to the bar.
     * Returns the father which must hold the menu which was
     * provided as parameter.
     *
     * Crea la estructura de menus necesaria para añadir el menu a la barra.
     * Devuelve el padre del cual debe colgar el menu que se pasa como
     * parametro.
     *
     * @param menu
     * The Menu whose support is going to be added
     * @param loader
     * The plugin's class loader
     *
     * @return The proper father for the menu which was provided as parameter
     */
    private JMenu createMenuAncestors(Menu menu, PluginClassLoader loader) {
        return createMenuAncestors(menu.getText());
    }

    private JMenu createMenuAncestors(String text) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        MenuElement menuPadre = null;

        String[] menues = text.split("/");
        List menuList = new ArrayList();
        menuList.add(menues[0]);
        menuPadre = getMenu(menuList, menuBar);

        JMenu padre = null;

        if ( menuPadre == null ) {
            padre = new JMenuTraslatable(i18nManager.getTranslation(menues[0]));
            ((JMenuTraslatable) padre).setTextKey(menues[0]);
            padre.setName(menues[0]);
            addControl(padre);
            menuBar.add(padre);
        } else if ( menuPadre instanceof JMenu ) {
            padre = (JMenu) menuPadre;
        } else {
            logger.warn("Error creating menu. Ancestor does not exist (" + text + ").");
            return null;
        }

        // Se crea el resto de menus
        ArrayList temp = new ArrayList();

        for ( int i = 1; i < (menues.length - 1); i++ ) {
            temp.add(menues[i]);
        }

        menuPadre = createMenus(temp, padre);

        return (JMenu) menuPadre;
    }

    /**
     * Añade la informacion del menu al framework.
     * Debido a que los menï¿½es se
     * pueden introducir en un orden determinado por el usuario, pero los
     * plugins se instalan en un orden arbitrario, primero se almacena la
     * informacion de todos los menus para luego ordenarlos y posteriormente
     * añadirlos al interfaz
     *
     * @param loader
     * Posicion del menu. Se ordena por este campo
     * @param ext
     * Array con los nombres de los padres del menu
     * @param menu
     * Texto del menu
     *
     * @throws ClassNotFoundException
     * @throws RuntimeException
     */
    public void addMenu(final PluginClassLoader loader, final SkinExtensionType ext,
            final Menu menu) throws ClassNotFoundException {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        try {
                            addMenu(loader, ext, menu);
                        } catch (ClassNotFoundException ex) {
                            logger.warn("¿¿¿ Eehh????", ex);
                        }
                    }
                });
            } catch (Exception ex) {
                // Do nothing
            }
            return;
        }
        JMenu menuPadre = createMenuAncestors(menu, loader);

        if ( menu.getIs_separator() ) {
            menuPadre.addSeparator();
            return;
        }

        JMenuItem nuevoMenu = createJMenuItem(loader, menu);
        nuevoMenu.addMouseListener(tooltipListener);
        menuPadre.add(nuevoMenu);
        addControl(nuevoMenu);

    }

    public void addMenu(final ActionInfo action, final String text) {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    addMenu(action, text);
                }
            });
            return;
        }
        JMenu menuPadre = createMenuAncestors(text);
        JMenuItem nuevoMenu = createJMenuItem(action, text);
        nuevoMenu.addMouseListener(tooltipListener);
        menuPadre.add(nuevoMenu);
        menuPadre.invalidate();
        Class<? extends IExtension> classExtension = action.getExtension().getClass();
        addControl(nuevoMenu);
    }

    /**
     * Dado lista de nombres de menu, encuentra el menu
     *
     * @param nombres
     * @param padre
     * @return
     */
    private javax.swing.JMenuItem getMenu(List nombres, MenuElement parent) {
        if ( parent instanceof javax.swing.JMenu ) {
            javax.swing.JMenu parentItem = (javax.swing.JMenu) parent;

            for ( int i = 0; i < parentItem.getMenuComponentCount(); i++ ) {

                String item_name = parentItem.getMenuComponent(i).getName();
                if ( ((item_name != null) && (item_name.compareTo((String) nombres.get(0)) == 0))
                        || /*
                         * We also accept "leaf menus" with no name
                         * (Project manager, View-1, View-2, etc)
                         */ lastMenuItemWithoutName(parentItem.getMenuComponent(i), nombres) ) {

                    nombres.remove(0);
                    if ( nombres.isEmpty() ) {
                        if ( parentItem.getMenuComponent(i) instanceof javax.swing.JMenuItem ) {
                            return (javax.swing.JMenuItem) parentItem
                                    .getMenuComponent(i);
                        } else {
                            logger.error(PluginServices.getText(this,
                                    "Menu_type_not_supported_")
                                    + " "
                                    + parentItem.getMenuComponent(i).getClass()
                                    .getName());
                            return null;
                        }
                    } else {
                        return getMenu(nombres,
                                (MenuElement) parentItem.getMenuComponent(i));
                    }
                }
            }
        } else if ( parent instanceof JMenuBar ) {
            javax.swing.JMenuBar parentItem = (javax.swing.JMenuBar) parent;

            for ( int i = 0; i < parentItem.getMenuCount(); i++ ) {
                if ( (parentItem.getMenu(i).getName() != null // not a
                        // JToolBar.Separator
                        )
                        && (parentItem.getMenu(i).getName()
                        .compareTo((String) nombres.get(0)) == 0) ) {
                    nombres.remove(0);
                    if ( nombres.isEmpty() ) {
                        if ( parentItem.getMenu(i) instanceof javax.swing.JMenuItem ) {
                            return parentItem.getMenu(i);
                        } else {
                            logger.error(PluginServices.getText(this,
                                    "Menu_type_not_supported_")
                                    + " "
                                    + parentItem.getMenu(i).getClass()
                                    .getName());
                            return null;
                        }
                    } else {
                        return getMenu(nombres, parentItem.getMenu(i));
                    }
                }
            }
        } else {
            logger.error(PluginServices.getText(this,
                    "Menu_type_not_supported_")
                    + " "
                    + parent.getClass().getName() + " " + parent.toString());
        }
        return null;
    }

    /**
     * @param menuComponent
     * @param nombres
     * @return
     */
    private boolean lastMenuItemWithoutName(Component mc, List names) {

        /*
         * names must have 1 string
         */
        if ( names == null || names.size() != 1 ) {
            return false;
        }
        if ( !(mc instanceof JMenuItem) ) {
            return false;
        }
        JMenuItem jmi = (JMenuItem) mc;
        if ( jmi.getName() != null ) {
            /*
             * Name must be null, so this is a menu leaf
             */
            return false;
        }
        if ( jmi.getText() == null ) {
            return false;
        }
        return jmi.getText().compareTo((String) names.get(0)) == 0;
    }

    private class JMenuTraslatable extends JMenu implements TranslatableButton {

        private TranslatableButtonHelper i18nHelper = new TranslatableButtonHelper();

        public JMenuTraslatable(String text) {
            super(text);
        }

        public void setTextKey(String key) {
            this.i18nHelper.setText(key);
        }

        public void setToolTipKey(String key) {
            this.i18nHelper.setTooltip(key);
        }

        public void translate() {
            if ( this.i18nHelper.needTranslation() ) {
                this.i18nHelper.translate();
                this.setText(this.i18nHelper.getText());
            }
        }

    }

    /**
     * Crea la estructura de menus recursivamente. Por ejemplo, si se le pasa
     * en el parï¿½metro nombres el array {"Search", "References", "Workspace"}
     * crearï¿½ un menï¿½ Search, un submenï¿½ del anterior que se llamarï¿½
     * References y debajo de ï¿½ste ï¿½ltimo otro menu llamado Workspace
     *
     * @param nombres
     * Array con los nombres de los menï¿½s que se quieren crear
     * @param padre
     * Menu padre de los menï¿½s creados. Es ï¿½til porque es un
     * algoritmo recursivo
     *
     * @return Devuelve el menï¿½ creado. Al final de toda la recursividad,
     * devolverï¿½ el menï¿½ de mï¿½s abajo en la jerarquï¿½a
     *
     * @throws RuntimeException
     */
    private JMenu createMenus(ArrayList nombres, JMenu padre) {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            logger.warn("Este metodo requiere que se este ejecutando en el hilo de eventos de AWT.");
            throw new RuntimeException("No Event Dispatch Thread");
        }
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        // si no quedan nombres de menu por crear se vuelve: caso base
        if ( nombres.isEmpty() ) {
            return padre;
        }

        // Se busca el menu por si ya existiera para no crearlo otra vez
        JMenu buscado = null;

        for ( int i = 0; i < padre.getMenuComponentCount(); i++ ) {
            try {
                JMenu hijo = (JMenu) padre.getMenuComponent(i);

                if ( hijo.getName().compareTo((String) nombres.get(0)) == 0 ) {
                    buscado = hijo;
                }
            } catch (ClassCastException e) {
                /*
                 * Se ha encontrado un elemento hoja del arbol de menï¿½es
                 */
            }
        }

        if ( buscado != null ) {
            // Si lo hemos encontrado creamos el resto
            nombres.remove(0);

            return createMenus(nombres, buscado);
        } else {
            // Si no lo hemos encontrado se crea el menu, se aï¿½ade al padre
            // y se crea el resto
            String nombre = (String) nombres.get(0);
            JMenuTraslatable menuPadre = new JMenuTraslatable(i18nManager.getTranslation(nombre));
            menuPadre.setTextKey(nombre);
            menuPadre.setName(nombre);
            addControl(menuPadre);
            padre.add(menuPadre);

            nombres.remove(0);

            return createMenus(nombres, menuPadre);
        }
    }

    /**
     * Mï¿½todo invocado en respuesta a ciertos eventos de la interfaz que
     * pueden
     * ocultar botones de las barras de herramientas y que redimensiona ï¿½sta
     * de manera conveniente para que no se oculte ninguno
     */
    private void ajustarToolBar() {
        int margen = 8;
        int numFilas = 1;
        double acum = margen;

        int toolHeight = 0;

        for ( int i = 0; i < toolBars.getComponentCount(); i++ ) {
            Component c = toolBars.getComponent(i);

            if ( !c.isVisible() ) {
                continue;
            }

            double width = c.getPreferredSize().getWidth();
            acum = acum + width;

            if ( acum > this.getWidth() ) {
                numFilas++;
                acum = width + margen;
            }

            if ( c.getPreferredSize().getHeight() > toolHeight ) {
                toolHeight = c.getPreferredSize().height;
            }
        }

        toolBars.setPreferredSize(new Dimension(this.getWidth(),
                (numFilas * toolHeight)));

        toolBars.updateUI();
    }

    public void setClassesExtensions(Map<Class<? extends IExtension>, ExtensionDecorator> classesExtensions) {
        // Ya no es necesario que se mantenga el Map con las extensiones.

//    	Map<Class<? extends IExtension>, ExtensionDecorator> extensions = new HashMap<Class<? extends IExtension>, ExtensionDecorator>();
//    	for ( Entry<Class<? extends IExtension>, ExtensionDecorator>  entry: classesExtensions.entrySet()) {
//			if( ! (entry.getValue().getExtension() instanceof LibraryExtension) ) {
//				extensions.put(entry.getKey(), entry.getValue());
//			}
//		}
//        this.classesExtensions = extensions;
    }

    /**
     * Metodo de callback invocado cuando se selecciona un menu o un boton
     * de
     * la barra de herramientas. Se busca la extensiï¿½n asociada y se ejecuta
     *
     * @param e
     * Evento producido
     */
    public void actionPerformed(ActionEvent e) {

        String actionName = "(unknow)";
        try {
            JComponent control = (JComponent) e.getSource();
            actionName = control.getName();

            ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
            ActionInfo action = actionManager.getAction(control.getName());
            Object args = null;
            if ( control instanceof IControl ) {
                args = ((IControl) control).getValue();
            }
            if ( args == null ) {
                action.execute();
            } else {
                action.execute(args);
            }
            String actionCommand = e.getActionCommand();
            try {
                JToolBarToggleButton toggle = (JToolBarToggleButton) control;
                ToggleButtonModel model = (ToggleButtonModel) toggle.getModel();
                selectedTool.put(model.getGroupName(), actionCommand);
            } catch (ClassCastException ex) {
            } catch (NullPointerException ex) {
            }

        } catch (Throwable ex) {
            logger.error("Can't perform action '" + actionName + "'.", ex);
        }

        enableControls();
        showMemory();
    }

    private String getName(String name, PluginClassLoader loader) {
        if ( name.indexOf('.') == -1 ) {
            return loader.getPluginName() + "." + name;
        } else {
            return name;
        }
    }

    public void addPopupMenu(PluginClassLoader loader, PopupMenu menu) {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            throw new RuntimeException("No Event Dispatch Thread");
        }

        String name = getName(menu.getName(), loader);

        // Se crea el control popupmenu
        JPopUpMenu popupMenu = (JPopUpMenu) popupMap.get(name);

        if ( popupMenu == null ) {
            popupMenu = new JPopUpMenu(menu.getName());
            popupMap.put(name, popupMenu);
        }

        Menu[] menues = menu.getMenu();
        for ( int i = 0; i < menues.length; i++ ) {
            JMenuItem nuevoMenu = createJMenuItem(loader, menues[i]);
            popupMenu.add(nuevoMenu);
        }
    }

    private JMenuItem createJMenuItem(PluginClassLoader loader, Menu menu) {
        JMenuItem nuevoMenu = null;

        I18nManager i18nManager = ToolsLocator.getI18nManager();

        String text = menu.getText();
        int n = text.lastIndexOf('/');
        if ( n >= 0 ) {
            text = text.substring(n + 1);
        }
        String translatedText = i18nManager.getTranslation(text);

        IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getCurrent();
        if ( menu.getIcon() != null ) {
            if ( iconTheme.exists(menu.getIcon()) ) {
                ImageIcon image = iconTheme.get(menu.getIcon());
                nuevoMenu = new JMenuItem(translatedText, image);
            } else {
                nuevoMenu = new JMenuItem(translatedText);
                logger.info("menu icon '" + menu.getIcon() + "' not exists.");
            }
        } else {
            nuevoMenu = new JMenuItem(translatedText);
        }
        nuevoMenu.setTextKey(text);
        nuevoMenu.setName(menu.getName());

        if ( menu.getKey() != null ) {
            nuevoMenu.setAccelerator(KeyMapping.getKeyStroke(menu.getKey()));
        }

        nuevoMenu.setActionCommand(menu.getActionCommand());

        if ( menu.getTooltip() != null ) {
            nuevoMenu.setToolTip(i18nManager.getTranslation(menu.getTooltip()));
            nuevoMenu.setToolTipKey(menu.getTooltip());
        }

        if ( menu.getEnableText() != null ) {
            nuevoMenu.setEnableText(i18nManager.getTranslation(menu.getEnableText()));
        }

        nuevoMenu.setEnabled(true);
        nuevoMenu.setVisible(true);

        if ( menu.getName() != null ) {
            ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
            final ActionInfo actionInfo = actionManager.getAction(menu.getName());
            if ( actionInfo != null ) {
                nuevoMenu.addActionListener(actionInfo);
            }
        }
        return nuevoMenu;
    }

    private JMenuItem createJMenuItem(ActionInfo action, String text) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        String label = null;

        if ( text == null ) {
            label = action.getLabel();
        } else if ( text.contains("/") ) {
            String[] ss = text.split("/");
            label = ss[ss.length - 1];
        } else {
            label = text;
        }
        JMenuItem nuevoMenu = new JMenuItem();
        nuevoMenu.setTextKey(label);
        nuevoMenu.setText(i18nManager.getTranslation(label));
        nuevoMenu.setName(action.getName());
        if ( action.getIconName() != null ) {
            nuevoMenu.setIcon(action.getIcon());
        }
        KeyStroke key = action.getKeyStroke();
        if ( key != null ) {
            nuevoMenu.setAccelerator(key);
        }
        nuevoMenu.setActionCommand(action.getCommand());
        if ( action.getTooltip() != null ) {
            nuevoMenu.setToolTip(i18nManager.getTranslation(action.getTooltip()));
            nuevoMenu.setToolTipKey(action.getTooltip());
        }
        nuevoMenu.setEnabled(true);
        nuevoMenu.setVisible(true);
        nuevoMenu.addActionListener(action);
        return nuevoMenu;
    }

    /**
     * Muestra u oculta el menu de nombre 'name'
     *
     * @param name
     * Nombre del menu que se quiere mostrar
     * @param x
     * Evento de raton
     * @param y
     * @param c
     */
    private void showPopupMenu(String name, int x, int y, Component c) {
        JPopupMenu menu = (JPopupMenu) popupMap.get(name);

        if ( menu != null ) {
            menu.show(c, x, y);
        }
    }

    public void removePopupMenuListener(String name, ActionListener listener) {
        JPopupMenu menu = (JPopupMenu) popupMap.get(name);

        if ( menu != null ) {
            Component[] jmenuitems = menu.getComponents();

            for ( int i = 0; i < jmenuitems.length; i++ ) {
                if ( jmenuitems[i] instanceof JMenuItem ) {
                    ((JMenuItem) jmenuitems[i]).removeActionListener(listener);
                }
            }
        }
    }

    public void addPopupMenuListener(String popupName, Component c,
            ActionListener listener, PluginClassLoader loader) {
        final String name = getName(popupName, loader);

        JPopupMenu menu = (JPopupMenu) popupMap.get(name);

        if ( menu != null ) {
            Component[] jmenuitems = menu.getComponents();

            for ( int i = 0; i < jmenuitems.length; i++ ) {
                if ( jmenuitems[i] instanceof JMenuItem ) {
                    ((JMenuItem) jmenuitems[i]).addActionListener(listener);
                }
            }
        }

        c.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if ( e.isPopupTrigger() ) {
                    showPopupMenu(name, e.getX(), e.getY(), e.getComponent());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if ( e.isPopupTrigger() ) {
                    showPopupMenu(name, e.getX(), e.getY(), e.getComponent());
                }
            }
        });
    }

    /**
     * Loop on the controls to enable/disable and show/hide them, according to
     * its associated action (ActionInfo)
     *
     * @throws RuntimeException
     */
    public void enableControls() {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    enableControls();
                }
            });
            return;
        }

        ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
        ActionInfoStatusCache cache = actionManager.createActionStatusCache();

        // Enable or disable controls, according to its associated extensions
        Iterator<JComponent> e = controlsIterator();

        while ( e.hasNext() ) {
            JComponent control = (JComponent) e.next();
            String actionName = control.getName();
            ActionInfo action = actionManager.getAction(actionName);
            try {
                boolean enabled = false;
                boolean visible = false;

                if ( action == null ) {
                    if( control instanceof DropDownButton ) {
                        DropDownButton dropDownButton = (DropDownButton)control;
                        visible = !dropDownButton.isAllHiden();
                        enabled = !dropDownButton.isAllDisabled();
                        if (visible && enabled){
                            dropDownButton.updateMainButton();
                        }
                    } else if( control instanceof JMenuTraslatable ) {
                        enabled = true;
                        visible = true;
                    } else {
                        IExtension extension = this.control2extensions.get(control);
                        if( extension!=null ) {
                            enabled = extension.isEnabled();
                            visible = extension.isVisible();
                        } else {
                            logger.warn("Can't enable/show control '"+control.getClass().getName()+"/"+control.getName()+".");
                            enabled = true;
                            visible = true;
                        }
                    }
                } else {
                    enabled = false;
                    visible = cache.isVisible(action);
                    if ( visible ) {
                        enabled = cache.isEnabled(action);
                    }
                }
                //logger.info(control.getClass().getName() +" - " +control.getName() + " visible " + visible );
                control.setEnabled(enabled);
                control.setVisible(visible);
            } catch (Throwable ex) {
                // Catch exceptions and errors (class not found)
                logger.info("Can't enable/show control '" + actionName + "'.", ex);
                this.message("Can't enable/show control '" + actionName + "'.", JOptionPane.ERROR_MESSAGE);
                try {
                    control.setEnabled(false);
                    control.setVisible(false);
                } catch (Exception ex2) {
                    // Ignore errors
                }
            }
        }

        // Loop in the menus to hide the menus that don't have visible children
        for ( int i = 0; i < menuBar.getMenuCount(); i++ ) {
            MenuElement menu = menuBar.getMenu(i);
            hideMenus(menu);
            if ( menu instanceof JMenu ) {
                // hide (ugly) redundant separators and assign keyboard
                // mnemonics
                Component[] comps = ((JMenu) menu).getMenuComponents();
                // mnemonics have to be unique for each top-level menu
                char mnemonics[] = new char[comps.length];
                if ( comps.length > 0 ) {
                    // Set keyboard mnemonic for this top-level entry
                    String text = ((JMenu) menu).getText();
                    char mnemonic = getMnemonic(text, mnemonics);
                    if ( ' ' != mnemonic ) {
                        ((JMenu) menu).setMnemonic(mnemonic);
                        mnemonics[0] = mnemonic;
                    }
                }
                // now go through all entries in this menu, hid
                // separators if necessary and assing remaining mnemonics
                hideSeparatorsAndMakeMnemonics(menu, mnemonics);
            }
        }

        // hide the toolbars that don't contain any visible tool
        Iterator it = toolBarMap.values().iterator();

        while ( it.hasNext() ) {
            JComponent t = (JComponent) it.next();
            boolean todosOcultos = true;

            for ( int i = 0; i < t.getComponentCount(); i++ ) {
                if ( !(t.getComponent(i) instanceof JSeparator) // separators
                        // don't matter
                        && t.getComponent(i).isVisible() ) {
                    todosOcultos = false;
                }
            }

            if ( todosOcultos ) {
                t.setVisible(false);
            } else {
                if ( t instanceof SelectableToolBar ) {
                    t.setVisible(((SelectableToolBar) t).getAndamiVisibility());
                } else {
                    t.setVisible(true);
                }
            }
        }

        if ( mdiManager != null ) {
            JPanel f = (JPanel) mdiManager.getActiveWindow();

            if ( f != null ) {
                if ( lastLabelClass != f.getClass() ) {
                    lastLabelClass = f.getClass();

                    Label[] lbls = (Label[]) classLabels.get(lastLabelClass);

                    if ( lbls != null ) {
                        bEstado.setLabelSet(lbls);
                    }
                }
            }
        }

        ajustarToolBar();

        showMemory();
    }

    public synchronized void refreshControls() {
        if ( !SwingUtilities.isEventDispatchThread() ) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    refreshControls();
                }
            });
            return;
        }

        if(refreshingControls){
            return;
        }
        try {
        refreshingControls = true;
        ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
        ActionInfoStatusCache cache = actionManager.createActionStatusCache();
        IconTheme icontheme = ToolsSwingLocator.getIconThemeManager().getCurrent();

        Iterator e = controlsIterator();

        while ( e.hasNext() ) {
            JComponent control = (JComponent) e.next();
            if ( control instanceof TranslatableButton ) {
                ((TranslatableButton) control).translate();
            }
            String actionlName = control.getName();
            ActionInfo action = actionManager.getAction(actionlName);
            if ( action != null ) {
                if ( control instanceof AbstractButton ) {
                    AbstractButton button = (AbstractButton) control;
                    if ( control instanceof javax.swing.JMenuItem ) {
                        if ( action.getIconName() != null && action.getIconName().length() > 0 ) {
                            if ( icontheme.exists(action.getIconName()) ) {
                                button.setIcon(action.getIcon());
                            }
                        }
                    } else {
                        button.setIcon(action.getIcon());
                    }
                }
            }
        }

        enableControls();
        } finally {
            refreshingControls = false;
        }
    }

    public void message(String msg, int messageTyoe) {
        this.getStatusBar().message(msg, messageTyoe);
    }

    /**
     * Establece la visibilidad de un menu y todos sus descendientes en la
     * jerarquia teniendo en cuenta la visibilidad de todos los submenus.
     *
     * @param menu
     * Menu que se quiere visualizar
     *
     * @return Devuelve true si el menu es visible y false en caso contrario
     */
    private boolean hideMenus(MenuElement menu) {
        MenuElement[] submenus = menu.getSubElements();

        // Si no tiene hijos se devuelve su visibilidad
        if ( submenus.length == 0 ) {
            return menu.getComponent().isVisible();
        }

        /*
         * Si tiene hijos se devuelve true si algï¿½no de ellos es visible,
         * pero se itera por todos ellos
         */
        boolean visible = false;

        for ( int i = 0; i < submenus.length; i++ ) {
            if ( hideMenus(submenus[i]) ) {
                if ( !(menu instanceof JPopupMenu) ) {
                    menu.getComponent().setVisible(true);
                }

                visible = true;
            }
        }

        if ( visible ) {
            return true;
        }

        menu.getComponent().setVisible(false);

        return false;
    }

    /**
     *
     * Recurse through all menu elements and make sure there are no
     * redundant separators.
     * This method will make sure that a separator only becomes visible
     * if at least one visible non-separator menu entry preceeded it.
     *
     *
     */
    private void hideSeparatorsAndMakeMnemonics(MenuElement menu,
            char[] mnemonics) {
        // flag that indicates whether a separator is to be displayed or not
        boolean allowSeparator;

        allowSeparator = false; // separator not allowed as very first menu item
        Component[] comps = ((JMenu) menu).getMenuComponents();
        if ( comps.length < 1 ) {
            // zero-length menu: skip
            return;
        }

        for ( int i = 0; i < comps.length; i++ ) {
            if ( comps[i] instanceof JSeparator ) {
                // got a separator: display only if allowed at this position
                if ( allowSeparator == true ) {
                    // look at all successive menu entries to make sure that at
                    // least one
                    // is visible and not a separator (otherwise, this separator
                    // would
                    // be the last visible item in this menu) -- we don't want
                    // that
                    comps[i].setVisible(false);
                    for ( int j = i; j < comps.length; j++ ) {
                        if ( !(comps[j] instanceof JSeparator) ) {
                            if ( comps[j].isVisible() ) {
                                comps[i].setVisible(true); // display separator!
                                break;
                            }
                        }
                    }
                } else {
                    comps[i].setVisible(false);
                }
                allowSeparator = false; // separator is not allowed right after
                // another separator
            } else {
                if ( comps[i] instanceof JMenu ) { // got a submenu: recurse
                    // through it
                    // get number of submenu components
                    Component[] scomps = ((JMenu) comps[i]).getMenuComponents();
                    // make a new, fresh array to hold unique mnemonics for this
                    // submenu
                    char[] smnemonics = new char[scomps.length];
                    hideSeparatorsAndMakeMnemonics(((MenuElement) comps[i]),
                            smnemonics);
                    if ( comps[i].isVisible() ) {
                        allowSeparator = true; // separators are OK after
                        // visible submenus
                        // Set keyboard mnemonic for this submenu
                        String text = ((JMenu) comps[i]).getText();
                        char mnemonic = getMnemonic(text, mnemonics);
                        if ( ' ' != mnemonic ) {
                            ((JMenu) comps[i]).setMnemonic(mnemonic);
                            mnemonics[i] = mnemonic;
                        }
                    }
                } else {
                    if ( comps[i].isVisible() ) {
                        if ( comps[i] instanceof JMenuItem ) {
                            // Set keyboard mnemonic for this menu item
                            String text = ((JMenuItem) comps[i]).getText();
                            char mnemonic = getMnemonic(text, mnemonics);
                            if ( ' ' != mnemonic ) {
                                ((JMenuItem) comps[i]).setMnemonic(mnemonic);
                                mnemonics[i] = mnemonic;
                            }
                        }
                        allowSeparator = true; // separators are OK after
                        // regular, visible entries
                    }
                }
            }
        }
    }

    /**
     * Helper functios for assigning a unique mnemomic char from
     * a pool of unassigned onces, stored in the array "mnemomnics"
     */
    private char getMnemonic(String text, char[] mnemonics) {
        if( text==null ) {
            return ' ';
        }
        Vector words = new Vector();
        StringTokenizer t = new StringTokenizer(text);
        int maxsize = 0;

        while ( t.hasMoreTokens() ) {
            String word = t.nextToken();
            if ( word.length() > maxsize ) {
                maxsize = word.length();
            }
            words.addElement(word);
        }
        words.trimToSize();

        for ( int i = 0; i < maxsize; ++i ) {
            char mnemonic = getMnemonic(words, mnemonics, i);
            if ( ' ' != mnemonic ) {
                return mnemonic;
            }
        }

        return ' ';
    }

    private char getMnemonic(Vector words, char[] mnemonics, int index) {
        int numwords = words.size();

        for ( int i = 0; i < numwords; ++i ) {
            String word = (String) words.elementAt(i);
            if ( index >= word.length() ) {
                continue;
            }

            char c = word.charAt(index);
            if ( !isMnemonicExists(c, mnemonics) ) {
                /* pick only valid chars */
                if ( (c != ':') && (c != '.') && (c != ',') && (c != ';')
                        && (c != '-') && (c != '+') && (c != '/') && (c != '\\')
                        && (c != '\'') && (c != '\"') && (c != ' ') && (c != '=')
                        && (c != '(') && (c != ')') && (c != '[') && (c != ']')
                        && (c != '{') && (c != '}') && (c != '$') && (c != '*')
                        && (c != '&') && (c != '%') && (c != '!') && (c != '?')
                        && (c != '#') && (c != '~') && (c != '_') ) {
                    return c;
                }
            }
        }
        return ' ';
    }

    private boolean isMnemonicExists(char c, char[] mnemonics) {
        int num = mnemonics.length;
        for ( int i = 0; i < num; ++i ) {
            if ( mnemonics[i] == c ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Muestra la memoria consumida por el programa
     */
    private void showMemory() {
        Runtime r = Runtime.getRuntime();
        long mem = r.totalMemory() - r.freeMemory();
        logger.debug(PluginServices.getText(this, "memory_usage") + " " + mem
                / 1024 + " KB");
    }

    public MDIManager getMDIManager() {
        return mdiManager;
    }

    public NewStatusBar getStatusBar() {
        return bEstado;
    }

    /**
     * You can use this function to select the appropiate
     * tool inside the toolbars
     */
    public void setSelectedTool(String actionCommand) {
        setSelectedTool(defaultGroup, actionCommand);
    }

    /**
     * You can use this function to select the appropiate
     * tool inside the toolbars
     */
    public void setSelectedTool(String groupName, String actionCommand) {
        ButtonGroup group = (ButtonGroup) buttonGroupMap.get(groupName);
        if ( group == null ) {
            return;
        }

        Enumeration enumeration = group.getElements();
        while ( enumeration.hasMoreElements() ) {
            AbstractButton button = (AbstractButton) enumeration.nextElement();
            if ( button.getActionCommand().equals(actionCommand) ) {
                button.setSelected(true);
            }
        }

        selectedTool.put(groupName, actionCommand);
    }

    /**
     * You can use this function to select the appropiate
     * tool inside the toolbars
     */
    public void setSelectedTools(Map selectedTools) {
        selectedTool = selectedTools;
        if ( selectedTools == null ) {
            return;
        }
        Iterator groupNames = selectedTools.keySet().iterator();
        while ( groupNames.hasNext() ) {
            try {
                String groupName = (String) groupNames.next();
                ButtonGroup group = (ButtonGroup) buttonGroupMap.get(groupName);
                Enumeration enumeration = group.getElements();
                String actionCommand = (String) selectedTools.get(groupName);
                if ( actionCommand == null ) {
                    continue;
                }
                while ( enumeration.hasMoreElements() ) {
                    AbstractButton button
                            = (AbstractButton) enumeration.nextElement();
                    if ( button.getActionCommand().equals(actionCommand) ) {
                        button.setSelected(true);
                    }
                }
            } catch (ClassCastException ex) {
                logger
                        .error("selectedTool should only contain pairs (String groupName, JToolBarToggleButton button)");
            }
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param clase
     * @param label
     */
    public void setStatusBarLabels(Class<?> clase, Label[] label) {
        classLabels.put(clase, label);
    }

    public void removeStatusBarLabels(Class<?> clase) {
        classLabels.remove(clase);
    }

    public void addStatusBarControl(Class<?> extensionClass, IControl control) {
        control.addActionListener(this);
        bEstado.addControl(control.getName(), (Component) control);
        if ( control instanceof JComponent ) {
            addControl((JComponent) control);
            PluginsManager pluginsManager = PluginsLocator.getManager();
            IExtension extension = pluginsManager.getExtension((Class<? extends IExtension>) extensionClass);
            this.control2extensions.put((JComponent)control, extension);
        }
    }

    public void addToolBarControl(Class<?> extensionClass, JToolBar control, String name) {
        toolBars.add(control);
        addControl(control);

        PluginsManager pluginsManager = PluginsLocator.getManager();
        IExtension extension = pluginsManager.getExtension((Class<? extends IExtension>) extensionClass);
        this.control2extensions.put(control, extension);
    }

    public void removeStatusBarControl(String name) {
        Component c = bEstado.removeControl(name);
        if ( c instanceof JComponent ) {
            removeControl((JComponent) c);
        }
    }

    public void removeMenu(Menu menu) {
        JMenuItem delete = (JMenuItem) infoCodedMenus.get(menu);

        if ( delete == null ) {
            throw new NoSuchElementException(menu.getText());
        }

        delete.getParent().remove(delete);
        infoCodedMenus.remove(menu);
    }

    public void addMenu(Menu menu, ActionListener listener,
            PluginClassLoader loader) {
        JMenu menuPadre = createMenuAncestors(menu, loader);

        // Se registra y añaade el menu
        JMenuItem nuevoMenu = createJMenuItem(loader, menu);
        nuevoMenu.addMouseListener(tooltipListener);
        if ( listener != null && menu.getName() != null && menu.getName().trim().length() > 0 ) {
            ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
            final ActionInfo actionInfo = actionManager.getAction(menu.getName());
            if ( actionInfo != null ) {
                nuevoMenu.removeActionListener(actionInfo);
            }
            nuevoMenu.addActionListener(listener);
        } else {
            /*
             * We also add action listener for menu with
             * no name but with text:
             *
             * Window/Project manager
             * Window/View - 1
             * Window/View - 2
             * etc
             */
            if ( listener != null && menu.getText() != null && menu.getText().trim().length() > 0 ) {
                nuevoMenu.addActionListener(listener);
            }

        }
        menuPadre.add(nuevoMenu);

        infoCodedMenus.put(menu, nuevoMenu);
    }

    public void changeMenuName(String[] menu, String newName,
            PluginClassLoader loader) {

        ArrayList menuList = new ArrayList();
        for ( int i = 0; i < menu.length; i++ ) {
            menuList.add(menu[i]);
        }

        javax.swing.JMenuItem newMenu = getMenu(menuList, menuBar);
        if ( newMenu == null ) {
            throw new NoSuchMenuException(menu[0]);
        } else {
            newMenu.setText(PluginServices.getText(this, newName));
        }
    }

    public void componentHidden(ComponentEvent arg0) {
    }

    public void componentMoved(ComponentEvent arg0) {
    }

    public void componentResized(ComponentEvent arg0) {
        ajustarToolBar();
    }

    public void componentShown(ComponentEvent arg0) {
    }

    public void componentAdded(ContainerEvent arg0) {
        ajustarToolBar();
    }

    public void componentRemoved(ContainerEvent arg0) {
        ajustarToolBar();
    }

    public class TooltipListener extends MouseAdapter {

        @Override
        public void mouseEntered(MouseEvent e) {
            JComponent control = (JComponent) e.getSource();
            EnableTextSupport ets = (EnableTextSupport) e.getSource();

            String texto = null;
            texto = control.getToolTipText();

            if ( texto != null ) {
                bEstado.setInfoTextTemporal(texto);
            }
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
            bEstado.restaurarTexto();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            bEstado.restaurarTexto();
        }
    }

    public String getTitlePrefix() {
        return titlePrefix;
    }

    public void setTitlePrefix(String titlePrefix) {
        this.titlePrefix = titlePrefix;
    }

    public Map getSelectedTools() {
        return selectedTool;
    }

    public HashMap getInitialSelectedTools() {
        return initialSelectedTools;
    }

    /**
     * Get a previously added JComponent by name.
     * For example you can use it if you need to obtain a JToolBar to
     * add some customized component.
     *
     * @param name
     * @return the JComponent or null if none has been found
     */
    public JComponent getComponentByName(String name) {
        Iterator e = controlsIterator();

        while ( e.hasNext() ) {
            JComponent control = (JComponent) e.next();
            String nameCtrl = control.getName();
            if ( nameCtrl != null ) {
                if ( nameCtrl.compareTo(name) == 0 ) {
                    return control;
                }
            }
        }
        Iterator it = toolBarMap.values().iterator();
        while ( it.hasNext() ) {
            JComponent t = (JComponent) it.next();
            String nameCtrl = t.getName();
            if ( nameCtrl != null ) {
                if ( nameCtrl.compareTo(name) == 0 ) {
                    return t;
                }
            }

        }

        return null;
    }

    public SelectableToolBar[] getToolbars() {
        return (SelectableToolBar[]) toolBarMap.values().toArray(
                new SelectableToolBar[0]);
    }

    public boolean getToolbarVisibility(String name) {
        JComponent component
                = PluginServices.getMainFrame().getComponentByName(name);
        if ( (component != null) && (component instanceof SelectableToolBar) ) {
            SelectableToolBar toolBar = (SelectableToolBar) component;
            return toolBar.getAndamiVisibility();
        }
        return false;
    }

    public boolean setToolbarVisibility(String name, boolean visibility) {
        JComponent component
                = PluginServices.getMainFrame().getComponentByName(name);
        if ( (component != null) && (component instanceof SelectableToolBar) ) {
            SelectableToolBar toolBar = (SelectableToolBar) component;
            boolean oldVisibility = toolBar.getAndamiVisibility();
            toolBar.setAndamiVisibility(visibility);
            enableControls();
            return oldVisibility;
        }
        return false;
    }

    public javax.swing.JMenuItem getMenuEntry(String[] menuPath) {
        ArrayList menu = new ArrayList();
        for ( int i = 0; i < menuPath.length; i++ ) {
            menu.add(menuPath[i]);
        }
        return getMenu(menu, menuBar);
    }

    public void messageDialog(String message, String title, int messageType) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        helper.messageDialog(message, title, messageType);
    }

    public void messageDialog(String message, String[] messageArgs,
            String title, int messageType) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        helper.messageDialog(message, messageArgs, title, messageType);
    }

    public int confirmDialog(String message, String title, int optionType,
            int messageType) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.confirmDialog(message, title, optionType, messageType);
    }

    public String inputDialog(String message, String title, int messageType,
            String initialValue) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.inputDialog(message, title, messageType, initialValue);
    }

    public String inputDialog(String message, String title) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.inputDialog(message, title);
    }

    public void showDialog(Component contents, String title) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        helper.showDialog(contents, title);
    }

    public Component createComponent(Class<? extends Component> theClass,
            Object... parameters) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.createComponentWithParams(theClass, parameters);
    }

    public Component createComponentWithParams(
            Class<? extends Component> theClass, Object[] parameters) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.createComponentWithParams(theClass, parameters);
    }

    public File[] showChooserDialog(
            final String title,
            final int type, // SAVE_DIALOG / OPEN_DIALOG
            final int selectionMode, //    JFileChooser.FILES_ONLY, JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES
            final boolean multiselection,
            final File initialPath,
            final FileFilter filter,
            final boolean fileHidingEnabled
    ) {
        DefaultThreadSafeDialogs helper = new DefaultThreadSafeDialogs(this, this.bEstado);
        return helper.showChooserDialog(title, type, selectionMode, multiselection, initialPath, filter, fileHidingEnabled);
    }

    public File[] showOpenDirectoryDialog(String title, File initialPath) {
        return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.DIRECTORIES_ONLY, false, initialPath, null, false);
    }

    public File[] showOpenFileDialog(String title, File initialPath) {
        return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
    }

    public File[] showSaveFileDialog(String title, File initialPath) {
        return showChooserDialog(title, JFileChooser.SAVE_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
    }

    private void addControl(JComponent control) {
        controls.add(control);
    }

    private void removeControl(JComponent control) {
        controls.remove(control);
    }

    private Iterator<JComponent> controlsIterator() {
        return this.controls.iterator();
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale); //To change body of generated methods, choose Tools | Templates.
        MDIManager mdiManager = MDIManagerFactory.createManager();
        mdiManager.setLocale(locale);
        if ( this.controls != null && !this.controls.isEmpty() ) {
            this.refreshControls();
        }
    }

}
