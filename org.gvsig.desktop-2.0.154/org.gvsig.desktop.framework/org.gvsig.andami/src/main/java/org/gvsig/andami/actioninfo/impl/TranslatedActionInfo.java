package org.gvsig.andami.actioninfo.impl;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.tools.ToolsLocator;


public class TranslatedActionInfo implements ActionInfo {

    private ActionInfo actionInfo;

    TranslatedActionInfo(ActionInfo actionInfo) {
        this.actionInfo = actionInfo;
    }

    public Object getValue(String key) {
        if (Action.SHORT_DESCRIPTION.equalsIgnoreCase(key)) {
            return this.getLabel();
        }
        if (ActionInfo.TOOLTIP.equalsIgnoreCase(key)) {
            return this.getTooltip();
        }

        return this.actionInfo.getValue(key);
    }

    public void putValue(String key, Object value) {
        this.actionInfo.putValue(key, value);

    }

    public void setEnabled(boolean b) {
        this.actionInfo.setEnabled(b);

    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.actionInfo.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.actionInfo.removePropertyChangeListener(listener);
    }

    public void actionPerformed(ActionEvent e) {
        this.actionInfo.actionPerformed(e);
    }

    public PluginServices getPlugin() {
        return this.actionInfo.getPlugin();
    }

    public IExtension getExtension() {
        return this.actionInfo.getExtension();
    }

    public String getPluginName() {
        return this.actionInfo.getPluginName();
    }

    public String getExtensionName() {
        return this.actionInfo.getExtensionName();
    }

    public String getName() {
        return this.actionInfo.getName();
    }

    public String getLabel() {
        return ToolsLocator.getI18nManager().getTranslation(this.actionInfo.getLabel());
    }

    public String getCommand() {
        return this.actionInfo.getCommand();
    }

    public ImageIcon getIcon() {
        return this.actionInfo.getIcon();
    }

    public String getIconName() {
        return this.actionInfo.getIconName();
    }

    public String getAccelerator() {
        return this.actionInfo.getAccelerator();
    }

    public KeyStroke getKeyStroke() {
        return this.actionInfo.getKeyStroke();
    }

    public String getTooltip() {
        return ToolsLocator.getI18nManager().getTranslation(this.actionInfo.getTooltip());
    }

    public long getPosition() {
        return this.actionInfo.getPosition();
    }

    public boolean isVisible() {
        return this.actionInfo.isVisible();
    }

    public boolean isEnabled() {
        return this.actionInfo.isEnabled();
    }

    public void execute() {
        this.actionInfo.execute();

    }

    public void execute(Object[] args) {
        this.actionInfo.execute(args);

    }

    public void execute(Map args) {
        this.actionInfo.execute(args);
    }

    public void execute(Object arg) {
        this.actionInfo.execute(arg);

    }

    public boolean isActive() {
        return this.actionInfo.isActive();
    }

    public void setActive(boolean active) {
        this.actionInfo.setActive(active);

    }

    public Collection<ActionInfo> getRedirections() {
        return this.actionInfo.getRedirections();
    }

    public Object clone() throws CloneNotSupportedException {
        TranslatedActionInfo other = (TranslatedActionInfo) super.clone();
        other.actionInfo = (ActionInfo) this.actionInfo.clone();
        return other;
    }
}
