/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.help;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.help.BadIDException;
import javax.help.HelpSet;
import javax.help.JHelp;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelpPanel extends JPanel  {

	private static final long serialVersionUID = 4164482247505362337L;

	public static int WIDTH = 645;
	public static int HEIGHT = 495;
	private String title = null;
	private JHelp jh;

	private Logger log() {
		return LoggerFactory.getLogger("org.gvsig");
	}

	public HelpPanel(HelpSet hs){
		log().info("HelpPanel(hs)");
		init(hs,null);
	}

	public HelpPanel(HelpSet hs, String id){
		log().info("HelpPanel(hs,id) id="+id);
		init(hs,id);
	}

	private void init(HelpSet hs, String id){
		jh = new JHelp(hs);
		log().info("init() ID "+ id);
		if (id != null) {
			try {
				log().info("init() setCurrentID "+ id);
				jh.setCurrentID(id);
			} catch (BadIDException ex) {
				log().error(ex.toString());

			} catch (NullPointerException ex) {
				log().error(ex.toString());
			}
		}
		String hsTitle = hs.getTitle();
		if (hsTitle == null || hsTitle.equals("")) {
			hsTitle = "gvSIG Help";
		}
		title = hsTitle;
		setLayout(new BorderLayout());
		add(jh,BorderLayout.CENTER);
	}

	public void showWindow() {

		log().info("showWindow()");
		Frame frame = new Frame();
        frame.add(this);
        frame.setSize(WIDTH, HEIGHT + 30);
        frame.setTitle(getTitle());
        frame.setResizable(true);
		this.setVisible(true);
		frame.show();
	}

	public void showWindow(String id) {
		if (id != null) {
			try {
				log().info("showWindow(id) -> setCurrentID "+ id);
				jh.setCurrentID(id);
			} catch (BadIDException ex) {
				log().error(ex.toString());
			}
		}
		String hsTitle = jh.getHelpSetPresentation().getTitle();
		if (hsTitle == null || hsTitle.equals("")) {
			hsTitle = "gvSIG Help";
		}
		title = hsTitle;
		showWindow();
	}

	public String getTitle() {
		return this.title;
	}

}
