package org.gvsig.andami.firewall;

import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.prefs.Preferences;
import org.apache.commons.lang3.StringUtils;

public class DefaultFirewallConfiguration implements FirewallConfiguration {

    private static Preferences prefs = Preferences.userRoot().node("gvsig.connection");

    private List<String> nomProxyHosts = null;
    private URL host = null;

    private static final class ProxyAuth extends Authenticator {

        private PasswordAuthentication auth;

        private ProxyAuth(String user, String pass) {
            auth = new PasswordAuthentication(user, pass.toCharArray());
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return auth;
        }
    }

    public void apply() {

        Properties systemSettings = System.getProperties();
        if (this.isEnabled()) {
            if( this.getHost()!=null ) {
                if ("https://".equalsIgnoreCase(this.getHost().getProtocol())) {
                    systemSettings.put("https.proxyHost", this.getHost().getHost());
                    systemSettings.put("https.proxyPort", String.valueOf(this.getHost().getPort()));
                } else {
                    systemSettings.put("http.proxyHost", this.getHost().getHost());
                    systemSettings.put("http.proxyPort", String.valueOf(this.getHost().getPort()));
                }
            }
            systemSettings.put("http.proxyUserName", this.getUserName());
            systemSettings.put("http.proxyPassword", this.getPassword());
            systemSettings.put("http.nonProxyHosts", this.getNonProxyHostsAsString());
            if( this.isAuthenticate() ) {
                Authenticator.setDefault(
                        new ProxyAuth(
                                this.getUserName(), 
                                this.getPassword()
                        )
                );
            }
        } else {
            systemSettings.put("http.proxySet", "false");
            systemSettings.remove("http.proxyHost");
            systemSettings.remove("http.proxyPort");
            systemSettings.remove("http.proxyUserName");
            systemSettings.remove("http.proxyPassword");
            systemSettings.remove("http.nonProxyHosts");
        }
    }

    public boolean isAuthenticate() {
        if (StringUtils.isBlank(this.getUserName())
                || StringUtils.isBlank(this.getPassword())) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return prefs.getBoolean("firewall.http.enabled", false);
    }

    public void setEnabled(boolean enabled) {
        prefs.putBoolean("firewall.http.enabled", enabled);
    }
    
    public URL getHost() {
        if (this.host == null) {
            String host = prefs.get("firewall.socks.host", "");
            String port = prefs.get("firewall.socks.port", "");
            this.setHost(host, port);
        }
        return this.host;
    }
    
    public void setHost(URL host) {
        if (host == null) {
            prefs.put("firewall.socks.host", "");
            prefs.put("firewall.socks.port", "");
        } else {
            prefs.put("firewall.socks.host", host.getHost());
            prefs.put("firewall.socks.port", String.valueOf(host.getPort()));
        }
        this.host = host;
    }

    public boolean setHost(String host, String port) {
        StringBuilder b = new StringBuilder();
        URL hosturl = null;
        
        if (StringUtils.isBlank(host)) {
            hosturl = null;
        } else {
            if (!(host.toLowerCase().startsWith("http://")
                    || host.toLowerCase().startsWith("https://"))) {
                b.append("http://");
            }
            b.append(host);
            if (!StringUtils.isBlank(port)) {
                b.append(":");
                b.append(port);
            }
            try {
                hosturl = new URL(b.toString());
            } catch (MalformedURLException ex) {
                return false;
            }
        }
        this.setHost(hosturl);
        return true;
    }

    public String getUserName() {
        return prefs.get("firewall.http.user", "");
    }

    public String getPassword() {
        return prefs.get("firewall.http.password", "");
    }

    public void setUserName(String userName) {
        prefs.put("firewall.http.user", userName);
    }

    public void setPassword(String password) {
        prefs.put("firewall.http.password", password);
    }

    public List<String> getNonProxyHosts() {
        if (this.nomProxyHosts == null) {
            String s = prefs.get("firewall.http.nonProxyHosts", "");
            this.setNonProxyHosts(s);
        }
        return Collections.unmodifiableList(this.nomProxyHosts);
    }

    public String getNonProxyHostsAsString() {
        StringBuilder builder = new StringBuilder();

        this.getNonProxyHosts();
        for (int i = 0; i < this.nomProxyHosts.size(); i++) {
            if (builder.length() == 0) {
                builder.append("|");
            }
            builder.append(this.nomProxyHosts.get(i));
        }
        return builder.toString();
    }

    public void setNonProxyHosts(String nonProxyHosts) {
        this.nomProxyHosts = new ArrayList<String>();
        String ss[] = nonProxyHosts.split("|");
        for (int i = 0; i < ss.length; i++) {
            if (StringUtils.isNotBlank(ss[i])) {
                this.nomProxyHosts.add(ss[i]);
            }
        }
    }

    public void addNonProxyHost(String host) {
        this.getNonProxyHosts();
        this.nomProxyHosts.add(host);
    }
}
