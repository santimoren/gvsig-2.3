/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andami.plugins.config.generate;

import org.exolab.castor.xml.ValidationContext;
import org.exolab.castor.xml.ValidationException;

class MyFieldValidator extends org.exolab.castor.xml.FieldValidator {
	public void validate(Object arg0, ValidationContext arg1)
			throws ValidationException {
		try {
			super.validate(arg0, arg1);
		} catch (ValidationException e) {
			throw getException(e,arg0);
		}
	}
	
	public void validate(Object object) throws ValidationException {
		try {
			super.validate(object);
   		} catch (ValidationException e) {
			throw getException(e,object);
		}
	}
	
	private ValidationException getException(ValidationException ex, Object obj) {
		if( obj == null ) {
			return ex;
		}
		return new ValidationException( ex.getMessage() + ". " + obj.toString(), ex);
	}
}