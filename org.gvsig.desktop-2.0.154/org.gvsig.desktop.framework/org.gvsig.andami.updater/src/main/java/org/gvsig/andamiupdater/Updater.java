/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.andamiupdater;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import com.sardak.antform.AntForm;
import com.sardak.antform.AntMenu;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class Updater {

    private String[] args;

    /**
     * @param args
     */
    public Updater(String[] args) {
        this.args = args;
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Updater updater = new Updater(args);
        updater.copyFiles();
        updater.executeScripts();
        updater.removeAll();
        updater.launchApp(args);
    }

    private File getApplicationDirectory() {
        return new File("").getAbsoluteFile();
    }

    private File getPluginsDirectory() {
        return new File(getApplicationDirectory(), this.args[1]);
    }

    /**
     * @param args
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private void launchApp(String[] args) throws Exception {

        try {
            Class launcher_class = Class.forName("org.gvsig.andami.Launcher");
            Object launcher = launcher_class.newInstance();
            Method main = launcher_class.getMethod("main", new Class[]{String[].class});
            main.invoke(launcher, new Object[]{args});

        } catch (ClassNotFoundException e) {
            Class launcher_class = Class.forName("com.iver.andami.Launcher");
            Object launcher = launcher_class.newInstance();
            Method main = launcher_class.getMethod("main", new Class[]{String[].class});
            main.invoke(launcher, new Object[]{args});
        }
    }

    /**
     *
     */
    private void removeAll() {
        File updateDirectory = new File(getApplicationDirectory(), "update");
        if (updateDirectory.exists()) {
            deleteDir(updateDirectory);
        }

    }

    private boolean deleteDir(File folder) {
        if (folder == null) {
            return true;
        }
        try {
            if (folder.isDirectory()) {
                String[] children = folder.list();
                for (String children1 : children) {
                    boolean success = deleteDir(new File(folder, children1));
                    if (!success) {
                        return false;
                    }
                }
            }
            return folder.delete();
        } catch (Throwable th) {
            this.warning("Can't remove '" + folder.getAbsolutePath() + "'.", th);
            return false;
        }
    }

    /**
     * Reads every line of appDirectory/update/scripts.lst and executes the ant
     * file that this line points to
     */
    private void executeScripts() {
        File updateFolder = new File(getApplicationDirectory(), "update");
        File antFilesScript = new File(updateFolder, "scripts.lst");

        if (antFilesScript.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(antFilesScript));
                String line;
                while ((line = reader.readLine()) != null) {
                    File antFile = new File(line);
                    try {
                        executeAntFile(antFile);
                    } catch (Throwable th) {
                        this.warning("Error running file '" + antFile.getAbsolutePath() + "'.", th);
                    }
                }
            } catch (Throwable th) {
                this.warning("Error running ant scripts.", th);
            } finally {
                closeQuietly(reader, antFilesScript);
            }

        }

    }

    /**
     * Copies all files and folders from gvSIG/update to gvSIG/extensions
     *
     * @throws IOException
     *
     */
    private void copyFiles() throws IOException {
        File updateFolder = new File(getApplicationDirectory(), "update");
        File filesFolder = new File(updateFolder, "files");
        File pluginsDirectory = getPluginsDirectory();

        if (updateFolder.exists()) {
            String[] childrenDirs = filesFolder.list();

            if (childrenDirs != null) {
                for (String childrenDir : childrenDirs) {
                    File sourceLocation = new File(filesFolder, childrenDir);
                    File targetLocation = new File(pluginsDirectory, childrenDir);
                    try {
                        deleteDir(targetLocation);
                        copyFilesAndFolders(sourceLocation, targetLocation);
                    } catch (Throwable th) {
                        this.warning("Can't copy files to '" + targetLocation.getAbsolutePath() + "'.", th);
                    }
                }
            }
        }
    }

    private boolean copyFilesAndFolders(File source, File target)
            throws IOException {

        if (source.isDirectory()) {
            if (!target.exists()) {
                target.mkdir();
            }

            String[] children = source.list();
            if (children != null) {
                for (String children1 : children) {
                    copyFilesAndFolders(
                            new File(source, children1),
                            new File(target, children1)
                    );
                }
            }
        } else {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(source);
                out = new FileOutputStream(target);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            } catch (Throwable th) {
                this.warning("Can't copy '" + source + "' to '" + target + "'.", th);
            } finally {
                closeQuietly(in, source);
                closeQuietly(out, target);
            }
        }

        return true;
    }

    private void closeQuietly(Closeable closeable, File f) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException ex) {
            if (f == null) {
                this.warning("Can't close file '" + closeable.toString() + "'.", ex);
            } else {
                this.warning("Can't close file '" + f.getAbsolutePath() + "'.", ex);
            }
        }
    }

    private void executeAntFile(File file) {
        Project p = new Project();
        p.setUserProperty("ant.file", file.getAbsolutePath());
        p.setUserProperty("gvsig_dir", getApplicationDirectory().getAbsolutePath());
        p.setUserProperty("extensions_dir", getPluginsDirectory().getAbsolutePath());
        p.init();
        p.setBaseDir(file.getParentFile());
        p.addTaskDefinition("antform", AntForm.class);
        p.addTaskDefinition("antmenu", AntMenu.class);
        p.init();
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        p.addReference("ant.projectHelper", helper);
        helper.parse(p, file);
        p.executeTarget(p.getDefaultTarget());
    }

    private void warning(String message, Throwable th) {
        System.err.println(message);
        th.printStackTrace();
    }

}
