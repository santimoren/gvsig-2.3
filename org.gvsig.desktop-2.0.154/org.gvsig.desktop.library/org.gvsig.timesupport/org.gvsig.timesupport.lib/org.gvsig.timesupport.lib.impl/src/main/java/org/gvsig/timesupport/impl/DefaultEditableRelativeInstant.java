/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import org.joda.time.MutableDateTime;

import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.EditableRelativeInstant;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class DefaultEditableRelativeInstant extends DefaultRelativeInstant implements EditableRelativeInstant{
    private org.joda.time.MutableDateTime jodaMutableDateTime;

    DefaultEditableRelativeInstant(MutableDateTime jodaMutableDateTime, Chronology chronology) {
        super(jodaMutableDateTime.getMillis(), chronology);
        this.jodaMutableDateTime = jodaMutableDateTime;       
    }

    public void setTime(int hour, int minuteOfHour, int secondOfMinute,
        int millisOfSecond) {
        jodaMutableDateTime.setTime(hour, minuteOfHour, secondOfMinute, millisOfSecond); 
    }

    public void setDate(int year, int monthOfYear, int dayOfMonth) {
        jodaMutableDateTime.setDate(year, monthOfYear, dayOfMonth);        
    }

    public void addYears(int years) {
        jodaMutableDateTime.addYears(years);
    }

    public void addDays(int days) {
        jodaMutableDateTime.addDays(days);        
    }

    public void addHours(int hours) {
        jodaMutableDateTime.addHours(hours);        
    }

    public void addMinutes(int minutes) {
        jodaMutableDateTime.addMinutes(minutes);        
    }

    public void addSeconds(int seconds) {
        jodaMutableDateTime.addSeconds(seconds);        
    }

    public void addMillis(int millis) {
        jodaMutableDateTime.addMillis(millis);        
    }  
        
    public String toString() {      
        return jodaMutableDateTime.toString();
    }  
}
