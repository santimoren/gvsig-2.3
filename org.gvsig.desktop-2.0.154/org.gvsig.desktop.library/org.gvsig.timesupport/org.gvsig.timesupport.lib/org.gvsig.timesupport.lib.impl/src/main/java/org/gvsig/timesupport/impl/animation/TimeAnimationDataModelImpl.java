/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl.animation;

import java.util.Observable;

import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;

/**
 * Data model for animation 
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 * 
 */
public class TimeAnimationDataModelImpl extends Observable implements TimeAnimationDataModel {
	private RelativeInstant  initTime        = null;
	private RelativeInstant  endTime         = null;
	private RelativeInstant  initWindow      = null;
	private RelativeInstant  endWindow       = null;
	private RelativeInstant  position        = null;
	private int              iterations      = 0;
	private int              speed           = 0;
	private int              timeStep        = 0;
	private int              windowTime      = 0;
	private int              windowTimeScale = 0;
	private int              timeStepScale   = 0;
	private int              loop            = 0;

	public void setTimeWindow(RelativeInstant start, RelativeInstant end) {
		this.initWindow = start;
		this.endWindow = end;
	}
	
	public RelativeInstant getEndTimeWindow() {
		return endWindow;
	}

	public RelativeInstant getInitTimeWindow() {
		return initWindow;
	}
	
	//*********************************
	
	public void setTimeInterval(RelativeInstant start, RelativeInstant end) {
		this.initTime = start;
		this.endTime = end;
	}
	
	public RelativeInstant getEndTime() {
		return endTime;
	}

	public RelativeInstant getInitTime() {
		return initTime;
	}

	//*********************************
	
	public int getIterations() {
		return iterations;
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setTimeStep(int timeStep) {
		this.timeStep = timeStep;
	}
	
	public int getTimeStep() {
		return timeStep;
	}

	public void setWindowTime(int windowTime) {
		this.windowTime = windowTime;
	}
	
	public int getWindowTime() {
		return windowTime;
	}
	
	public void setLoop(int loop) {
		this.loop = loop;
	}
	
	public int getLoop() {
		return loop;
	}
	
	public void setInitTime(RelativeInstant initTime) {
		this.initTime = initTime;
	}

	public void setEndTime(RelativeInstant endTime) {
		this.endTime = endTime;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getWindowTimeScale() {
		return windowTimeScale;
	}

	public void setWindowTimeScale(int windowTimeScale) {
		this.windowTimeScale = windowTimeScale;
	}

	public int getTimeStepScale() {
		return timeStepScale;
	}

	public void setTimeStepScale(int timeStepScale) {
		this.timeStepScale = timeStepScale;
	}
	
	public RelativeInstant getSliderPosition() {
		return position;
	}

	public void setSliderPosition(RelativeInstant position) {
		this.position = position;
		updateObservers();
	}
	
	public int[] getWindowTimeByPartOfData() {
		int[] value = new int[7];
		if(windowTimeScale == TimeAnimation.MILLISECOND) {
			value[6] = windowTime % 1000;
			if(windowTime > 999) {
				windowTimeScale = TimeAnimation.SECOND;
				windowTime = ((windowTime - value[6]) / 1000);
			}
		}
		if(windowTimeScale == TimeAnimation.SECOND) {
			value[5] = windowTime % 60;
			if(windowTime > 59) {
				windowTimeScale = TimeAnimation.MINUTE;
				windowTime = ((windowTime - value[5]) / 60);
			}
		}
		if(windowTimeScale == TimeAnimation.MINUTE) {
			value[4] = windowTime % 60;
			if(windowTime > 59) {
				windowTimeScale = TimeAnimation.HOUR;
				windowTime = ((windowTime - value[4]) / 60);
			}
		}
		if(windowTimeScale == TimeAnimation.HOUR) {
			value[3] = windowTime % 24;
			if(windowTime > 23) {
				windowTimeScale = TimeAnimation.DAY;
				windowTime = ((windowTime - value[3]) / 24);
			}
		}
		
		if(windowTimeScale == TimeAnimation.WEEK) {
			windowTimeScale = TimeAnimation.DAY;
			windowTime *= 7;
		}
		
		if(windowTimeScale == TimeAnimation.DAY) {
			//Meses de 31 dias
			int t = 31;
			
			//Meses de 30 dias
			if(value[1] == 4 || value[1] == 6 || value[1] == 9 || value[1] == 11) 
				t = 30;
			
			//Meses de 29 dias
			if(value[1] == 2 && (value[0] % 4) == 0) 
				t = 29;
			
			//Meses de 28 dias
			if(value[1] == 2 && (value[0] % 4) != 0) 
				t = 28;
			
			value[2] = windowTime % t;
			if(windowTime > t) {
				windowTimeScale = TimeAnimation.MONTH;
				windowTime = ((windowTime - value[2]) / t);
			}
		}
		if(windowTimeScale == TimeAnimation.MONTH) {
			value[1] = windowTime % 12;
			if(windowTime > 11) {
				windowTimeScale = TimeAnimation.YEAR;
				windowTime = ((windowTime - value[1]) / 12);
			}
		}
		if(windowTimeScale == TimeAnimation.YEAR) {
			value[0] = windowTime;
		}
		return value;
	}
	
	public int[] getStepByPartOfData() {
		int[] value = new int[7];
		if(timeStepScale == TimeAnimation.MILLISECOND) {
			value[6] = timeStep % 1000;
			if(timeStep > 999) {
				timeStepScale = TimeAnimation.SECOND;
				timeStep = ((timeStep - value[6]) / 1000);
			}
		}
		if(timeStepScale == TimeAnimation.SECOND) {
			value[5] = timeStep % 60;
			if(timeStep > 59) {
				timeStepScale = TimeAnimation.MINUTE;
				timeStep = ((timeStep - value[5]) / 60);
			}
		}
		if(timeStepScale == TimeAnimation.MINUTE) {
			value[4] = timeStep % 60;
			if(timeStep > 59) {
				timeStepScale = TimeAnimation.HOUR;
				timeStep = ((timeStep - value[4]) / 60);
			}
		}
		if(timeStepScale == TimeAnimation.HOUR) {
			value[3] = timeStep % 24;
			if(timeStep > 23) {
				timeStepScale = TimeAnimation.DAY;
				timeStep = ((timeStep - value[3]) / 24);
			}
		}
		if(timeStepScale == TimeAnimation.WEEK) {
			timeStepScale = TimeAnimation.DAY;
			timeStep *= 7;
		}
		if(timeStepScale == TimeAnimation.DAY) {
			//Meses de 31 dias
			int t = 31;
			
			//Meses de 30 dias
			if(value[1] == 4 || value[1] == 6 || value[1] == 9 || value[1] == 11) 
				t = 30;
			
			//Meses de 29 dias
			if(value[1] == 2 && (value[0] % 4) == 0) 
				t = 29;
			
			//Meses de 28 dias
			if(value[1] == 2 && (value[0] % 4) != 0) 
				t = 28;
			
			value[2] = timeStep % t;
			if(timeStep > t) {
				timeStepScale = TimeAnimation.MONTH;
				timeStep = ((timeStep - value[2]) / t);
			}
		}
		if(timeStepScale == TimeAnimation.MONTH) {
			value[1] = timeStep % 12;
			if(timeStep > 11) {
				timeStepScale = TimeAnimation.YEAR;
				timeStep = ((timeStep - value[1]) / 12);
			}
		}
		if(timeStepScale == TimeAnimation.YEAR) {
			value[0] = timeStep;
		}
		return value;
	}

	/**
	 * Actualiza datos y llama al update de los observadores
	 */
	public void updateObservers() {
		setChanged();
		notifyObservers();
	}

}
