/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import java.util.Date;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.EditableRelativeInstant;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.timesupport.Time;

/**
 * @author gvSIG team
 * @version $Id$
 */
public class DefaultRelativeInstant implements RelativeInstant{
    private org.joda.time.DateTime jodaDateTime = null;
    private Chronology chronology = null;       
    
    DefaultRelativeInstant(long instant, Chronology chronology) {
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(instant, ((DefaultChronology)chronology).getJodaType());
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(Chronology chronology) {
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(((DefaultChronology)chronology).getJodaType());   
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(Chronology chronology,  org.joda.time.DateTime dateTime ) {
        super();    
        this.jodaDateTime = dateTime; 
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(Chronology chronology,  org.joda.time.Period period ) {
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(period.getYears(), period.getMonths(), period.getDays(),
            period.getHours(), period.getMinutes(), period.getSeconds(), period.getMillis(), ((DefaultChronology)chronology).getJodaType()); 
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(Chronology chronology,  AbsoluteInstant absoluteInstant) {
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(absoluteInstant.getYears(), absoluteInstant.getMonths(), absoluteInstant.getDays(),
            absoluteInstant.getHours(), absoluteInstant.getMinutes(), absoluteInstant.getSeconds(), absoluteInstant.getMillis(), ((DefaultChronology)chronology).getJodaType()); 
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(Date date, Chronology chronology) {
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(date, ((DefaultChronology)chronology).getJodaType());        
        this.chronology = chronology;
    }
    
    DefaultRelativeInstant(org.joda.time.DateTime jodaDateTime, Chronology chronology){
        super();    
        this.jodaDateTime = jodaDateTime; 
        this.chronology = chronology;
    }

    DefaultRelativeInstant(int year, int monthOfYear, int dayOfMonth, int hourOfDay,
        int minuteOfHour, int secondOfMinute, int millisOfSecond, Chronology chronology){
        super();    
        this.jodaDateTime = new org.joda.time.DateTime(year, monthOfYear, dayOfMonth, hourOfDay,
            minuteOfHour, secondOfMinute, millisOfSecond, ((DefaultChronology)chronology).getJodaType());          
        this.chronology = chronology;
    }

    public int getHourOfDay() {
       return jodaDateTime.getHourOfDay();
    }

    public int getMinuteOfHour() {
        return jodaDateTime.getMinuteOfHour();
    }

    public int getSecondOfMinute() {
       return jodaDateTime.getSecondOfMinute();
    }

    public int getMinuteOfDay() {
        return jodaDateTime.getMinuteOfDay();
    }

    public int getSecondOfDay() {
        return jodaDateTime.getSecondOfDay();
    }

    public EditableRelativeInstant getEditableCopy() {    
        return new DefaultEditableRelativeInstant(jodaDateTime.toMutableDateTime(), chronology);
    }

    public long toMillis() {
        return jodaDateTime.getMillis();
    }  
    
    public String toString(String pattern) {
        return jodaDateTime.toString(pattern);
    }

    public Date toDate() {       
        return jodaDateTime.toDate();
    }

    public boolean isEqual(Instant instant) {       
        return jodaDateTime.isEqual(((DefaultRelativeInstant)instant).getJodaType());
    } 

    public boolean isAfter(Instant instant) {
        return jodaDateTime.isAfter(((DefaultRelativeInstant)instant).getJodaType());
    }

    public boolean isBefore(Instant instant) {
        return jodaDateTime.isBefore(((DefaultRelativeInstant)instant).getJodaType());
    }
    
    public String toString() {      
        return jodaDateTime.toString();
    }  
    
    public boolean equals(Object obj) {      
        if ((obj == null) || ((Time)obj).isAbsolute() || ((Time)obj).isInterval()){
            return false;
        }
        return ((DefaultRelativeInstant)obj).getJodaType().equals(jodaDateTime);
    }

    public int getMillisOfSecond() {      
        return jodaDateTime.getMillisOfSecond();
    }

    public Chronology getChronology() {      
        return chronology;
    }
    public int getYear() {
        return jodaDateTime.getYear();
    }

    public int getMonthOfYear() {
        return jodaDateTime.getMonthOfYear();
    }

    public int getWeekOfWeekyear() {    
        return jodaDateTime.getWeekOfWeekyear();
    }

    public int getDayOfWeek() {
        return jodaDateTime.getDayOfWeek();
    }    

    public int getDayOfMonth() { 
        return jodaDateTime.getDayOfMonth();
    } 
    
    public Object clone() throws CloneNotSupportedException {
        return new DefaultRelativeInstant(chronology, new org.joda.time.DateTime(jodaDateTime));
    }  

    public boolean intersects(Time time) {
      if (time.isRelative()){
          if (time.isInterval()){
              return (((RelativeInterval)time).toDurationMillis() == 0) && ((RelativeInterval)time).getStart().equals(this);
          }else{
              return this.equals(time);
          }
      }
      return true;
  }

    public boolean isRelative() {
        return true;
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean isInterval() {
        return false;
    }

    public boolean isInstant() {
        return true;
    }

    public int compareTo(Instant instant) {
        return getJodaType().compareTo(((DefaultRelativeInstant)instant).getJodaType());
    }
    

    org.joda.time.DateTime getJodaType() {
        return jodaDateTime;
    }

  
    
//	protected org.joda.time.Instant jodaInstant = null;	
//	private static Chronology chronology = 
//	    TimeSupportLocator.getManager().getChronology(Chronology.ISO).withUTC();
//	
//	DefaultRelativeInstant(long instant) {
//		super();	
//		this.jodaInstant = new org.joda.time.Instant(instant);		
//	}
//	
//   DefaultRelativeInstant(Date date) {
//        super();    
//        this.jodaInstant = new org.joda.time.Instant(date);        
//    }
//   
//   DefaultRelativeInstant(DateTime dateTime) {
//       this(dateTime.toDate());      
//   }
//   
//   DefaultRelativeInstant(org.joda.time.Instant instant) {
//       super();    
//       this.jodaInstant = instant;      
//   }
//
//    public Date toDate() {       
//        return jodaInstant.toDate();
//    }
//
//    public int compareTo(DateTime dateTime) {        
//        return jodaInstant.compareTo(((DefaultDateTime)dateTime).getJodaType());
//    }
//
//    public DateTime toDateTime() {
//        return new DefaultDateTime(jodaInstant.toDateTime().toDate(), chronology);
//    }
//
//    public long getMillis() {
//        return jodaInstant.getMillis();
//    }	
//    
//    org.joda.time.Instant getJodaType() {
//        return jodaInstant;
//    }
//
//    public boolean isEqual(RelativeInstant instant) {       
//        return jodaInstant.isEqual(((DefaultRelativeInstant)instant).getJodaType());
//    }
//
//    public boolean isAfter(RelativeInstant instant) {
//        return jodaInstant.isAfter(((DefaultRelativeInstant)instant).getJodaType());
//    }
//
//    public boolean isBefore(RelativeInstant instant) {
//        return jodaInstant.isBefore(((DefaultRelativeInstant)instant).getJodaType());
//    }    
//    
//    public String toString() {      
//        return jodaInstant.toString();
//    }
//
//    public boolean equals(Object obj) {      
//        return ((DefaultRelativeInstant)obj).getJodaType().equals(jodaInstant);
//    }       
//
//    public Chronology getChronology() {      
//        return chronology;
//    }


}

