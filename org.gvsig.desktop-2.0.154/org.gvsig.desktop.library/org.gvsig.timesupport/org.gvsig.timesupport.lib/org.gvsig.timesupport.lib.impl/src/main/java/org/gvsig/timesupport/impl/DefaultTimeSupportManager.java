/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteInstantTypeNotRegisteredException;
import org.gvsig.timesupport.AbsoluteInterval;
import org.gvsig.timesupport.AbsoluteIntervalType;
import org.gvsig.timesupport.AbsoluteIntervalTypeNotRegisteredException;
import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.impl.animation.TimeAnimationImpl;

/**
 * Default {@link TimeSupportManager} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
@SuppressWarnings("unchecked")
public class DefaultTimeSupportManager implements TimeSupportManager {
    private Logger LOG = LoggerFactory.getLogger(DefaultTimeSupportManager.class);
	private Map absoluteInstantTypes = new HashMap();
    private Map chronologyes = new HashMap();
    private Map absoluteIntervalTypes = new HashMap();

    private Chronology defaultChronology = null;

    private List relativeInstantPatterns = null;

    public DefaultTimeSupportManager(){
        relativeInstantPatterns = new ArrayList();
    }

    public void registerAbsoluteInstantType(AbsoluteInstantType absoluteInstantType) {
        absoluteInstantTypes.put(absoluteInstantType.getType(), absoluteInstantType);   
    }

    public AbsoluteInstantType getAbsoluteInstantType(int type) throws AbsoluteInstantTypeNotRegisteredException {
        if (absoluteInstantTypes.containsKey(type)){
            return (AbsoluteInstantType)absoluteInstantTypes.get(type);
        }
        throw new AbsoluteInstantTypeNotRegisteredException(type);     
    }

    public void registerAbsoluteIntervalType(AbsoluteIntervalType absoluteIntervalType) {
        absoluteIntervalTypes.put(absoluteIntervalType.getType(), absoluteIntervalType);                  
    }

    public AbsoluteIntervalType getAbsoluteIntervalType(int type)
    throws AbsoluteIntervalTypeNotRegisteredException {
        if (absoluteIntervalTypes.containsKey(type)){
            return (AbsoluteIntervalType)absoluteIntervalTypes.get(type);
        }
        throw new AbsoluteIntervalTypeNotRegisteredException(type);
    }

    public void registerChronology(Chronology chronology) {
        chronologyes.put(chronology.getType(), chronology);        
    }

    public Chronology getChronology(int chronology){
        if (chronologyes.containsKey(chronology)){
            return (Chronology)chronologyes.get(chronology);
        }
        return null;
    }

    public void setDefaultChronology(int chronology){
        this.defaultChronology = getChronology(chronology).withUTC();
    }

    public RelativeInstant createRelativeInstant(long instant) {
        return new DefaultRelativeInstant(instant, defaultChronology);
    }	

    public RelativeInstant createRelativeInstant(Date date) {
        return new DefaultRelativeInstant(date, defaultChronology);
    }    

    public AbsoluteInstant createAbsoluteInstant(){
        return new DefaultAbsoluteInstant(defaultChronology);
    }


    public RelativeInstant createRelativeInstant(long instant, Chronology chronology) {
        return new DefaultRelativeInstant(instant, chronology);
    }

    public RelativeInstant createRelativeInstant(Date date, Chronology chronology) {
        return new DefaultRelativeInstant(date, chronology);
    }

    public RelativeInstant createRelativeInstant(Chronology chronology) {
        return new DefaultRelativeInstant(chronology);
    }

    public RelativeInstant createRelativeInstant(int year, int monthOfYear,
        int dayOfMonth, int hourOfDay, int minuteOfHour, int secondOfMinute,
        int millisOfSecond) {
        return new DefaultRelativeInstant(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute, millisOfSecond, defaultChronology);
    }

    public RelativeInstant createRelativeInstant(int year, int monthOfYear,
        int dayOfMonth, int hourOfDay, int minuteOfHour, int secondOfMinute,
        int millisOfSecond, Chronology chronology) {
        return new DefaultRelativeInstant(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute, millisOfSecond, chronology);
    }

    public RelativeInstant createRelativeInstant(int year, int monthOfYear,
        int dayOfMonth, int hourOfDay, int minuteOfHour, int secondOfMinute,
        int millisOfSecond, int chronology) {
        return new DefaultRelativeInstant(year, monthOfYear, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute, millisOfSecond, getChronology(chronology));
    }

    public AbsoluteInstant createAbsoluteInstant(int type, int value) throws AbsoluteInstantTypeNotRegisteredException {
        return new DefaultAbsoluteInstant((DefaultAbsoluteInstantType)getAbsoluteInstantType(type), value, defaultChronology);
    }	

    public AbsoluteInstant createAbsoluteInstant(int[] types, int[] values)
    throws AbsoluteInstantTypeNotRegisteredException {
        return createAbsoluteInstant(types, values, defaultChronology);
    }    

    public AbsoluteInstant createAbsoluteInstant(int type, int value, Chronology chronology)
    throws AbsoluteInstantTypeNotRegisteredException {
        return new DefaultAbsoluteInstant((DefaultAbsoluteInstantType)getAbsoluteInstantType(type), value, chronology);
    }

    public AbsoluteInstant createAbsoluteInstant(int[] types, int[] values,
        Chronology chronology) throws AbsoluteInstantTypeNotRegisteredException {
        DefaultAbsoluteInstantType[] dateTimeFieldTypes = new DefaultAbsoluteInstantType[types.length];
        for (int i=0 ; i<types.length ; i++){
            dateTimeFieldTypes[i] = (DefaultAbsoluteInstantType)getAbsoluteInstantType(types[i]);
        }
        return new DefaultAbsoluteInstant(dateTimeFieldTypes, values, chronology);
    }

    public AbsoluteInstant createAbsoluteInstant(Chronology chronology){
        return new DefaultAbsoluteInstant(chronology);
    }

    public RelativeInterval createRelativeInterval(long startInstant, long endInstant) {
        return new DefaultRelativeInterval(startInstant, endInstant, defaultChronology);
    }	

    public RelativeInterval createRelativeInterval(RelativeInstant startDateTime, RelativeInstant endDateTime) {
        return new DefaultRelativeInterval(startDateTime, endDateTime, defaultChronology);
    }    

    public RelativeInterval createRelativeInterval(long startInstant, long endInstant,
        Chronology chronology) {
        return new DefaultRelativeInterval(startInstant, endInstant, chronology);
    }

    public RelativeInterval createRelativeInterval(RelativeInstant startDateTime,
        RelativeInstant endDateTime, Chronology chronology) {
        return new DefaultRelativeInterval(startDateTime, endDateTime, chronology);
    }      

    public AbsoluteInterval createAbsoluteInterval(int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis) throws AbsoluteIntervalTypeNotRegisteredException {
        return new DefaultAbsoluteInterval(years, months, weeks, days, hours, minutes, seconds, millis, defaultChronology);       
    }	

    public AbsoluteInterval createAbsoluteInterval(AbsoluteInstant startPartial, int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis) throws AbsoluteIntervalTypeNotRegisteredException {
        return new DefaultAbsoluteInterval(startPartial, years, months, weeks, days, hours, minutes, seconds, millis, defaultChronology);       
    }   

    public AbsoluteInterval createAbsoluteInterval(int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis, Chronology chronology)
    throws AbsoluteIntervalTypeNotRegisteredException {
        return new DefaultAbsoluteInterval(years, months, weeks, days, hours, minutes, seconds, millis, chronology);
    }    

    public AbsoluteInterval createAbsoluteInterval(AbsoluteInstant startPartial, int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis, Chronology chronology)
    throws AbsoluteIntervalTypeNotRegisteredException {
        return new DefaultAbsoluteInterval(startPartial, years, months, weeks, days, hours, minutes, seconds, millis, chronology);
    }

    public AbsoluteInterval createAbsoluteInterval(AbsoluteInstant startPartial, AbsoluteInstant endPartial)
    {
        return new DefaultAbsoluteInterval(startPartial, endPartial, defaultChronology);
    }

    public AbsoluteInterval createAbsoluteInterval(RelativeInstant startPartial, RelativeInstant endPartial)
    {
        return new DefaultAbsoluteInterval(startPartial, endPartial, defaultChronology);
    }

    public AbsoluteInterval createAbsoluteInterval(
        AbsoluteInstant startInstant, AbsoluteInstant endInstant,
        Chronology chronology)
    throws AbsoluteIntervalTypeNotRegisteredException {
        return new DefaultAbsoluteInterval(startInstant, endInstant, chronology);
    }

    public void addRelativeInstantPattern(String relativeInstantPattern) {
        relativeInstantPatterns.add(relativeInstantPattern);        
    }

    public RelativeInstant parseRelativeInstant(String relativeInstantString) {
        for (int i=0 ; i<relativeInstantPatterns.size() ; i++){
            try {     
                Date date = new SimpleDateFormat((String)relativeInstantPatterns.get(i)).parse(relativeInstantString);
                if (date != null){
                    return createRelativeInstant(date);
                }
            } catch (ParseException e) {
                //LOG.info("Impossible to parse the date using the " +  relativeInstantPatterns.get(i) + " pattern");    
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.timesupport.TimeSupportManager#createTimeAnimation()
     */
	public TimeAnimation createTimeAnimation() {
		return new TimeAnimationImpl();
	}
}
