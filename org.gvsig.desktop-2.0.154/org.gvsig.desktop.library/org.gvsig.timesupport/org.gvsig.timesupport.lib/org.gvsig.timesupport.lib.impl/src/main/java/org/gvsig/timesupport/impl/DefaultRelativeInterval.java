/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.Duration;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.timesupport.Time;

/**
 * @author gvSIG team
 * @version $Id$
 */
public class DefaultRelativeInterval implements RelativeInterval {
    private org.joda.time.Interval jodaInterval = null;
    private Chronology chronology = null;

    DefaultRelativeInterval(long startInstant, long endInstant, Chronology chronology) {
        super();	
        this.jodaInterval = new org.joda.time.Interval(startInstant, endInstant, ((DefaultChronology)chronology).getJodaType());
        this.chronology = chronology;
    }

    DefaultRelativeInterval(RelativeInstant startInstant, RelativeInstant endInstant, Chronology chronology) {
        this(startInstant.toMillis(), endInstant.toMillis(), chronology);    
    }

    DefaultRelativeInterval(Chronology chronology, org.joda.time.Interval interval) {
        super();    
        this.jodaInterval = interval;
        this.chronology = chronology; 
    }

    public long toDurationMillis() {     
        return jodaInterval.toDurationMillis();
    }

    public boolean contains(RelativeInstant instant) {       
        return jodaInterval.contains(((DefaultRelativeInstant)instant).getJodaType());
    }

    public boolean isAfter(RelativeInstant instant) {
        return jodaInterval.isAfter(((DefaultRelativeInstant)instant).getJodaType());
    }

    public boolean isAfter(RelativeInterval interval) {
        return jodaInterval.isAfter(((DefaultRelativeInterval)interval).getJodaType());
    }

    public boolean isBefore(RelativeInstant instant) {
        return jodaInterval.isBefore(((DefaultRelativeInstant)instant).getJodaType());
    }

    public boolean isBefore(RelativeInterval interval) {
        return jodaInterval.isBefore(((DefaultRelativeInterval)interval).getJodaType());
    }

    public Duration toDuration() {    
        return new DefaultDuration(toDurationMillis());
    }

    org.joda.time.Interval getJodaType() {
        return jodaInterval;
    }    

    public String toString() {      
        return jodaInterval.toString();
    }     

    public boolean equals(Object obj) {      
        if ((obj == null) || ((Time)obj).isAbsolute() || ((Time)obj).isInstant()){
            return false;
        }
        return ((DefaultRelativeInterval)obj).getJodaType().equals(jodaInterval);
    }

    public boolean contains(RelativeInterval interval) {       
        return jodaInterval.contains(((DefaultRelativeInterval)interval).getJodaType());
    }

    public boolean overlaps(RelativeInterval interval) {
        return jodaInterval.overlaps(((DefaultRelativeInterval)interval).getJodaType());
    }     

    public Chronology getChronology() {      
        return chronology;
    }

    public Object clone() throws CloneNotSupportedException {
        return new DefaultRelativeInterval(chronology, new org.joda.time.Interval(jodaInterval));
    } 
    
    public RelativeInstant getStart(){
        return new DefaultRelativeInstant(chronology, jodaInterval.getStart());
    }
    
    public RelativeInstant getEnd(){
        return new DefaultRelativeInstant(chronology, jodaInterval.getEnd());
    }
    
    public boolean isRelative() {
        return true;
    }

    public boolean isAbsolute() {
        return false;
    }    

    public boolean isInterval() {
        return true;
    }

    public boolean isInstant() { 
        return false;
    }

    public boolean contains(Instant instant) {
        if (instant.isRelative()){
            return contains((RelativeInstant)instant);
        }
        return false;  
    }

    public boolean intersects(Time time) {
        if (time.isRelative()){
            if (time.isInterval()){
                return intersects((RelativeInterval)time);
            }else{
                return contains((RelativeInstant)time);
            }
        }
        return false;
    }

    public boolean intersects(RelativeInterval relativeInterval){
        return (this.getStart().isAfter((RelativeInstant)relativeInterval.getStart()) && this.getEnd().isBefore((RelativeInstant)relativeInterval.getStart())) ||
        (this.getEnd().isBefore((RelativeInstant)relativeInterval.getEnd()) && this.getEnd().isAfter((RelativeInstant)relativeInterval.getEnd()));
    }

    public Duration toStandardDuration() {
        return new DefaultDuration(jodaInterval.toDuration());
    }
}

