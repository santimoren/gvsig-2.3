/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */ 
package org.gvsig.timesupport.impl;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteInterval;
import org.gvsig.timesupport.AbsoluteIntervalType;
import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.Duration;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Time;

/**
 * @author gvSIG team
 * @version $Id$
 */
public class DefaultAbsoluteInstant implements AbsoluteInstant {
    private org.joda.time.Partial jodaPartial = null;
    private AbsoluteInstantType[] dateTimeFieldTypes = null;
    private Chronology chronology = null;
    private org.joda.time.DateTimeFieldType[] indexes = null;

    DefaultAbsoluteInstant(Chronology chronology) {
        super();    
        this.jodaPartial = new org.joda.time.Partial(((DefaultChronology)chronology).getJodaType());
        this.chronology = chronology;
        initializeIndexes();
    }

    DefaultAbsoluteInstant(Chronology chronology, org.joda.time.Partial jodaPartial) {
        super();    
        this.jodaPartial = jodaPartial;
        this.chronology = chronology;
        initializeIndexes();
    }

    DefaultAbsoluteInstant(DefaultAbsoluteInstantType dateTimeFieldType, int value, Chronology chronology) {
        super();	
        this.jodaPartial = new org.joda.time.Partial(dateTimeFieldType.getJodaType(), value, ((DefaultChronology)chronology).getJodaType());
        this.dateTimeFieldTypes = new DefaultAbsoluteInstantType[1];
        this.dateTimeFieldTypes[0] = dateTimeFieldType;
        this.chronology = chronology;
        initializeIndexes();
    }

    DefaultAbsoluteInstant(DefaultAbsoluteInstantType[] dateTimeFieldTypes, int[] values, Chronology chronology) {
        super();    
        org.joda.time.DateTimeFieldType[] jodaDateTimeFieldTypes = new org.joda.time.DateTimeFieldType[dateTimeFieldTypes.length];
        this.dateTimeFieldTypes = new DefaultAbsoluteInstantType[dateTimeFieldTypes.length];
        for (int i=0 ; i<dateTimeFieldTypes.length ; i++){
            this.dateTimeFieldTypes[i] = dateTimeFieldTypes[i];
            jodaDateTimeFieldTypes[i] = dateTimeFieldTypes[i].getJodaType();
        }
        this.jodaPartial = new org.joda.time.Partial(jodaDateTimeFieldTypes, values, ((DefaultChronology)chronology).getJodaType());
        this.chronology = chronology;
        initializeIndexes();
    }
    
    private void initializeIndexes(){
        indexes = new org.joda.time.DateTimeFieldType[8];
        org.joda.time.DateTimeFieldType[] dateTimeFieldTypes = jodaPartial.getFieldTypes();    
        for (int i=0 ; i<dateTimeFieldTypes.length ; i++){    
            if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.year())){
                indexes[AbsoluteIntervalType.YEARS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.monthOfYear())){
                indexes[AbsoluteIntervalType.MONTHS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.weekOfWeekyear())){
                indexes[AbsoluteIntervalType.WEEKS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.dayOfMonth())){
                indexes[AbsoluteIntervalType.DAYS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.hourOfDay())){
                indexes[AbsoluteIntervalType.HOURS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.minuteOfHour())){
                indexes[AbsoluteIntervalType.MINUTES] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.secondOfMinute())){
                indexes[AbsoluteIntervalType.SECONDS] = dateTimeFieldTypes[i];
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.millisOfSecond())){
                indexes[AbsoluteIntervalType.MILLIS] = dateTimeFieldTypes[i];
            } 
        }         
    }

    public int size() {
        return jodaPartial.size();
    }

    public AbsoluteInstantType getFieldType(int index) {        
        return dateTimeFieldTypes[index];
    }

    public int getValue(int index) {       
        return jodaPartial.getValue(index);
    }

    public String toString() {      
        return jodaPartial.toString();
    } 

    public boolean equals(Object obj) {      
        if (((Time)obj).isRelative() || ((Time)obj).isInterval()){
            return false;
        }    
        return ((DefaultAbsoluteInstant)obj).getJodaType().equals(jodaPartial);
    }

    org.joda.time.Partial getJodaType() {
        return jodaPartial;
    }

    public boolean isAfter(Instant partial) {
        return jodaPartial.isAfter(((DefaultAbsoluteInstant)partial).getJodaType());
    }

    public boolean isBefore(Instant partial) {
        return jodaPartial.isBefore(((DefaultAbsoluteInstant)partial).getJodaType());
    }

    public boolean isEqual(Instant partial) {
        return jodaPartial.isEqual(((DefaultAbsoluteInstant)partial).getJodaType());
    }    

    public Chronology getChronology() {      
        return chronology;
    }

    public Object clone() throws CloneNotSupportedException {
        return new DefaultAbsoluteInstant(chronology, new org.joda.time.Partial(jodaPartial));
    }  
    
    public int getYears(){
        if (indexes[AbsoluteIntervalType.YEARS] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.YEARS])).intValue();
    }    

    public int getMonths(){
        if (indexes[AbsoluteIntervalType.MONTHS] == null){
            return 1;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.MONTHS])).intValue();
    }
    
    public int getWeeks(){
        if (indexes[AbsoluteIntervalType.WEEKS] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.WEEKS])).intValue();
    }

    public int getDays(){
        if (indexes[AbsoluteIntervalType.DAYS] == null){
            return 1;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.DAYS])).intValue();
    }

    public int getHours(){
        if (indexes[AbsoluteIntervalType.HOURS] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.HOURS])).intValue();
    }

    public int getMinutes(){
        if (indexes[AbsoluteIntervalType.MINUTES] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.MINUTES])).intValue();
    }

    public int getSeconds(){
        if (indexes[AbsoluteIntervalType.SECONDS] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.SECONDS])).intValue();
    }

    public int getMillis(){
        if (indexes[AbsoluteIntervalType.MILLIS] == null){
            return 0;
        }
        return ((Integer)jodaPartial.get(indexes[AbsoluteIntervalType.MILLIS])).intValue();
    }

    public boolean isRelative() {
        return false;
    }

    public boolean isAbsolute() {
        return true;
    }

    public boolean isInterval() {
        return false;
    }

    public boolean isInstant() { 
        return true;
    }

    public boolean intersects(Time time) {
        if (time.isAbsolute()){
            if (time.isInterval()){
                return (((AbsoluteInterval)time).getMillis() == 0) && ((AbsoluteInterval)time).getStart().equals(this);
            }else{
                return this.equals(time);
            }
        }
        return true;
    }

    public AbsoluteInstant minus(AbsoluteInterval interval) {
        return new DefaultAbsoluteInstant(chronology, jodaPartial.minus(((DefaultAbsoluteInterval)interval).getJodaType()));
    }

    public AbsoluteInstant plus(AbsoluteInterval interval) {
        return new DefaultAbsoluteInstant(chronology, jodaPartial.plus(((DefaultAbsoluteInterval)interval).getJodaType()));
    }

    public Duration toStandardDuration() {
        long millis = 0;                  
        millis += getYears() * DefaultAbsoluteInterval.MILLIS_BY_YEAR;
        millis += (getMonths() - 1) * DefaultAbsoluteInterval.MILLIS_BY_MONTH;
        millis += (getDays() - 1) * DefaultAbsoluteInterval.MILLIS_BY_YEAR;
        millis += getHours() * DefaultAbsoluteInterval.MILLIS_BY_HOUR;
        millis += getMinutes() * DefaultAbsoluteInterval.MILLIS_BY_MINUTE;
        millis += getSeconds() * DefaultAbsoluteInterval.MILLIS_BY_SECOND;       
        millis += getMillis();
        return new DefaultDuration(millis);    
    }    
}

