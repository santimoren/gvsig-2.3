/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteIntervalType;
import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.DataTypes;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Library for default implementation initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class TimeSupportDefaultImplLibrary extends AbstractLibrary {
       
    @Override
    protected void doInitialize() throws LibraryException {
        TimeSupportLocator.registerManager(DefaultTimeSupportManager.class);
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        //Register the types
        DataTypesManager dataTypesManager = ToolsLocator.getDataTypesManager();
        dataTypesManager.addtype(DataTypes.INSTANT, "Instant", "Instant", DefaultRelativeInstant.class, null);
        dataTypesManager.addtype(DataTypes.INTERVAL, "Interval","Interval", DefaultRelativeInterval.class, null);
        
        // Do nothing
        TimeSupportManager timeSupportManager = TimeSupportLocator.getManager();
        
        //register AbsoluteInstantType 
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.ERA, org.joda.time.DateTimeFieldType.era()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.YEAR_OF_ERA, org.joda.time.DateTimeFieldType.yearOfEra()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.CENTURY_OF_ERA, org.joda.time.DateTimeFieldType.centuryOfEra()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.YEAR_OF_CENTURY, org.joda.time.DateTimeFieldType.yearOfCentury()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.YEAR, org.joda.time.DateTimeFieldType.year()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.DAY_OF_YEAR, org.joda.time.DateTimeFieldType.dayOfYear()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.MONTH_OF_YEAR, org.joda.time.DateTimeFieldType.monthOfYear()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.DAY_OF_MONTH, org.joda.time.DateTimeFieldType.dayOfMonth()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.WEEKYEAR_OF_CENTURY, org.joda.time.DateTimeFieldType.weekyearOfCentury()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.WEEKYEAR, org.joda.time.DateTimeFieldType.weekyear()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.WEEK_OF_WEEKYEAR, org.joda.time.DateTimeFieldType.weekOfWeekyear()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.DAY_OF_WEEK, org.joda.time.DateTimeFieldType.dayOfWeek()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.HALFDAY_OF_DAY, org.joda.time.DateTimeFieldType.halfdayOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.HOUR_OF_HALFDAY, org.joda.time.DateTimeFieldType.hourOfHalfday()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.CLOCKHOUR_OF_HALFDAY, org.joda.time.DateTimeFieldType.clockhourOfHalfday()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.CLOCKHOUR_OF_DAY, org.joda.time.DateTimeFieldType.clockhourOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.HOUR_OF_DAY, org.joda.time.DateTimeFieldType.hourOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.MINUTE_OF_DAY, org.joda.time.DateTimeFieldType.minuteOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.MINUTE_OF_HOUR, org.joda.time.DateTimeFieldType.minuteOfHour()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.SECOND_OF_DAY, org.joda.time.DateTimeFieldType.secondOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.SECOND_OF_MINUTE, org.joda.time.DateTimeFieldType.secondOfMinute()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.MILLIS_OF_DAY, org.joda.time.DateTimeFieldType.millisOfDay()));
        timeSupportManager.registerAbsoluteInstantType(new DefaultAbsoluteInstantType(AbsoluteInstantType.MILLIS_OF_SECOND, org.joda.time.DateTimeFieldType.millisOfSecond()));
       
        //Register period types
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.YEARS, org.joda.time.PeriodType.years()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.MONTHS, org.joda.time.PeriodType.months()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.WEEKS, org.joda.time.PeriodType.weeks()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.DAYS, org.joda.time.PeriodType.days()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.HOURS, org.joda.time.PeriodType.hours()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.MINUTES, org.joda.time.PeriodType.minutes()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.SECONDS, org.joda.time.PeriodType.seconds()));
        timeSupportManager.registerAbsoluteIntervalType(new DefaultAbsoluteIntervalType(AbsoluteIntervalType.MILLIS, org.joda.time.PeriodType.millis()));
                              
        //register Chronologies
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.BUDDHIST, org.joda.time.chrono.BuddhistChronology.getInstance()));
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.COPTIC, org.joda.time.chrono.CopticChronology.getInstance()));
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.GJ, org.joda.time.chrono.GJChronology.getInstance()));
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.GREGORIAN, org.joda.time.chrono.GregorianChronology.getInstance()));
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.ISO, org.joda.time.chrono.ISOChronology.getInstance()));
        timeSupportManager.registerChronology(new DefaultChronology(Chronology.JULIAN, org.joda.time.chrono.JulianChronology.getInstance()));
        
        //Sets the default chronology
        timeSupportManager.setDefaultChronology(Chronology.GREGORIAN);     
        
        //Add some string patterns to parse relative instants
        timeSupportManager.addRelativeInstantPattern("yyyy:MM:dd HH:mm:ss");
        timeSupportManager.addRelativeInstantPattern("yyyy:MM:dd");
        timeSupportManager.addRelativeInstantPattern("yyyy/MM/dd HH:mm:ss");
        timeSupportManager.addRelativeInstantPattern("yyyy/MM/dd");
        timeSupportManager.addRelativeInstantPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        timeSupportManager.addRelativeInstantPattern("yyyy-MM-dd HH:mm:ss"); 
        timeSupportManager.addRelativeInstantPattern("yyyy-MM-dd");   
    }

}
