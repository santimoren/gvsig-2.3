/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteInstantTypeNotRegisteredException;
import org.gvsig.timesupport.AbsoluteInterval;
import org.gvsig.timesupport.AbsoluteIntervalType;
import org.gvsig.timesupport.AbsoluteIntervalTypeNotRegisteredException;
import org.gvsig.timesupport.Chronology;
import org.gvsig.timesupport.Duration;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;

/**
 * @author gvSIG team
 * @version $Id$
 */
public class DefaultAbsoluteInterval implements AbsoluteInterval{
    static final long MILLIS_BY_SECOND = 1000;
    static final long MILLIS_BY_MINUTE = MILLIS_BY_SECOND * 60;
    static final long MILLIS_BY_HOUR = MILLIS_BY_MINUTE * 60;
    static final long MILLIS_BY_DAY = MILLIS_BY_HOUR * 24;
    static final long MILLIS_BY_MONTH = MILLIS_BY_DAY * 30;
    static final long MILLIS_BY_YEAR = MILLIS_BY_MONTH * 365;

    private static Logger LOG = LoggerFactory.getLogger(DefaultAbsoluteInterval.class);

    private org.joda.time.Period jodaPeriod = null;
    private AbsoluteInstant startAbsoluteInstant = null;
    private AbsoluteInstant endAbsoluteInstant = null;
    private AbsoluteIntervalType[] absoluteIntervalTypes = null; 
    private Chronology chronology = null;

    private static final TimeSupportManager timeSupportManager = TimeSupportLocator.getManager(); 

    DefaultAbsoluteInterval(AbsoluteInstant startPartial, int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis, Chronology chronology) throws AbsoluteIntervalTypeNotRegisteredException{
        this(years, months, weeks, days, hours, minutes, seconds, millis, chronology);
        initilizePartials(((DefaultAbsoluteInstant)startPartial).getJodaType());
    }

    DefaultAbsoluteInterval(int years, int months, int weeks, int days,
        int hours, int minutes, int seconds, int millis, Chronology chronology) throws AbsoluteIntervalTypeNotRegisteredException{
        super();	
        this.jodaPeriod = new org.joda.time.Period(years, months, weeks, days, hours, minutes, seconds, millis);
        this.absoluteIntervalTypes = new AbsoluteIntervalType[8];
        this.absoluteIntervalTypes[0] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.YEARS);
        this.absoluteIntervalTypes[1] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.MONTHS);
        this.absoluteIntervalTypes[2] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.WEEKS);
        this.absoluteIntervalTypes[3] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.DAYS);
        this.absoluteIntervalTypes[4] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.HOURS);
        this.absoluteIntervalTypes[5] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.MINUTES);
        this.absoluteIntervalTypes[6] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.SECONDS);
        this.absoluteIntervalTypes[7] = timeSupportManager.getAbsoluteIntervalType(AbsoluteIntervalType.MILLIS); 
        this.chronology = chronology;
        initilizePartials();
    }    

    DefaultAbsoluteInterval(AbsoluteInstant startAbsoluteInstant, AbsoluteInstant endAbsoluteInstant, Chronology chronology) {
        super(); 
        this.jodaPeriod = new org.joda.time.Period(startAbsoluteInstant.toStandardDuration().getMillis(),
            endAbsoluteInstant.toStandardDuration().getMillis());         
            this.chronology = chronology;
            this.startAbsoluteInstant = startAbsoluteInstant;
            this.endAbsoluteInstant = endAbsoluteInstant;
            
//        int[] mergedTypes = mergeTypes(startAbsoluteInstant, endAbsoluteInstant);
//        try {
//            AbsoluteInstant startAbsoluteInstantAux = mergeTypes(mergedTypes, startAbsoluteInstant);
//            AbsoluteInstant endAbsoluteInstantAux = mergeTypes(mergedTypes, endAbsoluteInstant);
//            this.jodaPeriod = new org.joda.time.Period(((DefaultAbsoluteInstant)startAbsoluteInstantAux).getJodaType(), 
//                ((DefaultAbsoluteInstant)endAbsoluteInstantAux).getJodaType());
//            this.chronology = chronology;
//            this.startAbsoluteInstant = startAbsoluteInstantAux;
//            this.endAbsoluteInstant = endAbsoluteInstantAux;
//        } catch (AbsoluteInstantTypeNotRegisteredException e) {
//           LOG.error("Error creating the interval");
//        }        
    }

//    private AbsoluteInstant mergeTypes(int[] mergedTypes,
//        AbsoluteInstant absoluteInstant) throws AbsoluteInstantTypeNotRegisteredException{
//        int[] values = new int[mergedTypes.length];
//        for (int i=0 ; i<mergedTypes.length ; i++){
//            if (mergedTypes[i] == AbsoluteInstantType.YEAR){
//                values[i] = absoluteInstant.getYears();
//            }else if (mergedTypes[i] == AbsoluteInstantType.MONTH_OF_YEAR){
//                values[i] = absoluteInstant.getMonths();
//                if (values[i] == 0){
//                    values[i] = 1;
//                }
//            }else if (mergedTypes[i] == AbsoluteInstantType.DAY_OF_MONTH){
//                values[i] = absoluteInstant.getDays();
//                if (values[i] == 0){
//                    values[i] = 1;
//                }
//            }else if (mergedTypes[i] == AbsoluteInstantType.HOUR_OF_DAY){
//                values[i] = absoluteInstant.getHours();
//            }else if (mergedTypes[i] == AbsoluteInstantType.MINUTE_OF_HOUR){
//                values[i] = absoluteInstant.getMinutes();
//            }else if (mergedTypes[i] == AbsoluteInstantType.SECOND_OF_MINUTE){
//                values[i] = absoluteInstant.getSeconds();
//            }else if (mergedTypes[i] == AbsoluteInstantType.MILLIS_OF_SECOND){
//                values[i] = absoluteInstant.getMillis();
//            }else{
//                values[i] = 0;
//            }                  
//        }
//        return timeSupportManager.createAbsoluteInstant(mergedTypes, values, absoluteInstant.getChronology());
//    }
//
//    private int[] mergeTypes(AbsoluteInstant startAbsoluteInstant,
//        AbsoluteInstant endAbsoluteInstant) {
//        ArrayList types = new ArrayList();
//        for (int i=0 ; i<startAbsoluteInstant.size() ; i++){
//            types.add(startAbsoluteInstant.getFieldType(i).getType());
//        }
//        
//        for (int i=0 ; i<endAbsoluteInstant.size() ; i++){
//            int type = endAbsoluteInstant.getFieldType(i).getType();
//            int position = -1;
//            for (int j=0 ; j<types.size() ; j++){
//                int addedType = (Integer)types.get(j);
//                if (type < addedType){
//                    position = j;
//                    break;
//                }else if (type == addedType){
//                    position = -2;
//                }                
//            }
//            if (position > -1){
//                types.add(position, type);
//            }else if (position == -1){
//                types.add(type);
//            }
//        }        
//        
//        int[] mergedTypes = new int[types.size()];     
//        for (int i=0 ; i<types.size() ; i++){
//            mergedTypes[i] = (Integer)types.get(i);          
//        }
//        return mergedTypes;
//    }

    DefaultAbsoluteInterval(RelativeInstant startPeriod, RelativeInstant endPeriod, Chronology chronology) {
        super();    
        this.jodaPeriod = new org.joda.time.Period(((DefaultRelativeInstant)startPeriod).getJodaType(), 
            ((DefaultRelativeInstant)endPeriod).getJodaType());
        this.chronology = chronology;
        initilizePartials();
    }

    DefaultAbsoluteInterval(Chronology chronology, org.joda.time.Period period) {
        super();    
        this.jodaPeriod = period;
        this.chronology = chronology; 
        initilizePartials();
    }    

    DefaultAbsoluteInterval(AbsoluteInstant startPartial, AbsoluteInstant endPartial) {
        super(); 
        this.startAbsoluteInstant = startPartial;
        this.endAbsoluteInstant = endPartial;
        this.jodaPeriod = new org.joda.time.Period(endPartial.getYears() - startPartial.getYears(),
            0,
            endPartial.getMonths() - startPartial.getMonths(),
            endPartial.getDays() - startPartial.getDays(),
            endPartial.getHours() - startPartial.getHours(),
            endPartial.getMinutes() - startPartial.getMinutes(),
            endPartial.getSeconds() - startPartial.getSeconds(),
            endPartial.getMillis() - startPartial.getMillis());            
    }
   
    @SuppressWarnings({"unchecked" })
    private void initilizePartials(){
        this.startAbsoluteInstant = new DefaultAbsoluteInstant(chronology);        
        ArrayList types = new ArrayList();
        ArrayList values = new ArrayList();
        if (jodaPeriod.getYears() > 0){
            types.add(AbsoluteInstantType.YEAR);
            values.add(jodaPeriod.getYears());
        }
        if (jodaPeriod.getMonths() > 0){
            types.add(AbsoluteInstantType.MONTH_OF_YEAR);
            values.add(jodaPeriod.getMonths());
        }
        if (jodaPeriod.getWeeks() > 0){
            types.add(AbsoluteInstantType.WEEK_OF_WEEKYEAR);
            values.add(jodaPeriod.getWeeks());
        }
        if (jodaPeriod.getDays() > 0){
            types.add(AbsoluteInstantType.DAY_OF_MONTH);
            values.add(jodaPeriod.getDays());
        }
        if (jodaPeriod.getHours() > 0){
            types.add(AbsoluteInstantType.HOUR_OF_DAY);
            values.add(jodaPeriod.getHours());
        }
        if (jodaPeriod.getMinutes() > 0){
            types.add(AbsoluteInstantType.MINUTE_OF_HOUR);
            values.add(jodaPeriod.getMinutes());
        }
        if (jodaPeriod.getSeconds() > 0){
            types.add(AbsoluteInstantType.SECOND_OF_MINUTE);
            values.add(jodaPeriod.getSeconds());
        }
        if (jodaPeriod.getMillis() > 0){
            types.add(AbsoluteInstantType.MILLIS_OF_SECOND);
            values.add(jodaPeriod.getMillis());
        }        
        int[] nativeTypes = new int[types.size()];
        int[] nativeValues = new int[values.size()];
        for (int i=0 ; i<types.size() ; i++){
            nativeTypes[i] = (Integer)types.get(i);
            nativeValues[i] = (Integer)values.get(i);
        }
        try {
            this.endAbsoluteInstant = timeSupportManager.createAbsoluteInstant(nativeTypes, nativeValues, chronology);
        } catch (AbsoluteInstantTypeNotRegisteredException e) {
            LOG.error("Impossible to create the endPartial for a period", e);
        }  
    }

    @SuppressWarnings({ "unchecked" })
    private void initilizePartials(org.joda.time.Partial jodaPartial){
        this.startAbsoluteInstant = new DefaultAbsoluteInstant(chronology, jodaPartial);
        org.joda.time.DateTimeFieldType[] dateTimeFieldTypes = jodaPartial.getFieldTypes();
        int[] jodaValues = jodaPartial.getValues();
        ArrayList types = new ArrayList();
        ArrayList values = new ArrayList();
        if (jodaPeriod.getYears() > 0){
            types.add(AbsoluteInstantType.YEAR);
            values.add(jodaPeriod.getYears());
        }
        if (jodaPeriod.getMonths() > 0){
            types.add(AbsoluteInstantType.MONTH_OF_YEAR);
            values.add(jodaPeriod.getMonths());
        }
        if (jodaPeriod.getDays() > 0){
            types.add(AbsoluteInstantType.DAY_OF_MONTH);
            values.add(jodaPeriod.getDays());
        }
        if (jodaPeriod.getHours() > 0){
            types.add(AbsoluteInstantType.HOUR_OF_DAY);
            values.add(jodaPeriod.getHours());
        }
        if (jodaPeriod.getMinutes() > 0){
            types.add(AbsoluteInstantType.MINUTE_OF_HOUR);
            values.add(jodaPeriod.getMinutes());
        }
        if (jodaPeriod.getSeconds() > 0){
            types.add(AbsoluteInstantType.SECOND_OF_MINUTE);
            values.add(jodaPeriod.getSeconds());
        }
        if (jodaPeriod.getMillis() > 0){
            types.add(AbsoluteInstantType.MILLIS_OF_SECOND);
            values.add(jodaPeriod.getMillis());
        }    
        for (int i=0 ; i<dateTimeFieldTypes.length ; i++){           
            if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.year())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.YEAR);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.monthOfYear())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.MONTH_OF_YEAR);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.dayOfMonth())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.DAY_OF_MONTH);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.hourOfDay())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.HOUR_OF_DAY);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.minuteOfHour())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.MINUTE_OF_HOUR);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.secondOfMinute())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.SECOND_OF_MINUTE);
            } else if (dateTimeFieldTypes[i].equals(org.joda.time.DateTimeFieldType.millisOfSecond())){
                initializePartialsIteration(types, values, jodaValues[i], AbsoluteInstantType.MILLIS_OF_SECOND);
            } 
        } 
        int[] nativeTypes = new int[types.size()];
        int[] nativeValues = new int[values.size()];
        for (int i=0 ; i<types.size() ; i++){
            nativeTypes[i] = (Integer)types.get(i);
            nativeValues[i] = (Integer)values.get(i);
        }
        try {
            this.endAbsoluteInstant = timeSupportManager.createAbsoluteInstant(nativeTypes, nativeValues, chronology);
        } catch (AbsoluteInstantTypeNotRegisteredException e) {
            LOG.error("Impossible to create the endPartial for a period", e);
        }  
    }

    @SuppressWarnings({ "unchecked" })
    private void initializePartialsIteration(List types, List values, int jodaValue, int dateTimefieldtype){
        boolean found = false;
        for (int j=0 ; j<types.size() ; j++){
            if (types.get(j).equals(dateTimefieldtype)){
                int value = (Integer)values.get(j);
                values.remove(j);
                values.add(j, value + jodaValue);
                found = true;
                break;
            }
        }
        if (!found){
            types.add(dateTimefieldtype);
            values.add(jodaValue);
        }
    }

    public int size() {       
        return jodaPeriod.size();
    }

    public AbsoluteIntervalType getFieldType(int index) {       
        return absoluteIntervalTypes[index];
    }

    public int getValue(int index) {      
        return jodaPeriod.getValue(index);
    } 

    public String toString() {      
        return jodaPeriod.toString();
    } 

    public boolean equals(Object obj) {      
        if (((Time)obj).isRelative() || ((Time)obj).isInstant()){
            return false;
        }          
        return ((DefaultAbsoluteInterval)obj).getJodaType().equals(jodaPeriod);
    }

    org.joda.time.Period getJodaType() {
        return jodaPeriod;
    }   

    public Chronology getChronology() {      
        return chronology;
    }

    public int getYears() {
        return jodaPeriod.getYears();
    }

    public int getMonths() {
        return jodaPeriod.getMonths();
    }

    public int getWeeks() {
        return jodaPeriod.getWeeks();
    }

    public int getDays() {
        return jodaPeriod.getDays();
    }

    public int getHours() {
        return jodaPeriod.getHours();
    }

    public int getMinutes() {
        return jodaPeriod.getMinutes();
    }

    public int getSeconds() {
        return jodaPeriod.getSeconds();
    }

    public int getMillis() {
        return jodaPeriod.getMillis();
    }

    public boolean isAfter(AbsoluteInterval period) {
        return jodaPeriod.getMillis() > period.getMillis();
    }

    public boolean isBefore(AbsoluteInterval period) {
        return jodaPeriod.getMillis() < period.getMillis();
    }    

    public Object clone() throws CloneNotSupportedException {
        return new DefaultAbsoluteInterval(chronology, new org.joda.time.Period(jodaPeriod));
    }

    public AbsoluteInstant getStart(){
        return startAbsoluteInstant;
    }

    public AbsoluteInstant getEnd(){
        return endAbsoluteInstant;
    }

    public boolean isRelative() {
        return false;
    }

    public boolean isAbsolute() {
        return true;
    }    

    public boolean isInterval() {
        return true;
    }

    public boolean isInstant() { 
        return false;
    }

    public boolean intersects(Time time) {
        if (time.isAbsolute()){
            if (time.isInterval()){
                return intersects((AbsoluteInterval)time);
            }else{
                return contains((AbsoluteInstant)time);
            }
        }
        return true;
    }

    public boolean intersects(AbsoluteInterval absoluteInsterInterval){
        return (this.getStart().isAfter(absoluteInsterInterval.getStart()) && this.getEnd().isBefore(absoluteInsterInterval.getStart())) ||
        (this.getEnd().isBefore(absoluteInsterInterval.getEnd()) && this.getEnd().isAfter(absoluteInsterInterval.getEnd()));
    }

    public boolean contains(Instant instant) {
        if (instant.isAbsolute()){
            return this.getMillis() >= ((AbsoluteInstant)instant).getMillis();
        }
        return false;  
    }

    public Duration toStandardDuration() {
        long millis = 0;                  
        millis += getYears() * MILLIS_BY_YEAR;
        millis += getMonths() * MILLIS_BY_MONTH;
        millis += getDays() * MILLIS_BY_YEAR;
        millis += getHours() * MILLIS_BY_HOUR;
        millis += getMinutes() * MILLIS_BY_MINUTE;
        millis += getSeconds() * MILLIS_BY_SECOND;       
        millis += getMillis();
        return new DefaultDuration(millis);
    }
}

