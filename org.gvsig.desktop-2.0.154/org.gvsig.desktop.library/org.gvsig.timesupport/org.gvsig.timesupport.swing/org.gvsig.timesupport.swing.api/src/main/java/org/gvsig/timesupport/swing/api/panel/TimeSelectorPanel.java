/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.api.panel;

import java.util.List;

import javax.swing.JPanel;

import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.observer.Observable;

/**
 * Abstract class to controls window panels
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public abstract class TimeSelectorPanel extends JPanel implements Observable{
	private static final long serialVersionUID = 6668413815112740122L;

   /**
     * Intializes the control with an interval. If the interval is absolute,
     * the control shows absolute intervals and if is relative, the control
     * only shows relative intervals
     * @param interval
     *          the maximum and minimum instants that can be selected
     */
    public abstract void initialize(Interval interval);	
	
	/*
	 * Set the selected time
	 * @param time
	 *         the time to select
	 *
	public abstract void setSeletedTime(Time start);
	
	/**
	 * @return
	 *         the selected time
	 *
	public abstract Time getSelectedTime();*/
	
    /**
     * Set valid instants
     */
	public abstract void setInstants(List<Instant> instants);
}
