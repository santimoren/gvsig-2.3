/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;

import org.gvsig.i18n.Messages;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanel;
import org.gvsig.timesupport.swing.impl.components.InstantComparator;
import org.gvsig.timesupport.swing.impl.panel.TimePanel.TIME_MODE;
import org.gvsig.timesupport.swing.impl.rdv.TimeAdjustmentListener;
import org.gvsig.timesupport.swing.impl.rdv.TimeEvent;
import org.gvsig.tools.observer.Observer;

/**
 * Creates a panel with slider and time control calendars
 * 
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * 
 */
public class DefaultTimeSliderPanel extends TimeSelectorPanel implements
		TimeAdjustmentListener {

	private static final long serialVersionUID = 213502960867760481L;

	private JRadioButton rdbNone = null;
	private JRadioButton rdbInterval = null;
	private JRadioButton rdbInstant = null;
	private TimePanel timePanel = null;
	private List<Observer> observers = null;
	private JButton btnClose = null;
	private String title = null;
	private Interval interval;
	
	public DefaultTimeSliderPanel() {
		super();
		observers = new ArrayList<Observer>();
	}

    @Override
    public void initialize(Interval interval) {
    	this.interval = interval;
        initialize(this.interval.getStart(), this.interval.getEnd());
    }
	
	/**
	 * Intializes in x, y coordinates and start,end dates
	 * 
	 * @param x
	 * @param y
	 * @param start
	 * @param end
	 */
	public void initialize(Instant start, Instant end) {
	    removeAll();
        revalidate();
        repaint();
		createUI();
		if(start.isAbsolute())
			timePanel.setTimeMode(TIME_MODE.ABSOLUTE, start, end);
		else
			timePanel.setTimeMode(TIME_MODE.RELATIVE, start, end);
	}

	public void timeChanged(TimeEvent event) {
		if (getInstantRadioButton().isSelected()) {
			    Instant instante =  timePanel.getInstant();
				if (this.observers.size() != 0)
					for (Observer o : this.observers)
						o.update(this, new UpdateTimeNotification(instante));
		}
	}

	public void rangeChanged(TimeEvent event) {
		if (getIntervalRadioButton().isSelected()) {
				if (this.observers.size() != 0)
					for (Observer o : this.observers)
						o.update(this, new UpdateTimeNotification(timePanel.getInterval()));
		}
	}

	public void boundsChanged(TimeEvent event) {

	}

	@Override
	public void setBounds(Rectangle r) {
		super.setBounds(new Rectangle(r.x, r.y, WIDTH, HEIGHT));
	}

	
	public boolean isInterval() {
		// TODO Auto-generated method stub
		return getIntervalRadioButton().isSelected();
	}
	
	public boolean isInstant() {
		// TODO Auto-generated method stub
		return getInstantRadioButton().isSelected();
	}
	
	public boolean isNone() {
		// TODO Auto-generated method stub
		return getNoneRadioButton().isSelected();
	}

	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		observers.add(o);
	}

	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		observers.remove(o);
	}

	public void deleteObservers() {
		// TODO Auto-generated method stub
		observers.clear();
	}
	
	public void setInstants(Instant start, Instant end) {	
		timePanel.setInstants(start, end);
	}
	
	/*
	 * PRIVATE METHODS
	 */
	private void createUI() {
		Border etchedBdr = BorderFactory.createEtchedBorder();
		Border titledBdr = BorderFactory.createTitledBorder(etchedBdr,
				Messages.getText("temporal_filter"));
		setBorder(titledBdr);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		//getChkBoxChange().addChangeListener(this);

		add(createRadioButtons());

		timePanel = new TimePanel();
		timePanel.setListener(this);

		add(timePanel);

		getBtnClose().addActionListener(new TemporalActionListener(this));
		getBtnClose().setPreferredSize(new Dimension(110, 30));
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BorderLayout());
		JPanel panel = new JPanel(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0.5;
	    c.gridx = 2;
	    c.gridy = 0;
		panel.add(getBtnClose(),c);
		panel2.add(panel, BorderLayout.EAST);
		add(panel2);

		timePanel.fireRangeInterval();
	}

	private JPanel createRadioButtons() {
		ButtonGroup group = new ButtonGroup();
		getNoneRadioButton().addActionListener(new ActionNoneButton(this));
		group.add(getNoneRadioButton());
		getIntervalRadioButton().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				timePanel.setEnabled(true);
				timePanel.resetValues();
				timePanel.setValueChangeableSlider(false);
				timePanel.fireRangeInterval();
			}
		});
		getIntervalRadioButton().setSelected(true);
		group.add(getIntervalRadioButton());
		getInstantRadioButton().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				timePanel.setEnabled(true);
				timePanel.setEnabledEndCalendar(false);
				timePanel.setValueChangeableSlider(true);
				
				timePanel.resetValues();
				timePanel.fireValueInstant();
			}
		});
		group.add(getInstantRadioButton());
		// return group;
		JPanel radio = new JPanel();
		radio.setLayout(new FlowLayout());
		radio.add(getNoneRadioButton());
		radio.add(getIntervalRadioButton());
		radio.add(getInstantRadioButton());

		return radio;
	}

	private JRadioButton getNoneRadioButton() {
		if (rdbNone == null)
			rdbNone = new JRadioButton(Messages.getText("rdButton_none"));
		return rdbNone;
	}

	private JRadioButton getIntervalRadioButton() {
		if (rdbInterval == null)
			rdbInterval = new JRadioButton(
					Messages.getText("rdButton_interval"));
		return rdbInterval;
	}

	private JRadioButton getInstantRadioButton() {
		if (rdbInstant == null)
			rdbInstant = new JRadioButton(Messages.getText("rdButton_instant"));
		return rdbInstant;
	}

	public class TemporalActionListener implements ActionListener {

		private TimeSelectorPanel frame = null;

		public TemporalActionListener(TimeSelectorPanel p) {
			frame = p;
		}

		public void actionPerformed(ActionEvent e) {
		    for (Observer o : observers)
                o.update(frame, new CloseSelectorPanelNotification());
			frame.setVisible(false);
		}

	}

	private JButton getBtnClose() {
		if (btnClose == null)
			btnClose = new JButton(Messages.getText("temporal_btn_cancel"));
		return btnClose;
	}

	void noneButtonAction() {
		if (this.observers.size() != 0)
			for (Observer o : this.observers)
				o.update(this, new UpdateTimeNotification(null));
	}

	private class ActionNoneButton implements ActionListener {
		private DefaultTimeSliderPanel panel;

		public ActionNoneButton(DefaultTimeSliderPanel panel) {
			this.panel = panel;
		}

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			timePanel.setEnabled(false);
			
			this.panel.noneButtonAction();
		}

	}

	public void setInterval(Interval interval) {
		timePanel.setInterval((RelativeInterval)interval);	
	}


	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.title = name;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.title;
	}

    @Override
    public void setInstants(List<Instant> instants) {
        // TODO Auto-generated method stub
        java.util.Collections.sort(instants, new InstantComparator());
        //Delete duplicates
        Iterator<Instant> iterador = instants.iterator();
        List<Instant> sortedList = new ArrayList<Instant>();
        Instant obj;
        while(iterador.hasNext()){
            obj = iterador.next();
            if(!sortedList.contains(obj)){
                 sortedList.add(obj);
            }
        }
        timePanel.setInstants(sortedList);
    }
    
}
