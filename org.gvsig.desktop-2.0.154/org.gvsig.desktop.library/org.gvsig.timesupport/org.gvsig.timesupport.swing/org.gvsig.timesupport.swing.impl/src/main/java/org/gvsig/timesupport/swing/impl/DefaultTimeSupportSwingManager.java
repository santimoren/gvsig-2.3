/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl;

import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.TimeSupportSwingException;
import org.gvsig.timesupport.swing.api.TimeSupportSwingManager;
import org.gvsig.timesupport.swing.api.panel.AnimationBarPanel;
import org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanel;
import org.gvsig.timesupport.swing.impl.panel.DefaultTimeSliderPanel;
import org.gvsig.timesupport.swing.impl.panel.animation.AnimationBarPanelImpl;
import org.gvsig.timesupport.swing.impl.panel.animation.AnimationPreferencesPanelImpl;

/**
 * Manager for the Swing Timesupport to register all control classes.
 * 
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 */
public class DefaultTimeSupportSwingManager implements TimeSupportSwingManager{
	
	public TimeSelectorPanel createTimeSelectorPanel(Interval interval) throws TimeSupportSwingException{
            TimeSelectorPanel panel = new DefaultTimeSliderPanel();
            panel.initialize(interval);
            return panel;
	}

	public AnimationBarPanel createAnimationBarPanel(
			TimeAnimationDataModel dataModel) {
		return new AnimationBarPanelImpl(dataModel);
	}

	public AnimationPreferencesPanel createAnimationPreferencesPanel(
			TimeAnimationDataModel dataModel) {
		return new AnimationPreferencesPanelImpl(dataModel);
	}
}
