/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.swing.impl.panel.animation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.panel.AnimationBarPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * Main panel for animation bar
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 * 
 */
public class AnimationBarPanelImpl extends JPanel implements AnimationBarPanel, ActionListener, Observer {
	private static final long  serialVersionUID          = 1L;
	private static int         buttonSize                = 20;
	private String             pattern                   = "dd/MM/yyyy HH:mm:ss:SSS";
	
	private JSlider            sliderSpeed               = null;
	private JButton            buttonPlay                = null;
	private JButton            buttonStop                = null;
	private JButton            buttonPause               = null;
	private JButton            buttonClose               = null;
	private JTextField         textTimeStep              = null;
	private JTextField         textTimeSpeed             = null;
	private JTextField         textWindowTime            = null;
	private JLabel             labelMinInterval          = null;
	private JLabel             labelMaxInterval          = null;
	private JLabel             timeStep                  = null;
	private JLabel             speedOfProgress           = null;
	
	private JPanel             panelSlider               = null;
	private JPanel             panelInfo                 = null;
	private long               startMillis               = 0;
	private long               endMillis                 = 0;
	
	public AnimationBarPanelImpl(TimeAnimationDataModel dataModel) {
		((Observable)dataModel).addObserver(this);
		setTimeWindow(dataModel.getInitTime(), dataModel.getEndTimeWindow());
		setTimeInterval(dataModel.getInitTime(), dataModel.getEndTime());
		setTimeStep(dataModel.getTimeStep(), dataModel.getTimeStepScale());
		setSpeedOfProgress(dataModel.getSpeed());
		
		init();
	}
	
	private void init() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		
		add(getPanelSlider(), gbc);
		
		gbc.gridy = 1;
		add(getPanelInfo(), gbc);
	}
	
	public JPanel getPanelInfo() {
		if(panelInfo == null) {
			panelInfo = new JPanel();
			panelInfo.setLayout(new GridBagLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(2, 2, 2, 2);
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			
			gbc.gridx = 0;
			panelInfo.add(getLabelTimeStep(), gbc);
			
			gbc.gridx = 1;
			panelInfo.add(getTextTimeStep(), gbc);
			
			gbc.gridx = 2;
			panelInfo.add(getLabelSpeedOfProgress(), gbc);
			
			gbc.gridx = 3;
			panelInfo.add(getTextSpeedOfProgress(), gbc);
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridx = 4;
			panelInfo.add(new JPanel(), gbc);
			
			/*gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			gbc.gridx = 5;
			panelInfo.add(getButtonClose(), gbc);*/
		}
		return panelInfo;
	}
	
	public JPanel getPanelSlider() {
		if(panelSlider == null) {
			panelSlider = new JPanel();
			panelSlider.setLayout(new GridBagLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(2, 2, 2, 2);
			
			//Fila 0
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridwidth = 5;
			
			gbc.gridx = 0;
			gbc.gridy = 0;
			panelSlider.add(getTextWindowTime(), gbc);
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			gbc.gridwidth = 3;
			
			gbc.gridx = 5;
			gbc.gridy = 0;
			panelSlider.add(new JPanel(), gbc);
			
			//Fila 1
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridwidth = 5;
			gbc.gridx = 0;
			gbc.gridy = 1;
			panelSlider.add(getSliderSpeed(), gbc);
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			
			gbc.gridwidth = 1;
			gbc.gridx = 5;
			gbc.gridy = 1;
			panelSlider.add(getButtonPlay(), gbc);
			
			gbc.gridx = 6;
			gbc.gridy = 1;
			panelSlider.add(getButtonStop(), gbc);
			
			gbc.gridx = 7;
			gbc.gridy = 1;
			panelSlider.add(getButtonPause(), gbc);
			
			//Fila 2
			
			gbc.gridx = 0;
			gbc.gridy = 2;
			panelSlider.add(getLabelMinInterval(), gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 2;
			panelSlider.add(getTextTimeStep(), gbc);
			
			gbc.anchor = GridBagConstraints.EAST;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridx = 2;
			gbc.gridy = 2;
			panelSlider.add(new JPanel(), gbc);
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			gbc.gridx = 3;
			gbc.gridy = 2;
			panelSlider.add(getTextSpeedOfProgress(), gbc);
			
			gbc.gridx = 4;
			gbc.gridy = 2;
			panelSlider.add(getLabelMaxInterval(), gbc);
			
			gbc.gridx = 5;
			gbc.gridy = 2;
			panelSlider.add(new JPanel(), gbc);
		}
		return panelSlider;
	}
	
	public JSlider getSliderSpeed() {
		if(sliderSpeed == null) {
			sliderSpeed = new JSlider(0, 100);
			sliderSpeed.setPaintTicks(true);
			sliderSpeed.setMajorTickSpacing(10);
			sliderSpeed.setMinorTickSpacing(1);
			sliderSpeed.setValue(0);
		}
		return sliderSpeed;
	}

	public JButton getButtonPlay() {
		if(buttonPlay == null) {
			URL play = getClass().getClassLoader().getResource("org/gvsig/timesupport/swing/impl/images/play.png");
			ImageIcon icon = new ImageIcon(play);
			if(icon != null)
				buttonPlay = new JButton(icon);
			else 
				buttonPlay = new JButton();
			buttonPlay.setPreferredSize(new Dimension(buttonSize, buttonSize));
			buttonPlay.addActionListener(this);
			buttonPlay.setEnabled(true);
		}
		return buttonPlay;
	}

	public JButton getButtonStop() {
		if(buttonStop == null) {
			URL stop = getClass().getClassLoader().getResource("org/gvsig/timesupport/swing/impl/images/stop.png");
			ImageIcon icon = new ImageIcon(stop);
			if(icon != null)
				buttonStop = new JButton(icon);
			else 
				buttonStop = new JButton();
			buttonStop.setPreferredSize(new Dimension(buttonSize, buttonSize));
			buttonStop.addActionListener(this);
			buttonStop.setEnabled(false);
		}
		return buttonStop;
	}

	public JButton getButtonPause() {
		if(buttonPause == null) {
			URL pause = getClass().getClassLoader().getResource("org/gvsig/timesupport/swing/impl/images/pause.png");
			ImageIcon icon = new ImageIcon(pause);
			if(icon != null)
				buttonPause = new JButton(icon);
			else 
				buttonPause = new JButton();
			buttonPause.setPreferredSize(new Dimension(buttonSize, buttonSize));
			buttonPause.addActionListener(this);
			buttonPause.setEnabled(false);
		}
		return buttonPause;
	}
	
	public JButton getButtonClose() {
		if(buttonClose == null) {
			URL close = getClass().getClassLoader().getResource("org/gvsig/timesupport/swing/impl/images/close.png");
			ImageIcon icon = new ImageIcon(close);
			if(icon != null)
				buttonClose = new JButton(icon);
			else 
				buttonClose = new JButton();
			buttonClose.setPreferredSize(new Dimension(buttonSize, buttonSize));
			buttonClose.addActionListener(this);
			buttonClose.setEnabled(true);
		}
		return buttonClose;
	}
	
	public JLabel getLabelMinInterval() {
		if(labelMinInterval == null) {
			labelMinInterval = new JLabel("31/12/2000 12:00:00 AM", JLabel.LEFT);
		}
		return labelMinInterval;
	}
	
	public JLabel getLabelMaxInterval() {
		if(labelMaxInterval == null) {
			labelMaxInterval = new JLabel("1/1/2012 12:00:00 AM", JLabel.RIGHT);
		}
		return labelMaxInterval;
	}
	
	public JLabel getLabelTimeStep() {
		if(timeStep == null) {
                        I18nManager i18nManager = ToolsLocator.getI18nManager();
			timeStep = new JLabel(i18nManager.getTranslation("timestep"));
		}
		return timeStep;
	}

	public JLabel getLabelSpeedOfProgress() {
		if(speedOfProgress == null) {
                        I18nManager i18nManager = ToolsLocator.getI18nManager();
			speedOfProgress = new JLabel(i18nManager.getTranslation("speedofprogress"));
		}
		return speedOfProgress;
	}

	public JTextField getTextTimeStep() {
		if(textTimeStep == null) {
			textTimeStep = new JTextField("2 years");
			textTimeStep.setPreferredSize(new Dimension(70, 20));
			textTimeStep.setEditable(false);
		}
		return textTimeStep;
	}

	public JTextField getTextSpeedOfProgress() {
		if(textTimeSpeed == null) {
			textTimeSpeed = new JTextField();
			textTimeSpeed.setPreferredSize(new Dimension(70, 20));
			textTimeSpeed.setEditable(false);
		}
		return textTimeSpeed;
	}

	public JTextField getTextWindowTime() {
		if(textWindowTime == null) {
			textWindowTime = new JTextField();
			textWindowTime.setEditable(false);
			textWindowTime.setHorizontalAlignment(JTextField.CENTER);
		}
		return textWindowTime;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#setSpeedOfProgress(int)
	 */
	private void setSpeedOfProgress(int speed) {
		getTextSpeedOfProgress().setText(speed + "");
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#setTimeStep(int, int)
	 */
	private void setTimeStep(int timeStep, int scale) {
		String textScale = "year";
		switch (scale) {
		case TimeAnimation.YEAR: textScale = "year"; break;
		case TimeAnimation.MONTH: textScale = "month"; break;
		case TimeAnimation.WEEK: textScale = "week"; break;
		case TimeAnimation.DAY: textScale = "day"; break;
		case TimeAnimation.HOUR: textScale = "hour"; break;
		case TimeAnimation.MINUTE: textScale = "minute"; break;
		case TimeAnimation.SECOND: textScale = "second"; break;
		case TimeAnimation.MILLISECOND: textScale = "millisecond"; break;
		}
                I18nManager i18nManager = ToolsLocator.getI18nManager();
		getTextTimeStep().setText(timeStep + " " + i18nManager.getTranslation(textScale));
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#setTimeInterval(org.gvsig.timesupport.RelativeInstant, org.gvsig.timesupport.RelativeInstant)
	 */
	private void setTimeInterval(RelativeInstant start, RelativeInstant end) {
		startMillis = start.toMillis();
		endMillis = end.toMillis();
		getLabelMinInterval().setText(start.toString(pattern));
		getLabelMaxInterval().setText(end.toString(pattern));
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#setTimeWindow(org.gvsig.timesupport.RelativeInstant, org.gvsig.timesupport.RelativeInstant)
	 */
	private void setTimeWindow(RelativeInstant start, RelativeInstant end) {
		getTextWindowTime().setText(start.toString(pattern) + " to " + end.toString(pattern));
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#updateSliderPosition(org.gvsig.timesupport.RelativeInstant)
	 */
	public void updateSliderPosition(RelativeInstant start) {
		long dist = endMillis - startMillis;
		long s = start.toMillis();
		long dist1 = s - startMillis;
		if(dist > 0) {
			int positionBar = (int)((dist1 * 100) / dist);
			getSliderSpeed().setValue(positionBar);
		}
	}
	
	/**
	 * Adds the listener for de accept button
	 * @param listener
	 */
	public void addButtonsListener(ActionListener listener) {
		getButtonPlay().addActionListener(listener);
		getButtonPause().addActionListener(listener);
		getButtonStop().addActionListener(listener);
		getButtonClose().addActionListener(listener);
	}
	
	/**
	 * Gets the ID of the component selected. The ID is a constant defined in this
	 * interface. This is useful to know which component is throwing an event. 
	 * @return
	 */
	public int getIDSource(Object obj) {
		if(obj == getButtonPlay())
			return TimeAnimationDataModel.PLAY;
		if(obj == getButtonStop())
			return TimeAnimationDataModel.STOP;
		if(obj == getButtonPause())
			return TimeAnimationDataModel.PAUSE;
		if(obj == getButtonClose())
			return TimeAnimationDataModel.CLOSE;
		return -1;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == getButtonPlay()) {
			getButtonPlay().setEnabled(false);
			getButtonStop().setEnabled(true);
			getButtonPause().setEnabled(true);
		}
		if(e.getSource() == getButtonStop()) {
			getButtonPlay().setEnabled(true);
			getButtonStop().setEnabled(false);
			getButtonPause().setEnabled(false);
		}
		if(e.getSource() == getButtonPause()) {
			if(!getButtonPlay().isEnabled() && getButtonStop().isEnabled()) { //Si est� en marcha
				getButtonPlay().setEnabled(false);
				getButtonStop().setEnabled(false);
				getButtonPause().setEnabled(true);
			} else {
				getButtonPlay().setEnabled(false);
				getButtonStop().setEnabled(true);
				getButtonPause().setEnabled(true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationBarPanel#resetButtonsStatus()
	 */
	public void resetButtonsStatus() {
		getButtonPlay().setEnabled(true);
		getButtonStop().setEnabled(false);
		getButtonPause().setEnabled(false);
		getButtonClose().setEnabled(true);
	}

	public void update(Observable o, Object arg) {
		TimeAnimationDataModel dataModel = (TimeAnimationDataModel)o;
		setTimeWindow(dataModel.getInitTimeWindow(), dataModel.getEndTimeWindow());
		updateSliderPosition(dataModel.getSliderPosition());
	}
}
