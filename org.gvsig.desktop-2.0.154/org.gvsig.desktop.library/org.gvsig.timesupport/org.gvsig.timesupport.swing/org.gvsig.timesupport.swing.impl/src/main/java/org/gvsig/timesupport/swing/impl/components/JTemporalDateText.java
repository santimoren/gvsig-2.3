/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.components;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.gvsig.i18n.Messages;
import org.gvsig.timesupport.AbsoluteInstant;

/**
 * Control to manipulate absolute times
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public class JTemporalDateText extends JPanel implements DateTextListener  {

    private static final long serialVersionUID = 3122343944275532842L;
    private JTextField txtYears = null;
	private JLabel lblYears = null;
	private JTextField txtMonths = null;
	private JLabel lblMonths = null;
	private JTextField txtWeeks = null;
	private JLabel lblWeeks = null;
	private JTextField txtDays = null;
	private JLabel lblDays = null;
	private JTextField txtHours = null;
	private JLabel lblHours = null;
	private JTextField txtMinutes = null;
	private JLabel lblMinutes = null;
	private JTextField txtSeconds = null;
	private JLabel lblSeconds = null;
	private JTextField txtMillis = null;
	private JLabel lblMillis = null;

	private DateTextListener listener = null;
	
	public JTemporalDateText() {
		createUI();
	}

	public void setListener(DateTextListener listener)
	{
		this.listener = listener;
	}
	
	public DateTextListener getListener()
	{
		return this.listener;
	}
	
	public void setDate(int years, int months, int weeks, int days, int hours,
			int minutes, int seconds, int millis) {
		setYears(years);
		setMonths(months);
		setWeeks(weeks);
		setDays(days);
		setHours(hours);
		setMinutes(minutes);
		setSeconds(seconds);
		setMillis(millis);
	}
	
	public void setInstant(AbsoluteInstant instant)
	{
		setDate(instant.getYears(), instant.getMonths(), instant.getWeeks(), instant.getDays(), instant.getHours(), instant.getMinutes(), instant.getSeconds(), instant.getMillis());
	}

	public void setYears(int years) {
		if(years >=0)
			txtYears.setText(Integer.toString(years));
	}
	
	public int getYears() throws NumberFormatException
	{
		return Integer.parseInt(txtYears.getText());
	}

	public void setMonths(int months) {
		if(months >= 1)
			txtMonths.setText(Integer.toString(months));
	}
	
	public int getMonthOfYear()throws NumberFormatException
	{
		return Integer.parseInt(txtMonths.getText());
	}

	public void setWeeks(int weeks)
	{
		if(weeks >= 1)
		txtWeeks.setText(Integer.toString(weeks));
	}
	
	public int getWeekOfWeekYear()throws NumberFormatException
	{
		return Integer.parseInt(txtWeeks.getText());
	}
	
	public void setDays(int days) {
		if(days >=1)
		txtDays.setText(Integer.toString(days));
	}

	public int getDayOfMonth()throws NumberFormatException
	{
		return Integer.parseInt(txtDays.getText());
	}
	public void setHours(int hours) {
		if(hours >= 1)
		txtHours.setText(Integer.toString(hours));
	}

	public int getHourOfDay()throws NumberFormatException
	{
		return Integer.parseInt(txtHours.getText());
	}
	public void setMinutes(int minutes) {
		if(minutes >= 0)
			txtMinutes.setText(Integer.toString(minutes));
	}
	
	public int getMinuteOfHour()throws NumberFormatException
	{
		return Integer.parseInt(txtMinutes.getText());
	}

	public void setSeconds(int seconds) {
		if(seconds >= 0)
			txtSeconds.setText(Integer.toString(seconds));
	}

	public int getSecondOfMinute()throws NumberFormatException
	{
		return Integer.parseInt(txtSeconds.getText());
	}
	public void setMillis(int millis) {
		if(millis >= 0)
			txtMillis.setText(Integer.toString(millis));
	}
	
	public int getMillisOfSecond()throws NumberFormatException
	{
		return Integer.parseInt(txtMillis.getText());
	}
	

	/**** PRIVATE METHODS ****/

	private void createUI() {
		lblYears = new JLabel(Messages.getText("lbl_sos_years"));
		txtYears = new JTextField();
		txtYears.addFocusListener(this);
		txtYears.addActionListener(this);
		lblMonths = new JLabel(Messages.getText("lbl_sos_months"));
		txtMonths = new JTextField();
		txtMonths.addFocusListener(this);
		txtMonths.addActionListener(this);
		lblWeeks = new JLabel(Messages.getText("lbl_sos_weeks"));
		txtWeeks = new JTextField();
		txtWeeks.addFocusListener(this);
		txtMonths.addActionListener(this);
		lblDays = new JLabel(Messages.getText("lbl_sos_days"));
		txtDays = new JTextField();
		txtDays.addFocusListener(this);
		txtDays.addActionListener(this);
		lblHours = new JLabel(Messages.getText("lbl_sos_hours"));
		txtHours = new JTextField();
		txtHours.addFocusListener(this);
		txtHours.addActionListener(this);
		lblMinutes = new JLabel(Messages.getText("lbl_sos_minutes"));
		txtMinutes = new JTextField();
		txtMinutes.addFocusListener(this);
		txtMinutes.addActionListener(this);
		lblSeconds = new JLabel(Messages.getText("lbl_sos_seconds"));
		txtSeconds = new JTextField();
		txtSeconds.addFocusListener(this);
		txtSeconds.addActionListener(this);
		lblMillis = new JLabel(Messages.getText("lbl_sos_millis"));
		txtMillis = new JTextField();
		txtMillis.addFocusListener(this);
		txtMillis.addActionListener(this);
		
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		
		add(lblYears,constraints);
		constraints.gridx = 2;
		add(txtYears,constraints);
		
		constraints.gridx = 3;
		add(lblMonths,constraints);
		constraints.gridx = 4;
		add(txtMonths,constraints);

		constraints.gridx = 5;
		add(lblWeeks,constraints);
		constraints.gridx = 6;
		add(txtWeeks,constraints);
		
		constraints.gridx = 7;
		add(lblDays,constraints);
		constraints.gridx = 8;
		add(txtDays,constraints);
		
		constraints.gridx = 9;
		add(lblHours,constraints);
		constraints.gridx = 10;
		add(txtHours,constraints);
		
		constraints.gridx = 11;
		add(lblMinutes,constraints);
		constraints.gridx = 12;
		add(txtMinutes,constraints);
		
		constraints.gridx = 13;
		add(lblSeconds,constraints);
		constraints.gridx = 14;
		add(txtSeconds,constraints);
		
		constraints.gridx = 15;
		add(lblMillis,constraints);
		constraints.gridx = 16;
		add(txtMillis,constraints);
	}
	
	@Override
	public void setEnabled(boolean enabled)
	{
		lblYears.setEnabled(enabled);
		txtYears.setEnabled(enabled);
		lblMonths.setEnabled(enabled);
		txtMonths.setEnabled(enabled);
		lblWeeks.setEnabled(enabled);
		txtWeeks.setEnabled(enabled);
		lblDays.setEnabled(enabled);
		txtDays.setEnabled(enabled);
		lblHours.setEnabled(enabled);
		txtHours.setEnabled(enabled);
		lblMinutes.setEnabled(enabled);
		txtMinutes.setEnabled(enabled);
		lblSeconds.setEnabled(enabled);
		txtSeconds.setEnabled(enabled);
		lblMillis.setEnabled(enabled);
		txtMillis.setEnabled(enabled);
	}

	public void focusGained(FocusEvent e) {
	}

	public void focusLost(FocusEvent e) {
		if(this.listener != null)
			listener.focusLost(new FocusEvent(this,0));
	}

	public void actionPerformed(ActionEvent e) {
		if(this.listener != null)
			listener.actionPerformed(new ActionEvent(this,0,"Enter"));
	}
	
}
