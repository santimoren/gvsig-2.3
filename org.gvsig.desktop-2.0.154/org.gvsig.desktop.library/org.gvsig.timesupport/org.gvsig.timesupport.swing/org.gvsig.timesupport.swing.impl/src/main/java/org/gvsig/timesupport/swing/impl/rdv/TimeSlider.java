/*
 * RDV
 * Real-time Data Viewer
 * http://rdv.googlecode.com/
 * 
 * Copyright (c) 2005-2007 University at Buffalo
 * Copyright (c) 2005-2007 NEES Cyberinfrastructure Center
 * Copyright (c) 2008 Palta Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * $URL: http://rdv.googlecode.com/svn/trunk/src/org/rdv/ui/TimeSlider.java $
 * $Revision: 1149 $
 * $Date: 2008-07-03 16:23:04 +0200 (jue 03 de jul de 2008) $
 * $Author: jason@paltasoftware.com $
 */

package org.gvsig.timesupport.swing.impl.rdv;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolTip;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteInstantTypeNotRegisteredException;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;

/**
 * A component that lets the user select a specific time or time range between a
 * bounded time interval by sliding knobs along a timeline. It also allows one
 * to display an event indicator at a specific time in the time line. (Modified by Pablo Viciano)
 * 
 * @author Jason P. Hanley
 */
public class TimeSlider extends JComponent implements MouseListener,
MouseMotionListener {
    public static final long MILLIS_BY_SECOND = 1000;
    public static final long MILLIS_BY_MINUTE = MILLIS_BY_SECOND * 60;
    public static final long MILLIS_BY_HOUR = MILLIS_BY_MINUTE * 60;
    public static final long MILLIS_BY_DAY = MILLIS_BY_HOUR * 24;
    public static final long MILLIS_BY_WEEK = MILLIS_BY_DAY * 7;
    public static final long MILLIS_BY_MONTH = MILLIS_BY_DAY * 30;
    public static final long MILLIS_BY_YEAR = MILLIS_BY_DAY * 365;

    private static final Logger LOG = LoggerFactory.getLogger(TimeSlider.class);

    private static TimeSupportManager timeSupportManager = TimeSupportLocator.getManager();

    boolean isAbsolute = false;

    /** serialization version identifier */
    private static final long serialVersionUID = 2429052022677466231L;

    /** The minimum time. */
    protected double minimum;

    /** The start of the selected time range. */
    protected double start;

    /** The selected time. */
    protected double value;

    /** The end of the selected time range. */
    protected double end;

    /** The maximum time. */
    protected double maximum;

    /** Indicate if the time value may be changed from the UI. */
    private boolean valueChangeable;

    /** Indicates if a time range may be changed from the UI. */
    private boolean rangeChangeable;

    /** Indicates if the time value is adjusting. */
    private boolean isAdjusting;

    /** The starting offset of a click (for buttons). */
    private int clickStart;

    /**
     * List of event markers. * protected final List<EventMarker> markers;
     * 
     * /** List of valid time ranges.
     */
    protected final List<TimeRange> timeRanges;

    /** List of valid times ranges, taking into account the minimum and maximum */
    protected final List<TimeRange> actualTimeRanges;

    /** List of time adjustment listeners. */
    private final List<TimeAdjustmentListener> adjustmentListeners;

    /** The button used to indicate the start time. */
    protected final JButton startButton;

    /** The button used to indicate the time. */
    protected final JButton valueButton;

    /** The button used to indicate the end time. */
    protected final JButton endButton;

    protected boolean right;
    protected boolean left;
    /**
     * Creates a time slider with the maximum available range.
     */
    public TimeSlider(boolean isAbsolute) {
        super();
        this.isAbsolute = isAbsolute;    

        minimum = 0;
        start = 0;
        value = 0;
        end = Double.MAX_VALUE;
        maximum = Double.MAX_VALUE;

        right = true;
        left = true;

        rangeChangeable = true;

        isAdjusting = false;

        timeRanges = new ArrayList<TimeRange>();

        actualTimeRanges = new ArrayList<TimeRange>();
        calculateActualTimeRanges();

        adjustmentListeners = new ArrayList<TimeAdjustmentListener>();

        setBorder(null);
        setLayout(null);

        setToolTipText("");

        addMouseListener(this);
      
          valueButton = new JButton() {
            /** serialization version identifier */
            private static final long serialVersionUID = 8729851598067678522L;

            public JToolTip createToolTip() {
                return TimeSlider.this.createToolTip();
            }

            public String getToolTipText(MouseEvent me) {
                return TimeSlider.this.getToolTipText(me);
            }
        };
        valueButton.setToolTipText("");
        valueButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        valueButton.setBorder(null);
        valueButton.addMouseMotionListener(this);
        valueButton.addMouseListener(this);        
        try {
            BufferedImage bufferedImage = ImageIO.read(getClass().getClassLoader()
                .getResource("org/gvsig/timesupport/swing/impl/images/time.png"));
            valueButton.setIcon(new ImageIcon(getScaledImage(bufferedImage, 7, 7)));
        } catch (IOException e) {
            valueButton.setBackground(Color.BLACK);
        }
        add(valueButton);

        startButton = new JButton();
        startButton.setBorder(null);
        startButton.addMouseListener(this);
        startButton.addMouseMotionListener(this);
        try {
            BufferedImage bufferedImage = ImageIO.read(getClass().getClassLoader()
                .getResource("org/gvsig/timesupport/swing/impl/images/left_bound.png"));
            startButton.setIcon(new ImageIcon(getScaledImage(bufferedImage, 6, 11)));
        } catch (IOException e) {
            startButton.setBackground(Color.BLACK);
        }
        add(startButton);

        endButton = new JButton(); 
        endButton.setBorder(null);
        endButton.addMouseListener(this);
        endButton.addMouseMotionListener(this);
        try {
            BufferedImage bufferedImage = ImageIO.read(getClass().getClassLoader()
                .getResource("org/gvsig/timesupport/swing/impl/images/right_bound.png"));
            endButton.setIcon(new ImageIcon(getScaledImage(bufferedImage, 6, 11)));
        } catch (IOException e) {
            endButton.setBackground(Color.BLACK);
        }
        add(endButton);

        setValueChangeable(true);
    }

    private Image getScaledImage(BufferedImage image, int w, int h)  
    {   
        double scaleW = w/image.getWidth();
        double scaleH = h/image.getHeight();
        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);  
        Graphics2D g2 = bi.createGraphics();  
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,  
            RenderingHints.VALUE_INTERPOLATION_BILINEAR);  
        AffineTransform at = AffineTransform.getScaleInstance(scaleW, scaleH);  
        g2.drawRenderedImage(image, at);  
        g2.dispose();  
        return bi;  
    } 

    /**
     * Sets if the time value of the slider can be changed by the UI element.
     * 
     * @param changeable
     *            if true, the value can be changed, false otherwise.
     */
    public void setValueChangeable(boolean changeable) {
        if (valueChangeable == changeable) {
            return;
        }

        valueChangeable = changeable;

        valueButton.setEnabled(valueChangeable);

        if (valueChangeable) {
            addMouseListener(this);
            valueButton.addMouseListener(this);
            valueButton.addMouseMotionListener(this);
            valueButton.setVisible(true);
            startButton.setCursor(null);
            endButton.setCursor(null);
        } else {
            removeMouseListener(this);
            valueButton.removeMouseListener(this);
            valueButton.removeMouseMotionListener(this);
            valueButton.setVisible(false);
            startButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            endButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }
    }

    public boolean getValueChangeable()
    {
        return valueChangeable;
    }

    /**
     * Set if the value is visible in the slider UI.
     * 
     * @param visible
     *            if true the value is shown, if false the value is not shown
     */
    public void setValueVisible(boolean visible) {
        if (valueButton.isVisible() == visible) {
            return;
        }

        valueButton.setVisible(visible);
    }

    /**
     * Set if a time range can be changed via the UI elements
     * 
     * @param changeable
     *            if true, the time range can be changed, false otherwsie
     */
    public void setRangeChangeable(boolean changeable) {
        if (rangeChangeable == changeable) {
            return;
        }

        rangeChangeable = changeable;

        startButton.setEnabled(rangeChangeable);
        endButton.setEnabled(rangeChangeable);

        if (rangeChangeable) {
            startButton.addMouseMotionListener(this);
            endButton.addMouseMotionListener(this);
        } else {
            startButton.removeMouseMotionListener(this);
            endButton.removeMouseMotionListener(this);
        }
    }

    /**
     * Gets the time value set by the slider.
     * 
     * @return the time set by the slider
     */
    private double getValue() {
        return value;
    }

    /**
     * Sets the time value of the slider.
     * 
     * @param value
     *            the time value to set
     */
    private void setValue(double value) {
        if (this.value != value) {
            this.value = value;
            doLayout();
            fireValueChanged();
        }
    }

    public void moveRight(boolean right)
    {
        this.right = right;
    }

    public void moveLeft(boolean left)
    {
        this.left = left;
    }

    /**
     * Sets the current time of the slider
     * @param current
     */
    public void setCurrentInstant(Instant currentInstant)
    {
        long millis = toMillis(currentInstant);
        if(millis >= this.minimum && millis <= this.maximum)
            setValue(millis);
        else if(millis < this.minimum)
            setValue(this.minimum);
        else
            setValue(this.maximum);
    }

    /**
     * Set the start DateTime of the slider
     * @param start
     */
    public void setStartInstant(Instant startInstant)
    {
        double millis = toMillis(startInstant);
        if(millis > this.minimum && millis <= this.end)
            setStart(millis);
        else if(millis > this.end)
            setStart(this.end);
        else
            setStart(this.minimum);
    }

    /**
     * Sets the end DateTime of the slider
     * @param end
     */
    public void setEndInstant(Instant endInstant)
    {
        double millis = toMillis(endInstant);
        if(millis < this.maximum && millis >= this.start)
            setEnd(millis);
        else if(millis < this.start)
            setEnd(this.start);
        else
            setEnd(this.maximum);
    }

    public void setValues(Instant minimunInstant, Instant maximumInstant) {        
        setValues(toMillis(minimunInstant), toMillis(maximumInstant));
    }   

    /**
     * Sets the maximum and minimum values.
     * 
     * @param minimum
     *            the minimum allowed time value
     * @param maximum
     *            the maximum allowed time value
     */
    private void setValues(long minimum, long maximum) {
        setMinimum(minimum);
        setMaximum(maximum);
        setValue(this.minimum);
    }

    /**
     * Sets the time value, and the maximum and minimum time values.
     * 
     * @param value
     *            the time value
     * @param minimum
     *            the minimum allowed time value
     * @param maximum
     *            the maximum allowed time value
     *
    private void setValues(double value, double minimum, double maximum) {
        setMinimum(minimum);
        setMaximum(maximum);
        setValue(value);
    }

    /**
     * Sets the maximum and minimum time value, and the time range.
     * 
     * @param minimum
     *            the minimum allowed time value
     * @param maximum
     *            the maximum allowed time value
     * @param start
     *            the start of the time range
     * @param end
     *            the end of the time range
     *
    private void setValues(long minimum, long maximum, long start, long end) {
        setStart(start);
        setEnd(end);
        setMinimum(minimum);
        setMaximum(maximum);
    }

    /**
     * Sets the maximum and minimum time value, the time range, and the time
     * value.
     * 
     * @param value
     *            the time value
     * @param minimum
     *            the minimum allowed time value
     * @param maximum
     *            the maximum allowed time value
     * @param start
     *            the start of the time range
     * @param end
     *            the end of the time range
     *
    private void setValues(double value, double minimum, double maximum,
        double start, double end) {
        setStart(start);
        setEnd(end);
        setMinimum(minimum);
        setMaximum(maximum);
        setValue(value);
    }*/

    /**
     * Gets the start of the selected time range.
     * 
     * @return the start of the time range
     */
    private double getStart() {
        return start;
    }

    /**
     * Sets the start of the time range.
     * 
     * @param start
     *            the start of the time range
     */
    private void setStart(double start) {
        if (this.start != start) {
            this.start = start;
            doLayout();
            fireRangeChanged();
        }
    }

    /**
     * Gets the end of the selected time range.
     * 
     * @return the end of the time range
     */
    private double getEnd() {
        return end;
    }

    /**
     * Sets the end of the time range.
     * 
     * @param end
     *            the end of the time range
     */
    private void setEnd(double end) {
        if (this.end != end) {
            this.end = end;
            doLayout();
            fireRangeChanged();
        }
    }

    /**
     * Gets the minimum allowed time value.
     * 
     * @return the minimum allowed time value
     */
    public double getMinimum() {
        return minimum;
    }

    /**
     * Sets the minimum allowed time value. If this is greater than the selected
     * time range, it will be set to be equal to this value.
     * 
     * @param minimum
     *            the minimum allowed time value
     */
    private void setMinimum(double minimum) {
        if (this.minimum != minimum) {
            this.minimum = minimum;

            calculateActualTimeRanges();

            if (rangeChangeable) {
                if (minimum > start) {
                    setStart(minimum);
                    fireRangeChanged();
                }
                if (minimum > end) {
                    setEnd(minimum);
                    fireRangeChanged();
                }
            } else {
                setStart(minimum);
            }

            fireBoundsChanged();
            doLayout();
        }
    }

    /**
     * Gets the maximum allowed time value.
     * 
     * @return the maximum allowed time value.
     */
    public double getMaximum() {
        return maximum;
    }

    /**
     * Sets the maximum allowed time value. If this is less than the selected
     * time range, it will be set to be equal to this value.
     * 
     * @param maximum
     *            the maximym allowed time value
     */
    private void setMaximum(double maximum) {
        if (this.maximum != maximum) {
            this.maximum = maximum;

            calculateActualTimeRanges();

            if (rangeChangeable) {
                if (maximum < end) {
                    setEnd(maximum);
                    fireRangeChanged();
                }
                if (maximum < start) {
                    setStart(maximum);
                    fireRangeChanged();
                }
            } else {
                setEnd(maximum);
            }

            fireBoundsChanged();
            doLayout();
        }
    }

    /**
     * Reset to minimum date
     */
    public void resetMinimumBound() {
        setStart(this.minimum);
    }

    /**
     * Reset to maximum date
     */
    public void resetMaximumBound() {
        setEnd(this.maximum);
    }

    /**
     * Reset to initial value date
     */
    public void resetValue() {
        setValue(this.minimum);
    }

    /**
     * Reset all dates
     */
    public void resetAllDates() {
        resetMinimumBound();
        resetMaximumBound();
        resetValue();
    }

    /**
     * Replace the current set of time ranges with this one.
     * 
     * @param newTimeRanges
     *            a list of time ranges
     */
    public void setTimeRanges(List<TimeRange> newTimeRanges) {
        timeRanges.clear();
        timeRanges.addAll(newTimeRanges);

        calculateActualTimeRanges();

        double newValue = getClosestValidTime(value);
        if (value != newValue) {
            value = newValue;
            fireValueChanged();
        }

        double newStart = getClosestValidTime(start);
        double newEnd = getClosestValidTime(end);
        if (start != newStart || end != newEnd) {
            setStart(newStart);
            setEnd(newEnd);
            fireRangeChanged();
        }

        doLayout();
    }

    /**
     * Tell if the time is valid with respect to the current time bounds and
     * ranges.
     * 
     * @param time
     *            the time to check
     * @return true if the time is valid, false if it is not
     */
    public boolean isTimeValid(double time) {
        for (TimeRange timeRange : actualTimeRanges) {
            if (timeRange.contains(time)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a time that is valid and greater than or equal to the current one.
     * 
     * @return the next valid time, or -1 if there is none
     */
    public double getNextValidTime(double time) {
        for (TimeRange tr : actualTimeRanges) {
            if (tr.contains(time)) {
                return time;
            } else if (tr.start > time) {
                return tr.start;
            }
        }

        return -1;
    }

    /**
     * Return a time that is valid and the closest possible to the given time.
     * If the provided time is valid, then the same time will be returned. If
     * the time is invalid, the nearest valid time will be returned.
     * 
     * @param time
     *            the time to use
     * @return the closest valid time to the given time
     */
    private double getClosestValidTime(double time) {
        double possibleTime = 0;

        for (int i = 0; i < actualTimeRanges.size(); i++) {
            TimeRange tr = actualTimeRanges.get(i);

            if (i == 0 && time < tr.start) {
                return tr.start;
            } else if (i == actualTimeRanges.size() - 1 && time > tr.end) {
                return tr.end;
            } else if (tr.contains(time)) {
                return time;
            } else if (time > tr.end) {
                possibleTime = tr.end;
            } else if (time - possibleTime < tr.start - time) {
                return possibleTime;
            } else {
                return tr.start;
            }
        }

        // this shouldn't be possible
        return time;
    }

    /**
     * Remove all time ranges.
     */
    public void clearTimeRanges() {
        timeRanges.clear();
        calculateActualTimeRanges();
        doLayout();
    }

    /**
     * Indicates if the value is being continuously changed in the UI.
     * 
     * @return true if the value is changing, false otherwise
     */
    public boolean isValueAdjusting() {
        return isAdjusting;
    }

    /**
     * Updates the list of time ranges represented by the time slider. This
     * enforces the maximum and minimum values on the list of time ranges.
     * 
     * @see #actualTimeRanges
     */
    private void calculateActualTimeRanges() {
        actualTimeRanges.clear();

        if (timeRanges.size() == 0) {
            actualTimeRanges.add(new TimeRange(minimum, maximum));
        } else {
            for (TimeRange t : timeRanges) {
                if (t.start > maximum) {
                    break;
                }

                if (t.end < minimum) {
                    continue;
                }

                double rangeStart;
                if (t.start >= minimum) {
                    rangeStart = t.start;
                } else {
                    rangeStart = minimum;
                }

                double rangeEnd;
                if (t.end <= maximum) {
                    rangeEnd = t.end;
                } else {
                    rangeEnd = maximum;
                }

                actualTimeRanges.add(new TimeRange(rangeStart, rangeEnd));
            }
        }
    }

    /**
     * Get the minimum value allowed on the time slider. This takes into account
     * the time ranges.
     * 
     * @return the minimum value allowed on the time slider
     */
    public double getActualMinimum() {
        if (actualTimeRanges.size() == 0) {
            return minimum;
        } else {
            return actualTimeRanges.get(0).start;
        }
    }

    /**
     * Get the maximum value allowed on the time slider. This takes into account
     * the time ranges.
     * 
     * @return the maximum value allowed on the time slider
     */
    public double getActualMaximum() {
        if (actualTimeRanges.size() == 0) {
            return maximum;
        } else {
            return actualTimeRanges.get(actualTimeRanges.size() - 1).end;
        }
    }

    /**
     * Get the total length of time represented by the time slider. This removes
     * time contained in the gaps from time ranges.
     * 
     * @return the total length of time represented by the time slider
     */
    private double getTimeLength() {
        double length = 0;

        for (TimeRange t : actualTimeRanges) {
            length += t.length();
        }

        return length;
    }

    /**
     * Get the width (in pixels) of the time portion of the slider.
     * 
     * @return the width of the time portion of the slider
     */
    private int getTimeWidth() {
        Insets insets = getInsets();
        return getWidth() - 13 - insets.left - insets.right;
    }

    /**
     * Return the time corresponding to the x coordinate of the time slider.
     * 
     * @param x
     *            the horizontal point on the slider component
     * @return the time
     */
    private double getTimeFromX(int x) {
        // remove left inset and button width
        x = x - 6 - getInsets().left;

        int width = getTimeWidth();

        if (x < 0) {
            return getActualMinimum();
        } else if (x > width) {
            return getActualMaximum();
        }

        double factor = ((double) (x)) / width;
        double length = getTimeLength();
        double value = factor * length;

        double position = 0;
        for (TimeRange t : actualTimeRanges) {
            double startPosition = position;
            position += t.length();

            if (value <= position) {
                double startFactor = startPosition / length;
                double endFactor = position / length;
                factor = (factor - startFactor) / (endFactor - startFactor);
                return (t.length()) * factor + t.start;
            }
        }

        return 0;
    }

    /**
     * Get the horizontal point on the slider component corresponding to the
     * given time. If the time is not visible on the slider, -1 will be
     * returned.
     * 
     * @param time
     *            the time to get the point from
     * @return the horizontal (x) point of the time, or -1 if the point is not
     *         visible
     */
    private int getXFromTime(double time) {
        boolean timeVisible = false;
        double position = 0;
        for (TimeRange t : actualTimeRanges) {
            if (t.contains(time)) {
                timeVisible = true;
                position += (time - t.start);
                break;
            }
            position += t.length();
        }
        if (!timeVisible) {
            return -1;
        }

        double factor = position / getTimeLength();
        int width = getTimeWidth();

        return (int) Math.round(factor * width) + getInsets().left + 6;
    }

    /**
     * Gets the amount of time one pixel on the time slider represends.
     * 
     * @return the amount of time represented by one pixel, in seconds
     */
    @SuppressWarnings("unused")
    private double getPixelTime() {
        return getTimeLength() / getTimeWidth();
    }

    /**
     * Layout the buttons.
     */
    public void doLayout() {
        Insets insets = getInsets();

        if (maximum == minimum) {
            startButton.setVisible(false);
            endButton.setVisible(false);
        } else {
            startButton.setVisible(true);
            endButton.setVisible(true);
        }

        startButton.setBounds(getXFromTime(start) - 6, insets.top, 6, 11);
        endButton.setBounds(getXFromTime(end) + 1, insets.top, 6, 11);
        valueButton.setBounds(getXFromTime(value) - 3, insets.top + 2, 7, 7);			
        repaint();
    }

    /**
     * Paint the components. Also paint the slider and the markers.
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Insets insets = getInsets();

        g.setColor(Color.lightGray);
        g.fillRect(insets.left + 6, insets.top + 4, getWidth() - insets.left
            - 12 - insets.right, 3);

        if (isEnabled()) {
            g.setColor(Color.gray);
            int startX = getXFromTime(start);
            int endX = getXFromTime(end);
            g.fillRect(startX, insets.top + 4, endX - startX, 3);
        }
    }

    /**
     * Get the minimum dimensions for this component.
     * 
     * @return the minimum dimensions
     */
    public Dimension getMinimumSize() {
        Insets insets = getInsets();
        return new Dimension(insets.left + 6 + 6 + 1 + insets.right, insets.top
            + 11 + insets.bottom);
    }

    /**
     * Get the preferred size for this component.
     * 
     * @return the preferred dimensions
     */
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }

    /**
     * Creates the tooltip for the component. This changes the default tooltip
     * by setting a different border.
     * 
     * @return the tooltip created
     */
    public JToolTip createToolTip() {
        JToolTip toolTip = super.createToolTip();
        toolTip.setBackground(Color.decode("#FFFFFC"));
        toolTip.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createEtchedBorder(),
            BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        return toolTip;
    }

    /**
     * Gets the text for the tooltip. This will return a description of the
     * markers the mouse pointer is around.
     * 
     * @param me
     *            the mouse event that triggered this
     * @return text describing the markers around the mouse pointer
     *
	public String getToolTipText(MouseEvent me) {
		int x = me.getX();
		if (me.getSource() == valueButton) {
			x += valueButton.getX();
		}

		double time = getTimeFromX(x);
		double timeOffset = time - value;

		double markerOffset = 2 * getPixelTime();
		// List<EventMarker> markersOver = getMarkersAroundTime(time,
		// markerOffset);

		String text = new String("<html><font size=\"5\">");
		return text;
	}

	/**
     * Returns a DateTime instance to represents the start date
     * 
     */
    public Instant getStartInstant() {
        return toInstant((long) getStart());        
    }

    /**
     * Returns a DateTime instance to represents the end date
     * 
     */
    public Instant getEndInstant() {
        return toInstant((long) getEnd());
    }

    /**
     * Get the current DateTime if the value is changeable or null if the value
     * isn't changeable.
     * 
     * @return
     */
    public Instant getCurrentInstant() {
        if (valueChangeable)
            return toInstant((long) getValue());
        else
            return null;
    }

    /**
     * Called when the mouse is dragged. This deals dragging the time value and
     * range controls.
     * 
     * @param me
     *            the mosue event that triggered this
     */
    public void mouseDragged(MouseEvent me) {
        if (!isEnabled()) {
            return;
        }

        JButton button = (JButton) me.getSource();
        int x = me.getX();
        if (button == startButton) {
            x += endButton.getWidth() - clickStart;
        } else if (button == valueButton) {
            x += Math.round(valueButton.getWidth() / 2d) - clickStart;
        } else if (button == endButton) {
            x -= clickStart;
        }
        double time = getTimeFromX(button.getX() + x);

        if (button == startButton) {
            if(left)
            {
                if(!valueChangeable)
                {
                    if (time < minimum) {
                        time = minimum;
                    } else if (time >= end) {
                        time = end;
                    }

                    setStart(time);
                }
            }
        } else if (button == valueButton) {
            if (rangeChangeable) {
                if (time < start) {
                    time = start;
                } else if (time > end) {
                    time = end;
                }
            } else {
                if (time < minimum) {
                    time = minimum;
                } else if (time > maximum) {
                    time = maximum;
                }
            }

            setValue(time);
        } else if (button == endButton) {
            if(right)
            {
                if(!valueChangeable)
                {
                    if (time < start) {
                        time = start;
                    } else if (time > maximum) {
                        time = maximum;
                    }

                    setEnd(time);
                }
            }
        }
    }

    /**
     * Called when the mouse is pressed. Used to tell when the value control is
     * being adjusted.
     * 
     * @param me
     *            the mouse event that triggered this
     */
    public void mousePressed(MouseEvent me) {
        clickStart = me.getX();

        if (me.getSource() == valueButton) {
            isAdjusting = true;
        }
    }

    /**
     * Called whn the mouse is released. Used to tell when the value control is
     * being adjusted.
     * 
     * @param me
     *            the mouse event that triggered this
     */
    public void mouseReleased(MouseEvent me) {
        if (me.getSource() == valueButton) {
            isAdjusting = false;
            TimeEvent event = new TimeEvent(this);
            for (TimeAdjustmentListener l : adjustmentListeners) {
                l.timeChanged(event);
            }
        }
    }

    public void mouseMoved(MouseEvent me) {
    }

    /**
     * Called when the mouse is clicked. Used to set the time value when there
     * is a click on the time slider.
     * 
     * @param me
     *            the mouse event that triggered this
     */
    public void mouseClicked(MouseEvent me) {
        double time;
        if (me.getSource() == this) {
            time = getTimeFromX(me.getX());
        } else if (me.getSource() == valueButton) {
            time = getTimeFromX(valueButton.getX() + me.getX());
        } else {
            return;
        }
        if (me.getButton() == MouseEvent.BUTTON1) {
            setValue(time);
        }
    }

    public void mouseEntered(MouseEvent me) {
    }

    public void mouseExited(MouseEvent me) {
    }

    /**
     * Add a listener for time adjustments.
     * 
     * @param l
     *            the listener to add
     */
    public void addTimeAdjustmentListener(TimeAdjustmentListener l) {
        adjustmentListeners.add(l);
    }

    /**
     * Remove a listener for time adjustments.
     * 
     * @param l
     *            the listener to remove
     */
    public void removeTimeAdjustmentListener(TimeAdjustmentListener l) {
        adjustmentListeners.remove(l);
    }

    /**
     * Fires a value changed event to all adjustment listeners.
     */
    public void fireValueChanged() {
        TimeEvent event = new TimeEvent(this);
        for (TimeAdjustmentListener l : adjustmentListeners) {
            l.timeChanged(event);
        }
    }

    /**
     * Fires a range changed event to all adjustment listeners.
     */
    public void fireRangeChanged() {
        TimeEvent event = new TimeEvent(this);
        for (TimeAdjustmentListener l : adjustmentListeners) {
            l.rangeChanged(event);
        }
    }

    /**
     * Fires a bounds changed event to all adjustment listeners.
     */
    protected void fireBoundsChanged() {
        TimeEvent event = new TimeEvent(this);
        for (TimeAdjustmentListener l : adjustmentListeners) {
            l.boundsChanged(event);
        }
    }

    /**
     * Sets if this component is enabled. If the component is not enabled, the
     * time value and range controls will not respond to user input.
     * 
     * @param enabled
     *            true if this component should be enabled, false otherwise
     */
    public void setEnabled(boolean enabled) {
        if (isEnabled() == enabled) {
            return;
        }
        super.setEnabled(enabled);
        valueButton.setEnabled(enabled && valueChangeable);
        startButton.setEnabled(enabled && rangeChangeable);
        endButton.setEnabled(enabled && rangeChangeable);
        repaint();
    }


    private long toMillis(Instant instant){
        if (instant.isAbsolute()){
            AbsoluteInstant absoluteInstant = (AbsoluteInstant)instant;
            long millis = 0;                  
            millis += absoluteInstant.getYears() * MILLIS_BY_YEAR;
            millis += absoluteInstant.getMonths() * MILLIS_BY_MONTH;
            millis += absoluteInstant.getDays() * MILLIS_BY_YEAR;
            millis += absoluteInstant.getHours() * MILLIS_BY_HOUR;
            millis += absoluteInstant.getMinutes() * MILLIS_BY_MINUTE;
            millis += absoluteInstant.getSeconds() * MILLIS_BY_SECOND;       
            millis += absoluteInstant.getMillis();
            return millis;
        }else{
            return ((RelativeInstant)instant).toMillis();
        }
    } 

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Instant toInstant(long millis){
        if (isAbsolute){  

            ArrayList types = new ArrayList();;
            ArrayList values = new ArrayList();

            long mod = millis % MILLIS_BY_YEAR;
            if ((millis / MILLIS_BY_YEAR) > 0){
                types.add(AbsoluteInstantType.YEAR);
                values.add((int)(millis / MILLIS_BY_YEAR));
                millis = mod;
            }

            mod = millis % MILLIS_BY_MONTH;            
            if ((millis / MILLIS_BY_MONTH) > 0){
                if((int)(millis / MILLIS_BY_MONTH) > 0)
                {
                    types.add(AbsoluteInstantType.MONTH_OF_YEAR);
                    values.add((int)(millis / MILLIS_BY_MONTH));
                }
                millis = mod;
            }    

            mod = millis % MILLIS_BY_WEEK;            
            if ((millis / MILLIS_BY_WEEK) > 0){
                if((int)(millis / MILLIS_BY_WEEK) > 0)
                {
                    types.add(AbsoluteInstantType.WEEK_OF_WEEKYEAR);
                    values.add((int)(millis / MILLIS_BY_WEEK));
                }
                millis = mod;
            }

            mod = millis % MILLIS_BY_DAY; 
            if ((millis / MILLIS_BY_DAY) > 0){
                if((int)(millis / MILLIS_BY_DAY) > 0)
                {
                    types.add(AbsoluteInstantType.DAY_OF_MONTH);
                    values.add((int)(millis / MILLIS_BY_DAY));
                }
                millis = mod;
            }           

            mod = millis % MILLIS_BY_HOUR;
            if ((millis / MILLIS_BY_HOUR) > 0){
                types.add(AbsoluteInstantType.HOUR_OF_DAY);
                values.add((int)(millis / MILLIS_BY_HOUR));
                millis = mod;
            }            

            mod = millis % MILLIS_BY_MINUTE;
            if ((millis / MILLIS_BY_MINUTE) > 0){
                types.add(AbsoluteInstantType.MINUTE_OF_HOUR);
                values.add((int)(millis / MILLIS_BY_MINUTE));
                millis = mod;
            }

            mod = millis % MILLIS_BY_SECOND;
            if ((millis / MILLIS_BY_SECOND) > 0){
                types.add(AbsoluteInstantType.SECOND_OF_MINUTE);
                values.add((int)(millis / MILLIS_BY_SECOND));
                millis = mod;
            }            

            mod = (int)millis;
            if (mod > 0){
                types.add(AbsoluteInstantType.MILLIS_OF_SECOND);
                values.add((int)millis);
                millis = mod;
            }

            try {
                int[] intTypes = new int[types.size()];
                int[] intValues = new int[types.size()];
                for (int i=0 ; i<types.size() ; i++){
                    intTypes[i] = (Integer)types.get(i);
                    intValues[i] = (Integer)values.get(i);
                }
                return timeSupportManager.createAbsoluteInstant(intTypes, intValues);
            } catch (AbsoluteInstantTypeNotRegisteredException e) {
                LOG.error("Error creating an absolute instants");
                return null;
            }
        }else{
            return timeSupportManager.createRelativeInstant((long) millis); 
        }
    }
}
