/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.components;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;

import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantType;
import org.gvsig.timesupport.AbsoluteInstantTypeNotRegisteredException;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.swing.impl.rdv.TimeAdjustmentListener;
import org.gvsig.timesupport.swing.impl.rdv.TimeEvent;
/**
 * Panel to manipulate AbsoluteInstants / AbsoluteIntervals
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public class AbsoluteTemporalComponent extends javax.swing.JPanel implements ITemporalComponent, DateTextListener {
    private static final long serialVersionUID = 4486793564004694709L;
    private static TimeSupportManager timeSupportManager = TimeSupportLocator
	.getManager();
	private TimeAdjustmentListener listener;
	private AbsoluteInstant initTime;
	private JTemporalDateText jDateText;
	private JLabel lblLabel = null;
	
	public AbsoluteTemporalComponent()
	{
		super();
		initComponents();
	}
	
	public TimeAdjustmentListener getListener() {
		// TODO Auto-generated method stub
		return listener;
	}
	
	public void setListener(TimeAdjustmentListener listener) {
		// TODO Auto-generated method stub
		this.listener = listener;
	}

	public void setTimes(String label, Time init, Time lower, Time upper) {
		// TODO Auto-generated method stub
		initTime = (AbsoluteInstant) init;
		getTemporalDateText().setInstant(initTime);
		getLabel().setText(label + " " + initTime.toString());
	}

	public void setCurrentInstant(Time date) {
		// TODO Auto-generated method stub
		getTemporalDateText().setInstant((AbsoluteInstant)date);
	}

	public void setStartTime(Time date) {
		// TODO Auto-generated method stub
		
	}

	public void setEndTime(Time date) {
		// TODO Auto-generated method stub
		
	}
	
	public Time getTime() {
		// TODO Auto-generated method stub
//		int [] types = new int[8];
//		int [] values = new int[8];
//		
//		types[0] = AbsoluteInstantType.YEAR;
//		values[0] = getTemporalDateText().getYears();
//		types[1] = AbsoluteInstantType.MONTH_OF_YEAR;
//		values[1] = getTemporalDateText().getMonthOfYear();
//		types[2] = AbsoluteInstantType.WEEK_OF_WEEKYEAR;
//		values[2] = getTemporalDateText().getWeekOfWeekYear();
//		types[3] = AbsoluteInstantType.DAY_OF_MONTH;
//		values[3] = getTemporalDateText().getDayOfMonth();
//		types[4] = AbsoluteInstantType.HOUR_OF_DAY;
//		values[4] = getTemporalDateText().getHourOfDay();
//		types[5] = AbsoluteInstantType.MINUTE_OF_HOUR;
//		values[5] = getTemporalDateText().getMinuteOfHour();
//		types[6] = AbsoluteInstantType.SECOND_OF_MINUTE;
//		values[6] = getTemporalDateText().getSecondOfMinute();
//		types[7] = AbsoluteInstantType.MILLIS_OF_SECOND;
//		values[7] = getTemporalDateText().getMillisOfSecond();
		
		ArrayList<Integer> types = new ArrayList<Integer>();
		ArrayList<Integer> values = new ArrayList<Integer>();
		
		try
		{
		values.add(getTemporalDateText().getYears());
		types.add(AbsoluteInstantType.YEAR);
		}catch(NumberFormatException e){
		}		
				
		try
		{
		values.add(getTemporalDateText().getWeekOfWeekYear());
		types.add(AbsoluteInstantType.MONTH_OF_YEAR);
		}catch(NumberFormatException e){
		}	
		try
		{
		values.add(getTemporalDateText().getMonthOfYear());
		types.add(AbsoluteInstantType.WEEK_OF_WEEKYEAR);
		}catch(NumberFormatException e){
		}	
		try
		{
		values.add(getTemporalDateText().getDayOfMonth());
		types.add(AbsoluteInstantType.DAY_OF_MONTH);
		}catch(NumberFormatException e){
		}		
		try
		{
		values.add(getTemporalDateText().getHourOfDay());
		types.add(AbsoluteInstantType.HOUR_OF_DAY);
		}catch(NumberFormatException e){
		}		
		try
		{
		values.add(getTemporalDateText().getMinuteOfHour());
		types.add(AbsoluteInstantType.MINUTE_OF_HOUR);
		}catch(NumberFormatException e){
		}		
		try
		{
		values.add(getTemporalDateText().getSecondOfMinute());
		types.add(AbsoluteInstantType.SECOND_OF_MINUTE);
		}catch(NumberFormatException e){
		}
		try
		{
		values.add(getTemporalDateText().getMillisOfSecond());
		types.add(AbsoluteInstantType.MILLIS_OF_SECOND);
		}catch(NumberFormatException e){
		}		
		int [] vTypes = new int[types.size()];
		int [] vValues = new int[types.size()];
		for(int i = 0; i < vTypes.length; i++)
		{
			vTypes[i] = (Integer) types.get(i);
			vValues[i] = (Integer) values.get(i);
		}
		
		try {
			return timeSupportManager.createAbsoluteInstant(vTypes,vValues);
		} catch (AbsoluteInstantTypeNotRegisteredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}

	public void resetLowertTime() {
		// TODO Auto-generated method stub
		
	}

	public void resetUpperTime() {
		// TODO Auto-generated method stub
		
	}

	public void resetValues() {
		// TODO Auto-generated method stub
		getTemporalDateText().setInstant(initTime);
	}
	
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		if(listener != null)
		{
			listener.timeChanged(new TimeEvent(this));
		}
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(listener != null)
		{
			listener.timeChanged(new TimeEvent(this));
		}
		
	}
	/*PRIVATE METHODS*/
	
	private void initComponents()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(getLabel());
		getTemporalDateText().setListener(this);
		add(getTemporalDateText());
	}
	
	private JTemporalDateText getTemporalDateText()
	{
		if(jDateText == null)
			jDateText = new JTemporalDateText();
		return jDateText;
	}

	private JLabel getLabel()
	{
		if(lblLabel == null)
			lblLabel = new JLabel();
		return lblLabel;
	}


}
