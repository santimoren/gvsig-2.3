/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.components;

import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.swing.impl.rdv.TimeAdjustmentListener;

/**
 * Interface to control all type of panels (relative or absolute)
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public interface ITemporalComponent{

	public TimeAdjustmentListener getListener();
	public void setListener(TimeAdjustmentListener listener);
	/**
	 * Set the name's label of control and all times 
	 * @param label
	 * @param init
	 * @param lower
	 * @param upper
	 */
	public void setTimes(String label, Time init, Time lower, Time upper);
	/**
	 * Set current instant to all controls
	 * @param date
	 */
	public void setCurrentInstant(Time date);
	/**
	 * Set start Time to all controls
	 * @param date
	 */
	public void setStartTime(Time date);
	/**
	 * Set end Time to all controls
	 * @param date
	 */
	public void setEndTime(Time date);
	/**
	 * Get current time
	 * @return
	 */
	public Time getTime();
	public void setEnabled(boolean enabled);
	/**
	 * Reset lower (start) time with first value
	 */
	public void resetLowertTime();
	/**
     * Reset upper (end) time with first value
     */
	public void resetUpperTime();
	/**
	 * Reset all value with first values
	 */
	public void resetValues();
	/**
	 * Return if the control is enabled
	 * @return
	 */
	public boolean isEnabled();
}
