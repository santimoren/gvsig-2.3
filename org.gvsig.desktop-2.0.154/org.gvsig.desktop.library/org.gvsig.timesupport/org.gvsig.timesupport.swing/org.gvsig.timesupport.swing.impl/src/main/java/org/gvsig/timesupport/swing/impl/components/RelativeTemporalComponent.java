/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
package org.gvsig.timesupport.swing.impl.components;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.toedter.calendar.JDateChooser;
import com.toedter.components.JSpinField;

import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.swing.impl.rdv.TimeAdjustmentListener;
import org.gvsig.timesupport.swing.impl.rdv.TimeEvent;
/**
 * Control to manipulate RelativeInstants / RelativeIntervals
 * @author <a href="mailto:Pablo.Viciano@uji.es">Pablo Viciano Negre</a>
 * @version $Id$
 *
 */
public class RelativeTemporalComponent extends javax.swing.JPanel implements ITemporalComponent,
		PropertyChangeListener {
	private static final long serialVersionUID = -4793729838825841885L;

	private static TimeSupportManager timeSupportManager = TimeSupportLocator
			.getManager();

	private JDateChooser jdDateChooser = null;
	public static int width = 300;
	public static int height = 40;
	private JSpinField jsfHours = null;
	private JSpinField jsfMinutes = null;
	private JSpinField jsfSeconds = null;
	private JSpinField jsfMillis = null;
	private RelativeInstant lowerTime = null;
	private RelativeInstant upperTime = null;
	private RelativeInstant initTime = null;
	private TimeAdjustmentListener listener = null;
	private JLabel lblLabel = null;
	private String lastLabel = null;

	public RelativeTemporalComponent() {
		super();
		initComponents();
	}

	public TimeAdjustmentListener getListener() {
		return listener;
	}

	public void setListener(TimeAdjustmentListener listener) {
		this.listener = listener;
	}

	/**
	 * Initializes the control with the parameter values
	 * 
	 * @param init
	 *            Date initial for the control
	 * @param lower
	 *            Lower limit
	 * @param upper
	 *            Upper Limit
	 */
	public void setTimes(String label, Time init, Time lower,
	    Time upper) {
		lastLabel = label;
		lowerTime = (RelativeInstant) lower;
		upperTime = (RelativeInstant) upper;
		initTime = (RelativeInstant) init;
		setMinInstantToCalendar(lowerTime);
		setMaxInstantToCalendar(upperTime);
		Date date = new Date(initTime.getYear()-1900, initTime.getMonthOfYear()-1, initTime.getDayOfMonth());
		setInstantToCalendar(initTime);
		setHours(initTime.getHourOfDay());
		setMinutes(initTime.getMinuteOfHour());
		setSeconds(initTime.getSecondOfMinute());
		setMilis(initTime.getMillisOfSecond());
		lblLabel.setText(label + " " + initTime.toString());
	}

	private void setInstantToCalendar(RelativeInstant time)
	{
	    getJdDateChooser().setDate(new Date(time.getYear()-1900, time.getMonthOfYear()-1, time.getDayOfMonth()));
	}
	
	private void setMaxInstantToCalendar(RelativeInstant time)
	{
	    getJdDateChooser().setMaxSelectableDate(new Date(time.getYear()-1900, time.getMonthOfYear()-1, time.getDayOfMonth()));
	}
	
	private void setMinInstantToCalendar(RelativeInstant time)
	{
	    getJdDateChooser().setMinSelectableDate(new Date(time.getYear()-1900, time.getMonthOfYear()-1, time.getDayOfMonth()));
	}
	
	/**
	 * Set the hours, minutes and seconds
	 */
	public void setTime(int hours, int minutes, int seconds, int milis) {
		setHours(hours);
		setMinutes(minutes);
		setSeconds(seconds);
		setMilis(milis);
	}

	/**
	 * Set hours
	 * 
	 * @params hours
	 */
	public void setHours(int hours) {
		// if(hours >= 0 && hours < 24)
		if (hours >= getJsfHours().getMinimum()
				&& hours <= getJsfHours().getMaximum())
			getJsfHours().setValue(hours);
	}

	/**
	 * Set minutes
	 * 
	 * @param minutes
	 */
	public void setMinutes(int minutes) {
		// if(minutes >= 0 && minutes < 59)
		if (minutes >= getJsfMinutes().getMinimum()
				&& minutes <= getJsfMinutes().getMaximum())
			getJsfMinutes().setValue(minutes);
	}

	/**
	 * Set seconds
	 * 
	 * @param seconds
	 */
	public void setSeconds(int seconds) {
		// if(seconds >= 0 && seconds < 59)
		if (seconds >= getJsfSeconds().getMinimum()
				&& seconds <= getJsfSeconds().getMaximum())
			getJsfSeconds().setValue(seconds);
	}

	/**
	 * Set milis
	 * 
	 * @param milis
	 */
	public void setMilis(int milis) {
		if (milis >= getJsfMillis().getMinimum()
				&& milis <= getJsfMillis().getMaximum())
			getJsfMillis().setValue(milis);
	}

	/**
	 * Get hours
	 * 
	 * @return
	 */
	public int getHours() {
		return getJsfHours().getValue();
	}

	/**
	 * Get minutes
	 * 
	 * @return
	 */
	public int getMinutes() {
		return getJsfMinutes().getValue();
	}

	/**
	 * Get seconds
	 * 
	 * @return
	 */
	public int getSeconds() {
		return getJsfSeconds().getValue();
	}

	/**
	 * Get milis
	 * 
	 * @return
	 */
	public int getMilis() {
		return getJsfMillis().getValue();
	}

	public void setCurrentInstant(Time time) {
		RelativeInstant date = (RelativeInstant) time;
		setInstantToCalendar(date);
		setTime(date.getHourOfDay(), date.getMinuteOfHour(),
				date.getSecondOfMinute(), date.getMillisOfSecond());
	}

	public void setStartTime(Time date) {
		setMinInstantToCalendar((RelativeInstant)date);

	}

	public void setEndTime(Time date) {
	    setMaxInstantToCalendar((RelativeInstant)date);
	}

	/**
	 * Returns the date and time selected by the user
	 * 
	 * @return
	 */
	public Time getTime() {
		// Set time
		Date temp = getJdDateChooser().getDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(temp);
		RelativeInstant date = timeSupportManager.createRelativeInstant(
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH), getJsfHours().getValue(),
				getJsfMinutes().getValue(), getJsfSeconds().getValue(),
				getJsfMillis().getValue());

		return date;
	}

	/**
	 * Property change listener implementation for editing controls. Update
	 * slider position according to control values. It uses a flag to avoid
	 * infinite recursive actualization.
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		try { // this try is for the daylight saving (it creates exceptions)
			//updateLimits(); 
	        if (listener != null) {
	        	TimeEvent t = new TimeEvent(this);
	        	listener.timeChanged(t);
	            listener.rangeChanged(t);
	        }
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		getJdDateChooser().setEnabled(enabled);
		getJsfHours().setEnabled(enabled);
		getJsfMinutes().setEnabled(enabled);
		getJsfSeconds().setEnabled(enabled);
		getJsfMillis().setEnabled(enabled);
	}

	public void resetLowertTime() {
		setMinInstantToCalendar(lowerTime);
		setInstantToCalendar((RelativeInstant)getTime());

	}

	public void resetUpperTime() {
	    setMaxInstantToCalendar(upperTime);

	}

	public void resetValues() {
		setTimes(lastLabel, this.initTime, this.lowerTime,
				this.upperTime);
	}

	/*
	 * 
	 * PRIVATE REGION
	 */

	private void initComponents() {

		setLayout(new BorderLayout());
		lblLabel = new JLabel();
		add(lblLabel, BorderLayout.NORTH);

		getJdDateChooser().addPropertyChangeListener(this);

		getJsfHours().setMaximum(23);
		getJsfHours().setMinimum(0);
		getJsfHours().setName("jSpinFieldHour");
		getJsfHours().addPropertyChangeListener(this);

		getJsfMinutes().setMaximum(59);
		getJsfMinutes().setMinimum(0);
		getJsfMinutes().setName("jSpinFieldMinute");
		getJsfMinutes().addPropertyChangeListener(this);

		getJsfSeconds().setMaximum(59);
		getJsfSeconds().setMinimum(0);
		getJsfSeconds().setName("jSpinFieldSecond");
		getJsfSeconds().addPropertyChangeListener(this);

		getJsfMillis().setMaximum(999);
		getJsfMillis().setMinimum(0);
		getJsfMillis().setName("jSpinFieldMillis");
		getJsfMillis().addPropertyChangeListener(this);

		add(getCalendarLayout(), BorderLayout.CENTER);
	}

	private JPanel getCalendarLayout() {
		/*
		 * JPanel panel = new JPanel(); panel.setLayout(new FlowLayout());
		 * panel.add(getJdDateChooser());
		 * 
		 * /*JPanel panelHours = new JPanel(); panelHours.setLayout(new
		 * GridLayout()); panelHours.add(getJsfHours());
		 * panelHours.add(getJsfMinutes()); panel.add(getJsfSeconds());
		 * panelHours.add(getJsfMillis());
		 * 
		 * panel.add(panelHours); panel.add(getJsfHours());
		 * panel.add(getJsfMinutes()); panel.add(getJsfSeconds());
		 * panel.add(getJsfMillis()); return panel;
		 */
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
		panel.add(getJdDateChooser(), gridBagConstraints);

		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		panel.add(getJsfHours(), gridBagConstraints);

		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		panel.add(getJsfMinutes(), gridBagConstraints);

		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		panel.add(getJsfSeconds(), gridBagConstraints);

		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		panel.add(getJsfMillis(), gridBagConstraints);

		return panel;
	}

	private JDateChooser getJdDateChooser() {
		if (jdDateChooser == null)
			jdDateChooser = new JDateChooser();
		return jdDateChooser;
	}

	private JSpinField getJsfHours() {
		if (jsfHours == null) {
			jsfHours = new JSpinField();
			jsfHours.addPropertyChangeListener(this);
		}
		return jsfHours;
	}

	private JSpinField getJsfMinutes() {
		if (jsfMinutes == null)
			jsfMinutes = new JSpinField();
		return jsfMinutes;
	}

	private JSpinField getJsfSeconds() {
		if (jsfSeconds == null)
			jsfSeconds = new JSpinField();
		return jsfSeconds;
	}

	private JSpinField getJsfMillis() {
		if (jsfMillis == null)
			jsfMillis = new JSpinField();
		return jsfMillis;
	}

	/**
	 * Update the limits of the JSpinFields to max and min data values
	 */
	private void updateLimits() {
	    RelativeInstant maxDate = timeSupportManager.createRelativeInstant(getJdDateChooser()
				.getMaxSelectableDate());
	    RelativeInstant minDate = timeSupportManager.createRelativeInstant(getJdDateChooser()
				.getMinSelectableDate());
	    RelativeInstant date = ((RelativeInstant)getTime());
		if (date.compareTo(maxDate) >= 0) {
			getJsfHours().setMaximum(maxDate.getHourOfDay());
			getJsfMinutes().setMaximum(maxDate.getMinuteOfHour());
			getJsfSeconds().setMaximum(maxDate.getSecondOfMinute());
			getJsfMillis().setMaximum(maxDate.getMillisOfSecond());
		} else if (date.compareTo(minDate) <= 0) {
			getJsfHours().setMinimum(minDate.getHourOfDay());
			getJsfMinutes().setMinimum(minDate.getMinuteOfHour());
			getJsfSeconds().setMinimum(minDate.getSecondOfMinute());
			getJsfMillis().setMinimum(minDate.getMillisOfSecond());
		} else if (date.compareTo(lowerTime) <= 0) {
			getJsfHours().setMinimum(lowerTime.getHourOfDay());
			getJsfMinutes().setMinimum(lowerTime.getMinuteOfHour());
			getJsfSeconds().setMinimum(lowerTime.getSecondOfMinute());
			getJsfMillis().setMinimum(lowerTime.getMillisOfSecond());
		} else if (date.compareTo(upperTime) >= 0) {
			getJsfHours().setMaximum(upperTime.getHourOfDay());
			getJsfMinutes().setMaximum(upperTime.getMinuteOfHour());
			getJsfSeconds().setMaximum(upperTime.getSecondOfMinute());
			getJsfMillis().setMaximum(upperTime.getMillisOfSecond());
		} else {
			getJsfHours().setMaximum(23);
			getJsfMinutes().setMaximum(59);
			getJsfSeconds().setMaximum(59);
			getJsfMillis().setMaximum(999);

			getJsfHours().setMinimum(0);
			getJsfMinutes().setMinimum(0);
			getJsfSeconds().setMinimum(0);
			getJsfMillis().setMinimum(0);
		}
	}

}
