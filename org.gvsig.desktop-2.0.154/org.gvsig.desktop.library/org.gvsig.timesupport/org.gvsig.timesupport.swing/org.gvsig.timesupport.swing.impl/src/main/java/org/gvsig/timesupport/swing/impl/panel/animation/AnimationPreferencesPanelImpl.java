/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.swing.impl.panel.animation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel;
import org.gvsig.timesupport.swing.impl.panel.TimePanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * Main panel for animation preferences
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 * 
 */
public class AnimationPreferencesPanelImpl extends JPanel implements ActionListener, AnimationPreferencesPanel, ChangeListener {
	private static final long  serialVersionUID          = 1L;
	private RelativeInstant    startInstant              = null;
	private RelativeInstant    endInstant                = null;
	
	private JComboBox          comboLoopValue            = null;
	private JSlider            sliderSpeed               = null;
	private JTextField         textFieldTimeStep         = null;
	private JTextField         textFieldTimeWindowValue  = null;
	private JTextField         textFieldNLoops           = null;
	private JTextField         textSpeed                 = null;
	private JComboBox          comboTimeStepScale        = null;
	private JComboBox          comboTimeWindowScale      = null;
	private TimePanel          timePanel                 = null;
	
	private JPanel             panelInterval             = null;
	private JPanel             panelWindowTime           = null;
	private JPanel             panelVisualization        = null;
	private JPanel             panelButtons              = null;
	private DecimalFormat      format                    = null;
	
	private JButton            buttonAccept              = null;
	private JButton            buttonCancel              = null;
	private TimeAnimationDataModel
	                           dataModel                 = null;

	
	public AnimationPreferencesPanelImpl(TimeAnimationDataModel dataModel) {
		this.dataModel = dataModel;
		this.startInstant = dataModel.getInitTime();
		this.endInstant = dataModel.getEndTime();
		
		format = new DecimalFormat("#.##");
		init();
		
		setDefaultTimeWindowScale(dataModel.getWindowTimeScale());
  	    setDefaultTimeStepScale(dataModel.getTimeStepScale());
  	    setInitialTimeWindowValue(1);
  	    setInitialTimeStep(1);
  	    setInitialSpeed(1000);
  	    setInitialLoopValue(TimeAnimation.NO_LOOP, 0);
	}
	
	private void init() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0, 2, 4, 2);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		
		add(getJPanelInterval(), gbc);
		
		gbc.gridy = 1;
		gbc.insets = new Insets(2, 2, 4, 2);
		add(getJPanelStep(), gbc);
		
		gbc.gridy = 2;
		add(getJPanelVisualization(), gbc);
		
		gbc.insets = new Insets(2, 2, 0, 2);
		gbc.gridy = 3;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1;
		add(new JPanel(), gbc);
	}
	
	public JPanel getJPanelButtons() {
		if(panelButtons == null) {
			panelButtons = new JPanel();
			FlowLayout fl = new FlowLayout();
			fl.setAlignment(FlowLayout.RIGHT);
			fl.setHgap(2);
			fl.setVgap(0);
			panelButtons.setLayout(fl);
			panelButtons.add(getButtonAccept(), fl);
			panelButtons.add(getButtonCancel(), fl);
		}
		return panelButtons;
	}
	
        private String getTranslation(String key) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            return i18nManager.getTranslation(key);
        }
        
	public JPanel getJPanelInterval() {
		if(panelInterval == null) {
			panelInterval = new JPanel();
			panelInterval.setBorder(BorderFactory.createTitledBorder(getTranslation("interval")));
			panelInterval.setLayout(new BorderLayout());
			panelInterval.add(getTimePanel(), BorderLayout.CENTER);
			
		}
		return panelInterval;
	}
	
	public JPanel getJPanelStep() {
		if(panelWindowTime == null) {
			panelWindowTime = new JPanel();
			panelWindowTime.setBorder(BorderFactory.createTitledBorder(getTranslation("step")));
			panelWindowTime.setLayout(new GridBagLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(2, 2, 4, 2);
			gbc.fill = GridBagConstraints.NONE;
			gbc.anchor = GridBagConstraints.WEST;
			gbc.weightx = 0;
			
			gbc.gridx = 0;
			gbc.gridy = 0;
			panelWindowTime.add(new JLabel(getTranslation("windowtime") + ":"), gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 0;
			panelWindowTime.add(getJTextFieldTimeWindow(), gbc);
			
			gbc.gridx = 2;
			gbc.gridy = 0;
			panelWindowTime.add(getJComboTimeWindowScale(), gbc);
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridx = 3;
			gbc.gridy = 0;
			panelWindowTime.add(new JPanel(), gbc);
			
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			panelWindowTime.add(new JLabel(getTranslation("timestep") + ":"), gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 1;
			panelWindowTime.add(getJTextFieldTimeStep(), gbc);
			
			gbc.gridx = 2;
			gbc.gridy = 1;
			panelWindowTime.add(getJComboTimeStepScale(), gbc);
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridx = 3;
			gbc.gridy = 1;
			panelWindowTime.add(new JPanel(), gbc);
			
		}
		return panelWindowTime;
	}
	
	
	public JPanel getJPanelVisualization() {
		if(panelVisualization == null) {
			panelVisualization = new JPanel();
			panelVisualization.setBorder(BorderFactory.createTitledBorder(getTranslation("visualization")));

			panelVisualization.setLayout(new GridBagLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(2, 2, 4, 2);
			gbc.anchor = GridBagConstraints.WEST;
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			gbc.gridwidth = 1;
			gbc.gridx = 0;
			gbc.gridy = 0;
			panelVisualization.add(new JLabel(getTranslation("speed")), gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 0;
			panelVisualization.add(getJTextFieldSpeed(), gbc);
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			gbc.gridwidth = 3;
			gbc.gridx = 2;
			gbc.gridy = 0;
			panelVisualization.add(getJSliderSpeed(), gbc);
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.weightx = 0;
			gbc.gridwidth = 1;
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			panelVisualization.add(new JLabel(getTranslation("loop")), gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 1;
			panelVisualization.add(getJComboLoopValue(), gbc);
			
			gbc.gridx = 2;
			gbc.gridy = 1;
			panelVisualization.add(getJTextFieldNLoops(), gbc);
			
			gbc.gridx = 3;
			gbc.gridy = 1;
			panelVisualization.add(new JLabel(getTranslation("iterations")), gbc);
			
			gbc.gridx = 4;
			gbc.gridy = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			panelVisualization.add(new JPanel(), gbc);
		}
		return panelVisualization;
	}
	
	public TimePanel getTimePanel() {
		if(timePanel == null) {
			timePanel = new TimePanel();
			timePanel.initialize(startInstant, endInstant);
		}
		return timePanel;
	}
	
	public JComboBox getJComboTimeWindowScale() {
		if(comboTimeWindowScale == null) {
			comboTimeWindowScale = new JComboBox();
			comboTimeWindowScale.addItem(getTranslation("year"));
			comboTimeWindowScale.addItem(getTranslation("month"));
			comboTimeWindowScale.addItem(getTranslation("week"));
			comboTimeWindowScale.addItem(getTranslation("day"));
			comboTimeWindowScale.addItem(getTranslation("hour"));
			comboTimeWindowScale.addItem(getTranslation("minute"));
			comboTimeWindowScale.addItem(getTranslation("second"));
			comboTimeWindowScale.addItem(getTranslation("millisecond"));
			comboTimeWindowScale.setPreferredSize(new Dimension(150, 20));
		}
		return comboTimeWindowScale;
	}
	
	public JComboBox getJComboTimeStepScale() {
		if(comboTimeStepScale == null) {
			comboTimeStepScale = new JComboBox();
			comboTimeStepScale.addItem(getTranslation("year"));
			comboTimeStepScale.addItem(getTranslation("month"));
			comboTimeStepScale.addItem(getTranslation("week"));
			comboTimeStepScale.addItem(getTranslation("day"));
			comboTimeStepScale.addItem(getTranslation("hour"));
			comboTimeStepScale.addItem(getTranslation("minute"));
			comboTimeStepScale.addItem(getTranslation("second"));
			comboTimeStepScale.addItem(getTranslation("millisecond"));
			comboTimeStepScale.setPreferredSize(new Dimension(150, 20));
		}
		return comboTimeStepScale;
	}
	
	public JComboBox getJComboLoopValue() {
		if(comboLoopValue == null) {
			comboLoopValue = new JComboBox();
			comboLoopValue.addItem(getTranslation("noloop"));
			comboLoopValue.addItem(getTranslation("loop"));
			comboLoopValue.addItem(getTranslation("loop_n"));
			comboLoopValue.addActionListener(this);
			comboLoopValue.setPreferredSize(new Dimension(110, 20));
		}
		return comboLoopValue;
	}
	
	public JSlider getJSliderSpeed() {
		if(sliderSpeed == null) {
			sliderSpeed = new JSlider(0, 10000);
			sliderSpeed.setPaintTicks(true);
			sliderSpeed.setMinorTickSpacing(100);
			sliderSpeed.setMajorTickSpacing(1000);
			sliderSpeed.addChangeListener(this);
		}
		return sliderSpeed;
	}
	
	public JTextField getJTextFieldTimeWindow() {
		if(textFieldTimeWindowValue == null) {
			textFieldTimeWindowValue = new JTextField();
			textFieldTimeWindowValue.setPreferredSize(new Dimension(70, 20));
		}
		return textFieldTimeWindowValue;
	}
	
	public JTextField getJTextFieldTimeStep() {
		if(textFieldTimeStep == null) {
			textFieldTimeStep = new JTextField();
			textFieldTimeStep.setPreferredSize(new Dimension(70, 20));
		}
		return textFieldTimeStep;
	}
	
	public JTextField getJTextFieldNLoops() {
		if(textFieldNLoops == null) {
			textFieldNLoops = new JTextField();
			textFieldNLoops.setPreferredSize(new Dimension(70, 20));
		}
		return textFieldNLoops;
	}
	
	public JTextField getJTextFieldSpeed() {
		if(textSpeed == null) {
			textSpeed = new JTextField();
			textSpeed.setPreferredSize(new Dimension(110, 20));
			textSpeed.addActionListener(this);
		}
		return textSpeed;
	}

	/**
	 * Scale default value for the time window. The value of the parameter is
	 * defined as a constant in this class
	 * @param timeWindowScale
	 */
	private void setDefaultTimeWindowScale(int timeWindowScale) {
		getJComboTimeWindowScale().setSelectedIndex(timeWindowScale);
	}

	/**
	 * Scale default value for the time step. The value of the parameter is
	 * defined as a constant in this class
	 * @param timeWindowScale
	 */
	private void setDefaultTimeStepScale(int timeStepScale) {
		getJComboTimeStepScale().setSelectedIndex(timeStepScale);
	}
	
	/**
	 * Sets the time window initial value in the text field
	 * @param timeWindowValue
	 */
	private void setInitialTimeWindowValue(double timeWindowValue) {
		getJTextFieldTimeWindow().setText(format.format(timeWindowValue));
	}

	/**
	 * Sets the time step initial value in the text field
	 * @param timeStep
	 */
	private void setInitialTimeStep(int timeStep) {
		getJTextFieldTimeStep().setText(format.format(timeStep));
	}

	/**
	 * Sets the initial speed in milliseconds
	 * @param speed
	 */
	private void setInitialSpeed(int speed) {
		getJTextFieldSpeed().setText(speed + "");
		getJSliderSpeed().setValue(speed);
	}

	/**
	 * Sets the initial value for the loop. The value of the parameter is
	 * defined as a constant in this class. The second parameter
	 * is taken into account when the first parameter is three (LOOP_N)
	 * @param loopValue
	 * @param nIterations
	 */
	private void setInitialLoopValue(int loopValue, int nIterations) {
		getJComboLoopValue().setSelectedIndex(loopValue);
		getJTextFieldNLoops().setText(nIterations + "");
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == getJComboLoopValue()) {
			if(getJComboLoopValue().getSelectedIndex() == TimeAnimation.LOOP_N)
				getJTextFieldNLoops().setEnabled(true);
			else
				getJTextFieldNLoops().setEnabled(false);
		}
		if(e.getSource() == getJTextFieldSpeed()) {
			try {
				Integer value = new Integer(getJTextFieldSpeed().getText());
				getJSliderSpeed().setValue(value);
			}catch (NumberFormatException exc) {
			}
		}
	}

	public JButton getButtonAccept() {
		if(buttonAccept == null)
			buttonAccept = new JButton(getTranslation("accept"));
		return buttonAccept;
	}

	public JButton getButtonCancel() {
		if(buttonCancel == null)
			buttonCancel = new JButton(getTranslation("cancel"));
		return buttonCancel;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel#addAcceptCancelListener(java.awt.event.ActionListener)
	 */
	public void addAcceptCancelListener(ActionListener listener) {
		getButtonAccept().addActionListener(listener);
		getButtonCancel().addActionListener(listener);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel#getIDSource(java.lang.Object)
	 */
	public int getIDSource(Object obj) {
		if(obj == getButtonAccept())
			return TimeAnimationDataModel.ACCEPT;
		if(obj == getButtonCancel())
			return TimeAnimationDataModel.CANCEL;
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel#getOutput()
	 */
	public TimeAnimationDataModel getOutput() {
		Interval interval = timePanel.getInterval();
		dataModel.setTimeInterval((RelativeInstant)interval.getStart(),
							(RelativeInstant)interval.getEnd());
		try {
			dataModel.setIterations(new Integer(getJTextFieldNLoops().getText()));
		}catch (NumberFormatException e) {
		}
		dataModel.setSpeed(getJSliderSpeed().getValue());
		try {
			dataModel.setTimeStep(new Integer(getJTextFieldTimeStep().getText()));
		}catch (NumberFormatException e) {
		}
		try {
			dataModel.setWindowTime(new Integer(getJTextFieldTimeWindow().getText()));
		}catch (NumberFormatException e) {
		}
		dataModel.setWindowTimeScale(getJComboTimeWindowScale().getSelectedIndex());
		dataModel.setTimeStepScale(getJComboTimeStepScale().getSelectedIndex());
		dataModel.setLoop(getJComboLoopValue().getSelectedIndex());
		return dataModel;
	}

	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == getJSliderSpeed()) {
			getJTextFieldSpeed().setText(getJSliderSpeed().getValue() + "");
		}
	}

    public JComponent asJComponent() {
        return this;
    }

}
