/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.swing.impl;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.impl.panel.animation.AnimationBarPanelImpl;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Test for the panel AnimationPreferencesPanelImpl
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 * 
 */
public class TestAnimationBarPanel implements ActionListener {
		private int                              w        = 600;
		private int                              h        = 200;
		private JFrame                           frame    = new JFrame();
		private AnimationBarPanelImpl            desc     = null;

		public TestAnimationBarPanel() {
			new DefaultLibrariesInitializer().fullInitialize();
			TimeSupportManager timeSupportManager = TimeSupportLocator.getManager();
			RelativeInstant startInstant = timeSupportManager.createRelativeInstant(2000, 1, 1, 12, 00, 00, 00);
			RelativeInstant endInstant = timeSupportManager.createRelativeInstant(2001, 1, 12, 13, 30, 00, 00);
			TimeAnimation animation = timeSupportManager.createTimeAnimation();
			TimeAnimationDataModel model = animation.getDataModel();
			model.setTimeStep(3);
			model.setTimeStepScale(TimeAnimation.HOUR);
			model.setSpeed(5);
			model.setTimeWindow(startInstant, endInstant);
			
			RelativeInstant start = timeSupportManager.createRelativeInstant(1990, 1, 1, 12, 00, 00, 00);
			RelativeInstant end = timeSupportManager.createRelativeInstant(2010, 1, 12, 13, 30, 00, 00);
			model.setTimeInterval(start, end);
			
			desc = new AnimationBarPanelImpl(animation.getDataModel());
			frame.getContentPane().add(desc);
			frame.setSize(w, h);
			frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
			frame.setVisible(true);
		}

		public static void main(String[] args) {
			new TestAnimationBarPanel();
		}

		private boolean doubleSize = false;
		public void actionPerformed(ActionEvent e) {
			if(!doubleSize)
				frame.setSize(w * 2, h);
			else
				frame.setSize(w, h);
			doubleSize = !doubleSize;
		}

//		public void stateChanged(ChangeEvent e) {
//			if(e.getSource() == desc.getTabs()) {
//				if(desc.getTabs().getSelectedIndex() == 2) {
//					frame.setSize(w, 600);
//				} else {
//					frame.setSize(w, h);
//				}
//			}
//		}
	}
