/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.geom.Point2D;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;


public class AreaListenerTest extends AbstractLibraryAutoInitTestCase {
	private IProjection projectionUTM = CRSFactory.getCRS("EPSG:23030");
	private IProjection projectionGeo = CRSFactory.getCRS("EPSG:4230");
		
	@Override
	protected void doSetUp() throws Exception {
		// Nothing to do	
	}

	public void test1() {
		AreaListenerImpl areaListenerUTM=new AreaListenerImpl(newMapControlUTM());
		AreaListenerImpl areaListenerGeo=new AreaListenerImpl(newMapControlGeo());
		Double[] xsUTM=new Double[] {new Double(731292),new Double(731901),new Double(730138)};
		Double[] ysUTM=new Double[] {new Double(4351223),new Double(4350768),new Double(4349232)};
		double areaUTM=areaListenerUTM.returnCoordsArea(xsUTM,ysUTM,new Point2D.Double(730138,4349232));
		Double[] xsGeo=new Double[] {new Double(-0.31888183),new Double(-0.31173131),new Double(-0.33268401)};
		Double[] ysGeo=new Double[] {new Double(39.27871741),new Double(39.27464327),new Double(39.26117368)};
		double areaGeo=areaListenerGeo.returnGeoCArea(xsGeo,ysGeo,new Point2D.Double(-0.33268401,39.26117368));
		assertTrue("Area UTM igual a Geo",areaUTM<(areaGeo+1000)&& areaUTM>(areaGeo-1000));
	}
	private MapControl newMapControlUTM() {
		ViewPort vp = new ViewPort(projectionUTM);
		MapControl mc=new MapControl();
		mc.setMapContext(new MapContext(vp));
		return mc;
	}
	private MapControl newMapControlGeo() {
		ViewPort vp = new ViewPort(projectionGeo);
		MapControl mc=new MapControl();
		mc.setMapContext(new MapContext(vp));
		return mc;
	}
}
