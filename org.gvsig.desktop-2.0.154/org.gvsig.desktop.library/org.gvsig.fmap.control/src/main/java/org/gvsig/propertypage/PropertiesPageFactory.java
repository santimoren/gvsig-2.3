
package org.gvsig.propertypage;

public interface PropertiesPageFactory {

    /**
     * Return the group identifier.
     * The group identifier identify the object that store the properties that
     * this property page handle. Bty excample: 
     * - Project
     * - View
     * - Layer
     * 
     * @return the group identifier
     */
    public String getGroupID();
    
    /**
     * Return true if this propeties page is enabled for the object.
     * 
     * @param obj object that store the properties of this properties page.
     * @return true if the page is enabled
     */
    public boolean isVisible(Object obj);
    
    /**
     * Create a instance of the propeties page for the object.
     * 
     * @param obj object that store the properties of this properties page.
     * @return the properties page
     */
    public PropertiesPage create(Object obj);
}
