/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Listeners;

import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;



/**
 * <p>Interface for all tools that reply for a {@link MeasureEvent MeasureEvent} produced in the 
 *  associated {@link MapControl MapControl} object, as a consequence of a circle area drawn by the mouse.</p>
 * 
 * @see ToolListener
 */
public interface CircleListener extends ToolListener {
	/**
	 * <p>Called when user defines a circular area.</p>
	 *
	 * @param event information about the mouse event, and geometry data related with the operation done
	 *
	 * @throws BehaviorException will be thrown when fails the process of this tool
	 */
	public void circle(MeasureEvent event) throws BehaviorException;
}
