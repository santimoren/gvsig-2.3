/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.fmap.mapcontrol.impl;

import org.gvsig.fmap.mapcontrol.MapControlLibrary;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.swing.dynformfield.CoordinateReferenceSystem.JDynFormFieldCoordinateReferenceSystemFactory;
import org.gvsig.fmap.mapcontrol.swing.dynformfield.Geometry.JDynFormFieldGeometryFactory;
import org.gvsig.propertypage.impl.DefaultPropertiesPageManager;
import org.gvsig.tools.dynform.spi.DynFormSPILibrary;
import org.gvsig.tools.dynform.spi.DynFormSPILocator;
import org.gvsig.tools.dynform.spi.DynFormSPIManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultMapControlLibrary extends AbstractLibrary{
	
    @Override
    public void doRegistration() {
        registerAsImplementationOf(MapControlLibrary.class);
        require(DynFormSPILibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
		MapControlLocator.registerMapControlManager(DefaultMapControlManager.class);
                MapControlLocator.registerPropertiesPageManager(DefaultPropertiesPageManager.class);
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		// Validate there is any implementation registered.
		MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
		if (mapControlManager == null) {
			throw new ReferenceNotRegisteredException(
					MapControlLocator.MAPCONTROL_MANAGER_NAME, 
					MapControlLocator.getInstance());
		}
		
		//Register the default implementation for a view, that will be
		//the 2D MapControlDrawer.
		mapControlManager.registerDefaultMapControlDrawer(MapControlDrawer2D.class);
		
		//
		// Register the custom controls for CRS and Geometry en the DynFormManager
		DynFormSPIManager dynFormManager = DynFormSPILocator.getDynFormSPIManager();
		if( dynFormManager != null ) {
			dynFormManager.addServiceFactory(new JDynFormFieldCoordinateReferenceSystemFactory());
			dynFormManager.addServiceFactory(new JDynFormFieldGeometryFactory());
		}

	}
}

