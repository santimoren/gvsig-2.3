/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.fmap.mapcontrol;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;

/**
 * <p>
 * Represents a class that can write objects in a map like a raster image, a
 * {@link Geometry} or other graphical objects. This class doesn't depend of the
 * dimension of the map (2D or 3D).
 * </p>
 * <p>
 * The Map Control has to have an instance of this class that can be accessed
 * using the {@link MapControl#getMapControlDrawer()} method. When a Map Control
 * is created some tools are added to this component using the
 * {@link MapControl#addBehavior(String, org.gvsig.fmap.mapcontrol.tools.Behavior.Behavior)}
 * method. Some of these tools need to draw some objects in the map and they use
 * the MapControlDrawer class to do that.
 * </p>
 *
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface MapControlDrawer extends PrimitivesDrawer {

    /**
     * The <code>ViewPort</code> is used to transform the map
     * coordinates in the screen coordinates.
     *
     * @param viewPort
     *            The <code>ViewPort</code>
     */
    public void setViewPort(ViewPort viewPort);

    /**
     * It draws a <code>Geometry</code> on the map using the color
     * specified using the {@link #setColor(Color)} method.
     *
     * @param geometry
     *            The <code>Geometry</code> to draw.
     */
    public void draw(Geometry geometry);

    /**
     * It draws a <code>Geometry</code> on the map using a concrete
     * symbol.
     *
     * @param geometry
     *            The <code>Geometry</code> to draw.
     * @param symbol
     *            The symbol used to draw the geometry.
     */
    public void draw(Geometry geometry, ISymbol symbol);

    /**
     * It draws a <code>Geometry</code> on the map using a concrete
     * symbol.
     *
     * @param geometry
     *            The <code>Geometry</code> to draw.
     * @param symbol
     *            The symbol used to draw the geometry.
     * @param feature
     *            The feature used to draw the geometry.
     */
    public void draw(Geometry geometry, ISymbol symbol, Feature feature);


    /**
     * It draws the <code>Handler</code>'s that compose a geometry
     * on the map.
     *
     * @param handlers
     *            An array of <code>Handler</code>'s.
     * @param at
     *            A transformation that has to be applied to the
     *            <code>Handler</code>'s.
     * @param symbol
     *            The symbol used to draw the handlers.
     */
    public void drawHandlers(Handler[] handlers, AffineTransform at,
        ISymbol symbol);

    /**
     * It draws a line using a concrete symbol.
     *
     * @param firstPoint
     *            The first point of the line.
     * @param endPoint
     *            The end point of the line.
     * @param symbol
     *            The symbol used to draw the line.
     */
    public void drawLine(Point2D firstPoint, Point2D endPoint, ISymbol symbol);

    /**
     * It draws an image on a map in a concrete position.
     *
     * @param img
     *            The image to draw.
     * @param x
     *            The X coordinate,
     * @param y
     *            The Y coordinate.
     */
    public void drawImage(Image img, int x, int y);

    /**
     * It draws image, applying a transform from image space
     * into user space before drawing.
     *
     * @param img
     *            The image to draw.
     * @param xform
     *            The transform to apply.
     */
    public void drawImage(Image img, AffineTransform xform);

    /**
     * It draws a <code>Handler</code> on the map.
     *
     * @param handler
     *            The <code>Handler</code> to draw.
     * @param at
     *            A transformation that has to be applied to the
     *            <code>Handler</code>.
     */
    public void drawHandler(Handler handler, AffineTransform at);

    /**
     * @param at
     */
    public void transform(AffineTransform at);

    /**
     * @param instance
     */
    public void setComposite(Composite instance);

    /**
     * Replaces the values of all preferences for the rendering algorithms with the specified hints. The existing
     * values for all rendering hints are discarded and the new set of known hints and values are initialized
     * from the specified Map object. Hint categories include controls for rendering quality and overall time/
     * quality trade-off in the rendering process. Refer to the RenderingHints class for definitions of some
     * common keys and values.
     * @param hints  the rendering hints to be set
     */
    public void setRenderingHints(RenderingHints hints);

    /**
     * Sets the Stroke for the Graphics2D context.
     * @param stroke the Stroke object to be used to stroke a Shape during the rendering
     */
    public void setStroke(Stroke stroke);

    /**
     * Cleans the graphics using the image of the MapControl
     * @param mapCtrl
     */
    public void cleanCanvas(MapControl mapCtrl);

}
