/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.mapcontrol;

import org.gvsig.propertypage.PropertiesPageManager;
import org.gvsig.propertypage.impl.DefaultPropertiesPageManager;
import org.gvsig.tools.locator.AbstractLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MapControlLocator extends AbstractLocator {
	private static final String LOCATOR_NAME = "MapContolLocator";
	
	/**
	 * MapControlManager name used by the locator to access the instance
	 */
	public static final String MAPCONTROL_MANAGER_NAME = "MapControlManager";
	private static final String MAPCONTROL_MANAGER_DESCRIPTION = "MapControlManager of gvSIG";

	public static final String EDITING_NOTIFICATION_MANAGER_NAME = "EditingNotificationManager";
	private static final String EDITING_NOTIFICATION_MANAGER_DESCRIPTION = "EditingNotificationManager of gvSIG";

	public static final String PROPERTIES_PAGE_MANAGER_NAME = "PropertiesPageManager";
	private static final String PROPERTIES_PAGE_MANAGER_DESCRIPTION = "PropertiesPage manager of gvSIG";

        /**
	 * Unique instance.
	 */
	private static final MapControlLocator instance = new MapControlLocator();
	
	/* (non-Javadoc)
	 * @see org.gvsig.tools.locator.Locator#getLocatorName()
	 */
	public String getLocatorName() {
		return LOCATOR_NAME;
	}
	
	/**
	 * Return a reference to {@link MapControlManager}.
	 *
	 * @return a reference to MapControlManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static MapControlManager getMapControlManager() throws LocatorException {
		return (MapControlManager) getInstance().get(MAPCONTROL_MANAGER_NAME);
	}
	
	/**
	 * Return the singleton instance.
	 *
	 * @return the singleton instance
	 */
	public static MapControlLocator getInstance() {
		return instance;
	}
	
	/**
	 * Registers the Class implementing the {@link MapControlManager} interface.
	 *
	 * @param clazz
	 *            implementing the MapControlManager interface
	 */
	public static void registerMapControlManager(Class clazz) {
		getInstance().register(MAPCONTROL_MANAGER_NAME, 
				MAPCONTROL_MANAGER_DESCRIPTION,
				clazz);
	}
        
        public static PropertiesPageManager getPropertiesPageManager() {
            return (PropertiesPageManager) getInstance().get(PROPERTIES_PAGE_MANAGER_NAME);
        }

	public static void registerPropertiesPageManager(Class clazz) {
		getInstance().register(PROPERTIES_PAGE_MANAGER_NAME, 
				PROPERTIES_PAGE_MANAGER_DESCRIPTION,
				clazz);
	}
}


