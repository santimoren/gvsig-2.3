/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.fmap.mapcontrol;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface PrimitivesDrawer {
	
	/**
	 * This method is used to start a drawing process that needs to 
	 * be synchronized.
	 * @param obj
	 * The object that locks the resource.
	 * @throws InterruptedException
	 */
	public void startDrawing(Object obj) throws InterruptedException;
	
	/**
	 * This method is used to finish a drawing process that needs to 
	 * be synchronized. 
	 * @param obj
	 * The object that has locked the resource.
	 */
	public void stopDrawing(Object obj);
	
	/**
	 * This method sets the <code>Graphics</code> where the 
	 * <code>Drawer</code> has to draw all the objects. 
	 * @param graphics
	 * The component where the new objects has to be drawn. 
	 */
	public void setGraphics(Graphics graphics);
	
	/**
	 * Sets the color that is used to draw the objects that don't 
	 * have a symbol.
	 * @param color
	 * The color to use on the drawing operations.
	 */
	public void setColor(Color color);
		
	/**
	 * It draws a rectangle on the map using the color 
	 * specified using the {@link #setColor(Color)} method. 
	 * @param x
	 * The minimum X coordinate.
	 * @param y
	 * The minimum Y coordinate.
	 * @param width
	 * The rectangle width.
	 * @param height
	 * The rectangle height.
	 */
	public void drawRect(int x, int y, int width, int height);
		
	/**
	 * It fills a rectangle on the map using the color 
	 * specified using the {@link #setColor(Color)} method. 
	 * @param x
	 * The minimum X coordinate.
	 * @param y
	 * The minimum Y coordinate.
	 * @param width
	 * The rectangle width.
	 * @param height
	 * The rectangle height.
	 */
	public void fillRect(int x, int y, int width, int height);
	
	public void drawOval(int i, int j, int sizePixels, int sizePixels2);

	public void drawLine(int x1, int y1, int x3, int y12);
}
