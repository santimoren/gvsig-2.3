/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.util.List;

import javax.swing.AbstractListModel;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;

/**
 * Model to be used by components showing a list of {@link FeatureType}s.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class FeatureTypeListModel extends AbstractListModel implements Observer {

    private static final long serialVersionUID = -4274815135770376868L;
    private final FeatureStore featureStore;
    private final FeatureQuery featureQuery;

    /**
     * Constructs a {@link FeatureTypeListModel}.
     * 
     * @param featureStore
     *            to get the {@link FeatureType}s from
     * @param featureQuery
     *            to get the FeatureType to filter if any
     */
    public FeatureTypeListModel(FeatureStore featureStore,
        FeatureQuery featureQuery) {
        this.featureStore = featureStore;
        this.featureQuery = featureQuery;
        this.featureStore.addObserver(this);
    }

    /**
     * Returns the number of {@link FeatureType} to show.
     */
    public int getSize() {
        return queryHasFeatureType() ? 1 : getFeatureTypes().size();
    }

    /**
     * Returns if the query has a {@link FeatureType} to filter with.
     * 
     * @return if the query has a {@link FeatureType} to filter with
     */
    private boolean queryHasFeatureType() {
        return featureQuery != null && featureQuery.getFeatureTypeId() != null;
    }

    @SuppressWarnings("unchecked")
    private List<FeatureType> getFeatureTypes() {
        try {
            return featureStore.getFeatureTypes();
        } catch (DataException e) {
            throw new RuntimeException(
                "Error getting the feature types of the FeatureStore: "
                    + featureStore, e);
        }
    }

    public Object getElementAt(int index) {
        return queryHasFeatureType() ? featureQuery.getFeatureTypeId()
            : getFeatureTypes().get(index).getId();
    }

    public void update(Observable observable, Object notification) {
        if (observable.equals(this.featureStore)
            && notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification fsNotification =
                (FeatureStoreNotification) notification;
            String type = fsNotification.getType();

            if (FeatureStoreNotification.AFTER_UPDATE_TYPE.equals(type)) {
                if (queryHasFeatureType()) {
                    fireContentsChanged(this, 0, 0);
                } else {
                    FeatureType featureType = fsNotification.getFeatureType();
                    int index = getFeatureTypes().indexOf(featureType);
                    fireContentsChanged(this, index, index);
                }
            }
        }
    }

    /**
     * 
     * Returns the {@link FeatureStore}
     * 
     * @return the featureStore
     */
    public FeatureStore getFeatureStore() {
        return featureStore;
    }

    /**
     * Returns the {@link FeatureType} with the given identifier.
     * 
     * @param featureTypeId
     *            identifier of the {@link FeatureType}
     * @return a {@link FeatureType}
     */
    public FeatureType getFeatureType(String featureTypeId) {
        try {
            return featureStore.getFeatureType(featureTypeId);
        } catch (DataException e) {
            throw new FeatureTypeListException(e);
        }
    }

}
