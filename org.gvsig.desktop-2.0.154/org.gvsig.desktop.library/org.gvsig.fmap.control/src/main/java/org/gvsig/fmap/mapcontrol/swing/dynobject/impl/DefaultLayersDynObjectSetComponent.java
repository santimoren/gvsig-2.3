/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.swing.dynobject.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dynform.AbortActionException;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.JDynFormField;
import org.gvsig.tools.dynform.JDynFormSet;
import org.gvsig.tools.dynform.JDynFormSet.JDynFormSetListener;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class DefaultLayersDynObjectSetComponent extends JPanel implements
        LayersDynObjectSetComponent, TreeSelectionListener, JDynFormSetListener {

    private static final long serialVersionUID = 5864674721657215264L;

    private static final Logger LOG = LoggerFactory
            .getLogger(DefaultLayersDynObjectSetComponent.class);

    private final LayersDynObjectSetComponentModel model;

    private JDynFormSet component;

    private JTree layersTree;
    private JPanel contentPanel;

    private final boolean writable;

    private DefaultMutableTreeNode topNode;
    private MutableTreeNode previousSelection = null;

    private MapContext mapContext = null;

    /**
     * Creates a new {@link DefaultLayersDynObjectSetComponent} with the given
     * information for a list of layers.
     */
    public DefaultLayersDynObjectSetComponent(
            Map<String, DynObjectSet> layerName2InfoByPoint) {
        this(layerName2InfoByPoint, true);
    }

    /**
     * @param isDoubleBuffered
     */
    public DefaultLayersDynObjectSetComponent(
            Map<String, DynObjectSet> layerName2InfoByPoint,
            boolean writable) {
        super(new BorderLayout());
        this.writable = writable;
        model = new LayersDynObjectSetComponentModel(layerName2InfoByPoint);
        initializeUI();
    }

    private void initializeUI() {
        addLayerTree();
    }

    private void addLayerTree() {
        topNode = new DefaultMutableTreeNode("");
        layersTree = new JTree(topNode);
        layersTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        //layersTree.setShowsRootHandles(true);
        layersTree.addTreeSelectionListener(this);
        createTreeModel();

        JScrollPane scroll = new JScrollPane(layersTree);
        scroll.setPreferredSize(new Dimension(200, 200));

        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                scroll, contentPanel);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(170);

        //Provide minimum sizes for the two components in the split pane
        Dimension minimumSize = new Dimension(100, 150);
        scroll.setMinimumSize(minimumSize);

        add(splitPane, BorderLayout.CENTER);

        for (int i = 0; i < layersTree.getRowCount(); i++) {
            layersTree.expandRow(i);
        }
        layersTree.setRootVisible(false);

        if (topNode.getChildCount() > 0 && topNode.getFirstChild().getChildCount() > 0) {
            layersTree.setSelectionPath(new TreePath(((DefaultTreeModel) layersTree.getModel()).getPathToRoot(topNode.getFirstChild().getChildAt(0))));
        }
    }

    private void createTreeModel() {
        for (int i = 0; i < model.getSize(); i++) {
            MutableTreeNode newLayer = new DefaultMutableTreeNode(
                    model.getElementAt(i),
                    true);

            DynObjectSet dynObjects = model.getLayerInfoByPoint((String) model.getElementAt(i));
            Iterator it;
            try {
                it = dynObjects.iterator();
                int j = 0;
                while (it.hasNext()) {
                    DynObject dynObject = (DynObject) it.next();
                    MutableTreeNode newDynObject = new DefaultMutableTreeNode(
                            concatString(dynObject.toString(), 20),
                            false);
                    newLayer.insert(newDynObject, j);
                    j++;
                }
            } catch (BaseException e) {
                LOG.error("Error getting the DynObjects of "
                        + "the DynObjectSet: " + dynObjects, e);
            }
            topNode.add(newLayer);

        }
    }

    private String concatString(String cadena, int i) {
        if (cadena.length() > i && i > 3) {
            String newCadena = cadena.substring(0, i - 3);
            cadena = newCadena + "...";
        }
        return cadena;
    }

    private void setCurrentLayerInfoByPoint(String layerName) {
        JDynFormSet newComponent = null;

        DynObjectSet dynObjectSet = model.getLayerInfoByPoint(layerName);
        try {
            newComponent
                    = DynFormLocator.getDynFormManager()
                    .createJDynFormSet(dynObjectSet);
            newComponent.setReadOnly(!this.writable);
        } catch (BaseException e) {
            LOG.error("Error creating the JDynFormSet for "
                    + "the DynObjectSet: " + dynObjectSet, e);
        }

        if (newComponent != null) {
            newComponent.setAllowDelete(false);
            newComponent.setAllowNew(false);
            newComponent.setAllowSearch(false);
            newComponent.setAllowUpdate(false);
            if (this.mapContext != null) {
                DataTypesManager dataManager = ToolsLocator.getDataTypesManager();
                newComponent.addActionToPopupMenu(dataManager.get(DataTypes.GEOMETRY), "Center in current view", new CenterGeometryInMapContext(this.mapContext));
            }
            removeCurrentDynObjectSetComponent();
            component = newComponent;
            component.addListener(this);
            JComponent comp = component.asJComponent();
            contentPanel.add(comp, BorderLayout.CENTER);
            revalidate();
            repaint();
        }
    }

    public JComponent asJComponent() {
        return this;
    }

    public void dispose() {
        removeCurrentDynObjectSetComponent();
        model.dispose();
    }

    private void removeCurrentDynObjectSetComponent() {
        if (component != null) {
            remove(component.asJComponent());
            //  component.dispose();
            contentPanel.removeAll();
            contentPanel.repaint();
        }
    }

    public void valueChanged(TreeSelectionEvent e) {
        TreePath newPath = e.getNewLeadSelectionPath();
        TreePath oldPath = e.getOldLeadSelectionPath();

        if (!newPath.equals(oldPath)) {
            DefaultMutableTreeNode aux
                    = (DefaultMutableTreeNode) layersTree.getLastSelectedPathComponent();

            if (layersTree.getSelectionPath() != null) {
                if (aux.isLeaf()) { // A Feature node is selected
                    String layerName = "";
                    if (previousSelection == null
                            || !aux.getParent().equals(previousSelection.getParent())) {
                        layerName = aux.getParent().toString();
                        setCurrentLayerInfoByPoint(layerName);
                    }
                    int childIndex = aux.getParent().getIndex(aux);
                    if (childIndex != -1) {
                        component.setCurrentIndex(childIndex);
                    }
                    previousSelection = aux;
                }
            }
        }
    }

    public void message(String arg0) {
        // Do nothing
    }

    public void formClose() {
        this.setVisible(false);
    }

    public void formMessage(String arg0) {
        // Do nothing
    }

    public void formMovedTo(int arg0) {
        DefaultMutableTreeNode aux
                = (DefaultMutableTreeNode) layersTree.getLastSelectedPathComponent();
        TreeNode parent = aux.getParent();
        TreeNode selectNode = parent.getChildAt(arg0);

        TreePath path = new TreePath(((DefaultTreeModel) layersTree.getModel()).getPathToRoot(selectNode));
        layersTree.setSelectionPath(path);
    }

    public void setMapContext(MapContext mapContext) {
        this.mapContext = mapContext;
    }

    public void formBeforeSave(JDynFormSet jdfs) throws AbortActionException {
    }

    public void formBeforeNew(JDynFormSet jdfs) throws AbortActionException {
    }

    public void formBeforeDelete(JDynFormSet jdfs) throws AbortActionException {
    }

    public void formBeforeSearch(JDynFormSet jdfs) throws AbortActionException {
    }

    public void formAfterSave(JDynFormSet jdfs) {
    }

    public void formAfterNew(JDynFormSet jdfs) {
    }

    public void formAfterDelete(JDynFormSet jdfs) {
    }

    public void formAfterSearch(JDynFormSet jdfs) {
    }

    @Override
    public void formBeforeCancelNew(JDynFormSet dynformSet) throws AbortActionException {
    }

    @Override
    public void formAfterCancelNew(JDynFormSet dynformSet) throws AbortActionException {
    }

    public class CenterGeometryInMapContext extends AbstractAction implements Action {

        private MapContext mapContext;

        public CenterGeometryInMapContext(MapContext mapContext) {
            super("Center in the current view");
            this.mapContext = mapContext;
            this.putValue(Action.SHORT_DESCRIPTION, "Center geometry in the current view");
            this.putValue(Action.LONG_DESCRIPTION, "Center geometry in the current view");
        }

        public void actionPerformed(ActionEvent arg0) {
            if (mapContext == null) {
                return;
            }
            Object source = null;
            try {
                source = arg0.getSource();
                JDynFormField field = (JDynFormField) source;
                Geometry geom = (Geometry) field.getValue();
                if (geom == null) {
                    JOptionPane.showMessageDialog(component.asJComponent(), "The geometri is not valid. Can't center the view in it.", "Warning", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                mapContext.getViewPort().setEnvelope(geom.getEnvelope());
                mapContext.invalidate();
            } catch (Exception ex) {
                LOG.warn("Can't center view in the geometry (source=" + source.toString() + ").", ex);
            }
        }

    }

}
