/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui;

import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.i18n.Messages;
import org.gvsig.utils.swing.JComboBox;


/**
 * <p>Class representing a JComboBox with the measure units handled by the application.
 * It takes values from Attributes.NAMES and Attributes.CHANGE static fields. So, to
 * add more measure units, you must edit Attributes class and change will be automatically
 * reflected in the combo box.</p>
 *
 * <p>The internatiolanization of the field is automatically handled by the system</p>
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class JComboBoxUnits extends JComboBox {
	private static final long serialVersionUID = 8015263853737441433L;

	/**
	 * Creates a new instance of JUnitComboBox including "pixel" units and
	 * setting them as automatically pre-selected.
	 */
	public JComboBoxUnits() {
		this(true);
	}

	/**
	 *
	 * Creates a new instance of JUnitComboBox. If includePixel is true
	 * then pixel units are included in the list and they are automatically
	 * pre-selected. Otherwise, meters are preselected.
	 *
	 */
	public JComboBoxUnits(boolean includePixel) {
		super();
		String[] names=MapContext.getDistanceNames();

		for (int i = 0; i < names.length; i++) {
			super.addItem(Messages.getText(names[i]));
		}
		if (includePixel) {
			super.addItem(Messages.getText("_Pixels"));
			setSelectedItem(Messages.getText("_Pixels"));
		} else {
			setSelectedIndex(1);
		}
		setMaximumRowCount(10);
	}


	/**
	 * Returns the conversion factor from the <b>unit selected in the combo box</b>
	 * to <b>meters</b> or <b>0</b> if pixels have been selected as the size unit.
	 * @return
	 */
	public double getUnitConversionFactor() {
			double unitFactor;
			try {
				unitFactor = MapContext.getDistanceTrans2Meter()[getSelectedIndex()];
			} catch (ArrayIndexOutOfBoundsException aioobEx) { //jijiji
				unitFactor = 0; // which represents size in pixel
			}
			return unitFactor;

	}

	/**
	 * the use of this method is not allowed in this combo box.
	 * @deprecated
	 */
	public void addItem(Object anObject) {
		throw new Error("Operation not allowed");
	}

	/**
	 * the use of this method is not allowed for this combo box.
	 * @deprecated
	 */
	public void removeAllItems() {
		throw new Error("Operation not allowed");
	}

	public int getSelectedUnitIndex() {
		int i = getSelectedIndex();
		if (i>MapContext.getDistanceNames().length-1)
			return -1;
		else return i;
	}

	public void setSelectedUnitIndex(int unitIndex) {
		if (unitIndex == -1) {
			setSelectedIndex(getItemCount()-1);
		} else {
			setSelectedIndex(unitIndex);
		}
	}



}
