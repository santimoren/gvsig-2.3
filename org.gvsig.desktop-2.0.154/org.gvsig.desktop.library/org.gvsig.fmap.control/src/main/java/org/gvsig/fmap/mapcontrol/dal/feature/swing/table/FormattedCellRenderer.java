package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.awt.Component;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import org.apache.commons.lang3.StringUtils;

public class FormattedCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -162038647556726890L;
    private ConfigurableFeatureTableModel tableModel;
    private DecimalFormat decimalFormat;
    private SimpleDateFormat dateFormat;

    public FormattedCellRenderer(ConfigurableFeatureTableModel tableModel) {
        this.tableModel = tableModel;
        this.decimalFormat = (DecimalFormat) NumberFormat.getInstance(this.tableModel.getLocaleOfData());
        this.dateFormat = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.MEDIUM, this.tableModel.getLocaleOfData());
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table,value, isSelected, hasFocus,row, column);
        try {
            if ( value instanceof Number ) {
                String pattern = this.tableModel.getFormattingPattern(column);
                if ( !StringUtils.isBlank(pattern) ) {
                    this.decimalFormat.applyPattern(pattern);
                    String formated = this.decimalFormat.format(value).toString();
                    this.setHorizontalAlignment(SwingConstants.RIGHT);
                    this.setText(formated);
                    return this;
                }
            }
            if ( value instanceof Date ) {
                String pattern = this.tableModel.getFormattingPattern(column);
                if ( !StringUtils.isBlank(pattern) ) {
                    this.dateFormat.applyPattern(pattern);
                    String formated = this.dateFormat.format(value).toString();
                    this.setHorizontalAlignment(SwingConstants.RIGHT);
                    this.setText(formated);
                    return this;
                }
            }
        } catch (Exception ex) {
            // Do nothing, use values from default renderer
        }
        return this;
    }

}
