package org.gvsig.fmap.mapcontrol.swing.dynformfield.CoordinateReferenceSystem;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cresques.Messages;
import org.cresques.cts.IProjection;
import org.gvsig.app.gui.panels.CRSSelectPanelFactory;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dynform.JDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.AbstractJDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.JCustomTextField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.spi.ServiceManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;

public class JDynFormFieldCoordinateReferenceSystem extends AbstractJDynFormField implements JDynFormField, FocusListener {

    protected IProjection assignedValue = null;
    protected IProjection currentValue = null;
    protected JTextField jtext = null;
    protected JButton jbutton = null;

    public JDynFormFieldCoordinateReferenceSystem(DynObject parameters,
            ServiceManager serviceManager) {
        super(parameters, serviceManager);
        this.assignedValue = (IProjection) this.getParameterValue();
    }

    public Object getAssignedValue() {
        return this.assignedValue;
    }

    public void initComponent() {
        this.contents = new JPanel();
        this.contents.setLayout(new BorderLayout());

        this.jtext = new JCustomTextField(getLabel());
        this.jtext.addFocusListener(this);
        this.jtext.setEditable(false);
        this.jbutton = new JButton("...");
        this.jbutton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClickBrowse();
            }
        });

        this.contents.add(jtext, BorderLayout.CENTER);
        this.contents.add(jbutton, BorderLayout.LINE_END);
        this.contents.setVisible(true);
        this.setValue(this.assignedValue);
    }

    public void onClickBrowse() {
        fireFieldEnterEvent();
        this.problemIndicator().restore();

        ISelectCrsPanel csSelect = CRSSelectPanelFactory.getUIFactory().getSelectCrsPanel(this.currentValue, true);
        ToolsSwingLocator.getWindowManager().showWindow((JComponent) csSelect, Messages.getText("selecciona_sistema_de_referencia"), MODE.DIALOG);
        if (csSelect.isOkPressed()) {
            this.currentValue = csSelect.getProjection();
            this.jtext.setText(this.currentValue.getAbrev());
        }
    }

    public boolean hasValidValue() {
        return true;
    }

    public void focusGained(FocusEvent arg0) {
        fireFieldEnterEvent();
        this.problemIndicator().restore();
    }

    public void focusLost(FocusEvent arg0) {
        fireFieldExitEvent();
    }

    public void setValue(Object value) {
        IProjection x = null;
        try {
            x = (IProjection) this.getDefinition().getDataType().coerce(value);
        } catch (CoercionException e) {
            throw new IllegalFieldValue(this, e.getMessage());
        }
        this.currentValue = x;
        if (x != null) {
            this.jtext.setText(x.getAbrev());
        }
        this.assignedValue = x;
    }

    public Object getValue() {
        return this.currentValue;
    }

}
