/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.swing.datastore;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.mapcontrol.swing.dynobject.DynObjectEditor;
import org.gvsig.tools.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class DefaultDataStoreParametersCreationPanel extends DataStoreParametersCreationPanel implements ActionListener{   
    private static final long serialVersionUID = -1453149638996485029L;

    private static final Logger LOG =
        LoggerFactory.getLogger(DefaultDataStoreParametersCreationPanel.class);

    private static final DataManager DATA_MANAGER = DALLocator.getDataManager();

    private static final int FORM_HEIGTH = 18;

    private DataServerExplorer dataServerExplorer = null;
    private DataServerExplorerParameters dataServerExplorerParameters = null;

    private NewDataStoreParameters dataStoreParameters = null;

    private JButton explorersButton;
    private JComboBox explorersComboBox;
    private JButton providersButton;
    private JComboBox providersComboBox;

    public DefaultDataStoreParametersCreationPanel() {
        super(); 
        initComponents();
        populateExplorerCombo();
        initListeners();
        enableExplorerControls(false);
    }     

    private void initListeners() {
        this.explorersComboBox.addActionListener(this);
        this.explorersButton.addActionListener(this);
        this.providersComboBox.addActionListener(this);
        this.providersButton.addActionListener(this);        
    }

    @SuppressWarnings("rawtypes")
    private void populateExplorerCombo(){
        explorersComboBox.addItem(null);

        Iterator it = DATA_MANAGER.getExplorerProviders().iterator();

        while(it.hasNext()) {
            String explorer = (String)it.next();
            explorersComboBox.addItem(explorer);            
        }
    }   

    private void initComponents() {
        GridBagConstraints gridBagConstraints;

        explorersComboBox = new JComboBox();
        providersComboBox = new JComboBox();
        explorersButton = new JButton();
        providersButton = new JButton();

        setLayout(new GridBagLayout());

        explorersComboBox.setPreferredSize(new Dimension(0, FORM_HEIGTH));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 2, 0, 2);
        add(explorersComboBox, gridBagConstraints);

        explorersButton.setText("...");
        explorersButton.setPreferredSize(new Dimension(25, FORM_HEIGTH));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 2, 0, 2);
        add(explorersButton, gridBagConstraints);

        providersComboBox.setPreferredSize(new Dimension(0, FORM_HEIGTH));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 2, 0, 2);
        add(providersComboBox, gridBagConstraints);      

        providersButton.setText("...");
        providersButton.setPreferredSize(new Dimension(25, FORM_HEIGTH));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 2, 0, 2);
        add(providersButton, gridBagConstraints);
    }

    @Override
    public NewDataStoreParameters getDataStoreParameters(){       
        return dataStoreParameters;
    }

    @Override
    public DataServerExplorer getDataServerExplorer() {       
        return dataServerExplorer;
    }

    public void actionPerformed(ActionEvent e) {       
        if (e.getSource().equals(explorersButton)) {           
            explorersButtonClicked();
        }else if (e.getSource().equals(providersButton)) {
            providersButtonClicked();
        }else if (e.getSource().equals(explorersComboBox)) {           
            explorersComboSelectionChanged();
        }else if (e.getSource().equals(providersComboBox)) {
            providersComboSelectionChanged();
        }
    }  

    private void explorersComboSelectionChanged(){        
        String explorerName = (String)explorersComboBox.getSelectedItem();
        if (explorerName == null){
            dataServerExplorerParameters = null;
            dataServerExplorer = null;
            removeProvidersCombo();
            enableExplorerControls(false);
        }else{
            //Only update the explorer if the selection is different
            if ((dataServerExplorer == null) || (!explorerName.equals(dataServerExplorer.getProviderName()))){        
                dataServerExplorerParameters = null;

                //Remove all the previous providers
                removeProvidersCombo(); 

                //Disable all the components and enable the explorer button
                this.enableExplorerControls(false);
                this.explorersButton.setEnabled(true);       
                //Sometimes is possible to create an explorer without parameters (e.g: filesystem explorer"
                try {                    
                    dataServerExplorerParameters =
                        DATA_MANAGER.createServerExplorerParameters(explorerName);
                    dataServerExplorer = 
                        DATA_MANAGER.openServerExplorer(explorerName, dataServerExplorerParameters);
                    populateProvidersCombo();  
                    this.providersComboBox.setEnabled(true);     
                } catch (InitializeException e) {
                    LOG.error("Error creating the explorer parameters", e);                    
                } catch (ProviderNotRegisteredException e) {
                    LOG.error("The explorer has not been registeger", e);                    
                } catch (ValidateDataParametersException e) {
                    LOG.error("Error creating the explorer", e);                  
                }                
            }
        }
    }

    private void enableExplorerControls(boolean isEnabled){        
        this.explorersButton.setEnabled(isEnabled);  
        this.providersComboBox.setEnabled(isEnabled);    
        this.providersButton.setEnabled(isEnabled);
    }

    private void enableProviderControls(boolean isEnabled){       
        this.providersButton.setEnabled(isEnabled);  
    }

    private void explorersButtonClicked(){
        dataServerExplorer = null;
        String explorerName = (String)explorersComboBox.getSelectedItem();
        if (explorerName != null){
            try {
                if ((dataServerExplorerParameters == null) || (!dataServerExplorerParameters.getExplorerName().equals(explorerName))){
                    dataServerExplorerParameters =
                        DATA_MANAGER.createServerExplorerParameters(explorerName);
                }
                DynObjectEditor dynObjectEditor = new DynObjectEditor(dataServerExplorerParameters);
                dynObjectEditor.editObject(true);               
                dataServerExplorer = 
                    DATA_MANAGER.openServerExplorer(explorerName, dataServerExplorerParameters);
                //Remove all the previous providers
                removeProvidersCombo(); 
                populateProvidersCombo();
                this.providersComboBox.setEnabled(true);  
            } catch (InitializeException e) {
                LOG.error("Error creating the explorer parameters", e);
            } catch (ProviderNotRegisteredException e) {
                LOG.error("The explorer has not been registered", e);
            } catch (ServiceException e) {
                LOG.error("Error creating the explorer panel", e);
            } catch (ValidateDataParametersException e) {
                LOG.error("Error creating the explorer", e);
            }    
        }
    }

    @SuppressWarnings("rawtypes")
    private void populateProvidersCombo(){       
        providersComboBox.addItem(null);
        if (dataServerExplorer != null){
            List providerNames = dataServerExplorer.getDataStoreProviderNames();
            for (int i=0 ; i<providerNames.size() ; i++){
                providersComboBox.addItem(providerNames.get(i));         
            }
        }
    }

    private void removeProvidersCombo(){
        providersComboBox.removeAllItems();
    }

    private void providersComboSelectionChanged(){
        dataStoreParameters = null;
        String providerName = (String)providersComboBox.getSelectedItem();   
        if ((dataServerExplorer != null) && (providerName != null)){
            enableProviderControls(true);
        }else{
            enableProviderControls(false);
        }
    }

    private void providersButtonClicked(){
        String providerName = (String)providersComboBox.getSelectedItem();       
        if ((dataServerExplorer != null) && (providerName != null)){
            try {
                if ((dataStoreParameters == null) || (!dataStoreParameters.getDataStoreName().equals(providerName))){
                    dataStoreParameters = 
                        DATA_MANAGER.createNewStoreParameters(dataServerExplorer.getProviderName(), providerName);
                }
                DynObjectEditor dynObjectEditor = new DynObjectEditor(dataStoreParameters);
                dynObjectEditor.editObject(true);             
            } catch (InitializeException e) {
                LOG.error("Error creating the store parameters", e);
            } catch (ProviderNotRegisteredException e) {
                LOG.error("The provider has not been registered", e);
            } catch (ServiceException e) {
                LOG.error("Error creating the store panel", e);
            }
        }
    }  
}
