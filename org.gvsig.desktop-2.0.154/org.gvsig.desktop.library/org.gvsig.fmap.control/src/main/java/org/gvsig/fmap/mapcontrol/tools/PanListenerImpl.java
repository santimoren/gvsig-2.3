/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.MoveEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PanListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * <p>Listener for moving the extent of the associated {@link MapControl MapControl} object
 *  according the movement between the initial and final points of line determined by the movement
 *  dragging with the third button of the mouse.</p>
 *
 * <p>Updates the extent of its <code>ViewPort</code> with the new position.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class PanListenerImpl implements PanListener {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(PanListenerImpl.class);
	/**
	 * The image to display when the cursor is active.
	 */
//	private final Image ipan = PluginServices.getIconTheme().get("cursor-pan").getImage();
	
	/**
	 * Reference to the <code>MapControl</code> object that uses.
	 */
	private MapControl mapControl;

	/**
  	 * <p>Creates a new listener for changing the position of the extent of the associated {@link MapControl MapControl} object.</p>
	 *
	 * @param mapControl the <code>MapControl</code> where will be applied the changes
	 */
	public PanListenerImpl(MapControl mapControl) {
		this.mapControl = mapControl;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PanListener#move(com.iver.cit.gvsig.fmap.tools.Events.MoveEvent)
	 */
	public void move(MoveEvent event) {
		ViewPort vp = mapControl.getMapContext().getViewPort();

		Point2D from = vp.toMapPoint(event.getFrom());
		Point2D to = vp.toMapPoint(event.getTo());


		Rectangle2D extent = vp.getExtent();
		double x = extent.getX() - (to.getX() - from.getX());
		double y = extent.getY() - (to.getY() - from.getY());
		double width = extent.getWidth();
		double height = extent.getHeight();
		Envelope r;
		try {
			r = geomManager.createEnvelope(x, y, x+width, y+height, SUBTYPES.GEOM2D);
			vp.setEnvelope(r);
		} catch (CreateEnvelopeException e) {
			logger.warn("Error creating the envelope", e);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getCursor()
	 */
	public Image getImageCursor() {
		return IconThemeHelper.getImage("cursor-pan");
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
	 */
	public boolean cancelDrawing() {
		return true;
	}
}
