/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Events;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontrol.MapControl;


/**
 * <p><code>PointEvent</code> is used to notify a selection of a point with the mouse.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class PointEvent {
	/**
	 * <p>The point 2D associated to this event.</p>
	 */
	private Point2D p;

	/**
	 * <p>The point 2D associated to this event.</p>
	 */
	private MouseEvent e;

	private MapControl mapControl;

	/**
	 * <p>Creates a new <code>PointEvent</code> with all necessary data.</p>
	 *
	 * @param p the point 2D associated to this event
	 * @param e event that has been the cause of creating this one
	 */
	public PointEvent(Point2D p, MouseEvent e) {
		this.p = p;
		this.e = e;
		this.mapControl = null;
	}

	public PointEvent(Point2D p, MouseEvent e, MapControl mapControl) {
		this(p, e);
		this.mapControl = mapControl;
	}
	
	/**
	 * <p>Gets the point 2D in screen coordinates where this event was produced.</p>
	 *
	 * @return the point 2D associated to this event
	 */
	public Point2D getPoint() {
		return p;
	}

	/**
	 * Gets the Geometry point 2D in map coordinates where this event was produced.
	 *
	 * @return the Geometry point 2D associated to this event
	 */
	public Point getMapPoint() {
		if( mapControl == null ) {
			throw new UnsupportedOperationException();
		}
		return this.mapControl.getViewPort().convertToMapPoint(p);
	}

	
	/**
	 * <p>Sets the point 2D where this event was produced.</p>
	 *
	 * @param the point 2D associated to this event
	 */
	public void setPoint(Point2D p) {
		this.p = p;
	}

    /**
	 * <p>Gets the event that has been the cause of creating this one.</p>
	 * 
	 * @return mouse event that has been the cause of creating this one
     */
	public MouseEvent getEvent() {
		return e;
	}
}
