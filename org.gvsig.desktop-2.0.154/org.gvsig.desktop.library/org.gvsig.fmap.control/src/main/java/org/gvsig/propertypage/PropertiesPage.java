package org.gvsig.propertypage;

import org.gvsig.tools.swing.api.Component;




public interface PropertiesPage extends Component {
    
    /**
     * Title of the properties page
     * 
     * @return the title.
     */    
    public String getTitle();
    
    /**
     * The priority of this properties page.
     * When more high is the priority more to the left are set properties page.
     * 
     * @return the priority of the properties page
     */
    public int getPriority();

    /**
     * Called when the button accept is presed by the user.
     * If return false the panel is not hidden and do not call whenAccept of
     * other properties pages.
     * 
     * @return false to cancel the user action.
     */
    public boolean whenAccept();

    /**
     * Called when the button apply is presed by the user.
     * If return false the panel do not call whenApply of
     * other properties pages.
     * 
     * @return false to cancel the user action.
     */
    public boolean whenApply();

    /**
     * Called when the button accept is presed by the user.
     * If return false the panel is not hidden and do not call whenCancel of
     * other properties pages.
     * 
     * @return false to cancel the user action.
     */
    public boolean whenCancel();
    
}
