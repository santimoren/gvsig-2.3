/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a Table component for Features}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;


/**
 * Panel to configure a FeatureTable. Allows to configure visible columns and
 * column aliases.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class FeatureTableConfigurationPanel extends JPanel {

    private static final long serialVersionUID = -4912657164727512361L;

    private ConfigurationTableModel tableModel;

    /**
     * Creates a new FeatureTableConfigurationPanel.
     *
     * @param configurableTableModel
     *            the table model of the FeatureTable to configure.
     */
    public FeatureTableConfigurationPanel(
            ConfigurableFeatureTableModel configurableTableModel) {
        this(configurableTableModel, true);
    }

    /**
     * Creates a new FeatureTableConfigurationPanel.
     *
     * @param configurableTableModel
     *            the table model of the FeatureTable to configure.
     * @param isDoubleBuffered
     *            a boolean, true for double-buffering, which uses additional
     *            memory space to achieve fast, flicker-free updates
     */
    public FeatureTableConfigurationPanel(
            ConfigurableFeatureTableModel configurableTableModel,
            boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        tableModel = new ConfigurationTableModel(configurableTableModel);
        createGUI();
    }

    private void createGUI() {
		setPreferredSize(new java.awt.Dimension(391, 160));

		// TODO: set a fixed column width for the first column with the
        // checkboxes
        JTable table = new JTable(tableModel);
		// Set the width of the "Visible" column
		table.getColumnModel().getColumn(0).setPreferredWidth(
				ConfigurationTableModel.getVisibilityColumn());

        JScrollPane scrollPane = new JScrollPane(table);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(scrollPane);
    }

    /**
     * Make current changes in configuration (visible columns and aliases)
     * as definitive.
     */
    public void accept() {
    	tableModel.acceptChanges();
    }

    /**
     * Cancel current changes in configuration (visible columns and aliases)
     * and return to previous status.
     */
    public void cancel() {
    	tableModel.cancelChanges();
    }
}