package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.tools.Behavior.Behavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.IBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.MoveBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PointBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.RectangleBehavior;
import org.gvsig.fmap.mapcontrol.tools.Events.EnvelopeEvent;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;
import org.gvsig.fmap.mapcontrol.tools.Events.MoveEvent;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.CircleListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PanListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PolylineListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.RectangleListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.ToolListener;

public class BaseMapTool implements MapTool, ActionListener, IBehavior, 
	ToolListener, CircleListener, PanListener, PointListener, PolylineListener, RectangleListener {

	protected int type = TYPE_UNKNOW;
			
	protected ToolListener listener = null;
	protected IBehavior behavior = null;
	protected boolean isCancelDrawing = false;
	private String name;
	
	public BaseMapTool(int type) {
		super();
		this.setType(type);
	}
	
	public BaseMapTool(ToolListener listener) {
		super();
		this.setListener(listener);
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		throw new UnsupportedOperationException();
	}

	public Image getImageCursor() {
		return null;
	}

	public boolean cancelDrawing() {
		return this.isCancelDrawing;
	}

	public void setType(int type) {
		if( this.getMapControl()!=null ) {
			throw new IllegalStateException("Can't change the type if the tool is already binded to a MapControl.");
		}
		this.type = type;
	}

	public int getType() {
		return this.type;
	}
	
	public MapControl getMapControl() {
		return this.behavior.getMapControl();
	}

	public void setMapControl(MapControl mc) {
		if( this.behavior.getMapControl()!= null ) {
			throw new IllegalStateException("The tools is already binded to a MapControl, can't change it.");
		}
		this.behavior.setMapControl(mc);
		switch(this.type) {
			case TYPE_POINT:
				this.behavior = new PointBehavior(this);
				break;
			case TYPE_RECTANGLE:
				this.behavior = new RectangleBehavior(this);
				break;

			// TODO: a�adir el resto de casos TYPE_ al switch

		
			case ACTION_PAN:
				this.listener = new PanListenerImpl(this.getMapControl());
	        	this.behavior = new MoveBehavior((PanListener) this.listener);
	           	this.type = TYPE_MOVE;		
	           	break;
			
			case ACTION_ZOOM:
				this.behavior = new CompoundBehavior( new Behavior[] {
					new RectangleBehavior(new ZoomInListenerImpl(this.getMapControl())),
					new PointBehavior(new ZoomOutRightButtonListener(this.getMapControl()))
				});
				this.listener = null;
		       	this.type = TYPE_POINT;		
		       	break;
		       	
			// TODO: a�adir el resto de casos ACTION_ al switch
		    default:
		    	throw new IllegalStateException("The type "+this.type+" is not a valid value.");
			}
	}
	
	private void setListener(ToolListener listener) {
		if( listener instanceof CircleListener ) {
			this.setType(TYPE_CIRCLE);
			this.listener = listener;
			return;
		} 
		if( listener instanceof PointListener ) {
			this.setType(TYPE_POINT);
			this.listener = listener;
			return;
		} 
		if( listener instanceof RectangleListener ) {
			this.setType(TYPE_RECTANGLE);
			this.listener = listener;
			return;
		} 
		// TODO: a�adir el resto de ToolListeners
	}
	
	public ToolListener getListener() {
		return this;
	}

	protected void callActionPerformed(Object event, int type, String actionCommand) {
		try {
			ActionEvent actionEvent = new ActionEvent(event,type, actionCommand);
			this.actionPerformed(actionEvent);
		} catch(Throwable e) {
			this.isCancelDrawing = true;
		}
	}
	
	public void rectangle(EnvelopeEvent event) throws BehaviorException {
		if( this.listener==null ) {
			this.callActionPerformed(event,EVENT_RECTANGLE, "rectangle");
			return;
		}
		((RectangleListener)this.listener).rectangle(event);
	}

	public void points(MeasureEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_POINTS, "points");
	}

	public void pointFixed(MeasureEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_POINTFIXED, "pointfixed");
	}

	public void polylineFinished(MeasureEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_POLYLINEFINISHED, "polylinefinished");
	}

	public void point(PointEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_POINT, "point");
	}

	public void pointDoubleClick(PointEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_POINTDOUBLECLICKED, "pointdoubleclicked");
	}

	public void move(MoveEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_MOVE, "move");
	}

	public void circle(MeasureEvent event) throws BehaviorException {
		this.callActionPerformed(event,EVENT_MEASURE, "measure");
	}

	public void paintComponent(MapControlDrawer renderer) {
		this.behavior.paintComponent(renderer);
	}

	public void mouseClicked(MouseEvent e) throws BehaviorException {
		this.behavior.mouseClicked(e);
	}

	public void mouseEntered(MouseEvent e) throws BehaviorException {
		this.behavior.mouseEntered(e);
	}

	public void mouseExited(MouseEvent e) throws BehaviorException {
		this.behavior.mouseExited(e);
	}

	public void mousePressed(MouseEvent e) throws BehaviorException {
		this.behavior.mousePressed(e);
	}

	public void mouseReleased(MouseEvent e) throws BehaviorException {
		this.behavior.mouseReleased(e);
	}

	public void mouseDragged(MouseEvent e) throws BehaviorException {
		this.behavior.mouseDragged(e);
	}

	public void mouseMoved(MouseEvent e) throws BehaviorException {
		this.behavior.mouseMoved(null);
	}

	public void mouseWheelMoved(MouseWheelEvent e) throws BehaviorException {
		this.behavior.mouseWheelMoved(e);
	}

}
