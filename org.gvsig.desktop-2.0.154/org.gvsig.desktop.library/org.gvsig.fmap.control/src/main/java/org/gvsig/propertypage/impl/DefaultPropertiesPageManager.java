package org.gvsig.propertypage.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;
import org.gvsig.propertypage.PropertiesPageManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.identitymanagement.SimpleIdentity;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPropertiesPageManager implements PropertiesPageManager {

    public static final String ACCESS_PAGE_AUTHORIZATION = "propertypage-access";

    private static final Logger logger = LoggerFactory.getLogger(DefaultPropertiesPageManager.class);

    private final Map<String, List<PropertiesPageFactory>> groups = new HashMap<>();

    public DefaultPropertiesPageManager() {

    }

    @Override
    public void registerFactory(PropertiesPageFactory factory) {
        logger.info("Register PropertisPageFactory " + factory.getClass().getName() + " , groupID=" + factory.getGroupID() + " (" + factory.toString() + ").");
        List<PropertiesPageFactory> group = this.getFactories(factory.getGroupID());
        group.add(factory);
    }

    private List<PropertiesPageFactory> getFactories(String groupID) {
        logger.info("get propeties page factories for groupID '" + groupID + "'");
        List<PropertiesPageFactory> group = this.groups.get(groupID);
        if (group == null) {
            group = new ArrayList<>();
            this.groups.put(groupID, group);
        }
        return group;
    }

    @Override
    public List<PropertiesPage> getPages(String groupID, Object obj) {
        logger.info("get propeties page for groupID '" + groupID + "', and object '" + obj.toString() + "'.");
        List<PropertiesPage> pages = new ArrayList<>();
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        SimpleIdentity currentUser = identityManager.getCurrentIdentity();
        if (!currentUser.isAuthorized(ACCESS_PAGE_AUTHORIZATION,null,groupID ) ) {
            return pages; // Empty list, no access to pages authoriced
        }

        List<PropertiesPageFactory> factories = this.getFactories(groupID);
        String authorizationId = ACCESS_PAGE_AUTHORIZATION+"-"+groupID;
        for (PropertiesPageFactory factory : factories) {
            if (factory.isVisible(obj)) {
                PropertiesPage page = factory.create(obj);
                if (currentUser.isAuthorized(authorizationId,page, page.getTitle()) ) {
                    pages.add(page);
                }
            }
        }
        Collections.sort(pages, new Comparator<PropertiesPage>() {
            public int compare(PropertiesPage f1, PropertiesPage f2) {
                int n = f2.getPriority() - f1.getPriority();
                if (n != 0) {
                    return n;
                }
                if (f1.getTitle() == null || f2.getTitle() == null) {
                    // Mas que nada para que no pete, y aunque el orden
                    // no sea correcto muestre algo al usuario.
                    return 0;
                }
                return f1.getTitle().compareTo(f2.getTitle());
            }
        });
        return pages;
    }

}
