/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a Table component for Features}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractListModel;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.text.StyleConstants;

import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.ConfigurableFeatureTableModel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureTableConfigurationPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel to show a table of Feature data.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class FeatureTablePanel extends JPanel implements Observer {

    private static final Logger logger = LoggerFactory.getLogger(FeatureTablePanel.class);

    private static final long serialVersionUID = -9199073063283531216L;

    private ConfigurableFeatureTableModel tableModel;

    private JScrollPane jScrollPane = null;

    private FeatureTable table = null;
    private JPanel selectionPanel;
    private JLabel selectionLabel;

    /**
     * Constructs a Panel to show a table with the features of a FeatureStore.
     *
     * @param featureStore
     * to extract the features from
     * @throws BaseException
     * if there is an error reading data from the FeatureStore
     */
    public FeatureTablePanel(FeatureStore featureStore) throws BaseException {
        this(featureStore, true);
    }

    /**
     * Constructs a Panel to show a table with the features of a FeatureStore.
     *
     * @param featureStore
     * to extract the features from
     * @param isDoubleBuffered
     * a boolean, true for double-buffering, which uses additional
     * memory space to achieve fast, flicker-free updates
     * @throws BaseException
     * if there is an error reading data from the FeatureStore
     */
    public FeatureTablePanel(FeatureStore featureStore, boolean isDoubleBuffered)
            throws BaseException {
        this(featureStore, null, isDoubleBuffered);
    }

    /**
     * @throws BaseException
     *
     */
    public FeatureTablePanel(FeatureStore featureStore,
            FeatureQuery featureQuery) throws BaseException {
        this(featureStore, featureQuery, true);
    }

    /**
     * @param isDoubleBuffered
     * @throws BaseException
     */
    public FeatureTablePanel(FeatureStore featureStore,
            FeatureQuery featureQuery, boolean isDoubleBuffered)
            throws BaseException {
        this(createModel(featureStore, featureQuery));
    }

    public FeatureTablePanel(ConfigurableFeatureTableModel tableModel)
            throws DataException {
        this(tableModel, true);
    }

    public FeatureTablePanel(ConfigurableFeatureTableModel tableModel,
            boolean isDoubleBuffered) throws DataException {
        super(isDoubleBuffered);
        this.tableModel = tableModel;
        this.setLayout(new BorderLayout());
        this.add(getJScrollPane(), BorderLayout.CENTER);
        this.add(getSelectionPanel(), BorderLayout.SOUTH);
        tableModel.getFeatureStore().addObserver(this);
    }

    private JPanel getSelectionPanel() {
        if ( selectionPanel == null ) {
            selectionLabel = new JLabel();
            selectionLabel.setText(getFeatureSelectionSize() + " / "
                    + getTableModel().getHelper().getTotalSize() + " "
                    + Messages.getText("registros_seleccionados_total") + ".");
            selectionPanel = new JPanel();

            selectionPanel.add(selectionLabel, BorderLayout.EAST);
        }
        return selectionPanel;
    }

    private void updateSelection() {
        selectionLabel.setText(getFeatureSelectionSize() + " / "
                + getTableModel().getRowCount() + " "
                + Messages.getText("registros_seleccionados_total") + ".");
    }

    private long getFeatureSelectionSize() {
        try {
            return getFeatureStore().getFeatureSelection().getSize();
        } catch (DataException e) {
            throw new RuntimeException("Error updating selection information",
                    e);
        }
    }

    public JPanel createConfigurationPanel() {
        return new FeatureTableConfigurationPanel(tableModel);
    }

    /**
     * Returns the internal Table Model for the Features of the FeatureStore.
     *
     * @return the internal Table Model
     */
    public ConfigurableFeatureTableModel getTableModel() {
        return tableModel;
    }

    /**
     * Returns the {@link FeatureStore} of the {@link Feature}s being shown in
     * the table.
     *
     * @return the store of the features
     */
    public FeatureStore getFeatureStore() {
        return getTableModel().getFeatureStore();
    }

    /**
     * Returns the FeatureQuery used to get the Features.
     *
     * @return the FeatureQuery
     */
    public FeatureQuery getFeatureQuery() {
        return getTableModel().getFeatureQuery();
    }

    /**
     * Sets that the selected Features to be viewed first.
     */
    public void setSelectionUp(boolean selectionUp) {
        try {
            getTable().setSelectionUp(selectionUp);
        } catch (DataException e) {
            throw new RuntimeException("Error setting the selection up", e);
        }
    }

    private static ConfigurableFeatureTableModel createModel(
            FeatureStore featureStore, FeatureQuery featureQuery)
            throws BaseException {
        FeatureQuery query
                = featureQuery == null ? featureStore.createFeatureQuery()
                : featureQuery;

        return new ConfigurableFeatureTableModel(featureStore, query);
    }

    public FeatureTable getTable() throws DataException {
        if ( table == null ) {
            table = new FeatureTable(tableModel);
            // Change the selection model to link with the FeatureStore
            // selection
            // through the FeatureTableModel
            table.setRowSelectionAllowed(true);
            table.setColumnSelectionAllowed(false);
            // table.setSelectionModel(new FeatureSelectionModel(tableModel));
        }
        return table;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     * @throws DataException
     */
    private JScrollPane getJScrollPane() throws DataException {
        if ( jScrollPane == null ) {
            FeatureTable theTable = getTable();
            jScrollPane = new JScrollPane();
            jScrollPane.setViewportView(theTable);
            createTheRowHeader();
            theTable.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent pce) {
                    if( "RowHeight".equalsIgnoreCase(pce.getPropertyName())) {
                        // No he averigado como cambiar el ancho de las lineas
                        // ya creadas de la cabezera de lineas, asi que la
                        // reconstruyo entera cuando algo cambia.
                        createTheRowHeader();
                    }
                }
            });
            TableModel model = theTable.getModel();
            model.addTableModelListener(new TableModelListener() {
                public void tableChanged(TableModelEvent tme) {
                    // No he averigado como cambiar el ancho de las lineas
                    // ya creadas de la cabezera de lineas, asi que la
                    // reconstruyo entera cuando algo cambia.
                    createTheRowHeader();
                }
            });
        }
        return jScrollPane;
    }

    void createTheRowHeader() {
        // No se si ha sido paranoia o que, pero parece que si la recreo sin mas
        // a veces parece como si no la cambiase, asi que he probado a encolarlo 
        // en la cola de eventos de swing y parece que siempre funciona.
        //
        // Cuando se estan mostrando las geometrias, que cada linea tiene un ancho
        // distinto, se le llama muchisimas veces;
        // habria que evaluar retenerlas por ejemplo durante un segundo y solo 
        // recrearlo entonces.
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                ListModel lm = new AbstractListModel() {
                    public int getSize() {
                        return table.getRowCount();
                    }

                    public Object getElementAt(int index) {
                        return String.valueOf(index + 1);
                    }
                };
                final JList rowHeader = new JList(lm);
                rowHeader.setBackground(table.getTableHeader().getBackground());
                rowHeader.setCellRenderer(new RowHeaderRenderer(table,rowHeader));
                jScrollPane.setRowHeaderView(rowHeader);
            }
        });
    }

    private static class RowHeaderRenderer extends JButton implements ListCellRenderer {
        private JTable table = null;
        private final Dimension dimension = new Dimension();
        private JList rowHeader;
        
        RowHeaderRenderer(JTable table,JList rowHeader) {
            JTableHeader header = table.getTableHeader();
            setOpaque(true);
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(CENTER);
            setForeground(header.getForeground());
            setBackground(header.getBackground());
            setFont(header.getFont());
            this.table = table;
            this.rowHeader = rowHeader;
        }

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            setText((value == null) ? "" : value.toString());
            this.setPreferredSize(null); // Fuerza recalcular el tama�o del boton
            this.dimension.height = this.table.getRowHeight(index);
            this.dimension.width = this.getPreferredSize().width+10;
            this.setPreferredSize(this.dimension);
            return this;
        }

    }

    public void update(Observable observable, Object notification) {
        // If selection changes from nothing selected to anything selected
        // or the reverse, update selection info
        if ( notification instanceof DataStoreNotification ) {
            String type = ((DataStoreNotification) notification).getType();
            if ( DataStoreNotification.SELECTION_CHANGE.equals(type) ) {
                updateSelection();
            }
        }
    }
    // public void valueChanged(ListSelectionEvent e) {
    // updateSelection();
    // updateUI();
    // }
    //
    // public void tableChanged(TableModelEvent e) {
    // updateSelection();
    // updateUI();
    // }
}
