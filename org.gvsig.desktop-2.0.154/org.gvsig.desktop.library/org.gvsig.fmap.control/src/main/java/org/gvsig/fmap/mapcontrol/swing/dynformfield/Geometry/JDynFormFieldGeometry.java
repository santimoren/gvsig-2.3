package org.gvsig.fmap.mapcontrol.swing.dynformfield.Geometry;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dynform.JDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.AbstractJDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.JCustomTextField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.spi.ServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JDynFormFieldGeometry extends AbstractJDynFormField implements JDynFormField, FocusListener {
	
        private static final Logger logger = LoggerFactory.getLogger(JDynFormFieldGeometry.class);
        
	protected Geometry assignedValue  = null;
	protected Geometry currentValue = null;
	protected boolean showWKT = false;

	public JDynFormFieldGeometry(DynObject parameters,
			ServiceManager serviceManager) {
		super(parameters, serviceManager);
		this.assignedValue = (Geometry) this.getParameterValue();
	}

	public Object getAssignedValue() {
		return this.assignedValue;
	}
	
	protected JCustomTextField getJTextField() {
		return (JCustomTextField) this.contents;
	}
	
	public void initComponent() {	
                I18nManager i18nManager = ToolsLocator.getI18nManager();
		JCustomTextField jtext = new JCustomTextField(getLabel());
		jtext.addFocusListener(this);
		jtext.setEditable(false);

		jtext.addSeparatorToPopupMenu();
		jtext.addActionToPopupMenu(
                        i18nManager.getTranslation("_Toggle_show_geometry_as_WKT"),
                        new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				toggleShowGeometryAsWKT();
			}
		});
		
		this.contents = jtext;
		this.contents.setVisible(true);
		this.setValue(this.assignedValue);
	}
	
	private void toggleShowGeometryAsWKT() {
		if( this.showWKT ) {
			Geometry x = null;
			try {
				x = (Geometry) this.getValue();
			} catch( IllegalFieldValue ex) {
				x = null;
			}
			if( x!= null ) {
				this.getJTextField().setText(x.toString());
			} else {
				this.getJTextField().setText("");
			}
			this.showWKT = false;			
			this.getJTextField().setEditable(false);
			this.currentValue = x;
		} else {
			String s = null;
			if( this.currentValue == null ) {
				s = "" ;
			} else {
				try {
					s = this.currentValue.convertToWKT();
				} catch (Exception e) {
                                    I18nManager i18nManager = ToolsLocator.getI18nManager();
                                    logger.warn("Can't convert geometry to WKT.",e);
                                    JOptionPane.showMessageDialog(
                                            this.asJComponent(), 
                                            i18nManager.getTranslation("_Cant_convert_geometry_to_WKT"), 
                                            i18nManager.getTranslation("_Warning"), 
                                            JOptionPane.WARNING_MESSAGE
                                    );
                                    s = "";
				} 
			}
			this.getJTextField().setText(s);
			this.getJTextField().setEditable(true);
			this.showWKT = true;
		}
	}
	
	public boolean hasValidValue() {
		if( !showWKT ) {
			return true;
		}
		try {
			this.getDefinition().getDataType().coerce(getJTextField().getText());
		} catch (CoercionException e) {
			return false;
		}
		return true;
	}

	public void focusGained(FocusEvent arg0) {
		fireFieldEnterEvent();
		this.problemIndicator().restore();
	}

	public void focusLost(FocusEvent arg0) {
		fireFieldExitEvent();
	}

	public void setValue(Object value) {
		String s = null;
		Geometry x = null;
		try {
			x = (Geometry) this.getDefinition().getDataType().coerce(value);
		} catch (CoercionException e) {
			throw new IllegalFieldValue(this,e.getMessage());
		}
		if( x!=null ) {
			if( this.showWKT ) {
				try {
					s = x.convertToWKT();
				} catch (Exception e) {
					this.showWKT = false;
					this.getJTextField().setEditable(false);
					s = x.toString();
				}
			} else {
				s = x.toString();
			}
			this.getJTextField().setText(s);
		}
		this.currentValue = x;
		this.assignedValue = x;
	}

	public Object getValue() {
		if( !showWKT ) {
			return this.currentValue;
		}
		Geometry x = null;
		try {
			x = (Geometry) this.getDefinition().getDataType().coerce(getJTextField().getText());
		} catch (CoercionException e) {
			throw new IllegalFieldValue(this,e.getMessage());
		}
		this.currentValue = x;
		return this.currentValue;
	}

}
