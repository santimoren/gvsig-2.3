/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.fmap.mapcontrol;

import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.tree.TreeModel;

import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;

/**
 * <p>
 * This class is the manager of the MapControl library. It is used to
 * manage all the properties related with the drawing of objects 
 * in a map, including default symbols used to draw objects
 * in a map, the tolerance used by the selection or edition tools...
 * </p>
 * <p>
 * It also holds the implementations of the {@link MapControlDrawer}'s, 
 * that is the responsible to draw graphical objects in a map.
 * </p> 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface MapControlManager {

	public MapControl createJMapControlPanel(MapContext mapContext) throws MapControlCreationException;
	  
    public void addMapControlCreationListener(MapControlCreationListener listener);
        
	/**
	 * Register a <code>MapControlDrawer</code> by name.
	 * @param name
	 * Name of the <code>MapControlDrawer</code>.
	 * @param mapControolDrawerClass
	 * Class used to draw graphical objects on a map.
	 */
	public void registerMapControlDrawer(String name, Class mapControolDrawerClass);
	
	/**
	 * Creates a <code>MapControlDrawer</code> from a name.
	 * @param name
	 * Name of the <code>MapControlDrawer</code>.
	 * @return
	 * A <code>MapControlDrawer</code>.
	 * @throws MapControlCreationException
	 */
	public MapControlDrawer createMapControlDrawer(String name) throws MapControlCreationException;
	
	/**
	 * It registers the default implementation for the <code>MapControlDrawer</code>.
	 * @param mapControlDrawerClass
	 * A <code>MapControlDrawer</code>. 
	 */
	public void registerDefaultMapControlDrawer(Class mapControlDrawerClass);
	
	/**
	 * It returns the default implementation for the <code>MapControlDrawer</code>.
	 * @return
	 * The default <code>MapControlDrawer</code>.
	 * @throws MapControlCreationException
	 */
	public MapControlDrawer createDefaultMapControlDrawer() throws MapControlCreationException;
	
	/**
	 * Returns a snapper in a concrete position;
	 * @param index
	 * Snapper position.
	 * @return
	 * A snapper. 
	 */
	public ISnapper getSnapperAt(int index);
	
	/**
	 * Returns the number of registered snappers.
	 * @return
	 * The number of registered snappers.
	 */
	public int getSnapperCount();
	
	/**
	 * Add a snapper.
	 * @param snapper
	 */
	public void registerSnapper(String name, Class snapperClass);
	
	
	public Preferences getEditionPreferences();
	
	/**
	 * Tolerance (in pixels) that has to be used by the tools
	 * that use snapping.
	 * @return
	 * The distance in pixels.
	 */
	public int getTolerance();
	
	/**
	 * Sets the tolerance (in pixels) that has to be used by the
	 * tools that use snapping.
	 * @param tolerance
	 * The tolerance to apply
	 */
	public void setTolerance(int tolerance);
	
	/**
	 * Sets the symbol that has to be used to draw a geometry when
	 * it is selected.
	 * @param selectionSymbol
	 * The symbol to apply.
	 * @deprecated the symbol for edition is the selection symbol
	 */
	public void setSelectionSymbol(ISymbol selectionSymbol);
	
	/**
	 * Gets the symbol used to draw the selected geometries.
	 * @return
	 * The symbol used to draw the selected geometries.
	 * @deprecated the symbol for edition is the selection symbol
	 */
	public ISymbol getSelectionSymbol();
	
	/**
	 * Sets the symbol that has to be used to draw a geometry that
	 * represent the axis of a geometry.
	 * @param axisReferencesSymbol
	 * The symbol to apply.
	 */
	public void setAxisReferenceSymbol(ISymbol axisReferencesSymbol);
	
	/**
	 * Gets the symbol used to draw the axis of a geometry.
	 * @return
	 * The symbol used to draw the axis of a geometry.
	 */
	public ISymbol getAxisReferenceSymbol();
	
	/**
	 * Sets the symbol that has to be used to draw a geometry when
	 * it is selected.
	 * @param geometrySelectionSymbol
	 * The symbol to apply.
	 */
	public void setGeometrySelectionSymbol(ISymbol geometrySelectionSymbol);
	
	/**
	 * Gets the symbol used to draw the selected geometries.
	 * @return
	 * The symbol used to draw the selected geometries.
	 */
	public ISymbol getGeometrySelectionSymbol();
	
	/**
	 * Sets the symbol that has to be used to draw the handlers.
	 * @param handlerSymbol
	 * The symbol to apply.
	 */
	public void setHandlerSymbol(ISymbol handlerSymbol);
	
	/**
	 * Gets the symbol used to draw the handlers.
	 * @return
	 * The symbol used to draw the handlers.
	 */
	public ISymbol getHandlerSymbol();

    /**
     * Creates a readonly component to view information of a set of layers. The
     * information must be provided as a set of {@link DynObject}s, through a
     * {@link DynObjectSet}.
     * 
     * @param layerName2InfoByPoint
     *            the map of {@link DynObjectSet} for each layer.
     * @return the component to view the information
     */
    public LayersDynObjectSetComponent createLayersDynObjectSetComponent(
        Map<String, DynObjectSet> layerName2InfoByPoint);

    /**
     * Creates a component to view information of a set of layers. The
     * information must be provided as a set of {@link DynObject}s, through a
     * {@link DynObjectSet}.
     * 
     * @param layerName2InfoByPoint
     *            the map of {@link DynObjectSet} for each layer.
     * @param writable
     *            if the DynObjects loaded must be able to be edited
     * @return the component to view the information
     */
    public LayersDynObjectSetComponent createLayersDynObjectSetComponent(
        Map<String, DynObjectSet> layerName2InfoByPoint, boolean writable);
    
    /**
     * Create a TreeModel based in the MapControl's layers.
     * 
     * @param mapContext
     * @return the TreeModel 
     */
    public TreeModel createLayersTreeModel(MapContext mapContext);
    
    /**
     * Create a TreeModel based in the layers collection.
     * @param layers the layers collection to use for create the TreeModel
     * @return the TreeModel
     */
    public TreeModel createLayersTreeModel(FLayers layers);
    
    /**
     * Create a TreeModel based in the layers collection of all project's views.
     * @return the TreeModel
     */
    public TreeModel createCompoundLayersTreeModel();
    
}

