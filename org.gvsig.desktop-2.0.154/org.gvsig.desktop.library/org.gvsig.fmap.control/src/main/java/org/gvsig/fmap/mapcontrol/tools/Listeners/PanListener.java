/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Listeners;

import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MoveEvent;



/**
 * <p>Interface for all tools that reply for a {@link MoveEvent MoveEvent} produced in the 
 *  associated {@link MapControl MapControl} object, as a consequence of a 2D drag and drop
 *  movement of the mouse.</p>
 *
 * @author Vicente Caballero Navarro
 */
public interface PanListener extends ToolListener {
	/**
	 * <p>Called when user drags the mouse on the view.</p>
	 * <p>Updates the extent coordinates according to the direction of the movement between
	 *  the initial and final points of line determined by the move of the mouse.</p>
	 *
	 * @param event mouse event information about the initial and final positions of the movement 
	 *
	 * @throws BehaviorException will be thrown when fails the process of this tool
	 */
	public void move(MoveEvent event) throws BehaviorException;
}
