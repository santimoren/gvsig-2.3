/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;

import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.EnvelopeEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.RectangleListener;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;

/**
 * <p>Listener that selects all features of the active and vector layers which intersect with the defined
 *  rectangle area in the associated {@link MapControl MapControl} object.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class RectangleSelectionListener implements RectangleListener {
	/**
	 * Reference to the <code>MapControl</code> object that uses.
	 */
	private MapControl mapCtrl;

	/**
  	 * <p>Creates a new <code>RectangleSelectionListener</code> object.</p>
	 *
	 * @param mc the <code>MapControl</code> where is defined the rectangle
	 */
	public RectangleSelectionListener(MapControl mc) {
		this.mapCtrl = mc;
	}

	public void rectangle(EnvelopeEvent event) throws BehaviorException {
		try {
			// mapCtrl.getMapContext().selectByRect(event.getWorldCoordRect());
            Envelope rect = event.getWorldCoordRect();
            FLayer[] actives = mapCtrl.getMapContext().getLayers().getActives();
            
            for (int i = 0; i < actives.length; i++) {
                if (actives[i] instanceof FLyrVect) {
                    FLyrVect lyrVect = (FLyrVect) actives[i];
					FeatureSet newSelection = null;
					try {
						newSelection =
								lyrVect.queryByEnvelope(rect,
										lyrVect.getFeatureStore()
												.getDefaultFeatureType());
						if (event.getEvent().isControlDown()) {
							FeatureSelection currentSelection = (FeatureSelection)lyrVect.getFeatureStore().getSelection();
							DisposableIterator it = newSelection.fastIterator();
							while(it.hasNext()) {
								Object obj = it.next();
								if(obj instanceof Feature) {
									Feature feat = ((Feature)obj);
									if(currentSelection.isSelected(feat))
										currentSelection.deselect(feat);
									else
										currentSelection.select(feat);
								}
							}
						} else {
							lyrVect.getDataStore().setSelection(newSelection);
						}
					} finally {
						DisposeUtils.dispose(newSelection);
                    }
                }
            }

		} catch (DataException e) {
			throw new BehaviorException("No se pudo hacer la selecci�n", e);
		}
	}

	public Image getImageCursor() {
		return IconThemeHelper.getImage("cursor-select-by-rectangle");
	}

	public boolean cancelDrawing() {
		return false;
	}
}
