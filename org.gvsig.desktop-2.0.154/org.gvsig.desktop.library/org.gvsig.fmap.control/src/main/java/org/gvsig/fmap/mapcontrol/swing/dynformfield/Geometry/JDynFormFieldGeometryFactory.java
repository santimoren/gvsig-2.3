package org.gvsig.fmap.mapcontrol.swing.dynformfield.Geometry;

import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.tools.dynform.spi.DynFormSPILocator;
import org.gvsig.tools.dynform.spi.DynFormSPIManager;
import org.gvsig.tools.dynform.spi.dynformfield.AbstractJDynFormFieldFactory;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ServiceManager;

public class JDynFormFieldGeometryFactory extends AbstractJDynFormFieldFactory {

	public String getName() {
		if( name == null ) {
			DynFormSPIManager manager = DynFormSPILocator.getDynFormSPIManager();
			this.name = manager.makeServiceName(DataTypes.GEOMETRY, null);
		}
		return this.name;
	}

	public Service create(DynObject parameters, ServiceManager serviceManager)
			throws ServiceException {
		return new JDynFormFieldGeometry(parameters, serviceManager);
	}

}
