

package org.gvsig.fmap.mapcontrol;


public interface MapControlCreationListener {
    
    public MapControl mapControlCreated(MapControl mapControl);
}
