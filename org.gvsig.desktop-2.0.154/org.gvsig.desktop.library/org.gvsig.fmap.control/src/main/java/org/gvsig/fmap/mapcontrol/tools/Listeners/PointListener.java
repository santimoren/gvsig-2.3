/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Listeners;

import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;



/**
 * <p>Interface for all tools that reply for a {@link PointEvent PointEvent} produced in the 
 *  associated {@link MapControl MapControl} object, produced by a simple or double click of a button
 *  of the mouse.</p>
 *
 * @author Vicente Caballero Navarro
 */
public interface PointListener extends ToolListener {
	/**
	 * <p>Called when one click is pressed on the associated <code>MapControl</code>, or the location of the cursor
	 *  of the mouse has changed on it.</p>
	 *
	 * @param event mouse event with the coordinates of the point selected on the associated <code>MapControl</code>
	 *
	 * @throws BehaviorException will be thrown when fails the process of this tool
	 */
	public void point(PointEvent event) throws BehaviorException;

	/**
	 * <p>Called when a double click is pressed on the associated <code>MapControl</code>.</p>
	 *
	 * @param event mouse event and the coordinates of the point selected on the associated <code>MapControl</code>
	 *
	 * @throws BehaviorException will be thrown when fails the process of this tool
	 */
	public void pointDoubleClick(PointEvent event) throws BehaviorException;
}
