
package org.gvsig.fmap.mapcontrol.dal.expresionPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExpresionPanel extends ExpresionPanelLayout {
    
    private static final Logger logger = LoggerFactory.getLogger(ExpresionPanel.class);
            
    private FeatureStore store;
    private Map values;
    private boolean okCancelVisible = true;
    private boolean isCanceled;

    private static class FeatureTypeListModel implements ListModel {
        private final FeatureType featureType;

        public FeatureTypeListModel(FeatureType featureType) {
            this.featureType = featureType;
        }
        public int getSize() {
            return this.featureType.size();
        }

        public Object getElementAt(int index) {
            return this.featureType.get(index);
        }

        public void addListDataListener(ListDataListener l) {
            
        }

        public void removeListDataListener(ListDataListener l) {
            
        }
        
    }
    
    public ExpresionPanel(FeatureStore store) {
        this.isCanceled = false;
        this.initComponents();
    }
    
    public void setFeatureStore(FeatureStore store) {
        this.store = store;
        try {
            this.lstFields.setModel(new FeatureTypeListModel(this.store.getDefaultFeatureType()));
        } catch (DataException ex) {
            logger.warn("Can't create model for field list.",ex);
        }
    }
    
    public void setOkCancelVisible(boolean acceptVisible) {
        this.okCancelVisible = acceptVisible;
    }
    
    public boolean getOkCancelVisible() {
        return this.okCancelVisible;
    }
    
    private void initComponents() {
        this.butAccept.setVisible(this.okCancelVisible);
        
        this.lstFields.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                doSelectField();
            }
        });
        this.lstValues.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                doSelectValue();
            }
        });
        ActionListener action = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doInsert(e.getActionCommand()+" ");
            }
        };
        this.butAnd.addActionListener(action);
        this.butOr.addActionListener(action);
        this.butNot.addActionListener(action);
        this.butEq.addActionListener(action);
        this.butNe.addActionListener(action);
        this.butGt.addActionListener(action);
        this.butLt.addActionListener(action);
        this.butGe.addActionListener(action);
        this.butLe.addActionListener(action);
        
        this.butClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doClear();
            }
        });
    
        this.butAccept.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doAcept();
            }
        });

        this.butCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doCancel();
            }
        });

    }

    public boolean isCanceled() {
        return this.isCanceled;
    }
    
    public String getExpresion() {
        return StringUtils.defaultIfBlank(this.txtExpresion.getText(), null);
    }
    
    public void showAsDialog(String title) {
        WindowManager winmgr = ToolsSwingLocator.getWindowManager();
        winmgr.showWindow(this, title, WindowManager.MODE.DIALOG);
    }
    
    private void doAcept() {
        this.isCanceled=false;
        this.setVisible(false);
    }

    private void doCancel() {
        this.isCanceled=true;
        this.setVisible(false);
    }

    private void doSelectField() {
        FeatureAttributeDescriptor x = (FeatureAttributeDescriptor) this.lstFields.getSelectedValue();
        if( x == null ) {
            return;
        }
        this.doInsert(x.getName());
        this.doInsert(" ");
        this.fillValuesList(x.getName());
    }

    private void doClear() {
        this.txtExpresion.setText("");
    }

    private void doSelectValue() {
        // TODO: falta por implemetar doSelectValue
    }

    private void doInsert(String s) {
        this.txtExpresion.insert(s, this.txtExpresion.getCaretPosition());
    }

    private void fillValuesList(String name) {
        // TODO: falta por implemetar fillValuesList
    }
    

}
