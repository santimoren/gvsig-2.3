/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Behavior;

import java.awt.AlphaComposite;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MoveEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PanListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.ToolListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Behavior that permits user to move the image of the associated
 * <code>MapControl</code> using a {@link PanListener PanListener}.
 * </p>
 *
 * @author Vicente Caballero Navarro
 * @author Pablo Piqueras Bartolomé
 */
public class MoveBehavior extends Behavior {
    private static final Logger logger = LoggerFactory.getLogger(MoveBehavior.class);
    /**
     * First point of the path in image coordinates.
     */
    protected Point2D m_FirstPoint;

    /**
     * Last point of the path in image coordinates.
     */
    protected Point2D m_LastPoint;

    /**
     * Tool listener used to work with the <code>MapControl</code> object.
     *
     * @see #getListener()
     * @see #setListener(ToolListener)
     */
    private PanListener listener;

    /**
     * <p>
     * Creates a new behavior for moving the mouse.
     * </p>
     *
     * @param pli
     *            listener used to permit this object to work with the
     *            associated <code>MapControl</code>
     * @param mouseButton
     */
    public MoveBehavior(PanListener pli, int mouseButton) {
        super(mouseButton);
        listener = pli;
    }

    public MoveBehavior(PanListener pli) {
        this(pli,BUTTON_LEFT);
    }

    @Override
    public void paintComponent(MapControlDrawer renderer) {
        if( ! this.isMyButton() ) {
            return;
        }
        super.paintComponent(renderer);
        BufferedImage image = getMapControl().getImage();
        if (image != null) {
            if ((m_FirstPoint != null) && (m_LastPoint != null)) {
                renderer.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, 0.5f));
                renderer.drawImage(image,
                    (int) (m_LastPoint.getX() - m_FirstPoint.getX()),
                    (int) (m_LastPoint.getY() - m_FirstPoint.getY()));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (this.isMyButton(e)) {
            m_FirstPoint = e.getPoint();
            if (listener.cancelDrawing()) {
               getMapControl().cancelDrawing();
           }
        }


    }

    @Override
    public void mouseReleased(MouseEvent e) throws BehaviorException {
        if (this.isMyButton(e) && m_FirstPoint != null) {
            doMouseReleased(e);
            this.resetMyButton();
        }
    }

    protected void doMouseReleased(MouseEvent e) throws BehaviorException {
        MoveEvent event = new MoveEvent(m_FirstPoint, e.getPoint(), e);
        listener.move(event);

        getMapControl().drawMap(true);

        m_FirstPoint = null;
    }



    @Override
    public void mouseDragged(MouseEvent e) {
        if (this.isMyButton(e)) {
            m_LastPoint = e.getPoint();
            getMapControl().repaint();
        }
    }

    /**
     * <p>
     * Sets a tool listener to work with the <code>MapControl</code> using this
     * behavior.
     * </p>
     *
     * @param listener
     *            a <code>PanListener</code> object for this behavior
     */
    public void setListener(ToolListener listener) {
        this.listener = (PanListener) listener;
    }

    public ToolListener getListener() {
        return listener;
    }
}
