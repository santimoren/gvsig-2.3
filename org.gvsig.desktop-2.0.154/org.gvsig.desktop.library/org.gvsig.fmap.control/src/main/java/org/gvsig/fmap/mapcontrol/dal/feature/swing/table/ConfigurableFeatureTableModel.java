/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a JTable TableModel for a FeatureQuery}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.event.TableModelEvent;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extends the FeatureTableModel to add more configurable options, like the
 * visible columns, column name aliases and row order.
 * 
 * TODO: a�adir la persistencia.
 * 
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class ConfigurableFeatureTableModel extends FeatureTableModel {

    private static final long serialVersionUID = -8223987814719746492L;
    
    private static final Logger logger = LoggerFactory.getLogger(ConfigurableFeatureTableModel.class);

    private List<String> columnNames;

    private List<String> visibleColumnNames;

    private List<String> visibleColumnNamesOriginal;

    private Map<String, String> name2Alias;

    private Map<String, String> name2AliasOriginal;

    private Map<String,String> patterns = null;

    private Locale localeOfData;    
    /**
     * @see FeatureTableModel#FeatureTableModel(FeatureStore, FeatureQuery)
     */
    public ConfigurableFeatureTableModel(FeatureStore featureStore,
        FeatureQuery featureQuery) throws BaseException {
        super(featureStore, featureQuery);
        this.localeOfData = Locale.getDefault();
    }

    /**
     * @see FeatureTableModel#FeatureTableModel(FeatureStore, FeatureQuery, int)
     */
    public ConfigurableFeatureTableModel(FeatureStore featureStore,
        FeatureQuery featureQuery, int pageSize) throws BaseException {
        super(featureStore, featureQuery, pageSize);
    }

    @Override
    public int getColumnCount() {
        return visibleColumnNames.size();
    }

    public int getOriginalColumnCount() {
        return super.getColumnCount();
    }

    @Override
    public String getColumnName(int column) {
        int originalIndex = getOriginalColumnIndex(column);
        return getAliasForColumn(getOriginalColumnName(originalIndex));
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);
        return super.getColumnClass(originalIndex);
    }

    @Override
    public FeatureAttributeDescriptor getDescriptorForColumn(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);
        return super.getDescriptorForColumn(originalIndex);
    }

    /**
     * Returns the original name of the column, ignoring the alias.
     * 
     * @param column
     *            the original index of the column
     * @return the original column name
     */
    public String getOriginalColumnName(int column) {
        return super.getColumnName(column);
    }

    /**
     * Sets the visibility of a table column.
     * 
     * @param columnIndex
     *            the index of the column to update
     * @param visible
     *            if the column will be visible or not
     */
    public void setVisible(String name, boolean visible) {
        // If we don't have already the column as visible,
        // add to the list, without order, and recreate
        // the visible columns list in the original order
        if (!columnNames.contains(name)) {
            throw new InvalidParameterException(name); // FIXME
        }
        if (visible && !visibleColumnNames.contains(name)) {
            visibleColumnNames.add(name);
            setVisibleColumns(visibleColumnNames);
        } else {
            visibleColumnNames.remove(name);
            setVisibleColumns(visibleColumnNames);
            fireTableStructureChanged();
        }

    }

    @Override
    public void setFeatureType(FeatureType featureType) {
        // Check if there is a new column name
        List<String> newColumns = new ArrayList<String>();
        List<String> renamedColumnsNewName = new ArrayList<String>();
        
        @SuppressWarnings("unchecked")
        Iterator<FeatureAttributeDescriptor> attrIter = featureType.iterator();
        FeatureAttributeDescriptor fad = null;
        EditableFeatureAttributeDescriptor efad = null;
        
        String colName;
        while (attrIter.hasNext()) {
            fad = attrIter.next();
            colName = fad.getName();
            if (!columnNames.contains(colName)) {
                if (fad instanceof EditableFeatureAttributeDescriptor) {
                    efad = (EditableFeatureAttributeDescriptor) fad; 
                    /*
                     * If editable att descriptor,
                     * check original name
                     */
                    if (efad.getOriginalName() != null) {
                        if (!columnNames.contains(efad.getOriginalName())) {
                            /*
                             * Check with original name but add current name
                             */
                            newColumns.add(colName);
                        } else {
                            /*
                             * List of new names of renamed columns
                             */
                            renamedColumnsNewName.add(colName);
                        }
                    } else {
                        newColumns.add(colName);
                    }
                } else {
                    newColumns.add(colName);
                }
            }
        }

        // Update column names
        columnNames.clear();
        @SuppressWarnings("unchecked")
        Iterator<FeatureAttributeDescriptor> visibleAttrIter =
            featureType.iterator();
        while (visibleAttrIter.hasNext()) {
            fad = visibleAttrIter.next();
            colName = fad.getName();
            columnNames.add(colName);
            //If the column is added has to be visible
            if (!visibleColumnNames.contains(colName)) {

                if (((newColumns.contains(colName)
                    || renamedColumnsNewName.contains(colName)))
                    &&
                    fad.getType() != DataTypes.GEOMETRY) {
                    // Add new columns and renamed
                    visibleColumnNames.add(colName);
                    visibleColumnNamesOriginal.add(colName);
                }
                /*
                if (renamedColumnsNewName.contains(colName)) {
                    // Add renamed
                    insertWhereOldName(visibleColumnNames, colName, fad);
                    insertWhereOldName(visibleColumnNamesOriginal, colName, fad);
                }
                */
            }
        }

        // remove from visible columns removed columns
        visibleColumnNames = intersectKeepOrder(columnNames, visibleColumnNames);
        // instead of: visibleColumnNames.retainAll(columnNames);

        visibleColumnNamesOriginal = intersectKeepOrder(columnNames, visibleColumnNamesOriginal);
        // instead of: visibleColumnNamesOriginal.retainAll(columnNames);

        // remove from alias map removed columns
        name2Alias.keySet().retainAll(columnNames);
        name2AliasOriginal.keySet().retainAll(columnNames);

        super.setFeatureType(featureType);

    }

    /**
     * keeps order of first parameter
     * 
     * @param lista
     * @param listb
     * @return
     */
    private List<String> intersectKeepOrder(List<String> lista, List<String> listb) {
        
        List<String> resp = new ArrayList<String>();
        resp.addAll(lista);
        resp.retainAll(listb);
        return resp;
    }

    private void insertWhereOldNamee(
        List<String> str_list,
        String str,
        FeatureAttributeDescriptor fad) {
        
        if (fad instanceof EditableFeatureAttributeDescriptor) {
            EditableFeatureAttributeDescriptor efad =
                (EditableFeatureAttributeDescriptor) fad;
            if (efad.getOriginalName() != null) {
                int old_ind = str_list.indexOf(efad.getOriginalName());
                if (old_ind != -1) {
                    // Insert before old name
                    str_list.add(old_ind, str);
                } else {
                    // Insert anyway (add)
                    str_list.add(str);
                }
            } else {
                // Insert anyway (add)
                str_list.add(str);
            }
        } else {
            // Insert anyway (add)
            str_list.add(str);
        }
    }

    /**
     * Sets the current visible columns list, in the original order.
     * 
     * @param names
     *            the names of the columns to set as visible
     */
    public void setVisibleColumns(List<String> names) {
        // Recreate the visible column names list
        // to maintain the original order        
        visibleColumnNames = new ArrayList<String>(names.size());
        for (int i = 0; i < columnNames.size(); i++) {
            String columnName = columnNames.get(i);
            if (names.contains(columnName)) {
                visibleColumnNames.add(columnName);
            }
        }
        updatePaginHelperWithHiddenColums();
        fireTableStructureChanged();
    }

    protected String[] getHiddenColumnNames() {
        List<String> hiddenColumns = new ArrayList<String>();
        hiddenColumns.addAll(columnNames);
        
        for (int i = 0; i < visibleColumnNames.size(); i++) {
            String columnName = visibleColumnNames.get(i);
            hiddenColumns.remove(columnName);
        }
        if( hiddenColumns.size()<1 ) {
            return null;
        }
        return (String[]) hiddenColumns.toArray(new String[hiddenColumns.size()]);
    }
        
    /**
     * Changes all columns to be visible.
     */
    public void setAllVisible() {
        visibleColumnNames.clear();
        visibleColumnNames.addAll(columnNames);
        fireTableStructureChanged();
    }

    /**
     * Sets the alias for a column.
     * 
     * @param name
     *            the name of the column
     * @param alias
     *            the alias for the column
     */
    public void setAlias(String name, String alias) {
        name2Alias.put(name, alias);
        fireTableStructureChanged();
    }

    public void orderByColumn(String name, boolean ascending)
        throws BaseException {
        FeatureQueryOrder order = getHelper().getFeatureQuery().getOrder();
        if (order == null) {
            order = new FeatureQueryOrder();
            getHelper().getFeatureQuery().setOrder(order);
        }
        order.clear();
        order.add(name, ascending);
        getHelper().reload();
        fireTableChanged(new TableModelEvent(this, 0, getRowCount() - 1));
    }

    @Override
    protected void initialize() {
        super.initialize();

        initializeVisibleColumns();
        initializeAliases();
        updatePaginHelperWithHiddenColums();
    }

    /**
     * Returns if a column is visible.
     * 
     * @param name
     *            the name of the column
     * @return if the column is visible
     */
    public boolean isVisible(String name) {
        return visibleColumnNames.contains(name);
    }

    /**
     * Initializes the table name aliases.
     */
    private void initializeAliases() {
        int columns = super.getColumnCount();
        name2Alias = new HashMap<String, String>(columns);
        name2AliasOriginal = new HashMap<String, String>(columns);
    }

    /**
     * Initializes the table visible columns.
     */
    protected void initializeVisibleColumns() {
        int columns = super.getColumnCount();
        columnNames = new ArrayList<String>(columns);
        visibleColumnNames = new ArrayList<String>(columns);

        for (int i = 0; i < columns; i++) {
            String columnName = super.getColumnName(i);
            columnNames.add(columnName);

            // By default, geometry columns will not be visible
            FeatureAttributeDescriptor descriptor =
                super.getDescriptorForColumn(i);
            if (descriptor.getType() != DataTypes.GEOMETRY) {
                visibleColumnNames.add(columnName);
            }
        }
        
        visibleColumnNamesOriginal = new ArrayList<String>(visibleColumnNames);
    }

    /**
     * Returns the alias for the name of a column.
     * 
     * @param name
     *            of the column
     * @return the alias
     */
    protected String getAliasForColumn(String name) {
        String alias = name2Alias.get(name);
        return alias == null ? name : alias;
    }

    /**
     * Returns the original position of a column.
     * 
     * @param columnIndex
     *            the current visible column index
     * @return the original column index
     */
    public int getOriginalColumnIndex(int columnIndex) {
        String columnName = visibleColumnNames.get(columnIndex);
        return columnNames.indexOf(columnName);
    }

    @Override
    protected Object getFeatureValue(Feature feature, int columnIndex) {
        int realColumnIndex = getOriginalColumnIndex(columnIndex);
        return super.getFeatureValue(feature, realColumnIndex);
    }

    @Override
    protected EditableFeature setFeatureValue(Feature feature, int columnIndex,
        Object value) {
        int realColumnIndex = getOriginalColumnIndex(columnIndex);
        return super.setFeatureValue(feature, realColumnIndex, value);
    }
    
    /**
     * Make current changes in configuration (visible columns and aliases)
     * as definitive.
     */
    public void acceptChanges() {
    	visibleColumnNamesOriginal = new ArrayList<String>(visibleColumnNames);
    	name2AliasOriginal = new HashMap<String, String>(name2Alias);
    }
    
    /**
     * Cancel current changes in configuration (visible columns and aliases)
     * and return to previous status.
     */
    public void cancelChanges() {
    	visibleColumnNames = new ArrayList<String>(visibleColumnNamesOriginal);
    	name2Alias = new HashMap<String, String>(name2AliasOriginal);
    	fireTableStructureChanged();
    }

    protected void initializeFormattingPatterns() {
        this.patterns = new HashMap<String, String>();

        int columns = super.getColumnCount();

        for (int i = 0; i < columns; i++) {
            String columnName = super.getColumnName(i);
            initializeFormattingPattern(columnName);
        }
    } 
    
    protected void initializeFormattingPattern(String columnName) {
        FeatureAttributeDescriptor descriptor = 
                this.getFeatureType().getAttributeDescriptor(columnName);

        switch(descriptor.getDataType().getType()) {
        case DataTypes.BYTE:
        case DataTypes.INT:
        case DataTypes.LONG:
            String defaultIntegerPattern = "#,##0";
            this.patterns.put(columnName,defaultIntegerPattern);
            break;
        case DataTypes.DOUBLE:
            String defaultDoublePattern = "#,##0.0000000000";
            this.patterns.put(columnName,defaultDoublePattern);
            break;
        case DataTypes.FLOAT:
            String defaultFloatPattern = "#,##0.0000";
            this.patterns.put(columnName,defaultFloatPattern);
            break;
        case DataTypes.DATE:
            String defaultDatePattern = new SimpleDateFormat().toPattern();
            this.patterns.put(columnName,defaultDatePattern);
            break;
        default:
            this.patterns.put(columnName,null);
        }
    }
    
    public String getFormattingPattern(int column) {
        String columnName = this.visibleColumnNames.get(column);
        return this.getFormattingPattern(columnName);
    }
    
    public String getFormattingPattern(String columnName) {
        if( this.patterns==null ) {
            initializeFormattingPatterns();
        }
        String pattern = this.patterns.get(columnName);
        if( StringUtils.isBlank(pattern) ) {
            initializeFormattingPattern(columnName);
        }
        return pattern;
    }
    
    public void setFormattingPattern(String columnName, String pattern) {
        if( this.patterns==null ) {
            initializeFormattingPatterns();
        }
        this.patterns.put(columnName,pattern);
    }
    
    public Locale getLocaleOfData() {
        return this.localeOfData;
    }
    
    public void setLocaleOfData(Locale locale) {
        this.localeOfData = locale;
    }
    

}
