/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Behavior;

import java.awt.event.MouseEvent;

import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.gvsig.fmap.mapcontrol.tools.Listeners.ToolListener;



/**
 * <p>Behavior that permits user to select a point with a double click mouse action, on the associated
 *  <code>MapControl</code> using a {@link PointListener PointListener}.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class PointBehavior extends Behavior {
	/**
	 * Tool listener used to work with the <<code>MapControl</code> object.
	 *
	 * @see #getListener()
	 * @see #setListener(ToolListener)
	 */
	private PointListener listener;

	/**
	 * Flag that determines a double click user action.
	 */
	private boolean doubleClick=false;

	/**
 	 * <p>Creates a new behavior for selecting a point.</p>
 	 *
	 * @param l listener used to permit this object to work with the associated <code>MapControl</code>
	 */
	public PointBehavior(PointListener l, int mouseButton) {
            super(mouseButton);
            listener = l;
	}

	public PointBehavior(PointListener l) {
            this(l,BUTTON_LEFT);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.Behavior#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
                if( !isMyButton(e) ) {
                    return;
                }
		if (listener.cancelDrawing()) {
			getMapControl().cancelDrawing();
		}
		if (e.getClickCount()==2){
			doubleClick=true;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.Behavior#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) throws BehaviorException {
                if( !isMyButton(e) ) {
                    return;
                }
		PointEvent event = new PointEvent(e.getPoint(), e, this.getMapControl());
		listener.point(event);
		if (doubleClick){
			listener.pointDoubleClick(event);
			doubleClick=false;
		}
	}

	/**
	 * <p>Sets a tool listener to work with the <code>MapControl</code> using this behavior.</p>
	 *
	 * @param listener a <code>PointListener</code> object for this behavior
	 */
	public void setListener(ToolListener listener) {
		this.listener = (PointListener) listener;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.Behavior#getListener()
	 */
	public ToolListener getListener() {
		return listener;
	}
}
