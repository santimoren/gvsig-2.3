/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.util.HashMap;
import java.util.Map;

import javax.swing.table.TableModel;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTypesTablePanel;
import org.gvsig.tools.exception.BaseException;

/**
 * A model for the {@link FeatureTypesTablePanel} which unifies the access
 * to the different models needed by the components included into it.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class FeatureStoreModel {

    private final FeatureStore featureStore;
    private final FeatureQuery featureQuery;
    private final FeatureTypeListModel featureTypeListModel;
    private final Map<String, ConfigurableFeatureTableModel> type2TableModel =
        new HashMap<String, ConfigurableFeatureTableModel>();
    private String currentFeatureTypeId;

    /**
     * Constructor.
     * 
     * @param store
     *            the FeatureStore to get data from
     * @param query
     *            the FeatureQuery to use to get data
     * @throws DataException
     *             if there is an error getting the default {@link FeatureType}
     */
    public FeatureStoreModel(FeatureStore store, FeatureQuery query)
        throws DataException {
        this.featureStore = store;
        this.featureQuery = query;
        this.featureTypeListModel = new FeatureTypeListModel(store, query);

        this.currentFeatureTypeId =
            query.getFeatureTypeId() == null ? store.getDefaultFeatureType()
                .getId() : query.getFeatureTypeId();
    }

    /**
     * Constructor.
     * 
     * @param store
     *            the FeatureStore to get data from
     * @throws DataException
     *             if there is an error getting the default {@link FeatureType}
     */
    public FeatureStoreModel(FeatureStore store) throws DataException {
        this(store, null);
    }

    /**
     * Returns the {@link FeatureTypeListModel} to show a list of available
     * {@link FeatureType}s.
     * 
     * @return the {@link FeatureTypeListModel} to show a list of available
     *         {@link FeatureType}s
     */
    public FeatureTypeListModel getFeatureTypeListModel() {
        return featureTypeListModel;
    }

    /**
     * Returns the {@link TableModel} to use to show data of the current feature
     * type.
     * 
     * @return the TableModel to use
     */
    public ConfigurableFeatureTableModel getCurrentFeatureTableModel() {
        return getFeatureTableModel(this.currentFeatureTypeId);
    }

    /**
     * Returns the {@link TableModel} to use to show data of a
     * {@link FeatureType}.
     * 
     * @param type
     *            to use to filter the data
     * @return the TableModel to use
     */
    public ConfigurableFeatureTableModel getFeatureTableModel(FeatureType type) {
        return getFeatureTableModel(type.getId());
    }

    /**
     * Returns the {@link TableModel} to use to show data of a
     * {@link FeatureType}.
     * 
     * @param typeId
     *            the id of the {@link FeatureType} to use to filter the data
     * @return the TableModel to use
     */
    public ConfigurableFeatureTableModel getFeatureTableModel(String typeId) {
        ConfigurableFeatureTableModel tableModel = type2TableModel.get(typeId);
        if (tableModel == null) {
            try {
                tableModel =
                    new ConfigurableFeatureTableModel(featureStore,
                        featureQuery);
                type2TableModel.put(typeId, tableModel);
            } catch (BaseException e) {
                throw new RuntimeException("Error creating a table model", e);
            }
        }
        return tableModel;
    }

    /**
     * Sets the id of the current {@link FeatureType}.
     * 
     * @param currentFeatureTypeId
     *            the id of the current {@link FeatureType}
     */
    public void setCurrentFeatureTypeId(String currentFeatureTypeId) {
        this.currentFeatureTypeId = currentFeatureTypeId;
    }

    /**
     * Returns the id of the current {@link FeatureType}.
     * 
     * @return the id of the current {@link FeatureType}
     */
    public String getCurrentFeatureTypeId() {
        return currentFeatureTypeId;
    }

    /**
     * @return the featureQuery
     */
    public FeatureQuery getFeatureQuery() {
        return featureQuery;
    }

    /**
     * @return the featureStore
     */
    public FeatureStore getFeatureStore() {
        return featureStore;
    }
}
