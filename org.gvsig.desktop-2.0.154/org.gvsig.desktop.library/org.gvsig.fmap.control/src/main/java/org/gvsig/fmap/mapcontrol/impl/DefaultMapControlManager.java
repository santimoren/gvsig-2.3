/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.fmap.mapcontrol.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.tree.TreeModel;
import org.gvsig.fmap.mapcontrol.LayersTreeModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.CompoundLayersTreeModel;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlCreationException;
import org.gvsig.fmap.mapcontrol.MapControlCreationListener;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;
import org.gvsig.fmap.mapcontrol.swing.dynobject.impl.DefaultLayersDynObjectSetComponent;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultMapControlManager implements MapControlManager{
	private static final Logger logger = LoggerFactory.getLogger(MapControlManager.class);
	private static final String MAPCONTROL_MANAGER_EXTENSION_POINT = "MapControlManagerExtensionPoint";
	private static final String DEFAULT_MAPCONTROLMANAGER_NAME = null;
	private static final String SNAPPING_EXTENSION_POINT = "Snapper";

	private ExtensionPointManager extensionPoints = ToolsLocator.getExtensionPointManager();
	private int snappingTolerance = 4;
	private ISymbol selectionSymbol = null;
	private ISymbol axisReferencesSymbol = null;
	private ISymbol geometrySelectionSymbol = null;
	private ISymbol handlerSymbol = null;
	private static MapContextManager mapContextManager = MapContextLocator
	.getMapContextManager();
	private Preferences prefs = Preferences.userRoot().node( "cadtooladapter" );
	private static Preferences prefSnappers = Preferences.userRoot().node("snappers");
	private List<ISnapper> snappers = null;
        private Set<MapControlCreationListener> mapControlCreationListeners = new LinkedHashSet<MapControlCreationListener>();


	public DefaultMapControlManager() {
		super();
		snappers = new ArrayList<ISnapper>();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#createDefaultMapControlDrawer()
	 */
	public MapControlDrawer createDefaultMapControlDrawer() throws MapControlCreationException {
		ExtensionPoint ep = extensionPoints.add(MAPCONTROL_MANAGER_EXTENSION_POINT);
		try {
			return (MapControlDrawer)ep.create(DEFAULT_MAPCONTROLMANAGER_NAME);
		} catch (Exception e) {
			throw new MapControlCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#createMapControlDrawer(java.lang.String)
	 */
	public MapControlDrawer createMapControlDrawer(String name) throws MapControlCreationException {
		ExtensionPoint ep = extensionPoints.add(MAPCONTROL_MANAGER_EXTENSION_POINT);
		try {
			return (MapControlDrawer)ep.create(name);
		} catch (Exception e) {
			throw new MapControlCreationException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#registerDefaultMapControlDrawer(java.lang.Class)
	 */
	public void registerDefaultMapControlDrawer(Class mapControlDrawerClass) {
		if (!MapControlDrawer.class.isAssignableFrom(mapControlDrawerClass)) {
			throw new IllegalArgumentException(mapControlDrawerClass.getName()
					+ " must implement the MapControlDrawer interface");
		}

		ExtensionPoint extensionPoint = extensionPoints.add(MAPCONTROL_MANAGER_EXTENSION_POINT, "");
		extensionPoint.append(DEFAULT_MAPCONTROLMANAGER_NAME, "Default MapControl", mapControlDrawerClass);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#registerMapControlDrawer(int, java.lang.Class)
	 */
	public void registerMapControlDrawer(String name,
			Class mapControlDrawerClass) {

		if (!MapControlDrawer.class.isAssignableFrom(mapControlDrawerClass)) {
			throw new IllegalArgumentException(mapControlDrawerClass.getName()
					+ " must implement the MapControlDrawer interface");
		}

		ExtensionPoint extensionPoint = extensionPoints.add(MAPCONTROL_MANAGER_EXTENSION_POINT, "");
		extensionPoint.append(name, "Default MapControl", mapControlDrawerClass);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getSnappingTolerance()
	 */
	public int getTolerance() {
		return snappingTolerance;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#setSnappingTolerance(int)
	 */
	public void setTolerance(int tolerance) {
		snappingTolerance = tolerance;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#clearSnappers()
	 */
	public void clearSnappers() {
		snappers.clear();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getAxisReferenceSymbol()
	 */
	public ISymbol getAxisReferenceSymbol() {
		if (axisReferencesSymbol == null){
			axisReferencesSymbol =
					mapContextManager.getSymbolManager()
			.createSymbol(Geometry.TYPES.GEOMETRY,
					new Color(100, 100, 100, 100));
		}
		return axisReferencesSymbol;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getGeometrySelectionSymbol()
	 */
	public ISymbol getGeometrySelectionSymbol() {
		if (geometrySelectionSymbol == null){
			geometrySelectionSymbol =
					mapContextManager.getSymbolManager()
			.createSymbol(Geometry.TYPES.GEOMETRY, Color.RED);
		}
		return geometrySelectionSymbol;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getHandlerSymbol()
	 */
	public ISymbol getHandlerSymbol() {
		if (handlerSymbol == null){
			handlerSymbol =
					mapContextManager.getSymbolManager().createSymbol(
					Geometry.TYPES.GEOMETRY, Color.ORANGE);
		}
		return handlerSymbol;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getSelectionSymbol()
	 */
	public ISymbol getSelectionSymbol() {
		if (selectionSymbol == null){
			selectionSymbol =
					mapContextManager.getSymbolManager().createSymbol(
					Geometry.TYPES.GEOMETRY, new Color(255, 0, 0, 100));
		}
		return selectionSymbol;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#setAxisReferenceSymbol(org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void setAxisReferenceSymbol(ISymbol axisReferencesSymbol) {
		this.axisReferencesSymbol = axisReferencesSymbol;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#setGeometrySelectionSymbol(org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void setGeometrySelectionSymbol(ISymbol geometrySelectionSymbol) {
		this.geometrySelectionSymbol = geometrySelectionSymbol;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#setHandlerSymbol(org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void setHandlerSymbol(ISymbol handlerSymbol) {
		this.handlerSymbol = handlerSymbol;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#setSelectionSymbol(org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void setSelectionSymbol(ISymbol selectionSymbol) {
		this.selectionSymbol = selectionSymbol;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#addSnapper(org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper)
	 */
	public void registerSnapper(String name, Class snapperClass) {
		if (!ISnapper.class.isAssignableFrom(snapperClass)) {
			throw new IllegalArgumentException(snapperClass.getName()
					+ " must implement the ISnapper interface");
		}

		ExtensionPoint extensionPoint = extensionPoints.add(SNAPPING_EXTENSION_POINT, "");
		Extension extension = extensionPoint.append(name, "", snapperClass);

		ISnapper snapper;
		try {
			snapper = (ISnapper)extension.create();
			snappers.add(snapper);
            String nameClass=snapper.getClass().getName();
	        nameClass=nameClass.substring(nameClass.lastIndexOf('.'));
	        boolean select = prefSnappers.getBoolean("snapper_activated" + nameClass, false);
	        int priority = prefs.getInt("snapper_priority" + nameClass,3);
	        snapper.setPriority(priority);
	        if (select){
	          	snapper.setEnabled(select);
	        }

		} catch (Exception e) {
			logger.error("It is not possible to create the snapper");
		}
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getSnapperAt(int)
	 */
	public ISnapper getSnapperAt(int index) {
		return snappers.get(index);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getSnapperCount()
	 */
	public int getSnapperCount() {
		return snappers.size();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#disableSnapping()
	 */
	public void disableSnapping() {
		snappers.clear();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#enableSnapping()
	 */
	public void enableSnapping() {
		ExtensionPointManager extensionPoints = ToolsLocator.getExtensionPointManager();
		ExtensionPoint ep = extensionPoints.get(SNAPPING_EXTENSION_POINT);
		Iterator iterator = ep.iterator();

		while (iterator.hasNext()) {
			try {
				Extension obj= (Extension)iterator.next();
				ISnapper snapper = (ISnapper) ep.create(obj.getName());
				snappers.add(snapper);
			} catch (Exception e) {
				logger.error("Creating a snapper", e);
			}
		}

		for (int n = 0; n < getSnapperCount(); n++) {
			ISnapper snp = getSnapperAt(n);
			String nameClass = snp.getClass().getName();
			nameClass = nameClass.substring(nameClass.lastIndexOf('.'));
			boolean select = prefs.getBoolean("snapper_activated" + nameClass, false);
			if (select) {
				snp.setEnabled(select);
			}
			int priority = prefs.getInt("snapper_priority" + nameClass, 3);
			snp.setPriority(priority);
		}

	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlManager#getEditionPreferences()
	 */
	public Preferences getEditionPreferences() {
		return prefs;
	}

	public MapControl createJMapControlPanel(MapContext mapContext) throws MapControlCreationException {
		MapControl mapControl = new MapControl(mapContext);
		mapControl.setMapControlDrawer(this.createDefaultMapControlDrawer());
                for( MapControlCreationListener listener : this.mapControlCreationListeners ) {
                    if( listener!=null ) {
                        mapControl = listener.mapControlCreated(mapControl);
                    }
                }
		return mapControl;
	}

        public void addMapControlCreationListener(MapControlCreationListener listener) {
            this.mapControlCreationListeners.add(listener);
        }

    public LayersDynObjectSetComponent createLayersDynObjectSetComponent(
        Map<String, DynObjectSet> layerName2InfoByPoint) {
        return createLayersDynObjectSetComponent(layerName2InfoByPoint, false);
    }

    public LayersDynObjectSetComponent createLayersDynObjectSetComponent(
        Map<String, DynObjectSet> layerName2InfoByPoint, boolean writable) {
        return new DefaultLayersDynObjectSetComponent(layerName2InfoByPoint,
            writable);
    }
    
    public TreeModel createLayersTreeModel(MapContext mapContext) {
        return new LayersTreeModel(mapContext.getLayers());
    }
    
    public TreeModel createLayersTreeModel(FLayers layers) {
        return new LayersTreeModel(layers);
    }
    
    public TreeModel createCompoundLayersTreeModel() {
        return new CompoundLayersTreeModel();
    }
    
    
}
