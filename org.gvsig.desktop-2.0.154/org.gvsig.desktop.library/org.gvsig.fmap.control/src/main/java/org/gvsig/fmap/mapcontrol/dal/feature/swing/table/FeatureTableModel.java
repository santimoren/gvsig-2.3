/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a JTable TableModel for a FeatureCollection}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.ComplexNotification;
import org.gvsig.tools.observer.ComplexObserver;
import org.gvsig.tools.observer.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TableModel to access data of Features.
 *
 * This table model can't handle a FeatureSet with more than Integer.MAX_VALUE
 * elements. In that case, only the first Integer.MAX_VALUE elements will be
 * shown.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class FeatureTableModel extends AbstractTableModel implements ComplexObserver {


	private static final Logger logger = LoggerFactory
			.getLogger(FeatureTableModel.class);

    private static final long serialVersionUID = -2488157521902851301L;

    private FeaturePagingHelper helper;

    /** Used to know if a modification in the FeatureStore is created by us. */
    private EditableFeature editableFeature;

    private boolean selectionLocked=false;

    /**
     * Constructs a TableModel from the features of a FeatureStore, with the
     * default page size.
     *
     * @param featureStore
     *            to extract the features from
     * @param featureQuery
     *            the query to get the features from the store
     * @throws BaseException
     *             if there is an error reading data from the FeatureStore
     */
    public FeatureTableModel(FeatureStore featureStore,
        FeatureQuery featureQuery) throws BaseException {
        this(featureStore, featureQuery, FeaturePagingHelper.DEFAULT_PAGE_SIZE);
    }

    /**
     * Constructs a TableModel from the features of a FeatureStore, with the
     * default page size.
     *
     * @param featureStore
     *            to extract the features from
     * @param featureQuery
     *            the query to get the features from the store
     * @param pageSize
     *            the number of elements per page data
     * @throws BaseException
     *             if there is an error reading data from the FeatureStore
     */
    public FeatureTableModel(FeatureStore featureStore,
        FeatureQuery featureQuery, int pageSize) throws BaseException {
        this(DALLocator.getDataManager().createFeaturePagingHelper(
            featureStore, featureQuery, pageSize));
    }

    /**
     * Constructs a TableModel from a FeatureCollection and a Paging helper.
     *
     * @param featureCollection
     *            to extract data from
     * @param helper
     *            the paging helper
     * @throws DataException
     *             if there is an error reading data from the FeatureStore
     */
    protected FeatureTableModel(FeaturePagingHelper helper) {
        this.helper = helper;
        initialize();
    }

    public int getColumnCount() {
        // Return the number of fields of the Features
        FeatureType featureType = getFeatureType();
        return featureType.size();
    }

    public int getRowCount() {
        // Return the total size of the collection
        // If the size is bigger than INTEGER.MAX_VALUE, return that instead
    	try {
	        long totalSize = getHelper().getTotalSize();
	        if (totalSize > Integer.MAX_VALUE) {
	            return Integer.MAX_VALUE;
	        } else {
	            return (int) totalSize;
	        }
    	} catch (ConcurrentDataModificationException e) {
			logger.debug("Error while getting the total size of the set", e);
			return 0;
		}
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        // Get the Feature at row "rowIndex", and return the value of the
        // attribute at "columnIndex"
        Feature feature = getFeatureAt(rowIndex);
        return feature == null ? null : getFeatureValue(feature, columnIndex);
    }

    /**
     * Returns the value for a row position.
     *
     * @param rowIndex
     *            the row position
     * @return the Feature
     */
    public Feature getFeatureAt(int rowIndex) {
        try {
            return getHelper().getFeatureAt(rowIndex);
        } catch (BaseException ex) {
            throw new GetFeatureAtException(rowIndex, ex);
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        // Return the class of the FeatureAttributeDescriptor for the value
        FeatureAttributeDescriptor attributeDesc =
            internalGetFeatureDescriptorForColumn(columnIndex);
        if (attributeDesc == null) {
        	return super.getColumnClass(columnIndex);
        }
        Class<?> clazz = attributeDesc.getObjectClass();
        return (clazz == null ? super.getColumnClass(columnIndex) : clazz);
    }

    public String getColumnName(int column) {
        // Return the Feature attribute name
        FeatureAttributeDescriptor attributeDesc =
            internalGetFeatureDescriptorForColumn(column);
        return attributeDesc.getName();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (getFeatureStore().isEditing()) {
            FeatureAttributeDescriptor attributeDesc =
                internalGetFeatureDescriptorForColumn(columnIndex);
            return !attributeDesc.isReadOnly();
        }

        return false;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        // Get the feature at rowIndex
        Feature feature = getFeatureAt(rowIndex);
        // Only set the value if the feature exists
        if (feature != null) {
            // We only need to update if the value to set is not equal to the
            // current value
            Object currentValue = getFeatureValue(feature, columnIndex);
            if (value != currentValue
                && (value == null || !value.equals(currentValue))) {
                try {
                    // Store the editable feature to ignore the related store
                    // change notification
                    editableFeature =
                        setFeatureValue(feature, columnIndex, value);
                    EditingNotificationManager editingNotificationManager = DALSwingLocator.getEditingNotificationManager();
                    EditingNotification notification = editingNotificationManager.notifyObservers(
                            this,
                            EditingNotification.BEFORE_UPDATE_FEATURE,
                            null,
                            this.getHelper().getFeatureStore(),
                            editableFeature);
                    if( notification.isCanceled() ) {
                        return;
                    }
                    if( notification.shouldValidateTheFeature() ) {
                        if ( !editingNotificationManager.validateFeature(feature) ) {
                            return;
                        }
                    }
                    this.getHelper().update(editableFeature);
                    // We'll have already received the event, so we can forget
                    // about it
                    getHelper().reloadCurrentPage();
                    fireTableCellUpdated(rowIndex, columnIndex);

                    editingNotificationManager.notifyObservers(
                            this,
                            EditingNotification.AFTER_UPDATE_FEATURE,
                            null,
                            this.getHelper().getFeatureStore(),
                            editableFeature);
                    editableFeature = null;

                } catch (BaseException ex) {
                    throw new SetFeatureValueException(rowIndex, columnIndex,
                        value, ex);
                } finally {
                    // Just in case
                    editableFeature = null;
                }
            }
        }
    }

    /**
     * Returns a reference to the Paging Helper used to load the data from the
     * DataStore.
     *
     * @return the paging helper
     */
    public FeaturePagingHelper getHelper() {
        return helper;
    }

    /**
     * Sets the FeatureType to show in the table. Used for FeatureStores with
     * many simultaneous FeatureTypes supported. Will cause a reload of the
     * current data.
     *
     * @param featureType
     *            the FeatureType of the Features
     * @throws DataException
     *             if there is an error loading the data
     */
    public void setFeatureType(FeatureType featureType) {
        getFeatureQuery().setFeatureType(featureType);
        reloadFeatures();
        //Selection must be locked to avoid losing it when the table is refreshed
        selectionLocked=true;
        //The table is refreshed
        try {
            fireTableStructureChanged();
        } catch (Exception e) {
            logger.warn("Couldn't reload changed table");
        }finally{
            //The locked selection is unlocked.
            selectionLocked=false;
        }
    }

    /**
     * Sets that the selected Features get returned first.
     */
    public void setSelectionUp(boolean selectionUp) {
        getHelper().setSelectionUp(selectionUp);
        fireTableChanged(new TableModelEvent(this, 0, getRowCount() - 1));
    }

    private class DelayAction extends Timer implements ActionListener, Runnable {
        private static final int STATE_NONE = 0;
        private static final int STATE_NEED_RELOADALL = 1;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED = 2;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_UPDATED = 4;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED = 8;
        private static final int STATE_NEED_RELOAD_FEATURE_TYPE = 16;
        private static final int STATE_NEED_SELECTION_UP = 32;
        private static final int STATE_NEED_RELOAD_ALL_FEATURES=64;

        private int state = STATE_NONE;
        private Feature feature;
        private FeatureType featureType;
        private boolean isSelecctionUp;

        public DelayAction() {
            super(1000,null);
            this.setRepeats(false);
            this.reset();
            this.addActionListener(this);
        }

        public void reset() {
            this.state = STATE_NONE;
            this.isSelecctionUp = false;
            this.feature = null;
            this.featureType = null;
        }

        public void actionPerformed(ActionEvent ae) {
            this.run();
        }

        public void run() {
            if( !SwingUtilities.isEventDispatchThread() ) {
                SwingUtilities.invokeLater(this);
                return;
            }
            this.stop();
            logger.info("DelayAction.run["+this.state+"] begin");
            switch(this.state) {
            case STATE_NEED_RELOADALL:
                reloadAll();
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                reloadIfFeatureCountChanged(feature);
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                reloadIfFeatureUpdated(feature);
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                reloadIfTypeChanged(featureType);
                break;
            case STATE_NEED_RELOAD_FEATURE_TYPE:
                reloadFeatureType();
                updatePaginHelperWithHiddenColums();
                break;
            case STATE_NEED_RELOAD_ALL_FEATURES:
                reloadFeatures();
                fireTableChanged(new TableModelEvent(FeatureTableModel.this, 0, getRowCount()));
                break;
            case STATE_NEED_SELECTION_UP:
            case STATE_NONE:
            default:
                break;
            }
            if( isSelecctionUp ) {
                getHelper().setSelectionUp(true);
            }
            this.reset();
            logger.info("DelayAction.run["+this.state+"] end");
        }

        public void nextState(int nextstate) {
            this.nextState(nextstate, null, null);
        }

        public void nextState(int nextstate, Feature feature) {
            this.nextState(nextstate, feature, null);
        }

        public void nextState(int nextstate, FeatureType featureType) {
            this.nextState(nextstate, null, featureType);
        }

        public void nextState(int nextstate, Feature feature, FeatureType featureType) {
            this.feature = feature;
            this.featureType = featureType;
            switch(nextstate) {
            case STATE_NEED_RELOADALL:
            case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
            case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                //case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                    this.state = STATE_NEED_RELOADALL;
                    break;
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                    this.state = STATE_NEED_RELOAD_FEATURE_TYPE;
                    break;
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                    this.state=STATE_NEED_RELOAD_ALL_FEATURES;
                    break;
                case STATE_NEED_SELECTION_UP:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    break;
                }
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
            case STATE_NEED_RELOAD_FEATURE_TYPE:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                    this.state = STATE_NEED_RELOAD_FEATURE_TYPE;
                    break;
                case STATE_NEED_SELECTION_UP:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    break;
                }
                break;
            case STATE_NEED_SELECTION_UP:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                case STATE_NEED_SELECTION_UP:
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                }
                break;
            case STATE_NONE:
            default:
                this.state = STATE_NONE;
                break;
            }
            if( this.state != STATE_NONE ) {
                this.start();
            }
        }

    }

    private DelayAction delayAction = new DelayAction();

    public void update(final Observable observable, final Object notification) {
        if (notification instanceof ComplexNotification) {
            // A lot of things might have happened in the store, so don't
            // bother looking into each notification.
            this.delayAction.nextState(DelayAction.STATE_NEED_RELOADALL);
//            reloadAll();
        } else if (observable.equals(getFeatureStore())
                && notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification fsNotification
                    = (FeatureStoreNotification) notification;
            String type = fsNotification.getType();

            // If there are new, updated or deleted features
            // reload the table data
            if (FeatureStoreNotification.AFTER_DELETE.equals(type)
                    || FeatureStoreNotification.AFTER_INSERT.equals(type)) {
//                reloadIfFeatureCountChanged(fsNotification.getFeature());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED, fsNotification.getFeature());

            } else if (FeatureStoreNotification.AFTER_UPDATE.equals(type)) {
//                reloadIfFeatureUpdated(fsNotification.getFeature());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_UPDATED, fsNotification.getFeature());

            } else if (FeatureStoreNotification.AFTER_UPDATE_TYPE.equals(type)) {
//                reloadIfTypeChanged(fsNotification.getFeatureType());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED, fsNotification.getFeatureType());

            } else if (FeatureStoreNotification.TRANSFORM_CHANGE.equals(type)
                    || FeatureStoreNotification.AFTER_UNDO.equals(type)
                    || FeatureStoreNotification.AFTER_REDO.equals(type)
                    || FeatureStoreNotification.AFTER_REFRESH.equals(type))  {
//                reloadAll();
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOADALL);

            } else if (FeatureStoreNotification.AFTER_FINISHEDITING.equals(type)
                    || FeatureStoreNotification.AFTER_STARTEDITING.equals(type)
                    || FeatureStoreNotification.AFTER_CANCELEDITING.equals(type)) {
                /*
                No tengo nada claro por que es necesario llamar al reloadFeatureType
                pero si no se incluye hay problemas si durante la edicion se a�aden
                campos a la tabla. Sin esto, al cerrar la edicion, los campos a�adidos
                desaparecen de la tabla aunque estan en el fichero.
                Ver ticket #2434 https://devel.gvsig.org/redmine/issues/2434
                */
//                reloadFeatureType();
//                updatePaginHelperWithHiddenColums();
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_FEATURE_TYPE, fsNotification.getFeatureType());
            } else if (FeatureStoreNotification.SELECTION_CHANGE.equals(type)) {
                if( this.getHelper().isSelectionUp() ) {
                    getHelper().setSelectionUp(true);
                    this.delayAction.nextState(DelayAction.STATE_NEED_SELECTION_UP);
                }
            }
        }
    }

    protected void updatePaginHelperWithHiddenColums() {
        FeatureQuery query = this.getHelper().getFeatureQuery();
        if (this.getHelper().getFeatureStore().isEditing()) {
            if (query.hasConstantsAttributeNames()) {
                query.clearConstantsAttributeNames();
            }
        } else {
            query.setConstantsAttributeNames(this.getHiddenColumnNames());
        }
        try {
            this.getHelper().reload();
        } catch (BaseException ex) {
            logger.warn("Can't reload paging-helper.", ex);
        }
    }

    protected String[] getHiddenColumnNames() {
        return null;
    }

    /**
     * Returns the FeatureStore of the Collection.
     *
     * @return the FeatureStore
     */
    public FeatureStore getFeatureStore() {
        return getHelper().getFeatureStore();
    }

    /**
     * Returns the descriptor of a Feature attribute for a table column.
     *
     * @param columnIndex
     *            the column index
     */
    public FeatureAttributeDescriptor getDescriptorForColumn(int columnIndex) {
        return internalGetFeatureDescriptorForColumn(columnIndex);
    }

    /**
     * @param columnIndex
     * @return
     */
	protected FeatureAttributeDescriptor internalGetFeatureDescriptorForColumn(
			int columnIndex) {
		FeatureType featureType = getFeatureType();
		return featureType == null ? null : featureType
				.getAttributeDescriptor(columnIndex);
	}

    /**
     * Initialize the TableModel
     */
    protected void initialize() {
        // Add as observable to the FeatureStore, to detect data and selection
        // changes
        helper.getFeatureStore().addObserver(this);
    }

    /**
     * Returns the value of a Feature attribute, at the given position.
     *
     * @param feature
     *            the feature to get the value from
     * @param columnIndex
     *            the Feature attribute position
     * @return the value
     */
    protected Object getFeatureValue(Feature feature, int columnIndex) {
        return feature.get(columnIndex);
    }

    /**
     * Sets the value of an Feature attribute at the given position.
     *
     * @param feature
     *            the feature to update
     * @param columnIndex
     *            the attribute position
     * @param value
     *            the value to set
     * @throws IsNotFeatureSettingException
     *             if there is an error setting the value
     */
    protected EditableFeature setFeatureValue(Feature feature, int columnIndex,
        Object value) {
        EditableFeature editableFeature = feature.getEditable();
        editableFeature.set(columnIndex, value);
        return editableFeature;
    }

    /**
     * Returns the FeatureQuery used to get the Features.
     *
     * @return the FeatureQuery
     */
    public FeatureQuery getFeatureQuery() {
        return getHelper().getFeatureQuery();
    }

    /**
     * Returns the type of the features.
     */
    protected FeatureType getFeatureType() {
        return getHelper().getFeatureType();
    }

    /**
     * Reloads the table data if a feature has been changed, not through the
     * table.
     */
    private void reloadIfFeatureCountChanged(Feature feature) {
        // Is any data is changed in the FeatureStore, notify the model
        // listeners. Ignore the case where the updated feature is
        // changed through us.
        if (editableFeature == null || !editableFeature.equals(feature)) {
            reloadFeatures();
            //Selection must be locked to avoid losing it when the table is refreshed
            selectionLocked=true;
            //The table is refreshed
            try {
                fireTableDataChanged();
            } catch (Exception e) {
                logger.warn("Couldn't reload changed table");
            }finally{
                //The locked selection is unlocked.
                selectionLocked=false;
            }
        }
    }

    private void reloadIfFeatureUpdated(Feature feature) {
        // Is any data is changed in the FeatureStore, notify the model
        // listeners. Ignore the case where the updated feature is
        // changed through us.
        if (editableFeature == null || !editableFeature.equals(feature)) {
            reloadFeatures();
            fireTableChanged(new TableModelEvent(this, 0, getRowCount()));
        }
    }

    /**
     * Reloads data and structure if the {@link FeatureType} of the features
     * being shown has changed.
     */
    private void reloadIfTypeChanged(FeatureType updatedType) {
        // If the updated featured type is the one currently being
        // shown, reload the table.
        if (updatedType != null
            && updatedType.getId().equals(getFeatureType().getId())) {
            setFeatureType(updatedType);
        }
    }

    private void reloadAll() {
    	reloadFeatureType();
    }

    private void reloadFeatureType() {
        try {
            setFeatureType(getHelper().getFeatureStore().getFeatureType(
                getHelper().getFeatureType().getId()));
        } catch (DataException e) {
            throw new FeaturesDataReloadException(e);
        }
    }

    /**
     * Reloads the features shown on the table.
     */
    private void reloadFeatures() {
        try {
            getHelper().reload();
        } catch (BaseException ex) {
            throw new FeaturesDataReloadException(ex);
        }
    }

    /**
     * Returns true if selection must not be changed.
     * @return
     */
    public boolean isSelectionLocked() {
        return selectionLocked;
    }

}
