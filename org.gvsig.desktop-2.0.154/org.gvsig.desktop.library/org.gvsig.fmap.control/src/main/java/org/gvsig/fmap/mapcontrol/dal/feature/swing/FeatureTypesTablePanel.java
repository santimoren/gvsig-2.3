/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.ConfigurableFeatureTableModel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureStoreModel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureTypeList;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.SelectedFeatureTypeChangeListener;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel to show a table of Features. It is able to take into account the
 * availability of many FeatureTypes, and if it is the case, allows the user to
 * select the {@link FeatureType} of the {@link Feature}s to show.
 * 
 * @author 2005- Vicente Caballero
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class FeatureTypesTablePanel extends JPanel implements
    SelectedFeatureTypeChangeListener {

    private static final long serialVersionUID = 5857146295213412304L;
    private static final Logger LOG = LoggerFactory
        .getLogger(FeatureTypesTablePanel.class);
    private FeatureTablePanel tablePanel;
    private FeatureTypeList typesControl;
    private JSplitPane jSplitPane = null;
    private final FeatureStoreModel model;

    /**
     * Constructs a Panel to show a table with the features of a FeatureStore.
     * 
     * @param featureStore
     *            to extract the features from
     * @throws BaseException
     *             if there is an error reading data from the FeatureStore
     */
    public FeatureTypesTablePanel(FeatureStore featureStore)
        throws BaseException {
        this(featureStore, true);
    }

    /**
     * Constructs a Panel to show a table with the features of a FeatureStore.
     * 
     * @param featureStore
     *            to extract the features from
     * @param isDoubleBuffered
     *            a boolean, true for double-buffering, which uses additional
     *            memory space to achieve fast, flicker-free updates
     * @throws BaseException
     *             if there is an error reading data from the FeatureStore
     */
    public FeatureTypesTablePanel(FeatureStore featureStore,
        boolean isDoubleBuffered) throws BaseException {
        this(featureStore, null, isDoubleBuffered);
    }

    /**
     * @throws BaseException
     * 
     */
    public FeatureTypesTablePanel(FeatureStore featureStore,
        FeatureQuery featureQuery) throws BaseException {
        this(featureStore, featureQuery, true);
    }

    /**
     * @param isDoubleBuffered
     * @throws BaseException
     */
    public FeatureTypesTablePanel(FeatureStore featureStore,
        FeatureQuery featureQuery, boolean isDoubleBuffered)
        throws BaseException {
        this(new FeatureStoreModel(featureStore, featureQuery),
            isDoubleBuffered);
    }

    public FeatureTypesTablePanel(FeatureStoreModel model) throws DataException {
        this(model, true);
    }

    public FeatureTypesTablePanel(FeatureStoreModel model,
        boolean isDoubleBuffered) throws DataException {
        super(isDoubleBuffered);
        this.model = model;
        typesControl =
            new FeatureTypeList(model.getFeatureStore(),
                model.getFeatureQuery());
        this.initializeUI();
        // Add a listener to change the selection
        typesControl.addSelectedFeatureTypeChangeListener(this);
    }

    private boolean hasManyFeatureTypes() throws DataException {
        return typesControl.getFeatureTypesSize() > 1;
    }

    /**
     * @return
     * @see org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTablePanel#createConfigurationPanel()
     */
    public JPanel createConfigurationPanel() {
        return tablePanel.createConfigurationPanel();
    }

    public void change(FeatureStore featureStore, FeatureType featureType) {
        if (featureType != null) {
            showFeatureTypeData(featureType);
        }
    }

    private void showFeatureTypeData(FeatureType featureType) {
        showFeatureTypeData(featureType.getId());
    }

    private void showFeatureTypeData(String typeId) {
        this.model.setCurrentFeatureTypeId(typeId);
        try {
            tablePanel =
                new FeatureTablePanel(this.model.getCurrentFeatureTableModel());
        } catch (BaseException e) {
            throw new RuntimeException(
                "Error creating a new FeatureTablePanel to show "
                    + "the selected FeatureType with id: " + typeId, e);
        }
        getJSplitPane().setRightComponent(tablePanel);
    }

    public FeatureTablePanel getTablePanel() {
        return tablePanel;
    }

    public FeatureTypeList getTypesControl() {
        return typesControl;
    }

    /**
     * Sets that the selected Features to be viewed first.
     */
    public void setSelectionUp(boolean selectionUp) {
        getTablePanel().setSelectionUp(selectionUp);
    }

    /**
     * Returns the internal Table Model for the Features of the FeatureStore.
     * 
     * @return the internal Table Model
     */
    protected ConfigurableFeatureTableModel getTableModel() {
        return getTablePanel().getTableModel();
    }

    /**
     * This method initializes jPanel
     * 
     * @return javax.swing.JPanel
     */
    private void initializeUI() {
        this.setLayout(new BorderLayout());
        this.setSize(new Dimension(331, 251));
        this.add(getJSplitPane(), BorderLayout.CENTER);
        showFeatureTypeData(model.getCurrentFeatureTypeId());
    }

    /**
     * This method initializes jSplitPane
     * 
     * @return javax.swing.JSplitPane
     */
    private JSplitPane getJSplitPane() {
        if (jSplitPane == null) {
            jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
            jSplitPane.setLeftComponent(getTypesControl());
            jSplitPane.setOneTouchExpandable(true);
            try {
                if (hasManyFeatureTypes()) {
                    jSplitPane.setDividerLocation(0.2d);
                } else {
                    jSplitPane.setDividerLocation(0.0d);
                }
            } catch (DataException ex) {
                LOG.error("Error getting the number of feature types", ex);
            }
        }
        return jSplitPane;
    }

}
