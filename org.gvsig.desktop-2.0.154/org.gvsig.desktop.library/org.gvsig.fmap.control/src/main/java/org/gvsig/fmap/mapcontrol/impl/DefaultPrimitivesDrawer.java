/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.impl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultPrimitivesDrawer implements PrimitivesDrawer {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultPrimitivesDrawer.class);

    protected Graphics2D graphics = null;
    protected Color color = Color.BLACK;
    protected Color xorColor = Color.WHITE;
    private Object lockObject = null;

    public DefaultPrimitivesDrawer() {
        super();
    }

    public DefaultPrimitivesDrawer(Graphics2D graphics) {
        super();
        this.graphics = graphics;
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        graphics.setXORMode(xorColor);
        graphics.drawLine(x1, y1, x2, y2);
        graphics.setPaintMode();
    }

    public void drawOval(int x, int y, int width, int height) {
        graphics.setXORMode(xorColor);
        graphics.drawOval(x, y, width, height);
        graphics.setPaintMode();
    }

    public void drawRect(int x, int y, int width, int height) {
        graphics.setXORMode(xorColor);
        graphics.drawRect(x, y, width, height);
        graphics.setPaintMode();
    }

    public void fillRect(int x, int y, int width, int height) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setGraphics(Graphics graphics) {
        this.graphics = (Graphics2D) graphics;
    }

    public void stopDrawing(Object obj) {
        if (lockObject != obj) {
            LOG.info("Trying to unlock a resource that is not locked");
            return;
        }
        lockObject = null;
    }

    public void startDrawing(Object obj) throws InterruptedException {
        while ((lockObject != null) && (obj != lockObject)) {
            Thread.sleep(100);
        }
        lockObject = obj;
    }
    
    public void setRenderingHints(RenderingHints hints) {
    	this.graphics.setRenderingHints(hints);
    }
    
    public void setStroke(Stroke stroke) {
    	this.graphics.setStroke(stroke);
    }
    
    public void cleanCanvas(MapControl mapCtrl) {
    	BufferedImage img = mapCtrl.getImage();
    	this.graphics.drawImage(img, 0, 0, null);
    }
}
