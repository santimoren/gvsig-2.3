/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @deprecated use ZoomOutListenerImpl setting mouseButton to right in the Behavior.
 */
public class ZoomOutRightButtonListener implements PointListener {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(ZoomOutRightButtonListener.class);
		
	/**
	 * Reference to the <code>MapControl</code> object that uses.
	 */
	private MapControl mapControl;

	/**
	 * <p>Creates a new <code>ZoomOutRightButtonListener</code> object.</p>
	 *
	 * @param mapControl the <code>MapControl</code> where will be applied the changes
	 */
	public ZoomOutRightButtonListener(MapControl mapControl) {
		this.mapControl = mapControl;
                logger.warn("Using deprecated class ZoomOutRightButtonListener");
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PointListener#point(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
	 */
	public void point(PointEvent event) {
		if (event.getEvent().getButton() == MouseEvent.BUTTON3){
			logger.debug("Zoom out bot�n derecho");
			ViewPort vp = mapControl.getMapContext().getViewPort();
			Point2D p2 = vp.toMapPoint(event.getPoint());

			double nuevoX;
			double nuevoY;
			double factor = 1 / MapContext.ZOOMOUTFACTOR;
			if (vp.getExtent() != null) {
				nuevoX = p2.getX()
						- ((vp.getExtent().getWidth() * factor) / 2.0);
				nuevoY = p2.getY()
						- ((vp.getExtent().getHeight() * factor) / 2.0);
				double x = nuevoX;
				double y = nuevoY;
				double width = vp.getExtent().getWidth() * factor;
				double height = vp.getExtent().getHeight() * factor;

				try {
					vp.setEnvelope(geomManager.createEnvelope(x, y ,x + width, y + height, SUBTYPES.GEOM2D));
				} catch (CreateEnvelopeException e) {
					logger.error("Error creating the envelope", e);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getImageCursor()
	 */
	public Image getImageCursor() {
		return IconThemeHelper.getImage("cursor-zoom-out");
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
	 */
	public boolean cancelDrawing() {
	    logger.debug("cancelDrawing true");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PointListener#pointDoubleClick(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
	 */
	public void pointDoubleClick(PointEvent event) {
		// TODO Auto-generated method stub
	}
}
