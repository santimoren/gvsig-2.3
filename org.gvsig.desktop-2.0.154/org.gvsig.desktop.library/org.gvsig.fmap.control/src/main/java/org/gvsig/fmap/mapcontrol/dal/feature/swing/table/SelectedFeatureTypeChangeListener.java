/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.exception.BaseException;

/**
 * Listener for changes in the selected {@link FeatureType}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public interface SelectedFeatureTypeChangeListener {

    /**
     * Called when the selected {@link FeatureType} changes.
     * 
     * @param featureStore
     *            where the {@link FeatureType}s belong to
     * @param featureType
     *            the new selected {@link FeatureType}
     * @throws BaseException
     */
    void change(FeatureStore featureStore, FeatureType featureType);

}
