/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.fmap.mapcontrol.impl;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.Draw;
import org.gvsig.fmap.geom.operation.DrawOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * MapControlDrawer for a 2D view.
 *
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MapControlDrawer2D extends DefaultPrimitivesDrawer implements MapControlDrawer{
    private static final Logger LOG = LoggerFactory.getLogger(MapControlDrawer2D.class);

	protected GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private ViewPort viewPort = null;

	/**
	 * @param graphics
	 * @param viewPort
	 */
	public MapControlDrawer2D() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.renderer.Renderer#draw(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void draw(Geometry geometry, ISymbol symbol) {
		if (geometry != null) {
		    symbol.draw(graphics, viewPort.getAffineTransform(), geometry, null, null);
		}
	}

    public void draw(Geometry geometry, ISymbol symbol, Feature feature) {
        if (geometry != null) {
            symbol.draw(graphics, viewPort.getAffineTransform(), geometry, feature, null);
        }
    }

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.renderer.Renderer#drawHandlers(org.gvsig.fmap.geom.Geometry, java.awt.geom.AffineTransform, org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void drawHandlers(Handler[] handlers, AffineTransform at, ISymbol symbol) {
		for (int i = 0; i < handlers.length; i++) {

		    try {
	            Point2D point = handlers[i].getPoint();
	            at.transform(point, point);

	            graphics.setPaintMode();
	            graphics.setColor(symbol.getColor());
	            graphics.fillRect((int) (point.getX() - 3), (int) (point.getY() - 3), 7, 7);
	            graphics.drawRect((int) (point.getX() - 5), (int) (point.getY() - 5), 10, 10);
	            graphics.drawString( "" + i, (int) (point.getX() - 5), (int) (point.getY() - 5));
		    } catch (Throwable th) {
		        /*
		         * jmvivo experienced a "java.lang.InternalError" here, possibly
		         * a graphic bug in the JRE when dealing with windows.
		         * Since we are simply drawing a handler, we will ignore this
		         * error and simply write a line in the log file..
		         */
		        LOG.warn("Unexpected error while drawing handler: " + th.getMessage(), th);
		    }
		}
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.renderer.Renderer#drawLine(java.awt.geom.Point2D, java.awt.geom.Point2D, org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol)
	 */
	public void drawLine(Point2D firstPoint, Point2D endPoint, ISymbol symbol) {
		DrawOperationContext doc = new DrawOperationContext();
		doc.setGraphics(graphics);
		doc.setViewPort(viewPort);
		doc.setSymbol(symbol);
		try {
			Curve curve = (Curve)geomManager.create(Geometry.TYPES.CURVE, Geometry.SUBTYPES.GEOM2D);
			curve.addVertex(geomManager.createPoint(firstPoint.getX(), firstPoint.getY(), Geometry.SUBTYPES.GEOM2D) );
			curve.addVertex(geomManager.createPoint(endPoint.getX(), endPoint.getY(), Geometry.SUBTYPES.GEOM2D) );
			curve.invokeOperation(Draw.CODE, doc);
		} catch (GeometryOperationNotSupportedException e) {
			e.printStackTrace();
		} catch (GeometryOperationException e) {
			e.printStackTrace();
		} catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.renderer.Renderer#drawImage(java.awt.Image, int, int)
	 */
	public void drawImage(Image img, int x, int y) {
		if (img != null){
			graphics.drawImage(img, x, y, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlDrawer#drawHandler(org.gvsig.fmap.geom.handler.Handler, java.awt.geom.AffineTransform)
	 */
	public void drawHandler(Handler handler, AffineTransform at) {
		Point2D point = handler.getPoint();
		Point2D dest = (Point2D) point.clone();
		at.transform(point, dest);
		graphics.setColor(Color.black);
		graphics.drawLine((int)dest.getX()-2,(int)dest.getY()-10,(int)dest.getX()-2,(int)dest.getY()+10);
		graphics.drawLine((int)dest.getX()+2,(int)dest.getY()-10,(int)dest.getX()+2,(int)dest.getY()+10);
		graphics.drawLine((int)dest.getX()-10,(int)dest.getY()-2,(int)dest.getX()+10,(int)dest.getY()-2);
		graphics.drawLine((int)dest.getX()-10,(int)dest.getY()+2,(int)dest.getX()+10,(int)dest.getY()+2);
		graphics.setColor(Color.red);
		graphics.drawLine((int)dest.getX()-1,(int)dest.getY()-10,(int)dest.getX()-1,(int)dest.getY()+10);
		graphics.drawLine((int)dest.getX()+1,(int)dest.getY()-10,(int)dest.getX()+1,(int)dest.getY()+10);
		graphics.drawLine((int)dest.getX()-10,(int)dest.getY()-1,(int)dest.getX()+10,(int)dest.getY()-1);
		graphics.drawLine((int)dest.getX()-10,(int)dest.getY()+1,(int)dest.getX()+10,(int)dest.getY()+1);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.renderer.Renderer#drawImage(java.awt.Image, java.awt.geom.AffineTransform)
	 */
	public void drawImage(Image img, AffineTransform xform) {
		graphics.drawImage(img, xform, null);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontext.rendering.Renderer#setViewPort(org.gvsig.fmap.mapcontext.ViewPort)
	 */
	public void setViewPort(ViewPort viewPort) {
		this.viewPort = viewPort;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlDrawer#draw(org.gvsig.fmap.geom.Geometry)
	 */
	public void draw(Geometry geometry) {
		draw(geometry, MapContextLocator.getSymbolManager().createSymbol(geometry.getType()));
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlDrawer#setComposite(java.awt.AlphaComposite)
	 */
	public void setComposite(Composite composite) {
		graphics.setComposite(composite);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.MapControlDrawer#transform(java.awt.geom.AffineTransform)
	 */
	public void transform(AffineTransform at) {
		graphics.transform(at);
	}

}

