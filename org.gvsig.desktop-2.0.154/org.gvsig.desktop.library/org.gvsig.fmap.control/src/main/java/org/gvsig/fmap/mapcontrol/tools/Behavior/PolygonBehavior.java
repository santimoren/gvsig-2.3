/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Behavior;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PolylineListener;



/**
 * <p>Behavior that permits user to draw a polygon by its vertexes on the image of the associated
 *  <code>MapControl</code> using a {@link PolylineListener PolylineListener}.</p>
 *
 * @author Vicente Caballero Navarro
 * @author Pablo Piqueras Bartolomé
 */
public class PolygonBehavior extends PolylineBehavior {
	/**
	 * <p>Creates a new behavior for drawing a polygon by its vertexes.</p>
	 *
	 * @param ali tool listener used to permit this object to work with the associated <code>MapControl</code>
	 */
	public PolygonBehavior(PolylineListener ali) {
		super(ali);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.Behavior.Behavior#paintComponent(org.gvsig.fmap.mapcontrol.MapControlDrawer)
	 */
	public void paintComponent(MapControlDrawer mapControlDrawer) {
		super.paintComponent(mapControlDrawer);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.PolylineBehavior#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) throws BehaviorException {
            if( !isMyButton(e) ) {
                return;
            }
		if (e.getClickCount() == 2) {
			listener.polylineFinished(new MeasureEvent((Double[]) arrayX.toArray(new Double[arrayX.size()]), (Double[]) arrayY.toArray(new Double[arrayY.size()]), e));

			arrayX.clear();
			arrayY.clear();

			isClicked = false;
		} else {
			isClicked = true;

			if (arrayX.size() == 0) {
				addPoint(getMapControl().getViewPort().toMapPoint(e.getPoint()));
				addPoint(getMapControl().getViewPort().toMapPoint(e.getPoint()));
				addPoint(getMapControl().getViewPort().toMapPoint(e.getPoint()));
			} else {
				addAntPoint(getMapControl().getViewPort().toMapPoint(e.getPoint()));
			}

			listener.pointFixed(new MeasureEvent((Double[]) arrayX.toArray(new Double[arrayX.size()]), (Double[]) arrayY.toArray(new Double[arrayY.size()]), e));
		}
	}

	/**
	 * <p>Adds a new vertex to the polygon.</p>
	 *
	 * @param p a new vertex to the polygon
	 */
	private void addAntPoint(Point2D p) {
		arrayX.add(arrayX.size() - 1, new Double(p.getX()));
		arrayY.add(arrayY.size() - 1, new Double(p.getY()));
	}

	/**
	 * <p>Changes the last vertex added to the polygon.</p>
	 *
	 * @param p a new vertex to the polygon
	 */
	private void changeAntPoint(Point2D p) {
		if (arrayX.size() > 2) {
			arrayX.set(arrayX.size() - 2, new Double(p.getX()));
			arrayY.set(arrayY.size() - 2, new Double(p.getY()));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.PolylineBehavior#mouseMoved(java.awt.event.MouseEvent)
	 */
	public void mouseMoved(MouseEvent e) throws BehaviorException {
		if (isClicked) {
			changeAntPoint(getMapControl().getViewPort().toMapPoint(e.getPoint()));

			MeasureEvent event = new MeasureEvent((Double[]) arrayX.toArray(new Double[arrayX.size()]), (Double[]) arrayY.toArray(new Double[arrayY.size()]), e);

			listener.points(event);

			getMapControl().repaint();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Behavior.PolylineBehavior#mouseDragged(java.awt.event.MouseEvent)
	 */
	public void mouseDragged(MouseEvent e) throws BehaviorException {
		mouseMoved(e);
	}
}
