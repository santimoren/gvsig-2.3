/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 IVER T.I. S.A.   {{Task}}
 */

/**
 *
 */
package org.gvsig.fmap.mapcontrol.swing.dynobject;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Editor for a store parameters.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DynObjectEditor extends JPanel implements ActionListener {

    private static final long serialVersionUID = 23898787077741411L;

    private static final Logger LOG = LoggerFactory
        .getLogger(DynObjectEditor.class);

    private String title;

    private JButton botAcept;
    private JButton botCancel;
    private JButton botRestoreDefaults;
    private JPanel panButtons;

//    private boolean modal;

    private JDynForm form = null;
    private DynObject data = null;
    private boolean isCanceled = false;
    private Runnable onclose = null;

    public DynObjectEditor(DynObject data, boolean showDefaultsButton) {
    	this.data = data;
        try {
            this.form = DynFormLocator.getDynFormManager().createJDynForm(this.data);
        } catch (ServiceException ex) {
            throw new RuntimeException("Can't create JDynForm from '"+this.data.getDynClass().getFullName()+"'.", ex);
        }
    	this.form.setLayoutMode(this.form.USE_TABS);
    	this.setLayout(new BorderLayout());
        this.add(this.form.asJComponent(), BorderLayout.CENTER);
        this.add(getButtonsPanel(showDefaultsButton), BorderLayout.SOUTH);
        this.setPreferredSize(new Dimension(500,250));
        String s = this.data.getDynClass().getDescription();
        if( s==null || s.length()==0 ) {
        	s = this.data.getDynClass().getName();
        }
        this.setTitle(s);
    }
    
    public DynObjectEditor(DynObject data) throws ServiceException {
        this(data, false);
    }

    private JPanel getButtonsPanel(boolean add_defaults_button) {
        if (this.panButtons == null) {
            this.panButtons = new JPanel();
            this.panButtons.setLayout(new GridBagLayout());
            GridBagConstraints constr = new GridBagConstraints();
            constr.anchor = GridBagConstraints.LAST_LINE_END;
            constr.fill = GridBagConstraints.HORIZONTAL;
            constr.weightx = 1;
            constr.weighty = 0;
            this.panButtons.add(new JLabel(), constr);

            constr = this.getDefaultParametersConstraints();
            constr.fill = GridBagConstraints.NONE;
            constr.weightx = 0;
            constr.weighty = 0;

            this.panButtons.add(this.getAcceptButton(), constr);
            this.panButtons.add(this.getCancelButton(), constr);
            if (add_defaults_button) {
                this.panButtons.add(this.getRestoreDefaults(), constr);
            }
        }
        return this.panButtons;
    }

    private GridBagConstraints getDefaultParametersConstraints() {
        GridBagConstraints constr = new GridBagConstraints();
        constr.insets = new Insets(2, 2, 2, 2);
        constr.ipadx = 2;
        constr.ipady = 2;
        constr.anchor = GridBagConstraints.PAGE_START;
        return constr;

    }

    private JButton getRestoreDefaults() {
        if (this.botRestoreDefaults == null) {
            this.botRestoreDefaults =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton(
                    Messages.getText("restoreDefaults"));
            this.botRestoreDefaults.addActionListener(this);
        }
        return this.botRestoreDefaults;
    }

    private JButton getCancelButton() {
        if (this.botCancel == null) {
            this.botCancel =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton(
                    Messages.getText("cancel"));
            this.botCancel.addActionListener(this);
        }
        return this.botCancel;
    }

    private JButton getAcceptButton() {
        if (this.botAcept == null) {
            this.botAcept =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton(
                    Messages.getText("accept"));
            this.botAcept.addActionListener(this);
        }
        return this.botAcept;
    }

    public void actionPerformed(ActionEvent e) {
        Component source = (Component) e.getSource();
        if (source == this.botAcept) {
            this.form.getValues(this.data);
            this.isCanceled = false;
            this.closeWindow();
        } else if (source == this.botCancel) {
            this.isCanceled = true;
            this.closeWindow();
        } else if (source == this.botRestoreDefaults) {
            this.form.setValues(this.data);
        }
    }
    
    public void setOnClose(Runnable onclose) {
        this.onclose = onclose;
    }

    protected void closeWindow() {
        LOG.debug("Closing window, values edited: ", this.data);
        this.setVisible(false);    
        if( this.onclose != null ) {
            this.onclose.run();
        }
    }

    public void editObject(boolean modal) {
        WindowManager wmanager = ToolsSwingLocator.getWindowManager();
        WindowManager.MODE mode = WindowManager.MODE.WINDOW;
        if( modal ) {
            mode = WindowManager.MODE.DIALOG;
        }
        wmanager.showWindow(this,title,mode);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @deprecated use getData
     * @return the data values of the DynObject managed
     */
    public DynObject getParameters() {
        return this.getData();
    }
    
    /**
     * Return the data values of the DynObject managed.
     * @return the dynobject
     */
    public DynObject getData() {
    	this.form.getValues(this.data);
        return this.data;
    }
    
    public DynObject getData(DynObject data) {
    	this.form.getValues(data);
        return data;
    }
    
    public void putData(DynObject data) {
        this.form.setValues(data);
    }
    
    public boolean isCanceled() {
        return this.isCanceled;
    }
}
