/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a Table component for Features}
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing;

import java.awt.Color;
import java.awt.Component;
import java.sql.Timestamp;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.ConfigurableFeatureTableModel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureCellRenderer;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureTableModel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FormattedCellEditor;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FormattedCellRenderer;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.GeometryWKTCellEditor;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.GeometryWKTCellRenderer;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.JToggleButtonHeaderCellRenderer;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.notification.ColumnHeaderSelectionChangeNotification;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Table extension to show Feature values.
 *
 * It's based on the usage of a FeatureTableModel, and adds renderers for
 * Geometry and Feature cell values.
 *
 * Observers are notified about column header selection changes, with a
 * {@link ColumnHeaderSelectionChangeNotification}.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class FeatureTable extends JTable implements Observer, Observable {

    private static final Logger logger = LoggerFactory.getLogger(FeatureTable.class);
    /**
     * Generated Serial UID
     */
    private static final long serialVersionUID = -6139395189283163964L;

    
    private final FeatureTableModel featureTableModel;
    private JToggleButtonHeaderCellRenderer headerCellRenderer;

	private static final int COLUMN_HEADER_MARGIN = 8;

	private static final int COLUMN_HEADER_MIN_WIDTH = 50;

    /**
     * Creates a new FeatureTable with a {@link FeatureTableModel}.
     *
     * @param featureTableModel
     *            the table model to get data to be shown on the table
     * @throws DataException
     *             if there is an error while loading the Features to show
     * @see JTable#JTable(TableModel)
     */
    public FeatureTable(FeatureTableModel featureTableModel)
            throws DataException {
        super(featureTableModel);
        this.featureTableModel = featureTableModel;
        init();
    }

    /**
     * Creates a new FeatureTable with a {@link FeatureTableModel}.
     *
     * @param featureTableModel
     *            the table model to get data to be shown on the table
     * @param cm
     *            the table column model to use
     * @throws DataException
     *             if there is an error while loading the Features to show
     * @see JTable#JTable(TableModel, TableColumnModel)
     */
    public FeatureTable(FeatureTableModel featureTableModel, TableColumnModel cm)
            throws DataException {
        super(featureTableModel, cm);
        this.featureTableModel = featureTableModel;
        init();
    }

    public void update(Observable observable, Object notification) {
        if (notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification fsNotification = (FeatureStoreNotification) notification;
            String type = fsNotification.getType();
            // If the selection has changed, repaint the table to show the new
            // selected rows
            if (FeatureStoreNotification.SELECTION_CHANGE.equals(type)) {
                repaint();
            }

            /*
             * This is necessary to let Swing know
             * that editing (in terms of Swing, not gvsig editing)
             * must be cancelled because the deleted row
             * is perhaps the row that was being edited
             */
            if (FeatureStoreNotification.BEFORE_DELETE.equals(type)) {
                if (this.isEditing()) {
                    ChangeEvent che = new ChangeEvent(this);
                    this.editingCanceled(che);
                }
            }
        }
    }

    /**
     * Returns the FeatureAttributeDescriptor related to the selected columns.
     *
     * @return an array of FeatureAttributeDescriptor
     *
     * @see org.gvsig.fmap.mapcontrol.dal.feature.swing.table.JToggleButtonHeaderCellRenderer#getSelectedColumns()
     */
    public FeatureAttributeDescriptor[] getSelectedColumnsAttributeDescriptor() {
        int[] columns = headerCellRenderer.getSelectedColumns();
        FeatureAttributeDescriptor[] descriptors = new FeatureAttributeDescriptor[columns.length];

        for (int i = 0; i < descriptors.length; i++) {
            descriptors[i] = featureTableModel
                    .getDescriptorForColumn(columns[i]);
        }

        return descriptors;
    }

    public void addObserver(Observer observer) {
        headerCellRenderer.addObserver(observer);
    }

    public void deleteObserver(Observer observer) {
        headerCellRenderer.deleteObserver(observer);
    }

    public void deleteObservers() {
        headerCellRenderer.deleteObservers();
    }

	/**
	 * Sets that the selected Features to be viewed first.
	 */
	public void setSelectionUp(boolean selectionUp) {
		((FeatureTableModel) getModel()).setSelectionUp(selectionUp);
		scrollRectToVisible(getCellRect(0, 0, true));
	}

    // @Override
    // public void tableChanged(TableModelEvent e) {
    // super.tableChanged(e);
    // if (headerCellRenderer != null) {
    // headerCellRenderer.deselectAll();
    // }
    // }

    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();
        // Add a cell renderer for Geometries and Features
        setDefaultRenderer(Geometry.class, new GeometryWKTCellRenderer());
        setDefaultEditor(Geometry.class, new GeometryWKTCellEditor());
        setDefaultRenderer(Feature.class, new FeatureCellRenderer());

        if( this.getModel() instanceof ConfigurableFeatureTableModel ) {
            ConfigurableFeatureTableModel model = (ConfigurableFeatureTableModel)this.getModel();
            setDefaultRenderer(Double.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Float.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Integer.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Long.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Date.class, new FormattedCellRenderer(model));
            setDefaultEditor(Double.class, new FormattedCellEditor(model));
            setDefaultEditor(Float.class, new FormattedCellEditor(model));
            setDefaultEditor(Integer.class, new FormattedCellEditor(model));
            setDefaultEditor(Long.class, new FormattedCellEditor(model));
            setDefaultEditor(Date.class, new FormattedCellEditor(model));
        }

        // Set the selected row colors
        setSelectionForeground(Color.blue);
        setSelectionBackground(Color.yellow);
    }

    /**
     * Initializes the table GUI.
     */
    private void init() throws DataException {
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        featureTableModel.getFeatureStore().addObserver(this);
        // Change the selection model to link with the FeatureStore selection
        // through the FeatureTableModel
        setRowSelectionAllowed(true);
        setColumnSelectionAllowed(false);
		setSelectionModel(new FeatureSelectionModel(featureTableModel));

		headerCellRenderer = new JToggleButtonHeaderCellRenderer(this);
		getTableHeader().setDefaultRenderer(headerCellRenderer);

		TableColumnModel tcmodel = getColumnModel();
		for (int i = 0; i < tcmodel.getColumnCount(); i++) {
			TableColumn col = tcmodel.getColumn(i);
			// Get width of column header
			TableCellRenderer renderer = col.getHeaderRenderer();
			if (renderer == null) {
				renderer = getTableHeader().getDefaultRenderer();
			}
			Component comp =
					renderer.getTableCellRendererComponent(this,
							col.getHeaderValue(), false, false, 0, i);
			int width = comp.getPreferredSize().width;
			width =
					width < COLUMN_HEADER_MIN_WIDTH ? COLUMN_HEADER_MIN_WIDTH
							: width;
			col.setPreferredWidth(width + 2 * COLUMN_HEADER_MARGIN);
		}
    }

    /**
     * Returns the number of selected columns.
     *
     * @return the number of selected columns, 0 if no columns are selected
     */
    public int getSelectedColumnCount() {
        return headerCellRenderer.getSelectedColumns().length;
    }

    public void tableChanged(TableModelEvent e) {
        // Clear the header selection
        if (e != null && e.getFirstRow() == TableModelEvent.HEADER_ROW
            && headerCellRenderer != null) {
            headerCellRenderer.deselectAll();
        }

        super.tableChanged(e);
    }

    public Class<?> getColumnClass(int column) {
        Class resp = super.getColumnClass(column);
        if (Timestamp.class.isAssignableFrom(resp)) {
            return Object.class;
        } else {
            return resp;
        }
    }

    @Override
    public int getSelectedRowCount() {
        try {
            return (int) this.featureTableModel.getFeatureStore().getFeatureSelection().getSelectedCount();
        } catch (DataException ex) {
            logger.error("Can't calculate selected rows in table.", ex);
            return 0;
        }
    }


}