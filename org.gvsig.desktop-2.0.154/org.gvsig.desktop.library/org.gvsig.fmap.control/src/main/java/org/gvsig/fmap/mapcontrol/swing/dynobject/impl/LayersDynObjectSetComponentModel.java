/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.swing.dynobject.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractListModel;

import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dynobject.DynObjectSet;

/**
 * Model for a {@link LayersDynObjectSetComponent}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class LayersDynObjectSetComponentModel extends AbstractListModel
    implements Disposable {

    private static final long serialVersionUID = -7978388308830573063L;

    private final Map<String, DynObjectSet> layerName2InfoByPoint;
    private final String[] layerNames;

    public LayersDynObjectSetComponentModel(
        Map<String, DynObjectSet> layerName2InfoByPoint) {
        this.layerName2InfoByPoint = layerName2InfoByPoint;
        Set<String> keyset = layerName2InfoByPoint.keySet();
        layerNames = keyset.toArray(new String[keyset.size()]);
    }

    public String[] getLayerNames() {
        Set<String> keyset = layerName2InfoByPoint.keySet();
        return keyset.toArray(new String[keyset.size()]);
    }

    public DynObjectSet getLayerInfoByPoint(String layerName) {
        return layerName2InfoByPoint.get(layerName);
    }

    public int getSize() {
        return layerName2InfoByPoint.size();
    }

    public Object getElementAt(int index) {
        return layerNames[index];
    }

    public void dispose() {
        for (Iterator<DynObjectSet> iterator =
            layerName2InfoByPoint.values().iterator(); iterator.hasNext();) {
            iterator.next().dispose();
        }
        layerName2InfoByPoint.clear();
    }
}
