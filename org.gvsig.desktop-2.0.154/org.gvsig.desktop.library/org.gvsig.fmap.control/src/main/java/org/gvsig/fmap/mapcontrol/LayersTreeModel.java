package org.gvsig.fmap.mapcontrol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.apache.commons.lang3.BooleanUtils;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;

public class LayersTreeModel implements TreeModel {

    private final List<TreeModelListener> listeners = new ArrayList();
    protected FLayers layers;
    private Evaluator filter;
    private MyEvaluatorData datafilter;

    private static class MyEvaluatorData implements EvaluatorData {

        public FLayer layer = null;

        @Override
        public Object getDataValue(String name) {
            if (name.equalsIgnoreCase("layer")) {
                return layer;
            }
            return null;
        }

        @Override
        public Object getContextValue(String name) {
            if (name.equalsIgnoreCase("layer")) {
                return layer;
            }
            return null;
        }

        @Override
        public Iterator getDataValues() {
            return Arrays.asList(new FLayer[]{this.layer}).iterator();
        }

        @Override
        public Iterator getDataNames() {
            return Arrays.asList(new String[]{"layer"}).iterator();
        }

        @Override
        public boolean hasDataValue(String name) {
            if (name.equalsIgnoreCase("layer")) {
                return true;
            }
            return false;
        }

        @Override
        public boolean hasContextValue(String name) {
            if (name.equalsIgnoreCase("layer")) {
                return true;
            }
            return false;
        }

    }

    public LayersTreeModel(FLayers layers) {
        this.layers = layers;
    }

    public void setFilter(Evaluator filter) {
        this.filter = filter;
        this.datafilter = new MyEvaluatorData();
    }

    @Override
    public Object getRoot() {
        return layers;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (!(parent instanceof FLayers)) {
            return null;
        }
        return ((FLayers) parent).getLayer(index);
    }

    @Override
    public int getChildCount(Object parent) {
        if (!(parent instanceof FLayers)) {
            return 0;
        }
        if (this.filter == null) {
            return ((FLayers) parent).getLayersCount();
        }
        int count = 0;
        for (int i = 0; i < ((FLayers) parent).getLayersCount(); i++) {
            FLayer layer = ((FLayers) parent).getLayer(i);
            try {
                if (!(layer instanceof FLayers)) {
                    this.datafilter.layer = layer;
                    if (BooleanUtils.isTrue((Boolean) this.filter.evaluate(this.datafilter))) {
                        count++;
                    }
                }
            } catch (Exception ex) {
                // Do nothing, skyp this layer on error.
            }
        }
        return count;

    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof FLayers);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (this.filter == null) {
            for (int i = 0; i < ((FLayers) parent).getLayersCount(); i++) {
                FLayer layer = ((FLayers) parent).getLayer(i);
                if (child == layer) {
                    return i;
                }
            }
            return -1;
        }
        int count = 0;
        for (int i = 0; i < ((FLayers) parent).getLayersCount(); i++) {
            FLayer layer = ((FLayers) parent).getLayer(i);
            try {
                if (!(layer instanceof FLayers)) {
                    this.datafilter.layer = layer;
                    if (BooleanUtils.isTrue((Boolean) this.filter.evaluate(this.datafilter))) {
                        if (child == layer) {
                            return count;
                        }
                        count++;
                    }
                }
            } catch (Exception ex) {
                // Do nothing, skyp this layer on error.
            }
        }
        return -1;

    }

    @Override
    public void valueForPathChanged(TreePath path, Object value) {
    }

    @Override
    public void addTreeModelListener(TreeModelListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener listener) {
        listeners.remove(listener);
    }

    public void reload() {
        TreeModelEvent event = new TreeModelEvent(this, new TreePath(this.layers));
        Iterator<TreeModelListener> iterator = listeners.iterator();
        TreeModelListener listener;
        while (iterator.hasNext()) {
            listener = iterator.next();
            listener.treeStructureChanged(event);
        }
    }

}
