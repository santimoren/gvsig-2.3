/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools.Behavior;

import java.awt.event.MouseWheelEvent;

import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Listeners.ToolListener;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class MouseWheelBehavior extends Behavior {

    /**
     * Constructor.
     */
    public MouseWheelBehavior() {
    }

    @Override
    public ToolListener getListener() {
        return null;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent event) throws BehaviorException {
        MapControl control = getMapControl();
        control.cancelDrawing();
        super.mouseWheelMoved(event);
        ViewPort viewPort = control.getViewPort();
        Envelope envelope = viewPort.getEnvelope();
        if (envelope != null) {
            double factor = event.getWheelRotation() < 0 ? 0.75d : 1.3333d;
            Point mousePosition = viewPort.convertToMapPoint(event.getPoint());
            Point envelopPoint1 = envelope.getLowerCorner();
            Point envelopPoint2 = envelope.getUpperCorner();

            double width1 = mousePosition.getX() - envelopPoint1.getX();
            double heigth1 = mousePosition.getY() - envelopPoint1.getY();
            double width2 = envelopPoint2.getX() - mousePosition.getX();
            double heigth2 = envelopPoint2.getY() - mousePosition.getY();

            double xmin = mousePosition.getX() - width1 * factor;
            double ymin = mousePosition.getY() - heigth1 * factor;
            double xmax = mousePosition.getX() + width2 * factor;
            double ymax = mousePosition.getY() + heigth2 * factor;

            try {
                viewPort.setEnvelope(geomManager.createEnvelope(xmin, ymin,
                    xmax, ymax, SUBTYPES.GEOM2D));
            } catch (CreateEnvelopeException e) {
                throw new BehaviorException(
                    "Error changing the viewport envelope to (" + xmin + ","
                        + ymin + "),(" + xmax + "," + ymax + ")", e);
            }
            getMapControl().drawMap(true);
        }
        event.consume();
    }
    
    @Override
    public void paintComponent(MapControlDrawer mapControlDrawer) {
    	// Do nothing
    }
}
