/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.dal.feature.swing.table;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;

/**
 * Shows a list of {@link FeatureType}s to allow the user to select one of them.
 * 
 * @author 2005- Vicente Caballero
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class FeatureTypeList extends JPanel {

    private static final long serialVersionUID = 6168364334623238504L;

    private JScrollPane jScrollPane = null;
    private JList jList = null;
    private List<SelectedFeatureTypeChangeListener> featureTypeChangeListeners =
        new ArrayList<SelectedFeatureTypeChangeListener>();

    private final FeatureTypeListModel model;

    /**
     * Creates a new {@link FeatureTypeList} which loads the available
     * {@link FeatureType}s from a {@link FeatureStore} and a
     * {@link FeatureQuery}.
     * 
     * @param featureStore
     *            the store to load the {@link FeatureType}s from
     * @param featureQuery
     *            the query to load the {@link FeatureType} from, if any
     */
    public FeatureTypeList(FeatureStore featureStore, FeatureQuery featureQuery) {
        this(new FeatureTypeListModel(featureStore, featureQuery));
    }

    /**
     * Creates a new {@link FeatureTypeList} which loads the available
     * {@link FeatureType}s from a {@link FeatureTypeListModel}.
     * 
     * @param model
     *            the model to get the {@link FeatureType}s from
     */
    public FeatureTypeList(FeatureTypeListModel model) {
        this.model = model;
        initialize();
    }

    private void initialize() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.gridx = 0;
        this.setLayout(new GridBagLayout());
        this.setPreferredSize(new Dimension(146, 281));
        this.add(getJScrollPane(), gridBagConstraints);
    }

    /**
     * This method initializes jScrollPane
     * 
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane() {
        if (jScrollPane == null) {
            jScrollPane = new JScrollPane();
            jScrollPane.setViewportView(getJList());
        }
        return jScrollPane;
    }

    /**
     * This method initializes jList
     * 
     * @return javax.swing.JList
     */
    private JList getJList() {
        if (jList == null) {
            jList = new javax.swing.JList();
            jList.setModel(this.model);
            jList.setSelectedIndex(0);
            jList.addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e) {
                    FeatureType fType = getSelectedFeatureType();
                    for (int i = 0; i < featureTypeChangeListeners.size(); i++) {
                        featureTypeChangeListeners.get(i).change(
                            model.getFeatureStore(), fType);
                    }
                }
            });
        }

        return jList;
    }

    public void addSelectedFeatureTypeChangeListener(
        SelectedFeatureTypeChangeListener listener) {
        featureTypeChangeListeners.add(listener);
    }

    public void removeSelectedFeatureTypeChangeListener(
        SelectedFeatureTypeChangeListener listener) {
        featureTypeChangeListeners.remove(listener);
    }

    /**
     * Returns the currently selected {@link FeatureType}.
     * 
     * @return the currently selected {@link FeatureType}
     */
    public FeatureType getSelectedFeatureType() {
        int index = jList.getSelectedIndex();
        if (index >= 0) {
            String typeId = (String) jList.getModel().getElementAt(index);
            return model.getFeatureType(typeId);
        } else {
            return null;
        }
    }

    /**
     * Returns the number of available {@link FeatureType}s.
     * 
     * @return the number of available {@link FeatureType}s
     */
    public int getFeatureTypesSize() {
        return model.getSize();
    }
}
