package org.gvsig.fmap.mapcontrol;

import org.gvsig.fmap.mapcontext.layers.FLayers;


public class CompoundLayersTreeModel extends LayersTreeModel {

        public CompoundLayersTreeModel() {
            super(new FLayers());
        }
        
        public void addLayers(FLayers layers) {
            this.layers.add(layers);
        }
}
