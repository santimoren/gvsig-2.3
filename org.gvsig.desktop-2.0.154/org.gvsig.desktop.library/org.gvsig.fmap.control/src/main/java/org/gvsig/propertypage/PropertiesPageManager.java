
package org.gvsig.propertypage;

import java.util.List;


public interface PropertiesPageManager {

    /**
     * Register a new PropertiesPageFactory in this manager.
     * 
     * @param factory 
     */
    void registerFactory(PropertiesPageFactory factory);
    
    /**
     * Get a list of the property page instances that can be show for the object.
     * 
     * @param obj
     * @return the list of PropertiesPage
     */
    List<PropertiesPage> getPages(String groupID, Object obj);

}
