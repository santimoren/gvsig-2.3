/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;
import java.awt.Point;

import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PolylineListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * <p>Listener for calculating the length of the segments of a polyline, defined in the associated {@link MapControl MapControl}
 *  object.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class MeasureListenerImpl implements PolylineListener {

	private static final Logger logger = LoggerFactory.getLogger(MeasureListenerImpl.class);
	/**
	 * The image to display when the cursor is active.
	 */
//	private final Image iruler = PluginServices.getIconTheme().get("cursor-query-area").getImage();
	/**
	 * Reference to the <code>MapControl</code> object that uses.
	 */
	protected MapControl mapCtrl;

	/**
	 * <p>Creates a new listener for calculating the length of a polyline.</p>
	 *
	 * @param mc the <code>MapControl</code> where is calculated the length
	 */
	public MeasureListenerImpl(MapControl mc) {
		this.mapCtrl = mc;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#points(com.iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
	 */
	public void points(MeasureEvent event) {
		double dist = 0;
		double distAll = 0;

		ViewPort vp = mapCtrl.getMapContext().getViewPort();

		for (int i = 0; i < (event.getXs().length - 1); i++) {
			dist = 0;

			Point p = new Point(event.getXs()[i].intValue(),
					event.getXs()[i].intValue());
			Point p2 = new Point(event.getXs()[i + 1].intValue(),
					event.getXs()[i + 1].intValue());

			///dist = vp.toMapDistance((int) p.distance(p2));
			dist = vp.distanceWorld(p, p2);
			distAll += dist;
		}

		logger.debug("Distancia = {}, Distancia Total = {}.",dist,distAll);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getImageCursor()
	 */
	public Image getImageCursor() {
		return IconThemeHelper.getImage("cursor-query-area");
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#pointFixed(com.iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
	 */
	public void pointFixed(MeasureEvent event) {
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
	 */
	public boolean cancelDrawing() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#polylineFinished(com.iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
	 */
	public void polylineFinished(MeasureEvent event) {
	}
}
