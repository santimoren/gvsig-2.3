/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontrol.tools;

import java.awt.Image;
import java.awt.geom.Point2D;

import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * <p>Listener that selects all features of the active, and vector layers of the associated <code>MapControl</code>
 *  that their area intersects with the point selected by a single click of any button of the mouse.</p>
 *
 * @author Vicente Caballero Navarro
 */
public class PointSelectionListener implements PointListener {
	/**
	 * Reference to the <code>MapControl</code> object that uses.
	 */
	protected MapControl mapCtrl;

	/**
	 * <p>Creates a new <code>PointSelectionListener</code> object.</p>
	 *
	 * @param mc the <code>MapControl</code> where will be applied the changes
	 */
	public PointSelectionListener(MapControl mc) {
		this.mapCtrl = mc;
	}

	public void point(PointEvent event) throws BehaviorException {
		try {
            Point2D p = event.getPoint();
            Point2D mapPoint = mapCtrl.getViewPort().toMapPoint((int) p.getX(), (int) p.getY());

            // Tolerancia de 3 pixels
            double tol = mapCtrl.getViewPort().toMapDistance(3);
            FLayer[] actives = mapCtrl.getMapContext().getLayers().getActives();
            for (int i = 0; i < actives.length; i++) {
                if (actives[i] instanceof FLyrVect) {
                    FLyrVect lyrVect = (FLyrVect) actives[i];
					FeatureSet newSelection = null;
					try {
						newSelection = lyrVect.queryByPoint(
										mapPoint, tol,
										lyrVect.getFeatureStore().getDefaultFeatureType());
						if (event.getEvent().isControlDown()) {
							FeatureSelection currentSelection = (FeatureSelection)lyrVect.getDataStore().getSelection();
							DisposableIterator it = newSelection.fastIterator();
							while(it.hasNext()) {
								Object obj = it.next();
								if(obj instanceof Feature) {
									Feature feat = ((Feature)obj);
									if(currentSelection.isSelected(feat))
										currentSelection.deselect(feat);
									else
										currentSelection.select(feat);
								}
							}
						} else {
							lyrVect.getFeatureStore()
									.setSelection(newSelection);
						}
					} finally {
						if (newSelection!=null){
							newSelection.dispose();
						}
                    }
                    mapCtrl.drawMap(false); // FIXME Esto deber�a sobrar (la capa deber�a de ser observada)
                }
            }

		} catch (DataException e) {
			throw new BehaviorException("No se pudo hacer la selecci�n", e);
		}
	}
	

	public Image getImageCursor() {
		return IconThemeHelper.getImage("cursor-select-by-point");
	}

	public boolean cancelDrawing() {
		return false;
	}

	public void pointDoubleClick(PointEvent event) throws BehaviorException {

	}
}
