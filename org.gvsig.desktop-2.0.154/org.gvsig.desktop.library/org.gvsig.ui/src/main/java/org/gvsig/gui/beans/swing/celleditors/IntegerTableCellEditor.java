/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.swing.celleditors;

import java.awt.Component;
import java.util.ArrayList;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import org.gvsig.gui.beans.swing.cellrenderers.NumberTableCellRenderer;

/**
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class IntegerTableCellEditor implements TableCellEditor {
	private static NumberTableCellRenderer renderer = new NumberTableCellRenderer(true, false);
	private ArrayList listeners = new ArrayList();

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (value == null) return null;
		return renderer.getTableCellRendererComponent(table, value, isSelected, false, row, column);
	}


	public void cancelCellEditing() {
		for (int i = 0; i < listeners.size(); i++) {
	        CellEditorListener l = (CellEditorListener) listeners.get(i);
	        ChangeEvent evt = new ChangeEvent(this);
	        l.editingCanceled(evt);
	    }
	}

	public boolean stopCellEditing() {
		for (int i = 0; i < listeners.size(); i++) {
            CellEditorListener l = (CellEditorListener) listeners.get(i);
            ChangeEvent evt = new ChangeEvent(this);
            l.editingStopped(evt);
        }
        return true;
	}


	public Object getCellEditorValue() {
		if (renderer!=null && renderer.getIncrementalNumberField()!=null)
			return new Integer(renderer.getIncrementalNumberField().getInteger());
		return null;
	}

	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	public boolean shouldSelectCell(EventObject anEvent) {
		return false;
	}

	public void addCellEditorListener(CellEditorListener l) {
		listeners.add(l);
	}

	public void removeCellEditorListener(CellEditorListener l) {
		listeners.remove(l);
	}
}
