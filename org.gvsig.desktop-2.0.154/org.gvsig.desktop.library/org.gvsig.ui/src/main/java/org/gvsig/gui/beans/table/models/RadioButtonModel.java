/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.table.models;

import javax.swing.table.DefaultTableModel;

public class RadioButtonModel extends DefaultTableModel implements IModel {
	private static final long serialVersionUID = 7113401200946271448L;

	public RadioButtonModel(String[] columnNames) {
		super(new Object[0][2], columnNames);
	}

	public Class getColumnClass(int c) {
		if (c < 1) {
			return Boolean.class;
		}

		return String.class;
	}

	public void setValueAt(Object value, int row, int col) {
		super.setValueAt(value, row, col);
	}

	public void addNew() {
		super.addRow(new Object[] { new Boolean(false), "" });
	}

	public Object[] getNewLine() {
		return new Object[] { new Boolean(false), "" };
	}
}