/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.listview.painters;

import java.awt.*;
import java.util.ArrayList;

import org.gvsig.gui.beans.listview.IListViewPainter;
import org.gvsig.gui.beans.listview.ListViewItem;
import org.gvsig.i18n.Messages;

/**
 * Iconos de 82x28
 *
 * @version 28/06/2007
 * @author BorSanZa - Borja S�nchez Zamorano (borja.sanchez@iver.es)
 */
public class SmallIcon implements IListViewPainter {

  ArrayList items = null;

  int iconsWidth = 40;

  int minIconsWidth = 40;

  int iconsHeight = 28;

  Dimension lastDimension = new Dimension(0, 0);

  int cols = 0;

  public SmallIcon(ArrayList items) {
    this.items = items;
  }

  /*
   * (non-Javadoc)
   * @see org.gvsig.gui.beans.graphic.listview.IListViewPainter#getName()
   */
  public String getName() {
    return Messages.getText("smallIcon");
  }

  /*
   * (non-Javadoc)
   * @see
   * org.gvsig.gui.beans.graphic.listview.IListViewPainter#getPreferredSize()
   */
  public Dimension getPreferredSize() {
    return lastDimension;
  }

  /*
   * (non-Javadoc)
   * @see
   * org.gvsig.gui.beans.graphic.listview.IListViewPainter#paint(java.awt.Graphics2D
   * , int, int)
   */
  public void paint(Graphics2D g, Rectangle visibleRect) {
    int aux = (int) Math.floor(visibleRect.getWidth() / (minIconsWidth + 2));
    if (aux > items.size()) aux = items.size();
    iconsWidth = (int) (Math.floor(visibleRect.getWidth() / aux) - 2);

    int height2 = 0;

    int posX = 0;
    int posY = 0;
    cols = 0;
    for (int i = 0; i < items.size(); i++) {
      // Evito que se pueda editar el nombre
      ((ListViewItem) items.get(i)).setNameRectangle(null);

      ((ListViewItem) items.get(i)).setShowTooltip(true);
      if (posX != 0) {
        if (((posX + 1) * (iconsWidth + 2)) > visibleRect.getWidth()) {
          posX = 0;
          posY++;
        }
      }

      ((ListViewItem) items.get(i)).getItemRectangle().setBounds(
          posX * (iconsWidth + 2), (posY * (iconsHeight + 2)), iconsWidth + 2,
          iconsHeight + 2);
      if (((ListViewItem) items.get(i)).getItemRectangle().intersects(
          visibleRect)) {
        if (((ListViewItem) items.get(i)).isSelected()) {
          g.setColor(new Color(49, 106, 197));
          g.fillRect(posX * (iconsWidth + 2), posY * (iconsHeight + 2),
              iconsWidth + 2, iconsHeight + 2);
        }

        Shape clip = g.getClip();
        g.translate(posX * (iconsWidth + 2) + 1, (posY * (iconsHeight + 2)) + 1);
        g.setClip(0, 0, iconsWidth, iconsHeight);

        if (((ListViewItem) items.get(i)).getIcon() != null)
          ((ListViewItem) items.get(i)).getIcon().paint(g,
              ((ListViewItem) items.get(i)).isSelected());

        g.setClip(clip);
        g.translate(-(posX * (iconsWidth + 2) + 1),
            -((posY * (iconsHeight + 2)) + 1));
      }

      if (height2 < ((posY + 1) * (iconsHeight + 2)))
        height2 = (posY + 1) * (iconsHeight + 2);

      if (cols < posX) cols = posX;

      posX++;
    }

    lastDimension = new Dimension(minIconsWidth + 2, height2);
  }
}