/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.table.models;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

public class ROIsTableModel extends DefaultTableModel implements IModel {
	private static final long serialVersionUID = 8716862990277121681L;

	
	private static boolean[]	canEdit = new boolean[] { true, false, false, false, true};
	private static Class[]   	types   = new Class[] { String.class, Integer.class, Integer.class, Integer.class, JButton.class};
	private static Color[]   	colors  = new Color[] {Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA, Color.CYAN,
		Color.ORANGE, Color.PINK, Color.WHITE, Color.BLACK};
	
	public ROIsTableModel(String[] columnNames) {
		super(new Object[0][5], columnNames);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.gui.beans.table.models.IModel#getNewLine()
	 */
	public Object[] getNewLine() {
		Color color = null;
		if (this.getRowCount() < colors.length) {
			color = colors[this.getRowCount()];
		}
		else{
			color = new Color((float)Math.random(),(float)Math.random(),(float)Math.random());
		}
		return new Object[] {"", new Integer(0), new Integer(0), new Integer(0), color};
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class getColumnClass(int columnIndex) {
		return types [columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return canEdit [columnIndex];
	}
}
