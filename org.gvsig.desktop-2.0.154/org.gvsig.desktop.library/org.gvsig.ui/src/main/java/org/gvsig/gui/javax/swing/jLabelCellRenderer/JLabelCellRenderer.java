/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.javax.swing.jLabelCellRenderer;

import java.awt.Color;
import java.awt.Component;
import java.io.Serializable;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * This class allows render a JLabel in a Cell of other graphic component (as a JList)
 * 
 * @author Pablo Piqueras Bartolomé (p_queras@hotmail.com)
 */
public class JLabelCellRenderer extends DefaultListCellRenderer implements Serializable {
	private static final long serialVersionUID = -3820198983465837422L;

	/* (non-Javadoc)
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        /* The DefaultListCellRenderer class will take care of
         * the JLabels text property, it's foreground and background
         * colors, and so on.
         */
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		
		// Set the text property, background and foreground color
        if (value instanceof JLabel) {
        	setText(((JLabel)value).getText());
    		setBackground(((JLabel)value).getBackground());
    		
    		if (isSelected)
    			setForeground(Color.red);
    		else
    			setForeground(((JLabel)value).getForeground());
        }

		return this;
	}
}
