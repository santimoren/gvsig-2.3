/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.table.models;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 * Componente tabla
 * 
 * @author Nacho Brodin (brodin_ign@gva.es)
 *
 */
public class CheckBoxColumnRenderer extends JCheckBox implements TableCellRenderer {
    final private static long            serialVersionUID = -3370601314380922368L;
    private ActionListener               listener = null;

    public CheckBoxColumnRenderer() {}
    
    public CheckBoxColumnRenderer(ActionListener listener) {
    	this.listener = listener;
    }
    
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row, int column) {
        if (value == null) {
            this.setSelected(false);
        }
        if(isSelected)
        	if(listener != null) 
        		listener.actionPerformed(new ActionEvent(this, row, "SELECT_ROW"));
        	
        if(value instanceof Boolean){
	        Boolean ValueAsBoolean = (Boolean) value;
	        this.setSelected(ValueAsBoolean.booleanValue());
	        this.setHorizontalAlignment(SwingConstants.CENTER);
        }
        	
        return this;
    }
}