/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.controls;

import java.awt.event.ActionListener;

/**
 * This interface models a control to be used in Andami.
 *
 * @author Cesar Martinez Izquierdo <cesar.martinez@iver.es>
 *
 */
public interface IControl {
	/**
	 *  Adds an ActionListener to the control
	 *
	 * @param listener
	 */
	public void addActionListener(ActionListener listener);

	/**
	 *  Removes an ActionListener from the control
	 *
	 * @param listener
	 */
	public void removeActionListener(ActionListener listener);

	/**
	 *  Sets the value of the control. It may mean different things for
	 *  different kinds of controls.
	 *
	 *  @return The value which was set, or null if the value was not valid
	 *  for this control
	 */
	public Object setValue(Object value);

	public Object getValue();

	/**
	 * Gets the name of the control, used to identify it
	 * @return the name of the control
	 */
	public String getName();

	/**
	 * Sets the name of the control, used to identify it
	 *
	 * @param name
	 * @return
	 */
	public void setName(String name);
	
}
