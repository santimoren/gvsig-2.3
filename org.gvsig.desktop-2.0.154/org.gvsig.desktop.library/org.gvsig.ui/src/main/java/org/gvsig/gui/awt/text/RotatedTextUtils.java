/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2015 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.awt.text;


import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

/**
 * <p>A convenience class to easily draw rotated text which is positioned on
 * a specific side of the rotation point (TOP, BOTTOM, LEFT or RIGHT).
 * The text can be anchored by its central point or by the text corner.</p>
 * 
 * <p>The following diagrams illustrate the behaviour of each positioning and
 * anchor point in relation to the rotation point:</p>
 * <pre>
 * top center:         top corner:
 *        o                     o
 *       l                     l
 *      l                     l
 *     e                     e
 *    h                     h
 *      .                   . 
 * 
 * 
 * right center:      right corner:
 *                          o 
 *                         l 
 *        o               l
 *       l               e
 *   .  l              .h 
 *     e
 *    h
 * </pre>
 * 
 * <p>The  class provides 2 separate families of methods:
 * <ul>
 * <li><strong>draw methods</strong>, which rotate the graphics, draw the rotated text and
 * restore the graphics transformation</li>
 * <li><strong>getPosition methods</strong>, which are used to get the position of the
 * rotated text on an already rotated graphics (faster when drawing several
 * rotated texts using the same rotation angle). Note that this family of
 * methods deals with coordinates in 2 different coordinate spaces
 * (the original, non-rotated space and the rotated space). The origin point
 * coordinates has to be referred to the non-rotated space, while the returned
 * position is referred to the rotated space.</li>
 * </ul>
 *
 * Generally speaking the <code>draw</code> family of methods can be considered a simpler, higher level
 * API, while the <code>getPosition</code> family is conceptually more complex but faster for some
 * scenarios.
 * </p>
 * 
 * <p>
 * Example of <strong>draw</strong> method usage:
 * <pre>
 * // draw coordinates axis
 * Graphics2D g = ...
 * Point2D origin1 = new Point2D.Double(100, 200);
 * int lenght = 100;
 * g.drawLine((int)origin1.getX()-length, (int)origin1.getY(), (int)origin1.getX()+length, (int)origin1.getY());
 * g.drawLine((int)origin1.getX(), (int)origin1.getY()-length, (int)origin1.getX(), (int)origin1.getY()+length);
 * // draw the rotated text
 * RotatedTextUtils.draw(origin1, g, "Hello world", angle, RotatedTextUtils.PLACEMENT_BOTTOM, RotatedTextUtils.ANCHOR_CORNER);
 * </pre>
 * </p>
 * 
 * 
 * <p>
 * Example of <strong>getPosition</strong> method usage:
 * <pre>
 * // draw coordinates axis
 * Graphics2D g = ...
 * AffineTransform defaultAt = g.getTransform();
 * Point2D origin1 = new Point2D.Double(100, 200);
 * Point2D origin2 = new Point2D.Double(200, 200);
 * int lenght = 100;
 * g.drawLine((int)origin1.getX()-length, (int)origin1.getY(), (int)origin1.getX()+length, (int)origin1.getY());
 * g.drawLine((int)origin1.getX(), (int)origin1.getY()-length, (int)origin1.getX(), (int)origin1.getY()+length);
 * // draw the rotated text
 * AffineTransform finalTransform = g.getTransform();
 * finalTransform.rotate(angle);
 * g.setTransform(finalTransform);
 * TextLayout text = new TextLayout("Hello world", g.getFont(), g.getFontRenderContext());
 * Point2D p = RotatedTextUtils.getPosition(origin1, angle, text, RotatedTextUtils.PLACEMENT_BOTTOM, RotatedTextUtils.ANCHOR_CORNER);
 * text.draw(g, (float)p.getX(), (float)p.getY());
 * // faster than RotatedTextUtils.draw if we are writing the same rotated text at different points
 * p = RotatedTextUtils.getPosition(origin2, angle, text, RotatedTextUtils.PLACEMENT_BOTTOM, RotatedTextUtils.ANCHOR_CORNER);
 * text.draw(g, (float)p.getX(), (float)p.getY());
 * g.setTransform(defaultAt);
 * </pre>
 * </p>
 * 
 * @author Cesar Martinez Izquierdo <cmartinez@scolab.es>
 *
 */
public class RotatedTextUtils {
	/**
	 * PI/2
	 */
	private static final double PI_HALF = Math.PI/2;
	/**
	 * 3*PI/2
	 */
	private static final double PI_HALF3 = 3*Math.PI/2;
	/**
	 * 2*PI
	 */
	private static final double PI_HALF4 = 2*Math.PI;
	
	/**
	 * Anchor a corner of the text on the rotation center. In this way
	 * a corner (left or right) of the text string will be aligned
	 * with the rotation point.
	 * 
	 * The corner (left or right) to anchor will be automatically selected
	 * depending on the rotation angle (choosing the corner which is closer
	 * to the rotation center)
	 */
	public static final int ANCHOR_CORNER = 0;
	/**
	 * Anchor the center of the text on the rotation center. In this way
	 * the center of the text string will be aligned with the rotation point.
	 */
	public static final int ANCHOR_CENTER = 1;
	
	/**
	 * Place the text on the top of the rotation point, meaning that no part
	 * of the text is under the rotation point. 
	 */
	public static final int PLACEMENT_TOP = 0;
	/**
	 * Place the text bellow the rotation point, meaning that no part
	 * of the text is over the rotation point. 
	 */
	public static final int PLACEMENT_BOTTOM = 1;
	/**
	 * Place the text on the left of the rotation point, meaning that no part
	 * of the text is on the right the rotation point. 
	 */
	public static final int PLACEMENT_LEFT = 2;
	/**
	 * Place the text on the right of the rotation point, meaning that no part
	 * of the text is on the left the rotation point. 
	 */
	public static final int PLACEMENT_RIGHT = 3;
	
    /**
     * Draws rotated text which is positioned on
     * a specific side of the rotation point (TOP, BOTTOM, LEFT or RIGHT).
     * The text can be anchored by its central point or by the text corner.
     * 
     * @param origin	The rotation center point
     * @param g			The target Graphics2D
     * @param strText 	The text to draw .Use the Graphics2D options (font,
     * color, etc) to style the text before calling this method.
     * @param angle		The rotation angle, in radians. The angle should be comprised
     * in the [0, 2*PI[ range, result is otherwise unexpected
     * (a convenience method is provided to normalize it:
     *  {@link RotatedTextUtils#normalizeAngle(double)})
     * @param relativePosition The position of the text compared with the origin point.
     *  See {@link #PLACEMENT_TOP}, {@link #PLACEMENT_LEFT}, {@link #PLACEMENT_RIGHT} and
     *  {@value #PLACEMENT_BOTTOM}.
     * @param anchor 	Whether the center of the label should be aligned with the
     * point ({@link #ANCHOR_CENTER}) or a corner of the label should be used
     * ({@link #ANCHOR_CORNER}).
     */
	public static void draw(Point2D origin, Graphics2D g, String strText, double angle, int relativePosition, int anchor) {
		AffineTransform defaultAt = g.getTransform();

		// affine transform containing the rotation plus the previous graphics transformations (if any)
		AffineTransform finalAt = g.getTransform();
		finalAt.rotate(angle);
		g.setTransform(finalAt);

		TextLayout text = new TextLayout(strText, g.getFont(), g.getFontRenderContext());
		Point2D position = null;

		if (anchor==ANCHOR_CORNER) {
			switch (relativePosition) {
			case PLACEMENT_RIGHT:
				position = getPositionRightCorner(origin, text, angle);
				break;
			case PLACEMENT_BOTTOM:
				position = getPositionBottomCorner(origin, text, angle);
				break;
			case PLACEMENT_LEFT:
				position = getPositionLeftCorner(origin, text, angle);
				break;
			case PLACEMENT_TOP:
			default:
				position = getPositionTopCorner(origin, text, angle);
				break;
			}
		}
		else {
			switch (relativePosition) {
			case PLACEMENT_RIGHT:
				position = getPositionRightCenter(origin, text, angle);
				break;
			case PLACEMENT_BOTTOM:
				position = getPositionBottomCenter(origin, text, angle);
				break;
			case PLACEMENT_LEFT:
				position = getPositionLeftCenter(origin, text, angle);
				break;
			case PLACEMENT_TOP:
			default:
				position = getPositionTopCenter(origin, text, angle);
				break;
			}
		}
		text.draw(g, (float)position.getX(), (float)position.getY());
		g.setTransform(defaultAt);
    }
    
    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point, angle, align and anchor.</p>
     * 
     * <p>You may consider using
     * the higher level draw methods (e.g. {@link #draw(Point2D, Graphics2D, String, double, int, int)},
     * {@link #drawTopCenter(Point2D, Graphics2D, String, double)}, etc) if
     * you are drawing a single label, as this method makes some assumptions
     * for getting maximum performance when drawing several texts using the
     * same rotation. In particular, this method assumes that the target
     * Graphics2D has been rotated using the provided angle and the text
     * has been laid out for this rotated target Graphics2D.</p> 
	 * 
	 * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     * 
     * @param origin The point used as the center of the rotation
     * @param text   The text to be positioned, which has to be prepared for
     * a rotated graphics, matching the rotation angle 
     * @param angle The rotation angle, in radians. The angle should be comprised
     * in the [0, 2*PI[ range, result is otherwise unexpected
     * (a convenience method is provided to normalize it:
     *  {@link RotatedTextUtils#normalizeAngle(double)}
     * @param relativePosition The position of the text compared with the origin point.
     *  See {@link #PLACEMENT_TOP}, {@link #PLACEMENT_LEFT}, {@link #PLACEMENT_RIGHT} and
     *  {@value #PLACEMENT_BOTTOM}.
     * @param anchor Whether the center of the label should be aligned with the
     * point ({@link #ANCHOR_CENTER}) or a corner of the label should be used
     * ({@link #ANCHOR_CORNER}).
     * @return The position in which the text should be drawn.
     */
    public static Point2D getPosition(Point2D origin, TextLayout text, double angle, int relativePosition, int anchor) {
    	if (anchor==ANCHOR_CORNER) {
    		switch (relativePosition) {
    		case PLACEMENT_RIGHT:
    			return getPositionRightCorner(origin, text, angle);
    		case PLACEMENT_BOTTOM:
    			return getPositionBottomCorner(origin, text, angle);
    		case PLACEMENT_LEFT:
    			return getPositionLeftCorner(origin, text, angle);
    		case PLACEMENT_TOP:
    		default:
    			return getPositionTopCorner(origin, text, angle);
    		}
    	}
    	else {
    		switch (relativePosition) {
    		case PLACEMENT_RIGHT:
    			return getPositionRightCenter(origin, text, angle);
    		case PLACEMENT_BOTTOM:
    			return getPositionBottomCenter(origin, text, angle);
    		case PLACEMENT_LEFT:
    			return getPositionLeftCenter(origin, text, angle);
    		case PLACEMENT_TOP:
    		default:
    			return getPositionTopCenter(origin, text, angle);
    		}
    	}
    }

    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the top of the
     * point and using a corner of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_TOP
     * @see RotatedTextUtils#ANCHOR_CORNER
     */
    public static Point2D getPositionTopCorner(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
    	
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);

    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = -getRotatedWidth1(width, angle)-getRotatedWidth2(height, angle)/2.0d;
    		yOffset = -getRotatedHeight1(width, angle);
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = 0.0d;
    		yOffset = 0.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = getRotatedWidth2(height, angle)/2.0d;
    		yOffset = 0.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = getRotatedWidth1(width, angle)-getRotatedWidth2(height, angle)/2.0d;
    		yOffset = -(getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle));
    	}
    	else { // third quadrant
    		xOffset = getRotatedWidth2(height, angle)/2.0d; 
    		yOffset = -getRotatedHeight2(height, angle);
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
        return result;
    }

    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the top of the
     * point and using the center of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_TOP
     * @see RotatedTextUtils#ANCHOR_CENTER
     */
    public static Point2D getPositionTopCenter(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
    	
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);

    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = -(getRotatedWidth1(width, angle)+getRotatedWidth2(height, angle))/2.0d;
    		yOffset = -getRotatedHeight1(width, angle);
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = -width/2.0d;
    		yOffset = 0.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = (getRotatedWidth2(height, angle)-getRotatedWidth1(width, angle))/2.0d;
    		yOffset = 0.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = (getRotatedWidth1(width, angle)-getRotatedWidth2(height, angle))/2.0d;
    		yOffset = -(getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle));
    	}
    	else { // third quadrant
    		xOffset = (getRotatedWidth1(width, angle) + getRotatedWidth2(height, angle))/2.0d; 
    		yOffset = -getRotatedHeight2(height, angle);
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
        return result;
    }


    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the right of the
     * point and using a corner of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_RIGHT
     * @see RotatedTextUtils#ANCHOR_CORNER
     */
    public static Point2D getPositionRightCorner(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);
		
    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = 0.0d;
    		yOffset = getRotatedHeight2(height, angle)/2.0d;
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = 0.0d;
    		yOffset = height/2.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = getRotatedWidth2(height, angle);
    		yOffset = getRotatedHeight2(height, angle)/2.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = getRotatedWidth1(width, angle);
    		yOffset = -getRotatedHeight1(width, angle)-(getRotatedHeight2(height, angle))/2.0d;
    	}
    	else { // third quadrant
    		xOffset = getRotatedWidth1(width, angle) + getRotatedWidth2(height, angle);
    		yOffset = -getRotatedHeight2(height, angle)/2.0d +getRotatedHeight1(width, angle);
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }

    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the right of the
     * point and using the center of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_RIGHT
     * @see RotatedTextUtils#ANCHOR_CENTER
     */
    public static Point2D getPositionRightCenter(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
    	
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);
		
    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = 0.0;
    		yOffset = (getRotatedHeight2(height, angle)-getRotatedHeight1(width, angle))/2.0d;
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = 0.0;
    		yOffset = height/2.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = (getRotatedWidth2(height, angle));
    		yOffset = (getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle))/2.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = getRotatedWidth1(width, angle);
    		yOffset = -(getRotatedHeight2(height, angle)+getRotatedHeight1(width, angle))/2.0d;
    	}
    	else { // third quadrant
    		xOffset = getRotatedWidth1(width, angle)+getRotatedWidth2(height, angle); 
    		yOffset = (getRotatedHeight1(width, angle)-getRotatedHeight2(height, angle))/2.0d;
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }

    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the bottom of the
     * point and using a corner of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_BOTTOM
     * @see RotatedTextUtils#ANCHOR_CORNER
     */
    public static Point2D getPositionBottomCorner(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);
		
    	if (angle>0.0d && angle<=PI_HALF) { // first quadrant
    		xOffset = -getRotatedWidth2(height, angle)/2.0d;
    		yOffset = getRotatedHeight2(height, angle);
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = 0.0d;
    		yOffset = height;
    	}
    	else if (angle>=PI_HALF3) {  // fourth quadrant
    		xOffset = getRotatedWidth2(height, angle)/2.0d-getRotatedWidth1(width, angle);
    		yOffset = getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle);

    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = -getRotatedWidth2(height, angle)/2.0d;
    		yOffset = 0.0d;
    	}
    	else { // third quadrant
    		xOffset = getRotatedWidth1(width, angle) + getRotatedWidth2(height, angle)/2.0d; 
    		yOffset = getRotatedHeight1(width, angle);
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }

    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the bottom of the
     * point and using the center of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_BOTTOM
     * @see RotatedTextUtils#ANCHOR_CENTER
     */
    public static Point2D getPositionBottomCenter(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);

    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = -(getRotatedWidth1(width, angle)+getRotatedWidth2(height, angle))/2.0d;
    		yOffset = getRotatedHeight2(height, angle);
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = -width/2.0d;
    		yOffset = height;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = (getRotatedWidth2(height, angle)-getRotatedWidth1(width, angle))/2.0d;
    		yOffset = getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle);
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = (getRotatedWidth1(width, angle)-getRotatedWidth2(height, angle))/2.0d;
    		yOffset = 0.0d;
    	}
    	else { // third quadrant
    		xOffset = (getRotatedWidth1(width, angle) + getRotatedWidth2(height, angle))/2.0d; 
    		yOffset = getRotatedHeight1(width, angle);
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }


    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the left of the
     * point and using a corner of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_LEFT
     * @see RotatedTextUtils#ANCHOR_CORNER
     */
    public static Point2D getPositionLeftCorner(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);
		
    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = -(getRotatedWidth1(width, angle)+getRotatedWidth2(height, angle));
    		yOffset = getRotatedHeight2(height, angle)/2.0d -getRotatedHeight1(width, angle);
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = -width;
    		yOffset = height/2.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = (-getRotatedWidth1(width, angle));
    		yOffset = getRotatedHeight1(width, angle)+(getRotatedHeight2(height, angle))/2.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = -(getRotatedWidth2(height, angle));
    		yOffset = -getRotatedHeight2(height, angle)/2.0d;
    	}
    	else { // third quadrant
    		xOffset = 0.0d; 
    		yOffset = -getRotatedHeight2(height, angle)/2.0d;
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }
    /**
     * <p>Gets the position in which the text should be drawn according to the
     * provided origin point and angle, placing the text at the left of the
     * point and using the center of the text as anchor.</p>
     * 
     * <p>This method deals with coordinates in 2 different coordinate spaces
     * (the original, non-rotated space and the rotated space). The origin point
     * coordinates has to be referred to the non-rotated space, while the returned
     * position is referred to the rotated space.</p>
     *  
     * @param origin The center point of the rotation
     * @param text   The text to be drawn, created for the rotated Graphics2D
     * @param angle  The rotation angle, in radians. Angle is assumed to be normalized (see
     * {@link #normalizeAngle(double)}) 
     * @return The position in which the text should be drawn, referenced to the rotated
     * coordinate space
     * 
     * @throws NoninvertibleTransformException
     * @see {@link RotatedTextUtils#getPosition(Point2D, double, TextLayout, int, int)}
     * @see RotatedTextUtils#PLACEMENT_LEFT
     * @see RotatedTextUtils#ANCHOR_CENTER
     */
    public static Point2D getPositionLeftCenter(Point2D origin, TextLayout text, double angle) {
    	double height = text.getBounds().getHeight();
    	double descent = text.getBounds().getHeight()+text.getBounds().getY();
    	double width = text.getAdvance();
    	double yOffset;
    	double xOffset;
		double correctedOriginX = origin.getX()+getRotatedDescent2(descent, angle);
		double correctedOriginY = origin.getY()-getRotatedDescent1(descent, angle);
		
    	if (angle>0.0d && angle<PI_HALF) { // first quadrant
    		xOffset = -(getRotatedWidth1(width, angle)+getRotatedWidth2(height, angle));
    		yOffset = (getRotatedHeight2(height, angle)-getRotatedHeight1(width, angle))/2.0d;
    	}
    	else if (angle<0.1d) { // when is 0
    		xOffset = -width;
    		yOffset = height/2.0d;
    	}
    	else if (angle>PI_HALF3) {  // fourth quadrant
    		xOffset = (-getRotatedWidth1(width, angle));
    		yOffset = (getRotatedHeight1(width, angle)+getRotatedHeight2(height, angle))/2.0d;
    	}
    	else if (angle<Math.PI) { // second quadrant
    		xOffset = -(getRotatedWidth2(height, angle));
    		yOffset = -(getRotatedHeight2(height, angle)+getRotatedHeight1(width, angle))/2.0d;
    	}
    	else { // third quadrant
    		xOffset = 0.0d; 
    		yOffset = (getRotatedHeight1(width, angle)-getRotatedHeight2(height, angle))/2.0d;
    	}
    	Point2D result = new Point2D.Double(correctedOriginX+xOffset, correctedOriginY+yOffset);
    	// Transform the calculated drawing point from the non-rotated coordinate space
    	// to the final (rotated) coordinate space.
    	// All the above calculations have been made using the non-rotated coordinate space
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
    	return result;
    }
    
    /**
     * Draws the provided text rotated by angle radians using location as rotation center
     * without any positioning or anchoring adjustments.
     * Use the Graphics2D options (font, color, etc) to style the text before calling
     * this method.
     * 
     * @param location The rotation center
     * @param g        The Graphics2D on which the text should be drawn
     * @param strText  The text to be drawn
     * @param angle    The rotation angle, in radians
     */
    public static void drawRotated(Point2D location, Graphics2D g, String strText, double angle) {
    	AffineTransform defaultAt = g.getTransform();
    	AffineTransform at = AffineTransform.getRotateInstance(angle);
        g.setTransform(at);
        TextLayout text = new TextLayout(strText, g.getFont(), g.getFontRenderContext());
        Point2D result = new Point2D.Double(location.getX(), location.getY());
    	try {
			at.inverseTransform(result, result);
		} catch (NoninvertibleTransformException e) {
			// can't happen: rotation always has inverste tranform
		}
        text.draw(g, (float)result.getX(), (float)result.getY());
        g.setTransform(defaultAt);
    }

	/**
	 * Normalizes an angle, in radians. A normalized angle
	 * is an angle contained in the range [0, 2*PI[.
	 * 
	 * @param angle The angle to normalize, in radians
	 * @return Normalized angled, in radians
	 */
	public static double normalizeAngle(double angle) {
		double module = angle%(PI_HALF4);
		if (module>=0) {
			return module;
		}
		else {
			return (angle + PI_HALF4);
		}
	}
    
    private static double getRotatedHeight1(double width, double angle) {
    	return Math.abs(width*Math.sin(angle));
    }
    
    private static double getRotatedHeight2(double height, double angle) {
    	return Math.abs(height*Math.cos(angle));
    }
    
    private static double getRotatedWidth1(double width, double angle) {
    	return Math.abs(width*Math.cos(angle));
    }
    
    private static double getRotatedWidth2(double height, double angle) {
    	return Math.abs(height*Math.sin(angle));
    }
    
    private static double getRotatedDescent1(double descent, double angle) {
    	return descent*Math.sin(angle+PI_HALF);
    }
    
    private static double getRotatedDescent2(double descent, double angle) {
    	return descent*Math.sin(angle);
    }
    
    
    private static double getRotatedOffsetX1(double baseOffsetX, double angle) {
    	return Math.abs(baseOffsetX*Math.cos(angle+PI_HALF));
    }
    
    private static double getRotatedOffsetX2(double baseOffsetX, double angle) {
    	return Math.abs(baseOffsetX*Math.sin(angle));
    }
}


