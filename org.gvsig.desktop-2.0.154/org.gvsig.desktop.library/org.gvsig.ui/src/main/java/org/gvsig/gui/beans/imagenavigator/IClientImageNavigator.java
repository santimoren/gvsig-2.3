/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.imagenavigator;

import java.awt.Graphics2D;
/**
 * Interfaz que debe implementar quien quiera dibujar en ImageNavigator
 * @version 12/06/2008
 * @author BorSanZa - Borja S�nchez Zamorano (borja.sanchez@iver.es)
 */
public interface IClientImageNavigator {

	/**
	 * Metodo que es invocado cuando ImageNavigator va a pintar la imagen.
	 * Pasa un Graphics para hacer el dibujado, y luego todas las posibles
	 * posiciones para hacer el pintado.
	 * 
	 * @param g
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 * @param zoom
	 * @param width
	 * @param height
	 * @throws ImageUnavailableException
	 */
	public void drawImage(Graphics2D g,
			double left,
			double top,
			double right,
			double bottom,
			double zoom,
			int width,
			int height) throws ImageUnavailableException;
}