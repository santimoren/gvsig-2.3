/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.NumberFormatter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JNumberSpinner extends JSpinner {

    /**
     *
     */
    private static final long serialVersionUID = 8614785196224519553L;

    private static final Logger logger = LoggerFactory.getLogger(JNumberSpinner.class);

    private boolean fireChanges;
    private List<ActionListener> actionListeners = new ArrayList<ActionListener>();

    private final int defaultColumns = 10;

    public JNumberSpinner() {
        super(new SpinnerNumberModel(0.0, Double.NEGATIVE_INFINITY, Double.MAX_VALUE, 1.0));
        init(defaultColumns,0);
    }

    public JNumberSpinner(int currentValue) {
        super(new SpinnerNumberModel(currentValue, Integer.MIN_VALUE, Integer.MAX_VALUE, 1));
        init(defaultColumns,0);
    }

    public JNumberSpinner(int currentValue, int columns) {
        super(new SpinnerNumberModel(currentValue, Integer.MIN_VALUE, Integer.MAX_VALUE, 1));
        init(columns,0);
    }

    public JNumberSpinner(int currentValue, int columns, int minValue, int maxValue, int step) {
        super(new SpinnerNumberModel(currentValue, minValue, maxValue, step));
        init(columns,0);
    }

    /*
     * @deprecated use {@link #JNumberSpinner(int, int, int, int, int)} instead.
    */
   @Deprecated
   public JNumberSpinner(String currentValue, int columns, int minValue, int maxValue, int step) {
        super(new SpinnerNumberModel());

        Number value;

        if (StringUtils.isBlank(currentValue)) {
            value = null;
        } else {
            try {
                value = new Integer(Integer.parseInt(currentValue));
            } catch (Exception ex) {
                value = null;
            }
        }
        init(
                value == null ? 0 : value,
                columns,
                new Integer(minValue),
                new Integer(maxValue),
                new Integer(step),
                0
        );
        if (value == null) {
            JSpinner.NumberEditor editor = (JSpinner.NumberEditor) this.getEditor();
            JFormattedTextField textField = editor.getTextField();
            textField.setText("");
        }
    }

    public JNumberSpinner(int currentValue, int columns, int minValue, int maxValue, int step, int decimals) {
        super(new SpinnerNumberModel(currentValue, minValue, maxValue, step));
        init(
                columns,
                decimals
        );
    }

    public JNumberSpinner(double currentValue) {
        super(new SpinnerNumberModel(currentValue, Double.NEGATIVE_INFINITY, Double.MAX_VALUE, 1.0));
        init(defaultColumns,4);
    }

    /*
     * @deprecated use {@link #JNumberSpinner(double, int, double, double, double)} instead.
     */
    @Deprecated
    public JNumberSpinner(String currentValue, int columns, double minValue, double maxValue, double step) {
        this(
                currentValue,
                columns,
                minValue,
                maxValue,
                step,
                0
        );
    }

    /*
     * @deprecated use {@link #JNumberSpinner(double currentValue, int columns,
     * double minValue, double maxValue, double step, int decimals)} instead.
     */
    @Deprecated
    public JNumberSpinner(String currentValue, int columns, double minValue, double maxValue, double step, int decimals) {
        super(new SpinnerNumberModel());

        Number value;

        if (StringUtils.isBlank(currentValue)) {
            value = null;
        } else {
            try {
                value = new Double(Double.parseDouble(currentValue));
            } catch (Exception ex) {
                value = null;
            }
        }
        init(
                value == null ? 0.0 : value,
                columns,
                new Double(minValue),
                new Double(maxValue),
                new Double(step),
                decimals
        );
        if (value == null) {
            JSpinner.NumberEditor editor = (JSpinner.NumberEditor) this.getEditor();
            JFormattedTextField textField = editor.getTextField();
            textField.setText("");
        }
    }

    public JNumberSpinner(double currentValue, int columns, double minValue, double maxValue, double step) {
        super(new SpinnerNumberModel(currentValue, minValue, maxValue, step));
        init(columns,4);
    }

    public JNumberSpinner(double currentValue, int columns, double minValue, double maxValue, double step, int decimals) {
        super(new SpinnerNumberModel(currentValue, minValue, maxValue, step));
        init(columns,decimals);
    }

    private void init(int columns,int decimals) {
        JSpinner.NumberEditor editor = (JSpinner.NumberEditor) this.getEditor();
        JFormattedTextField textField = editor.getTextField();
        NumberFormatter formatter = (NumberFormatter) textField.getFormatter();
        DecimalFormat format = editor.getFormat();

        if (columns > 0) {
            textField.setColumns(columns);
        }

        format.setMinimumFractionDigits(decimals);
        formatter.setAllowsInvalid(true);

        this.fireChanges = true;

    }

    @Deprecated
    private void init(Number currentValue, int columns, Comparable minValue, Comparable maxValue, Number step, int decimals) {

        JSpinner.NumberEditor editor = (JSpinner.NumberEditor) this.getEditor();
        JFormattedTextField textField = editor.getTextField();
        NumberFormatter formatter = (NumberFormatter) textField.getFormatter();
        DecimalFormat format = editor.getFormat();
        SpinnerNumberModel model = getNumberModel();

        if (columns > 0) {
            textField.setColumns(columns);
        }
        model.setValue(currentValue);

        // Las siguientes lineas son un chapuza.
        //
        // Si meto un Double/double en setMaximun/setMinimun
        // No deja cambiar el valor asignado desde el UI.
        model.setMaximum(hackValue(maxValue));
        model.setMinimum(hackValue(minValue));
        // Como meta un double en setStepSize tampoco me deja
        // incrementar/decrementar el valor
        model.setStepSize((int)(Math.ceil(step.doubleValue())));
        //
        // Esto es lo que esperaba que deberia ir y no va
        // model.setMaximum(maxValue);
        // model.setMinimum(minValue);
        // model.setStepSize(step);
        //
        // Fin chapuza

        // dump();
        format.setMinimumFractionDigits(decimals);
        formatter.setAllowsInvalid(true);

        this.fireChanges = true;
    }
//
//    private void dump() {
//        SpinnerNumberModel model = getNumberModel();
//        logger.info(
//                "Current="+model.getValue()+" ("+model.getValue().getClass().getName()+"), "+
//                "maxValue="+model.getMaximum()+" ("+model.getMaximum().getClass().getName()+"), "+
//                "minValue="+model.getMinimum()+" ("+model.getMinimum().getClass().getName()+"),"+
//                "step="+model.getStepSize()+" ("+model.getStepSize().getClass().getName()+")"
//        );
//    }

    protected SpinnerNumberModel getNumberModel() {
        return (SpinnerNumberModel) this.getModel();
    }

    private Comparable hackValue(Comparable value) {
        if( !(value instanceof Double) ) {
            return value;
        }
        return ((Number)value).intValue();
    }

    public int getInteger() {
        return getNumberModel().getNumber().intValue();
    }

    public double getDouble() {
        return getNumberModel().getNumber().doubleValue();
    }

    public void setDouble(double v) {
        if (this.fireChanges) {
            this.fireChanges = false;
            try {
                setDouble(v);
            } finally {
                this.fireChanges = true;
            }
            return;
        }
        SpinnerNumberModel model = getNumberModel();
        model.setValue(v);
        //dump();
    }

    public void setInteger(int v) {
        if (this.fireChanges) {
            this.fireChanges = false;
            try {
                setInteger(v);
            } finally {
                this.fireChanges = true;
            }
            return;
        }
        SpinnerNumberModel model = getNumberModel();
        model.setValue(v);
        // dump();
    }

    public double getMaxValue() {
        Number n = (Number) getNumberModel().getMaximum();
        return n.doubleValue();
    }

    public void setMaxValue(double maxValue) {
        if (this.fireChanges) {
            this.fireChanges = false;
            try {
                setMaxValue(maxValue);
            } finally {
                this.fireChanges = true;
            }
            return;
        }
        SpinnerNumberModel model = getNumberModel();

        if (model.getValue() instanceof Double) {
            model.setMaximum(new Double(maxValue));
        } else {
            model.setMaximum(new Integer((int) maxValue));
        }
        // dump();
    }

    public double getMinValue() {
        Number n = (Number) getNumberModel().getMinimum();
        return n.doubleValue();
    }

    public void setMinValue(double minValue) {
        if (this.fireChanges) {
            this.fireChanges = false;
            try {
                setMinValue(minValue);
            } finally {
                this.fireChanges = true;
            }
            return;
        }
        SpinnerNumberModel model = getNumberModel();

        if (model.getValue() instanceof Double) {
            model.setMinimum(new Double(minValue));
        } else {
            model.setMinimum(new Integer((int) minValue));
        }
        // dump();
    }

    public double getStep() {
        Number n = (Number) getNumberModel().getStepSize();
        return n.doubleValue();
    }

    public void setStep(double step) {
        if (this.fireChanges) {
            this.fireChanges = false;
            try {
                setStep(step);
            } finally {
                this.fireChanges = true;
            }
            return;
        }

        SpinnerNumberModel model = getNumberModel();

        if (model.getValue() instanceof Double) {
            model.setStepSize(new Double(step));
        } else {
            model.setStepSize(new Integer((int) step));
        }
        // dump();
    }

    public void addActionListener(final ActionListener listener) {
        // Me guardo los ActionListeners a parte y no los registro
        // directamente en el spinner ya que el cliente de JnumberSpiner
        // espera que solo se invoque a estos listeners por eventos
        // provocados desde el GUI y no por asignar valores a las
        // propiedades del JNumberSpinner
        this.actionListeners.add(listener);
    }

    @Override
    protected void fireStateChanged() {
        // Disparo los evento normales
        super.fireStateChanged();
        if( this.fireChanges ) {
            // Si los cambios no son producidos por asignacion a las propiedades
            // del JNumberSpinner disparo los actionlistener que haya registrados.
            for( int i=0; i<this.actionListeners.size(); i ++ ) {
                ActionListener listener = (ActionListener) this.actionListeners.get(i);
                listener.actionPerformed(new ActionEvent(this, -1, null));
            }
        }
    }


}
