/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans.labelslidertext;

import javax.swing.JFrame;

public class TestLabelSliderTextTable {
	private int                      w      = 375;
	private int                      h      = 150;
	private JFrame                   frame  = new JFrame();
	private LabelSliderTextContainer slider = null;

	public TestLabelSliderTextTable() {
		slider = new LabelSliderTextContainer(0, 255, 255, true, "Longaniza de nombre");
		slider.setDecimal(false);
		slider.setBorder("Cabecera");
		frame.getContentPane().add(slider);
		frame.setSize(w, h);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new TestLabelSliderTextTable();
	}
}