/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.gui.beans;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.gvsig.gui.beans.panelGroup.Test1ExceptionsUsingTabbedPanel;
import org.gvsig.gui.beans.panelGroup.Test1ExceptionsUsingTreePanel;
import org.gvsig.gui.beans.panelGroup.TestPanelGroupLoaderFromList;
import org.gvsig.gui.beans.panelGroup.TestPanelGroupManager;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for org.gvsig.gui.beans");
		//$JUnit-BEGIN$
		suite.addTestSuite(TestPanelGroupLoaderFromList.class);
		suite.addTestSuite(TestPanelGroupManager.class);
		suite.addTestSuite(Test1ExceptionsUsingTabbedPanel.class);
		suite.addTestSuite(Test1ExceptionsUsingTreePanel.class);
		//$JUnit-END$
		return suite;
	}

}
