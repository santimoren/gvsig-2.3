/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.tools.evaluator.sqljep;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.evaluator.EvaluatorFieldsInfo;
import org.gvsig.tools.evaluator.EvaluatorParseException;
import org.gvsig.tools.evaluator.EvaluatorWithDescriptions;
import org.gvsig.tools.evaluator.sqljep.function.Boundary;
import org.gvsig.tools.evaluator.sqljep.function.Equals;
import org.gvsig.tools.evaluator.sqljep.function.GeomFromText;
import org.gvsig.tools.evaluator.sqljep.function.Intersects;
import org.gvsig.tools.evaluator.sqljep.function.Overlaps;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

import org.medfoster.sqljep.ParseException;
import org.medfoster.sqljep.RowJEP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLJEPEvaluator extends RowJEP
implements EvaluatorWithDescriptions, Persistent {

    private static final Logger logger =
        LoggerFactory.getLogger(SQLJEPEvaluator.class); 
    
    public static final String TODATE_FUNCTION_NAME = "to_timestamp"; 

//	private String expresion;
	private EvaluatorData data;
	private boolean not_parsed;
	

	public SQLJEPEvaluator(String expresion) {

		super(expresion);
//		this.expresion = expresion;
		this.data = null;
		not_parsed = true;
	}

	public String getName() {
		return "SQLJEP(" + expression + ")";
	}

	public String getSQL() {
		return expression;
	}

	public String getDescription() {
		return "SQLJEP-based evaluator";
	}

	/*
	public EvaluatorFieldValue[] getFieldValues(String name) {
		return null;
	}
	*/
	
	public Object evaluate(EvaluatorData data) throws EvaluatorException {
		this.data = data;
		Object value = null;
		try {
			if (not_parsed) {
				this.parseExpression();
				not_parsed = false;
			}
		} catch (ParseException e) {
		    /*
		     * Notify parse exception
		     */
		    throw new EvaluatorParseException(e);
		}
		
        try {
            value = this.getValue();
        } catch (Exception e) {
            /*
             * Notify evaluation exception
             * (catching also runtime exceptions)
             */
            throw new EvaluatorException(e);
        }
		this.data = null;
		return value;
	}

	/*
	public int findColumn(String name) {
		return -1;
	}
	*/

	/*
	public Comparable getColumnObject(int arg0) throws ParseException {
		return null;
	}
	*/

	public Entry getVariable(String name) throws ParseException {
		return new MyEntry(name);
	}

        @Override
        public void saveToState(PersistentState ps) throws PersistenceException {
            ps.set("expression", this.expression);
        }

        @Override
        public void loadFromState(PersistentState ps) throws PersistenceException {
            this.clear();
            this.expression = ps.getString("expression");
        }

	private class MyEntry implements Map.Entry {
		private String key;

		public MyEntry(String key) {
			this.key = key;
		}

		public Object getKey() {
			return this.key;
		}

		public Object getValue() {
		    
		    Object resp = null;
		    
			if (data.hasDataValue(key)) {
			    resp = data.getDataValue(key);
			    resp = fixDecimal(resp);
				return resp;
			}
			resp = data.getDataValue(key);;
            resp = fixDecimal(resp);
			return resp;
		}

		/**
         * @param resp
         * @return
         */
        private Object fixDecimal(Object obj) {
            
            if (obj instanceof Double) {
                Double dou = (Double) obj;
                String doustr = Double.toString(dou);
                return new BigDecimal(doustr);
            } else {
                if (obj instanceof Float) {
                    Float flo = (Float) obj;
                    String flostr = Float.toString(flo);
                    return new BigDecimal(flostr);
                } else {
                    return obj;
                }
            }
        }

        public Object setValue(Object arg0) {
			return null;
		}

	}

	public EvaluatorFieldsInfo getFieldsInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	private static Description[] available_operators;
	private static Description[] available_functions;

    public Description[] getAvailableOperators() {
        
        if (available_operators == null) {
            available_operators = new Description[] {
                
                new DefaultDescription(
                    "+",
                    "Returns the sum (addition) of the left operand and"
                    + " the right operand.",
                    " + ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                new DefaultDescription(
                        "-",
                        "Returns the difference (subtraction) of the left operand minus"
                        + " the right operand. It can be used as unary operator to"
                        + " change the sign of the right operand.",
                        " - ",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                new DefaultDescription(
                    "*",
                    "Returns the product (multiplication) of the left operand by"
                    + " the right operand.",
                    " * ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                new DefaultDescription(
                        "/",
                        "Returns the division of the left operand by"
                        + " the right operand.",
                        " / ",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                new DefaultDescription(
                            ">",
                            "Returns TRUE if the left operand is greater than "
                            + "the right operand; FALSE otherwise.",
                            " > ",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                            "<",
                            "Returns TRUE if the left operand is smaller"
                            + " than the right operand; FALSE otherwise.",
                            " < ",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                            ">=",
                            "Returns TRUE if the left operand is greater or "
                            + "equal to the right operand; FALSE otherwise.",
                            " >= ",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "<=",
                    "Returns TRUE if the left operand is less or "
                    + "equal to the right operand; FALSE otherwise.",
                    " <= ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "=",
                    "Returns TRUE if the left operand equals "
                    + "the right operand; FALSE otherwise.",
                    " = ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                        "!=",
                        "Returns TRUE if the left operand is not equal to "
                        + "the right operand; FALSE otherwise.",
                        " != ",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                        | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                        | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                        | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                            "And",
                            "Returns TRUE if both operands are TRUE"
                            + "; FALSE otherwise.",
                            " and <Right_value>",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "Or",
                    "Returns TRUE if the left operand is TRUE or the "
                    + "right operand is TRUE; FALSE otherwise.",
                    " or ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "Not",
                    "Returns the logical inverse of the right operand: TRUE if the right "
                    + "operand is FALSE; FALSE otherwise.",
                    "not ",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "In",
                    "Returns TRUE if the left operand equals one of the comma-separated"
                    + " values; FALSE otherwise.",
                    " in ( <Comma_separated_values> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "Between",
                    "Returns TRUE if the left operand is greater or equal than " +
                    "the first right operand and less or equal than the second " +
                    "right operand; FALSE otherwise.",
                    " between <Right_value_1> and <Right_value_2>",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                        "Like",
                        "Returns TRUE if the left operand matches the pattern expressed " +
                        "by the right operand; FALSE otherwise. Use '%' as wildcard.",
                        " like <Right_value>",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                            "Is Null",
                            "Returns TRUE if the left operand has NULL value;" +
                            " FALSE otherwise.",
                            " is null",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                            | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                                "Is Not Null",
                                "Returns TRUE if the left operand does not have NULL value;" +
                                " FALSE otherwise.",
                                " is not null",
                                EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                                | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                                | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                                | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN)
                            
                       

            };
        }
        return available_operators;
    }


    public Description[] getAvailableFunctions() {

        if (available_functions == null) {
            available_functions = new Description[] {
                
                new DefaultDescription(
                    "Equals",
                    "Returns TRUE if the two input values are equal; "
                    + "FALSE otherwise.",
                    Equals.NAME + "( <Value_1> , <Value_2> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_GEOMETRY),
                new DefaultDescription(
                    GeomFromText.NAME,
                    "Returns the geometry object equivalent to the "
                    + "input WKT string (single quotes).",
                    GeomFromText.NAME + "( <Geometry_in_WKT> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_GEOMETRY),
                new DefaultDescription(
                    "Intersects",
                    "Returns TRUE if the two input geometries intersect; "
                    + "FALSE otherwise.",
                    Intersects.NAME + "( <Geometry_1> , <Geometry_2> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_GEOMETRY),
                new DefaultDescription(
                    "Overlaps",
                    "Returns TRUE if the two input geometries overlap; "
                    + "FALSE otherwise. Geometries overlap when they have some but "
                    + "not all points in common, they have the same dimension, "
                    + "and the intersection of the interiors of the two "
                    + "geometries has the same dimension as the "
                    + "geometries themselves.",
                    Overlaps.NAME + "( <Geometry_1> , <Geometry_2> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_GEOMETRY),
                new DefaultDescription(
                    "Boundary",
                    "Returns a geometry which is the bounding box of the "
                    + "input geometry.",
                    Boundary.NAME + "( <Geometry> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_GEOMETRY),
                new DefaultDescription(
                    "To_timestamp",
                    "Converts the input value to the timestamp type.",
                    "to_timestamp( <Value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                // ===============================================
                // ===============================================
                // ===============================================

                    new DefaultDescription(
                        "Abs",
                        "Returns the absolute value of the input numeric value.",
                        "abs( <Numeric_value> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Power",
                        "Takes two numeric inputs and returns numeric result as "
                        + "first argument raised to second argument.",
                        "power( <Base> , <Exponent> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Mod",
                        "Returns the remainder of the first argument divided by "
                        + "the second argument.",
                        "mod( <Numeric_value_1> , <Numeric_value_2> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Substr",
                        "Returns a substring of the first argument, starting at "
                        + "the character indicated by the second argument. "
                        + "The optional third argument indicates the length "
                        + "of the result.",
                        "substr( <String> , <Start_index> [ , <Length> ] )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                    new DefaultDescription(
                        "Sign",
                        "Returns 1 for positive input values, -1 for negative values"
                        + " and 0 when input is 0.",
                        "sign( <Numeric_value> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Ceil",
                        "Returns the smallest whole number greater than or equal to "
                        + "the input numeric value.",
                        "ceil( <Numeric_value> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Floor",
                        "Returns the largest whole number equal to or less than "
                        + "the input numeric value.",
                        "floor( <Numeric_value> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Trunc",
                        "Returns the first argument truncated to as many decimal "
                        + "places as indicated by the second optional argument "
                        + "(defaults to zero).",
                        "trunc( <Numeric_value> [ , <Integer_value> ] )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Round",
                        "Returns the first argument rounded to as many places "
                        + "to the right of the decimal point as "
                        + "indicated by the second optional argument "
                        + "(defaults to zero).",
                        "round( <Numeric_value> [ , <Integer_value> ] )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER),
                    new DefaultDescription(
                        "Length",
                        "Returns the length (number of characters) of the "
                        + "input string.",
                        "length( <String> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                        "Concat",
                        "Returns the concatenation of the two input strings.",
                        "concat( <String_1> , <String_2> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                            "Instr",
                            "Searches for a string using some parameters (similar to " +
                            "same function in Oracle).",
                            "instr( <String_1> , <String_2> , <Int_1> , <Int_2> )",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                                "Trim",
                                "Removes all leading and trailing spaces from the input" +
                                " string.",
                                "trim( <String> )",
                                EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                                    "Rtrim",
                                    "Removes all trailing (right-hand) spaces from " +
                                    "the input string.",
                                    "Rtrim( <String> )",
                                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Ltrim",
                    "Removes all leading (left-hand) spaces from " +
                    "the input string.",
                    "Ltrim( <String> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Rpad",
                    "Appends <String_2> to <String_1> as many times as needed to reach " +
                    "length <L>. <String_2> defaults to a white space.",
                    "rpad( <String_1> , <L> [ , <String_2> ] )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                        "Lpad",
                        "Prepends <String_2> to <String_1> as many times as needed to reach " +
                        "length <L>. <String_2> defaults to a white space.",
                        "lpad( <String_1> , <L> [ , <String_2> ] )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                            "Upper",
                            "Returns the input string in upper case.",
                            "upper( <String> )",
                            EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                                "Lower",
                                "Returns the input string in lower case.",
                                "lower( <String> )",
                                EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Translate",
                    "Replaces characters following the order of the second and third" +
                    " parameters.",
                    "translate( <String_1> , <String_2> , <String_3> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Replace",
                    "Replaces in <String_1> the occurrences of <String_2>" +
                    " with <String_3>.",
                    "replace( <String_1> , <String_2> , <String_3> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Initcap",
                    "Sets the first character in each word to uppercase and the " +
                    "rest to lowercase.",
                    "initcap( <String> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Value",
                    "Returns the first non-null value from the input values. " +
                    "Can be used to avoid null values in a field.",
                    "value( <Value_1> , <Value_2> ... )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "Decode",
                    "This function works like a IF-THEN-ELSE block.",
                    "decode( <Expression> , <Search> , <Result> ... )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "To_char",
                    "Converts the input value to string.",
                    "to_char( <Value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "To_number",
                    "Converts the input string to a number.",
                    "to_number( <String> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Imatch",
                    "Returns a string representing the phonetic value of the " +
                    "input string. Similar to Soundex function in Oracle.",
                    "imatch( <String> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Case",
                    "This function works like a CASE-WHEN block.",
                    "case( <Expression> ... )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                    
                    
                new DefaultDescription(
                    "Index",
                    "Returns the position of a substring in a string.",
                    "index( <String> , <Substring> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING),
                new DefaultDescription(
                    "Num",
                    "Returns the numeric value of the input string.",
                    "num( <String> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                new DefaultDescription(
                    "Chr",
                    "Converts the input value to string.",
                    "chr( <Value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_NUMBER
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_STRING
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME
                    | EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_BOOLEAN),
                    /*
                     * date
                     */
                new DefaultDescription(
                    "Months_between",
                    "Returns the number of months (as a float-point value) between " +
                    "<Earliest_date> and <Latest_date>.",
                    "months_between( <Latest_date> , <Earliest_date>  )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Add_months",
                    "Adds a number of months to the input date.",
                    "add_months( <Input_date> , <Number_of_months_integer>)",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Last_day",
                    "Returns the date corresponding to the last day of the month " +
                    "of the input date.",
                    "last_day( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Next_day",
                    "Returns the date of the first weekday named by the string " +
                    "<Day_of_the_week> " +
                    "that is later than <Input_date>.",
                    "next_day( <Input_date> , <Day_of_the_week> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "To_date",
                    "Converts the input string to a date following the" +
                    " optional format.",
                    "to_date( <Input_string> [ , <Format> ] )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Adddate",
                    "Returns a date which is <Number_of_days> days after the" +
                    " input date.",
                    "adddate( <Input_date> , <Number_of_days> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Subdate",
                    "Returns a date which is <Number_of_days> days before the" +
                    " input date.",
                    "subdate( <Input_date> , <Number_of_days> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Addtime",
                    "Adds the second input time <Add_time> to the first " +
                    "input time <Input_date>.",
                    "addtime( <Input_time> , <Add_time> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Subtime",
                    "Subtracts the second input time <Sub_time> from the first " +
                    "input time <Input_date>.",
                    "subtime( <Input_time> , <Sub_time> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Year",
                    "Returns a numeric value representing the year of the " +
                    "input date.",
                    "year( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Month",
                    "Returns a numeric value representing the month of the " +
                    "input date.",
                    "month( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Day",
                    "Returns a numeric value representing the day of the " +
                    "input date.",
                    "day( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Dayofmonth",
                    "Returns a numeric value between 1 and 31 corresponding to " +
                    "the day of the month of the input date.",
                    "dayofmonth( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Hour",
                    "Returns a numeric value representing the number " +
                    "of hours of the input time value.",
                    "hour( <Time_value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Minute",
                    "Returns a numeric value representing the number " +
                    "of minutes of the input time value.",
                    "minute( <Time_value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Second",
                    "Returns a numeric value representing the number " +
                    "of seconds of the input time value.",
                    "second( <Time_value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Microsecond",
                    "Returns the number of microseconds of the input " +
                    "timestamp value",
                    "microseconds( <Timestamp_value> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Datediff",
                    "Returns the number of days between the two input dates.",
                    "datediff( <Date_1> , <Date_2> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Dayofweek",
                    "Returns a numeric value between 1 (Monday) and 7 " +
                    "(Sunday) corresponding to " +
                    "the day of the week of the input date.",
                    "dayofweek( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Weekofyear",
                    "Returns a numeric value between 1 and 53 " +
                    "corresponding to" +
                    " the number of the week of the input date.",
                    "weekofyear( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                        "Dayname",
                        "Returns the name of the day corresponding to the input" +
                        " date ('Sunday', 'Monday', etc).",
                        "dayname( <Input_date> )",
                        EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Monthname",
                    "Returns the name of the month corresponding to the input" +
                    " date ('January', 'February', etc).",
                    "monthname( <Input_date> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Makedate",
                    "Returns a date corresponding to the day denoted by " +
                    "<Day_integer> (between 1 and 366) of the year " +
                    "<Year_integer>.",
                    "makedate( <Year_integer> , <Day_integer> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME),
                new DefaultDescription(
                    "Maketime",
                    "Creates a time value with the input number of" +
                    " hours, minutes and seconds.",
                    "maketime( <Hours> , <Minutes> , <Seconds> )",
                    EvaluatorWithDescriptions.Description.DATATYPE_CATEGORY_DATETIME)
                    
            };
        }
        return available_functions;
    }

    // =======================================================
    // =======================================================
    // =======================================================
    
    private class DefaultDescription  implements Description{
        
        private String name, desc, template;
        private int categories;
        
        public DefaultDescription(
            String name, String desc, String template,
            int datatype_categories) {
            
            this.name = name;
            this.desc = desc;
            this.template = template;
            this.categories = datatype_categories;
        }

        /* (non-Javadoc)
         * @see org.gvsig.tools.evaluator.EvaluatorWithDescriptions.Description#getName()
         */
        public String getName() {
            return name;
        }

        /* (non-Javadoc)
         * @see org.gvsig.tools.evaluator.EvaluatorWithDescriptions.Description#getDescription()
         */
        public String getDescription() {
            return desc;
        }

        /* (non-Javadoc)
         * @see org.gvsig.tools.evaluator.EvaluatorWithDescriptions.Description#getTemplate()
         */
        public String getTemplate() {
            return template;
        }

        /* (non-Javadoc)
         * @see org.gvsig.tools.evaluator.EvaluatorWithDescriptions.Description#getDataTypeCategories()
         */
        public int getDataTypeCategories() {
            return this.categories;
        }
        
    }
}
