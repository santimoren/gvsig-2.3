package org.gvsig.tools.evaluator.sqljep.function;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataTypes;
import org.medfoster.sqljep.ASTFunNode;
import org.medfoster.sqljep.BaseJEP;
import org.medfoster.sqljep.JepRuntime;
import org.medfoster.sqljep.ParseException;
import org.medfoster.sqljep.function.PostfixCommand;

public class OpenGISFunctions {

    public static BaseFunction[] functions = {
        new  ST_Distance(),
        new  ST_Disjoint(),
        new  ST_Touches(),
        new  ST_Crosses(),
        new  ST_Within(),
        new  ST_Overlaps(),
        new  ST_Contains(),
        new  ST_Covers(),
        new  ST_CoveredBy(),
        new  ST_Intersects(),
        new  ST_Centroid(),
        new  ST_Area(),
//        new  ST_Length(),
//        new  ST_Boundary(),
        new  ST_Buffer(),
        new  ST_ConvexHull(),
        new  ST_Intersection(),
        new  ST_Difference(),
        new  ST_Union(),
        new  ST_AsText(),
//        new  ST_AsBinary(),
//        new  ST_SRID(),
        new  ST_Dimension(),
        new  ST_Envelope(),
        new  ST_IsSimple(),
//        new  ST_IsRing(),
        new  ST_NumGeometries(),
        new  ST_GeometryN(),
        new  ST_NumPoints(),
        new  ST_PointN(),
        new  ST_StartPoint(),
        new  ST_EndPoint(),
        new  ST_X(),
        new  ST_Y(),
        new  ST_Z(),
        new  ST_GeomFromText(),
        new  ST_isValid()
    };
    
    public static void registerFunctions() {
      for( int i=0; i<functions.length; i++ ) {
          BaseFunction f = functions[i];
          BaseJEP.addFunction(f.getName(),f );
      } 
    }
    
    
    public static abstract class BaseFunction extends PostfixCommand {

        public String getName() {
            String name = this.getClass().getName();
            int index = name.lastIndexOf(".");
            if ( index < 1 ) {
                return name;
            }
            return name.substring(index + 1);
        }

        public Geometry pop_geometry(JepRuntime runtime)
                throws ParseException {
            return (Geometry) runtime.stack.pop();
        }

        public double pop_double(JepRuntime runtime)
                throws ParseException {
            Double value = null;
            try {
                value = (Double) ToolsLocator.getDataTypesManager().coerce(
                        DataTypes.DOUBLE, runtime.stack.pop()
                );
            } catch (CoercionException ex) {
                throw new ParseException(this.getName(), ex);
            }
            return value.doubleValue();
        }

        public int pop_int(JepRuntime runtime)
                throws ParseException {
            Integer value = null;
            try {
                value = (Integer) ToolsLocator.getDataTypesManager().coerce(
                        DataTypes.INT, runtime.stack.pop()
                );
            } catch (CoercionException ex) {
                throw new ParseException(this.getName(), ex);
            }
            return value.intValue();
        }

        public String pop_string(JepRuntime runtime)
                throws ParseException {
            String value = null;
            try {
                value = (String) ToolsLocator.getDataTypesManager().coerce(
                        DataTypes.STRING, runtime.stack.pop()
                );
            } catch (CoercionException ex) {
                throw new ParseException(this.getName(), ex);
            }
            return value;
        }

        public void evaluate(ASTFunNode node, JepRuntime runtime)
                throws ParseException {
            try {
                Comparable value = this.evaluate(runtime);
                runtime.stack.push(value);
            } catch (Exception ex) {
                throw new ParseException(getName(), ex);
            }
        }

        public abstract Comparable evaluate(JepRuntime runtime) throws Exception;
    }

    public static abstract class Function_geom1_geom2 extends BaseFunction {

        final public int getNumberOfParameters() {
            return 2;
        }

        public Comparable evaluate(JepRuntime runtime)
                throws Exception {
            Geometry geom1 = pop_geometry(runtime);
            Geometry geom2 = pop_geometry(runtime);
            return evaluate(geom1, geom2);
        }

        public abstract Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception;
    }

    public static abstract class Function_geom1 extends BaseFunction {

        final public int getNumberOfParameters() {
            return 1;
        }

        public Comparable evaluate(JepRuntime runtime)
                throws Exception {
            Geometry geom1 = pop_geometry(runtime);
            return evaluate(geom1);
        }

        public abstract Comparable evaluate(Geometry geom1) throws Exception;
    }

    public static abstract class Function_geom1_double extends BaseFunction {

        final public int getNumberOfParameters() {
            return 2;
        }

        public Comparable evaluate(JepRuntime runtime)
                throws Exception {
            Geometry geom1 = pop_geometry(runtime);
            double value = pop_double(runtime);
            return evaluate(geom1, value);
        }

        public abstract Comparable evaluate(Geometry geom1, double value) throws Exception;
    }

    public static abstract class Function_geom1_int extends BaseFunction {

        final public int getNumberOfParameters() {
            return 2;
        }

        public Comparable evaluate(JepRuntime runtime)
                throws Exception {
            Geometry geom1 = pop_geometry(runtime);
            int value = pop_int(runtime);
            return evaluate(geom1, value);
        }

        public abstract Comparable evaluate(Geometry geom1, int value) throws Exception;
    }

    public static abstract class Function_string extends BaseFunction {

        final public int getNumberOfParameters() {
            return 1;
        }

        public Comparable evaluate(JepRuntime runtime)
                throws Exception {
            String value = pop_string(runtime);
            return evaluate(value);
        }

        public abstract Comparable evaluate(String value) throws Exception;
    }

    public static class ST_Distance extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Double.valueOf(geom1.distance(geom2));
        }
    }

    public static class ST_Disjoint extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.disjoint(geom2));
        }
    }

    public static class ST_Touches extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.touches(geom2));
        }
    }

    public static class ST_Crosses extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.crosses(geom2));
        }
    }

    public static class ST_Within extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.within(geom2));
        }
    }

    public static class ST_Overlaps extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.overlaps(geom2));
        }
    }

    public static class ST_Contains extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.contains(geom2));
        }
    }

    public static class ST_Covers extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.covers(geom2));
        }
    }

    public static class ST_CoveredBy extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.coveredBy(geom2));
        }
    }

    public static class ST_Intersects extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return Boolean.valueOf(geom1.intersects(geom2));
        }
    }

    public static class ST_Centroid extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return geom1.centroid();
        }
    }

    public static class ST_Area extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return Double.valueOf(geom1.area());
        }
    }

//    public static class ST_Length extends Function_geom1 {
//
//        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
//            return Double.valueOf(geom1.length());
//        }
//    }
//    public static class ST_Boundary extends Function_geom1 {
//
//        public Comparable evaluate(Geometry geom1) throws Exception {
//            return geom1.boundary();
//        }
//    }
    public static class ST_Buffer extends Function_geom1_double {

        public Comparable evaluate(Geometry geom1, double value) throws Exception {
            return geom1.buffer(value);
        }
    }

    public static class ST_ConvexHull extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return geom1.convexHull();
        }
    }

    public static class ST_Intersection extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return geom1.intersection(geom2);
        }
    }

    public static class ST_Difference extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return geom1.difference(geom2);
        }
    }

    public static class ST_Union extends Function_geom1_geom2 {

        public Comparable evaluate(Geometry geom1, Geometry geom2) throws Exception {
            return geom1.union(geom2);
        }
    }

    public static class ST_AsText extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return geom1.convertToWKT();
        }
    }

//    public static class ST_AsBinary extends Function_geom1 {
//
//        public Comparable evaluate(Geometry geom1) throws Exception {
//            byte[] bytes = geom1.convertToWKB();
//            return bytes;
//        }
//    }
//    public static class ST_SRID extends Function_geom1 {
//
//        public Comparable evaluate(Geometry geom1) throws Exception {
//            return geom1.getCRS();
//        }
//    }
    public static class ST_Dimension extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            GeometryType type = geom1.getGeometryType();
            if ( type.isSubTypeOf(Geometry.TYPES.POINT)
                    || type.isSubTypeOf(Geometry.TYPES.MULTIPOINT) ) {
                return 0;
            }
            if ( type.isSubTypeOf(Geometry.TYPES.CURVE)
                    || type.isSubTypeOf(Geometry.TYPES.MULTICURVE) ) {
                return 1;
            }
            if ( type.isSubTypeOf(Geometry.TYPES.SURFACE)
                    || type.isSubTypeOf(Geometry.TYPES.MULTISURFACE) ) {
                return 2;
            }
            if ( type.isSubTypeOf(Geometry.TYPES.SOLID) ) {
                return 3;
            }
            return -1;
        }
    }

    public static class ST_Envelope extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return geom1.getEnvelope().getGeometry();
        }
    }

    public static class ST_IsSimple extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return geom1.isSimple();
        }
    }

//    public static class ST_IsRing extends Function_geom1 {
//        public Comparable evaluate(Geometry geom1) throws Exception {
//            return geom1.isClosed();
//        }
//    }
    public static class ST_NumGeometries extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            if ( geom1 instanceof Aggregate ) {
                int n = ((Aggregate) geom1).getPrimitivesNumber();
                return Integer.valueOf(n);
            }
            return null;
        }
    }

    public static class ST_GeometryN extends Function_geom1_int {

        public Comparable evaluate(Geometry geom1, int value) throws Exception {
            if ( geom1 instanceof Aggregate ) {
                return ((Aggregate) geom1).getPrimitiveAt(value);
            }
            return null;
        }
    }

    public static class ST_NumPoints extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            if ( geom1 instanceof OrientablePrimitive ) {
                int n = ((OrientablePrimitive) geom1).getNumVertices();
                return Integer.valueOf(n);
            }
            return null;
        }
    }

    public static class ST_PointN extends Function_geom1_int {

        public Comparable evaluate(Geometry geom1, int value) throws Exception {
            if ( geom1 instanceof OrientablePrimitive ) {
                return ((OrientablePrimitive) geom1).getVertex(value);
            }
            return null;
        }
    }

    public static class ST_StartPoint extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            if ( geom1 instanceof OrientablePrimitive ) {
                return ((OrientablePrimitive) geom1).getVertex(0);
            }
            return null;
        }
    }

    public static class ST_EndPoint extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            if ( geom1 instanceof OrientablePrimitive ) {
                int n = ((OrientablePrimitive) geom1).getNumVertices();
                return ((OrientablePrimitive) geom1).getVertex(n - 1);
            }
            return null;
        }
    }

    public static class ST_X extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return ((Point) geom1).getX();
        }
    }

    public static class ST_Y extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return ((Point) geom1).getX();
        }
    }

    public static class ST_Z extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            Point p = (Point) geom1;
            if ( p.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3D ) {
                return Double.valueOf(p.getCoordinateAt(2));
            }
            return null;
        }
    }

    public static class ST_GeomFromText extends Function_string {

        public Comparable evaluate(String value) throws Exception {
            GeometryManager manager = GeometryLocator.getGeometryManager();
            return manager.createFrom(value);
        }
    }

    public static class ST_isValid extends Function_geom1 {

        public Comparable evaluate(Geometry geom1) throws Exception {
            return Boolean.valueOf(geom1.isValid());
        }
    }

}
