/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA  02110-1301, USA.
*
*/
package org.gvsig.tools.evaluator.sqljep;

import org.medfoster.sqljep.BaseJEP;
import org.medfoster.sqljep.function.ToDate;

import org.gvsig.fmap.geom.GeometryLibrary;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.evaluator.sqljep.function.Boundary;
import org.gvsig.tools.evaluator.sqljep.function.Equals;
import org.gvsig.tools.evaluator.sqljep.function.GeomFromText;
import org.gvsig.tools.evaluator.sqljep.function.Intersects;
import org.gvsig.tools.evaluator.sqljep.function.OpenGISFunctions;
import org.gvsig.tools.evaluator.sqljep.function.Overlaps;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class SQLJEPLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(ToolsLibrary.class);
        require(GeometryLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		BaseJEP.addFunction(Equals.NAME, new Equals());
		BaseJEP.addFunction(GeomFromText.NAME, new GeomFromText());
		BaseJEP.addFunction(Intersects.NAME, new Intersects());
		BaseJEP.addFunction(Overlaps.NAME, new Overlaps());
		BaseJEP.addFunction(Boundary.NAME, new Boundary());
		BaseJEP.addFunction(SQLJEPEvaluator.TODATE_FUNCTION_NAME, new ToDate());
                OpenGISFunctions.registerFunctions();
	}
}
