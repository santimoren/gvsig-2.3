/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.main.execution;

import java.io.File;

import javax.swing.JFrame;

import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.main.utils.FrameWizardListener;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.api.execution.InstallPackageWizardException;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallPackageServiceFrame extends JFrame {

	private static final long serialVersionUID = -5107758157530922356L;
	private AbstractInstallPackageWizard installerExecutionWizard;

	public InstallPackageServiceFrame(File applicationFolder, File installFolder)
			throws LocatorException, InstallPackageWizardException {
		super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		installerExecutionWizard = SwingInstallerLocator
				.getSwingInstallerManager().createInstallPackageWizard(
						applicationFolder, installFolder);
		installerExecutionWizard
				.setWizardActionListener(new FrameWizardListener(this));
		this.add(installerExecutionWizard);
		pack();
	}

	public void installFromDefaultDirectory()
			throws InstallPackageServiceException {
		installerExecutionWizard.installFromDefaultDirectory();
	}

}
