/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.main.utils;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.gvsig.gui.beans.openfile.FileTextField;
import org.gvsig.installer.lib.impl.utils.Compress;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class CreateBoundleFrame extends JFrame implements WindowListener,
		ActionListener {

	/**
     * 
     */
	private static final long serialVersionUID = -2128261599641241144L;
	private javax.swing.JButton executeButton;
	private javax.swing.JLabel inputLabel;
	private FileTextField inputText;
	private javax.swing.JLabel jLabel2;
	private FileTextField outputText;

	public CreateBoundleFrame() throws HeadlessException {
		super();
		initializeComponents();
		this.addWindowListener(this);
		executeButton.addActionListener(this);
	}

	private void initializeComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		inputLabel = new javax.swing.JLabel();
		executeButton = new javax.swing.JButton();
		inputText = new FileTextField();
		outputText = new FileTextField();
		jLabel2 = new javax.swing.JLabel();

		setLayout(new java.awt.GridBagLayout());

		inputLabel.setText("Input directory");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(inputLabel, gridBagConstraints);

		executeButton.setText("Execute");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(executeButton, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.ipadx = 67;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(inputText, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.ipadx = 67;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(outputText, gridBagConstraints);

		jLabel2.setText("Output");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(jLabel2, gridBagConstraints);

	}

	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void windowClosed(WindowEvent arg0) {
		System.exit(0);

	}

	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void actionPerformed(ActionEvent arg0) {
		File inputDirectory = inputText.getSelectedFile();
		File outputFile = outputText.getSelectedFile();

		if (!inputDirectory.exists()) {
			JOptionPane
					.showMessageDialog(this, "Input directory doesn't exist");
			return;
		}

		if (outputFile.exists()) {
			outputFile.delete();
		}

		Compress compress = new Compress();
		try {
			File[] files = inputDirectory.listFiles();
			List<File> filesArray = new ArrayList<File>();
			List<String> fileNamesArray = new ArrayList<String>();
			for (int i = 0; i < files.length; i++) {
				if (!files[i].getName().toUpperCase().equals(".SVN")) {
					filesArray.add(files[i]);
					fileNamesArray.add(files[i].getName());
				}
			}
			compress.compressPluginsAsPackageSet(filesArray, fileNamesArray,
					new FileOutputStream(outputFile));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.toString());
		}
	}
}
