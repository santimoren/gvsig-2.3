/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.main.utils;

import javax.swing.JFrame;

import org.gvsig.installer.swing.api.wizard.InstallerWizardActionListener;
import org.gvsig.installer.swing.api.wizard.InstallerWizardPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class FrameWizardListener implements InstallerWizardActionListener {

	private JFrame frame = null;

	public FrameWizardListener(JFrame frame) {
		super();
		this.frame = frame;
	}

	public void cancel(InstallerWizardPanel installerWizard) {
		frame.setVisible(false);
		System.exit(0);

	}

	public void finish(InstallerWizardPanel installerWizard) {
		frame.setVisible(false);
		System.exit(0);
	}

}
