/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */
package org.gvsig.installer.prov.plugin.execution;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.installer.lib.spi.InstallerProviderManager;
import org.gvsig.installer.lib.spi.execution.InstallPackageProvider;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sardak.antform.AntForm;
import com.sardak.antform.AntMenu;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PluginInstallerExecutionProvider extends AbstractProvider
        implements InstallPackageProvider {

    private static Logger logger = LoggerFactory.getLogger(PluginInstallerExecutionProvider.class);
    private static final String ANT_FILE_NAME = "install.xml";

    public PluginInstallerExecutionProvider(ProviderServices providerServices) {
        super(providerServices);
    }

    public class InstallerPluginsDirectoryNotFoundException extends
            InstallPackageServiceException {

        private static final long serialVersionUID = 4416143986837955880L;

        private static final String message = "Plugins directory not found";

        private static final String KEY = "plugins_directory_not_found";

        public InstallerPluginsDirectoryNotFoundException() {
            super(message, KEY, serialVersionUID);
        }

    }

    public void install(File applicationDirectory, InputStream inputStream,
            PackageInfo packageInfo) throws InstallPackageServiceException {

        InstallerProviderManager installerProviderManager = InstallerProviderLocator.getProviderManager();
        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        
        File pluginsDirectory = installerManager.getDefaultLocalAddonRepository(
                "plugin", 
                InstallerManager.ACCESS_WRITE
        );

        logger.info("Installing package '" + packageInfo.getCode() + "' in '" + pluginsDirectory + "'.");
        try {
            if ( !pluginsDirectory.exists() ) {
                logger.warn("Can install package '" + packageInfo.getCode() + "', install folder '" + pluginsDirectory + "' does not exists");
                throw new InstallerPluginsDirectoryNotFoundException();
            }

            InstallPackageProviderServices installerProviderServices = installerProviderManager
                    .createInstallerProviderServices();

            installerProviderServices.decompress(inputStream, pluginsDirectory);
            File antFile = new File(pluginsDirectory + File.separator
                    + packageInfo.getCode() + File.separator + "install"
                    + File.separator + ANT_FILE_NAME);

            if ( antFile.exists() ) {
                executeAntFile(applicationDirectory, pluginsDirectory, antFile);
            }

        } catch (Exception e) {
            try {
                logger.warn("Can install package '" + packageInfo.getCode() + "'.", e);
                // if there is an exception, installLater is called
                installLater(applicationDirectory, inputStream, packageInfo);
            } catch (IOException e1) {
                logger.warn("Can install package '" + packageInfo.getCode() + "'.", e1);
                throw new InstallPackageServiceException(e1);
            }
        }
    }

    /**
     * This function will is called when an exception is produced during the
     * installation of a plugin. It will prepare the package to be installed on
     * the next gvSIG restart.
     */
    public void installLater(File applicationDirectory,
            InputStream inputStream, PackageInfo packageInfo)
            throws InstallPackageServiceException, IOException {

        InstallerProviderManager installerProviderManager = InstallerProviderLocator.getProviderManager();
        File decompressDir = null;
        
        logger.info("Delayed the package installation (" + packageInfo.getCode() + ").");

        File updateDirectory = new File(applicationDirectory + File.separator
                + "update");

        if ( !updateDirectory.exists() ) {
            forceMkdir(updateDirectory);
        }

        File filesDirectory = new File(updateDirectory + File.separator
                + "files");
        if ( !filesDirectory.exists() ) {
            forceMkdir(filesDirectory);
        }

        File scriptsFile = new File(updateDirectory + File.separator
                + "scripts.lst");

        decompressDir = new File(filesDirectory.toString() + File.separator
                + packageInfo.getCode());
        if ( decompressDir.exists() ) {
            deleteDir(decompressDir);
        }

        InstallPackageProviderServices installerProviderServices = installerProviderManager
                .createInstallerProviderServices();
        installerProviderServices.decompress(inputStream, filesDirectory);

        File antFile = new File(decompressDir + File.separator + "install"
                + File.separator + ANT_FILE_NAME);

        if ( antFile.exists() ) {
            if ( !scriptsFile.exists() ) {
                forceMkdir(scriptsFile);
                scriptsFile.createNewFile();
            }
            FileWriter writer = new FileWriter(scriptsFile, true);
            BufferedWriter out = new BufferedWriter(writer);
            String str = antFile.toString();
            out.append(str + "\n");
            out.close();
        }

    }

    private void forceMkdir(File file) throws IOException {
        if ( file.isDirectory() ) {
            org.apache.commons.io.FileUtils.forceMkdir(file);
        } else {
            org.apache.commons.io.FileUtils.forceMkdir(file.getParentFile());
        }
    }

    private void executeAntFile(File applicationDirectory, File pluginsDirectory, File file) {
        logger.info("Running install script '" + file.getAbsolutePath() + "'.");
        try {
            Project p = new Project();
            p.setUserProperty("ant.file", file.getAbsolutePath());
            p.setUserProperty("gvsig_dir", applicationDirectory.getAbsolutePath());
            p.setUserProperty("extensions_dir", pluginsDirectory.getAbsolutePath());
            p.setBaseDir(file.getParentFile());
            p.addTaskDefinition("antform", AntForm.class);
            p.addTaskDefinition("antmenu", AntMenu.class);
            p.init();
            ProjectHelper helper = ProjectHelper.getProjectHelper();
            p.addReference("ant.projectHelper", helper);
            helper.parse(p, file);
            p.executeTarget(p.getDefaultTarget());
        } catch (Exception ex) {
            logger.warn("Problems executing installation script.", ex);
        }
    }

    private boolean deleteDir(File dir) {
        if ( dir.isDirectory() ) {
            String[] children = dir.list();
            for ( int i = 0; i < children.length; i++ ) {
                boolean success = deleteDir(new File(dir, children[i]));
                if ( !success ) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }

}
