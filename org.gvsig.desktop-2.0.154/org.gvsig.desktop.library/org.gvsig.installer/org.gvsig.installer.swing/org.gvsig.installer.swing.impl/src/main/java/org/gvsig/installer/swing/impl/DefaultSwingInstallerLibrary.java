/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl;

import java.util.Locale;

import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLibrary;
import org.gvsig.installer.swing.api.SwingInstallerLibrary;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultSwingInstallerLibrary extends AbstractLibrary {

	@Override
	public void doRegistration() {
		registerAsImplementationOf(SwingInstallerLibrary.class);
		require(InstallerLibrary.class);
	}

	@Override
	protected void doInitialize() throws LibraryException {
		SwingInstallerLocator
				.registerSwingInstallerManager(DefaultSwingInstallerManager.class);
		if (!Messages.hasLocales()) {
			Messages.addLocale(Locale.getDefault());
		}
		Messages.addResourceFamily("org.gvsig.installer.swing.impl.text",
				DefaultSwingInstallerLibrary.class.getClassLoader(),
				DefaultSwingInstallerLibrary.class.getClass().getName());
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		SwingInstallerManager swingInstallerManager = SwingInstallerLocator
				.getSwingInstallerManager();

		swingInstallerManager
				.registerMakePluginPackageWizardInstallerCreationWizard(DefaultMakePluginPackageWizard.class);
		swingInstallerManager
				.registerInstallPackageWizard(DefaultInstallPackageWizard.class);
	}
}
