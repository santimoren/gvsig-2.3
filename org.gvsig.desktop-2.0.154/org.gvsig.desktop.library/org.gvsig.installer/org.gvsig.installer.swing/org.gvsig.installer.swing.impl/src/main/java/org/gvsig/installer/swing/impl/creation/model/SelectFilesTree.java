/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreeSelectionModel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectFilesTree extends JTree {

	/**
     * 
     */
	private static final long serialVersionUID = 9167217334192981460L;

	public SelectFilesTree() {
		super();
	}

	public void setApplicationDirectory(File applicationDirectory) {
		setModel(new SelectFilesTreeModel(applicationDirectory));
		setCellRenderer(new SelectFilesTreeCellRenderer());
		getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		addMouseListener(new SelectFilesTreeNodeSelectionListener(this));
	}

	public List<File> getSelectedFiles() {
		List<File> selectedFiles = new ArrayList<File>();
		addSelectedFiles((SelectFilesTreeCheckNode) getModel().getRoot(),
				selectedFiles);
		return selectedFiles;
	}

	public void addSelectedFiles(SelectFilesTreeCheckNode node,
			List<File> selectedFiles) {

		if (node.isSelected) {
			File absolutePath = new File(node.getFile().getAbsolutePath());
			if (!absolutePath.isDirectory()) {
				selectedFiles.add(absolutePath);
			} else {
				traverseChildren(node, selectedFiles);
			}
		} else {
			if (node.getFile().isDirectory()) {
				for (int i = 0; i < node.getChildCount(); i++) {
					addSelectedFiles((SelectFilesTreeCheckNode) node
							.getChildAt(i), selectedFiles);
				}
				traverseChildren(node, selectedFiles);
			}
		}
	}

	protected void traverseChildren(SelectFilesTreeCheckNode node,
			List<File> selectedFiles) {
		for (int i = 0; i < node.getChildCount(); i++) {
			addSelectedFiles((SelectFilesTreeCheckNode) node.getChildAt(i),
					selectedFiles);
		}

	}

}
