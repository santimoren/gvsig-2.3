/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.model;

import java.awt.Component;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.impl.creation.panel.SelectPluginToInstallPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PluginListCellRenderer extends JCheckBox implements
		ListCellRenderer {

	/**
     * 
     */
	private static final long serialVersionUID = -3727657020492852977L;
	private SelectPluginToInstallPanel selectPluginToInstallPanel;

	public PluginListCellRenderer(
			SelectPluginToInstallPanel selectPluginToInstallPanel) {
		this.selectPluginToInstallPanel = selectPluginToInstallPanel;
	}

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
            
		PackageInfo installerInfo = (PackageInfo) value;
                if( this.selectPluginToInstallPanel.getShowFullPaths() ) {
                    InstallerManager manager = InstallerLocator.getInstallerManager();
                    File pathname = manager.getAddonFolder(installerInfo.getCode());
                    setText(installerInfo.getCode() + " ("+pathname.getAbsolutePath()+")");
                } else {
                    setText(installerInfo.getCode());
                }
		setSelected(isSelected);
		if (isSelected) {
			selectPluginToInstallPanel.setSelectedInstallerInfo(installerInfo);
		}
		return this;

	}

}
