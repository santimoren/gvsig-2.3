/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.renderers;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageOfficialRecommended;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class OfficialRecommendedCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1195015856704835320L;

	private DefaultSwingInstallerManager swingInstallerManager;

	public OfficialRecommendedCellRenderer() {
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		Icon icon;
		PackageOfficialRecommended status = (PackageOfficialRecommended) value;
		JLabel check = new JLabel();
		URL resource;

		switch (status) {

		case OFFICIAL_RECOMMENDED:
			resource = this.getClass().getResource(
					"/images/officialRecommended.png");
			icon = new ImageIcon(resource);
			check.setToolTipText(swingInstallerManager
					.getText("_official_recommended"));
			check.setIcon(icon);

			return check;

		case OFFICIAL_NOT_RECOMMENDED:
			resource = this.getClass().getResource(
					"/images/officialNotRecommended.png");
			icon = new ImageIcon(resource);
			check.setToolTipText(swingInstallerManager.getText("_official"));
			check.setIcon(icon);

			return check;

		case NOT_OFFICIAL_RECOMMENDED:
			resource = this.getClass().getResource(
					"/images/notOfficialRecommended.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_not_official_recommended"));
			return check;

		case NOT_OFFICIAL_NOT_RECOMMENDED:
			resource = this.getClass().getResource("/images/notOfficial.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check
					.setToolTipText(swingInstallerManager
							.getText("_not_official"));
			return check;

		default:
			// no image as default
			return check;

		}
	}

}
