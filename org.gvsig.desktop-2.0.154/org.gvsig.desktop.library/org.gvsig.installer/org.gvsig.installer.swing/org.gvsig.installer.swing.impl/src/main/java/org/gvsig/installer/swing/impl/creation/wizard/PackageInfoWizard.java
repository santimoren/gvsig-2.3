/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.wizard;

import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.panel.DefaultPackageInfoPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PackageInfoWizard extends DefaultPackageInfoPanel implements
		OptionPanel {

	private static final long serialVersionUID = 6880987804806802863L;

	private DefaultMakePluginPackageWizard installerCreationWizard;

	public PackageInfoWizard(
			DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_package_description");
	}

	public void lastPanel() {
		// TODO Auto-generated method stub
	}

	public void nextPanel() throws NotContinueWizardException {
		if (!this.validatePanel()) {
			throw new NotContinueWizardException("", null, false);
		}
		PackageInfo installerInfo = installerCreationWizard
				.getSelectedPackageInfo();
		this.panelToPackageInfo(installerInfo);
	}

	public void updatePanel() {
		PackageInfo installerInfo = installerCreationWizard
				.getSelectedPackageInfo();
		this.packageInfoToPanel(installerInfo);
	}

}
