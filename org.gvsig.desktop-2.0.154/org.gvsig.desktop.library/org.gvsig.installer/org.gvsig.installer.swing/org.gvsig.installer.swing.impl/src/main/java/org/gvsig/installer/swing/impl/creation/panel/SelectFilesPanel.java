/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.creation.model.SelectFilesTree;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectFilesPanel extends JPanel {

	/**
     * 
     */
	private static final long serialVersionUID = 2281611103908058218L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JScrollPane resourcesScrollPane;
	private SelectFilesTree resourcesTree;

	public SelectFilesPanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	public void setApplicationDirectory(File pluginsDirectory) {
		resourcesTree.setApplicationDirectory(pluginsDirectory);
	}

	private void initComponents() {
		GridBagConstraints gridBagConstraints;

		resourcesScrollPane = new JScrollPane();
		resourcesTree = new SelectFilesTree();

		setLayout(new GridBagLayout());

		resourcesScrollPane.setViewportView(resourcesTree);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		add(resourcesScrollPane, gridBagConstraints);
	}

	public List<File> getSelectedFiles() {
		return resourcesTree.getSelectedFiles();
	}
}
