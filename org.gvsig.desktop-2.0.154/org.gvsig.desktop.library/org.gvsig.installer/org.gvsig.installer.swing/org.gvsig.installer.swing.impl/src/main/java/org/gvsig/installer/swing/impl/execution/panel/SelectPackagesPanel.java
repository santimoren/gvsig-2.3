/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel;
import org.gvsig.installer.swing.impl.execution.wizard.SelectPackagesWizard;

/**
 * @author gvSIG Team
 * @version $Id$
 */
public class SelectPackagesPanel extends JPanel {

	private SelectPackagesWizard wizard;
	private static final long serialVersionUID = -7554097983061858479L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private PackagesTablePanel packagesTablePanel;

	private PackagesTableModel pluginsTableModel = null;

	public SelectPackagesPanel(SelectPackagesWizard selectPackagesWizard) {
		super();
		this.wizard = selectPackagesWizard;
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	public JPanel getJPanel() {
		return this;
	}

	public void updateTableModel(PackagesTableModel pluginsTableModel) {
		packagesTablePanel.setTableModel(pluginsTableModel);
		add(packagesTablePanel, BorderLayout.CENTER);
	}

	public void selectPackages() {
		packagesTablePanel.selectPackages();
	}

	private void initComponents() {
		setLayout(new BorderLayout(0, 0));

		packagesTablePanel = new PackagesTablePanel(this);
		add(packagesTablePanel, BorderLayout.CENTER);
	}

	public List<PackageInfo> getPackagesToInstall() {

		List<PackageInfo> packages = packagesTablePanel.getPackagesToInstall();

		List<PackageInfo> packagesToInstall = new ArrayList<PackageInfo>();
		packagesToInstall.addAll(packages);
		return packagesToInstall;
	}

	public boolean isPackageSelected() {
		return packagesTablePanel.isPackageSelected();
	}

	public void checkIfPluginSelected() {
		wizard.checkIfPluginSelected();
	}

	public void packageSelectionChanged(Object value, int row, int column) {
	}

	public void updatePanel() {

		InstallPackageService installerExecutionService = wizard
				.getDefaultInstallPackageWizard()
				.getInstallerExecutionService();

		if (pluginsTableModel == null) {
			pluginsTableModel = new PackagesTableModel(swingInstallerManager,
					installerExecutionService, true);
		}
		pluginsTableModel.updatePackages();

		updateTableModel(pluginsTableModel);

		if (wizard.isDefaultPackagesSelectionSet()) {
			pluginsTableModel.selectDefaultPackages();
		}
		
		
		
		

	}

	public DefaultInstallPackageWizard getDefaultInstallPackageWizard() {
		return wizard.getDefaultInstallPackageWizard();
	}

	public PackagesTableModel getModel() {
		return this.pluginsTableModel;
	}

	public void clearPanel() {
		packagesTablePanel.clearAllPanels();
	}

    /**
     * 
     */
    public void setInitialFilter() {
        packagesTablePanel.setInitialFilter();
        
    }

}
