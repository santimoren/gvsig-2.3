/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.installer.swing.impl.packagebuilder.options;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.creation.MakePackageService;
import org.gvsig.installer.lib.api.creation.MakePackageServiceException;
import org.gvsig.installer.swing.api.JProgressPanel;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.packagebuilder.BasePackageWizard;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.task.SimpleTaskStatus;

public class ProgressOption implements OptionPanel {

    private static final Logger logger = LoggerFactory.getLogger(ProgressOption.class);
    private final BasePackageWizard wizard;
    private final JProgressPanel progressPanel;

    public ProgressOption(BasePackageWizard wizard) {
        super();
        this.wizard = wizard;
        progressPanel =
            SwingInstallerLocator.getSwingInstallerManager()
                .createProgressPanel();
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

    @Override
    public JPanel getJPanel() {
        return this.progressPanel;
    }

    @Override
    public String getPanelTitle() {
        return Messages.getText("_Installer_progress");
    }

    @Override
    public void lastPanel() {
        wizard.setFinishButtonEnabled(false);
        wizard.setCancelButtonEnabled(true);
    }

    @Override
    public void nextPanel() {
        // Do nothing
    }

    @Override
    public void updatePanel() {

        OutputStream outputStream =
            openFileOutputStream(wizard.getPackageFile());
        if (outputStream == null) {
            return;
        }
        OutputStream indexOutputStream = null;
        if (wizard.shouldCreateIndex()) {
            indexOutputStream =
                openFileOutputStream(wizard.getPackageIndexFile());
            if (indexOutputStream == null) {
                return;
            }
        }

        wizard.setFinishButtonEnabled(false);
        wizard.setCancelButtonEnabled(false);

        SimpleTaskStatus taskStatus =
            ToolsLocator.getTaskStatusManager().createDefaultSimpleTaskStatus(
                wizard.getPackageInfo().getName());

        this.progressPanel.bind(taskStatus);

        try {
            String downloadURL = wizard.getPackageInfo().getDownloadURLAsString();
            wizard.getPackageInfo().setDownloadURL("");
            
            MakePackageService makePackageService =
                InstallerLocator.getInstallerManager().createMakePackage(
                    wizard.getFolderToPackaging(), wizard.getPackageInfo());

            taskStatus.message(getText("_Compressing"));

            makePackageService.preparePackage();
            makePackageService.createPackage(outputStream);
            if (indexOutputStream != null) {
                taskStatus.message(getText("_Creating_index"));
                makePackageService.createPackageIndex(
                        wizard.getDownloadURL(), indexOutputStream);
            }
            
            wizard.getPackageInfo().setDownloadURL(downloadURL);
            taskStatus.terminate();

            // Set the finished text
            taskStatus.message(getText("_Finished"));
        } catch (MakePackageServiceException e) {
            logger.info("Error while creating package.", e);
            JOptionPane.showMessageDialog(null, getText("_cant_create_the_package")
                + "\n\n" + e.getLocalizedMessageStack(),
                getText("_create_package"), JOptionPane.ERROR_MESSAGE);
        }
        wizard.setFinishButtonEnabled(true);
        wizard.setCancelButtonEnabled(false);
    }

    private OutputStream openFileOutputStream(File file) {
        OutputStream os;

        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e1) {
            JOptionPane.showMessageDialog(null,
                getText("_cant_create_the_package") + " (" + file + ").",
                getText("_create_package"), JOptionPane.WARNING_MESSAGE);
            return null;
        }
        return os;
    }
}
