/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.renderers;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.TablePackageInfo;

/**
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractTablePackageInfoCellRenderer extends
		DefaultTableCellRenderer {

	private static final long serialVersionUID = -8534540354786328208L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

        Component component =
            super.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);

		TableModel tableModel = table.getModel();

		if (tableModel instanceof PackagesTableModel) {
			PackagesTableModel packagesTableModel = (PackagesTableModel) tableModel;

			TablePackageInfo info = packagesTableModel.getPackageInfo(row);

            component =
                getTableCellRendererComponent(info, component, table,
					value, isSelected, hasFocus, row, column);
		}

        // Make sure the row height allows the cell renderer to fit in height
        Dimension dim = component.getPreferredSize();
        if (dim.height > table.getRowHeight()) {
            table.setRowHeight(dim.height);
        }

        return component;
	}

	protected abstract Component getTableCellRendererComponent(
			TablePackageInfo info, Component defaultComponent, JTable table,
			Object value, boolean isSelected, boolean hasFocus, int row,
			int column);
}
