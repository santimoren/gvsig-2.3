/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.model;

import java.io.File;
import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectFilesTreeCheckNode extends DefaultMutableTreeNode {

	/**
     * 
     */
	private static final long serialVersionUID = 8722862821986059646L;
	public final static int SINGLE_SELECTION = 0;
	public final static int DIG_IN_SELECTION = 4;
	protected int selectionMode;
	protected boolean isSelected;

	private File file = null;

	public SelectFilesTreeCheckNode(File file) {
		this(file, true, false);
	}

	public SelectFilesTreeCheckNode(File file, boolean allowsChildren,
			boolean isSelected) {
		super(file, allowsChildren);
		this.isSelected = isSelected;
		setSelectionMode(DIG_IN_SELECTION);
		this.file = file;
		if (file.isDirectory()) {
			retrieveChildren();
		}
	}

	public void setSelectionMode(int mode) {
		selectionMode = mode;
	}

	public int getSelectionMode() {
		return selectionMode;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;

		if ((selectionMode == DIG_IN_SELECTION) && (children != null)) {
			Enumeration e = children.elements();
			while (e.hasMoreElements()) {
				SelectFilesTreeCheckNode node = (SelectFilesTreeCheckNode) e
						.nextElement();
				node.setSelected(isSelected);
			}
		}
	}

	public boolean isSelected() {
		return isSelected;
	}

	public boolean isFile() {
		return file.isFile();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.DefaultMutableTreeNode#getChildAt(int)
	 */
	@Override
	public TreeNode getChildAt(int index) {
		return super.getChildAt(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.DefaultMutableTreeNode#getChildCount()
	 */
	@Override
	public int getChildCount() {
		return super.getChildCount();
	}

	private void retrieveChildren() {
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			add(new SelectFilesTreeCheckNode(files[i]));
		}
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.DefaultMutableTreeNode#toString()
	 */
	@Override
	public String toString() {
		return file.getName();
	}

	// If you want to change "isSelected" by CellEditor,

	// public void setUserObject(Object obj) {
	// if (obj instanceof Boolean) {
	// setSelected(((Boolean)obj).booleanValue());
	// }else{
	// super.setUserObject(obj);
	// }
	// }

}
