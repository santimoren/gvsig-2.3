/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.api.execution.JShowUnresolvedDependenciesAndAskContinuePanel;

public class DefaultJShowUnresolvedDependenciesAndAskContinuePanel extends
		JShowUnresolvedDependenciesAndAskContinuePanel implements
		ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2135985029410816305L;
	private Dependencies dependencies;
	private boolean doCancel = true;

	public DefaultJShowUnresolvedDependenciesAndAskContinuePanel(
			SwingInstallerManager manager, Dependencies dependencies,
			String message) {
		// this.buildPackageList(dependencies);
		this.dependencies = dependencies;
		this.createUI(manager, message);
	}

	// private void buildPackageList(Dependencies dependencies) {
	// this.dependencies = new ArrayList<String>();
	// for( int i=0 ; i<packages.size(); i++ ) {
	// PackageInfo pkg = packages.get(i);
	// this.packages.add(pkg.getName());
	// }
	// }

	private void createUI(SwingInstallerManager manager, String message) {
		this.setLayout(new BorderLayout());

		JButton cancelButton = new JButton(manager.getText("_cancel"));
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		//
		final JButton continueButton = new JButton(manager.getText("_continue"));
		continueButton.setActionCommand("continue");
		continueButton.addActionListener(this);

		JList list = new JList(this.dependencies.toArray());

		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(250, 80));
		listScroller.setAlignmentX(LEFT_ALIGNMENT);

		JPanel listPane = new JPanel();
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
		
		String html_msg = DefaultJShowPackageStatusAndAskContinuePanel.getHtmlText(message);
		JLabel label = new JLabel(html_msg);
		
		label.setLabelFor(list);
		listPane.add(label);
		listPane.add(Box.createRigidArea(new Dimension(0, 5)));
		listPane.add(listScroller);
		listPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(cancelButton);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(continueButton);

		this.add(listPane, BorderLayout.CENTER);
		this.add(buttonPane, BorderLayout.PAGE_END);
		
		this.setPreferredSize(new Dimension(500, 300));
	}

	@Override
	public boolean cancelled() {
		return doCancel;
	}

	@Override
	public boolean needShow() {
		return this.dependencies.size() > 0;
	}

	public void actionPerformed(ActionEvent e) {
		if ("cancel".equals(e.getActionCommand())) {
			doCancel = true;
		} else {
			doCancel = false;
		}
		this.setVisible(false);
	}
}
