/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.wizard;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.panel.DefaultProgressPanel;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */

public class DownloadProgressWizard extends DefaultProgressPanel implements
		OptionPanel {

	private static final long serialVersionUID = -7064334977678611609L;
	private static Logger logger = LoggerFactory
			.getLogger(DownloadProgressWizard.class);
	private AbstractInstallPackageWizard abstractInstallPackageWizard;
	// private abstractInstallPackageWizard abstractInstallPackageWizard;
	public JButton startDownload;
	private int direccion = WizardPanelWithLogo.ACTION_NEXT;
	private DefaultSwingInstallerManager swingInstallerManager;

	public DownloadProgressWizard(
			AbstractInstallPackageWizard abstractInstallPackageWizard) {
		super();
		this.abstractInstallPackageWizard = abstractInstallPackageWizard;
	}

	@Override
	protected void initComponents() {
		super.initComponents();

		final DownloadProgressWizard wizard = this;

		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();

		startDownload = new JButton(swingInstallerManager
				.getText("_start_download"));

		java.awt.GridBagConstraints gridBagConstraints;
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.EAST;
		gridBagConstraints.insets = new Insets(5, 2, 2, 2);
		add(startDownload, gridBagConstraints);

		startDownload.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				UpdatePanel task = new UpdatePanel(wizard);
				wizard.abstractInstallPackageWizard.addCancellableTask(task);
				bind(task.getTaskStatus());
				task.start();
			}
		});
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_download_progress");
	}

	public void lastPanel() {
		abstractInstallPackageWizard.setFinishButtonVisible(false);
		abstractInstallPackageWizard.setCancelButtonEnabled(true);
		direccion = WizardPanelWithLogo.ACTION_NEXT;
	}

	public void nextPanel() throws NotContinueWizardException {
		direccion = WizardPanelWithLogo.ACTION_PREVIOUS;
	}

	public void updatePanel() {
		boolean thereIsDownloablePackage = false;

		InstallPackageService service = abstractInstallPackageWizard
				.getInstallerExecutionService();
		service.getPackageCount();

		for (int i = 0; i < abstractInstallPackageWizard
				.getInstallersToInstall().size(); i++) {

			if (abstractInstallPackageWizard.getInstallersToInstall().get(i)
					.getDownloadURL() != null) {
				thereIsDownloablePackage = true;
				break;
			}
		}

		if (thereIsDownloablePackage) {
			this.abstractInstallPackageWizard.setNextButtonEnabled(false);
			this.startDownload.setEnabled(true);
		} else {
			((DefaultInstallPackageWizard) abstractInstallPackageWizard)
					.doAction(direccion);
		}
	}

	private String getText(String key) {
		return this.swingInstallerManager.getText(key);
	}

	private static class UpdatePanel extends AbstractMonitorableTask {

		private final DownloadProgressWizard panel;

		public UpdatePanel(DownloadProgressWizard panel) {
            super(panel.getText("_downloading_files"));
			// super(panel.getText("_downloading"));
			this.panel = panel;
		}

		@Override
        public synchronized void run() {
            SimpleTaskStatus taskStatus =
                (SimpleTaskStatus) this.getTaskStatus();

            List<String> not_downloaded = new ArrayList<String>();
            
            try {

                List<PackageInfo> installersToInstall =
                    this.panel.abstractInstallPackageWizard
                        .getInstallersToInstall();
                this.panel.startDownload.setEnabled(false);
                this.panel.abstractInstallPackageWizard
                    .setBackButtonEnabled(false);
                InstallPackageService installerService =
                    this.panel.abstractInstallPackageWizard
                        .getInstallerExecutionService();

                

                for (int i = 0; i < installersToInstall.size(); i++) {
                    taskStatus.setCurValue(0);
                    PackageInfo installerInfo = installersToInstall.get(i);
                    taskStatus.message(installerInfo.getName() + " " + (i + 1)
                        + " / " + installersToInstall.size());
                    if (installerInfo.getDownloadURL() != null) {
                        try {
                            installerService.downloadPackage(installerInfo,
                                taskStatus);
                        } catch (Exception e) {

                            not_downloaded.add(installerInfo.getName());
                            String msg =
                                panel.swingInstallerManager
                                    .getText("_Cant_download_package_files")
                                    + " " + installerInfo.getName();
                            logger.info(msg, e);
                        }
                    }

                    if (not_downloaded.size() > 0) {
                        break;
                    }
                }

                this.panel.abstractInstallPackageWizard
                    .setBackButtonEnabled(true);

            } finally {
                if (not_downloaded.size() == 0) {
                    // Set the finished text
                    taskStatus.message(panel.getText("_finished"));
                    taskStatus.terminate();
                    taskStatus.remove();
                    this.panel.abstractInstallPackageWizard
                        .setNextButtonEnabled(true);
                } else {
                    taskStatus.message(panel.getText("_finished_Some_files_not_downloaded"));
                    taskStatus.terminate();
                    taskStatus.remove();
                    this.panel.abstractInstallPackageWizard
                        .setNextButtonEnabled(false);
                    showNotDownloaded(this.panel, not_downloaded);
                }            
                
            }
        }

	}
	
	
	   private static synchronized void showNotDownloaded(
	       final Component parentPanel, final List<String> names) {
	       
	       int sz = names.size();
           String list_str = ""; 
	       for (int i=0; i<sz; i++) {
	           list_str = list_str + "\n - " + names.get(i);
	       }
	       
	       list_str = Messages.getText("_These_packages_not_downloaded")
	           + ":\n" + list_str + "\n";
	       final String list_str_copy = list_str; 
	       if (!SwingUtilities.isEventDispatchThread()) {
	            try {
	                SwingUtilities.invokeLater(new Runnable() {
	                    public void run() {
	                        
	                        JOptionPane.showMessageDialog(
	                            parentPanel, list_str_copy,
	                            Messages.getText("_Download_error"),
	                            JOptionPane.ERROR_MESSAGE);
	                        
	                    }
	                });
	            } catch (Throwable e) {
	                logger.info("Error while showing message dialog. ", e);
	            }
	            
	        } else {
	            JOptionPane.showMessageDialog(
	                parentPanel, list_str, Messages.getText("_Download_error"),
	                JOptionPane.ERROR_MESSAGE);
	        }
	        
	    }
	   
	   
	
	private static synchronized int showOptionDialog(
	    final Component parentComponent,
	    final String message,
	    final String title,
	    final int optionType,
	    final int messageType) {

        if (!SwingUtilities.isEventDispatchThread()) {
            try {
                final int[] resp = new int[1];
                SwingUtilities.invokeAndWait(new Runnable() {

                    public void run() {
                        resp[0] = showOptionDialog(
                            parentComponent, message, title, optionType, messageType);
                        
                    }
                });
                
                return resp[0];
            } catch (Throwable e) {
                logger.info("Error while showing message dialog. ", e);
            }
        }
        
	    return JOptionPane.showOptionDialog(
	        parentComponent, message, title,
	        optionType, messageType, null, null, null);
	    
	}
	
	

}
