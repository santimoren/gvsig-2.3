/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.List;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.filters.NameDescriptionOrCodeFilter;
import org.gvsig.installer.swing.impl.execution.panel.filters.PackageFilter;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageOfficialRecommended;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageOsAndArchitecture;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageStatus;
import org.gvsig.installer.swing.impl.execution.panel.renderers.AbstractTablePackageInfoCellRenderer;
import org.gvsig.installer.swing.impl.execution.panel.renderers.InstallStatusCellEditor;
import org.gvsig.installer.swing.impl.execution.panel.renderers.InstallStatusCellRenderer;
import org.gvsig.installer.swing.impl.execution.panel.renderers.InstalledPackageCellRenderer;
import org.gvsig.installer.swing.impl.execution.panel.renderers.OfficialRecommendedCellRenderer;
import org.gvsig.installer.swing.impl.execution.panel.renderers.OsAndArchitectureCellRenderer;

/**
 * @author gvSIG Team
 * @version $Id$
 */
public class PackagesTablePanel extends JPanel implements ListSelectionListener {

	private static final long serialVersionUID = 8156088357208685689L;
	protected SwingInstallerManager swingInstallerManager = null;

	private JScrollPane descriptionScrollPane;
	private JEditorPane descriptionTextEditor;
	private JScrollPane pluginsScrollPane;
	private JTable pluginsTable;
	private PackagePropertiesFilterPanel filterPanel;
	private FastFilterButtonsPanel fastFilterPanel;
	private SelectPackagesPanel selectPackagesPanel;

	public PackagesTablePanel(SelectPackagesPanel selectPackagesPanel) {
		super();
		swingInstallerManager = SwingInstallerLocator
				.getSwingInstallerManager();
		this.selectPackagesPanel = selectPackagesPanel;
		initComponents();
		pluginsTable.getSelectionModel().addListSelectionListener(this);
		pluginsTable.setDefaultRenderer(PackageOfficialRecommended.class,
				new OfficialRecommendedCellRenderer());
		pluginsTable.setDefaultRenderer(PackageStatus.class,
				new InstallStatusCellRenderer());
		pluginsTable.setDefaultEditor(PackageStatus.class,
				new InstallStatusCellEditor(this.selectPackagesPanel));
		AbstractTablePackageInfoCellRenderer ipcr = new InstalledPackageCellRenderer();
		pluginsTable.setDefaultRenderer(PackageOsAndArchitecture.class,
				new OsAndArchitectureCellRenderer());
		pluginsTable.setDefaultRenderer(String.class, ipcr);
		pluginsTable.setDefaultRenderer(Number.class, ipcr);
		pluginsTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		pluginsTable = new JTable();

		MouseListener mouseListener = new MyMouseListener();
		pluginsTable.addMouseListener(mouseListener);

		pluginsScrollPane = new JScrollPane(pluginsTable);

		descriptionTextEditor = new JEditorPane();
		descriptionTextEditor.setBackground(Color.WHITE);
		descriptionTextEditor.setEditable(false);
		descriptionTextEditor.setContentType("text/html");

		descriptionScrollPane = new JScrollPane(descriptionTextEditor);

		filterPanel = new PackagePropertiesFilterPanel(this);
		filterPanel.setVisible(true);

		fastFilterPanel = new FastFilterButtonsPanel(this);

		setLayout(new GridBagLayout());

		// left panel filter
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 0.13;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridheight = 2;
		gridBagConstraints.insets = new Insets(0, 2, 2, 2);
		add(filterPanel, gridBagConstraints);

		// fast filters buttons panel
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.weightx = 0.75;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(fastFilterPanel, gridBagConstraints);

		// plugins scroll panel
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 0.75;
		gridBagConstraints.weighty = 0.7;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(pluginsScrollPane, gridBagConstraints);

		// description panel
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.75;
		gridBagConstraints.weighty = 0.3;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(descriptionScrollPane, gridBagConstraints);
		
		// filterPanel.setInitialFilter();
	}

	public void resetFilter() {
		filterPanel.resetPanel();
		valueChanged();
	}
	
	public void selectPackages() {
		PackagesTableModel pluginsTableModel = (PackagesTableModel) pluginsTable
				.getModel();
		pluginsTableModel.selectDefaultPackages();
	}

	public void setTableModel(PackagesTableModel pluginsTableModel) {
		pluginsTable.setModel(pluginsTableModel);
		pluginsTableModel.fireTableDataChanged();
		TableColumnModel tableColumnModel = pluginsTable.getColumnModel();
		tableColumnModel.getColumn(0).setPreferredWidth(20);
		tableColumnModel.getColumn(1).setPreferredWidth(20);
		tableColumnModel.getColumn(2).setPreferredWidth(20);
		tableColumnModel.getColumn(3).setPreferredWidth(225);
		tableColumnModel.getColumn(4).setPreferredWidth(130);
		tableColumnModel.getColumn(5).setPreferredWidth(53);

	}

	public List<PackageInfo> getPackagesToInstall() {
		return ((PackagesTableModel) pluginsTable.getModel())
				.getPackagesToInstall();
	}

	public boolean isPackageSelected() {
		return ((PackagesTableModel) pluginsTable.getModel())
				.hasAnyPackageSelected();
	}

	private class HTMLCoder {
		private StringBuffer buffer = new StringBuffer();
	
		public String toString() {
			return buffer.toString();
		}
		
		public void addTitle(String title) {
			if( title == null ) {
				return;
			}
			buffer.append("<h1>");
			buffer.append(title);
			buffer.append("</h1>");
		}
		
		public void addParagraph(Object text) {
			if( text == null ) {
				return;
			}
			String t = text.toString().replaceAll("\\n", "<br>\n");
			buffer.append("<p>");
			buffer.append(t);
			buffer.append("</p>");
		}
		
		public void addBeginList() {
			buffer.append("<ul>");
		}
		
		public void addEndList() {
			buffer.append("</ul>");
		}
		
		public void addListItem(String label, Object value) {
			if( value == null ) {
				return;
			}
			buffer.append("<li>");
			buffer.append(label);
			buffer.append(": <i>");
			buffer.append(value.toString());
			buffer.append("</i></li>");
		}
		
		public void addListItem(String label, URL value) {
			if( value == null ) {
				return;
			}
			buffer.append("<li>");
			buffer.append(label);
			buffer.append(": <a href=\"");
			buffer.append(value.toString());
			buffer.append("\">");
			buffer.append(value.toString());
			buffer.append("</a></li>");
		}
		
	}
	public void valueChanged(ListSelectionEvent e) {
		valueChanged();
	}

	public void valueChanged() {
		int row = pluginsTable.getSelectedRow();
		if (row < 0) {
			descriptionTextEditor.setText("<p></p>");
			return;
		}

		PackageInfo pkginfo = ((PackagesTableModel) pluginsTable.getModel())
				.getPackageInfoAt(row);

		HTMLCoder html = new HTMLCoder();
		html.addTitle(pkginfo.getName());
		html.addParagraph(pkginfo.getDescription());
		html.addBeginList();
		html.addListItem("code", pkginfo.getCode());
		html.addListItem("Organization", pkginfo.getOwner());
		html.addListItem("Organization URL", pkginfo.getOwnerURL());
		html.addListItem("Sources", pkginfo.getSourcesURL());
		html.addListItem("Dependencies", pkginfo.getDependencies());
		html.addListItem("Categories", pkginfo.getCategoriesAsString());
		html.addListItem("Official", pkginfo.isOfficial());
		html.addListItem("Signed", pkginfo.isSigned());
		html.addEndList();

		descriptionTextEditor.setText(html.toString());

		// get the view area to the top-left corner
		descriptionTextEditor.setCaretPosition(0);

	}

	public void setFilter(PackageFilter filter) {
		PackagesTableModel pluginsTableModel = (PackagesTableModel) pluginsTable
				.getModel();
		pluginsTableModel.setFilter(filter);
		selectPackagesPanel.updatePanel();
	}

	public void setFilter(NameDescriptionOrCodeFilter filter) {
		PackagesTableModel pluginsTableModel = (PackagesTableModel) pluginsTable
				.getModel();
		pluginsTableModel.setFilter(filter);
		selectPackagesPanel.updatePanel();
	}

	private class MyMouseListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			selectPackagesPanel.checkIfPluginSelected();
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}
	}

	public SelectPackagesPanel getSelectPackagesPanel() {
		return this.selectPackagesPanel;
	}

	public void clearAllPanels() {
		filterPanel.resetPanel();
		descriptionTextEditor.setText("");
		fastFilterPanel.resetPanel();
	}

	public void resetPanel() {
		filterPanel.resetPanel();
		descriptionTextEditor.setText("");
	}

    /**
     * 
     */
    public void setInitialFilter() {
        filterPanel.setInitialFilter();
    }

}
