/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.creation.JPackageInfoPanel;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.DynFormManager;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.packageutils.impl.PackageInfoTags;
import org.gvsig.tools.service.spi.ProviderFactory;
import org.gvsig.tools.service.spi.ProviderManager_WithGetFactories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;

public class DefaultPackageInfoPanel extends JPackageInfoPanel {

	private static final Logger logger = LoggerFactory.getLogger(DefaultPackageInfoPanel.class);
	
	private static final long serialVersionUID = 3220127550495628087L;

	protected DefaultSwingInstallerManager swingInstallerManager = null;

	private JDynForm form = null;
	private DynObject data = null;
        private final String packageType;

	
	public DefaultPackageInfoPanel() {
            this(null);
        }
        
        public DefaultPackageInfoPanel(String packageType) {
		super();
                this.packageType = packageType;
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {
		DynObjectManager dynObjectManager = ToolsLocator.getDynObjectManager();
		InputStream resource = this.getClass().getClassLoader().getResourceAsStream("org/gvsig/installer/swing/impl/creation/panel/formPackageinfo.xml");
		try {
			Map definitions = dynObjectManager.importDynClassDefinitions(resource, this.getClass().getClassLoader());
			DynStruct definition = (DynStruct) definitions.get("FormPackageinfo");
                        DynField dynField = definition.getDynField("type");
                        setAvailableValuesOfFieldType(dynField);
                        if( this.packageType!=null ) {
                            dynField.setReadOnly(true);
                        }
			this.data = dynObjectManager.createDynObject(definition);
                        if( this.packageType!=null ) {
                            this.data.setDynValue("type", this.packageType);
                        }
			DynFormManager dynFormManager = DynFormLocator.getDynFormManager();
			this.form = dynFormManager.createJDynForm(data);
		} catch (Exception e) {
			// Uf....
			logger.warn("No se puede crear el formulario.", e);
			throw new RuntimeException("No se puede crear el formulario.", e);
		}
		
		this.setLayout(new BorderLayout(4, 4));
		this.add(form.asJComponent(),BorderLayout.CENTER);
	}
	
	
	protected String findCurrentGvSIGVersion() {
		return SwingInstallerLocator.getSwingInstallerManager()
				.getApplicationVersion();
	}


	public String getName() {
		return (String) this.form.getValue(PackageInfoTags.NAME);
	}

	public void setName(String name) {
		this.form.setValue(PackageInfoTags.NAME, name);
	}


	@Override
	public boolean validatePanel() {
		return this.form.hasValidValues();
	}

	@Override
	public void panelToPackageInfo(PackageInfo packageInfo) {
		
		this.form.getValues(data);
		packageInfo.setType( (String) data.getDynValue(PackageInfoTags.TYPE));
		packageInfo.setCode( (String) data.getDynValue(PackageInfoTags.CODE));
		packageInfo.setName((String) data.getDynValue(PackageInfoTags.NAME));
		packageInfo.setDescription((String) data.getDynValue(PackageInfoTags.DESCRIPTION));
		packageInfo.setVersion((Version) data.getDynValue(PackageInfoTags.VERSION));
		packageInfo.setBuild(((Integer) data.getDynValue(PackageInfoTags.BUILD)).intValue());
		packageInfo.setState((String) data.getDynValue(PackageInfoTags.STATE));
		packageInfo.setOfficial(((Boolean) data.getDynValue(PackageInfoTags.OFFICIAL)).booleanValue());
		packageInfo.setOperatingSystem((String) data.getDynValue(PackageInfoTags.OS));
		packageInfo.setArchitecture((String) data.getDynValue(PackageInfoTags.ARCHITECTURE));
		packageInfo.setJavaVM((String) data.getDynValue(PackageInfoTags.JVM));
		packageInfo.setGvSIGVersion((String) data.getDynValue(PackageInfoTags.GVSIG_VERSION));
		packageInfo.setSourcesURL((URL) data.getDynValue(PackageInfoTags.SOURCES_URL));
		packageInfo.setOwnerURL((URL) data.getDynValue(PackageInfoTags.OWNER_URL));
		packageInfo.setOwner((String) data.getDynValue(PackageInfoTags.OWNER));
		packageInfo.setDependencies((String) data.getDynValue(PackageInfoTags.DEPENDENCIES));
		packageInfo.addCategoriesAsString((String) data.getDynValue(PackageInfoTags.CATEGORIES));
	}

	private boolean isEmptyString(String s) {
		if( s==null) {
			return true;
		}
		return s.length() == 0; 
	}
	
	@Override
	public void packageInfoToPanel(PackageInfo packageInfo) {
                if( this.packageType!=null ) {
                    this.data.setDynValue(PackageInfoTags.TYPE, this.packageType);
                } else {
                    this.data.setDynValue(PackageInfoTags.TYPE, packageInfo.getType());
                }
		this.data.setDynValue(PackageInfoTags.CODE, packageInfo.getCode());
		this.data.setDynValue(PackageInfoTags.NAME, packageInfo.getName());
		this.data.setDynValue(PackageInfoTags.VERSION, packageInfo.getVersion());
		this.data.setDynValue(PackageInfoTags.GVSIG_VERSION, findCurrentGvSIGVersion());
		this.data.setDynValue(PackageInfoTags.BUILD, packageInfo.getBuild());
		this.data.setDynValue(PackageInfoTags.OFFICIAL, packageInfo.isOfficial());
		this.data.setDynValue(PackageInfoTags.STATE, packageInfo.getState());
		this.data.setDynValue(PackageInfoTags.DESCRIPTION, packageInfo.getDescription());
		this.data.setDynValue(PackageInfoTags.OS, packageInfo.getOperatingSystem());
		this.data.setDynValue(PackageInfoTags.ARCHITECTURE, packageInfo.getArchitecture());
		this.data.setDynValue(PackageInfoTags.JVM, packageInfo.getJavaVM());
		this.data.setDynValue(PackageInfoTags.SOURCES_URL, packageInfo.getSourcesURL());
		this.data.setDynValue(PackageInfoTags.OWNER, packageInfo.getOwner());
		this.data.setDynValue(PackageInfoTags.OWNER_URL, packageInfo.getOwnerURL());
		this.data.setDynValue(PackageInfoTags.DEPENDENCIES, packageInfo.getDependencies());
		this.data.setDynValue(PackageInfoTags.CATEGORIES, packageInfo.getCategoriesAsString());
		//this.data.setDynValue(PackageInfoTags.CODEALIAS, packageInfo.getAllCodes().toString());
		this.form.getField(PackageInfoTags.CODEALIAS).setReadOnly(true);
		
//		if (  !isEmptyString(packageInfo.getCode()) ){
//			this.form.getField(PackageInfoTags.CODE).setReadOnly(true);
//		}
		this.form.setValues(data);

	}

    private void setAvailableValuesOfFieldType(DynField dynField) {
        ProviderManager_WithGetFactories providerManager = InstallerProviderLocator.getProviderManager();
        List<ProviderFactory> factories = providerManager.getProviderFactories();
        DynObjectValueItem[] values = new DynObjectValueItem[factories.size()];
        int i=0;
        for( ProviderFactory factory : factories ) {
            String name = factory.getName();
            values[i++] = new DynObjectValueItem(name,name);
        }
        dynField.setAvailableValues(values);
    }
	

}
