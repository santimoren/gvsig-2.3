/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PlugintoInstallTablePanel extends JPanel {

	/**
     * 
     */
	private static final long serialVersionUID = 4098431540205466849L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JScrollPane jScrollPane2;
	private JTextArea jTextArea1;
	private JTable pluginTable;
	private JScrollPane pluginjScrollPane;

	public PlugintoInstallTablePanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		pluginjScrollPane = new JScrollPane();
		pluginTable = new JTable();
		jScrollPane2 = new JScrollPane();
		jTextArea1 = new JTextArea();

		setLayout(new GridBagLayout());

		pluginjScrollPane.setViewportView(pluginTable);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 0.5;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(pluginjScrollPane, gridBagConstraints);

		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane2.setViewportView(jTextArea1);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 0.5;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(jScrollPane2, gridBagConstraints);
	}

}
