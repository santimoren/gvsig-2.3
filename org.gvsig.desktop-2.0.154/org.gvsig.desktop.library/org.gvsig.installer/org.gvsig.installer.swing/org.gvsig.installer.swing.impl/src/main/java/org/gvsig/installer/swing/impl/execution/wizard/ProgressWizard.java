/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */
package org.gvsig.installer.swing.impl.execution.wizard;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.panel.DefaultProgressPanel;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class ProgressWizard extends DefaultProgressPanel implements OptionPanel {

    private static final long serialVersionUID = 8531884535246881448L;
    private AbstractInstallPackageWizard installerExecutionWizard;
    private DefaultSwingInstallerManager swingInstallerManager;

    public ProgressWizard(DefaultInstallPackageWizard installerExecutionWizard) {
        super();
        this.installerExecutionWizard = installerExecutionWizard;
        swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
                .getSwingInstallerManager();
    }

    public JPanel getJPanel() {
        return this;
    }

    public String getPanelTitle() {
        return swingInstallerManager.getText("_progress");
    }

    public void lastPanel() {
        installerExecutionWizard.setFinishButtonVisible(false);
        installerExecutionWizard.setCancelButtonEnabled(true);
    }

    public void nextPanel() {
        // Nothing to do, no next panel
    }

    public void updatePanel() {
        installerExecutionWizard.setFinishButtonVisible(false);
        installerExecutionWizard.setCancelButtonEnabled(false);

        final ProgressWizard wizard = this;

        UpdatePanel task = new UpdatePanel(wizard);
        wizard.installerExecutionWizard.addCancellableTask(task);
        bind(task.getTaskStatus());
        task.setDaemon(true);
        task.start();
    }

    private static class UpdatePanel extends AbstractMonitorableTask {

        private final ProgressWizard panel;

        public UpdatePanel(ProgressWizard panel) {
            super(panel.swingInstallerManager
                    .getText("_Install_package"));
            this.panel = panel;
        }

        private void showWarning(final String msg) {
            if( !SwingUtilities.isEventDispatchThread() )  {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                        }
                    });
                } catch (Exception ex) {
                    logger.warn("Cant show message to the user. "+msg);
                }
                return;
            }
            JOptionPane.showMessageDialog(panel, msg, "Warning", JOptionPane.WARNING_MESSAGE);
        }
        
        @Override
        public synchronized void run() {

            int errcount = 0;
            try {
                logger.info("Package installation iniitiated");
                SimpleTaskStatus taskStatus = (SimpleTaskStatus) this
                        .getTaskStatus();
                // bundle of packages
                InstallPackageService installerExecutionService = this.panel.installerExecutionWizard
                        .getInstallerExecutionService();
                // packages to install
                List<PackageInfo> installersToInstall = this.panel.installerExecutionWizard
                        .getInstallersToInstall();
                Collections.sort(installersToInstall, new Comparator<PackageInfo>() {
                    public int compare(PackageInfo o1, PackageInfo o2) {
                        return o1.getCode().compareTo(o2.getCode());
                    }
                });
                taskStatus.setRangeOfValues(0, installersToInstall.size());
                for ( int i = 0; i < installersToInstall.size(); i++ ) {
                    PackageInfo installerInfo = installersToInstall.get(i);
                    taskStatus.message(installerInfo.getName());
                    try {
                        installerExecutionService.installPackage(
                                panel.installerExecutionWizard.getApplicationFolder(),
                                installerInfo
                        );
                    } catch (Throwable th) {
                        logger.warn("Can't install package '" + installerInfo.getCode() + "'.", th);
                        errcount++;
                    }
                    taskStatus.setCurValue(i + 1);
                }
                this.panel.installerExecutionWizard.setFinishButtonVisible(true);
                if( errcount>0 ) {
                    showWarning("Failed to install "+errcount+" packages.");
                }
                taskStatus.message(panel.swingInstallerManager.getText("_Finished"));

                taskStatus.terminate();
                taskStatus.remove();
                logger.info("Package installation finished");
            } catch (Throwable th) {
                logger.warn("Problems install packages.",th);
                showWarning(panel.swingInstallerManager.getText("_Cant_install_packege"));
            }

        }

    }
}
