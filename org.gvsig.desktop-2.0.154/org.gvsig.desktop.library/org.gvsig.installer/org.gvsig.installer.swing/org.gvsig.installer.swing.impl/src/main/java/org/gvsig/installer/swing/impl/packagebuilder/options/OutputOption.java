/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.installer.swing.impl.packagebuilder.options;

import java.io.File;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.installer.swing.impl.packagebuilder.BasePackageWizard;


public class OutputOption implements OptionPanel {

    private final JOutputPanel outputPanel;
    private final BasePackageWizard wizard;

    public OutputOption(BasePackageWizard wizard) {
        super();
        this.wizard = wizard;
        SwingInstallerManager installerManager = SwingInstallerLocator.getSwingInstallerManager();

        outputPanel = installerManager.createOutputPanel();
        this.outputPanel.setPackageInfo(wizard.getPackageInfo());
    }

    @Override
    public JPanel getJPanel() {
        return this.outputPanel;
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

    @Override
    public String getPanelTitle() {
        return getText("_output_options");
    }

    @Override
    public void lastPanel() {

    }

    @Override
    public void nextPanel() throws NotContinueWizardException {

        File file = outputPanel.getPackageFile();
        File filePath = new File(file.getParent());
        if (!filePath.exists()) {
            JOptionPane.showMessageDialog(outputPanel,
                getText("_the_package_file_folder_does_not_exist") + ": "
                    + filePath);
            throw new NotContinueWizardException("", null, false);
        }
        if (outputPanel.isCreatePackageIndex()) {
            URL url = outputPanel.getDownloadURL();
            if (url == null) {
                throw new NotContinueWizardException("", null, false);
            }
            File indexFile = outputPanel.getPackageIndexFile();
            File indexFilePath = new File(indexFile.getParent());
            if (!indexFilePath.exists()) {
                JOptionPane.showMessageDialog(outputPanel,
                    getText("_the_package_index_file_folder_does_not_exist")
                        + ": " + indexFilePath);
                throw new NotContinueWizardException("", null, false);
            }
        }

    }

    @Override
    public void updatePanel() {
        outputPanel.setPackageFile(this.wizard.getDefaultPackageBundleFile());
        outputPanel.setPackageIndexFile(this.wizard.getDefaultPackageIndexBundleFile());
    }

    public URL getDownloadURL() {
        return this.outputPanel.getDownloadURL();
    }
    
    public File getPackageIndexFile() {
        return this.outputPanel.getPackageIndexFile();
    }
    
    public boolean shouldCreateIndex() {
        return this.outputPanel.isCreatePackageIndex();
    }
    
    public File getPackageFile() {
        return this.outputPanel.getPackageFile();
    }
}
