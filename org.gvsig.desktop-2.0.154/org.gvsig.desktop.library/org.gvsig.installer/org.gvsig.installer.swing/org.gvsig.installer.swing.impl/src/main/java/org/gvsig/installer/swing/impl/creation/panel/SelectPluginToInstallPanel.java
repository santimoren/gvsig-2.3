/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.creation.model.PluginListCellRenderer;
import org.gvsig.installer.swing.impl.creation.model.PluginListModel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectPluginToInstallPanel extends JPanel {

	/**
     * 
     */
	private static final long serialVersionUID = 7910780973467189103L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JList pluginList;
	private JScrollPane pluginScrollPane1;
	private PackageInfo selectedInstallerInfo = null;
        private JCheckBox chkShowFullPath;
    private PluginListModel pluginListModel;

	public SelectPluginToInstallPanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	public void setPluginsDirectory(
			MakePluginPackageService installerCreationService)
			throws MakePluginPackageServiceException {
                pluginListModel = new PluginListModel(installerCreationService);
		pluginList.setModel(pluginListModel);
		pluginList.setCellRenderer(new PluginListCellRenderer(this));
	}

	private void initComponents() {

		pluginScrollPane1 = new JScrollPane();
		pluginList = new JList();
		pluginScrollPane1.setViewportView(pluginList);
                
                chkShowFullPath = new JCheckBox("Show full paths", false);
                chkShowFullPath.setHorizontalAlignment(SwingConstants.RIGHT);
                chkShowFullPath.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        pluginListModel.refresh();
                    }
                });

//		java.awt.GridBagConstraints gridBagConstraints;
//		setLayout(new GridBagLayout());
//		gridBagConstraints = new GridBagConstraints();
//		gridBagConstraints.fill = GridBagConstraints.BOTH;
//		gridBagConstraints.weightx = 1.0;
//		gridBagConstraints.weighty = 1.0;
//		add(pluginScrollPane1, gridBagConstraints);
                
                
                setLayout(new BorderLayout());
                this.add(pluginScrollPane1, BorderLayout.CENTER);
                this.add(chkShowFullPath,BorderLayout.SOUTH);
	}

        public boolean getShowFullPaths() {
            return this.chkShowFullPath.isSelected();
        }
        
	/**
	 * @return the selectedPlugin
	 */
	public PackageInfo getSelectedInstallerInfo() {
		return selectedInstallerInfo;
	}

	/**
	 * @param selectedPlugin
	 *            the selectedPlugin to set
	 */
	public void setSelectedInstallerInfo(PackageInfo installerInfo) {
		this.selectedInstallerInfo = installerInfo;
	}
}
