/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.packagebuilder;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.net.URL;
import javax.swing.JComponent;

import javax.swing.JPanel;
import org.apache.commons.io.FilenameUtils;

import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.swing.api.packagebuilder.PackageBuildder;

import org.gvsig.installer.swing.impl.packagebuilder.options.OutputOption;
import org.gvsig.installer.swing.impl.packagebuilder.options.PackageInfoOption;
import org.gvsig.installer.swing.impl.packagebuilder.options.ProgressOption;
import org.gvsig.installer.swing.impl.packagebuilder.options.SelectFolderToPackagingOption;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.packageutils.PackageInfo;

public class BasePackageWizard extends JPanel implements WizardPanel, PackageBuildder {

    private static final long serialVersionUID = 9205891710214122265L;

    private WizardPanelWithLogo wizardPanelWithLogo = null;

    private PackageInfo packageInfo = null;
    private File folderToPackaging = null;
    private WizardPanelActionListener wizardListenerAdapter = null;

    private OutputOption outputOption = null;
    private OptionPanel packageInfoOption = null;
    private OptionPanel progressOption = null;
    private SelectFolderToPackagingOption selectFolderToPackagingOption = null;
    private final String packageType;
    private final File baseFolder;
    private final File outputFolder;

    public BasePackageWizard(String packageType, File selectionFolder, File outputFolder) throws LocatorException {
        this.packageType = packageType;
        this.baseFolder = selectionFolder;
        this.outputFolder = outputFolder;
        initComponents();
    }

    private void initComponents() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        this.wizardPanelWithLogo = new WizardPanelWithLogo();

        this.setCancelButtonEnabled(true);
        this.setFinishButtonEnabled(false);

        this.packageInfo = ToolsLocator.getPackageManager().createPackageInfo();
        this.packageInfo.setType(this.packageType);

        this.addSelectFolderToPackagingOption(
                i18nManager.getTranslation("_Seleccione_la_carpeta_que_desea_empaquetar")
        );
        this.addPackageInfoOptionPanel();
        this.addOutputOptionPanel();
        this.addProgressOptionPanel();
        this.setFolderToPackaging(this.baseFolder);

        wizardPanelWithLogo.setWizardListener(this);

        this.setLayout(new BorderLayout());
        this.add(wizardPanelWithLogo, BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(700, 550));
    }

    @Override
    public String getPackageType() {
        return this.packageType;
    }

    protected void addPackageInfoOptionPanel() {
        if (this.packageInfoOption == null) {
            this.packageInfoOption = new PackageInfoOption(this);
        }
        this.wizardPanelWithLogo.addOptionPanel(this.packageInfoOption);
    }

    protected void addOutputOptionPanel() {
        if (this.outputOption == null) {
            this.outputOption = new OutputOption(this);
        }
        this.wizardPanelWithLogo.addOptionPanel(this.outputOption);
    }

    protected void addProgressOptionPanel() {
        if (this.progressOption == null) {

            this.progressOption = new ProgressOption(this);
        }
        this.wizardPanelWithLogo.addOptionPanel(this.progressOption);
    }

    protected void addSelectFolderToPackagingOption(String message) {
        if (this.selectFolderToPackagingOption == null) {
            this.selectFolderToPackagingOption = new SelectFolderToPackagingOption(this, message);
        }
        this.wizardPanelWithLogo.addOptionPanel(this.selectFolderToPackagingOption);
    }

    public void setNextButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setNextButtonEnabled(isEnabled);
    }

    public void setCancelButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setCancelButtonEnabled(isEnabled);
    }

    public void setFinishButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setFinishButtonEnabled(isEnabled);
    }

    public void setBackButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setBackButtonEnabled(isEnabled);
    }

    @Override
    public WizardPanelActionListener getWizardPanelActionListener() {
        if (this.wizardListenerAdapter == null) {
            this.wizardListenerAdapter = new WizardPanelActionListener() {
                @Override
                public void finish(WizardPanel wizardPanel) {
                    BasePackageWizard.this.setVisible(false);
                }

                @Override
                public void cancel(WizardPanel wizardPanel) {
                    BasePackageWizard.this.setVisible(false);
                }
            };
        }
        return this.wizardListenerAdapter;
    }

    @Override
    public void setWizardPanelActionListener(
            WizardPanelActionListener wizardActionListener) {
        // this.wizardListenerAdapter = wizardActionListener;
        throw new RuntimeException("Esto falta por ver que hace");
    }

    @Override
    public void setFolderToPackaging(File file) {
        this.folderToPackaging = file;
        if (this.selectFolderToPackagingOption != null) {
            this.selectFolderToPackagingOption.setCurrentFolder(file);
        }
        packageInfo.setType(this.packageType);
        packageInfo.setCode(FilenameUtils.getBaseName(file.getName()));
        packageInfo.setName(FilenameUtils.getBaseName(file.getName()));
        File pinfo = new File(file, "package.info");
        if (pinfo.exists()) {
            try {
                ToolsLocator.getPackageManager().readPacakgeInfo(
                        (org.gvsig.tools.packageutils.PackageInfo) packageInfo, pinfo);
                packageInfo.setType(this.packageType);
            } catch (Exception e) {
                /*
                 * Do nothing, packageInfo remains empty
                 * form will be empty
                 * 
                 */
            }
        }

    }

    public File getFolderToPackaging() {
        return this.folderToPackaging;
    }

    @Override
    public PackageInfo getPackageInfo() {
        return this.packageInfo;
    }

    public URL getDownloadURL() {
        return this.outputOption.getDownloadURL();
    }

    public File getPackageIndexFile() {
        return this.outputOption.getPackageIndexFile();
    }

    public boolean shouldCreateIndex() {
        return this.outputOption.shouldCreateIndex();
    }

    public File getPackageFile() {
        return this.outputOption.getPackageFile();
    }

    public void addOptionPanel(OptionPanel optionPanel) {
        this.wizardPanelWithLogo.addOptionPanel(optionPanel);
    }

    public File getDefaultPackageBundleFile() {
        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        String fname = installerManager.getPackageFileName(this.getPackageInfo());
        return new File(this.outputFolder, fname);
    }

    public File getDefaultPackageIndexBundleFile() {
        InstallerManager installerManager = InstallerLocator.getInstallerManager();
        String fname = installerManager.getPackageIndexFileName(this.getPackageInfo());
        return new File(this.outputFolder, fname);
    }

    @Override
    public JComponent asJComponent() {
        return this;
    }

}
