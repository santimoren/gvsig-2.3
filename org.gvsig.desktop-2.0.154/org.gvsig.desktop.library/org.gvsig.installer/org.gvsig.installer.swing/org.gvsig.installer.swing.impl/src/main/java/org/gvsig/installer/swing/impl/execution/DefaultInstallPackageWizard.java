/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution;

import java.awt.BorderLayout;
import java.io.File;
import java.net.URL;
import java.util.List;

import jwizardcomponent.DefaultJWizardComponents;

import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager.UrlAndLabel;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.impl.execution.wizard.DownloadProgressWizard;
import org.gvsig.installer.swing.impl.execution.wizard.ProgressWizard;
import org.gvsig.installer.swing.impl.execution.wizard.SelectBundlesWizard;
import org.gvsig.installer.swing.impl.execution.wizard.SelectPackagesWizard;
import org.gvsig.installer.swing.impl.execution.wizard.TypicalOrAdvancedWizard;
import org.gvsig.installer.swing.impl.wizard.WizardListenerAdapter;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultInstallPackageWizard extends AbstractInstallPackageWizard
		implements WizardPanel {

	private static final long serialVersionUID = 554068938842141497L;
	private static final int TYPICAL_MODE = 0;
	private static final int ADVANCED_MODE = 1;

	private int installationMode = ADVANCED_MODE;
	private boolean askInstallationMode = false;
	private boolean selectDefaultPackages;
        private boolean goToSelectInstalationPackages = false;

	private WizardPanelWithLogo wizardPanelWithLogo = null;
	private InstallPackageService installerExecutionService = null;

	// Wizards
	private SelectBundlesWizard selectInstallersWizard = null;
	private SelectPackagesWizard selectPluginsWizard = null;
	private ProgressWizard progressWizard = null;
	private DownloadProgressWizard downloadProgressWizard = null;
	private TypicalOrAdvancedWizard typicalOrAdvancedWizard = null;

	private WizardListenerAdapter wizardListenerAdapter = null;
        private boolean skipBundleSelection = false;

	public DefaultInstallPackageWizard(File applicationFolder,
			File installFolder) throws InstallPackageServiceException {
		super(applicationFolder, installFolder);

		installerExecutionService = InstallerLocator.getInstallerManager()
				.getInstallPackageService();

		wizardPanelWithLogo = new WizardPanelWithLogo();

		List<UrlAndLabel> downloadURLs = SwingInstallerLocator
				.getSwingInstallerManager().getDefaultDownloadUrlAndLabels();
		selectInstallersWizard = new SelectBundlesWizard(this, downloadURLs);
		typicalOrAdvancedWizard = new TypicalOrAdvancedWizard(this);
		selectPluginsWizard = new SelectPackagesWizard(this);
		progressWizard = new ProgressWizard(this);
		downloadProgressWizard = new DownloadProgressWizard(this);

		addWizards();

		// Adding the listeners
		wizardPanelWithLogo.setWizardListener(this);

		setFinishButtonVisible(false);

		this.setLayout(new BorderLayout());
		this.add(wizardPanelWithLogo, BorderLayout.CENTER);
	}

	public void doAction(int action) {
		this.wizardPanelWithLogo.doAction(action);
	}

	private void addWizards() {
		wizardPanelWithLogo.addOptionPanel(selectInstallersWizard);
		wizardPanelWithLogo.addOptionPanel(typicalOrAdvancedWizard);
		wizardPanelWithLogo.addOptionPanel(selectPluginsWizard);
		wizardPanelWithLogo.addOptionPanel(downloadProgressWizard);
		wizardPanelWithLogo.addOptionPanel(progressWizard);
	}

	public DefaultJWizardComponents getWizardComponents() {
		return wizardPanelWithLogo.getWizardComponents();
	}

	/**
	 * @return the installerExecutionService
	 */
	@Override
	public InstallPackageService getInstallerExecutionService() {
		return installerExecutionService;
	}

	/**
	 * @return the installersToInstall
	 */
	@Override
	public List<PackageInfo> getInstallersToInstall() {
		return this.selectPluginsWizard.getPackagesToInstall();
	}

	@Override
	public void setNextButtonEnabled(boolean isEnabled) {
		getWizardComponents().getNextButton().setEnabled(isEnabled);
	}

	@Override
	public void setFinishButtonVisible(boolean isVisible) {
		getWizardComponents().getFinishButton().setEnabled(isVisible);
	}

	@Override
	public void setCancelButtonEnabled(boolean isEnabled) {
		getWizardComponents().getCancelButton().setEnabled(isEnabled);
	}

	@Override
	public void setBackButtonEnabled(boolean isEnabled) {
		getWizardComponents().getBackButton().setEnabled(isEnabled);
	}

	@Override
	public void installFromDefaultDirectory()
			throws InstallPackageServiceException {
		getWizardComponents().removeWizardPanel(0);
		installerExecutionService.addBundlesFromDirectory(getInstallFolder());
		selectPluginsWizard.updatePanel();
	}

	public WizardPanelActionListener getWizardPanelActionListener() {
		if (((wizardListenerAdapter == null) && (getWizardActionListener() != null))) {
			wizardListenerAdapter = new WizardListenerAdapter(this);
		}
		return wizardListenerAdapter;
	}

	public void setWizardPanelActionListener(
			WizardPanelActionListener wizardActionListener) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setSelectDefaultPackages(boolean isActivated) {
		this.selectDefaultPackages = isActivated;
	}

	@Override
	public boolean getSelectDefaultPackages() {
		return this.selectDefaultPackages;
	}

	public void setShowSelectPackagesPanel(boolean mode) {
		if (mode) {
			installationMode = ADVANCED_MODE;
		} else {
			installationMode = TYPICAL_MODE;
			selectDefaultPackages = true;
		}
	}

	public boolean showSelectPackagesPanel() {
		return installationMode == ADVANCED_MODE;
	}

	@Override
	public boolean getAskTypicalOrCustom() {
		return askInstallationMode;
	}

	@Override
	public void setAskTypicalOrCustom(boolean b) {
		askInstallationMode = true;
	}

        public void setSkipBundleSelection(boolean skipBundleSelection) {
            if( skipBundleSelection ) {
                this.doAction(WizardPanelWithLogo.ACTION_NEXT);
                this.setBackButtonEnabled(false);
            }
            this.skipBundleSelection = skipBundleSelection;
        }
        
        public boolean getSkipBundleSelection() {
            return this.skipBundleSelection;
        }
}
