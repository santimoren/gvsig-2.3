/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.model;

import java.util.Arrays;
import java.util.Comparator;

import javax.swing.AbstractListModel;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PluginListModel extends AbstractListModel {

	private static final long serialVersionUID = -6434919440184484361L;
	private PackageInfo[] plugins;

	public PluginListModel(MakePluginPackageService installerCreationService)
			throws MakePluginPackageServiceException {
		super();

		plugins = new PackageInfo[installerCreationService
				.getPluginPackageCount()];
		for (int i = 0; i < plugins.length; i++) {
			plugins[i] = installerCreationService.getPluginPackageInfo(i);
		}

		Arrays.sort(plugins, new Comparator<PackageInfo>() {

			public int compare(PackageInfo info1, PackageInfo info2) {
				if (info2 == null) {
					return info1 == null ? 0 : 1;
				}

				if (info1 == null) {
					return -1;
				}

				return info1.getCode().compareTo(info2.getCode());
			}
		});
	}

	public Object getElementAt(int index) {
		return plugins[index];
	}

	public int getSize() {
		return plugins.length;
	}

        public void refresh() {
            this.fireContentsChanged(this, 0,this.getSize());
        }
}
