/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.swing.api.JProgressPanel;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.installer.swing.api.creation.JPackageInfoPanel;
import org.gvsig.installer.swing.api.creation.MakePluginPackageWizard;
import org.gvsig.installer.swing.api.creation.MakePluginPackageWizardException;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.api.execution.InstallPackageWizardException;
import org.gvsig.installer.swing.api.execution.JShowPackageStatusAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowPackagesAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowRequiredPackagesAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowUnresolvedDependenciesAndAskContinuePanel;
import org.gvsig.installer.swing.api.packagebuilder.PackageBuildder;
import org.gvsig.installer.swing.impl.creation.panel.DefaultOutputPanel;
import org.gvsig.installer.swing.impl.creation.panel.DefaultPackageInfoPanel;
import org.gvsig.installer.swing.impl.packagebuilder.BasePackageWizard;
import org.gvsig.installer.swing.impl.panel.DefaultProgressPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultSwingInstallerManager implements SwingInstallerManager {

	public class DefaultUrlAndLabel implements UrlAndLabel {

		private URL url;
		private String label;

		public DefaultUrlAndLabel(URL url, String label) {
			this.url = url;
			this.label = label;
		}
		public URL getURL() {
			return this.url;
		}

		public String getLabel() {
			return this.label;
		}
		
		public void setLabel(String label) {
			this.label = label;
		}
		@Override
		public String toString() {
			if( label == null ) {
				return this.url.toString();
			}
			return this.label + " - " + this.url.toString();
		}
	}

	/**
	 * 
	 * This class is for compatibility with deprecated method 
	 * List<URL> getDefaultDownloadURLs()
	 *  
	 * @author jjdelcerro
	 *
	 */
	public class DefaultDownloadURLsIterator<URL> implements ListIterator<URL> {
		private List<UrlAndLabel> urlAndLabels;
		private ListIterator<UrlAndLabel> it;
		
		DefaultDownloadURLsIterator(List<UrlAndLabel> urlAndLabels, int index) {
			this.urlAndLabels = urlAndLabels;
			this.it = this.urlAndLabels.listIterator(index);
		}

		DefaultDownloadURLsIterator(List<UrlAndLabel> urlAndLabels) {
			this.urlAndLabels = urlAndLabels;
			this.it = this.urlAndLabels.listIterator();
		}

		public boolean hasNext() {
			return this.it.hasNext();
		}

		public URL next() {
			UrlAndLabel value = this.it.next();
			if( value == null ) {
				return null;
			}
			return (URL) value.getURL();
		}

		public boolean hasPrevious() {
			return it.hasPrevious();
		}

		public int nextIndex() {
			return it.nextIndex();
		}

		public URL previous() {
			UrlAndLabel value = this.it.previous();
			if( value == null ) {
				return null;
			}
			return (URL) value.getURL();
		}

		public int previousIndex() {
			return it.previousIndex();
		}

		public void remove() {
			throw new UnsupportedOperationException("This is read-only");
		}

		public void add(Object arg0) {
			throw new UnsupportedOperationException("This is read-only");
		}

		public void set(Object arg0) {
			throw new UnsupportedOperationException("This is read-only");
		}
	}
	
	/**
	 * 
	 * This class is for compatibility with deprecated method 
	 * List<URL> getDefaultDownloadURLs()
	 *  
	 * @author jjdelcerro
	 *
	 */
	public class DefaultDownloadURLs<URL> implements List<URL> {

		private List<UrlAndLabel> urlAndLabels;

		DefaultDownloadURLs(List<UrlAndLabel> urlAndLabels) {
			this.urlAndLabels = urlAndLabels;
		}
		
		public boolean add(Object url) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public void add(int arg0, Object url) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public boolean addAll(Collection arg0) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public boolean addAll(int arg0, Collection arg1) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public void clear() {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public boolean contains(Object arg0) {
			Iterator<UrlAndLabel> it = this.urlAndLabels.iterator();
			while( it.hasNext() ) {
				UrlAndLabel x = it.next();
				if( x.getURL().equals(arg0) ) {
					return true;
				}
			}
			return false;
		}

		public boolean containsAll(Collection arg0) {
			throw new UnsupportedOperationException("This method is not supported");
		}

		public URL get(int arg0) {
			return (URL) this.urlAndLabels.get(arg0).getURL();
		}

		public int indexOf(Object arg0) {
			for( int i=0; i<this.urlAndLabels.size(); i++) {
				if( this.urlAndLabels.get(i).getURL().equals(arg0) ) {
					return i;
				}
			}
			return -1;
		}

		public boolean isEmpty() {
			return this.urlAndLabels.isEmpty();
		}

		public Iterator iterator() {
			return new DefaultDownloadURLsIterator(this.urlAndLabels);
		}

		public int lastIndexOf(Object arg0) {
			for( int i=this.urlAndLabels.size()-1; i>=0; i--) {
				if( this.urlAndLabels.get(i).getURL().equals(arg0) ) {
					return i;
				}
			}
			return -1;
		}

		public ListIterator listIterator() {
			return new DefaultDownloadURLsIterator(this.urlAndLabels);
		}

		public ListIterator listIterator(int arg0) {
			return new DefaultDownloadURLsIterator(this.urlAndLabels, arg0);
		}

		public boolean remove(Object arg0) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public URL remove(int arg0) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public boolean removeAll(Collection arg0) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public boolean retainAll(Collection arg0) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public Object set(int arg0, Object arg1) {
			throw new UnsupportedOperationException("This is a read-only list");
		}

		public int size() {
			return this.urlAndLabels.size();
		}

		public List subList(int arg0, int arg1) {
			throw new UnsupportedOperationException("This method is not supported");
		}

		public Object[] toArray() {
			throw new UnsupportedOperationException("This method is not supported");
		}

		public Object[] toArray(Object[] arg0) {
			throw new UnsupportedOperationException("This method is not supported");
		}
		
	}
	
	private static Logger logger = LoggerFactory
			.getLogger(DefaultSwingInstallerManager.class);

	private static final String SWING_INSTALLER_MANAGER_EXTENSION_POINT = "SwingInstallerManagerExtensionPoint";
	private static final String CREATE_INSTALLER_WIZARD_NAME = "CreateInstallerWizard";
	private static final String EXECUTE_INSTALLER_WIZARD_NAME = "ExecuteInstallerWizard";
	private ExtensionPointManager extensionPoints = ToolsLocator
			.getExtensionPointManager();

	private String applicationVersion = "1.0.0";
	private List<UrlAndLabel> defaultDownloadURLs = new ArrayList<UrlAndLabel>();

	public String getText(String key) {
		return Messages.translate(key);
	}

	public void registerMakePluginPackageWizardInstallerCreationWizard(
			Class<? extends MakePluginPackageWizard> clazz) {
		ExtensionPoint extensionPoint = extensionPoints.add(
				SWING_INSTALLER_MANAGER_EXTENSION_POINT, "");
		extensionPoint.append(CREATE_INSTALLER_WIZARD_NAME, "", clazz);
	}

	public void registerInstallPackageWizard(
			Class<? extends AbstractInstallPackageWizard> clazz) {
		ExtensionPoint extensionPoint = extensionPoints.add(
				SWING_INSTALLER_MANAGER_EXTENSION_POINT, "");
		extensionPoint.append(EXECUTE_INSTALLER_WIZARD_NAME, "", clazz);
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String gvSIGVersion) {
		this.applicationVersion = gvSIGVersion;
	}

	public URL getDefaultDownloadURL() {
		if (defaultDownloadURLs.size() < 1) {
			return null;
		}
		return defaultDownloadURLs.get(0).getURL();
	}

	public List<URL> getDefaultDownloadURLs() {
		return new DefaultDownloadURLs(this.defaultDownloadURLs);
	}

    public List<UrlAndLabel> getDefaultDownloadUrlAndLabels() {
    	return this.defaultDownloadURLs;
    }
    
	public void setDefaultDownloadURL(String defaultDownloadURLs) {
		String[] urls = defaultDownloadURLs.split(";");
		for (int i = 0; i < urls.length; i++) {
			try {
				this.addDefaultDownloadURL(urls[i]);
			} catch (MalformedURLException e) {
				logger.info("Malformed URL ("+urls[i]+").", e);
			}
		}
	}

	public void setDefaultDownloadURL(URL defaultDownloadURL) {
		this.defaultDownloadURLs.clear();
		this.addDefaultDownloadURL(defaultDownloadURL);
	}

	public void setDefaultDownloadURL(File defaultDownloadURLs) {
                if( defaultDownloadURLs == null || !defaultDownloadURLs.exists() ) {
                    return;
                }
		InputStream is;
		try {
			is = new FileInputStream(defaultDownloadURLs);
		} catch (FileNotFoundException e1) {
			logger.warn("Can't open the file " + defaultDownloadURLs.getName(), e1);
			return;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		String line = null;
		try {
			for (line = in.readLine(); line != null; line = in.readLine()) {
				try {
					this.addDefaultDownloadURL(line);
				} catch (MalformedURLException e) {
					logger.warn(
							"Error creating the default packages download URL pointing to "
									+ line, e);
				}
			}
		} catch (IOException e) {
			logger.warn("Error reading the file "
					+ defaultDownloadURLs.getName(), e);
		}
	}

	public void addDefaultDownloadURL(URL url) {
		addDefaultDownloadURL(url,null);
	}
	
	public void addDefaultDownloadURL(URL url, String label) {
		Iterator<UrlAndLabel> it = this.defaultDownloadURLs.iterator();
		while( it.hasNext() ) {
			DefaultUrlAndLabel value = (DefaultUrlAndLabel) it.next();
                        // Hacket by jbadia ;)
			if( value.getURL().toString().equals(url.toString()) ) {
				if( value.getLabel() == null ) {
					value.setLabel(label);
				}
				// Already exists the url, don't duplicate.
				return;
			}
		}
		this.defaultDownloadURLs.add( new DefaultUrlAndLabel(url, label));
	}
	
	public void addDefaultDownloadURL(String url) throws MalformedURLException {
		String label = null;
		
		if( url.startsWith("#") ) {
			return;
		}
		Version version = getInstallerManager().createVersion();
		String versionFormat = version.getMajor() + "." + version.getMinor()
				+ "." + version.getRevision();
		int n = url.indexOf("##");
		if( n>0 ) {
			label = url.substring(n+2);
			url = url.substring(0, n-1);
		}
		url = url.replace("$version", versionFormat);
		url = url.replace("<%Version%>", versionFormat);
		url = url.replace("$build", Integer.toString(version.getBuild()));
		url = url.replace("<%Build%>", Integer.toString(version.getBuild()));
		addDefaultDownloadURL(new URL(url), label);
	}

	public InstallerManager getInstallerManager() {
		return InstallerLocator.getInstallerManager();
	}

	public JShowPackageStatusAndAskContinuePanel createJShowPackageStatusAndAskContinuePanel(
			List<PackageInfo> packages, String message) {
		return new DefaultJShowPackageStatusAndAskContinuePanel(this, packages,
				message);
	}

	public JShowPackagesAndAskContinuePanel createJShowTroubledPackagesAndAskContinuePanel(
			List<PackageInfo> packages, String message) {
		return new DefaultJShowTroubledPackagesAndAskContinuePanel(this, packages,
				message);
	}
	
	public JShowRequiredPackagesAndAskContinuePanel createJShowRequiredPackagesAndAskContinuePanel(
			List<PackageInfo> packages, String message) {
		return new DefaultJShowRequiredPackagesAndAskContinuePanel(this,
				packages, message);
	}

	public JShowUnresolvedDependenciesAndAskContinuePanel createJShowUnresolvedDependenciesAndAskContinuePanel(
			Dependencies dependencies, String message) {
		return new DefaultJShowUnresolvedDependenciesAndAskContinuePanel(this,
				dependencies, message);
	}

	public JPackageInfoPanel createPackageInfoPanel() {
		return new DefaultPackageInfoPanel();
	}

	public JPackageInfoPanel createPackageInfoPanel(String packageType) {
		return new DefaultPackageInfoPanel(packageType);
	}

	public JOutputPanel createOutputPanel() {
		return new DefaultOutputPanel();
	}

	public JProgressPanel createProgressPanel() {
		return new DefaultProgressPanel();
	}

	public AbstractInstallPackageWizard createInstallPackageWizard(
			File applicationDirectory, File installFolder)
			throws InstallPackageWizardException {
		ExtensionPoint ep = extensionPoints
				.add(SWING_INSTALLER_MANAGER_EXTENSION_POINT);
		try {
			Object[] args = new Object[2];
			args[0] = applicationDirectory;
			args[1] = installFolder;
			return (AbstractInstallPackageWizard) ep.create(
					EXECUTE_INSTALLER_WIZARD_NAME, args);

		} catch (Exception e) {
			throw new InstallPackageWizardException(
					"Error creating the wizard", e);
		}
	}

	public MakePluginPackageWizard createMakePluginPackageWizard(
			File applicationDirectory, File installFolder)
			throws MakePluginPackageWizardException {
		ExtensionPoint ep = extensionPoints
				.add(SWING_INSTALLER_MANAGER_EXTENSION_POINT);
		try {
			Object[] args = new Object[2];
			args[0] = applicationDirectory;
			args[1] = installFolder;
			return (MakePluginPackageWizard) ep.create(
					CREATE_INSTALLER_WIZARD_NAME, args);
		} catch (Exception e) {
			throw new MakePluginPackageWizardException(
					"Error creating the wizard", e);
		}
	}

        @Override
        public PackageBuildder createPackagerPanel(String packageType, File selectionFolder, File outputFolder) {
            PackageBuildder packager = new BasePackageWizard(packageType, selectionFolder, outputFolder);
            return packager;
        }
}
