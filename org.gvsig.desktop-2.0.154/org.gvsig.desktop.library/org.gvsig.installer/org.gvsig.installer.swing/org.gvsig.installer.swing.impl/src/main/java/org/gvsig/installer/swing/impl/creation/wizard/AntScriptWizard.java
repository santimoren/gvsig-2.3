/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.wizard;

import java.io.ByteArrayInputStream;

import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.panel.AntScriptPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class AntScriptWizard extends AntScriptPanel implements OptionPanel {

	/**
     * 
     */
	private static final long serialVersionUID = -4741237867123090223L;
	private DefaultMakePluginPackageWizard installerCreationWizard;
	private static final Logger log = LoggerFactory
			.getLogger(AntScriptWizard.class);

	public AntScriptWizard(
			DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_ant_script");
	}

	public void lastPanel() {
		// TODO Auto-generated method stub

	}

	public void nextPanel() throws NotContinueWizardException {
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			documentBuilder.parse(new ByteArrayInputStream(getAntScript()
					.getBytes()));
		} catch (Exception e) {
			throw new NotContinueWizardException("not_valid_xml", e,
					installerCreationWizard);
		}
		PackageInfo packageInfo = installerCreationWizard
				.getSelectedPackageInfo();
		packageInfo.setAntScript(getAntScript());
	}

	public void updatePanel() {
		PackageInfo installerInfo;
		try {
			MakePluginPackageService installerCreationService = installerCreationWizard
					.getInstallerCreationService();
			installerInfo = installerCreationWizard.getSelectedPackageInfo();
			if (installerInfo.getAntScript() != null) {
				setAntScript(installerInfo.getAntScript());
			} else {
				setAntScript(installerCreationService.getDefaultAntScript());
			}
		} catch (MakePluginPackageServiceException e) {
			log.error("There is not a selected installer info", e);
		}
	}

}
