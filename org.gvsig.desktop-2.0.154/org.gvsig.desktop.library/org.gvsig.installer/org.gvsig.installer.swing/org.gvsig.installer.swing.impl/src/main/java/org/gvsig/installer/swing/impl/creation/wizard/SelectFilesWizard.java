/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.wizard;

import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.panel.SelectFilesPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectFilesWizard extends SelectFilesPanel implements OptionPanel {

	private static final long serialVersionUID = 1645239143301238773L;
	private DefaultMakePluginPackageWizard installerCreationWizard;

	public SelectFilesWizard(
			DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_files_to_copy");
	}

	public void lastPanel() {
		// Do nothing

	}

	public void nextPanel() {

		PackageInfo packageInfo = installerCreationWizard
				.getSelectedPackageInfo();

		packageInfo.clearFilesToCopy();
		List<File> selectedFiles = getSelectedFiles();

		for (int i = 0; i < selectedFiles.size(); i++) {
			packageInfo.addFileToCopy(selectedFiles.get(i));
		}
	}

	public void updatePanel() {

		InstallerManager manager = InstallerLocator.getInstallerManager();
		String pluginName = installerCreationWizard.getSelectedPackageInfo()
				.getCode();
		File pluginLocation = manager.getAddonFolder(pluginName);
		File folder = null;

		if (pluginLocation != null) {
			String path = pluginLocation.getAbsolutePath() + File.separator
					+ "install" + File.separator + "files";
			folder = new File(path);
		}

		if (folder != null && folder.exists() && folder.isDirectory()) {
			String msg = Messages
					.getText("_the_folder_install/files_already_exists_do_you_want_to_delete_it_before_proceeding?");

			int resp = JOptionPane.showConfirmDialog(null, msg, Messages
					.getText("_select_an_option"), JOptionPane.YES_NO_OPTION);

			if (resp == JOptionPane.OK_OPTION) {
				PackageInfo packageInfo = installerCreationWizard
						.getSelectedPackageInfo();

				if (!packageInfo.removeFilesFolder(folder)) {
					JOptionPane.showMessageDialog(null, Messages
							.getText("_Couldn't_delete_the_directory"));
				}
			}

		}

	}

}
