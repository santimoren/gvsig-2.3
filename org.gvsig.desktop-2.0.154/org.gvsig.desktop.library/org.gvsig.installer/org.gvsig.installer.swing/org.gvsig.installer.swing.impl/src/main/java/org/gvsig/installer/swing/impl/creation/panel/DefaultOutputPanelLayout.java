package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;


public class DefaultOutputPanelLayout extends JPanel
{
   JLabel fileLabel = new JLabel();
   JTextField fileTextField = new JTextField();
   JButton fileBrowserButton = new JButton();
   JLabel createPackageIndexLabel = new JLabel();
   JCheckBox createPackageIndexCheckBox = new JCheckBox();
   JLabel indexFileLabel = new JLabel();
   JTextField indexFileTextField = new JTextField();
   JButton indexFileBrowserButton = new JButton();
   JLabel packageURLLabel = new JLabel();
   JRadioButton useAbsoluteURLRadioButton = new JRadioButton();
   ButtonGroup buttongroup1 = new ButtonGroup();
   JTextField packageURLTextField = new JTextField();
   JRadioButton usePoolURLRadioButton = new JRadioButton();
   JTextField packageURLPoolTextField = new JTextField();
   private SwingInstallerManager swingInstallerManager;

   /**
    * Default constructor
    */
   public DefaultOutputPanelLayout()
   {
	  swingInstallerManager = SwingInstallerLocator.getSwingInstallerManager();
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      fileLabel.setName("fileLabel");
      fileLabel.setText(swingInstallerManager.getText("select_package_location")+":");
      jpanel1.add(fileLabel,cc.xywh(2,2,5,1));

      fileTextField.setName("fileTextField");
      jpanel1.add(fileTextField,cc.xywh(2,3,4,1));

      fileBrowserButton.setActionCommand("Examinar...");
      fileBrowserButton.setName("fileBrowserButton");
      fileBrowserButton.setText("...");
      jpanel1.add(fileBrowserButton,cc.xy(6,3));

      createPackageIndexLabel.setName("createPackageIndexLabel");
      createPackageIndexLabel.setText(swingInstallerManager.getText("create_package_index"));
      jpanel1.add(createPackageIndexLabel,cc.xywh(2,5,5,1));

      createPackageIndexCheckBox.setActionCommand("Crear indice para el paquete");
      createPackageIndexCheckBox.setName("createPackageIndexCheckBox");
      createPackageIndexCheckBox.setText(swingInstallerManager.getText("create_index_for_package"));
      jpanel1.add(createPackageIndexCheckBox,cc.xywh(2,6,5,1));

      indexFileLabel.setName("indexFileLabel");
      indexFileLabel.setText(swingInstallerManager.getText("select_package_index_location"));
      jpanel1.add(indexFileLabel,cc.xywh(3,7,4,1));

      indexFileTextField.setName("indexFileTextField");
      jpanel1.add(indexFileTextField,cc.xywh(3,8,3,1));

      indexFileBrowserButton.setActionCommand("Examinar ...");
      indexFileBrowserButton.setName("indexFileBrowseerButton");
      indexFileBrowserButton.setText(" ...");
      jpanel1.add(indexFileBrowserButton,cc.xy(6,8));

      packageURLLabel.setName("packageURLLabel");
      packageURLLabel.setText(swingInstallerManager.getText("destination_package_url"));
      jpanel1.add(packageURLLabel,cc.xywh(3,9,4,1));

      useAbsoluteURLRadioButton.setActionCommand("Indique una url absoluta a donde se encontrara el paquete:");
      useAbsoluteURLRadioButton.setName("useAbsoluteURLRadioButton");
      useAbsoluteURLRadioButton.setText(swingInstallerManager.getText("absolute_url_package_location")+":");
      buttongroup1.add(useAbsoluteURLRadioButton);
      jpanel1.add(useAbsoluteURLRadioButton,cc.xywh(3,10,4,1));

      packageURLTextField.setName("packageURLTextField");
      jpanel1.add(packageURLTextField,cc.xywh(4,11,3,1));

      usePoolURLRadioButton.setActionCommand("Indique la URL al pool desde el que se podra descargar su paquete (recomendado):");
      usePoolURLRadioButton.setName("usePoolURLRadioButton");
      usePoolURLRadioButton.setText(swingInstallerManager.getText("pool_url_package_location")+":");
      buttongroup1.add(usePoolURLRadioButton);
      jpanel1.add(usePoolURLRadioButton,cc.xywh(3,12,4,1));

      packageURLPoolTextField.setName("packageURLPoolTextField");
      jpanel1.add(packageURLPoolTextField,cc.xywh(4,13,3,1));

      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5,6,7,8 },new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
