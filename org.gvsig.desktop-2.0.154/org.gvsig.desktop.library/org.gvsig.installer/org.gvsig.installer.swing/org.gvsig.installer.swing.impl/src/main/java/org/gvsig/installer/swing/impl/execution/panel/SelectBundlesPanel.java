/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.gvsig.gui.beans.openfile.FileFilter;
import org.gvsig.gui.beans.openfile.FileTextField;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager.UrlAndLabel;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectBundlesPanel extends JPanel implements ItemListener,
		DocumentListener {

	/**
     * 
     */
	private static final long serialVersionUID = -6580729307001414868L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JRadioButton fileRadioButton;
	private ButtonGroup buttonGroup;
	private JPanel northPanel;
	private FileTextField selectFileText;
	private JRadioButton standardRadioButton;
	private JComboBox downloadURL;
	private JRadioButton urlRadioButton;
	private final List<UrlAndLabel> defaultDownloadURL;

	public SelectBundlesPanel(List<UrlAndLabel> defaultDownloadURL2, File installFolder) {
		super();
		this.defaultDownloadURL = defaultDownloadURL2;
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
		initListeners();
		
		String[] files = installFolder.list();
		
		if (files!=null && files.length >0){
			standardRadioButton.setSelected(true);
			urlRadioButton.setSelected(false);	
		} else {
			urlRadioButton.setSelected(true);	
			standardRadioButton.setEnabled(false);
		}
	}

	private void initListeners() {
		standardRadioButton.addItemListener(this);
		fileRadioButton.addItemListener(this);
		urlRadioButton.addItemListener(this);
		Object obj = selectFileText.getComponent(0);
		if ((obj != null) && (obj instanceof JTextField)) {
			((JTextField) obj).getDocument().addDocumentListener(this);
		}
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		northPanel = new JPanel();
		standardRadioButton = new JRadioButton();
		fileRadioButton = new JRadioButton();
		urlRadioButton = new JRadioButton();
		selectFileText = new FileTextField();
		selectFileText.setFileFilter(new FileFilter() {

			private String packageExt = swingInstallerManager
					.getInstallerManager().getDefaultPackageFileExtension();
			private String packageSetExt = swingInstallerManager
					.getInstallerManager().getDefaultPackageSetFileExtension();
			private String indexSetExt = swingInstallerManager
					.getInstallerManager().getDefaultIndexSetFileExtension();

			@Override
			public String getDescription() {
			    
				return swingInstallerManager.getText(
				    "_gvSIG_packages_and_packages_and_index_sets")
				    + " (*." + packageExt + ", *." + packageSetExt
				    + ", *." + indexSetExt + ")";
			}

			@Override
			public boolean accept(File file) {
				if (file.isFile()) {
					String name = file.getName().toLowerCase();
					return name.endsWith(packageExt)
							|| name.endsWith(packageSetExt)
							|| name.endsWith(indexSetExt);
				}
				return true;
			}

			@Override
			public String getDefaultExtension() {
				return packageSetExt;
			}
		});

		downloadURL = new JComboBox();
		downloadURL.setEditable(true);
		if (defaultDownloadURL != null) {
			for (int i = 0; i < defaultDownloadURL.size(); i++) {
				downloadURL.addItem(defaultDownloadURL.get(i));
			}
		}

		buttonGroup = new ButtonGroup();

		buttonGroup.add(standardRadioButton);
		buttonGroup.add(fileRadioButton);
		buttonGroup.add(urlRadioButton);

		setLayout(new BorderLayout());

		northPanel.setLayout(new GridBagLayout());

		standardRadioButton.setText(swingInstallerManager
				.getText("_standard_installation") +
				" (" +
				swingInstallerManager.getText("_install_addons_in_gvsig_standard_dist") +
				")");

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		northPanel.add(standardRadioButton, gridBagConstraints);

		fileRadioButton.setText(swingInstallerManager
				.getText("_installation_from_file") +
		             " (" +
		             swingInstallerManager.getText("_install_addons_in_gvspki_or_gvspks_file") +
		             ")");

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		northPanel.add(fileRadioButton, gridBagConstraints);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new Insets(2, 20, 2, 2);
		northPanel.add(selectFileText, gridBagConstraints);

		urlRadioButton.setText(swingInstallerManager
				.getText("_installation_from_url") +
	             " (" +
	             swingInstallerManager.getText("_install_addons_from_remote_repo") +
	             ")");
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		northPanel.add(urlRadioButton, gridBagConstraints);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new Insets(2, 20, 2, 2);
		northPanel.add(downloadURL, gridBagConstraints);

		add(northPanel, java.awt.BorderLayout.NORTH);
	}

	public void itemStateChanged(ItemEvent e) {
		selectFileText.setEnabled(fileRadioButton.isSelected());
		downloadURL.setEnabled(urlRadioButton.isSelected());
		downloadURL.setEditable(urlRadioButton.isSelected());
		checkNextButtonEnabled();
	}

	protected File getSelectedFile() {
		return selectFileText.getSelectedFile();
	}

	protected URL getSelectedURL() throws MalformedURLException {
		Object value = downloadURL.getSelectedItem();
		if( value instanceof UrlAndLabel) {
			// clicked fron the combo
			return ((UrlAndLabel)value).getURL();
		} else {
			// String entered in the text field
			return new URL(value.toString());
		}

	}

	protected boolean isStandardSelected() {
		return standardRadioButton.isSelected();
	}

	protected boolean isFileSelected() {
		return fileRadioButton.isSelected();
	}

	protected boolean isURLSelected() {
		return urlRadioButton.isSelected();
	}

	protected boolean isNextButtonEnabled() {
		if (isStandardSelected()) {
			return true;
		}
		if (isFileSelected()) {
			File file = selectFileText.getSelectedFile();
			if (file == null) {
				return false;
			}
			return file.exists();
		}
		if (isURLSelected()) {
			try {
				getSelectedURL();
				return true;
			} catch (MalformedURLException e) {
				return false;
			}
		}
		return false;
	}

	protected void checkNextButtonEnabled() {

	}

	public void changedUpdate(DocumentEvent e) {
		checkNextButtonEnabled();
	}

	public void insertUpdate(DocumentEvent e) {
		checkNextButtonEnabled();
	}

	public void removeUpdate(DocumentEvent e) {
		checkNextButtonEnabled();
	}

}
