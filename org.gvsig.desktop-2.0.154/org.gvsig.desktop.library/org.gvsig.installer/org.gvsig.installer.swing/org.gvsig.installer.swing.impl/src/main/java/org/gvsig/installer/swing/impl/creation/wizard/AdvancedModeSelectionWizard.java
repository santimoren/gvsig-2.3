/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.wizard;

import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.panel.AdvancedModeSelectionPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class AdvancedModeSelectionWizard extends AdvancedModeSelectionPanel
		implements OptionPanel {

	private static final long serialVersionUID = 257540289911849694L;
	private DefaultMakePluginPackageWizard installerCreationWizard;

	public AdvancedModeSelectionWizard(
			DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_advanced_options");
	}

	public void lastPanel() {
		// TODO Auto-generated method stub
	}

	public void nextPanel() {

		if (!isAdvancedModeSelected()) {

			InstallerManager manager = InstallerLocator.getInstallerManager();
			String pluginName = installerCreationWizard
					.getSelectedPackageInfo().getCode();
			File pluginLocation = manager.getAddonFolder(pluginName);
			File folder = null;

			if (pluginLocation != null) {
				String path = pluginLocation.getAbsolutePath() + File.separator
						+ "install";
				folder = new File(path);
			}

			if (folder != null && folder.exists()) {
				String msg = Messages
						.getText("_the_folder_install_already_exists._do_you_want_to_delete_it_before_proceeding?");

				int resp = JOptionPane.showConfirmDialog(null, msg, Messages
						.getText("_select_an_option"),
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.OK_OPTION) {
					PackageInfo packageInfo = installerCreationWizard
							.getSelectedPackageInfo();
					boolean success = packageInfo.removeInstallFolder(folder);

					if (!success) {
						JOptionPane.showMessageDialog(null, Messages
								.getText("_Couldn't_delete_the_directory"));
					}
				}
			}
		}
		installerCreationWizard
				.setAdvancedModeSelected(isAdvancedModeSelected());
	}

	public void updatePanel() {
            // TODO Auto-generated method stub
            PackageInfo pkg = this.installerCreationWizard.getSelectedPackageInfo();
            if( pkg.getType().equalsIgnoreCase("plugin") ) {
                setAvancedModeSupported(true);
            } else {
                setAvancedModeSupported(false);
            }
	}

}
