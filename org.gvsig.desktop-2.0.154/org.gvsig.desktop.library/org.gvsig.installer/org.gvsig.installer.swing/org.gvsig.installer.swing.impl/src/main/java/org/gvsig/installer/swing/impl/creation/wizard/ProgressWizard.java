/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */
package org.gvsig.installer.swing.impl.creation.wizard;

import java.io.File;

import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.creation.DefaultMakePluginPackageWizard;
import org.gvsig.installer.swing.impl.panel.DefaultProgressPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.task.SimpleTaskStatus;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class ProgressWizard extends DefaultProgressPanel implements OptionPanel {

	private static final long serialVersionUID = -2940454513497249659L;
	private DefaultMakePluginPackageWizard installerCreationWizard;
	private DefaultSwingInstallerManager swingInstallerManager;

	public ProgressWizard(DefaultMakePluginPackageWizard installerCreationWizard) {
		super();
		this.installerCreationWizard = installerCreationWizard;
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_installer_progress");
	}

	public void lastPanel() {
		installerCreationWizard.setFinishButtonEnabled(false);
		installerCreationWizard.setCancelButtonEnabled(true);
	}

	public void nextPanel() {
		// Do nothing
	}

	public void updatePanel() {
		MakePluginPackageService installerCreationService = installerCreationWizard
				.getInstallerCreationService();

		PackageInfo selectedPackageInfo = installerCreationWizard
				.getSelectedPackageInfo();
		File originalPackageFolder = installerCreationWizard
				.getOriginalPluginFolder();

		SimpleTaskStatus taskStatus = ToolsLocator.getTaskStatusManager()
				.createDefaultSimpleTaskStatus(selectedPackageInfo.getName());
		this.bind(taskStatus);
		try {
			// Compress the plugin
			taskStatus.message(swingInstallerManager.getText("_Compressing"));
			installerCreationService.preparePackage(selectedPackageInfo,
					originalPackageFolder);
			installerCreationService.createPackage(selectedPackageInfo,
					installerCreationWizard.getOutputStream());
			if (installerCreationWizard.getIndexOutputStream() != null) {
				taskStatus.message(swingInstallerManager
						.getText("_Creating_index"));
				PackageInfo info = installerCreationWizard
						.getSelectedPackageInfo();
				File pluginFolder = installerCreationWizard
						.getMakePluginPackageService().getPluginFolder(info);

				PackageInfo infoIndex = (PackageInfo) info.clone();
				infoIndex.setDownloadURL(installerCreationWizard
						.getDownloadURL());

				installerCreationService.writePackageInfoForIndex(infoIndex,
						pluginFolder);
				installerCreationService.createPackageIndex(info,
						installerCreationWizard.getIndexOutputStream());
			}
			taskStatus.terminate();

			// Set the finished text
			taskStatus.message(swingInstallerManager.getText("_Finished"));
		} catch (MakePluginPackageServiceException e) {
			this.showErrorMessage(swingInstallerManager
					.getText("_Cant_create_package"), e);
		} catch (CloneNotSupportedException e) {
			this.showErrorMessage(swingInstallerManager
					.getText("_Cant_create_package"), e);
		}
		installerCreationWizard.setFinishButtonEnabled(true);
		installerCreationWizard.setCancelButtonEnabled(false);
	}

}
