/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class AdvancedModeSelectionPanel extends JPanel {

	/**
     * 
     */
	private static final long serialVersionUID = -4336979089415520761L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JCheckBox advancedModeCheckBox;

	private JScrollPane advancedModeScrollPane;
	private JTextArea advancedModeTextArea;

	public AdvancedModeSelectionPanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
		initLabels();
	}

	private void initLabels() {
		advancedModeTextArea.setText(swingInstallerManager
				.getText("_advanced_mode"));
	}

	private void initComponents() {
		advancedModeCheckBox = new JCheckBox();
		advancedModeScrollPane = new JScrollPane();
		advancedModeTextArea = new JTextArea();

		setLayout(new java.awt.BorderLayout());

		advancedModeCheckBox.setText(swingInstallerManager
				.getText("_enable_advanced_mode"));
		add(advancedModeCheckBox, BorderLayout.NORTH);

		advancedModeScrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5,
				5, 5));

		advancedModeTextArea.setBackground(this.getBackground());
		advancedModeTextArea.setColumns(20);
		advancedModeTextArea.setLineWrap(true);
		advancedModeTextArea.setRows(5);
		advancedModeTextArea.setBorder(BorderFactory.createEmptyBorder(1, 1, 1,
				1));
		advancedModeScrollPane.setViewportView(advancedModeTextArea);

		add(advancedModeScrollPane, BorderLayout.CENTER);
	}

	/**
	 * @return the advancedMode
	 */
	public boolean isAdvancedModeSelected() {
		return advancedModeCheckBox.isSelected();
	}
        
        public void setAvancedModeSupported(boolean supported) {
            this.advancedModeCheckBox.setEnabled(supported);
            if( !supported ) {
                this.advancedModeCheckBox.setSelected(false);
            }
            //this.advancedModeTextArea.setEnabled(supported);
        }
}
