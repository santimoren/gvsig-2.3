/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.api.execution.JShowPackageStatusAndAskContinuePanel;

public class DefaultJShowPackageStatusAndAskContinuePanel extends
		JShowPackageStatusAndAskContinuePanel implements ActionListener {

	private static final long serialVersionUID = -3142143854199963997L;
	private List<String> packages;
	private boolean doCancel = true;
	DefaultSwingInstallerManager swingInstallerManager;

	public DefaultJShowPackageStatusAndAskContinuePanel(
			SwingInstallerManager manager, List<PackageInfo> packages,
			String message) {
		this.swingInstallerManager = (DefaultSwingInstallerManager) manager;
		this.buildPackageList(packages);
		this.createUI(manager, message);
	}

	private void buildPackageList(List<PackageInfo> packages) {
		this.packages = new ArrayList<String>();
		for (int i = 0; i < packages.size(); i++) {
			PackageInfo pkg = packages.get(i);
			if (!pkg.isOfficial()) {
				this.packages.add(pkg.getName() + " ("
						+ swingInstallerManager.getText("_not_official") + ")");
			} else {
			    
			    if (InstallerManager.STATE.FINAL.equalsIgnoreCase(pkg.getState())) {
			        continue;
			    } else if (InstallerManager.STATE.TESTING.equalsIgnoreCase(pkg.getState())) {
			        continue;
                } else if (pkg.getState().toLowerCase().startsWith(InstallerManager.STATE.RC.toLowerCase())) {
                    continue;
                } else if (pkg.getState().toLowerCase().startsWith(InstallerManager.STATE.BETA.toLowerCase())) {
                    continue;
                } else {
                    this.packages.add(pkg.getName() + " (" + pkg.getState() + ")");
                }
			}
		}
	}

	private void createUI(SwingInstallerManager manager, String message) {
		this.setLayout(new BorderLayout());

		JButton cancelButton = new JButton(swingInstallerManager
				.getText("_cancel"));
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		//
		final JButton continueButton = new JButton(swingInstallerManager
				.getText("_continue"));
		continueButton.setActionCommand("continue");
		continueButton.addActionListener(this);

		JList list = new JList(this.packages.toArray());

		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(250, 80));
		listScroller.setAlignmentX(LEFT_ALIGNMENT);

		JPanel listPane = new JPanel();
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
		
		String html_msg = getHtmlText(message);
		JLabel label = new JLabel(html_msg);
		label.setLabelFor(list);
		listPane.add(label);
		listPane.add(Box.createRigidArea(new Dimension(0, 5)));
		listPane.add(listScroller);
		listPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(cancelButton);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(continueButton);

		this.add(listPane, BorderLayout.CENTER);
		this.add(buttonPane, BorderLayout.PAGE_END);
		
		this.setPreferredSize(new Dimension(500, 300));
	}


	/*
	 * Utility method to HTML code (this allows multiline in Swing
	 * components)
	 */
    public static String getHtmlText(String txt) {
        String resp = txt.replace("\n", "<br><br>");
        resp = "<html>" + resp + "</html>";
        return resp;
    }

    @Override
	public boolean cancelled() {
		return doCancel;
	}

	@Override
	public boolean needShow() {
		return this.packages.size() > 0;
	}

	public void actionPerformed(ActionEvent e) {
		if ("cancel".equals(e.getActionCommand())) {
			doCancel = true;
		} else {
			doCancel = false;
		}
		this.setVisible(false);
	}
}
