/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultOutputPanel extends JOutputPanel {

	private static final long serialVersionUID = 1219577194134960697L;

	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private DefaultOutputPanelLayout form = null;

	private PackageInfo packageInfo = null;
	
	public DefaultOutputPanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {
		this.form = new DefaultOutputPanelLayout();
		this.form.indexFileBrowserButton.setEnabled(false);
		this.form.indexFileTextField.setEditable(false);
		this.form.packageURLTextField.setEnabled(false);
		this.form.packageURLPoolTextField.setEnabled(false);
		this.form.useAbsoluteURLRadioButton.setEnabled(false);
		this.form.usePoolURLRadioButton.setEnabled(false);
		this.form.createPackageIndexCheckBox.setSelected(false);
		
                this.form.usePoolURLRadioButton.setSelected(true);
                
		this.form.indexFileBrowserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File[] f = showOpenFileDialog("_Select_package_index_file",null);
				if( f!=null ) {
					setPackageIndexFile(f[0]);
				}
			}
		});
		this.form.fileBrowserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File[] f = showOpenFileDialog("_Select_package_file",null);
				if( f!=null ) {
					setPackageFile(f[0]);
				}
			}
		});
		this.form.createPackageIndexCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean x = form.createPackageIndexCheckBox.isSelected();
				setCreatePackageIndex(x);
			}
		});
		this.form.useAbsoluteURLRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if( form.useAbsoluteURLRadioButton.isSelected() ) {
					form.packageURLTextField.setEnabled(true);
					form.packageURLPoolTextField.setEnabled(false);
				} else {
					form.packageURLTextField.setEnabled(false);
					form.packageURLPoolTextField.setEnabled(true);
				}
			}
		});
		this.form.usePoolURLRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if( form.useAbsoluteURLRadioButton.isSelected() ) {
					form.packageURLTextField.setEnabled(true);
					form.packageURLPoolTextField.setEnabled(false);
				} else {
					form.packageURLTextField.setEnabled(false);
					form.packageURLPoolTextField.setEnabled(true);
				}
			}
		});
		
		
		setLayout(new BorderLayout());
		add(this.form, BorderLayout.CENTER);
	}

	@Override
	public void setPackageInfo(PackageInfo packageInfo) {
		this.packageInfo = packageInfo;
	}
	
	@Override
	public File getPackageFile() {
		String s = this.form.fileTextField.getText().trim();
		if( s.length()==0 ) {
			return null;
		}
		return new File(s);
	}

	@Override
	public void setPackageFile(File selectedFile) {
		this.form.fileTextField.setText(selectedFile.toString());
	}

	@Override
	public boolean isCreatePackageIndex() {
		return this.form.createPackageIndexCheckBox.isSelected();
	}

	@Override
	public void setCreatePackageIndex(boolean create) {
		if( this.form.createPackageIndexCheckBox.isSelected() != create ) { 
			this.form.createPackageIndexCheckBox.setSelected(create);
		}
		if( create ) {
			this.form.indexFileBrowserButton.setEnabled(true);
			this.form.indexFileTextField.setEditable(true);
			this.form.packageURLTextField.setEnabled(true);
			this.form.packageURLPoolTextField.setEnabled(true);
			this.form.useAbsoluteURLRadioButton.setEnabled(true);
			this.form.usePoolURLRadioButton.setEnabled(true);
		} else {
			this.form.indexFileBrowserButton.setEnabled(false);
			this.form.indexFileTextField.setEditable(false);
			this.form.packageURLTextField.setEnabled(false);
			this.form.packageURLPoolTextField.setEnabled(false);
			this.form.useAbsoluteURLRadioButton.setEnabled(false);
			this.form.usePoolURLRadioButton.setEnabled(false);
		}
	}

	@Override
	public File getPackageIndexFile() {
		if ( !isCreatePackageIndex()) {
			return null;
		}
		String s = this.form.indexFileTextField.getText().trim();
		if( s.length()==0 ) {
			return null;
		}
		return new File(s);
	}

	@Override
	public void setPackageIndexFile(File selectedIndexFile) {
		this.form.indexFileTextField.setText(selectedIndexFile.toString());
	}

	@Override
	public URL getDownloadURL() {
		String s = null;
		if (! isCreatePackageIndex()) {
			return null;
		}
		try {
			if( this.form.useAbsoluteURLRadioButton.isSelected() ) {
				s = this.form.packageURLTextField.getText();
			} else {
				s = this.form.packageURLPoolTextField.getText();
				File ff = this.getPackageFile();
				PackageInfo pkg = this.getPackageInfo();
				if( pkg == null ) {
					this.form.useAbsoluteURLRadioButton.setSelected(true);
					this.form.usePoolURLRadioButton.setEnabled(false);
					JOptionPane.showMessageDialog(null, swingInstallerManager
							.getText("_Cant_use_pool_URL"));
					return null;
				}
				String fileName = InstallerLocator.getInstallerManager()
						.getPackageFileName(pkg);
				s = s + pkg.getCode() + "/" + fileName;
			}
			return new URL(s);
		} catch (MalformedURLException e) {
			JOptionPane.showMessageDialog(null, swingInstallerManager
					.getText("_invalid_download_url") + ": "
							+ this.form.packageURLTextField.getText());
			return null;
		} catch(Exception ex) {
			return null;
		}
	}

	@Override
	public void setDownloadURL(URL downloadURL) {
		this.form.useAbsoluteURLRadioButton.setSelected(false);
		this.form.usePoolURLRadioButton.setSelected(true);
		if( downloadURL==null ) {
			this.form.packageURLPoolTextField.setText("");
			this.form.packageURLTextField.setText("");
			return;
		}
		this.form.packageURLTextField.setText(downloadURL.toString());
		try {
			File ff = new File(downloadURL.getPath());
			String s = ff.getParentFile().getParentFile().getAbsolutePath();
			URL url = new URL(downloadURL, s);
			this.form.packageURLPoolTextField.setText(url.toString());
		} catch (MalformedURLException e) {
			this.form.packageURLPoolTextField.setText("");
		}
	}
	
	private File[] showChooserDialog(
			final String title,
			final int type, // SAVE_DIALOG / OPEN_DIALOG
			final int selectionMode, //    JFileChooser.FILES_ONLY, JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES
			final boolean multiselection, 
			final File initialPath,
			final FileFilter filter,
			final boolean fileHidingEnabled
			) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(title);
		fc.setDialogType(type);
		fc.setFileSelectionMode(selectionMode);
		fc.setMultiSelectionEnabled(multiselection);
		fc.setCurrentDirectory(initialPath);
		fc.setFileFilter(filter);
		fc.setFileHidingEnabled(fileHidingEnabled);
		int r = JFileChooser.CANCEL_OPTION;
		switch(type) {
		case JFileChooser.SAVE_DIALOG:
			r = fc.showSaveDialog(this);
			break;
		case JFileChooser.OPEN_DIALOG:
		default:
			r = fc.showOpenDialog(this);
			break;
		}
		if( r != JFileChooser.APPROVE_OPTION ) {
			return null;
		}
		if( fc.isMultiSelectionEnabled() ) {
			return fc.getSelectedFiles();
		} else {
			return new File[] { fc.getSelectedFile() };
		}
	}
	
	private File[] showOpenFileDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
	}
	
	protected PackageInfo getPackageInfo() {
		return this.packageInfo;
	}
}
