/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.packagebuilder.options;


import java.io.File;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.api.creation.JPackageInfoPanel;
import org.gvsig.installer.swing.impl.packagebuilder.BasePackageWizard;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.packageutils.PackageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PackageInfoOption implements OptionPanel {

    private static final Logger logger = LoggerFactory.getLogger(PackageInfoOption.class);
    
    private final JPackageInfoPanel packageInfoPanel;

    private final BasePackageWizard wizard;

    public PackageInfoOption(BasePackageWizard wizard) {
        super();
        this.wizard = wizard;
        SwingInstallerManager installerManager = SwingInstallerLocator.getSwingInstallerManager();

        this.packageInfoPanel = installerManager.createPackageInfoPanel(wizard.getPackageType());
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

    @Override
    public JPanel getJPanel() {
        return this.packageInfoPanel;
    }

    @Override
    public String getPanelTitle() {
        return getText("_package_description");
    }

    @Override
    public void lastPanel() {
        // TODO Auto-generated method stub

    }

    @Override
    public void nextPanel() throws NotContinueWizardException {
        if (!this.packageInfoPanel.validatePanel()) {
            throw new NotContinueWizardException("", null, false);
        }
        this.packageInfoPanel.panelToPackageInfo(this.wizard.getPackageInfo());
        PackageManager pkgmgr = ToolsLocator.getPackageManager();
        File fileout = new File(wizard.getFolderToPackaging(),"package.info");
        try {
            pkgmgr.writePacakgeInfo(
                    (org.gvsig.tools.packageutils.PackageInfo)(wizard.getPackageInfo()),
                    fileout
            );
        } catch (Exception ex) {
            logger.warn("Can't save package information (file="+fileout.getAbsolutePath()+").",ex);
            JOptionPane.showMessageDialog(
                    null, 
                    Messages.getText("_cant_save_package_information_in_the_selected_folder"),
                    Messages.getText("_Warning"),
                    JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    @Override
    public void updatePanel() {
        this.packageInfoPanel.packageInfoToPanel(this.wizard.getPackageInfo());
    }

}
