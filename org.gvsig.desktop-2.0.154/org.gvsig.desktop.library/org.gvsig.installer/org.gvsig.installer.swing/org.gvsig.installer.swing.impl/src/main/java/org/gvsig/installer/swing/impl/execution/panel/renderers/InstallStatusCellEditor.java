/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.renderers;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;

import org.gvsig.installer.swing.impl.execution.panel.SelectPackagesPanel;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageStatus;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class InstallStatusCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 1271382732175530111L;
	PackageStatus status;
	SelectPackagesPanel panel;

	public InstallStatusCellEditor(SelectPackagesPanel selectPackagesPanel) {
		super(new JCheckBox());
		this.panel = selectPackagesPanel;
	}

	@Override
	public Component getTableCellEditorComponent(JTable arg0, Object value,
			boolean isSelected, int row, int col) {
		JCheckBox check = (JCheckBox) this.getComponent();
		status = (PackageStatus) value;

		switch (status) {

		case INSTALLED:
			check.setEnabled(true);
			check.setSelected(false);
			break;

		case TO_REINSTALL:
			check.setEnabled(true);
			check.setSelected(true);
			break;

		case NOT_INSTALLED:
			check.setEnabled(true);
			check.setSelected(false);
			break;

		case TO_INSTALL:
			check.setEnabled(true);
			check.setSelected(true);
			break;

		case INSTALLED_NOT_INSTALLABLE:
			check.setEnabled(false);
			break;

		case INSTALLATION_NOT_AVAILABLE:
			check.setEnabled(false);
			check.setSelected(false);
			break;

		case BROKEN:
			check.setEnabled(false);
			check.setSelected(false);
			break;

		default:
			check.setSelected(false);
			check.setEnabled(false);
			break;
		}
		return check;
	}

	@Override
	public Object getCellEditorValue() {
		JCheckBox check = (JCheckBox) this.getComponent();
		switch (status) {
		case INSTALLED:
			if (check.isSelected()) {
				status = PackageStatus.TO_REINSTALL;
			}
			break;

		case TO_REINSTALL:
			if (!check.isSelected()) {
				status = PackageStatus.INSTALLED;
			}
			break;

		case NOT_INSTALLED:
			if (check.isSelected()) {
				status = PackageStatus.TO_INSTALL;
			}
			break;

		case TO_INSTALL:
			if (!check.isSelected()) {
				status = PackageStatus.NOT_INSTALLED;
			}
			break;

		default:
			break;
		}
		return status;
	}
}
