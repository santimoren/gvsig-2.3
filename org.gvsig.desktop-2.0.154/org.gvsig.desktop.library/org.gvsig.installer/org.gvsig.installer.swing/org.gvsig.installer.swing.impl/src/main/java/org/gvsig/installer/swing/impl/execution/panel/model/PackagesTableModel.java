/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution.panel.model;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.filters.NameDescriptionOrCodeFilter;
import org.gvsig.installer.swing.impl.execution.panel.filters.PackageFilter;
import org.gvsig.tools.packageutils.PackageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PackagesTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -454014676003979512L;
	private Logger logger = LoggerFactory.getLogger(PackagesTableModel.class);

	public enum PackageStatus {
		INSTALLED, NOT_INSTALLED, TO_REINSTALL, TO_INSTALL, INSTALLED_NOT_INSTALLABLE, BROKEN, INSTALLATION_NOT_AVAILABLE;
	}

	public enum PackageOfficialRecommended {
		OFFICIAL_NOT_RECOMMENDED, OFFICIAL_RECOMMENDED, NOT_OFFICIAL_RECOMMENDED, NOT_OFFICIAL_NOT_RECOMMENDED
	}

	public class PackageOsAndArchitecture {
		public String osfamily;
		public String osname;
                public String osversion;
		public String arch;
                
                public String getOperatingSystemId() {
                    if( StringUtils.isEmpty(osname)) {
                        return osfamily;
                    }
                    return osfamily + "-" + osname;
                }
	}
	
//	
//	public enum PackageOsAndArchitecture {
//		WINDOLS_32, WINDOLS_64, LINUX_32, LINUX_64, ALL, OTHER
//	}

	private TablePackageInfo[] currentlyVisiblePackages = null;

	private final String[] columnNames;

	private InstallerManager installerManager = InstallerLocator
			.getInstallerManager();
	private InstallPackageService installerExecutionService;

	boolean isOfficial;
	private NameDescriptionOrCodeFilter packageFastFilter = null;
	private PackageFilter packageFilter = null;
	
	private Set<PackageInfo> selectedPackages = new HashSet<PackageInfo>();
	private Map<String, TablePackageInfo> allPackages = null;

	public PackagesTableModel(SwingInstallerManager swingInstallerManager,
			InstallPackageService installerExecutionService, boolean isOfficial) {

		this.installerExecutionService = installerExecutionService;
		this.isOfficial = isOfficial;
		
		Map<String, TablePackageInfo> infos = getDefaultPackages();

		currentlyVisiblePackages = infos.values().toArray(
				new TablePackageInfo[infos.size()]);
		sortInstallerInfos();

		columnNames = new String[] {
				"", // Check column
				"", "", swingInstallerManager.getText("_name"),
				swingInstallerManager.getText("_version"),
				swingInstallerManager.getText("_type") };

	}

	private void sortInstallerInfos() {
		Arrays.sort(currentlyVisiblePackages, new Comparator<TablePackageInfo>() {

			public int compare(TablePackageInfo o1, TablePackageInfo o2) {
				PackageInfo p1 = o1.getPackageInfo();
				PackageInfo p2 = o2.getPackageInfo();
				String s1 = p1.getName() + "/" + p1.getVersion().fullFormat() + "/" + p1.getOperatingSystem() + "/" + p1.getArchitecture();
				String s2 = p2.getName() + "/" + p2.getVersion().fullFormat() + "/" + p2.getOperatingSystem() + "/" + p2.getArchitecture();
				return s1.compareToIgnoreCase(s2);
			}
		});
	}

	private boolean canSetAsDefault(List<String> defaultIDs, PackageInfo pkg) {
    	String system_osfamily = installerManager.getOperatingSystemFamily();
        String system_arch = installerManager.getArchitecture();
    	String pkg_os = pkg.getOperatingSystem(); 
    	String pkg_arch = pkg.getArchitecture();
        if (defaultIDs != null) {
            for (int j = 0; j < defaultIDs.size(); j++) {
            	String code = defaultIDs.get(j);
                if (pkg.matchID(code)) {
                    if ( pkg.getOperatingSystemFamily().equals(system_osfamily) || pkg_os.equals(PackageManager.OS.ALL)) {
                        if( PackageManager.ARCH.ALL.equalsIgnoreCase(pkg_arch) ) {
                                return true;
                        } else if( system_arch.equalsIgnoreCase(pkg_arch) ) {
                                return true;
                        }
                    }
                }
            }
        }
        logger.info("package '"+pkg.getCode()+"/"+pkg_os+"/"+pkg_arch+"' don't install as default.");
		return false;
	}
	
    private Map<String, TablePackageInfo> getDefaultPackages() {

        if (allPackages == null) {
            List<String> defaultIDs =
                installerExecutionService.getDefaultSelectedPackagesIDs();
            allPackages =
                new HashMap<String, PackagesTableModel.TablePackageInfo>();
            // Add installable package infos
            for (int i = 0; i < installerExecutionService.getPackageCount(); i++) {
                PackageInfo installerInfo =
                    installerExecutionService.getPackageInfo(i);
                TablePackageInfo info =
                    new TablePackageInfo(installerInfo, false, true);
            	PackageInfo pkg = info.getPackageInfo();
            	if( canSetAsDefault(defaultIDs, pkg) ) {
            		info.setDefault(true);
            	}
                allPackages.put(info.getID(), info);
            }
            // Add already installed package infos
            try {
                PackageInfo[] installedPackages =
                    installerManager.getInstalledPackages();

                for (int i = 0; i < installedPackages.length; i++) {
                    TablePackageInfo info =
                        new TablePackageInfo(installedPackages[i], true, false);
                    TablePackageInfo x = allPackages.get(info.getID());
                    if (x == null) {
                        allPackages.put(info.getID(), info);
                    } else {
                        x.setInstalled(true);
                    }
                }
            } catch (MakePluginPackageServiceException e) {
                throw new RuntimeException(e);
            }
            
            // ==========================================
            // Set default
            
            TablePackageInfo tpi = null;
            String os = null;
            List<String> idList = installerExecutionService
                .getDefaultSelectedPackagesIDs();
            
            if (idList != null) {
                for (int i=0; i<idList.size(); i++) {
                    
                    tpi = getMatch(idList.get(i), allPackages);
                    if (tpi != null) {
                        os = tpi.getPackageInfo().getOperatingSystem();
                        if (os.equals(installerManager.getOperatingSystem())
                            || os.equals("all")) {
                            
                            tpi.setDefault(true);
                        }
                    }
                }
            }
        }
        return allPackages;
    }
    
    private TablePackageInfo getMatch(String id, Map<String, TablePackageInfo> map) {
        
        Iterator<TablePackageInfo> iter = map.values().iterator();
        TablePackageInfo item = null;
        while (iter.hasNext()) {
            item = iter.next();
            if (item.getPackageInfo().matchID(id)) {
                return item;
            }
        }
        return null;
    }

	// get filtered packages
	private Map<String, TablePackageInfo> getFilteredPackages(
			Map<String, TablePackageInfo> packageInfos) {

		Map<String, TablePackageInfo> infos = new HashMap<String, TablePackageInfo>();

		TablePackageInfo[] packs = packageInfos.values().toArray(
				new TablePackageInfo[infos.size()]);

		for (int i = 0; i < packs.length; i++) {
			PackageInfo installerInfo = packs[i].getPackageInfo();

			if (packageFilter.match(installerInfo)) {
				TablePackageInfo info = packs[i];
				infos.put(installerInfo.getID(), info);
			}
		}
		return infos;
	}

	// get fast filtered packages (by name or description)
	private Map<String, TablePackageInfo> getFastFilteredPackages(
			Map<String, TablePackageInfo> packageInfos) {

		Map<String, TablePackageInfo> infos = new HashMap<String, TablePackageInfo>();

		TablePackageInfo[] packs = packageInfos.values().toArray(
				new TablePackageInfo[infos.size()]);

		for (int i = 0; i < packs.length; i++) {
			PackageInfo installerInfo = packs[i].getPackageInfo();

			if (packageFastFilter.match(installerInfo)) {
				TablePackageInfo info = packs[i];
				infos.put(installerInfo.getID(), info);
			}
		}
		return infos;
	}

	public void updatePackages() {

		Map<String, TablePackageInfo> infos = getDefaultPackages();

		// check if there is a filter set (category, type, etc)
		if (isPackageFilterSet()) {
			infos = getFilteredPackages(infos);
		}

		// check if there is any filter set (fastFilter)
		if (isFastFilterSet()) {
			infos = getFastFilteredPackages(infos);
		}

		currentlyVisiblePackages = infos.values().toArray(
				new TablePackageInfo[infos.size()]);
		sortInstallerInfos();
	}


	public void selectDefaultPackages() {
	    
	    Iterator<String> iter = allPackages.keySet().iterator();
	    TablePackageInfo tpi = null;
	    while (iter.hasNext()) {
	        
	        tpi = allPackages.get(iter.next());
	        if (tpi.isDefault()) {
	            tpi.setSelected(true);
	        }
	    }
	}

	public void selectPackage(PackageInfo pkg) {
	    
	    Iterator<String> iter = allPackages.keySet().iterator();
        TablePackageInfo tpi = null;
        while (iter.hasNext()) {
            
            tpi = allPackages.get(iter.next());
            if (pkg.equals(tpi.getPackageInfo())) {
                tpi.setSelected(true);
                return;
            }
        }
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return PackageStatus.class;
		case 1:
			return PackageOfficialRecommended.class;
		case 2:
			return PackageOsAndArchitecture.class;
		default:
			return String.class;
		}
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnIndex >= columnNames.length ? ""
				: columnNames[columnIndex];
	}

	public int getRowCount() {
		return currentlyVisiblePackages.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		TablePackageInfo tableInfo = currentlyVisiblePackages[rowIndex];
		PackageInfo installerInfo = tableInfo.getPackageInfo();

		switch (columnIndex) {
		case 0:
			PackageStatus installStatus = checkStatus(rowIndex);
			return installStatus;
		case 1:
			PackageOfficialRecommended officialRecommended = checkOfficialRecommended(rowIndex);
			return officialRecommended;
		case 2:
			PackageOsAndArchitecture packageSystem = checkPackageSystem(rowIndex);
			return packageSystem;
		case 3:
			return installerInfo.getName();
		case 4:
			return installerInfo.getVersion();
		case 5:
			return installerInfo.getType();
		default:
			return "";

		}
	}

	/**
	 * @param rowIndex
	 * @return
	 */
	private PackageOsAndArchitecture checkPackageSystem(int rowIndex) {
		PackageOsAndArchitecture oSArch = new PackageOsAndArchitecture();
		TablePackageInfo tableInfo = currentlyVisiblePackages[rowIndex];
		PackageInfo installerInfo = tableInfo.getPackageInfo();
		oSArch.osfamily = installerInfo.getOperatingSystemFamily();
		oSArch.osname = installerInfo.getOperatingSystemName();
		oSArch.osversion = installerInfo.getOperatingSystemVersion();
		oSArch.arch = installerInfo.getArchitecture();
		return oSArch;
	}

	/**
	 * @return
	 */
	private PackageStatus checkStatus(int rowIndex) {
		TablePackageInfo tableInfo = currentlyVisiblePackages[rowIndex];

		// TODO: checkboxes when a package is broken dont work correctly
		if (tableInfo.getPackageInfo().isBroken()) {
			return PackageStatus.BROKEN;
		}
		
		if( !installerManager.hasProviderToThisPackage( tableInfo.getPackageInfo()) ) {
			return PackageStatus.INSTALLATION_NOT_AVAILABLE;
		}
		
		if (tableInfo.isInstalled()) {
			if (tableInfo.isInstallable()) {
				if (tableInfo.isSelected()) {
					return PackageStatus.TO_REINSTALL;
				} else {
					return PackageStatus.INSTALLED;
				}
			} else {
				return PackageStatus.INSTALLED_NOT_INSTALLABLE;
			}

		} else {
			if (tableInfo.isSelected()) {
				return PackageStatus.TO_INSTALL;
			} else {
				return PackageStatus.NOT_INSTALLED;
			}
		}
	}

	private PackageOfficialRecommended checkOfficialRecommended(int rowIndex) {
		TablePackageInfo tableInfo = currentlyVisiblePackages[rowIndex];
		PackageInfo packageInfo = tableInfo.getPackageInfo();

		if (packageInfo.isOfficial()) {
			if (currentlyVisiblePackages[rowIndex].isDefault()) {
				return PackageOfficialRecommended.OFFICIAL_RECOMMENDED;
			} else {
				return PackageOfficialRecommended.OFFICIAL_NOT_RECOMMENDED;
			}
		} else {
			if (currentlyVisiblePackages[rowIndex].isDefault()) {
				return PackageOfficialRecommended.NOT_OFFICIAL_RECOMMENDED;
			} else {
				return PackageOfficialRecommended.NOT_OFFICIAL_NOT_RECOMMENDED;
			}
		}
	}

	public PackageInfo getPackageInfoAt(int rowIndex) {
		return currentlyVisiblePackages[rowIndex].getPackageInfo();
	}

	public String getDescriptionAt(int rowIndex) {
		return getPackageInfoAt(rowIndex).getDescription();
	}

	public URL getSourcesAt(int rowIndex) {
		return getPackageInfoAt(rowIndex).getSourcesURL();
	}

	public URL getOwnerUrlAt(int rowIndex) {
		return getPackageInfoAt(rowIndex).getOwnerURL();
	}

	public String getOwnerAt(int rowIndex) {
		return getPackageInfoAt(rowIndex).getOwner();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return (columnIndex == 0) && currentlyVisiblePackages[rowIndex].isInstallable();
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		if (columnIndex == 0) {
			PackageStatus status = (PackageStatus) aValue;
			TablePackageInfo tableInfo = currentlyVisiblePackages[rowIndex];
			switch (status) {
			case INSTALLED:
			case NOT_INSTALLED:
				tableInfo.setSelected(false);
				break;

			case TO_REINSTALL:
			case TO_INSTALL:
				tableInfo.setSelected(true);
				break;
			}
		}
	}

	public List<PackageInfo> getPackagesToInstall() {
	    
	    List<PackageInfo> packageInfosToInstall = new ArrayList<PackageInfo>();
	    packageInfosToInstall.addAll(selectedPackages);
	    return packageInfosToInstall;
	}

	public boolean hasAnyPackageSelected() {
	    return (selectedPackages.size() > 0);
	}

	public final class TablePackageInfo {

		private final PackageInfo packageInfo;
		private boolean isInstalled;
		private final boolean isInstallable;
		// private boolean isSelected;
		private boolean isDefault;

		public TablePackageInfo(PackageInfo packageInfo, boolean isInstalled,
				boolean isInstallable) {
			this(packageInfo, isInstalled, isInstallable, false);
		}

		public void setInstalled(boolean installed) {
			this.isInstalled = installed;
		}

		public TablePackageInfo(PackageInfo packageInfo, boolean isInstalled,
				boolean isInstallable, boolean isDefault) {
			this.packageInfo = packageInfo;
			this.isInstalled = isInstalled;
			this.isInstallable = isInstallable;
			this.isDefault = isDefault;
		}

		public String getID() {
			PackageInfo pkg = this.getPackageInfo();
			return pkg.getID();
		}

		public PackageInfo getPackageInfo() {
			return packageInfo;
		}

		public boolean isInstalled() {
			return isInstalled;
		}

		public boolean isInstallable() {
			return isInstallable;
		}

		public boolean isSelected() {
			return selectedPackages.contains(packageInfo);
		}

		public void setSelected(boolean isSelected) {
		    if (isSelected) {
		        selectedPackages.add(packageInfo);
		    } else {
		        selectedPackages.remove(packageInfo);
		    }
		}

		public void setDefault(boolean isDefault) {
			this.isDefault = isDefault;
		}

		public boolean isDefault() {
			return isDefault;
		}

	}

	/**
	 * Returns the TablePackageInfo located in the given row.
	 * 
	 * @param row
	 *            the TablePackageInfo located in the given row
	 */
	public TablePackageInfo getPackageInfo(int row) {
		return currentlyVisiblePackages[row];
	}

	public void setFilter(PackageFilter filter) {
		this.packageFilter = filter;
	}

	public void setFilter(NameDescriptionOrCodeFilter filter) {
		this.packageFastFilter = filter;
	}

	private Boolean isFastFilterSet() {
		return this.packageFastFilter != null;
	}

	private Boolean isPackageFilterSet() {
		return this.packageFilter != null;
	}

	public PackageFilter getFilter() {
		return this.packageFastFilter;
	}

	public InstallPackageService getInstallPackageService() {
		return this.installerExecutionService;
	}
}
