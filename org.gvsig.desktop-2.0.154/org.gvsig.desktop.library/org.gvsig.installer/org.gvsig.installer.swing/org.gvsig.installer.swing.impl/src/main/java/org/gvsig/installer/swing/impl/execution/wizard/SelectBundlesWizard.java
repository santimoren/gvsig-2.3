/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.execution.wizard;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.JPanel;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.swing.api.SwingInstallerManager.UrlAndLabel;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.execution.panel.SelectBundlesPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectBundlesWizard extends SelectBundlesPanel implements
		OptionPanel {

	private static final long serialVersionUID = -3694039665800882085L;
	private DefaultInstallPackageWizard installerExecutionWizard;
	private static final Logger LOG = LoggerFactory
			.getLogger(SelectBundlesWizard.class);

	public SelectBundlesWizard(
			DefaultInstallPackageWizard installerExecutionWizard,
			List<UrlAndLabel> defaultDownloadURL) {
		super(defaultDownloadURL, installerExecutionWizard.getInstallFolder());
		this.installerExecutionWizard = installerExecutionWizard;
		updatePanel();
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_select_installation_source");
	}

	public void lastPanel() {
	}

	public void nextPanel() throws NotContinueWizardException {
		InstallPackageService installerExecutionService = installerExecutionWizard
				.getInstallerExecutionService();
		installerExecutionService.reset();

		try {
			if (isStandardSelected()) {
				installerExecutionService
						.addBundlesFromDirectory(installerExecutionWizard
								.getInstallFolder());
			} else {
				if (isURLSelected()) {
					installerExecutionService.addBundle(getSelectedURL());
				} else {
					File file = getSelectedFile();
					if ((file != null) && (file.exists())) {
						installerExecutionService.addBundle(file);
					} else {
						LOG.error(swingInstallerManager.getText(""
								+ "_the_selected_file_does_not_exist"));
					}
				}
			}
		} catch (InstallPackageServiceException e) {
			throw new NotContinueWizardException(swingInstallerManager
					.getText("_execute_adding_error"), e, this);
		} catch (MalformedURLException e) {
			throw new NotContinueWizardException(swingInstallerManager
					.getText("_execute_adding_error"), e, this);
		}

	}

	public void updatePanel() {
                if(installerExecutionWizard.getSkipBundleSelection() ) {
                    installerExecutionWizard.doAction(WizardPanelWithLogo.ACTION_NEXT);
                }
		if (installerExecutionWizard != null) {
			installerExecutionWizard.setBackButtonEnabled(false);
		}
	}

	@Override
	protected void checkNextButtonEnabled() {
		if (installerExecutionWizard != null) {
			installerExecutionWizard
					.setNextButtonEnabled(isNextButtonEnabled());
		}
	}

}
