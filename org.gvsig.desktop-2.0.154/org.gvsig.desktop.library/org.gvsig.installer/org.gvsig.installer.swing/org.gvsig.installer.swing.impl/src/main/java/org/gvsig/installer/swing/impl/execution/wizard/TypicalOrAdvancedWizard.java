/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.wizard;

import java.util.List;

import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.execution.panel.TypicalOrAdvancedPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class TypicalOrAdvancedWizard extends TypicalOrAdvancedPanel implements
		OptionPanel {

	private DefaultInstallPackageWizard installerExecutionWizard;

	private int direccion = WizardPanelWithLogo.ACTION_NEXT;

	public TypicalOrAdvancedWizard(
			DefaultInstallPackageWizard installerExecutionWizard) {
		super();
		this.installerExecutionWizard = installerExecutionWizard;
	}

	private static final long serialVersionUID = -7151675399781722322L;

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_select_installation_mode");
	}

	public void lastPanel() {
		direccion = WizardPanelWithLogo.ACTION_NEXT;

	}

	public void nextPanel() throws NotContinueWizardException {
		if (installerExecutionWizard.getAskTypicalOrCustom()) {
			installerExecutionWizard
					.setShowSelectPackagesPanel(isAdvancedModeSelected());
		}
		direccion = WizardPanelWithLogo.ACTION_PREVIOUS;
	}

	public void updatePanel() {
		InstallPackageService service = installerExecutionWizard
				.getInstallerExecutionService();

		if (!installerExecutionWizard.getAskTypicalOrCustom()) {
			(installerExecutionWizard).doAction(direccion);
		}

		// if no packages will be selected on typical mode
		List<String> defaultdPackagesIDs = service
				.getDefaultSelectedPackagesIDs();

		// disable or enable the typical mode
		if ((defaultdPackagesIDs == null) || (defaultdPackagesIDs.size() < 1)) {
			setEnableTypicalMode(false);
		} else {
			// typical mode is enabled
			setEnableTypicalMode(true);
		}

	}
}
