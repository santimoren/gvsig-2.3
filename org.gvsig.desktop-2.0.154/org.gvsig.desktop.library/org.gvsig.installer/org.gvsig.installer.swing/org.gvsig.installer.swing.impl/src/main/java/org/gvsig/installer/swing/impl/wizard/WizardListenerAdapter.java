/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.wizard;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.swing.api.wizard.InstallerWizardPanel;
import org.gvsig.tools.task.CancellableTask;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class WizardListenerAdapter implements WizardPanelActionListener {

	private InstallerWizardPanel installerWizardPanel;

	public WizardListenerAdapter(InstallerWizardPanel installerWizardPanel) {
		super();
		this.installerWizardPanel = installerWizardPanel;
	}

	public void cancel(WizardPanel wizardPanel) {
		if (installerWizardPanel.getWizardActionListener() != null) {
			for (CancellableTask task : installerWizardPanel
					.getCancellableTasks()) {
				task.cancelRequest();
			}
			installerWizardPanel.getWizardActionListener().cancel(
					installerWizardPanel);
		}
	}

	public void finish(WizardPanel wizardPanel) {
		if (installerWizardPanel.getWizardActionListener() != null) {
			installerWizardPanel.getWizardActionListener().finish(
					installerWizardPanel);
		}
		
		if (installerWizardPanel.needsToRestartApplicationAfterFinish()) {

		    // --------------------------- Suggest restart
		    Component parent = null;
		    if (installerWizardPanel instanceof Component) {
		        parent = (Component) installerWizardPanel;
		    }
		    JOptionPane.showMessageDialog(
		        parent,
		        Messages.getText("_gvSIG_must_be_restarted_to_have_changes_applied"),
                Messages.getText("_finished"),
		        JOptionPane.WARNING_MESSAGE);
		}
	}
}
