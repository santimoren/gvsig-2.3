/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.filters.NameDescriptionOrCodeFilter;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class FastFilterButtonsPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 3767011079359743742L;

	private PackagesTablePanel panel;
	private SwingInstallerManager swingInstallerManager;

	private JButton searchButton;
	private JButton resetButton;
	private JTextField textField;
	private NameDescriptionOrCodeFilter filter = null;

	public FastFilterButtonsPanel(PackagesTablePanel panel) {
		this.panel = panel;
		swingInstallerManager = SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {

		filter = new NameDescriptionOrCodeFilter("");

		JLabel fastFilterLabel = new JLabel(swingInstallerManager
				.getText("_fast_filter"));

		textField = new JTextField(20);

		MyKeyListener l = new MyKeyListener();

		textField.addKeyListener(l);

		searchButton = new JButton(swingInstallerManager.getText("_search"));
		searchButton.setActionCommand("search");
		searchButton.addActionListener(this);

		resetButton = new JButton(swingInstallerManager
				.getText("_reset_filters"));
		resetButton.setActionCommand("reset");
		resetButton.addActionListener(this);

		setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));

		add(fastFilterLabel);
		add(textField);
		// search button not needed with key listener
		// add(searchButton);
		add(resetButton);

	}

	public void actionPerformed(ActionEvent e) {
		if ("search".equals(e.getActionCommand())) {
			searchAction();
		} else if ("reset".equals(e.getActionCommand())) {
			resetAction();
		}
	}

	private void searchAction() {
		filter.setFilter(textField.getText());
		panel.setFilter(filter);
	}

	private void resetAction() {
		this.resetPanel();
	}

	public void resetPanel() {
		textField.setText("");
		filter.setFilter("");
		panel.setFilter(filter);
		panel.resetPanel();
	}

	private class MyKeyListener implements KeyListener {

		public void keyTyped(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
			searchAction();
			textField.requestFocus();
			textField.setCaretPosition(textField.getDocument().getLength());
		}

		public void keyPressed(KeyEvent e) {
		}
	}

}
