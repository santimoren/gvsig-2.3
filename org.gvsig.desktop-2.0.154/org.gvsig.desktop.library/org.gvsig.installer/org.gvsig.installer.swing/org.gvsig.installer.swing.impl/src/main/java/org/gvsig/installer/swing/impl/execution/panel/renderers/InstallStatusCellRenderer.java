/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.renderers;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.panel.model.PackagesTableModel.PackageStatus;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class InstallStatusCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1195015856704835320L;

	private DefaultSwingInstallerManager swingInstallerManager;

	public InstallStatusCellRenderer() {
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		Icon icon;
		PackageStatus status = (PackageStatus) value;
		JLabel check = new JLabel();
		URL resource;

		switch (status) {

		case INSTALLED:
			resource = this.getClass().getResource("/images/installed.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_already_installed,_check_to_reinstall"));
			return check;

		case TO_REINSTALL:
			resource = this.getClass().getResource("/images/toReinstall.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_checked_to_reinstall"));
			return check;

		case NOT_INSTALLED:
			resource = this.getClass().getResource("/images/notInstalled.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_not_installed,_check_to_install"));
			return check;

		case TO_INSTALL:
			resource = this.getClass().getResource("/images/toInstall.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_checked_to_install"));
			return check;

		case INSTALLED_NOT_INSTALLABLE:
			resource = this.getClass().getResource(
					"/images/installedNotInstallable.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_already_installed_but_not_installable"));
			return check;
			
		case BROKEN:
			resource = this.getClass().getResource(
					"/images/broken.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_broken_package"));
			return check;

		case INSTALLATION_NOT_AVAILABLE:
			resource = this.getClass().getResource(
					"/images/broken.png");
			icon = new ImageIcon(resource);
			check.setIcon(icon);
			check.setToolTipText(swingInstallerManager
					.getText("_can_install_package_at_this_moment"));
			return check;

		default:
			return check;
		}

	}
}
