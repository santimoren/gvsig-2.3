/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.apache.batik.util.gui.xmleditor.XMLEditorKit;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class AntScriptPanel extends JPanel {

	private static final long serialVersionUID = 6279969036777585961L;
	protected DefaultSwingInstallerManager swingInstallerManager = null;
	private JEditorPane scriptEditorPane;
	private JScrollPane scriptScrollPane;

	public AntScriptPanel() {
		super();
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
	}

	private void initComponents() {

		scriptEditorPane = new javax.swing.JEditorPane();
		scriptEditorPane.setEditorKitForContentType("text/xml",
				new XMLEditorKit());
		scriptEditorPane.setContentType("text/xml");

		scriptScrollPane = new javax.swing.JScrollPane(scriptEditorPane);
		scriptScrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scriptScrollPane.setPreferredSize(new Dimension(250, 300));
		scriptScrollPane.setMinimumSize(new Dimension(10, 10));

		setLayout(new BorderLayout());

		add(scriptScrollPane, BorderLayout.CENTER);
	}

	public String getAntScript() {
		return scriptEditorPane.getText();
	}

	public void setAntScript(String antScript) {
		scriptEditorPane.setText(antScript);
	}
}
