/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.impl.creation;

import java.awt.BorderLayout;
import java.io.File;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.ImageIcon;

import jwizardcomponent.DefaultJWizardComponents;

import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.swing.api.creation.MakePluginPackageWizard;
import org.gvsig.installer.swing.impl.creation.wizard.AdvancedModeSelectionWizard;
import org.gvsig.installer.swing.impl.creation.wizard.AntScriptWizard;
import org.gvsig.installer.swing.impl.creation.wizard.PackageInfoWizard;
import org.gvsig.installer.swing.impl.creation.wizard.ProgressWizard;
import org.gvsig.installer.swing.impl.creation.wizard.SelectFilesWizard;
import org.gvsig.installer.swing.impl.creation.wizard.SelectOutputFileWizard;
import org.gvsig.installer.swing.impl.creation.wizard.SelectPlugintoInstallWizard;
import org.gvsig.installer.swing.impl.wizard.WizardListenerAdapter;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultMakePluginPackageWizard extends MakePluginPackageWizard
		implements WizardPanel {

	private static final long serialVersionUID = 9205891710214122265L;
	private WizardPanelWithLogo wizardPanelWithLogo = null;
	private MakePluginPackageService makePluginPackageService = null;
	private OutputStream outputStream = null;
	private OutputStream indexOutputStream;
	private PackageInfo selectedPackageInfo = null;
	private File originalPluginFolder = null;

	// Wizards
	private AdvancedModeSelectionWizard advancedModeSelectionWizard = null;
	private AntScriptWizard antScriptWizard = null;
	private PackageInfoWizard pluginDescriptionWizard = null;
	private ProgressWizard progressWizard = null;
	private SelectFilesWizard selectFilesWizard = null;
	private SelectOutputFileWizard selectOutputFileWizard = null;
	private SelectPlugintoInstallWizard selectPlugintoInstallWizard = null;

	private WizardListenerAdapter wizardListenerAdapter = null;
	private URL downloadURL;

	public DefaultMakePluginPackageWizard(File applicationFolder,
			File installFolder) throws LocatorException,
			MakePluginPackageServiceException {
		super(applicationFolder, installFolder);
		URL iconURL = getClass().getClassLoader().getResource(
				"images/makepluginpackageicon.png");
		ImageIcon icon = new ImageIcon(iconURL);
		wizardPanelWithLogo = new WizardPanelWithLogo();

		advancedModeSelectionWizard = new AdvancedModeSelectionWizard(this);
		antScriptWizard = new AntScriptWizard(this);
		pluginDescriptionWizard = new PackageInfoWizard(this);
		progressWizard = new ProgressWizard(this);
		selectFilesWizard = new SelectFilesWizard(this);
		selectOutputFileWizard = new SelectOutputFileWizard(this);
		selectPlugintoInstallWizard = new SelectPlugintoInstallWizard(this);

		addWizards();

		// Adding the listeners
		wizardPanelWithLogo.setWizardListener(this);

		setFinishButtonEnabled(false);

		this.setLayout(new BorderLayout());
		this.add(wizardPanelWithLogo, BorderLayout.CENTER);

		makePluginPackageService = InstallerLocator.getInstallerManager()
				.getMakePluginPackageService();
		selectPlugintoInstallWizard
				.setPluginsDirectory(makePluginPackageService);
		selectFilesWizard.setApplicationDirectory(applicationFolder);
	}

	public MakePluginPackageService getMakePluginPackageService() {
		return makePluginPackageService;
	}

	private void addWizards() {
		wizardPanelWithLogo.addOptionPanel(selectPlugintoInstallWizard);
		wizardPanelWithLogo.addOptionPanel(pluginDescriptionWizard);
		wizardPanelWithLogo.addOptionPanel(advancedModeSelectionWizard);
		addLastWizards();
	}

	private void addLastWizards() {
		wizardPanelWithLogo.addOptionPanel(selectOutputFileWizard);
		wizardPanelWithLogo.addOptionPanel(progressWizard);
	}

	private void addAdvancedWizards() {
		wizardPanelWithLogo.addOptionPanel(selectFilesWizard);
		wizardPanelWithLogo.addOptionPanel(antScriptWizard);
	}

	private DefaultJWizardComponents getWizardComponents() {
		return wizardPanelWithLogo.getWizardComponents();
	}

	public void setNextButtonEnabled(boolean isEnabled) {
		wizardPanelWithLogo.setNextButtonEnabled(isEnabled);
	}

	public void setCancelButtonEnabled(boolean isEnabled) {
		wizardPanelWithLogo.setCancelButtonEnabled(isEnabled);
	}

	public void setFinishButtonEnabled(boolean isEnabled) {
		wizardPanelWithLogo.setFinishButtonEnabled(isEnabled);
	}

	public void setBackButtonEnabled(boolean isEnabled) {
		wizardPanelWithLogo.setBackButtonEnabled(isEnabled);
	}

	public void setAdvancedModeSelected(boolean advancedModeSelected) {
		for (int i = getWizardComponents().getWizardPanelList().size() - 1; i >= 3; i--) {
			getWizardComponents().removeWizardPanel(i);
		}
		if (advancedModeSelected) {
			addAdvancedWizards();
		}
		addLastWizards();
	}

	/**
	 * @return the installercreationService
	 */
	public MakePluginPackageService getInstallerCreationService() {
		return makePluginPackageService;
	}

	/**
	 * @return the outputStream
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * @param outputStream
	 *            the outputStream to set
	 */
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public OutputStream getIndexOutputStream() {
		return indexOutputStream;
	}

	public void setIndexOutputStream(OutputStream outputStream) {
		this.indexOutputStream = outputStream;
	}

	public PackageInfo getSelectedPackageInfo() {
		return selectedPackageInfo;
	}

	public void setSelectedPackageInfo(PackageInfo selectedPackageInfo) {
		this.selectedPackageInfo = selectedPackageInfo;
	}

	public void setOriginalPluginFolder(File originalPluginFolder) {
		this.originalPluginFolder = originalPluginFolder;
	}

	public File getOriginalPluginFolder() {
		return this.originalPluginFolder;
	}

	public WizardPanelActionListener getWizardPanelActionListener() {
		if (((wizardListenerAdapter == null) && (getWizardActionListener() != null))) {
			return new WizardListenerAdapter(this);
		}
		return wizardListenerAdapter;
	}

	public void setWizardPanelActionListener(
			WizardPanelActionListener wizardActionListener) {
		// TODO Auto-generated method stub

	}

	public void setDownloadURL(URL downloadURL) {
		this.downloadURL = downloadURL;
	}

	public URL getDownloadURL() {
		return downloadURL;
	}

}
