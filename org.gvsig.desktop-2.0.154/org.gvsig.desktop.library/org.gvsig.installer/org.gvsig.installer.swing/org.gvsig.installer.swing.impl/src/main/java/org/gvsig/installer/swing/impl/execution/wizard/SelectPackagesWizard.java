/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.wizard;

import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.DependenciesCalculator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.execution.JShowPackageStatusAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowPackagesAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowRequiredPackagesAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowUnresolvedDependenciesAndAskContinuePanel;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;
import org.gvsig.installer.swing.impl.execution.DefaultInstallPackageWizard;
import org.gvsig.installer.swing.impl.execution.panel.SelectPackagesPanel;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelectPackagesWizard implements OptionPanel {

	private static final long serialVersionUID = -4678172923039393643L;

	private static Logger LOG = LoggerFactory
			.getLogger(SelectPackagesWizard.class);

	protected DefaultInstallPackageWizard installerExecutionWizard;

	private SelectPackagesPanel selectPackagesPanel = null;
	private DefaultSwingInstallerManager swingInstallerManager = null;

	private int direccion = WizardPanelWithLogo.ACTION_NEXT;

	public SelectPackagesWizard(
			DefaultInstallPackageWizard installerExecutionWizard) {
		super();
		this.installerExecutionWizard = installerExecutionWizard;
		selectPackagesPanel = new SelectPackagesPanel(this);

		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
	}

	public JPanel getJPanel() {
		return selectPackagesPanel;
	}

	public String getPanelTitle() {
		return swingInstallerManager.getText("_select_plugins");
	}

	public void lastPanel() {
		direccion = WizardPanelWithLogo.ACTION_NEXT;
		selectPackagesPanel.clearPanel();
	}

	public void nextPanel() throws NotContinueWizardException {
	    /*
	     * First warn about packages in development or unofficial
	     */
        checkDevelAndUnoficialPackages();
        /*
         * Then check dependencies
         */
		checkDependencies();
		direccion = WizardPanelWithLogo.ACTION_PREVIOUS;
		installerExecutionWizard.setNextButtonEnabled(false);
	}

	private void checkDependencies() throws NotContinueWizardException {
		List<PackageInfo> requiredPackages = null;
		List<PackageInfo> troubledPackages = null;
		Dependencies unresolvedDependencies = null;
		try {
			// Creamos el calculador de dependencias
			DependenciesCalculator calculator = this.swingInstallerManager
					.getInstallerManager().createDependenciesCalculator(
							installerExecutionWizard
									.getInstallerExecutionService());

			// Le indicamos los paquetes que queremos instalar
			calculator.addPackageToInstall(this.installerExecutionWizard
					.getInstallersToInstall());

			// Le a�adimos los paquetes que ya hay instalados.

			PackageInfo[] pkgs = swingInstallerManager.getInstallerManager()
					.getInstalledPackages();
			calculator.addInstalledPackage(pkgs);

			// Calculamos las dependencias
			calculator.calculate();

			requiredPackages = calculator.getRequiredPackages();
			unresolvedDependencies = calculator.getUnresolvedDependencies();
			troubledPackages = calculator.getConflictPackages();
		} catch (Throwable e) {
			LOG
					.error(
							swingInstallerManager
									.getText("_an_error_has_occurred_when_calculating_the_dependecies_of_the_packages_to_install"),
							e);
			int resp = JOptionPane
					.showConfirmDialog(
							null,
							swingInstallerManager
									.getText("_an_error_has_occurred_when_calculating_the_dependecies_of_the_selected_packages")
									+ "\n"
									+ swingInstallerManager
											.getText("_it_is_not_possible_to_verify_if_any_additional_package_is_needed_to_install_the_selected_packages")
									+ "\n"
									+ swingInstallerManager
											.getText("_if_you_want_you_can_select_again_manually_the_packets_that_might_be_necessary")
									+ swingInstallerManager
											.getText("_do_you_want_to_continue_and_install_the_packages_you_already_have_selected"),
							swingInstallerManager
									.getText("_a_problem_has_occurred_when_calculating_the_dependencies"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
			if (resp == JOptionPane.NO_OPTION) {
				throw new NotContinueWizardException("", null, false);
			}
		}

		if (troubledPackages != null && troubledPackages.size() > 0) {
			JShowPackagesAndAskContinuePanel dlg = this.swingInstallerManager
					.createJShowTroubledPackagesAndAskContinuePanel(
							troubledPackages,
							swingInstallerManager
									.getText("_Some_of_the_selected_addons_should_not_be_installed_simultaneously")
									+ ". "
		                            + swingInstallerManager.getText("_Installing_these_addons_may_cause_errors")
									+ ".\n"
									+ swingInstallerManager.getText("_do_you_want_to_continue")
					);
			if (dlg.needShow()) {
				WindowManager wm = ToolsSwingLocator.getWindowManager();
				wm.showWindow(dlg, swingInstallerManager
						.getText("_do_you_want_to_continue"),
						WindowManager.MODE.DIALOG);
				if (dlg.cancelled()) {
					throw new NotContinueWizardException("", null, false);
				}
			}
		}

		if (requiredPackages != null && requiredPackages.size() > 0) {
			JShowRequiredPackagesAndAskContinuePanel dlg = this.swingInstallerManager
					.createJShowRequiredPackagesAndAskContinuePanel(
							requiredPackages,
							swingInstallerManager
									.getText("_the_selected_packages_require_to_install_or_update_the_following_packages")
									+ ":\n"
									+ swingInstallerManager
											.getText("_do_you_want_to_continue"));
			if (dlg.needShow()) {
				WindowManager wm = ToolsSwingLocator.getWindowManager();
				wm.showWindow(dlg, swingInstallerManager
						.getText("_do_you_want_to_continue"),
						WindowManager.MODE.DIALOG);
				if (dlg.cancelled()) {
					throw new NotContinueWizardException("", null, false);
				}
			}
		}

		if (unresolvedDependencies != null && unresolvedDependencies.size() > 0) {
			JShowUnresolvedDependenciesAndAskContinuePanel dlg = this.swingInstallerManager
					.createJShowUnresolvedDependenciesAndAskContinuePanel(
							unresolvedDependencies,
							swingInstallerManager
									.getText("_the_following_dependencies_have_not_been_possible_to_resolve")
                                    + ". " +
                                    swingInstallerManager.getText("_This_may_cause_issues_in_gvSIG")
									+ "\n" +
									swingInstallerManager.getText("_do_you_want_to_continue_anyway")
									);
			if (dlg.needShow()) {
				WindowManager wm = ToolsSwingLocator.getWindowManager();
				wm.showWindow(dlg, swingInstallerManager
						.getText("_do_you_want_to_continue"),
						WindowManager.MODE.DIALOG);
				if (dlg.cancelled()) {
					throw new NotContinueWizardException("", null, false);
				}
			}
		}

		Iterator<PackageInfo> it = requiredPackages.iterator();
		while (it.hasNext()) {
			PackageInfo pkg = it.next();
			selectPackagesPanel.getModel().selectPackage(pkg);
			LOG.info("Automatic selection of required package "+pkg.getCode());
		}
	}

	private void checkDevelAndUnoficialPackages()
			throws NotContinueWizardException {
		List<PackageInfo> packagesToInstall = this.installerExecutionWizard
				.getInstallersToInstall();

		JShowPackageStatusAndAskContinuePanel dlg = this.swingInstallerManager
				.createJShowPackageStatusAndAskContinuePanel(
						packagesToInstall,
						swingInstallerManager.getText(
						    "_you_have_selected_in_development_or_not_official_versions")
                            + ". "
                            + swingInstallerManager.getText(
                                "_Installing_devel_addons_may_cause_errors")
						    + "\n"
						    + swingInstallerManager.getText(
						        "_do_you_want_to_continue_anyway"));
		if (dlg.needShow()) {
			WindowManager wm = ToolsSwingLocator.getWindowManager();
			wm.showWindow(dlg, swingInstallerManager
					.getText("_do_you_want_to_continue"),
					WindowManager.MODE.DIALOG);
			if (dlg.cancelled()) {
				throw new NotContinueWizardException("", null, false);
			}
		}
	}

	public void updatePanel() {
                if(installerExecutionWizard.getSkipBundleSelection() ) {
			installerExecutionWizard.setBackButtonEnabled(false);
		}
            
		InstallPackageService installerExecutionService = installerExecutionWizard
				.getInstallerExecutionService();

		selectPackagesPanel.updatePanel();
		selectPackagesPanel.setInitialFilter();

		// if default packages must be selected or not
		if (installerExecutionWizard.getSelectDefaultPackages()) {
			selectPackagesPanel.selectPackages();
		}

		// if this panel has not to be shown
		if (!installerExecutionWizard.showSelectPackagesPanel()) {
			saltaOno(installerExecutionService);
		}

		checkIfPluginSelected();
	}

	public void checkIfPluginSelected() {
		if (installerExecutionWizard.showSelectPackagesPanel()) {
			installerExecutionWizard.setNextButtonEnabled(selectPackagesPanel
					.isPackageSelected());
		}
	}

	public void saltaOno(InstallPackageService installerExecutionService) {

		List<String> defaultPackageIDs = installerExecutionService
				.getDefaultSelectedPackagesIDs();

		boolean defaultPacketsExist = false;

		// check if there is any default package
		if (defaultPackageIDs != null) {
			for (int i = 0; i < installerExecutionService.getPackageCount(); i++) {
				for (int j = 0; j < defaultPackageIDs.size(); j++) {
					// if the package is in the default packages list
					if (installerExecutionService.getPackageInfo(i).matchID(
							defaultPackageIDs.get(j))) {
						// if package is for all operating systems or the system
						// operating system equals the project's one
						if (installerExecutionService.getPackageInfo(i)
								.getOperatingSystem().equals("all")
								|| installerExecutionService.getPackageInfo(i)
										.getOperatingSystem().equals(
												this.swingInstallerManager
														.getInstallerManager()
														.getOperatingSystem())) {
							defaultPacketsExist = true;
							break;
						}
					}
				}
			}
		}

		if (defaultPacketsExist) {
			(installerExecutionWizard).doAction(direccion);
			// if there is not any package, show error and jump back.
		} else {
			(installerExecutionWizard)
					.doAction(WizardPanelWithLogo.ACTION_PREVIOUS);
			try {
				throw new Exception(
						swingInstallerManager
								.getText("_There_are_no_packages_in_typical_installation_to_select"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public List<PackageInfo> getPackagesToInstall() {
		return selectPackagesPanel.getPackagesToInstall();
	}

	public DefaultInstallPackageWizard getDefaultInstallPackageWizard() {
		return this.installerExecutionWizard;
	}

	public Boolean isDefaultPackagesSelectionSet() {
		return installerExecutionWizard.getSelectDefaultPackages();
	}

}
