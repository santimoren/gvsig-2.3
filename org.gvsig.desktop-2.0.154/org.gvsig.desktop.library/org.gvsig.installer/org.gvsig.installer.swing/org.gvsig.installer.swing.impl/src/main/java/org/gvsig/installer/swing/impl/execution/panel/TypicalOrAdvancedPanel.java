/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class TypicalOrAdvancedPanel extends JPanel implements ItemListener,
		DocumentListener {

	private static final long serialVersionUID = 8488517767926838568L;

	protected DefaultSwingInstallerManager swingInstallerManager = null;

	private JPanel northPanel;
	private JRadioButton typicalModeRadioButton;
	private JRadioButton advancedModeRadioButton;
	private ButtonGroup buttonGroup;

	public TypicalOrAdvancedPanel() {
		swingInstallerManager = (DefaultSwingInstallerManager) SwingInstallerLocator
				.getSwingInstallerManager();
		initComponents();
		initListeners();
		typicalModeRadioButton.setSelected(true);

	}

	private void initListeners() {
		typicalModeRadioButton.addItemListener(this);
		advancedModeRadioButton.addItemListener(this);
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		northPanel = new JPanel();
		typicalModeRadioButton = new JRadioButton();
		advancedModeRadioButton = new JRadioButton();

		buttonGroup = new ButtonGroup();

		buttonGroup.add(typicalModeRadioButton);
		buttonGroup.add(advancedModeRadioButton);

		setLayout(new BorderLayout());

		northPanel.setLayout(new GridBagLayout());

		typicalModeRadioButton.setText(swingInstallerManager
				.getText("_typical_installation"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		northPanel.add(typicalModeRadioButton, gridBagConstraints);

		advancedModeRadioButton.setText(swingInstallerManager
				.getText("_advanced_installation"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		northPanel.add(advancedModeRadioButton, gridBagConstraints);

		add(northPanel, java.awt.BorderLayout.WEST);
	}

	protected boolean isTypicalModeSelected() {
		return typicalModeRadioButton.isSelected();
	}

	protected boolean isAdvancedModeSelected() {
		return advancedModeRadioButton.isSelected();
	}

	protected void setEnableTypicalMode(boolean active) {
		if (active) {
			typicalModeRadioButton.setSelected(true);
		} else {
			advancedModeRadioButton.setSelected(true);
		}
		typicalModeRadioButton.setEnabled(active);
	}

	public void itemStateChanged(ItemEvent arg0) {
	}

	public void changedUpdate(DocumentEvent arg0) {
	}

	public void insertUpdate(DocumentEvent arg0) {
	}

	public void removeUpdate(DocumentEvent arg0) {
	}

}
