/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.swing.impl.execution.panel.filters;

import org.gvsig.installer.lib.api.PackageInfo;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class NameDescriptionOrCodeFilter implements PackageFilter {

	private String filterString;

	public NameDescriptionOrCodeFilter(String filterString) {
		this.filterString = filterString;
	}

	public void setFilter(String filter) {
		this.filterString = filter;
	}

	public String getFilter() {
		return this.filterString;
	}

	public boolean match(PackageInfo pkg) {
		Boolean matchesName;
		Boolean matchesDescription;
		Boolean matchesCode;
		String regularExpressionString = "(?i).*" + filterString + ".*";
		String packageString;

		packageString = pkg.getName();
		matchesName = packageString.matches(regularExpressionString);
		packageString = pkg.getDescription();
		matchesDescription = packageString.matches(regularExpressionString);
		packageString = pkg.getCode();
		matchesCode = packageString.matches(regularExpressionString);

		return matchesName || matchesDescription || matchesCode;
	}

}
