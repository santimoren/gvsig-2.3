/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.api.creation;

import java.io.File;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.wizard.AbstractInstallerWizard;

/**
 * <p>
 * Wizard that is used to create a bundle with a package of type plugin from a
 * gvSIG directory. This class receive a gvSIG root directory and allows to the
 * user to select the plugin that is used to create the bundle.
 * </p>
 * <p>
 * The user can also set some plugin properties, like the version of the
 * installer, the build number... The definition of all these properties can be
 * found in {@link PackageInfo}.
 * </p>
 * <p>
 * All the classes that inherit if this abstract class have to have a
 * constructor with an argument of type {@link File}, that is the application
 * directory.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public abstract class MakePluginPackageWizard extends AbstractInstallerWizard {

	private static final long serialVersionUID = 6387360455696226183L;

	/**
	 * Constructor
	 * 
	 * @see AbstractInstallerWizard#AbstractInstallerWizard(File, File)
	 */
	public MakePluginPackageWizard(File applicationFolder, File installFolder) {
		super(applicationFolder, installFolder);
	}
}
