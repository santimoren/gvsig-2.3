/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.api;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.JPanel;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.installer.swing.api.creation.JPackageInfoPanel;
import org.gvsig.installer.swing.api.creation.MakePluginPackageWizard;
import org.gvsig.installer.swing.api.creation.MakePluginPackageWizardException;
import org.gvsig.installer.swing.api.execution.AbstractInstallPackageWizard;
import org.gvsig.installer.swing.api.execution.InstallPackageWizardException;
import org.gvsig.installer.swing.api.execution.JShowPackageStatusAndAskContinuePanel;
import org.gvsig.installer.swing.api.execution.JShowPackagesAndAskContinuePanel;
import org.gvsig.installer.swing.api.packagebuilder.PackageBuildder;
/**
 * 
 * <p>
 * This manager is used to register and create the wizards that are used to
 * create and execute an installer. These wizards are classes that inherit of
 * {@link JPanel}.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface SwingInstallerManager {


	public interface UrlAndLabel {
		/**
		 * Return the url.
		 * 
		 * @return the url
		 */
		public URL getURL();
		
		/**
		 * Return the url associated to the url or null
		 * if the url don't have a label.
		 *  
		 * @return the label or null
		 */
		public String getLabel();
		
		/**
		 * Return a string with the label and the url like:
		 *   "label - http://..."
		 *   
		 * @return the label and url
		 */
		public String toString(); // Return "label - url"
	}
	
	/**
	 * Registers a class that implements a wizard to create an installer of a
	 * plugin from a gvSIG installation directory.
	 * 
	 * @param clazz
	 *            Class that inherits of the {@link MakePluginPackageWizard}
	 *            abstract class.
	 */
	public void registerMakePluginPackageWizardInstallerCreationWizard(
			Class<? extends MakePluginPackageWizard> clazz);

	/**
	 * This method returns a class that is used to create an installer from a
	 * gvSIG installation directory.
	 * 
	 * @return The wizard to create an installer.
	 * @throws MakePluginPackageWizardException
	 *             If there is a problem creating the wizard.
	 */
	public MakePluginPackageWizard createMakePluginPackageWizard(
			File applicationDirectory, File installFolder)
			throws MakePluginPackageWizardException;

	/**
	 * Registers a class that implements a wizard to execte an installer to
	 * install a set of plugins in a gvSIG installation directory.
	 * 
	 * @param clazz
	 *            Class that inherits of the
	 *            {@link AbstractInstallPackageWizard} abstract class.
	 */
	public void registerInstallPackageWizard(
			Class<? extends AbstractInstallPackageWizard> clazz);

	/**
	 * This method returns a class that is used to execute an installer to
	 * install a set of plugins in a gvSIG installation directory.
	 * 
	 * @return The wizard to execute an installer.
	 * @throws InstallPackageWizardException
	 *             If there is a problem creating the wizard.
	 */
	public AbstractInstallPackageWizard createInstallPackageWizard(
			File applicationDirectory, File installFolder)
			throws InstallPackageWizardException;

	/**
	 * Returns the current application version.
	 * 
	 * @return the current application version
	 */
	public String getApplicationVersion();

	/**
	 * Sets the current application version.
	 * 
	 * @param gvSIGVersion
	 *            the current application version
	 */
	public void setApplicationVersion(String gvSIGVersion);

	/**
	 * Returns the default URL to download packages from.
	 * 
	 * @return the default URL to download packages from
	 */
	public URL getDefaultDownloadURL();

	/**
	 * Sets the default URL to download packages from
	 * 
	 * @param defaultDownloadURL
	 *            the default URL to download packages from
	 */
	public void setDefaultDownloadURL(URL defaultDownloadURL);

	/**
	 * Translate a key in a text using the current application language
	 * 
	 * @param key
	 *            The key to translate
	 * @return The translated key
	 */
	public String getText(String key);

	/**
	 * Returns a reference to the {@link InstallerManager}.
	 * 
	 * @return a reference to the {@link InstallerManager}
	 */
	public InstallerManager getInstallerManager();

	public JShowPackageStatusAndAskContinuePanel createJShowPackageStatusAndAskContinuePanel(
			List<PackageInfo> packages, String message);

	public JShowPackagesAndAskContinuePanel createJShowTroubledPackagesAndAskContinuePanel(
			List<PackageInfo> packages, String message);
	
	public JPackageInfoPanel createPackageInfoPanel();

	public JPackageInfoPanel createPackageInfoPanel(String packageType);

	public JOutputPanel createOutputPanel();

	public JProgressPanel createProgressPanel();

	public void setDefaultDownloadURL(String defaultDownloadURLs);

	public void setDefaultDownloadURL(File defaultDownloadURLs);

	public void addDefaultDownloadURL(URL url);

	public void addDefaultDownloadURL(URL url, String label);
	
	public void addDefaultDownloadURL(String url) throws MalformedURLException;

	/**
	 * Return the list of default URL used to retrieve the package.gvspki
	 * 
	 * @return list of defaults urls
	 * @deprecated use getDefaultDownloadUrlAndLabels
	 */
	public List<URL> getDefaultDownloadURLs();
	
	/**
	 * Return the list of default URL used to retrieve the package.gvspki
	 * the list contains the URLs and their labels.
	 *  
	 * @return list of default urls and their labels
	 */
	public List<UrlAndLabel> getDefaultDownloadUrlAndLabels();
        
        public PackageBuildder createPackagerPanel(String packageType, File selectionFolder, File outputFolder);

}
