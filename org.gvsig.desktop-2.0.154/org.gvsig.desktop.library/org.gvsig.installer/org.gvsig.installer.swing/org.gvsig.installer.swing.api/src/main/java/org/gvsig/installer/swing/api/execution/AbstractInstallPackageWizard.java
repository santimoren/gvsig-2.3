/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.api.execution;

import java.io.File;
import java.util.List;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.swing.api.wizard.AbstractInstallerWizard;

/**
 * <p>
 * Wizard that is used to open a bundle file to install one or more packages in
 * a valid gvSIG directory. This class receive a directory where gvSIG is
 * installed and it allows to select from the user interface the packages to
 * install.
 * </p>
 * <p>
 * All the classes that inherit if this abstract class have to have a
 * constructor with an argument of type {@link File}, that is the application
 * directory.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public abstract class AbstractInstallPackageWizard extends
		AbstractInstallerWizard {

	private static final long serialVersionUID = -758246118332926598L;

	/**
	 * Constructor
	 * 
	 * @see AbstractInstallerWizard#AbstractInstallerWizard(File, File)
	 */
	public AbstractInstallPackageWizard(File applicationFolder,
			File installFolder) {
		super(applicationFolder, installFolder);
	}

	/**
	 * If this method is selected the first window of the wizard is not visible
	 * and the installation process takes the installers form the default
	 * directory.
	 * 
	 * @throws InstallPackageServiceException
	 *             if there an error reading the default directory.
	 */
	public abstract void installFromDefaultDirectory()
			throws InstallPackageServiceException;

	/**
	 * @return the installerExecutionService
	 */
	public abstract InstallPackageService getInstallerExecutionService();

	/**
	 * @return the installersToInstall
	 */
	public abstract List<PackageInfo> getInstallersToInstall();

	public abstract void setNextButtonEnabled(boolean isEnabled);

	public abstract void setFinishButtonVisible(boolean isVisible);

	public abstract void setCancelButtonEnabled(boolean isEnabled);

	public abstract void setBackButtonEnabled(boolean isEnabled);

	/**
	 * It will ask the user to select the installation mode to Typical or
	 * Advanced.
	 * 
	 * @param askIt
	 *            True or False if the option will be shown to the user or not.
	 */
	public abstract void setAskTypicalOrCustom(boolean askIt);

	/**
	 * To know if the question is set to be shown or not.
	 * 
	 * @return True if the question is set to be shown or false if not.
	 */
	public abstract boolean getAskTypicalOrCustom();

	public abstract void setSelectDefaultPackages(boolean isActivated);

	public abstract boolean getSelectDefaultPackages();
	
	   
	public boolean needsToRestartApplicationAfterFinish() {
	    // installers need to restart
	    return true;
	}

        public abstract void setSkipBundleSelection(boolean skipBundleSelection);
}
