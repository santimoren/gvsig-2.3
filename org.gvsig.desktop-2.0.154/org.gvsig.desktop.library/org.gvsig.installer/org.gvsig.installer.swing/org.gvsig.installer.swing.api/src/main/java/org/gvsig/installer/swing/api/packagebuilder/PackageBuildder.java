package org.gvsig.installer.swing.api.packagebuilder;

import java.io.File;
import org.gvsig.tools.packageutils.PackageInfo;
import org.gvsig.tools.swing.api.Component;


public interface PackageBuildder extends Component {

    PackageInfo getPackageInfo();

    String getPackageType();

    void setFolderToPackaging(File file);
    
}
