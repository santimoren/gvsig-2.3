/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.swing.api.wizard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.gvsig.tools.task.CancellableTask;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public abstract class AbstractInstallerWizard extends JPanel implements
		InstallerWizardPanel {

	private static final long serialVersionUID = 5609799260650093733L;
	protected InstallerWizardActionListener wizardActionListener = null;
	private final File applicationFolder;
	private final File installFolder;
	private List<CancellableTask> cancelableTasks;

	/**
	 * Constructor.
	 * 
	 * @param applicationFolder
	 *            the main root application folder
	 * @param installFolder
	 *            the default bundle files location folder
	 */
	public AbstractInstallerWizard(File applicationFolder, File installFolder) {
		super();
		this.applicationFolder = applicationFolder;
		this.installFolder = installFolder;
		this.cancelableTasks = new ArrayList<CancellableTask>();
	}

	public void addCancellableTask(CancellableTask task) {
		this.cancelableTasks.add(task);
	}

	public List<CancellableTask> getCancellableTasks() {
		return this.cancelableTasks;
	}

	public void setWizardActionListener(
			InstallerWizardActionListener wizardActionListener) {
		this.wizardActionListener = wizardActionListener;
	}

	public InstallerWizardActionListener getWizardActionListener() {
		return wizardActionListener;
	}

	public File getApplicationFolder() {
		return applicationFolder;
	}

	public File getInstallFolder() {
		return installFolder;
	}
	
	public boolean needsToRestartApplicationAfterFinish() {
	    // false by default
	    // to be overriden by subclasses
	    return false;
	}
}
