/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.spi;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.spi.execution.InstallPackageProvider;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ProviderManager;
import org.gvsig.tools.service.spi.ProviderManager_WithGetFactories;

/**
 * <p>
 * The installation process install packages in gvSIG. These packages has a
 * type, that can be a plugin, theme, translation, etc. For every type of
 * package the installation process needs a provider for installing the
 * packages.
 * </p>
 * <p>
 * All the packages to install have to have some install properties defined by
 * the {@link PackageInfo} class. This class is just a set of properties and
 * there is a property named <b>type</b> that can be retrieved by the
 * {@link PackageInfo#getType()} method that defines the package type. This
 * property is used to create a {@link InstallPackageProvider} that is used to
 * install the selected package.
 * </p>
 * <p>
 * This manager provides the functionality to register and to create a providers
 * for every package.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface InstallerProviderManager extends ProviderManager, ProviderManager_WithGetFactories {

	/**
	 * Creates a new provider to execute an installer to add a new package in
	 * gvSIG.
	 * 
	 * @param providerName
	 *            the provider name used on the registration of the provider.
	 *            This name is the type attribute defined by {@link PackageInfo}
	 *            .
	 * @return a provider that can be used to install a package.
	 * @throws ServiceException
	 *             if the provider doesn't exist or if there is a problem
	 *             creating the provider.
	 */
	public InstallPackageProvider createExecutionProvider(String providerName)
			throws ServiceException;

	/**
	 * Creates the services that be used for the providers to execute or create
	 * a new bundle.
	 * 
	 * @return the services used to create or execute an bundle.
	 */
	public InstallPackageProviderServices createInstallerProviderServices();

	/**
	 * Returns the name to use for the package info file.
	 * 
	 * @return the name of the package info file
	 */
	public String getPackageInfoFileName();
}
