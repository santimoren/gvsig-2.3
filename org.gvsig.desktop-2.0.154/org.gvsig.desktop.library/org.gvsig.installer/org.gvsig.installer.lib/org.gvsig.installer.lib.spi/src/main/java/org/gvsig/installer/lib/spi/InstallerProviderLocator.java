/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.spi;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * This Locator provides the entry point for the gvSIG
 * {@link InstallerProviderManager}
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerProviderLocator extends BaseLocator {

	private static final String LOCATOR_NAME = "Installer.provider.locator";
	public static final String PROVIDER_MANAGER_NAME = "Installer.provider.manager";
	public static final String PROVIDER_MANAGER_DESCRIPTION = "Installer Manager";

	/**
	 * Unique instance.
	 */
	private static final InstallerProviderLocator instance = new InstallerProviderLocator();

	/**
	 * Return the singleton instance.
	 * 
	 * @return the singleton instance
	 */
	public static InstallerProviderLocator getInstance() {
		return instance;
	}

	/**
	 * Return the Locator's name
	 * 
	 * @return a String with the Locator's name
	 */
	@Override
	public String getLocatorName() {
		return LOCATOR_NAME;
	}

	/**
	 * Return a reference to InstallerProviderManager.
	 * 
	 * @return a reference to InstallerProviderManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static InstallerProviderManager getProviderManager()
			throws LocatorException {
		return (InstallerProviderManager) getInstance().get(
				PROVIDER_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the InstallerProviderManager interface.
	 * 
	 * @param clazz
	 *            implementing the InstallerProviderManager interface
	 */
	public static void registerInstallerProviderManager(
			Class<? extends InstallerProviderManager> clazz) {
		getInstance().register(PROVIDER_MANAGER_NAME,
				PROVIDER_MANAGER_DESCRIPTION, clazz);
	}

}
