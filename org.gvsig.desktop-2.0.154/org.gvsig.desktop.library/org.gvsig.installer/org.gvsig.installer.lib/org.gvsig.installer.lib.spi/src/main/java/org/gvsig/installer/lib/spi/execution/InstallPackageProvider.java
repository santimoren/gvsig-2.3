/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.spi.execution;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;

/**
 * <p>
 * Provider that manage the installation process of a concrete package. There is
 * a different provider for each package type that is supported. The provider
 * just have a method to install the package in gvSIG
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface InstallPackageProvider {

	/**
	 * This method install a package in a valid gvSIG directory.
	 * 
	 * @param applicationDirectory
	 *            the directory where gvSIG is located.
	 * @param inputStream
	 *            the stream that contains the package information.
	 * @param taskStatus
	 * @throws InstallPackageServiceException
	 *             if there is a problem reading the stream or installing the
	 *             package.
	 */
	public void install(File applicationDirectory, InputStream inputStream,
			PackageInfo packageInfo) throws InstallPackageServiceException;

	public void installLater(File applicationDirectory,
			InputStream inputStream, PackageInfo packageInfo)
			throws InstallPackageServiceException, IOException;

}
