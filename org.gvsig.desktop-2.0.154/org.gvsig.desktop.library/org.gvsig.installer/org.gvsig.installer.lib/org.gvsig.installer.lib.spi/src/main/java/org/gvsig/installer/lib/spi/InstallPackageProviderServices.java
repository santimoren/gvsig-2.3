/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.spi;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * Services that can be used by the providers to create or exceute the bundle.
 * It contains methods to compress and to decompress files and methods to read
 * an install a package file from a directory.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface InstallPackageProviderServices extends ProviderServices {

	/**
	 * It decompress an input stream and write it on a directory.
	 * 
	 * @param is
	 *            The input stream to decompress
	 * @param outputDirectory
	 *            . The output directory.
	 * @throws InstallPackageServiceException
	 *             If there is a problem decompressing the stream.
	 */
	public void decompress(InputStream is, File outputDirectory)
			throws InstallPackageServiceException;

	/**
	 * Compress a folder as a zipped package set in the given outputstream with
	 * a concrete name.
	 * 
	 * @param folder
	 *            the folder to compress.
	 * @param fileName
	 *            name of the zip entry that has the output file. The name that
	 *            have to have
	 * @param os
	 *            output stream to write the output.
	 * @throws MakePluginPackageServiceException
	 *             if there is any problem compressing.
	 */
	public void compressPackageSet(File folder, String fileName, OutputStream os)
			throws MakePluginPackageServiceException;

	/**
	 * Compress a plugin folder using the plugin's name as a package.
	 * 
	 * @param folder
	 *            the directory to compress.
	 * @param os
	 *            output stream to write the output.
	 * @throws MakePluginPackageServiceException
	 *             if there is any problem compressing.
	 */
	public void compressPackage(File folder, OutputStream os)
			throws MakePluginPackageServiceException;

	/**
	 * Compress a plugin folder using the plugin's name as a package, only with
	 * the files needed by the package index.
	 * 
	 * @param folder
	 *            the directory to compress.
	 * @param os
	 *            output stream to write the output.
	 * @throws MakePluginPackageServiceException
	 *             if there is any problem compressing.
	 */
	public void compressPackageIndex(File folder, OutputStream os)
			throws MakePluginPackageServiceException;

	/**
	 * Reads the package.info file from a directory a fills the the properties
	 * of the {@link PackageInfo} object.
	 * 
	 * @param directory
	 *            the root directory that contains the installinfo file
	 * @param installInfo
	 *            the installinfo file that has to be filled.
	 * @throws InstallerInfoFileException. if there is any problem reading the
	 *         file
	 */
	public void readPackageInfo(File directory, PackageInfo installerInfo)
			throws InstallerInfoFileException;

	/**
	 * Reads the package.info file from a package set and fills the the
	 * properties of the {@link PackageInfo} objects.
	 * 
	 * @param is
	 *            the input stream of a bundle.
	 * @param packageInfos
	 *            a list of the information of the packages to install.
	 * @param zipEntriesMap
	 *            a map to retrieve the zipEntry for every package. This
	 *            information is necessary to select the plugin to decompress.
	 * @throws InstallPackageServiceException
	 *             if there is a problem reading the bundle.
	 */
	public void readPackageSetInfo(InputStream is,
			List<PackageInfo> packageInfos,
			Map<PackageInfo, String> zipEntriesMap)
			throws InstallPackageServiceException;

	/**
	 * Reads the package.info file from a bundle fills the the properties of the
	 * {@link PackageInfo} objects.
	 * 
	 * @param is
	 *            the input stream of a bundle.
	 * @param packageInfos
	 *            a list of the information of the packages to install.
	 * @param zipEntriesMap
	 *            a map to retrieve the zipEntry for every package. This
	 *            information is necessary to select the plugin to decompress.
	 * @throws InstallPackageServiceException
	 *             if there is a problem reading the bundle.
	 */
	public void readPackageInfo(InputStream is, List<PackageInfo> packageInfos,
			Map<PackageInfo, String> zipEntriesMap, String name)
			throws InstallPackageServiceException;

	/**
	 * Writes the package.info file in a concrete directory.
	 * 
	 * @param directory
	 *            the directory.
	 * @param packageInfo
	 *            the information to write.
	 * @throws InstallerInfoFileException
	 *             if there is any problem writing the package.info file.
	 */
	public void writePackageInfo(File directory, PackageInfo packageInfo)
			throws InstallerInfoFileException;

	/**
	 * Writes the package.info file in a concrete directory to be used in a
	 * package index.
	 * 
	 * @param directory
	 *            the directory.
	 * @param packageInfo
	 *            the information to write.
	 * @throws InstallerInfoFileException
	 *             if there is any problem writing the package.info file.
	 */
	public void writePackageInfoForIndex(File directory, PackageInfo packageInfo)
			throws InstallerInfoFileException;

	/**
	 * It search a package inside an installer file by the zip name and returns
	 * the stream in this position ready to decompres.
	 * 
	 * @param is
	 *            the input stream of a bundle.
	 * @param zipEntry
	 *            the name of the zip entry.
	 * @return the input stream ready to install.
	 * @throws InstallPackageServiceException
	 *             if there is a problem reading the stream
	 */
	public InputStream searchPackage(InputStream is, String zipEntry)
			throws InstallPackageServiceException;

	/**
	 * It reads a compressed file and retrieve the package information.
	 * 
	 * @param is
	 *            the compressed file
	 * @return the information of the package
	 * @throws InstallPackageServiceException
	 *             if there is a problem decompressing the file.
	 */
	public PackageInfo readCompressedPackageInfo(InputStream is)
			throws InstallPackageServiceException;

	public List<String> getDefaultSelectedPackagesIDs();

}
