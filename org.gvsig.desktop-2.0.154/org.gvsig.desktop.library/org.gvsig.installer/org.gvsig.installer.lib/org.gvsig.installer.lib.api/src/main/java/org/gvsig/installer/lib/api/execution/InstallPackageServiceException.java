/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api.execution;

import org.gvsig.tools.exception.BaseException;

/**
 * Base exception for all the exceptions that are thrown by the process of
 * installation of a package. The {@link InstallPackageService} throws this
 * exception on its methods.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallPackageServiceException extends BaseException {

	private static final String message = "Error installing a package";

	private static final long serialVersionUID = -7645352975209203727L;

	private static final String KEY = "installer_execution_exception";

	/**
	 * @see BaseException#BaseException(String, long)
	 */
	public InstallPackageServiceException() {
		super(message, KEY, serialVersionUID);
	}

	/**
	 * @see BaseException#BaseException(Throwable, String, long)
	 */
	public InstallPackageServiceException(Throwable cause) {
		super(message, cause, KEY, serialVersionUID);
	}

	protected InstallPackageServiceException(String message, Throwable cause,
			String key, long code) {
		super(message, cause, key, code);
	}

	protected InstallPackageServiceException(String message, String key,
			long code) {
		super(message, key, code);
	}

}
