/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api.execution;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.task.SimpleTaskStatus;

/**
 * <p>
 * This service is used to read a bundle file and install the packages contained
 * in it. It has methods to read the packages contained in a bundle and to
 * install one of them.
 * </p>
 * <p>
 * The bundle is a file that has been created using the
 * {@link MakePluginPackageService} service. It is possible to create an
 * installer manually, but it has to have the structure defined by the
 * {@link InstallerManager} class.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface InstallPackageService extends Service {

	public void reset();

	/**
	 * Adds a the URI of a bundle that contains some packages to install. A
	 * bundle is a compressed zip file with a structure defined in the
	 * {@link InstallerManager} class.
	 * 
	 * @param bundleURI
	 *            the URI of a bundle.
	 * @throws InstallPackageServiceException
	 *             it is thrown if there is an exception reading the URI.
	 */
	public void addBundle(File bundleFile)
			throws InstallPackageServiceException;

	/**
	 * Adds a URL of a bundle that contains some packages to install. A bundle
	 * is a compressed zip file with a structure defined in the
	 * {@link InstallerManager} class.
	 * 
	 * @param bundleURL
	 *            the {@link URL} of a bundle.
	 * @throws InstallPackageServiceException
	 *             it is thrown if there is an exception reading the {@link URL}
	 *             .
	 */
	public void addBundle(URL bundleURL) throws InstallPackageServiceException;

	/**
	 * In reads a directory and reads all the bundles. This method retrieve all
	 * the information of the packages located in the bundles and allows to the
	 * user to show this information.
	 * 
	 * @param bundlesDirectory
	 *            a directory that contains bundles.
	 * @throws InstallPackageServiceException
	 *             If there is any problem reading the directory of bundles.
	 */
	public void addBundlesFromDirectory(File bundlesDirectory)
			throws InstallPackageServiceException;

	/**
	 * Install a package in a concrete gvSIG installation directory. The
	 * selected package has to be contained in one of the bundles that has been
	 * previously added using the {@link #addBundle(File)} or the
	 * {@link #addBundlesFromDirectory(File)} method.
	 * 
	 * @param applicationDirectory
	 *            a valid root directory where an instance og gvSIG is
	 *            installed.
	 * @param packageInfo
	 *            the package to install.
	 * @throws InstallPackageServiceException
	 *             If there is an error installing the package.
	 */
	public void installPackage(File applicationDirectory,
			PackageInfo packageInfo) throws InstallPackageServiceException;

	/**
	 * Install a package in a concrete gvSIG installation directory. The
	 * selected package has to be contained in one of the bundles that has been
	 * previously added using the {@link #addBundle(File)} or the
	 * {@link #addBundlesFromDirectory(File)} method.
	 * 
	 * @param applicationDirectory
	 *            a valid root directory where an instance og gvSIG is
	 *            installed.
	 * @param packageCode
	 *            the code of the package.
	 * @throws InstallPackageServiceException
	 *             if there is an error installing the {@link PackageInfo#}
	 */
	public void installPackage(File applicationDirectory, String packageCode)
			throws InstallPackageServiceException;

	/**
	 * @return the number of packages that has been loaded form one or more
	 *         bundles using the {@link #addBundle(File)} or the
	 *         {@link #addBundlesFromDirectory(File)} method.
	 */
	public int getPackageCount();

	/**
	 * Return the package information for a concrete position. All the packages
	 * has been loaded form one or more bundles using the
	 * {@link #addBundle(File)} or the {@link #addBundlesFromDirectory(File)}
	 * method.
	 * 
	 * @param index
	 *            the position of the package.
	 * @return the information of a package. returns <code>null</code> if the
	 *         package doesn't exist.
	 */
	public PackageInfo getPackageInfo(int index);

	/**
	 * Return the package information for a concrete position. All the packages
	 * has been loaded form one or more bundles using the
	 * {@link #addBundle(File)} or the {@link #addBundlesFromDirectory(File)}
	 * method.
	 * 
	 * @param packageCode
	 *            the code of the package.
	 * @return the information of a package. returns <code>null</code> if the
	 *         package doesn't exist.
	 */
	public PackageInfo getPackageInfo(String packageCode);

	/**
	 * Download the package and register it in the manager.
	 * 
	 * @param packageInfo
	 */
	public void downloadPackage(PackageInfo packageInfo,
			SimpleTaskStatus taskStatus) throws InstallPackageServiceException;

	/**
     * 
     */
	// public void resetPackages();

	/**
	 * @return
	 */
	public List<String> getDefaultSelectedPackagesIDs();

	/**
	 * Gets the list of all the categories of *all* the packages.
	 * 
	 * @return a list with the categories of all the packages.
	 */
	public List<String> getCategories();

	/**
	 * Gets the list of different the types of *all* the packages.
	 * 
	 * @return a list with the types of packages.
	 */
	public List<String> getTypes();
}
