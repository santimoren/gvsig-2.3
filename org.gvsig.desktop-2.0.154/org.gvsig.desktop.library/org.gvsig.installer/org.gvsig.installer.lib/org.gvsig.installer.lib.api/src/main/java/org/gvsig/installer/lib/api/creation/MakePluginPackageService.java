/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api.creation;

import java.io.File;
import java.io.OutputStream;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.tools.service.Service;

/**
 * <p>
 * This service is used to create a bundle that contains a package of plugin
 * type. It supports just one package for every installer. It contains a method
 * to set the plugin path and some methods to set the installer information.
 * </p>
 * <p>
 * It has also methods to load the installed plugins from a directory (if
 * exists) and methods to create the bundle file in an {@link OutputStream}.
 * </p>
 * <p>
 * A plugin package has the same structure of a standard package defined in
 * {@link InstallerManager} but it adds some new files. An example of the
 * structure of a bundle with a plugin package could be:
 * </p>
 * 
 * <pre>
 * - bundle (compressed file)
 * 		- org.gvsig.plugin1-1_0_0-23 (compressed file)
 * 			- org.gvsig.plugin1
 * 			  	- package.info   
 *  			- install.xml
 *  			- files
 *  				- gvSIG
 *  					- extensiones
 *  						- org.gvsig.plugin2
 *  							- file1
 *  							- file2
 * </pre>
 * <p>
 * This structure has some extra files:
 * </p>
 * <lu> <li><b>install.xml</b>: ant file that is executed in the execution of
 * the installer to do some extra actions in the installation process. One of
 * these actions is copy all the files located in the files directory</li> <li>
 * <b>files directory</b>: it contains some files of other extensions that have
 * to be copied using the ant script.</li> </lu>
 * <p>
 * The usage of the ant script to copy files from other plugins is not
 * recommended because it is possible that different installers overrides the
 * same file. The suggestion it that one plugin has to have all the files that
 * it needs to work inside the plugin and it never has to override some external
 * files.
 * </p>
 * <p>
 * The implementations of this interface have to have a constructor with a File
 * parameter that is the directory when the plugins are located. If the
 * directory doens't exists the creation throws an exception.
 * </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface MakePluginPackageService extends Service {

	/**
	 * It creates a package set with a single plugin package inside.
	 * 
	 * @param packageInfo
	 *            the information of the plugin that has to be included in the
	 *            package set
	 * @param packageStream
	 *            the stream where the the package set will be created
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception creating the package
	 *             set
	 */
	public void createPackageSet(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException;

	/**
	 * It creates a package of a plugin.
	 * 
	 * @param packageInfo
	 *            the information of the plugin that has to be included in the
	 *            package
	 * @param packageStream
	 *            the stream where the the package will be created
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception creating the package
	 */
	public void createPackage(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException;

	/**
	 * Prepares and copies the needed files for the package. It is used to copy
	 * the ant build file and the additional external files to be included in
	 * the package.
	 * 
	 * @param newPackageInfo
	 *            the information of the new plugin.
	 * @param originalPackageInfo
	 *            the information of the original plugin.
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception preparing the package
	 */
	public void preparePackage(PackageInfo packageInfo,
			File originalPackageFolder)
			throws MakePluginPackageServiceException;

	/**
	 * It creates the index for a package of a plugin. This file may be used to
	 * be included in the main gvSIG remote index, or downloaded as it is.
	 * 
	 * @param packageInfo
	 *            the information of the plugin that has to be included in the
	 *            package index
	 * @param packageStream
	 *            the stream where the the package index will be created
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception creating the package
	 */
	public void createPackageIndex(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException;

	/**
	 * Writes a package info file with the information provided.
	 * 
	 * @param packageInfo
	 *            the package that has to be written into the file
	 * @param folder
	 *            the folder where the file is to be created
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception writing the file
	 */
	public void writePackageInfo(PackageInfo packageInfo, File folder)
			throws MakePluginPackageServiceException;

	/**
	 * Writes a package info file with the information provided.
	 * 
	 * @param packageInfo
	 *            the package that has to be written into the file
	 * @param folder
	 *            the folder where the file is to be created
	 * @throws MakePluginPackageServiceException
	 *             it is thrown when there is an exception writing the file
	 */
	public void writePackageInfoForIndex(PackageInfo packageInfo, File folder)
			throws MakePluginPackageServiceException;

	/**
	 * It returns the number of plugin packages that are installed in the folder
	 * that has been added using the constructor. It can be used in an iteration
	 * process combined with the {@link #getPluginPackageInfo(int)} method to
	 * know all the installed plugins.
	 * 
	 * @return the number of plugins installed in a directory.
	 */
	public int getPluginPackageCount();

	/**
	 * It returns an instance of an {@link PackageInfo} class, that is a class
	 * that contains all the package information (name, version...).
	 * 
	 * @param index
	 *            the position of the plugin that has to be retrieved.
	 * @return the installer information of a plugin package. Returns
	 *         <code>null</code> if the package doesn't exist.
	 */
	public PackageInfo getPluginPackageInfo(int index);

	/**
	 * It returns an instance of an {@link PackageInfo} class, that is a class
	 * that contains all the package information (name, version...).
	 * 
	 * @param code
	 *            code of the plugin, defined in the <b>package.info</b> file
	 *            like a property.
	 * @return the installer information of a plugin package. Returns
	 *         <code>null</code> if the package doesn't exist.
	 */
	public PackageInfo getPluginPackageInfo(String code);

	/**
	 * This method returns the default ant script that will be used by some
	 * plugins to copy some external files in the installation process.
	 * 
	 * @return a string that contains the standard ant script
	 * @throws MakePluginPackageServiceException
	 *             if there is a problem reading the file
	 */
	public String getDefaultAntScript()
			throws MakePluginPackageServiceException;

	/**
	 * Returns the list of packages already installed in the application.
	 * 
	 * @return the list of packages already installed in the application
	 * @throws MakePluginPackageServiceException
	 *             if there is an error getting the installed packages
	 */
	public PackageInfo[] getInstalledPackages()
			throws MakePluginPackageServiceException;

	/**
	 * Returns the folder where a plugin is located.
	 * 
	 * @param selectedPackageInfo
	 *            the info of theplugin to look for
	 * @return the plugin's folder
	 * @throws MakePluginPackageServiceException
	 *             if there is an error locating the folder
	 */
	public File getPluginFolder(PackageInfo packageInfo)
			throws MakePluginPackageServiceException;

	/**
	 * @param folder
	 * @return
	 */
	public boolean deleteDir(File folder);

}
