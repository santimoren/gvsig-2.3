/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api.creation;

import org.gvsig.tools.exception.BaseException;

/**
 * Base exception for all the exceptions that are thrown by the process of
 * creation of an installer for a package of a plugin. The
 * {@link MakePluginPackageService} throws this exception on its methods.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class MakePluginPackageServiceException extends
		MakePackageServiceException {

	private static final long serialVersionUID = 4388665353519171171L;

	private static final String KEY = "make_plugin_package_exception";

	/**
	 * @see BaseException#BaseException(String, String, long)
	 */
	public MakePluginPackageServiceException(String message) {
		super(message, KEY, serialVersionUID);
	}

	/**
	 * @see BaseException#BaseException(String, Throwable, String, long)
	 */
	public MakePluginPackageServiceException(String message, Throwable cause) {
		super(message, cause, KEY, serialVersionUID);
	}

	/**
	 * @see BaseException#BaseException(String, String, long)
	 */
	public MakePluginPackageServiceException(String message, String key,
			long serialVersion) {
		super(message, key, serialVersion);
	}

	/**
	 * @see BaseException#BaseException(String, Throwable, String, long)
	 */
	public MakePluginPackageServiceException(String message, Throwable cause,
			String key, long serialVersion) {
		super(message, cause, key, serialVersion);
	}

}
