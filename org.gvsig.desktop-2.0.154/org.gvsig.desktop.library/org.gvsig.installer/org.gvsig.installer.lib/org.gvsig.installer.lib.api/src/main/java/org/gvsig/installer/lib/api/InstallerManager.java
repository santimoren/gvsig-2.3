/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.MessageFormat;
import java.util.List;

import org.gvsig.installer.lib.api.creation.MakePackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.service.Manager;

/**
 * <p>
 * This manager is used to register and create the services that are used to
 * manage the creation and the execution of installers. An installer is a file
 * called <b>bundle</b> that is composed of a set <b>packages</b>.
 * </p>
 * <p>
 * A package has some information that is defined by the {@link PackageInfo}
 * class and is composed of a set of attributes. One of these attributes, the
 * type, denotes if the package is a plugin, theme, translation, etc.
 * </p>
 * <p>
 * In practice a bundle is just a compressed zip file that has a compressed zip
 * file for every package to install. The structure of a bundle file with two
 * packages of type plugin could be:
 * </p>
 * 
 * <pre>
 * - bundle (compressed file)
 * 		- org.gvsig.plugin1-1_0_0-23 (compressed file)
 * 			- org.gvsig.plugin1
 * 			  	- package.info   			
 *  	- org.gvsig.plugin2-2_0_1-35 (compressed file)
 *  		- org.gvsig.plugin1
 *  			- package.info
 * </pre>
 * <p>
 * <b>bundle</b> is the compressed file that contains a zip entry for every
 * package to install. The name of the zip entry follows next pattern:
 * </p>
 * 
 * <pre>
 * 		[package code]-[version]-[build]
 * </pre>
 * <p>
 * Every zip entry contains a main folder inside that contains all the package
 * files that are used in the installation process. Depending of the type of
 * packages, the information inside this folder can be different, but all the
 * types of packages have to have the <b>package.info</b>file that has all the
 * package information. To see the <b>package.info</b> description see
 * {@link PackageInfo}.
 * <p>
 * </p>
 * The services that offers this managers are basically two: the creation of
 * bundles for just one package of plugin type and a service for the
 * installation of packages from a bundle. </p>
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface InstallerManager extends Manager {

	public static final String PACKAGE_INDEX_EXTENSION = ".gvspki";
	public static final String PACKAGE_SET_EXTENSION = ".gvspks";
	public static final String PACKAGE_EXTENSION = ".gvspkg";
	
        public static final String PACKAGE_INFO_FILE_NAME = "package.info";
	/**
	 * Package state default values.
	 */
	public static interface STATE {

		static final String DEVEL = "devel";
		static final String TESTING = "testing";
		static final String PILOT = "pilot";
		static final String PROTOTYPE = "prototype";
		static final String ALPHA = "alpha";
		static final String BETA = "beta";
		static final String RC = "RC";
		static final String FINAL = "final";
	}

	/**
	 * Supported operating system default values.
	 */
	public static interface OS {

		static final String ALL = "all";
		static final String LINUX = "lin";
		static final String WINDOWS = "win";
		static final String DARWIN = "darwin";
	}

	/**
	 * Supported architecture default values.
	 */
	public static interface ARCH {

		static final String ALL = "all";
		static final String X86 = "x86";
		static final String X86_64 = "x86_64";
	}

	/**
	 * Supported Java virtual machine version default values.
	 */
	public static interface JVM {

		static final String J1_5 = "j1_5";
		static final String J1_6 = "j1_6";
		static final String J1_7 = "j1_7";
		static final String J1_8 = "j1_8";
		static final String J1_9 = "j1_9";
	}

	/**
	 * Fields into the bundle file name message format.
	 * 
	 * @see InstallerManager#getPackageSetNameFormat()
	 * @see InstallerManager#setPackageSetNameFormat(String)
	 */
	public static interface PACKAGE_FILE_NAME_FIELDS {

		static final int GVSIG_VERSION = 0;
		static final int NAME = 1;
		static final int VERSION = 2;
		static final int BUILD = 3;
		static final int STATE = 4;
		static final int OS = 5;
		static final int ARCH = 6;
		static final int JVM = 7;
	}

	/**
	 * It registers a class that implements the service for the creation of
	 * bundle that contains inside a package of type plugin. The registered
	 * class have to implement the {@link MakePluginPackageService} interface.
	 * 
	 * @param clazz
	 *            class that implements the {@link MakePluginPackageService}
	 *            interface.
	 */
	public void registerMakePluginPackageService(
			Class<? extends MakePluginPackageService> clazz);

	// /**
	// * It creates and returns an object that is used to create a bundle that
	// * contains inside a package of type plugin. All the parameters are set
	// * using the {@link MakePluginPackageService} interface. *
	// *
	// * @return an object that is used to create a plugin installer
	// * @throws MakePluginPackageServiceException
	// * when there is a problem creating the service
	// */
	// public MakePluginPackageService getMakePluginPackageService(
	// File pluginsDirectory) throws MakePluginPackageServiceException;

	/**
	 * It creates and returns an object that is used to create a bundle that
	 * contains inside a package of type plugin. All the parameters are set
	 * using the {@link MakePluginPackageService} interface. *
	 * 
	 * @return an object that is used to create a plugin installer
	 * @throws MakePluginPackageServiceException
	 * @throws MakePluginPackageServiceException
	 *             when there is a problem creating the service
	 */
	public MakePluginPackageService getMakePluginPackageService()
			throws MakePluginPackageServiceException;

	/**
	 * Returns a list of package infos for the already installed plugins.
	 * 
	 * @param pluginsDirectory
	 *            where to look for the installed plugins
	 * @return the list of package infos for the already installed plugins
	 * @throws MakePluginPackageServiceException
	 *             if there is an error loading the information of the installed
	 *             plugins
         * @deprecated use getInstalledPackages() without parameters
	 */
	public PackageInfo[] getInstalledPackages(File pluginsDirectory)
			throws MakePluginPackageServiceException;

	/**
	 * Returns a list of package infos for the already installed plugins.
	 * 
	 * @return the list of package infos for the already installed plugins
	 * @throws MakePluginPackageServiceException
	 *             if there is an error loading the information of the installed
	 *             plugins
	 */
	public PackageInfo[] getInstalledPackages()
			throws MakePluginPackageServiceException;

	/**
	 * Returns the package bundle file name format.
	 * <p>
	 * The string has to use a suitable {@link MessageFormat} format, and the
	 * available field numbers are the ones defined in the
	 * BUNDLE_FILE_NAME_FIELDS interface.
	 * </p>
	 * 
	 * @return the package bundle file name format.
	 */
	public String getPackageSetNameFormat();

	/**
	 * Sets the package bundle file name format.
	 * 
	 * @see InstallerManager#getPackageSetNameFormat()
	 * @param packageBundleNameFormat
	 *            the package bundle file name format.
	 */
	public void setPackageSetNameFormat(String packageBundleNameFormat);

	/**
	 * Returns the name of the package set file for a given package info.
	 * 
	 * @param info
	 *            of the plugin
	 * @return the name of the package set file
	 */
	public String getPackageSetFileName(PackageInfo info);

	/**
	 * Returns the name of the package file for a given package info.
	 * 
	 * @param info
	 *            of the plugin
	 * @return the name of the package file
	 */
	public String getPackageFileName(PackageInfo info);

	/**
	 * Returns the name of the package index file for a given package info.
	 * 
	 * @param info
	 *            of the plugin
	 * @return the name of the package index file
	 */
	public String getPackageIndexFileName(PackageInfo info);

	/**
	 * It registers a class that implements the service for the installation of
	 * a package that is inside a bundle. This class has to implement the
	 * {@link InstallPackageService} interface.
	 * 
	 * @param clazz
	 *            class that implements the {@link InstallPackageService}
	 *            interface.
	 */
	public void registerInstallPackageService(
			Class<? extends InstallPackageService> clazz);

	/**
	 * It creates and returns an object that is used to install a package in
	 * gvSIG. All the parameters are set using the {@link InstallPackageService}
	 * interface.
	 * 
	 * @return an object that is used to install the package.
	 * @throws InstallPackageServiceException
	 *             when there is a problem creating the service.
	 */
	public InstallPackageService getInstallPackageService()
			throws InstallPackageServiceException;

	/**
	 * Returns the default extensions of the package files.
	 * 
	 * @return the default extensions of the package files
	 */
	public String getDefaultPackageFileExtension();

	/**
	 * Returns the default extensions of the package set files.
	 * 
	 * @return the default extensions of the package set files
	 */
	public String getDefaultPackageSetFileExtension();

	/**
	 * Returns the default extensions of the index set files.
	 * 
	 * @return the default extensions of the index set files
	 */
	public String getDefaultIndexSetFileExtension();

	/**
	 * Return the OS code of the system
	 * 
	 * @return os code of the system
	 */
	public String getOperatingSystem();
        public String getOperatingSystemFamily();
	public String getOperatingSystemName();
	public String getOperatingSystemVersion();
	/**
	 * Returns the Architecture code of the system
	 * 
	 * @return architecture code of the system
	 */
	public String getArchitecture();

	/**
	 * Create a empty dependency object.
	 * 
	 * @return the dependency
	 */
	public Dependency createDependency();

	/**
	 * Create a dependency instance with the data of the package.
	 * 
	 * @param packageInfo
	 * @return a dependency of the package
	 */
	public Dependency createDependency(PackageInfo packageInfo);

	/**
	 * Create a dependencies calculator.
	 * 
	 * @return the dependencias calculator
	 */
	public DependenciesCalculator createDependenciesCalculator(
			InstallPackageService installService);

	/**
	 * Create a version instance
	 * 
	 * @return the version
	 */
	public Version createVersion();

	public PackageInfo createPackageInfo();
	
	/**
	 * Create a PackageInfo and load contents from the specified InputStream using the
	 * default reader.
	 * 
	 * @param packegeinfo as URL
	 * @return the created packageInfo
	 * @throws IOException 
	 * @throws BaseException 
	 */
	public PackageInfo createPackageInfo(InputStream packegeinfo) throws BaseException;

	public PackageInfoWriter getDefaultPackageInfoWriter();

	public PackageInfoReader getDefaultPackageInfoReader();

	public MakePackageService createMakePackage(File packageFolder,
			PackageInfo packageInfo);

	public void setDownloadBaseURL(URL url);

	public URL getDownloadBaseURL();

	public void setVersion(Version version);

	public Version getVersionEx();


	/**
	 * 
	 * @param version
	 * @deprecated Use {@link #setVersion(Version)}
	 */
    public void setVersion(String version);

    /**
     * 
     * @return
     * @deprecated Use {@link #getVersionEx()}
     */
    public String getVersion();

	/**
	 * Gets a List of all the folders where there might be addons folders
	 * (addons repositories).
	 * 
	 * @return list of repositories paths
	 */
	public List<File> getLocalAddonRepositories();

	/**
	 * Gets a List of all the folders where there might be addons folders
         * for the specified type of packages
	 * (addons repositories).
	 * 
	 * @return list of repositories paths
	 */
        public List<File> getLocalAddonRepositories(String type);
                
	/**
	 * Adds an addon local repository location to the manager list,
         * and register the type of addons for this repository.
	 * 
	 * @param path
	 *            of the repository
         * @param type
         *            of addons in this repository
	 */
	public void addLocalAddonRepository(File path, String type);
        
        /**
         * @deprecated use addLocalAddonRepository(File path, String type)
         */
	public void addLocalAddonRepository(File path);

        /**
         * Retrieve the default type of addons for the file.
         * Use the registry of local addon repositories to identify the
         * type for this file.
         * 
         * @param file
         * @return 
         */
        public String getDefaultLocalRepositoryType(File file);
                
	/**
	 * Gets a List of all the addon folders. An addon folder is a folder inside
	 * any addon repository with a package.info file inside. (addons
	 * repositories).
	 * 
	 * @return list of addon paths folders.
	 */
	public List<File> getAddonFolders();
        
	/**
	 * Gets a List of all the addon folders of a type of package. 
         * An addon folder is a folder inside
	 * any addon repository with a package.info file inside. (addons
	 * repositories).
	 * 
	 * @return list of addon paths folders.
	 */
        public List<File> getAddonFolders(String type);

	/**
	 * Gets the folder of the addon with the code provided, or null if not
	 * found.
	 * 
	 * @param code
	 *            of the addon
	 * 
	 * @return File of the folder of the addon.
	 */
	public File getAddonFolder(String code);

	/**
	 * The local addons folder where normally all addons are installed.
	 * 
	 * @param defaultAddonsRepository
         * @deprecated use {@link #setDefaultLocalAddonRepository(java.io.File, java.lang.String) }
	 */
	public void setDefaultLocalAddonRepository(File defaultAddonsRepository);
	
	/**
	 * The local addons folder where normally all addons are installed.
	 * 
	 * @return default addons repository folder.
         * @deprecated use {@link #getDefaultLocalAddonRepository(java.lang.String) }
	 */
	public File getDefaultLocalAddonRepository();

        public static final int ACCESS_READ = 0;
        public static final int ACCESS_WRITE = 1;
        /**
         * Get the path to the default local repository for a type of package.
         * Grant that the path has the access required, can be:
         * 
         * ACCESS_READ
         * ACCESS_WRITE
         * 
         * If don't has a repository for this type of package or don't has the
         * requiered access mode return null.
         * 
         * @param packageType type of package for that ask the default path
         * @param access type of access to the repository.
         * @return the path to the repository
         */
        public File getDefaultLocalAddonRepository(String packageType, int access);
        
        /**
         * Get the path to the default local repository for a type of package.
         * This is a utility method using by default ACCESS_READ to the reppository.
         * 
         * @param packageType type of package for that ask the default path
         * @return the path to the repository
         */
        public File getDefaultLocalAddonRepository(String packageType);

        
        /**
         * Set the path to the default local repository for a type of package
         * @param defaultAddonsRepository path to the repository
         * @param packageType type of package in this repository path
         */
        public void setDefaultLocalAddonRepository(File defaultAddonsRepository, String packageType);
	
        
	public List<byte[]> getPublicKeys();

	// public PackageInfoFiles getPackageInfoFiles(PackageInfo packageInfo);

	public boolean hasProviderToThisPackage(PackageInfo packageInfo);
        
        public boolean needAdminRights();
}
