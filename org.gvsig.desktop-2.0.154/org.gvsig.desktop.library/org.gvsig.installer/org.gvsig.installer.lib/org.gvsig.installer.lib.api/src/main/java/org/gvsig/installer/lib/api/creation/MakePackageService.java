/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.api.creation;

import java.io.OutputStream;
import java.net.URL;

import org.gvsig.tools.service.Manager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public interface MakePackageService {

	public static final String ANT_FILE_NAME = "install.xml";

	public abstract Manager getManager();

	public abstract void preparePackage() throws MakePackageServiceException;

	public abstract void createPackage(OutputStream packageStream)
			throws MakePackageServiceException;

	public abstract void createPackageIndex(URL downloadurl,
			OutputStream packageStream) throws MakePackageServiceException;

	public abstract void createPackageSet(OutputStream packageStream)
			throws MakePackageServiceException;

	public void createPackageIndexSet(URL downloadurl,
			OutputStream packageStream) throws MakePackageServiceException;

	public abstract String getDefaultAntScript()
			throws MakePackageServiceException;

}
