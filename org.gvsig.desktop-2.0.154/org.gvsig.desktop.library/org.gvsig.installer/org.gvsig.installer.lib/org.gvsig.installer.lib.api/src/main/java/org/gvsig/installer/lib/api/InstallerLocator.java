/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.api;

import org.gvsig.tools.locator.AbstractLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * This Locator provides the entry point for the gvSIG {@link InstallerManager}
 * 
 * @see {@link Locator}
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerLocator extends AbstractLocator {

	private static final String LOCATOR_NAME = "InstallerLocator";
	/**
	 * InstallerManager name used by the locator to access the instance
	 */
	public static final String INSTALLER_MANAGER_NAME = "InstallerManager";
	private static final String INSTALLER_MANAGER_DESCRIPTION = "InstallerManager of gvSIG";

	/**
	 * Unique instance.
	 */
	private static final InstallerLocator instance = new InstallerLocator();

	/**
	 * @see Locator#getLocatorName()
	 */
	public String getLocatorName() {
		return LOCATOR_NAME;
	}

	/**
	 * Return a reference to {@link InstallerManager}.
	 * 
	 * @return a reference to InstallerManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static InstallerManager getInstallerManager()
			throws LocatorException {
		return (InstallerManager) getInstance().get(INSTALLER_MANAGER_NAME);
	}

	/**
	 * Return the singleton instance.
	 * 
	 * @return the singleton instance
	 */
	public static InstallerLocator getInstance() {
		return instance;
	}

	/**
	 * Registers the Class implementing the {@link InstallerManager} interface.
	 * 
	 * @param clazz
	 *            implementing the InstallerManager interface
	 */
	public static void registerInstallerManager(
			Class<? extends InstallerManager> clazz) {

		getInstance().register(INSTALLER_MANAGER_NAME,
				INSTALLER_MANAGER_DESCRIPTION, clazz);
	}
}
