/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.execution;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.InstallerServiceTest;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallPackageServiceTest extends InstallerServiceTest {

	@Override
	protected void doSetUp() throws Exception {
		// TODO Auto-generated method stub

	}

	public void testReadPlugins() throws LocatorException,
			MakePluginPackageServiceException, InstallPackageServiceException,
			IOException {
		File installersDirectory = super.getInstallersDirectory();

		InstallPackageService installPackageService = InstallerLocator
				.getInstallerManager().getInstallPackageService();
		installPackageService.addBundlesFromDirectory(installersDirectory);

		Assert.assertEquals(3, installPackageService.getPackageCount());

		int pluginsNumber = 0;
		for (int i = 0; i < 3; i++) {
			PackageInfo packageInfo = installPackageService.getPackageInfo(i);
			if (packageInfo.getCode().equals("org.gvsig.plugin1")) {
				assertNotNull(packageInfo.getState());
				pluginsNumber++;
			} else if (packageInfo.getCode().equals("org.gvsig.plugin2")) {
				assertEquals("RC1", packageInfo.getState());
				pluginsNumber++;
			} else if (packageInfo.getCode().equals("org.gvsig.plugin3")) {
				assertEquals("Testing", packageInfo.getState());
				pluginsNumber++;
			}
		}

		Assert.assertEquals(3, pluginsNumber);
	}

	public void testInstallPluginFromApplication()
			throws InstallPackageServiceException, IOException,
			LocatorException, MakePluginPackageServiceException {
		File installersDirectory = super.getInstallersDirectory();
		File applicationDirectory = super.getApplicationDirectory();

		InstallPackageService installerExecutionService = InstallerLocator
				.getInstallerManager().getInstallPackageService();
		installerExecutionService.addBundlesFromDirectory(installersDirectory);

		PackageInfo packageInfo = installerExecutionService
				.getPackageInfo("org.gvsig.plugin1");
		installerExecutionService.installPackage(applicationDirectory,
				packageInfo);

		Assert.assertEquals(3, installerExecutionService.getPackageCount());

		// Reading the excution and check that the file is installed
		MakePluginPackageService installerCreationService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();
		Assert
				.assertEquals(4, installerCreationService
						.getPluginPackageCount());
	}

	public void testInstallPluginFromExternal()
			throws InstallPackageServiceException, IOException,
			LocatorException, MakePluginPackageServiceException {
		File applicationDirectory = super.getApplicationDirectory();
		File externalInstallationFile = new File(applicationDirectory
				.getAbsoluteFile()
				+ File.separator + "install" + File.separator + "bundle1.zip");

		InstallPackageService installerExecutionService = InstallerLocator
				.getInstallerManager().getInstallPackageService();
		installerExecutionService.addBundle(externalInstallationFile);

		PackageInfo packageInfo = installerExecutionService
				.getPackageInfo("org.gvsig.plugin1");

		Assert.assertEquals(3, installerExecutionService.getPackageCount());

		installerExecutionService.installPackage(applicationDirectory,
				packageInfo);

		// Reading the excution and check that the file is installed
		MakePluginPackageService installerCreationService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();
		Assert
				.assertEquals(4, installerCreationService
						.getPluginPackageCount());
	}

	// public void testInstallPluginWithAntScript() throws
	// InstallPackageServiceException, LocatorException,
	// MakePluginPackageServiceException, IOException{
	// File applicationDirectory = super.getApplicationDirectory();
	// File installersDirectory = super.getInstallersDirectory();
	// File pluginsDirectory = super.getPluginsDirectory();
	//
	// InstallPackageService installerExecutionService =
	// InstallerLocator.getInstallerManager().getInstallPackageService();
	// installerExecutionService.addBundlesFromDirectory(installersDirectory);
	//
	// PackageInfo packageInfo =
	// installerExecutionService.getPackageInfo("org.gvsig.plugin2");
	//
	// Assert.assertEquals(3, installerExecutionService.getPackageCount());
	//
	// installerExecutionService.installPackage(applicationDirectory,
	// packageInfo);
	//
	// //Reading the excution and check that the file is installed
	// MakePluginPackageService installerCreationService =
	// InstallerLocator.getInstallerManager().getMakePluginPackageService(pluginsDirectory);
	//
	// Assert.assertEquals(5, installerCreationService.getPluginPackageCount());
	// }

}
