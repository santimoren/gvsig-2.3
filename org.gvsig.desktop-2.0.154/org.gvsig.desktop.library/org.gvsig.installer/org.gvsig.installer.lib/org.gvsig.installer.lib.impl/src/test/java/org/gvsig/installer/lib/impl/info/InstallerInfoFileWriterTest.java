/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.impl.DefaultPackageInfo;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerInfoFileWriterTest extends
		AbstractLibraryAutoInitTestCase {

	@Override
	protected void doSetUp() throws Exception {
		// TODO Auto-generated method stub

	}

	public void testWriteFile() throws LocatorException,
			InstallerInfoFileException {
		DefaultPackageInfo installerInfo = new DefaultPackageInfo();

		installerInfo.setCode("org.gvsig.myplugin");
		installerInfo.setName("My name");
		installerInfo.setDescription("My description");
		installerInfo.setVersion("1.0.0");
		installerInfo.setBuild(4);
		installerInfo.setState("final");
		installerInfo.setOfficial(false);
		installerInfo.setType("plugin");

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		InstallerInfoFileWriter installerInfoFileWriter = new InstallerInfoFileWriter();
		installerInfoFileWriter.write(installerInfo, out);

		PackageInfo installerInfo2 = new DefaultPackageInfo();

		InstallerInfoFileReader installerInfoFileReader = new InstallerInfoFileReader();
		installerInfoFileReader.read(installerInfo2, new ByteArrayInputStream(
				out.toByteArray()));

		assertEquals("org.gvsig.myplugin", installerInfo2.getCode());
		assertEquals("My name", installerInfo2.getName());
		assertEquals("My description", installerInfo2.getDescription());
		assertEquals("1.0.0-4", installerInfo2.getVersion().toString());
		assertEquals(4, installerInfo2.getBuild());
		assertEquals("final", installerInfo2.getState());
		assertEquals(false, installerInfo2.isOfficial());
	}
}
