/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl;

import junit.framework.TestCase;

import org.gvsig.installer.lib.api.Version;

public class VersionTest extends TestCase {

	public void testParseVersion() {
		Version v = new DefaultVersion();

		v.parse("2.3.20-SNAPSHOT-10");
		assertEquals("0002.0003.0020-SNAPSHOT-0010", v.fullFormat());

		v.parse("2.3.20-SNAPSHOT");
		assertEquals("0002.0003.0020-SNAPSHOT-0000", v.fullFormat());

		v.parse("2.3.20-10");
		assertEquals("0002.0003.0020-ZZZZ-0010", v.fullFormat());

		v.parse("2.3.20");
		assertEquals("0002.0003.0020-ZZZZ-0000", v.fullFormat());

		v.parse("2.3");
		assertEquals("0002.0003.0000-ZZZZ-0000", v.fullFormat());

		v.parse("2");
		assertEquals("0002.0000.0000-ZZZZ-0000", v.fullFormat());
	}

	public void testCompareVersion() {
		Version v1 = new DefaultVersion();
		Version v2 = new DefaultVersion();

		v1.parse("2.3.20-SNAPSHOT-10");
		v2.parse("2.3");
		boolean b = v2.check(">=", v1);
		assert (b);

	}
}
