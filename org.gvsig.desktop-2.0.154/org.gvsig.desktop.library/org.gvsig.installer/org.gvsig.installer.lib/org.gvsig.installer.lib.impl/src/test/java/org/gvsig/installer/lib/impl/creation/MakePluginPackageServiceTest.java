/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.creation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import junit.framework.Assert;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.InstallerServiceTest;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class MakePluginPackageServiceTest extends InstallerServiceTest {

	@Override
	protected void doSetUp() throws Exception {

	}

	public void testReadPlugins() throws LocatorException,
			MakePluginPackageServiceException, InstallPackageServiceException,
			IOException {

		MakePluginPackageService makePluginPackageService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();

		Assert
				.assertEquals(3, makePluginPackageService
						.getPluginPackageCount());

		int pluginsNumber = 0;
		for (int i = 0; i < 3; i++) {
			PackageInfo packageInfo = makePluginPackageService
					.getPluginPackageInfo(i);
			if (packageInfo.getCode().equals("org.gvsig.wms")) {
				assertNotNull(packageInfo.getState());
				pluginsNumber++;
			} else if (packageInfo.getCode().equals("org.gvsig.wfs")) {
				assertEquals("RC2", packageInfo.getState());
				pluginsNumber++;
			} else if (packageInfo.getCode().equals("org.gvsig.wcs")) {
				assertEquals("RC1", packageInfo.getState());
				pluginsNumber++;
			}
		}

		Assert.assertEquals(3, pluginsNumber);
	}

	public void testMakePluginPackage() throws IOException, LocatorException,
			MakePluginPackageServiceException, InstallPackageServiceException {
		File installerFile = super.getTemporalFile();

		MakePluginPackageService makePluginPackageService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();

		PackageInfo packageInfo = makePluginPackageService
				.getPluginPackageInfo("org.gvsig.wfs");

		assertEquals("1.0.0", packageInfo.getVersion());

		// Change the version to check that the installation process works fine
		packageInfo.setVersion("1.0.1");

		// Compress the plugin and create the installer compressed file
		FileOutputStream os = new FileOutputStream(installerFile);
		makePluginPackageService.createPackageSet(packageInfo, os);

		// decompress the plugin and read the plugin information
		InstallPackageService installPackageService = InstallerLocator
				.getInstallerManager().getInstallPackageService();

		installPackageService.addBundle(installerFile);
		assertEquals(1, installPackageService.getPackageCount());
		PackageInfo newInstallerInfo = makePluginPackageService
				.getPluginPackageInfo(0);
		assertEquals("1.0.1", newInstallerInfo.getVersion());
	}

	public void testCreateInstallerWithAntFile() throws IOException,
			LocatorException, MakePluginPackageServiceException,
			InstallPackageServiceException {
		File installerFile = super.getTemporalFile();

		MakePluginPackageService makePluginPackageService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();

		PackageInfo packageInfo = makePluginPackageService
				.getPluginPackageInfo("org.gvsig.wfs");
		packageInfo
				.setAntScript(makePluginPackageService.getDefaultAntScript());

		// Compress the plugin and create the installer compressed file
		FileOutputStream os = new FileOutputStream(installerFile);
		makePluginPackageService.createPackageSet(packageInfo, os);

		// decompress the plugin and read the plugin information
		InstallPackageService installPackageService = InstallerLocator
				.getInstallerManager().getInstallPackageService();
		installPackageService.addBundle(installerFile);
		assertEquals(1, installPackageService.getPackageCount());
		PackageInfo newInstallerInfo = makePluginPackageService
				.getPluginPackageInfo(0);
		assertTrue(newInstallerInfo.getAntScript().startsWith("<project"));
	}

	public void testCreateInstallerWithFiles() throws IOException,
			LocatorException, MakePluginPackageServiceException,
			InstallPackageServiceException {
		File pluginsDirectory = super.getPluginsDirectory();
		File installerFile = super.getTemporalFile();
		File resource = new File(pluginsDirectory + File.separator
				+ "org.gvsig.wms" + File.separator + "resources2.txt");

		MakePluginPackageService makePluginPackageService = InstallerLocator
				.getInstallerManager().getMakePluginPackageService();

		PackageInfo packageInfo = makePluginPackageService
				.getPluginPackageInfo("org.gvsig.wcs");
		packageInfo.getFilesToCopy().add(resource);

		// Compress the plugin and create the installer compressed file
		FileOutputStream os = new FileOutputStream(installerFile);
		makePluginPackageService.createPackageSet(packageInfo, os);

		// decompress the plugin and read the plugin information
		InstallPackageService installPackageService = InstallerLocator
				.getInstallerManager().getInstallPackageService();
		installPackageService.addBundle(installerFile);
		assertEquals(1, installPackageService.getPackageCount());
		PackageInfo newInstallerInfo = makePluginPackageService
				.getPluginPackageInfo(0);
		assertEquals(1, newInstallerInfo.getFilesToCopy().size());
	}
}
