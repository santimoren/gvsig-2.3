/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.gvsig.installer.lib.impl.DefaultPackageInfo;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerInfoFileReaderTest extends
		AbstractLibraryAutoInitTestCase {

	@Override
	protected void doSetUp() throws Exception {
		// TODO Auto-generated method stub

	}

	public void testReadFile() throws LocatorException,
			InstallerInfoFileException, FileNotFoundException {
		FileInputStream fis = new FileInputStream(getClass().getClassLoader()
				.getResource("package.info1").getFile());

		DefaultPackageInfo installerInfo = new DefaultPackageInfo();

		InstallerInfoFileReader installerInfoFileReader = new InstallerInfoFileReader();
		installerInfoFileReader.read(installerInfo, fis);

		assertEquals(installerInfo.getCode(), "org.gvsig.myplugin");
		assertEquals(installerInfo.getName(), "myplugin");
		assertEquals(installerInfo.getDescription(), "Test");
		assertEquals(installerInfo.getVersion().toString(), "1.0.0-1");
		assertEquals(installerInfo.getBuild(), 1);
		assertEquals(installerInfo.getState(), "RC1");
		assertEquals(installerInfo.isOfficial(), true);
	}

	public void testCodeAlias() throws LocatorException,
			InstallerInfoFileException, FileNotFoundException {
		FileInputStream fis = new FileInputStream(getClass().getClassLoader()
				.getResource("package.info2").getFile());

		DefaultPackageInfo installerInfo = new DefaultPackageInfo();

		InstallerInfoFileReader installerInfoFileReader = new InstallerInfoFileReader();
		installerInfoFileReader.read(installerInfo, fis);

		assertEquals(installerInfo.hasThisCode("org.gvsig.myplugin"),true);
		assertEquals(installerInfo.hasThisCode("org.gvsig.myplugin.app.mainplugin"),true);
		assertEquals(installerInfo.getName(), "myplugin");
		assertEquals(installerInfo.getDescription(), "Test");
		assertEquals(installerInfo.getVersion().toString(), "1.0.0-1");
		assertEquals(installerInfo.getBuild(), 1);
		assertEquals(installerInfo.getState(), "RC1");
		assertEquals(installerInfo.isOfficial(), true);
	}

}
