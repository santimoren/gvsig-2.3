/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.execution;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.lib.impl.DefaultDependenciesCalculator;
import org.gvsig.installer.lib.impl.DefaultPackageInfo;
import org.gvsig.installer.lib.impl.DefaultVersion;

public class DependencyCalculatorTest extends TestCase {

	private MyInstallPackageService getInstallService() {
		MyInstallPackageService serv = new MyInstallPackageService();
		serv.addPackage("p1", "1.0.0", null);
		serv.addPackage("p1", "2.0.0", null);
		serv.addPackage("p1", "3.0.0", null);
		serv.addPackage("p2", "3.0.0", null);
		serv.addPackage("p2", "5.0.0", null);
		serv.addPackage("p2", "9.0.0", null);
		serv.addPackage("p3", "1.0.0", null);
		serv.addPackage("p4", "1.0.0", null);
		serv.addPackage("p5", "1.0.0", "required: p2 -ge 5.0.0");
		serv.addPackage("p6", "1.0.0", "required: p1 >= 1.0.0");
		serv.addPackage("p7", "1.0.0", "required: p1 -ge 2.0.0");
		serv.addPackage("p8", "1.0.0", "required: p2 >= 1, required: p6 -ge 1");
		serv.addPackage("p9", "1.0.0",
				"required: p8 >= 1.0.0, required: p7 >= 1.0.0");
		return serv;
	}

	public void testCalculate() {
		MyInstallPackageService installService = this.getInstallService();
		DefaultDependenciesCalculator calculator = new DefaultDependenciesCalculator(
				installService);

		calculator.addInstalledPackage(installService.getPackageInfo("p1",
				"1.0.0"));
		calculator.addInstalledPackage(installService.getPackageInfo("p2",
				"3.0.0"));
		calculator.addInstalledPackage(installService.getPackageInfo("p5",
				"1.0.0"));

		calculator.addPackageToInstall(installService.getPackageInfo("p7",
				"1.0.0"));
		calculator.addPackageToInstall(installService.getPackageInfo("p9",
				"1.0.0"));

		calculator.calculate();

		List<PackageInfo> requireds = calculator.getRequiredPackages();

		/*
		 * required packages plugin p1 3.0.0-0 devel all all j1_5 null plugin p2
		 * 9.0.0-0 devel all all j1_5 null plugin p6 1.0.0-0 devel all all j1_5
		 * required: p1 >= 1.0.0-0 plugin p8 1.0.0-0 devel all all j1_5
		 * required: p2 >= 1.0.0-0, required: p6 -ge 1.0.0-0
		 */
		PackageInfo pkg;
		assertEquals(4, requireds.size());
		pkg = requireds.get(0);
		assertEquals("p1", pkg.getCode());
		assertEquals("3.0.0-0", pkg.getVersion().toString());
		pkg = requireds.get(1);
		assertEquals("p2", pkg.getCode());
		assertEquals("9.0.0-0", pkg.getVersion().toString());
		pkg = requireds.get(2);
		assertEquals("p6", pkg.getCode());
		assertEquals("1.0.0-0", pkg.getVersion().toString());
		pkg = requireds.get(3);
		assertEquals("p8", pkg.getCode());
		assertEquals("1.0.0-0", pkg.getVersion().toString());
	}

	private class MyInstallPackageService extends InstallPackageServiceDumb {

		Map<String, PackageInfo> packages = new LinkedHashMap<String, PackageInfo>();
		List<PackageInfo> pacakgesList = null;

		MyInstallPackageService() {

		}

		public void addPackage(String code, String version, String dependencies) {
			DefaultPackageInfo pkg = new DefaultPackageInfo();
			pkg.setCode(code);
			pkg.setType("plugin");
			pkg.setVersion(new DefaultVersion().parse(version));
			pkg.setDependencies(dependencies);
			pkg.setName(code + " (name)");
			pkg.setDescription(code + " (description)");

			packages.put(pkg.getCode() + "-" + pkg.getVersion().fullFormat(),
					pkg);
		}

		public PackageInfo getPackageInfo(String code, String version) {
			Version ver = new DefaultVersion().parse(version);
			return packages.get(code + "-" + ver.fullFormat());
		}

		@Override
		public int getPackageCount() {
			if (pacakgesList == null) {
				pacakgesList = new ArrayList<PackageInfo>();
				pacakgesList.addAll(packages.values());
			}
			return pacakgesList.size();
		}

		@Override
		public PackageInfo getPackageInfo(int index) {
			if (pacakgesList == null) {
				pacakgesList = new ArrayList<PackageInfo>();
				pacakgesList.addAll(packages.values());
			}
			return pacakgesList.get(index);
		}
	}

}
