/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.execution;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.tools.service.Manager;
import org.gvsig.tools.task.SimpleTaskStatus;

public class InstallPackageServiceDumb implements InstallPackageService {

	public Manager getManager() {
		return null;
	}

	public void reset() {
	}

	public void addBundle(File bundleFile)
			throws InstallPackageServiceException {
	}

	public void addBundle(URL bundleURL) throws InstallPackageServiceException {
	}

	public void addBundlesFromDirectory(File bundlesDirectory)
			throws InstallPackageServiceException {
	}

	public void installPackage(File applicationDirectory,
			PackageInfo packageInfo) throws InstallPackageServiceException {
	}

	public void installPackage(File applicationDirectory, String packageCode)
			throws InstallPackageServiceException {
	}

	public int getPackageCount() {
		return 0;
	}

	public PackageInfo getPackageInfo(int index) {
		return null;
	}

	public PackageInfo getPackageInfo(String packageCode) {
		return null;
	}

	public void downloadPackage(PackageInfo packageInfo,
			SimpleTaskStatus taskStatus) throws InstallPackageServiceException {
	}

	public List<String> getDefaultSelectedPackagesIDs() {
		return null;
	}

	public List<String> getCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getTypes() {
		// TODO Auto-generated method stub
		return null;
	}

}
