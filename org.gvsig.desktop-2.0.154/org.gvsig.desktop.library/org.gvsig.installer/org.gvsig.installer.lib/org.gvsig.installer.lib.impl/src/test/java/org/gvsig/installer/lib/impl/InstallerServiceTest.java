/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public abstract class InstallerServiceTest extends
		AbstractLibraryAutoInitTestCase {

	public File getApplicationDirectory() throws IOException {
		File templateFile = new File(getClass().getClassLoader().getResource(
				"application").getFile());
		File applicationDirectory = new File(System
				.getProperty("java.io.tmpdir")
				+ File.separator + "tmp_gvsig_installer");

		if (applicationDirectory.exists()) {
			deleteDir(applicationDirectory);
		}
		copy(templateFile, applicationDirectory);

		return applicationDirectory;
	}

	public File getTemporalFile() {
		return new File(System.getProperty("java.io.tmpdir") + File.separator
				+ "installer" + Math.random());
	}

	public File getInstallersDirectory() throws IOException {
		File applicationDirectory = getApplicationDirectory();
		return new File(applicationDirectory + File.separator + "install");
	}

	public File getPluginsDirectory() throws IOException {
		File applicationDirectory = getApplicationDirectory();
		return new File(applicationDirectory + File.separator + "gvSIG"
				+ File.separator + "extensiones");
	}

	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public void copy(File sourceLocation, File targetLocation)
			throws IOException {
		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copy(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {
			targetLocation.getParentFile().mkdirs();

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	public void writeToFile(InputStream is, File file) {
		try {
			DataOutputStream out = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(file)));
			InputStream is2 = is;
			boolean again = true;
			while (again) {
				if (is2.read() > -1) {
					out.writeByte(is.read());
				} else {
					again = false;
				}
			}
			is.close();
			out.close();
		} catch (IOException e) {
			System.err.println("Error Writing/Reading Streams.");
		}
	}

}
