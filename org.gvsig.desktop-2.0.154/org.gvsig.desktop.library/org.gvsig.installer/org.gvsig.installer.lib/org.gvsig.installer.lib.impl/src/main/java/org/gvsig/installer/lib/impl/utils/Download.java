/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatusManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class Download extends AbstractMonitorableTask {

	private static final Logger LOG = LoggerFactory.getLogger(Download.class);
	
	/**
	 * If file size provided by URLConnection is smaller than this,
	 * then the downloaded file size will always be accepted (unit: bytes)
	 */
    private static final int FILE_SIZE_ESTIMATED_MINIMUM = 10000;
    
    /**
     * Downloaded file size will be accepted if it is bigger than this ratio
     * multiplied by the estimated file size provided by URLConnection
     * (unit: bytes)
     */
    private static final double FILE_SIZE_ESTIMATED_RATIO = 0.85;
    
	private boolean selfCreatedTaskStatus = true;

	/**
	 * @param taskName
	 */
	public Download(SimpleTaskStatus taskStatus) {
		this();
		
		/*
		 * constructor without params is adding
		 * itself to manager, so we remove it
		 */
        TaskStatusManager manager = ToolsLocator.getTaskStatusManager();
        manager.remove(getTaskStatus());
        
        /*
         * The received taskstatus is being managed by somebody else,
         * we don not add it to the manager
         */
		this.taskStatus = taskStatus;
		selfCreatedTaskStatus = false;
	}

	public Download() {
	    /**
	     * this call is adding the task status to the manager
	     */
		super("Downloading...");
	}

	public File downloadFile(URL url, String defaultFileName)
			throws IOException {

		URL downloadURL = url;

		// check if the URL ends with '/' and append the file name
		if (defaultFileName != null) {
			String urlStr = url.toString();
			if (urlStr.endsWith("/")) {
				urlStr = urlStr.concat(defaultFileName);
				downloadURL = new URL(urlStr);
			}
		}

		URLConnection connection = downloadURL.openConnection();
		connection.setUseCaches(false);
		connection.setConnectTimeout(30000);
		connection.setReadTimeout(20000);
		
		String fileName = getFileName(connection);

		// if (LOG.isDebugEnabled()) {
			Date date = new Date(connection.getLastModified());
			LOG
					.info(
							"Downloading file {} from URL {}, with last modified date: {}",
							new Object[] { fileName, downloadURL, date });
		// }

		String fileNamePrefix = fileName;
		String fileNameSuffix = "zip";
		int dotPosition = fileName.lastIndexOf('.');
		if ((dotPosition > -1) && (dotPosition < fileName.length() - 1)) {
			fileNamePrefix = fileName.substring(0, dotPosition);
			fileNameSuffix = fileName.substring(dotPosition);
		}

		BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());

		File localFile = File.createTempFile(fileNamePrefix, fileNameSuffix);

		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(localFile));

		int expected_size = connection.getContentLength();
		this.taskStatus.setRangeOfValues(0, expected_size);
		

			byte[] data = new byte[1024];
			int count = 0;
			long totalCount = 0; // total bytes read
			while ((count = bis.read(data, 0, 1024)) >= 0) {
			    
				bos.write(data, 0, count);
				totalCount += count;
				
				this.taskStatus.setCurValue(totalCount);

				if (this.taskStatus.isCancellationRequested()) {
					break;
				}
			}
			
			try {
		         bis.close();
		         bos.flush();
		         bos.close();
			} catch (Exception ex) {
			    LOG.info("Error while closing download streams: " + ex.getMessage());
			}
			
			if (selfCreatedTaskStatus) {
				this.taskStatus.terminate();
				this.taskStatus.remove();
			}
			
			// out from the read loop
			// perhaps it was cancelled:
            if (this.taskStatus.isCancellationRequested()) {
                return null;
            }
            
            String md5_ = MD5BinaryFileUtils.getMD5InRemoteFile(downloadURL);
            
            if (md5_ != null) {
                // ==========================================
                // check md5
                String local_md5 = null;
                try {
                    local_md5 = MD5BinaryFileUtils.getMD5Checksum(localFile);
                } catch (Exception e) {
                    throw new IOException("Unable to get MD5 for file: " + downloadURL.toString());
                }
                if (local_md5.compareTo(md5_) != 0) {
                    throw new IOException("MD5 does not match for file: " + downloadURL.toString());
                }
                // ==========================================
            } else {
                // ==========================================
                // check real size and expected size:
                if (!acceptableFileSize(localFile, expected_size)) {
                    throw new IOException("Bad download file size ("
                        + localFile.length()
                        + " / "
                        + expected_size
                        + ")");
                }
                // ==========================================
            }
			
            // everything seems to be OK
			return localFile;

	}

	/**
	 * Returns the file name associated to an url connection.<br />
	 * The result is not a path but just a file name.
	 * 
	 * @param urlConnection
	 *            - the url connection
	 * @return the file name
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private String getFileName(URLConnection urlConnection) throws IOException {
		String fileName = null;

		String contentDisposition = urlConnection
				.getHeaderField("content-disposition");

		if (contentDisposition != null) {
			fileName = extractFileNameFromContentDisposition(contentDisposition);
		}

		// if the file name cannot be extracted from the content-disposition
		// header, using the url.getFilename() method
		if (fileName == null) {
			StringTokenizer st = new StringTokenizer(urlConnection.getURL()
					.getFile(), "/");
			while (st.hasMoreTokens()) {
				fileName = st.nextToken();
			}
		}

		return fileName;
	}

	/**
	 * Extract the file name from the content disposition header.
	 * <p>
	 * See <a
	 * href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html">http:
	 * //www.w3.org/Protocols/rfc2616/rfc2616-sec19.html</a> for detailled
	 * information regarding the headers in HTML.
	 * 
	 * @param contentDisposition
	 *            - the content-disposition header. Cannot be <code>null>/code>.
	 * @return the file name, or <code>null</code> if the content-disposition
	 *         header does not contain the filename attribute.
	 */
	private String extractFileNameFromContentDisposition(
			String contentDisposition) {
		String[] attributes = contentDisposition.split(";");

		for (String a : attributes) {
			if (a.toLowerCase().contains("filename")) {
				// The attribute is the file name. The filename is between
				// quotes.
				return a.substring(a.indexOf('\"') + 1, a.lastIndexOf('\"'));
			}
		}

		// not found
		return null;

	}
	
	/**
	 * {@link URLConnection}'s method getContentLength() does not give
	 * exact size.
	 * 
	 * @return whether the size is acceptable
	 */
	public static boolean acceptableFileSize(File f, int estimated_size) {
	    
	    if (f == null) {
	        return false;
	    }
	    
	    if (estimated_size == -1) {
	        // -1 means unknown
	        return true;
	    }
	    
	    if (estimated_size < FILE_SIZE_ESTIMATED_MINIMUM) {
	        return true;
	    }
	    
	    return f.length() > (FILE_SIZE_ESTIMATED_RATIO * estimated_size);
	}
}
