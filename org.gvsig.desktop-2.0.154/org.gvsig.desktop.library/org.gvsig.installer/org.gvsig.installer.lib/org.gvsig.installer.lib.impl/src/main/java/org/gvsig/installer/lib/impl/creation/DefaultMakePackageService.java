/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.creation;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePackageService;
import org.gvsig.installer.lib.api.creation.MakePackageServiceException;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.tools.service.Manager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultMakePackageService implements MakePackageService {

	private static final Logger LOG = LoggerFactory
			.getLogger(DefaultMakePackageService.class);

	public static final String COPIED_FILES_DIRECTORY_NAME = "files";

	private final InstallerManager manager;
	protected File packageFolder;

	protected String antScript = null;
	private InstallPackageProviderServices installerProviderServices = null;

	private PackageInfo packageInfo;

	public DefaultMakePackageService(InstallerManager manager,
			File packageFolder, PackageInfo packageInfo) {
		super();
		this.manager = manager;
		this.packageFolder = packageFolder;
		this.packageInfo = packageInfo;
		installerProviderServices = InstallerProviderLocator
				.getProviderManager().createInstallerProviderServices();
	}

	public Manager getManager() {
		return this.manager;
	}

	public void preparePackage() throws MakePackageServiceException {

		LOG.debug("Preparing a package set of the package info: \n{0}",
				packageInfo);

		if (packageInfo.getAntScript() != null) {
			// Create the ant file
			writeAntFile(packageInfo);
		}
	}

	public void createPackageSet(OutputStream packageStream)
			throws MakePackageServiceException {

		LOG.debug("Creating a package set of the package info: \n{0}",
				packageInfo);

		installerProviderServices.writePackageInfo(this.packageFolder,
				this.packageInfo);
		String pluginFileName = manager.getPackageFileName(packageInfo);
		installerProviderServices.compressPackageSet(this.packageFolder,
				pluginFileName, packageStream);
	}

	public void createPackageIndexSet(URL downloadurl,
			OutputStream packageStream) throws MakePackageServiceException {

		LOG.debug("Creating a package set of the package info: \n{0}",
				packageInfo);

		try {
			PackageInfo infoIndex = (PackageInfo) packageInfo.clone();
			infoIndex.setDownloadURL(downloadurl);
			installerProviderServices.writePackageInfo(this.packageFolder,
					this.packageInfo);

			String pluginFileName = manager.getPackageFileName(packageInfo);
			installerProviderServices.compressPackageSet(this.packageFolder,
					pluginFileName, packageStream);
		} catch (Exception e) {
			throw new MakePackageServiceException(e);
		}
	}

	public void createPackage(OutputStream packageStream)
			throws MakePackageServiceException {
		LOG.debug("Creating package of the package info: \n{0}", packageInfo);

		installerProviderServices.writePackageInfo(this.packageFolder,
				this.packageInfo);
		installerProviderServices.compressPackage(this.packageFolder,
				packageStream);
	}

	public void createPackageIndex(URL downloadurl, OutputStream packageStream)
			throws MakePackageServiceException {
		LOG.debug("Creating package index of the package info: \n{0}",
				packageInfo);

		try {
			PackageInfo infoIndex = (PackageInfo) packageInfo.clone();
			infoIndex.setDownloadURL(downloadurl);
			installerProviderServices.writePackageInfo(this.packageFolder,
					infoIndex);
			installerProviderServices.compressPackageIndex(this.packageFolder,
					packageStream);
		} catch (Exception e) {
			throw new MakePackageServiceException(e);
		}
	}

	protected void createInstallFolderInPackageFolder() {
		File installdir = new File(this.packageFolder, "install");
		installdir.mkdir();
	}

	private void writeAntFile(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		try {
			createInstallFolderInPackageFolder();
			ByteArrayInputStream in = new ByteArrayInputStream(packageInfo
					.getAntScript().getBytes());
			OutputStream out = new FileOutputStream(getAntFile());

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Exception writing the ant file");
		}

	}

	private File getAntFile() {
		return new File(this.packageFolder.getAbsolutePath() + File.separator
				+ "install" + File.separator + ANT_FILE_NAME);
	}

	public String getDefaultAntScript()
			throws MakePluginPackageServiceException {
		try {
			URL resource = getClass().getResource(ANT_FILE_NAME);
			return readUrlAsString(resource);
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Impossible to read the default ant file", e);
		}
	}

	private String readUrlAsString(URL url) throws java.io.IOException {
		URLConnection urlConnection = url.openConnection();
		InputStream inputStream = urlConnection.getInputStream();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		StringBuffer fileData = new StringBuffer(1000);
		String line = bufferedReader.readLine();
		while (line != null) {
			fileData.append(line + "\n");
			line = bufferedReader.readLine();
		}
		bufferedReader.close();
		return fileData.toString();
	}

}
