/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.PackageInfoWriter;
import org.gvsig.installer.lib.impl.utils.SignUtil;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerInfoFileWriter implements PackageInfoWriter {

	/**
	 * Writes the install.info file
	 * 
	 * @param fileName
	 *            The file name to write the installation information
	 * @param installInfo
	 *            The installation information
	 * @throws InstallerInfoFileException
	 */
	public void write(PackageInfo installInfo, String fileName)
			throws InstallerInfoFileException {
		write(installInfo, new File(fileName));
	}

	/**
	 * Writes the install.info file
	 * 
	 * @param file
	 *            The file to write the installation information
	 * @param installInfo
	 *            The installation information
	 * @throws InstallerInfoFileException
	 */
	public void write(PackageInfo installInfo, File file)
			throws InstallerInfoFileException {
		try {
			write(installInfo, new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			throw new InstallerInfoFileException("install_infofile_not_found",
					e);
		}
		SignUtil signutil = new SignUtil();
		if( signutil.canSign() ) {
			signutil.sign(file);
		}
	}

    /**
     * Writes the install.info file
     * 
     * @param os
     *            The file to write the installation information
     * @param installInfo
     *            The installation information
     * @throws InstallerInfoFileException
     */
     public void write(PackageInfo installInfo, OutputStream os)
        throws InstallerInfoFileException {
        try {
            // Fill the properties
            Properties properties = new Properties();
            properties.setProperty(InstallerInfoTags.CODE, toStr(installInfo.getCode()));
            properties.setProperty(InstallerInfoTags.NAME, toStr(installInfo.getName()));
            properties.setProperty(InstallerInfoTags.DESCRIPTION, toStr(installInfo.getDescription()));
            properties.setProperty(InstallerInfoTags.VERSION, installInfo.getVersion().toString());
            properties.setProperty(InstallerInfoTags.BUILD, Integer.toString(installInfo.getBuild()));
            properties.setProperty(InstallerInfoTags.STATE, toStr(installInfo.getState()));
            properties.setProperty(InstallerInfoTags.OFFICIAL, Boolean.toString(installInfo.isOfficial()));
            properties.setProperty(InstallerInfoTags.TYPE, toStr(installInfo.getType()));
            properties.setProperty(InstallerInfoTags.OS, toStr(installInfo.getOperatingSystem()));
            properties.setProperty(InstallerInfoTags.ARCHITECTURE, toStr(installInfo.getArchitecture()));
            properties.setProperty(InstallerInfoTags.JVM, toStr(installInfo.getJavaVM()));
            properties.setProperty(InstallerInfoTags.GVSIG_VERSION, toStr(installInfo.getGvSIGVersion()));
            properties.setProperty(InstallerInfoTags.OWNER, toStr(installInfo.getOwner()));
            properties.setProperty(InstallerInfoTags.CATEGORIES, toStr(installInfo.getCategoriesAsString()));
            properties.setProperty(InstallerInfoTags.DEPENDENCIES, toStr(installInfo.getDependencies()));
            properties.setProperty(InstallerInfoTags.SOURCES_URL, toStr(installInfo.getSourcesURL()));
            properties.setProperty(InstallerInfoTags.WEB_URL, toStr(installInfo.getWebURL()));
            properties.setProperty(InstallerInfoTags.OWNER_URL, toStr(installInfo.getOwnerURL()));
            properties.setProperty(InstallerInfoTags.MODEL_VERSION, toStr(installInfo.getModelVersion()));
            if( !StringUtils.isBlank(installInfo.getDownloadURLAsString()) ) {
                properties.setProperty(InstallerInfoTags.DOWNLOAD_URL, toStr(installInfo.getDownloadURLAsString()));
            }
            
            properties.store(os, "");
            os.close();
        } catch (IOException e) {
            throw new InstallerInfoFileException(
                    "install_infofile_writing_error", e);
        }
    }
           
    private String toStr(Object s) {
       return s == null ? "" : s.toString();
   }
}
