/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class Compress {

	// private static final int BUFFER = 2048;

	public void compressPluginAsPackageSet(File file, String fileName,
			OutputStream os) throws MakePluginPackageServiceException {
		List<File> files = new ArrayList<File>();
		List<String> fileNames = new ArrayList<String>();
		files.add(file);
		fileNames.add(fileName);
		compressPluginsAsPackageSet(files, fileNames, os);
	}

	public void compressPluginsAsPackageSet(File directory, OutputStream os)
			throws MakePluginPackageServiceException {
		File[] files = directory.listFiles();
		List<File> filesArray = new ArrayList<File>();
		List<String> fileNamesArray = new ArrayList<String>();
		for (int i = 0; i < files.length; i++) {
			filesArray.add(files[i]);
			fileNamesArray.add(files[i].getName());
		}
		compressPluginsAsPackageSet(filesArray, fileNamesArray, os);
	}

	public void compressPluginsAsPackageSet(List<File> files,
			List<String> fileNames, OutputStream os)
			throws MakePluginPackageServiceException {
		try {
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(
					os));

			for (int i = 0; i < files.size(); i++) {
				ZipEntry zipEntry = new ZipEntry(fileNames.get(i));
				zos.putNextEntry(zipEntry);
				compressPluginFiles(files.get(i), zos);
				zos.closeEntry();
			}
			zos.close();
		} catch (Exception e) {
			throw new MakePluginPackageServiceException(
					"Error compressing as package set the plugin files: "
							+ files, e);
		}
	}

	public void compressPluginAsPackage(File folder, OutputStream os)
			throws MakePluginPackageServiceException {
		ZipOutputStream zos = new ZipOutputStream(os);
		try {
			int parentFileLenght = folder.getParentFile().toString().length();
			compressPluginFile(folder, parentFileLenght, zos);
			zos.flush();
			zos.close();
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Error compressing as package the plugin folder: " + folder,
					e);
		}
	}

	public void compressPluginAsPackageIndex(File folder, OutputStream os)
			throws MakePluginPackageServiceException {
		ZipOutputStream zos = new ZipOutputStream(os);
		try {
			int parentFileLength = folder.getParentFile().toString().length();
			String folderName = folder.toString().substring(
					parentFileLength + 1, folder.toString().length());

			File infoFile = new File(folder, getPackageInfoFileName()
					+ ".index");
			if (!infoFile.exists()) {
				infoFile = new File(folder, getPackageInfoFileName());
			}

			byte[] buf = new byte[1024];
			int len;

			// No usar File.separator hace cosas raras en G�indous
			ZipEntry zipEntry = new ZipEntry(folderName + "/" + getPackageInfoFileName());
			zos.putNextEntry(zipEntry);

			FileInputStream fin = new FileInputStream(infoFile);
			BufferedInputStream in = new BufferedInputStream(fin);
			while ((len = in.read(buf)) >= 0) {
				zos.write(buf, 0, len);
			}
			in.close();
			zos.closeEntry();
			zos.flush();
			zos.close();
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Error compressing as package index the plugin folder: "
							+ folder, e);
		}
	}

	private void compressPluginFiles(File fileOrFolder, ZipOutputStream zos)
			throws IOException {
		int parentFileLenght = fileOrFolder.getParentFile().toString().length();
		ZipOutputStream zosPlugin = new ZipOutputStream(zos);
		compressPluginFile(fileOrFolder, parentFileLenght, zosPlugin);
		zosPlugin.finish();
	}

	private void compressPluginFile(File file, int parenFileLength,
			ZipOutputStream zosPlugin) throws IOException {
		String fileName = file.toString().substring(parenFileLength,
				file.toString().length());
		if (File.separatorChar != '/') {
			fileName = fileName.replace(File.separatorChar, '/');
		}

		if (file.isDirectory()) {
			// Remove the subversion folders
			if (!file.getName().toUpperCase().equals(".SVN")) {
				// Adding the files
				String[] fileNames = file.list();
				if (fileNames != null) {
					for (int i = 0; i < fileNames.length; i++) {
						compressPluginFile(new File(file, fileNames[i]),
								parenFileLength, zosPlugin);
					}
				}
			}
		} else {
			byte[] buf = new byte[1024];
			int len;

			ZipEntry zipEntry = new ZipEntry(fileName);
			zosPlugin.putNextEntry(zipEntry);

			FileInputStream fin = new FileInputStream(file);
			BufferedInputStream in = new BufferedInputStream(fin);
			while ((len = in.read(buf)) >= 0) {
				zosPlugin.write(buf, 0, len);
			}
			in.close();
			zosPlugin.closeEntry();
		}
	}

	private String getPackageInfoFileName() {
		return InstallerProviderLocator.getProviderManager()
				.getPackageInfoFileName();
	}
}
