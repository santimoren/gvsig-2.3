/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author jldominguez
 *
 */
public class MD5BinaryFileUtils {
    
    private static final Logger LOG = LoggerFactory.getLogger(MD5BinaryFileUtils.class);
    
    /**
     * 
     * Gets the MD5 string of a md5 file which is associated with
     * the file provided.
     * 
     * @param remote_file the file that has a ".md5" file next to it
     * @return
     */
    public static String getMD5InRemoteFile(URL remote_file) {

        BufferedReader in = null;
        InputStreamReader istr = null;
        try {
            String _url = remote_file.toString() + ".md5";
            URL md5_url = new URL(_url);
            
            URLConnection connection = md5_url.openConnection();
            connection.setUseCaches(false);
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(20000);
            
            istr = new InputStreamReader(connection.getInputStream());
            in = new BufferedReader(istr);
            String inputLine = in.readLine();
            
            int space = inputLine.indexOf(" "); 
            inputLine = inputLine.substring(0, space);
            return inputLine;
        } catch (Exception ex) {
            LOG.info("Error while reading MD5 file: " + ex.getMessage());
            return null;
        } finally {
            try {
                in.close();
                istr.close();
            } catch (Exception ex) { }  
        }
        
    }
    
    public static byte[] createChecksum(File the_file) throws Exception {
        
        InputStream fis =  new FileInputStream(the_file);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    /**
     * In case this is too slow for you:
     * There are faster ways to convert
     * a byte array to a HEX string
     *  
     * @param the_file the input file
     * @return the given file's MD5 checksum as a hex string
     * @throws Exception
     */
    public static String getMD5Checksum(File the_file) throws Exception {
        byte[] b = createChecksum(the_file);
        String result = "";

        for (int i=0; i < b.length; i++) {
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }

}
