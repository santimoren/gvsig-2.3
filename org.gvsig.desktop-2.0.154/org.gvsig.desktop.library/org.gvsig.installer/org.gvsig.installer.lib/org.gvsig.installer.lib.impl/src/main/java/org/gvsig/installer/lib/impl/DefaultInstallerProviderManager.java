/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.installer.lib.impl.execution.NotInstallerExecutionProviderException;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerProviderManager;
import org.gvsig.installer.lib.spi.execution.InstallPackageProvider;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.AbstractProviderManager;
import org.gvsig.tools.service.spi.NotRegisteredException;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderFactory;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultInstallerProviderManager extends AbstractProviderManager
		implements InstallerProviderManager {

	public static final String PROVIDERS_NAMESPACE = "Installer.providers";
	public static final String PROVIDERS_NAME = "Installer.providers";
	public static final String PROVIDERS_DESCRIPTION = "Installer providers";

	private static Logger logger = LoggerFactory
			.getLogger(DefaultInstallerProviderManager.class);

	private static final String INSTALL_INFO_FILE = "package.info";

	public DefaultInstallerProviderManager() {
		super();
	}

	@Override
	protected String getRegistryDescription() {
		return PROVIDERS_DESCRIPTION;
	}

	@Override
	protected String getRegistryKey() {
		return PROVIDERS_NAME;
	}

	public ProviderServices createProviderServices(Service service) {
		return new DefaultInstallerProviderServices();
	}

	public InstallPackageProvider createExecutionProvider(String providerName)
			throws ServiceException {
		DynObjectManager dynObjectManager = ToolsLocator.getDynObjectManager();
		DynClass dynClass = dynObjectManager.get(providerName);
		if (dynClass == null) {
			throw new NotRegisteredException(providerName);
		}
		DynObject serviceParameters = dynObjectManager
				.createDynObject(dynClass);
		Provider provider = createProvider(serviceParameters,
				new DefaultInstallerProviderServices());
		if (!(provider instanceof InstallPackageProvider)) {
			throw new NotInstallerExecutionProviderException(providerName);
		}
		return (InstallPackageProvider) provider;
	}

	public InstallPackageProviderServices createInstallerProviderServices() {
		return new DefaultInstallerProviderServices();
	}

	public String getPackageInfoFileName() {
		return INSTALL_INFO_FILE;
	}

	@SuppressWarnings("unchecked")
	public List<ProviderFactory> getProviderFactories() {
		ExtensionPointManager epm = ToolsLocator.getExtensionPointManager();
		ExtensionPoint ep = epm.get(this.getRegistryKey());
		List<String> names = ep.getNames();
		List<ProviderFactory> factories = new ArrayList<ProviderFactory>();

		for (int i = 0; i < names.size(); i++) {
			try {
				factories.add((ProviderFactory) ep.create(names.get(i)));
			} catch (InstantiationException e) {
				logger.info("Cannot retreive the factory provider of "
						+ names.get(i), e);
			} catch (IllegalAccessException e) {
				logger.info("Cannot retreive the factory provider of "
						+ names.get(i), e);
			}
		}

		return factories;
	}
}
