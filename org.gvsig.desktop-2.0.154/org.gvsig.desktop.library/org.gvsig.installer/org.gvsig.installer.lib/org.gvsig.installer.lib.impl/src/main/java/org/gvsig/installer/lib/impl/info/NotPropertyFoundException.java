/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

import java.util.HashMap;
import java.util.Map;

import org.gvsig.installer.lib.spi.InstallerInfoFileException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class NotPropertyFoundException extends InstallerInfoFileException {

	private static final long serialVersionUID = -3465150550431966595L;

	private String propertyName = null;

	private static final String MESSAGE_KEY = "install_infofile_property_not_found";
	private static final String FORMAT_STRING = "The property %(propertyName) is mandatory but it is not in the install.info file.";

	public NotPropertyFoundException(String propertyName) {
		super(FORMAT_STRING, MESSAGE_KEY, serialVersionUID);
		this.propertyName = propertyName;
	}

	@Override
	protected Map<String, String> values() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("propertyName", String.valueOf(propertyName));
		return map;
	}
}
