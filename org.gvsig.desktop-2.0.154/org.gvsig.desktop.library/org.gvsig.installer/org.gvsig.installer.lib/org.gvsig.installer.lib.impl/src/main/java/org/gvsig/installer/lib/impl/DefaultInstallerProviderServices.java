/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.info.InstallerInfoFileReader;
import org.gvsig.installer.lib.impl.info.InstallerInfoFileWriter;
import org.gvsig.installer.lib.impl.utils.Compress;
import org.gvsig.installer.lib.impl.utils.Decompress;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.tools.service.spi.AbstractProviderServices;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultInstallerProviderServices extends AbstractProviderServices
		implements InstallPackageProviderServices {

	private List<String> defaultSelectedPacketsIDs = null;

	// private static final Logger LOG = LoggerFactory
	// .getLogger(DefaultInstallerProviderServices.class);

	public void decompress(InputStream is, File outputDirectory)
			throws InstallPackageServiceException {
		Decompress decompress = new Decompress();
		decompress.decompressPlugin(is, outputDirectory);
	}

	public class ReadPackageException extends InstallPackageServiceException {

		private static final long serialVersionUID = 1989187468965303199L;

		private static final String message = "Exception reading a compressed file";

		private static final String KEY = "Exception_reading_a_compressed_file";

		public ReadPackageException(InstallerInfoFileException e) {
			super(message, e, KEY, serialVersionUID);
		}

	}

	public PackageInfo readCompressedPackageInfo(InputStream is)
			throws InstallPackageServiceException {
		Decompress decompress = new Decompress();
		try {
			return decompress.readInstallerInfo(is);
		} catch (InstallerInfoFileException e) {
			throw new ReadPackageException(e);
		}
	}

	public void compressPackageSet(File folder, String fileName, OutputStream os)
			throws MakePluginPackageServiceException {
		Compress compress = new Compress();
		compress.compressPluginAsPackageSet(folder, fileName, os);
	}

	public void compressPackage(File folder, OutputStream os)
			throws MakePluginPackageServiceException {
		Compress compress = new Compress();
		compress.compressPluginAsPackage(folder, os);
	}

	public void compressPackageIndex(File folder, OutputStream os)
			throws MakePluginPackageServiceException {
		Compress compress = new Compress();
		compress.compressPluginAsPackageIndex(folder, os);
	}

	public void readPackageInfo(File directory, PackageInfo installerInfo)
			throws InstallerInfoFileException {
		File installInfoFile = new File(directory + File.separator
				+ getPackageInfoFileName());
		if (installInfoFile.exists()) {
			InstallerInfoFileReader reader = new InstallerInfoFileReader();
			reader.read(installerInfo, installInfoFile.getAbsolutePath());
		}

		if (installerInfo.getCode() == null) {
			installerInfo.setCode(directory.getName());
		}
		if (installerInfo.getName() == null) {
			installerInfo.setName(directory.getName());
		}
	}

	private String getPackageInfoFileName() {
		return InstallerProviderLocator.getProviderManager()
				.getPackageInfoFileName();
	}

	public void writePackageInfo(File directory, PackageInfo packageInfo)
			throws InstallerInfoFileException {
		writePackageInfoFile(directory, getPackageInfoFileName(), packageInfo);
	}

	public void writePackageInfoForIndex(File directory, PackageInfo packageInfo)
			throws InstallerInfoFileException {
		writePackageInfoFile(directory, getPackageInfoFileName() + ".index",
				packageInfo);
	}

	private void writePackageInfoFile(File directory, String fileName,
			PackageInfo installerInfo) throws InstallerInfoFileException {
		if (!directory.exists()) {
			// throw new
			// InstallerInfoFileException("The directory doesn't exist");
			directory.mkdir();
		}
		InstallerInfoFileWriter installerInfoFileWriter = new InstallerInfoFileWriter();
		installerInfoFileWriter.write(installerInfo, directory + File.separator
				+ fileName);
	}

	public InputStream searchPackage(InputStream is, String zipEntry)
			throws InstallPackageServiceException {
		Decompress decompress = new Decompress();
		return decompress.searchPlugin(is, zipEntry);
	}

	public void readPackageSetInfo(InputStream is,
			List<PackageInfo> installerInfos,
			Map<PackageInfo, String> zipEntriesMap)
			throws InstallPackageServiceException {
		Decompress decompress = new Decompress();
		decompress
				.readPackageSetInstallInfos(is, installerInfos, zipEntriesMap);

		defaultSelectedPacketsIDs = decompress.getDefaultSelectedPackages();
	}

	public void readPackageInfo(InputStream is,
			List<PackageInfo> installerInfos,
			Map<PackageInfo, String> zipEntriesMap, String name)
			throws InstallPackageServiceException {

		Decompress decompress = new Decompress();
		decompress.readPackageInstallInfo(is, installerInfos, zipEntriesMap,
				name);
	}

	public List<String> getDefaultSelectedPackagesIDs() {
		return defaultSelectedPacketsIDs;
	}
}
