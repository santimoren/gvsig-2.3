/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.PackageInfoReader;
import org.gvsig.installer.lib.spi.InstallerInfoFileException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerInfoFileReader implements PackageInfoReader {

	private static final Logger LOG = LoggerFactory
			.getLogger(InstallerInfoFileReader.class);

	/**
	 * Reads and parses the install.info file and creates one object that
	 * contains the information.
	 * 
	 * @param fileName
	 *            The file name that contains the install.info information.
	 * @return An object with the information about the installation
	 * @throws InstallerInfoFileException
	 */
	public void read(PackageInfo installerInfoResource, String fileName)
			throws InstallerInfoFileException {
		read(installerInfoResource, new File(fileName));
	}

	/**
	 * Reads and parses the install.info file and creates one object that
	 * contains the information.
	 * 
	 * @param file
	 *            The file that contains the install.info information.
	 * @return An object with the information about the installation
	 * @throws InstallerInfoFileException
	 */
	public void read(PackageInfo installerInfoResource, File file)
			throws InstallerInfoFileException {
		if (!file.exists()) {
			throw new InstallerInfoFileException("install_infofile_not_found");
		}
		try {
			read(installerInfoResource, new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new InstallerInfoFileException("install_infofile_not_found",
					e);
		}
	}

	/**
	 * Reads and parses the install.info file and creates one object that
	 * contains the information.
	 * 
	 * @param is
	 *            The input stream that contains the install.info information.
	 * @return An object with the information about the installation
	 * @throws InstallerInfoFileException
	 */
	public void read(PackageInfo installerInfoResource, InputStream is)
			throws InstallerInfoFileException {
		Properties properties = new Properties();
		try {
			properties.load(is);
			is.close();
		} catch (IOException e) {
			throw new InstallerInfoFileException(
					"install_infofile_reading_error", e);
		}

		installerInfoResource.setCode(properties
				.getProperty(InstallerInfoTags.CODE));
		
		String codealias1 = properties.getProperty(InstallerInfoTags.CODEALIAS);
		if( codealias1 != null ) {
			String[] s = codealias1.split(",");
			for( int i=0; i<s.length; i++) {
				s[i] = s[i].trim();
				if(! "".equals(s[i]) ) {
					installerInfoResource.getAllCodes().add(s[i]);
				}
			}
		}
		
		String name = properties.getProperty(InstallerInfoTags.NAME);
		installerInfoResource.setName(name);
		installerInfoResource.setDescription(properties
				.getProperty(InstallerInfoTags.DESCRIPTION));
		String version = properties.getProperty(InstallerInfoTags.VERSION);
		if (version == null) {
			LOG
					.warn(
							"Got a null version string for the {} plugin "
									+ "Ignoring it and leaving the default version value",
							name);
		} else {
			installerInfoResource.setVersion(version);
		}
		installerInfoResource.setType(properties
				.getProperty(InstallerInfoTags.TYPE));
		String build = properties.getProperty(InstallerInfoTags.BUILD);
		if (build != null && installerInfoResource.getVersion().getBuild() == 0) {
			try {
				installerInfoResource.setBuild(Integer.parseInt(build));
			} catch (Exception e) {
				installerInfoResource.setBuild(0);
				LOG.info(
						"Error while converting to int the "
								+ InstallerInfoTags.BUILD + " property value: "
								+ build, e);
			}
		}

		// Look for the old build tag just in case (the new one is buildNumber).
		if (installerInfoResource.getBuild() <= 0) {
			String oldBuild = properties
					.getProperty(InstallerInfoTags.BUILD_OLD);
			if (oldBuild != null) {
				try {
					installerInfoResource.setBuild(Integer.parseInt(oldBuild));
				} catch (Exception e) {
					installerInfoResource.setBuild(0);
					LOG.info("Error while converting to int the "
							+ InstallerInfoTags.BUILD_OLD + " property value: "
							+ oldBuild, e);
				}
			}
		}
		installerInfoResource.setState(properties
				.getProperty(InstallerInfoTags.STATE));
		try {
			installerInfoResource.setOfficial(Boolean.parseBoolean(properties
					.getProperty(InstallerInfoTags.OFFICIAL)));
		} catch (Exception e) {
			installerInfoResource.setOfficial(false);
			LOG.info("Error while converting to boolean the "
					+ InstallerInfoTags.OFFICIAL + " property value: "
					+ properties.getProperty(InstallerInfoTags.OFFICIAL), e);
		}

		String os = properties.getProperty(InstallerInfoTags.OS);
		if (os != null) {
			installerInfoResource.setOperatingSystem(os);
		}

		String arch = properties.getProperty(InstallerInfoTags.ARCHITECTURE);
		if (arch != null) {
			installerInfoResource.setArchitecture(arch);
		}

		String jvm = properties.getProperty(InstallerInfoTags.JVM);
		if (jvm != null) {
			installerInfoResource.setJavaVM(jvm);
		}

		installerInfoResource.setGvSIGVersion(properties
				.getProperty(InstallerInfoTags.GVSIG_VERSION));

		String urlStr = properties.getProperty(InstallerInfoTags.DOWNLOAD_URL);
		if (urlStr != null) {
			installerInfoResource.setDownloadURL(urlStr);
		}

		String modelVersion = properties
				.getProperty(InstallerInfoTags.MODEL_VERSION);
		if (modelVersion != null) {
			installerInfoResource.setModelVersion(modelVersion);
		}

		String owner = properties.getProperty(InstallerInfoTags.OWNER);
		if (owner != null) {
			installerInfoResource.setOwner(owner);
		}

		String dependencies = properties
				.getProperty(InstallerInfoTags.DEPENDENCIES);
		if (dependencies != null) {
			installerInfoResource.setDependencies(dependencies);
		}

		String sourcesUrlStr = properties
				.getProperty(InstallerInfoTags.SOURCES_URL);
		if (sourcesUrlStr != null && !sourcesUrlStr.equals("")) {
			URL sourcesURL;
			try {
				if( sourcesUrlStr.toLowerCase().startsWith("scm:svn:http") ) {
					sourcesUrlStr = sourcesUrlStr.substring(8);
				}
				sourcesURL = new URL(sourcesUrlStr);
			} catch (MalformedURLException e) {
				throw new InstallerInfoFileException(
						"Error getting the value of the sources url property as URL: "
								+ sourcesUrlStr, e);
			}
			installerInfoResource.setSourcesURL(sourcesURL);
		} else {
			installerInfoResource.setSourcesURL(null);
		}

		String webUrlStr = properties.getProperty(InstallerInfoTags.WEB_URL);
		if (webUrlStr != null && !webUrlStr.equals("")) {
			URL webUrl;
			try {
				webUrl = new URL(webUrlStr);
			} catch (MalformedURLException e) {
				throw new InstallerInfoFileException(
						"Error getting the value of the web url property as URL: "
								+ webUrlStr, e);
			}
			installerInfoResource.setWebURL(webUrl);
		} else {
			installerInfoResource.setWebURL(null);
		}

		String ownerURLStr = properties
				.getProperty(InstallerInfoTags.OWNER_URL);
		if (ownerURLStr != null && !ownerURLStr.equals("")) {
			URL ownerUrl;
			try {
				ownerUrl = new URL(ownerURLStr);
			} catch (MalformedURLException e) {
				throw new InstallerInfoFileException(
						"Error getting the value of the owner url property as URL: "
								+ ownerURLStr, e);
			}
			installerInfoResource.setOwnerURL(ownerUrl);
		} else {
			installerInfoResource.setOwnerURL(null);
		}

		String categories = properties
				.getProperty(InstallerInfoTags.CATEGORIES);
		if (categories != null && !categories.equals("")) {
			installerInfoResource.addCategoriesAsString(categories);
		}

	}

}
