/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl;

import java.security.InvalidParameterException;
import java.text.MessageFormat;

import org.gvsig.installer.lib.api.Version;

public class DefaultVersion implements Version {

	private int mayor = 0;
	private int minor = 0;
	private int rev = 0;
	private String classifier = null;
	private int build = 0;

	public DefaultVersion() {
		super();
	}

	protected DefaultVersion(int mayor, int minor, int rev, String classifier,
			int build) {
		this();
		this.mayor = mayor;
		this.minor = minor;
		this.rev = rev;
		this.classifier = classifier;
		this.build = build;
	}

	public Version parse(String version) {
		// Formato: mayor.minor.rev-classifier-build

		String[] x = version.split("[.]");
		int lx = x.length;
		if (lx == 1) {
			this.mayor = parseIntClassifierAndBuild(x[0], version);
			this.minor = 0;
			this.rev = 0;
			this.classifier = null;
			this.build = 0;
		} else if (lx == 2) {
			this.mayor = Integer.parseInt(x[0]);
			this.minor = parseIntClassifierAndBuild(x[1], version);
			this.rev = 0;
			this.classifier = null;
			this.build = 0;
		} else if (lx == 3) {
			this.mayor = Integer.parseInt(x[0]);
			this.minor = Integer.parseInt(x[1]);
			this.rev = parseIntClassifierAndBuild(x[2], version);
		} else {
			throw new InvalidParameterException(version);
		}
		return this;
	}

	private int parseIntClassifierAndBuild(String s, String fullversion) {
		int value;

		String[] y = s.split("[-]");
		int ly = y.length;
		if (ly == 1) {
			value = Integer.parseInt(y[0]);
			this.classifier = null;
			this.build = 0;
		} else if (ly == 2) {
			value = Integer.parseInt(y[0]);
			try {
				this.build = Integer.parseInt(y[1]);
				this.classifier = null;
			} catch (NumberFormatException e) {
				this.build = 0;
				this.classifier = y[1];
			}
		} else if (ly == 3) {
			value = Integer.parseInt(y[0]);
			this.classifier = y[1];
			this.build = Integer.parseInt(y[2]);
		} else {
			throw new InvalidParameterException(fullversion);
		}
		return value;
	}

	public int getMayor() {
		return this.mayor;
	}


	public int getMajor() {
	    return this.mayor;
	}

	public int getMinor() {
		return this.minor;
	}

	public int getRevision() {
		return this.rev;
	}

	public String getClassifier() {
		return this.classifier;
	}

	public int getBuild() {
		return this.build;
	}

	public boolean check(String op, Version other) {
		if ("=".equals(op) || "==".equals(op) || "-eq".equals(op)) {
			return this.fullFormat().compareTo(other.fullFormat()) == 0;
		}
		if (">".equals(op) || "-gt".equals(op)) {
			return this.fullFormat().compareTo(other.fullFormat()) > 0;
		}
		if (">=".equals(op) || "-ge".equals(op)) {
			return this.fullFormat().compareTo(other.fullFormat()) >= 0;
		}
		if ("<".equals(op) || "-lt".equals(op)) {
			return this.fullFormat().compareTo(other.fullFormat()) < 0;
		}
		if ("<=".equals(op) || "-le".equals(op)) {
			return this.fullFormat().compareTo(other.fullFormat()) <= 0;
		}
		return false;
	}

	@Override
	public String toString() {
		if (this.classifier == null) {
			return MessageFormat.format("{0}.{1}.{2}-{3,number,####}",
					this.mayor, this.minor, this.rev, this.build);
		} else {
			return MessageFormat.format("{0}.{1}.{2}-{3}-{4,number,####}",
					this.mayor, this.minor, this.rev, this.classifier,
					this.build);
		}
	}

	public String fullFormat() {
		if (this.classifier == null) {
			// classifier ZZZZ allows compare correctly tow versions
			return MessageFormat
					.format(
							"{0,number,0000}.{1,number,0000}.{2,number,0000}-ZZZZ-{3,number,0000}",
							this.mayor, this.minor, this.rev, this.build);
		} else {
			return MessageFormat
					.format(
							"{0,number,0000}.{1,number,0000}.{2,number,0000}-{3}-{4,number,0000}",
							this.mayor, this.minor, this.rev, this.classifier,
							this.build);
		}
	}

	public int compareTo(Object arg0) {
		Version other = (Version)arg0;
		return this.fullFormat().compareTo(other.fullFormat());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		Version other = (Version) obj;
		if (this.mayor != other.getMayor()) {
			return false;
		}
		if (this.minor != other.getMinor()) {
			return false;
		}
		if (this.rev != other.getRevision()) {
			return false;
		}
		if (this.build != other.getBuild()) {
			return false;
		}
		if (this.classifier == null) {
			if (other.getClassifier() == null) {
				return true;
			} else {
				return false;
			}
		}
		if (!this.classifier.equalsIgnoreCase(other.getClassifier())) {
			return false;
		}
		return true;
	}
	
	public int hashCode() {
	    return (classifier == null ? 0 : classifier.hashCode()) +
	        (mayor<<13) + (minor<<19) + (rev<<25) + build;
	}

	public Version setBuild(int build) {
		this.build = build;
		return this;
	}
		
	public String format(String fmt) {
		String s = fmt;
		s = s.replaceAll("%M", String.valueOf(this.mayor));
		s = s.replaceAll("%m", String.valueOf(this.minor));
		s = s.replaceAll("%r", String.valueOf(this.rev));
		s = s.replaceAll("%c", this.classifier);
		s = s.replaceAll("%b", String.valueOf(this.build));
		return s;
	}
}
