/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.tools.packageutils.StringWithAlias;

public class DefaultDependencies extends ArrayList implements
		Dependencies {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6743931124069465522L;

	private Dependency createDependency() {
		// InstallerManager manager = InstallerLocator.getInstallerManager();
		// return manager.createDependency();
		return new DefaultDependency();
	}

	public Dependencies parse(String dependecies) {
		if (dependecies == null) {
			this.clear();
			return this;
		}
		dependecies = dependecies.trim();
		if (dependecies.equals("")) {
			this.clear();
			return this;
		}

		String[] x = dependecies.split(",");
		for (int i = 0; i < x.length; i++) {
			this.add(createDependency().parse(x[i]));
		}
		return this;
	}

	@Override
	public String toString() {
		StringBuffer s = null;
		Iterator<Dependency> it = this.iterator();
		while (it.hasNext()) {
			if (s == null) {
				s = new StringBuffer();
			} else {
				s.append(", ");
			}
			s.append(it.next().toString());
		}
		if (s == null) {
			return "";
		}
		return s.toString();
	}

	@Override
	public boolean contains(Object o) {
		if (!(o instanceof Dependency)) {
			return false;
		}
		Iterator<Dependency> it = this.iterator();
		while (it.hasNext()) {
			Dependency dep = it.next();
			if (dep.equals(o)) {
				return true;
			}
		}
		return false;
	}

	public boolean match(String type, String code, Version version) {
		Iterator<Dependency> it = this.iterator();
		while (it.hasNext()) {
			Dependency dependency = it.next();
			if (dependency.match(type, code, version)) {
				return true;
			}
		}
		return false;
	}

	public Dependency find(String type, String code, Version version) {
		Iterator<Dependency> it = this.iterator();
		while (it.hasNext()) {
			Dependency dependency = it.next();
			if (dependency.match(type, code, version)) {
				return dependency;
			}
		}
		return null;
	}
	
	   public List findAll(String type, String code, Version version) {
	       
	       List<Dependency> resp = null;
	        Iterator<Dependency> it = this.iterator();
	        while (it.hasNext()) {
	            Dependency dependency = it.next();
	            if (dependency.match(type, code, version)) {
	                if (resp == null) {
	                    resp = new ArrayList<Dependency>();
	                }
	                resp.add(dependency);
	            }
	        }
	        return resp;
	    }

		public boolean match(String type, StringWithAlias code, Version version) {
			Iterator<Dependency> it = this.iterator();
			while (it.hasNext()) {
				Dependency dependency = it.next();
				if (dependency.match(type, code, version)) {
					return true;
				}
			}
			return false;
		}

		public Dependency find(String type, StringWithAlias code, Version version) {
			Iterator<Dependency> it = this.iterator();
			while (it.hasNext()) {
				Dependency dependency = it.next();
				if (dependency.match(type, code, version)) {
					return dependency;
				}
			}
			return null;
		}
		
	public List findAll(String type, StringWithAlias code, Version version) {

		List<Dependency> resp = null;
		Iterator<Dependency> it = this.iterator();
		while (it.hasNext()) {
			Dependency dependency = it.next();
			if (dependency.match(type, code, version)) {
				if (resp == null) {
					resp = new ArrayList<Dependency>();
				}
				resp.add(dependency);
			}
		}
		return resp;
	}


	   
	   
}
