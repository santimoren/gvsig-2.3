/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.DependenciesCalculator;
import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.tools.packageutils.StringWithAlias;

public class DependenciesCalculatorV1 implements DependenciesCalculator {

	private static Logger logger = LoggerFactory
			.getLogger(DefaultDependenciesCalculator.class);

	private InstallPackageService installService = null;
	private List<PackageInfo> packagesInstalleds = null;
	private List<PackageInfo> packagesToInstall = null;
	private List<PackageInfo> requieredPackages = null;
	private List<PackageInfo> conflictsPackages = null;
	private Dependencies unresolvedDependencies = null;
	private Map<Dependency, Set<PackageInfo>> requiredPackagesOfDependency = null;

	public DependenciesCalculatorV1(InstallPackageService installService) {
		super();
		this.installService = installService;
		this.packagesInstalleds = new ArrayList<PackageInfo>();
		this.packagesToInstall = new ArrayList<PackageInfo>();
		this.requiredPackagesOfDependency = new HashMap<Dependency, Set<PackageInfo>>();
	}

	private boolean addDependency(Dependencies dependencies,
			Dependency dependency) {
		try {
			if (dependencies.contains(dependency)) {
				return false;
			}
			Dependency dep = (Dependency) dependency.clone();
			dependencies.add(dep);
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		return true;
	}

	private void addDependency(Dependencies dependencies,
			PackageInfo packageInfo) {

		Dependencies pkgdependencies = packageInfo.getDependencies();
		if (pkgdependencies != null) {
			Iterator<Dependency> it = pkgdependencies.iterator();
			Dependency dependency = null;
			while (it.hasNext()) {
				dependency = it.next();
				Set<PackageInfo> requirers = requiredPackagesOfDependency
						.get(dependency);
				if (requirers == null) {
					requirers = new HashSet<PackageInfo>();
					requiredPackagesOfDependency.put(dependency, requirers);
				}

				this.addDependency(dependencies, dependency);
				requirers.add(packageInfo);
			}
		}
	}

	public void addPackageToInstall(PackageInfo packageInfo) {
		this.packagesToInstall.add(packageInfo);
	}

	public void addPackageToInstall(Collection<PackageInfo> packages) {
		Iterator<PackageInfo> it = packages.iterator();
		while (it.hasNext()) {
			this.addPackageToInstall(it.next());
		}
	}

	public void addInstalledPackage(PackageInfo packageInfo) {
		this.packagesInstalleds.add(packageInfo);
	}

	public void addInstalledPackage(PackageInfo[] packages) {
		for (int i = 0; i < packages.length; i++) {
			this.addInstalledPackage(packages[i]);
		}
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public void calculate() {

		// Inicializa el conjunto de paquetes ya instalados mas los que
		// se han solicitado instalar.
		List<PackageInfo> packages = new ArrayList<PackageInfo>();
		packages.addAll(this.packagesInstalleds);
		packages.addAll(this.packagesToInstall);

		Set<PackageInfo> allPackages = new HashSet<PackageInfo>();
		allPackages.addAll(packages);
		for (int i = 0; i < this.installService.getPackageCount(); i++) {
			PackageInfo pkg = this.installService.getPackageInfo(i);
			allPackages.add(pkg);
		}
			
		List<PackageInfo> requieredPackages = null;
		conflictsPackages = new ArrayList<PackageInfo>();

		for (int retries = 0; retries < 100; retries++) {

			requieredPackages = new ArrayList<PackageInfo>();

			if( logger.isDebugEnabled() ) {
				logger.debug("Pass " + retries);
				logger.debug("All packages (installed+to-install):");
				logger.debug("\n" + dumpPackages(packages));
			}
			Dependencies dependencies = new DefaultDependencies();
			Iterator<PackageInfo> it = packages.iterator();
			while (it.hasNext()) {
				this.addDependency(dependencies, it.next());
			}
			if( logger.isDebugEnabled() ) {
				logger.debug("Dependencies:");
				logger.debug("\n"+dumpDependencies(dependencies));
			}
			unresolvedDependencies = new DefaultDependencies();
			unresolvedDependencies.addAll(dependencies);

			Iterator<PackageInfo> itAllPackages = allPackages.iterator();
			while( itAllPackages.hasNext() ) {
				PackageInfo pkg = itAllPackages.next();
				if( logger.isDebugEnabled() ) {
					logger.debug("Check " +pkg.toStringCompact());
				}
				List <Dependency> foundDependencies = null;
				foundDependencies = dependencies.findAll(Dependency.REQUIRED, pkg.getAllCodes(), pkg.getVersion());
				if (foundDependencies != null) {
					PackageInfo pkg1 = getByCodeAndVersion(packages, pkg.getAllCodes(), pkg.getVersion());
					if (pkg1 == null) {
						logger.debug("           Add required");
						requieredPackages.add(pkg);
					}
					unresolvedDependencies.removeAll(foundDependencies);
				}
				foundDependencies = dependencies.findAll(Dependency.CONFLICT, pkg.getAllCodes(), pkg.getVersion());
				if (foundDependencies != null) {
					PackageInfo pkg1 = getByCodeAndVersion(packages, pkg.getAllCodes(), pkg.getVersion());
					if (pkg1 == null) {
						logger.debug("           Add conflicts");
						conflictsPackages.add(pkg);
					}
					unresolvedDependencies.removeAll(foundDependencies);
				}
			}
			if( logger.isDebugEnabled() ) {
				logger.debug("required packages:");
				logger.debug("\n"+dumpPackages(requieredPackages));
			}
			if (requieredPackages.size() == 0) {
				break;
			}
			packages.addAll(requieredPackages);
			removeDuplicateds(packages);
		}
		// Eliminamos de la lista de paquetes los que ya estan instalados o
		// los que elusuario pidio instalar.
		requieredPackages = new ArrayList<PackageInfo>();
		Iterator<PackageInfo> it = packages.iterator();
		while (it.hasNext()) {
			PackageInfo pkg = it.next();
			if (this.packagesInstalleds.contains(pkg)) {
				continue;
			}
			if (this.packagesToInstall.contains(pkg)) {
				continue;
			}
			requieredPackages.add(pkg);
		}

		Collections.sort(requieredPackages, new Comparator<PackageInfo>() {
			public int compare(PackageInfo o1, PackageInfo o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		if( logger.isDebugEnabled() ) {
			logger.debug("required packages");
			logger.debug("\n"+dumpPackages(requieredPackages));
			logger.debug("Conflict packages");
			logger.debug("\n"+dumpPackages(conflictsPackages));
		}
		this.requieredPackages = requieredPackages;
		dumpUnresolvedDependencies();
		
	}

	private void dumpUnresolvedDependencies() {

		Iterator<Dependency> iter = unresolvedDependencies.iterator();
		while (iter.hasNext()) {
			Dependency dep = iter.next();
			Set<PackageInfo> required = requiredPackagesOfDependency.get(dep);
			if (required != null) {
				StringBuffer str_buffer = new StringBuffer();
				str_buffer.append("Unresolved dependency: " + dep.toString()
						+ " required by:\n");
				Iterator<PackageInfo> iter2 = required.iterator();
				while (iter2.hasNext()) {
					str_buffer.append("   - " + iter2.next().toStringCompact()
							+ "\n");
				}
				logger.info(str_buffer.toString());
			}
		}

	}

	public List<PackageInfo> getRequiredPackages() {
		return this.requieredPackages;
	}

	@SuppressWarnings("deprecation")
	public List<PackageInfo> getConflictPackages() {
		return this.conflictsPackages;
	}

	public Dependencies getUnresolvedDependencies() {
		return this.unresolvedDependencies;
	}

	private PackageInfo getByCodeAndVersion(Collection<PackageInfo> packages,
			StringWithAlias code, Version version) {
		Iterator<PackageInfo> it1 = packages.iterator();
		while (it1.hasNext()) {
			PackageInfo pkg1 = it1.next();
			if (pkg1.hasThisCode(code)) {
				if (pkg1.getVersion().check(">=", version)) {
					return pkg1;
				}
			}
		}
		return null;
	}

	private void removeDuplicateds(List<PackageInfo> packages) {

		boolean copy;
		List<PackageInfo> lpackages = new ArrayList<PackageInfo>();

		Iterator<PackageInfo> it1 = packages.iterator();
		while (it1.hasNext()) {
			PackageInfo pkg1 = it1.next();
			copy = true;
			Iterator<PackageInfo> it2 = packages.iterator();
			while (it2.hasNext()) {
				PackageInfo pkg2 = it2.next();
				if (pkg1 == pkg2) {
					continue;
				}
				if (!pkg1.hasThisCode(pkg2.getAllCodes())) {
					continue;
				}
				if (pkg2.getVersion().check("<", pkg1.getVersion())) {
					continue;
				}
				copy = false;
				break;
			}
			if (copy) {
				lpackages.add(pkg1);
			}
		}
		packages.clear();
		packages.addAll(lpackages);
	}

	private String dumpPackages(Collection<PackageInfo> pkgs) {
		StringBuffer s = new StringBuffer();
		Iterator<PackageInfo> it = pkgs.iterator();
		while (it.hasNext()) {
			s.append(it.next().toStringCompact());
			s.append("\n");
		}

		return s.toString();
	}

	private String dumpDependencies(Collection<Dependency> dependencies) {
		StringBuffer s = new StringBuffer();
		Iterator<Dependency> it = dependencies.iterator();
		while (it.hasNext()) {
			s.append(it.next().toString());
			s.append("\n");
		}
		return s.toString();
	}

}
