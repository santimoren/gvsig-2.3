package org.gvsig.installer.lib.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.DependenciesCalculator;
import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.tools.packageutils.StringWithAlias;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("deprecation")
public class DependenciesCalculatorV2 implements DependenciesCalculator {

	private static Logger logger = LoggerFactory
			.getLogger(DefaultDependenciesCalculator.class);

	private InstallPackageService installService = null;
	private List<PackageInfo> packagesInstalleds = null;
	private List<PackageInfo> packagesToInstall = null;
	private List<PackageInfo> requieredPackages = null;
	private List<PackageInfo> conflictsPackages = null;
	private Dependencies unresolvedDependencies = null;
	
	public DependenciesCalculatorV2(InstallPackageService installService) {
		super();
		this.installService = installService;
		this.packagesInstalleds = new ArrayList<PackageInfo>();
		this.packagesToInstall = new ArrayList<PackageInfo>();
	}
	public void addPackageToInstall(PackageInfo packageInfo) {
		this.packagesToInstall.add(packageInfo);
	}

	public void addPackageToInstall(Collection<PackageInfo> packages) {
		Iterator<PackageInfo> it = packages.iterator();
		while (it.hasNext()) {
			this.addPackageToInstall(it.next());
		}
	}

	public void addInstalledPackage(PackageInfo packageInfo) {
		this.packagesInstalleds.add(packageInfo);
	}

	public void addInstalledPackage(PackageInfo[] packages) {
		for (int i = 0; i < packages.length; i++) {
			this.addInstalledPackage(packages[i]);
		}
	}

	public List<PackageInfo> getRequiredPackages() {
		return this.requieredPackages;
	}

	public List<PackageInfo> getConflictPackages() {
		return this.conflictsPackages;
	}

	public Dependencies getUnresolvedDependencies() {
		return this.unresolvedDependencies;
	}

	private class DependencyAndPackages implements Dependency {
	
		private List<PackageInfo> packages = null;
		private Dependency delegate = null;
		
		DependencyAndPackages(Dependency dependency, PackageInfo packageInfo) {
			this.delegate = dependency;
			this.packages = new ArrayList<PackageInfo>();
			this.packages.add(packageInfo);
		}
		
		public List<PackageInfo> getPackages() {
			return this.packages;
		}
		
		public void addPackage(PackageInfo packageInfo) {
			if( this.packages.contains(packageInfo)) {
				return;
			}
			this.packages.add(packageInfo);
		}

		public Dependency parse(String dependency) {
			return this.delegate.parse(dependency);
		}

		public String getType() {
			return this.delegate.getType();
		}

		public String getCode() {
			return this.delegate.getCode();
		}

		public String getOp() {
			return this.delegate.getOp();
		}

		public Version getVersion() {
			return this.delegate.getVersion();
		}

		public boolean match(String type, String code, Version version) {
			return this.delegate.match(type, code, version);
		}

		public boolean match(String type, StringWithAlias code, Version version) {
			return this.delegate.match(type, code, version);
		}
		public Object clone() throws CloneNotSupportedException {
			DependencyAndPackages x = (DependencyAndPackages) super.clone();
			x.packages = new ArrayList<PackageInfo>();
			for (PackageInfo packageInfo : this.packages) {
				x.packages.add(packageInfo);
			}
			return x;
		}
		
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append( this.delegate.toString() );
			buffer.append(" ( ");
			for (PackageInfo packageInfo : this.packages) {
				buffer.append(packageInfo.getCode());
				buffer.append(" ");
			}
			buffer.append(" )");
			return buffer.toString();
		}
		
	}
	
	public void calculate() {
		int retryCount = 1;
		boolean retry = true;
		
		this.conflictsPackages = new ArrayList<PackageInfo>();
		this.requieredPackages = new ArrayList<PackageInfo>();
		this.unresolvedDependencies = new DefaultDependencies();
		
		if( logger.isInfoEnabled() ) {
			logger.info("Installeds packages:\n" + dumpPackages(this.packagesInstalleds));
			logger.info("Packages to install:\n" + dumpPackages(this.packagesToInstall));
		}
		while( retry ) {
			if( retryCount > 50 ) {
				logger.info("Too many retries to calculate dependencies.");
				throw new RuntimeException("Too many retries to calculate dependencies");
			}
			/* 
			 * Inicializamos el conjunto de paquetes que al final han de quedar
			 * instalados con:
			 * - Los paqetes ya instalados
			 * - Los paquetes que hay que instalar
			 * - Los paquetes que hay que instalar por que son depenendencia de 
			 *   alguno instalado o a instalar.
			 */
			List<PackageInfo> packages = new ArrayList<PackageInfo>();
			packages.addAll(this.packagesInstalleds);
			packages.addAll(this.packagesToInstall);
			packages.addAll(this.requieredPackages);
			
			/*
			 * Una vez tenemos la lista de paquetes que han de quedar instalados,
			 * Creamos la lista de dependencias de todos los paquetes. 
			 */
			Dependencies dependencies = new DefaultDependencies();
			for (PackageInfo packageInfo : packages) {
				Dependencies pkgDependencies = packageInfo.getDependencies();
				for( int i=0; i< pkgDependencies.size(); i++) {
					Dependency dependency = (Dependency) pkgDependencies.get(i);
					List x = dependencies.findAll( dependency.getType(), dependency.getCode(), dependency.getVersion());
					if( x == null ) {
						dependencies.add(new DependencyAndPackages(dependency,packageInfo));
					} else if(x.size()==1) {
						((DependencyAndPackages)x.get(0)).addPackage(packageInfo);;
					} else {
						// Hay paquetes que tienen dependencias con el mismo paquete pero
						// con versiones distintas. Cogemos la version mayor.
						Collections.sort(x, new Comparator<Dependency>() {
							public int compare(Dependency arg0, Dependency arg1) {
								// Orden inverso, la primera la mas alta
								return - arg0.getVersion().compareTo(arg1.getVersion());
							}
						});
						((DependencyAndPackages)x.get(0)).addPackage(packageInfo);;
						
					}
				}
			}
//			if( logger.isInfoEnabled() ) {
//				logger.info("Attempt: " + retryCount);
//				logger.info("Required packages:\n" + dumpPackages(this.requieredPackages));
//				logger.info("Unresolved dependencies:\n" + dumpDependencies(this.unresolvedDependencies));
//				logger.info("Conflict packages:\n" + dumpPackages(this.conflictsPackages));
//				logger.info("All dependencies:\n" + dumpDependencies(dependencies));
//			}

			/*
			 * Ahora calcularemos si se cumplen esas dependencias, y en caso de que no
			 * si tenemos el paquete que se corresponde con esa dependencia.
			 * Si se a�ade un paquete a instalar debido a que es dependencia de otro,
			 * deberemos recalcular las dependencias de nuevo y reintentarlo para ver
			 * si este nuevo paquete a introducido nuevas dependencias.
			 */
			retry = calculateRequiredPackagesAndUnresolvedDependencies(dependencies);
			if( retry ) {
				retryCount +=1;
			} else {
				calculateConflictPackages(dependencies);
			}
		}

		// Ordenamos los resultados para hacer previsible y repetibles los resultados.
		if( this.requieredPackages!=null && !this.requieredPackages.isEmpty() ) {
			Collections.sort(this.requieredPackages, new Comparator<PackageInfo>() {
				public int compare(PackageInfo o1, PackageInfo o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
		}
		if( this.conflictsPackages!=null && !this.conflictsPackages.isEmpty() ) {
			Collections.sort(this.conflictsPackages, new Comparator<PackageInfo>() {
				public int compare(PackageInfo o1, PackageInfo o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
		}
		if( this.unresolvedDependencies!=null && !this.unresolvedDependencies.isEmpty() ) {
			Collections.sort(this.unresolvedDependencies, new Comparator<Dependency>() {
				public int compare(Dependency o1, Dependency o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
		}
		
		if( logger.isInfoEnabled() ) {
			logger.info("Required packages:\n" + dumpPackages(this.requieredPackages));
			logger.info("Unresolved dependencies:\n" + dumpDependencies(this.unresolvedDependencies));
			logger.info("Conflict packages:\n" + dumpPackages(this.conflictsPackages));
			logger.info("Attempts Requireds: " + retryCount);
		}
	}
	
	private boolean calculateRequiredPackagesAndUnresolvedDependencies(Dependencies dependencies) {
		boolean retry = false;
		for( int i=0; i<dependencies.size(); i++ ) {
			DependencyAndPackages dependency = (DependencyAndPackages) dependencies.get(i);
			if( Dependency.REQUIRED.equals(dependency.getType()) ) {
				PackageInfo availablePackage = getInstalledOrToInstallOrRequiredsPackage(dependency);
				if( availablePackage==null ) {
					availablePackage = getAvalible(dependency);
					if( availablePackage != null ) {
						logger.info("Added new package "+availablePackage.getCode()+" required by dependency "+dependency.toString()+".");
						this.requieredPackages.add(availablePackage);
						/*
						 * Al a�adir paquetes nuevos, debemos recualcular las
						 * dependencias de nuevo y volver a comprobar si se cumplen.
						 */
						retry = true; 
					} else {
						this.unresolvedDependencies.add(dependency);
					}
				}
			}
		}
		return retry;
	}
	
	private void calculateConflictPackages(Dependencies dependencies) {
		for( int i=0; i<dependencies.size(); i++ ) {
			DependencyAndPackages dependency = (DependencyAndPackages) dependencies.get(i);
			if( Dependency.CONFLICT.equals(dependency.getType()) ) {
				PackageInfo pkg = getInstalledOrToInstallOrRequiredsPackage(dependency);
				if( pkg != null ) {
					this.conflictsPackages.addAll(dependency.getPackages());
				}
			}
		}
	}

	/**
	 * Busca entre los paquetes instalados o a instalar alguno que
	 * se corresponda con los requerimientos de la dependencia y lo
	 * devuelve. Devuelve null si no lo encuentra.
	 * 
	 * @param dependency
	 * @return the package that match with the dependency or null
	 */
	private PackageInfo getInstalledOrToInstallOrRequiredsPackage(
			DependencyAndPackages dependency) {
		for (PackageInfo pkg : this.packagesInstalleds ) {
			if( dependency.match(dependency.getType(), pkg.getAllCodes(), pkg.getVersion())) {
				return pkg;
			}
		}
		for (PackageInfo pkg : this.packagesToInstall ) {
			if( dependency.match(dependency.getType(), pkg.getAllCodes(), pkg.getVersion())) {
				return pkg;
			}
		}
		for (PackageInfo pkg : this.requieredPackages ) {
			if( dependency.match(dependency.getType(), pkg.getAllCodes(), pkg.getVersion())) {
				return pkg;
			}
		}
		return null;
	}
	
	/**
	 * Busca entre los paquetes disponibles a ver si encuentra uno que se 
	 * corresponda con los requerimientos de la dependencia y si lo encunetra
	 * lo devuelve. Si no devuelbe null.
	 * 
	 * @param dependency
	 * @return the package that match with the dependency or null
	 */
	private PackageInfo getAvalible(DependencyAndPackages dependency) {
		for (int i = 0; i < this.installService.getPackageCount(); i++) {
			PackageInfo pkg = this.installService.getPackageInfo(i);
			if( dependency.match(dependency.getType(), pkg.getAllCodes(), pkg.getVersion())) {
				return pkg;
			}
		}
		return null;
	}
	
	private String dumpPackages(Collection<PackageInfo> pkgs) {
		StringBuffer s = new StringBuffer();
		Iterator<PackageInfo> it = pkgs.iterator();
		while (it.hasNext()) {
			s.append(it.next().toStringCompact());
			s.append("\n");
		}

		return s.toString();
	}
	
	private String dumpDependencies(Collection<Dependency> dependencies) {
		StringBuffer s = new StringBuffer();
		Iterator<Dependency> it = dependencies.iterator();
		while (it.hasNext()) {
			s.append(it.next().toString());
			s.append("\n");
		}
		return s.toString();
	}
}
