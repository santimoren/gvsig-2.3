/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.installer.lib.api.Dependencies;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.InstallerManager.ARCH;
import org.gvsig.installer.lib.api.InstallerManager.JVM;
import org.gvsig.installer.lib.api.InstallerManager.OS;
import org.gvsig.installer.lib.api.InstallerManager.STATE;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.info.InstallerInfoTags;
import org.gvsig.installer.lib.impl.utils.DeleteFile;
import org.gvsig.installer.lib.impl.utils.Download;
import org.gvsig.installer.lib.impl.utils.SignUtil;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.packageutils.PackageManager;
import org.gvsig.tools.packageutils.StringWithAlias;
import org.gvsig.tools.packageutils.impl.DefaultStringWithAlias;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultPackageInfo implements PackageInfo {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultPackageInfo.class);

    private StringWithAlias code = null;
    private String name = null;
    private String description = null;
    private Version version = null;
    private boolean official;
    private List<File> auxFiles = null;
    private String antScript = null;
    private String type = "unknow";
    private Boolean signed = false;
    private Boolean broken = false;

    private String state = STATE.DEVEL;
//    private String operatingSystem = OS.ALL;
    private String operatingSystemFamily = OS.ALL;
    private String operatingSystemName = null;
    private String operatingSystemVersion = null;
    
    private String architecture = ARCH.ALL;
    private String javaVM = JVM.J1_5;

    private String owner = "";
    private URL ownerURL = null;
    private URL sources = null;
    private String gvSIGVersion = "";

    private String defaultDownloadURL = null;

    private String modelVersion = "1.0.1";
    private Dependencies dependencies = null;
    private List<String> categories = null;

    private URL webURL = null;
    
    private static PackageManager packageManager = null;
   
    public DefaultPackageInfo() {
        super();
        auxFiles = new ArrayList<File>();
        this.version = new DefaultVersion().parse("0.0.1");
        this.dependencies = new DefaultDependencies();
        this.categories = new ArrayList<String>();
    }

    public String getCode() {
    	if( code == null ) {
    		return null;
    	}
        return code.toString();
    }

    public StringWithAlias getAllCodes() {
    	return this.code;
    }
    
    private String getAliasAsString() {
    	if( this.code == null ) {
    		return "";
    	}
    	StringBuffer s = new StringBuffer();
    	Iterator alias = this.code.getAlias().iterator();
    	while( alias.hasNext() ) {
    		String ss = (String) alias.next();
    		s.append(ss);
    		if( alias.hasNext() ) {
    			s.append(", ");
    		}
    	}
    	return s.toString();
    }
    
    public boolean hasThisCode(String code) {
    	if( this.code == null ) {
    		if( code == null ) {
    			return true;
    		}
    		return false;
    	}
    	return this.code.equalsIgnoreCase(code);
    }
    
    public boolean hasThisCode(StringWithAlias code) {
    	if( this.code == null ) {
    		if( code == null ) {
    			return true;
    		}
    		return false;
    	}
    	return this.code.equalsIgnoreCase(code);
    }
    

    public String getID() {
        String id =
            this.getCode() + "#" + this.getVersion() 
                + "#" + this.getOperatingSystem() + "#"
                + this.getArchitecture();
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Version getVersion() {
        return version;
    }

    public int getBuild() {
        return this.version.getBuild();
    }

    public String getState() {
        return state;
    }

    public boolean isOfficial() {
        return official;
    }

    public void setCode(String code) {
    	if( code == null ) {
    		this.code = null;
    	} else {
    		this.code = new DefaultStringWithAlias(code);
    	}
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVersion(String version) {
        if (version == null) {
            return;
        }
        int prev = this.version.getBuild();
        this.version.parse(version);
        int curr = this.version.getBuild();
        if (prev != 0 && curr == 0) {
            this.version.setBuild(prev);
        }
    }

    public void setVersion(Version version) {
        try {
            int prev = this.version.getBuild();
            this.version = (Version) version.clone();
            int curr = this.version.getBuild();
            if (prev != 0 && curr == 0) {
                this.version.setBuild(prev);
            }
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public void setBuild(int build) {
        this.version.setBuild(build);
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setOfficial(boolean official) {
        this.official = official;
    }

    @Override
    public String getOperatingSystem() {
        StringBuilder operatingSystem = new StringBuilder();
        operatingSystem.append(this.operatingSystemFamily);
        if( !StringUtils.isEmpty(this.operatingSystemName) ) {
            operatingSystem.append("_");
            operatingSystem.append(this.operatingSystemName);
            if( !StringUtils.isEmpty(this.operatingSystemVersion) ) {
                operatingSystem.append("_");
                operatingSystem.append(this.operatingSystemVersion);
            }
        }
        return operatingSystem.toString();
    }

    @Override
    public void setOperatingSystem(String operatingSystem) {
    	if( StringUtils.isEmpty(operatingSystem) ) {
    		this.operatingSystemFamily = OS.ALL;
    		this.operatingSystemName = null;
    		this.operatingSystemVersion = null;
    	} else {
            if( operatingSystem.contains("_") ) {
                String s[] = operatingSystem.split("_");
                switch(s.length) {
                    case 2:
                        this.operatingSystemFamily = s[0];
                        this.operatingSystemName = s[1];
                        break;
                    case 3:
                        this.operatingSystemFamily = s[0];
                        this.operatingSystemName = s[1];
                        this.operatingSystemVersion = s[2];
                        break;
                    default:
                        throw new IllegalArgumentException("Can't parse OS '"+operatingSystem+"'.");
                }
            } else {
                this.operatingSystemFamily = operatingSystem;
            }
    	}
    }
    
    @Override
    public String getOperatingSystemFamily() {
        return this.operatingSystemFamily;
    }
    
    @Override
    public String getOperatingSystemName() {
        return this.operatingSystemName;
    }
    
    @Override
    public String getOperatingSystemVersion() {
        return this.operatingSystemVersion;
    }

    public void setOperatingSystemFamily(String operatingSystemFamily) {
        this.operatingSystemFamily = operatingSystemFamily;
    }
    
    public void setOperatingSystemName(String operatingSystemName) {
        this.operatingSystemName = operatingSystemName;
    }
    
    public void setOperatingSystemVersion(String operatingSystemVersion) {
        this.operatingSystemVersion = operatingSystemVersion;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public String getJavaVM() {
        return javaVM;
    }

    public void setJavaVM(String javaVM) {
        this.javaVM = javaVM;
    }

    public String getAntScript() {
        return antScript;
    }

    public void setAntScript(String antScript) {
        this.antScript = antScript;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGvSIGVersion() {
        return gvSIGVersion;
    }

    public void setGvSIGVersion(String gvSIGVersion) {
        this.gvSIGVersion = gvSIGVersion;
    }

    private URL internalGetDownloadURL() {
        if (defaultDownloadURL != null) {
            try {
                return new URL(defaultDownloadURL);
            } catch (MalformedURLException e) {
                throw new RuntimeException(
                    "Error converting to URL the package download url: "
                        + defaultDownloadURL, e);
            }
        }
        return null;
    }

    public URL getDownloadURL() {
        InstallerManager manager = InstallerLocator.getInstallerManager();
        if (manager == null) {
            return null;
        }
        return getDownloadURL(manager.getDownloadBaseURL());
    }

    public URL getDownloadURL(URL baseURL) {
        try {
            return internalGetDownloadURL();
        } catch (RuntimeException e) {
            // The download URL in the package info is not a valid URL,
            // so it might be a relative one.
            // Try to create and absolute one.
        }

        // Create a full URL with the base one and the download one.
        try {
            return new URL(baseURL, this.defaultDownloadURL);
        } catch (MalformedURLException e) {
            throw new RuntimeException(
                "Error converting to URL the package download url, "
                    + "with the base URL: " + baseURL
                    + ", the package download URL: " + this.defaultDownloadURL,
                e);
        }
    }

    public String getDownloadURLAsString() {
        return this.defaultDownloadURL;
    }

    public void setDownloadURL(URL defaultDownloadURL) {
        this.defaultDownloadURL = defaultDownloadURL.toString();
    }

    public void setDownloadURL(String defaultDownloadURL) {
        this.defaultDownloadURL = defaultDownloadURL;
    }

    public String getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(String modelVersion) {
        this.modelVersion = modelVersion;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public URL getOwnerURL() {
        return ownerURL;
    }

    public void setOwnerURL(URL sources) {
        this.ownerURL = sources;
    }

    public URL getSourcesURL() {
        return sources;
    }

    public void setSourcesURL(URL sources) {
        this.sources = sources;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(super.toString()).append(" (");

        append(buffer, InstallerInfoTags.CODE, getCode());
        append(buffer, InstallerInfoTags.NAME, getName());
        append(buffer, InstallerInfoTags.DESCRIPTION, getDescription());
        append(buffer, InstallerInfoTags.GVSIG_VERSION, getGvSIGVersion());
        append(buffer, InstallerInfoTags.VERSION, getVersion());
        append(buffer, InstallerInfoTags.BUILD, getBuild());
        append(buffer, InstallerInfoTags.OS, getOperatingSystem());
        append(buffer, InstallerInfoTags.ARCHITECTURE, getArchitecture());
        append(buffer, InstallerInfoTags.JVM, getJavaVM());
        append(buffer, InstallerInfoTags.DOWNLOAD_URL, getDownloadURL());
        append(buffer, InstallerInfoTags.STATE, getState());
        append(buffer, InstallerInfoTags.OFFICIAL, isOfficial());
        append(buffer, InstallerInfoTags.TYPE, getType());
        append(buffer, InstallerInfoTags.MODEL_VERSION, getModelVersion());
        append(buffer, InstallerInfoTags.OWNER, getOwner());
        append(buffer, InstallerInfoTags.OWNER_URL, getOwnerURL());
        append(buffer, InstallerInfoTags.SOURCES_URL, getSourcesURL());
        append(buffer, InstallerInfoTags.DEPENDENCIES, getDependencies());
        append(buffer, InstallerInfoTags.WEB_URL, getWebURL());
        append(buffer, InstallerInfoTags.CATEGORIES, getCategories());
        append(buffer, InstallerInfoTags.CODEALIAS, getAliasAsString());

        return buffer.append(')').toString();
    }

    public String toStringCompact() {
        // type code version state os arch jvm dep
        return String
            .format(
                "%1$-8.8s %2$-40s %3$-20.20s %4$-5.5s %5$-5.5s %6$-6.6s %7$-5.5s %8$s %8$s",
                this.type, this.code, this.version, this.state,
                this.getOperatingSystem(), this.architecture, this.javaVM,
                this.dependencies, this.getAliasAsString());
    }

    private DefaultPackageInfo append(StringBuffer buffer, String key,
        Object value) {
        buffer.append("\n\t").append(key).append(": ")
            .append(value == null ? "" : value);
        return this;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DefaultPackageInfo clone = (DefaultPackageInfo) super.clone();
        clone.auxFiles = new ArrayList<File>(auxFiles);
        return clone;
    }

    public File downloadFile() throws InstallPackageServiceException {
        return this.downloadFile(null);
    }

    public class FileDownloadException extends InstallPackageServiceException {

        private static final long serialVersionUID = 8640183295766490512L;

        private static final String message = "File '%(url)s' download error";

        private static final String KEY = "_File_XurlX_download_error";

        public FileDownloadException(URL url, IOException e) {
            super(message, e, KEY, serialVersionUID);
            setValue("url", url.toString());
        }

    }

    public File downloadFile(SimpleTaskStatus taskStatus)
        throws InstallPackageServiceException {
    	File file = null;
    	try {
	        Download download = new Download(taskStatus);
	
	        // First try to download from the index base URL this package info has
	        // been downloaded from, looking first as if the index is located in the
	        // main version folder, and if not in the build folder. 
	        // If also not there, download from the URL available in the 
	        // downloadURL property.
	        // Example baseURL: http://downloads.gvsig.org/download/gvsig-desktop/dists/2.0.0/
			String versionRelativePath = "../../pool/" + getCode() + "/"
					+ getPackageFileName();
			file = downloadFromRelativeURL(download, versionRelativePath);
	
			if (file == null) {
		        // Example baseURL: http://downloads.gvsig.org/download/gvsig-desktop/dists/2.0.0/builds/2048/
				String buildRelativePath = "../../../../pool/" + getCode() + "/"
						+ getPackageFileName();
				file = downloadFromRelativeURL(download, buildRelativePath);
			}
	
			if (file == null) {
				file = downloadFromPackageInfoURL(download);
			}
			
    	} catch(InstallPackageServiceException ex) {
    		LOG.info("Can't download package '"+this.getCode()+"' from server '"+getBaseURL().toString()+"'.",ex);
    		throw ex;
    	} catch(RuntimeException ex) {
    		LOG.info("Can't download package '"+this.getCode()+"' from server '"+getBaseURL().toString()+"'.",ex);
    		throw ex;
    	}
    	if( file == null ) {
    		LOG.info("Can't download package '"+this.getCode()+"' from server '"+getBaseURL().toString()+"'.");
    	}
		return file;
	}

    private URL getBaseURL() {
		InstallerManager manager = InstallerLocator.getInstallerManager();
		URL baseURL = manager.getDownloadBaseURL();
		return baseURL;
    }
    
	private File downloadFromRelativeURL(Download download,
			String buildRelativePath) {
		InstallerManager manager = InstallerLocator.getInstallerManager();
		URL baseURL = manager.getDownloadBaseURL();
		try {
			URL downloadURL = new URL(baseURL, buildRelativePath);
			return download.downloadFile(downloadURL, null);
		} catch (IOException e) {
			LOG.debug("Package " + getName()
					+ " not found relative to the build index URL: " + baseURL
					+ ", in the URL: " + buildRelativePath, e);
		}
		return null;
	}

	private File downloadFromPackageInfoURL(Download download) throws FileDownloadException {
		try {
			return download.downloadFile(this.getDownloadURL(), null);
		} catch (IOException ex3) {
			throw new FileDownloadException(this.getDownloadURL(), ex3);
		}
	}

    private String getPackageFileName() {
        Object[] values =
            new Object[] { "gvSIG-desktop", getGvSIGVersion(), getCode(),
                getVersion(), getState(), getOperatingSystem(),
                getArchitecture(), getJavaVM() };
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < values.length - 1; i++) {
            buffer.append(values[i]).append('-');
        }
        buffer.append(values[values.length - 1]).append(".gvspkg");
        return buffer.toString();
    }

    public void addFileToCopy(File file) {
        auxFiles.add(file);
    }

    public File getFileToCopy(int i) {
        return auxFiles.get(i);
    }

    public void removeFileToCopy(File file) {
        auxFiles.remove(file);
    }

    public void clearFilesToCopy() {
        auxFiles.clear();
    }

    public List<File> getFilesToCopy() {
        return auxFiles;
    }

    public boolean removeInstallFolder(File folder) {
        DeleteFile delete = new DeleteFile();
        boolean success = delete.delete(folder);
        setAntScript(null);
        clearFilesToCopy();
        return success;
    }

    public boolean removeFilesFolder(File folder) {
        DeleteFile delete = new DeleteFile();
        return delete.delete(folder);
    }

    @Override
    public boolean matchID(String string) {
        final int PART_CODE = 0;
        final int PART_VERSION = 1;
        final int PART_OS = 2;
        final int PART_ARCH = 3;
        
        String id = this.getID();
        String[] parts = string.split("#");

        switch (parts.length) {
        case 1:
            if (parts[PART_CODE].equals(this.getCode())) {
                return true;
            } else {
                return false;
            }
        case 2:
            if (!parts[PART_CODE].equals(this.getCode())) {
                return false;
            }
            Version version = new DefaultVersion();
            try {
                version.parse(parts[PART_VERSION]);
            } catch (InvalidParameterException ex) {
                return false;
            }

            if (version.equals(this.getVersion())) {
                return true;
            }
            return false;
        case 4: // id, version, os, arch
            if (!parts[PART_CODE].equals(this.getCode())) {
                return false;
            }
            if( !"any".equalsIgnoreCase(parts[PART_VERSION]) ) {
                version = new org.gvsig.tools.packageutils.impl.DefaultVersion();
                try {
                    version.parse(parts[PART_VERSION]);
                } catch (InvalidParameterException ex) {
                    return false;
                }

                if (!version.equals(this.getVersion())) {
                    return false;
                }
            }
            if( !"any".equalsIgnoreCase(parts[PART_OS]) ) {
                if( "current".equalsIgnoreCase(parts[PART_OS]) ) {
                    if( !this.getOperatingSystemFamily().equalsIgnoreCase(this.getPackageManager().getOperatingSystemFamily()) ) {
                        return false;
                    }
                    if( !StringUtils.isEmpty(this.getOperatingSystemName())) {
                        if( !this.getOperatingSystemName().equalsIgnoreCase(this.getPackageManager().getOperatingSystemName()) ) {
                            return false;
                        }
                        if( !StringUtils.isEmpty(this.getOperatingSystemVersion())) {
                            if( !this.getOperatingSystemVersion().equalsIgnoreCase(this.getPackageManager().getOperatingSystemVersion()) ) {
                                return false;
                            }
                        }
                    }
                        
                } else if( !this.getOperatingSystem().equalsIgnoreCase(parts[PART_OS]) ) {
                    return false;
                }
            }
            if( !"any".equalsIgnoreCase(parts[PART_ARCH]) ) {
                if( "current".equalsIgnoreCase(parts[PART_ARCH]) ) {
                    if( !this.getArchitecture().equalsIgnoreCase(this.getPackageManager().getArchitecture()) ) {
                        return false;
                    }
                } else if( !this.getArchitecture().equalsIgnoreCase(parts[3]) ) {
                    return false;
                }
            }
            return true;
        default:
            return string.equals(id);

        }

    }
    
    private PackageManager getPackageManager() {
        if( packageManager==null ) {
            packageManager = ToolsLocator.getPackageManager();
        }
        return packageManager;
    }
    
    public Dependencies getDependencies() {
        return this.dependencies;
    }

    public void setDependencies(Dependencies dependencies) {
        this.dependencies = dependencies;
    }

    public void setDependencies(String dependencies) {
        if (dependencies == null) {
            this.dependencies = null;
        } else {
            this.dependencies = new DefaultDependencies().parse(dependencies);
        }
    }

    @Override
    public boolean equals(Object obj) {
        PackageInfo other;
        try {
            other = (PackageInfo) obj;
        } catch (Exception e) {
            return false;
        }
        if (!code.equalsIgnoreCase(other.getCode())) {
            return false;
        }
        if (!version.check("=", other.getVersion())) {
            return false;
        }
        if (!getOperatingSystem().equalsIgnoreCase(other.getOperatingSystem())) {
            return false;
        }
        if (!gvSIGVersion.equalsIgnoreCase(other.getGvSIGVersion())) {
            return false;
        }
        if (!state.equalsIgnoreCase(other.getState())) {
            return false;
        }
        if (!architecture.equalsIgnoreCase(other.getArchitecture())) {
            return false;
        }
        if (!javaVM.equalsIgnoreCase(other.getJavaVM())) {
            return false;
        }
        if (!type.equalsIgnoreCase(other.getType())) {
            return false;
        }
        if (official != other.isOfficial()) {
            return false;
        }
        return true;
    }

    public URL getWebURL() {
        return webURL;
    }

    public void setWebURL(URL webURL) {
        this.webURL = webURL;
    }

    public List<String> getCategories() {
        return this.categories;
    }

    public void setCategories(List categoriesList) {
        for (int i = 0; i < categoriesList.size(); i++) {
            if (!this.categories.contains(categoriesList.get(i))) {
                this.categories.add((String) categoriesList.get(i));
            }
        }
    }

    public String getCategoriesAsString() {
        String categoriesString = "";
        for (int i = 0; i < this.categories.size(); i++) {
            if (i + 1 < this.categories.size()) {
                categoriesString += this.categories.get(i) + ", ";
            } else {
                categoriesString += this.categories.get(i);
            }
        }
        return categoriesString;
    }

    public void addCategoriesAsString(String categoriesString) {
        if (categoriesString == null) {
            return;
        }
        categoriesString.trim();
        String[] cadena = categoriesString.split(",");

        for (int i = 0; i < cadena.length; i++) {
            String trimCadena = cadena[i].trim();
            if ((!this.categories.contains(trimCadena)) && (trimCadena != "")) {
                this.categories.add(trimCadena);
            }
        }

    }

    public void checkSignature(byte[] pkgdata) {
        this.signed = false;
        SignUtil signutil = null;
        List<byte[]> keys =
            InstallerLocator.getInstallerManager().getPublicKeys();
        if (keys.size() < 1) {
            // No hay claves publicas, asi que no comprobamos ninguna firma
            // y decirmos a todos que no estan firmados.
            this.signed = false;
            return;
        }
        for (byte[] rawkey : keys) {
            signutil = new SignUtil(rawkey);
            if (signutil.canVerify()) {
                int status = signutil.verify(pkgdata);
                switch (status) {
                case SignUtil.SIGN_OK:
                    // El paquete tiene una firma correcta,lo marcamos
                    // como firmado y salimos.
                    this.signed = true;
                    return;
                case SignUtil.NOT_SIGNED:
                    // El paquete no va firmado, lo marcamos como no
                    // firmado y salimos.
                    this.signed = false;
                    return;
                case SignUtil.SIGN_FAIL:
                default:
                    // La firma del paquete no es correcta para esta clave,
                    // lo intentamos con el resto de claves.
                    break;
                }
            }
        }
        // Si habiendo claves publicas la comprobacion de la firma con todas
        // ellas
        // falla marcamos el paquete como roto.
        this.broken = true;
    }

    public boolean isBroken() {
        if (this.broken) {
            return true;
        }
        // if( this.isOfficial() && !this.isSigned() ) {
        // return true;
        // }
        return false;
    }

    public boolean isSigned() {
        return signed;
    }

    /* (non-Javadoc)
     * @see org.gvsig.installer.lib.api.PackageInfo#getApplicationVersion()
     */
    public Version getApplicationVersion() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.installer.lib.api.PackageInfo#setApplicationVersion(org.gvsig.installer.lib.api.Version)
     */
    public void setApplicationVersion(Version version) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see org.gvsig.installer.lib.api.PackageInfo#setValue(java.lang.String, java.lang.Object)
     */
    public void setValue(String name, Object value) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see org.gvsig.installer.lib.api.PackageInfo#getValue(java.lang.String)
     */
    public Object getValue(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.installer.lib.api.PackageInfo#getPreferedPackageFileName()
     */
    public String getPreferedPackageFileName() {
        // TODO Auto-generated method stub
        return null;
    }

}
