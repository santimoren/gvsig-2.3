/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.lib.impl.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;

public class SignUtil {

	public static final int SIGN_OK = 0;
	public static final int SIGN_FAIL = 1;
	public static final int NOT_SIGNED = 2;

	private PublicKey pubKey = null;
	private PrivateKey privKey = null;

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, FileNotFoundException {
		new SignUtil().doMain(args);
	}

	private void doMain(String[] args) {

		try {
			if( "sign".equalsIgnoreCase(getCommand(args)) ) {
				if( !this.canSign() ) {
					System.out.println("Can't locate private key to sign.");
					return;
				}
				this.sign( this.getArg1AsFile(args));
				System.out.println("File "+this.getArg1(args)+" signed.");
				return;
			}
			
			if( "verify".equalsIgnoreCase(getCommand(args)) ) {
				if( !this.canVerify() ) {
					System.out.println("Can't locate public key to verify.");
					return;
				}
				switch(this.verify(this.getArg1AsFile(args))) {
				case NOT_SIGNED:
					System.out.println("NOT SIGNED");
					break;
				case SIGN_FAIL:
					System.out.println("SIGN FAIL");
					break;
				case SIGN_OK:
					System.out.println("SIGN OK");
					break;
				default:
					System.out.println("ERROR");
				}
				return;
			}

			if( "generateKeys".equalsIgnoreCase(getCommand(args)) ) {
				this.generateKeys();
				System.out.println("Generate keys ok");
				return;
			}

			System.out.println("Usage: SignUtil sign|verify|generateKeys\n" +
					"  sign file\n" +
					"  verify file\n" +
					"  generateKeys\n");
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getCommand(String[] args) {
		if( args.length>=1) {
			return args[0];
		}
		return null;
	}
	
	private File getArg1AsFile(String[] args) {
		if( args.length>=2) {
			return new File(args[1]);
		}
		return null;
	}
	
	private String getArg1(String[] args) {
		if( args.length>=2) {
			return args[1];
		}
		return null;
	}
	
	public SignUtil()  {
		loadPrivateKey();
		loadPublicKey();
	}

	public SignUtil(byte[] publicKey){
		loadPrivateKey();
		loadPublicKey(publicKey);
	}

	private void loadPrivateKey()  {
		File home = new File(System.getProperty("user.home"));
		File keyfile = new File(home, ".gvsig-keys" + File.separatorChar
				+ "key.private");
		if (keyfile.exists()) {
			try {
				loadPrivateKey(keyfile);
			} catch (Exception e) {
				// Ignore errors
			}
		}
	}

	private PrivateKey loadPrivateKey(File key) {
		try {
			byte[] rawkey;
			rawkey = loadFileAsByteArray(new FileInputStream(key));
			EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(rawkey);
			KeyFactory keyFactory = KeyFactory.getInstance("DSA", "SUN");
			this.privKey = keyFactory.generatePrivate(keySpec);
		} catch (Exception e) {
			this.privKey = null;
		}
		return this.privKey;
	}

	private PublicKey loadPublicKey() {
		File home = new File(System.getProperty("user.home"));
		File keyfile = new File(home, ".gvsig-keys" + File.separatorChar
				+ "key.public");
		if (keyfile.exists()) {
				return loadPublicKey(loadFileAsByteArray(keyfile));
		}
		return null;
	}
	private PublicKey loadPublicKey(byte[] rawkey)  {
		try {
			EncodedKeySpec keySpec = new X509EncodedKeySpec(rawkey);
			KeyFactory keyFactory;
			keyFactory = KeyFactory.getInstance("DSA", "SUN");
			this.pubKey = keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			this.pubKey = null;
		}
		return this.pubKey;
	}

	private PublicKey getPublicKey() {
		return this.pubKey;
	}

	private PrivateKey getPrivateKey() {
		return this.privKey;
	}

	private byte[] loadFileAsByteArray(InputStream in) {
		byte[] alldata;
		try {
			alldata = new byte[in.available()];
			in.read(alldata);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return alldata;
	}

	private byte[] loadFileAsByteArray(File file) {
		try {
			return loadFileAsByteArray(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getData(byte[] alldata) {
		BufferedReader reader = new BufferedReader(new StringReader(
				new String(alldata)));
		StringWriter writer = new StringWriter();
		String line;
		try {
			boolean inSignature = false;
			while ((line = reader.readLine()) != null) {
				if (inSignature) {
					if (line.startsWith("## END SIGNATURE")) {
						inSignature = false;
					}
				} else {
					if (line.startsWith("## BEGIN SIGNATURE")) {
						inSignature = true;
					} else {
						writer.append(line);
						writer.append("\n");
					}
				}
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return writer.toString();
	}

	private byte[] getSignature(byte[] alldata) {
		BufferedReader reader = new BufferedReader(new StringReader(
				new String(alldata)));
		StringWriter writer = new StringWriter();
		String line;
		try {
			boolean inSignature = false;
			while ((line = reader.readLine()) != null) {
				if (inSignature) {
					if (line.startsWith("## END SIGNATURE")) {
						inSignature = false;
					} else {
						writer.append(line);
						writer.append("\n");
					}
				} else {
					if (line.startsWith("## BEGIN SIGNATURE")) {
						inSignature = true;
					}
				}
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		String s = writer.toString();
		if (s.length() < 1) {
			return null;
		}
		Base64 coder = new Base64(60);
		return coder.decode(s);
	}

	public void sign(File file) {
		Signature dsa;
		Base64 coder = new Base64(60);

		try {
			String data = getData(loadFileAsByteArray(file));
			dsa = Signature.getInstance("SHA1withDSA", "SUN");
			dsa.initSign(getPrivateKey());
			dsa.update(data.getBytes());

			String sig = coder.encodeAsString(dsa.sign());
			String[] lines = sig.split("\n");

			FileWriter fw = new FileWriter(file);
			fw.append(data);
			fw.append("## BEGIN SIGNATURE\n");
			for (int i = 0; i < lines.length; i++) {
				fw.append("## ");
				fw.append(lines[i]);
				fw.append("\n");
			}
			fw.append("## END SIGNATURE\n");
			fw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public int verify(File file) {
		return verify(loadFileAsByteArray(file));
	}

	public int verify(byte[] alldata) {
		try {
			String data = getData(alldata);
			byte[] signature = getSignature(alldata);
			if (signature == null) {
				return NOT_SIGNED;
			}
			Signature dsa;
			dsa = Signature.getInstance("SHA1withDSA", "SUN");
			dsa.initVerify(getPublicKey());
			dsa.update(data.getBytes());
			if (!dsa.verify(signature)) {
				return SIGN_FAIL;
			}
			return SIGN_OK;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return NOT_SIGNED;
	}

	public void generateKeys() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA",
					"SUN");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG",
					"SUN");
			keyGen.initialize(1024, random);

			KeyPair pair = keyGen.generateKeyPair();
			PrivateKey privKey = pair.getPrivate();
			PublicKey pubKey = pair.getPublic();

			FileOutputStream fos;
			fos = new FileOutputStream("key.private");
			fos.write(privKey.getEncoded());
			fos.close();

			fos = new FileOutputStream("key.public");
			fos.write(pubKey.getEncoded());
			fos.close();

			System.out
					.println("Generados los ficheros key.private y key.public en la carpeta corriente.");
			System.out
					.println("Por defecto la aplicaccion las buscara en la carpeta $HOME/.gvsig-keys .");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public boolean canSign() {
		if( this.getPrivateKey() == null ) {
			return false;
		}
		return true;
	}
	
	public boolean canVerify() {
		if( this.getPublicKey() == null ) {
			return false;
		}
		return true;
	}
	
	
}
