/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.installer.lib.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.installer.lib.api.DependenciesCalculator;
import org.gvsig.installer.lib.api.Dependency;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.PackageInfoReader;
import org.gvsig.installer.lib.api.PackageInfoWriter;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.lib.api.creation.MakePackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.api.execution.InstallPackageService;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.impl.creation.DefaultMakePackageService;
import org.gvsig.installer.lib.impl.info.InstallerInfoFileReader;
import org.gvsig.installer.lib.impl.info.InstallerInfoFileWriter;
import org.gvsig.installer.lib.spi.InstallerProviderManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.packageutils.PackageManager;
import org.gvsig.tools.service.AbstractManager;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultInstallerManager extends AbstractManager implements
        InstallerManager {

    private static Logger logger = LoggerFactory.getLogger(DefaultInstallerManager.class);
            
    private static class LocalRepositoryLocation {

        private File location;
        private Set<String> types;

        public LocalRepositoryLocation(File localtion, String type) {
            this.location = localtion;
            this.types = new HashSet<String>();
            this.addType(type);
        }

        public LocalRepositoryLocation(File localtion) {
            this(localtion, null);
        }

        public void addType(String type) {
            if ( !StringUtils.isBlank(type) ) {
                this.types.add(type);
            }
        }

        public void addType(LocalRepositoryLocation location) {
            this.types.addAll(location.getTypes());
        }

        public Collection<String> getTypes() {
            return this.types;
        }

        public File getLocation() {
            return this.location;
        }

        public String getDefaultType() {
            if ( this.types.isEmpty() ) {
                return null;
            }
            return this.types.iterator().next();
        }

        public boolean is(File location) {
            if ( location.equals(this.location) ) {
                return true;
            }
            return false;
        }

        public boolean contains(File file) {
            if ( file.getAbsolutePath().startsWith(this.location.getAbsolutePath()) ) {
                return true;
            }
            return false;
        }
        
        public boolean contains(PackageInfo packageInfo) {
            if( !this.support(packageInfo.getType()) ) {
                return false;
            }
            String packageInfoName = packageInfo.getCode() + File.separator + PACKAGE_INFO_FILE_NAME;
            File packageInfoFile = new File(this.location,packageInfoName);
            return packageInfoFile.exists();
        }
        
        public boolean support(String type) {
            for( String atype : this.types ) {
                if( atype != null ) {
                    if( atype.equalsIgnoreCase(type) ) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private static class LocalRepositoriesLocations extends ArrayList<LocalRepositoryLocation> {

        public LocalRepositoryLocation getLocation(File location) {
            Iterator<LocalRepositoryLocation> it = super.iterator();
            while ( it.hasNext() ) {
                LocalRepositoryLocation x = it.next();
                if ( x.is(location) ) {
                    return x;
                }
            }
            return null;
        }

        public boolean add(LocalRepositoryLocation location) {
            LocalRepositoryLocation old = this.getLocation(location.getLocation());
            if ( old != null ) {
                old.addType(location);
                return true;
            }
            return super.add(location);
        }

    }

    private static final String INSTALLER_MANAGER_EXTENSION_POINT = "InstallerManagerExtensionPoint";
    private static final String INSTALLER_CREATION_SERVICE_NAME = "InstallerCreationService";
    private static final String INSTALLER_EXECUTION_SERVICE_NAME = "InstallerExecutionService";
    private ExtensionPointManager extensionPoints = ToolsLocator
            .getExtensionPointManager();

    private String packageSetNameFormat = "gvSIG-desktop-{0}-{1}-{2}-{4}-{5}-{6}-{7}.gvspks";
    private String packageNameFormat = "gvSIG-desktop-{0}-{1}-{2}-{4}-{5}-{6}-{7}.gvspkg";
    private String packageIndexNameFormat = "gvSIG-desktop-{0}-{1}-{2}-{4}-{5}-{6}-{7}.gvspki";

    private URL BaseDownloadURL = null;
    private Version version = null;
    private List<LocalRepositoryLocation> localRepositoriesLocation = null;
    private Map<String,File> defaultRepositoryLocation = null;

    private String operatingSystemName;
    private String operatingSystemFamily;
    private String operatingSystemVersion;

    public DefaultInstallerManager() {
        super(new DefaultInstallerProviderManager());
        this.defaultRepositoryLocation = new HashMap<String,File>();
        localRepositoriesLocation = new LocalRepositoriesLocations();
    }

    public String getPackageSetNameFormat() {
        return packageSetNameFormat;
    }

    public void setPackageSetNameFormat(String packageSetNameFormat) {
        this.packageSetNameFormat = packageSetNameFormat;
    }

    public String getPackageNameFormat() {
        return packageNameFormat;
    }

    public void setPackageNameFormat(String packageNameFormat) {
        this.packageNameFormat = packageNameFormat;
    }

    public String getPackageIndexNameFormat() {
        return packageIndexNameFormat;
    }

    public void setPackageIndexNameFormat(String packageIndexNameFormat) {
        this.packageIndexNameFormat = packageIndexNameFormat;
    }

    public MakePluginPackageService getMakePluginPackageService()
            throws MakePluginPackageServiceException {
        ExtensionPoint ep = extensionPoints
                .add(INSTALLER_MANAGER_EXTENSION_POINT);
        try {
            Object[] args = new Object[]{this};
            return (MakePluginPackageService) ep.create(
                    INSTALLER_CREATION_SERVICE_NAME, args);
        } catch (Exception e) {
            throw new MakePluginPackageServiceException(
                    "Exception creating the installer service to create installers",
                    e);
        }
    }

    public class InstallerCreationException extends
            InstallPackageServiceException {

        private static final long serialVersionUID = 759329820705535873L;

        private static final String message = "Error creating the installer service to install plugins";

        private static final String KEY = "_Error_creating_the_installer_service_to_install_plugins";

        public InstallerCreationException(Exception e) {
            super(message, e, KEY, serialVersionUID);
        }

    }

    public InstallPackageService getInstallPackageService()
            throws InstallPackageServiceException {
        ExtensionPoint ep = extensionPoints
                .add(INSTALLER_MANAGER_EXTENSION_POINT);
        try {
            Object[] args = new Object[1];
            args[0] = this;
            return (InstallPackageService) ep.create(
                    INSTALLER_EXECUTION_SERVICE_NAME, args);
        } catch (Exception e) {
            throw new InstallerCreationException(e);
        }
    }

    public void registerMakePluginPackageService(
            Class<? extends MakePluginPackageService> clazz) {
        ExtensionPoint extensionPoint = extensionPoints.add(
                INSTALLER_MANAGER_EXTENSION_POINT, "");
        extensionPoint.append(INSTALLER_CREATION_SERVICE_NAME, "", clazz);
    }

    public void registerInstallPackageService(
            Class<? extends InstallPackageService> clazz) {
        ExtensionPoint extensionPoint = extensionPoints.add(
                INSTALLER_MANAGER_EXTENSION_POINT, "");
        extensionPoint.append(INSTALLER_EXECUTION_SERVICE_NAME, "", clazz);
    }

    public Service getService(DynObject parameters) throws ServiceException {
        return null;
    }

    public String getPackageSetFileName(PackageInfo info) {
        Object[] parameters = getPackageNameFormatParameters(info);
        return MessageFormat.format(getPackageSetNameFormat(), parameters);
    }

    public String getPackageFileName(PackageInfo info) {
        Object[] parameters = getPackageNameFormatParameters(info);
        return MessageFormat.format(getPackageNameFormat(), parameters);
    }

    public String getPackageIndexFileName(PackageInfo info) {
        Object[] parameters = getPackageNameFormatParameters(info);
        return MessageFormat.format(getPackageIndexNameFormat(), parameters);
    }

    private Object[] getPackageNameFormatParameters(PackageInfo info) {
        Object[] parameters = new Object[8];
        parameters[PACKAGE_FILE_NAME_FIELDS.GVSIG_VERSION] = info
                .getGvSIGVersion();
        parameters[PACKAGE_FILE_NAME_FIELDS.NAME] = info.getCode();
        parameters[PACKAGE_FILE_NAME_FIELDS.VERSION] = info.getVersion();
        parameters[PACKAGE_FILE_NAME_FIELDS.BUILD] = info.getBuild();
        parameters[PACKAGE_FILE_NAME_FIELDS.STATE] = info.getState();
        parameters[PACKAGE_FILE_NAME_FIELDS.OS] = info.getOperatingSystem();
        parameters[PACKAGE_FILE_NAME_FIELDS.ARCH] = info.getArchitecture();
        parameters[PACKAGE_FILE_NAME_FIELDS.JVM] = info.getJavaVM();
        return parameters;
    }

    public PackageInfo[] getInstalledPackages(File pluginsDirectory)
            throws MakePluginPackageServiceException {
        MakePluginPackageService service = getMakePluginPackageService();
        return service.getInstalledPackages();
    }

    public PackageInfo[] getInstalledPackages()
            throws MakePluginPackageServiceException {
        MakePluginPackageService service = getMakePluginPackageService();
        return service.getInstalledPackages();
    }

    public String getDefaultPackageFileExtension() {
        return "gvspkg";
    }

    public String getDefaultPackageSetFileExtension() {
        return "gvspks";
    }

    public String getDefaultIndexSetFileExtension() {
        return "gvspki";
    }
	
    private void loadOperatingSystemInfo() {
        String osname = System.getProperty("os.name");
        String osversion = System.getProperty("os.version");
        if (osname.toLowerCase().startsWith("linux")) {
            this.operatingSystemFamily = PackageManager.OS.LINUX;
            FileInputStream fis = null;
            try {
                Properties p = new Properties();
                fis = new FileInputStream("/etc/os-release");
                p.load(fis);
                this.operatingSystemName = p.getProperty("ID", null);
                this.operatingSystemVersion = p.getProperty("VERSION_ID", null);
            } catch(Exception ex) {

            } finally {
                IOUtils.closeQuietly(fis);
            }

        } else if (osname.toLowerCase().startsWith("window")) {
            this.operatingSystemFamily = PackageManager.OS.WINDOWS;
            String s = osname.replace(" ", "");
            s = s.replace("_", "");
            s = s.replace("-", "");
            this.operatingSystemName = s;
            this.operatingSystemVersion = osversion;

        } else {
            String s = osname.replace(" ", "");
            s = s.replace("_", "");
            s = s.replace("-", "");
            this.operatingSystemFamily = s;
            this.operatingSystemName = null;
            this.operatingSystemVersion = osversion;
        }
    }

    @Override
    public String getOperatingSystem() {
        if( this.operatingSystemFamily == null ) {
            loadOperatingSystemInfo();
        }
        StringBuilder builder = new StringBuilder();
        builder.append(this.operatingSystemFamily);
        if( this.operatingSystemName!=null ) {
            builder.append("_");
            builder.append(this.operatingSystemName);
            if( this.operatingSystemVersion!=null ) {
                builder.append("_");
                builder.append(this.operatingSystemVersion);
            }
        }
        return builder.toString();
    }

    @Override
    public String getOperatingSystemFamily() {
        if( this.operatingSystemFamily == null ) {
            loadOperatingSystemInfo();
        }
        return this.operatingSystemFamily;
    }

    @Override
    public String getOperatingSystemName() {
        if( this.operatingSystemFamily == null ) {
            loadOperatingSystemInfo();
        }
        return this.operatingSystemName;
    }

    @Override
    public String getOperatingSystemVersion() {
        if( this.operatingSystemFamily == null ) {
            loadOperatingSystemInfo();
        }
        return this.operatingSystemVersion;
    }

    @Override
    public String getArchitecture() {
        String osarch = System.getProperty("os.arch");
        if (osarch.toLowerCase().startsWith("i386")) {
                return ARCH.X86;
        }
        if (osarch.toLowerCase().startsWith("i686")) {
                return ARCH.X86;
        }
        if (osarch.toLowerCase().startsWith("x86_64")) {
                return ARCH.X86_64;
        }
        if (osarch.toLowerCase().startsWith("x86")) {
                return ARCH.X86;
        }
        if (osarch.toLowerCase().startsWith("amd64")) {
                return ARCH.X86_64;
        }
        return osarch;	
    }

    public Dependency createDependency(PackageInfo packageInfo) {
        return new DefaultDependency(packageInfo);
    }

    public Dependency createDependency() {
        return new DefaultDependency();
    }

    public DependenciesCalculator createDependenciesCalculator(
            InstallPackageService installService) {
        return new DefaultDependenciesCalculator(installService);
    }

    public Version createVersion() {
        if ( version == null ) {
            return new DefaultVersion();
        }
        Version v = null;
        try {
            v = (Version) version.clone();
        } catch (CloneNotSupportedException e) {
            // Version clone can't trow exception
        }
        return v;
    }

    public PackageInfoReader getDefaultPackageInfoReader() {
        return new InstallerInfoFileReader();
    }

    public PackageInfoWriter getDefaultPackageInfoWriter() {
        return new InstallerInfoFileWriter();
    }

    public MakePackageService createMakePackage(File packageFolder,
            PackageInfo packageInfo) {
        return new DefaultMakePackageService(this, packageFolder, packageInfo);
    }

    public PackageInfo createPackageInfo() {
        return new DefaultPackageInfo();
    }

    public PackageInfo createPackageInfo(InputStream stream) throws BaseException {
        PackageInfo pkg = new DefaultPackageInfo();
        PackageInfoReader reader = this.getDefaultPackageInfoReader();
        reader.read(pkg, stream);
        return pkg;
    }

    public PackageInfo createPackageInfo(File file) throws BaseException {
        FileInputStream fis = null;
        PackageInfo pkg = null;
        try {
            fis = new FileInputStream(file);
            pkg = this.createPackageInfo(fis);
            fis.close();
        } catch (Exception ex) {

        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                //
            }
        }
        return pkg;
    }

    public URL getDownloadBaseURL() {
        return this.BaseDownloadURL;
    }

    public String getVersion() {
        return this.version.toString();
    }

    public void setVersion(Version version) {
        try {
            this.version = (Version) version.clone();
        } catch (CloneNotSupportedException e) {
            // This should not happen
        }

    }

    public Version getVersionEx() {
        try {
            return (Version) this.version.clone();
        } catch (CloneNotSupportedException e) {
            // This should not happen
            return null;
        }
    }

    public void setDownloadBaseURL(URL url) {
        this.BaseDownloadURL = url;
    }

    public void setVersion(String version) {
        if ( this.version == null ) {
            this.version = new DefaultVersion();
        }
        this.version.parse(version);
    }

    public File getDefaultLocalAddonRepository() {
        File f = this.defaultRepositoryLocation.get("plugin");
        return f;
    }

    public void setDefaultLocalAddonRepository(File defaultAddonsRepository) {
        this.defaultRepositoryLocation.put("plugin", defaultAddonsRepository);
        this.localRepositoriesLocation.add(new LocalRepositoryLocation(defaultAddonsRepository, "plugin"));
    }

    public void addLocalAddonRepository(File path) {
        this.addLocalAddonRepository(path, "plugin");
    }

    public File getDefaultLocalAddonRepository(String packageType) {
        return this.getDefaultLocalAddonRepository(packageType,ACCESS_READ);
    }

    public boolean needAdminRights() {
       List<File> folders = getLocalAddonRepositories(); 
       for( File folder : folders) {
           if( !canWrite(folder) ) {
               return true;
           }
       }
       return false;
    }
    
    private boolean canWrite(File f) {
        if( !f.canWrite() ) {
            return false;
        }
        // Esto requiere java 1.7 o superior y aun debemos ejecutar con 1.6
//        Path path = FileSystems.getDefault().getPath(f.getAbsolutePath());
//        boolean b = Files.isWritable(path);
//        return b;
        
//        En MS Windows File.canWrite retorna true aunque luego no se pueden crear 
//        escribir en esa carpeta, asi que probamos a crear una carpeta para 
//        asegurarnos si se puede escribir realmente.
        File f2 = new File(f,"test.dir");
        if( f2.mkdir() ) {
            f2.delete();
            return true;
        }
        return false;
    }
    
    public File getDefaultLocalAddonRepository(String packageType, int access) {
        File f = this.defaultRepositoryLocation.get(packageType);
        switch(access) {
        case ACCESS_WRITE:
            if( canWrite(f) ) {
                return f;
            }
            break;
        case ACCESS_READ:
        default:
            if( f.canRead()) {
                return f;
            }
            break;
        }
        List<File> repositoriesLocaltions = this.getLocalAddonRepositories(packageType);
        for( File repositoryLocation : repositoriesLocaltions ) {
            switch(access) {
            case ACCESS_WRITE:
                if( canWrite(repositoryLocation) ) {
                    return repositoryLocation;
                }
                break;
            case ACCESS_READ:
            default:
                if( repositoryLocation.canRead()) {
                    return repositoryLocation;
                }
                break;
            }
        }
        return null;
    }

    public void setDefaultLocalAddonRepository(File defaultAddonsRepository, String packageType) {
        this.defaultRepositoryLocation.put(packageType, defaultAddonsRepository);
        this.localRepositoriesLocation.add(new LocalRepositoryLocation(defaultAddonsRepository, packageType));
    }

    public void addLocalAddonRepository(File path, String type) {
        localRepositoriesLocation.add(new LocalRepositoryLocation(path, type));
    }

    public String getDefaultLocalRepositoryType(File file) {
        Iterator<LocalRepositoryLocation> it = localRepositoriesLocation.iterator();
        while ( it.hasNext() ) {
            LocalRepositoryLocation location = it.next();
            if ( location.contains(file) ) {
                return location.getDefaultType();
            }
        }
        return null;
    }

    public List<File> getLocalAddonRepositories() {
        return this.getLocalAddonRepositories(null);
    }

    public List<File> getLocalAddonRepositories(String type) {
        List<File> l = new ArrayList<File>();
        Iterator<LocalRepositoryLocation> it = localRepositoriesLocation.iterator();
        while ( it.hasNext() ) {
            LocalRepositoryLocation location = it.next();
            if( type==null || location.support(type) ) {
                l.add(location.getLocation());
            }
        }
        return l;
    }

    public List<File> getAddonFolders() {
        return this.getAddonFolders(null);
    }
    
    public List<File> getAddonFolders(String type) {
        List<File> addonFolders = new ArrayList<File>();

        // Para cada directorio en la lista de repositorios locales
        List<File> localAddonRepositories = this.getLocalAddonRepositories(type);
        for ( int i = 0; i < localAddonRepositories.size(); i++ ) {
            File repoPath = localAddonRepositories.get(i);
            if ( repoPath.isDirectory() && repoPath.exists() ) {
                File[] folderRepoList = repoPath.listFiles();

                // recorrer los directorios que haya dentro
                for ( int j = 0; j < folderRepoList.length; j++ ) {
                    File addonFolder = folderRepoList[j];
                    if ( addonFolder.isDirectory() ) {
                        File pkginfofile = new File(addonFolder, "package.info");
                        if ( pkginfofile.exists() ) {
                            addonFolders.add(addonFolder);
                        }
                    }

                }
            }
        }

        return addonFolders;
    }

    public File getAddonFolder(String code) {
        List<File> packagePaths = this.getAddonFolders();
        for ( int i = 0; i < packagePaths.size(); i++ ) {
            try {
                File pkgfile = new File(packagePaths.get(i), "package.info");
                PackageInfo pkg = this.createPackageInfo(pkgfile);
                if ( pkg.getCode().equalsIgnoreCase(code) ) {
                    return packagePaths.get(i);
                }
            } catch (Exception ex) {
                
            }
        }
        return null;
    }

    public List<byte[]> getPublicKeys() {
        byte[] rawkey;
        try {
            InputStream is = this.getClass().getResourceAsStream("/org/gvsig/installer/lib/keys/key.public");
            rawkey = new byte[is.available()];
            is.read(rawkey);
            is.close();
        } catch (IOException e) {
            return null;
        }
        List<byte[]> keys = new ArrayList<byte[]>();
        keys.add(rawkey);
        return keys;
    }

    public boolean hasProviderToThisPackage(PackageInfo packageInfo) {
        InstallerProviderManager provmgr = (InstallerProviderManager) this.getProviderManager();
        try {
            return provmgr.getProviderFactory(packageInfo.getType()) != null;
        } catch (Exception e) {
            return false;
        }
    }

}
