/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.info;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallerInfoTags {

	public static final String CODE = "code";
	public static final String CODEALIAS = "code-alias";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String VERSION = "version";
	public static final String BUILD = "buildNumber";
	public static final String BUILD_OLD = "build";
	public static final String STATE = "state";
	public static final String OFFICIAL = "official";
	public static final String TYPE = "type";
	public static final String OS = "operating-system";
	public static final String ARCHITECTURE = "architecture";
	public static final String JVM = "java-version";
	public static final String GVSIG_VERSION = "gvSIG-version";
	public static final String DOWNLOAD_URL = "download-url";
	public static final String MODEL_VERSION = "model-version";
	public static final String OWNER = "owner";
	public static final String OWNER_URL = "owner-url";
	public static final String SOURCES_URL = "sources-url";
	public static final String WEB_URL = "web-url";
	public static final String DEPENDENCIES = "dependencies";
	public static final String CATEGORIES = "categories";
}
