/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.lib.impl.creation;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.impl.DefaultPackageInfo;
import org.gvsig.installer.lib.impl.utils.DeleteFile;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.tools.service.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class DefaultMakePluginPackageService implements
		MakePluginPackageService {

	private static final Logger LOG = LoggerFactory
			.getLogger(DefaultMakePluginPackageService.class);

	public static final String ANT_FILE_NAME = "install.xml";
	public static final String COPIED_FILES_DIRECTORY_NAME = "files";
	private static final Logger logger = org.slf4j.LoggerFactory
			.getLogger(DefaultMakePluginPackageService.class);
	private final InstallerManager manager;

	private List<PackageInfo> installerInfos = null;

	protected String antScript = null;
	private InstallPackageProviderServices installerProviderServices = null;

	public DefaultMakePluginPackageService(InstallerManager manager)
			throws MakePluginPackageServiceException {
		super();
		this.manager = manager;
		this.installerInfos = new ArrayList<PackageInfo>();
		this.installerProviderServices = InstallerProviderLocator
				.getProviderManager().createInstallerProviderServices();
		initialize();
	}

	private void initialize() throws MakePluginPackageServiceException {

		List<File> addonFolders = manager.getAddonFolders();

		if (addonFolders == null) {
			throw new MakePluginPackageServiceException(
					"The addonFolders list is null");
		}
		// else if (addonFolders.size() <= 0) {
		// LOG.info("The addonFolders list is empty");
		// }
		else {
			// Read all the installed plugins
			for (int i = 0; i < addonFolders.size(); i++) {
				File pluginDirectoryFile = addonFolders.get(i);

				if (!pluginDirectoryFile.exists()) {
					pluginDirectoryFile.mkdirs();
					LOG.info("Plugins folder created: {}", pluginDirectoryFile);
				}

				DefaultPackageInfo packageInfo = new DefaultPackageInfo();

				installerProviderServices.readPackageInfo(new File(
						pluginDirectoryFile.getAbsolutePath()), packageInfo);

				// Checks if the ant file exists
				File antFile = getAntFile(pluginDirectoryFile);
				if (antFile.exists()) {
					try {
						packageInfo.setAntScript(readFileAsString(antFile
								.getAbsolutePath()));
					} catch (IOException e) {
						logger.error("Not possible to read the ant file");
					}
				}

				// Checks if there are files to copy
				File copiedFilesDirectory = getCopiedFilesDirectory(pluginDirectoryFile);
				if (copiedFilesDirectory.exists()) {
					loadCopiedFiles(copiedFilesDirectory, copiedFilesDirectory,
							packageInfo);
				}

				installerInfos.add(packageInfo);
			}
		}
	}

	private void loadCopiedFiles(File file, File filesDirectory,
			PackageInfo packageInfo) throws MakePluginPackageServiceException {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				loadCopiedFiles(files[i], filesDirectory, packageInfo);
			}
		} else {
			// Removing the plugin prefix

			String pluginFileName = file.getAbsolutePath().substring(
					filesDirectory.getAbsolutePath().length(),
					file.getAbsolutePath().length());

			File pluginFile = new File(System.getProperty("user.dir")
					+ pluginFileName);

			packageInfo.getFilesToCopy().add(pluginFile);
		}
	}

	public void createPackageSet(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException {

		LOG.debug("Creating a package set of the package info: \n{0}",
				packageInfo);

		String pluginFileName = manager.getPackageFileName(packageInfo);
		installerProviderServices.compressPackageSet(
				getAbsolutePluginPackageDirectory(packageInfo), pluginFileName,
				packageStream);
	}

	public void preparePackage(PackageInfo packageInfo, File originalPluginDir)
			throws MakePluginPackageServiceException {

		LOG.debug("Preparing a package set of the package info: \n{0}",
				packageInfo);
		// Write the package.info file
		writePackageInfo(packageInfo);

		if (packageInfo.getAntScript() != null) {
			// Create the ant file
			writeAntFile(packageInfo);

			// Copy the selected files
			writeSelectedFiles(packageInfo);
		}
	}

	public void createPackage(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException {

		LOG.debug("Creating package of the package info: \n{0}", packageInfo);
		installerProviderServices.compressPackage(
				getAbsolutePluginPackageDirectory(packageInfo), packageStream);
	}

	public void createPackageIndex(PackageInfo packageInfo,
			OutputStream packageStream)
			throws MakePluginPackageServiceException {
		LOG.debug("Creating package index of the package info: \n{0}",
				packageInfo);

		installerProviderServices.compressPackageIndex(
				getAbsolutePluginPackageDirectory(packageInfo), packageStream);
	}

	private void writePackageInfo(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		// conseguir directorio destino
		writePackageInfo(packageInfo,
				getAbsolutePluginPackageDirectory(packageInfo));
	}

	public void writePackageInfo(PackageInfo packageInfo, File folder)
			throws MakePluginPackageServiceException {
		installerProviderServices.writePackageInfo(folder, packageInfo);
	}

	public void writePackageInfoForIndex(PackageInfo packageInfo, File folder)
			throws MakePluginPackageServiceException {
		installerProviderServices.writePackageInfoForIndex(folder, packageInfo);
	}

	private void createInstallDirInPluginDir(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		File pluginDirectory;
		pluginDirectory = getAbsolutePluginPackageDirectory(packageInfo);
		File installdir = new File(pluginDirectory, "install");
		installdir.mkdir();

	}

	private void writeAntFile(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		try {
			createInstallDirInPluginDir(packageInfo);
			ByteArrayInputStream in = new ByteArrayInputStream(packageInfo
					.getAntScript().getBytes());
			OutputStream out = new FileOutputStream(
					getAbsoluteAntFile(packageInfo));

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Exception writing the ant file");
		}

	}

	private void writeSelectedFiles(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		try {
			createInstallDirInPluginDir(packageInfo);
			String copiedFilesDirectoryName = getAbsoulteCopiedFilesDirectoryName(packageInfo);

			// Deleting the previous files folder
			File copiedFilesDirectoryfile = new File(copiedFilesDirectoryName);
			deleteDirectory(copiedFilesDirectoryfile);

			// It works if the plugins directory is in the folder
			// "gvSIG/extensiones"
			String applicationDirectory = System.getProperty("user.dir");

			List<File> files = packageInfo.getFilesToCopy();

			for (int i = 0; i < files.size(); i++) {
				String sourceFile = files.get(i).getAbsolutePath();

				// Create the final path
				String destFile = sourceFile.substring(applicationDirectory
						.length(), sourceFile.length());
				destFile = copiedFilesDirectoryName + destFile;

				// Copy the files
				copy(new File(sourceFile), new File(destFile));
			}
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Exception copying the files", e);
		}
	}

	static public boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	private void copy(File sourceLocation, File targetLocation)
			throws IOException {
		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copy(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {
			targetLocation.getParentFile().mkdirs();

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	private File getCopiedFilesDirectory(File pluginDirectory)
			throws MakePluginPackageServiceException {
		return new File(pluginDirectory.getAbsolutePath() + File.separator
				+ "install" + File.separator + COPIED_FILES_DIRECTORY_NAME);
	}

	private String getAbsoulteCopiedFilesDirectoryName(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		return getAbsolutePluginPackageDirectory(packageInfo).getAbsolutePath()
				+ File.separator + "install" + File.separator
				+ COPIED_FILES_DIRECTORY_NAME;
	}

	private File getAntFile(File pluginDirectory)
			throws MakePluginPackageServiceException {
		return new File(pluginDirectory.getAbsolutePath() + File.separator
				+ "install" + File.separator + ANT_FILE_NAME);
	}

	private File getAbsoluteAntFile(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		String destinationFilePath = manager.getDefaultLocalAddonRepository()
				.getAbsolutePath()
				+ File.separator + packageInfo.getCode();
		return new File(destinationFilePath + File.separator + "install" + File.separator  + ANT_FILE_NAME);
	}

	private File getAbsolutePluginPackageDirectory(PackageInfo packageInfo)
			throws MakePluginPackageServiceException { 
            File folder = manager.getAddonFolder(packageInfo.getCode());
            // Si existe un addon para ese packageinfo lo usamos, y si no
            // devolbemos la carpeta del addon en el repositorio local por defecto
            // para ese tipo de addon en el que se pueda escribir para que se
            // pueda crear alli el addon.
            if( folder == null ) {
                folder = manager.getDefaultLocalAddonRepository(packageInfo.getType(), InstallerManager.ACCESS_WRITE);
                folder = new File(folder, packageInfo.getCode());
            }
            return folder.getAbsoluteFile();
	}

	public Manager getManager() {
		return this.manager;
	}

	public PackageInfo getPluginPackageInfo(int index) {
		if (index >= installerInfos.size()) {
			return null;
		}
		return installerInfos.get(index);
	}

	public PackageInfo getPluginPackageInfo(String code) {
		for (int i = 0; i < getPluginPackageCount(); i++) {
			if (installerInfos.get(i).getCode() != null
					&& installerInfos.get(i).getCode().equals(code)) {
				return installerInfos.get(i);
			}
		}
		PackageInfo info = new DefaultPackageInfo();
		info.setCode(code);
		return info;
	}

	public int getPluginPackageCount() {
		return installerInfos.size();
	}

	public String getDefaultAntScript()
			throws MakePluginPackageServiceException {
		try {
			URL resource = getClass().getResource(ANT_FILE_NAME);
			return readUrlAsString(resource);
		} catch (IOException e) {
			throw new MakePluginPackageServiceException(
					"Impossible to read the default ant file", e);
		}
	}

	private String readUrlAsString(URL url) throws java.io.IOException {
		URLConnection urlConnection = url.openConnection();
		InputStream inputStream = urlConnection.getInputStream();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		StringBuffer fileData = new StringBuffer(1000);
		String line = bufferedReader.readLine();
		while (line != null) {
			fileData.append(line + "\n");
			line = bufferedReader.readLine();
		}
		bufferedReader.close();
		return fileData.toString();
	}

	private String readFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}

	public PackageInfo[] getInstalledPackages()
			throws MakePluginPackageServiceException {
		return installerInfos.toArray(new PackageInfo[installerInfos.size()]);
	}

	public File getPluginFolder(PackageInfo packageInfo)
			throws MakePluginPackageServiceException {
		return getAbsolutePluginPackageDirectory(packageInfo);
	}

	public boolean deleteDir(File folder) {
		DeleteFile file = new DeleteFile();
		return file.delete(folder);
	}

}
