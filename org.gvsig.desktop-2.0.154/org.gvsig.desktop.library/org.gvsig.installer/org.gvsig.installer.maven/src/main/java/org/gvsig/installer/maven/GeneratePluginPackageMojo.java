/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.maven;

import java.io.OutputStream;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;

/**
 * Maven mojo to launch the gvSIG installer to generate a package of a gvSIG
 * plugin.
 * <p>
 * Look at the <a href="http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/anexos/proyectos-oficiales-en-gvsig/nombrado-de-binarios-para-un-plugin-de-gvsig"
 * >gvSIG plugin naming standard</a> for information about installers naming and
 * versioning.
 * </p>
 * 
 * @see InstallerManager
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 * @goal create-package
 */
public class GeneratePluginPackageMojo extends AbstractGeneratePackageMojo {

	@Override
	protected void createPackage(MakePluginPackageService makePluginService,
			PackageInfo info, OutputStream os)
			throws MakePluginPackageServiceException {
		makePluginService.createPackage(info, os);
	}

	@Override
	protected String getPackageTypeName() {
		return "Package";
	}

	@Override
	protected String getPackageFileName(PackageInfo info) {
		return InstallerLocator.getInstallerManager().getPackageFileName(info);
	}
}
