/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.maven;

import java.io.File;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;

/**
 * Maven mojo to launch the gvSIG installer to generate a package index of a
 * gvSIG plugin.
 * <p>
 * Look at the <a href="http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/anexos/proyectos-oficiales-en-gvsig/nombrado-de-binarios-para-un-plugin-de-gvsig"
 * >gvSIG plugin naming standard</a> for information about installers naming and
 * versioning.
 * </p>
 * 
 * @see InstallerManager
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 * @goal create-package-index
 */
public class GeneratePluginPackageIndexMojo extends AbstractGeneratePackageMojo {

	/**
	 * Remote plugin package base download URL. The base path URL where the
	 * installation package will be located.
	 * 
	 * @required
	 * @parameter
	 */
	private String baseDownloadURL;

	@Override
	protected void createPackage(MakePluginPackageService makePluginService,
			PackageInfo info, OutputStream os)
			throws MakePluginPackageServiceException {

		File pluginFolder = makePluginService.getPluginFolder(info);

		PackageInfo infoIndex;
		try {
			infoIndex = (PackageInfo) info.clone();
		} catch (CloneNotSupportedException e) {
			throw new MakePluginPackageServiceException(
					"Error cloning the package info: " + info, e);
		}
        
	    InstallerManager manager = InstallerLocator.getInstallerManager();
		String fullDownloadURL = baseDownloadURL
				+ "/"
				+ manager.getPackageFileName(info);
    	infoIndex.setDownloadURL(fullDownloadURL);

		makePluginService.writePackageInfoForIndex(infoIndex, pluginFolder);
		makePluginService.createPackageIndex(info, os);
	}

	@Override
	protected String getPackageTypeName() {
		return "PackageIndex";
	}

	@Override
	protected String getPackageFileName(PackageInfo info) {
		return InstallerLocator.getInstallerManager().getPackageIndexFileName(
				info);
	}
}
