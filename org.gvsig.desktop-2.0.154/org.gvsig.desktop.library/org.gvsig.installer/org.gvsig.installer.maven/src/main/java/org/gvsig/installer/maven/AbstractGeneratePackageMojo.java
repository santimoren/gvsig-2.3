/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.maven;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.installer.lib.impl.utils.MD5BinaryFileUtils;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;
import org.gvsig.tools.locator.LocatorException;

/**
 * Abstract Maven mojo to launch the gvSIG installer to generate a package of a
 * gvSIG plugin.
 * <p>
 * Look at the <a href="http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/anexos/proyectos-oficiales-en-gvsig/nombrado-de-binarios-para-un-plugin-de-gvsig"
 * >gvSIG plugin naming standard</a> for information about installers naming and
 * versioning.
 * </p>
 * 
 * @see InstallerManager
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractGeneratePackageMojo extends AbstractMojo {

	/**
	 * Location of the gvSIG plugins folder.
	 * 
	 * @parameter
	 * @required
	 */
	private File pluginsFolder;

	/**
	 * Location of the folder where to create the package file.
	 * 
	 * @parameter
	 * @required
	 */
	private File packageFolder;

	/**
	 * Name of the package file. If not provided, the official gvSIG name will
	 * be used, as provided by the org.gvsig.installer.lib.api.InstallerManager.
	 * 
	 * @parameter
	 */
	private String packageFileName;

	/**
	 * Plugin project artifactId or code, used as gvSIG plugin name.
	 * 
	 * @parameter expression="${project.artifactId}"
	 * @required
	 */
	private String artifactId;

	/**
	 * Plugin project packaging, to check it is of jar type.
	 * 
	 * @parameter expression="${project.packaging}"
	 * @required
	 */
	private String packaging;

	/**
	 * If the mojo execution is disabled. Useful for projects that inherit the
	 * maven plugin configuration but don't generate installer.
	 * 
	 * @parameter
	 */
	private boolean disabled = false;

	public void execute() throws MojoExecutionException, MojoFailureException {
		String packageName;
		MakePluginPackageService makePluginService;
		File packageBundleFile = null;

		Log log = getLog();

		if (disabled) {
			log.info(getPackageTypeName() + " generation disabled.");
			return;
		}

		if (!"jar".equals(packaging)) {
			log
					.info("Running on a project with packaging of type "
							+ packaging
							+ ". Do nothing, as we only create installers for projects "
							+ "with jar packaging");
			return;
		}

		log.info("Generating a " + getPackageTypeName() + " for the plugin: "
				+ artifactId + " with the following information:");
		log.info("\tgvSIG Plugin's folder: " + pluginsFolder);
		log.info("\tPackage file destination folder: " + packageFolder);

		try {
			new DefaultLibrariesInitializer().fullInitialize();

			InstallerManager manager = InstallerLocator.getInstallerManager();
            manager.setDefaultLocalAddonRepository(pluginsFolder);
			makePluginService = manager.getMakePluginPackageService();
		} catch (LocatorException e) {
			throw new MojoExecutionException(
					"Error getting a reference to the InstallerManager", e);
		} catch (MakePluginPackageServiceException e) {
			throw new MojoExecutionException(
					"Error getting a MakePluginPackageService for the "
							+ " plugin folder: " + pluginsFolder, e);
		}

		// Get and fill the package info data
		PackageInfo info = makePluginService.getPluginPackageInfo(artifactId);

		log.info("Creating package of the package info:");
		log.info(info.toString());

		// Create the package bundle file
		packageName = packageFileName == null ? getPackageFileName(info)
				: packageFileName;
		if (!packageFolder.exists()) {
			packageFolder.mkdirs();
		}
		packageBundleFile = new File(packageFolder, packageName);

		try {
			FileOutputStream fos = new FileOutputStream(packageBundleFile);
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			createPackage(makePluginService, info, bos);

			bos.flush();
			bos.close();
			fos.close();
			
			writeMD5FileForBinaryFile(packageBundleFile);
		} catch (FileNotFoundException e) {
			throw new MojoExecutionException("Error creating the "
					+ getPackageTypeName() + " file: " + packageBundleFile, e);
		} catch (MakePluginPackageServiceException e) {
			throw new MojoExecutionException(
					"Error creating the package file: " + packageName
							+ ", for the plugin: " + artifactId, e);
		} catch (IOException e) {
			throw new MojoExecutionException("I/O error writing the "
					+ getPackageTypeName() + " file: " + packageBundleFile, e);
		}

		log.info(getPackageTypeName() + " file created successfully as: "
				+ packageName);
	}

    private void writeMD5FileForBinaryFile(File the_file) throws IOException {
        
        String md5_str = "";
        try {
            md5_str = MD5BinaryFileUtils.getMD5Checksum(the_file);
        } catch (Exception ex) {
            throw new IOException(
                "Unable to compute MD5 checksum for file: "
                + the_file.getAbsolutePath() + " (" + ex.getMessage() + ")");
        }
        
        String separator = " *";
        // for ascii file: separator = "  ";
        
        md5_str = md5_str + separator + the_file.getName();
        
        File md5_file = new File(the_file.getAbsolutePath() + ".md5");
        if (md5_file.exists()) {
            md5_file.delete();
        }

        FileOutputStream fos = new FileOutputStream(md5_file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        
        bos.write(md5_str.getBytes());

        bos.flush();
        bos.close();
        fos.close();
    }

    /**
	 * Returns the name of the package file to create.
	 * 
	 * @param info
	 *            package information
	 * @return the name of the file to create
	 */
	protected abstract String getPackageFileName(PackageInfo info);

	/**
	 * Returns the name of the package type to create.
	 * 
	 * @return the name of the package type to create
	 */
	protected abstract String getPackageTypeName();

	/**
	 * Creates the package file into the given {@link OutputStream}.
	 * 
	 * @param makePluginService
	 *            to use to create the package
	 * @param info
	 *            the information of the package to create
	 * @param os
	 *            where to create the package file
	 * @throws MakePluginPackageServiceException
	 *             if there is an error creating the package file
	 */
	protected abstract void createPackage(
			MakePluginPackageService makePluginService, PackageInfo info,
			OutputStream os) throws MakePluginPackageServiceException;
}
