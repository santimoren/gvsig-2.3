/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.installer.maven;

import java.io.File;
import java.net.URL;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;

import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.gvsig.installer.lib.api.creation.MakePluginPackageServiceException;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;
import org.gvsig.tools.locator.LocatorException;

/**
 * Maven mojo to launch the gvSIG installer to generate a file with the package
 * info.
 * <p>
 * Look at the <a href="http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/anexos/proyectos-oficiales-en-gvsig/nombrado-de-binarios-para-un-plugin-de-gvsig"
 * >gvSIG plugin naming standard</a> for information about installers naming and
 * versioning.
 * </p>
 * 
 * @see InstallerManager
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 * @goal write-info
 */
public class GeneratePackageInfoMojo extends AbstractMojo {

	/**
	 * Location of the gvSIG plugins folder.
	 * 
	 * @parameter
	 * @required
	 */
	private File pluginsFolder;

	/**
	 * Plugin project artifactId or code, used as gvSIG plugin name.
	 * 
	 * @parameter expression="${project.artifactId}"
	 * @required
	 */
	private String artifactId;

	/**
	 * Plugin project packaging, to check it is of jar type.
	 * 
	 * @parameter expression="${project.packaging}"
	 * @required
	 */
	private String packaging;

	/**
	 * Plugin project name.
	 * 
	 * @parameter expression="${project.name}"
	 * @required
	 */
	private String name;

	/**
	 * Plugin project description.
	 * 
	 * @parameter expression="${project.description}"
	 * @required
	 */
	private String description;

	/**
	 * Plugin project version.
	 * 
	 * @parameter expression="${project.version}"
	 * @required
	 */
	private String version;

	/**
	 * Plugin build number.
	 * 
	 * @parameter
	 */
	private int buildNumber = -1;

	/**
	 * Plugin state. One of: devel, pilot, prototype, alpha[num], beta[num],
	 * RC[num], final.
	 * 
	 * Defaults to "devel".
	 * 
	 * @parameter
	 */
	private String state;

	/**
	 * If the plugin is a gvSIG official one.
	 * 
	 * @parameter
	 * @required
	 */
	private boolean official;

	/**
	 * The supported operating system. Examples: lin (linux), win (windows),
	 * osx_10_4, osx_10_5, osx_10_6.
	 * 
	 * Defaults to all.
	 * 
	 * @parameter
	 */
	private String operatingSystem;

	/**
	 * The supported architecture. Examples: x86 (32 bits), x86_64 (64 bits).
	 * 
	 * Defaults to all.
	 * 
	 * @parameter
	 */
	private String architecture;

	/**
	 * Minimum Java VM version supported. Example: j1_5, j1_6.
	 * 
	 * Defaults to j1_5.
	 * 
	 * @parameter
	 */
	private String javaVM;

	/**
	 * The minimum gvSIG version supported.
	 * 
	 * @parameter
	 * @required
	 */
	private String gvSIGVersion;

	/**
	 * Installabe type. Only <strong>plugin</strong> is supported.
	 * 
	 * @parameter default="${plugin}"
	 */
	private String installableType = "plugin";

	/**
	 * If the mojo execution is disabled. Useful for projects that inherit the
	 * maven plugin configuration but don't generate installer.
	 * 
	 * @parameter
	 */
	private boolean disabled = false;

	/**
	 * Plugin project folder where to store the package info.
	 * 
	 * @parameter expression="${basedir}"
	 * @required
	 */
	private File packageInfoFolder;

	/**
	 * Dependencies with other packages
	 * 
	 * @parameter
	 */
	private String pkgdependencies;

	/**
	 * Owner of the package.
	 * 
	 * @parameter
	 */
	private String owner;

	/**
	 * URL of the plugin sources.
	 * 
	 * @parameter
	 */
	private URL sourcesURL;

	/**
	 * URL of the project's web.
	 * 
	 * @parameter
	 */
	private URL webURL;

	/**
	 * addon categories.
	 * 
	 * @parameter
	 */
	private String categories;

	public void execute() throws MojoExecutionException, MojoFailureException {
		Log log = getLog();

		if (disabled) {
			log.info("Package info generation disabled.");
			return;
		}
		if( buildNumber == -1 ) {
			log.info("Skip package info generation, undefined buildNumber property.");
			return;
		}

		if (!"jar".equals(packaging)) {
			log
					.info("Running on a project with packaging of type "
							+ packaging
							+ ". Do nothing, as we only create installers for projects "
							+ "with jar packaging");
			return;
		}
		packageInfoFolder = new File(packageInfoFolder, "target") ;
		
		log.info("Generating the package info for the plugin: " + artifactId
				+ " with the following information:");
		log.info("\tPlugin name: " + name);
		log.info("\tPlugin description: " + description);
		log.info("\tPlugin version: " + version);
		log.info("\tPlugin build number: " + buildNumber);
		log.info("\tPlugin state: " + state);
		log.info("\tPlugin official: " + official);
		log.info("\tPlugin operatingSystem: " + operatingSystem);
		log.info("\tPlugin architecture: " + architecture);
		log.info("\tPlugin javaVM: " + javaVM);
		log.info("\tPlugin gvSIGVersion: " + gvSIGVersion);
		log.info("\tgvSIG Plugin's folder: " + pluginsFolder);
		log.info("\tPackage info folder: " + packageInfoFolder);
		log.info("\tDependencies: " + pkgdependencies);
		log.info("\tOwner: " + owner);
		log.info("\tSources URL: " + sourcesURL);
		log.info("\tWeb URL: " + webURL);
		log.info("\tCategories: " + categories);

		try {
			new DefaultLibrariesInitializer().fullInitialize();

			InstallerManager manager = InstallerLocator.getInstallerManager();
			
			manager.setDefaultLocalAddonRepository(pluginsFolder);
			MakePluginPackageService makePluginService = manager.getMakePluginPackageService();

			// Get and fill the package info data
			PackageInfo info = makePluginService.getPluginPackageInfo(artifactId);

			// info.setCode(artifactId);
			info.setDescription(description);
			info.setName(name);
			info.setOfficial(official);
			info.setState(state);
			info.setType(installableType);
			info.setVersion(version);
			info.setBuild(buildNumber);
			info.setOperatingSystem(operatingSystem);
			info.setArchitecture(architecture);
			info.setJavaVM(javaVM);
			info.setGvSIGVersion(gvSIGVersion);
			info.setDependencies(pkgdependencies);
			info.setOwner(owner);
			info.setSourcesURL(sourcesURL);
			info.setWebURL(webURL);
			info.addCategoriesAsString(categories);

			makePluginService.writePackageInfo(info, packageInfoFolder);

		} catch (LocatorException e) {
			throw new MojoExecutionException(
					"Error getting a reference to the InstallerManager", e);
		} catch (MakePluginPackageServiceException e) {
			throw new MojoExecutionException(
					"Error getting a MakePluginPackageService for the "
							+ " plugin folder: " + pluginsFolder, e);
		}

		log.info("Package info file created successfully");
	}
}
