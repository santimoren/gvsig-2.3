/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestDrawFills;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestDrawLines;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestDrawMarkers;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestMultiLayerSymbol;

public class AllTests extends TestCase{
	/**
	 * The EPSG:4326 projection
	 */
	public static IProjection TEST_DEFAULT_PROJECTION =
		CRSFactory.getCRS("EPSG:4326");

	/**
	 * The EPSG:23030 projection
	 */
	public static IProjection TEST_DEFAULT_MERCATOR_PROJECTION =
		CRSFactory.getCRS("EPSG:23030");

	/**
	 * The EPSG:23029 projection
	 */
	public static IProjection test_newProjection =
		CRSFactory.getCRS("EPSG:23029");

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.iver.cit.gvsig.fmap");
		//$JUnit-BEGIN$


//		/* Symbols (jaume) */
//			// integration tests
//				// ISymbol
//				suite.addTestSuite(TestISymbol.class);
//				suite.addTestSuite(TestMultiLayerSymbol.class);
//
//				// CartographicSupport
//				suite.addTestSuite(TestCartographicSupportForSymbol.class);
//
//		//
//		/*
//		 * Feature iterators
//		 * */
////		suite.addTestSuite(FeatureIteratorTest.class);
//
//		/*
//		 * Other Tests present in FMap (cesar)
//		 * Remove them from here and the src-test dir if they are not
//		 * useful anymore.
//		 */
//
////		suite.addTestSuite(TestAbstractIntervalLegend.class);
//		suite.addTestSuite(TestCartographicSupportForSymbol.class);
//		suite.addTestSuite(TestDrawFills.class);
//		suite.addTestSuite(TestDrawLines.class);
//		suite.addTestSuite(TestDrawMarkers.class);
////		suite.addTestSuite(TestIClassifiedLegend.class);
////		suite.addTestSuite(TestILegend.class);
//		suite.addTestSuite(TestMultiLayerSymbol.class);

		//$JUnit-END$
		return suite;
	}

//// jaume
//// PASTED FROM FeatureIteratorTest.java to be globally accessible
	static final String fwAndamiDriverPath = "../_fwAndami/gvSIG/extensiones/com.iver.cit.gvsig/drivers";
	private static File baseDataPath;
	private static File baseDriversPath;


	public static void setUpDrivers() {
//		try {
//			URL url = AllTests.class.getResource("testdata");
//			if (url == null)
//				throw new Exception("No se encuentra el directorio con datos de prueba");
//
//			baseDataPath = new File(url.getFile());
//			if (!baseDataPath.exists())
//				throw new Exception("No se encuentra el directorio con datos de prueba");
//
//			baseDriversPath = new File(fwAndamiDriverPath);
//			if (!baseDriversPath.exists())
//				throw new Exception("Can't find drivers path: " + fwAndamiDriverPath);
//
//			LayerFactory.setDriversPath(baseDriversPath.getAbsolutePath());
//			if (LayerFactory.getDM().getDriverNames().length < 1)
//				throw new Exception("Can't find drivers in path: " + fwAndamiDriverPath);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
	}

	public static FLayer newLayer(String fileName,
			String driverName)
	throws LoadLayerException {
//		File file = new File(baseDataPath, fileName);
//		return LayerFactory.createLayer(fileName,
//				driverName,
//				file, TEST_DEFAULT_MERCATOR_PROJECTION);
	return null;
	}

	public static MapContext newMapContext(IProjection projection) {
		ViewPort vp = new ViewPort(projection);
		return new MapContext(vp);
	}

//// end past
}