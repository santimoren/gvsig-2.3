/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataQuery;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.DummyFetureStore;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureCache;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureIndexes;
import org.gvsig.fmap.dal.feature.FeatureLocks;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.rendering.legend.IClassifiedLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IClassifiedVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.AbstractIntervalLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.TestISymbol;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.undo.RedoException;
import org.gvsig.tools.undo.UndoException;
import org.gvsig.tools.visitor.Visitor;


/**
 * Integration test to ensure that the legends which implements the
 * IClassifiedLegend interface follow the rules that follow the managing of them
 * by the application.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * @author pepe vidal salvador - jose.vidal.salvador@iver.es
 */
public class TestIClassifiedLegend extends AbstractLibraryAutoInitTestCase {

	private static final Integer v0 = new Integer(0);
	private static final Integer v1 = new Integer(1);
	private static final Integer v2 = new Integer(2);
	private static final Integer v3 = new Integer(3);

	private static final String FIELD3 = "field3";
	private static final String FIELD2 = "field2";
	private static final String FIELD1 = "field1";
	private static final String FIELD0 = "field0";
	private static final int FIELDID = 0;

//	private static final Integer[] feature0Values = new Integer[] { v0, v1, v2, v3, };
//	private static final Integer[] feature1Values = new Integer[] { v3, v0, v1, v2, };
//	private static final Integer[] feature2Values = new Integer[] { v2, v3, v0, v1, };
//	private static final Integer[] feature3Values = new Integer[] { v1, v2, v3, v0, };
//
//	private static final Integer[][] featureValues = new Integer[][] {
//		feature0Values,
//		feature1Values,
//		feature2Values,
//		feature3Values,};

	private static String[] fieldNames = new String[] {FIELD0,FIELD1,FIELD2,FIELD3,};


	// private static final Value v4 = (Value)ValueFactory.createValue(4);
	// private static final Value v5 = (Value)ValueFactory.createValue(5);
	// private static final Value v6 = (Value)ValueFactory.createValue(6);
	// private static final Value v7 = (Value)ValueFactory.createValue(7);
	// private static final Value v8 = (Value)ValueFactory.createValue(8);
	// private static final Value v9 = (Value)ValueFactory.createValue(9);

	private AbstractIntervalLegend[] intervalLegends;
	//private DummyFetureStore mockDataSource = new DummyFetureStore();


	//private static final FInterval interval0=new FInterval(0,2);
	//private static final FInterval interval1=new FInterval(3,5);
	//private static final FInterval interval2=new FInterval(6,2);
	//private static final FInterval interval3=new FInterval(9,2);
	// private static final Value interval4;
	// private static final Value interval5;
	// private static final Value interval6;
	// private static final Value interval7;
	// private static final Value interval8;
	// private static final Value interval9;

	Hashtable symTable;

	private IClassifiedVectorLegend[] classifiedLegends;
	private ISymbol[] symbols;
	private Object[] sampleValues = new Object[] { v0, v1, v2, v3, };
	private Feature[] features;

	// private FInterval[] intervals = new FInterval[] {
	// interval0,
	// interval1,
	// interval2,
	// interval3,
	// interval4,
	// interval5,
	// interval6,
	// interval7,
	// interval8,
	// interval9,
	// };

	protected void doSetUp() throws Exception {

		features = new Feature[4];

//		//Initializes the geometries library
//		DefaultGeometryLibrary lib = new DefaultGeometryLibrary();
//		lib.initialize();
//		lib.postInitialize();

		// initialize test values
		for (int i = 0; i < features.length; i++) {
			// create the geometry associated to the feature
			int size = 200;
			Dimension d = new Dimension(size, size);
			Rectangle aShape = new Rectangle(i * size, i * size, d.width,
					d.height);
			GeometryManager geomManager = GeometryLocator.getGeometryManager();
			Curve curve = (Curve)geomManager.create(TYPES.CURVE, SUBTYPES.GEOM2D);
			curve.setGeneralPath(new GeneralPathX(aShape.getPathIterator(null)));

			/*
			 * create a full-featured Feature with randomed values at its fields
			 * to avoid testing over the same values each time
			 */
//			features[i] = new DefaultFeature(geom, featureValues[i], "[" + i
//					+ "]");
		}

		// initialize the symbol subset for this test
		symbols = TestISymbol.getNewSymbolInstances();

		// initialize the legends for this test
		ILegend[] allLegends = TestILegend.getNewLegendInstances();
		ArrayList clegends = new ArrayList();
		ArrayList intervalLegends = new ArrayList();
		for (int i = 0; i < allLegends.length; i++) {
			if (allLegends[i] instanceof AbstractIntervalLegend) {
				intervalLegends.add(allLegends[i]);
			} else if (allLegends[i] instanceof IClassifiedLegend) {
				clegends.add(allLegends[i]);
			}

			if (allLegends[i] instanceof IClassifiedVectorLegend) {
				IClassifiedVectorLegend cvl = (IClassifiedVectorLegend) allLegends[i];
				cvl.setClassifyingFieldNames(new String[] { fieldNames[FIELDID] });
//				cvl.setFeatureStore(mockDataSource);

			}
		}

		this.classifiedLegends = (IClassifiedVectorLegend[]) clegends
				.toArray(new IClassifiedVectorLegend[clegends.size()]);
		this.intervalLegends =
				(AbstractIntervalLegend[]) intervalLegends
				.toArray(new AbstractIntervalLegend[intervalLegends.size()]);
	}

	/**
	 * This method is used to add symbols to a legend.That is, it takes an array
	 * of IClassifiedVectorialLegend which is empty andm, using a second array
	 * of objects (values), the first one is filled.Also, a hash table is filled
	 * too using the array of objects (it will be useful in some tests to check
	 * that a symbol can be taken using a feature) .
	 *
	 * @param legend
	 * @return
	 */
	private void fillClassifiedLegend(IClassifiedVectorLegend legend,
			Object[] values) {
		// initialize the hash table
		symTable = new Hashtable();

		// to add symbols to the legend and the hash table
		for (int j = 0; j < values.length; j++) {

			ISymbol sym = symbols[j % symbols.length];
			legend.addSymbol(values[j], sym);
			symTable.put(values[j], sym);
		}
	}

	/**
	 * This test ensures that when a legend is filled, the number of symbols
	 * added is correct. To do it, is checked that the number of symbols of a
	 * legend is the same as the length of the array of example values that we
	 * have.
	 *
	 * @throws ReadDriverException
	 */
	public void testICLAdittion() throws ReadException {

		// Fills the legend
		for (int i = 0; i < classifiedLegends.length; i++) {
			fillClassifiedLegend(classifiedLegends[i], sampleValues);
		}

		for (int i = 0; i < classifiedLegends.length; i++) {
			assertEquals(classifiedLegends[i].getClass().getName()
					+ " fails with the comparation of the number of symbols",
					classifiedLegends[i].getSymbols().length,
					sampleValues.length);
		}

	}

	/**
	 * This test ensures that the symbols that we have previously added to a
	 * legend are accessible using its features.To do it, this test compares the
	 * symbol taken from the legend with the symbol taken from the hashTable
	 * (using the same feature).
	 * @throws Exception
	 */
	public void testICLCheckValueSymbols() throws Exception {
		// fills the legends
		for (int i = 0; i < classifiedLegends.length; i++) {
			fillClassifiedLegend(classifiedLegends[i], sampleValues);
		}

		for (int i = 0; i < classifiedLegends.length; i++) {
			// For each feature
			for (int j = 0; j < features.length; j++) {
				Feature myFeature = features[i];
				// takes the value of the field that identifies the feature
				Object val = myFeature.get(FIELDID);
				// the last value is used to access to the hash table to obtain
				// a symbol
				ISymbol tableSym = (ISymbol) symTable.get(val);

				IClassifiedVectorLegend leg = classifiedLegends[i];
				// takes the symbol from a legend using the feature
				ISymbol legendSym = leg.getSymbolByFeature(myFeature);
				// compares that both symbols are the same
				assertEquals(legendSym.getClass().getName()
						+ " fails with the comparation of the class symbols",
						legendSym, tableSym);
			}
		}
	}

}
