/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import org.gvsig.fmap.mapcontext.rendering.legend.IInterval;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Clase intervalo.
 * 
 * @author Vicente Caballero Navarro
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class FInterval implements IInterval {
	public static final String FINTERVAL_PERSISTENCE_DEFINITION_NAME = "FInterval";

	private static final String FIELD_MIN = "min";
	private static final String FIELD_MAX = "max";

	private double min;
	private double max;

	/**
	 * Creates a new empty FInterval, used to be instantiated by persistence
	 */
	public FInterval(){
		// Nothing to do
	}

	/**
	 * Crea un nuevo FInterval.
	 *
	 * @param from Origen.
	 * @param to Destino.
	 */
	public FInterval(double from, double to) {
		this.min = from;
		this.max = to;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.rendering.IInterval#isInInterval(com.hardcode.gdbms.engine.values.Value)
	 */
	public boolean isInInterval(Object v) {
		double valor=0;
		if (v == null){
			return false;
		}else if (v instanceof Number) {
			valor = ((Number) v).doubleValue();
		}
		return ((valor >= min) && (valor <= max));
	}

	/**
	 * Devuelve el n�mero de origen.
	 *
	 * @return N�mero de inicio.
	 */
	public double getMin() {
		return min;
	}

	/**
	 * Devuelve el n�mero final.
	 *
	 * @return N�mero final.
	 */
	public double getMax() {
		return max;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.rendering.IInterval#toString()
	 */
	public String toString() {
		return min + "-" + max;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		min = state.getDouble(FIELD_MIN);
		max = state.getDouble(FIELD_MAX);
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_MIN, getMin());
		state.set(FIELD_MAX, getMax());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(FINTERVAL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						FInterval.class,
						FINTERVAL_PERSISTENCE_DEFINITION_NAME,
						FINTERVAL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				definition.addDynFieldDouble(FIELD_MIN)
						.setMandatory(true)
						.setDescription("Minimum interval value");
				definition.addDynFieldDouble(FIELD_MAX).setMandatory(true).setDescription(
						"Maximum interval value");
			}
			return Boolean.TRUE;
		}
		
	}


}
