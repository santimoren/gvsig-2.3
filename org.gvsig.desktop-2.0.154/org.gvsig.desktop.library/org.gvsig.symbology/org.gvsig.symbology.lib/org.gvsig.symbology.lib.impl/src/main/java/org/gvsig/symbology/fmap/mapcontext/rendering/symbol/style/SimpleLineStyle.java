/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * @see http://www.oreilly.com/catalog/java2d/chapter/ch04.html
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SimpleLineStyle extends AbstractStyle implements CartographicSupport, ISimpleLineStyle {
	public static final String SIMPLE_LINE_STYLE_PERSISTENCE_DEFINITION_NAME = "SimpleLineStyle";

	private static final String FIELD_DASH_ARRAY = "dashArray";
	private static final String FIELD_TEMP_DASH_ARRAY = "tempDashArray";
	private static final String FIELD_LINE_WIDTH = "lineWidth";
	private static final String FIELD_DASH_PHASE = "dashPhase";
	private static final String FIELD_END_CAP = "endCap";
	private static final String FIELD_LINE_JOIN = "lineJoin";
	private static final String FIELD_MITER_LIMIT = "miterLimit";
	private static final String FIELD_OFFSET = "offset";
	private static final String FIELD_UNIT = "unit";
	private static final String FIELD_ARROW_DECORATOR = "arrowDecorator";

	private final static Color PREVIEW_COLOR_1= new Color(150, 255, 200); //light blue
	private final static Color PREVIEW_COLOR_2 = new Color(255, 200, 100); //orange
	private final static int COLOR1_STROKE = 1;
	private final static int COLOR2_STROKE = 3;
	private float[] dashArray, tempDashArray;
	private float dashPhase;
	private int endCap = BasicStroke.CAP_BUTT;
	private int lineJoin;
	private float miterlimit;
	private float lineWidth = 1f;
	private int measureUnit;// for the offset distance
	private double offset = 0, csOffset = 0;
	private int referenceSystem;
	private IArrowDecoratorStyle arrowDecorator;
	/**
	 * Constructor method
	 *
	 */
	public SimpleLineStyle() {
		super();
		BasicStroke dummy = new BasicStroke();
		dashArray = dummy.getDashArray();
		tempDashArray = dummy.getDashArray();
		dashPhase = dummy.getDashPhase();
		endCap = BasicStroke.CAP_BUTT;
		lineJoin = BasicStroke.JOIN_BEVEL;
		miterlimit = dummy.getMiterLimit();
	}
	/**
	 * Constructor method
	 *
	 * @param width
	 * @param cap
	 * @param join
	 * @param miterlimit
	 * @param dash
	 * @param dash_phase
	 */
	public SimpleLineStyle(float width, int cap, int join, float miterlimit, float[] dash, float dash_phase) {
		super();
		this.lineWidth = width;
		this.endCap = cap;
		this.lineJoin = join;
		this.miterlimit = miterlimit;
		this.dashArray = dash;
		this.tempDashArray = dash;
		this.dashPhase = dash_phase;
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r) {
		Stroke oldStroke = g.getStroke();
		int h = (int) (r.getHeight() / 2);
		int margins = 2;

		BasicStroke stroke;
		stroke = new BasicStroke(COLOR1_STROKE, endCap, lineJoin, miterlimit, tempDashArray, dashPhase);
		g.setStroke(stroke);
		g.setColor(PREVIEW_COLOR_1);
		g.drawLine(margins, h, r.width-margins-margins, h);

		stroke = new BasicStroke(COLOR2_STROKE, endCap, lineJoin, miterlimit, tempDashArray, dashPhase);
		g.setStroke(stroke);
		g.setColor(PREVIEW_COLOR_2);
		g.drawLine(margins, h, r.width-margins-margins, h);
		g.setStroke(oldStroke);
	}

	public String getClassName() {
		return getClass().getName();
	}

	public Stroke getStroke() {
		return new BasicStroke((float) lineWidth, endCap, lineJoin, miterlimit, tempDashArray, dashPhase);
	}

	public float getLineWidth() {
		return lineWidth;
	}


	public void setLineWidth(float width) {
		if (lineWidth != 0) {
			double scale = width / this.lineWidth;
			if (dashArray != null) {
				for (int i = 0; scale > 0 && i < dashArray.length; i++) {
					tempDashArray[i] = (float) (dashArray[i] * scale);
				}
			}
			this.csOffset = offset * scale;
		}
		this.lineWidth = width;

	}

	public boolean isSuitableFor(ISymbol symbol) {
		return symbol instanceof ILineSymbol;
	}

	public void setStroke(Stroke stroke) {
		if (stroke != null) {
			BasicStroke dummy = (BasicStroke) stroke;
			dashArray = dummy.getDashArray();
			if (dashArray != null) {
				tempDashArray = new float[dashArray.length];
				for (int i = 0; i < dashArray.length; i++) {
					tempDashArray[i] = dashArray[i];
				}
			} else
				tempDashArray = null;


			dashPhase = dummy.getDashPhase();
			endCap = dummy.getEndCap();
			lineJoin = dummy.getLineJoin();
			miterlimit = dummy.getMiterLimit();
			lineWidth = dummy.getLineWidth();
			offset = getOffset();
			csOffset = offset;
		}
	}

	public void drawOutline(Graphics2D g, Rectangle r) {
		drawInsideRectangle(g, r);
	}

	public double getOffset() {
		return csOffset;
	}

	public void setOffset(double offset) {
		this.offset = offset;
		this.csOffset = offset;
	}

	public IArrowDecoratorStyle getArrowDecorator() {
		return arrowDecorator;

	}

	public void setArrowDecorator(IArrowDecoratorStyle arrowDecoratorStyle) {
		this.arrowDecorator = arrowDecoratorStyle;
	}

	public void setUnit(int unitIndex) {
		this.measureUnit = unitIndex;
	}

	public int getUnit() {
		return measureUnit;
	}

	public int getReferenceSystem() {
		return referenceSystem;
	}

	public void setReferenceSystem(int referenceSystem) {
		this.referenceSystem = referenceSystem;
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double oldWidth = getLineWidth();
		setCartographicSize(getCartographicSize(viewPort, dpi, geom), geom);
		return oldWidth;
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		setLineWidth((float) cartographicSize);

	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return CartographicSupportToolkit.
		getCartographicLength(this,
				getOffset(),
				viewPort,
				dpi);

	}
	

	public Object clone() throws CloneNotSupportedException {
		SimpleLineStyle copy = (SimpleLineStyle) super.clone();
		if (arrowDecorator != null) {
			copy.arrowDecorator = (ArrowDecoratorStyle) arrowDecorator.clone();
		}

		if (dashArray != null) {
			copy.dashArray = new float[dashArray.length];
			System.arraycopy(dashArray, 0, copy.dashArray, 0, dashArray.length);
		}
		if (tempDashArray != null) {
			copy.tempDashArray = new float[tempDashArray.length];
			System.arraycopy(tempDashArray, 0, copy.tempDashArray, 0,
					tempDashArray.length);
		}

		return copy;
	}

	/**
	 * @return the dashArray
	 */
	private float[] getDashArray() {
		return dashArray;
	}

	/**
	 * @param dashArray
	 *            the dashArray to set
	 */
	private void setDashArray(float[] dashArray) {
		this.dashArray = dashArray;
	}

	/**
	 * @return the dashPhase
	 */
	private float getDashPhase() {
		return dashPhase;
	}

	/**
	 * @param dashPhase
	 *            the dashPhase to set
	 */
	private void setDashPhase(float dashPhase) {
		this.dashPhase = dashPhase;
	}

	/**
	 * @return the endCap
	 */
	private int getEndCap() {
		return endCap;
	}

	/**
	 * @param endCap
	 *            the endCap to set
	 */
	private void setEndCap(int endCap) {
		this.endCap = endCap;
	}

	/**
	 * @return the lineJoin
	 */
	private int getLineJoin() {
		return lineJoin;
	}

	/**
	 * @param lineJoin
	 *            the lineJoin to set
	 */
	private void setLineJoin(int lineJoin) {
		this.lineJoin = lineJoin;
	}

	/**
	 * @return the miterlimit
	 */
	private float getMiterlimit() {
		return miterlimit;
	}

	/**
	 * @param miterlimit
	 *            the miterlimit to set
	 */
	private void setMiterlimit(float miterlimit) {
		this.miterlimit = miterlimit;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent style properties
		super.loadFromState(state);

		// Set own properties
		setArrowDecorator((ArrowDecoratorStyle) state
				.get(FIELD_ARROW_DECORATOR));
		setDashArray(state.getFloatArray(FIELD_DASH_ARRAY));
		tempDashArray = state.getFloatArray(FIELD_TEMP_DASH_ARRAY);
		setDashPhase(state.getFloat(FIELD_DASH_PHASE));
		setEndCap(state.getInt(FIELD_END_CAP));
		setLineJoin(state.getInt(FIELD_LINE_JOIN));
		setLineWidth(state.getFloat(FIELD_LINE_WIDTH));
		setMiterlimit(state.getFloat(FIELD_MITER_LIMIT));
		setOffset(state.getDouble(FIELD_OFFSET));
		setUnit(state.getInt(FIELD_UNIT));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_ARROW_DECORATOR, getArrowDecorator());
		state.set(FIELD_DASH_ARRAY, getDashArray());
		state.set(FIELD_TEMP_DASH_ARRAY, tempDashArray);
		state.set(FIELD_DASH_PHASE, getDashPhase());
		state.set(FIELD_END_CAP, getEndCap());
		state.set(FIELD_LINE_JOIN, getLineJoin());
		state.set(FIELD_LINE_WIDTH, getLineWidth());
		state.set(FIELD_MITER_LIMIT, getMiterlimit());
		state.set(FIELD_OFFSET, getOffset());
		state.set(FIELD_UNIT, getUnit());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_LINE_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleLineStyle.class,
						SIMPLE_LINE_STYLE_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_LINE_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				// Extend the Style base definition
				definition.extend(manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME));

				// Arrow decorator
				definition.addDynFieldObject(FIELD_ARROW_DECORATOR)
					.setClassOfValue(ArrowDecoratorStyle.class);
				// Dash array
				definition.addDynFieldArray(FIELD_DASH_ARRAY)
					.setClassOfItems(float.class);
				// Temporal dash array
				definition.addDynFieldArray(FIELD_TEMP_DASH_ARRAY)
					.setClassOfItems(float.class);
				// Dash phase
				definition.addDynFieldFloat(FIELD_DASH_PHASE).setMandatory(true);
				// End cap
				definition.addDynFieldInt(FIELD_END_CAP).setMandatory(true);
				// Line join
				definition.addDynFieldInt(FIELD_LINE_JOIN).setMandatory(true);
				// Line width
				definition.addDynFieldFloat(FIELD_LINE_WIDTH).setMandatory(true);
				// Miter limit
				definition.addDynFieldFloat(FIELD_MITER_LIMIT).setMandatory(true);
				// Offset
				definition.addDynFieldDouble(FIELD_OFFSET).setMandatory(true);
				// Unit
				definition.addDynFieldInt(FIELD_UNIT).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}
}