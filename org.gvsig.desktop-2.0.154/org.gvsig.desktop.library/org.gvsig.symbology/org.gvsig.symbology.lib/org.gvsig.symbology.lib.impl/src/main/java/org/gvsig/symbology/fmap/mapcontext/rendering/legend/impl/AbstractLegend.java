/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.BaseWeakReferencingObservable;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class that implements the interface for legends.It is considered as
 * the father of all XXXLegends and will implement all the methods that these
 * classes had not developed.
 * 
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2008 -    pepe vidal salvador - jose.vidal.salvador@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractLegend implements ILegend {

	public static final String LEGEND_PERSISTENCE_DEFINITION_NAME = "Legend";

//	private static final String FIELD_LISTENERS = "listeners";
//
//	private static final String FIELD_OBSERVABLE = "observable";

	private static final Logger LOG =
			LoggerFactory.getLogger(AbstractLegend.class);

	/**
	 * List of LegendContentsChangedListener.
	 */
	private List<LegendContentsChangedListener> listeners =
			new ArrayList<LegendContentsChangedListener>();

	private BaseWeakReferencingObservable delegateDrawingObservable = 
			new BaseWeakReferencingObservable();
	
	private MapContextManager manager = MapContextLocator.getMapContextManager();

	public MapContextManager getManager() {
		return manager;
	}

	public SymbolManager getSymbolManager() {
		return manager.getSymbolManager();
	}

	public void addLegendListener(LegendContentsChangedListener listener) {
		if (listener != null && !listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeLegendListener(LegendContentsChangedListener listener) {
		listeners.remove(listener);
	}

	public void fireDefaultSymbolChangedEvent(SymbolLegendEvent event) {

		for (int i = 0; i < listeners.size(); i++) {
			((LegendContentsChangedListener) listeners.get(i))
					.symbolChanged(event);
		}
	}

	public LegendContentsChangedListener[] getListeners() {
		return (LegendContentsChangedListener[]) listeners
				.toArray(new LegendContentsChangedListener[listeners.size()]);
	}
	
	public void addDrawingObserver(Observer observer) {
		delegateDrawingObservable.addObserver(observer);
	}

	public void deleteDrawingObserver(Observer observer) {
		delegateDrawingObservable.deleteObserver(observer);
	}

	public void deleteDrawingObservers() {
		delegateDrawingObservable.deleteObservers();
	}

	public void addObserver(Reference<Observer> ref) {
		delegateDrawingObservable.addObserver(ref);
	}

	public void beginComplexNotification() {
		delegateDrawingObservable.beginComplexNotification();
	}

	public int countObservers() {
		return delegateDrawingObservable.countObservers();
	}

	public void deleteObserver(Reference<Observer> ref) {
		delegateDrawingObservable.deleteObserver(ref);
	}

	public void disableNotifications() {
		delegateDrawingObservable.disableNotifications();
	}

	public void enableNotifications() {
		delegateDrawingObservable.enableNotifications();
	}

	public void endComplexNotification() {
		delegateDrawingObservable.endComplexNotification();
	}

	public boolean inComplex() {
		return delegateDrawingObservable.inComplex();
	}

	public boolean isEnabledNotifications() {
		return delegateDrawingObservable.isEnabledNotifications();
	}

	public void notifyObservers() {
		delegateDrawingObservable.notifyObservers();
	}

	public void notifyObservers(Object arg) {
		delegateDrawingObservable.notifyObservers(arg);
	}
	
	public ILegend cloneLegend() {
		try {
			return (ILegend) clone();
		} catch (CloneNotSupportedException e) {
			LOG.error("Error creating the clone of the legend: " + this, e);
		}
		return null;
    }

	public Object clone() throws CloneNotSupportedException {
		AbstractLegend legendClone = (AbstractLegend) super.clone();

		return legendClone;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Do nothing
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Do nothing
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(LEGEND_PERSISTENCE_DEFINITION_NAME)==null ) {
				@SuppressWarnings("unused")
				DynStruct definition = manager.addDefinition(
						AbstractLegend.class, 
						LEGEND_PERSISTENCE_DEFINITION_NAME, 
						LEGEND_PERSISTENCE_DEFINITION_NAME + " persistence definition", 
						null, 
						null
					);
//					// Listeners
//					definition.addDynFieldList(FIELD_LISTENERS)
//						.setClassOfItems(LegendContentsChangedListener.class);
//					// Observable
//					definition.addDynFieldObject(FIELD_OBSERVABLE)
//						.setClassOfValue(BaseWeakReferencingObservable.class);
			}
			return Boolean.TRUE;
		}
		
	}

}