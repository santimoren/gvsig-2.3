/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: LabelingFactory.java 13913 2007-09-20 09:36:02Z jaume $
* $Log$
* Revision 1.9  2007-09-20 09:33:15  jaume
* Refactored: fixed name of IPersistAnce to IPersistence
*
* Revision 1.8  2007/05/22 12:17:41  jaume
* *** empty log message ***
*
* Revision 1.7  2007/05/22 10:05:31  jaume
* *** empty log message ***
*
* Revision 1.6  2007/04/13 11:59:30  jaume
* *** empty log message ***
*
* Revision 1.5  2007/03/28 16:48:01  jaume
* *** empty log message ***
*
* Revision 1.4  2007/03/20 16:16:20  jaume
* refactored to use ISymbol instead of FSymbol
*
* Revision 1.3  2007/03/09 11:20:57  jaume
* Advanced symbology (start committing)
*
* Revision 1.2  2007/03/09 08:33:43  jaume
* *** empty log message ***
*
* Revision 1.1.2.4  2007/02/21 07:34:08  jaume
* labeling starts working
*
* Revision 1.1.2.3  2007/02/12 15:15:20  jaume
* refactored interval legend and added graduated symbol legend
*
* Revision 1.1.2.2  2007/02/09 07:47:05  jaume
* Isymbol moved
*
* Revision 1.1.2.1  2007/02/02 16:21:24  jaume
* start commiting labeling stuff
*
* Revision 1.1.2.1  2007/01/30 18:10:45  jaume
* start commiting labeling stuff
*
*
*/
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LabelingFactory {
	final static private Logger logger = LoggerFactory.getLogger(LabelingFactory.class);
	@SuppressWarnings("unchecked")
	private static Class defaultLabelingStrategy =
			AttrInTableLabelingStrategy.class;
	/**
	 * Given a layer, a labeling method, a label placements constraints, and a label
	 * zoom constraints it will figure out the best ILabelingStrategy that meets all
	 * the needs.
	 * @param layer, the target layer
	 * @param method, the desired methods
	 * @param placement, the desired placement constraints
	 * @param zoom, the desired zoom constraints
	 * @return ILabelingStrategy
	 */
	public static ILabelingStrategy createStrategy(FLayer layer,
			ILabelingMethod method, IPlacementConstraints placement,
			IZoomConstraints zoom) 	{
		if (method == null && placement == null && zoom == null)
			return createDefaultStrategy(layer);

		ILabelingStrategy strat = createDefaultStrategy(layer);
		if (method != null)
			strat.setLabelingMethod(method);
		if (placement != null)
			strat.setPlacementConstraints(placement);
		return strat;

	}

	/**
	 * Creates a default labeling strategy from an XMLEntity.
	 * @param layer
	 * @return an instance of DefaultStrategy
	 */
	public static ILabelingStrategy createDefaultStrategy(FLayer layer) {
		ILabelingStrategy st;
		try {
			st = (ILabelingStrategy) defaultLabelingStrategy.newInstance();
			st.setLayer(layer);
			return st;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public static void setDefaultLabelingStrategy(Class defaultLabelingStrategy) {
		LabelingFactory.defaultLabelingStrategy = defaultLabelingStrategy;
	}
}
