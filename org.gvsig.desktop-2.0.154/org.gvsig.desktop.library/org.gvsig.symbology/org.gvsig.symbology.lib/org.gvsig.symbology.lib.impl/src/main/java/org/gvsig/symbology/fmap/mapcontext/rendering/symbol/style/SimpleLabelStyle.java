/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.SimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.SimpleLineSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implements a style for the creation of simple labels.
 *
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public class SimpleLabelStyle extends AbstractStyle implements ILabelStyle {
	
	private static Logger logger = LoggerFactory.getLogger(SimpleLabelStyle.class);
	
	public static final String SIMPLE_LABEL_STYLE_PERSISTENCE_NAME =
			"SIMPLE_LABEL_STYLE_PERSISTENCE_NAME";
	
	private Point2D markerPoint = new Point2D.Double();
	// conertir a�� a Rectangle2D[] ja que pot arribar a gastar-se massivament
	// en el pintat
	private List<Rectangle2D> textFieldAreas = new ArrayList<Rectangle2D>();
	private IBackgroundFileStyle background;
	private Dimension defaultSize = new Dimension(32,32);
	private Dimension sz;



	public int getFieldCount() {
		return textFieldAreas.size();
	}

	public void setTextFields(String[] texts) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; texts != null && i < texts.length; i++) {
			sb.append(texts[i]);
			if (i<texts.length)
				sb.append(" ");
		}
	}

	public boolean isSuitableFor(ISymbol symbol) {
		return true;
	}

	public String getClassName() {
		return getClass().getName();
	}

	/*
	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());
		xml.putProperty("desc", getDescription());
		xml.putProperty("markerPointX", markerPoint.getX());
		xml.putProperty("markerPointY", markerPoint.getY());

		int size = getFieldCount();
		String[] minx = new String[size];
		String[] miny = new String[size];
		String[] widths = new String[size];
		String[] heights = new String[size];

		Rectangle2D[] rects = getTextBounds();
		for (int i = 0; i < rects.length; i++) {
			minx[i] = String.valueOf(rects[i].getMinX());
			miny[i] = String.valueOf(rects[i].getMinY());
			widths[i] = String.valueOf(rects[i].getWidth());
			heights[i] = String.valueOf(rects[i].getHeight());
		}

		xml.putProperty("minXArray", minx);
		xml.putProperty("minYArray", miny);
		xml.putProperty("widthArray", widths);
		xml.putProperty("heightArray", heights);
		if(getBackgroundFileStyle() != null){
			XMLEntity bgXML = getBackgroundFileStyle().getXMLEntity();
			bgXML.putProperty("id", "LabelStyle");
			xml.addChild(bgXML);
		}
		if (sz!=null){
			xml.putProperty("sizeW",(int)sz.getWidth());
			xml.putProperty("sizeH",(int)sz.getHeight());
		}
		return xml;
	}

	public void setXMLEntity(XMLEntity xml) {
		setDescription(xml.getStringProperty("desc"));

		double x = xml.getDoubleProperty("markerPointX");
		double y = xml.getDoubleProperty("markerPointY");

		double[] minx = xml.getDoubleArrayProperty("minXArray");
		double[] miny = xml.getDoubleArrayProperty("minYArray");
		double[] widths = xml.getDoubleArrayProperty("widthArray");
		double[] heights = xml.getDoubleArrayProperty("heightArray");

		textFieldAreas.clear();
		for (int i = 0; i < minx.length; i++) {
			addTextFieldArea(new Rectangle2D.Double(
					minx[i],
					miny[i],
					widths[i],
					heights[i]));
		}
		markerPoint.setLocation(x, y);
		XMLEntity bgXML = xml.firstChild("id", "LabelStyle");
		if (bgXML!=null) {
			background = (BackgroundFileStyle) SymbologyFactory.createStyleFromXML(xml.getChild(0), null);
		}
		if (xml.contains("sizeW"))
			sz=new Dimension(xml.getIntProperty("sizeW"),xml.getIntProperty("sizeH"));
		
	}
	*/

	public Rectangle2D[] getTextBounds() {
		return (Rectangle2D[]) textFieldAreas.toArray(new Rectangle2D[textFieldAreas.size()]);
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r)
			throws SymbolDrawingException {
		if(getBackgroundFileStyle() != null)
			getBackgroundFileStyle().drawInsideRectangle(g, r);
	}


	public Dimension getSize() {
		if (sz == null && getBackgroundFileStyle() != null) {
			Rectangle bgBounds = getBackgroundFileStyle().getBounds();
			setSize(bgBounds.getWidth(), bgBounds.getHeight());
		}else if (sz==null)
			sz = defaultSize;
		return sz;
	}

	public Point2D getMarkerPoint() {
		return markerPoint;
	}

	public void setMarkerPoint(Point2D p) throws IllegalArgumentException {
		if (p.getX()<0 || p.getX()>1)
			throw new IllegalArgumentException("X must be >=0 and <=1 ("+p.getX()+")");
		if (p.getY()<0 || p.getY()>1)
			throw new IllegalArgumentException("Y must be >=0 and <=1 ("+p.getY()+")");
		// the marker represents the point labeled in relative percent units
		this.markerPoint = p;
	}


	public void drawOutline(Graphics2D g, Rectangle r) throws SymbolDrawingException {
		if(getBackgroundFileStyle() != null)
			getBackgroundFileStyle().drawOutline(g, r);

		final double[] xy = new double[2];
		// draw the pointer
		{
			xy[0] = markerPoint.getX();
			xy[1] = markerPoint.getY();

			int x = (int) Math.round(r.width * xy[0]);
			int y = (int) Math.round(r.height * xy[1]);

			int size = 7;
			g.setColor(Color.ORANGE.darker());
			g.fillOval(x, y, size, size);
			g.setColor(Color.BLACK.brighter());
			g.drawString(Messages.getText("labeled_point"), x + size + 10, y + size);
			g.setColor(Color.BLACK);
			g.drawLine(x-size, (int) (y+(size*0.5)), x+2*size-1, (int) (y+(size*0.5)));
			g.drawLine((int) (x+(size*0.5)), y-size, (int) (x+(size*0.5)), y+2*size-1);
		}

		// draw the text fields
		if (textFieldAreas.size() > 0) {
			SimpleFillSymbol sym = new SimpleFillSymbol();
			Color c = Color.blue.brighter().brighter();

			sym.setFillColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 100));
			SimpleLineSymbol outline = new SimpleLineSymbol();
			c = Color.BLACK;
			outline.setLineColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 100));
			sym.setOutline(outline);
			for (int i = 0; i < textFieldAreas.size(); i++) {
				//FIXME: Esto es un parche, habr�a que cambiar el API de los simbolos y/o estilos
				//pero mientras tanto
				if(getBackgroundFileStyle() == null){
					Rectangle2D textFieldArea = (Rectangle2D) textFieldAreas.get(i);
					xy[0] = textFieldArea.getX();
					xy[1] = textFieldArea.getY();

					int x = (int) Math.round(((r.width) * xy[0]));
					int y = (int) Math.round((r.height) * xy[1]);

					xy[0] = textFieldArea.getMaxX();
					xy[1] = textFieldArea.getMaxY();

					int width = (int) Math.round((r.width * xy[0]) -x);
					int height = (int) Math.round((r.height * xy[1]) - y) ;

					Envelope env = null;
					try {
						env = GeometryLocator.getGeometryManager().
								createEnvelope(x, y, x+width, y+height, SUBTYPES.GEOM2D);
					} catch (Exception e) {
						logger.error("While creating envelope", e);
						throw new SymbolDrawingException(TYPES.SURFACE);
					}
					
					sym.draw(g, null, env.getGeometry(), null, null);
					g.setColor(Color.BLACK);
					g.drawString(String.valueOf(i+1), x+5, y + 10); // start with 1
				} else {
					double xOffset = 0;
					double yOffset = 0;
					double scale = 1;
					Dimension backgroundBounds = getSize();
					if (backgroundBounds.getWidth()>backgroundBounds.getHeight()) {
						scale = r.getWidth()/backgroundBounds.getWidth();
						yOffset = 0.5*(r.getHeight() - backgroundBounds.getHeight()*scale);
					} else {
						scale = r.getHeight()/backgroundBounds.getHeight();
						xOffset = 0.5*(r.getWidth() - backgroundBounds.getWidth()*scale);
					}

					Rectangle2D textFieldArea = (Rectangle2D) textFieldAreas.get(i);
					xy[0] = textFieldArea.getX();
					xy[1] = textFieldArea.getY();

					int x = (int) Math.round(xy[0]*backgroundBounds.getWidth()*scale+xOffset);
					int y = (int) Math.round(xy[1]*backgroundBounds.getHeight()*scale+yOffset);

					xy[0] = textFieldArea.getMaxX();
					xy[1] = textFieldArea.getMaxY();

					int width = (int) Math.round((xy[0]*backgroundBounds.getWidth()*scale+xOffset)-x);
					int height = (int) Math.round((xy[1]*backgroundBounds.getHeight()*scale+yOffset)-y);

					Envelope env = null;
					try {
						env = GeometryLocator.getGeometryManager().
								createEnvelope(x, y, x+width, y+height, SUBTYPES.GEOM2D);
					} catch (Exception e) {
						logger.error("While creating envelope", e);
						throw new SymbolDrawingException(TYPES.SURFACE);
					}
					
					sym.draw(g, null, env.getGeometry(), null, null);
					g.setColor(Color.BLACK);
					g.drawString(String.valueOf(i+1), x+5, y + 10); // start with 1

				}
			}
		}
	}

	public void setTextFieldArea(int index, Rectangle2D rect) {
		textFieldAreas.set(index, rect);
	}

	public void addTextFieldArea(Rectangle2D rect) {
		textFieldAreas.add(rect);
	}

	public void deleteTextFieldArea(int index) {
		textFieldAreas.remove(index);
	}

	public void setSize(double width, double height) {
		sz = new Dimension( (int) Math.round(width), (int) Math.round(height));
	}

	public IBackgroundFileStyle getBackgroundFileStyle() {
		return background;
	}

	public void setBackgroundFileStyle(IBackgroundFileStyle bg) {
		this.background = bg;
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {

			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			DynStruct definition = manager.getDefinition(
	    		SIMPLE_LABEL_STYLE_PERSISTENCE_NAME);

			if (definition == null){
				definition = manager.addDefinition(
	        		SimpleLabelStyle.class,
	        		SIMPLE_LABEL_STYLE_PERSISTENCE_NAME,
	        		SIMPLE_LABEL_STYLE_PERSISTENCE_NAME + " Persistent definition", 
	        		null, 
	        		null);
				definition.extend(manager.getDefinition(
						AbstractStyle.STYLE_PERSISTENCE_DEFINITION_NAME));
				definition.addDynFieldObject("markerPoint").setClassOfValue(Point2D.class)
				.setMandatory(true);
				definition.addDynFieldList("textFieldAreas").setClassOfItems(Rectangle2D.class)
				.setMandatory(true);
				definition.addDynFieldObject("background").setClassOfValue(IBackgroundFileStyle.class)
				.setMandatory(false);
				definition.addDynFieldObject("defaultSize").setClassOfValue(Dimension.class)
				.setMandatory(true);
				definition.addDynFieldObject("size").setClassOfValue(Dimension.class)
				.setMandatory(true);
			}
			return Boolean.TRUE;
		}
	}
	
	
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		
		super.loadFromState(state);
		
		this.markerPoint = (Point2D) state.get("markerPoint");
		this.textFieldAreas = (List<Rectangle2D>) state.get("textFieldAreas");
		
		if (state.hasValue("background")) {
			this.background = (IBackgroundFileStyle) state.get("background");
		}
		
		this.defaultSize = (Dimension) state.get("defaultSize");
		this.sz = (Dimension) state.get("size");
	}

	public void saveToState(PersistentState state) throws PersistenceException {

		super.saveToState(state);
		
		state.set("markerPoint", this.markerPoint);
		state.set("textFieldAreas", this.textFieldAreas);
		
		if (this.background != null) {
			state.set("background", this.background);
		}
		
		state.set("defaultSize", this.defaultSize);
		state.set("size", this.getSize());
	}
}
