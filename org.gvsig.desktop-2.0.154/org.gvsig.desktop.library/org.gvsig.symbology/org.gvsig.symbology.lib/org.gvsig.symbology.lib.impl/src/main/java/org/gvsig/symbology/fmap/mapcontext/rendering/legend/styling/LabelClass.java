/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: LabelClass.java 13953 2007-09-21 12:26:04Z jaume $
 * $Log$
 * Revision 1.14  2007-09-21 12:26:04  jaume
 * cancellation support extended down to the IGeometry and ISymbol level
 *
 * Revision 1.13  2007/09/17 14:16:11  jaume
 * multilayer symbols sizing bug fixed
 *
 * Revision 1.12  2007/08/22 09:48:13  jvidal
 * javadoc
 *
 * Revision 1.11  2007/05/09 11:04:58  jaume
 * refactored legend hierarchy
 *
 * Revision 1.10  2007/05/08 08:47:40  jaume
 * *** empty log message ***
 *
 * Revision 1.9  2007/04/26 11:41:00  jaume
 * attempting to let defining size in world units
 *
 * Revision 1.8  2007/04/18 15:35:11  jaume
 * *** empty log message ***
 *
 * Revision 1.7  2007/04/12 14:28:43  jaume
 * basic labeling support for lines
 *
 * Revision 1.6  2007/04/11 16:01:08  jaume
 * maybe a label placer refactor
 *
 * Revision 1.5  2007/04/10 16:34:01  jaume
 * towards a styled labeling
 *
 * Revision 1.4  2007/04/05 16:07:14  jaume
 * Styled labeling stuff
 *
 * Revision 1.3  2007/04/02 16:34:56  jaume
 * Styled labeling (start commiting)
 *
 * Revision 1.2  2007/03/09 08:33:43  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.6  2007/02/15 16:23:44  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.5  2007/02/09 07:47:05  jaume
 * Isymbol moved
 *
 * Revision 1.1.2.4  2007/02/02 16:21:24  jaume
 * start commiting labeling stuff
 *
 * Revision 1.1.2.3  2007/02/01 17:46:49  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.2  2007/02/01 11:42:47  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2007/01/30 18:10:45  jaume
 * start commiting labeling stuff
 *
 *
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelLocationMetrics;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl.SimpleTextSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentContext;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.persistence.spi.PersistentContextServices;
import org.gvsig.tools.util.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * LabelClass is the model of the label in the new simbology of gvSIG. In this
 * class is contained its definition, the expresion that defines the text which
 * is going to be showed, if it will be visible or not, the text symbol that is
 * going to paint the label and the style for its background.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class LabelClass implements ILabelClass {

	private static final String FIELD_REFERENCE_SYSTEM = "referenceSystem";
	private static final String FIELD_UNIT = "unit";
	private static final String FIELD_SQL_QUERY = "sqlQuery";
	private static final String FIELD_SCALE = "scale";
	private static final String FIELD_PRIORITY = "priority";
	private static final String FIELD_TEXTS = "texts";
	private static final String FIELD_LABEL_STYLE = "labelStyle";
	private static final String FIELD_VISIBLE = "visible";
	private static final String FIELD_LABEL_EXPRESSIONS = "labelExpressions";
	private static final String FIELD_TEXT_SYMBOL = "textSymbol";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_IS_USESQL = "useSQL";
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(GeometryManager.class);
	private static final String LABEL_CLASS_PERSISTENCE_DEFINITION_NAME = "LabelClass";
	private String theName = "";
	private ITextSymbol textSymbol;
	private String[] labelExpressions = new String[0];
	private boolean isVisible = true;
	private ILabelStyle labelStyle;
	private String[] texts;
	private int priority;
	private double scale = 1;

	private String sqlQuery;
	private boolean usesSQL;

	private int unit = CartographicSupportToolkit.DefaultMeasureUnit;
	private int referenceSystem =
			CartographicSupportToolkit.DefaultReferenceSystem;

	/**
	 * Returns true if the label will be showed in the map
	 *
	 * @return isVisible boolean
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * Returns true if the label will be showed in the map
	 *
	 * @return isVisible boolean
	 */
	public boolean isVisible(double scale) {
		return isVisible();
	}

	/**
	 * Sets the visibility of the label in the map.
	 *
	 * @param isVisible boolean
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	/**
	 * Returns the expression that defines the text which will be showed in
	 * the label
	 *
	 * @return labelExpression String
	 */
	public String[] getLabelExpressions() {
		return labelExpressions;
	}

	/**
	 * Stablishes the expresion that, when it is evaluated, returns the text
	 * which will be showed by the label.
	 *
	 * @param labelExpression String
	 */
	public void setLabelExpressions(String[] lbl_exps) {
		if (lbl_exps == null) {
			this.labelExpressions = new String[0];
		} else {
			this.labelExpressions = lbl_exps;
		}

	}

	/**
	 * Returns the text symbol that is being used for the text(the font,
	 * size,style,aligment)
	 *
	 * @return label ITextSymbol
	 */
	public ITextSymbol getTextSymbol() {
		if (textSymbol == null) {
			textSymbol = new SimpleTextSymbol();
		}
		return textSymbol;
	}

	private Dimension getSize() {
		if (labelStyle == null) {
			if (texts!=null && texts.length >0) {
				String t = "";
				for (int i = 0; i < texts.length; i++) {
					t += texts[i];
				}
				getTextSymbol().setText(t);
			}

			Rectangle bounds = getTextSymbol().getBounds();
			bounds.setLocation(
					(int) Math.round(bounds.getX()),
					(int) Math.round(bounds.getY()+bounds.getHeight()));
			return new Dimension(bounds.width, bounds.height);
		} else {
			labelStyle.setTextFields(texts);
			return labelStyle.getSize();
		}
	}
	/**
	 * Stablishes the text symbol that is going to be used for the text(the
	 * font,size,style,aligment)
	 *
	 * @param textSymbol ITextSymbol
	 */
	public void setTextSymbol(ITextSymbol textSymbol) {
		this.textSymbol = textSymbol;
		if (textSymbol == null) {
			this.textSymbol = new SimpleTextSymbol();
		}
		setReferenceSystem(referenceSystem);
		setUnit(unit);
	}

	/**
	 * Stablishes the style for the label.
	 *
	 * @param labelStyle ILabelStyle
	 */
	public void setLabelStyle(ILabelStyle labelStyle) {
		this.labelStyle = labelStyle;
	}

	/**
	 * Returns the style of the label
	 *
	 */
	public ILabelStyle getLabelStyle() {
		return this.labelStyle;
	}

	/**
	 * Returns the name of the label
	 *
	 */
	public String getName() {
		return theName;
	}

	/**
	 * Stablishes the name of the label
	 * @param name
	 */
	public void setName(String name) {
	    if (name == null) {
            theName = "";
	    } else {
	        theName = name;
	    }
	}

	public String toString() {
		// for debugging
		//		return name+"{label expression="+labelExpression+", visible="+isVisible+", priority="+priority+"}";
		return getName();
	}

	/**
	 * Sets the text for the label
	 *
	 * @param texts String[]
	 */
	public void setTexts(String[] texts) {
		this.texts = null;
		this.texts = texts;

	}

	/**
	 * Return the text for the label
	 *
	 * @param texts String[]
	 */
	public String[] getTexts() {
		return this.texts;
	}

	/**
	 * <p>
	 * LabelLocationMetrics, contains the anchor point, rotation, and some
	 * other geometric calculations computed by the PlacementManager.
	 * </p>
	 *
	 * <p>
	 * The shp argument is passed as an accessory for subclasses of this
	 * class in case they need futher geometric calculations
	 * </p>
	 * @param graphics, graphics to use to paint the label.
	 * @param llm, concrete settings of the placement of this layer
	 * @param shp, the Shape over whose the label is painted
	 */
	public void draw(Graphics2D graphics, ILabelLocationMetrics llm, Geometry geom) {
		if (scale == 0)
			return;


		Dimension size = getSize();
		int width = (int) Math.round(size.getWidth()*scale);
		if (width  < 1)
			return;

		int height = (int) Math.round(size.getHeight()*scale);
		if (height < 1)
			return;

		Rectangle r = new Rectangle(0,0, width, height);
		org.gvsig.fmap.geom.primitive.Point anchor;
		try {
			anchor = geomManager.createPoint(llm.getAnchor().getX(), llm.getAnchor().getY(), SUBTYPES.GEOM2D);
			double xAnchor = anchor.getX();
			double yAnchor = anchor.getY();
			double theta = llm.getRotation();

			graphics.translate(xAnchor, yAnchor);
			graphics.rotate(theta);
			synchronized (this) {
				float fontSizeBefore = textSymbol.getFont().getSize2D();
				try {
					textSymbol.setFontSize(fontSizeBefore*scale);
					drawInsideRectangle(graphics, r);
					textSymbol.setFontSize(fontSizeBefore);
				} catch (SymbolDrawingException e) {
					logger.warn("Error drawing", e);
				}
			}
			graphics.rotate(-theta);
			graphics.translate(-xAnchor, -yAnchor);
		} catch (CreateGeometryException e1) {
			logger.warn("Error creating a point", e1);
		}
	}

	private void relativeToAbsolute(double[] xy, Rectangle r, Dimension labelSz, double ratioLabel, double ratioViewPort) {
		int x;
		int y;
		if (ratioViewPort > ratioLabel) {
			// size is defined by the viewport height
			y = (int) (r.height*xy[1]);
			x = (int) ((0.5*r.width) - (0.5-xy[0])*(ratioLabel*r.height));
		} else {
			// size is defined by the viewport width
			x = (int) (r.width * xy[0]);
			y = (int) ((0.5 * r.height) - (0.5-xy[1])*(r.width/ratioLabel));
		}
		xy[0] = x;
		xy[1] = y;
	}

	/**
	 * Useful to render a Label with size inside little rectangles.
	 *
	 * @param graphics Graphics2D
	 * @param bounds Rectangle
	 * @throws SymbolDrawingException
	 */
	public void drawInsideRectangle(Graphics2D graphics, Rectangle bounds) throws SymbolDrawingException {
		if (labelStyle != null) {
			labelStyle.drawInsideRectangle(graphics, bounds);
			Rectangle2D[] textBounds = labelStyle.getTextBounds();
			Dimension labelSz = getSize();
			final double ratioLabel = labelSz.getWidth()/labelSz.getHeight();
			final double ratioViewPort = bounds.getWidth() / bounds.getHeight();
			final double[] xy = new double[2];


			// draw the text fields
			if (textBounds.length > 0 && texts!=null) {
				for (int i = 0; i < textBounds.length && i < texts.length; i++) {
					getTextSymbol().setText(texts[i]);
					Rectangle2D textFieldArea = textBounds[i];
					xy[0] = textFieldArea.getX();
					xy[1] = textFieldArea.getY();
					relativeToAbsolute(xy, bounds, labelSz, ratioLabel, ratioViewPort);
					int x = (int) Math.round(xy[0]);
					int y = (int) Math.round(xy[1]);

					xy[0] = textFieldArea.getMaxX();
					xy[1] = textFieldArea.getMaxY();
					relativeToAbsolute(xy, bounds, labelSz, ratioLabel, ratioViewPort);
					int width = (int) Math.round(xy[0]) -x;
					int height = (int) Math.round(xy[1] - y) ;

					Rectangle textRect = new Rectangle(x, y, width, height);
					Shape oldClip = graphics.getClip();
					graphics.setClip(textRect);
					getTextSymbol().drawInsideRectangle(graphics, null, textRect, null);
					graphics.setClip(oldClip);
				}
			}
		} else {

			if (texts != null && texts.length>0){
				String fullText = "";
				for(int ig=0; ig<texts.length; ig++){
					fullText = fullText + texts[ig];
				}
				getTextSymbol().setText(fullText);
			}
			getTextSymbol().drawInsideRectangle(graphics, null, bounds, null);
		}
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Geometry getShape(ILabelLocationMetrics llm) throws CreateGeometryException {
		if (llm==null)
			return null;
		Point2D anchor = llm.getAnchor();
		double theta = llm.getRotation();

		// 2. calculate the container shape
		Polygon returnedValue;
		Rectangle bounds = getBounds();

		AffineTransform at = AffineTransform.getTranslateInstance(anchor.getX(), anchor.getY());
		at.concatenate(AffineTransform.getRotateInstance(theta));
		returnedValue = geomManager.createPolygon(SUBTYPES.GEOM2D);
		returnedValue.addVertex(bounds.getMinX(), bounds.getMinY());
		returnedValue.addVertex(bounds.getMinX(), bounds.getMaxY());
		returnedValue.addVertex(bounds.getMaxX(), bounds.getMaxY());
		returnedValue.addVertex(bounds.getMaxX(), bounds.getMinY());
		returnedValue.addVertex(bounds.getMinX(), bounds.getMinY());

		returnedValue.transform(at);
		return returnedValue;
	}

	public String getClassName() {
		return getClass().getName();
	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		Dimension sz = getSize();
		double width = sz.getWidth();
		double height = sz.getHeight();
		return CartographicSupportToolkit.
		getCartographicLength(this,
				Math.max(width, height),
				viewPort,
				dpi);
	}

	public int getReferenceSystem() {
		return referenceSystem;
	}

	public int getUnit() {
		return unit;
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		Dimension sz = getSize();
		double width = sz.getWidth();
		double height = sz.getHeight();
		if (width >= height) {
			scale = cartographicSize / width;
		} else {
			scale = cartographicSize / height;
		}
	}

	public void setReferenceSystem(int referenceSystem) {
		this.referenceSystem = referenceSystem;
		if (textSymbol != null && textSymbol instanceof CartographicSupport) {
			((CartographicSupport) textSymbol).setReferenceSystem(referenceSystem);
		}
	}

	public void setUnit(int unitIndex) {
		this.unit = unitIndex;
		if (textSymbol != null && textSymbol instanceof CartographicSupport) {
			((CartographicSupport) textSymbol).setUnit(unitIndex);
		}
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		setCartographicSize(getCartographicSize(
				viewPort,
				dpi,
				geom),
				geom);
		return 0;
	}

	public Rectangle getBounds() {
		Dimension cBounds = getSize();
		return new Rectangle(
				0,
				0,
				(int) Math.round(cBounds.width*scale),
				(int) Math.round(cBounds.height*scale));
	}

	public String getSQLQuery() {
		if (sqlQuery == null) sqlQuery = "";
		return sqlQuery;
	}

	public void setSQLQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seeorg.gvsig.tools.persistence.Persistent#saveToState(org.gvsig.tools.
	 * persistence.PersistentState)
	 */
	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_NAME, getName());
		state.set(FIELD_TEXT_SYMBOL, getTextSymbol());
		state.set(FIELD_LABEL_EXPRESSIONS, getLabelExpressions());
		state.set(FIELD_VISIBLE, isVisible());

		ILabelStyle sty = getLabelStyle();
		if (sty != null) {
			state.set(FIELD_LABEL_STYLE, getLabelStyle());
		}

		state.set(FIELD_TEXTS, getTexts());
		state.set(FIELD_PRIORITY, getPriority());

		state.set(FIELD_SCALE, scale);
		state.set(FIELD_SQL_QUERY, getSQLQuery());
		state.set(FIELD_IS_USESQL, isUseSqlQuery());

		state.set(FIELD_UNIT, getUnit());
		state.set(FIELD_REFERENCE_SYSTEM, getReferenceSystem());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.tools.persistence.Persistent#loadFromState(org.gvsig.tools.
	 * persistence.PersistentState)
	 */
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setName(state.getString(FIELD_NAME));
		setTextSymbol((ITextSymbol) state.get(FIELD_TEXT_SYMBOL));
		setLabelExpressions(state.getStringArray(FIELD_LABEL_EXPRESSIONS));
		setVisible(state.getBoolean(FIELD_VISIBLE));

		if (state.hasValue(FIELD_LABEL_STYLE)) {
			setLabelStyle((ILabelStyle) state.get(FIELD_LABEL_STYLE));
		}

		setTexts((String[]) state.getArray(FIELD_TEXTS, String.class));
		setPriority(state.getInt(FIELD_PRIORITY));
		scale = state.getDouble(FIELD_SCALE);

		setUseSqlQuery(state.getBoolean(FIELD_IS_USESQL));
		setSQLQuery(state.getString(FIELD_SQL_QUERY));

		setUnit(state.getInt(FIELD_UNIT));
		setReferenceSystem(state.getInt(FIELD_REFERENCE_SYSTEM));
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(LABEL_CLASS_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						LabelClass.class,
						LABEL_CLASS_PERSISTENCE_DEFINITION_NAME,
						LABEL_CLASS_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);

				definition.addDynFieldString(FIELD_NAME).setMandatory(true);
				definition.addDynFieldObject(FIELD_TEXT_SYMBOL).setMandatory(true).setClassOfValue(ITextSymbol.class);

				definition.addDynFieldArray(FIELD_LABEL_EXPRESSIONS)
				.setClassOfItems(String.class).setMandatory(true);

				definition.addDynFieldBoolean(FIELD_VISIBLE).setMandatory(true);

				definition.addDynFieldObject(FIELD_LABEL_STYLE).setMandatory(false)
				.setClassOfValue(ILabelStyle.class);

				definition.addDynFieldList(FIELD_TEXTS).setMandatory(true).setClassOfItems(String.class);
				definition.addDynFieldInt(FIELD_PRIORITY).setMandatory(true);
				definition.addDynFieldDouble(FIELD_SCALE).setMandatory(true);
				definition.addDynFieldString(FIELD_SQL_QUERY).setMandatory(true);
				definition.addDynFieldBoolean(FIELD_IS_USESQL).setMandatory(true);
				definition.addDynFieldInt(FIELD_UNIT).setMandatory(true);
				definition.addDynFieldInt(FIELD_REFERENCE_SYSTEM).setMandatory(true);
			}
			return Boolean.TRUE;
		}

	}

	public void setUseSqlQuery(boolean use_sql) {
		this.usesSQL = use_sql;
	}

	public boolean isUseSqlQuery() {
		return this.usesSQL;
	}

	public String getStringLabelExpression(){
		String expr = "";

		if(labelExpressions != null && labelExpressions.length > 0) {

			for (int i = 0; i < labelExpressions.length-1; i++) {
				expr += (String) labelExpressions[i] +  ":";//EOFIELD
			}
			expr += labelExpressions[labelExpressions.length - 1];

		} else {
			expr = "";
		}
		return expr;
	}

    public Object clone() throws CloneNotSupportedException {

	PersistenceManager persman = ToolsLocator.getPersistenceManager();

        PersistentContext context = null;
        Object resp = null;
        PersistentState state = null;
        try {
            state = persman.getState(this, true);
        } catch (PersistenceException e) {
            logger.warn("Can't clone, return me !!!!!!!!!!!!!!", e);
            return this;
        }
        context = state.getContext();
        if (context instanceof PersistentContextServices) {
            /*
             * Ensure that previous instances are not used,
             * so objects are recreated
             */
            ((PersistentContextServices) context).clear();
        }
        try {
            resp = persman.create(state);
        } catch (Exception e) {
            logger.warn("Can't clone, return me !!!!!!!!!!!!!!", e);
            return this;
        }
        return resp;

    }




}