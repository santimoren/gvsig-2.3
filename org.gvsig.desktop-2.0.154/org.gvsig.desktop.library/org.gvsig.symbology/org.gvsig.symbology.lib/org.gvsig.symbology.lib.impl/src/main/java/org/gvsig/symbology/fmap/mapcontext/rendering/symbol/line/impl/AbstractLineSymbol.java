/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.AbstractSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ILineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleLineStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;


/**
 * AbstractLineSymbol is the class that implements the interface for line symbols.
 * It is considered as the father of all the XXXLineSymbols and will implement all the
 * methods that these classes had not developed (and correspond with one of the methods
 * of AbstractLineSymbol class).
 *
 *
 * @author 2005-2009 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractLineSymbol extends AbstractSymbol implements ILineSymbol {

    private static final String FIELD_LINESTYLE = "lineStyle";

    private static final String FIELD_COLOR = "color";

    public static final String LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME = "LineSymbol";


	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private Color color;
	private ILineStyle lineStyle = new SimpleLineStyle();

	public AbstractLineSymbol() {
		super();
		color =
				MapContextLocator.getSymbolManager()
						.getSymbolPreferences()
						.getDefaultSymbolFillColor();
	}

	public Color getColor() {
		return color;
	}

	public void setLineColor(Color color) {
		this.color = color;
	}

	public void setColor(Color color) {
		setLineColor(color);
	}

	public int getOnePointRgb() {
		return color.getRGB();
	}

	public final int getSymbolType() {
		return Geometry.TYPES.CURVE;
	}

	public void getPixExtentPlus(Geometry geom, float[] distances, ViewPort viewPort, int dpi) {
		float cs = (float) getCartographicSize(viewPort, dpi, geom);
		// TODO and add the line offset
		distances[0] = cs;
		distances[1] = cs;
	}

	public boolean isSuitableFor(Geometry geom) {
		switch(geom.getType()) {
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
		case Geometry.TYPES.LINE:
			return true;
		}
		return false;
	}

	public ILineStyle getLineStyle() {
		return lineStyle;
	}

	public void setLineStyle(ILineStyle lineStyle) {
		this.lineStyle = lineStyle;
	}

	public int getAlpha() {
		return color.getAlpha();
	}

	public void setAlpha(int outlineAlpha) {
		color = new Color(color.getRed(), color.getGreen(), color.getBlue(), outlineAlpha);
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		final int hGap = (int) (r.getWidth() * 0.1); // the left and right margins
		final int vPos = 1; 						 // the top and bottom margins
		final int splitCount = 3; 					 // number of lines
		final int splitHSize = (r.width - hGap - hGap) / splitCount;
		int hPos = hGap;
		boolean swap = false;

		GeneralPathX gpx = new GeneralPathX();
		gpx.moveTo(r.x + hPos, r.y + r.height-vPos);

		boolean hasDecorator = (getLineStyle().getArrowDecorator() != null);
		boolean hasPictureDeco = hasDecorator &&
		    getLineStyle().getArrowDecorator().getMarker() instanceof IPictureMarkerSymbol;

		for (int i = 0; i < splitCount; i++) {
			swap = !swap;
			if (i == (splitCount-1) && hasPictureDeco) {
			    /*
			     * If we have a picture decorator, the last segment is shorter so that
			     * the decorator is entirely inside the rectangle
			     */
			    gpx.lineTo(r.x + hPos + splitHSize / 2, r.y + r.height/2);
			} else {
			    gpx.lineTo(r.x + hPos + splitHSize, (swap ? vPos : r.height-vPos) + r.y);
			}

			hPos += splitHSize;
		}


		double old_deco_size = 0;
		if (hasDecorator) {
		    old_deco_size = getLineStyle().getArrowDecorator().getMarker().getSize();
		    /*
		     * Temporarily resize decorator so it fits inside the rectangle
		     */
		    getLineStyle().getArrowDecorator().getMarker().setSize(0.75 * r.getHeight());
		}

		try {
			if (properties==null)
				draw(g, new AffineTransform(), geomManager.createCurve(gpx, SUBTYPES.GEOM2D), null, null);
			else
				print(g, new AffineTransform(), geomManager.createCurve(gpx, SUBTYPES.GEOM2D), properties);
		} catch (Exception e) {
			throw new SymbolDrawingException(SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
		}

		if (hasDecorator) {
            getLineStyle().getArrowDecorator().getMarker().setSize(old_deco_size);
		}

	}



	public void setCartographicSize(double cartographicSize, Geometry geom) {
	    float currw = this.getLineStyle().getLineWidth();
	    double ratio = cartographicSize / currw;

		getLineStyle().setLineWidth((float) cartographicSize);

		if (getLineStyle().getArrowDecorator() != null) {
	        double marker_size = getLineStyle().getArrowDecorator().getMarker().getSize();
	        marker_size = marker_size * ratio;
	        getLineStyle().getArrowDecorator().getMarker().setSize(marker_size);
		}
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double oldSize = getLineWidth();
		setCartographicSize(getCartographicSize(
								viewPort,
								dpi,
								geom),
							geom);
		return oldSize;
	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return CartographicSupportToolkit.
					getCartographicLength(this,
										  getLineWidth(),
										  viewPort,
										  dpi);
	}

	public static Geometry offsetFShape(Geometry shp, double offset) {
		Geometry offsetFShape = null;
		if (shp != null) {
			if (offset == 0)
				return shp;

			List<Point2D[]> segments = new ArrayList<Point2D[]>();
			GeneralPathX gpx = new GeneralPathX(shp.getPathIterator(null));
			PathIterator it = gpx.getPathIterator(null);
			double[] data = new double[6];
			Point2D segmentIni = null;
			Point2D segmentEnd = null;
			while (!it.isDone()) {
				switch (it.currentSegment(data)) {
				case PathIterator.SEG_MOVETO:
					segmentEnd = new Point2D.Double(
							data[0], data[1]);
					break;

				case PathIterator.SEG_LINETO:
					segmentEnd = segmentIni;
					segmentIni = new Point2D.Double(
							data[0], data[1]);

					segments.add(getParallel(segmentIni, segmentEnd, offset));
					break;

				case PathIterator.SEG_QUADTO:
					break;

				case PathIterator.SEG_CUBICTO:
					break;

				case PathIterator.SEG_CLOSE:
					break;
				}
			}

		}


		return offsetFShape;
	}

	private static Point2D[] getParallel(Point2D p1, Point2D p2, double distance) {
		Point2D[] pParallel=new Point2D[2];
		pParallel[0]=getPerpendicularPoint(p1,p2,p1,distance);
		pParallel[1]=getPerpendicularPoint(p1,p2,p2,distance);
		return pParallel;
	}

	/**
	 * Obtiene el punto que se encuentra a una distancia 'dist' de la recta
	 * p1-p2 y se encuentra en la recta perpendicular que pasa por perpPoint
	 *
	 * @param p1 Punto de la recta p1-p2
	 * @param p2 Punto de la recta p1-p2
	 * @param perpPoint Punto de la recta perpendicular
	 * @param dist Distancia del punto que se quiere obtener a la recta p1-p2
	 *
	 * @return DOCUMENT ME!
	 */
	private static Point2D getPerpendicularPoint(Point2D p1, Point2D p2,
		Point2D perpPoint, double dist) {
		Point2D[] p = getPerpendicular(p1, p2, perpPoint);
		Point2D unit = getUnitVector(p[0], p[1]);

		return new Point2D.Double(perpPoint.getX() + (unit.getX() * dist),
			perpPoint.getY() + (unit.getY() * dist));
	}


	/**
	 * Obtiene un par de puntos que definen la recta perpendicular a p1-p2 que
	 * pasa por el punto perp
	 *
	 * @param p1 punto de la recta p1-p2
	 * @param p2 punto de la recta p1-p2
	 * @param perp Punto por el que pasa la recta perpendicular, debe ser
	 * 		  distinto a p2
	 *
	 * @return Array con dos puntos que definen la recta resultante
	 */
	private static Point2D[] getPerpendicular(Point2D p1, Point2D p2,
		Point2D perp) {
		if ((p2.getY() - p1.getY()) == 0) {
			return new Point2D[] {
				new Point2D.Double(perp.getX(), 0),
				new Point2D.Double(perp.getX(), 1)
			};
		}

		//Pendiente de la recta perpendicular
		double m = (p1.getX() - p2.getX()) / (p2.getY() - p1.getY());

		//b de la funcion de la recta perpendicular
		double b = perp.getY() - (m * perp.getX());

		//Obtenemos un par de puntos
		Point2D[] res = new Point2D[2];

		res[0] = new Point2D.Double(0, (m * 0) + b);
		res[1] = new Point2D.Double(1000, (m * 1000) + b);

		return res;
	}

	/**
	 * Devuelve un vector unitario en forma de punto a partir de dos puntos.
	 *
	 * @param p1 punto origen.
	 * @param p2 punto destino.
	 *
	 * @return vector unitario.
	 */
	private static Point2D getUnitVector(Point2D p1, Point2D p2) {
		Point2D paux = new Point2D.Double(p2.getX() - p1.getX(),
				p2.getY() - p1.getY());
		double v = Math.sqrt(Math.pow(paux.getX(), 2d) +
				Math.pow(paux.getY(), 2d));
		paux = new Point2D.Double(paux.getX() / v, paux.getY() / v);

		return paux;
	}
	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		double originalSize = getLineWidth();
		double size=originalSize;
		// scale it to size
		int pq = properties.getPrintQuality();
		if (pq == PrintAttributes.PRINT_QUALITY_NORMAL){
			size *= (double) 300/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_HIGH){
			size *= (double) 600/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_DRAFT){
			// size *= 72/72; // (which is the same than doing nothing)
		}
		setLineWidth(size);
		draw(g,at,geom,null, null);
		setLineWidth(originalSize);
	}

	public Object clone() throws CloneNotSupportedException {
		AbstractLineSymbol copy = (AbstractLineSymbol) super.clone();

		// Clone the line style
		if (lineStyle != null) {
			copy.lineStyle = (ILineStyle) lineStyle.clone();
		}

		return copy;
	}


	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent symbol properties
		super.loadFromState(state);
		// Set own properties
		setColor((Color) state.get(FIELD_COLOR));
		setLineStyle((ILineStyle) state.get(FIELD_LINESTYLE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent symbol properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_COLOR, getColor());
		state.set(FIELD_LINESTYLE, getLineStyle());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractLineSymbol.class,
						LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);

				// Extend the Symbol base definition
				definition.extend(manager.getDefinition(SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Color
				definition.addDynFieldObject(FIELD_COLOR).setMandatory(false).setClassOfValue(Color.class);
				// LineStyle
				definition.addDynFieldObject(FIELD_LINESTYLE).setMandatory(false).setClassOfValue(ILineStyle.class);
			}
			return Boolean.TRUE;
		}

	}


}