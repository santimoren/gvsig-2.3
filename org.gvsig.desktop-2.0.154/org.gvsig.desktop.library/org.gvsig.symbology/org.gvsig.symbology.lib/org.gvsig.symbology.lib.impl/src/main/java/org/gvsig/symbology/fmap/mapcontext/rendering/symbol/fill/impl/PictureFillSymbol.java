/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IPictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.BackgroundFileStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleMarkerFillPropertiesStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PictureFillSymbol allows to use an image file suported by gvSIG as a padding
 * for the polygons.This image can be modified using methods to scale or rotate it.
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class PictureFillSymbol extends AbstractFillSymbol implements IPictureFillSymbol {
	private static final Logger logger = LoggerFactory.getLogger(PictureFillSymbol.class);

    public static final String PICTURE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME =
        "PictureFillSymbol";
    private static final String SELECTED = "selected";
    private static final String SELECTION_SYMBOL = "selectionSym";
    private static final String BACKGROUND_IMAGE = "bgImage";
    private static final String BACKGROUND_SELECTION_IMAGE = "bgSelImage";
    private static final String ANGLE = "angle";
    private static final String X_SCALE = "xScale";
    private static final String Y_SCALE = "yScale";
    private static final String MARKER_FILL_PROPERTIES = "markerFillProperties";

	private double angle = 0;
	private double xScale = 1;
	private double yScale = 1;
	transient private PictureFillSymbol selectionSym;
	private boolean selected;
	private IMarkerFillPropertiesStyle markerFillProperties = new SimpleMarkerFillPropertiesStyle();
	private BackgroundFileStyle bgImage;
	private BackgroundFileStyle bgSelImage;
	private PrintAttributes properties;
	private GeometryManager geometryManager = GeometryLocator.getGeometryManager();

	private void draw(Graphics2D g, AffineTransform affineTransform, Geometry geom, Cancellable cancel) {
 		Color fillColor = getFillColor();

 		Shape transf_geom = geom.getShape(affineTransform);

		if (fillColor != null) {
			g.setColor(fillColor);
			g.fill(transf_geom);
		}

		g.setClip(transf_geom);
		BackgroundFileStyle bg = (!selected) ? bgImage : bgSelImage ;
		if (bg != null && bg.getBounds() != null){
			int sizeW=(int) ((int)bg.getBounds().getWidth() * xScale);
			int sizeH=(int) ((int)bg.getBounds().getHeight() * yScale);
			Rectangle rProv = new Rectangle();
			rProv.setFrame(0,0,sizeW,sizeH);
			Paint resulPatternFill = null;
			BufferedImage sample= null;

			sample= new BufferedImage(sizeW,sizeH,BufferedImage.TYPE_INT_ARGB);
			Graphics2D gAux = sample.createGraphics();

			double xSeparation = markerFillProperties.getXSeparation(); // TODO apply CartographicSupport
			double ySeparation = markerFillProperties.getYSeparation(); // TODO apply CartographicSupport
			double xOffset = markerFillProperties.getXOffset();
			double yOffset = markerFillProperties.getYOffset();

			try {
				bg.drawInsideRectangle(gAux, rProv);
			} catch (SymbolDrawingException e) {
				logger.warn(Messages.getText("label_style_could_not_be_painted"), e);
			}

			Dimension sz = rProv.getSize();
			sz = new Dimension((int) Math.round(sz.getWidth()+(xSeparation)),
					(int) Math.round(sz.getHeight() + (ySeparation)));
			rProv.setSize(sz);

			BufferedImage bi = new BufferedImage(rProv.width, rProv.height, BufferedImage.TYPE_INT_ARGB);
			gAux = bi.createGraphics();
			gAux.drawImage(sample, null, (int) (xSeparation*0.5), (int) (ySeparation*0.5));

			rProv.x = rProv.x+(int)xOffset;
			rProv.y = rProv.y+(int)yOffset;
			resulPatternFill = new TexturePaint(bi,rProv);

			g.setColor(null);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			g.setPaint(resulPatternFill);
		}

		if (angle == 0) {
		    g.fill(transf_geom);
		} else {

            AffineTransform at = new AffineTransform();
            at.rotate(angle);

		    Geometry pixels_geom = geom.cloneGeometry();
		    pixels_geom.transform(affineTransform);
		    pixels_geom.transform(at);

		    g.rotate(-angle);
		    g.fill(pixels_geom.getShape());
		    g.rotate(angle);
		}

		g.setClip(null);
		if (getOutline()!=null) {
			getOutline().draw(g, affineTransform, geom, null, cancel);
		}
	}

	/**
	 * Constructor method
	 *
	 */
	public PictureFillSymbol() {
		super();
	}
	/**
	 * Constructor method
	 * @param imageURL, URL of the normal image
	 * @param selImageURL, URL of the image when it is selected in the map
	 * @throws IOException
	 */

	public PictureFillSymbol(URL imageURL, URL selImageURL) throws IOException {
		setImage(imageURL);
		if (selImageURL!=null)
			setSelImage(selImageURL);
		else setSelImage(imageURL);
	}


	/**
	 * Sets the URL for the image to be used as a picture fill symbol (when it is selected in the map)
	 * @param imageFile, File
	 * @throws IOException
	 */
	public void setSelImage(URL selImageUrl) throws IOException{

		bgSelImage= BackgroundFileStyle.createStyleByURL(selImageUrl);
//		selImagePath = selImageUrl.toString();
	}



	/**
	 * Defines the URL from where the picture to fill the polygon is taken.
	 * @param imageFile
	 * @throws IOException
	 */
	public void setImage(URL imageUrl) throws IOException{
		bgImage = BackgroundFileStyle.createStyleByURL(imageUrl);
	}



	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		Surface surf;
		try {
			surf = geometryManager.createSurface(new GeneralPathX(r.getPathIterator(new AffineTransform())), SUBTYPES.GEOM2D);
			if (properties==null){
				//			draw(g, null, new FPolygon2D(new GeneralPathX(r)), null);
				draw(g, null, surf, null);
			} else {
				print(g, new AffineTransform(), surf, properties);
			}
		} catch (CreateGeometryException e) {
			throw new SymbolDrawingException(TYPES.POINT);
		}
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSym == null) {
			selectionSym = (PictureFillSymbol) cloneForSelection();
			selectionSym.selected=true;
			selectionSym.selectionSym = selectionSym; // avoid too much lazy creations
		}else {
		    selectionSym.setFillColor(MapContext.getSelectionColor());
		}
		return selectionSym;

	}

	public int getSymbolType() {
		return Geometry.TYPES.SURFACE;
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom,
			PrintAttributes properties) {
		this.properties=properties;
        draw(g, at, geom, null);
        this.properties=null;
	}
	/**
	 * Returns the IMarkerFillProperties that allows this class to treat the picture as
	 * a marker in order to scale it, rotate it and so on.
	 * @return markerFillProperties,IMarkerFillPropertiesStyle
	 */
	public IMarkerFillPropertiesStyle getMarkerFillProperties() {
		return markerFillProperties;
	}

	/**
	 * Sets the MarkerFillProperties in order to allow the user to modify the picture as
	 * a marker (it is possible to scale it, rotate it and so on)
	 * @param prop
	 */
	public void setMarkerFillProperties(IMarkerFillPropertiesStyle prop) {
		this.markerFillProperties = prop;
	}
	/**
	 * Defines the angle for the rotation of the image when it is added to create the
	 * padding
	 *
	 * @return angle
	 */
	public double getAngle() {
		return angle;
	}
	/**
	 * Sets the angle for the rotation of the image when it is added to create the padding
	 * @param angle
	 */
	public void setAngle(double angle) {
		this.angle = angle;
	}

	/**
	 * Defines the scale for the x axis of the image when it is added to create the
	 * padding
	 * @return xScale
	 */
	public double getXScale() {
		return xScale;
	}
	/**
	 * Returns the scale for the x axis of the image when it is added to create the
	 * padding
	 * @param xScale
	 */
	public void setXScale(double xScale) {
		this.xScale = xScale;
	}
	/**
	 * Defines the scale for the y axis of the image when it is added to create the
	 * padding
	 * @return yScale
	 */
	public double getYScale() {
		return yScale;
	}
	/**
	 * Returns the scale for the y axis of the image when it is added to create the
	 * padding
	 * @param yScale
	 */
	public void setYScale(double yScale) {
		this.yScale = yScale;
	}

	/**
	 * Returns the URL of the image that is used to create the padding to fill the
	 * polygon
	 * @return imagePath
	 */
	public URL getSource() {
		if (bgImage != null){
			return bgImage.getSource();
		}
		return null;
	}

	/**
	 * Returns the URL of the image used when the polygon is selected
	 * @return
	 */
	public URL getSelectedSource(){
		if( bgSelImage != null){
			return bgSelImage.getSource();
		}
		return null;
	}



	@Override
	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return super.toCartographicSize(viewPort, dpi, geom);
		// this symbol cannot apply any cartographic transfomation to its filling
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature f, Cancellable cancel) {
		draw(g, affineTransform, geom, cancel);

	}

    public Object clone() throws CloneNotSupportedException {
    	PictureFillSymbol copy = (PictureFillSymbol) super.clone();

        // clone selection
    	if (selectionSym != null) {
    		//Parche por lo que hace el m�todo getSymbolForSelection
    		if (this == selectionSym){
    			copy.selectionSym = copy;
    		} else {
    			copy.selectionSym = (PictureFillSymbol) selectionSym.clone();
    		}
    	}

        // clone brackground image
        if (bgImage != null) {
            copy.bgImage = (BackgroundFileStyle) bgImage.clone();
        }

        // clone selection brackground image
        if (bgSelImage != null) {
            copy.bgSelImage = (BackgroundFileStyle) bgSelImage.clone();
        }

        // FIXME: clone properties

        return copy;
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        // Set parent style properties
        super.loadFromState(state);

        this.selected = (Boolean) state.get(SELECTED);
        this.selectionSym = (PictureFillSymbol) state.get(SELECTION_SYMBOL);
        this.bgImage = (BackgroundFileStyle) state.get(BACKGROUND_IMAGE);
        this.bgSelImage =
            (BackgroundFileStyle) state.get(BACKGROUND_SELECTION_IMAGE);


        this.angle = (Double) state.get(ANGLE);
        this.xScale = (Double) state.get(X_SCALE);
        this.yScale = (Double) state.get(Y_SCALE);
        this.markerFillProperties = (IMarkerFillPropertiesStyle) state.get(MARKER_FILL_PROPERTIES);


    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent fill symbol properties
        super.saveToState(state);

        // Save own properties
        state.set(SELECTED, this.selected);
        state.set(SELECTION_SYMBOL, this.getSymbolForSelection());
        state.set(BACKGROUND_IMAGE, this.bgImage);
        state.set(BACKGROUND_SELECTION_IMAGE, this.bgSelImage);
        state.set(ANGLE, this.angle);
        state.set(X_SCALE, this.xScale);
        state.set(Y_SCALE, this.yScale);
        state.set(MARKER_FILL_PROPERTIES, this.markerFillProperties);
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(PICTURE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition =
                    manager.addDefinition(PictureFillSymbol.class,
                    		PICTURE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
                    		PICTURE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME
                            + " Persistence definition",
                        null,
                        null);

                // Extend the Style base definition
                definition.extend(manager.getDefinition(FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldBoolean(SELECTED).setMandatory(false);
                definition.addDynFieldObject(SELECTION_SYMBOL)
                    .setClassOfValue(PictureFillSymbol.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_SELECTION_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);
                definition.addDynFieldDouble(ANGLE).setMandatory(true);
                definition.addDynFieldDouble(X_SCALE).setMandatory(true);
                definition.addDynFieldDouble(Y_SCALE).setMandatory(true);
                definition.addDynFieldObject(MARKER_FILL_PROPERTIES)
                .setClassOfValue(IMarkerFillPropertiesStyle.class).setMandatory(true);
            }
            return Boolean.TRUE;
        }
    }

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
	        SymbolManager manager = MapContextLocator.getSymbolManager();

	        manager.registerSymbol(PICTURE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
	            PictureFillSymbol.class);

			return Boolean.TRUE;
		}

	}

	public IBackgroundFileStyle getBackgroundFileStyle() {
		return this.bgImage;
	}

	public IBackgroundFileStyle getSelectedBackgroundFileStyle() {
		return this.bgSelImage;
	}

}
