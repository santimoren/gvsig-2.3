/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {gvSIG}  {{Task}}
 */
package org.gvsig.symbology.impl;

import org.gvsig.fmap.mapcontext.MapContextLibrary;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialIntervalLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialUniqueValueLegend;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.SymbologyLibrary;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.driver.impl.PersistenceBasedLegendReader;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.driver.impl.PersistenceBasedLegendWriter;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.AbstractClassifiedVectorLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.AbstractIntervalLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.AbstractLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.AbstractVectorialLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.FInterval;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.SingleSymbolLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.VectorialIntervalLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.VectorialUniqueValueLegend;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.AttrInTableLabelingStrategy;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.DefaultLabelingMethod;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelClass;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelClassFactory;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.ZoomConstraintsImpl;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.AbstractFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.MarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.MultiLayerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.PictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.SimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.AbstractSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.MultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.AbstractLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.MultiLayerLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.PictureLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.SimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.AbstractMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.ArrowMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.MultiLayerMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.PictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.SimpleMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.AbstractStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.BackgroundFileStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.DefaultMask;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ImageStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.RemoteFileStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SVGStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleLabelStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleLineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl.SimpleTextSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.warning.impl.WarningSymbol;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.util.Caller;
import org.gvsig.tools.util.impl.DefaultCaller;

/**
 * Library for the Basic Symbology implementation.
 * 
 * @author gvSIG team
 */
public class SymbologyDefaultImplLibrary extends AbstractLibrary {
	
    @Override
    public void doRegistration() {
        registerAsImplementationOf(SymbologyLibrary.class);
        require(MapContextLibrary.class);
    }

	protected void doInitialize() throws LibraryException {
		Caller caller = new DefaultCaller();
		caller.add(new DefaultSymbologyManager.RegisterSymbologyManager());
		if (!caller.call()) {
			throw new LibraryException(SymbologyDefaultImplLibrary.class,
					caller.getExceptions());
		}
	}

    protected void doPostInitialize() throws LibraryException {
        Caller caller = new DefaultCaller();

        /*
         * Add registry of symbols 
         */
        caller.add( new SimpleFillSymbol.RegisterSymbol() );
        caller.add( new MultiLayerFillSymbol.RegisterSymbol() );
        caller.add( new SimpleLineSymbol.RegisterSymbol() );
        caller.add( new MultiLayerLineSymbol.RegisterSymbol() );
        caller.add( new SimpleMarkerSymbol.RegisterSymbol() );
        caller.add( new MultiLayerMarkerSymbol.RegisterSymbol() );
        caller.add( new SimpleTextSymbol.RegisterSymbol() );
        caller.add( new MultiShapeSymbol.RegisterSymbol() );
        caller.add( new WarningSymbol.RegisterSymbol() );

        caller.add( new PictureFillSymbol.RegisterSymbol() );
        caller.add( new PictureLineSymbol.RegisterSymbol() );
        caller.add( new PictureMarkerSymbol.RegisterSymbol() );

//        caller.add( new MarkerFillSymbol.RegisterSymbol() );

        /*
         * Add registry of legends
         */
        caller.add( new VectorialUniqueValueLegend.RegisterLegend() );
        caller.add( new VectorialIntervalLegend.RegisterLegend() );
        caller.add( new SingleSymbolLegend.RegisterLegend() );
        
        /*
         * Add registry of persistent styles
         */
        caller.add( new AbstractStyle.RegisterPersistence() );
        caller.add( new ArrowDecoratorStyle.RegisterPersistence() );
        caller.add( new DefaultMask.RegisterPersistence() );
        caller.add( new SimpleLineStyle.RegisterPersistence() );

        caller.add( new BackgroundFileStyle.RegisterPersistence() );
        caller.add( new ImageStyle.RegisterPersistence() );
        caller.add( new RemoteFileStyle.RegisterPersistence() );
        caller.add( new SVGStyle.RegisterPersistence() );

        /*
         * Add registry of persistent symbols
         */
        caller.add( new AbstractSymbol.RegisterPersistence() );
        caller.add( new AbstractFillSymbol.RegisterPersistence() );
        caller.add( new SimpleFillSymbol.RegisterPersistence() );
        caller.add( new MultiLayerFillSymbol.RegisterPersistence() );
        caller.add( new AbstractLineSymbol.RegisterPersistence() );
        caller.add( new SimpleLineSymbol.RegisterPersistence() );
        caller.add( new MultiLayerLineSymbol.RegisterPersistence() );
        caller.add( new AbstractMarkerSymbol.RegisterPersistence() );
        caller.add( new SimpleMarkerSymbol.RegisterPersistence() );
        caller.add( new ArrowMarkerSymbol.RegisterPersistence() );
        caller.add( new MultiLayerMarkerSymbol.RegisterPersistence() );
        caller.add( new SimpleTextSymbol.RegisterPersistence() );
        caller.add( new MultiShapeSymbol.RegisterPersistence() );
        caller.add( new WarningSymbol.RegisterPersistence() );
                
        caller.add( new PictureFillSymbol.RegisterPersistence() );
        caller.add( new PictureLineSymbol.RegisterPersistence() );
        caller.add( new PictureMarkerSymbol.RegisterPersistence() );
        caller.add( new MarkerFillSymbol.RegisterPersistence() );
        caller.add( new SimpleMarkerFillPropertiesStyle.RegisterPersistence() );


        /*
         * Add registry of persistent legends
         */
        caller.add(new AbstractLegend.RegisterPersistence());
        caller.add(new AbstractVectorialLegend.RegisterPersistence());
        caller.add(new AbstractClassifiedVectorLegend.RegisterPersistence());
        caller.add(new FInterval.RegisterPersistence());
        caller.add(new AbstractIntervalLegend.RegisterPersistence());
        caller.add(new VectorialIntervalLegend.RegisterPersistence());
        caller.add(new VectorialUniqueValueLegend.RegisterPersistence());
        caller.add(new SingleSymbolLegend.RegisterPersistence());

        /*
         * Add registry of labeling classes
         */
        caller.add(new ZoomConstraintsImpl.RegisterPersistence());
        caller.add(new LabelClass.RegisterPersistence());
        caller.add(new DefaultLabelingMethod.RegisterPersistence());
        caller.add(new AttrInTableLabelingStrategy.RegisterPersistence());
                 
        /*
         * Default impl of simple label style
         */
        caller.add(new SimpleLabelStyle.RegisterPersistence());

        /*
         * Do register of all
         */
        if( !caller.call() ) {
        	throw new LibraryException(SymbologyDefaultImplLibrary.class, caller.getExceptions());
        }
        
        // Registering persistence-based legend reader/writer
        // ===============================================
        MapContextManager mcoman = MapContextLocator.getMapContextManager();
        // =================================
        mcoman.registerLegendReader(
            // removing dot
            SymbolManager.LEGEND_FILE_EXTENSION.substring(1),
            PersistenceBasedLegendReader.class);
        // =================================
        mcoman.registerLegendWriter(
            ISingleSymbolLegend.class,
            SymbolManager.LEGEND_FILE_EXTENSION.substring(1),
            PersistenceBasedLegendWriter.class);
        mcoman.registerLegendWriter(
            IVectorialUniqueValueLegend.class,
            SymbolManager.LEGEND_FILE_EXTENSION.substring(1),
            PersistenceBasedLegendWriter.class);
        mcoman.registerLegendWriter(
            IVectorialIntervalLegend.class,
            SymbolManager.LEGEND_FILE_EXTENSION.substring(1),
            PersistenceBasedLegendWriter.class);
        // ===========================================================
        
        SymbologyManager symbologyManager = SymbologyLocator.getSymbologyManager();
        symbologyManager.registerLabelClass(new LabelClassFactory());
    }

}
