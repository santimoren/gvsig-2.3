/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMultiLayerMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * MultiLayerMarkerSymbol allows to group several marker symbols (xxxMarkerSymbol
 * implementing IMarkerSymbol)in one and treat it as an only one symbol.
 *
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiLayerMarkerSymbol extends AbstractMarkerSymbol implements IMarkerSymbol, IMultiLayerSymbol, IMultiLayerMarkerSymbol {

    public static final String MULTILAYER_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME = "MultiLayerMarkerSymbol";

    private static final String FIELD_LAYERS = "layers";
    private static final String FIELD_ROTATION = "rotation";

    private IMarkerSymbol[] layers = new IMarkerSymbol[0];
	private MultiLayerMarkerSymbol selectionSymbol;
	private double markerSize;
	private double rotation;

	public MultiLayerMarkerSymbol() {
		super();
	}

    @Override
	public Color getColor() {
		/*
		 * a multilayer symbol does not define any color, the color
		 * of each layer is defined by the layer itself
		 */
		return null;
	}

	public double getRotation() {
		return rotation;
	}

	public void setColor(Color color) {
		/*
		 * will apply the color to each layer
		 */
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setColor(color);
		}
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public double getSize() {
		double myMarkerSize = 0;

		for (int i = 0; i < getLayerCount(); i++) {
			myMarkerSize = Math.max(myMarkerSize, ((IMarkerSymbol) getLayer(i)).getSize());
		}

		if (markerSize != myMarkerSize) {
			markerSize = myMarkerSize;
		}
		return markerSize;
	}

	public void setSize(double size) {
		if (size > 0 && size != getSize()) {

			double scale = size / getSize();
			this.markerSize = size;
			for (int i = 0; layers != null && i < layers.length; i++) {
				double lSize = layers[i].getSize();
				layers[i].setSize(lSize*scale);
			}
		}
	}



	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		Point p = (Point) geom;
		g.rotate(rotation, p.getX(), p.getY());
		for (int i = 0; (cancel == null || !cancel.isCanceled())
				&& layers != null && i < layers.length; i++) {
			layers[i].draw(g, affineTransform, geom, feature, cancel);
		}
		g.rotate(-rotation, p.getX(), p.getY());
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		g.rotate(rotation, r.getCenterX(), r.getCenterY());
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].drawInsideRectangle(g, scaleInstance, r, properties);
		}
		g.rotate(-rotation, r.getCenterX(), r.getCenterY());
	}

	public int getOnePointRgb() {
		// will paint only the last layer pixel
		return layers[layers.length-1].getOnePointRgb();
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		float[] myDistances = new float[] {0,0};
		distances[0] = 0;
		distances[1] = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].getPixExtentPlus(geom, myDistances, viewPort, dpi);
			distances[0] = Math.max(myDistances[0], distances[0]);
			distances[1] = Math.max(myDistances[1], distances[1]);
		}
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			selectionSymbol = new MultiLayerMarkerSymbol();
			selectionSymbol.setDescription(getDescription());
			for (int i = 0; layers != null && i < layers.length; i++) {
				selectionSymbol.addLayer(layers[i].getSymbolForSelection());
			}
		}else {
            for (int i = 0; i < selectionSymbol.getLayerCount(); i++) {
                selectionSymbol.setLayer(i, selectionSymbol.getLayer(i).getSymbolForSelection());
            }
        }
		return selectionSymbol;

	}

	public boolean isSuitableFor(Geometry geom) {
		return geom.getType() == Geometry.TYPES.POINT;
	}


	public String getClassName() {
		return getClass().getName();
	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].print(g, at, geom, properties);
		}

	}

	public void setLayer(int index, ISymbol layer) throws IndexOutOfBoundsException {
		layers[index] = (IMarkerSymbol) layer;
	}

	public void swapLayers(int index1, int index2) {
		ISymbol aux1 = getLayer(index1), aux2 = getLayer(index2);
		layers[index2] = (IMarkerSymbol) aux1;
		layers[index1] = (IMarkerSymbol) aux2;
	}

	public ISymbol getLayer(int layerIndex) {
//		try{
			return layers[layerIndex];
//		} catch (Exception e) {
//			return null;
//		}
	}

	public int getLayerCount() {
		return layers.length;
	}

	public void addLayer(ISymbol newLayer) {
		addLayer(newLayer, layers.length);
	}

	public void addLayer(ISymbol newLayer, int layerIndex) throws IndexOutOfBoundsException {
		if (newLayer == null ) {
			/*|| newLayer instanceof ILabelStyle)*/ return; // null or symbols that are styles are not allowed
		}

		IMarkerSymbol newMarker = (IMarkerSymbol) newLayer;
		if (getLayerCount() == 0) {
			// apply the new layer properties to this multilayer

			setReferenceSystem(newMarker.getReferenceSystem());
			markerSize = newMarker.getSize();
			//setSize(newMarker.getSize());
			setUnit(newMarker.getUnit());
		} else {
			if (newMarker.getSize() > getSize()) {
				//setSize(newMarker.getSize());
				markerSize = newMarker.getSize();
			}
			newMarker.setReferenceSystem(getReferenceSystem());
			newMarker.setUnit(getUnit());
		}
		selectionSymbol = null; /* forces the selection symbol to be re-created
		 						 * next time it is required
		 						 */
		if (layerIndex < 0 || layers.length < layerIndex) {
			throw new IndexOutOfBoundsException(layerIndex+" < 0 or "+layerIndex+" > "+layers.length);
		}
		List<ISymbol> newLayers = new ArrayList<ISymbol>();
		for (int i = 0; i < layers.length; i++) {
			newLayers.add(layers[i]);
		}
		try {
			newLayers.add(layerIndex, newLayer);
			layers = (IMarkerSymbol[]) newLayers.toArray(new IMarkerSymbol[0]);
		} catch (ArrayStoreException asEx) {
			throw new ClassCastException(newLayer.getClass().getName()+" is not an IMarkerSymbol");
		}
	}

	public boolean removeLayer(ISymbol layer) {

		int capacity = 0;
		capacity = layers.length;
		List<ISymbol> lst = new ArrayList<ISymbol>(capacity);
		for (int i = 0; i < capacity; i++) {
			lst.add(layers[i]);
		}
		boolean contains = lst.remove(layer);
		layers = (IMarkerSymbol[])lst.toArray(new IMarkerSymbol[0]);
		return contains;
	}

	public void setUnit(int unit) {
		super.setUnit(unit);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setUnit(unit);
		}
	}

	public void setReferenceSystem(int system) {
		super.setReferenceSystem(system);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setReferenceSystem(system);
		}
	}

	public void setAlpha(int alpha) {
		// first, get the biggest alpha in the layers and the index if such layer
		int maxAlpha = Integer.MIN_VALUE;
		int maxAlphaLayerIndex = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			if (layers[i].getColor().getAlpha() > maxAlpha) {
				maxAlpha = layers[i].getColor().getAlpha();
				maxAlphaLayerIndex = i;
			}
		}

		// now, max alpha takes the value of the desired alpha and the rest
		// will take a scaled (to biggest alpha) alpha value
		for (int i = 0; layers != null && i < layers.length; i++) {
			int r = layers[i].getColor().getRed();
			int g = layers[i].getColor().getGreen();
			int b = layers[i].getColor().getBlue();

			if (i!=maxAlphaLayerIndex) {
				double scaledAlpha = (double) layers[i].getColor().getAlpha()/maxAlpha;
				int myAlpha = (int) (alpha*scaledAlpha);
				if (myAlpha == 0) {
					myAlpha = 1;
				}
				layers[i].setColor(new Color(r, g, b, myAlpha));
			} else {
				int myAlpha = alpha;
				if (myAlpha == 0) {
					myAlpha = 1;
				}
				layers[i].setColor(new Color(r, g, b, myAlpha));
			}
		}

	}

	public Object clone() throws CloneNotSupportedException {
		MultiLayerMarkerSymbol copy = (MultiLayerMarkerSymbol) super.clone();

		// Clone layers
		if (layers != null) {
			IMarkerSymbol[] layersCopy = new IMarkerSymbol[layers.length];
			for (int i = 0; i < layers.length; i++) {
				layersCopy[i] = (IMarkerSymbol) layers[i].clone();
			}
			copy.layers = layersCopy;
		}

		// Clone selection
		if (selectionSymbol != null) {
			copy.selectionSymbol = (MultiLayerMarkerSymbol) selectionSymbol
					.clone();
		}

		return copy;
	}

	@SuppressWarnings("unchecked")
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);
		// Set own properties
		List layers = state.getList(FIELD_LAYERS);
		if (layers != null) {
			for (int i = 0; i < layers.size(); i++) {
				addLayer((ISymbol) layers.get(i));
			}
		}
		setRotation(state.getDouble(FIELD_ROTATION));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);
		// Save own properties
		if (layers != null && layers.length > 0) {
			state.set(FIELD_LAYERS, Arrays.asList(layers));
		}
		state.set(FIELD_ROTATION, getRotation());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(MULTILAYER_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						MultiLayerMarkerSymbol.class,
						MULTILAYER_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						MULTILAYER_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the MarkerSymbol base definition
				definition.extend(manager.getDefinition(MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Layers
				definition.addDynFieldList(FIELD_LAYERS).setClassOfItems(IMarkerSymbol.class);
				// Rotation
				definition.addDynFieldDouble(FIELD_ROTATION).setMandatory(true);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

	        shapeTypes =
	            new int[] { Geometry.TYPES.POINT, Geometry.TYPES.MULTIPOINT };
	        manager.registerMultiLayerSymbol(IMarkerSymbol.SYMBOL_NAME,
	            shapeTypes,
	            MultiLayerMarkerSymbol.class);

			return Boolean.TRUE;
		}

	}

}