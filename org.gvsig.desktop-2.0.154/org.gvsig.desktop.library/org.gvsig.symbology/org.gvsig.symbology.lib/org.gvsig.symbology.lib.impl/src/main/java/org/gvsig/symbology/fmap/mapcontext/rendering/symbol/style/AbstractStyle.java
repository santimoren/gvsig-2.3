/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Implements the IStyle interface in order to complete the methods ot the
 * IStyle interface that allow the users to add the description of an object or
 * obtain it.
 * 
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */

public abstract class AbstractStyle implements IStyle {

	public static final String STYLE_PERSISTENCE_DEFINITION_NAME = "Style";

	private static final String FIELD_DESCRIPTION = "description";

	private String desc;

	public final void setDescription(String desc) {
		this.desc = desc;
	}

	public final String getDescription() {
		return desc;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setDescription(state.getString(FIELD_DESCRIPTION));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DESCRIPTION, getDescription());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractStyle.class,
						STYLE_PERSISTENCE_DEFINITION_NAME,
						STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				// Description
				definition.addDynFieldString(FIELD_DESCRIPTION);
			}
			return Boolean.TRUE;
		}
		
	}
}