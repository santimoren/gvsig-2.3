/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.AbstractSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Abstract class that any MARKER SYMBOL should extend.
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractMarkerSymbol extends AbstractSymbol implements IMarkerSymbol {
    
    public static final String MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME = "MarkerSymbol";    
    
    private static final String FIELD_COLOR = "color";

    private static final String FIELD_ROTATION = "rotation";

    private static final String FIELD_OFFSET = "offset";

    private static final String FIELD_SIZE = "size";
    
    private static final String FIELD_MASK = "mask";
    
    private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private Color color;
	private double rotation;
	private Point2D offset = new Point2D.Double();
	private double size = 4d;
	private IMask mask;

	public AbstractMarkerSymbol() {
		super();
		color =
				MapContextLocator.getSymbolManager()
						.getSymbolPreferences()
						.getDefaultSymbolFillColor();
	}

	public final int getSymbolType() {
		return Geometry.TYPES.POINT;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double r) {
		this.rotation = r;
	}

	public Point2D getOffset() {
		if (offset == null) {
			offset = new Point();
		}
		return offset;
	}

	public void setOffset(Point2D offset) {
		this.offset = offset;
	}

	public boolean isSuitableFor(Geometry geom) {
		return geom.getType() == Geometry.TYPES.POINT;
	}

	public int getOnePointRgb() {
		return color.getRGB();
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

    @Override
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setAlpha(int outlineAlpha) {
		Color color = getColor();
		setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(),
				outlineAlpha));
	}

	public void getPixExtentPlus(Geometry geom, float[] distances, ViewPort viewPort, int dpi) {
		float cs = (float) getCartographicSize(viewPort, dpi, geom);
		distances[0] = cs;
		distances[1] = cs;
	}


	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		double originalSize = getSize();
		double size=originalSize;
		// scale it to size
		int pq = properties.getPrintQuality();
		if (pq == PrintAttributes.PRINT_QUALITY_NORMAL){
			size *= (double) 300/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_HIGH){
			size *= (double) 600/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_DRAFT){
			// size *= 72/72; // (which is the same than doing nothing)
		}
//		setSize(size);
		draw(g,at,geom, null, null);
//		setSize(originalSize);
	}

	public final IMask getMask() {
		return mask;
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		try {
			if (properties==null)
				draw(g, scaleInstance, geomManager.createPoint(r.getCenterX(), r.getCenterY(), SUBTYPES.GEOM2D), null, null);
			else{
				double originalSize = getSize();
				double size=originalSize;
				int pq = properties.getPrintQuality();
				if (pq == PrintAttributes.PRINT_QUALITY_NORMAL){
					size *= (double) 300/72;
				}else if (pq == PrintAttributes.PRINT_QUALITY_HIGH){
					size *= (double) 600/72;
				}else if (pq == PrintAttributes.PRINT_QUALITY_DRAFT){
					// d *= 72/72; // (which is the same than doing nothing)
				}
				setSize(size);
				print(g, scaleInstance, geomManager.createPoint(r.getCenterX(), r.getCenterY(), SUBTYPES.GEOM2D), properties);
				setSize(originalSize);
			}



		} catch (CreateGeometryException e) {
			throw new SymbolDrawingException(TYPES.POINT);
		}
	}

	public final void setMask(IMask mask) {
		this.mask = mask;
	}



	public void setCartographicSize(double cartographicSize, Geometry geom) {
		setSize(cartographicSize);
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double oldSize = getSize();
		setCartographicSize(getCartographicSize(
								viewPort,
								dpi,
								geom),
							geom);
		return oldSize;
	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return CartographicSupportToolkit.
					getCartographicLength(this,
										  getSize(),
										  viewPort,
										  dpi);
	}
	
	public Object clone() throws CloneNotSupportedException {
		AbstractMarkerSymbol copy  = (AbstractMarkerSymbol) super.clone();
		
		// Clone the offset
		if (offset != null) {
			copy.offset = (Point2D) offset.clone();
		}
		
		// clone the mask
		if (mask != null) {
			copy.mask = (IMask) mask.clone();
		}
		return copy;
	}
	
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent symbol properties
		super.loadFromState(state);
		// Set own properties
		setColor((Color) state.get(FIELD_COLOR));
		setMask((IMask) state.get(FIELD_MASK));
		setOffset((Point2D) state.get(FIELD_OFFSET));
		setRotation(state.getDouble(FIELD_ROTATION));
		setSize(state.getDouble(FIELD_SIZE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent symbol properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_COLOR, getColor());
		state.set(FIELD_MASK, getMask());
		state.set(FIELD_OFFSET, getOffset());
		state.set(FIELD_ROTATION, getRotation());
		state.set(FIELD_SIZE, getSize());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractMarkerSymbol.class,
						MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				// Extend the Symbol base definition
				definition.extend(manager.getDefinition(SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Color
				definition.addDynFieldObject(FIELD_COLOR).setMandatory(false).setClassOfValue(Color.class);
				// Mask
				definition.addDynFieldObject(FIELD_MASK).setMandatory(false).setClassOfValue(IMask.class);

				// Offset
				definition.addDynFieldObject(FIELD_OFFSET).setMandatory(false).setClassOfValue(Point2D.class);

				// Rotation
				definition.addDynFieldDouble(FIELD_ROTATION).setMandatory(true);

				// Size
				definition.addDynFieldDouble(FIELD_SIZE).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}

}
