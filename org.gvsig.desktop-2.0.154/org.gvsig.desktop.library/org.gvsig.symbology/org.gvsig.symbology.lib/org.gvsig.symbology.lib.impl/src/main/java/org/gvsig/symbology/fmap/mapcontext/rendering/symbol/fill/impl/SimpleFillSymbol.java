/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.ISimpleFillSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Basic fill symbol. It will allow to paint a shape with its filling color (and transparency) and the outline.
 * @author 2005-2008  jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SimpleFillSymbol extends AbstractFillSymbol implements ISimpleFillSymbol {


	private static final String SIMPLE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME = "SimpleFillSymbol";

	private static final Logger LOG = LoggerFactory.getLogger(SimpleFillSymbol.class);
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();

    private static final String FIELD_SYMBOL_FOR_SELECTION = "symbolForSelection";

	private SimpleFillSymbol symbolForSelection;

	public ISymbol getSymbolForSelection() {
		if (symbolForSelection == null) {
			SimpleFillSymbol selectionSymbol = (SimpleFillSymbol) cloneForSelection();
			if (selectionSymbol != null) {
				selectionSymbol.setHasFill(true);
				setSymbolForSelection(selectionSymbol);
			}
		} else {
		    symbolForSelection.setColor(MapContext.getSelectionColor());
		}
		return symbolForSelection;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		Color c = getFillColor();

		if (c!=null && hasFill()) {
			g.setColor(c);
			g.fill(geom.getShape(affineTransform));
		}
		if (getOutline() != null && hasOutline()) {
			getOutline().draw(g, affineTransform, geom, feature, cancel);
		}
	}


	public int getSymbolType() {
		return Geometry.TYPES.SURFACE;
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		Rectangle rect = new Rectangle(r.x, r.y, r.width, r.height);
		rect.setFrame(((int) rect.getMinX())+1, ((int) rect.getMinY())+1, ((int) rect.getWidth())-2, ((int) rect.getHeight())-2);
		Geometry geom;
		try {
			geom = geomManager.createSurface(new GeneralPathX(rect.getPathIterator(null)), SUBTYPES.GEOM2D);
		} catch (CreateGeometryException e) {
			LOG.error("Creating a surface", e);
			throw new SymbolDrawingException(getSymbolType());
		}

		Color c = getFillColor();
		if (c != null && hasFill()) {
			g.setColor(c);
			g.fillRect(rect.x, rect.y, rect.width, rect.height);
		}

		if (getOutline() != null && hasOutline()) {
			if (properties==null)
				getOutline().draw(g, scaleInstance, geom, null, null);
			else
				print(g, new AffineTransform(), geom, properties);
		}
	}

	public String getClassName() {
		return getClass().getName();
	}


	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		Color c = getFillColor();
		if (c!=null && hasFill()) {
			g.setColor(c);
			g.fill(geom.getShape(at));
		}
		if (getOutline() != null && hasOutline()) {
			getOutline().print(g, at, geom, properties);
		}
	}


	public Object clone() throws CloneNotSupportedException {
		SimpleFillSymbol copy = (SimpleFillSymbol) super.clone();

		// Clone selection
		if (symbolForSelection != null) {
			copy.symbolForSelection = (SimpleFillSymbol) symbolForSelection
					.clone();
		}

		return copy;
	}

	private void setSymbolForSelection(SimpleFillSymbol symbolForSelection) {
		this.symbolForSelection = symbolForSelection;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);
		 setSymbolForSelection((SimpleFillSymbol)state.get(FIELD_SYMBOL_FOR_SELECTION));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Don't use the getSymbolForSelection method, as it will create it
		// if it does not exist, and persistence will enter an infinite loop
		 state.set(FIELD_SYMBOL_FOR_SELECTION, symbolForSelection);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleFillSymbol.class,
						SIMPLE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);

				// Extend the FillSymbol base definition
				definition.extend(manager.getDefinition(FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Selection Symbol
				definition.addDynFieldObject(FIELD_SYMBOL_FOR_SELECTION).setClassOfValue(SimpleFillSymbol.class).setMandatory(false);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
	        int[] shapeTypes;
	        SymbolManager manager = MapContextLocator.getSymbolManager();

	        shapeTypes =
	            new int[] { Geometry.TYPES.SURFACE, Geometry.TYPES.CIRCLE,
	                Geometry.TYPES.ELLIPSE, Geometry.TYPES.MULTISURFACE,
	                Geometry.TYPES.ELLIPTICARC, Geometry.TYPES.FILLEDSPLINE,
                    Geometry.TYPES.MULTIPOLYGON
                    };
	        manager.registerSymbol(IFillSymbol.SYMBOL_NAME,
	            shapeTypes,
	            SimpleFillSymbol.class);

			return Boolean.TRUE;
		}

	}

}
