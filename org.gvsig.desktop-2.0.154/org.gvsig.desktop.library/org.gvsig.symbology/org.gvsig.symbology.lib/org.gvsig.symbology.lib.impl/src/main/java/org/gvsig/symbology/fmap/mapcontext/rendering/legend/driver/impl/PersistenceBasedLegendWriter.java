/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.driver.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.gvsig.fmap.mapcontext.exceptions.WriteLegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendWriter;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;


/**
 * Legend writer that uses the standard persistence
 */
public class PersistenceBasedLegendWriter implements ILegendWriter {

    public static final String
    LONG_LEGEND_PERSISTENCE_FORMAT = "application/zip; subtype=gvsleg";
    
    public void write(ILegend legend, File outFile, String format)
        throws WriteLegendException, IOException {
        
        // Removing dot
        String legext = SymbolManager.LEGEND_FILE_EXTENSION.substring(1).toLowerCase();
        String fmt = format.toLowerCase();
        
        if (fmt.compareTo(legext) != 0 &&
            fmt.compareTo(LONG_LEGEND_PERSISTENCE_FORMAT) != 0) {
            throw new WriteLegendException(
                legend, new Exception("Unexpected format requested: " + format));
        }
        
        FileOutputStream fos = new FileOutputStream(outFile);
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        manager.putObject(fos, legend);
        fos.close();
    }

}
