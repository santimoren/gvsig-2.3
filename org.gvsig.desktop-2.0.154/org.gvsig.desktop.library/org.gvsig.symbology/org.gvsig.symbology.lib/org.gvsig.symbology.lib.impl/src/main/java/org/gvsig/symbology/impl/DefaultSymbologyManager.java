/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {}  {{Task}}
*/
package org.gvsig.symbology.impl;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.gvsig.fmap.mapcontext.rendering.legend.IInterval;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClassFactory;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.FInterval;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.AttrInTableLabelingStrategy;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.DefaultLabelingMethod;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.IAttrInTableLabelingStrategy;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelClass;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.ZoomConstraintsImpl;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.IMultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IMarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IPictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.ISimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.MarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.PictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.SimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.MultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.IPictureLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ISimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.PictureLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl.SimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.ISimpleMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.PictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.SimpleMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.BackgroundFileStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.DefaultMask;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ISimpleLineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleLabelStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleLineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.ISimpleTextSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl.SimpleTextSymbol;
import org.gvsig.tools.util.Callable;

/**
 * Default {@link SymbologyManager} implementation.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class DefaultSymbologyManager implements SymbologyManager {

        private Map labelClassFactories = new HashMap();
    private ILabelClassFactory defaultLabelClassFactory;
        
	public IInterval createInterval(double min, double max) {
		return new FInterval(min, max);
	}

	public static class RegisterSymbologyManager implements Callable {

		public Object call() throws Exception {

			SymbologyLocator.registerSymbologyManager(DefaultSymbologyManager.class);
	        
			return Boolean.TRUE;
		}
		
	}

	public IArrowDecoratorStyle createArrowDecoratorStyle() {
		return new ArrowDecoratorStyle();
	}
	
//	public IMarkerSymbol createMarkerSymbol(){
//		return new SimpleMarkerSymbol();
//	}

	public ISimpleMarkerSymbol createSimpleMarkerSymbol() {
		return new SimpleMarkerSymbol();
	}

	public IMask createMask() {
		return new DefaultMask();
	}

	public IMultiShapeSymbol createMultiShapeSymbol() {
		return new MultiShapeSymbol();
	}

	public ISimpleFillSymbol createSimpleFillSymbol() {
		return new SimpleFillSymbol();
	}

	public ISimpleLineSymbol createSimpleLineSymbol() {
		return new SimpleLineSymbol();
	}

	public ISimpleLineStyle createSimpleLineStyle() {
		return new SimpleLineStyle();
	}
	
	public IBackgroundFileStyle createBackgroundFileStyle(URL imgURL)
			throws IOException {
		return BackgroundFileStyle.createStyleByURL(imgURL);
	}

	public ISimpleTextSymbol createSimpleTextSymbol() {
		return new SimpleTextSymbol();
	}

	public IMarkerFillPropertiesStyle createSimpleMarkerFillPropertiesStyle() {
		return new SimpleMarkerFillPropertiesStyle();
	}

	public IPictureFillSymbol createPictureFillSymbol(URL imageURL,
			URL selImageURL) throws IOException {
		return new PictureFillSymbol(imageURL, selImageURL);
	}

	public IPictureLineSymbol createPictureLineSymbol(URL imageURL,
			URL selImageURL) throws IOException {
		return new PictureLineSymbol(imageURL, selImageURL);
	}

	public IPictureMarkerSymbol createPictureMarkerSymbol(URL imageURL,
			URL selImageURL) throws IOException {
		return new PictureMarkerSymbol(imageURL, selImageURL);

	}

	public IMarkerFillSymbol createMarkerFillSymbol() {
		return new MarkerFillSymbol();
	}

	public IAttrInTableLabelingStrategy createAttrInTableLabelingStrategy() {
		return new AttrInTableLabelingStrategy();
	}

	public ILabelingStrategy createDefaultLabelingStrategy() {
		return new AttrInTableLabelingStrategy();
	}
	
	public ILabelingMethod createDefaultLabelingMethod() {
		return new DefaultLabelingMethod();
	}
	
	public IZoomConstraints createDefaultZoomConstraints() {
		return new ZoomConstraintsImpl();
	}

	public ILabelStyle createDefaultLabelStyle() {
		return new SimpleLabelStyle();
	}

        public void registerLabelClass(ILabelClassFactory factory) {
            this.labelClassFactories.put(factory.getID().toLowerCase(), factory);
            if( this.defaultLabelClassFactory==null ) {
                this.defaultLabelClassFactory = factory;
            }
        }
        
        public Collection<ILabelClassFactory> getLabelClassFactories() {
            return Collections.unmodifiableCollection(this.labelClassFactories.values());
        }
        
        public ILabelClassFactory getLabelClassFactory(String id) {
            if( id==null ) {
                return null;
            }
            return (ILabelClassFactory) this.labelClassFactories.get(id.toLowerCase());
        }
        
	public ILabelClass createDefaultLabel() {
            ILabelClassFactory f = this.getDefaultLabelFactory();
            if( f==null ) {
                return null;
            }
            return f.create();
	}
	
        public ILabelClassFactory getDefaultLabelFactory() {
            return this.defaultLabelClassFactory;
        }
        
        public void setDefaultLabelFactory(String id) {
            ILabelClassFactory f = this.getLabelClassFactory(id);
            if( f==null ) {
                throw new IllegalArgumentException("factory id is null");
            }
            this.setDefaultLabelFactory(f);
        }
        
        public void setDefaultLabelFactory(ILabelClassFactory factory) {
            if( !this.labelClassFactories.containsValue(factory) ) {
                this.labelClassFactories.put(factory.getID().toLowerCase(), factory);
            }
            this.defaultLabelClassFactory = factory;
        }
        
        
}