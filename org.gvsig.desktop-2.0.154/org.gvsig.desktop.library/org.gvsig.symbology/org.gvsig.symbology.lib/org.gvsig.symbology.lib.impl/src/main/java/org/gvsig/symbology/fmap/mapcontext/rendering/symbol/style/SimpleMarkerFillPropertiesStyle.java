/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.MarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

public class SimpleMarkerFillPropertiesStyle extends AbstractStyle implements IMarkerFillPropertiesStyle {
	public static final String SIMPLE_MARKER_FILL_STYLE_PERSISTENCE_DEFINITION_NAME = "SimpleMarkerFillPropertiesStyle";

	private static final String FIELD_SAMPLE_SYMBOL = "sampleSymbol";
	private static final String FIELD_ROTATION = "rotation";
	private static final String FIELD_XOFFSET = "xOffset";
	private static final String FIELD_YOFFSET = "yOffset";
	private static final String FIELD_XSEPARATION = "xSeparation";
	private static final String FIELD_YSEPARATION = "ySeparation";
	private static final String FIELD_FILL_STYLE = "fillStyle";
	
	private IMarkerSymbol sampleSymbol = SymbologyLocator.getSymbologyManager().createSimpleMarkerSymbol();
	private double rotation = 0;
	private double xOffset = 0;
	private double yOffset = 0;
	private double xSeparation = 20;
	private double ySeparation = 20;
	private int fillStyle = MarkerFillSymbol.DefaultFillStyle;

	public void drawInsideRectangle(Graphics2D g, Rectangle r) {
		int s = (int) sampleSymbol.getSize();
		Rectangle rProv = new Rectangle();
		rProv.setFrame(0, 0, s, s);
		Paint resulPatternFill = null;
		BufferedImage bi = null;
		bi= new BufferedImage(s, s, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gAux = bi.createGraphics();
		try {
			sampleSymbol.drawInsideRectangle(gAux, gAux.getTransform(), rProv, null);
		} catch (SymbolDrawingException e) {
			if (e.getType() == SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS) {
				try {
					IWarningSymbol warning =
							(IWarningSymbol) MapContextLocator.getSymbolManager()
									.getWarningSymbol(
											SymbolDrawingException.STR_UNSUPPORTED_SET_OF_SETTINGS,
											"",
											SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
					warning.drawInsideRectangle(gAux, gAux.getTransform(), rProv, null);
				} catch (SymbolDrawingException e1) {
					// IMPOSSIBLE TO REACH THIS
				}
			}

		}
		resulPatternFill = new TexturePaint(bi,rProv);
		g.setColor(null);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			RenderingHints.VALUE_ANTIALIAS_ON);
		g.setPaint(resulPatternFill);
		g.fill(r);

	}

	public boolean isSuitableFor(ISymbol symbol) {
		return (symbol instanceof IFillSymbol);
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void drawOutline(Graphics2D g, Rectangle r) {
		drawInsideRectangle(g, r);
	}

	/**
	 * <p>
	 * Define an utility symbol to show up a thumbnail
	 * by default, this symbol is a SimpleMarkerSymbol.
	 * Thus, the drawInsideRectangle will always work. But
	 * it can be changed with setSampleSymbol(IMakerSymbol).<br>
	 * </p>
	 * <p>
	 * If <b>marker</b> is null, it does nothing
	 * </p>
	 */
	public void setSampleSymbol(IMarkerSymbol marker) {
		if (marker != null)
			this.sampleSymbol = marker;
	}


	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public double getXOffset() {
		return xOffset;
	}

	public void setXOffset(double offset) {
		xOffset = offset;
	}

	public double getXSeparation() {
		return xSeparation;
	}

	public void setXSeparation(double separation) {
		xSeparation = separation;
	}

	public double getYOffset() {
		return yOffset;
	}

	public void setYOffset(double offset) {
		yOffset = offset;
	}

	public double getYSeparation() {
		return ySeparation;
	}

	public void setYSeparation(double separation) {
		ySeparation = separation;
	}

	public void setFillStyle(int fillStyle) {
		this.fillStyle = fillStyle;
	}

	public int getFillStyle() {
		return fillStyle;
	}

	public Object clone() throws CloneNotSupportedException {
		SimpleMarkerFillPropertiesStyle copy = (SimpleMarkerFillPropertiesStyle) super.clone();
		if (sampleSymbol != null) {
			copy.sampleSymbol = (IMarkerSymbol) sampleSymbol.clone();
		}
		return copy;
	}

	public void loadFromState(PersistentState state)
	throws PersistenceException {
		// Set parent style properties
		super.loadFromState(state);

		// Set own properties
		setRotation(state.getDouble(FIELD_ROTATION));
		setXOffset(state.getDouble(FIELD_XOFFSET));
		setYOffset(state.getDouble(FIELD_YOFFSET));
		setXSeparation(state.getDouble(FIELD_XSEPARATION));
		setYSeparation(state.getDouble(FIELD_YSEPARATION));
		setFillStyle(state.getInt(FIELD_FILL_STYLE));
		
//		please, avoid initialize "sampleSymbol" field. It
//		is already controlled by this style's owner.

	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_ROTATION, getRotation());
		state.set(FIELD_XOFFSET, getXOffset());
		state.set(FIELD_YOFFSET, getYOffset());
		state.set(FIELD_XSEPARATION, getXSeparation());
		state.set(FIELD_YSEPARATION, getYSeparation());
		state.set(FIELD_FILL_STYLE, getFillStyle());

		/* please, avoid persist "sampleSymbol" field.
		 it is always initialized by this style's owner
		 when needs it. */
//		state.set(FIELD_SAMPLE_SYMBOL, this.sampleSymbol);
		
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_MARKER_FILL_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleMarkerFillPropertiesStyle.class,
						SIMPLE_MARKER_FILL_STYLE_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_MARKER_FILL_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				// Extend the Style base definition
				definition.extend(manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME));

				// Sample Symbol
				definition.addDynFieldObject(FIELD_SAMPLE_SYMBOL)
				.setClassOfValue(IMarkerSymbol.class);

				// Rotation
				definition.addDynFieldDouble(FIELD_ROTATION).setMandatory(true);

				// offset
				definition.addDynFieldDouble(FIELD_XOFFSET).setMandatory(true);
				definition.addDynFieldDouble(FIELD_YOFFSET).setMandatory(true);

				// Separation
				definition.addDynFieldDouble(FIELD_XSEPARATION).setMandatory(true);
				definition.addDynFieldDouble(FIELD_YSEPARATION).setMandatory(true);

				// Fill Style
				definition.addDynFieldInt(FIELD_FILL_STYLE).setMandatory(true);
			}
			return Boolean.TRUE;
		}

	}

}
