/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.net.URL;

import org.apache.batik.ext.awt.geom.PathLength;
import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.IPictureLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.BackgroundFileStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * PictureLineSymbol allows to use any symbol defined as an image (by an image file)
 * supported  by gvSIG.This symbol will be used as an initial object.The line will be
 * painted as a succession of puntual symbols through the path defined by it(the line).
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class PictureLineSymbol extends AbstractLineSymbol implements IPictureLineSymbol  {

	private static final Logger logger = LoggerFactory.getLogger(PictureLineSymbol.class);

    public static final String PICTURE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME =
        "PictureLineSymbol";
    private static final String SELECTED = "selected";
    private static final String SELECTION_SYMBOL = "selectionSym";
    private static final String BACKGROUND_IMAGE = "bgImage";
    private static final String BACKGROUND_SELECTION_IMAGE = "bgSelImage";
    private static final String WIDTH = "width";

	transient private PictureLineSymbol selectionSym;
	private double width;
	private boolean selected;
	private double xScale = 1, csXScale = xScale;
	private double yScale = 1, csYScale = yScale;

	private BackgroundFileStyle bgImage;
	private BackgroundFileStyle bgSelImage;
	private PrintAttributes properties;

	/**
	 * Constructor method
	 *
	 */
	public PictureLineSymbol() {
		super();
	}
	/**
	 * Constructor method
	 * @param imageURL, URL of the normal image
	 * @param selImageURL, URL of the image when it is selected in the map
	 * @throws IOException
	 */

	public PictureLineSymbol(URL imageURL, URL selImageURL) throws IOException {
		setImage(imageURL);
		if (selImageURL!=null)
			setSelImage(selImageURL);
		else setSelImage(imageURL);
	}
	/**
	 * Sets the URL for the image to be used as a picture line symbol
	 * @param imageFile, File
	 * @throws IOException
	 */
	public void setImage(URL imageUrl) throws IOException{

		bgImage= BackgroundFileStyle.createStyleByURL(imageUrl);
	}
	/**
	 * Sets the URL for the image to be used as a picture line symbol (when it is selected in the map)
	 * @param imageFile, File
	 * @throws IOException
	 */
	public void setSelImage(URL selImageUrl) throws IOException{

		bgSelImage= BackgroundFileStyle.createStyleByURL(selImageUrl);
	}


	public void setLineWidth(double width) {
		this.width = width;
		getLineStyle().setLineWidth((float) width);
	}

	public double getLineWidth() {
		return width;
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSym == null) {
			selectionSym = (PictureLineSymbol) cloneForSelection();
			selectionSym.selected=true;
			selectionSym.selectionSym = selectionSym; // avoid too much lazy creations
		}else{
		    selectionSym.setColor(MapContext.getSelectionColor());
		}
		return selectionSym;

	}

	public void draw(Graphics2D g, AffineTransform affineTransform,	Geometry geom, Feature f, Cancellable cancel) {
		draw(g, affineTransform, geom, cancel);
	}

	private void draw(Graphics2D g, AffineTransform affineTransform, Geometry geom, Cancellable cancel) {

	    if (csXScale<=0 && csYScale<=0) {
	        return;
	    }

	    float csWidth = getLineStyle().getLineWidth();

		BasicStroke bs = new BasicStroke(
		    (float) csWidth,
		    BasicStroke.CAP_ROUND,
		    BasicStroke.CAP_ROUND);

		Shape geom_transf_clip = geom.getShape(affineTransform);
		g.setClip(bs.createStrokedShape(geom_transf_clip));

		BackgroundFileStyle bg = (!selected) ? bgImage : bgSelImage ;

		Rectangle bounds = bg.getBounds();
		final double imageWidth  = bounds.getWidth()  * csXScale;
		final double imageHeight = bounds.getHeight() * csYScale;

		if (imageWidth==0 || imageHeight==0) return;
		int height = (int) csWidth;

		PathLength pl = new PathLength(geom_transf_clip);
		PathIterator iterator = geom_transf_clip.getPathIterator(null, 0.8);
		double[] theData = new double[6];
		Point2D firstPoint = null, startPoint = null, endPoint = null;
		if (!iterator.isDone()) {
			if ( iterator.currentSegment(theData) != PathIterator.SEG_CLOSE) {
				firstPoint = new Point2D.Double(theData[0], theData[1]);
			}
		}
		float currentPathLength = 1;

		Rectangle rect = new Rectangle();

		while ((cancel==null || !cancel.isCanceled()) && !iterator.isDone()) {

			int theType = iterator.currentSegment(theData);
			switch (theType) {
			case PathIterator.SEG_MOVETO:
				startPoint = new Point2D.Double(theData[0], theData[1]);

				endPoint = null;
				iterator.next();

				continue;

			case PathIterator.SEG_LINETO:
			case PathIterator.SEG_QUADTO:
			case PathIterator.SEG_CUBICTO:
				endPoint = new Point2D.Double(theData[0], theData[1]);

				break;
			case PathIterator.SEG_CLOSE:
				endPoint = startPoint;
				startPoint = firstPoint;
				break;
			}

			double a = endPoint.getX() - startPoint.getX();
			double b = endPoint.getY() - startPoint.getY();
			double theta = pl.angleAtLength(currentPathLength);

			double x = startPoint.getX();
			double y = startPoint.getY();

			// Theorem of Pythagoras
			float segmentLength = (float) Math.sqrt(a*a + b*b);

			// compute how many times the image has to be drawn
			// to completely cover this segment's length
			int count = (int) Math.ceil(segmentLength/imageWidth);

			for (int i = 0; (cancel==null || !cancel.isCanceled()) && i < count; i++) {
				g.translate(x, y);
				g.rotate(theta);

				double xOffsetTranslation = imageWidth*i;
				g.translate(xOffsetTranslation, -csWidth);

				rect.setBounds(0, (int) Math.round(height*.5), (int) Math.ceil(imageWidth), height);
				try {
					bg.drawInsideRectangle(g, rect, false);
				} catch (SymbolDrawingException e) {
					logger.warn(Messages.getText("label_style_could_not_be_painted"), e);
				}
				g.translate(-xOffsetTranslation, csWidth);

				g.rotate(-theta);
				g.translate(-x, -y);
			}

			startPoint = endPoint;
			currentPathLength += segmentLength;
			iterator.next();
		}
		g.setClip(null);
	}

	/**
	 * Sets the yscale for the picture line symbol
	 * @param yScale
	 */
	public void setYScale(double yScale) {
		this.yScale = yScale;
		this.csYScale = yScale;
	}
	/**
	 * Sets the xscale for the picture line symbol
	 * @param xScale
	 */
	public void setXScale(double xScale) {
		this.xScale = xScale;
		this.csXScale = xScale;
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom,
			PrintAttributes properties) {
		this.properties=properties;
        draw(g, at, geom, null);
        this.properties=null;

	}

	/**
	 * Returns the URL of the image that is used as a picture line symbol (when it
	 * is selected in the map)
	 * @return selimagePath,URL
	 */
	public URL getSelectedSource(){
		return bgSelImage.getSource();
	}
	/**
	 * Returns the URL of the image that is used as a picture line symbol
	 * @return imagePath,URL
	 */
	public URL getSource() {
		return bgImage.getSource();
	}

	/**
	 * Returns the xscale for the picture line symbol
	 * @param xScale
	 */
	public double getXScale() {
		return xScale;
	}
	/**
	 * Returns the yscale for the picture line symbol
	 * @param yScale
	 */
	public double getYScale() {
		return yScale;
	}

	@Override
	public void setCartographicSize(double cartographicSize, Geometry geom) {
		getLineStyle().setLineWidth((float) cartographicSize);
		double scale = cartographicSize/width;
		csXScale = xScale * scale;
		csYScale = yScale * scale;
	}

	@Override
	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double s = super.toCartographicSize(viewPort, dpi, geom);
		setCartographicSize(CartographicSupportToolkit.
				getCartographicLength(this, width, viewPort, dpi), geom);
		return s;
	}


    public Object clone() throws CloneNotSupportedException {
        PictureLineSymbol copy = (PictureLineSymbol) super.clone();

        // clone selection
        if (selectionSym != null) {
        	//to avoid an infinite loop
        	if (this == selectionSym){
        		copy.selectionSym = copy;
        	} else {
        		copy.selectionSym = (PictureLineSymbol) selectionSym.clone();
        	}
        }

        // clone brackground image
        if (bgImage != null) {
            copy.bgImage = (BackgroundFileStyle) bgImage.clone();
        }

        // clone selection brackground image
        if (bgSelImage != null) {
            copy.bgSelImage = (BackgroundFileStyle) bgSelImage.clone();
        }

        // FIXME: clone properties

        return copy;
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        // Set parent style properties
        super.loadFromState(state);

        this.selected = (Boolean) state.get(SELECTED);
        this.selectionSym = (PictureLineSymbol) state.get(SELECTION_SYMBOL);
        this.bgImage = (BackgroundFileStyle) state.get(BACKGROUND_IMAGE);
        this.bgSelImage =
            (BackgroundFileStyle) state.get(BACKGROUND_SELECTION_IMAGE);
        this.setLineWidth(state.getDouble(WIDTH));
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent fill symbol properties
        super.saveToState(state);

        // Save own properties
        state.set(SELECTED, this.selected);
        state.set(SELECTION_SYMBOL, this.getSymbolForSelection());
        state.set(BACKGROUND_IMAGE, this.bgImage);
        state.set(BACKGROUND_SELECTION_IMAGE, this.bgSelImage);
        state.set(WIDTH, width);
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(PICTURE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition =
                    manager.addDefinition(PictureLineSymbol.class,
                    		PICTURE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
                    		PICTURE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME
                            + " Persistence definition",
                        null,
                        null);

                // Extend the Style base definition
                definition.extend(manager.getDefinition(LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldBoolean(SELECTED).setMandatory(false);
                definition.addDynFieldObject(SELECTION_SYMBOL)
                    .setClassOfValue(PictureLineSymbol.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_SELECTION_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);

                definition.addDynFieldObject(WIDTH)
                    .setClassOfValue(Double.class).setMandatory(true);
            }
            return Boolean.TRUE;
        }
    }

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			SymbolManager manager = MapContextLocator.getSymbolManager();

		        manager.registerSymbol(PICTURE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
		            PictureLineSymbol.class);

			return Boolean.TRUE;
		}
	}
}