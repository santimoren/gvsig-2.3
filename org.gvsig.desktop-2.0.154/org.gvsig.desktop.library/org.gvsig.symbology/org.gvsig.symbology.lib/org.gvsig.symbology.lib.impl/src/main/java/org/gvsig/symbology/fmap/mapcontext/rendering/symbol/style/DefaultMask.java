/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class DefaultMask extends AbstractStyle implements IMask {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultMask.class);

	public static final String DEFAULT_MASK_PERSISTENCE_DEFINITION_NAME = "DefaultMask";

	private static final String FIELD_SIZE = "size";
	private static final String FIELD_FILL_SYMBOL = "fillSymbol";

	private static final GeometryManager geomManager = GeometryLocator
			.getGeometryManager();
	private double size;
	private IFillSymbol fill;

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public IFillSymbol getFillSymbol() {
		return fill;

	}

	public void setFillSymbol(IFillSymbol fill) {
		this.fill = fill;
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r) {
		// TODO Implement it
		LOG.error("TODO: Not yet implemented");
		throw new Error("Not yet implemented!");

	}

	public Geometry getHaloShape(Shape shp) {
		BasicStroke stroke = new BasicStroke(
				(int) /* falta CartographicSupport */getSize());
		Shape myShp = stroke.createStrokedShape(shp);
		Geometry haloShape = null;
		;
		try {
			haloShape = geomManager.createSurface(new GeneralPathX(myShp
					.getBounds2D().getPathIterator(null)), SUBTYPES.GEOM2D);
		} catch (CreateGeometryException e) {
			LOG.error("Error creating a surface", e);
		}

		return haloShape;
	}

	public boolean isSuitableFor(ISymbol symbol) {
		// TODO Implement it
		LOG.error("TODO: Not yet implemented");
		throw new Error("Not yet implemented!");

	}

	public void drawOutline(Graphics2D g, Rectangle r) {
		// TODO Implement it
		LOG.error("TODO: Not yet implemented");
		throw new Error("Not yet implemented!");

	}

	public Object clone() throws CloneNotSupportedException {
		DefaultMask copy = (DefaultMask) super.clone();
		if (fill != null) {
			copy.fill = (IFillSymbol) fill.clone();
		}
		return copy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent style properties
		super.loadFromState(state);

		// Set own properties
		setFillSymbol((IFillSymbol) state.get(FIELD_FILL_SYMBOL));
		setSize(state.getDouble(FIELD_SIZE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		if (getFillSymbol() != null) {
			state.set(FIELD_FILL_SYMBOL, getFillSymbol());
		}
		state.set(FIELD_SIZE, getSize());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(DEFAULT_MASK_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						DefaultMask.class,
						DEFAULT_MASK_PERSISTENCE_DEFINITION_NAME,
						DEFAULT_MASK_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				
				// Extend the Style base definition
				definition.extend(manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME));

				// Fill symbol
				definition.addDynFieldObject(FIELD_FILL_SYMBOL)
					.setClassOfValue(IFillSymbol.class);
				// size
				definition.addDynFieldDouble(FIELD_SIZE).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}
}