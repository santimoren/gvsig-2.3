/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.CompatLocator;
import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.exception.ReprojectionRuntimeException;
import org.gvsig.fmap.geom.operation.DrawInts;
import org.gvsig.fmap.geom.operation.DrawOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsEnvelopeEvaluator;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.LegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.ZSort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.util.Callable;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * Base implementation for Vectorial data Legends.
 *
 * Provides a draw method implementation which loads the {@link Feature}s and
 * uses the {@link ISymbol} objects to draw the {@link Geometry} objects.
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractVectorialLegend extends AbstractLegend implements
IVectorLegend {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractVectorialLegend.class);

    private static final int DRAW_MAX_ATTEMPTS = 5;

	public static final String VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME = "VectorialLegend";

    private static final String FIELD_HAS_ZSORT = "hasZSort";
    private static final String FIELD_SHAPETYPE = "shapeType";
    private static final String FIELD_DEFAULT_SYMBOL = "defaultSymbol";

    private static final GeometryManager geomManager = GeometryLocator
    .getGeometryManager();

    protected ZSort zSort;

    public ZSort getZSort() {
        return zSort;
    }

    public void setZSort(ZSort zSort) {
        if (zSort == null) {
            removeLegendListener(this.zSort);
        }
        this.zSort = zSort;
        addLegendListener(zSort);
    }

    @SuppressWarnings("unchecked")
    public void draw(BufferedImage image, Graphics2D g, ViewPort viewPort,
        Cancellable cancel, double scale, Map queryParameters,
        ICoordTrans coordTrans, FeatureStore featureStore)
    throws LegendException {
        double dpi = viewPort.getDPI();
        draw(image, g, viewPort, cancel, scale, queryParameters, coordTrans,
            featureStore, null, dpi);
    }

    @SuppressWarnings("unchecked")
    public void draw(BufferedImage image, Graphics2D g, ViewPort viewPort,
        Cancellable cancel, double scale, Map queryParameters,
        ICoordTrans coordTrans, FeatureStore featureStore, FeatureQuery featureQuery)
    throws LegendException {
        double dpi = viewPort.getDPI();
        draw(image, g, viewPort, cancel, scale, queryParameters, coordTrans,
            featureStore, featureQuery, dpi);
    }

    @SuppressWarnings("unchecked")
    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, PrintAttributes properties)
    throws LegendException {
        print(g, viewPort, cancel, scale, queryParameters, coordTrans,
            featureStore, null, properties);
    }

    @SuppressWarnings("unchecked")
    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, FeatureQuery fquery, PrintAttributes properties)
    throws LegendException {
        double dpi = 72;

        int resolution = properties.getPrintQuality();

        if (resolution == PrintAttributes.PRINT_QUALITY_DRAFT
            || resolution == PrintAttributes.PRINT_QUALITY_NORMAL
            || resolution == PrintAttributes.PRINT_QUALITY_HIGH) {
            dpi = PrintAttributes.PRINT_QUALITY_DPI[resolution];
        }

        FeatureSet featureSet = null;
        DisposableIterator it = null;
        try {
            ZSort zSort = getZSort();

            // if layer has map levels it will use a ZSort
            boolean useZSort = zSort != null && zSort.isUsingZSort();

            int mapLevelCount = (useZSort) ? zSort.getLevelCount() : 1;
            for (int mapPass = 0; mapPass < mapLevelCount; mapPass++) {

                Envelope vp_env_in_store_crs = null;
                IProjection store_crs = null;
                if (coordTrans != null) {
                    // 'coordTrans' is from store crs to vp crs
                    ICoordTrans inv = coordTrans.getInverted();
                    Envelope aux = viewPort.getAdjustedEnvelope();
                    vp_env_in_store_crs = aux.convert(inv);
                    store_crs = coordTrans.getPOrig();
                } else {
                    vp_env_in_store_crs = viewPort.getAdjustedEnvelope();
                    store_crs = viewPort.getProjection();
                }

                FeatureQuery feat_query = fquery;
                Envelope store_env = featureStore.getEnvelope();
                boolean use_intersection_cond = false;
                if (store_env == null) {
                    // Store does not know its envelope, so we must:
                    use_intersection_cond = true;
                } else {
                    if (vp_env_in_store_crs.contains(store_env)) {
                        use_intersection_cond = false;
                    } else {
                        use_intersection_cond = true;
                    }
                }

                if (use_intersection_cond) {
                    FeatureType ft = featureStore.getDefaultFeatureType();
                    IntersectsEnvelopeEvaluator iee =
                        new IntersectsEnvelopeEvaluator(
                            vp_env_in_store_crs,
                            store_crs,
                            ft, ft.getDefaultGeometryAttributeName());
                    if (feat_query == null) {
                        feat_query = featureStore.createFeatureQuery();
                        String[] fns = getRequiredFeatureAttributeNames(featureStore);
                        feat_query.setAttributeNames(fns);
                    }
                    feat_query.addFilter(iee);
                }

                // 'feat_query' can still be NULL here, so we only filter
                // the featureStore if it's not null
                if (feat_query == null) {
                    featureSet = featureStore.getFeatureSet();
                } else {
                    featureSet = featureStore.getFeatureSet(feat_query);
                }
                it = featureSet.fastIterator();
                // Iteration over each feature
                while (!cancel.isCanceled() && it.hasNext()) {
                    Feature feat = (Feature) it.next();
                    Geometry geom = feat.getDefaultGeometry();
                    if (geom==null) {
                    	continue;
                    }
                    // Reprojection if needed
                    if (coordTrans != null) {
                        geom = geom.cloneGeometry();
                        geom.reProject(coordTrans);
                    }

                    // retrieve the symbol associated to such feature
                    ISymbol sym = getSymbolByFeature(feat);
                    if (sym == null) {
                        continue;
                    }
                    if (useZSort) {
                        int[] symLevels = zSort.getLevels(sym);
                        if (symLevels != null) {

                            // Check if this symbol is a multilayer
                            if (sym instanceof IMultiLayerSymbol) {
                                // if so, get the layer corresponding to the
                                // current level. If none, continue to next
                                // iteration
                                IMultiLayerSymbol mlSym = (IMultiLayerSymbol) sym;
                                for (int i = 0; i < mlSym.getLayerCount(); i++) {
                                    ISymbol mySym = mlSym.getLayer(i);
                                    if (symLevels[i] == mapPass) {
                                        sym = mySym;
                                        break;
                                    }
                                }

                            } else {
                                // else, just draw the symbol in its level
                                if (symLevels[0] != mapPass) {
                                    continue;
                                }
                            }
                        }
                    }

                    // Check if this symbol is sized with CartographicSupport
                    CartographicSupport csSym = null;
                    int symbolType = sym.getSymbolType();

                    if (symbolType == Geometry.TYPES.POINT
                        || symbolType == Geometry.TYPES.CURVE
                        || sym instanceof CartographicSupport) {

                        csSym = (CartographicSupport) sym;
                    }

                    DrawOperationContext doc = new DrawOperationContext();
                    doc.setGraphics(g);
                    doc.setViewPort(viewPort);
                    if (csSym == null) {
                        doc.setSymbol(sym);
                    } else {
                        doc.setDPI(dpi);
                        doc.setCancellable(cancel);
                        doc.setSymbol((ISymbol) csSym);
                    }
                    geom.invokeOperation(DrawInts.CODE, doc);
                }
            }
        } catch (ReadException e) {
            throw new LegendDrawingException(e);
        } catch (GeometryOperationNotSupportedException e) {
            throw new LegendDrawingException(e);
        } catch (GeometryOperationException e) {
            throw new LegendDrawingException(e);
        } catch (DataException e) {
            throw new LegendDrawingException(e);
        } catch (MapContextException e) {
            throw new LegendDrawingException(e);
        } finally {
            if (it != null) {
                it.dispose();
            }
            if (featureSet != null) {
                featureSet.dispose();
            }
        }
    }

    /**
     * Draws the features from the {@link FeatureStore}, filtered with the scale
     * and the query parameters, with the symbols of the legend.
     */
    @SuppressWarnings("unchecked")
	protected void draw(BufferedImage image, Graphics2D g, ViewPort viewPort,
			Cancellable cancel, double scale, Map queryParameters,
			ICoordTrans coordTrans, FeatureStore featureStore,
			FeatureQuery featureQuery, double dpi) throws LegendException {

		SimpleTaskStatus taskStatus = ToolsLocator.getTaskStatusManager()
				.createDefaultSimpleTaskStatus(featureStore.getName());
		taskStatus.add();
		try {
			// Avoid ConcurrentModificationException errors if
			// while drawing another thread edits the store data.
			synchronized (featureStore) {
				internalDraw(image, g, viewPort, cancel, scale,
						queryParameters, coordTrans, featureStore,
						featureQuery, dpi, taskStatus);
			}
		} finally {
			if (taskStatus != null) {
				taskStatus.terminate();
				taskStatus.remove();
				taskStatus = null;
			}
		}
	}

	protected void internalDraw(BufferedImage image, Graphics2D g,
			ViewPort viewPort, Cancellable cancel, double scale,
			Map queryParameters, ICoordTrans coordTrans,
			FeatureStore featureStore, FeatureQuery featureQuery, double dpi,
			SimpleTaskStatus taskStatus) throws LegendDrawingException {

		if (!getDefaultSymbol().isShapeVisible()) {
			return;
		}

		if (cancel.isCanceled()) {
			return;
		}

		IProjection dataProjection;
		Envelope dataEnvelope;
		Envelope reprojectedDataEnvelope;
		// Gets the view envelope
		Envelope viewPortEnvelope = viewPort.getAdjustedEnvelope();
        Envelope reprojectedViewPortEnvelope;
		try {
		    dataEnvelope = featureStore.getEnvelope();
			if (coordTrans == null) {
				dataProjection = featureStore.getDefaultFeatureType()
						.getDefaultSRS();

				// If the data does not provide a projection, use the
				// current view one
				if (dataProjection == null) {
					dataProjection = viewPort.getProjection();
				}

				reprojectedDataEnvelope = dataEnvelope;
                reprojectedViewPortEnvelope = viewPortEnvelope;
			} else {
				dataProjection = coordTrans.getPOrig();

				if ( dataEnvelope!=null && !dataEnvelope.isEmpty()) {
					reprojectedDataEnvelope = dataEnvelope.convert(coordTrans);
				} else {
					reprojectedDataEnvelope = dataEnvelope;
				}
                if ( viewPortEnvelope!=null && !viewPortEnvelope.isEmpty()) {
                    reprojectedViewPortEnvelope = viewPortEnvelope.convert(coordTrans.getInverted());
                } else {
                    reprojectedViewPortEnvelope = viewPortEnvelope;
                }
			}
		} catch (DataException e) {
			throw new LegendDrawingException(e);
		}


		// Gets the data envelope with the viewport SRS
//		Envelope myEnvelope = reprojectedDataEnvelope;

		// TODO: in some cases, the legend may need a different check to
		// decide if the data must be drawn or not
		// Checks if the viewport envelope intersects with the data envelope
		// This condition may seem redundant, but sometimes the transformations may fail and cause false negatives.
		if (!viewPortEnvelope.intersects(reprojectedDataEnvelope) && !(reprojectedViewPortEnvelope!=null && reprojectedViewPortEnvelope.intersects(dataEnvelope))) {
			// The data is not visible in the current viewport, do nothing.
			return;
		}

		// Check if all the data is contained into the viewport envelope
        // This condition may seem redundant, but sometimes the transformations may fail and cause false negatives.
		boolean containsAll = viewPortEnvelope.contains(reprojectedDataEnvelope) || (reprojectedViewPortEnvelope!=null && reprojectedViewPortEnvelope.contains(dataEnvelope));

		// Create the drawing notification to be reused on each iteration
		DefaultFeatureDrawnNotification drawnNotification = new DefaultFeatureDrawnNotification();

		if (cancel.isCanceled()) {
			return;
		}

		FeatureSet featureSet = null;
		try {
			taskStatus.message("Retrieve selection");
			FeatureSelection selection = featureStore.getFeatureSelection();

			if (featureQuery == null) {
				featureQuery = featureStore.createFeatureQuery();
			}

			completeQuery(featureStore, featureQuery, scale, queryParameters,
					coordTrans, dataProjection, viewPortEnvelope, containsAll);

			taskStatus.message("Retrieve data");
			featureSet = featureStore.getFeatureSet(featureQuery);

			if (cancel.isCanceled()) {
				return;
			}

			taskStatus.message("Drawing");
			drawFeatures(image, g, viewPort, cancel, coordTrans, dpi,
					drawnNotification, featureSet, selection);

        } catch (RuntimeException e) {
            /*
             * Probably a reprojection exception (for example,
             * trying to reproject Canada to EPSG:23030)
             */
            throw new LegendDrawingException(e);
		} catch (BaseException e) {
			throw new LegendDrawingException(e);
		} finally {
			if (featureSet != null) {
				featureSet.dispose();
			}
		}
	}

    /**
     * Complete a {@link FeatureQuery} to load the {@link Feature}s to draw.
     */
    @SuppressWarnings("unchecked")
    private FeatureQuery completeQuery(FeatureStore featureStore, FeatureQuery featureQuery, double scale,
        Map queryParameters, ICoordTrans coordTrans,
        IProjection dataProjection, Envelope viewPortEnvelope,
        boolean containsAll) throws DataException {

        featureQuery.setScale(scale);

        //Adds the attributes
        String[] fieldNames = getRequiredFeatureAttributeNames(featureStore);
        for (int i=0 ; i<fieldNames.length ;i++){
            featureQuery.addAttributeName(fieldNames[i]);
        }

        // TODO: Mobile has it's own IntersectsEnvelopeEvaluator
        if (!containsAll) {
            // Gets the viewport envelope with the data SRS
            Envelope viewPortEnvelopeInMyProj = viewPortEnvelope;
            // FIXME
            if (coordTrans != null) {
                viewPortEnvelopeInMyProj = viewPortEnvelope.convert(coordTrans
                        .getInverted());
            }

            if (dataProjection == null) {
                throw new IllegalArgumentException(
                "Error, the projection parameter value is null");
            }

            IntersectsEnvelopeEvaluator iee = new IntersectsEnvelopeEvaluator(
                viewPortEnvelopeInMyProj, dataProjection,
                featureStore.getDefaultFeatureType(), featureStore
                .getDefaultFeatureType()
                .getDefaultGeometryAttributeName());
            featureQuery.addFilter(iee);
        }
        if (queryParameters != null) {
            Iterator iterEntry = queryParameters.entrySet().iterator();
            Entry entry;
            while (iterEntry.hasNext()) {
                entry = (Entry) iterEntry.next();
                featureQuery.setQueryParameter((String) entry.getKey(),
                    entry.getValue());
            }
        }
        return featureQuery;
    }

    /**
     * Draws the features from the {@link FeatureSet}, with the symbols of the
     * legend.
     */
    private void drawFeatures(BufferedImage image, Graphics2D g,
        ViewPort viewPort, Cancellable cancel, ICoordTrans coordTrans,
        double dpi, DefaultFeatureDrawnNotification drawnNotification,
        FeatureSet featureSet, FeatureSelection selection)
    throws BaseException {
        if (isUseZSort()) {
            drawFeaturesMultiLayer(image, g, viewPort, cancel, coordTrans, dpi,
                drawnNotification, featureSet, selection);
        } else {
            drawFeaturesSingleLayer(image, g, viewPort, cancel, coordTrans,
                dpi, drawnNotification, featureSet, selection);
        }
    }

    /**
     * Draws the features from the {@link FeatureSet}, with the symbols of the
     * legend, using a single drawing layer.
     */
    private void drawFeaturesSingleLayer(final BufferedImage image,
        final Graphics2D g, final ViewPort viewPort,
        final Cancellable cancel, final ICoordTrans coordTrans,
        final double dpi,
        final DefaultFeatureDrawnNotification drawnNotification,
        FeatureSet featureSet, final FeatureSelection selection)
    throws BaseException {

        try {
            featureSet.accept(new Visitor() {
                public void visit(Object obj) throws VisitCanceledException,
                BaseException {
                    Feature feat = (Feature) obj;
                    drawFeatureSingleLayer(image, g, viewPort, cancel,
                        coordTrans, dpi, drawnNotification, feat, selection);
                }
            });

        } catch (ConcurrentDataModificationException e) {
            cancel.setCanceled(true);
            return;
        }
    }

    /**
     * Draws a Feature with the symbols of the legend, using a single drawing
     * layer.
     */
    private void drawFeatureSingleLayer(BufferedImage image, Graphics2D g,
        ViewPort viewPort, Cancellable cancel, ICoordTrans coordTrans,
        double dpi, DefaultFeatureDrawnNotification drawnNotification,
        Feature feat, FeatureSelection selection)
    throws MapContextException, CreateGeometryException {

        Geometry geom = feat.getDefaultGeometry();
        if (geom == null) {
            return;
        }

        if (geom.getType() == Geometry.TYPES.NULL) {
            return;
        }

        ISymbol sym = getSymbol(feat, selection);
        if (sym == null) {
            return;
        }

        if (coordTrans != null) {
            geom = geom.cloneGeometry();
            try {
                geom.reProject(coordTrans);
            } catch (ReprojectionRuntimeException re) {
                LOG.warn("Can't reproject geometry "+geom.toString(), re);
                return;

                /*
                 * Library was unable to reproject
                 * See reproject method in Point2D (geometry)
                 */
//                throw new CreateGeometryException(
//                    geom.getGeometryType().getType(),
//                    geom.getGeometryType().getSubType(),
//                    re);
            }
        }

        if (cancel.isCanceled()) {
            return;
        }

        drawGeometry(geom, image, feat, sym, viewPort, g, dpi, cancel);

        // Notify the drawing observers
        drawnNotification.setFeature(feat);
        drawnNotification.setDrawnGeometry(geom);
        notifyObservers(drawnNotification);
    }

    /**
     * Draws the features from the {@link FeatureSet}, with the symbols of the
     * legend, using a multiple drawing layer.
     */
    private void drawFeaturesMultiLayer(BufferedImage image, Graphics2D g,
        ViewPort viewPort, Cancellable cancel, ICoordTrans coordTrans,
        double dpi, DefaultFeatureDrawnNotification drawnNotification,
        FeatureSet featureSet, FeatureSelection selection)
    throws MapContextException, CreateGeometryException, DataException {

        // -- visual FX stuff
        long time = System.currentTimeMillis();

        boolean bSymbolLevelError = false;
        // render temporary map each screenRefreshRate milliseconds;
        int screenRefreshDelay = (int) ((1D / MapContext.getDrawFrameRate()) * 3 * 1000);
        BufferedImage[] imageLevels = null;
        Graphics2D[] graphics = null;

        imageLevels = new BufferedImage[getZSort().getLevelCount()];
        graphics = new Graphics2D[imageLevels.length];
        for (int i = 0; !cancel.isCanceled() && i < imageLevels.length; i++) {

            imageLevels[i] = CompatLocator.getGraphicsUtils()
            .createBufferedImage(image.getWidth(), image.getHeight(),
                image.getType());

            graphics[i] = imageLevels[i].createGraphics();
            graphics[i].setTransform(g.getTransform());
            graphics[i].setRenderingHints(g.getRenderingHints());
        }
        // -- end visual FX stuff

        DisposableIterator it = null;
        try {
            it = featureSet.fastIterator();
            // Iteration over each feature
            while (it.hasNext()) {
                if (cancel.isCanceled()) {
                    return;
                }
                Feature feat = (Feature) it.next();

                bSymbolLevelError |= drawFeatureMultiLayer(image, g, viewPort,
                    cancel, coordTrans, dpi, drawnNotification, selection,
                    time, screenRefreshDelay, imageLevels, graphics, feat);

            }
        } catch (ConcurrentDataModificationException e) {
            cancel.setCanceled(true);
            return;
        } finally {
            if (it != null) {
                it.dispose();
            }
        }

        g.drawImage(image, 0, 0, null);

        Point2D offset = viewPort.getOffset();
        CompatLocator.getGraphicsUtils().translate(g, offset.getX(),
            offset.getY());

        for (int i = 0; !cancel.isCanceled() && i < imageLevels.length; i++) {
            g.drawImage(imageLevels[i], 0, 0, null);
            imageLevels[i] = null;
            graphics[i] = null;
        }

        CompatLocator.getGraphicsUtils().translate(g, -offset.getX(),
            -offset.getY());

        imageLevels = null;
        graphics = null;

        if (bSymbolLevelError) {
            setZSort(null);
        }
    }

    /**
     * Draws a Feature with the symbols of the legend, using a multiple drawing
     * layer.
     */
    private boolean drawFeatureMultiLayer(BufferedImage image, Graphics2D g,
        ViewPort viewPort, Cancellable cancel, ICoordTrans coordTrans,
        double dpi, DefaultFeatureDrawnNotification drawnNotification,
        FeatureSelection selection, long time, int screenRefreshDelay,
        BufferedImage[] imageLevels, Graphics2D[] graphics, Feature feat)
    throws MapContextException, CreateGeometryException {

        Geometry geom = feat.getDefaultGeometry();
        boolean bSymbolLevelError = false;
        long drawingTime = time;

        if (geom==null && geom.getType() == Geometry.TYPES.NULL) {
            return false;
        }

        ISymbol sym = getSymbol(feat, selection);

        if (sym == null) {
            return false;
        }

        if (coordTrans != null) {
            geom = geom.cloneGeometry();
            geom.reProject(coordTrans);
        }

        if (cancel.isCanceled()) {
            return false;
        }

        // Check if this symbol is a multilayer
        int[] symLevels = getZSort().getLevels(sym);
        if (sym instanceof IMultiLayerSymbol) {
            // if so, treat each of its layers as a single
            // symbol
            // in its corresponding map level
            IMultiLayerSymbol mlSym = (IMultiLayerSymbol) sym;
            for (int i = 0; !cancel.isCanceled() && i < mlSym.getLayerCount(); i++) {
                ISymbol mySym = mlSym.getLayer(i);
                int symbolLevel = 0;
                if (symLevels != null) {
                    symbolLevel = symLevels[i];
                } else {
                    /*
                     * an error occured when managing symbol levels some of the
                     * legend changed events regarding the symbols did not
                     * finish satisfactory and the legend is now inconsistent.
                     * For this drawing, it will finish as it was at the bottom
                     * (level 0) but, when done, the ZSort will be reset to
                     * avoid app crashes. This is a bug that has to be fixed.
                     */
                    bSymbolLevelError = true;
                }
                drawGeometry(geom, imageLevels[symbolLevel], feat, mySym,
                    viewPort, graphics[symbolLevel], dpi, cancel);
            }
        } else {
            // else, just draw the symbol in its level
            int symbolLevel = 0;
            if (symLevels != null) {
                symbolLevel = symLevels[0];
            }
            drawGeometry(geom, imageLevels[symbolLevel], feat, sym, viewPort,
                graphics[symbolLevel], dpi, cancel);
        }

        // -- visual FX stuff
        // Cuando el offset!=0 se est� dibujando sobre el
        // Layout y por tanto no tiene que ejecutar el
        // siguiente c�digo.
        Point2D offset = viewPort.getOffset();
        if (offset.getX() == 0 && offset.getY() == 0) {
            if ((System.currentTimeMillis() - drawingTime) > screenRefreshDelay) {

                BufferedImage virtualBim = CompatLocator.getGraphicsUtils()
                .createBufferedImage(image.getWidth(),
                    image.getHeight(), BufferedImage.TYPE_INT_ARGB);

                Graphics2D virtualGraphics = virtualBim.createGraphics();
                virtualGraphics.drawImage(image, 0, 0, null);
                for (int i = 0; !cancel.isCanceled() && i < imageLevels.length; i++) {
                    virtualGraphics.drawImage(imageLevels[i], 0, 0, null);
                }
                g.clearRect(0, 0, image.getWidth(), image.getHeight());
                g.drawImage(virtualBim, 0, 0, null);
                drawingTime = System.currentTimeMillis();
            }
            // -- end visual FX stuff

        }
        // Notify the drawing observers
        drawnNotification.setFeature(feat);
        drawnNotification.setDrawnGeometry(geom);
        notifyObservers(drawnNotification);
        return bSymbolLevelError;
    }

    /**
     * Returns the symbol to use to draw a {@link Feature} taking into account
     * if it is selected.
     */
    private ISymbol getSymbol(Feature feat, FeatureSelection selection)
    throws MapContextException {
        // retrieve the symbol associated to such feature
        ISymbol sym = getSymbolByFeature(feat);

        if (sym != null && selection.isSelected(feat)) {
            sym = sym.getSymbolForSelection();
        }
        return sym;
    }

    /**
     * Returns if the legend is using a ZSort.
     */
    private boolean isUseZSort() {
        return getZSort() != null && getZSort().isUsingZSort();
    }

    /**
     * Draws a {@link Geometry} using the given {@link ISymbol}, over the
     * {@link Graphics2D} object.
     */
    private void drawGeometry(Geometry geom, BufferedImage image,
        Feature feature, ISymbol symbol, ViewPort viewPort,
        Graphics2D graphics, double dpi, Cancellable cancellable)
    throws CreateGeometryException {

        if (geom instanceof Aggregate) {
            drawAggregate((Aggregate) geom, image, feature, symbol, viewPort,
                graphics, dpi, cancellable);
        } else {
            boolean bDrawCartographicSupport = false;
            if (symbol instanceof CartographicSupport) {
                bDrawCartographicSupport = ((CartographicSupport) symbol)
                .getUnit() != -1;
            }

            double previousSize = 0.0d;

            if (bDrawCartographicSupport) {
                // make the symbol to resize itself with the current rendering
                // context
                previousSize = ((CartographicSupport) symbol)
                .toCartographicSize(viewPort, dpi, geom);
            }

            try {
                symbol.draw(graphics, viewPort.getAffineTransform(), geom,
                    feature, cancellable);
            } finally {
                if (bDrawCartographicSupport) {
                    // restore previous size
                    ((CartographicSupport) symbol).setCartographicSize(
                        previousSize, geom);
                }
            }
        }
    }

    /**
     * Draws an {@link Aggregate} using the given {@link ISymbol}, over the
     * {@link Graphics2D} object.
     */
    private void drawAggregate(Aggregate aggregate, BufferedImage image,
        Feature feature, ISymbol symbol, ViewPort viewPort,
        Graphics2D graphics, double dpi, Cancellable cancellable)
    throws CreateGeometryException {
        for (int i = 0; i < aggregate.getPrimitivesNumber(); i++) {
            Geometry prim = aggregate.getPrimitiveAt(i);
            drawGeometry(prim, image, feature, symbol, viewPort, graphics, dpi,
                cancellable);
        }
    }

    public Object clone() throws CloneNotSupportedException {
        AbstractVectorialLegend clone = (AbstractVectorialLegend) super.clone();

        // Clone zSort
        ZSort zSort = getZSort();
        if (zSort != null) {
            clone.setZSort(new ZSort(clone));
        }
        return clone;
    }

    public void loadFromState(PersistentState state)
    throws PersistenceException {
        // Set parent properties
        super.loadFromState(state);
        // Set own properties

        setShapeType(state.getInt(FIELD_SHAPETYPE));
        if (state.getBoolean(FIELD_HAS_ZSORT)) {
            setZSort(new ZSort(this));
        }
        setDefaultSymbol((ISymbol) state.get(FIELD_DEFAULT_SYMBOL));
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent properties
        super.saveToState(state);
        // Save own properties
        state.set(FIELD_SHAPETYPE, getShapeType());
        state.set(FIELD_HAS_ZSORT, this.zSort != null);
        state.set(FIELD_DEFAULT_SYMBOL, getDefaultSymbol());
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager
                .getDefinition(VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition = manager.addDefinition(
                    AbstractVectorialLegend.class,
                    VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME,
                    VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME
                    + " persistence definition", null, null);
                // Extend the Legend base definition
                definition.extend(manager
                    .getDefinition(LEGEND_PERSISTENCE_DEFINITION_NAME));

                // Shapetype
                definition.addDynFieldInt(FIELD_SHAPETYPE).setMandatory(true);
                // ZSort
                definition.addDynFieldBoolean(FIELD_HAS_ZSORT).setMandatory(
                    true);
                // Default symbol
                definition.addDynFieldObject(FIELD_DEFAULT_SYMBOL)
                .setClassOfValue(ISymbol.class).setMandatory(true);
            }
            return Boolean.TRUE;
        }

    }

    /**
     * Returns the names of the {@link Feature} attributes required for the
     * {@link ILegend} to operate.
     *
     * @param featureStore
     *            the store where the {@link Feature}s belong to
     * @return the names of required {@link Feature} attribute names
     * @throws DataException
     *             if there is an error getting the attribute names
     */
    protected abstract String[] getRequiredFeatureAttributeNames(
        FeatureStore featureStore) throws DataException;
}
