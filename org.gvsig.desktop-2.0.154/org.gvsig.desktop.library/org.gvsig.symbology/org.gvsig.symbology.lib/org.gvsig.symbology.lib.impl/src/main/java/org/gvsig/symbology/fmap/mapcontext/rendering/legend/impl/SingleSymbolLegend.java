/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.MultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements a legend composed by single symbols.
 * 
 * @author Vicente Caballero Navarro
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SingleSymbolLegend extends AbstractVectorialLegend implements
		ISingleSymbolLegend {

	final static private Logger LOG = LoggerFactory.getLogger(VectorialUniqueValueLegend.class);

	public static final String SINGLE_SYMBOL_PERSISTENCE_DEFINITION_NAME =
			"SimgleSymbolLegend";
	
	private ISymbol defaultSymbol;
    private int shapeType = Geometry.TYPES.SURFACE; // Por defecto, tipo pol�gono

	/**
	 * Constructor method, needed by persistence.
	 */
	public SingleSymbolLegend() {
		super();
	}

	/**
	 * Convenience fast constructor.
	 *
	 * @param style S�mbolo.
	 */
	public SingleSymbolLegend(ISymbol style) {
		super();
		setDefaultSymbol(style);
	}


	public void setDefaultSymbol(ISymbol s) {
		if (s == null) throw new NullPointerException("Default symbol cannot be null");
		ISymbol old = defaultSymbol;
		defaultSymbol = s;
		fireDefaultSymbolChangedEvent(new SymbolLegendEvent(old, s));
	}


	public ISymbol getSymbol(int recordIndex) {
		return defaultSymbol;
	}

	public ISymbol getDefaultSymbol() {
		if(defaultSymbol==null) {
			defaultSymbol = getSymbolManager().createSymbol(shapeType);
		}
		return defaultSymbol;
	}



	public int getShapeType() {
		return shapeType;
	}

	public void setShapeType(int shapeType) {
		if (this.shapeType != shapeType) {
			if(defaultSymbol==null || defaultSymbol.getSymbolType()!=shapeType){
				defaultSymbol = getSymbolManager().createSymbol(shapeType);
			}
			this.shapeType = shapeType;
		}
	}

    public ISymbol getSymbolByFeature(Feature feat) {
        return getDefaultSymbol();
    }

	public void useDefaultSymbol(boolean b) {
		LOG.warn("TODO: SingleSymbolLegend.useDefaultSymbol");
	}

    public String[] getUsedFields() {
        return new String[0];
    }

    public boolean isUseDefaultSymbol() {
    	return true;

    }


    public String getClassName() {
		return getClass().getName();
	}

    public boolean isSuitableForShapeType(int shapeType) {
		return getShapeType() == shapeType;
	}


	public void setFeatureStore(FeatureStore fs) throws DataException {
		LOG.warn("TODO: SingleSymbolLegend.useDefaultSymbol");
	}
	
	protected String[] getRequiredFeatureAttributeNames(
			FeatureStore featureStore) throws DataException {
		// We only need the default Geometry to draw
		return new String[] { featureStore
				.getDefaultFeatureType().getDefaultGeometryAttributeName() };
	}

	public Object clone() throws CloneNotSupportedException {
		SingleSymbolLegend clone = (SingleSymbolLegend) super.clone();

		// Clone default symbol
		if (defaultSymbol != null) {
			clone.defaultSymbol = (ISymbol) defaultSymbol.clone();
		}

		return clone;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent properties
		super.loadFromState(state);
		// LOG.warn("FIXME: SingleSymbolLegend.loadFromState");
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent properties
		super.saveToState(state);
		// LOG.warn("FIXME: SingleSymbolLegend.saveToState");
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SINGLE_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SingleSymbolLegend.class,
						SINGLE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						SINGLE_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				// Extend the Vectorial Legend base definition
				definition.extend(manager.getDefinition(VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME));
			}
			return Boolean.TRUE;
		}
		
	}

	public static class RegisterLegend implements Callable {

		public Object call() throws Exception {
	        MapContextManager manager = MapContextLocator.getMapContextManager();

            manager.registerLegend(ISingleSymbolLegend.LEGEND_NAME,
                    SingleSymbolLegend.class);

			return Boolean.TRUE;
		}
		
	}

}