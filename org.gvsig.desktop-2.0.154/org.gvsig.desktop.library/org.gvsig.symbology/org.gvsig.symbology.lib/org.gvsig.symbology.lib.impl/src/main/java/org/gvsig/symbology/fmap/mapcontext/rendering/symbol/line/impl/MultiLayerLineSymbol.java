/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.IMultiLayerLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ILineStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * MultiLayerLineSymbol allows to create new symbols using a composition of several lineal
 * symbols (xxxLineSymbol implementing ILineSymbol)and be treated as an only one symbol.
 *
 * @author  jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiLayerLineSymbol extends AbstractLineSymbol implements
		ILineSymbol, IMultiLayerSymbol, IMultiLayerLineSymbol {

    public static final String MULTILAYER_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME = "MultiLayerLineSymbol";

    private static final String FIELD_LAYERS = "layers";

    private ILineSymbol[] layers = new ILineSymbol[0];
	private MultiLayerLineSymbol selectionSymbol;

	// Cached greatest line width from all the layers
	private double lineWidth;

	public MultiLayerLineSymbol() {
		super();
	}

	public Color getColor() {
		/*
		 * a multilayer symbol does not define any color, the color
		 * of each layer is defined by the layer itself
		 */
		return null;
	}

	public ILineStyle getLineStyle() {
		/*
		 * a multilayer symbol does not define any style, the style
		 * of each layer is defined by the layer itself
		 */
		return null;
	}

    public double getLineWidth() {
	return lineWidth;
    }

//    private double calculateLineWidth() {
//	double myLineWidth = 0;
//	for (int i = 0; i < getLayerCount(); i++) {
//	    myLineWidth = Math.max(myLineWidth,
//		    ((ILineSymbol) getLayer(i)).getLineWidth());
//	}
//	return myLineWidth;
//    }

    public void setLineWidth(double width) {
	if (width > 0 && width != lineWidth) {
	    this.lineWidth = width;
	    double scaleFactor = width / lineWidth;
			for (int i = 0; layers != null && i < layers.length; i++) {
		layers[i].setLineWidth(layers[i].getLineWidth() * scaleFactor);
	    }
	}
    }

	public void setLineColor(Color color) {
		/*
		 * will apply the color to each layer
		 */
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setLineColor(color);
		}
	}

	public void setLineStyle(ILineStyle lineStyle) {
		/*
		 * will apply the same patter to each layer
		 */
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setLineStyle(lineStyle);
		}
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		for (int i = 0; (cancel == null || !cancel.isCanceled())
				&& layers != null && i < layers.length; i++) {
			layers[i].draw(g, affineTransform, geom, feature, cancel);
		}
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].drawInsideRectangle(g, scaleInstance, r, properties);
		}
	}

	public int getOnePointRgb() {
		// will paint only the last layer pixel
		return layers[layers.length-1].getOnePointRgb();
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		float[] myDistances = new float[] {0,0};
		distances[0] = 0;
		distances[1] = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].getPixExtentPlus(geom, myDistances, viewPort, dpi);
			distances[0] = Math.max(myDistances[0], distances[0]);
			distances[1] = Math.max(myDistances[1], distances[1]);
		}
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			selectionSymbol = new MultiLayerLineSymbol();
			selectionSymbol.setDescription(getDescription());
			for (int i = 0; layers != null && i < layers.length; i++) {
				selectionSymbol.addLayer(layers[i].getSymbolForSelection());
			}
		} else {
	          for (int i = 0; i < selectionSymbol.getLayerCount(); i++) {
	                selectionSymbol.setLayer(i, selectionSymbol.getLayer(i).getSymbolForSelection());
	            }
	        }
		return selectionSymbol;
	}

	public boolean isSuitableFor(Geometry geom) {
	      switch(geom.getType()) {
	        case Geometry.TYPES.CURVE:
	        case Geometry.TYPES.LINE:
	            return true;
	        }
	        return false;
	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].print(g, at, geom, properties);
		}

	}

	public void setLayer(int index, ISymbol layer) throws IndexOutOfBoundsException {
		layers[index] = (ILineSymbol) layer;
	}

	public void swapLayers(int index1, int index2) {
		ISymbol aux1 = getLayer(index1), aux2 = getLayer(index2);
		layers[index2] = (ILineSymbol) aux1;
		layers[index1] = (ILineSymbol) aux2;
	}

	public ISymbol getLayer(int layerIndex) {
		return layers[layerIndex];
	}

	public int getLayerCount() {
		return layers.length;
	}

	public void addLayer(ISymbol newLayer) {
		addLayer(newLayer, layers.length);
	}

	public void addLayer(ISymbol newLayer, int layerIndex) throws IndexOutOfBoundsException {

		if (newLayer == null) {
			return; // null are not allowed
		}
		ILineSymbol newLine = (ILineSymbol) newLayer;
		if (getLayerCount() == 0) {
			// apply the new layer properties to this multilayer

			setReferenceSystem(newLine.getReferenceSystem());
			setUnit(newLine.getUnit());
			lineWidth = newLine.getLineWidth();
		} else {
			if (newLine.getLineWidth() > getLineWidth()) {
				lineWidth = newLine.getLineWidth();
			}
			newLine.setReferenceSystem(getReferenceSystem());
			newLine.setUnit(getUnit());
		}

		selectionSymbol = null; /* forces the selection symbol to be re-created
		 						 * next time it is required
		 						 */


		if (layerIndex < 0 || layers.length < layerIndex) {
			throw new IndexOutOfBoundsException(layerIndex+" < 0 or "+layerIndex+" > "+layers.length);
		}
		List<ISymbol> newLayers = new ArrayList<ISymbol>();
		for (int i = 0; i < layers.length; i++) {
			newLayers.add(layers[i]);
		}
		try {
			newLayers.add(layerIndex, newLayer);
			layers = (ILineSymbol[])newLayers.toArray(new ILineSymbol[0]);
		} catch (ArrayStoreException asEx) {
			throw new ClassCastException(newLayer.getClass().getName()+" is not an ILineSymbol");
		}
	}

	public boolean removeLayer(ISymbol layer) {

		int capacity = 0;
		capacity = layers.length;
		List<ILineSymbol> lst = new ArrayList<ILineSymbol>(capacity);
		for (int i = 0; i < capacity; i++) {
			lst.add(layers[i]);
		}
		boolean contains = lst.remove(layer);
		layers = (ILineSymbol[])lst.toArray(new ILineSymbol[0]);
		return contains;
	}

	public int getAlpha() {
		// will compute the acumulated opacity
		double myAlpha = 0;
		for (int i = 0; i < layers.length; i++) {
			double layerAlpha = layers[i].getAlpha()/255D;
			myAlpha += (1-myAlpha)*layerAlpha;
		}
		int result = (int) Math.round(myAlpha * 255);
		return (result>255) ? 255 : result;
	}

	public void setAlpha(int outlineAlpha) {
		// first, get the biggest alpha in the layers and the index if such layer
		int maxAlpha = Integer.MIN_VALUE;
		int maxAlphaLayerIndex = 0;
		for (int i = 0; i < layers.length; i++) {
			if (layers[i].getAlpha() > maxAlpha) {
				maxAlpha = layers[i].getAlpha();
				maxAlphaLayerIndex = i;
			}
		}

		// now, max alpha takes the value of the desired alpha and the rest
		// will take a scaled (to biggest alpha) alpha value
		for (int i = 0; layers != null && i < layers.length; i++) {
			if (i!=maxAlphaLayerIndex) {
				double scaledAlpha = (double) layers[i].getAlpha()/maxAlpha;
				int myAlpha = (int) (outlineAlpha*scaledAlpha);
				if (myAlpha == 0) {
					myAlpha = 1;
				}
				layers[i].setAlpha(myAlpha);
			} else {
				int myAlpha = outlineAlpha;
				if (myAlpha == 0) {
					myAlpha = 1;
				}
				layers[i].setAlpha(myAlpha);
			}
		}

	}

	public void setUnit(int unitIndex) {
		super.setUnit(unitIndex);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setUnit(unitIndex);
		}
	}

	public void setReferenceSystem(int system) {
		super.setReferenceSystem(system);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setReferenceSystem(system);
		}
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
//		super.setCartographicSize(cartographicSize, shp);
		setLineWidth(cartographicSize);
	}

	public Object clone() throws CloneNotSupportedException {
		MultiLayerLineSymbol copy = (MultiLayerLineSymbol) super.clone();

		// Clone layers
		if (layers != null && layers.length > 0) {
			ILineSymbol[] layersCopy = new ILineSymbol[layers.length];
			for (int i = 0; i < layers.length; i++) {
				layersCopy[i] = (ILineSymbol) layers[i].clone();
			}
			copy.layers = layersCopy;
		}

		// Clone selection
		if (selectionSymbol != null) {
			copy.selectionSymbol = (MultiLayerLineSymbol) selectionSymbol
					.clone();
		}

		return copy;
	}

	@SuppressWarnings("unchecked")
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);
		// Set own properties
		List layers = state.getList(FIELD_LAYERS);
		if (layers != null) {
			for (int i = 0; i < layers.size(); i++) {
				addLayer((ISymbol) layers.get(i));
			}
		}
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_LAYERS, layers);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(MULTILAYER_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						MultiLayerLineSymbol.class,
						MULTILAYER_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						MULTILAYER_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the LineSymbol base definition
				definition.extend(manager.getDefinition(LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Layers
				definition.addDynFieldList(FIELD_LAYERS).setClassOfItems(ILineSymbol.class);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

		       shapeTypes = new int[] { Geometry.TYPES.CURVE, Geometry.TYPES.ARC,
		                Geometry.TYPES.ELLIPTICARC, Geometry.TYPES.MULTICURVE };
		        manager.registerMultiLayerSymbol(ILineSymbol.SYMBOL_NAME,
		            shapeTypes,
		            MultiLayerLineSymbol.class);

			return Boolean.TRUE;
		}

	}

}