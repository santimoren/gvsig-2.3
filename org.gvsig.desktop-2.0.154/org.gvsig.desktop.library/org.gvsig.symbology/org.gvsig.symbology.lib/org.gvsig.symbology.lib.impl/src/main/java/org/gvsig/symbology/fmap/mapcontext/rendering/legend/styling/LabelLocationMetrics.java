/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import java.awt.Point;
import java.awt.geom.Point2D;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelLocationMetrics;

/**
 * 
 * LabelLocationMetrics.java
 *
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es Dec 17, 2007
 *
 */
public class LabelLocationMetrics implements ILabelLocationMetrics {
	private Point2D anchor;
	private double rotation;
	private boolean isFixed;
	
	public LabelLocationMetrics(Point2D point2D, double rotation, boolean isFixed) {
		this.anchor = point2D;
		this.rotation = rotation;
		this.isFixed = isFixed;
	}
	
	public Point2D getAnchor() {
		return anchor;
	}
	
	public void setAnchor(Point anchor) {
		this.anchor = anchor;
	}
	
	public double getRotation() {
		return rotation;
	}
	
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public boolean isFixed() {
		return isFixed;
	}
	
	
}
