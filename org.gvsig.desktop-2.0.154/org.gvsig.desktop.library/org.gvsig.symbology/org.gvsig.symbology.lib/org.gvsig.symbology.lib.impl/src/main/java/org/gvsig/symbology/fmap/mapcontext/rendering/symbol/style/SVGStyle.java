/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.bridge.ViewBox;
import org.apache.batik.dom.svg.SVGOMDocument;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.gvt.renderer.StaticRenderer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Style for a SVG files.This is a XML specification and file format for
 * describing two-dimensional vector graphics, both static and animated.
 * SVG can be purely declarative or may include scripting. Images can contain
 * hyperlinks using outbound simple XLinks.It is an open standard created
 * by the World Wide Web Consortium..
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * 
 */
public class SVGStyle extends BackgroundFileStyle {

    public static final String SVG_STYLE_PERSISTENCE_DEFINITION_NAME =
        "SVGStyle";
    private static final String SOURCE = "source";
    private final GVTBuilder gvtBuilder = new GVTBuilder();
    private final UserAgentAdapter userAgent;
    private final DocumentLoader loader;
    private final StaticRenderer renderer = new StaticRenderer();
    private GraphicsNode gvtRoot;
    private final BridgeContext ctx;
    private Element elt;

    protected static final RenderingHints defaultRenderingHints;
    static {
        defaultRenderingHints = new RenderingHints(null);
        defaultRenderingHints.put(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);

        defaultRenderingHints.put(RenderingHints.KEY_INTERPOLATION,
            RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    }

    /**
     * Constructor method
     * 
     */
    public SVGStyle() {
        userAgent = new UserAgentAdapter();
        loader = new DocumentLoader(userAgent);
        ctx = new BridgeContext(userAgent, loader);
        renderer.setDoubleBuffered(true);
    }

    @Override
    public void drawInsideRectangle(Graphics2D g,
        Rectangle rect,
        boolean keepAspectRatio) throws SymbolDrawingException {
        if (keepAspectRatio) {
            AffineTransform ataux;
            if (elt.hasAttribute("viewBox")) {

                try {
                    ataux =
                        ViewBox.getViewTransform(null,
                            elt,
                            (float) rect.getWidth(),
                            (float) rect.getHeight(),
                            ctx);
                } catch (NullPointerException e) {
                    throw new SymbolDrawingException(SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
                }
            } else {
                Rectangle2D bounds = gvtRoot.getBounds();
                
                ataux = getNoRotationTransform(
                    bounds,
                    new Rectangle2D.Double(
                        rect.x,
                        rect.y,
                        rect.width,
                        rect.height),
                    true);
            }
            RenderingHints renderingHints = new RenderingHints(null);
            renderingHints.putAll(defaultRenderingHints);
            g.setRenderingHints(renderingHints);
            gvtRoot.setTransform(ataux);
            gvtRoot.paint(g);

        } else {

            Rectangle2D bounds = gvtRoot.getBounds();
            AffineTransform ataux = getNoRotationTransform(
                bounds,
                new Rectangle2D.Double(rect.x, rect.y, rect.width, rect.height),
                false);
            
            RenderingHints renderingHints = new RenderingHints(null);
            renderingHints.putAll(defaultRenderingHints);
            g.setRenderingHints(renderingHints);
            gvtRoot.setTransform(ataux);
            gvtRoot.paint(g);
        }
    }

    @Override
    public boolean isSuitableFor(ISymbol symbol) {
        return true;
    }

    @Override
    public void setSource(URL url) throws IOException {

    	source = url;
    	Document svgDoc;
    	try {
    		svgDoc = loader.loadDocument(url.toURI().toString());
    	} catch (URISyntaxException e) {
            throw new IOException(e);
    	}
    	gvtRoot = gvtBuilder.build(ctx, svgDoc);
    	renderer.setTree(gvtRoot);
    	elt = ((SVGOMDocument) svgDoc).getRootElement();
    }

    @Override
    public Rectangle getBounds() {
        try {
            Rectangle2D r = gvtRoot.getBounds();
            return new Rectangle((int) r.getX(),
                (int) r.getY(),
                (int) r.getWidth(),
                (int) r.getHeight());
        } catch (Exception e) {
            return new Rectangle();
        }
    }

    @Override
    public void drawOutline(Graphics2D g, Rectangle r) throws SymbolDrawingException {
        drawInsideRectangle(g, r);
    }

    
    @Override
    public void loadFromState(PersistentState state) throws PersistenceException {
    	try {
    		String sourceSymbolInLibrary = state.getString(SOURCE_SYMBOL_IN_LIBRARY);
    		if (sourceSymbolInLibrary != null){
    			setSource(new URL(getSymbolLibraryURL().toString()+sourceSymbolInLibrary));
    		} else {
    			setSource(state.getURL(SOURCE));
    		}
    	} catch (Exception e) {
    		throw new PersistenceCantSetSourceException(e);
    	}
    }

    @Override
    public void saveToState(PersistentState state) throws PersistenceException {
    	if (isLibrarySymbol()){
    		state.set(SOURCE_SYMBOL_IN_LIBRARY, getSourceSymbolInLibrary());
    	} else {
    		state.setNull(SOURCE_SYMBOL_IN_LIBRARY);
    	}
        state.set(SOURCE, source);
    }

    public static class RegisterPersistence implements Callable {

        @Override
        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(SVG_STYLE_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition =
                    manager.addDefinition(SVGStyle.class,
                        SVG_STYLE_PERSISTENCE_DEFINITION_NAME,
                        SVG_STYLE_PERSISTENCE_DEFINITION_NAME
                            + " Persistence definition",
                        null,
                        null);

                // Extend the Style base definition
                definition.extend(manager.getDefinition(BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldURL(SOURCE).setMandatory(true);
                definition.addDynFieldString(SOURCE_SYMBOL_IN_LIBRARY).setMandatory(false);
            }
            return Boolean.TRUE;
        }

    }
    
    
    private AffineTransform getNoRotationTransform(
        Rectangle2D from_rect,
        Rectangle2D to_rect,
        boolean keep_aspect) {
        
        double scalex = to_rect.getWidth() / from_rect.getWidth();
        double scaley = to_rect.getHeight() / from_rect.getHeight();
        
        if (keep_aspect) {
            // force min value for both
            scalex = Math.min(scalex,  scaley);
            scaley = scalex;
        }
        
        double from_new_center_x = scalex * from_rect.getCenterX(); 
        double from_new_center_y = scaley * from_rect.getCenterY(); 
        
        double offx = to_rect.getCenterX() - from_new_center_x;
        double offy = to_rect.getCenterY() - from_new_center_y;
        
        AffineTransform resp =
            AffineTransform.getTranslateInstance(offx, offy);
        
        // this composition is equivalent to:
        // first scale, then move
        resp.concatenate(
            AffineTransform.getScaleInstance(scalex, scaley));
        return resp;
    }
}
