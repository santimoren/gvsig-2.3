/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.IMultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ILineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * MultiShapeSymbol class allows to create a composition of several symbols with
 * different shapes and be treated as a single symbol.These shapes can be
 * marker,line or fill.
 * 
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiShapeSymbol implements ILineSymbol, IMarkerSymbol, IFillSymbol, IMultiShapeSymbol {

	private static final Logger LOG = LoggerFactory.getLogger(MultiShapeSymbol.class); 

	public static final String MULTI_SHAPE_SYMBOL_PERSISTENCE_DEFINITION_NAME = "MultiShapeSymbol";
	
	private static final String FIELD_MARKER = "marker";
	private static final String FIELD_LINE = "line";
	private static final String FIELD_FILL = "fill";
	private static final String FIELD_DESCRIPTION = "description";

	public static final String SYMBOL_NAME = "multiShape";
	
	private SymbolManager manager = MapContextLocator.getSymbolManager();
	
	private IMarkerSymbol marker;
	private ILineSymbol line;
	private IFillSymbol fill;
	private IMask mask;
	private String desc;
	private int referenceSystem;
	private MultiShapeSymbol symSelect;
	
	public MultiShapeSymbol() {
		super();
		marker = (IMarkerSymbol) createSymbol(IMarkerSymbol.SYMBOL_NAME);
		line = (ILineSymbol) createSymbol(ILineSymbol.SYMBOL_NAME);
		fill = (IFillSymbol) createSymbol(IFillSymbol.SYMBOL_NAME);
	}
	
	private ISymbol createSymbol(String symbolName) {
		return manager.createSymbol(symbolName);
	}

	public Color getLineColor() {
		return line.getColor();
	}

	public void setLineColor(Color color) {
		line.setLineColor(color);
	}

	public ILineStyle getLineStyle() {
		return line.getLineStyle();
	}

	public void setLineStyle(ILineStyle lineStyle) {
		line.setLineStyle(lineStyle);
	}

	public void setLineWidth(double width) {
		line.setLineWidth(width);
	}

	public double getLineWidth() {
		return line.getLineWidth();
	}

	public int getAlpha() {
		return line.getAlpha();
	}

	public void setAlpha(int outlineAlpha) {
		line.setAlpha(outlineAlpha);
	}

	public ISymbol getSymbolForSelection() {
		if (symSelect == null) {
			symSelect = new MultiShapeSymbol();
		}

		if (marker!=null){
			symSelect.setMarkerSymbol((IMarkerSymbol) marker.getSymbolForSelection());
		}

		if (line!=null){
			symSelect.setLineSymbol((ILineSymbol) line.getSymbolForSelection());
		}

		if (fill!=null ){
			symSelect.setFillSymbol((IFillSymbol) fill.getSymbolForSelection());
		}

		return symSelect;

	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
	    GeometryType geometryType = geom.getGeometryType();
	    if (geometryType.isTypeOf(Geometry.TYPES.POINT)){
	        if (marker != null) {
	            marker.draw(g, affineTransform, geom, feature, cancel);
	        }
	    }else if (geometryType.isTypeOf(Geometry.TYPES.CURVE)){
	        if (line != null) {
	            line.draw(g, affineTransform, geom, feature, cancel);
	        }
	    }else if (geometryType.isTypeOf(Geometry.TYPES.SURFACE)){
	        if (fill != null) {
	            fill.draw(g, affineTransform, geom, feature, cancel);
	        }			
	    }else if (geometryType.isTypeOf(Geometry.TYPES.AGGREGATE)){
	        Aggregate aggregate = (Aggregate)geom;
	        for (int i=0 ; i<aggregate.getPrimitivesNumber() ; i++){
	            draw(g, affineTransform, aggregate.getPrimitiveAt(i), feature, cancel);
	        }
	    }
	}
	
	public boolean isOneDotOrPixel(Geometry geom,
			double[] positionOfDotOrPixel, ViewPort viewPort, int dpi) {
		switch (geom.getType()) {
		case Geometry.TYPES.POINT: //Tipo punto
           	if (marker != null) {
				return marker.isOneDotOrPixel(geom, positionOfDotOrPixel, viewPort, dpi);
			}
			break;
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
		case Geometry.TYPES.SPLINE:
		if (line != null) {
				return line.isOneDotOrPixel(geom, positionOfDotOrPixel, viewPort, dpi);
			}
			break;

		case Geometry.TYPES.SURFACE:
        case Geometry.TYPES.ELLIPSE:
		case Geometry.TYPES.CIRCLE:
			if (fill != null) {
				return fill.isOneDotOrPixel(geom, positionOfDotOrPixel, viewPort, dpi);
			}
			break;
		}
		if (!geom.isSimple()){
			Aggregate aggregate = (Aggregate)geom;			
			for (int i=0 ; i<aggregate.getPrimitivesNumber() ; i++){
				if (!isOneDotOrPixel(aggregate.getPrimitiveAt(i), positionOfDotOrPixel, viewPort, dpi)){
					return false;
				}
			}
			return true;
		}
		return true;
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		// TODO Implement it
		throw new Error("Not yet implemented!");

	}

	public int getOnePointRgb() {
		// will return a mixture of all symbol's getOnePointRgb() value

		int rMarker = 0;
		int gMarker = 0;
		int bMarker = 0;
		int aMarker = 0;

		if (marker!=null && marker.getColor() != null) {
			rMarker = marker.getColor().getRed();
			gMarker = marker.getColor().getGreen();
			bMarker = marker.getColor().getBlue();
			aMarker = marker.getColor().getAlpha();
		}

		int rLine = 0;
		int gLine = 0;
		int bLine = 0;
		int aLine = 0;

		if (line != null  && line.getColor() != null) {
			rLine = line.getColor().getRed();
			gLine = line.getColor().getGreen();
			bLine = line.getColor().getBlue();
			aLine = line.getColor().getAlpha();
		}

		int rFill = 0;
		int gFill = 0;
		int bFill = 0;
		int aFill = 0;

		if (fill != null ) {
			Color colorOfFill = null;
			if (fill.getOutline()!=null) {
				colorOfFill = fill.getOutline().getColor();
			} else if (fill.getFillColor()!=null) {
				colorOfFill = fill.getFillColor();
			}
			if (colorOfFill != null) {
				rFill = colorOfFill.getRed();
				gFill = colorOfFill.getGreen();
				bFill = colorOfFill.getBlue();
				aFill = colorOfFill.getAlpha();

			}
		}

		int red = (rMarker + rLine + rFill) / 3;
		int green = (gMarker + gLine + gFill) / 3;
		int blue = (bMarker + bLine + bFill) / 3;
		int alpha = (aMarker + aLine + aFill) / 3;

		return (alpha) << 24 + (red << 16) + (green << 8) + blue;
	}

	public String getDescription() {
		return desc;
	}

	public boolean isShapeVisible() {
		if (marker!=null) {
			return marker.isShapeVisible();
		}

		if (line != null) {
			return line.isShapeVisible();
		}

		if (fill != null) {
			fill.isShapeVisible();
		}

		return false;
	}

	public void setDescription(String desc) {
		this.desc = desc ;
	}

	public int getSymbolType() {
		return Geometry.TYPES.GEOMETRY;
	}

	public boolean isSuitableFor(Geometry geom) {
		// suitable for everything (why else does it exist?)
		return true;
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		double myWidth =  (r.getWidth()/3);

		Rectangle rect = new Rectangle(0, 0, (int) myWidth, r.height);

		if (marker != null) {
			g.translate(r.x, r.y);
			marker.drawInsideRectangle(g, scaleInstance, rect, properties);
			g.translate(-(r.x), -(r.y));
		}

		if (line != null) {
			g.translate(r.x+myWidth, r.y);
			line.drawInsideRectangle(g, scaleInstance, rect, properties);
			g.translate(-(r.x+myWidth), -(r.y));
		}

		if (fill != null) {
			g.translate(r.x+myWidth+myWidth, r.y);
			fill.drawInsideRectangle(g, scaleInstance, rect, properties);
			g.translate(-(r.x+myWidth+myWidth), -(r.y));

		}
	}

//	public String getClassName() {
//		return getClass().getName();
//	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		switch (geom.getType()) {
		case Geometry.TYPES.POINT: //Tipo punto
           	if (marker != null) {
				marker.print(g, at, geom, properties);
			}
			break;
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
			if (line != null) {
				line.print(g, at, geom, properties);
			}
			break;

		case Geometry.TYPES.SURFACE:
		case Geometry.TYPES.ELLIPSE:
        case Geometry.TYPES.CIRCLE:
			if (fill != null) {
				fill.print(g, at, geom, properties);
			}
			break;
		}
	}

	public double getRotation() {
		if (marker != null) {
			return marker.getRotation();
		}
		return 0;
	}

	public void setRotation(double rotation) {
		if (marker != null) {
			marker.setRotation(rotation);
		}
	}

	public Point2D getOffset() {
		if (marker != null) {
			return marker.getOffset();
		}
		return new Point2D.Double();
	}

	public void setOffset(Point2D offset) {
		if (marker != null) {
			marker.setOffset(offset);
		}
	}

	public double getSize() {
		if (marker != null) {
			return marker.getSize();
		}
		return 0;
	}

	public void setSize(double size) {
		if (marker != null) {
			marker.setSize(size);
		}

	}

	public Color getColor() {
		if (marker != null) {
			return marker.getColor();
		}
		return null;
	}

	public void setColor(Color color) {
		if (marker != null) {
			marker.setColor(color);
		}
	}

	public void setFillColor(Color color) {
		if (fill != null) {
			fill.setFillColor(color);
		}
	}

	public void setOutline(ILineSymbol outline) {
		if (fill != null) {
			fill.setOutline(outline);
		}
	}

	public Color getFillColor() {
		if (fill != null) {
			return fill.getFillColor();
		}
		return null;
	}

	public ILineSymbol getOutline() {
		if (fill != null) {
			return fill.getOutline();
		}
		return null;
	}

	public int getFillAlpha() {
		if (fill != null) {
			return fill.getFillAlpha();
		}
		return 255;
	}

	public IMask getMask() {
		return mask;
	}

	public void setUnit(int unitIndex) {
		if (marker != null) {
			marker.setUnit(unitIndex);
		}
		if (line != null) {
			line.setUnit(unitIndex);
		}
	}

	public int getUnit() {
		if (marker != null) {
			return marker.getUnit();
		}
		if (line != null) {
			return line.getUnit();
		}
		return -1;
	}

	public void setMask(IMask mask) {
		// TODO Implement it
		throw new Error("Not yet implemented!");

	}

	public int getReferenceSystem() {
		return this.referenceSystem;
	}

	public void setReferenceSystem(int system) {
		this.referenceSystem = system;

	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		switch (geom.getType()) {
		case Geometry.TYPES.POINT: //Tipo punto
        	if (marker != null) {
				return marker.toCartographicSize(viewPort, dpi, geom);
			}
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
			if (line != null) {
				return line.toCartographicSize(viewPort, dpi, geom);
			}
		case Geometry.TYPES.SURFACE:
		case Geometry.TYPES.ELLIPSE:
		case Geometry.TYPES.CIRCLE:
			LOG.warn("Cartographic size does not have any sense for fill symbols");

		}
		return -1;
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		switch (geom.getType()) {
		case Geometry.TYPES.POINT: //Tipo punto
        	if (marker != null) {
				marker.setCartographicSize(cartographicSize, null);
			}
        	break;
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
			if (line != null) {
				line.setCartographicSize(cartographicSize, null);
			}
        	break;
		case Geometry.TYPES.SURFACE:
		case Geometry.TYPES.ELLIPSE:
	    case Geometry.TYPES.CIRCLE:
	    	LOG.warn("Cartographic size does not have any sense for fill symbols");
		}
	}


	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		switch (geom.getType()) {
		case Geometry.TYPES.POINT: //Tipo punto
        	return CartographicSupportToolkit.
			getCartographicLength(marker,
								  getSize(),
								  viewPort,
								  dpi);
		case Geometry.TYPES.CURVE:
		case Geometry.TYPES.ARC:
			return CartographicSupportToolkit.
			getCartographicLength(line,
								  getSize(),
								  viewPort,
								  dpi);
		case Geometry.TYPES.SURFACE:
     	case Geometry.TYPES.ELLIPSE:
	    case Geometry.TYPES.CIRCLE:
	    	LOG.warn("Cartographic size does not have any sense for fill symbols");
		}
		return -1;
	}

	public IMarkerSymbol getMarkerSymbol() {
		return marker;
	}

	public ILineSymbol getLineSymbol() {
		return line;
	}

	public IFillSymbol getFillSymbol() {
		return fill;
	}

	public void setMarkerSymbol(IMarkerSymbol markerSymbol) {
		this.marker = markerSymbol;
	}

	public void setLineSymbol(ILineSymbol lineSymbol) {
		this.line = lineSymbol;
	}

	public void setFillSymbol(IFillSymbol fillFillSymbol) {
		this.fill = fillFillSymbol;
	}

	public boolean hasFill() {
	if (fill == null) {
		return false;
	}
	return fill.hasFill();
	}

	public void setHasFill(boolean hasFill) {
		if (fill != null) {
			fill.setHasFill(hasFill);
//			this.hasFill = hasFill;
		}
	}

	public boolean hasOutline() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setHasOutline(boolean hasOutline) {
		// TODO Auto-generated method stub

	}

	public Object clone() throws CloneNotSupportedException {
		MultiShapeSymbol copy = (MultiShapeSymbol) super.clone();

		// Clone inner symbols
		if (marker != null) {
			copy.marker = (IMarkerSymbol) marker.clone();
		}
		if (line != null) {
			copy.line = (ILineSymbol) line.clone();
		}
		if (fill != null) {
			copy.fill = (IFillSymbol) fill.clone();
		}
		
		if (mask != null) {
			copy.mask = (IMask) mask.clone();
		}
		
		// Clone selection symbol
		if (symSelect != null) {
			copy.symSelect = (MultiShapeSymbol) symSelect.clone();
		}
		
		return copy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setDescription(state.getString(FIELD_DESCRIPTION));
		setFillSymbol((IFillSymbol) state.get(FIELD_FILL));
		setMarkerSymbol((IMarkerSymbol) state.get(FIELD_MARKER));
		setLineSymbol((ILineSymbol) state.get(FIELD_LINE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DESCRIPTION, getDescription());
		state.set(FIELD_FILL, getFillSymbol());
		state.set(FIELD_MARKER, getMarkerSymbol());
		state.set(FIELD_LINE, getLineSymbol());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(MULTI_SHAPE_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						MultiShapeSymbol.class,
						MULTI_SHAPE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						MULTI_SHAPE_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				// Description
				definition.addDynFieldString(FIELD_DESCRIPTION).setMandatory(false);
				// Fill symbol
				definition.addDynFieldObject(FIELD_FILL).setClassOfValue(IFillSymbol.class).setMandatory(true);
				// Line symbol
				definition.addDynFieldObject(FIELD_LINE).setClassOfValue(ILineSymbol.class).setMandatory(true);
				// Marker symbol
				definition.addDynFieldObject(FIELD_MARKER).setClassOfValue(IMarkerSymbol.class).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
	        int[] shapeTypes;
	        SymbolManager manager = MapContextLocator.getSymbolManager();
	        
	        shapeTypes = new int[] { Geometry.TYPES.GEOMETRY };
	        manager.registerSymbol(MultiShapeSymbol.SYMBOL_NAME,
	            shapeTypes,
	            MultiShapeSymbol.class
	        );
			return Boolean.TRUE;
		}
		
	}
	
}