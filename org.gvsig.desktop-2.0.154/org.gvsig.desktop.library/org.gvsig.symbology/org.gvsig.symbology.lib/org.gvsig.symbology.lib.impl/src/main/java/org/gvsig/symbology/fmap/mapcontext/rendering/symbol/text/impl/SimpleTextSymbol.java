/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.AbstractSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.ISimpleTextSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * SimpleTextSymbol is a class used to create symbols composed using a text defined by
 * the user.This text can be edited (changing the color, the font of the characters, and
 * the rotation of the text).
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SimpleTextSymbol extends AbstractSymbol implements ITextSymbol, ISimpleTextSymbol {
	final static private Logger LOG = LoggerFactory.getLogger(SimpleTextSymbol.class);

	public static final String SIMPLE_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME = "SimpleTextSymbol";

	private static final String FIELD_TEXT = "text";
	private static final String FIELD_FONT = "font";
	private static final String FIELD_TEXT_COLOR = "textColor";
	private static final String FIELD_ROTATION = "rotation";
	private static final String FIELD_AUTO_RESIZE = "autoResize";

	private static final String FIELD_HAS_HALO = "hasHalo";
	private static final String FIELD_HALO_COLOR = "haloColor";
	private static final String FIELD_HALO_WIDTH = "haloWidth";

	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();

	/*
	 * This value must be get using getText() if the 'null'
	 * value has to prevented
	 */
	private String text = "";
	private Font font;

	private Color haloColor = Color.WHITE;
	private float haloWidth = 3;
	private boolean drawWithHalo = false;
	private BasicStroke haloStroke = new BasicStroke(
			6, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

	private Color textColor;
	private double rotation;
	private FontRenderContext frc = new FontRenderContext(
			new AffineTransform(), false, true);
	private boolean autoresize;

	public SimpleTextSymbol() {
		super();
		SymbolPreferences preferences =
				MapContextLocator.getSymbolManager().getSymbolPreferences();
		font = preferences.getDefaultSymbolFont();
		textColor = preferences.getDefaultSymbolColor();
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		if (!isShapeVisible()) {
			return;
		}

		Point point = (Point)geom.cloneGeometry();
	    point.transform(affineTransform);

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.translate(point.getX(), point.getY());

		g.rotate(rotation);

		if (isDrawWithHalo()) {
			char[] charText = new char[getText().length()];
			getText().getChars(0, charText.length, charText, 0);
			GlyphVector glyph = font.layoutGlyphVector(frc, charText, 0, charText.length, Font.LAYOUT_NO_LIMIT_CONTEXT);
			g.setColor(getHaloColor());
			g.setStroke(haloStroke);
			g.draw(glyph.getOutline());
		}
		// Rectangle2D bounds = getBounds();
//		 getBounds devuelve el bounds de un texto dibujado de manera que
		// la linea base de la primera letra est� en el punto (0,0).
		// Por eso, si queremos alinear el texto de manera que la parte superior
		// izquierda de la primera letra est� en (0,0) debemos moverlo seg�n
		// el valor de la ordenada, en lugar de seg�n su altura.
		g.setColor(textColor);
		g.setFont(font);

		g.drawString(getText(), 0, 0);//(int) bounds.getHeight());
		g.rotate(-rotation);
		g.translate(-point.getX(), -point.getY());
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		int s = getFont().getSize();
		if (autoresize) {
			if (s==0) {
				s =1;
				setFontSize(s);
			}
			g.setFont(getFont());
		    FontMetrics fm = g.getFontMetrics();
		    Rectangle2D rect = fm.getStringBounds(getText(), g);
		    double width = rect.getWidth();
		    double height = rect.getHeight();
		    double rWidth = r.getWidth();
		    double rHeight = r.getHeight();
		    double ratioText = width/height;
		    double ratioRect = rWidth/rHeight;

		    if (ratioText>ratioRect) {
		    	s = (int) (s*(rWidth/width));
		    } else {
		    	s = (int) (s*(rHeight/height));
		    }
//		    r.setLocation(r.x, (int) Math.round(r.getY()+r.getHeight()));
		}
		setFontSize(s);
//		g.setColor(Color.red);
//		g.draw(r);
		try {
			if (properties==null)
				draw(g, null, geomManager.createPoint(
						r.getX(),
						// Small adjustment to center the fonts
						r.getY() + 0.725 * r.getHeight(),
						SUBTYPES.GEOM2D), null, null);
			else
				print(g, new AffineTransform(), geomManager.createPoint(r.getX(), r.getY(), SUBTYPES.GEOM2D), properties);
		} catch (CreateGeometryException e) {
			LOG.error("Error creatint a point", e);
			e.printStackTrace();
		}


	}

	public int getOnePointRgb() {
		return textColor.getRGB();
	}

	public void getPixExtentPlus(org.gvsig.fmap.geom.Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		throw new Error("Not yet implemented!");

	}

	public ISymbol getSymbolForSelection() {
		return this; // a text is not selectable
	}

	public int getSymbolType() {
		//This method returned the deleted type Geometry.TYPES.TEXT. I suppose that now
	    //it has to retrun geometry
	    return Geometry.TYPES.GEOMETRY;
	}

	public boolean isSuitableFor(Geometry geom) {
		return true;
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void print(Graphics2D g, AffineTransform at, org.gvsig.fmap.geom.Geometry geom, PrintAttributes properties){
		float originalSize = getFont().getSize2D();
		float size=originalSize;
		// scale it to size
		int pq = properties.getPrintQuality();
		if (pq == PrintAttributes.PRINT_QUALITY_NORMAL){
			size *= (double) 300/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_HIGH){
			size *= (double) 600/72;
		}else if (pq == PrintAttributes.PRINT_QUALITY_DRAFT){
			// d *= 72/72; // (which is the same than doing nothing)
		}
		setFont(getFont().deriveFont(size));
		draw(g,at,geom, null, null);
		setFont(getFont().deriveFont(originalSize));
	}

	public String getText() {
	    if (text == null) {
	        return "";
	    } else {
	        return text;
	    }
	}

	public Font getFont() {
		return font;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setFont(Font font) {
		if (font == null) {
			LOG.warn("font <-- null");

			return;
		}
		this.font = font;
	}

	public void setTextColor(Color color) {
		this.textColor = color;
	}

	public Color getColor() {
		return getTextColor();
	}

	public void setColor(Color color) {
		setTextColor(color);
	}

	public void setFontSize(double size) {
		Font newFont = new Font(font.getName(), font.getStyle(), (int) size);
//		if (newFont == null) {
//			logger.warn(
//					"Font(" + font.getName() + ", "
//					+ font.getStyle() + ", " + (int) size + " --> null");
//		} else {
			this.font = newFont;
//		}
	}

	public double getRotation() {
		return rotation;
	}

	/**
	 * Defines the angle of rotation for the text that composes the symbol
	 *
	 * @param rotation
	 */
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	/**
	 * Returns an Geometry which represents a rectangle containing the text in
	 * <b>screen</b> units.
	 */
	public Geometry getTextWrappingShape(org.gvsig.fmap.geom.primitive.Point p) {
		Font font = getFont();
		GlyphVector gv = font.createGlyphVector(frc, getText());

		Shape shape = gv.getOutline((float) p.getX(), (float) p.getY());
		Geometry myFShape;
		try {
			myFShape = geomManager.createSurface(new GeneralPathX(shape
					.getBounds2D().getPathIterator(null)), SUBTYPES.GEOM2D);
			myFShape.transform(AffineTransform.getTranslateInstance(p.getX(), p.getY()));

			if (rotation != 0) {
				myFShape.transform(AffineTransform.getRotateInstance(rotation));
			}
			return myFShape;
		} catch (CreateGeometryException e) {
			LOG.error("Error creating a surface", e);
			e.printStackTrace();
		}
		return null;
	}

	public Rectangle getBounds() {
//		FontMetrics fm = g.getFontMetrics();
//		Rectangle2D rect = fm.getStringBounds("graphics", g);

		Rectangle bounds = null;
		try {
			bounds = getTextWrappingShape(geomManager.createPoint(0,0, SUBTYPES.GEOM2D)).getShape().getBounds();
		} catch (CreateGeometryException e) {
			LOG.error("Error creating a point", e);
		}
		return bounds;
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		setFontSize(cartographicSize);
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double oldSize = getFont().getSize();
		setCartographicSize(getCartographicSize(
								viewPort,
								dpi,
								geom),
							geom);
		return oldSize;
	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return CartographicSupportToolkit.
					getCartographicLength(this,
										  getFont().getSize(),
										  viewPort,
										  dpi);
	}
	public boolean isAutoresizeEnabled() {
		return autoresize;
	}

	public void setAutoresizeEnabled(boolean autoresizeFlag) {
		this.autoresize = autoresizeFlag;
	}


	public Object clone() throws CloneNotSupportedException {
		SimpleTextSymbol copy = (SimpleTextSymbol) super.clone();

		return copy;
	}

	public void loadFromState(PersistentState state)

			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);

		// Set own properties
		setAutoresizeEnabled(state.getBoolean(FIELD_AUTO_RESIZE));
		setFont((Font) state.get(FIELD_FONT));
		setRotation(state.getDouble(FIELD_ROTATION));
		setText(state.getString(FIELD_TEXT));
		setTextColor((Color) state.get(FIELD_TEXT_COLOR));
		// halo
		this.setDrawWithHalo(state.getBoolean(FIELD_HAS_HALO));
		this.setHaloColor((Color) state.get(FIELD_HALO_COLOR));
		this.setHaloWidth(state.getFloat(FIELD_HALO_WIDTH));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_AUTO_RESIZE, isAutoresizeEnabled());
		state.set(FIELD_FONT, getFont());
		state.set(FIELD_ROTATION, getRotation());
		state.set(FIELD_TEXT, getText());
		state.set(FIELD_TEXT_COLOR, getTextColor());
		// halo
		state.set(FIELD_HAS_HALO, this.isDrawWithHalo());
		state.set(FIELD_HALO_COLOR, this.getHaloColor());
		state.set(FIELD_HALO_WIDTH, this.getHaloWidth());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleTextSymbol.class,
						SIMPLE_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the Symbol base definition
				definition.extend(manager.getDefinition(SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Auto resize?
				definition.addDynFieldBoolean(FIELD_AUTO_RESIZE)
					.setMandatory(true);

				// Font
				definition.addDynFieldObject(FIELD_FONT)
					.setClassOfValue(Font.class)
					.setMandatory(true);

				// Rotation
				definition.addDynFieldDouble(FIELD_ROTATION)
					.setMandatory(true);

				// Text
				definition.addDynFieldString(FIELD_TEXT);

				// Text color
				definition.addDynFieldObject(FIELD_TEXT_COLOR)
					.setClassOfValue(Color.class)
					.setMandatory(true);

				// halo
				definition.addDynFieldBoolean(FIELD_HAS_HALO).setMandatory(true);
				definition.addDynFieldObject(FIELD_HALO_COLOR).setClassOfValue(
						Color.class).setMandatory(true);
				definition.addDynFieldFloat(FIELD_HALO_WIDTH).setMandatory(true);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

			//This method registered the deleted type Geometry.TYPES.TEXT. I suppose that now
	        //it has to register geometry
	        shapeTypes = new int[] { Geometry.TYPES.GEOMETRY };
	        manager.registerSymbol(ITextSymbol.SYMBOL_NAME,
	            shapeTypes,
	            SimpleTextSymbol.class);

			return Boolean.TRUE;
		}

	}


	public Color getHaloColor() {
		return haloColor;
	}

	public void setHaloColor(Color co) {
		if (co != null) {
			this.haloColor = co;
		}
	}

	public float getHaloWidth() {
		return haloWidth;
	}

	public void setHaloWidth(float haloWidth) {
		this.haloWidth = haloWidth;
		this.haloStroke = new BasicStroke(
				2*haloWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	}

	public boolean isDrawWithHalo() {
		return drawWithHalo;
	}

	public void setDrawWithHalo(boolean drawWithHalo) {
		this.drawWithHalo = drawWithHalo;
	}


}