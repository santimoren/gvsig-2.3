/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Defines methods that allows the user to create an style based in a
 * background file.For this reason, BackgroundFileStyle will
 * have a parameter that will be an string in order to specify this source file.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public abstract class BackgroundFileStyle extends AbstractStyle
implements IBackgroundFileStyle {

    public static final String BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME = "BackgroundFileStyle";
    protected static final String SOURCE_SYMBOL_IN_LIBRARY = "sourceSymbolInLibrary";

    public static BackgroundFileStyle createStyleByURL(URL url) throws IOException {
        BackgroundFileStyle bgImage;
        String l = url.toString().toLowerCase();
        if (l.startsWith("http://") ||
            l.startsWith("https://"))  {
            bgImage = new RemoteFileStyle();
        } else if (l.toLowerCase().endsWith(".svg")) {
            bgImage = new SVGStyle();
        } else {
            bgImage = new ImageStyle();
        }
        bgImage.setSource(url);
        return bgImage;
    }
    protected URL source;
    /**
     * Sets the file that is used as a source to create the Background
     * @param f, File
     * @throws IOException
     */
    public abstract void setSource(URL url) throws IOException;
    /**
     * Gets the bounding <code>Rectangle</code> of this <code>Rectangle</code>.
     * <p>
     * This method is included for completeness, to parallel the
     * <code>getBounds</code> method of
     * {@link Component}.
     * @return    a new <code>Rectangle</code>, equal to the
     * bounding <code>Rectangle</code> for this <code>Rectangle</code>.
     * @see       java.awt.Component#getBounds
     * @see       #setBounds(Rectangle)
     * @see       #setBounds(int, int, int, int)
     * @since     JDK1.1
     */
    public abstract Rectangle getBounds();

    /**
     * Obtains the source of the file which is used to create the background
     * @return
     */
    public final URL getSource() {
        return source;
    }

    public final void drawInsideRectangle(Graphics2D g, Rectangle r) throws SymbolDrawingException {
        drawInsideRectangle(g, r, true);
    }

    public abstract void drawInsideRectangle(Graphics2D g, Rectangle r, boolean keepAspectRatio) throws SymbolDrawingException ;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    protected boolean isLibrarySymbol() {
    	return source.toString().startsWith(getSymbolLibraryURL().toString());
    }
    
    protected URL getSymbolLibraryURL(){
    	String symbolLibraryPath = MapContextLocator.getSymbolManager().getSymbolPreferences().getSymbolLibraryPath();
    	URL symbolLibraryURL = null;
    	try {
			symbolLibraryURL = new File(symbolLibraryPath).toURI().toURL();
		} catch (MalformedURLException e) {
			new RuntimeException(e);
		}
    	return symbolLibraryURL;
    }
    
    /**
     * Return the final substring of the URL symbol in the library
     * 
     * @return
     */
    protected String getSourceSymbolInLibrary(){
    	return source.toString().substring(getSymbolLibraryURL().toString().length());
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if( manager.getDefinition(BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
                DynStruct definition = manager.addDefinition(
                    BackgroundFileStyle.class,
                    BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME,
                    BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
                    null, 
                    null
                );

                // Extend the Style base definition
                definition.extend(manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME));
            }
            return Boolean.TRUE;
        }

    }

    public class PersistenceCantSetSourceException extends PersistenceException{

        /**
         * 
         */
        private static final long serialVersionUID = -3783412193739147977L;
        /**
         * 
         */
        private final static String MESSAGE_FORMAT = "Can't set source file.";
        private final static String MESSAGE_KEY = "_PersistenceCantSetSourceException";

        public PersistenceCantSetSourceException() {
            super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
        }
        
        public PersistenceCantSetSourceException(Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
        }

    }
}