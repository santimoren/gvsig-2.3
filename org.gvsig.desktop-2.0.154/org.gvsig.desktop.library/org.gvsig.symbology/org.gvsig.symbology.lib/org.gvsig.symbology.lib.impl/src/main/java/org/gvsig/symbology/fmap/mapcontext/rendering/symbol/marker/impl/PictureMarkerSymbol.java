/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: PictureMarkerSymbol.java 15593 2007-10-29 13:01:13Z jdominguez $
 * $Log$
 * Revision 1.17  2007-09-21 12:25:32  jaume
 * cancellation support extended down to the IGeometry and ISymbol level
 *
 * Revision 1.16  2007/09/19 16:22:04  jaume
 * removed unnecessary imports
 *
 * Revision 1.15  2007/09/11 07:46:55  jaume
 * *** empty log message ***
 *
 * Revision 1.14  2007/08/16 06:55:19  jvidal
 * javadoc updated
 *
 * Revision 1.13  2007/08/09 06:42:24  jvidal
 * javadoc
 *
 * Revision 1.12  2007/08/08 12:05:17  jvidal
 * javadoc
 *
 * Revision 1.11  2007/07/18 06:54:35  jaume
 * continuing with cartographic support
 *
 * Revision 1.10  2007/07/03 10:58:29  jaume
 * first refactor on CartographicSupport
 *
 * Revision 1.9  2007/06/29 13:07:01  jaume
 * +PictureLineSymbol
 *
 * Revision 1.8  2007/06/11 12:25:48  jaume
 * ISymbol drawing integration tests (markers and lines)
 *
 * Revision 1.7  2007/06/07 06:50:40  jaume
 * *** empty log message ***
 *
 * Revision 1.6  2007/05/29 15:46:37  jaume
 * *** empty log message ***
 *
 * Revision 1.5  2007/05/08 08:47:40  jaume
 * *** empty log message ***
 *
 * Revision 1.4  2007/03/21 17:36:22  jaume
 * *** empty log message ***
 *
 * Revision 1.3  2007/03/09 11:20:57  jaume
 * Advanced symbology (start committing)
 *
 * Revision 1.1.2.4  2007/02/21 07:34:09  jaume
 * labeling starts working
 *
 * Revision 1.1.2.3  2007/02/16 10:54:12  jaume
 * multilayer splitted to multilayerline, multilayermarker,and  multilayerfill
 *
 * Revision 1.1.2.2  2007/02/15 16:23:44  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2007/02/09 07:47:05  jaume
 * Isymbol moved
 *
 * Revision 1.1  2007/01/24 17:58:22  jaume
 * new features and architecture error fixes
 *
 *
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URL;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.BackgroundFileStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PictureMarkerSymbol allows to use an image file as a definition to be painted
 * instead of a marker symbol.
 */
public class PictureMarkerSymbol extends AbstractMarkerSymbol implements IPictureMarkerSymbol {

    private final Logger LOG =
        LoggerFactory.getLogger(PictureMarkerSymbol.class);

    public static final String PICTURE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME =
        "PictureMarkerSymbol";
    private static final String SELECTED = "selected";
    private static final String SELECTION_SYMBOL = "selectionSym";
    private static final String BACKGROUND_IMAGE = "bgImage";
    private static final String BACKGROUND_SELECTION_IMAGE = "bgSelImage";

//    private static final float SELECTION_OPACITY_FACTOR = .3F;

    private boolean selected;
    private PictureMarkerSymbol selectionSym;

    private BackgroundFileStyle bgImage;
    private BackgroundFileStyle bgSelImage;

    /**
     * Constructor method
     */
    public PictureMarkerSymbol() {
        super();
        setSize(18);
    }

    /**
     * Constructor method
     *
     * @param imageURL
     *            , URL of the normal image
     * @param selImageURL
     *            , URL of the image when it is selected in the map
     * @throws IOException
     */
    public PictureMarkerSymbol(URL imageURL, URL selImageURL) throws IOException {
        setImage(imageURL);
        if (selImageURL != null)
            setSelImage(selImageURL);
        else
            setSelImage(imageURL);

    }

    /**
     * Sets the file for the image to be used as a marker symbol
     *
     * @param imageFile
     *            , File
     * @throws IOException
     */
    public void setImage(URL imageUrl) throws IOException {

        bgImage = BackgroundFileStyle.createStyleByURL(imageUrl);
    }

    /**
     * Sets the file for the image to be used as a marker symbol (when it is
     * selected in the map)
     *
     * @param imageFile
     *            , File
     * @throws IOException
     */
    public void setSelImage(URL imageFileUrl) throws IOException {

        bgSelImage = BackgroundFileStyle.createStyleByURL(imageFileUrl);
    }

    public ISymbol getSymbolForSelection() {
        if (selectionSym == null) {
            try {
                selectionSym = (PictureMarkerSymbol) this.clone();
            } catch (CloneNotSupportedException e) {
                LOG.error("Error creating the selection symbol for the symbol "
                    + this, e);
            }
            selectionSym.selected = true;
            selectionSym.selectionSym = selectionSym; // avoid too much lazy
                                                      // creations
        }else{
            selectionSym.setColor(MapContext.getSelectionColor());
        }
        return selectionSym;
    }

    public void draw(Graphics2D g,
        AffineTransform affineTransform,
        Geometry geom,
        Feature f,
        Cancellable cancel) {
		org.gvsig.fmap.geom.primitive.Point p = (org.gvsig.fmap.geom.primitive.Point)geom.cloneGeometry();
		if (affineTransform!=null) {
			p.transform(affineTransform);
		}
        double x, y;
        int size = (int) Math.round(getSize());
        double halfSize = getSize() / 2;
        x = p.getX() - halfSize;
        y = p.getY() - halfSize;
        int xOffset = (int) getOffset().getX();
        int yOffset = (int) getOffset().getY();

        if (size > 0) {
            BackgroundFileStyle bg = (!selected) ? bgImage : bgSelImage;
            Rectangle rect = new Rectangle(size, size);
            g.translate(x + xOffset, y + yOffset);
            double auxRotation = getRotation();
            g.rotate(auxRotation, halfSize, halfSize);
            if (bg != null) {
                try {
                    bg.drawInsideRectangle(g, rect);
                } catch (SymbolDrawingException e) {
                    LOG.warn(Messages.getText("label_style_could_not_be_painted")
                        + ": " + bg.getSource().toString(),
                        e);
                }
            } else {
                LOG.warn(Messages.getText("label_style_could_not_be_painted")
                    + ": bg is Null");
            }
            g.rotate(-auxRotation, halfSize, halfSize);
            g.translate(-(x + xOffset), -(y + yOffset));

        }

    }


    public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {

        /*
         * Marker symbols which are not simple (images, etc)
         * are resized when drawn inside a rectangle
         */
        double saved_size = this.getSize();
        this.setSize(r.getHeight());

        super.drawInsideRectangle(g, scaleInstance, r, properties);

        // =================== Restoring size
        this.setSize(saved_size);
    }


    public String getClassName() {
        return getClass().getName();
    }

    // public void print(Graphics2D g, AffineTransform at, FShape shape)
    // throws ReadDriverException {
    // // TODO Implement it
    // throw new Error("Not yet implemented!");
    //
    // }
     /**
      * Returns the URL of the image that is used as a marker symbol
      * @return imagePath,URL
      */
    public URL getSource() {
    	return bgImage.getSource();
    }
    /**
     * Returns the URL of the image that is used as a marker symbol (when it
     is selected in the map)
     * @return selimagePath,URL
     */
    public URL getSelectedSource(){
    	return bgSelImage.getSource();
    }

    public Object clone() throws CloneNotSupportedException {
        PictureMarkerSymbol copy = (PictureMarkerSymbol) super.clone();

        // clone selection
        if (selectionSym != null) {
        	//to avoid an infinite loop
        	if (this == selectionSym){
        		copy.selectionSym = copy;
        	} else {
        		copy.selectionSym = (PictureMarkerSymbol) selectionSym.clone();
        	}
        }

        // clone brackground image
        if (bgImage != null) {
            copy.bgImage = (BackgroundFileStyle) bgImage.clone();
        }

        // clone selection brackground image
        if (bgSelImage != null) {
            copy.bgSelImage = (BackgroundFileStyle) bgSelImage.clone();
        }
        return copy;
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        // Set parent style properties
        super.loadFromState(state);

        this.selected = (Boolean) state.get(SELECTED);
        this.selectionSym = (PictureMarkerSymbol) state.get(SELECTION_SYMBOL);
        this.bgImage = (BackgroundFileStyle) state.get(BACKGROUND_IMAGE);
        this.bgSelImage =
            (BackgroundFileStyle) state.get(BACKGROUND_SELECTION_IMAGE);
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent fill symbol properties
        super.saveToState(state);

        // Save own properties
        state.set(SELECTED, this.selected);
        state.set(SELECTION_SYMBOL, this.getSymbolForSelection());
        state.set(BACKGROUND_IMAGE, this.bgImage);
        state.set(BACKGROUND_SELECTION_IMAGE, this.bgSelImage);
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(PICTURE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition =
                    manager.addDefinition(PictureMarkerSymbol.class,
                        PICTURE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME,
                        PICTURE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME
                            + " Persistence definition",
                        null,
                        null);

                // Extend the Style base definition
                definition.extend(manager.getDefinition(MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldBoolean(SELECTED).setMandatory(false);
                definition.addDynFieldObject(SELECTION_SYMBOL).setMandatory(false)
                    .setClassOfValue(PictureMarkerSymbol.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);
                definition.addDynFieldObject(BACKGROUND_SELECTION_IMAGE)
                    .setClassOfValue(BackgroundFileStyle.class).setMandatory(false);
            }
            return Boolean.TRUE;
        }
    }

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			SymbolManager manager = MapContextLocator.getSymbolManager();

	        manager.registerSymbol(PictureMarkerSymbol.PICTURE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME,
	            PictureMarkerSymbol.class);

			return Boolean.TRUE;
		}

	}

}
