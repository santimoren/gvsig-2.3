/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsEnvelopeEvaluator;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.AndEvaluator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

public class DefaultLabelingMethod implements ILabelingMethod {


	private static final String FIELD_DEFAULT_LABEL = "defaultLabel";
	private static final String DEFAULT_LABELING_METHOD_PERSISTENCE_DEFINITION_NAME =
			"DefaultLabelingMethod";
	private ILabelClass defaultLabel;

	/**
	 * Empty constructor for persistence.
	 */
	public DefaultLabelingMethod() {
		// Nothing to do
	}

	public DefaultLabelingMethod(LabelClass defaultLabel) {
		this();
		this.defaultLabel = defaultLabel;
	}


	public void addLabelClass(ILabelClass lbl) {
		defaultLabel = lbl;
	}


	public void deleteLabelClass(ILabelClass lbl) {
		// does nothing
	}

	public String getClassName() {
		return getClass().getName();
	}

	public boolean allowsMultipleClass() {
		return false;
	}

	public void renameLabelClass(ILabelClass lbl, String newName) {
		// does nothing
	}


	public FeatureSet getFeatureIteratorByLabelClass(
			FLyrVect layer,
			ILabelClass lc,
			ViewPort viewPort,
			String[] usedFields) throws DataException {
		
		FeatureStore featureStore=layer.getFeatureStore();
		
		if (viewPort == null && usedFields == null) {
			// nothing more to do
			return featureStore.getFeatureSet();
		}
		FeatureQuery featureQuery=featureStore.createFeatureQuery();
		
		if (usedFields != null) {
			featureQuery.setAttributeNames(usedFields);
		}

		Evaluator eva = null;
		
		if (viewPort != null) {
			FeatureType fty = featureStore.getDefaultFeatureType(); 
			eva = new IntersectsEnvelopeEvaluator(
					viewPort.getAdjustedEnvelope(),
					layer.getProjection(),
					fty, fty.getDefaultGeometryAttributeName());
			
		}
		
		if (lc.getSQLQuery() != null && lc.getSQLQuery().trim().length() > 0) {
		    Evaluator vp_eva = eva;
		    AndEvaluator and_eva = new AndEvaluator(vp_eva);
		    
		    String sql_str = lc.getSQLQuery();
		    /*
		     * SQL filter was validated in the dialog
		     */
	        Evaluator sql_eval = EvaluatorCreator.getEvaluator(sql_str);
	        and_eva.addEvaluator(sql_eval);
	        eva = and_eva; 
		}

		featureQuery.setFilter(eva);
		return (FeatureSet)featureStore.getFeatureSet(featureQuery);
	}

	public boolean definesPriorities() {
		return false;
	}

	public void setDefinesPriorities(boolean flag) {
		/* nothing, since only one label class is suported */
	}
	public ILabelClass getLabelClassByName(String labelName) {
		return defaultLabel;
	}

	public void clearAllClasses() {
		defaultLabel = null;
	}

	public ILabelClass[] getLabelClasses() {
		return defaultLabel == null ? new ILabelClass[0] : new ILabelClass[] { defaultLabel };
	}

	public ILabelingMethod cloneMethod() {
//XMLENTITY-UPDATED	
//		if (defaultLabel !=null)
//			try {
//				return new DefaultLabelingMethod(LabelingFactory.createLabelClassFromXML(defaultLabel.getXMLEntity()));
//			} catch (XMLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		return new DefaultLabelingMethod();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#loadFromState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		defaultLabel = (ILabelClass) state.get(FIELD_DEFAULT_LABEL);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#saveToState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DEFAULT_LABEL, defaultLabel);
	}
	
	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(DEFAULT_LABELING_METHOD_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						DefaultLabelingMethod.class,
						DEFAULT_LABELING_METHOD_PERSISTENCE_DEFINITION_NAME,
						DEFAULT_LABELING_METHOD_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				
				definition.addDynFieldObject(FIELD_DEFAULT_LABEL).setMandatory(false).setClassOfValue(ILabelClass.class);
			}
			return Boolean.TRUE;
		}
		
	}


	
}
