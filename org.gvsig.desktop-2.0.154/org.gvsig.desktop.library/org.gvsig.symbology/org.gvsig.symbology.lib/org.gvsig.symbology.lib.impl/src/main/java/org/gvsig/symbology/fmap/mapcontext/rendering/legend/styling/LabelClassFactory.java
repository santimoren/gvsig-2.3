package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClassFactory;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

public class LabelClassFactory implements ILabelClassFactory {

	I18nManager i18nManager = ToolsLocator.getI18nManager();

	public String getName() {
		return i18nManager.getTranslation("simple_label");
	}

	public String getID() {
		return "SimpleLabel";
	}

	public ILabelClass create() {
		return new LabelClass();
	}

	public String toString() {
		return this.getName();
	}

}
