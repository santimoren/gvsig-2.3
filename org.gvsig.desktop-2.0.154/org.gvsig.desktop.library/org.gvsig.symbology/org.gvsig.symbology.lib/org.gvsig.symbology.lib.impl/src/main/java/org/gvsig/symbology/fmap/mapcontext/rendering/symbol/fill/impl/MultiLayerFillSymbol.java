/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IMultiLayerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * MultiLayerFillSymbol is a symbol which allows to group several kind of fill symbols
 * (xxxFillSymbol implementing IFillSymbol)in one and treats it like single symbol.
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiLayerFillSymbol extends AbstractFillSymbol implements IFillSymbol, IMultiLayerSymbol, IMultiLayerFillSymbol{
	private static final Logger LOG = LoggerFactory.getLogger(MultiLayerFillSymbol.class);

    public static final String MULTILAYER_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME = "MultiLayerFillSymbol";

    private static final String FIELD_LAYERS = "layers";

	private static final double OPACITY_SELECTION_FACTOR = .8;
	private IFillSymbol[] layers = new IFillSymbol[0];
	private MultiLayerFillSymbol selectionSymbol;

	public Color getFillColor() {
		/*
		 * a multilayer symbol does not define any color, the color
		 * of each layer is defined by the layer itself
		 */
		return null;
	}

	public int getOnePointRgb() {
		// will paint only the last layer pixel
		return layers[layers.length-1].getOnePointRgb();
	}

	public ILineSymbol getOutline() {
		/*
		 * a multilayer symbol does not define any outline, the outline
		 * of each layer is defined by the layer it self
		 */
		return null;
	}

	public boolean isSuitableFor(Geometry geom) {
        switch(geom.getType()) {
        case Geometry.TYPES.SURFACE:
        case Geometry.TYPES.POLYGON:
            return true;
        }
        return false;
	}

	public void setFillColor(Color color) {
		/*
		 * Will apply the color to each layer
		 */
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setFillColor(color);
		}
	}

	public void setOutline(ILineSymbol outline) {
		if (layers != null && layers.length > 0) {
			for (int i = 0; i < layers.length - 1; i++) {
				layers[i].setOutline(null);
			}
			layers[layers.length - 1].setOutline(outline);
		}
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		for (int i = 0; (cancel == null || !cancel.isCanceled())
				&& layers != null && i < layers.length; i++) {
			layers[i].draw(g, affineTransform, geom, feature, cancel);
		}
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].drawInsideRectangle(g, scaleInstance, r, properties);
		}
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		float[] myDistances = new float[] {0,0};
		distances[0] = 0;
		distances[1] = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].getPixExtentPlus(geom, myDistances, viewPort, dpi);
			distances[0] = Math.max(myDistances[0], distances[0]);
			distances[1] = Math.max(myDistances[1], distances[1]);
		}
	}

	public ISymbol getSymbolForSelection() {
	    // TODO: revisar
        Color c = MapContext.getSelectionColor();
        c = new Color(
                c.getRed(),
                c.getGreen(),
                c.getBlue(),
                (int) (c.getAlpha()));//*OPACITY_SELECTION_FACTOR));
        if (selectionSymbol == null) {
		    	MultiLayerFillSymbol selectionSymbol =
		    	    new MultiLayerFillSymbol();
			selectionSymbol.setDescription(getDescription());
			for (int i = 0; layers != null && i < layers.length; i++) {
				selectionSymbol.addLayer(layers[i].getSymbolForSelection());
			}
//			SimpleFillSymbol selLayer = new SimpleFillSymbol();
//
//			selLayer.setFillColor(c);
//			selLayer.setOutline(getOutline());
//			selectionSymbol.addLayer(selLayer);
			setSymbolForSelection(selectionSymbol);
		} else {
          for (int i = 0; i < selectionSymbol.getLayerCount(); i++) {
                selectionSymbol.setLayer(i, selectionSymbol.getLayer(i).getSymbolForSelection());
//                if (i==selectionSymbol.getLayerCount()-1){
//                    ((SimpleFillSymbol)selectionSymbol.getLayer(i)).setFillColor(c);
//                }
            }
        }
		return selectionSymbol;

	}

	public int getSymbolType() {
		return Geometry.TYPES.SURFACE;
	}

//	public void setPrintingProperties(PrintAttributes printProperties) {
//		// TODO Implement it
//		throw new Error("Not yet implemented!");
//
//	}

//	public String getClassName() {
//		return getClass().getName();
//	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].print(g, at, geom, properties);
		}
	}

	public void setLayer(int index, ISymbol layer) throws IndexOutOfBoundsException {
		layers[index] = (IFillSymbol) layer;
	}

	public void swapLayers(int index1, int index2) {
		ISymbol aux1 = getLayer(index1), aux2 = getLayer(index2);
		layers[index2] = (IFillSymbol) aux1;
		layers[index1] = (IFillSymbol) aux2;
	}

	public ISymbol getLayer(int layerIndex) {
//		try{
			return layers[layerIndex];
//		} catch (Exception e) {
//			return null;
//		}
	}

	public int getLayerCount() {
		return layers.length;
	}

	public void addLayer(ISymbol newLayer) {
		addLayer(newLayer, layers.length);
	}

	public void addLayer(ISymbol newLayer, int layerIndex) throws IndexOutOfBoundsException {
		if (newLayer == null ) {
			/*|| newLayer instanceof ILabelStyle)*/ return; // null or symbols that are styles are not allowed
		}

		selectionSymbol = null; /* forces the selection symbol to be re-created
		 						 * next time it is required
		 						 */
		if (layerIndex < 0 || layers.length < layerIndex) {
			throw new IndexOutOfBoundsException(layerIndex+" < 0 or "+layerIndex+" > "+layers.length);
		}
		List<ISymbol> newLayers = new ArrayList<ISymbol>();
		for (int i = 0; i < layers.length; i++) {
			newLayers.add(layers[i]);
		}
		try {
			newLayers.add(layerIndex, newLayer);
			layers = (IFillSymbol[]) newLayers.toArray(new IFillSymbol[0]);
		} catch (ArrayStoreException asEx) {
			throw new ClassCastException(newLayer.getClass().getName() + " is not an IFillSymbol");
		}
	}

	public boolean removeLayer(ISymbol layer) {

		int capacity = 0;
		capacity = layers.length;
		List<IFillSymbol> lst = new ArrayList<IFillSymbol>(capacity);
		for (int i = 0; layers != null && i < capacity; i++) {
			lst.add(layers[i]);
		}
		boolean contains = lst.remove(layer);
		layers = (IFillSymbol[])lst.toArray(new IFillSymbol[0]);
		return contains;
	}

	public void setUnit(int unitIndex) {
		super.setUnit(unitIndex);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setUnit(unitIndex);
		}
	}

	public void setReferenceSystem(int system) {
		super.setReferenceSystem(system);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].setReferenceSystem(system);
		}
	}

	/**
	 *Returns the transparency of the multi layer fill symbol created
	 */
	public int getFillAlpha() {
		// will compute the acumulated opacity
		double myAlpha = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			double layerAlpha = layers[i].getFillAlpha()/255D;
			myAlpha += (1-myAlpha)*layerAlpha;
		}
		int result = (int) Math.round(myAlpha * 255);
		return (result>255) ? 255 : result;
	}


	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double size = 0;
		for (int i = 0; layers != null && i < layers.length; i++) {
			size = Math.max(size, layers[i].toCartographicSize(viewPort, dpi, geom));
		}
		return size;
	}

	public Object clone() throws CloneNotSupportedException {
		MultiLayerFillSymbol copy = (MultiLayerFillSymbol) super.clone();

		// Clone layers
		if (layers != null) {
			IFillSymbol[] layersCopy = new IFillSymbol[layers.length];
			for (int i = 0; i < layers.length; i++) {
				layersCopy[i] = (IFillSymbol) layers[i].clone();
			}
			copy.layers = layersCopy;
		}

		// Clone selection
		if (selectionSymbol != null) {
			copy.selectionSymbol = (MultiLayerFillSymbol) selectionSymbol
					.clone();
		}

		return copy;
	}

	private void setSymbolForSelection(MultiLayerFillSymbol symbolForSelection) {
		this.selectionSymbol = symbolForSelection;
	}

	@SuppressWarnings("unchecked")
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);
		LOG.warn("FIXME, fix implement of loadFromState");
		// Set own properties
		// setSymbolForSelection((MultiLayerFillSymbol)
		// state.get(FIELD_SYMBOL_FOR_SELECTION));
		List layers = state.getList(FIELD_LAYERS);
		if (layers != null) {
			for (int i = 0; i < layers.size(); i++) {
				addLayer((ISymbol) layers.get(i));
			}
		}

	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);
		LOG.warn("FIXME, fix implement of saveToState");
		// Save own properties

		// Don't use the getSymbolForSelection method, as it will create it
		// if it does not exist, and persistence will enter an infinite loop
		// state.set(FIELD_SYMBOL_FOR_SELECTION, selectionSymbol);
		state.set(FIELD_LAYERS, layers);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(MULTILAYER_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						MultiLayerFillSymbol.class,
						MULTILAYER_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						MULTILAYER_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);

				// Extend the FillSymbol base definition
				definition.extend(manager.getDefinition(FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Selection Symbol
				// definition.addDynFieldSingle(FIELD_SYMBOL_FOR_SELECTION).setType(
				// DataTypes.OBJECT);

				// Layers
				definition.addDynFieldList(FIELD_LAYERS).setClassOfItems(IFillSymbol.class);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

			shapeTypes = new int[] { Geometry.TYPES.SURFACE,
					Geometry.TYPES.CIRCLE, Geometry.TYPES.ELLIPSE,
					Geometry.TYPES.MULTISURFACE };
			manager.registerMultiLayerSymbol(IFillSymbol.SYMBOL_NAME,
					shapeTypes, MultiLayerFillSymbol.class);

			return Boolean.TRUE;
		}

	}

}