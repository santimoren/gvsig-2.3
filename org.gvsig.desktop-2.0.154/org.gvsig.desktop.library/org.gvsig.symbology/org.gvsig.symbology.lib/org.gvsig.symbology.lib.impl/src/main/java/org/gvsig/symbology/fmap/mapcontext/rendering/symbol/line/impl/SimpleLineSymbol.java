/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.impl;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ISimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.Line2DOffset;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * SimpleLineSymbol is the most basic symbol for the representation of line objects.
 * Allows to define the width of the line, the color and the drawn pattern.
 *
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SimpleLineSymbol extends AbstractLineSymbol implements ISimpleLineSymbol {

	private static final Logger LOG = LoggerFactory.getLogger(SimpleLineSymbol.class);
    public static final String SIMPLE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME = "SimpleLineSymbol";

    private static final String SELECTION_SYMBOL = "symbolForSelection";

	SimpleLineSymbol symbolForSelection;
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();

	public SimpleLineSymbol() {
		super();
		setLineWidth(1d);
	}

	public ISymbol getSymbolForSelection() {
		if (symbolForSelection == null) {
			symbolForSelection = (SimpleLineSymbol) cloneForSelection();
		}else{
		    symbolForSelection.setColor(MapContext.getSelectionColor());
		}
		return symbolForSelection;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {

		Geometry geomToDraw = geom;
		g.setStroke(getLineStyle().getStroke());

		if (getLineStyle().getOffset() != 0) {
			double offset = getLineStyle().getOffset();
			try {
				geomToDraw =
						geomManager.createSurface(Line2DOffset.offsetLine(
								geomToDraw.getShape(), offset), SUBTYPES.GEOM2D);
			} catch (CreateGeometryException e) {
				LOG.error("Creating a Surface", e);
			}
		}
		g.setColor(getColor());
		g.draw(geomToDraw.getShape(affineTransform));

		ArrowDecoratorStyle arrowDecorator = (ArrowDecoratorStyle) getLineStyle().getArrowDecorator();

		if (arrowDecorator != null) {
			try {
				arrowDecorator.draw(g, affineTransform, geomToDraw, feature);
			} catch (CreateGeometryException e) {
				LOG.error("Error creating a geometry");
			}
		}
	}

	public int getOnePointRgb() {
		return getColor().getRGB();
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		g.setColor(getColor());
		g.setStroke(getLineStyle().getStroke());
		super.drawInsideRectangle(g, scaleInstance, r, properties);
	}

	public void setLineWidth(double width) {
		getLineStyle().setLineWidth((float) width);
	}

	public double getLineWidth() {
		return getLineStyle().getLineWidth();
	}

	public Object clone() throws CloneNotSupportedException {
		SimpleLineSymbol copy = (SimpleLineSymbol) super.clone();

		if (symbolForSelection != null) {
			copy.symbolForSelection = (SimpleLineSymbol) symbolForSelection
					.clone();
		}

		return copy;
	}

    public void loadFromState(PersistentState state) throws PersistenceException {
        // Set parent style properties
        super.loadFromState(state);

        this.symbolForSelection = (SimpleLineSymbol) state.get(SELECTION_SYMBOL);
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent fill symbol properties
        super.saveToState(state);

        // Save own properties
        if (this.symbolForSelection != null){
            state.set(SELECTION_SYMBOL, this.getSymbolForSelection());
        }
    }



	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleLineSymbol.class,
						SIMPLE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the LineSymbol base definition
				definition.extend(manager.getDefinition(LINE_SYMBOL_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldObject(SELECTION_SYMBOL).setClassOfValue(SimpleLineSymbol.class);

			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

	        shapeTypes = new int[] { Geometry.TYPES.CURVE, Geometry.TYPES.ARC,
	                Geometry.TYPES.MULTICURVE, Geometry.TYPES.CIRCUMFERENCE,
	                Geometry.TYPES.PERIELLIPSE, Geometry.TYPES.SPLINE,
	                Geometry.TYPES.LINE, Geometry.TYPES.MULTILINE};
	        manager.registerSymbol(ILineSymbol.SYMBOL_NAME,
	            shapeTypes,
	            SimpleLineSymbol.class);

			return Boolean.TRUE;
		}

	}

}