/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.rendering.legend.IClassifiedVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendClearEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Abstract class that implements the interface for legends composed by
 * classified symbols.It will have two methods that will be executed depending
 * on the action that had been done with the legend (addition of a new symbol or
 * clear the legend).
 * 
 * @author 2008- pepe vidal salvador - jose.vidal.salvador@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractClassifiedVectorLegend extends
		AbstractVectorialLegend implements IClassifiedVectorLegend {

	public static final String CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME =
			"ClassifiedVectorLegend";

	private static final String FIELD_FIELD_NAMES = "fieldNames";
	private static final String FIELD_FIELD_TYPES = "fieldTypes";

	private String[] fieldNames;
	private int[] fieldTypes;

	/**
	 * Looks for a change in a classified symbol of a legend. To perform it, the
	 * Array of LegendListeners is scaned and when the corresponding listener is
	 * true, the method is invoked and the change will be done.
	 * 
	 * @param event
	 */
	public void fireClassifiedSymbolChangeEvent(SymbolLegendEvent event) {
		for (int i = 0; i < getListeners().length; i++) {
			getListeners()[i].symbolChanged(event);
		}
	}

	/**
	 * Looks for a change in a legend of classified symbols. In this case if the
	 * specific legend is cleared.
	 * 
	 * @param event
	 */
	public void fireLegendClearEvent(LegendClearEvent event) {
		for (int i = 0; i < getListeners().length; i++) {
			getListeners()[i].legendCleared(event);
		}
	}

	public String[] getClassifyingFieldNames() {
		return fieldNames;
	}

	public void setClassifyingFieldNames(String[] fieldNames) {
		this.fieldNames = fieldNames;
	}

	public int[] getClassifyingFieldTypes() {
		return fieldTypes;
	}

	public void setClassifyingFieldTypes(int[] fieldTypes) {
		this.fieldTypes = fieldTypes;
	}

	public boolean isSuitableForShapeType(int shapeType) {
		return getShapeType() == shapeType;
	}

	protected String[] getRequiredFeatureAttributeNames(
			FeatureStore featureStore) throws DataException {
		String[] requiredFieldNames = null;
		String[] classified = getClassifyingFieldNames();
		requiredFieldNames = new String[classified.length + 1];
		requiredFieldNames[0] =
				featureStore.getDefaultFeatureType()
						.getDefaultGeometryAttributeName();
		for (int i = 1; i < requiredFieldNames.length; i++) {
			requiredFieldNames[i] = classified[i - 1];
		}

		return requiredFieldNames;
	}

	public Object clone() throws CloneNotSupportedException {
		AbstractClassifiedVectorLegend clone =
				(AbstractClassifiedVectorLegend) super.clone();

		// Clone fieldNames
		clone.fieldNames = new String[fieldNames.length];
		System.arraycopy(fieldNames, 0, clone.fieldNames, 0, fieldNames.length);
		// Clone fieldTypes
		clone.fieldTypes = new int[fieldTypes.length];
		System.arraycopy(fieldTypes, 0, clone.fieldTypes, 0, fieldTypes.length);

		return clone;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent properties
		super.loadFromState(state);
		// Set own properties
		this.fieldNames = state.getStringArray(FIELD_FIELD_NAMES);
		this.fieldTypes = state.getIntArray(FIELD_FIELD_TYPES);
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_FIELD_NAMES, this.fieldNames);
		state.set(FIELD_FIELD_TYPES, this.fieldTypes);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractClassifiedVectorLegend.class,
						CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME,
						CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				// Extend the Vectorial Legend base definition
				definition.extend(manager.getDefinition(VECTORIAL_LEGEND_PERSISTENCE_DEFINITION_NAME));

				// Field names
				definition.addDynFieldArray(FIELD_FIELD_NAMES)
					.setClassOfItems(String.class);
				// Field types
				definition.addDynFieldArray(FIELD_FIELD_TYPES)
					.setClassOfItems(int.class);
			}
			return Boolean.TRUE;
		}
		
	}

}