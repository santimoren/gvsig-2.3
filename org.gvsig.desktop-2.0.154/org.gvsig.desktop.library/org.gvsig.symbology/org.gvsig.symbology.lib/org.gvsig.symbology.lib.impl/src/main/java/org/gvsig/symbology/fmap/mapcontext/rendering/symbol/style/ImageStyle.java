/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Controls the style of an image to be correctly painted. This class controls
 * aspects like the source path of the image, creates a rectangle to paint inside 
 * the image, draws the outline of the image and so on.
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class ImageStyle extends BackgroundFileStyle {
    public static final String IMAGE_STYLE_PERSISTENCE_DEFINITION_NAME = "ImageStyle";
    private static final String SOURCE = "source";

	private BufferedImage img;
	/**
	 * Creates a rectangle with the dimensions of the buffered image
	 * @return Rectangle
	 */
	public Rectangle getBounds() {
		if (img == null) return new Rectangle();
		return new Rectangle(new Dimension(img.getWidth(), img.getHeight()));
	}
	/**
	 * Defines the source (file) from where the buffered image will be taken.
	 * @param f,File
	 */
	public void setSource(URL url) throws IOException {
    	source = url;
		img = ImageIO.read(url);
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r, boolean keepAspectRatio) {
		if (img != null) {

			double xOffset = 0;
			double yOffset = 0;
			double xScale = 1;
			double yScale = 1;
			if (keepAspectRatio) {
				double scale;
				if (img.getWidth()>img.getHeight()) {
					scale = r.getWidth()/img.getWidth();
					yOffset = 0.5*(r.getHeight() - img.getHeight()*scale);
				} else {
					scale = r.getHeight()/img.getHeight();
					xOffset = 0.5*(r.getWidth() - img.getWidth()*scale);
				}
				xScale = yScale = scale;

			} else {
				xScale = r.getWidth()/img.getWidth();
				yScale = r.getHeight()/img.getHeight();
				yOffset = img.getHeight()*0.5*yScale ;

			}


			AffineTransform at = AffineTransform.getTranslateInstance(xOffset, yOffset);
			at.concatenate(AffineTransform.getScaleInstance(xScale, yScale));
			g.drawRenderedImage(img, at);
		}
	}

	public boolean isSuitableFor(ISymbol symbol) {
		// TODO Implement it
		throw new Error("Not yet implemented!");

	}

	public void drawOutline(Graphics2D g, Rectangle r) throws SymbolDrawingException {
		drawInsideRectangle(g, r);
	}

	public void loadFromState(PersistentState state)
	throws PersistenceException {
	    try {
        	String sourceSymbolInLibrary = state.getString(SOURCE_SYMBOL_IN_LIBRARY);
        	if (sourceSymbolInLibrary != null){
        		setSource(new URL(getSymbolLibraryURL().toString()+sourceSymbolInLibrary));
        	} else {
        		setSource(state.getURL(SOURCE));
        	}
        } catch (Exception e) {
            throw new PersistenceCantSetSourceException(e);
        }
	}

	public void saveToState(PersistentState state) throws PersistenceException {
    	if (isLibrarySymbol()){
    		state.set(SOURCE_SYMBOL_IN_LIBRARY, getSourceSymbolInLibrary());
    	} else {
    		state.setNull(SOURCE_SYMBOL_IN_LIBRARY);
    	}
        state.set(SOURCE, source);
	}
	
    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if( manager.getDefinition(IMAGE_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
                DynStruct definition = manager.addDefinition(
                    ImageStyle.class,
                    IMAGE_STYLE_PERSISTENCE_DEFINITION_NAME,
                    IMAGE_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
                    null, 
                    null
                );

                // Extend the Style base definition
                definition.extend(manager.getDefinition(BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldURL(SOURCE).setMandatory(true);
                definition.addDynFieldString(SOURCE_SYMBOL_IN_LIBRARY).setMandatory(false);
            }
            return Boolean.TRUE;
        }
    }
}


