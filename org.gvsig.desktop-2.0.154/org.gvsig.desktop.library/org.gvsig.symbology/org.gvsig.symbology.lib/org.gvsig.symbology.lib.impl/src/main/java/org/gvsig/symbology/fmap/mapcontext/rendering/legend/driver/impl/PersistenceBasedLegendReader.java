/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.driver.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.exceptions.IncompatibleLegendReadException;
import org.gvsig.fmap.mapcontext.exceptions.ReadLegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendReader;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.persistence.PersistenceManager;


/**
 * Legend reader that uses the standard persistence
 */
public class PersistenceBasedLegendReader implements ILegendReader {

    public ILegend read(File inFile, int geometryType)
        throws ReadLegendException, IncompatibleLegendReadException,
        IOException {
        
        InputStream istr = null;
        istr = new FileInputStream(inFile);
        ILegend resp = null;

        try {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            Object inobj = manager.getObject(istr);
            resp = (ILegend) inobj; 
        } catch (Exception exc) {
            throw new ReadLegendException(inFile, exc);
        }
        istr.close();
        
        if (resp instanceof IVectorLegend) {
            IVectorLegend vleg = (IVectorLegend) resp;
            
            GeometryType foundgt = null;
            try {
                foundgt = GeometryLocator.getGeometryManager().getGeometryType(
                vleg.getShapeType(), Geometry.SUBTYPES.GEOM2D);
            } catch (Exception e) {
                throw new IncompatibleLegendReadException(
                    inFile, geometryType, e);
            }
            
            if (geometryType != Geometry.TYPES.GEOMETRY &&
                !foundgt.isTypeOf(geometryType)) {
                
                throw new IncompatibleLegendReadException(
                    inFile,
                    geometryType,
                    new Exception("Geometry type found: " + vleg.getShapeType()));
            }
        }
        return resp;
    }

}
