/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.remoteclient.utils.Utilities;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * 
 * RemoteFileStyle.java
 *
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es Feb 13, 2008
 *
 */
public class RemoteFileStyle extends BackgroundFileStyle {
    public static final String REMOTE_FILE_STYLE_PERSISTENCE_DEFINITION_NAME = "RemoteFileStyle";
    private static final String SOURCE = "source";

    private BackgroundFileStyle bgStyle;
	
	@Override
	public Rectangle getBounds() {
		return bgStyle.getBounds();
	}

	@Override
	public void setSource(URL url) throws IOException {
		File f = Utilities.downloadFile(url, "remote_picture_symbol_style", null);
		source = url;
		bgStyle = BackgroundFileStyle.createStyleByURL(f.toURI().toURL());
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r, boolean keepAspectRatio) throws SymbolDrawingException {
		bgStyle.drawInsideRectangle(g, r, keepAspectRatio);
	}

	public void drawOutline(Graphics2D g, Rectangle r) throws SymbolDrawingException {
		bgStyle.drawOutline(g, r);
	}

	public boolean isSuitableFor(ISymbol symbol) {
		return bgStyle.isSuitableFor(symbol);
	}

	public String getClassName() {
		return getClass().getName();
	}

	   public void loadFromState(PersistentState state)
	    throws PersistenceException {
	        try {
	            setSource(state.getURL(SOURCE));
	        } catch (Exception e) {
	            throw new PersistenceCantSetSourceException(e);
	        }
	    }

	    public void saveToState(PersistentState state) throws PersistenceException {
	        state.set(SOURCE, source);
	    }
	    
	    public static class RegisterPersistence implements Callable {

	        public Object call() throws Exception {
	            PersistenceManager manager = ToolsLocator.getPersistenceManager();
	            if( manager.getDefinition(REMOTE_FILE_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
	            	DynStruct definition = manager.addDefinition(
	            			RemoteFileStyle.class,
	            			REMOTE_FILE_STYLE_PERSISTENCE_DEFINITION_NAME,
	            			REMOTE_FILE_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
	            			null, 
	            			null
	            	);

	                // Extend the Style base definition
	                definition.extend(manager.getDefinition(BACKGROUND_FILE_STYLE_PERSISTENCE_DEFINITION_NAME));

	                definition.addDynFieldURL(SOURCE);
	            }
	            return Boolean.TRUE;
	        }
	    }
}