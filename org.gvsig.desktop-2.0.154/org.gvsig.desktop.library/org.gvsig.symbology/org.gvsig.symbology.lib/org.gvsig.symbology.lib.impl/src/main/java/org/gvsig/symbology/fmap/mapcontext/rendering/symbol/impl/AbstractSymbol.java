/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl;

import java.awt.Rectangle;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol_v2;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class that implements the interface the interface for symbols.It is
 * considered as the father of all XXXSymbols and will implement all the methods
 * that these classes had not developed (and correspond with one of the methods
 * of AbstractSymbol class)
 * 
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractSymbol implements ISymbol_v2, CartographicSupport {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractSymbol.class);

	public static final String SYMBOL_PERSISTENCE_DEFINITION_NAME = "Symbol";
	
	private static final String FIELD_UNIT = "unit";

	private static final String FIELD_REFERENCE_SYSTEM = "referenceSystem";

	private static final String FIELD_IS_SHAPE_VISIBLE = "isShapeVisible";

	private static final String FIELD_DESCRIPTION = "description";


	private String desc;
        private String id; 
	private int unit;
	private int referenceSystem;

	private boolean isShapeVisible = true;

	public AbstractSymbol() {
		super();
		SymbolPreferences preferences =
				MapContextLocator.getSymbolManager().getSymbolPreferences();
		unit =
				preferences.getDefaultCartographicSupportMeasureUnit();
		referenceSystem =
				preferences.getDefaultCartographicSupportReferenceSystem();
	}

	public final void setDescription(String desc) {
		this.desc = desc;
	}

	public final String getDescription() {
		return desc;
	}

        /**
         * Set the id ob the symbol (the basename of file)
         * 
         * @param id 
         */
	public final void setID(String id) {
		this.id = id;
	}

        /**
         * Get the id ob the symbol (the basename of file)
         * 
         * @param id 
         */
	public final String getID() {
		return id;
	}

        
	/**
	 * @return
	 * @uml.property name="isShapeVisible"
	 */
	public final boolean isShapeVisible() {
		return isShapeVisible;
	}

	/**
	 * Sets this symbol to visible
	 * 
	 * @param isShapeVisible
	 */
	public final void setIsShapeVisible(boolean isShapeVisible) {
		this.isShapeVisible = isShapeVisible;
	}

	public void setUnit(int unitIndex) {
		this.unit = unitIndex;
	}

	public int getUnit() {
		return this.unit;
	}

	public int getReferenceSystem() {
		return this.referenceSystem;
	}

	public void setReferenceSystem(int system) {
		this.referenceSystem = system;

	}
	
	private boolean areEquals(Object a,Object b) {
		if( a==null ) {
			if( b==null) {
				return true;
			}
			return false;
		} else {
			if( b == null ) {
				return false;
			}
		}
		return a.equals(b);
	}

	public boolean equals(Object obj) {
		ISymbol other = null;
		
		if( obj==null ) {
			return false;
		}
		if( ! areEquals(obj.getClass(),this.getClass()) ) {
			// El try/catch y la salida al log es una medida
			// temporal hasta que averiguemos quien causa que
			// pase por aqui, ya que no parece razonable que 
			// desde gvSIG se invoque con algo que no sea un ISymbol.
			try {
				other = (ISymbol) obj;
			} catch(Exception ex) {
				LOG.warn("Suspicious comparison between ISymbol and '"+obj.getClass().getName()+"'.",ex);
			}
			return false;
		}
		
		other = (ISymbol) obj;
		
		if( ! areEquals(other.getColor(),this.getColor()) ) {
			return false;
		}
		if( other.getOnePointRgb() != this.getOnePointRgb()) {
			return false;
		}
		if( ! areEquals(other.getDescription(),this.getDescription()) ) {
			return false;
		}
		return true;
	}

	public boolean isOneDotOrPixel(Geometry geom,
			double[] positionOfDotOrPixel, ViewPort viewPort, int dpi) {

		int type = geom.getType();
		switch (type) {
		case Geometry.TYPES.NULL:
		case Geometry.TYPES.POINT:
		case Geometry.TYPES.MULTIPOINT:
			return false;
		default:
			org.gvsig.fmap.geom.primitive.Envelope geomBounds = geom
			.getEnvelope();

			double dist1Pixel = viewPort.getDist1pixel();

			float[] distances = new float[2];
			this.getPixExtentPlus(geom, distances, viewPort, dpi);

			boolean onePoint =
					(geomBounds.getLength(0) + distances[0] <= dist1Pixel && geomBounds
					.getLength(1)
					+ distances[1] <= dist1Pixel);

			if (onePoint) {
				Rectangle bounds = geom.getShape().getBounds();
				positionOfDotOrPixel[0] = bounds.x;
				positionOfDotOrPixel[1] = bounds.y;
			}
			return onePoint;
		}
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();	
	}

	protected ISymbol cloneForSelection() {
		try {
			ISymbol selectionSymbol = (ISymbol) clone();
			selectionSymbol.setColor(MapContext.getSelectionColor());
			if (getDescription() != null){
				selectionSymbol.setDescription(getDescription().concat(
				" version for selection"));
			}else{
				selectionSymbol.setDescription("version for selection");
			}
			return selectionSymbol;
		} catch (CloneNotSupportedException e) {
			LOG.error(
					"Error creating the selection symbol for the symbol "
					+ this, e);
		}
		return null;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setDescription(state.getString(FIELD_DESCRIPTION));
		setIsShapeVisible(state.getBoolean(FIELD_IS_SHAPE_VISIBLE));
		setReferenceSystem(state.getInt(FIELD_REFERENCE_SYSTEM));
		setUnit(state.getInt(FIELD_UNIT));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DESCRIPTION, getDescription());
		state.set(FIELD_IS_SHAPE_VISIBLE, isShapeVisible());
		state.set(FIELD_REFERENCE_SYSTEM, getReferenceSystem());
		state.set(FIELD_UNIT, getUnit());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractSymbol.class, 
						SYMBOL_PERSISTENCE_DEFINITION_NAME, 
						SYMBOL_PERSISTENCE_DEFINITION_NAME+" persistence definition", 
						null, 
						null
				);
				// Description
				definition.addDynFieldString(FIELD_DESCRIPTION);
				// Is Shape Visible
				definition.addDynFieldBoolean(FIELD_IS_SHAPE_VISIBLE).setMandatory(true);
				// Reference system
				definition.addDynFieldInt(FIELD_REFERENCE_SYSTEM).setMandatory(true);
				// Unit
				definition.addDynFieldInt(FIELD_UNIT).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}

	
}