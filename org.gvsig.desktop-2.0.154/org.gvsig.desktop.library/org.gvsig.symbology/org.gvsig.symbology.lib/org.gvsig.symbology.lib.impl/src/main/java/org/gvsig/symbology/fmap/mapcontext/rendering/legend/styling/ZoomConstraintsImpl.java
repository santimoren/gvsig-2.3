/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: ZoomConstraintsImpl.java 10671 2007-03-09 08:33:43Z jaume $
* $Log$
* Revision 1.2  2007-03-09 08:33:43  jaume
* *** empty log message ***
*
* Revision 1.1.2.2  2007/02/15 16:23:44  jaume
* *** empty log message ***
*
* Revision 1.1.2.1  2007/02/09 07:47:05  jaume
* Isymbol moved
*
* Revision 1.1.2.1  2007/01/30 18:10:45  jaume
* start commiting labeling stuff
*
*
*/
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;


import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * @author  jaume dominguez faus - jaume.dominguez@iver.es
 */
public class ZoomConstraintsImpl implements IZoomConstraints {
	private static final String FIELD_MIN_SCALE = "minScale";
	private static final String FIELD_MAX_SCALE = "maxScale";
	private static final String FIELD_MODE = "mode";
	private static final String ZOOM_CONSTRAINTS_IMPL_PERSISTENCE_DEFINITION_NAME =
			"ZoomConstraintsImpl";
	/**
	 * @uml.property  name="mode"
	 */
	private int mode = DEFINED_BY_THE_LAYER;
	/**
	 * @uml.property  name="minScale"
	 */
	private long minScale = -1;
	/**
	 * @uml.property  name="maxScale"
	 */
	private long maxScale = -1;

	/**
	 * @param mode
	 * @uml.property  name="mode"
	 */
	public void setMode(int mode) {
		if (mode != DEFINED_BY_THE_LAYER &&
			mode != DEFINED_BY_THE_USER)
			throw new IllegalArgumentException();
		this.mode = mode;
	}

	/**
	 * @return
	 * @uml.property  name="maxScale"
	 */
	public long getMaxScale() {
		return maxScale;
	}

	/**
	 * @param maxScale
	 * @uml.property  name="maxScale"
	 */
	public void setMaxScale(long maxScale) {
		this.maxScale = maxScale;
	}

	/**
	 * @return
	 * @uml.property  name="minScale"
	 */
	public long getMinScale() {
		return minScale;
	}

	/**
	 * @param minScale
	 * @uml.property  name="minScale"
	 */
	public void setMinScale(long minScale) {
		this.minScale = minScale;
	}

	public boolean isUserDefined() {
		return mode == DEFINED_BY_THE_USER;
	}

	public boolean isLayerDefined() {
		return mode == DEFINED_BY_THE_LAYER;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setMode(state.getInt(FIELD_MODE));
		setMaxScale(state.getLong(FIELD_MAX_SCALE));
		setMinScale(state.getLong(FIELD_MIN_SCALE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_MODE, mode);
		state.set(FIELD_MAX_SCALE, getMaxScale());
		state.set(FIELD_MIN_SCALE, getMinScale());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(ZOOM_CONSTRAINTS_IMPL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						ZoomConstraintsImpl.class,
						ZOOM_CONSTRAINTS_IMPL_PERSISTENCE_DEFINITION_NAME,
						ZOOM_CONSTRAINTS_IMPL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				definition.addDynFieldInt(FIELD_MODE).setMandatory(true);
				definition.addDynFieldLong(FIELD_MIN_SCALE).setMandatory(true);
				definition.addDynFieldLong(FIELD_MAX_SCALE).setMandatory(true);
			}
			return Boolean.TRUE;
		}
		
	}


	
}