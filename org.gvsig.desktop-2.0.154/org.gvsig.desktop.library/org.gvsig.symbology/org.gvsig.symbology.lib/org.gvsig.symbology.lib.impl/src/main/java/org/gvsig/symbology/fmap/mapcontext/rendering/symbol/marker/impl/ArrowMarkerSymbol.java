/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IArrowMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class ArrowMarkerSymbol extends AbstractMarkerSymbol implements CartographicSupport, IArrowMarkerSymbol {

	public static final String ARROW_MARKER_SYMBOL_DYNCLASS_NAME = "ArrowMarkerSymbol";

	private static final String FIELD_SHARPNESS = "sharpness";

	private ArrowMarkerSymbol symSel;
	private double sharpeness;

	public ISymbol getSymbolForSelection() {
		if (symSel == null) {
			//symSel = new ArrowMarkerSymbol();
		    symSel = (ArrowMarkerSymbol)this.cloneForSelection();
			symSel.setColor(MapContext.getSelectionColor());
		}else{
		    symSel.setColor(MapContext.getSelectionColor());
		}

		return symSel;
	}

	public String getClassName() {
		return getClass().getName();
	}

	/**
	 * To get the sharpeness attribute.It will determine the final form of the arrow
	 * @return
	 */
	public double getSharpness() {
		return sharpeness;
	}

	/**
	 * To set the sharpeness.It will determine the final form of the arrow
	 * @param sharpeness, the value of the arrow's edge angle IN RADIANS
	 * @see #getSharpeness()
	 */
	public void setSharpness(double sharpeness) {
		this.sharpeness = sharpeness;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {

		if (geom == null) {
			return;
		}
		Point p = (Point) geom;
		double radian_half_sharpeness = Math.toRadians(getSharpness()*.5); //

		double size = getSize();
		double halfHeight = size * Math.tan(radian_half_sharpeness);
		double theta = getRotation();

		g.setColor(getColor());

		g.setStroke(new BasicStroke());

		AffineTransform newAf = (AffineTransform)affineTransform.clone();

		newAf.translate(p.getX(), p.getY());
		newAf.rotate(theta);

		GeneralPathX gp = new GeneralPathX();
		gp.moveTo(0, 0);
		gp.lineTo(size, -halfHeight);
		gp.lineTo(size, halfHeight);
		gp.closePath();

		gp.transform(newAf);

		g.fill(gp);
	}

	public Object clone() throws CloneNotSupportedException {
		ArrowMarkerSymbol copy  = (ArrowMarkerSymbol) super.clone();

		// Clone the selection symbol
		if (symSel != null) {
			copy.symSel = (ArrowMarkerSymbol) symSel.clone();
		}
		return copy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);

		// Set own properties
		setSharpness(state.getDouble(FIELD_SHARPNESS));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_SHARPNESS, getSharpness());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(ARROW_MARKER_SYMBOL_DYNCLASS_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						ArrowMarkerSymbol.class,
						ARROW_MARKER_SYMBOL_DYNCLASS_NAME,
						ARROW_MARKER_SYMBOL_DYNCLASS_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the FillSymbol base definition
				definition.extend(manager.getDefinition(MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Sharpeness
				definition.addDynFieldDouble(FIELD_SHARPNESS).setMandatory(true);
			}
			return Boolean.TRUE;
		}

	}

}