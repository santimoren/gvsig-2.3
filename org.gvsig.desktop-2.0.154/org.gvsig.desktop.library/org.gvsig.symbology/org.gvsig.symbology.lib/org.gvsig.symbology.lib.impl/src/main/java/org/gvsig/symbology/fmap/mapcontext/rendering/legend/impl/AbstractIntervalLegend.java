/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import javax.swing.JOptionPane;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.exceptions.LegendLayerException;
import org.gvsig.fmap.mapcontext.rendering.legend.IInterval;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialIntervalLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.IMultiShapeSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractIntervalLegend extends
		AbstractClassifiedVectorLegend implements IVectorialIntervalLegend {

	public static final String INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME = "IntervalLegend";

	private static final String FIELD_KEYS = "keys";
	private static final String FIELD_INTERVAL_TYPE = "intervalType";
	private static final String FIELD_USE_DEFAULT_SYMBOL = "useDefaultSymbol";
	private static final String FIELD_SYMBOLS = "symbols";
	
	protected List<Object> keys = new ArrayList<Object>(); // <Object>
	// protected int index = 0;
	// protected int fieldId;
	protected ISymbol defaultSymbol;
	// protected FeatureStore featureStore;
	protected int intervalType = NATURAL_INTERVALS;
	protected boolean useDefaultSymbol = false;

	// private ISymbol nullIntervalSymbol = null;

	public static final int EQUAL_INTERVALS = 0;
	public static final int NATURAL_INTERVALS = 1;
	public static final int QUANTILE_INTERVALS = 2;
	protected Map<Object, ISymbol> symbols = createSymbolMap();
	final static private Logger logger = LoggerFactory.getLogger(AbstractIntervalLegend.class);
	
	public void addSymbol(Object key, ISymbol symbol) {
		if (key == null) {
		    // why did we use this?
			// nullIntervalSymbol = symbol;
		    logger.info(
		        "Warning: tried to add symbol with null key.",
		        new Exception("Key is NULL"));
		} else {
			symbols.put(key, symbol);
			keys.add(key);
			fireClassifiedSymbolChangeEvent(new SymbolLegendEvent(null, symbol));
		}
	}

	/*
	 * @see com.iver.cit.gvsig.fmap.rendering.IVectorialLegend#getSymbol(int)
	 */
	public ISymbol getSymbol(FeatureReference featureID)
			throws MapContextException, DataException {
		return getSymbolByFeature(featureID.getFeature());
		
//		Object val = featureStore.getFeatureByReference(featureID).get(getClassifyingFieldNames()[0]);
//		IInterval interval = getInterval(val);
//		ISymbol theSymbol = getSymbolByInterval(interval);
//
//		if (theSymbol != null) {
//			return theSymbol;
//		} else if (useDefaultSymbol) {
//			return getDefaultSymbol();
//		}
//		return null;

	}

	public ISymbol getSymbolByFeature(Feature feat) throws MapContextException {
		Object val = feat.get(getClassifyingFieldNames()[0]);
		IInterval interval = getInterval(val);
		ISymbol theSymbol = getSymbolByInterval(interval);

		if (theSymbol != null) {
			return theSymbol;
		} else if (useDefaultSymbol) {
			return getDefaultSymbol();
		}
		
		return null;
//		return theSymbol;
	}


	public IInterval[] calculateIntervals(FeatureStore featureStore,
			String fieldName, int numIntervalos, int shapeType)
	throws DataException {
		logger.debug("elRs.start()");
//		recordSet.start();

//		int idField = -1;
		FeatureSet set = null;
		DisposableIterator iterator = null;
		try {

		setClassifyingFieldNames(new String[] {fieldName});

		String[] fieldNames = getClassifyingFieldNames();
		FeatureQuery featureQuery=featureStore.createFeatureQuery();
		featureQuery.setAttributeNames(fieldNames);

		set = featureStore.getFeatureSet(featureQuery);
		iterator = set.fastIterator();


//		for (int i = 0; i < recordSet.getFieldCount(); i++) {
//			String nomAux = recordSet.getFieldName(i).trim();
//
//			if (fieldNames[0].compareToIgnoreCase(nomAux) == 0) {
//				idField = i;
//
//				break;
//			}
//		}

//		if (idField == -1) {
//			logger.error("Campo no reconocido " + fieldNames);
//
//			return null;
//		}

		double minValue = Double.MAX_VALUE;
		double maxValue = Double.NEGATIVE_INFINITY;

		IVectorialIntervalLegend auxLegend = (IVectorialIntervalLegend) MapContextLocator
					.getMapContextManager().createLegend("VectorialInterval");
		auxLegend.setShapeType(shapeType);

		Object clave;

		while (iterator.hasNext()) {
			Feature feature = (Feature) iterator.next();

//		for (int j = 0; j < recordSet.getRowCount(); j++) {
			clave = feature.get(fieldName);

			IInterval interval = auxLegend.getInterval(clave);

			////Comprobar que no esta repetido y no hace falta introducir en el hashtable el campo junto con el simbolo.
			if (auxLegend.getSymbolByInterval(interval) == null) {
				//si no esta creado el simbolo se crea
				double valor = 0;


				if (clave instanceof Number) {
					valor=((Number)clave).doubleValue();
				}else if (clave instanceof Date) {
					//TODO POR IMPLEMENTAR
					///valorDate = elRs.getFieldValueAsDate(idField);
					///if (valorDate.before(minValueDate)) minValueDate = valorDate;
					///if (valorDate.after(maxValueDate)) maxValueDate = valorDate;
//				} else if (clave instanceof NullValue) {
//					continue;
				}

				if (valor < minValue) {
					minValue = valor;
				}

				if (valor > maxValue) {
					maxValue = valor;
				}
			}
		}

			IInterval[] intervalArray = null;
		switch (getIntervalType()) {
		case VectorialIntervalLegend.EQUAL_INTERVALS:
			intervalArray = calculateEqualIntervals(numIntervalos,
					minValue, maxValue, fieldName);

			break;

		case VectorialIntervalLegend.NATURAL_INTERVALS:
			intervalArray = calculateNaturalIntervals(featureStore, numIntervalos,
					minValue, maxValue, fieldName);

			break;

		case VectorialIntervalLegend.QUANTILE_INTERVALS:
			intervalArray = calculateQuantileIntervals(featureStore, numIntervalos,
					minValue, maxValue, fieldName);

			break;
		}
//		recordSet.stop();
		return intervalArray;
		} finally {
			if (iterator != null) {
				iterator.dispose();
			}
			if (set != null) {
				set.dispose();
			}
		}
	}


    /**
     * EQUAL INTERVAL Devuelve un Array con el n�mero de intervalos que se
     * quieren crear. Los intervalos se crean con un tama�o igual entre ellos.
     * @param numIntervals n�mero de intervalos
     * @param minValue Valor m�nimo.
     * @param maxValue Valor m�ximo.
     * @param fieldName Nombre del campo
     *
     * @return Array con los intervalos.
     */
	private IInterval[] calculateEqualIntervals(int numIntervals,
			double minValue,
        double maxValue, String fieldName) {
		IInterval[] theIntervalArray = new IInterval[numIntervals];
        double step = (maxValue - minValue) / numIntervals;

        if (numIntervals > 1) {
            theIntervalArray[0] = new FInterval(minValue, minValue + step);

            for (int i = 1; i < (numIntervals - 1); i++) {
                theIntervalArray[i] = new FInterval(minValue + (i * step) +
                        0.01, minValue + ((i + 1) * step));
            }

            theIntervalArray[numIntervals - 1] = new FInterval(minValue +
                    ((numIntervals - 1) * step) + 0.01, maxValue);
        } else {
            theIntervalArray[0] = new FInterval(minValue, maxValue);
        }

        return theIntervalArray;
    }

    /**
     * NATURAL INTERVAL Devuelve un Array con el n�mero de intervalos que se
     * quieren crear. Los intervalos se distribuyen de forma natural.
     *
     * @param numIntervals n�mero de intervalos
     * @param minValue Valor m�nimo.
     * @param maxValue Valor m�ximo.
     * @param fieldName Nombre del campo
     *
     * @return Array con los intervalos.
     * @throws DataException 
     * @throws LegendLayerException
     */
	private IInterval[] calculateNaturalIntervals(FeatureStore featureStore,
			int numIntervals, double minValue,
        double maxValue, String fieldName) throws DataException {
        NaturalIntervalGenerator intervalGenerator = new NaturalIntervalGenerator(
        		featureStore, fieldName, numIntervals);

        intervalGenerator.generarIntervalos();

        int numIntervalsGen = intervalGenerator.getNumIntervals() - 1;

        if (numIntervalsGen == -1) {
            //TODO cuando no puede calcular los intervalos.
            numIntervalsGen = 1;
        }

        FInterval[] theIntervalArray = new FInterval[numIntervalsGen];

        if (numIntervalsGen > 1) {
            theIntervalArray[0] = new FInterval(minValue,
                    intervalGenerator.getValorRuptura(0));

            for (int i = 1; i < (numIntervalsGen - 1); i++) {
                theIntervalArray[i] = new FInterval(intervalGenerator.getValInit(i -
                            1), intervalGenerator.getValorRuptura(i));
            }

            theIntervalArray[numIntervalsGen - 1] = new FInterval(intervalGenerator.getValInit(numIntervalsGen -
                        2), maxValue);
        } else {
            theIntervalArray[numIntervalsGen - 1] = new FInterval(minValue,
                    maxValue);
        }

        return theIntervalArray;
    }

    /**
     * QUANTILE INTERVAL Devuelve un Array con el n�mero de intervalos que se
     * quieren crear. Los intervalos se distribuyen de forma quantile.
     * @param recordSet
     *
     * @param numIntervals n�mero de intervalos
     * @param minValue Valor m�nimo.
     * @param maxValue Valor m�ximo.
     * @param fieldName Nombre del campo
     *
     * @return Array con los intervalos.
     * @throws LegendLayerException
     */
	private IInterval[] calculateQuantileIntervals(FeatureStore featureStore,
			int numIntervals, double minValue,
        double maxValue, String fieldName) throws DataException {
        QuantileIntervalGenerator intervalGenerator = new QuantileIntervalGenerator(
        		featureStore, fieldName, numIntervals);

        intervalGenerator.generarIntervalos();

        int numIntervalsGen = intervalGenerator.getNumIntervalGen();
        FInterval[] theIntervalArray = new FInterval[numIntervalsGen];

        if (intervalGenerator.getNumIntervalGen() > 1) {
            theIntervalArray[0] = new FInterval(minValue,
                    intervalGenerator.getValRuptura(0));

            for (int i = 1; i < (numIntervalsGen - 1); i++) {
                theIntervalArray[i] = new FInterval(intervalGenerator.getValInit(i -
                            1), intervalGenerator.getValRuptura(i));
            }

            theIntervalArray[numIntervalsGen - 1] = new FInterval(intervalGenerator.getValInit(numIntervalsGen -
                        2), maxValue);
        } else {
            theIntervalArray[numIntervalsGen - 1] = new FInterval(minValue,
                    maxValue);
        }

        return theIntervalArray;
    }

    public ISymbol getSymbolByInterval(IInterval key) {

		if (key == null){
			if (isUseDefaultSymbol()) {
				return defaultSymbol;
			}
			return null;
		}
		if (symbols.containsKey(key)) {
			return (ISymbol) symbols.get(key);
		}

		if (isUseDefaultSymbol()) {
			return defaultSymbol;
		}

		return null;
	}


	public String[] getDescriptions() {
		String[] descriptions = new String[symbols.size()];
		ISymbol[] auxSym = getSymbols();

		for (int i = 0; i < descriptions.length; i++) {
			descriptions[i] = auxSym[i].getDescription();
		}

		return descriptions;
	}


	public Object[] getValues() {
		return symbols.keySet().toArray();
	}

	public void clear() {
		// index = 0;
		keys.clear();
		symbols.clear();
	}

	public ISymbol[] getSymbols() {
		ISymbol[] symbolList;
        symbolList = new ISymbol[symbols.size()];
        return (ISymbol[]) symbols.values().toArray(symbolList);
	}

	public void setDefaultSymbol(ISymbol s) {
	    ISymbol old = defaultSymbol;
		if (s == null) {
			throw new NullPointerException("Default symbol cannot be null");
		}
		defaultSymbol = s;
		fireDefaultSymbolChangedEvent(new SymbolLegendEvent(old, defaultSymbol));
	}

        @Override
	public ISymbol getDefaultSymbol() {
//		NullIntervalValue niv=new NullIntervalValue();
//		if (symbols.containsKey(niv)) {
//			return (ISymbol)symbols.get(niv);
//		}

		if(defaultSymbol==null) {
			defaultSymbol = getSymbolManager().createSymbol(getShapeType());
		}
		return defaultSymbol;
	}



//    public void setFeatureStore(FeatureStore featureStore)
//        throws DataException {
//    		/*
//    		 * when we move definitely to feature iterators this
//    		 * method
//    		 */
//    		this.featureStore = featureStore;
////    		fieldId = ((FeatureType)featureStore.getFeatureTypes().get(0)).getIndex(fieldNames[0]);
//    }


	public IInterval getInterval(Object v) {
		if (v != null ) {
			for (int i = 0; i < keys.size(); i++) {
			    
			    Object obj_key = keys.get(i);
			    if (obj_key instanceof IInterval) {
	                if (((IInterval) obj_key).isInInterval(v)) {
	                    return (IInterval) obj_key;
	                }
			    }
			}
		}

		return null;
	}

	public void setIntervalType(int intervalType) {
		this.intervalType = intervalType;
	}


	public int getIntervalType() {
		return intervalType;
	}

	public void useDefaultSymbol(boolean b) {
		useDefaultSymbol = b;
	}


	public boolean isUseDefaultSymbol() {
		return useDefaultSymbol;
	}


	public void delSymbol(Object obj) {
		keys.remove(obj);
		symbols.remove(obj);
		fireClassifiedSymbolChangeEvent(
				new SymbolLegendEvent(
						(ISymbol)symbols.remove(obj),
						null));
	}


	public void replace(ISymbol oldSymbol, ISymbol newSymbol) {
		if (symbols.containsValue(oldSymbol)) {
		    
			Iterator<Object> it = symbols.keySet().iterator();
			List key_list = new ArrayList();
			
			while (it.hasNext()) {
				Object key = it.next();
				if (symbols.get(key).equals(oldSymbol)) {
				    key_list.add(key);
				    // symbols.remove(key);
				}
			}
			
			for (int i=0; i<key_list.size(); i++) {
			    Object k = key_list.get(i);
			    symbols.put(k, newSymbol);
			}
            fireClassifiedSymbolChangeEvent(
                new SymbolLegendEvent(oldSymbol, newSymbol));

		}
	}

	public Object clone() throws CloneNotSupportedException {
		AbstractIntervalLegend clone = (AbstractIntervalLegend) super.clone();

		// Clone default symbol
		if (defaultSymbol != null) {
			clone.defaultSymbol = (ISymbol) defaultSymbol.clone();
		}
		// Clone keys
		clone.keys = new ArrayList<Object>();
		
		// Clone symbols
		if (symbols != null) {
			clone.symbols = createSymbolMap();
			for (Iterator<Entry<Object, ISymbol>> iterator =
					symbols.entrySet().iterator(); iterator.hasNext();) {
				Entry<Object, ISymbol> entry = iterator.next();
				
				Object key = entry.getKey();
				IInterval key_interval = null;
				if (key instanceof IInterval) {
				    key_interval = (IInterval) key;
				    key = key_interval.clone();
				}
				ISymbol symbolClone =
						(ISymbol) ((ISymbol) entry.getValue()).clone();
				clone.addSymbol(key, symbolClone);
			}
		}
		
		clone.setIntervalType(this.getIntervalType());

		return clone;
	}

	@SuppressWarnings("unchecked")
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent properties
		super.loadFromState(state);
		// Set own properties
		setIntervalType(state.getInt(FIELD_INTERVAL_TYPE));
		keys = new ArrayList<Object>(state.getList(FIELD_KEYS));
		useDefaultSymbol(state.getBoolean(FIELD_USE_DEFAULT_SYMBOL));
		Map persistedSymbolMap = (Map) state.get(FIELD_SYMBOLS);
		if (persistedSymbolMap != null) {
			symbols.putAll(persistedSymbolMap);
		}
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_INTERVAL_TYPE, getIntervalType());
		state.set(FIELD_KEYS, keys);
		state.set(FIELD_USE_DEFAULT_SYMBOL, isUseDefaultSymbol());
		state.set(FIELD_SYMBOLS, symbols);
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractIntervalLegend.class,
						INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME,
						INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME+" Persistence definition (FIXME check keys type)",
						null, 
						null
				);
				
				// Extend the Classified Vector Legend base definition
				definition.extend(manager.getDefinition(CLASSIFIED_VECTOR_LEGEND_PERSISTENCE_DEFINITION_NAME));

				// Interval type
				definition.addDynFieldInt(FIELD_INTERVAL_TYPE).setMandatory(true);
				// Keys
				definition.addDynFieldList(FIELD_KEYS)
					.setClassOfItems(int.class);
				// Use default symbol?
				definition.addDynFieldBoolean(FIELD_USE_DEFAULT_SYMBOL);
				// Symbols
				definition.addDynFieldMap(FIELD_SYMBOLS).setClassOfItems(ISymbol.class);
			}
			return Boolean.TRUE;
		}
		
	}
	

	
	
	private Map<Object, ISymbol> createSymbolMap() {
		return new TreeMap<Object, ISymbol>
		(new Comparator<Object>() { 
					public int compare(Object o1, Object o2) {
						if ((o1 != null) && (o2 != null)) {
						    
						    boolean o1_inte = (o1 instanceof FInterval);
                            boolean o2_inte = (o2 instanceof FInterval);
                            
                            if (!o1_inte && !o2_inte) {
                                return 0;
                            } else {
                                if (o1_inte && !o2_inte) {
                                    return 1;
                                } else {
                                    if (!o1_inte && o2_inte) {
                                        return -1;
                                    }
                                }
                            }

							FInterval i2 = (FInterval) o2;
							FInterval i1 = (FInterval) o1;

							if (i1.getMin() > i2.getMin()) {
								return 1;
							}

							if (i1.getMin() < i2.getMin()) {
								return -1;
							}
							if (i1.getMax() < i2.getMax()) {
								return -1;
							}
							if (i1.getMax() > i2.getMax()) {
								return 1;
							}
						}

						return 0;
					}
				});
	}

        @Override
    public Map<IInterval,ISymbol> createSymbols(IInterval[] intervals) {
        try {
            IInterval interval;
            NumberFormat formatter = NumberFormat.getInstance();
            Color startColor = this.getStartColor();
            Color endColor = this.getEndColor();
            Map<IInterval,ISymbol> symbols = new HashMap<>();
            SymbolManager symbolManager = MapContextLocator.getSymbolManager();

            int r;
            int g;
            int b;
            int stepR;
            int stepG;
            int stepB;

            formatter.setMaximumFractionDigits(2);
            r = startColor.getRed();
            g = startColor.getGreen();
            b = startColor.getBlue();
            stepR = (int) Math.floor((endColor.getRed() - r) / (intervals.length - 1));
            stepG = (int) Math.floor((endColor.getGreen() - g) / (intervals.length - 1));
            stepB = (int) Math.floor((endColor.getBlue() - b) / (intervals.length - 1));

            for (int k = 0; k < intervals.length; k++) {
                interval = intervals[k];

                ISymbol theSymbol = null;
                Color color = new Color(r, g, b);
                int intervalos = intervals.length - 1;
                if (intervalos == k) {
                    color = endColor;
                }

                theSymbol = symbolManager.createSymbol(this.getShapeType(), color);

                /*
                 * If multishape, we need to set line and fill color here.
                 * This is because the symbol manager is in mapcontext.api, which cannot
                 * depend on symbology.lib.api
                 */
                if (theSymbol instanceof IMultiShapeSymbol) {
                    IMultiShapeSymbol mss = (IMultiShapeSymbol) theSymbol;
                    mss.getLineSymbol().setLineColor(color);
                    mss.getFillSymbol().setFillColor(color);
                }
                theSymbol.setDescription(
                        formatter.format(interval.getMin())
                        + " - "
                        + formatter.format(interval.getMax())
                );
                r = r + stepR;
                g = g + stepG;
                b = b + stepB;

                symbols.put(interval, theSymbol);
            }
            return symbols;
        } catch (Exception e) {
            throw new RuntimeException("Can't create symbols from the intervals",e);
        }
    }
    
    @Override
    public void setIntervals(IInterval[] intervals) {
        Map<IInterval,ISymbol>symbols = this.createSymbols(intervals);
        this.clear();
        for (Entry<IInterval, ISymbol> entrySet : symbols.entrySet()) {
            IInterval key = entrySet.getKey();
            ISymbol value = entrySet.getValue();
            this.addSymbol(key,value);
        }
    }
        
        
       
}