/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl;

import java.awt.Color;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.IInterval;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialIntervalLegend;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * <p>
 * VectorialIntervalLegend (which should be better called GraduatedColorLegend)
 * is a legend that allows to classify ranges of values using a color gradient
 * based on a value.<b>
 * </p>
 * <p>
 * The color gradient will be calculated according the starting color, the
 * ending color and the amount of intervals.
 * </p>
 * 
 * @author Vicente Caballero Navarro
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class VectorialIntervalLegend extends AbstractIntervalLegend {
//	final static private Logger LOG = LoggerFactory.getLogger(VectorialIntervalLegend.class);

	public static final String VECTORIAL_INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME =
			"VectorialIntervalLegend";

	private static final String FIELD_START_COLOR = "startColor";
	private static final String FIELD_END_COLOR = "endColor";
	
	private int shapeType;
    private Color startColor = Color.red;
    private Color endColor = Color.blue;

    /**
     * Constructor method
     */
    public VectorialIntervalLegend() {
    	super();
    }

    /**
     * Constructor method
     *
     * @param type type of the shape.
     */
    public VectorialIntervalLegend(int type) {
    	super();
    	setShapeType(type);
    }

    /**
	 * Returns the final color
	 * @return Color  final color.
	 * @uml.property  name="endColor"
	 */
    public Color getEndColor() {
        return endColor;
    }

    /**
	 * Inserts the final color.
	 * @param endColor final color.
	 * @uml.property  name="endColor"
	 */
    public void setEndColor(Color endColor) {
        this.endColor = endColor;
    }

    /**
	 * Returns the initial color.
	 * @return  Color initial color.
	 * @uml.property  name="startColor"
	 */
    public Color getStartColor() {
        return startColor;
    }

    /**
	 * Inserts the initial color
	 * @param startColor initial color.
	 * @uml.property  name="startColor"
	 */
    public void setStartColor(Color startColor) {
        this.startColor = startColor;
    }

	public int getShapeType() {
		return shapeType;
	}

	public void setShapeType(int shapeType) {
		if (this.shapeType != shapeType) {
			if(defaultSymbol==null || defaultSymbol.getSymbolType()!=shapeType){
				setDefaultSymbol(getSymbolManager().createSymbol(shapeType));
			}
			this.shapeType = shapeType;
		}
	}


	public String getClassName() {
		return getClass().getName();
	}

	public Object clone() throws CloneNotSupportedException {
		VectorialIntervalLegend clone = (VectorialIntervalLegend) super.clone();

		// Nothing to clone, as Color objects are inmutable, keep the original
		// references.

		return clone;
	}

	public IInterval createInterval(double min, double max) {
		return new FInterval(min, max);
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent properties
		super.loadFromState(state);
		// Set own properties
		setStartColor((Color) state.get(FIELD_START_COLOR));
		setEndColor((Color) state.get(FIELD_END_COLOR));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_START_COLOR, getStartColor());
		state.set(FIELD_END_COLOR, getEndColor());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(VECTORIAL_INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						VectorialIntervalLegend.class,
						VECTORIAL_INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME,
						VECTORIAL_INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);

				// Extend the Interval Legend base definition
				definition.extend(manager.getDefinition(INTERVAL_LEGEND_PERSISTENCE_DEFINITION_NAME));

				// Start color
				definition.addDynFieldObject(FIELD_START_COLOR)
						.setClassOfValue(Color.class)
						.setMandatory(true)
						.setDescription("Start color");
				// End color
				definition.addDynFieldObject(FIELD_END_COLOR)
						.setClassOfValue(Color.class)
						.setMandatory(true)
						.setDescription("End color");
			}
			return Boolean.TRUE;
		}
		
	}

	public static class RegisterLegend implements Callable {

		public Object call() throws Exception {
	        MapContextManager manager = MapContextLocator.getMapContextManager();

	        manager.registerLegend(IVectorialIntervalLegend.LEGEND_NAME,
                    VectorialIntervalLegend.class);

			return Boolean.TRUE;
		}
		
	}


}
