/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl;

import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;



/**
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class CartographicSupportToolkit {

	/**
	 * Calculates the distance in pixels corresponding to the provided length,
	 * according to the current rendering context (output dpi, map scale, map units) and the
	 * symbol cartographic settings (unit, size, and unit reference system).
	 *
	 * @param cartographicElement CartographicSupport object which contains the reference system
	 * and the measuring units for the provided length
	 * @param length The length to be computed in pixels. The length is supposed to be
	 * provided in the units defined in the cartographicElement
	 * @param viewPort The viewport, which defines the relationship between pixels and maps units
	 * @param dpi The resolution (dots per inch) of the target display device
	 * (printer, screen, etc)
	 * @return The distance in pixels corresponding to the provided length
	 */
	public static double getCartographicLength(CartographicSupport cartographicElement, double length, ViewPort viewPort, double dpi) {
		int unit = cartographicElement.getUnit();
		double lengthInPixel = length;

		if (unit != -1) {
			double[] trans2Meter=MapContext.getDistanceTrans2Meter();
			if (cartographicElement.getReferenceSystem() == CartographicSupport.WORLD) {
				double dist1PixelInMeters = viewPort.getDist1pixel()*trans2Meter[viewPort.getMapUnits()];
				double realWidthMeter = length*trans2Meter[unit];
				lengthInPixel = realWidthMeter/dist1PixelInMeters;
			} else if (cartographicElement.getReferenceSystem() == CartographicSupport.PAPER) {
				double lengthInInches = 1/trans2Meter[7]*trans2Meter[unit]*length;
				lengthInPixel = lengthInInches*dpi;
			}
		} else{
			double scale=dpi/72;
			lengthInPixel=lengthInPixel*scale;
		}
		return  (lengthInPixel);

	}

	/**
	 * The unit that will be used when creating new symbols.<br>
	 * <b>Factory defaults to pixel.</b>
	 */
	public static int DefaultMeasureUnit = -1;
	/**
	 * The reference system that will be used when creating new symbols.<br>
	 * <b>Factory defaults to pixel.</b>
	 */
	public static int DefaultReferenceSystem = CartographicSupport.WORLD;

}


