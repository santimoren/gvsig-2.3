/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import org.apache.batik.ext.awt.geom.PathLength;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IArrowMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.ArrowMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Class ArrowDecoratorStyle. It is used to store the information about the
 * different options to draw an arrow in a line (and draw it too). This
 * information is taken from the panel.
 *
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class ArrowDecoratorStyle extends AbstractStyle implements IArrowDecoratorStyle  {
	public static final String ARROR_DECORATOR_STYLE_PERSISTENCE_DEFINITION_NAME = "ArrowDecoratorStyle";

	private static final String FIELD_FLIP_ALL = "flipAll";
	private static final String FIELD_FLIP_FIRST = "flipFirst";
	private static final String FIELD_ARROW_MARKER_COUNT = "arrowMarkerCount";
	private static final String FIELD_FOLLOW_LINE_ANGLE = "followLineAngle";
	private static final String FIELD_MARKER = "marker";

	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private boolean flipAll = false;
	private boolean flipFirst = false;
	private int arrowMarkerCount = 2;
	private boolean followLineAngle = true;
	private IMarkerSymbol marker = getDefaultMarker();

	public ArrowDecoratorStyle() {
		marker.setSize(10);
		((IArrowMarkerSymbol) marker).setSharpness(30);
	}

	private IMarkerSymbol getDefaultMarker() {
		return new ArrowMarkerSymbol();
	}
	
	/**
	 * Obtains the number of arrows that the user wants to draw in the same line.
	 * @return
	 */
	public int getArrowMarkerCount() {
		return arrowMarkerCount;
	}

	/**
	 * Defines the number of arrows that the user wants to draw in the same line.
	 * @return
	 */
	public void setArrowMarkerCount(int arrowMarkerCount) {
		this.arrowMarkerCount = arrowMarkerCount;
	}

	/**
	 * Defines the flipAll attribute.If the value of this attribute is true all the
	 * arrows that we had drawn in the same line will be flipped.
	 * @return
	 */
	public boolean isFlipAll() {
		return flipAll;
	}

	/**
	 * Obtains the flipAll attribute.If the value of this attribute is true all the
	 * arrows that we had drawn in the same line will be flipped.
	 * @return
	 */
	public void setFlipAll(boolean flipAll) {
		this.flipAll = flipAll;
	}

	/**
	 * Obtains the flipFirst attribute.If it is true only the first arrow of the line
	 * will be flipped.The rest will keep the same orientation.
	 * @return
	 */
	public boolean isFlipFirst() {
		return flipFirst;
	}

	/**
	 * Sets the flipFirst attribute.If it is true only the first arrow of the line
	 * will be flipped.The rest will keep the same orientation.
	 * @return
	 */
	public void setFlipFirst(boolean flipFirst) {
		this.flipFirst = flipFirst;
	}

	/**
	 * Gets the followLineAngle attribute.This attribute allows the arrow that we are
	 * going to draw to be more or less aligned with the line where it will be included (depending on the angle) .
	 * @return
	 */
	public boolean isFollowLineAngle() {
		return followLineAngle;
	}

	/**
	 * Sets the followLineAngle attribute.This attribute allows the arrow that we are
	 * going to draw to be more or less aligned with the line where it will be included.
	 * (depending on the angle).
	 * @param followingLineAngle
	 * @return
	 */
	public void setFollowLineAngle(boolean followLineAngle) {
		this.followLineAngle = followLineAngle;
	}
	/**
	 * Draws an arrow(or other symbol that substitutes an arrow selected by the user)
	 * in a line.When the line is drawn, the symbol is added and takes care of the different
	 * options of the user(for example if he wants to flip the first symbol or all and
	 * the number of symbols per line to be drawn)
	 * @param g
	 * @param affineTransform
	 * @param feature 
	 * @param shp
	 * @throws CreateGeometryException 
	 */
	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature) throws CreateGeometryException {
		if (arrowMarkerCount <= 0) return;

		Geometry geomToDraw = geom.cloneGeometry();
		geomToDraw.transform(affineTransform);
		PathLength pl = new PathLength(geomToDraw.getShape());
		float size = (float) marker.getSize();
		marker.setRotation(0.0);
		float myLineLength = pl.lengthOfPath();
		if (size > myLineLength){
			return;
		}
		float step = arrowMarkerCount>2 ? myLineLength/(arrowMarkerCount-1) : pl.lengthOfPath();
		float rotation1 = 0; // rotation at the arrow's vertex
		float rotation2 = 0; // rotation at the arrow's back;

		Point startP = null;

		// the first arrow at the end of the line
		float theLength = pl.lengthOfPath();
		{

			if ((flipFirst || flipAll) && (flipFirst != flipAll) && followLineAngle) { // logical XOR
				Point2D p = pl.pointAtLength(theLength-size);
				if (p!=null){
					startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
					if(followLineAngle){
						rotation1 = pl.angleAtLength(theLength-size);
						rotation2 = pl.angleAtLength(theLength);
						marker.setRotation(rotation1);
					}
					marker.draw(g, new AffineTransform(), startP, feature, null);
				}
			} else {
				Point2D p = pl.pointAtLength(theLength);
				if (p!=null){
					startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
					if(followLineAngle){
						rotation1 = pl.angleAtLength(theLength-size)+(float) Math.PI;
						rotation2 = pl.angleAtLength(theLength)+(float) Math.PI;
						marker.setRotation(rotation2);
					}
					marker.draw(g, new AffineTransform(), startP, feature, null);
				}
			}
		}
		// the other arrows but the first and the last
		float aLength;
		for (int i = 1; i < arrowMarkerCount-1; i++) {
			aLength = (float) (step*i);

			if (flipAll && followLineAngle) {
				Point2D p = pl.pointAtLength(aLength);
				if (p==null){
					p = pl.pointAtLength(theLength);
				}
				startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
				if(followLineAngle){
					rotation1 = (float) pl.angleAtLength(aLength);
					rotation2 = (float) pl.angleAtLength((float)(aLength+size));
					marker.setRotation(rotation1);
				}
					marker.draw(g, new AffineTransform(), startP, feature, null);
			} else {
				Point2D p = pl.pointAtLength(aLength+size);
				if (p==null){
					p = pl.pointAtLength(theLength);
				}

				startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
				if(followLineAngle){
					rotation1 = (float) (pl.angleAtLength(aLength) + Math.PI);
					rotation2 = (float) (pl.angleAtLength((float)(aLength+size)) + Math.PI);
					marker.setRotation(rotation2);
				}
				marker.draw(g, new AffineTransform(), startP, feature, null);
			}
		}

		startP = null;
		// and the last arrow at the begining of the line
		if (arrowMarkerCount>1) {
			if (flipAll) {
				Point2D p = null;
				p = pl.pointAtLength(0);
				if (p!=null){
					startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
					if(followLineAngle){
						rotation1 = (float) pl.angleAtLength(size);
						rotation2 = (float) pl.angleAtLength(0);
						marker.setRotation(rotation2);
					}
					marker.draw(g, new AffineTransform(), startP, feature, null);
				}				
			} else {
				Point2D p = null;
				if(followLineAngle){
					p = pl.pointAtLength(size);
				} else {
					p = pl.pointAtLength(0);
				}
				if (p!=null){
					startP = geomManager.createPoint(p.getX(), p.getY(), SUBTYPES.GEOM2D);
					if(followLineAngle){
						rotation1 = (float) (pl.angleAtLength(size) + Math.PI);
						rotation2 = (float) (pl.angleAtLength(0) + Math.PI);
						marker.setRotation(rotation1);
					}
					marker.draw(g, new AffineTransform(), startP, feature, null);
				}
			}
		}
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public void drawOutline(Graphics2D g, Rectangle r) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public boolean isSuitableFor(ISymbol symbol) {
		return symbol instanceof ILineSymbol;
	}

	public String getClassName() {
		return getClass().getName();
	}

	public IMarkerSymbol getMarker() {
		if(marker == null){
			marker = getDefaultMarker();
		}
		return marker;
	}

	public void setMarker(IMarkerSymbol marker) {
		this.marker = marker;
	}
	
	public Object clone() throws CloneNotSupportedException {
		ArrowDecoratorStyle copy  = (ArrowDecoratorStyle) super.clone();
		if (marker != null) {
			copy.marker = (IMarkerSymbol) marker.clone();
		}
		return copy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent style properties
		super.loadFromState(state);

		// Set own properties
		setArrowMarkerCount(state.getInt(FIELD_ARROW_MARKER_COUNT));
		setFlipAll(state.getBoolean(FIELD_FLIP_ALL));
		setFlipFirst(state.getBoolean(FIELD_FLIP_FIRST));
		setFollowLineAngle(state.getBoolean(FIELD_FOLLOW_LINE_ANGLE));
		setMarker((IMarkerSymbol) state.get(FIELD_MARKER));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_ARROW_MARKER_COUNT, getArrowMarkerCount());
		state.set(FIELD_FLIP_ALL, isFlipAll());
		state.set(FIELD_FLIP_FIRST, isFlipFirst());
		state.set(FIELD_FOLLOW_LINE_ANGLE, isFollowLineAngle());
		state.set(FIELD_MARKER, getMarker());
	}
	
	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(ARROR_DECORATOR_STYLE_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						ArrowDecoratorStyle.class,
						ARROR_DECORATOR_STYLE_PERSISTENCE_DEFINITION_NAME,
						ARROR_DECORATOR_STYLE_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null, 
						null
				);
				
				// Extend the Style base definition
				definition.extend(manager.getDefinition(STYLE_PERSISTENCE_DEFINITION_NAME));

				// Arrow marker count
				definition.addDynFieldInt(FIELD_ARROW_MARKER_COUNT).setMandatory(true);
				// Flip all
				definition.addDynFieldBoolean(FIELD_FLIP_ALL).setMandatory(true);
				// flip first
				definition.addDynFieldBoolean(FIELD_FLIP_FIRST).setMandatory(true);
				// Follow line angles
				definition.addDynFieldBoolean(FIELD_FOLLOW_LINE_ANGLE).setMandatory(true);
				// Marker
				definition.addDynFieldObject(FIELD_MARKER)
					.setClassOfValue(IMarkerSymbol.class)
					.setMandatory(true);

			}
			return Boolean.TRUE;
		}
		
	}

}