package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.gvsig.tools.evaluator.EvaluatorWithDescriptions;
import org.gvsig.tools.evaluator.sqljep.SQLJEPEvaluator;


/**
 * 
 * Utility class to centralize the creation of an
 * @link {@link EvaluatorWithDescriptions}
 * 
 * TODO Perhaps create a manager in Tools to perform this operation
 * 
 * @author jldominguez
 *
 */
public class EvaluatorCreator {

	private static Map<String, EvaluatorWithDescriptions> evCache =
			new HashMap<String, EvaluatorWithDescriptions>();
	
    /**
     * Create an {@link EvaluatorWithDescriptions} that uses
     * the string expression provided
     * 
     * @param expr
     * @return
     */
    public static EvaluatorWithDescriptions getEvaluator(String expr) {
    	
    	EvaluatorWithDescriptions resp = evCache.get(expr);
    	if (resp == null) {
    		resp = new SQLJEPEvaluator(expr);
    		if (evCache.size() > 100) {
    			removeOne(evCache);
    		}
    		evCache.put(expr, resp);
    	}
        return resp; 
    }
    
	private static void removeOne(
			Map<String, EvaluatorWithDescriptions> themap) {
		
		Iterator<String> iter = themap.keySet().iterator();
		int count = 50;
		String k = null;
		while (iter.hasNext() && count > 0) {
			k = iter.next();
			count--;
		}
		themap.remove(k);
	}

}
