/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl;

import java.awt.Color;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.AbstractSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Abstract class that any FILL SYMBOL should extend
 *
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractFillSymbol extends AbstractSymbol implements
		IFillSymbol {

	public static final String FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME = "FillSymbol";

	private static final String FIELD_OUTLINE = "outline";

	private static final String FIELD_HAS_OUTLINE = "hasOutline";

	private static final String FIELD_HAS_FILL = "hasFill";

	private static final String FIELD_COLOR = "color";

	private boolean hasFill = true;
	private boolean hasOutline = true;
	private Color color;
	private ILineSymbol outline;

	public AbstractFillSymbol() {
        super();
        SymbolManager symbolManager = MapContextLocator.getSymbolManager();
        SymbolPreferences symbolPreferences =
            symbolManager.getSymbolPreferences();
        color = symbolPreferences.getDefaultSymbolFillColor();

        outline =
            (ILineSymbol) symbolManager.createSymbol(ILineSymbol.SYMBOL_NAME);
        outline.setColor(symbolPreferences.getDefaultSymbolColor());
	}

	public boolean isSuitableFor(Geometry geom) {
	       switch(geom.getType()) {
	        case Geometry.TYPES.SURFACE:
	        case Geometry.TYPES.POLYGON:
	            return true;
	        }
	        return false;
	}

	public int getOnePointRgb() {
		int rgb = 0;
		if (outline != null) {
			rgb = outline.getOnePointRgb();
		} else if (color != null) {
			rgb = color.getRGB();
		}
		return rgb;
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		if (getOutline() != null) {
			getOutline().getPixExtentPlus(geom, distances, viewPort, dpi);
		} else {
			distances[0] = 0;
			distances[1] = 0;
		}
	}

	public void setFillColor(Color color) {
		this.color = color;
	}

	public void setOutline(ILineSymbol outline) {
		this.outline = outline;
	}

	public Color getFillColor() {
		return color;
	}

	public ILineSymbol getOutline() {
		return outline;
	}

	public int getFillAlpha() {
		// TODO debatir si es correcto o no (por ejemplo cuando hablamos de
		// LineFillSymbol's
		if (color == null)
			return 255;
		return color.getAlpha();
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		if (getOutline() != null) {
			getOutline().setLineWidth(cartographicSize);
		}
	}

	public double toCartographicSize(ViewPort viewPort, double dpi,
			Geometry geom) {
		if (getOutline() != null) {
			double oldSize = getOutline().getLineWidth();
			setCartographicSize(getCartographicSize(viewPort, dpi, geom), geom);
			return oldSize;
		}
		return 0;
	}

	public boolean hasFill() {
		return hasFill;
	}

	public void setHasFill(boolean hasFill) {
		this.hasFill = hasFill;
	}

	public boolean hasOutline() {
		return hasOutline;
	}

	public void setHasOutline(boolean hasOutline) {
		this.hasOutline = hasOutline;
	}

	public double getCartographicSize(ViewPort viewPort, double dpi,
			Geometry geom) {
		if (getOutline() != null) {
			return CartographicSupportToolkit.getCartographicLength(this,
					getOutline().getLineWidth(), viewPort, dpi);
		}
		return 0;
	}

	public void setUnit(int unitIndex) {
		super.setUnit(unitIndex);
		if (getOutline() != null) {
			getOutline().setUnit(unitIndex);
		}
	}

	public Color getColor() {
		return getFillColor();
	}

	public void setColor(Color color) {
		setFillColor(color);
	}

	public Object clone() throws CloneNotSupportedException {
		IFillSymbol copy = (IFillSymbol) super.clone();

		// Clone outline
		ILineSymbol outline = copy.getOutline();
		if (outline != null) {
			copy.setOutline((ILineSymbol) outline.clone());
		}
		return copy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent symbol properties
		super.loadFromState(state);
		// Set own properties
		setColor((Color) state.get(FIELD_COLOR));
		setHasFill(state.getBoolean(FIELD_HAS_FILL));
		setHasOutline(state.getBoolean(FIELD_HAS_OUTLINE));
		setOutline((ILineSymbol) state.get(FIELD_OUTLINE));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent symbol properties
		super.saveToState(state);
		// Save own properties
		state.set(FIELD_COLOR, getColor());
		state.set(FIELD_HAS_FILL, hasFill());
		state.set(FIELD_HAS_OUTLINE, hasOutline());
		state.set(FIELD_OUTLINE, getOutline());
	}

	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						AbstractFillSymbol.class,
						FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);

				// Extend the Symbol base definition
				definition.extend(manager.getDefinition(SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Color
				definition.addDynFieldObject(FIELD_COLOR).setMandatory(false).setClassOfValue(Color.class);
				// Has fill?
				definition.addDynFieldBoolean(FIELD_HAS_FILL).setMandatory(true);
				// Has outline?
				definition.addDynFieldBoolean(FIELD_HAS_OUTLINE).setMandatory(true);
				// Outline
				definition.addDynFieldObject(FIELD_OUTLINE).setMandatory(false).setClassOfValue(ILineSymbol.class);
			}
			return Boolean.TRUE;
		}

	}

}