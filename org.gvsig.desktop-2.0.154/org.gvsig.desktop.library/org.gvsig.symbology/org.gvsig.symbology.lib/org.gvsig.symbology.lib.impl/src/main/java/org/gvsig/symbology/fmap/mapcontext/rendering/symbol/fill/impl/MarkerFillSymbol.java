/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: MarkerFillSymbol.java 16176 2007-11-08 16:07:26Z jdominguez $
* $Log$
* Revision 1.19  2007-09-21 12:25:32  jaume
* cancellation support extended down to the IGeometry and ISymbol level
*
* Revision 1.18  2007/09/20 11:53:11  jvidal
* bug solved
*
* Revision 1.16  2007/08/09 10:39:41  jaume
* first round of found bugs fixed
*
* Revision 1.15  2007/08/08 12:04:05  jvidal
* javadoc
*
* Revision 1.14  2007/08/03 09:22:09  jaume
* refactored class names
*
* Revision 1.13  2007/08/02 11:13:50  jaume
* char encoding fix
*
* Revision 1.12  2007/08/01 11:45:59  jaume
* passing general tests (drawing test yet missing)
*
* Revision 1.11  2007/07/23 06:52:25  jaume
* default selection color refactored, moved to MapContext
*
* Revision 1.10  2007/05/28 15:36:42  jaume
* *** empty log message ***
*
* Revision 1.9  2007/05/08 08:47:40  jaume
* *** empty log message ***
*
* Revision 1.8  2007/03/28 16:48:14  jaume
* *** empty log message ***
*
* Revision 1.7  2007/03/26 14:25:17  jaume
* implements IPrintable
*
* Revision 1.6  2007/03/21 17:36:22  jaume
* *** empty log message ***
*
* Revision 1.5  2007/03/13 16:58:36  jaume
* Added QuantityByCategory (Multivariable legend) and some bugfixes in symbols
*
* Revision 1.4  2007/03/09 11:20:57  jaume
* Advanced symbology (start committing)
*
* Revision 1.2.2.4  2007/02/16 10:54:12  jaume
* multilayer splitted to multilayerline, multilayermarker,and  multilayerfill
*
* Revision 1.2.2.3  2007/02/15 16:23:44  jaume
* *** empty log message ***
*
* Revision 1.2.2.2  2007/02/12 15:15:20  jaume
* refactored interval legend and added graduated symbol legend
*
* Revision 1.2.2.1  2007/02/09 07:47:05  jaume
* Isymbol moved
*
* Revision 1.2  2007/01/10 16:39:41  jaume
* ISymbol now belongs to com.iver.cit.gvsig.fmap.core.symbols package
*
* Revision 1.1  2007/01/10 16:31:36  jaume
* *** empty log message ***
*
* Revision 1.10  2006/11/09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.9  2006/11/09 10:22:50  jaume
* *** empty log message ***
*
* Revision 1.8  2006/11/08 13:05:51  jaume
* *** empty log message ***
*
* Revision 1.7  2006/11/08 10:56:47  jaume
* *** empty log message ***
*
* Revision 1.6  2006/11/07 08:52:30  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/06 17:08:45  jaume
* *** empty log message ***
*
* Revision 1.4  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.2  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
*
*/
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IMarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl.PictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.SimpleMarkerFillPropertiesStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Allows to define a marker symbol of any type as a path image to be used for a filled of a
 * polygon's padding
 *
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public class MarkerFillSymbol extends AbstractFillSymbol implements IMarkerFillSymbol {
	private static final Logger logger = LoggerFactory.getLogger(MarkerFillSymbol.class);

    public static final String MARK_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME =
        "MarkerFillSymbol";
    private static final String MARKER_SYMBOL = "markerSymbol";
    private static final String SELECTION_SYMBOL = "selectionSymbol";
    private static final String MARKER_FILL_PROPERTIES = "markerFillProperties";
    private static final String PREVIOUS_MARKERSIZE = "previousMarkerSize";

	public static final int RANDOM_FILL = 3;
	public static final int GRID_FILL = 1;
	public static final int SINGLE_CENTERED_SYMBOL = 2;
	public static int DefaultFillStyle = GRID_FILL;
	private MarkerFillSymbol selectionSymbol;
	private IMarkerFillPropertiesStyle markerFillProperties = new SimpleMarkerFillPropertiesStyle();
	private IMarkerSymbol markerSymbol = (IMarkerSymbol) MapContextLocator.getSymbolManager().createSymbol(IMarkerSymbol.SYMBOL_NAME);
	private double previousMarkerSize = markerSymbol.getSize();
	private PrintAttributes properties;
	private GeometryManager geometryManager = GeometryLocator.getGeometryManager();

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			selectionSymbol = (MarkerFillSymbol) cloneForSelection();
			selectionSymbol.setFillColor(MapContext.getSelectionColor());
		}else {
            selectionSymbol.setColor(MapContext.getSelectionColor());
        }

		return selectionSymbol;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, Geometry geom, Feature feat, Cancellable cancel) {
		Point centroid = null;
		Point p = null;
			switch (markerFillProperties.getFillStyle()) {
			case SINGLE_CENTERED_SYMBOL:
				// case a single marker is used into a polygon shapetype
				//			Geometry geom = FConverter.java2d_to_jts(geom);
				//			com.vividsolutions.jts.geom.Point centroid = geom.getCentroid();
				try {
					centroid = geom.centroid();
				} catch (GeometryOperationNotSupportedException e2) {
					logger.warn("Can't get centroid", e2);
				} catch (GeometryOperationException e2) {
					logger.warn("Can't get centroid", e2);
				}

				/*
				 * Hay ocasiones en que jts no puede calcular un centroide y devuelve NaN
				 * (por ejemplo con geometr�as poligonales cuyos puntos tienen todos la misma
				 * abscisa y distinta ordenada con tan solo una diferencia de 1 � 2 unidades)
				 * entonces, en lugar de utilizar este centroide tomamos el centro del
				 * bounds del shp (la geometr�a es tan peque�a que consideramos que deben coincidir).
				 */
				p = null;
				if(centroid!=null && !(Double.isNaN(centroid.getX()) || Double.isNaN(centroid.getY()))){
					double pX = centroid.getX()+markerFillProperties.getXOffset();
					double pY = centroid.getY()+markerFillProperties.getYOffset();
					try {
						p = geometryManager.createPoint(pX, pY, SUBTYPES.GEOM2D);
					} catch (CreateGeometryException e) {
						logger.error("Can't create the point ("+pX+","+pY+")", e);
					}
					if (p != null) {
						markerSymbol.draw(g, affineTransform, p, feat, null);
					}
				} else {
					double pX = geom.getShape().getBounds().getCenterX();
					double pY = geom.getShape().getBounds().getCenterY();
					try {
					p = geometryManager.createPoint(pX, pY, SUBTYPES.GEOM2D);
					} catch (CreateGeometryException e) {
						logger.error("Can't create the point ("+pX+","+pY+")", e);
					}
					if (p != null) {
						markerSymbol.draw(g, affineTransform, p, feat, null);
					}
				}
				break;
			case GRID_FILL:
				// case a grid fill is used
			{
				Rectangle rClip = null;
				if (g.getClipBounds()!=null){
					rClip=(Rectangle)g.getClipBounds().clone();
					g.setClip(rClip.x, rClip.y, rClip.width, rClip.height);
				}
				g.clip(geom.getShape(affineTransform));

				int size = (int) markerSymbol.getSize();
				Rectangle rProv = new Rectangle();
				rProv.setFrame(0, 0, size, size);
				Paint resulPatternFill = null;

				double xSeparation = markerFillProperties.getXSeparation(); // TODO apply CartographicSupport
				double ySeparation = markerFillProperties.getYSeparation(); // TODO apply CartographicSupport
				double xOffset = markerFillProperties.getXOffset();
				double yOffset = markerFillProperties.getYOffset();

				BufferedImage sample = null;
				sample = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
				Graphics2D gAux = sample.createGraphics();

				try {
					markerSymbol.drawInsideRectangle(gAux, gAux.getTransform(), rProv, null);
				} catch (SymbolDrawingException e) {
					if (e.getType() == SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS) {
						try {
							IWarningSymbol warning =
								(IWarningSymbol) MapContextLocator.getSymbolManager()
								.getWarningSymbol(
										SymbolDrawingException.STR_UNSUPPORTED_SET_OF_SETTINGS,
										"",
										SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
							warning.drawInsideRectangle(gAux, gAux.getTransform(), rProv, null);
						} catch (SymbolDrawingException e1) {
							// IMPOSSIBLE TO REACH THIS
						}
					} else {
						// should be unreachable code
						throw new Error(Messages.getText("symbol_shapetype_mismatch"));
					}
				}
				rProv.setRect(0, 0,
						rProv.getWidth() + xSeparation,
						rProv.getHeight() + ySeparation);

				BufferedImage bi = new BufferedImage(rProv.width, rProv.height, BufferedImage.TYPE_INT_ARGB);
				gAux = bi.createGraphics();
				gAux.drawImage(sample, null, (int) (xSeparation*0.5), (int) (ySeparation*0.5));

				resulPatternFill = new TexturePaint(bi,rProv);
				sample = null;
				gAux.dispose();

				g.setColor(null);
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);

				g.translate(xOffset, -yOffset);
				g.setPaint(resulPatternFill);
				g.fill(geom.getShape(affineTransform));
				g.translate(-xOffset, +yOffset);
				g.setClip(rClip);
				bi = null;
			}
			break;
			case RANDOM_FILL:
			{

				double s = markerSymbol.getSize();
				Geometry auxgeo = geom.cloneGeometry();
				auxgeo.transform(affineTransform);
				Shape shp = auxgeo.getShape();
				Rectangle r = shp.getBounds();
				int drawCount = (int) (Math.min(r.getWidth(), r.getHeight())/s);
				Random random = new Random();

				int minx = r.x;
				int miny = r.y;
				int width = r.width;
				int height = r.height;

				r = new Rectangle();
				g.setClip(shp);

				for (int i = 0; (cancel==null || !cancel.isCanceled()) && i < drawCount; i++) {
					int x = (int) Math.abs(random.nextDouble() * width);
					int y = (int) Math.abs(random.nextDouble() * height);
					x = x + minx;
					y = y + miny;
					//				markerSymbol.draw(g, new AffineTransform(), new FPoint2D(x, y), cancel);
					p = null;
					try {
						p = geometryManager.createPoint(x, y, SUBTYPES.GEOM2D);
					} catch (CreateGeometryException e) {
						logger.error("Can't create the point ("+x+","+y+")", e);
					}
					if (p!=null){
						markerSymbol.draw(g, new AffineTransform(), p, feat, cancel);
					}
				}
				g.setClip(null);
			}
			break;
			}
			if(getOutline()!= null){
				getOutline().draw(g, affineTransform, geom, feat, cancel);
			}
	}

	public int getSymbolType() {
		return Geometry.TYPES.SURFACE;
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r, PrintAttributes properties) throws SymbolDrawingException {
		markerFillProperties.setSampleSymbol(markerSymbol);
		Point p;
		try {
			switch (markerFillProperties.getFillStyle()) {
			case SINGLE_CENTERED_SYMBOL:
				p = geometryManager.createPoint(r.getCenterX(), r.getCenterY(), SUBTYPES.GEOM2D);
				markerSymbol.draw(g, null, p, null, null);
				break;
			case GRID_FILL:
			{
				g.setClip(r);
				int size = (int) markerSymbol.getSize();
				if (size <= 0 ) size = 1;
				Rectangle rProv = new Rectangle();
				rProv.setFrame(0, 0, size, size);
				Paint resulPatternFill = null;

				BufferedImage sample = null;
				sample = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
				Graphics2D gAux = sample.createGraphics();

				double xSeparation = markerFillProperties.getXSeparation(); // TODO apply CartographicSupport
				double ySeparation = markerFillProperties.getYSeparation(); // TODO apply CartographicSupport
				double xOffset = markerFillProperties.getXOffset();
				double yOffset = markerFillProperties.getYOffset();

				markerSymbol.drawInsideRectangle(gAux, new AffineTransform(), rProv, properties);

				rProv.setRect(0, 0,
						rProv.getWidth() + xSeparation,
						rProv.getHeight() + ySeparation);

				BufferedImage bi = new BufferedImage(rProv.width, rProv.height, BufferedImage.TYPE_INT_ARGB);
				gAux = bi.createGraphics();
				gAux.drawImage(sample, null, (int) (xSeparation*0.5), (int) (ySeparation*0.5));


				resulPatternFill = new TexturePaint(bi,rProv);
				g.setColor(null);
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);

				g.translate(xOffset, -yOffset);
				g.setPaint(resulPatternFill);
				g.fill(r);
				g.translate(-xOffset, yOffset);
				g.setClip(null);
			}
			break;
			case RANDOM_FILL:
				g.setClip(r);
				int x = r.x;
				int y = r.y;
				int width = r.width;
				int height= r.height;
				g.setBackground(null);

				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*0.2), (y+height*0.8), SUBTYPES.GEOM2D), null, null);
				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*0.634), (y+height*0.3), SUBTYPES.GEOM2D), null, null);
				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*0.26), (y+height*0.35), SUBTYPES.GEOM2D), null, null);
				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*0.45), (y+height*0.98), SUBTYPES.GEOM2D), null, null);
				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*0.9), (y+height*0.54), SUBTYPES.GEOM2D), null, null);
				markerSymbol.draw(g, null, geometryManager.createPoint((x+width*1.1), (y+height*0.7), SUBTYPES.GEOM2D), null, null);
				g.setClip(null);
				break;
			}
			if(getOutline()!= null && hasOutline()){
				if (properties==null) {
					//				getOutline().draw(g, scaleInstance, new FPolyline2D(new GeneralPathX(r)), null);
					getOutline().draw(g, scaleInstance, geometryManager.createPoint(r.getCenterX(), r.getCenterY(), SUBTYPES.GEOM2D), null,  null);
				} else {
					//				getOutline().print(g, scaleInstance, new FPolyline2D(new GeneralPathX(r)), properties);
					getOutline().print(g, scaleInstance, geometryManager.createPoint(r.getCenterX(), r.getCenterY(), SUBTYPES.GEOM2D), properties);
				}
			}
		} catch (CreateGeometryException e) {
			throw new SymbolDrawingException(TYPES.POINT);
		}
	}


	public String getClassName() {
		return getClass().getName();
	}

	public void setMarker(IMarkerSymbol marker) {
		this.markerSymbol = marker;
	}

	public IMarkerSymbol getMarker() {
		return markerSymbol;
	}

	public Color getFillColor(){
		return markerSymbol.getColor();
	}

	public void setFillColor (Color color) {
		markerSymbol.setColor(color);
	}

	public void print(Graphics2D g, AffineTransform at, Geometry geom, PrintAttributes properties) {
		this.properties=properties;
        draw(g, at, geom, null, null);
        this.properties=null;

	}
	/**
	 * Sets the markerfillproperties to be used by the class
	 *
	 * @param markerFillStyle,IMarkerFillPropertiesStyle
	 */
	public void setMarkerFillProperties(IMarkerFillPropertiesStyle markerFillStyle) {
		this.markerFillProperties = markerFillStyle;
	}

	/**
	 * Returns the markerfillproperties that are used by the class
	 *
	 * @return markerFillProperties,IMarkerFillPropertiesStyle
	 */
	public IMarkerFillPropertiesStyle getMarkerFillProperties() {
		return markerFillProperties;
	}

	@Override
	public void setUnit(int unitIndex) {
		super.setUnit(unitIndex);
		if (getMarker()!=null) {
			getMarker().setUnit(unitIndex);
		}
	}

	@Override
	public void setReferenceSystem(int system) {
		super.setReferenceSystem(system);
		if (getMarker()!=null) {
			getMarker().setReferenceSystem(system);
		}
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {

		super.setCartographicSize(cartographicSize, geom);
		IMarkerSymbol marker = getMarker();
		if (marker!=null) {
				marker.setCartographicSize(previousMarkerSize, geom);
			}

		super.setCartographicSize(cartographicSize, geom);

	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		IMarkerSymbol marker = getMarker();
		if (marker!=null) {
			previousMarkerSize = marker.getSize();
			double size = CartographicSupportToolkit.getCartographicLength(this, previousMarkerSize, viewPort, dpi);
			marker.setSize(size);
		}
		double s = super.toCartographicSize(viewPort, dpi, geom);
		return s;

	}


    public Object clone() throws CloneNotSupportedException {
    	MarkerFillSymbol copy = (MarkerFillSymbol) super.clone();

        // clone marker
        if (markerSymbol != null) {
            copy.markerSymbol = (IMarkerSymbol) markerSymbol.clone();
        }

        // clone selection
        if (selectionSymbol != null) {
            copy.selectionSymbol = (MarkerFillSymbol) selectionSymbol.clone();
        }

        // clone markerFillProperties
        if (markerFillProperties != null) {
            copy.markerFillProperties = (IMarkerFillPropertiesStyle) markerFillProperties.clone();
        }

        // FIXME: clone properties

        return copy;
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        // Set parent style properties
        super.loadFromState(state);

        this.markerSymbol =  (IMarkerSymbol) state.get(MARKER_SYMBOL);
        this.selectionSymbol = (MarkerFillSymbol) state.get(SELECTION_SYMBOL);
        this.markerFillProperties = (IMarkerFillPropertiesStyle) state.get(MARKER_FILL_PROPERTIES);
        this.previousMarkerSize = (Double) state.get(PREVIOUS_MARKERSIZE);
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // Save parent fill symbol properties
        super.saveToState(state);

        // Save own properties
        state.set(MARKER_SYMBOL, this.markerSymbol);
        state.set(SELECTION_SYMBOL, this.selectionSymbol);
        state.set(MARKER_FILL_PROPERTIES, this.markerFillProperties);
        state.set(PREVIOUS_MARKERSIZE, this.previousMarkerSize);
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(MARK_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition =
                    manager.addDefinition(MarkerFillSymbol.class,
                    		MARK_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME,
                    		MARK_FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME
                            + " Persistence definition",
                        null,
                        null);

                // Extend the Style base definition
                definition.extend(manager.getDefinition(FILL_SYMBOL_PERSISTENCE_DEFINITION_NAME));

                definition.addDynFieldObject(MARKER_SYMBOL)
                .setClassOfValue(IMarkerSymbol.class).setMandatory(true);
                definition.addDynFieldObject(SELECTION_SYMBOL)
                    .setClassOfValue(IMarkerFillSymbol.class).setMandatory(false);
                definition.addDynFieldObject(MARKER_FILL_PROPERTIES)
                .setClassOfValue(IMarkerFillPropertiesStyle.class).setMandatory(true);
                definition.addDynFieldDouble(PREVIOUS_MARKERSIZE);
            }
            return Boolean.TRUE;
        }
    }

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
	        int[] shapeTypes;
	        SymbolManager manager = MapContextLocator.getSymbolManager();

	        shapeTypes =
	            new int[] { Geometry.TYPES.SURFACE, Geometry.TYPES.CIRCLE,
	                Geometry.TYPES.ELLIPSE, Geometry.TYPES.MULTISURFACE };
	        manager.registerMultiLayerSymbol(IFillSymbol.SYMBOL_NAME,
	            shapeTypes,
	            MarkerFillSymbol.class);

			return Boolean.TRUE;
		}

	}


}
