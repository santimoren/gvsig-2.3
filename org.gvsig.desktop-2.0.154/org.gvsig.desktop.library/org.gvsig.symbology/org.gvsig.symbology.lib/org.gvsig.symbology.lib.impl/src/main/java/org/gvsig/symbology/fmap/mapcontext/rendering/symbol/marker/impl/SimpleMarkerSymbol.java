/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.ISimpleMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * SimpleMarkerSymbol is the most basic symbol for the representation of point objects
 * which can define its size and color apart from the rotation and the offset from a point
 * in the map.
 * @author 2005-2008 jaume dominguez faus - jaume.dominguez@iver.es
 * @author 2009-     <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class SimpleMarkerSymbol extends AbstractMarkerSymbol implements ISimpleMarkerSymbol {

	public static final String SIMPLE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME = "SimpleMarkerSymbol";

	private static final String FIELD_OUTLINED = "outlined";
	private static final String FIELD_OUTLINECOLOR = "outlineColor";
	private static final String FIELD_OUTLINESIZE = "outlineSize";
	private static final String FIELD_MARKERSTYLE = "markerStyle";

	private boolean outlined;
	private Color outlineColor;
	private double outlineSize;
	private ISymbol selectionSymbol;
	private int markerStyle;

	public SimpleMarkerSymbol() {
		super();
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			selectionSymbol = (SimpleMarkerSymbol) cloneForSelection();
		}else {
		    selectionSymbol.setColor(MapContext.getSelectionColor());
		}
		return selectionSymbol;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature, Cancellable cancel) {
		int x, y;
		org.gvsig.fmap.geom.primitive.Point p = (org.gvsig.fmap.geom.primitive.Point)geom.cloneGeometry();
		p.transform(affineTransform);

		int size = (int) getSize();

		int halfSize = size/2;
		x = ((int) (p.getX() + getOffset().getX()) - halfSize);
		y = ((int) (p.getY() + getOffset().getY()) - halfSize);

		// IMask mask = getMask();
		// if (mask != null) {
		// IFillSymbol maskShape = mask.getFillSymbol();
		// // maskShape.draw(g, null, mask.getHaloShape(shp));
		// }

		g.setColor(getColor());
		GeneralPathX genPath = null;
		g.setStroke(new BasicStroke(1));

		switch (markerStyle) {
		case CIRCLE_STYLE:
			g.fillOval(x, y, size, size);
			break;
		case SQUARE_STYLE:
			g.fillRect(x, y, size, size);
			break;
		case CROSS_STYLE:
			x = x + halfSize;
			y = y + halfSize;
			genPath = new GeneralPathX();
			genPath.moveTo(x, y - halfSize);
			genPath.lineTo(x, y + halfSize);
			genPath.moveTo(x - halfSize, y);
			genPath.lineTo(x + halfSize, y);
			g.draw(genPath);
			break;
		case VERTICAL_LINE_STYLE:
			genPath = new GeneralPathX();
			genPath.moveTo(x + halfSize, y);
			genPath.lineTo(x + halfSize, y + size);
			g.draw(genPath);
			break;
		case DIAMOND_STYLE:
			x = x + halfSize;
			y = y + halfSize;
			genPath = new GeneralPathX();
			genPath.moveTo(x-halfSize, y);
			genPath.lineTo(x, y+halfSize);
			genPath.lineTo(x+halfSize, y);
			genPath.lineTo(x, y-halfSize);
			genPath.lineTo(x-halfSize, y);
			genPath.closePath();
			g.fill(genPath);
			break;
		case X_STYLE:
			x = x + halfSize;
			y = y + halfSize;
			genPath = new GeneralPathX();
			genPath.moveTo(x-halfSize, y - halfSize);
			genPath.lineTo(x+halfSize, y + halfSize);
			genPath.moveTo(x - halfSize, y + halfSize);
			genPath.lineTo(x + halfSize, y - halfSize);
			g.draw(genPath);
			break;
		case TRIANGLE_STYLE:
			x = x + halfSize;
			y = y + halfSize;
			int otherSize = (int) (size * 0.55);
			genPath = new GeneralPathX();
			genPath.moveTo(x - halfSize,
				y + halfSize);
			genPath.lineTo(x + halfSize,
				y + halfSize);
			genPath.lineTo(x, y - otherSize);
			genPath.closePath();

			g.fill(genPath);
			break;
		case STAR_STYLE:
			x = x + halfSize;
			y = y + halfSize;
			genPath = new GeneralPathX();
			genPath.moveTo(x-halfSize, y);

			genPath.lineTo(x-2*(halfSize/3), y+(halfSize/3));
			genPath.lineTo(x-2*(halfSize/3), y+2*(halfSize/3));
			genPath.lineTo(x-(halfSize/3), y+2*(halfSize/3));

			genPath.lineTo(x, y+halfSize);

			genPath.lineTo(x+(halfSize/3), y+2*(halfSize/3));
			genPath.lineTo(x+2*(halfSize/3), y+2*(halfSize/3));
			genPath.lineTo(x+2*(halfSize/3), y+(halfSize/3));

			genPath.lineTo(x+(halfSize), y);

			genPath.lineTo(x+2*(halfSize/3), y-(halfSize/3));
			genPath.lineTo(x+2*(halfSize/3), y-2*(halfSize/3));
			genPath.lineTo(x+(halfSize/3), y-2*(halfSize/3));

			genPath.lineTo(x, y-halfSize);

			genPath.lineTo(x-(halfSize/3), y-2*(halfSize/3));
			genPath.lineTo(x-2*(halfSize/3), y-2*(halfSize/3));
			genPath.lineTo(x-2*(halfSize/3), y-(halfSize/3));

			genPath.lineTo(x-halfSize, y);

			genPath.closePath();


			g.fill(genPath);

			break;
		}


		if (outlined) {
			g.setColor(outlineColor);
			switch (markerStyle) {
			case CIRCLE_STYLE:
				g.drawOval(x, y, size, size);
				break;
			case SQUARE_STYLE:
				g.drawRect(x, y, size, size);
				break;
			case CROSS_STYLE:
			case DIAMOND_STYLE:
			case STAR_STYLE:
			case X_STYLE:
			case TRIANGLE_STYLE:
			case VERTICAL_LINE_STYLE:
				g.draw(genPath);
				break;
			}
		}
	}

	/**
	 * Returns true or false depending if the simple marker symbol has an outline or not.
	 * @return Returns the outline.
	 */
	public boolean hasOutline() {
		return outlined;
	}

	/**
	 * Establishes the outline for the simple marker symbol.
	 * @param outline  The outline to set.
	 */
	public void setOutlined(boolean outlined) {
		this.outlined = outlined;
	}

	/**
	 * Returns the outline color for the symple marker symbol
	 *
	 * @return Color,outlineColor.
	 */
	public Color getOutlineColor() {
		return outlineColor;
	}

	/**
	 * Sets the outline color for the simple marker symbol
	 * @param outlineColor, Color
	 */
	public void setOutlineColor(Color outlineColor) {
		this.outlineColor = outlineColor;
	}

	/**
	 * Gets the size of the outline for the simple marker symbol
	 * @return  Returns the outlineSize.
	 */
	public double getOutlineSize() {
		return outlineSize;
	}

	/**
	 * Establishes the size for the outline of the simple marker symbol
	 * @param outlineSize  The outlineSize to set.
	 */
	public void setOutlineSize(double outlineSize) {
		this.outlineSize = outlineSize;
	}

	/**
	 * @return  Returns the selectionSymbol.
	 */
	public ISymbol getSelectionSymbol() {
		return selectionSymbol;
	}

	/**
	 * @param selectionSymbol  The selectionSymbol to set.
	 */
	public void setSelectionSymbol(ISymbol selectionSymbol) {
		this.selectionSymbol = selectionSymbol;
	}
	/**
	 * Sets the style for the simple marker symbol
	 * @param style
	 */
	public void setStyle(int style) {
		this.markerStyle = style;
	}

	/**
	 * Obtains the style for the simple marker symbol
	 * @return markerStyle,int
	 */
	public int getStyle() {
		return markerStyle;
	}

	public Object clone() throws CloneNotSupportedException {
		SimpleMarkerSymbol copy = (SimpleMarkerSymbol) super.clone();

		// clone selection
		if (selectionSymbol != null) {
			copy.selectionSymbol = (ISymbol) selectionSymbol.clone();
		}

		return copy;
	}

	/**
	 * Returns if the marker symbol should be drawn outlined.
	 * @return if it is outlined
	 */
	public boolean isOutlined() {
		return outlined;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		// Set parent fill symbol properties
		super.loadFromState(state);

		// Set own properties
		setStyle(state.getInt(FIELD_MARKERSTYLE));
		setOutlined(state.getBoolean(FIELD_OUTLINED));
		if (isOutlined()) {
			setOutlineColor((Color) state.get(FIELD_OUTLINECOLOR));
			setOutlineSize(state.getDouble(FIELD_OUTLINESIZE));
		}
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		// Save parent fill symbol properties
		super.saveToState(state);

		// Save own properties
		state.set(FIELD_MARKERSTYLE, getStyle());
		state.set(FIELD_OUTLINED, isOutlined());
		if (isOutlined()) {
			state.set(FIELD_OUTLINECOLOR, getOutlineColor());
			state.set(FIELD_OUTLINESIZE, getOutlineSize());
		}
	}


	public static class RegisterPersistence implements Callable {

		public Object call() throws Exception {
			PersistenceManager manager = ToolsLocator.getPersistenceManager();
			if( manager.getDefinition(SIMPLE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
				DynStruct definition = manager.addDefinition(
						SimpleMarkerSymbol.class,
						SIMPLE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME,
						SIMPLE_MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
						null,
						null
				);
				// Extend the FillSymbol base definition
				definition.extend(manager.getDefinition(MARKER_SYMBOL_PERSISTENCE_DEFINITION_NAME));

				// Marker style
				definition.addDynFieldInt(FIELD_MARKERSTYLE).setMandatory(true);
				// Outlined?
				definition.addDynFieldBoolean(FIELD_OUTLINED).setMandatory(true);
				// Outline color
				definition.addDynFieldObject(FIELD_OUTLINECOLOR).setClassOfValue(Color.class);
				// Outline size
				definition.addDynFieldDouble(FIELD_OUTLINESIZE);
			}
			return Boolean.TRUE;
		}

	}

	public static class RegisterSymbol implements Callable {

		public Object call() throws Exception {
			int[] shapeTypes;
			SymbolManager manager = MapContextLocator.getSymbolManager();

	        shapeTypes =
	            new int[] { Geometry.TYPES.POINT, Geometry.TYPES.MULTIPOINT };
	        manager.registerSymbol(IMarkerSymbol.SYMBOL_NAME,
	            shapeTypes,
	            SimpleMarkerSymbol.class);

			return Boolean.TRUE;
		}

	}

}