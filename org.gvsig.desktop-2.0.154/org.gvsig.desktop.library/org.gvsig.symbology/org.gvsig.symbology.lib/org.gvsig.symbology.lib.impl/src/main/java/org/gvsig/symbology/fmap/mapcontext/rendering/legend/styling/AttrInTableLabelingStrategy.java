/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.batik.ext.awt.geom.PathLength;
import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsEnvelopeEvaluator;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.impl.SimpleTextSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.slf4j.Logger;

/**
 * LabelingStrategy used when the user wants to use label sizes, rotations, etc.
 * from the values included in fields of the datasource's table
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class AttrInTableLabelingStrategy implements IAttrInTableLabelingStrategy {

    private static final String FIELD_LABELING_METHOD = "labelingMethod";
    private static final String FIELD_ZOOM_CONSTRAINTS = "zoomConstraints";
    private static final String FIELD_COLOR_FONT = "colorFont";
    private static final String FIELD_REFERENCE_SYSTEM = "referenceSystem";
    private static final String FIELD_FIXED_COLOR = "fixedColor";
    private static final String FIELD_USE_FIXED_COLOR = "useFixedColor";
    private static final String FIELD_FIXED_SIZE = "fixedSize";
    private static final String FIELD_USE_FIXED_SIZE = "useFixedSize";
    private static final String FIELD_FONT = "font";
    private static final String FIELD_UNIT = "Unit";
    private static final String FIELD_ROTATION = "RotationField";
    private static final String FIELD_COLOR = "ColorField";
    private static final String FIELD_HEIGHT = "HeightField";
    private static final String FIELD_TEXT = "TextField";
    private static final Logger logger = LoggerFactory.getLogger(AttrInTableLabelingStrategy.class);
    public static final double MIN_TEXT_SIZE = 3;
    private static final String ATTR_IN_TABLE_LABELING_STRATEGY_PERSISTENCE_DEFINITION_NAME
            = "AttrInTableLabelingStrategy";
    private ILabelingMethod method = new DefaultLabelingMethod();
    private IZoomConstraints zoom;
    private FLyrVect layer;
//	private double unitFactor = 1D;
    private double fixedSize = 10;
    private Color fixedColor;
    private int unit = -1; //(pixel)
    private boolean useFixedSize;
    private boolean useFixedColor;
    private int referenceSystem;
//	private boolean isPrinting;
    private String[] usedFields = null;
    private Font font;
    private Color colorFont;
    private String textFieldName;
    private String rotationFieldName;
    private String heightFieldName;
    private String colorFieldName;

    public ILabelingMethod getLabelingMethod() {
        return this.method;
    }

    public void setLabelingMethod(ILabelingMethod method) {
        this.method = method;
    }

    public IPlacementConstraints getPlacementConstraints() {
        return null; // (automatically handled by the driver)
    }

    public void setPlacementConstraints(IPlacementConstraints constraints) {
        // nothing
    }

    public IZoomConstraints getZoomConstraints() {
        return zoom;
    }

    public void setZoomConstraints(IZoomConstraints constraints) {
        this.zoom = constraints;
    }

//    private double getScale(ViewPort vp, PrintAttributes patts)
//            throws ReadException {
//
//        double dpi = 0;
//        if (patts != null) {
//            int len = PrintAttributes.PRINT_QUALITY_DPI.length;
//            int priq = patts.getPrintQuality();
//            if (priq < 0 || priq >= len) {
//                dpi = vp.getDPI();
//            } else {
//                dpi = PrintAttributes.PRINT_QUALITY_DPI[priq];
//            }
//        } else {
//            dpi = vp.getDPI();
//        }
//
//        // ============== We have dpi now ====================
//        IProjection proj = vp.getProjection();
//
//        if (vp.getImageSize() == null || vp.getAdjustedEnvelope() == null) {
//            throw new ReadException("",
//                    new Exception("Viewport does not have image size or envelope"));
//        }
//
//        double[] trans2Meter = MapContext.getDistanceTrans2Meter();
//        if (proj == null) {
//            double wmeters = ((vp.getImageSize().width / dpi) * 0.0254);
//            return (long) ((trans2Meter[vp.getMapUnits()] * vp.getAdjustedEnvelope().getLength(0))
//                    / wmeters);
//        } else {
//            return Math.round(proj.getScale(
//                    vp.getAdjustedEnvelope().getMinimum(0)
//                    * trans2Meter[vp.getMapUnits()],
//                    vp.getAdjustedEnvelope().getMaximum(0)
//                    * trans2Meter[vp.getMapUnits()],
//                    vp.getImageSize().width,
//                    dpi));
//        }
//
//    }

    private void draw(BufferedImage image, Graphics2D g, double scale, ViewPort viewPort,
            Cancellable cancel, PrintAttributes props) throws ReadException {

//        double scale = getScale(viewPort, props);

        SimpleTextSymbol sym = new SimpleTextSymbol();
        sym.setFont(getFont());
        sym.setUnit(unit);
        sym.setReferenceSystem(referenceSystem);
        if (zoom == null
                || (zoom.isUserDefined() && (scale >= zoom.getMaxScale())
                && (scale <= zoom.getMinScale()))) {
            FeatureSet set = null;
            DisposableIterator iterator = null;
            try {
                FilteredLogger logger = new FilteredLogger(AttrInTableLabelingStrategy.logger, 10);
                
                // limit the labeling to the visible extent
                List<String> fields = new ArrayList<String>();
                int heightPos = -1;
                int rotationPos = -1;
                int textPos = -1;
                int colorPos = -1;

                if (!this.usesFixedSize()) {
                    if (getHeightField() != null) {
                        heightPos = fields.size();
                        fields.add(getHeightField());
                    }
                }
                if (getRotationField() != null) {
                    rotationPos = fields.size();
                    fields.add(getRotationField());
                }
                if (getTextField() != null) {
                    textPos = fields.size();
                    fields.add(getTextField());
                }

                if (!this.usesFixedColor() && getColorField() != null) {
                    colorPos = fields.size();
                    fields.add(getColorField());
                }

                FeatureStore featureStore = layer.getFeatureStore();
                ICoordTrans ct = layer.getCoordTrans();

                String geomName = featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName();
                fields.add(geomName);

                FeatureQuery featureQuery = layer.createFeatureQuery();

                if (viewPort.getAdjustedEnvelope().contains(
                        layer.getFullEnvelope())) {
                    /*
                     * viewport contains layer completely
                     */
                    set = featureStore.getFeatureSet(featureQuery);
                } else {
                    /*
                     * view port does not contain layer completely
                     */
                    IntersectsEnvelopeEvaluator iee = null;

                    IProjection data_proj = null;
                    Envelope env_in_store_crs = null;

                    if (ct == null) {
                        env_in_store_crs = viewPort.getAdjustedEnvelope();
                        data_proj = viewPort.getProjection();
                    } else {
                        env_in_store_crs = viewPort.getAdjustedEnvelope().convert(
                                ct.getInverted());
                        data_proj = ct.getPOrig();
                    }

                    iee = new IntersectsEnvelopeEvaluator(
                            env_in_store_crs,
                            data_proj,
                            featureStore.getDefaultFeatureType(),
                            geomName);
                    featureQuery.setAttributeNames((String[]) fields.toArray(new String[fields.size()]));
                    featureQuery.setFilter(iee);
                    set = featureStore.getFeatureSet(featureQuery);
                }

                /*
                 * 'set' now has the features that have to be labeled
                 */
                iterator = set.fastIterator();
                while (iterator.hasNext()) {
                    if (cancel.isCanceled()) {
                        return;
                    }
                    try {
                        Feature feature = (Feature) iterator.next();
                        Geometry geom = feature.getDefaultGeometry();
                        if( geom == null ) {
                            continue;
                        }
                        
                        double size = 0;
                        Color color = null;
                        if (useFixedSize) {
                            // uses fixed size
                            size = fixedSize;// * fontScaleFactor;
                        } else if (heightFieldName != null) {
                            // text size is defined in the table
                            try {
                                size = feature.getDouble(fields.get(heightPos));
                            } catch (Exception e) {
                                logger.warn("Null text height value for text '"+ feature.getString(fields.get(textPos))+"'.");
                                continue;
                            }
                        } else {
                            // otherwise will use the size in the symbol
                            size = sym.getFont().getSize();
                        }

                        double the_dpi = viewPort.getDPI();
                        if (props != null) {
                            int len = PrintAttributes.PRINT_QUALITY_DPI.length;
                            int priq = props.getPrintQuality();
                            if (priq < 0 || priq >= len) {
                                throw new ReadException(featureStore.getName(),
                                        new Exception("Bad print quality value: "+ priq));
                            } else {
                                the_dpi = PrintAttributes.PRINT_QUALITY_DPI[priq];
                            }
                        }

                        size = CartographicSupportToolkit.getCartographicLength(this,
                                        size,
                                        viewPort,
                                        the_dpi);
                        if (size <= MIN_TEXT_SIZE) {
						// label is too small to be readable, will be skipped
                            // this speeds up the rendering in wider zooms
                            continue;
                        }

                        /*
                         * ym.setFontSize(int) assumes dpi = 72
                         */
                        double font_size = size * 72 / the_dpi;
                        sym.setFontSize(font_size);

                        if (useFixedColor) {
                            color = fixedColor;
                        } else if (colorFieldName != null) {
                            // text size is defined in the table
                            try {
                                color = new Color(feature.getInt(fields.get(colorPos)));
                            } catch (ClassCastException ccEx) {
                                if (feature.get(fields.get(colorPos)) != null) {
                                    throw new ReadException("Unknown", ccEx);
                                }
                                logger.warn("Null color value for text '"+feature.getString(textFieldName)+"'.");
                                continue;
                            }
                        } else {
                            color = sym.getTextColor();
                        }

                        sym.setTextColor(color);

                        if (ct != null) {
                            /*
                             * Reproject from store CRS to view CRS
                             */
                            geom = geom.cloneGeometry();
                            geom.reProject(ct);
                        }

                        double rotation = 0D;
                        Point p = null;
                        if (rotationFieldName != null) {
                            // text rotation is defined in the table
                            rotation = -Math.toRadians(((Number) feature.get(fields.get(rotationPos))).doubleValue());
                            p = createLabelPoint(geom);
                        } else {
                            GeometryType gty = geom.getGeometryType();
                            if (gty.isTypeOf(Geometry.TYPES.CURVE)
                                    || gty.isTypeOf(Geometry.TYPES.MULTICURVE)) {

                                Geometry aux_geom = geom.cloneGeometry();
                                aux_geom.transform(viewPort.getAffineTransform());

                                PathLength pathLen = new PathLength(aux_geom);
                                float length = pathLen.lengthOfPath();
                                float distance = (float) (length * 0.4);
                                rotation = pathLen.angleAtLength(distance);
                                if (rotation < (-Math.PI / 2)) {
                                    rotation = rotation + Math.PI;
                                } else {
                                    if (rotation > (Math.PI / 2)) {
                                        rotation = rotation - Math.PI;
                                    }
                                }
                                double norm = -rotation + Math.PI / 2;

                                Point2D p2 = pathLen.pointAtLength(distance);
                                Point2D p2_anchor = new Point2D.Double();

                                try {
                                    viewPort.getAffineTransform().inverseTransform(
                                            p2, p2_anchor);
                                } catch (NoninvertibleTransformException e) {
                                    logger.warn("While computing anchor point", e);
                                }
                                double font_offset = 0.8 * font_size
                                        / viewPort.getAffineTransform().getScaleX();
                                p2_anchor.setLocation(
                                        /*
                                         * Move away from line
                                         */
                                        p2_anchor.getX() + font_offset * Math.cos(norm),
                                        p2_anchor.getY() + font_offset * Math.sin(norm));

                                p = GeometryLocator.getGeometryManager().createPoint(
                                        p2_anchor.getX(),
                                        p2_anchor.getY(), Geometry.SUBTYPES.GEOM2D);

                            } else {
                                p = createLabelPoint(geom);
                            }

                        }

                        sym.setText(feature.getString(fields.get(textPos)));
                        sym.setRotation(rotation);

                        if (p != null) {
                            if (props == null) {
                                sym.draw(g, viewPort.getAffineTransform(), p, feature, cancel);
                            } else {
                                sym.print(g, viewPort.getAffineTransform(), p, props);
                            }
                        }
                    } catch(Exception ex) {
                        logger.warn("",ex);
                    }
                }

            } catch (BaseException e) {
                throw new ReadException(
                        "Could not draw annotation in the layer.", e);
            } finally {
                if (iterator != null) {
                    iterator.dispose();
                }
                if (set != null) {
                    set.dispose();
                }

            }

        }
    }

    static class FilteredLogger {
        private int count = 0;
        private final Logger logger;
        private final int max;
        
        public FilteredLogger(Logger logger, int max) {
            this.max = max;
            this.logger = logger;
        }
        public void warn(String msg, Throwable th) {
            if( ++this.count < this.max ) {
                this.logger.warn(msg,th);
            } else if( this.count == this.max ) {
                this.logger.warn(msg,th);
                this.logger.warn("Too many errors, don't dump more in this process.");
            }
        }
        public void warn(String msg) {
            if( ++this.count < this.max ) {
                this.logger.warn(msg);
            } else if( this.count == this.max ) {
                this.logger.warn(msg);
                this.logger.warn("Too many errors, don't dump more in this process.");
            }
        }
        public void info(String msg) {
            if( ++this.count < this.max ) {
                this.logger.info(msg);
            } else if( this.count == this.max ) {
                this.logger.info(msg);
                this.logger.info("Too many errors, don't dump more in this process.");
            }
        }
    }
    
    public void draw(BufferedImage image, Graphics2D g, double scale, ViewPort viewPort,
            Cancellable cancel, double dpi) throws ReadException {
        draw(image, g, scale, viewPort, cancel, null);
    }

    private org.gvsig.fmap.geom.primitive.Point createLabelPoint(org.gvsig.fmap.geom.Geometry geom)
            throws CreateGeometryException {

        try {
            return geom.centroid();
        } catch (GeometryOperationNotSupportedException e) {
            return null;
        } catch (GeometryOperationException e) {
            return null;
        }
    }

//	public String getClassName() {
//		return getClass().getName();
//	}
    public String getRotationField() {
        return this.rotationFieldName;
    }

    public String getTextField() {
        return this.textFieldName;
    }

    public String getHeightField() {
        return this.heightFieldName;
    }

    public String getColorField() {
        return this.colorFieldName;
    }

    public void setTextField(String textFieldName) {
        this.textFieldName = textFieldName;
        this.usedFields = null;
    }

    public void setRotationField(String rotationFieldName) {
        this.rotationFieldName = rotationFieldName;
        this.usedFields = null;
    }

    /**
     * Sets the field that contains the size of the text. The size is computed
     * in meters. To use any other unit, call setUnit(int) with the scale factor
     * from meters (for centimeters, call <b>setUnitFactor(0.01))</b>.
     *
     * @param heightFieldName
     */
    public void setHeightField(String heightFieldName) {
        this.heightFieldName = heightFieldName;
        this.usedFields = null;
    }

    public void setColorField(String colorFieldName) {
        this.colorFieldName = colorFieldName;
    }

    public void print(Graphics2D g, double scale, ViewPort viewPort, Cancellable cancel,
            PrintAttributes props) throws ReadException {
        draw(null, g, scale, viewPort, cancel, props);
    }

    public void setUsesFixedSize(boolean b) {
        useFixedSize = b;
        this.usedFields = null;
    }

    public boolean usesFixedSize() {
        return useFixedSize;
    }

    public double getFixedSize() {
        return fixedSize;
    }

    public void setFixedSize(double fixedSize) {
        this.fixedSize = fixedSize;
        this.usedFields = null;
    }

    public void setUsesFixedColor(boolean b) {
        useFixedColor = b;
        this.usedFields = null;
    }

    public boolean usesFixedColor() {
        return useFixedColor;
    }

    public Color getFixedColor() {
        return fixedColor;
    }

    public void setFixedColor(Color fixedColor) {
        this.fixedColor = fixedColor;
    }

    public void setUnit(int unitIndex) {
        unit = unitIndex;

    }

    public int getUnit() {
        return unit;
    }

    public String[] getUsedFields() {
        if (this.usedFields == null) {
            List<String> v = new ArrayList<String>(4);
            if (!this.usesFixedSize()) {
                if (getHeightField() != null) {
                    v.add(getHeightField());
                }
            }
            if (getRotationField() != null) {
                v.add(getRotationField());
            }
            if (getTextField() != null) {
                v.add(getTextField());
            }

            if (!this.usesFixedColor() && getColorField() != null) {
                v.add(getColorField());
            }
            this.usedFields = (String[]) v.toArray(new String[v.size()]);
        }
        return this.usedFields;
    }

    public int getReferenceSystem() {
        return referenceSystem;
    }

    public void setReferenceSystem(int referenceSystem) {
        this.referenceSystem = referenceSystem;
    }

    public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
        // not required here
        throw new Error("Undefined in this context");
    }

    public void setCartographicSize(double cartographicSize, Geometry geom) {
        // not required here
        throw new Error("Undefined in this context");
    }

    public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
        // not required here
        throw new Error("Undefined in this context");

    }

    public void setLayer(FLayer layer) {
        this.layer = (FLyrVect) layer;
        if (layer == null) {
            return;
        }
        FeatureType type;
        try {
            type = this.layer.getFeatureStore().getDefaultFeatureType();
        } catch (DataException e) {
            throw new RuntimeException(this.getClass().getName(), e);
        }

        if (textFieldName != null) {
            if (type.getIndex(textFieldName) < 0) {
                // FIXME exception ??
            }
        }
        if (rotationFieldName != null) {
            if (type.getIndex(rotationFieldName) < 0) {
                // FIXME exception ??
            }
        }
        if (heightFieldName != null) {
            if (type.getIndex(heightFieldName) < 0) {
                // FIXME exception ??
            }
        }
        if (colorFieldName != null) {
            if (type.getIndex(colorFieldName) < 0) {
                // FIXME exception ??
            }
        }
    }

    public boolean shouldDrawLabels(double scale) {
        return layer.isWithinScale(scale);
    }

    public Color getColorFont() {
        return colorFont;
    }

    public void setColorFont(Color colorFont) {
        this.colorFont = colorFont;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font selFont) {
        this.font = selFont;
    }

    /* (non-Javadoc)
     * @see org.gvsig.tools.persistence.Persistent#loadFromState(org.gvsig.tools.persistence.PersistentState)
     */
    public void loadFromState(PersistentState state)
            throws PersistenceException {

        setTextField(state.getString(FIELD_TEXT));
        setHeightField(state.getString(FIELD_HEIGHT));
        setColorField(state.getString(FIELD_COLOR));
        setRotationField(state.getString(FIELD_ROTATION));
        setUnit(state.getInt(FIELD_UNIT));
        setFont((Font) state.get(FIELD_FONT));
        useFixedSize = state.getBoolean(FIELD_USE_FIXED_SIZE);
        setFixedSize(state.getDouble(FIELD_FIXED_SIZE));
        useFixedColor = state.getBoolean(FIELD_USE_FIXED_COLOR);
        setFixedColor((Color) state.get(FIELD_FIXED_COLOR));
        setReferenceSystem(state.getInt(FIELD_REFERENCE_SYSTEM));
        setColorFont((Color) state.get(FIELD_COLOR_FONT));
        setZoomConstraints((IZoomConstraints) state.get(FIELD_ZOOM_CONSTRAINTS));
        setLabelingMethod((ILabelingMethod) state.get(FIELD_LABELING_METHOD));
    }

    /* (non-Javadoc)
     * @see org.gvsig.tools.persistence.Persistent#saveToState(org.gvsig.tools.persistence.PersistentState)
     */
    public void saveToState(final PersistentState state) throws PersistenceException {
        state.set(FIELD_TEXT, getTextField());
        state.set(FIELD_HEIGHT, getHeightField());
        state.set(FIELD_COLOR, getColorField());
        state.set(FIELD_ROTATION, getRotationField());
        state.set(FIELD_UNIT, getUnit());
        state.set(FIELD_FONT, getFont());
        state.set(FIELD_USE_FIXED_SIZE, useFixedSize);
        state.set(FIELD_FIXED_SIZE, getFixedSize());
        state.set(FIELD_USE_FIXED_COLOR, useFixedColor);
        state.set(FIELD_FIXED_COLOR, getFixedColor());
        state.set(FIELD_REFERENCE_SYSTEM, getReferenceSystem());
        state.set(FIELD_COLOR_FONT, getColorFont());
        state.set(FIELD_ZOOM_CONSTRAINTS, getZoomConstraints());
        state.set(FIELD_LABELING_METHOD, getLabelingMethod());
    }

    public static class RegisterPersistence implements Callable {

        public Object call() throws Exception {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            if (manager.getDefinition(ATTR_IN_TABLE_LABELING_STRATEGY_PERSISTENCE_DEFINITION_NAME) == null) {
                DynStruct definition = manager.addDefinition(
                        AttrInTableLabelingStrategy.class,
                        ATTR_IN_TABLE_LABELING_STRATEGY_PERSISTENCE_DEFINITION_NAME,
                        ATTR_IN_TABLE_LABELING_STRATEGY_PERSISTENCE_DEFINITION_NAME + " Persistence definition",
                        null,
                        null
                );
                definition.addDynFieldString(FIELD_TEXT).setMandatory(true);
                definition.addDynFieldString(FIELD_HEIGHT).setMandatory(false);
                definition.addDynFieldString(FIELD_COLOR).setMandatory(false);
                definition.addDynFieldString(FIELD_ROTATION).setMandatory(false);
                definition.addDynFieldInt(FIELD_UNIT).setMandatory(true);
                definition.addDynFieldObject(FIELD_FONT).setMandatory(false).setClassOfValue(Font.class);
                definition.addDynFieldBoolean(FIELD_USE_FIXED_SIZE).setMandatory(true);
                definition.addDynFieldDouble(FIELD_FIXED_SIZE).setMandatory(false);
                definition.addDynFieldBoolean(FIELD_USE_FIXED_COLOR).setMandatory(true);
                definition.addDynFieldObject(FIELD_FIXED_COLOR).setMandatory(false).setClassOfValue(Color.class);
                definition.addDynFieldInt(FIELD_REFERENCE_SYSTEM).setMandatory(true);
                definition.addDynFieldObject(FIELD_COLOR_FONT).setMandatory(false).setClassOfValue(Color.class);
                definition.addDynFieldObject(FIELD_ZOOM_CONSTRAINTS).setMandatory(false).setClassOfValue(IZoomConstraints.class);
                definition.addDynFieldObject(FIELD_LABELING_METHOD).setMandatory(true).setClassOfValue(ILabelingMethod.class);
            }
            return Boolean.TRUE;
        }

    }
}
