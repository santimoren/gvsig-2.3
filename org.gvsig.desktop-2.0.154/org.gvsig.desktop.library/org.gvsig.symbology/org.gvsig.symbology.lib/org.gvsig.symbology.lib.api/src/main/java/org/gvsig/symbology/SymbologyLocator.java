/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

public class SymbologyLocator extends BaseLocator {

	public static final String SYMBOLOGY_MANAGER_NAME = "SymbologyManager";

	private static final String SYMBOLOGY_MANAGER_DESCRIPTION =
			"Manager of the basic symbology library";

	/**
	 * Unique instance.
	 */
	private static final SymbologyLocator instance = new SymbologyLocator();

	/**
	 * Return the singleton instance.
	 * 
	 * @return the singleton instance
	 */
	public static SymbologyLocator getInstance() {
		return instance;
	}

	/**
	 * Return a reference to the SymbologyManager.
	 * 
	 * @return a reference to the SymbologyManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static SymbologyManager getSymbologyManager()
			throws LocatorException {
		return (SymbologyManager) getInstance().get(SYMBOLOGY_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the SymbologyManager interface.
	 * 
	 * @param clazz
	 *            implementing the SymbologyManager interface
	 */
	public static void registerSymbologyManager(
			Class<? extends SymbologyManager> clazz) {
		getInstance().register(SYMBOLOGY_MANAGER_NAME,
				SYMBOLOGY_MANAGER_DESCRIPTION, clazz);
	}
}
