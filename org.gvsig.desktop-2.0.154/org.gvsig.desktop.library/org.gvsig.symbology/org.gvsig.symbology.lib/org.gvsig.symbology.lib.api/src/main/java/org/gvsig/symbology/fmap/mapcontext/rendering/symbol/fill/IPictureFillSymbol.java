/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill;

import java.io.IOException;
import java.net.URL;

import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;

public interface IPictureFillSymbol extends IFillSymbol{

	/**
	 * Sets the URL for the image to be used as a picture fill symbol (when it is selected in the map)
	 * @param imageFile, File
	 * @throws IOException
	 */
	public abstract void setSelImage(URL selImageUrl) throws IOException;

	/**
	 * Defines the URL from where the picture to fill the polygon is taken.
	 * @param imageFile
	 * @throws IOException
	 */
	public abstract void setImage(URL imageUrl) throws IOException;

	/**
	 * Returns the IMarkerFillProperties that allows this class to treat the picture as
	 * a marker in order to scale it, rotate it and so on.
	 * @return markerFillProperties,IMarkerFillPropertiesStyle
	 */
	public abstract IMarkerFillPropertiesStyle getMarkerFillProperties();

	/**
	 * Sets the MarkerFillProperties in order to allow the user to modify the picture as
	 * a marker (it is possible to scale it, rotate it and so on)
	 * @param prop
	 */
	public abstract void setMarkerFillProperties(IMarkerFillPropertiesStyle prop);

	/**
	 * Defines the angle for the rotation of the image when it is added to create the
	 * padding
	 *
	 * @return angle
	 */
	public abstract double getAngle();

	/**
	 * Sets the angle for the rotation of the image when it is added to create the padding
	 * @param angle
	 */
	public abstract void setAngle(double angle);

	/**
	 * Defines the scale for the x axis of the image when it is added to create the
	 * padding
	 * @return xScale
	 */
	public abstract double getXScale();

	/**
	 * Returns the scale for the x axis of the image when it is added to create the
	 * padding
	 * @param xScale
	 */
	public abstract void setXScale(double xScale);

	/**
	 * Defines the scale for the y axis of the image when it is added to create the
	 * padding
	 * @return yScale
	 */
	public abstract double getYScale();

	/**
	 * Returns the scale for the y axis of the image when it is added to create the
	 * padding
	 * @param yScale
	 */
	public abstract void setYScale(double yScale);

	/**
	 * Returns the URL of the image that is used to create the padding to fill the
	 * polygon
	 * @return imagePath
	 */
	public abstract URL getSource();

	/**
	 * Returns the URL of the image used when the polygon is selected
	 * @return
	 */
	public abstract URL getSelectedSource();
	
	
	/**
	 * Returns the style associated to the image used in this symbol
	 * 
	 * @return
	 */
	public IBackgroundFileStyle getBackgroundFileStyle();

	/**
	 * Returns the style associated to the image used in this symbol
	 * when the feature is selected
	 * 
	 * @return
	 */
	public IBackgroundFileStyle getSelectedBackgroundFileStyle();
	

}