/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public interface IArrowDecoratorStyle extends IStyle {

	/**
	 * Obtains the number of arrows that the user wants to draw in the same line.
	 * @return
	 */
	public int getArrowMarkerCount();

	/**
	 * Defines the number of arrows that the user wants to draw in the same line.
	 * @return
	 */
	public void setArrowMarkerCount(int arrowMarkerCount);

	/**
	 * Defines the flipAll attribute.If the value of this attribute is true all the
	 * arrows that we had drawn in the same line will be flipped.
	 * @return
	 */
	public boolean isFlipAll();

	/**
	 * Obtains the flipAll attribute.If the value of this attribute is true all the
	 * arrows that we had drawn in the same line will be flipped.
	 * @return
	 */
	public void setFlipAll(boolean flipAll);

	/**
	 * Obtains the flipFirst attribute.If it is true only the first arrow of the line
	 * will be flipped.The rest will keep the same orientation.
	 * @return
	 */
	public boolean isFlipFirst();

	/**
	 * Sets the flipFirst attribute.If it is true only the first arrow of the line
	 * will be flipped.The rest will keep the same orientation.
	 * @return
	 */
	public void setFlipFirst(boolean flipFirst);

	/**
	 * Gets the followLineAngle attribute.This attribute allows the arrow that we are
	 * going to draw to be more or less aligned with the line where it will be included (depending on the angle) .
	 * @return
	 */
	public boolean isFollowLineAngle();

	/**
	 * Sets the followLineAngle attribute.This attribute allows the arrow that we are
	 * going to draw to be more or less aligned with the line where it will be included.
	 * (depending on the angle).
	 * @param followingLineAngle
	 * @return
	 */
	public void setFollowLineAngle(boolean followLineAngle);
	/**
	 * Draws an arrow(or other symbol that substitutes an arrow selected by the user)
	 * in a line.When the line is drawn, the symbol is added and takes care of the different
	 * options of the user(for example if he wants to flip the first symbol or all and
	 * the number of symbols per line to be drawn)
	 * @param g
	 * @param affineTransform
	 * @param feature 
	 * @param shp
	 * @throws CreateGeometryException 
	 */
	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature feature) throws CreateGeometryException;
	
	public IMarkerSymbol getMarker();

	public void setMarker(IMarkerSymbol marker);
}
