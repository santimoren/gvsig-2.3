/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: IFillSymbol.java 21071 2008-06-02 10:55:35Z vcaballero $
 * $Log$
 * Revision 1.3  2007-08-13 11:36:50  jvidal
 * javadoc
 *
 * Revision 1.2  2007/03/09 11:20:56  jaume
 * Advanced symbology (start committing)
 *
 * Revision 1.1.2.2  2007/02/21 16:09:02  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2007/02/16 10:54:12  jaume
 * multilayer splitted to multilayerline, multilayermarker,and  multilayerfill
 *
 *
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill;

import java.awt.Color;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;


/**
 * Interface that extends ISymbol interface in order to define methods for
 * fill symbols which can manage specific attributes of them.
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */

public interface IFillSymbol extends ISymbol, CartographicSupport{
	
	public static final String SYMBOL_NAME = "fill";

	public abstract boolean isSuitableFor(Geometry geom);

	public abstract int getOnePointRgb();

	/**
	 * Sets the color that will be used to draw the filling pattern of this symbol.
	 *
	 * @param Color
	 */
	public abstract void setFillColor(Color color);

	/**
	 * Sets the color of the outline.
	 * @deprectated will be substituted by setOutline(AbstractLineSymbol);
	 * @param color
	 */
	public abstract void setOutline(ILineSymbol outline);

	/**
	 * @return Returns the color.
	 */
	public abstract Color getFillColor();

	/**
	 * Obtains the ILineSymbol interface of the outline
	 * @return the outline,ILineSymbol.
	 */
	public abstract ILineSymbol getOutline();
	/**
	 * Obtains the transparency of the fill symbol
	 * @return the transparency of the fill symbol
	 */

	public abstract int getFillAlpha();

	public abstract boolean hasFill();

	public abstract void setHasFill(boolean hasFill);

	public abstract boolean hasOutline();

	public abstract void setHasOutline(boolean hasOutline);
}