/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker;

import java.awt.Color;

import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;

public interface ISimpleMarkerSymbol extends IMarkerSymbol{

	/**
	 * Returns true or false depending if the simple marker symbol has an outline or not.
	 * @return Returns the outline.
	 */
	public abstract boolean hasOutline();

	/**
	 * Establishes the outline for the simple marker symbol.
	 * @param outline  The outline to set.
	 */
	public abstract void setOutlined(boolean outlined);

	/**
	 * Returns the outline color for the symple marker symbol
	 *
	 * @return Color,outlineColor.
	 */
	public abstract Color getOutlineColor();

	/**
	 * Sets the outline color for the simple marker symbol
	 * @param outlineColor, Color
	 */
	public abstract void setOutlineColor(Color outlineColor);

	/**
	 * Gets the size of the outline for the simple marker symbol
	 * @return  Returns the outlineSize.
	 */
	public abstract double getOutlineSize();

	/**
	 * Establishes the size for the outline of the simple marker symbol
	 * @param outlineSize  The outlineSize to set.
	 */
	public abstract void setOutlineSize(double outlineSize);

	/**
	 * @return  Returns the selectionSymbol.
	 */
	public abstract ISymbol getSelectionSymbol();

	/**
	 * @param selectionSymbol  The selectionSymbol to set.
	 */
	public abstract void setSelectionSymbol(ISymbol selectionSymbol);

	/**
	 * Sets the style for the simple marker symbol
	 * @param style
	 */
	public abstract void setStyle(int style);

	/**
	 * Obtains the style for the simple marker symbol
	 * @return markerStyle,int
	 */
	public abstract int getStyle();

	/**
	 * Returns if the marker symbol should be drawn outlined.
	 * @return if it is outlined
	 */
	public abstract boolean isOutlined();

}