/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling;

import java.awt.Color;
import java.awt.Font;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;

public interface IAttrInTableLabelingStrategy extends ILabelingStrategy, CartographicSupport{

	public abstract String getRotationField();

	public abstract String getTextField();

	public abstract String getHeightField();

	public abstract String getColorField();

	public abstract void setTextField(String textFieldName);

	public abstract void setRotationField(String rotationFieldName);

	/**
	 * Sets the field that contains the size of the text. The size is computed
	 * in meters. To use any other unit, call setUnit(int) with the scale factor from meters
	 * (for centimeters, call <b>setUnitFactor(0.01))</b>.
	 * @param heightFieldName
	 */
	public abstract void setHeightField(String heightFieldName);

	public abstract void setColorField(String colorFieldName);

	public abstract void setUsesFixedSize(boolean b);

	public abstract boolean usesFixedSize();

	public abstract double getFixedSize();

	public abstract void setFixedSize(double fixedSize);

	public abstract void setUsesFixedColor(boolean b);

	public abstract boolean usesFixedColor();

	public abstract Color getFixedColor();

	public abstract void setFixedColor(Color fixedColor);

	public abstract Color getColorFont();

	public abstract void setColorFont(Color colorFont);

	public abstract Font getFont();

	public abstract void setFont(Font selFont);

}