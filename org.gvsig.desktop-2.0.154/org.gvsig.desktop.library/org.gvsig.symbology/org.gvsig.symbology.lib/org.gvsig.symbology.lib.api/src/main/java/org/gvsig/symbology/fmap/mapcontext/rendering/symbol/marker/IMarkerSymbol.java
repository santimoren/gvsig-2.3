/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;

/**
 * Represents an ISymbol that draws a <b>marker symbol</b>.It has the different methods to obtain or
 * define the attributes of a marker such as rotation, offset, size, color and
 * IMask.
 *
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface IMarkerSymbol extends ISymbol, CartographicSupport {

	public static final String SYMBOL_NAME = "marker";

	public static final int CIRCLE_STYLE = 0;
	public static final int SQUARE_STYLE = 1;
	public static final int CROSS_STYLE = 2;
	public static final int DIAMOND_STYLE = 3;
	public static final int X_STYLE = 4;
	public static final int TRIANGLE_STYLE = 5;
	public static final int STAR_STYLE = 6;
	public static final int VERTICAL_LINE_STYLE = 7;

	/**
	 * Returns the rotation (in radians, counter-clockwise) of the marker symbol
	 * @return double (rotation)
	 */
	public abstract double getRotation();
	/**
	 * Sets the rotation of the marker symbol
	 * @param rotation
	 */
	public abstract void setRotation(double rotation);
	/**
	 * Gets the offset for a marker symbol
	 * @return Point2D
	 */
	public abstract Point2D getOffset();
	/**
	 * Establishes the offset currently set for the marker symbol.
	 * @param offset
	 */
	public abstract void setOffset(Point2D offset);
	/**
	 * Obtains the size of a marker symbol
	 *
	 */
	public abstract double getSize();
	/**
	 * Sets the size of marker symbol
	 * @param size
	 */
	public abstract void setSize(double size);

	/**
	 * Returns the color of the marker symbol.
	 * @return Color
	 */
	public abstract Color getColor();

	/**
	 * Establishes a color for the marker symbol
	 * @param color
	 */
	public abstract void setColor(Color color);

	/**
	 * Defines the transparency of a line symbol.
	 *
	 * @param outlineAlpha
	 *            , the transparency
	 */
	void setAlpha(int alpha);

	/**
	 *
	 * @return the mask of the symbol
	 */
	public abstract IMask getMask();
	/**
	 * Defines a mask for the symbol
	 * @param mask,IMask
	 */
	public abstract void setMask(IMask mask);

}