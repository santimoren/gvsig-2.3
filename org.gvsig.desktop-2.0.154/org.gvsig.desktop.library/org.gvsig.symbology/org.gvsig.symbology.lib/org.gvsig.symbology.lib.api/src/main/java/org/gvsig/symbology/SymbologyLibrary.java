/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {gvSIG}  {{Task}}
 */
package org.gvsig.symbology;

import org.gvsig.fmap.mapcontext.MapContextLibrary;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Library for the Basic Symbology extended API.
 * 
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class SymbologyLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsAPI(SymbologyLibrary.class);
        require(MapContextLibrary.class);
    }

	protected void doInitialize() throws LibraryException {
		// Nothing to do
	}

	protected void doPostInitialize() throws LibraryException {
		// Validate there is any implementation registered.
		SymbologyManager manager = SymbologyLocator.getSymbologyManager();
		if (manager == null) {
			throw new ReferenceNotRegisteredException(
					SymbologyLocator.SYMBOLOGY_MANAGER_NAME,
					SymbologyLocator.getInstance());
		}

		MapContextManager mapContextManager = MapContextLocator
				.getMapContextManager();

		// Default vector legend
		mapContextManager
				.setDefaultVectorLegend(ISingleSymbolLegend.LEGEND_NAME);
	}
}