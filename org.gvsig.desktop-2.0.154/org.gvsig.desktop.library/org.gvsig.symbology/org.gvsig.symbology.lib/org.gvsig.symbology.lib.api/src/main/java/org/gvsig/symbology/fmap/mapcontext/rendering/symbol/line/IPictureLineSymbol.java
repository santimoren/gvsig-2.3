/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line;

import java.io.IOException;
import java.net.URL;


public interface IPictureLineSymbol extends ILineSymbol{

	/**
	 * Sets the URL for the image to be used as a picture line symbol
	 * @param imageFile, File
	 * @throws IOException
	 */
	public abstract void setImage(URL imageUrl) throws IOException;

	/**
	 * Sets the URL for the image to be used as a picture line symbol (when it is selected in the map)
	 * @param imageFile, File
	 * @throws IOException
	 */
	public abstract void setSelImage(URL selImageUrl) throws IOException;

	/**
	 * Sets the yscale for the picture line symbol
	 * @param yScale
	 */
	public abstract void setYScale(double yScale);

	/**
	 * Sets the xscale for the picture line symbol
	 * @param xScale
	 */
	public abstract void setXScale(double xScale);

	/**
	 * Returns the URL of the image that is used as a picture line symbol (when it
	 * is selected in the map)
	 * @return selimagePath,URL
	 */
	public abstract URL getSelectedSource();

	/**
	 * Returns the URL of the image that is used as a picture line symbol
	 * @return imagePath,URL
	 */
	public abstract URL getSource();

	/**
	 * Returns the xscale for the picture line symbol
	 * @param xScale
	 */
	public abstract double getXScale();

	/**
	 * Returns the yscale for the picture line symbol
	 * @param yScale
	 */
	public abstract double getYScale();

}