/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker;

import java.io.IOException;
import java.net.URL;

public interface IPictureMarkerSymbol extends IMarkerSymbol{

	/**
	 * Sets the file for the image to be used as a marker symbol
	 * 
	 * @param imageFile
	 *            , File
	 * @throws IOException
	 */
	public abstract void setImage(URL imageUrl) throws IOException;

	/**
	 * Sets the file for the image to be used as a marker symbol (when it is
	 * selected in the map)
	 * 
	 * @param imageFile
	 *            , File
	 * @throws IOException
	 */
	public abstract void setSelImage(URL imageFileUrl) throws IOException;

	// public void print(Graphics2D g, AffineTransform at, FShape shape)
	// throws ReadDriverException {
	// // TODO Implement it
	// throw new Error("Not yet implemented!");
	//
	// }
	/**
	 * Returns the URL of the image that is used as a marker symbol
	 * @return imagePath,URL
	 */
	public abstract URL getSource();

	/**
	 * Returns the URL of the image that is used as a marker symbol (when it
	 is selected in the map)
	 * @return selimagePath,URL
	 */
	public abstract URL getSelectedSource();

}