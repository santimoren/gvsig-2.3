/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.symbology;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import org.gvsig.fmap.mapcontext.rendering.legend.IInterval;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClassFactory;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.IAttrInTableLabelingStrategy;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.IMultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IMarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IPictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.ISimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.IPictureLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ISimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.ISimpleMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ISimpleLineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.ISimpleTextSymbol;

/**
 * Basic symbology manager.
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public interface SymbologyManager {

    /**
     * Creates a new interval with the given limit values
     *
     * @param min the minimum included interval value
     * @param max the maximum included interval value
     * @return a new interval
     */
    IInterval createInterval(double min, double max);

    IArrowDecoratorStyle createArrowDecoratorStyle();

//	IMarkerSymbol createMarkerSymbol();
    ISimpleMarkerSymbol createSimpleMarkerSymbol();

    IMask createMask();

    IMultiShapeSymbol createMultiShapeSymbol();

    ISimpleFillSymbol createSimpleFillSymbol();

    ISimpleLineSymbol createSimpleLineSymbol();

    ISimpleLineStyle createSimpleLineStyle();

    ISimpleTextSymbol createSimpleTextSymbol();

    IMarkerFillPropertiesStyle createSimpleMarkerFillPropertiesStyle();

    IBackgroundFileStyle createBackgroundFileStyle(URL imgURL) throws IOException;

    IPictureFillSymbol createPictureFillSymbol(URL imageURL, URL selImageURL) throws IOException;

    IPictureLineSymbol createPictureLineSymbol(URL imageURL, URL selImageURL) throws IOException;

    IPictureMarkerSymbol createPictureMarkerSymbol(URL imageURL, URL selImageURL) throws IOException;

    IMarkerFillSymbol createMarkerFillSymbol();

    IAttrInTableLabelingStrategy createAttrInTableLabelingStrategy();

    ILabelStyle createDefaultLabelStyle();

    ILabelingStrategy createDefaultLabelingStrategy();

    ILabelingMethod createDefaultLabelingMethod();

    IZoomConstraints createDefaultZoomConstraints();

    public void registerLabelClass(ILabelClassFactory factory);

    public Collection<ILabelClassFactory> getLabelClassFactories();

    public ILabelClassFactory getLabelClassFactory(String id);

    /**
     * Creates an instance of a class (provided by this manager) which
     * implements ILabelClass.
     *
     */
    ILabelClass createDefaultLabel();
    
    public ILabelClassFactory getDefaultLabelFactory();

    public void setDefaultLabelFactory(String id);

    public void setDefaultLabelFactory(ILabelClassFactory factory);

}
