/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main executable class for testing the Symbology library.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private SymbologyManager manager;
    private SymbologySwingManager swingManager;

    public static void main(String args[]) {
        new DefaultLibrariesInitializer().fullInitialize();
        Main main = new Main();
        main.show();
    }

    @SuppressWarnings("serial")
    public void show() {
        manager = SymbologyLocator.getSymbologyManager();
        swingManager = SymbologySwingLocator.getSwingManager();

        Action showCookie = new AbstractAction("Get a Symbology") {

            public void actionPerformed(ActionEvent e) {
                showSymbology(manager);
            }
        };

        Action exit = new AbstractAction("Exit") {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        };

        JFrame frame = new JFrame("Symbology example app");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Create the menu bar.
        JMenuBar menuBar = new JMenuBar();

        // Build the menu.
        JMenu menuFile = new JMenu("File");
        menuFile.add(new JMenuItem(showCookie));
        menuFile.add(new JMenuItem(exit));

        menuBar.add(menuFile);

        JToolBar toolBar = new JToolBar();
        toolBar.add(new JButton(showCookie));
        toolBar.add(new JButton(exit));

        frame.setPreferredSize(new Dimension(200, 100));
        frame.setJMenuBar(menuBar);
        frame.add(toolBar, BorderLayout.PAGE_START);

        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public void showSymbology(SymbologyManager manager) {
//        try {
//            SymbologyService service =
//                (SymbologyService) manager.getSymbologyService();
//            JSymbologyServicePanel panel =
//                swingManager.createSymbology(service);
//            swingManager.getWindowManager().showWindow(panel, "Symbology",
//                SymbologyWindowManager.MODE_WINDOW);
//
//        } catch (ServiceException e) {
//            LOG.error("Error showing a Symbology", e);
//        }
    }

}
