/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.swing.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.gvsig.app.gui.labeling.LabelClassEditor;
import org.gvsig.app.gui.labeling.LabelClassEditorFactory;

import org.gvsig.app.gui.styling.TypeSymbolEditor;
import org.gvsig.app.project.documents.view.legend.gui.ILabelingStrategyPanel;
import org.gvsig.app.project.documents.view.legend.gui.ILegendPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClassFactory;
import org.gvsig.gui.ColorTablePainter;
import org.gvsig.gui.ColorTablesFactory;
import org.gvsig.gui.DefaultColorTablesFactory;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.swing.SymbologySwingManager;
import org.gvsig.symbology.swing.SymbologyWindowManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the {@link SymbologySwingManager}.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultSymbologySwingManager implements
        SymbologySwingManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSymbologySwingManager.class);

    private SymbologyManager manager;
    private I18nManager i18nmanager = null;
    private SymbologyWindowManager windowManager;
    private ColorTablesFactory colorTablesFactory;

    private static Map<Integer, List<Class<? extends TypeSymbolEditor>>> symbolEditorRegistry;
    private List<Class<? extends ILegendPanel>> legendEditorRegistry = null;
    private List<Class<? extends ILabelingStrategyPanel>> labelingEditorRegistry = null;
    private Map<String,LabelClassEditorFactory> labelClassEditorFactories = new HashMap<String, LabelClassEditorFactory>();

    public DefaultSymbologySwingManager() {
        this.manager = SymbologyLocator.getSymbologyManager();
        this.windowManager = new DefaultSymbologyWindowManager();
        this.colorTablesFactory = new DefaultColorTablesFactory();

        this.legendEditorRegistry = new ArrayList<Class<? extends ILegendPanel>>();
        this.labelingEditorRegistry = new ArrayList<Class<? extends ILabelingStrategyPanel>>();
    }

    public SymbologyManager getManager() {
        return this.manager;
    }

    public String getTranslation(String key) {
        if (this.i18nmanager == null) {
            this.i18nmanager = ToolsLocator.getI18nManager();
        }
        return this.i18nmanager.getTranslation(key);
    }

    public void registerWindowManager(SymbologyWindowManager manager) {
        this.windowManager = manager;
    }

    public SymbologyWindowManager getWindowManager() {
        return this.windowManager;
    }

    public void setColorTablesFactory(ColorTablesFactory factory) {
        colorTablesFactory = factory;
    }

    public List<ColorTablePainter> createColorTables() {
        if (colorTablesFactory != null) {
            return colorTablesFactory.createColorTables();
        }
        return null;
    }

    public ColorTablesFactory getColorTablesFactory() {
        return this.colorTablesFactory;
    }

    /**
     * @deprecated use registerTypeSymbolEditor
     */
    public void addSymbolEditorPanel(Class abstractTypeSymbolEditorPanelClass, int shapeType) {
        Class<? extends TypeSymbolEditor> symbolEditor = abstractTypeSymbolEditorPanelClass;
        this.registerSymbolEditor(symbolEditor, shapeType);
    }

    public void registerSymbolEditor(Class<? extends TypeSymbolEditor> symbolEditor, int shapeType) {
        if (symbolEditorRegistry == null) {
            symbolEditorRegistry = new HashMap<Integer, List<Class<? extends TypeSymbolEditor>>>();
        }

        Integer key = new Integer(shapeType);
        List<Class<? extends TypeSymbolEditor>> l = symbolEditorRegistry.get(key);
        if (l == null) {
            l = new ArrayList<Class<? extends TypeSymbolEditor>>();
        }
        l.add(symbolEditor);

        symbolEditorRegistry.put(key, l);
    }

    public List<Class<? extends TypeSymbolEditor>> getSymbolEditorClassesByGeometryType(GeometryType geometryType) {
        if (symbolEditorRegistry == null) {
            return Collections.emptyList();
        }
        Iterator it = symbolEditorRegistry.keySet().iterator();
        while (it.hasNext()) {
            int currentType = (Integer) it.next();
            if (geometryType.isTypeOf(currentType)) {
                return (List) symbolEditorRegistry.get(currentType);
            }
        }
        return Collections.emptyList();
    }

    public void registerLegendEditor(Class<? extends ILegendPanel> legendEditor) {
        if (this.legendEditorRegistry == null) {
            this.legendEditorRegistry = new ArrayList<Class<? extends ILegendPanel>>();
        }
        if (!this.legendEditorRegistry.contains(legendEditor)) {
            this.legendEditorRegistry.add(legendEditor);
        }
    }

    public List<Class<? extends ILegendPanel>> getLegendEditorClasses() {
        return Collections.unmodifiableList(this.legendEditorRegistry);
    }

    public List<ILegendPanel> getLegendEditors(FLayer layer) {
        List<ILegendPanel> editors = new ArrayList<ILegendPanel>();
        Class<? extends ILegendPanel> legendEditorClass = null;
        ILegendPanel editor = null;

        for (int i = 0; i < legendEditorRegistry.size(); i++) {
            legendEditorClass = legendEditorRegistry.get(i);
            try {
                editor = null;
                editor = (ILegendPanel) legendEditorClass.newInstance();
            } catch (Exception e) {
                logger.info("Unable to instantiate legend editor.", e);
            }
            if (editor != null && editor.isSuitableFor(layer)) {
                editors.add(editor);
            }
        }
        return editors;
    }

    public void registerLabelingEditor(Class<? extends ILabelingStrategyPanel> labelingEditor) {
        if (!this.labelingEditorRegistry.contains(labelingEditor)) {
            this.labelingEditorRegistry.add(labelingEditor);
        }
    }

    public List<ILabelingStrategyPanel> getLabelingEditors() {
        List<ILabelingStrategyPanel> labelingEditors = new ArrayList<ILabelingStrategyPanel>();
        Iterator<Class<? extends ILabelingStrategyPanel>> it = this.labelingEditorRegistry.iterator();
        while (it.hasNext()) {
            Class<? extends ILabelingStrategyPanel> labelingEditorClass = it.next();
            try {
                ILabelingStrategyPanel labelingEditor = labelingEditorClass.newInstance();
                labelingEditors.add(labelingEditor);
            } catch (Exception ex) {
                String msg = "Can't create the labeling editor associated to '"+labelingEditorClass.getName()+"'.";
                logger.warn(msg,ex);
                throw new RuntimeException(msg,ex);
            }
        }
        return labelingEditors;
    }

    public void registerLabelClassEditor(LabelClassEditorFactory factory) {
        this.labelClassEditorFactories.put(factory.getID().toLowerCase(), factory);
    }

    public Collection<LabelClassEditorFactory> getLabelClassEditorFactories() {
        return Collections.unmodifiableCollection( this.labelClassEditorFactories.values());
    }

    public LabelClassEditorFactory getLabelClassEditorFactory(String id) {
        if( id==null ) {
            return null;
        }
        return this.labelClassEditorFactories.get(id.toLowerCase());
    }

    public LabelClassEditorFactory getLabelClassEditorFactory(ILabelClass labelClass) {
        Iterator<LabelClassEditorFactory> it = this.labelClassEditorFactories.values().iterator();
        while( it.hasNext() ) {
            LabelClassEditorFactory labelClassEditorFactory = it.next();
            if( labelClassEditorFactory!=null && labelClassEditorFactory.accept(labelClass.getClass()) ) {
                return labelClassEditorFactory;
            }
        }
        return null;
    }

    public LabelClassEditor createLabelClassEditor(ILabelClass labelClass, FeatureStore store) {
        LabelClassEditorFactory f = this.getLabelClassEditorFactory(labelClass);
        if( f == null ) {
            return null;
        }
        return f.createEditor(labelClass, store);
    }
    
}
