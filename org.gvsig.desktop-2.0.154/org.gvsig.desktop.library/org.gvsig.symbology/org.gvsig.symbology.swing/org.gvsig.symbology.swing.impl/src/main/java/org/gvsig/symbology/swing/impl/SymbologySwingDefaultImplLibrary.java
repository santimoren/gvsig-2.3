/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.swing.impl;


import org.gvsig.symbology.swing.SymbologySwingLibrary;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Library for default swing implementation initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class SymbologySwingDefaultImplLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsImplementationOf(SymbologySwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
        SymbologySwingLocator
            .registerSwingManager(DefaultSymbologySwingManager.class);
        
        // LineProperties
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-bevel", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-bevel-selected", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-miter", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-miter-selected", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-round", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-join-round-selected", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-butt", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-butt-selected", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-round", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-round-selected", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-square", this);
        IconThemeHelper.registerIcon("line-properties", "line-properties-cap-square-selected", this);
        // SymbolLayerManager
        IconThemeHelper.registerIcon("symbol-editor", "symbol-editor-unlocked", this);
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        // Nothing to do
    }

}
