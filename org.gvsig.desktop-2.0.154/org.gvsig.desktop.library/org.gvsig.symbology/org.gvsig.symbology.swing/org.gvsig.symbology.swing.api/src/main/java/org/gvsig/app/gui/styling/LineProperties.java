/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.gui.beans.DefaultBean;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.i18n.Messages;


public class LineProperties extends DefaultBean implements ActionListener  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6356385079456043011L;
	private JRadioButton joinBevel;
	private JRadioButton joinMiter;
	private JRadioButton joinRound;
	private JRadioButton capBut;
	private JRadioButton capRound;
	private JRadioButton capSquare;
	private PatternEditor pe;
	private JButton clearButton;
	private float width;


	public LineProperties(float lineWidth) {
		super();
		this.width=lineWidth;
		initialize();
	}

	private void initialize() {
		setName(Messages.getText("line_properties"));
		setLayout(new BorderLayout(10, 10));

		JPanel aux2 = new JPanel();
		JPanel pnlJoin = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 0));


		pnlJoin.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), Messages.getText("join_style")+":"));

		JPanel d = new JPanel();
		JPanel toCenter;
		d.setLayout(new BoxLayout(d, BoxLayout.Y_AXIS));
		joinBevel = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-join-bevel"));
		joinBevel.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-join-bevel-selected"));
		FlowLayout flowCenteredLayout = new FlowLayout(FlowLayout.CENTER, 0, 0);
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(joinBevel);
		d.add(toCenter);

		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("join_bevel")));
		d.add(toCenter);

		pnlJoin.add(d);


		d = new JPanel();
		d.setLayout(new BoxLayout(d, BoxLayout.Y_AXIS));
		joinMiter = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-join-miter"));
		joinMiter.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-join-miter-selected"));
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(joinMiter);
		d.add(toCenter);

		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("join_miter")));
		d.add(toCenter);
		pnlJoin.add(d);



		d = new JPanel();
		d.setLayout(new BoxLayout(d, BoxLayout.Y_AXIS));
		joinRound = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-join-round"));
		joinRound.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-join-round-selected"));
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(joinRound);
		d.add(toCenter);
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("join_round")));
		d.add(toCenter);
		pnlJoin.add(d);


		ButtonGroup groupJoin = new ButtonGroup();
		groupJoin.add(joinBevel);
		groupJoin.add(joinMiter);
		groupJoin.add(joinRound);
		aux2.add(pnlJoin);

		JPanel aux3 = new JPanel();

		JPanel pnlCap = new JPanel(new FlowLayout(FlowLayout.LEFT, 30, 0));
		pnlCap.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), Messages.getText("end_style")+":"));

		JPanel c = new JPanel();
		c.setLayout(new BoxLayout(c, BoxLayout.Y_AXIS));
		capBut = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-cap-butt"));
		capBut.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-cap-butt-selected"));
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(capBut);
		c.add(toCenter);
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("cap_butt")));
		c.add(toCenter);
		pnlCap.add(c);

		JPanel a = new JPanel();
		a.setLayout(new BoxLayout(a, BoxLayout.Y_AXIS));
		capRound = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-cap-round"));
		capRound.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-cap-round-selected"));
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(capRound);
		a.add(toCenter);
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("cap_round")));
		a.add(toCenter);
		pnlCap.add(a);


		JPanel b = new JPanel();
		b.setLayout(new BoxLayout(b, BoxLayout.Y_AXIS));
		capSquare = new JRadioButton(IconThemeHelper.getImageIcon("line-properties-cap-square"));
		capSquare.setSelectedIcon(IconThemeHelper.getImageIcon("line-properties-cap-square-selected"));
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(capSquare);

		b.add(toCenter);
		toCenter = new JPanel(flowCenteredLayout);
		toCenter.add(new JLabel(Messages.getText("cap_square")));
		b.add(toCenter);
		pnlCap.add(b);

		ButtonGroup groupCap = new ButtonGroup();
		groupCap.add(capBut);
		groupCap.add(capRound);
		groupCap.add(capSquare);
		aux3.add(pnlCap);

		JPanel aux4 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		aux4.add(new JBlank(5,40));
		pe = new PatternEditor();
		pe.setPreferredSize(new Dimension(440,40));
		aux4.add(pe);
		aux4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), Messages.getText("fill_pattern")+":"));

		JPanel aux5 = new JPanel(new FlowLayout(FlowLayout.LEFT,15, 0));
		clearButton = new JButton(Messages.getText("clear"));
		aux5.add(clearButton);

		joinBevel.addActionListener(this);
		joinMiter.addActionListener(this);
		joinRound.addActionListener(this);
		capBut.addActionListener(this);
		capRound.addActionListener(this);
		capSquare.addActionListener(this);
		clearButton.addActionListener(clear);
		pe.addActionListener(patternChange);

		JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux.add(aux2, BorderLayout.CENTER);
		aux.add(aux3, BorderLayout.CENTER);
		aux.add(aux4, BorderLayout.CENTER);
		aux.add(aux5, BorderLayout.WEST);
		add(aux, BorderLayout.CENTER);
	}

	public void actionPerformed(ActionEvent e) {
		callValueChanged(getLinePropertiesStyle());
	}

	private ActionListener patternChange = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			callValueChanged(getLinePropertiesStyle());
		}
	};

	private ActionListener clear = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			pe.clear_Dash();
			callValueChanged(getLinePropertiesStyle());
		}
	};



	public void setLinePropertiesStyle(BasicStroke str) {

		if (str == null) {
			str = new BasicStroke();
		}

		switch(str.getLineJoin()) {
		case 0:
			joinMiter.setSelected(true);
			break;
		case 1:
			joinRound.setSelected(true);
			break;
		case 2:
			joinBevel.setSelected(true);
			break;
		}

		switch(str.getEndCap()) {
		case 0:
			capBut.setSelected(true);
			break;
		case 1:
			capRound.setSelected(true);
			break;
		case 2:
			capSquare.setSelected(true);
			break;
		}

		pe.setDash(str.getDashArray());
		pe.repaint();
	}

	public BasicStroke getLinePropertiesStyle() {
		int capType=0, joinType=0;


		if (capBut.isSelected())			capType = 0;
		else if (capRound.isSelected())		capType = 1;
		else if (capSquare.isSelected())	capType = 2;

		if (joinMiter.isSelected())			joinType = 0;
		else if (joinRound.isSelected())	joinType = 1;
		else if (joinBevel.isSelected())	joinType = 2;

		float [] dash = pe.getDash();

		BasicStroke str = new BasicStroke(width, capType, joinType, 10, pe.getDash(), 0);
		pe.setDash(dash);
		return str;
	}
}
