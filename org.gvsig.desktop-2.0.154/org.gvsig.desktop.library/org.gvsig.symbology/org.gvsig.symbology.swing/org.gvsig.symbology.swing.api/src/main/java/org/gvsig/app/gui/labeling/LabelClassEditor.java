
package org.gvsig.app.gui.labeling;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.tools.swing.api.Component;

public interface LabelClassEditor extends Component {
    	public ILabelClass getLabelClass();
        
        public void setLabelClass(ILabelClass labelClass);
        
        public void showDialog();
        
        public boolean isAccepted();
}
