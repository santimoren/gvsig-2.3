/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.ISimpleMarkerSymbol;


/**
 * JComboBox used by the user to select the different styles of simple marker symbols.
 * The available options are: circle style,square style,cross style, diamond style,x style
 * and triangle style.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class JComboBoxSimpleMarkeStyles extends JComboBox{
	/**
	 *
	 */
	private static final long serialVersionUID = 3018193423738880772L;
	private Color symColor = Color.BLACK;
	private Color outlineColor = Color.BLACK;
	private boolean outlined = false;
	static MyItem[] pointTypes = new MyItem[] {
		new MyItem(IMarkerSymbol.CIRCLE_STYLE),
		new MyItem(IMarkerSymbol.SQUARE_STYLE),
		new MyItem(IMarkerSymbol.CROSS_STYLE),
		new MyItem(IMarkerSymbol.DIAMOND_STYLE),
		new MyItem(IMarkerSymbol.X_STYLE),
		new MyItem(IMarkerSymbol.TRIANGLE_STYLE),
		new MyItem(IMarkerSymbol.STAR_STYLE),
		new MyItem(IMarkerSymbol.VERTICAL_LINE_STYLE),
	};

	/**
	 * Constructor method
	 *
	 */

	public JComboBoxSimpleMarkeStyles() {
		super();
		removeAllItems();
		for (int i = 0; i < pointTypes.length; i++) {
			addItem(pointTypes[i]);
		}

		setEditable(false);
		setRenderer(new ListCellRenderer() {

			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				SymbolPreviewer preview = new SymbolPreviewer(true) ;
				ISimpleMarkerSymbol mySymbol = SymbologyLocator.getSymbologyManager().createSimpleMarkerSymbol();
				mySymbol.setColor(symColor);
				mySymbol.setOutlined(outlined);
				mySymbol.setOutlineColor(outlineColor);
				if (value instanceof MyItem) {
					mySymbol.setStyle(((MyItem) value).style);
				} else {
					mySymbol.setStyle(((Integer) value).intValue());
				}

				mySymbol.setUnit(-1); // pixel
				mySymbol.setSize(10);
				preview.setForeground(UIManager.getColor(isSelected
						? "ComboBox.selectionForeground"
								: "ComboBox.foreground"));
				preview.setBackground(UIManager.getColor(isSelected
						? "ComboBox.selectionBackground"
								: "ComboBox.background"));
				preview.setSymbol(mySymbol);
				preview.setSize(preview.getWidth(), 20);
				preview.setPreferredSize(new Dimension(preview.getWidth(), 20));
				return preview;
			}
		});
	}

	/**
	 * Establishes the color of the simple marker symbol.
	 * @param c,Color
	 */
	public void setSymbolColor(Color c) {
		this.symColor = c;
	}

	/**
	 * Sets the color for the outline of the simple marker symbol
	 * @param c,Color
	 */
	public void setOutlineColor(Color c) {
		outlined = c!=null;
		outlineColor = c;
	}

	public Object getSelectedItem() {
		return new Integer(((MyItem) super.getSelectedItem()).style);
	}

	public void setSelectedItem(Object item) {
		if (item instanceof Integer) {
			int myItem = ((Integer) item).intValue();
			for (int i = 0; i < pointTypes.length; i++) {
				if (myItem == pointTypes[i].style)
					setSelectedIndex(i);
			}
		}
		super.setSelectedItem(item);
	}
}
/**
 * Used to store the different options that the JComboBoxsimplemarkerStyles shows to
 * the user.
 *
 */
class MyItem {
	int style;

	/**
	 * Constructor method
	 * @param style
	 */
	MyItem(int style) {
		this.style = style;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Integer) {
			Integer integer = (Integer) obj;
			return integer.intValue()==style;
		}

		if (obj instanceof MyItem) {
			return ((MyItem) obj).style == style;

		}
		return super.equals(obj);
	}
}
