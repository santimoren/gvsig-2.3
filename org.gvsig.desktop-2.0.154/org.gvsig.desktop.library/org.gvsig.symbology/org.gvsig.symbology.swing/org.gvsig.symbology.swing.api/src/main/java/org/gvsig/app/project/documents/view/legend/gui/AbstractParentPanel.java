/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: AbstractParentPanel.java 29598 2009-06-29 16:09:14Z jpiera $
* $Log$
* Revision 1.3  2007-05-10 09:46:45  jaume
* Refactored legend interface names
*
* Revision 1.2  2007/03/09 11:25:00  jaume
* Advanced symbology (start committing)
*
* Revision 1.1.2.3  2007/02/21 07:35:14  jaume
* *** empty log message ***
*
* Revision 1.1.2.2  2007/02/09 11:00:03  jaume
* *** empty log message ***
*
* Revision 1.1.2.1  2007/01/26 13:49:03  jaume
* *** empty log message ***
*
*
*/
package org.gvsig.app.project.documents.view.legend.gui;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;


public abstract class AbstractParentPanel implements ILegendPanel {

	public final void setData(FLayer lyr, ILegend legend) {}

	public final ILegend getLegend() {
		return null;
	}

	public final ImageIcon getIcon() {
		return null;
	}

	public Class getParentClass() {
		return null;
	}

	public final JPanel getPanel() {
		return null;
	}

	public Class getLegendClass() {
		return null;
	}

}
