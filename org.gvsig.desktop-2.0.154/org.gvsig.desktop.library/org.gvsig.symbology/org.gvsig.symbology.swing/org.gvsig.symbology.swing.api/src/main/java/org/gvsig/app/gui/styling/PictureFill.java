/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.app.project.documents.view.legend.gui.JSymbolPreviewButton;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.gui.beans.swing.JFileChooser;
import org.gvsig.gui.beans.swing.JNumberSpinner;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IPictureFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;

/**
 * <b>PictureFill</b> allows to store and modify the properties that fills a
 * polygon with a padding and an outline<p>
 * <p>
 * This functionality is carried out thanks to two tabs (picture fill and MarkerFillProperties)
 * which are included in the panel to edit the properities of a symbol (SymbolEditor)
 * how is explained in AbstractTypeSymbolEditor.<p>
 * <p>
 * The first tab (picture fill)permits the user to select the picture for the padding and
 * differentes options to modify it such as the angle(<b>incrAngle</b>) and the scale
 * (<b>incrScaleX,incrScaleY</b>). Also, there is an option to select a color for the
 * fill (<b>jccFillColor</b>).
 * <p>
 * The second tab is implementes as a MarkerFillProperties class and offers the possibilities
 * to change the separtion and the offset.
 *
 *
 *@see MarkerFillProperties
 *@see AbstractTypeSymbolEditor
 *@author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class PictureFill extends AbstractTypeSymbolEditor implements
ActionListener {

	private JLabel lblFileName;
	private JLabel lblSelFileName;
	private ArrayList<JPanel> tabs = new ArrayList<JPanel>();
	private MarkerFillProperties fillProperties;
	private File picFile;
	private JNumberSpinner incrAngle;
	private JNumberSpinner incrScaleX;
	private JNumberSpinner incrScaleY;
	private ColorChooserPanel jccFillColor;
	private ILineSymbol outline;
	private JSymbolPreviewButton btnOutline;
	private JCheckBox useBorder;

	private JButton btnBrowseFile;
    private JButton btnBrowseFileSelected;

	private static final double DEGREE_TO_RADIANS = Math.PI/180D;

	private ActionListener chooseAction = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			boolean isSelection;
			JLabel targetLbl;

			if (e.getSource().equals(btnBrowseFile)) {
				targetLbl = lblFileName;
				isSelection = false;
			} else {
				targetLbl = lblSelFileName;
				isSelection = true;
			}

			FileFilter ff = new FileFilter() {
				public boolean accept(File f) {
					if (f.isDirectory()) return true;
					String fName = f.getAbsolutePath();
					if (fName!=null) {
						fName = fName.toLowerCase();
						return fName.endsWith(".png")
						|| fName.endsWith(".gif")
						|| fName.endsWith(".jpg")
						|| fName.endsWith(".jpeg")
						|| fName.endsWith(".bmp")
						|| fName.endsWith(".svg");
					}
					return false;
				}

				public String getDescription() {
					return Messages.getText("bitmap_and_svg_image_files");
				}
			};
			JUrlFileChooser jfc = new JUrlFileChooser(getName(), null);
			jfc.setFileFilter(ff);
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jfc.setSelectedFile(picFile);
			jfc.setMultiSelectionEnabled(false);
			int returnVal = jfc.showOpenDialog(PictureFill.this.owner);
//			if(returnVal == JFileChooser.APPROVE_OPTION) {
//				File myFile = jfc.getSelectedFile();
//				lastDir = jfc.getCurrentDirectory();
//				if (myFile != null && myFile.exists()) {
//					if(!isSelection){
//						picFile = myFile;
//					}
//					else{
//						selPicFile = myFile;
//					}
//					try {
//						targetLbl.setText(myFile.toURL().toString());
//					} catch (MalformedURLException e1) {
//						NotificationManager.addError(PluginServices.getText(this, "invalid_url"), e1);
//					}
//					fireSymbolChangedEvent();
//				}
//			}
			if(returnVal == JFileChooser.APPROVE_OPTION) {

				URL url = jfc.getSelectedURL();
				if (url == null) return;
				try {
					targetLbl.setText(url.toURI().getPath());
				} catch (URISyntaxException e1) {
					NotificationManager.addWarning("URI Syntax error", e1);
				}
				fireSymbolChangedEvent();
			}
			boolean enabled = (lblFileName.getText()!="");
			enableControls(lblFileName.getText()!="");

		}
	};

	/**
	 * Constructor method
	 * @param owner
	 */
	public PictureFill(SymbolEditor owner) {
		super(owner);
		initialize();
	}

	/**
	 * Initializes the parameters that allows the user to fill the padding of
	 * a polygon with a picture style.To do it, two tabs are created (picture fill and
	 * MarkerFillProperties)inside the SymbolEditor panel with default values for the
     * different attributes.
	 */
	private void initialize() {
		JPanel myTab = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
		myTab.setName(Messages.getText("picture_fill"));

		btnBrowseFile = new JButton(Messages.getText("browse"));
		btnBrowseFile.addActionListener(chooseAction);

		btnBrowseFileSelected = new JButton(Messages.getText("browse"));
		btnBrowseFileSelected.addActionListener(chooseAction);

		JPanel aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JPanel auxLabelPic=new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel lblName = new JLabel();
		lblName.setFont(lblName.getFont().deriveFont(Font.BOLD));
		lblName.setText(Messages.getText("picture_file")+":");
		auxLabelPic.add(lblName);

		aux2.add(btnBrowseFile);
		aux2.add(lblFileName = new JLabel(""));

		JPanel auxLabelSelPic=new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel lblSelName = new JLabel();
		lblSelName.setFont(lblSelName.getFont().deriveFont(Font.BOLD));
		lblSelName.setText(Messages.getText("selection_picture_file")+":");
		auxLabelSelPic.add(lblSelName);

		JPanel aux4 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		aux4.add(btnBrowseFileSelected);
		aux4.add(lblSelFileName = new JLabel(""));

		GridBagLayoutPanel aux = new GridBagLayoutPanel();
		aux.addComponent(new JBlank(5, 5));
		aux.addComponent(auxLabelPic);
		aux.addComponent(aux2);
		aux.addComponent(auxLabelSelPic);
		aux.addComponent(aux4);


		aux2 = new JPanel(new GridLayout(1, 2, 20, 5));
		GridBagLayoutPanel aux3;
		aux3 = new GridBagLayoutPanel();
		aux3.addComponent(Messages.getText("angle")+":",
				incrAngle = new JNumberSpinner(0, 20));
		aux3.addComponent(Messages.getText("scale")+"X:",
				incrScaleX = new JNumberSpinner(
						1.0,
                                                20,
						0.01,
						Double.POSITIVE_INFINITY,
						0.1));
		incrScaleX.setDouble(1);
		aux3.addComponent(Messages.getText("scale")+"Y:",
				incrScaleY = new JNumberSpinner(
						1.0,
                                                20,
						0.01,
						Double.POSITIVE_INFINITY,
						0.1));
		incrScaleY.setDouble(1);
		aux2.add(aux3);

		aux3 = new GridBagLayoutPanel();
		aux3.addComponent(new JBlank(5,5));
		aux3.addComponent(new JLabel (Messages.getText("fill_color")+":"));
		aux3.addComponent(new JBlank(5,5));
		aux3.addComponent(jccFillColor = new ColorChooserPanel(true,true));
		jccFillColor.setAlpha(255);

		aux3.addComponent(new JBlank(5,5));
		aux3.addComponent(new JBlank(5,5));
		aux2.add(aux3);

		aux.addComponent(aux2);
		aux.addComponent(new JBlank(10, 10));
		aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		aux2.add(btnOutline = new JSymbolPreviewButton(Geometry.TYPES.CURVE));
		useBorder = new JCheckBox(Messages.getText("use_outline"));
		aux.addComponent(useBorder);
		aux.addComponent(Messages.getText("outline")+":",
				aux2);

		fillProperties = new MarkerFillProperties();
		myTab.add(aux);

		fillProperties.addActionListener(this);
		incrAngle.addActionListener(this);
		incrScaleX.addActionListener(this);
		incrScaleY.addActionListener(this);
		jccFillColor.addActionListener(this);
		btnOutline.addActionListener(this);
		useBorder.addActionListener(this);

		tabs.add(myTab);
		tabs.add(fillProperties);

		enableControls(false);

	}

	public EditorTool[] getEditorTools() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public ISymbol getLayer() {

		IPictureFillSymbol sym=null;
		try {

			if( lblFileName.getText().equals("") )
				sym=null;

			else {
				sym =  SymbologyLocator.getSymbologyManager().createPictureFillSymbol(new File(lblFileName.getText()).toURI().toURL(),null);
				if (!lblSelFileName.getText().equals("")){
					sym = SymbologyLocator.getSymbologyManager().createPictureFillSymbol(new File(lblFileName.getText()).toURI().toURL(),new File(lblSelFileName.getText()).toURI().toURL());
				}
				sym.setHasFill(jccFillColor.getUseColorisSelected());
				Color c = jccFillColor.getColor();
				if (c != null)
					c = new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
				sym.setFillColor(c);

				sym.setHasOutline(useBorder.isSelected());
				outline = (ILineSymbol) btnOutline.getSymbol();
				sym.setOutline(outline);

				sym.setAngle(incrAngle.getDouble()*DEGREE_TO_RADIANS);
				sym.setXScale(incrScaleX.getDouble());
				sym.setYScale(incrScaleY.getDouble());
				sym.setMarkerFillProperties(fillProperties.getMarkerFillProperties());
			}

		} catch (IOException e) {
			IWarningSymbol warning =
				(IWarningSymbol) MapContextLocator.getSymbolManager()
				.getWarningSymbol(
						SymbolDrawingException.STR_UNSUPPORTED_SET_OF_SETTINGS,
						Messages.getText("failed_acessing_files"),
								SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
						return warning;
		}


		return sym;


	}

	public String getName() {
		return Messages.getText("picture_fill_symbol");
	}

	public JPanel[] getTabs() {
		return (JPanel[]) tabs.toArray(new JPanel[tabs.size()]);
	}

	public void refreshControls(ISymbol layer) {
		if (layer instanceof IPictureFillSymbol){
			IPictureFillSymbol sym = (IPictureFillSymbol) layer;

			URL source = sym.getSource();
			if (source != null) {
				try {
					lblFileName.setText(source.toURI().getPath());
				} catch (URISyntaxException e) {
					NotificationManager.addWarning("URI Syntax error", e);				}
			} else {
				lblFileName.setText("");
			}
			URL selSource = sym.getSelectedSource();
			if (selSource != null) {
				try {
					lblSelFileName.setText(selSource.toURI().getPath());
				} catch (URISyntaxException e) {
					NotificationManager.addWarning("URI Syntax error", e);
				}
			} else {
				lblSelFileName.setText("");
			}

			jccFillColor.setUseColorIsSelected(sym.hasFill());
			jccFillColor.setColor(sym.getFillColor());

			outline=sym.getOutline();
			btnOutline.setSymbol(outline);
			useBorder.setSelected(sym.hasOutline());

			incrAngle.setDouble(sym.getAngle()/DEGREE_TO_RADIANS);
			incrScaleX.setDouble(sym.getXScale());
			incrScaleY.setDouble(sym.getYScale());
			fillProperties.setModel(sym.getMarkerFillProperties());

			enableControls(lblFileName.getText()!="");
		}
	}

	private void enableControls(boolean enabled){
		btnBrowseFileSelected.setEnabled(enabled);
		incrAngle.setEnabled(enabled);
		incrScaleX.setEnabled(enabled);
		incrScaleY.setEnabled(enabled);
		incrAngle.setEnabled(enabled);
		incrScaleX.setEnabled(enabled);
		incrScaleY.setEnabled(enabled);
		jccFillColor.setEnabled(enabled);
		btnOutline.setEnabled(enabled);
		useBorder.setEnabled(enabled);

		fillProperties.setEnabled(enabled);
	}

	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();

		if(s.equals(jccFillColor)) {
			jccFillColor.getColor().getAlpha();
		}
		outline = (ILineSymbol) btnOutline.getSymbol();
		fireSymbolChangedEvent();
	}

	public boolean canManageSymbol(ISymbol symbol) {
		return symbol instanceof IPictureFillSymbol;
	}

}