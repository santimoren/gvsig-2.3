/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gvsig.app.gui.labeling;

import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;

/**
 *
 * @author jjdelcerro
 */
public interface LabelClassEditorFactory {

    public String getName();

    public String getID();
    
    public LabelClassEditor createEditor(ILabelClass labelClass, FeatureStore store);

    public boolean accept(Class<? extends ILabelClass> labelClass);
    
    public String toString();    
}
