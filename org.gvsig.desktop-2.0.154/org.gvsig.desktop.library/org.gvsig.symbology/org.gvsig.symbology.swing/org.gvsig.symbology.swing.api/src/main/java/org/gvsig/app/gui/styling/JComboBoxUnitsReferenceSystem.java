/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import javax.swing.JComboBox;
import org.gvsig.i18n.Messages;


/**
 * JComboBox to all reference systems
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class JComboBoxUnitsReferenceSystem extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5740096657624527194L;

	public JComboBoxUnitsReferenceSystem() {
		super();
		addItem(Messages.getText("in_the_world"));
		addItem(Messages.getText("in_the_paper"));
	}
}
