/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.gui.JComboBoxUnits;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;

/**
 * Creates the panel that is used to control the properties of a symbol in
 * order to modify or check them and to create a new one.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class SymbolEditor extends JPanel implements IWindow {
	private static final long serialVersionUID = 9209260593958625497L;
	
	private static final GeometryManager GEOMETRY_MANAGER = 
	    GeometryLocator.getGeometryManager();
	
	private static final Logger LOG =
	    LoggerFactory.getLogger(SymbolEditor.class);
	
	private WindowInfo wi;
	private JPanel pnlWest = null;
	private JPanel pnlCenter = null;
	private JPanel pnlPreview = null;
	private JPanel pnlLayers = null;
	private AcceptCancelPanel okCancelPanel;
	private ISymbol symbol;
	private SymbolPreviewer symbolPreview = null;
	private JPanel pnlTypeAndUnits = null;
	private JComboBox cmbType;
	private JComboBoxUnits cmbUnits;
	private JTabbedPane tabbedPane = null;
	private GeometryType shapeType;
	private ActionListener cmbTypeActionListener;

	private AbstractTypeSymbolEditor[] tabs;
	private SymbolLayerManager layerManager;
	private boolean replacing = false;
	private JComboBoxUnitsReferenceSystem cmbUnitsReferenceSystem;
	
	private ISymbol oldSymbol;
	
	private MapContextManager mapContextManager = MapContextLocator
			.getMapContextManager();
	
	
	public SymbolEditor(ISymbol symbol, GeometryType geometryType) {
	    super();
	    initialize(symbol, geometryType);
	}	
	
	public SymbolEditor(ISymbol symbol, int shapeType) {
	    super();
	    try {
            GeometryType geometryType = GEOMETRY_MANAGER.getGeometryType(shapeType, SUBTYPES.GEOM2D);
            initialize(symbol, geometryType);
        } catch (GeometryTypeNotSupportedException e1) {
            LOG.error("Impossible to get the geometry type", e1);
        } catch (GeometryTypeNotValidException e1) {
            LOG.error("Impossible to get the geometry type", e1);
        }	   
	}   
	    
	
	/**
	 * Constructor method
	 *
	 * @param symbol ISymbol
	 * @param shapeType int
	 */
	private void initialize(ISymbol symbol, GeometryType shapeType) {
//////////	/-------------------------------------
//      removing the Geometry.TYPES.TEXT: this code can not be executed
//		if (shapeType == Geometry.TYPES.TEXT) {
////			this.symbol = symbol == null ? new SimpleTextSymbol(): symbol;
//			this.symbol = symbol == null ? SymbologyLocator.getSymbologyManager().createSimpleTextSymbol(): symbol;
//		} else {
//////////	/-------------------------------------

			if (!(symbol instanceof IMultiLayerSymbol)) {
				// this is a simple symbol (or null one); it will be
				// converted to a multilayer one to accept layer addition
				IMultiLayerSymbol nSym =
						mapContextManager.getSymbolManager()
						.createMultiLayerSymbol(shapeType.getType());

//				if (!(symbol instanceof FSymbol)) {
					nSym.addLayer(symbol);
//				}

				if (symbol instanceof CartographicSupport) {
					CartographicSupport cs = (CartographicSupport) symbol;
					CartographicSupport nCs = (CartographicSupport) nSym;
					nCs.setReferenceSystem(cs.getReferenceSystem());
					nCs.setUnit(cs.getUnit());
				}

				this.symbol = nSym;
			} else {
				this.symbol = symbol;
			}

			// apply units and reference system to comboboxes
			if (this.symbol instanceof CartographicSupport) {
				CartographicSupport cs = (CartographicSupport) this.symbol;
				getCmbUnits().setSelectedUnitIndex(cs.getUnit());
				getCmbUnitsReferenceSystem().
					setSelectedIndex(cs.getReferenceSystem());

			}

//		}
		try {
			this.oldSymbol = (ISymbol) this.symbol.clone();
		} catch (CloneNotSupportedException e) {
			NotificationManager.addWarning("Symbol layer", e);
		}
		this.shapeType = shapeType;
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
                SymbologySwingManager symbologySwingManager = SymbologySwingLocator.getSwingManager();
		cmbTypeActionListener = new ActionListener() {
			int prevIndex = -2;

			public void actionPerformed(ActionEvent e) {
				int index = getCmbType().getSelectedIndex();
				if (prevIndex != index) {
					// needs to refresh
					prevIndex = index;

					AbstractTypeSymbolEditor options = (AbstractTypeSymbolEditor) getCmbType()
							.getSelectedItem();

//////////			/-------------------------------------
					if (layerManager!=null) {
//////////			/-------------------------------------
							ISymbol l = layerManager.getSelectedLayer();

						// if the symbol is not null and is it managed by the "options" class
						// refresh the controls
						if (l != null
//								&& l.getClass().equals(options.getSymbolClass())) {
								&& options.canManageSymbol(l)) {
							if (l instanceof CartographicSupport) {
								CartographicSupport cs = (CartographicSupport) l;
								getCmbUnits().setSelectedUnitIndex(cs.getUnit());
								getCmbUnitsReferenceSystem().setSelectedIndex(cs.getReferenceSystem());
							}
							options.refreshControls(l);
						}

						replaceOptions(options);
//////////			/-------------------------------------
					} else {
						replaceOptions(options);
					}
//////////			/-------------------------------------

				}
			}
		};

		Comparator tabComparator = new Comparator() {
			public int compare(Object o1, Object o2) {
				AbstractTypeSymbolEditor pnl1 = (AbstractTypeSymbolEditor) o1;
				AbstractTypeSymbolEditor pnl2 = (AbstractTypeSymbolEditor) o2;
				int result = pnl1.getName().compareTo(pnl2.getName());
				return result;
			}
		};


		TreeSet set = new TreeSet(tabComparator);
		List editors = symbologySwingManager.getSymbolEditorClassesByGeometryType(shapeType);
		Class[] constrLocator = new Class[] {SymbolEditor.class};
		Object[] constrInitargs = new Object[] { this };
		for (int i = 0; i < editors.size(); i++) {
			Class editorClass = (Class) editors.get(i);
			try {
				Constructor c = editorClass.getConstructor(constrLocator);
				AbstractTypeSymbolEditor instance = (AbstractTypeSymbolEditor) c.newInstance(constrInitargs); 
				set.add(instance);
			} catch (Exception e) {
				NotificationManager.addError(Messages.getText("failed_installing_symbol_editor")+" "
						+editorClass.getName(), e);
			}
		};
 		tabs = (AbstractTypeSymbolEditor[]) set
				.toArray(new AbstractTypeSymbolEditor[0]);

		this.setLayout(new BorderLayout());
		this.add(getPnlWest(), BorderLayout.WEST);
		this.add(getPnlCenter(), BorderLayout.CENTER);
		this.add(getOkCancelPanel(), BorderLayout.SOUTH);

		cmbTypeActionListener.actionPerformed(null);
		refresh();
	}
	/**
	 * Returns an array of tabs. The value of this array will depend on the
	 * symbol selected. For example, if the symbol is composed by lines this
	 * method will return tha tabs that allow the user to modify a simple line
	 * symbol(in this case simple line and arrow decorator tabs)
	 * @param sym
	 * @return tabs[] AbstractTypeSymbolEditor[]
	 */
	private AbstractTypeSymbolEditor getOptionsForSymbol(ISymbol sym) {
		if (sym == null) {
			return tabs[0];
		}
		for (int i = 0; i < tabs.length; i++) {
//			if (tabs[i].getSymbolClass().equals(sym.getClass())) {
			if (tabs[i].canManageSymbol(sym)) {
				return tabs[i];
			}
		}
		return tabs[0];
	}
	/**
	 * Initializes the OkCancel panel where the accept and cancel buttons
	 * will be placed
	 * @return okCancelPanel AcceptCancelPanel
	 */
	private AcceptCancelPanel getOkCancelPanel() {
		if (okCancelPanel == null) {
			ActionListener action = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if ("CANCEL".equals(e.getActionCommand())) {
						symbol = oldSymbol;
					}
					PluginServices.getMDIManager().closeWindow(
							SymbolEditor.this);
				}
			};
			okCancelPanel = new AcceptCancelPanel(action, action);
		}
		return okCancelPanel;
	}

	public WindowInfo getWindowInfo() {
		if (wi == null) {
			wi = new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
			wi.setWidth(650);
			wi.setHeight(400);
			wi.setTitle(Messages.getText("symbol_property_editor"));
		}
		return wi;
	}

	public ISymbol getSymbol() {
		if (symbol instanceof CartographicSupport) {
			CartographicSupport cs = (CartographicSupport) symbol;
			cs.setUnit(getUnit());
			cs.setReferenceSystem(getReferenceSystem());
		}
//
//		if (symbol instanceof MultiLayerLineSymbol) {
//			MultiLayerLineSymbol mLineSym = (MultiLayerLineSymbol) symbol;
//			double lineWidth = 0;
//			for (int i = 0; i < mLineSym.getLayerCount(); i++) {
//				lineWidth = Math.max(lineWidth, ((ILineSymbol) mLineSym.getLayer(i)).getLineWidth());
//			}
//
//			if (mLineSym.getLineWidth() != lineWidth)
//				mLineSym.setLineWidth(lineWidth);
//		}
		return symbol;
	}
	/**
	 * Initializes the west panel
	 * @return
	 */
	private JPanel getPnlWest() {
		if (pnlWest == null) {
			pnlWest = new JPanel();
			pnlWest.setLayout(new BorderLayout());
			pnlWest.add(getPnlPreview(), java.awt.BorderLayout.NORTH);
//////////	/-------------------------------------
			if (symbol instanceof IMultiLayerSymbol) {
//////////		/-------------------------------------


				pnlWest.add(getPnlLayers(), java.awt.BorderLayout.SOUTH);

//////////		/-------------------------------------
			} // otherwise, no layer manager needed
//////////	/-------------------------------------
		}
		return pnlWest;
	}
	/**
	 * Initializes the center panel that shows the properties of a symbol.
	 *
	 * @return pnlCenter JPanel
	 */
	private JPanel getPnlCenter() {
		if (pnlCenter == null) {
			pnlCenter = new JPanel(new BorderLayout());
			pnlCenter.setBorder(BorderFactory.createTitledBorder(null,
					Messages.getText("properties")));
			pnlCenter.add(getPnlTypeAndUnits(), java.awt.BorderLayout.NORTH);
		}
		return pnlCenter;
	}
	/**
	 * Initializes the preview panel that allows the user to see a previsualization
	 * of the final symbol
	 *
	 * @return pnlPreview JPanel
	 */
	private JPanel getPnlPreview() {
		if (pnlPreview == null) {
			pnlPreview = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlPreview.setBorder(BorderFactory.createTitledBorder(null,
					Messages.getText("preview")));
			pnlPreview.add(getSymbolPreviewer());
		}
		return pnlPreview;
	}
	/**
	 * Initializes the Layers panel that shows the different layers created that
	 * compose a symbol.
	 * @return pnlLayers JPanel
	 */
	private JPanel getPnlLayers() {
		if (pnlLayers == null) {
			pnlLayers = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlLayers.setBorder(BorderFactory.createTitledBorder(null,
					Messages.getText("layers")));
			pnlLayers.add(getLayerManager());
		}
		return pnlLayers;
	}
	/**
	 * Obtains the layer manager used in the panel that shows the different layers
	 * that compose the symbol.
	 *
	 * @return layerManager SymbolLayerManager
	 */
	private SymbolLayerManager getLayerManager() {
		if (layerManager == null) {
			layerManager = new SymbolLayerManager(this);
		}
		return layerManager;
	}
	/**
	 * Obtains the symbol previewer used in the panel that shows the previsualization
	 * of the final symbol.
	 *
	 * @return symbolPreview getSymbolPreviewer
	 */
	private SymbolPreviewer getSymbolPreviewer() {
		if (symbolPreview == null) {
			symbolPreview = new SymbolPreviewer();
			symbolPreview.setPreferredSize(new Dimension(150, 100));

		}
		return symbolPreview;
	}
	/**
	 * Initializes the type and units panel where two Jcomboboxes will be placed
	 * in order to change the type and the units used in the map.
	 *
	 * @return pnlTypeAndUnits JPanel
	 */
	private JPanel getPnlTypeAndUnits() {
		if (pnlTypeAndUnits == null) {
			pnlTypeAndUnits = new JPanel();
			pnlTypeAndUnits.setLayout(new BorderLayout());
			JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING));
			aux.add(new JLabel(Messages.getText("type")));
			aux.add(getCmbType());
			pnlTypeAndUnits.add(aux, BorderLayout.WEST);

			aux = new JPanel(new FlowLayout(FlowLayout.LEADING));
			aux.add(new JLabel(Messages.getText("units")));
			aux.add(getCmbUnits());
			aux.add(getCmbUnitsReferenceSystem());
			pnlTypeAndUnits.add(aux, BorderLayout.EAST);

		}
		return pnlTypeAndUnits;
	}
	/**
	 * Obtains the JCombobox to select the reference unit to be used in the
	 * final representation of the map in this case there are two options (in
	 * the paper and in the map).
	 * @return
	 */
	private JComboBoxUnitsReferenceSystem getCmbUnitsReferenceSystem() {
		if (cmbUnitsReferenceSystem == null) {
			cmbUnitsReferenceSystem = new JComboBoxUnitsReferenceSystem();

		}

		return cmbUnitsReferenceSystem;
	}
	/**
	 * Returns the Jcombobox used to select the reference unit (centimeters,
	 * milimeters and so on) to be used in the final representation of the map.
	 *
	 * @return cmbUnits JUnitsComboBox
	 */
	private JComboBoxUnits getCmbUnits() {
		if (cmbUnits == null) {
			cmbUnits = new JComboBoxUnits();
		}
		return cmbUnits;
	}
	/**
	 * Returns the option selected in the reference unit Jcombobox
	 *
	 */
	public int getUnit() {
		return getCmbUnits().getSelectedUnitIndex();
	}

	public int getReferenceSystem() {
		return getCmbUnitsReferenceSystem().getSelectedIndex();
	}
	/**
	 * Returns the Jcombobox used in the panel to select the type of symbol.
	 *
	 * @return cmbType JComboBox
	 */
	private JComboBox getCmbType() {
		if (cmbType == null) {
			cmbType = new JComboBox(tabs);
			cmbType.addActionListener(cmbTypeActionListener);
		}
		return cmbType;
	}
	/**
	 * Sets a layer to a symbol in order to create a final symbol composed
	 * by different layers.
	 *
	 * @param layer
	 */
	protected void setLayerToSymbol(ISymbol layer) {
		int i = getLayerManager().getSelectedLayerIndex();
		IMultiLayerSymbol s = (IMultiLayerSymbol) symbol;
		if (i >= 0 && i < s.getLayerCount()) {
			s.setLayer(s.getLayerCount() - 1 - i, layer);

		}
		refresh();
	}

	public void refresh() {
		getSymbolPreviewer().setSymbol(symbol);
		doLayout();
		repaint();
	}

	/**
	 * <p>
	 * Returns the type of the symbol that this panels is created for.<br>
	 * </p>
	 * <p>
	 * Possible values returned by this method are
	 * <ol>
	 * <li> <b> Geometry.TYPES.POINT </b>, for maker symbols </li>
	 * <li> <b> Geometry.TYPES.SURFACE </b>, for fill symbols </li>
	 * <li> <b> Geometry.TYPES.CURVE </b>, for line symbols (not yet implemented) </li>
	 * <li> <b> Geometry.TYPES.TEXT </b>, for text symbols (not yet implemented) </li>
	 * <li> maybe some other in the future </li>
	 * </ol>
	 * </p>
	 *
	 * @return
	 */
	public int getShapeType() {
		return shapeType.getType();
	}
	/**
	 * Obtains a new layer
	 *
	 * @return sym ISymbol
	 */
	public ISymbol getNewLayer() {
		ISymbol sym = ((AbstractTypeSymbolEditor) getCmbType().getSelectedItem())
				.getLayer();

		return sym;
	}

	private void replaceOptions(AbstractTypeSymbolEditor options) {
		if (!replacing) {
			replacing = true;
			if (tabbedPane != null) {
				getPnlCenter().remove(tabbedPane);
			}
			JPanel[] tabs = options.getTabs();
			tabbedPane = new JTabbedPane();
			tabbedPane.setPreferredSize(new Dimension(300, 300));
			for (int i = 0; i < tabs.length; i++) {
				tabbedPane.addTab(tabs[i].getName(), tabs[i]);
			}
			getPnlCenter().add(tabbedPane, BorderLayout.CENTER);
			getPnlCenter().doLayout();
			replacing = false;
		}
	}


	public void setOptionsPageFor(ISymbol symbol) {
		AbstractTypeSymbolEditor options = getOptionsForSymbol(symbol);

		options.refreshControls(symbol);
		getCmbType().setSelectedItem(options);
	}
	/**
	 * Obtains the units to be used for the reference system.
	 *
	 */
	public int getUnitsReferenceSystem() {
		return cmbUnitsReferenceSystem.getSelectedIndex();
	}

        /**
         * @deprecated use SymblogySwingManager
         */
	public static void addSymbolEditorPanel(Class<? extends TypeSymbolEditor> symbolEditor, int shapeType) {
            SymbologySwingManager manager = SymbologySwingLocator.getSwingManager();
            manager.registerSymbolEditor(symbolEditor, shapeType);
	}
	
        /**
         * @deprecated use SymblogySwingManager
         */
	private static List getSymbolsByType(GeometryType geometryType){
            SymbologySwingManager manager = SymbologySwingLocator.getSwingManager();
            return manager.getSymbolEditorClassesByGeometryType(geometryType);
	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}

}
