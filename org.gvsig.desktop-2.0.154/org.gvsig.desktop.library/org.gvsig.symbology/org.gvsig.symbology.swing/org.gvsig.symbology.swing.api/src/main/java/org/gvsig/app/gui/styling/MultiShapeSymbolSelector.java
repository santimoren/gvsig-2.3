/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.documents.view.legend.gui.ISymbolSelector;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.IMultiShapeSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.tools.exception.BaseException;


public class MultiShapeSymbolSelector extends JPanel implements
    ISymbolSelector, ActionListener {
    
    private static Logger logger = LoggerFactory.getLogger(
        MultiShapeSymbolSelector.class);

	private static final long serialVersionUID = 3967550608736418084L;
	private SymbolSelector markerSelector;
	private SymbolSelector lineSelector;
	private SymbolSelector fillSelector;
	private WindowInfo wi;
	private JTabbedPane tabbedPane;
	
	private GeometryManager gManager = null;
	private JButton closeButton = null;
	private JPanel closeButtonPanel = null;

	public static ISymbolSelector createSymbolBrowser() {
		WindowInfo winfo = new WindowInfo(WindowInfo.RESIZABLE|WindowInfo.MAXIMIZABLE|WindowInfo.ICONIFIABLE);
		winfo.setWidth(706);
		winfo.setHeight(500);
		winfo.setTitle(Messages.getText("symbols_browser"));
		MultiShapeSymbolSelector resp = null;
		
		try {
		    /*
		     * This instantiates a symbol browser
		     * (a selector without accept/cancel panel)
		     */
            resp = new MultiShapeSymbolSelector();
            resp.wi = winfo;
        } catch (BaseException e) {
            logger.info("Unable to create symbol browser, will use selector.", e);
            resp = new MultiShapeSymbolSelector(winfo);
        }
        return resp;
	}
	
	/*
	 * This instantiates a symbol browser 
	 */
    private MultiShapeSymbolSelector() throws BaseException {

        GeometryType
        gt = getGeomManager().getGeometryType(Geometry.TYPES.POINT,
            SUBTYPES.GEOM2D);
        markerSelector = new SymbolSelectorBrowser(gt, true);
        gt = getGeomManager().getGeometryType(Geometry.TYPES.CURVE,
            SUBTYPES.GEOM2D);
        lineSelector = new SymbolSelectorBrowser(gt, true);
        gt = getGeomManager().getGeometryType(Geometry.TYPES.SURFACE,
            SUBTYPES.GEOM2D);
        fillSelector = new SymbolSelectorBrowser(gt, true);

        initialize(true);
    }
	
	private MultiShapeSymbolSelector(WindowInfo wi) {
		this((Object)null);
		this.wi = wi;
	}
	
	MultiShapeSymbolSelector(Object currSymbol) {
                IMultiShapeSymbol sym = null;
                try {
                    sym = (IMultiShapeSymbol) currSymbol;
                } catch(Exception ex) {
                    logger.warn("Incorrect symbol type, use default.",ex);
                }
		if (sym == null) {
			markerSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(null, Geometry.TYPES.POINT);
			lineSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(null, Geometry.TYPES.CURVE);
			fillSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(null, Geometry.TYPES.SURFACE);
		} else {
			markerSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(sym.getMarkerSymbol(),
							Geometry.TYPES.POINT);
			lineSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(sym.getLineSymbol(),
							Geometry.TYPES.CURVE);
			fillSelector = (SymbolSelector) SymbolSelector
					.createSymbolSelector(sym.getFillSymbol(),
							Geometry.TYPES.SURFACE);
		}
		initialize(false);
	}


	private void initialize(boolean add_close_button) {
		setLayout(new BorderLayout());
		add(getJTabbedPane(), BorderLayout.CENTER);
		if (add_close_button) {
		    add(getCloseButtonPanel(), BorderLayout.SOUTH);
		}
	}

    private JTabbedPane getJTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane();
			tabbedPane.addTab(Messages.getText("marker"), markerSelector);
			tabbedPane.addTab(Messages.getText("line"), lineSelector);
			tabbedPane.addTab(Messages.getText("fill"), fillSelector);
			tabbedPane.setPreferredSize(getWindowInfo().getMinimumSize());
		}

		return tabbedPane;
	}


	public Object getSelectedObject() {
	    
		IMultiShapeSymbol sym = SymbologyLocator.getSymbologyManager().createMultiShapeSymbol();
		
		IMarkerSymbol ims = (IMarkerSymbol) markerSelector.getSelectedObject();
		ILineSymbol ils = (ILineSymbol) lineSelector.getSelectedObject();
		IFillSymbol ifs = (IFillSymbol) fillSelector.getSelectedObject();
		
		if (ims == null || ils == null || ifs == null) {
            /*
             * If one of the symbols is null, it's because the user
             * has cancelled so those who have called this method must
             * deal with null (many of them where already checking before
             * this change)
             */
            return null;
		} else {
            sym.setMarkerSymbol(ims);
            sym.setLineSymbol(ils);
            sym.setFillSymbol(ifs);
            return sym;
		}
		
		
	}

	public void setSymbol(Object symbol) {
		IMultiShapeSymbol sym = (IMultiShapeSymbol) symbol;
		markerSelector.setSymbol(sym.getMarkerSymbol());
		lineSelector.setSymbol(sym.getLineSymbol());
		fillSelector.setSymbol(sym.getFillSymbol());
	}

	public WindowInfo getWindowInfo() {
		if (wi == null) {
			wi = new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
			wi.setWidth(706);
			wi.setHeight(500);
			wi.setTitle(Messages.getText("symbol_selector"));
		}
		return wi;
	}

	public Object getWindowProfile() {
		if( wi.isModal() ) {
			return WindowInfo.DIALOG_PROFILE;
		}
		return WindowInfo.EDITOR_PROFILE;
	}
	
	private GeometryManager getGeomManager() {
	    
	    if (gManager == null) {
	        gManager = GeometryLocator.getGeometryManager();
	    }
	    return gManager;
	}

	private JButton getCloseButton() {
	    if (closeButton == null) {
	        String txt = Messages.getText("close");
	        closeButton = new JButton(txt);
	        closeButton.addActionListener(this);
	    }
	    return closeButton;
	}
	
    private Component getCloseButtonPanel() {
        
        if (closeButtonPanel == null) {
            closeButtonPanel = new JPanel();
            Border brd = BorderFactory.createEmptyBorder(10, 10, 10, 10);
            closeButtonPanel.setBorder(brd);
            closeButtonPanel.setLayout(new BorderLayout());
            closeButtonPanel.add(getCloseButton(), BorderLayout.EAST);
        }
        return closeButtonPanel;
    }

    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == this.getCloseButton()) {
            PluginServices.getMDIManager().closeWindow(this);
        }
        
    }

}
