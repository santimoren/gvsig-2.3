/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.gvsig.gui.ColorTablePainter;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.swing.SymbologySwingLocator;


/**
 * Creates a JComboBox where the user has the option to select different
 * colors.
 *
 * @autor jaume dominguez faus - jaume.dominguez@iver.es
 */
public class JComboBoxColorScheme extends JComboBox {
	/**
	 * 
	 */
	private static final long serialVersionUID = -749998859270723991L;
	private String palettesPath = System.getProperty("user.home") +
	File.separator +
	"gvSIG" +
	File.separator +
	"ColorSchemes";
//	private boolean interpolated = false;
	private List<ColorTablePainter> colorTables = new ArrayList<ColorTablePainter>();

	private ActionListener innerActionUpdateTooltip = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			ColorTablePainter colorTable = (ColorTablePainter)getSelectedItem();
			if (colorTable != null) {
				setToolTipText(colorTable.getTableName());
			} else {
				setToolTipText(Messages.getText("select_a_color_scheme"));
			}
		}
	};
	
	/**
	 * Constructor method
	 *
	 * @param interpolateValues
	 */
	public JComboBoxColorScheme(boolean interpolateValues) {
		super();
//		interpolated = interpolateValues;
		setPreferredSize(new Dimension(150, 20));
		
		List<ColorTablePainter> colorTables = SymbologySwingLocator.getSwingManager().createColorTables();
		if(colorTables != null){
			for (int i=0; i<colorTables.size(); i++) {
				ColorTablePainter colorTable = colorTables.get(i);
//				ArrayList array = new ArrayList();
//				array.add(colorTable.getTableName());
//				array.add(colorTable);
//				addItem(array);
				addItem(colorTable);
			}
			addActionListener(innerActionUpdateTooltip);
			setRenderer(new ListCellRenderer() {
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
					ColorTablePainter colorTable = (ColorTablePainter) value;
					ColorSchemeItemPainter paintItem = new ColorSchemeItemPainter(colorTable.getTableName(), colorTable, isSelected);
					paintItem.setPreferredSize(getPreferredSize());
					return paintItem;
				}
			});
		}

	}

	/**
	 * Returns an array composed with the selected colors
	 * @return
	 */
	public Color[] getSelectedColors() {
		ColorTablePainter colorTable = (ColorTablePainter) getSelectedItem();
		if (colorTable == null) {
			return null;
		}
		return colorTable.getColors();
	}

	public void setSelectedColors(Color[] colors) {

		colorTables.clear();
		if (colors == null) {
			setSelectedIndex(0);
			return;
		} else {
			for (int i = 0; i < getItemCount(); i++) {
				colorTables.add((ColorTablePainter) getItemAt(i));
			}

			for (int i = 0; i < colorTables.size(); i++) {
				ColorTablePainter colorTable = colorTables.get(i);
				Color[] myColors = colorTable.getColors();

				boolean isEqual = true;
				if(myColors.length == colors.length) {
					for (int j = 0; isEqual && j < myColors.length; j++) {
						Color c1 = myColors[j];
						Color c2 = colors[j];
						isEqual = c1.getRGB() == c2.getRGB() && c1.getAlpha() == c2.getAlpha();
					}
					if(isEqual) {
						setSelectedIndex(i);
						repaint();
						return;
					}
				}
			}
			if(getItemCount()> 0) {
				setSelectedItem(0);
			}
		}

	}


	private class ColorSchemeItemPainter extends JComponent {
		private static final long serialVersionUID = -6448740563809113949L;
		private boolean isSelected = false;
		private ColorTablePainter colorTablePaint = null;
		/**
		 * Constructor method
		 *
		 * @param name
		 * @param colorTablePaint
		 * @param isSelected
		 */
		public ColorSchemeItemPainter(String name, ColorTablePainter colorTablePaint, boolean isSelected) {
			super();
			this.colorTablePaint = colorTablePaint;
			this.isSelected = isSelected;
			setToolTipText(name);
		}

		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			if (isSelected) {
				Color color1 = new Color(89, 153, 229);
				Color color2 = new Color(31, 92, 207);
				g2.setPaint(new GradientPaint(0, 1, color1, 0, getHeight() - 1, color2, false));
				g2.fillRect(0, 1, getWidth(), getHeight() - 1);
				g2.setColor(new Color(61, 123, 218));
				g2.drawLine(0, 0, getWidth(), 0);
				g2.setColor(Color.white);
			} else {
				g2.setColor(Color.white);
				g2.fillRect(0, 0, getWidth(), getHeight());
				g2.setColor(Color.black);
			}

			colorTablePaint.paint(g2, isSelected);
		}
	}
	
}
