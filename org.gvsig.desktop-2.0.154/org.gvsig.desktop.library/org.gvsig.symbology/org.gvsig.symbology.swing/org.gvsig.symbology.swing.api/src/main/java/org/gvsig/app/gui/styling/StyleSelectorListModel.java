/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: StyleSelectorListModel.java 30011 2009-07-17 11:07:14Z cordinyana $
 * $Log$
 * Revision 1.5  2007-08-14 11:10:20  jvidal
 * javadoc updated
 *
 * Revision 1.4  2007/08/07 11:21:42  jvidal
 * javadoc
 *
 * Revision 1.3  2007/05/08 15:44:07  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2007/04/04 16:01:14  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2007/03/09 11:25:00  jaume
 * Advanced symbology (start committing)
 *
 * Revision 1.1.2.4  2007/02/21 07:35:14  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.3  2007/02/08 15:43:04  jaume
 * some bug fixes in the editor and removed unnecessary imports
 *
 * Revision 1.1.2.2  2007/01/30 18:10:10  jaume
 * start commiting labeling stuff
 *
 * Revision 1.1.2.1  2007/01/26 13:49:03  jaume
 * *** empty log message ***
 *
 *
 */
package org.gvsig.app.gui.styling;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.event.ListDataListener;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolException;
import org.gvsig.fmap.mapcontext.rendering.symbols.impl.LoadSymbolException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.utils.XMLEntity;
import org.gvsig.utils.listManager.ListModel;
import org.gvsig.utils.xmlEntity.generate.XmlTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implements a list to select styles.This list
 * has the property that allows the user to stablish a filter to accept or reject
 * elements for it from a directory which is also specified when the StyleSelectorModel
 * is created.
 *
 *
 */
public class StyleSelectorListModel implements ListModel {

	private static Logger logger = LoggerFactory.getLogger(
			StyleSelectorListModel.class);
	
	public static final String STYLE_FILE_EXTENSION = ".gvssty";

	protected Vector<IStyle> elements = null;
	protected File folder = null;
	protected FileFilter fFilter = null;
	protected SelectorFilter sFilter = null;
	protected String fileExt = "";
	
	/**
	 * <p>
	 * Creates a new instance of the model for the list in the Style Selector window
	 * where the styles are stored in the <b>dir</b> (root directory) param.<br>
	 * </p>
	 * <p>The <b>currentElement</b> defines which element is pre-selected.<br></p>
	 * <p>The <b>filter</b> is a user defined filter used to know which elements in
	 * the folder are accepted or rejected for this list and it is used to avoid
	 * mixing marker styles for polygons for example.<br></p>
	 * <p><b>fileExtension</b> param defines the extension of the file to be parsed. This
	 * is like that to enable inheritance of this class to other file selector such
	 * as StyleSelector.
	 *
	 * @param dir, the root dir where styles are located.
	 * @param currentElemet, the element to be pre-selected.
	 * @param filter, the filter used to show or hide some elements.
	 * @param fileExtension, file extension used for the files to be parsed.
	 */
	public StyleSelectorListModel(
			File dir,
			SelectorFilter filter,
			String fileExtension) {
		
		fileExt = fileExtension.toLowerCase();
		folder = dir;
		sFilter = filter;
		fFilter = new FileFilter() {
			public boolean accept(File pathname) {
				if (!pathname.isFile()) {
					return false;
				} else {
					return pathname.getAbsolutePath().toLowerCase().endsWith(
							StyleSelectorListModel.this.fileExt);
				}
			}
		};
	}

	
	
	
	
	/**
	 * TODO: use the new Persistence API.
	 */
	public Vector<IStyle> getObjects() {

		if (elements == null) {

			elements = new Vector();
			File[] ff = folder.listFiles(fFilter);
			for (int i = 0; i < ff.length; i++) {

				try {
					IStyle sty = this.loadStyle(ff[i]);
					if (sFilter.accepts(sty)) {
						add(sty);
					}
						
				} catch (Exception e) {
					NotificationManager.
					addWarning("Error in file ["+ff[i].getAbsolutePath()+"]. ", e);
				}

			}
		}
		return elements;
	}


	public void add(Object o) {
		TreeSet map = new TreeSet(new Comparator() {

			public int compare(Object o1, Object o2) {

				IStyle sym1 = (IStyle) o1;
				IStyle sym2 = (IStyle) o2;
				if (sym1.getDescription() == null && sym2.getDescription() != null) return -1;
				if (sym1.getDescription() != null && sym2.getDescription() == null) return 1;
				if (sym1.getDescription() == null && sym2.getDescription() == null) return 1;

				int result = sym1.getDescription().compareTo(sym2.getDescription());
				return (result!=0) ? result: 1; /* this will allow adding symbols with
				the same value for description than
				a previous one. */
			}

		});

		map.addAll(elements);
		map.add(o);
		elements = new Vector(map);

	}

	public int getSize() {
		return getObjects().size();
	}

	public Object getElementAt(int index) {
		return getObjects().get(index);
	}
	

	public void addListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	public void removeListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	public Object remove(int i) throws ArrayIndexOutOfBoundsException {
		return elements.remove(i);
	}

	public void insertAt(int i, Object o) {
		getObjects().insertElementAt((IStyle) o, i);
	}
	
	// =========================================================
	
	private IStyle loadStyle(File file) throws Exception {
		if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);

				PersistenceManager persistenceManager = ToolsLocator
						.getPersistenceManager();

				PersistentState state = persistenceManager.loadState(fis);
				IStyle sty = (IStyle) persistenceManager.create(state);
				fis.close();
				
				return sty;
		} else {
			return null;
		}
		
	}	
}
