/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.gui.beans.listeners.BeanListener;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JNumberSpinner;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ISimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ILineStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ISimpleLineStyle;


/**
 * SimpleLine allows the user to store and modify the main properties that
 * define a <b>simple line</b>.<p>
 * <p>
 * This functionality is carried out thanks to two tabs (simple line and arrow
 * decorator)which are included in the panel to edit the properities of a symbol
 * (SymbolEditor)how is explained in AbstractTypeSymbolEditor.<p>
 * <p>
 * The first tab (Simple Line)allows the user to change the color (<b>jccColor</b>),
 * the width (<b>txtWidth</b>) and the style of the line (<b>cmbLinStyles</b>).<p>
 * <p>
 * The second tab (<b>arrowDecorator</b>)allows the user to insert a symbol in the
 * line (for example an arrow to specify its orientation)and to modify it.
 *
 *@see ArrowDecorator
 *@see AbstractTypeSymbolEditor
 *@author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class SimpleLine extends AbstractTypeSymbolEditor implements ActionListener {

	private ColorChooserPanel jccColor;
	private JNumberSpinner txtWidth;
	private ArrayList<JPanel> tabs = new ArrayList<JPanel>();
	private ArrowDecorator arrowDecorator;
	private LineProperties lineProperties;
	private JNumberSpinner txtOffset;


	public SimpleLine(SymbolEditor owner) {
		super(owner);
		initialize();
	}


	/**
	 * Initializes the parameters that define a simpleline.To do it, two tabs
	 * are created inside the SymbolEditor panel with default values for the
	 * different attributes of the simple line.This two tabs will be simple
	 * line tab (options of color, width and style of the line)and arrow
	 * decorator tab (options to "decorate" the line with a symbol).
	 */
	private void initialize() {
		JPanel myTab = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
		myTab.setName(Messages.getText("simple_line"));
		GridBagLayoutPanel aux = new GridBagLayoutPanel();

		// color chooser
		jccColor = new ColorChooserPanel(true);
		jccColor.setAlpha(255);
		aux.addComponent(Messages.getText("color"),
				jccColor	);

		// line width
		txtWidth = new JNumberSpinner(3.0, 25, 0, Double.POSITIVE_INFINITY, 1);
		aux.addComponent(Messages.getText("width")+":",
				txtWidth );

		// line offset
		txtOffset = new JNumberSpinner(0, 25, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1);
		aux.addComponent(Messages.getText("offset")+":",
				txtOffset );



		aux.setPreferredSize(new Dimension(300, 300));
		myTab.add(aux);

		// initialize defaults
		jccColor.setColor(Color.BLACK);
		txtWidth.setDouble(1.0);
		jccColor.addActionListener(this);
		txtWidth.addActionListener(this);
		txtOffset.addActionListener(this);
		tabs.add(myTab);

		// Arrow Decorator
		arrowDecorator = new ArrowDecorator();
		arrowDecorator.addListener(new BeanListener() {

			public void beanValueChanged(Object value) {
				fireSymbolChangedEvent();
			}

		});
		tabs.add(arrowDecorator);

		lineProperties = new LineProperties((float) txtWidth.getDouble());
		lineProperties.addListener(new BeanListener(){

			public void beanValueChanged(Object value) {
				fireSymbolChangedEvent();
			}
		});
		tabs.add(lineProperties);
	}


	public ISymbol getLayer() {
		ISimpleLineSymbol layer = SymbologyLocator.getSymbologyManager().createSimpleLineSymbol();
		layer.setLineColor(jccColor.getColor());
//		layer.setIsShapeVisible(true); //true is the default value for this property
		// clone the selected style in the combo box

		ISimpleLineStyle simplLine= SymbologyLocator.getSymbologyManager().createSimpleLineStyle(); //new SimpleLineStyle();

		simplLine.setStroke(lineProperties.getLinePropertiesStyle());
		simplLine.setOffset(-txtOffset.getDouble());

		IArrowDecoratorStyle ads= arrowDecorator.getArrowDecoratorStyle();
		if (ads != null) {
			ads.getMarker().setColor(jccColor.getColor());
		}
		simplLine.setArrowDecorator(arrowDecorator.getArrowDecoratorStyle());
		layer.setLineStyle(simplLine);
		layer.setLineWidth((float) txtWidth.getDouble());

		return layer;
	}

	public String getName() {
		return Messages.getText("simple_line");
	}

	public JPanel[] getTabs() {
		return (JPanel[]) tabs.toArray(new JPanel[0]);
	}

	public void refreshControls(ISymbol layer) {
		ISimpleLineSymbol sym;
		try {
			if (layer == null) {
				// initialize defaults
				System.err.println(getClass().getName()+":: should be unreachable code");
				jccColor.setColor(Color.BLACK);
				txtWidth.setDouble(1.0);
				txtOffset.setDouble(0);
			} else {
				sym = (ISimpleLineSymbol) layer;
				jccColor.setColor(sym.getColor());
				txtWidth.setDouble(sym.getLineStyle().getLineWidth());
				txtOffset.setDouble(sym.getLineStyle().getOffset() == 0 ? 0 : -sym.getLineStyle().getOffset() );
				arrowDecorator.setArrowDecoratorStyle( sym.getLineStyle().getArrowDecorator());
				/*
				 * this line discards any temp changes in the linestyle
				 * widths made by any previous rendering and sets all the
				 * values to those to be persisted.
				 */
				ILineStyle tempLineStyle = (ILineStyle) sym.getLineStyle().clone();
				lineProperties.setLinePropertiesStyle((BasicStroke) tempLineStyle.getStroke());
			}
		} catch (IndexOutOfBoundsException ioEx) {
			NotificationManager.addWarning("Symbol layer index out of bounds", ioEx);
		} catch (ClassCastException ccEx) {
			NotificationManager.addWarning("Illegal casting from " +
					layer.getClass().getName() + " to ISimpleLineSymbol.", ccEx);
		} catch (CloneNotSupportedException e) {
			NotificationManager.addWarning(
					"Symbol line style does not support cloning", e);
		}
	}

	public void actionPerformed(ActionEvent e) {
		fireSymbolChangedEvent();
	}

	public EditorTool[] getEditorTools() {
		return null;
	}


	public boolean canManageSymbol(ISymbol symbol) {
		return symbol instanceof ISimpleLineSymbol;
	}
}