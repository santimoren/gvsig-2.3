/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.swing;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Library for Swing API initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class SymbologySwingLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsAPI(SymbologySwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {

    	/*
    	 * This property is read by the batik library and prevents
    	 * hundreds of warnings like this when drawing SVG files:
    	 * 
    	 * "Graphics2D from BufferedImage lacks BUFFERED_IMAGE hint"
    	 * 
    	 * The absence of the BUFFERED_IMAGE hint does not seem to have
    	 * consequences.
    	 */
    	System.setProperty("org.apache.batik.warn_destination", "false");
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        // Validate there is any implementation registered.
        SymbologySwingManager manager =
            SymbologySwingLocator.getSwingManager();
        if (manager == null) {
            throw new ReferenceNotRegisteredException(
                SymbologySwingLocator.SWING_MANAGER_NAME,
                SymbologySwingLocator.getInstance());
        }
        
        IconThemeHelper.registerIcon("symbol", "symbol-layer-add", this);
        IconThemeHelper.registerIcon("symbol", "symbol-layer-remove", this);
        IconThemeHelper.registerIcon("symbol", "symbol-layer-move-down", this);
        IconThemeHelper.registerIcon("symbol", "symbol-layer-move-up", this);
        IconThemeHelper.registerIcon("symbol", "symbol-layer-unlock", this);
        
        IconThemeHelper.registerIcon("style", "image-keep-ratio-disabled", this);
        IconThemeHelper.registerIcon("style", "image-keep-ratio-enabled", this);

        IconThemeHelper.registerIcon("librarybrowser", "librarybrowser-folder", this);
        IconThemeHelper.registerIcon("librarybrowser", "librarybrowser-folder-image", this);

    }

}
