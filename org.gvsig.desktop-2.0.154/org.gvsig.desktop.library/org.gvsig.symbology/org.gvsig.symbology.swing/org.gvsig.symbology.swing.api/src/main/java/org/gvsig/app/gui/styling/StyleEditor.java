/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.util.*;


/**
 * Implements the panel which is composed by a previsualization of the style for the label
 * and the different tools that the user has to modify that style in order to improve the
 * representation of the labelling in the layer. The different available tools and the preview
 * of the style change according with the geometry of the layer.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class StyleEditor extends JPanel implements IWindow, ActionListener {
	private static final long serialVersionUID = -4002620456610864510L;
	private JPanel pnlNorth = null;
	private JPanel jPanel = null;
	private JPanel pnlCenter = null;
	private AcceptCancelPanel pnlButtons = null;
	private JLabel lblTitle;
	private StylePreviewer preview;
	private JPanel pnlTools;
	private JTextField txtDesc;
	private Hashtable<AbstractButton, EditorTool> tools = new Hashtable<AbstractButton, EditorTool>();
	private static ArrayList<Class> installedTools = new ArrayList<Class>();
	private EditorTool prevTool;

	private ActionListener okAction = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			PluginServices.getMDIManager().closeWindow(StyleEditor.this);
		}
	};

	private ActionListener cancelAction = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			getStylePreviewer().setStyle(null);
			PluginServices.getMDIManager().closeWindow(StyleEditor.this);
		};
	};
	/**
	 * Constructor method
	 *
	 * @param style, an specific style for the labelling of the layer.This style depends on the
	 * geometry of the layer.
	 *
	 */
	public StyleEditor(IStyle style) {
		if (style!=null) {
			IStyle sty=null;
			try {
				sty = (IStyle) style.clone(); 
			} catch (CloneNotSupportedException e) {
				NotificationManager.addWarning("Symbol layer", e);
			}
			getStylePreviewer().setStyle(sty);
			String desc = sty.getDescription();
			getTxtDesc().setText(desc);
		} else {
			getTxtDesc().setText(Messages.getText("new_style"));
		}
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        this.setLayout(new BorderLayout());
        this.setSize(550,295);
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        
        this.add(getPnlNorth(), BorderLayout.NORTH);
        this.add(getPnlCenter(), BorderLayout.CENTER);        
        this.add(getPnlButtons(), BorderLayout.SOUTH);
	}
	/**
	 * Sets the previewer of the style in the panel
	 *
	 * @param sty, style to be previewed
	 *
	 */
	public void setStyle(IStyle sty) {
		preview.setStyle(sty);
	}

	/**
	 * This method initializes the north JPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPnlNorth() {
		if (pnlNorth == null) {
		    lblTitle = new JLabel(Messages.getText("Nombre").concat(":"));
		    pnlNorth = new JPanel(new GridBagLayout());
		    GridBagConstraints c = new GridBagConstraints();
		    
		    c.fill = GridBagConstraints.BOTH;
		    c.insets = new Insets(3,3,3,3);
		    c.gridx = 0;
		    c.gridy = 0;	
		    c.weightx = 1;
			pnlNorth.add(lblTitle,c);
			
			c.gridy = 1;
			pnlNorth.add(getTxtDesc(),c);
		}
		return pnlNorth;
	}

	/**
	 * This method initializes the textDesc JTextfield
	 *
	 * @return javax.swing.JTextfield
	 */
	private JTextField getTxtDesc() {
		if (txtDesc == null) {
			txtDesc = new JTextField(30);
			txtDesc.addActionListener(this);

		}

		return txtDesc;
	}

	/**
	 * This method initializes the center JPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPnlCenter() {
		if (pnlCenter == null) {
			pnlCenter = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			
			JPanel aux = new JPanel();
			aux.add(getStylePreviewer());
			
			c.fill = GridBagConstraints.BOTH;
			c.insets = new Insets(15, 5, 5, 5);
			
			c.gridx = 0;
			c.gridy = 0;	
			c.weightx = 0.9;
			c.weighty = 1;
			pnlCenter.add(getStylePreviewer(), c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.anchor = GridBagConstraints.WEST;
			c.gridx = 1;
            c.weightx = 0.1;
			c.gridwidth = 0;
			pnlCenter.add(getPnlTools(), c);
		}
		return pnlCenter;
	}
	/**
	 * This method initializes the StylePreviewer
	 *
	 * @return StylePreviewer for the label
	 */
	public StylePreviewer getStylePreviewer() {
		if (preview == null) {
			preview = new StylePreviewer();
			preview.setShowOutline(true);
		}

		return preview;
	}

	/**
	 * This method initializes pnlButtons JPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPnlButtons() {
		if (pnlButtons == null) {
			pnlButtons = new AcceptCancelPanel(okAction, cancelAction);
		}
		return pnlButtons;
	}

	public WindowInfo getWindowInfo() {
		WindowInfo wi = new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
		wi.setTitle(Messages.getText("edit_style"));
		wi.setWidth(getWidth());
		wi.setHeight(getHeight());
		return wi;
	}

	/**
	 * Obtains the style for the label
	 *
	 * @return IStyle
	 */
	public IStyle getStyle() {
		IStyle style = getStylePreviewer().getStyle();
		if (style != null) {
			style.setDescription(txtDesc.getText());
		}
		return style;
	}

	public void actionPerformed(ActionEvent e) {
		AbstractButton srcButton = (AbstractButton) e.getSource();
		EditorTool currTool = tools.get(srcButton);
		if (currTool != null) {
			prevTool = preview.setEditorTool(currTool);
		}
	}
	/**
	 * This method initializes pnlTools JPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPnlTools() {
	    
	    I18nManager i18nManager = ToolsLocator.getI18nManager();
	    
		if (pnlTools == null) {
			pnlTools = new JPanel();
			pnlTools.setBorder(BorderFactory.createTitledBorder(Messages.getText("tools")));
			pnlTools.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
                        
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(5,5,5,5);    
            c.gridx = 0;
            c.gridy = 0;

			ArrayList<EditorTool> availableTools = new ArrayList<EditorTool>();
			IStyle sty = preview.getStyle();
			Class<?> editorClazz = null;
			Class[] constrLocator = new Class[] {JComponent.class};
			Object[] constrInitargs = new Object[] { this };
			try {
				for (Iterator<Class> iterator = installedTools.iterator(); iterator
				.hasNext();) {
					editorClazz = iterator.next();
					Constructor<EditorTool> constructor = (Constructor<EditorTool>) editorClazz.getConstructor(constrLocator);
					EditorTool editorTool = constructor.newInstance(constrInitargs);
					if (editorTool.isSuitableFor(sty)) {
						editorTool.setModel(sty);
						availableTools.add(editorTool);
					}

				}
			
				ButtonGroup group = new ButtonGroup();
				for (Iterator<EditorTool> iterator = availableTools.iterator();
				iterator.hasNext();) {
					EditorTool editorTool = iterator.next();
					AbstractButton button = editorTool.getButton();
					
					JLabel lbl = new JLabel("");
					lbl.setText(button.getToolTipText());
					
					button.addActionListener(this);

					c.gridx = 0;
					c.gridy++;
					pnlTools.add(button,c);    
					group.add(button);
					
					c.gridx = 1;
					pnlTools.add(lbl, c);

					tools.put(button, editorTool);

				}
			} catch (Exception e) {
				NotificationManager.addWarning(Messages.getText("could_not_initialize_editor_")+"'"+editorClazz+"'"+
						" ["+new Date(System.currentTimeMillis()).toString()+"]");
			}
		}
		return pnlTools;
	}
	/**
	 * Sets the tool to be used with the previous one
	 *
	 */
	public void restorePreviousTool() {
		preview.setEditorTool(prevTool);
	}
	/**
	 * Adds a new tool for the style
	 *
	 * @param styleEditorClass, the new tool to be added
	 */
	public static void addEditorTool(Class styleEditorClass) {
		installedTools.add(styleEditorClass);
	}
	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
