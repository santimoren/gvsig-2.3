/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.swing;

import java.util.Collection;
import java.util.List;
import org.gvsig.app.gui.labeling.LabelClassEditor;
import org.gvsig.app.gui.labeling.LabelClassEditorFactory;
import org.gvsig.app.gui.styling.TypeSymbolEditor;
import org.gvsig.app.project.documents.view.legend.gui.ILabelingStrategyPanel;
import org.gvsig.app.project.documents.view.legend.gui.ILegendPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;

import org.gvsig.gui.ColorTablePainter;
import org.gvsig.gui.ColorTablesFactory;
import org.gvsig.symbology.SymbologyManager;

/**
 * This class is responsible of the management of the library's swing user
 * interface. It is the swing library's main entry point, and provides all the
 * services to manage library swing components.
 * 
 * @see SymbologyWindowManager
 * @see JSymbologyServicePanel
 * @author gvSIG team
 * @version $Id$
 */
public interface SymbologySwingManager {

    /**
     * Returns the {@link SymbologyManager}.
     * 
     * @return {@link SymbologyManager}
     * @see {@link SymbologyManager}
     */
    public SymbologyManager getManager();

    /**
     * Returns the translation of a string.
     * 
     * @param key
     *            String to translate
     * @return a String with the translation of the string passed by parameter
     */
    public String getTranslation(String key);

    /**
     * Registers a new instance of a WindowManager which provides services to
     * the management of the application windows.
     * 
     * @param manager
     *            {@link SymbologyWindowManager} to register in the
     *            ScriptingUIManager.
     * @see SymbologyWindowManager
     */
    public void registerWindowManager(SymbologyWindowManager manager);

    /**
     * Returns the {@link SymbologyWindowManager}.
     * 
     * @return {@link SymbologyWindowManager}
     * @see {@link SymbologyWindowManager}
     */
    public SymbologyWindowManager getWindowManager();
    
    public void setColorTablesFactory(ColorTablesFactory factory);
    
    public ColorTablesFactory getColorTablesFactory();
    
    public List<ColorTablePainter> createColorTables();
    
    public void registerSymbolEditor(Class<? extends TypeSymbolEditor> symbolEditor, int shapeType);
    
    public List<Class<? extends TypeSymbolEditor>> getSymbolEditorClassesByGeometryType(GeometryType geometryType);
    
    public void registerLegendEditor(Class<? extends ILegendPanel> legendEditor);
    
    public List<Class<? extends ILegendPanel>> getLegendEditorClasses();
    
    public List<ILegendPanel> getLegendEditors(FLayer layer);
    
    public void registerLabelingEditor(Class<? extends ILabelingStrategyPanel> labelingEditor);
    
    public List<ILabelingStrategyPanel> getLabelingEditors();
        
    public void registerLabelClassEditor(LabelClassEditorFactory factory);

    public Collection<LabelClassEditorFactory> getLabelClassEditorFactories();

    public LabelClassEditorFactory getLabelClassEditorFactory(String id);

    public LabelClassEditorFactory getLabelClassEditorFactory(ILabelClass labelClass);

    public LabelClassEditor createLabelClassEditor(ILabelClass labelClass, FeatureStore store);

}
