/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.Component;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentContext;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.spi.PersistentContextServices;

/**
 * 
 * SymbolLibrary.java
 * 
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es Dec 7, 2007
 * 
 */
public class SymbolLibrary extends DefaultTreeModel implements ILibraryModel {
    
    private static Logger logger =
        LoggerFactory.getLogger(SymbolLibrary.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 6802576248214649793L;
	static protected String rootDirString;
	private File rootDir;
	private Vector<TreeModelListener> listeners = new Vector<TreeModelListener>();
	private static SymbolLibrary instance = null;
	private SymbolManager manager;

	public static SymbolLibrary getInstance() {
		SymbolPreferences preferences = getPreferences();
		if (instance == null
				|| !preferences.getSymbolLibraryPath().equals(rootDirString)) {
			rootDirString = preferences.getSymbolLibraryPath();
			instance = new SymbolLibrary(new File(rootDirString));
		}
		return instance;
	}

	private static SymbolPreferences getPreferences() {
		return MapContextLocator.getSymbolManager().getSymbolPreferences();
	}

	protected SymbolLibrary(File rootDir) {
		super(new DefaultMutableTreeNode(rootDir));
		rootDirString = Messages.getText("symbol_library");
		this.rootDir = rootDir;
		manager = MapContextLocator.getSymbolManager();
	}

	final class MyFile extends File {
		private static final long serialVersionUID = 6332989815291224773L;

		public MyFile(String pathname) {
			super(pathname);
		}

		public String toString() {

			String path = getAbsolutePath();
			if (path.equals(rootDir.getAbsolutePath()))
				return rootDirString;
			return path.substring(path.lastIndexOf(File.separator) + 1, path
					.length());
		}
	};

	private FileFilter ff = new FileFilter() {
		public boolean accept(File pathname) {
			return pathname.isDirectory();
		}
	};

	@Override
	public Object getRoot() {
		return new DefaultMutableTreeNode(new MyFile(rootDir.getAbsolutePath()));
	}

	private File[] listFiles(File f, FileFilter ff) {
		File[] files = null;
		if( ff == null ) {
			files = f.listFiles();
		} else {
			files = f.listFiles(ff);
		}
		if (files != null) {
			Arrays.sort(files, new Comparator<File>() {
				public int compare(File o1, File o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
		}
		return files;
	}
	
	private File[] listFiles(File f) {
		return listFiles(f,null);
	}
	
	private int countFiles(File f, FileFilter ff) {
		// No precisa ordenarlos para saber cuantos hay
		File[] files = f.listFiles(ff);
		return files == null ? 0 : files.length;
	}
	
	@Override
	public int getChildCount(Object parent) {
		File f = null;
		if (parent instanceof DefaultMutableTreeNode) {
			f = (File) ((DefaultMutableTreeNode) parent).getUserObject();

		} else {
			f = (File) parent;
		}
		return countFiles(f,ff);
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node) == 0;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners.remove(l);
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	@Override
	public Object getChild(Object parent, int index) {
		File file = listFiles((File) (((DefaultMutableTreeNode) parent).getUserObject()),ff)[index];
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(new MyFile(
				file.getAbsolutePath()));
		node.setParent((DefaultMutableTreeNode) parent);
		return node;
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if (parent == null)
			return -1;
		File[] files = listFiles((File) ((DefaultMutableTreeNode) parent).getUserObject(),ff);
		for (int i = 0; i < files.length; i++) {
			if (files[i].equals(child))
				return i;
		}
		return -1;
	}

	private Object getChildFile(Object parent, int index) {
		File file = listFiles((File) (((DefaultMutableTreeNode) parent).getUserObject()))[index];
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(new MyFile(
				file.getAbsolutePath()));
		node.setParent((DefaultMutableTreeNode) parent);
		return node;
	}

	private int getIndexOfChildFiles(Object parent, Object child) {
		if (parent == null)
			return -1;
		File[] files = listFiles((File) ((DefaultMutableTreeNode) parent).getUserObject());
		for (int i = 0; files != null && i < files.length; i++) {
			if (files[i].getName().equals(child))
				return i;
		}
		return -1;
	}

	public Object getElement(Object containerFolder, String elementName) {
		if (containerFolder instanceof File) {
			containerFolder = new DefaultMutableTreeNode(containerFolder);
		}
		int index = getIndexOfChildFiles(containerFolder, elementName);
		if (index != -1) {
			return getChildFile(containerFolder, index);
		} else {
			for (int i = 0; i < getChildCount(containerFolder); i++) {
				Object o = getElement(getChildFile(containerFolder, i),
						elementName);
				if (o != null) {
					return o;
				}
			}
		}
		return null;
	}

	public void addElement(Object element, String elementName,
			Object containerFolder) {
		if (element instanceof ISymbol) {
			ISymbol sym = (ISymbol) element;
			File folder = containerFolder == null ? rootDir
					: (File) containerFolder;

			// copy files and update fields
			try {
			    sym = (ISymbol) copyFilesToFolder(
			        sym, folder, new HashMap());
			} catch (Exception exc) {
			    logger.info("Error while copying symbol files.", exc);
			    return;
			}

			String fExtension = getPreferences().getSymbolFileExtension();
			if (!elementName.toLowerCase().endsWith(fExtension)) {
				elementName = elementName.concat(fExtension);
			}

			File targetFile = new File(folder, elementName);
			if (targetFile.exists()) {
				int resp = JOptionPane.showConfirmDialog(
						(Component) PluginServices.getMainFrame(),
						Messages.getText(
								"fichero_ya_existe_seguro_desea_guardarlo"),
						Messages.getText("guardar"),
						JOptionPane.YES_NO_OPTION);
				if (resp != JOptionPane.YES_OPTION) {
					return;
				}
			}

			try {
				MapContextLocator.getSymbolManager().saveSymbol(sym,
						elementName, folder, true);
			} catch (SymbolException e) {
				NotificationManager.addError(Messages.getText(
						"save_error"), e);
			}
		} else {
			throw new IllegalArgumentException(Messages.getText(
					"adding_a_non_symbol_as_element"));
		}
	}

	public void addFolder(Object parentFolder, String folderName) {
		if (parentFolder == null) {
			parentFolder = rootDir;
		}
		try {
			File fParentFolder = (File) ((DefaultMutableTreeNode) parentFolder)
					.getUserObject();
			File f = new File(fParentFolder.getAbsolutePath() + File.separator
					+ folderName);
			if (!f.exists())
				f.mkdir();
			for (int i = 0; i < listeners.size(); i++) {
				listeners.get(i).treeNodesInserted(null);
			}
		} catch (ConcurrentModificationException cme) {
			cme.printStackTrace();
		} catch (Exception e) {
			throw new IllegalArgumentException(Messages.getText(
					"invalid_folder_name"), e);
		}
	}

	public void removeElement(Object element, Object containerFolder) {
		try {
			File fParentFolder = (File) containerFolder;
			File f = new File(fParentFolder.getAbsolutePath() + File.separator
					+ (String) element);
			if (f.exists())
				deleteRecursively(f);

		} catch (Exception e) {
			throw new IllegalArgumentException(Messages.getText(
					"invalid_folder_name"));
		}
	}

	private void deleteRecursively(File f) {
		if (f.isDirectory()) {
			FileUtils.deleteQuietly(f);
		}
		f.delete();
	}

	public void removeFolder(Object folderToRemove) {
		try {
			File f = (File) folderToRemove;

			TreePath tp = treePathNode(f, rootDir);
			if (f.exists())
				f.delete();
		} catch (Exception e) {
			throw new IllegalArgumentException(Messages.getText(
					"invalid_folder_name"));
		}
	}

	private TreePath treePathNode(File f, File startingNode) {
		for (int i = 0; i < getChildCount(startingNode); i++) {
			File child = (File) getChild(startingNode, i);
			TreePath tp = treePathNode(f, child);
			if (tp == null) {
				// it is not in this directory tree
				continue;
			} else {
				if (startingNode != rootDir)
					tp = tp.pathByAddingChild(startingNode);
				return tp;
			}
		}

		if (f.equals(startingNode)) {
			return new TreePath(f);
		}
		return null;
	}

	private void fireTreeNodesRemoved(TreePath removedObjectTreePath) {
		TreeModelEvent e = new TreeModelEvent(this, removedObjectTreePath);
		for (Iterator<TreeModelListener> iterator = listeners.iterator(); iterator
				.hasNext();) {
			iterator.next().treeNodesRemoved(e);
		}
	}
	
	
    private static Object copyFilesToFolder(
        Object sym,
        File theFolder,
        Map processedFilesMap
        // Set processedObjSet,
        /*int dep*/) throws Exception {
        
        PersistenceManager manager =
            ToolsLocator.getPersistenceManager();
        PersistentState state = null;
        state = manager.getState(sym, true);
        PersistentContext context = state.getContext();
        
        List<PersistentState> states_list = getList(context);
        int len = states_list.size();

        for (int i=0; i<len; i++) {
            
            PersistentState aState = states_list.get(i);
            DynStruct definition = aState.getDefinition();
            DynField[] fields = definition.getDynFields();
            
            File value_file = null;
            File value_file_new = null;
            URL value_url = null;
            URI value_uri = null;
            File donefile = null;
            Object value_obj = null;
            Object value_obj_new = null;
            
            for (DynField field : fields) {
                if (field.getType() == DataTypes.FILE) {
                    
                    value_file = aState.getFile(field.getName());
                    
                    donefile = (File) processedFilesMap.get(value_file);
                    if (donefile == null) {
                        /*
                         * It was NOT processed before
                         */
                        value_file_new = copyFile(value_file, theFolder);
                        aState.set(field.getName(), value_file_new);
                        processedFilesMap.put(value_file, value_file_new);
                    } else {
                        /*
                         * It was processed before
                         */
                        aState.set(field.getName(), donefile);
                    }
                    
                    
                } else if (field.getType() == DataTypes.URL) {
                    
                    value_url = aState.getURL(field.getName());
                    if ("FILE".equalsIgnoreCase(value_url.getProtocol())) {
                        
                        File urlfile = FileUtils.toFile(value_url);
                        donefile = (File) processedFilesMap.get(urlfile);
                        if (donefile == null) {
                            value_file_new = copyFile(urlfile, theFolder);
                            value_url = value_file_new.toURI().toURL();
                            aState.set(field.getName(), value_url);
                            processedFilesMap.put(urlfile, value_file_new);
                        } else {
                            value_url = donefile.toURI().toURL();
                            aState.set(field.getName(), value_url);
                        }
                        
                    }
                    
                } else if (field.getType() == DataTypes.URI) {
                    
                    value_uri = aState.getURI(field.getName());
                    if (value_uri.getScheme() == null ||
                        "FILE".equalsIgnoreCase(value_uri.getScheme())) {
                        
                        File urifile = FileUtils.toFile(value_uri.toURL()) ;
                        donefile = (File) processedFilesMap.get(urifile);
                        if (donefile == null) {
                            value_file_new = copyFile(urifile, theFolder);
                            value_uri = value_file_new.toURI();
                            aState.set(field.getName(), value_uri);
                            processedFilesMap.put(urifile, value_file_new);
                        } else {
                            value_uri = donefile.toURI();
                            aState.set(field.getName(), value_uri);
                        }
                        
                    }
                }
            }
        }
        
        // ================================
        if (context instanceof PersistentContextServices) {
            /*
             * Ensure that previous instances are not used,
             * so objects are recreated
             */
            ((PersistentContextServices) context).clear();
        }
        Object resp = manager.create(state);
        return resp;
    }
    
    /**
     * @param context
     * @return
     */
    private static List<PersistentState> getList(PersistentContext context) {

        Iterator<PersistentState> iter = context.iterator();
        List<PersistentState> resp = new ArrayList<PersistentState>();
        while (iter.hasNext()) {
            resp.add(iter.next());
        }
        return resp;
    }

    /*
    private static Object recreate(PersistentState stat)
        throws Exception {
        
        File tmp = File.createTempFile("sym" + System.currentTimeMillis(), ".xml");
        FileOutputStream fos = new FileOutputStream(tmp);
        PersistenceManager mana = ToolsLocator.getPersistenceManager();
        mana.saveState(stat, fos);
        fos.flush();
        fos.close();
        FileInputStream fis = new FileInputStream(tmp);
        PersistentState nstat = mana.loadState(fis);
        fis.close();
        Object resp = mana.create(nstat);
        return resp;
    }
    */
    
    /**
     * Copies file to destination folder. If a file with same name
     * exists, destination name is changed. The final destination
     * file is returned.
     * 
     * @param srcfile
     * @param dstfolder
     * @return
     * @throws IOException
     */
    private static File copyFile(File srcfile, File dstfolder) 
    throws IOException {
        
        if (srcfile == null || dstfolder == null
            || !srcfile.isFile() || !dstfolder.isDirectory()
            || !srcfile.exists()) {
            throw new IOException("Bad parameters in copyFile method (src='"+srcfile+"', target='"+dstfolder+"').");
        }
        
        if (srcfile.getParentFile().equals(dstfolder)) {
            // already in place, nothing to do
            return srcfile;
        }

        if (!dstfolder.exists()) {
            dstfolder.mkdirs();
        }
        
        String checkfile = dstfolder.getCanonicalPath()
            + File.separator + srcfile.getName();
        File aux = new File(checkfile);
        File targetFile = null;
        if (aux.exists()) {
            // a file with that name exists and is not the same file
            // so we must create a new target file name
            targetFile = getAvailableFileName(dstfolder, srcfile.getName());
        } else {
            targetFile = aux;
        }
        
        FileUtils.copyFile(srcfile, targetFile);
        return targetFile;
    }

    /**
     * 
     * Gets an available file name in the folder, using
     * the name provided. It will add an index until an
     * unused name is found
     * 
     * @param dstfolder
     * @param name
     * @return
     */
    private static File getAvailableFileName(
        File dstfolder, String name) throws IOException {
        
        int index = -1;
        boolean exists = true;
        String tryname = null;
        String checkfile = null;
        File tryfile = null;
        
        while (exists) {
            index++;
            tryname = "" + index + "_" + name;
            checkfile = dstfolder.getCanonicalPath()
                + File.separator + tryname;
            tryfile = new File(checkfile);
            exists = tryfile.exists();
        }
        return tryfile;
    }
    
    
    


}
