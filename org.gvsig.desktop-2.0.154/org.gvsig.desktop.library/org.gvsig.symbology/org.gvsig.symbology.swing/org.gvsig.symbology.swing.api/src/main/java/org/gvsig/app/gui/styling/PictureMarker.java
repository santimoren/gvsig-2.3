/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JNumberSpinner;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;

/**
 * PictureMarker allows the user to store and modify the properties that define a
 * <b>picture marker symbol</b>.<p>
 * <p>
 * This functionality is carried out thanks to a tab (simple marker)which is
 * included in the panel to edit the properities of a symbol (SymbolEditor)how is
 * explained in AbstractTypeSymbolEditor.
 * <p>
 * First of all, in the above mentioned tab the user will have options to change
 * the files from where the pictures for the symbol are taken (one for the symbol
 * when it is not selected in the map and the other when it is done).<p>
 * <p>
 * Secondly, the user will have options to modify the pictures which had been
 * selected before  (width and offset) .
 *
 *@see AbstractTypeSymbolEditor
 *@author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class PictureMarker extends AbstractTypeSymbolEditor implements
ActionListener {
    
    public static final double MARKER_IMAGE_DEFAULT_WIDTH = 18;
    
	protected ArrayList<JPanel> tabs = new ArrayList<JPanel>();
	protected JNumberSpinner txtSize;
	protected JNumberSpinner txtX;
	protected JNumberSpinner txtY;
	//TODO: Comentarizado hasta que mask est� acabado
//	protected Mask mask;
	protected JLabel lblFileName;
	protected JLabel lblSelFileName;
	private File picFile;
	protected JLabel lblSize = new JLabel(Messages.getText("width")+":");
	protected JLabel lblX = new JLabel(Messages.getText("x_offset")+":");
	protected JLabel lblY = new JLabel(Messages.getText("y_offset")+":");
	private JButton btn;
	private JButton btnSel;

	private ActionListener chooseAction = new ActionListener() {

		public void actionPerformed(ActionEvent e) {

			JLabel targetLbl;
			if (e.getSource().equals(btn)) {
				targetLbl = lblFileName;
			} else {
				targetLbl = lblSelFileName;
			}
			FileFilter ff = new FileFilter() {
				public boolean accept(File f) {
					if (f.isDirectory()) return true;
					String fName = f.getAbsolutePath();
					if (fName!=null) {
						fName = fName.toLowerCase();
						return fName.endsWith(".png")
						|| fName.endsWith(".gif")
						|| fName.endsWith(".jpg")
						|| fName.endsWith(".jpeg")
						|| fName.endsWith(".bmp")
						|| fName.endsWith(".svg");
					}
					return false;
				}

				public String getDescription() {
					return Messages.getText("bitmap_and_svg_image_files");
				}
			};
			JUrlFileChooser jfc = new JUrlFileChooser(getName(), null);
			jfc.setFileFilter(ff);
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jfc.setSelectedFile(picFile);
			jfc.setMultiSelectionEnabled(false);
			int returnVal = jfc.showOpenDialog(PictureMarker.this.owner);
			if(returnVal == JFileChooser.APPROVE_OPTION) {

				URL url = jfc.getSelectedURL();
				if (url == null) return;
				try {
					targetLbl.setText(url.toURI().getPath());
				} catch (URISyntaxException e1) {
					NotificationManager.addWarning("URI Syntax error", e1);
				} //.toString());
				fireSymbolChangedEvent();
			}
//			if(returnVal == JFileChooser.APPROVE_OPTION) {
//				File myFile = jfc.getSelectedFile();
//				lastDir = jfc.getCurrentDirectory();
//				if (myFile != null && myFile.exists()) {
//					if (isSelection) {
//						selPicFile = myFile;
//					} else {
//						picFile = myFile;
//					}
//					try {
//						targetLbl.setText(myFile.toURL().toString());
//					} catch (MalformedURLException e1) {
//						NotificationManager.addError(PluginServices.getText(this, "Error en la creaci?n" +
//						"de la URL"), e1);
//					}
//					fireSymbolChangedEvent();
//				}
//			}

			btnSel.setEnabled(lblFileName.getText()!="");

		}

	};


	public PictureMarker(SymbolEditor owner) {
		super(owner);
		initialize();
	}

	/**
	 * Initializes the parameters that define a picturmarker.To do it,
	 * a tab is created inside the SymbolEditor panel with default values
	 *  for the different attributes of the picture marker.
	 */

	private void initialize() {
		JPanel myTab = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
		myTab.setName(Messages.getText("picture_marker"));
		GridBagLayoutPanel aux = new GridBagLayoutPanel();

		// picture file label
		lblFileName = new JLabel();
		lblFileName.setFont(lblFileName.getFont().deriveFont(Font.BOLD));
		aux.addComponent(Messages.getText("picture_file")+":",
				lblFileName);

		// button browse
		btn = new JButton(Messages.getText("browse"));
		btn.addActionListener(chooseAction);

		JPanel aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux2.add(btn);
		aux.addComponent("", aux2);

		// selection picture file
		lblSelFileName = new JLabel();
		lblSelFileName.setFont(lblSelFileName.getFont().deriveFont(Font.BOLD));
		aux.addComponent(Messages.getText("selection_picture_file")+":",
				lblSelFileName);

		// button browse
		btnSel = new JButton(Messages.getText("browse"));
		btnSel.addActionListener(chooseAction);
		btnSel.setEnabled(false);
		aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux2.add(btnSel);
		aux.addComponent("", aux2);

		// picture width
		txtSize = new JNumberSpinner(5, 25, 0, Double.POSITIVE_INFINITY, 0.5);
		aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux2.add(txtSize);
		aux.addComponent(lblSize, aux2 );
		txtSize.setDouble(MARKER_IMAGE_DEFAULT_WIDTH);


		// picture xOffset
		txtX = new JNumberSpinner(0, 25);
		aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux2.add(txtX);
		aux.addComponent(lblX, aux2);


		// picture width
		txtY = new JNumberSpinner(0, 25);
		aux2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		aux2.add(txtY);
		aux.addComponent(lblY,
				aux2 );


		// initialize defaults
		txtSize.addActionListener(this);
		txtX.addActionListener(this);
		txtY.addActionListener(this);
		// buttons have their own listener!!!!

		myTab.add(aux);
		tabs.add(myTab);

//		mask = new Mask(this);
//		tabs.add(mask);
	}

	public ISymbol getLayer() {
		try {
			IPictureMarkerSymbol layer = null;

			if( lblFileName.getText().equals(""))
				layer=null;

			else {
				if (lblSelFileName.getText().equals("")){
					layer =  SymbologyLocator.getSymbologyManager().createPictureMarkerSymbol(new File(lblFileName.getText()).toURI().toURL(),null);
				}else {
					layer = SymbologyLocator.getSymbologyManager().createPictureMarkerSymbol(new File(lblFileName.getText()).toURI().toURL(),new File(lblSelFileName.getText()).toURI().toURL());
				}
//				layer.setIsShapeVisible(true); //True is the default value of this property
				layer.setSize(txtSize.getDouble());
				layer.setOffset(new Point2D.Double(
					txtX.getDouble(),
					txtY.getDouble()));
//				layer.setMask(mask.getMask());
			}

			return layer;
		} catch (IOException e) {
			IWarningSymbol warning =
				(IWarningSymbol) MapContextLocator.getSymbolManager()
				.getWarningSymbol(
						SymbolDrawingException.STR_UNSUPPORTED_SET_OF_SETTINGS,
						Messages.getText("failed_acessing_files"),
						SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
			return warning;

		}


	}

	public String getName() {
		return Messages.getText("picture_marker_symbol");

	}

	public JPanel[] getTabs() {
		return tabs.toArray(new JPanel[tabs.size()]);
	}

	public void refreshControls(ISymbol layer) {
		IPictureMarkerSymbol sym;
		try {
			double size, xOffset, yOffset;
			String fileName = null, selectionFileName = null;
			if (layer == null) {
				// initialize defaults
				System.err.println(getClass().getName()+":: should be unreachable code");

				size = 1D;
				xOffset = 0D;
				yOffset = 0D;
				fileName = "-";
				selectionFileName = "-";
			} else {
				sym = (IPictureMarkerSymbol) layer;

				size = sym.getSize();
				xOffset = sym.getOffset().getX();
				yOffset = sym.getOffset().getY();

				fileName = sym.getSource().toURI().getPath(); //.toString();
				selectionFileName = sym.getSelectedSource().toURI().getPath(); //.toString();
			}

			setValues(size, xOffset, yOffset, fileName, selectionFileName);
		} catch (IndexOutOfBoundsException ioEx) {
			NotificationManager.addWarning("Symbol layer index out of bounds", ioEx);
		} catch (ClassCastException ccEx) {
			NotificationManager.addWarning("Illegal casting from " +
					layer.getClass().getName() + " to IPictureMarkerSymbol.", ccEx);
		} catch (URISyntaxException e) {
			NotificationManager.addWarning("URI Syntax error", e);
		}
	}

	protected void setValues(double size, double xOffset, double yOffset, String fileName, String selectionFileName) {
		txtSize.setDouble(size);
		txtX.setDouble(xOffset);
		txtY.setDouble(yOffset);
		lblFileName.setText(fileName);
		lblSelFileName.setText(selectionFileName);
		btnSel.setEnabled(lblFileName.getText()!="");
	}

	public EditorTool[] getEditorTools() {
		return null;

	}

	public void actionPerformed(ActionEvent e) {
		fireSymbolChangedEvent();
	}

	public boolean canManageSymbol(ISymbol symbol) {
		return symbol instanceof IPictureMarkerSymbol;
	}

}