/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.IPictureLineSymbol;

/**
 * PictureLine initializes the properties that define a
 * <b>picture marker symbol</b> and are showed in the tab created by
 * PictureMarker which is called simple marker.<p>
 * <p>
 * Moreover, PictureLine has other methods such as getSymbolClass,getName,
 * refreshControls and getLayer.
 *
 *
 *@author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class PictureLine extends PictureMarker {
	public PictureLine(SymbolEditor owner) {
		super(owner);
		initialize();
	}
	/**
	 * Initializes the values in the tab
	 *
	 */
	private void initialize() {
//		tabs.remove(mask);
		lblSize.setText(Messages.getText("width")+":");
		lblX.setText(Messages.getText("scale")+" X:");
		lblY.setText(Messages.getText("scale")+" Y:");
		txtX.setMinValue(0.1);
		txtX.setMaxValue(Double.POSITIVE_INFINITY);
		txtX.setStep(0.1);
		txtX.setDouble(1);

		txtY.setMinValue(0.1);
		txtY.setMaxValue(Double.POSITIVE_INFINITY);
		txtY.setStep(0.1);
		txtY.setDouble(1);
	}

	public String getName() {
		return Messages.getText("picture_line_symbol");

	}

	public void refreshControls(ISymbol layer) {
		IPictureLineSymbol sym;
		try {
			double size, xScale, yScale;
			String fileName = null, selectionFileName = null;

			if (layer == null) {
				// initialize defaults
				System.err.println(getClass().getName()+":: should be unreachable code");

				size = 1D;
				xScale = 1D;
				yScale = 1D;
				fileName = "-";
				selectionFileName = "-";
			} else {
				sym = (IPictureLineSymbol) layer;

				size = sym.getLineWidth();
				xScale = sym.getXScale();
				yScale = sym.getYScale();

				fileName = sym.getSource().toURI().getPath();
				selectionFileName = sym.getSelectedSource().toURI().getPath();

			}

			setValues(size, xScale, yScale, fileName, selectionFileName);
		} catch (IndexOutOfBoundsException ioEx) {
			NotificationManager.addWarning("Symbol layer index out of bounds", ioEx);
		} catch (ClassCastException ccEx) {
			NotificationManager.addWarning("Illegal casting from " +
					layer.getClass().getName() + " to IPictureLineSymbol.", ccEx);
		} catch (URISyntaxException e) {
			NotificationManager.addWarning("URI Syntax error", e);
		}
	}

	public ISymbol getLayer() {

		try {
			IPictureLineSymbol layer = null;


			if(lblFileName.getText().equals("") )
				layer=null;

			else {
				layer =  SymbologyLocator.getSymbologyManager().createPictureLineSymbol(new File(lblFileName.getText()).toURI().toURL(),null);
				if (!lblSelFileName.getText().equals("")){
					layer = SymbologyLocator.getSymbologyManager().createPictureLineSymbol(new File(lblFileName.getText()).toURI().toURL(),new File(lblSelFileName.getText()).toURI().toURL());
				}
//				layer.setIsShapeVisible(true); //true is the default value of this property
				layer.setLineWidth(txtSize.getDouble());
				layer.setXScale(txtX.getDouble());
				layer.setYScale(txtY.getDouble());
			}

			return layer;
		} catch (IOException e) {
			IWarningSymbol warning =
				(IWarningSymbol) MapContextLocator.getSymbolManager()
				.getWarningSymbol(
						SymbolDrawingException.STR_UNSUPPORTED_SET_OF_SETTINGS,
						Messages.getText("failed_acessing_files"),
						SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
			return warning;

		}

	}
	
	public boolean canManageSymbol(ISymbol symbol) {
		return symbol instanceof IPictureLineSymbol;
	}

}
