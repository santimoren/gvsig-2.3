/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.gui.styling;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.app.project.documents.view.legend.gui.ISymbolSelector;
import org.gvsig.app.project.documents.view.legend.gui.JSymbolPreviewButton;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.gui.beans.swing.JNumberSpinner;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IMarkerFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMarkerFillPropertiesStyle;
/**
 * <b>MarkerFill</b> allows the user to store and modify the properties that fills a
 * polygon with a padding made of markers and an outline<p>
 * <p>
 * This functionality is carried out thanks to two tabs (marker fill and MarkerFillProperties)
 * which are included in the panel to edit the properities of a symbol (SymbolEditor)
 * how is explained in AbstractTypeSymbolEditor.<p>
 * <p>
 * The first tab (marker fill)permits the user to select the marker for the padding and
 * other options such as the color for the fill (<b>btnChooseMarker</b>),to select the
 * ouline (<b>btnOutline</b>)and the distribution (grid or random) of the marker inside
 * the padding (<b>rdGrid,rdRandom</b>).
 * <p>
 * The second tab is implementes as a MarkerFillProperties class and offers the possibilities
 * to change the separtion and the offset.
 *
 *
 *@see MarkerFillProperties
 *@see AbstractTypeSymbolEditor
 *@author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class MarkerFill extends AbstractTypeSymbolEditor implements ActionListener,ChangeListener {
	private ArrayList<JPanel> tabs = new ArrayList<JPanel>();
	private ColorChooserPanel markerCC;
	private JButton btnChooseMarker;
	private MarkerFillProperties panelStyle = new MarkerFillProperties();
	private JRadioButton rdGrid;
	private JRadioButton rdRandom;
	private IMarkerSymbol marker = SymbologyLocator.getSymbologyManager().createSimpleMarkerSymbol();

	private JNumberSpinner txtOutlineWidth;
	private JSymbolPreviewButton btnOutline;
	private JSlider sldOutlineTransparency;
	private int outlineAlpha = 255;
	private ILineSymbol outline;
	private JCheckBox useBorder;

	/**
	 * constructor method
	 * @param owner
	 */
	public MarkerFill(SymbolEditor owner) {
		super(owner);
		initialize();
	}

	/**
	 * Initializes the parameters that allows the user to fill the padding of
	 * a polygon with a style made of markers.To do it, two tabs are created (marker
	 * fill and MarkerFillProperties)inside the SymbolEditor panel with default values
	 * for the different attributes.
	 */
	private void initialize() {
//		GridLayout layout;
		JPanel myTab;
		// Marker fill tab

		{
			myTab = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
			myTab.setName(Messages.getText("marker_fill"));

			GridBagLayoutPanel p = new GridBagLayoutPanel();
			JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
			markerCC = new ColorChooserPanel(true);
			markerCC.setAlpha(255);
			markerCC.addActionListener(this);
			aux.add(markerCC);

			p.addComponent(Messages.getText("color")+":", aux);
			btnChooseMarker = new JButton(Messages.getText("choose_marker"));
			btnChooseMarker.addActionListener(this);
			aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
			aux.add(btnChooseMarker);
			p.addComponent("", aux);

			ButtonGroup group = new ButtonGroup();
			rdGrid = new JRadioButton(Messages.getText("grid"));
			rdGrid.addActionListener(this);
			rdRandom = new JRadioButton(Messages.getText("random"));
			rdRandom.addActionListener(this);
			group.add(rdGrid);
			group.add(rdRandom);

			aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
			aux.add(rdGrid);
			aux.add(rdRandom);
			rdGrid.setSelected(true);
			p.addComponent("", aux);


			JPanel myTab2 = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
			GridBagLayoutPanel aux3 = new GridBagLayoutPanel();

			JPanel aux2 = new JPanel();
			btnOutline = new JSymbolPreviewButton(Geometry.TYPES.CURVE);
			btnOutline.setPreferredSize(new Dimension(100, 35));
			aux2.add(btnOutline);

			aux3.addComponent(new JBlank(10, 10));
			useBorder = new JCheckBox(Messages.getText("use_outline"));
			aux3.addComponent(useBorder, aux2);
			aux3.addComponent(new JBlank(10, 10));

			sldOutlineTransparency = new JSlider();
			sldOutlineTransparency.setValue(100);
			aux3.addComponent(Messages.getText("outline")+":",
					aux2);
			aux3.addComponent(Messages.getText("outline_opacity")+":", sldOutlineTransparency);
			txtOutlineWidth = new JNumberSpinner(0.0, 25, 0.0, Double.MAX_VALUE, 1.0, 2);
			aux3.addComponent(Messages.getText("outline_width")+":", txtOutlineWidth);
			myTab2.add(aux3);

			p.addComponent("", myTab2);
			myTab.add(p);

			useBorder.addActionListener(this);
			btnOutline.addActionListener(this);
			txtOutlineWidth.addActionListener(this);
			sldOutlineTransparency.addChangeListener(this);

		}
		tabs.add(myTab);

		// Fill properties tab
		tabs.add(panelStyle);
		panelStyle.addActionListener(this);
	}

	public void refreshControls(ISymbol layer) {
		if (layer == null) {
			System.err.println(getClass().getName()+":: should be unreachable code");
			// set defaults
			markerCC.setColor(Color.BLACK);
			rdGrid.setSelected(true);
			rdRandom.setSelected(false);
		} else {

			IMarkerFillSymbol mfs = (IMarkerFillSymbol) layer;
			int fillStyle = mfs.getMarkerFillProperties().getFillStyle();
			marker = mfs.getMarker();
			rdGrid.setSelected(fillStyle == IMarkerFillPropertiesStyle.GRID_FILL);
			rdRandom.setSelected(fillStyle == IMarkerFillPropertiesStyle.RANDOM_FILL);
			panelStyle.setModel(mfs.getMarkerFillProperties());
			markerCC.setColor(marker.getColor());

			//outline
			sldOutlineTransparency.removeChangeListener(this);

			outline=mfs.getOutline();
			btnOutline.setSymbol(outline);
			useBorder.setSelected(mfs.hasOutline());

			if (outline != null) {
				outlineAlpha = outline.getAlpha();
				sldOutlineTransparency.setValue((int)((outlineAlpha/255D)*100));
				txtOutlineWidth.setDouble(outline.getLineWidth());
			} else {
				sldOutlineTransparency.setValue(100);
			}

			sldOutlineTransparency.addChangeListener(this);



		}
	}

	public String getName() {
		return Messages.getText("marker_fill_symbol");
	}

	public JPanel[] getTabs() {
		return (JPanel[]) tabs.toArray(new JPanel[0]);
	}

	public void actionPerformed(ActionEvent e) {

		JComponent comp = (JComponent) e.getSource();
		if (comp.equals(btnChooseMarker)) {
			ISymbolSelector symSelect = SymbolSelector.createSymbolSelector(marker, Geometry.TYPES.POINT);
			PluginServices.getMDIManager().addWindow(symSelect);
			marker = (IMarkerSymbol) symSelect.getSelectedObject();

			if (marker == null) return;

		}

		if (!(marker instanceof IMultiLayerSymbol)) {
			marker.setColor(markerCC.getColor());
		}

		if (comp.equals(btnOutline)) {
			ISymbol sym = btnOutline.getSymbol();
			if (sym instanceof ILineSymbol) {
				ILineSymbol outline = (ILineSymbol) sym;
				if (outline != null)
					txtOutlineWidth.setDouble(outline.getLineWidth());
				}

		}

		fireSymbolChangedEvent();
	}

	public ISymbol getLayer() {
		IMarkerFillSymbol mfs = SymbologyLocator.getSymbologyManager().createMarkerFillSymbol();
		IMarkerFillPropertiesStyle prop = panelStyle.getMarkerFillProperties();
		prop.setFillStyle(rdGrid.isSelected() ?
				IMarkerFillPropertiesStyle.GRID_FILL : IMarkerFillPropertiesStyle.RANDOM_FILL);

		IMarkerSymbol myMarker;
		try {
			myMarker = (IMarkerSymbol) marker.clone();
			mfs.setMarker(myMarker);
		} catch (CloneNotSupportedException e) {
			NotificationManager.addWarning(
					"Marker Symbol does not support cloning", e);
		}

		mfs.setMarkerFillProperties(prop);

		mfs.setHasOutline(useBorder.isSelected());
		outline = (ILineSymbol) btnOutline.getSymbol();

		if (outline!=null) {
			outline.setLineWidth(txtOutlineWidth.getDouble());
			outline.setAlpha(outlineAlpha);
		}

		mfs.setOutline(outline);

		return mfs;
	}

	public EditorTool[] getEditorTools() {
		return null;
	}

	public void stateChanged(ChangeEvent e) {
		Object s = e.getSource();

		if (s.equals(sldOutlineTransparency)) {
			outlineAlpha = (int) (255*(sldOutlineTransparency.getValue()/100.0));
		}

		outline = (ILineSymbol) btnOutline.getSymbol();
		fireSymbolChangedEvent();

	}

	public boolean canManageSymbol(ISymbol symbol) {
		return symbol instanceof IMarkerFillSymbol;
	}
}
