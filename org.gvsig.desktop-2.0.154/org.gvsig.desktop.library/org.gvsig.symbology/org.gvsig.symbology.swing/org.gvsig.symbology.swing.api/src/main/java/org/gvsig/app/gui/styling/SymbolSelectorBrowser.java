/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import java.awt.BorderLayout;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * Same as symbol selector but does not
 * ass Accept/Cancel button
 * 
 * Containing wizard will have a 'Close' button
 * or similar 
 * @author jldominguez
 *
 */
public class SymbolSelectorBrowser extends SymbolSelector {
    
    public SymbolSelectorBrowser(GeometryType shpt, boolean init)
    		throws IllegalArgumentException {
        
        super(null, shpt, init);
    }
    
    /**
     * Same as the symbol selector but without the accept/cancel
     * panel
     *
     */
    protected void initialize(Object currentElement) throws IllegalArgumentException {
        library = new SymbolLibrary(rootDir);

        this.setLayout(new BorderLayout());
        this.setSize(400, 221);

        this.add(getJNorthPanel(), BorderLayout.NORTH);
        this.add(getJSplitPane(), BorderLayout.CENTER);
        this.add(getJEastPanel(), BorderLayout.EAST);

        libraryBrowser.setSelectionRow(0);

        SillyDragNDropAction dndAction = new SillyDragNDropAction();
        libraryBrowser.addMouseListener(dndAction);
        libraryBrowser.addMouseMotionListener(dndAction);
        getJListSymbols().addMouseListener(dndAction);
        getJListSymbols().addMouseMotionListener(dndAction);
        setSymbol(currentElement);
    }


}
