/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.styling;

import javax.swing.JPanel;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;


public interface TypeSymbolEditor {

    boolean canManageSymbol(ISymbol symbol);

    /**
     * Returns the editor tools that are handled by this configuration panel.
     * @return <b>EditorTool</b>
     */
    EditorTool[] getEditorTools();

    /**
     * Produces and returns  the ISymbol according with the user settings.
     *
     * @return the ISymbol.
     */
    ISymbol getLayer();

    /**
     * <p>
     * Returns the name of the config tabs that will be shown in the selector combo box.
     * This is typically a human-readable (and also translatable) name for the symbol that
     * this TypeEditorPanel deals with, but maybe you prefer to use any other one.<br>
     * </p>
     * <p>
     * The order of the entries in the combo is alphabetically-based. So you can force a
     * position by defining a name that suits your needs.
     * </p>
     * @return A human-readable text naming this panel
     */
    String getName();

    /**
     * <p>
     * Due to the complexity that many symbols settings can reach, the SymbolEditorPanel is
     * designed in a tabbed-based fashion. So, you can use as many of pages you want to put
     * your components. This pages are regular JPanels that will be automatically added to
     * the SymbolEditor dialog.<br>
     * </p>
     * <p>
     * In case you need only one page, just return a JPanel array with a length of 1.
     * </p>
     * @return An array of JPanel containing the setting's interface.
     */
    JPanel[] getTabs();

    /**
     * Invoked when the user selects or adds a new layer. This method fills up
     * the components on the right according on the layer properties
     * @param <b>ISymbol</b> layer, the symbol
     */
    void refreshControls(ISymbol layer);
    
}
