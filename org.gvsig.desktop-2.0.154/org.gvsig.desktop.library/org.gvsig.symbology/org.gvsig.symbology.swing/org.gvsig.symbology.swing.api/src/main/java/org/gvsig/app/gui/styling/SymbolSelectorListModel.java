/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.gui.styling;

import java.io.File;
import java.io.FileFilter;
import java.util.Vector;

import javax.swing.AbstractListModel;
import javax.swing.SwingUtilities;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.task.CancellableTask;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;
import org.gvsig.utils.listManager.ListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class SymbolSelectorListModel implements a list to select symbols.This list
 * has the property that allows the user to stablish a filter to accept or
 * reject elements for it from a directory which is also specified when the
 * SymbolSelectorModel is created.
 * 
 */
public class SymbolSelectorListModel extends AbstractListModel implements
		ListModel, Disposable {

	private static Logger logger = LoggerFactory.getLogger(SymbolSelectorListModel.class);
	private String fileExtension;

	private SelectorFilter sfilter;
	private Vector<ISymbol> elements;
	private File folder;
	private CancellableTask symbolLoader = null;

	
	/**
	 * <p>
	 * Creates a new instance of the model for the list in the Symbol Selector
	 * window where the symbols are stored in the <b>dir</b> (root directory)
	 * param.<br>
	 * </p>
	 * <p>
	 * The <b>filter</b> is a user defined filter used to know which elements in
	 * the folder are accepted or rejected for this list and it is used to avoid
	 * mixing marker symbols for polygons for example.<br>
	 * </p>
	 * <p>
	 * <b>fileExtension</b> param defines the extension of the file to be
	 * parsed. This is like that to enable inheritance of this class to other
	 * file selector such as StyleSelector.
	 * 
	 * @param dir
	 *            , the root dir where symbols are located.
	 * @param filter
	 *            , the filter used to show or hide some elements.
	 * @param fileExtension
	 *            , file extension used for the files to be parsed.
	 */
	public SymbolSelectorListModel(File dir, SelectorFilter filter,
			String fileExtension) {
		this.fileExtension = fileExtension;
		this.folder = dir;
		this.sfilter = filter;
	}

	public synchronized void dispose() {
		/*
		 * Quien destruye la instancia de la clase se encarga de llamar al dispose
		 * pero no hace falta hacer seguimieto de las instancias de esta que hay
		 * en ejecucion, por eso no se a�ade en el constructor la instancia a la
		 * lista de disposable del DisposableManager.
		 */
		if( this.symbolLoader!=null ) {
			this.symbolLoader.cancelRequest();
		}
	}

	public Object remove(int i) throws ArrayIndexOutOfBoundsException {
		ISymbol o = elements.remove(i);
		this.fireIntervalRemoved(this, i, i);
		return o;
	}

	public void insertAt(int i, Object o) {
		getObjects().insertElementAt((ISymbol) o, i);
		this.fireIntervalAdded(this, i, i);
	}

	protected boolean accepts(ISymbol symbol) {
		if (sfilter == null) {
			return true;
		}
		return sfilter.accepts(symbol);
	}

	public void add(Object o) {
		ISymbol symbol = (ISymbol) o;
		if (!accepts(symbol)) {
			return;
		}
		Vector<ISymbol> e = this.getObjects();
		e.add((ISymbol) o);
		try {
			int n = e.size();
			this.fireIntervalAdded(this, n - 1, n);
		} catch (Exception ex) {
			// Do nothing.
		}
	}

	public Vector<ISymbol> getObjects() {
		if (elements == null) {
			elements = new Vector<ISymbol>(0);
			FileFilter fileFiler = new FileFilter() {
				public boolean accept(File pathname) {
					return pathname.getAbsolutePath().toLowerCase().endsWith(fileExtension);
				}
			};
			Visitor visitor = new Visitor() {
				public void visit(final Object obj)
					throws VisitCanceledException, BaseException {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							add(obj);
						}
					});
				}
			};
			if( this.symbolLoader!=null ) {
				this.symbolLoader.cancelRequest();
			}
			this.symbolLoader = MapContextLocator.getSymbolManager().loadSymbols(folder,fileFiler,visitor);
			 
		}
		return elements;
	}

	public int getSize() {
		return getObjects().size();
	}

	public Object getElementAt(int index) {
		return getObjects().get(index);
	}

}
