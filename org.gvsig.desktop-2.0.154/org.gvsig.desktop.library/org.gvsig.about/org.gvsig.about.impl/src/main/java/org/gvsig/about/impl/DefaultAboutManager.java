/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.gvsig.about.AboutContribution;
import org.gvsig.about.AboutDeveloper;
import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;
import org.gvsig.about.AboutProject;
import org.gvsig.about.AboutSponsor;
import org.gvsig.about.AboutTranslator;
import org.gvsig.about.swing.JAboutPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultAboutManager implements AboutManager {

    protected List<AboutDeveloper> developers;
    protected List<AboutTranslator> translators;
    protected List<AboutSponsor> sponsors;

    protected DefaultAboutProject root;

    protected int ICON_SIZE = 16;

    public DefaultAboutManager() {
        developers = new ArrayList<AboutDeveloper>();
        sponsors = new ArrayList<AboutSponsor>();
        translators = new ArrayList<AboutTranslator>();
    }

    public void setProject(String name, URL description, URL icon) {
        root = new DefaultAboutProject(this, name, description, icon);
    }

    public void setProject(String name, URL description, URL icon,
        Map<String, String> vars) {
        root = new DefaultAboutProject(this, name, description, icon, vars);

    }

    // public AboutContribution addContribution(AboutParticipant participant,
    // String title, String description) {
    // return participant.addContribution(title, description, null, null);
    // }
    //
    // public AboutContribution addContribution(AboutParticipant participant,
    // String title, String description, Date begin) {
    // return participant.addContribution(title, description, begin, null);
    // }
    //
    // public AboutContribution addContribution(AboutParticipant participant,
    // String title, String description, Date begin, Date end) {
    // return participant.addContribution(title, description, begin, end);
    // }

    public AboutParticipant addDeveloper(String name, URL description,
        int priority) {
        return addDeveloper(name, description, priority, null);
    }

    public AboutParticipant addDeveloper(String name, URL description,
        int priority, URL icon) {

        AboutDeveloper participant = this.getDeveloper(name);

        if (participant == null) {
            participant =
                new DefaultAboutDeveloper(this, name, description, priority,
                    icon);
            // Añadimos a la lista de contribuciones
            this.developers.add(participant);
            // Ordenamos por prioridad creciente
            Collections.sort(this.developers, new Comparator<AboutDeveloper>() {

                public int compare(AboutDeveloper p1, AboutDeveloper p2) {
                    return p1.getPriority() < p2.getPriority() ? -1 : (p1
                        .getPriority() > p2.getPriority() ? +1 : 0);
                }
            });
        }

        return participant;
    }

    public AboutParticipant addSponsor(String name, URL description,
        int priority) {
        return addSponsor(name, description, priority, null);
    }

    public AboutParticipant addSponsor(String name, URL description,
        int priority, URL icon) {

        AboutSponsor participant = this.getSponsor(name);

        if (participant == null) {
            participant =
                new DefaultAboutSponsor(this, name, description, priority, icon);
            // Añadimos a la lista de contribuciones
            this.sponsors.add(participant);
            // Ordenamos por prioridad creciente
            Collections.sort(this.sponsors, new Comparator<AboutSponsor>() {

                public int compare(AboutSponsor p1, AboutSponsor p2) {
                    return p1.getPriority() < p2.getPriority() ? -1 : (p1
                        .getPriority() > p2.getPriority() ? +1 : 0);
                }
            });
        }

        return participant;
    }

    public AboutParticipant addTranslator(String name, URL description,
        int priority) {
        return addTranslator(name, description, priority, null);
    }

    public AboutParticipant addTranslator(String name, URL description,
        int priority, URL icon) {

        AboutTranslator participant = this.getTranslator(name);

        if (participant == null) {
            participant =
                new DefaultAboutTranslator(this, name, description, priority,
                    icon);
            // Añadimos a la lista de contribuciones
            this.translators.add(participant);
            // Ordenamos por prioridad creciente
            Collections.sort(this.translators,
                new Comparator<AboutTranslator>() {

                    public int compare(AboutTranslator p1, AboutTranslator p2) {
                        return p1.getPriority() < p2.getPriority() ? -1 : (p1
                            .getPriority() > p2.getPriority() ? +1 : 0);
                    }
                });
        }

        return participant;
    }

    public JPanel getAboutPanel() {
        JPanel panel = new JAboutPanel(this);
        return panel;
    }

    public List<AboutContribution> getContributions(AboutParticipant participant) {
        return participant.getContributions();
    }

    public ImageIcon getImageIcon(String name) {
        return this.getImageIcon(name, null);
    }

    public ImageIcon getImageIcon(URL url) {
        return this.getImageIcon(url, null);
    }

    public ImageIcon getImageIcon(String name, String defaultName) {
        URL url =
            this.getClass().getClassLoader()
                .getResource("org/gvsig/about/images/i16x16/" + name);
        return this.getImageIcon(url, defaultName);
    }

    public ImageIcon getImageIcon(URL url, String defaultName) {
        if (url == null) {
            url =
                this.getClass()
                    .getClassLoader()
                    .getResource("org/gvsig/about/images/i16x16/" + defaultName);
        }
        if (url == null) {
            url =
                this.getClass()
                    .getClassLoader()
                    .getResource(
                        "org/gvsig/about/images/i16x16/image-missing.png");
        }
        if (url == null) {
            url =
                this.getClass()
                    .getClassLoader()
                    .getResource("org/gvsig/about/images/i16x16/" + defaultName);
        }
        if (url == null) {
            return new ImageIcon();
        }
        return new ImageIcon(url);
    }

    public AboutProject getProject() {
        return this.root;
    }

    public List<AboutDeveloper> getDevelopers() {
        return this.developers;
    }

    public List<AboutSponsor> getSponsors() {
        return this.sponsors;
    }

    public List<AboutTranslator> getTranslators() {
        return this.translators;
    }

    // public ImageIcon validIconFormat(URL iconUrl){
    // // comprobar si existe y comprobar si es imagen
    // if(iconUrl!=null){
    //
    // File f = new File(iconUrl.getFile());
    // String mimeType=null;
    //
    // try {
    // mimeType = iconUrl.openConnection().getContentType();
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // boolean isImage = false;
    // String[] formatNames = ImageIO.getWriterMIMETypes();
    // for(int i=0;i<formatNames.length;i++){
    // if(formatNames[i].equals(mimeType)){
    // isImage = true;
    // }
    // }
    //
    // if(f.exists() && isImage){
    // ImageIcon image = new ImageIcon(iconUrl);
    // // Escalamos, si corresponde, el tamaño del icono
    // if(image.getIconHeight()>ICON_SIZE || image.getIconWidth()>ICON_SIZE){
    // float height = image.getIconHeight();
    // float width = image.getIconWidth();
    // if(height>=width && height>ICON_SIZE){
    // width = (width/height*ICON_SIZE);
    // height = ICON_SIZE;
    // }else if(width>height && width>ICON_SIZE){
    // height = (height/width*ICON_SIZE);
    // width = ICON_SIZE;
    // }
    // Image im = image.getImage();
    // image.setImage(im.getScaledInstance((int)width,(int)height, 0));
    // }
    // return image;
    // }
    // }
    //
    // return null;
    // }

    @SuppressWarnings("rawtypes")
    private AboutParticipant getParticipantByName(List l, String name) {
        Iterator it = l.iterator();
        while (it.hasNext()) {
            AboutParticipant ap = (AboutParticipant) it.next();
            if (ap.getName().equals(name)) {
                return ap;
            }
        }
        return null;
    }

    public AboutDeveloper getDeveloper(String name) {
        AboutDeveloper ap =
            (AboutDeveloper) this.getParticipantByName(this.developers, name);
        if (ap != null) {
            return ap;
        }
        return null;
    }

    public AboutSponsor getSponsor(String name) {
        AboutSponsor ap =
            (AboutSponsor) this.getParticipantByName(this.sponsors, name);
        if (ap != null) {
            return ap;
        }
        return null;
    }

    public AboutTranslator getTranslator(String name) {
        AboutTranslator ap =
            (AboutTranslator) this.getParticipantByName(this.translators, name);
        if (ap != null) {
            return ap;
        }
        return null;
    }

    public String getStringFromFile(String filename)
        throws FileNotFoundException, IOException {

        BufferedReader b = new BufferedReader(new FileReader(filename));
        StringBuffer sb = new StringBuffer();
        String str;
        while ((str = b.readLine()) != null) {
            sb.append(str);
            sb.append('\n');
        }
        return sb.toString();

    }

    public String getStringFromUrl(URL url) {
        InputStream imp = null;
        try {
            imp = url.openStream();
        } catch (IOException e) {
            return null;
        }
        BufferedReader b = new BufferedReader(new InputStreamReader(imp));
        StringBuffer sb = new StringBuffer();
        String str;
        try {
            while ((str = b.readLine()) != null) {
                sb.append(str);
                sb.append('\n');
            }
            b.close();
        } catch (IOException e) {
            // Ignore it
        }
        String data = sb.toString();

        return addBaseURLTag(url, data);
    }

    /**
     * Add an HTML base tag to the document.
     */
    private String addBaseURLTag(URL url, String data) {
        String baseTag = "<base href=\"" + getURLBase(url) + "\"/>";
        int index = data.indexOf("<head>");
        // Document contains a head section
        if (index > -1) {
            data =
                data.substring(0, index) // HTML init
                    + "<head>"
                    + baseTag
                    + data.substring(index + "<head>".length());
        } else {
            index = data.indexOf("<html>");
            // Document contains an html section
            if (index > -1) {
                data = data.substring(0, index + "<html>".length()) // HTML init
                    + "<head>" + baseTag + "</head>" // Head
                    + data.substring(index + "<html>".length());
            } else {
                // Document doesn't contain nor a head nor an html section
                data = "<head>" + baseTag + "</head>" // Head
                    + data;
            }
        }
        return data;
    }

    public String getStringFromUrl(URL url, Map<String, String> vars) {
        String data = this.getStringFromUrl(url);
        return this.expandVars(data, vars);
    }

    public String expandVars(String data, Map<String, String> vars) {
        Iterator<Map.Entry<String, String>> it = vars.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            data = replaceVariable(data, entry.getKey(), entry.getValue());
        }
        return data;
    }

    private String replaceVariable(String text, String varName,
        String replacementString) {
        String searchString = "%(".concat(varName).concat(")");
        StringBuffer sBuffer = new StringBuffer();
        int pos = 0;
        while ((pos = text.indexOf(searchString)) != -1) {
            sBuffer.append(text.substring(0, pos) + replacementString);
            text = text.substring(pos + searchString.length());
        }
        sBuffer.append(text);
        return sBuffer.toString();
    }

    public String getURLBase(URL fileURL) {
        if (fileURL != null) {
            String urlStr = fileURL.toString();
            int index = urlStr.lastIndexOf('/');
            if (index == urlStr.length() - 1) {
                // It is a / ending the URL, get the previous one
                index = urlStr.substring(0, index).lastIndexOf('/');
            }
            return urlStr.substring(0, index + 1);
        }
        return null;
    }

    // public String generateContributorsPage(AboutParticipant participant) {
    // String fileString=null;
    //
    // String page=null;
    //
    // URL resource = participant.getDescription();
    // if( resource!=null ) {
    // page = this.getStringFromUrl(resource);
    // }
    //
    // if( page != null ) {
    // String[] s = fileString.split("</body>");
    // String result = "";
    // for(int i=0;i<s.length-1;i++){
    // result = result + s[i];
    // if(i<s.length-2){
    // result = result + "</body>";
    // }
    // }
    // }
    //
    // //Si tiene contribuciones
    // List contributions = participant.getContributions();
    // if(contributions.size()>0){
    // String table = "\t<div style=\"padding-top:5px\">\n" +
    // "\t\t<div style=\"background-color: white;padding-top: 5px\">\n" +
    // "\t\t\t<center><h3>Contribuciones realizadas</h3></center>\n" +
    // "\t\t\t<br>\n" +
    // "\t\t\t<table width=\"100%\" border=\"1\">\n" +
    // "\t\t\t\t<tr>\n" +
    // "\t\t\t\t\t<td valign=\"top\" align=\"center\">Nombre</td>\n" +
    // "\t\t\t\t\t<td valign=\"top\" align=\"center\">Descripcion</td>\n" +
    // "\t\t\t\t\t<td valign=\"top\" colspan=\"2\" align=\"center\">Periodo</td>\n"
    // +
    // "\t\t\t\t</tr>\n";
    // Iterator it = contributions.iterator();
    // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    // while(it.hasNext()){
    // AboutContribution ap = (AboutContribution)it.next();
    // String cont = "";
    // if(ap.getDescription()!=null){
    // cont=ap.getDescription();
    // }
    //
    // table = table +
    // "\t\t\t\t<tr>\n" +
    // "\t\t\t\t\t <td valign=\"top\">"+ap.getTitle()+"</td>\n" +
    // "\t\t\t\t\t <td valign=\"top\">"+cont+"</td>\n" +
    // "\t\t\t\t\t  <td valign=\"top\">"+formatter.format(ap.getBegin().getTime())+"</td>\n"
    // +
    // "\t\t\t\t\t  <td valign=\"top\">"+formatter.format(ap.getEnd().getTime())+"</td>\n"
    // +
    // "\t\t\t\t</tr>\n" ;
    // }
    // table = table + "\t\t\t</table>\n\t\t</div>\n\t</div>\n";
    // result = result + table+"</body>\n";
    // }
    //
    //
    // result = result+s[s.length-1];
    // return result;
    // }

}
