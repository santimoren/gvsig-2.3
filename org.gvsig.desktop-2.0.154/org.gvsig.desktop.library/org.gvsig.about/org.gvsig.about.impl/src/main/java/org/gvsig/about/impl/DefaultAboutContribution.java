/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.impl;

import java.util.Date;

import org.gvsig.about.AboutContribution;
import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultAboutContribution implements AboutContribution {

    private AboutParticipant participant;
    private Date begin;
    private Date end;
    private String description;
    private String title;

    public DefaultAboutContribution(AboutParticipant participant, String title,
        String description, Date begin, Date end) {
        this.participant = participant;
        this.title = title;
        this.description = description;
        this.begin = begin;
        this.end = end;
    }

    public DefaultAboutContribution(AboutManager manager, String tipo,
        String participantName, String title, String description, Date begin,
        Date end) {
        AboutParticipant ap = null;
        if (tipo.equals("AboutDeveloper")) {
            ap = manager.getDeveloper(participantName);
        } else
            if (tipo.equals("AboutSponsor")) {
                ap = manager.getSponsor(participantName);
            } else
                if (tipo.equals("AboutTranslator")) {
                    ap = manager.getTranslator(participantName);
                }
        this.participant = ap;
        this.title = title;
        this.description = description;
        this.begin = begin;
        this.end = end;
    }

    public String getTitle() {
        return this.title;
    }

    public Date getEnd() {
        return this.end;
    }

    public Date getBegin() {
        return this.begin;
    }

    public AboutParticipant getParticipant() {
        return this.participant;
    }

    public String getDescription() {
        return this.description;
    }
}
