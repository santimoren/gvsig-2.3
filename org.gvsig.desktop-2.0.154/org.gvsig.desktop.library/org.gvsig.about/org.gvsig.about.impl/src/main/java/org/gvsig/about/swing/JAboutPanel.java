/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.gvsig.about.AboutParticipant;
import org.gvsig.about.AboutProject;
import org.gvsig.about.impl.DefaultAboutManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class JAboutPanel extends JPanel implements ActionListener {

    /**
	 * 
	 */
    private static final long serialVersionUID = 5605530722128487156L;
    protected DefaultAboutManager manager;
    protected JAboutBrowser browser;
    protected JContentPanel content;

    public JAboutPanel(DefaultAboutManager manager) {
        this.manager = manager;

        setLayout(new BorderLayout());

        browser = new JAboutBrowser(manager, manager.getProject());
        browser.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        browser.addDefaultActionListener(this);
        add(browser, BorderLayout.WEST);

        content = new JContentPanel(manager, manager.getProject());
        content.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(content, BorderLayout.CENTER);
        setPreferredSize(new Dimension(650, 400));
    }

    public void actionPerformed(ActionEvent arg0) {

        if (arg0.getSource() instanceof JAboutBrowser) {
            JAboutBrowser browser = (JAboutBrowser) arg0.getSource();

            if (browser.getSelectedNode() instanceof AboutProject) {
                AboutProject project = (AboutProject) browser.getSelectedNode();
                content.setContent(project);
            } else
                if (browser.getSelectedNode() instanceof AboutParticipant) {
                    AboutParticipant participant =
                        (AboutParticipant) browser.getSelectedNode();
                    content.setContent(participant);
                }

        }
    }

}
