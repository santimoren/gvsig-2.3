/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.impl;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.gvsig.about.AboutProject;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultAboutProject implements AboutProject {

    protected String name;
    protected URL description;
    protected URL icon;
    private DefaultAboutManager manager;
    private Map<String, String> vars;

    public DefaultAboutProject(DefaultAboutManager manager, String name,
        URL description, URL icon) {
        this(manager, name, description, icon, null);
    }

    public DefaultAboutProject(DefaultAboutManager manager, String name,
        URL description, URL icon, Map<String, String> vars) {
        this.manager = manager;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.vars = new HashMap<String, String>();
        if (vars != null) {
            this.vars.putAll(vars);
        }
    }

    public void set(String name, String value) {
        this.vars.put(name, value);
    }

    public String getName() {
        return this.name;
    }

    public URL getDescription() {
        return this.description;
    }

    public URL getIcon() {
        return this.icon;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getInformationPage() {
        String description = null;

        if (this.description != null) {
            description = manager.getStringFromUrl(this.description, this.vars);
        }
        if (description == null) {
            return "<html><body></body></html>";
        }
        return description;
    }

}
