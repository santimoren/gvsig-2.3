/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;
import org.gvsig.about.AboutProject;
import org.gvsig.about.impl.DefaultAboutManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class JAboutBrowser extends JPanel {

    public static final int DEFAULT_ACTION = 1;
    public static final int SELECTION_ACTION = 2;
    public static final int DROPDOWN_ACTION = 3;
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    protected DefaultAboutManager manager;
    protected DefaultMutableTreeNode rootNode;
    protected DefaultTreeModel treeModel;
    protected JTree tree;
    protected AboutProject root;
    protected ActionListener defaultActionlistener = null;
    protected ActionListener selectionActionlistener = null;
    protected ActionListener dropDownActionlistener = null;

    public static class BrowserActionEvent extends ActionEvent {

        /**
		 * 
		 */
        private static final long serialVersionUID = -1474410768278633364L;
        private Object participant;

        public BrowserActionEvent(Object source, int id, String command,
            Object participant) {
            super(source, id, command);
            this.participant = participant;
        }

        public Object getParticipant() {
            return this.participant;
        }
    }

    public JAboutBrowser(DefaultAboutManager manager, AboutProject root) {
        super(new GridLayout(1, 2));
        this.manager = manager;
        this.root = root;
        this.makeUI();
    }

    public AboutManager getManager() {
        return this.manager;
    }

    public void addDefaultActionListener(ActionListener actionlistener) {
        this.defaultActionlistener = actionlistener;
    }

    public void addSelectionActionListener(ActionListener actionlistener) {
        this.selectionActionlistener = actionlistener;
    }

    public void addDropDownActionListener(ActionListener actionlistener) {
        this.dropDownActionlistener = actionlistener;
    }

    private void makeUI() {
        // Creamos el nodo raíz

        ImageIcon icon =
            this.manager.getImageIcon(this.root.getIcon(), "gvSIG.png");
        rootNode =
            new DefaultMutableTreeNode(new NodeData(icon, icon, this.root));

        // Lo asignamos al modelo del árbol e iniciamos el árbol con ese
        // modelo
        treeModel = new DefaultTreeModel(rootNode);
        tree = new JTree(treeModel);

        // Cargamos la información inicial del árbol
        this.initializeTree();

        // Para asignar iconos especiales según el tipo de objeto
        TreeCellRenderer renderer = new IconCellRenderer();
        tree.setCellRenderer(renderer);

        // Para marcar que no son editables sus propiedades o nodos
        tree.setEditable(false);
        this.expandAll();

        // Determina que sólo se puede seleccionar una linea
        tree.getSelectionModel().setSelectionMode(
            TreeSelectionModel.SINGLE_TREE_SELECTION);

        tree.addMouseListener(new MouseListener() {

            public void mouseClicked(MouseEvent arg0) {
                if (arg0.getClickCount() == 1) {
                    JTree tree = (JTree) arg0.getSource();
                    if (!tree.isSelectionEmpty()) {
                        throwEventElementSelected(tree.getSelectionPath());
                    }

                }
            }

            public void mouseEntered(MouseEvent arg0) {
            }

            public void mouseExited(MouseEvent arg0) {
            }

            public void mousePressed(MouseEvent arg0) {
            }

            public void mouseReleased(MouseEvent arg0) {
            }
        });

        // Indica si el nodo raíz actúa como un nodo más o queda desplegado
        // de
        // manera fija
        tree.setShowsRootHandles(true);

        // Añadimos el Scroll
        JScrollPane scrollPane = new JScrollPane(tree);
        add(scrollPane);

        setPreferredSize(new Dimension(200, 300));

    }

    public Object getSelectedNode() {
        DefaultMutableTreeNode node;
        if (tree.getSelectionPath() != null) {
            node =
                (DefaultMutableTreeNode) tree.getSelectionPath()
                    .getLastPathComponent();
        } else {
            node = this.rootNode;
        }
        NodeData nodeData = (NodeData) node.getUserObject();
        if (nodeData.getParticipant() != null) {
            return nodeData.getParticipant();
        } else
            if (nodeData.getProject() != null) {
                return nodeData.getProject();
            }
        return null;
    }

    private void throwEventElementSelected(TreePath path) {
        DefaultMutableTreeNode node =
            (DefaultMutableTreeNode) path.getLastPathComponent();
        NodeData nodeData = (NodeData) node.getUserObject();
        if (nodeData.getParticipant() != null) {
            if (this.defaultActionlistener != null) {
                ActionEvent event =
                    new BrowserActionEvent(this, DEFAULT_ACTION, "default",
                        nodeData.getParticipant());
                this.defaultActionlistener.actionPerformed(event);
            }
        } else
            if (nodeData.getProject() != null) {
                if (this.defaultActionlistener != null) {
                    ActionEvent event =
                        new BrowserActionEvent(this, DEFAULT_ACTION, "default",
                            nodeData.getProject());
                    this.defaultActionlistener.actionPerformed(event);
                }
            }
    }

    /**
     * Función para la inicialización de los datos del árbol.
     **/
    @SuppressWarnings("rawtypes")
    private void initializeTree() {

        AboutParticipant ap = null;
        List participants;
        ImageIcon icon;
        NodeData data;

        NodeData sponsors =
            new NodeData(this.manager.getImageIcon("folder.png"),
                this.manager.getImageIcon("folder-drag-accept.png"), "Sponsors");
        DefaultMutableTreeNode sponsorsNode =
            this.addObject(this.rootNode, sponsors);
        participants = this.manager.getSponsors();
        for (int i = 0; i < participants.size(); i++) {
            ap = (AboutParticipant) participants.get(i);
            icon =
                (this.manager).getImageIcon(ap.getIcon(),
                    "applications-internet.png");
            data = new NodeData(icon, icon, ap);
            this.addObject(sponsorsNode, data);
        }

        NodeData developers =
            new NodeData(this.manager.getImageIcon("edit.png"),
                this.manager.getImageIcon("edit.png"), "Developers");
        DefaultMutableTreeNode developersNode =
            this.addObject(this.rootNode, developers);
        participants = this.manager.getDevelopers();
        for (int i = 0; i < participants.size(); i++) {
            ap = (AboutParticipant) participants.get(i);
            icon =
                this.manager.getImageIcon(ap.getIcon(),
                    "preferences-system.png");
            data = new NodeData(icon, icon, ap);
            this.addObject(developersNode, data);
        }

        NodeData translators =
            new NodeData(this.manager.getImageIcon("system-users.png"),
                this.manager.getImageIcon("system-users.png"), "Contributors");
        DefaultMutableTreeNode translatorsNode =
            this.addObject(this.rootNode, translators);
        participants = this.manager.getTranslators();

        for (int i = 0; i < participants.size(); i++) {
            ap = (AboutParticipant) participants.get(i);
            icon = this.manager.getImageIcon(ap.getIcon(), "people.png");
            data = new NodeData(icon, icon, ap);
            this.addObject(translatorsNode, data);
        }

    }

    public void expandAll() {
        int row = 0;
        while (row < this.tree.getRowCount()) {
            this.tree.expandRow(row);
            row++;
        }
    }

    /**
     * Función para añadir un hijo al nodo actual.
     **/
    // private DefaultMutableTreeNode addObject(Object child) {
    // DefaultMutableTreeNode parentNode = rootNode;
    // return addObject(parentNode, child, true);
    // }

    private DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
        Object child) {
        return addObject(parent, child, false);
    }

    private DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
        Object child, boolean shouldBeVisible) {
        DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);

        // Si no especificamos padre, lo colgará del raíz
        if (parent == null) {
            parent = rootNode;
        }

        // Insertamos el nuevo nodo en el modelo del árbol
        treeModel.insertNodeInto(childNode, parent, parent.getChildCount());

        // Para que el usuario pueda ver el nuevo nodo
        if (shouldBeVisible) {
            tree.scrollPathToVisible(new TreePath(childNode.getPath()));
        }

        return childNode;
    }

    /**
     * Clase IconData permite asociar hasta dos iconos (para cuando está
     * expandido y cuando no) a un objeto del árbol
     **/
    static private class NodeData {

        private Icon m_icon;
        private Icon m_expandedIcon;
        private Object m_data;

        public NodeData(Icon icon, Icon expandedIcon, Object data) {
            m_icon = icon;
            m_expandedIcon = expandedIcon;
            m_data = data;
        }

        public Icon getIcon() {
            return m_icon;
        }

        public Icon getExpandedIcon() {
            return m_expandedIcon != null ? m_expandedIcon : m_icon;
        }

        public AboutParticipant getParticipant() {
            try {
                return (AboutParticipant) m_data;
            } catch (Exception e) {
                return null;
            }
        }

        public AboutProject getProject() {
            try {
                return (AboutProject) m_data;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        public String toString() {
            return m_data.toString();
        }
    }

    private class IconCellRenderer extends JLabel implements TreeCellRenderer {

        /**
		 * 
		 */
        private static final long serialVersionUID = 1L;
        protected Color m_textSelectionColor;
        protected Color m_textNonSelectionColor;
        protected Color m_bkSelectionColor;
        protected Color m_bkNonSelectionColor;
        protected Color m_borderSelectionColor;

        protected boolean m_selected;

        public IconCellRenderer() {
            super();
            m_textSelectionColor =
                UIManager.getColor("Tree.selectionForeground");
            m_textNonSelectionColor = UIManager.getColor("Tree.textForeground");
            m_bkSelectionColor = UIManager.getColor("Tree.selectionBackground");
            m_bkNonSelectionColor = UIManager.getColor("Tree.textBackground");
            m_borderSelectionColor =
                UIManager.getColor("Tree.selectionBorderColor");
            setOpaque(false);
        }

        public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            Object obj = node.getUserObject();
            setText(obj.toString());

            if (obj instanceof Boolean) {
                setText("Retrieving data...");
            }
            if (obj instanceof NodeData) {
                NodeData nodeData = (NodeData) obj;
                if (expanded) {
                    setIcon(nodeData.getExpandedIcon());
                } else {
                    setIcon(nodeData.getIcon());
                }
            } else {
                setIcon(null);
            }

            setFont(tree.getFont());
            setForeground(sel ? m_textSelectionColor : m_textNonSelectionColor);
            setBackground(sel ? m_bkSelectionColor : m_bkNonSelectionColor);
            m_selected = sel;
            return this;
        }

        @Override
        public void paintComponent(Graphics g) {
            Color bColor = getBackground();
            Icon icon = getIcon();

            g.setColor(bColor);
            int offset = 0;
            if ((icon != null) && (getText() != null)) {
                offset = (icon.getIconWidth() + getIconTextGap());
            }
            g.fillRect(offset, 0, getWidth() - 1 - offset, getHeight() - 1);

            if (m_selected) {
                g.setColor(m_borderSelectionColor);
                g.drawRect(offset, 0, getWidth() - 1 - offset, getHeight() - 1);
            }
            super.paintComponent(g);
        }
    }

}
