/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.impl;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.gvsig.about.AboutContribution;
import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class AbstractParticipant implements AboutParticipant {

    private String name;
    private URL description;
    private int priority;
    private URL icon;
    protected List<AboutContribution> contributions;
    private DefaultAboutManager manager;
    private Map<String, String> vars;

    public AbstractParticipant(AboutManager manager, String name,
        URL description, int priority, URL icon) {
        this.manager = (DefaultAboutManager) manager;
        this.name = name;
        this.description = description;
        this.priority = priority;
        this.icon = icon;
        this.contributions = new ArrayList<AboutContribution>();
        this.vars = new HashMap<String, String>();
    }

    public void addVariables(Map<String, String> vars) {
        this.vars.putAll(vars);
    }

    public void set(String name, String value) {
        this.vars.put(name, value);
    }

    public String getName() {
        return this.name;
    }

    public URL getDescription() {
        return this.description;
    }

    public int getPriority() {
        return this.priority;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public URL getIcon() {
        return this.icon;
    }

    public AboutManager getManager() {
        return this.manager;
    }

    protected Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
        return cal.getTime();
    }

    public AboutContribution addContribution(String title, String description,
        int begin_year, int begin_month, int begin_day, int end_year,
        int end_month, int end_day) {
        return this.addContribution(title, description,
            getDate(begin_year, begin_month, begin_day),
            getDate(end_year, end_month, end_day));
    }

    public AboutContribution addContribution(String title, String description,
        Date begin, Date end) {
        AboutContribution contribution =
            new DefaultAboutContribution(this, title, description, begin, end);
        // Añadimos a la lista de contribuciones
        this.contributions.add(contribution);
        // Ordenamos por fecha de inicio
        Collections.sort(this.contributions,
            new Comparator<AboutContribution>() {

                public int compare(AboutContribution p1, AboutContribution p2) {
                    if (p1.getBegin().before(p2.getBegin())) {
                        return -1;
                    } else
                        if (p1.getBegin().after(p2.getBegin())) {
                            return 1;
                        } else {
                            return 0;
                        }
                }
            });

        return contribution;
    }

    public List<AboutContribution> getContributions() {
        return this.contributions;
    }

    public String getInformationPage() {
        String description = null;
        String contributionsTable = this.getContributionsTable();

        if (this.description != null) {
            description = manager.getStringFromUrl(this.description, this.vars);
        }
        if (description == null) {
            return "<html>\n" // Begin html
                + "<body>\n" + contributionsTable + "</body>\n" // Body
                + "</html>"; // End html
        }

        String baseTag =
            "<base href=\"" + manager.getURLBase(getDescription()) + "\"/>";

        int index = description.indexOf("<head>");
        if (index > -1) {
            description =
                description.substring(0, index) // HTML init
                    + "<head>"
                    + baseTag
                    + description.substring(index + "<head>".length());
        } else {
            index = description.indexOf("<html>");
            if (index > -1) {
                description =
                    description.substring(0, index + "<html>".length())
                        + "<head>" + baseTag + "</head>" // Head
                        + description.substring(index + "<html>".length());
            } else {
                description = "<head>" + baseTag + "</head>" // Head
                    + description;
            }
        }

        if (description.toLowerCase().indexOf("</body>") < 0) {
            return description + "\n" + contributionsTable;
        }
        return Pattern.compile("</body>", Pattern.CASE_INSENSITIVE)
            .matcher(description)
            .replaceFirst(contributionsTable + "\n</body>");
    }

    public String getContributionsTable() {
        List<AboutContribution> contributions = this.getContributions();
        if (contributions.size() < 1) {
            return "";
        }
        String table =
            "\t<div style=\"padding-top:5px\">\n"
                + "\t\t<div style=\"background-color: white;padding-top: 5px\">\n"
                + "\t\t\t<center><h3>Contribuciones realizadas</h3></center>\n"
                + "\t\t\t<br>\n"
                + "\t\t\t<table width=\"100%\" border=\"1\">\n"
                + "\t\t\t\t<tr>\n"
                + "\t\t\t\t\t<td valign=\"top\" align=\"center\">Nombre</td>\n"
                + "\t\t\t\t\t<td valign=\"top\" align=\"center\">Descripcion</td>\n"
                + "\t\t\t\t\t<td valign=\"top\" colspan=\"2\" align=\"center\">Periodo</td>\n"
                + "\t\t\t\t</tr>\n";
        Iterator<AboutContribution> it = contributions.iterator();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        while (it.hasNext()) {
            AboutContribution ap = it.next();
            String cont = "";
            if (ap.getDescription() != null) {
                cont = ap.getDescription();
            }

            table =
                table + "\t\t\t\t<tr>\n" + "\t\t\t\t\t <td valign=\"top\">"
                    + ap.getTitle() + "</td>\n"
                    + "\t\t\t\t\t <td valign=\"top\">" + cont + "</td>\n"
                    + "\t\t\t\t\t  <td valign=\"top\">"
                    + formatter.format(ap.getBegin().getTime()) + "</td>\n"
                    + "\t\t\t\t\t  <td valign=\"top\">"
                    + formatter.format(ap.getEnd().getTime()) + "</td>\n"
                    + "\t\t\t\t</tr>\n";
        }
        table = table + "\t\t\t</table>\n\t\t</div>\n\t</div>\n";
        return table;
    }

}
