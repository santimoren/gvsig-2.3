/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.htmlBrowser;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class SimpleHtmlBrowserPanel extends HtmlBrowserPanel implements
    HyperlinkListener {

    private static final long serialVersionUID = 2222495134882233629L;
    private static final Logger LOG = LoggerFactory
        .getLogger(SimpleHtmlBrowserPanel.class);

    public JEditorPane htmlPane;
    public JScrollPane scroll;

    public SimpleHtmlBrowserPanel() {
        setLayout(new BorderLayout());

        this.htmlPane = new JEditorPane();
        this.htmlPane.setContentType("text/html");
        this.htmlPane.setEditable(false);

        this.scroll = new JScrollPane(htmlPane);
        // htmlPane.addHyperlinkListener(this);

        add(scroll, BorderLayout.CENTER);
    }

    @Override
    public void navigate(URL url) {
        try {
            this.htmlPane.setPage(url);
        } catch (IOException e) {
            this.htmlPane.setText("Error reading html content");
            LOG.error("Error reading html content from URL: " + url, e);
        }
    }

    @Override
    public void setText(String text) {
        this.htmlPane.setText(text);
        this.htmlPane.setCaretPosition(0);
    }

    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            try {
                htmlPane.setPage(event.getURL());
            } catch (IOException ioe) {
                this.htmlPane.setText("Error reading html content");
                LOG.error(
                    "Error reading html content from URL: " + event.getURL(),
                    ioe);
            }
        }
    }

    public JEditorPane getJEditorPane() {
        return htmlPane;
    }

}
