/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about.swing;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;
import org.gvsig.about.AboutProject;
import org.gvsig.htmlBrowser.HtmlBrowserPanel;
import org.gvsig.htmlBrowser.SimpleHtmlBrowserPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class JContentPanel extends JPanel /* implements HyperlinkListener */{

    /**
	 * 
	 */
    private static final long serialVersionUID = 5605530722128487156L;
    protected AboutManager manager;
    HtmlBrowserPanel htmlPane;

    public JContentPanel(AboutManager manager, AboutProject project) {
        this.manager = manager;
        this.htmlPane = null;

        setLayout(new BorderLayout());
        this.htmlPane = new SimpleHtmlBrowserPanel();
        add(this.htmlPane, BorderLayout.CENTER);

        this.setContent(manager.getProject());
        setVisible(true);
    }

    public void setContent(AboutParticipant participant) {
        htmlPane.setText(participant.getInformationPage());
    }

    public void setContent(AboutProject project) {
        htmlPane.setText(project.getInformationPage());
    }

}
