/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about;

import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public interface AboutManager {

    public void setProject(String name, URL description, URL icon);

    public void setProject(String name, URL description, URL icon,
        Map<String, String> vars);

    /**
     * Add a developer information if not exists
     */
    public AboutParticipant addDeveloper(String name, URL description,
        int priority);

    public AboutParticipant addDeveloper(String name, URL description,
        int priority, URL icon);

    /**
     * Add a sponsor information if not exists
     */
    public AboutParticipant addSponsor(String name, URL description,
        int priority);

    public AboutParticipant addSponsor(String name, URL description,
        int priority, URL icon);

    /**
     * Add a translator information if not exists
     */
    public AboutParticipant addTranslator(String name, URL description,
        int priority);

    public AboutParticipant addTranslator(String name, URL description,
        int priority, URL icon);

    public AboutProject getProject();

    public List<AboutDeveloper> getDevelopers();

    public List<AboutSponsor> getSponsors();

    public List<AboutTranslator> getTranslators();

    public AboutSponsor getSponsor(String name);

    public AboutDeveloper getDeveloper(String name);

    public AboutTranslator getTranslator(String name);

    /* API of the swing part */
    public JPanel getAboutPanel();

    /**
     * Returns the base path of a file URL.
     * <p>
     * The method will simply cut the last path of the URL. 
     * For example, if the URL is:
     * </p>
     * http://www.gvsig.org/docs/file.html
     * <p>
     * The returned base string would be:
     * </p>
     * http://www.gvsig.org/docs/
     * @param fileURL the {@link URL} of the file
     * @return the base path of a file URL
     */
    public String getURLBase(URL fileURL);
}
