/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public interface AboutParticipant {

    public String getName();

    public int getPriority();

    public URL getDescription();

    public URL getIcon();

    public List<AboutContribution> getContributions();

    public AboutContribution addContribution(String title, String description,
        Date begin, Date end);

    public AboutContribution addContribution(String title, String description,
        int begin_year, int begin_month, int begin_day, int end_year,
        int end_month, int end_day);

    public String getInformationPage();

    public String getContributionsTable();

    public AboutManager getManager();

    public void addVariables(Map<String, String> vars);

    public void set(String name, String value);

}
