/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.about;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class AboutLocator extends BaseLocator {

    private static final String LOCATOR_NAME = "AboutLocator";

    public static final String MANAGER_NAME = "aboutlocator.manager";

    public static final String MANAGER_DESCRIPTION = "About Manager";

    /**
     * Unique instance.
     */
    private static final AboutLocator instance = new AboutLocator();

    /**
     * Return the singleton instance.
     * 
     * @return the singleton instance
     */
    public static AboutLocator getInstance() {
        return instance;
    }

    @Override
    public String getLocatorName() {
        return LOCATOR_NAME;
    }

    /**
     * Return a reference to AboutManager.
     * 
     * @return a reference to AboutManager
     * @throws LocatorException
     *             if there is no access to the class or the class cannot be
     *             instantiated
     * @see Locator#get(String)
     */
    public static AboutManager getManager() throws LocatorException {
        return (AboutManager) getInstance().get(MANAGER_NAME);
    }

    /**
     * Registers the Class implementing the AboutManager interface.
     * 
     * @param clazz
     *            implementing the AboutManager interface
     */
    public static void registerManager(Class<? extends AboutManager> clazz) {
        getInstance().register(MANAGER_NAME, MANAGER_DESCRIPTION, clazz);
    }

}
