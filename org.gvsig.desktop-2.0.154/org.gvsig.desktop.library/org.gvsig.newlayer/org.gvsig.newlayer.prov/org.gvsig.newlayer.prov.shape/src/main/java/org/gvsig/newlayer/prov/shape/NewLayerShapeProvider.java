/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.shape;

import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.prov.file.AbstractNewLayerFileProvider;

/**
 * NewLayer provider to create a shape file.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerShapeProvider extends AbstractNewLayerFileProvider {
   
    protected NewLayerShapeProvider(NewLayerService service) {
        super(service);
        this.storeName = "Shape";
    }

    @Override
    protected String getFileParameterName() {
        return "shpFile";
    }

    @Override
    protected String getFileExtension() {       
        return "shp";
    } 
    
    public NewFeatureStoreParameters getNewStoreParameters() {
        NewFeatureStoreParameters storeParameters =
            super.getNewStoreParameters();
        if (storeParameters != null){
            storeParameters.setDynValue("geometryType", null);
        }
        return storeParameters;
    }

    @Override
	public DataStoreParameters getOpenStoreParameters() {
		DataStoreParameters openStoreParameters = super
				.getOpenStoreParameters();
		openStoreParameters.setDynValue("CRS", getNewStoreParameters()
				.getDynValue("CRS"));
		return openStoreParameters;
	}
}
