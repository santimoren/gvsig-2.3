/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.shape;

import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerProvider;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.prov.file.AbstractNewLayerFileProviderFactory;

/**
 * Factory to create shapes.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerShapeProviderFactory extends
    AbstractNewLayerFileProviderFactory {

    private static final String PROVIDER_NAME = "Shape";

    public boolean isSpatial() {
        return true;
    }

    public NewLayerProvider create(NewLayerService service) {
        return new NewLayerShapeProvider(service);
    }

    public String getName() {
        return PROVIDER_NAME;
    }

    public String getDescription() {
        return Messages.getText("newlayer_shape_description");
    }

    public String getLabel() {
        return Messages.getText("newlayer_shape_label");
    }
}
