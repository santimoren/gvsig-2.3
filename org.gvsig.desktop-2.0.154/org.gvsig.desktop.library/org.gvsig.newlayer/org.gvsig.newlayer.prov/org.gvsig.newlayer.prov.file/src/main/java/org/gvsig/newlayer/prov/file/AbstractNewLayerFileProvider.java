/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.newlayer.NewLayerProviderPanel;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.spi.AbstractNewLayerProvider;
import org.gvsig.tools.exception.BaseRuntimeException;

/**
 * NewLayer provider to create a shape file.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractNewLayerFileProvider extends AbstractNewLayerProvider {
    protected static final Logger LOG =
        LoggerFactory.getLogger(AbstractNewLayerFileProvider.class);

    protected List<NewLayerProviderPanel> panels = new ArrayList();
    protected NewLayerFileProviderPanel fileProviderPanel = null;
    protected NewFeatureStoreParameters storeParameters = null;

    protected AbstractNewLayerFileProvider(NewLayerService service) {
		super(service);
		fileProviderPanel = new NewLayerFileProviderPanel(this);
		panels.add(fileProviderPanel);
    }

    public List<NewLayerProviderPanel> getPanels() {
        return panels;
    }

    public EditableFeatureType getFeatureType() {
        return this.getNewStoreParameters().getDefaultFeatureType();
    }

    protected abstract String getFileParameterName();

    protected abstract String getFileExtension();

    public NewFeatureStoreParameters getNewStoreParameters() {
        DataManager dataManager = DALLocator.getDataManager();

        try {
            if (this.explorer == null){
                DataServerExplorerParameters explorerParameters =
                    dataManager.createServerExplorerParameters("FilesystemExplorer");
                this.explorer =
                    dataManager.openServerExplorer("FilesystemExplorer", explorerParameters);
            }

            File selectedFile = fileProviderPanel.getSelectedFile();
            String fileName = selectedFile.getName();
            if (fileName.indexOf(".") < 0){
                fileName = fileName + "." + this.getFileExtension();
            }
            if (selectedFile.getParent()!=null){
                selectedFile = new File(selectedFile.getParent() + File.separator + fileName);
            }else{
                selectedFile = new File(fileName);
            }
            if (!selectedFile.isAbsolute()){
                String home=System.getProperty("user.home");
                selectedFile=new File(home+ File.separator + fileName);
            }

            //If the file has not been updated, returns the same parameters
            if (this.storeParameters != null){
                File previousFile = (File)storeParameters.getDynValue(getFileParameterName());
                if (previousFile.equals(selectedFile)){
                    return storeParameters;
                }
            }

            storeParameters =
                (NewFeatureStoreParameters) explorer.getAddParameters(this.getStoreName());
            storeParameters.setDynValue(
                getFileParameterName(), selectedFile);
            return storeParameters;
        } catch (InitializeException e) {
            LOG.error("Can't initialize store parameters.");
            throw new CantInitializeStoreParametersException(e);
        } catch (ProviderNotRegisteredException e) {
            LOG.error("Can't create store parameters, provider not registered.");
            throw new CantCreateStoreParametersException(e);
        } catch (DataException e) {
            LOG.error("Can't create store parameters, provider not registered.");
            throw new CantCreateStoreParametersException(e);
        } catch (ValidateDataParametersException e) {
            LOG.error("Can't create the explorter.");
            throw new CantCreateExplorerException(e);
        }
    }

	public DataStoreParameters getOpenStoreParameters() {
		DataManager dataManager = DALLocator.getDataManager();

		try {
			File selectedFile = fileProviderPanel.getSelectedFile();
			String fileName = selectedFile.getName();
			if (fileName.indexOf(".") < 0) {
				fileName = fileName + "." + this.getFileExtension();
			}
            if (selectedFile.getParent()!=null){
                selectedFile = new File(selectedFile.getParent() + File.separator + fileName);
            }else{
                selectedFile = new File(fileName);
            }
            if (!selectedFile.isAbsolute()){
                String home=System.getProperty("user.home");
                selectedFile=new File(home+ File.separator + fileName);
            }

			DataStoreParameters storeParameters = (DataStoreParameters) dataManager
					.createStoreParameters(this.getStoreName());
			storeParameters.setDynValue(getFileParameterName(), selectedFile);
			return storeParameters;
		} catch (InitializeException e) {
			throw new CantInitializeStoreParametersException(e);
		} catch (ProviderNotRegisteredException e) {
			throw new CantCreateStoreParametersException(e);
		}
	}

    class CantCreateExplorerException extends BaseRuntimeException {
        private static final long serialVersionUID = -3487961532247258393L;
        private final static String MESSAGE_FORMAT = "Can't create explorer.";
        private final static String MESSAGE_KEY = "_Cant_create_explorer";

        public CantCreateExplorerException(Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
        }
    }

    class CantCreateStoreParametersException extends BaseRuntimeException {
        private static final long serialVersionUID = -3487961532247258393L;
        private final static String MESSAGE_FORMAT = "Can't create store parameters, provider not registered.";
        private final static String MESSAGE_KEY = "_Cant_create_store_parameters_provider_not_registered";

        public CantCreateStoreParametersException(Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
        }
    }

    class CantInitializeStoreParametersException extends BaseRuntimeException {
        private static final long serialVersionUID = -5263149018209261288L;
        private final static String MESSAGE_FORMAT = "Can't initialize store parameters.";
        private final static String MESSAGE_KEY = "_Cant_initialize_store_parameters";

        public CantInitializeStoreParametersException(Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
        }
    }

 }
