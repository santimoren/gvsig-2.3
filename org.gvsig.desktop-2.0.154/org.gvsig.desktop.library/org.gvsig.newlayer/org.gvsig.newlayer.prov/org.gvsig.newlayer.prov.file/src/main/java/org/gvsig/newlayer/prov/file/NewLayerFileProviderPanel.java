/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.file;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;

import javax.swing.JOptionPane;

import org.gvsig.gui.beans.wizard.panel.SelectFileOptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerException;
import org.gvsig.newlayer.NewLayerProviderPanel;

public class NewLayerFileProviderPanel extends NewLayerProviderPanel{
    private SelectFileOptionPanel selectFileOptionPanel; 
    
	protected NewLayerFileProviderPanel(AbstractNewLayerFileProvider provider) {
		super(provider);
		this.setLayout(new BorderLayout());
        selectFileOptionPanel =
            new org.gvsig.gui.beans.wizard.panel.SelectFileOptionPanel(null);
        add(selectFileOptionPanel, BorderLayout.NORTH);
	}

	public AbstractNewLayerFileProvider getProvider() {
		return (AbstractNewLayerFileProvider) this.provider;
	}
	
	public File getSelectedFile(){
	    return selectFileOptionPanel.getSelectedFile();
	}

    @Override
    public String getTitle() {
        return Messages.getText("output_file");
    }

    @Override
    public boolean isValidPanel() throws NewLayerException {
        File file = selectFileOptionPanel.getSelectedFile();
        if ((file == null) || (file.equals(""))) {
            throw new NotEmptyFileException();
        }
        if (file.exists()) {
            int resp =
                JOptionPane
                    .showConfirmDialog(
                        (Component) selectFileOptionPanel.getParent(),
                        Messages.getText("fichero_ya_existe_seguro_desea_guardarlo"),
                        Messages.getText("guardar"),
                        JOptionPane.YES_NO_OPTION);
            if (resp == JOptionPane.NO_OPTION) {
                return false;
            }
        }
        return true;       
    }
    
    class NotEmptyFileException extends NewLayerException {
        private static final long serialVersionUID = -3619912245694177044L;
        private final static String MESSAGE_FORMAT = "The file can not be empty.";
        private final static String MESSAGE_KEY = "_file_can_not_be_empty_exception";

        public NotEmptyFileException() {
            super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
        }
    }

    @Override
    public void updatePanel() {
        // TODO Auto-generated method stub
        
    }

}
