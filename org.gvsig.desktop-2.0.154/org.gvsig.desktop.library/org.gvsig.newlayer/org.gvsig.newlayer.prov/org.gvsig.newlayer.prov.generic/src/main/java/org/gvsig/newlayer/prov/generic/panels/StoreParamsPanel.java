/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.generic.panels;

import java.awt.BorderLayout;

import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerProvider;
import org.gvsig.newlayer.prov.generic.NewLayerGenericProviderPanel;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.dynobject.DynObjectSwingManager;
import org.gvsig.tools.swing.api.dynobject.JDynObjectComponent;

public class StoreParamsPanel extends NewLayerGenericProviderPanel{

    /**
	 * 
	 */
	private static final long serialVersionUID = -3700558197138685488L;

	private static final DynObjectSwingManager  DYN_OBJECT_SWING_MANAGER = 
        ToolsSwingLocator.getDynObjectSwingManager();

	private JDynObjectComponent dynObjectComponent = null;

	private String storeName;

	public StoreParamsPanel(NewLayerProvider provider) {
		super(provider);
	}

	private void initializeComponents() {
		this.setLayout(new BorderLayout());
		this.storeName = this.getProvider().getStoreName();
		
		try {
			this.removeAll();
			dynObjectComponent = DYN_OBJECT_SWING_MANAGER
					.createJDynObjectComponent(getProvider()
							.getNewStoreParameters(), true);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.add(dynObjectComponent.asJComponent(), BorderLayout.CENTER);

	}

	@Override
	public String getTitle() {
		return Messages.getText("select_datastore_parameters");
	}

	@Override
	public boolean isValidPanel() {
		if (dynObjectComponent == null){
			return false;
		}
        dynObjectComponent.saveStatus();
		DynObject dynObject = this.dynObjectComponent.getDynObject();
		try {
			dynObject.getDynClass().validate(dynObject);
		} catch (DynObjectValidateException e) {
			return false;
		}
		return true;
	}

	@Override
	public void updatePanel() {
		if (storeName != null && storeName.equalsIgnoreCase(this.getProvider().getStoreName())){
			return;
		}
		initializeComponents();
	}

}
