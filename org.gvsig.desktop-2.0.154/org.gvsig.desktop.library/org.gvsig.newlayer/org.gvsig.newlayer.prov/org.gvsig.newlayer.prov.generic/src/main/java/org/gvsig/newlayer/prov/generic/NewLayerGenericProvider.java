/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.generic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.newlayer.NewLayerProviderPanel;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.prov.generic.panels.ExplorerParamsPanel;
import org.gvsig.newlayer.prov.generic.panels.SelectExplorerPanel;
import org.gvsig.newlayer.prov.generic.panels.SelectStoreTypePanel;
import org.gvsig.newlayer.prov.generic.panels.StoreParamsPanel;
import org.gvsig.newlayer.spi.AbstractNewLayerProvider;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * NewLayer provider which create a NewLayer.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerGenericProvider extends AbstractNewLayerProvider {

	List<NewLayerProviderPanel> panels = new ArrayList<NewLayerProviderPanel>();
	private String explorerName = null; 
	private DataServerExplorerParameters explorerParameters = null;
	private NewFeatureStoreParameters storeParameters = null;
	private EditableFeatureType featureType = null;
	
    protected NewLayerGenericProvider(NewLayerService service) {
		super(service);
		panels.add(new SelectExplorerPanel(this));
		panels.add(new ExplorerParamsPanel(this));
		panels.add(new SelectStoreTypePanel(this));
		panels.add(new StoreParamsPanel(this));
	}

	private static final Logger LOG =
        LoggerFactory.getLogger(NewLayerGenericProvider.class);

	public EditableFeatureType getFeatureType() {
//		if (this.featureType!=null){
//			if (this.storeParameters != null){
//				if (this.storeParameters.getDataStoreName().equals(this.getStoreName())){
//					return this.featureType;
//				}
//			}
//		}
//		this.featureType = this.getNewStoreParameters().getDefaultFeatureType();
//		return this.featureType;
		return this.getNewStoreParameters().getDefaultFeatureType();
	}
	
	public NewFeatureStoreParameters getNewStoreParameters() {
		if (storeParameters != null){
			if (storeParameters.getDataStoreName().equals(this.getStoreName())){
				return storeParameters;
			}
		}
		try {
			storeParameters = (NewFeatureStoreParameters) getExplorer().getAddParameters(this.getStoreName());
			return storeParameters;
		} catch (InitializeException e) {
			LOG.error("Can't initialize store parameters.");
			throw new CantInitializeStoreParametersException(e);
		} catch (ProviderNotRegisteredException e) {
			LOG.error("Can't create store parameters, provider not registered.");
			throw new CantCreateStoreParametersException(e);
		} catch (DataException e) {
			LOG.error("Can't create store parameters, provider not registered.");
			throw new CantCreateStoreParametersException(e);
		}
	}
	
	public DataStoreParameters getOpenStoreParameters() {
		DataManager dataManager = DALLocator.getDataManager();
		try {
			DataStoreParameters storeParameters = (DataStoreParameters) dataManager
					.createStoreParameters(this.getStoreName());
			return storeParameters;
		} catch (InitializeException e) {
			throw new CantInitializeStoreParametersException(e);
		} catch (ProviderNotRegisteredException e) {
			throw new CantCreateStoreParametersException(e);
		}
	}
	
	class CantCreateStoreParametersException extends BaseRuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3487961532247258393L;
		private final static String MESSAGE_FORMAT = "Can't create store parameters, provider not registered.";
		private final static String MESSAGE_KEY = "_Cant_create_store_parameters_provider_not_registered";

		public CantCreateStoreParametersException(Throwable cause) {
			super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
		}
	}
	
	class CantInitializeStoreParametersException extends BaseRuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = -5263149018209261288L;
		private final static String MESSAGE_FORMAT = "Can't initialize store parameters.";
		private final static String MESSAGE_KEY = "_Cant_initialize_store_parameters";

		public CantInitializeStoreParametersException(Throwable cause) {
			super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
		}
	}

	
	public List<NewLayerProviderPanel> getPanels() {
		return panels;
	}
	
	public List<String> getExplorerNames(){
		DataManager dataManager = DALLocator.getDataManager();
        return dataManager.getExplorerProviders();
	}

	public List<String> getStoreNames(){
		DataManager dataManager = DALLocator.getDataManager();
		List<String> writables = new ArrayList<String>();
		if (getExplorer()!=null){
			List<String> storeProviders = getExplorer().getDataStoreProviderNames();// dataManager.getStoreProviders(); //

			for (Iterator iterator = storeProviders.iterator(); iterator.hasNext();) {
				String name = (String) iterator.next();
				DataStoreProviderFactory factory = dataManager.getStoreProviderFactory(name);
				if (factory != null && factory.allowCreate()!=DataStoreProviderFactory.NO){
					writables.add(name);
				}
			}
		}
		
        return writables;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public String getExplorerName(){
		return this.explorerName;
	}
	
	public void setExplorerName(String explorerName) {
		this.explorerName = explorerName;
	}
	
	public DataServerExplorerParameters getExplorerParameters() throws InitializeException, ProviderNotRegisteredException{
		if (this.explorerParameters==null || !this.explorerParameters.getExplorerName().equalsIgnoreCase(this.getExplorerName())) {
			if (this.explorerName == null) {
				LOG.error("Can't create explorer parameters, unknow explorer name.");
				throw new RuntimeException("Can't create explorer parameters, unknow explorer name.");
			}
			DataManager manager = DALLocator.getDataManager();
			this.explorerParameters = manager.createServerExplorerParameters(this.getExplorerName()); 
			
		}
		return this.explorerParameters;
	}

	public void createExplorer() {
		DataManager manager = DALLocator.getDataManager();
		try {
			explorer = manager.openServerExplorer(getExplorerName(), getExplorerParameters());
		} catch (ValidateDataParametersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InitializeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProviderNotRegisteredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 }
