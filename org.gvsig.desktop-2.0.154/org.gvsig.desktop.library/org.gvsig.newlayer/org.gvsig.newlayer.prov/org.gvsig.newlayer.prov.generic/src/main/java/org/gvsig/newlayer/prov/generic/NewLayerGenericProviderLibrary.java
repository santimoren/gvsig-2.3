/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.generic;

import java.util.Locale;

import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerLibrary;
import org.gvsig.newlayer.NewLayerLocator;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Library to initialize and register the file NewLayer provider
 * implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerGenericProviderLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsServiceOf(NewLayerLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        if (!Messages.hasLocales()) {
            Messages.addLocale(Locale.getDefault());
        }
        Messages.addResourceFamily("org.gvsig.newlayer.prov.generic.i18n.text",
            NewLayerGenericProviderLibrary.class.getClassLoader(),
            NewLayerGenericProviderLibrary.class.getClass().getName());
    }

    protected void doPostInitialize() throws LibraryException {
        NewLayerManager manager = NewLayerLocator.getManager();
        NewLayerGenericProviderFactory factory =
            new NewLayerGenericProviderFactory();
        manager.registerProvider(factory);

        // Disable this provider by default
        manager.enableProvider(factory, false);
    }

}
