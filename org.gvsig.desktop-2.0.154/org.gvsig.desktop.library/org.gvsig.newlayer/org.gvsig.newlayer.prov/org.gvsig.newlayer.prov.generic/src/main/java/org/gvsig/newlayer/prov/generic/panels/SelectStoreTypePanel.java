/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.prov.generic.panels;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerProvider;
import org.gvsig.newlayer.prov.generic.NewLayerGenericProviderPanel;
import org.gvsig.utils.DefaultListModel;

public class SelectStoreTypePanel extends NewLayerGenericProviderPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2269115175530699282L;
	private JScrollPane scrollPane = null;
	private String currentStoreType = null;

	public SelectStoreTypePanel(NewLayerProvider provider) {
		super(provider);
		initializeComponents();
	}

	private void initializeComponents() {
		this.removeAll();
		this.setLayout(new BorderLayout());

		JList storeNames = new JList();
		ListModel model = new DefaultListModel(this.getProvider().getStoreNames());
		storeNames.setModel(model);

		storeNames.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false) {
					JList list = (JList) e.getSource();

					if (list.getSelectedIndex() > -1) {
						currentStoreType = (String) list.getSelectedValue();
					}
				}
			}
		});

		scrollPane = new JScrollPane();
		scrollPane.setViewportView(storeNames);

		this.add(scrollPane, BorderLayout.CENTER);    
	}

	@Override
	public String getTitle() {
		return Messages.getText("select_datastore");
	}

	@Override
	public boolean isValidPanel() {
		if (currentStoreType != null){
			getProvider().setStoreName(currentStoreType);
			return true;
		}
		return false;
	}
	
	public void update() {
		initializeComponents();
	}

	@Override
	public void updatePanel() {
		initializeComponents();
	}

}
