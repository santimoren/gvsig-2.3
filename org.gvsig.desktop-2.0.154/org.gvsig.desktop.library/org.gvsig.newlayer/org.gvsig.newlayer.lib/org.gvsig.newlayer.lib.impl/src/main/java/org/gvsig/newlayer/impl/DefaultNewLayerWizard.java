/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.awt.Dimension;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import jwizardcomponent.CancelAction;
import jwizardcomponent.DefaultJWizardComponents;
import jwizardcomponent.FinishAction;
import jwizardcomponent.JWizardComponents;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerException;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.NewLayerProviderPanel;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerServiceException;
import org.gvsig.newlayer.NewLayerWizard;

public class DefaultNewLayerWizard extends NewLayerWizard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5827106636136663823L;
	private NewLayerService service;
	private AddNewLayerQuestionPanel addNewLayerQuestionPanel;
	private static final Logger LOG =
        LoggerFactory.getLogger(DefaultNewLayerWizard.class);

	public DefaultNewLayerWizard(ImageIcon logo, NewLayerService service) {
		super(logo);
		setPreferredSize(new Dimension(800, 400));
		this.service = service;
		this.getWizardComponents().setFinishAction(
				new MyFinishAction(this.getWizardComponents()));
		this.getWizardComponents().setCancelAction(
				new MyCancelAction(this.getWizardComponents()));
		this.createTypeSelector();
		
		// ================ disable at the start
        this.setFinishButtonEnabled(false);
        this.setBackButtonEnabled(false);
        this.setNextButtonEnabled(false);
	}

	public NewLayerService getService() {
		return this.service;
	}

	private void createTypeSelector() {

		TypeSelectorPanel panel = new TypeSelectorPanel(this);
		this.addOptionPanel(panel);

	}

	private void createFeatureTypePanel() {
	    
		FeatureTypePanel panel = new FeatureTypePanel(service);
		this.addOptionPanel(panel);
	}

	private void createProviderPanels() {
		DefaultJWizardComponents wizardComponents = this.getWizardComponents();
		for (int i = wizardComponents.getWizardPanelList().size() - 1; i >= 1; i--) {
			wizardComponents.removeWizardPanel(i);
		}
		List<NewLayerProviderPanel> panels = service.getProvider().getPanels();
		Iterator<NewLayerProviderPanel> it = panels.iterator();
		while (it.hasNext()) {
			NewLayerProviderPanel panel = it.next();
			this.addOptionPanel(new OptionPanelWrapper(panel));
		}
		this.createFeatureTypePanel();
		this.createAddNewLayerQuestionPanel();
	}

	private AddNewLayerQuestionPanel getAddNewLayerQuestionPanel() {
		if (this.addNewLayerQuestionPanel == null) {
			this.addNewLayerQuestionPanel = new AddNewLayerQuestionPanel(this);
		}
		return this.addNewLayerQuestionPanel;
	}

	private void createAddNewLayerQuestionPanel() {
		this.addOptionPanel(getAddNewLayerQuestionPanel());
	}

	private boolean getAddLayerToView() {
		return getAddNewLayerQuestionPanel().getAddLayerToView();
	}

	@Override
    public void setType(NewLayerProviderFactory currentType) {
        this.service.setProviderFactory(currentType);
		createProviderPanels();
	}

	private class OptionPanelWrapper implements OptionPanel {

		private NewLayerProviderPanel panel;

		public OptionPanelWrapper(NewLayerProviderPanel panel) {
			this.panel = panel;
		}

		public String getPanelTitle() {
			return this.panel.getTitle();
		}

		public void nextPanel() throws NotContinueWizardException {

		    try {
    			if (!this.panel.isValidPanel()) {
    				throw new NotContinueWizardException("Error validating the form", panel, false);
    			}
		    } catch (NewLayerException e) {
                LOG.info("Error validating the form", e);
                throw new NotContinueWizardException("Error validating the form",
                    e, panel);
            }
		}

		public void lastPanel() {
		}

		public void updatePanel() {
			this.panel.updatePanel();
		}

		public JPanel getJPanel() {
			return this.panel;
		}

	}

	private class MyFinishAction extends FinishAction {

		public MyFinishAction(JWizardComponents arg0) {
			super(arg0);
		}

		public void performAction() {

			if (getService().getStoreName() == null) {
				JOptionPane
				.showMessageDialog(
						null,
						Messages.getText("new_layer_not_store_name"),
						Messages.getText("new_layer"),
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			if (getService().getNewStoreParameters() == null) {
				JOptionPane
				.showMessageDialog(
						null,
						Messages.getText("new_layer_parameters_missing"),
						Messages.getText("new_layer"),
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			getService().addLayerToView(getAddLayerToView());
			try {
				getService().createLayer();
			} catch (NewLayerServiceException e) {
				LOG.info("Cannot create the new layer", e);
				JOptionPane
				.showMessageDialog(
						null,
						Messages.getText("cant_create_new_layer") + "\n" + e.getMessageStack(),
						Messages.getText("new_layer"),
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			try {
				getService().loadLayer();
			} catch (NewLayerServiceException e) {
				JOptionPane
				.showMessageDialog(
						null,
						"The new layer has been succesfully created but can't load the new layer.\n Try load it yourshelf, please.",
						"Can't create a new layer",
						JOptionPane.WARNING_MESSAGE);
			}
			setVisible(false);
			return;
		}

	}

	private class MyCancelAction extends CancelAction {

		public MyCancelAction(DefaultJWizardComponents wizardComponents) {
			super(wizardComponents);
		}

		public void performAction() {
			setVisible(false);
		}

	}
}
