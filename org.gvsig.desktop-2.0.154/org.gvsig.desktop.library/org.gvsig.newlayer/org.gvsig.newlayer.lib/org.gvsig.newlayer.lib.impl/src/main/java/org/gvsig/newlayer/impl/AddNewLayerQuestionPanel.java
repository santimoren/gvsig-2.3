/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.awt.BorderLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerWizard;

public class AddNewLayerQuestionPanel extends JPanel implements OptionPanel{
	
    private static final long serialVersionUID = -5251910576017225307L;
    private NewLayerWizard wizard;
	private JCheckBox questionCheckBox = null;
	
	public AddNewLayerQuestionPanel(NewLayerWizard wizard){
		this.wizard = wizard;
		initializeComponents();
	}

	public String getPanelTitle() {
		return Messages.getText("_Adding_layer");
	}

	public void nextPanel() throws NotContinueWizardException {
		getService().addLayerToView(getAddLayerToView());
	}

	public void lastPanel() {
		// do nothing
		
	}

	public void updatePanel() {
		initializeComponents();
	}

	public JPanel getJPanel() {
		return this;
	}

    private void initializeComponents() {
        this.setLayout(new BorderLayout());
        this.add(getQuestionCheckBox(), BorderLayout.NORTH);        
    }
    
    private JCheckBox getQuestionCheckBox(){
    	if (this.questionCheckBox == null){
    		this.questionCheckBox = new JCheckBox(
    		    Messages.getText("_Add_layer_to_view"), true);
    	}
    	return this.questionCheckBox;
    }

	private NewLayerService getService() {
		return this.wizard.getService();
	} 
	
	public boolean getAddLayerToView(){
		return getQuestionCheckBox().isSelected();
	}
}
