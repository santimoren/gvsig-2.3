/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerWizard;
import org.gvsig.newlayer.impl.preferences.DefaultNewLayerPreferencesComponent;
import org.gvsig.newlayer.preferences.NewLayerPreferencesComponent;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.ServiceException;

/**
 * Default {@link NewLayerManager} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultNewLayerManager implements NewLayerManager {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultNewLayerManager.class);

    final static private String EP_NEWLAYER_NAME = "NewLayer.manager.providers";
    final static private String EP_NEWLAYER_DESCRIPTION = "NewLayer providers";

    private Map<NewLayerProviderFactory, Boolean> providerStatus;

    /**
     * Empty constructor.
     */
    public DefaultNewLayerManager() {
        providerStatus = new HashMap<NewLayerProviderFactory, Boolean>();
    }

    public NewLayerService createNewLayerService(MapContext mapContext) {
        return new DefaultNewLayerService(this, mapContext);
    }

    public void registerProvider(NewLayerProviderFactory factory) {
        ExtensionPoint ep = getNewLayerProvidersExtensionPoint();
        ep.append(factory.getName(), factory.getDescription(), factory);
    }

    public NewLayerWizard createNewLayerWizard(NewLayerService service) {
        
        URL iconURL =
            getClass().getClassLoader().getResource(
                "org/gvsig/newlayer/lib/impl/images/panel/view-new-layer-illustration.png");
        ImageIcon logo = null;
        if (iconURL != null) {
            logo = new ImageIcon(iconURL);
        }

        return new DefaultNewLayerWizard(logo, service);
    }

    public NewLayerProviderFactory getNewLayerProviderFactory(
        String providerName) throws ServiceException {
        ExtensionPoint ep = getNewLayerProvidersExtensionPoint();
        try {
            return (NewLayerProviderFactory) ep.create(providerName);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void enableProvider(NewLayerProviderFactory factory, Boolean value) {
        providerStatus.put(factory, Boolean.valueOf(value));
    }

    public NewLayerPreferencesComponent createNewLayerProvidersPreferences() {
        return new DefaultNewLayerPreferencesComponent();
    }

    public List<NewLayerProviderFactory> getProviders() {
        ExtensionPoint ep = getNewLayerProvidersExtensionPoint();
        List<NewLayerProviderFactory> providers =
            new ArrayList<NewLayerProviderFactory>();
        @SuppressWarnings("unchecked")
        Iterator<Extension> it = ep.iterator();

        while (it.hasNext()) {
            Extension extension = (Extension) it.next();
            NewLayerProviderFactory factory;
            try {
                factory = (NewLayerProviderFactory) extension.create();
            } catch (InstantiationException e) {
                LOG.warn(
                    "Can't create NewLayerProviderFactory "
                        + extension.getName(), e);
                continue;
            } catch (IllegalAccessException e) {
                LOG.warn(
                    "Can't create NewLayerProviderFactory "
                        + extension.getName(), e);
                continue;
            }
            providers.add(factory);
        }

        return providers;
    }

    private ExtensionPoint getNewLayerProvidersExtensionPoint() {
        ExtensionPointManager epmgr = ToolsLocator.getExtensionPointManager();
        return
            epmgr.add(EP_NEWLAYER_NAME, EP_NEWLAYER_DESCRIPTION);
    }

    public List<NewLayerProviderFactory> getProviders(STORETYPE filter) {
        // TODO use filters
        // if (filter == STORETYPE.TABULAR){
        // if (!factory.isSpatial()){
        // providers.add(factory);
        // }
        // } else {
        // }
        return getProviders();
    }

    public boolean isProviderEnabled(NewLayerProviderFactory factory) {
        Boolean status = providerStatus.get(factory);
        return status == null ? true : status.booleanValue();
    }

}
