/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.tools.dataTypes.DataType;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class FeatureTypePanel extends JPanel implements OptionPanel {
    private static final int COLUMN_FIELD = 0;
    private static final int COLUMN_TYPE = 1;
    private static final int COLUMN_LENGTH = 2;
    private static final int COLUMN_GEOMETRYTYPE = 3;
    private static final int COLUMN_GEOMETRYSUBTYPE = 4;
    private static final int COLUMN_CRS = 5;
    private static final int COLUMN_PRIMARYKEY = 6;
    private static final int COLUMN_MANDATORY = 7;
 
	private static final long serialVersionUID = 263541873998597624L;
	
    private FeatureType defaultFeatType = null;
    
    private NewLayerService newLayer_Service = null;
    private int inDefaultFeatTypeFieldCount = 0;
	private String inStoreName = null;
	private IProjection inProjection = null;
	private FeatureStoreProviderFactory spFactory = null;
	private boolean defaultHasGeometry = false;
	
	private static final Logger logger = LoggerFactory
			.getLogger(FeatureTypePanel.class);

	private JLabel jLabel = null;
	private JScrollPane jScrollPane = null;
	protected JTable jTable = null;
	private JPanel jPanelEast = null;
	private JButton addFieldButton = null;
	private JButton deleteFieldButton = null;
	protected int MAX_FIELD_LENGTH = 254;
	
	// we dont need this list
	// we'll clear and rebuild featureType each time
	// the user clicks next
	// private List removedFields;

	// private DataServerExplorer explorer = null;

	public FeatureTypePanel(NewLayerService new_layer_service) {
	    
	    /*
	    service.getFeatureType(),
        service.getStoreName(),
        service.getMapContext().getProjection();
        */
	    newLayer_Service = new_layer_service;
	}
	
	private boolean fieldsInited = false;
	
	private void initFields() {
	    
	    if (newLayer_Service == null) {
	        return;
	    }
	    
	    fieldsInited = true;
	    
        inStoreName = newLayer_Service.getStoreName();
        inProjection = newLayer_Service.getMapContext().getProjection();
        
        DataStoreProviderFactory dspf =
                DALLocator.getDataManager().getStoreProviderFactory(inStoreName);
        spFactory = (FeatureStoreProviderFactory) dspf;
        defaultFeatType = spFactory.createDefaultFeatureType();
        if (defaultFeatType != null) {
            inDefaultFeatTypeFieldCount =
                defaultFeatType.getAttributeDescriptors().length; 
            int def_geom_fld_index = defaultFeatType.getDefaultGeometryAttributeIndex(); 
            if (def_geom_fld_index >= 0 &&
                def_geom_fld_index < inDefaultFeatTypeFieldCount &&
                defaultFeatType.
                getAttributeDescriptor(def_geom_fld_index).
                getDataType().getType() == DataTypes.GEOMETRY) {
                
                defaultHasGeometry = true;
            }
        }
	}

	private void initializeComponents() {
		jLabel = new JLabel();
		jLabel.setText(Messages.getText("define_fields"));
		this.setLayout(new BorderLayout(5, 5));
		this.setSize(new java.awt.Dimension(499, 232));
		this.add(jLabel, java.awt.BorderLayout.NORTH);
		this.add(getJScrollPane(), java.awt.BorderLayout.CENTER);
		this.add(getJPanelSouth(), java.awt.BorderLayout.SOUTH);

	}

	/**
	 * This method initializes jScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTable());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTable getJTable() {
		if (jTable == null) {
			jTable = new JTable(){
			    public boolean isCellEditable(int row,int column){
			        
			        
			        if (row < inDefaultFeatTypeFieldCount) {
			            // default fields
			            Object o = getValueAt(row,COLUMN_TYPE);  
			            if (o instanceof ComboItemDataType) {
			                
			                ComboItemDataType ot = (ComboItemDataType) o;
			                return (ot.dataType.getType() == DataTypes.GEOMETRY)
			                    &&
			                    (column == COLUMN_GEOMETRYTYPE ||
		                        column == COLUMN_GEOMETRYSUBTYPE);
			                
			            } else {
			                return false;
			            }
			            
			        } else {
			            // fields added by user
			            if (column == COLUMN_PRIMARYKEY) {
			                return spFactory.allowsPrimaryKeyAttributes();
			            }

			            if (column == COLUMN_MANDATORY) {
			                return spFactory.allowsMandatoryAttributes();
			            }
			            
			            if (column == COLUMN_LENGTH) {
			                
			                Object o = getValueAt(row,COLUMN_TYPE);  
	                        if (o instanceof ComboItemDataType) {
	                            
	                            ComboItemDataType ot = (ComboItemDataType) o;
	                            return dataTypeNeedsSize(ot.dataType.getType());
	                            
	                        } else {
	                            return false;
	                        }
			                
			            }

			            if (defaultHasGeometry &&
			                (column == COLUMN_GEOMETRYTYPE
                            || column == COLUMN_GEOMETRYSUBTYPE
                            || column == COLUMN_CRS)) {
			                // geom-related columns not editable
			                return false;
			            } else {
	                        return true;
			            }
			        }

			        /*
			        if ((column == COLUMN_GEOMETRYTYPE) ||
			            (column == COLUMN_GEOMETRYSUBTYPE) ||
			            (column == COLUMN_CRS)) {
			            DataType dataType = 
			                ((ComboItemDataType) getValueAt(row, COLUMN_TYPE)).dataType;			           
			            return dataType.getType() == DataTypes.GEOMETRY;
			        }
			        */
			    }  
			};

			DefaultTableModel tm = (DefaultTableModel) jTable.getModel();
			
			tm.addColumn(Messages.getText("_Name"));
			tm.addColumn(Messages.getText("type"));
			tm.addColumn(Messages.getText("length"));
			tm.addColumn(Messages.getText("_Geometry_type"));
			tm.addColumn(Messages.getText("_Dimensions"));
			tm.addColumn(Messages.getText("CRS"));
			tm.addColumn("PK");
			tm.addColumn(Messages.getText("_Mandatory_(short_text)"));

			// Ask to be notified of selection changes.
			ListSelectionModel rowSM = jTable.getSelectionModel();
			rowSM.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					// Ignore extra messages.
					if (e.getValueIsAdjusting())
						return;

					ListSelectionModel lsm = (ListSelectionModel) e.getSource();
					if (lsm.isSelectionEmpty()) {
						// no rows are selected
						deleteFieldButton.setEnabled(false);
					} else {
					    int min_sele = lsm.getMinSelectionIndex(); 
	                    if (min_sele > -1 &&
	                        min_sele < inDefaultFeatTypeFieldCount) {
	                        deleteFieldButton.setEnabled(false);
	                    } else {
	                        deleteFieldButton.setEnabled(allowEditableFeatureType());
	                    }
					}		
					
				}
			});
			jTable.getColumnModel().getColumn(0).setPreferredWidth(100);
			// PK column
            jTable.getColumnModel().getColumn(6).setPreferredWidth(30);
            // Oblig column
            jTable.getColumnModel().getColumn(7).setPreferredWidth(50);

			jTable.getModel().addTableModelListener(new TableModelListener() {                
                public void tableChanged(TableModelEvent e) {
                    if (e.getColumn() == COLUMN_TYPE){
                        jTable.repaint();
                    }                    
                }
            });
			

			if (!fieldsInited) {
			    initFields();
			}

			// Rellenado de la tabla
			if (defaultFeatType != null) {

				Iterator it = defaultFeatType.iterator();
				boolean some_error = false;
				while (it.hasNext()) {
					FeatureAttributeDescriptor descriptor = (FeatureAttributeDescriptor) it
							.next();

                    if (!addRowToFieldTable(
                        jTable,
                        null,
                        inProjection,
                        descriptor)) {
                        
                        some_error = true;
                    }
                    /*
					tm.setValueAt(descriptor.getName(), row, COLUMN_FIELD);
					tm.setValueAt(descriptor.getType(), row, COLUMN_TYPE);
					tm.setValueAt(descriptor.getSize(), row, COLUMN_LENGTH);
					tm.setValueAt(descriptor.getGeometryType(), row, COLUMN_GEOMETRYTYPE);
					tm.setValueAt(descriptor.getGeometrySubType(), row, COLUMN_GEOMETRYSUBTYPE);
					tm.setValueAt(descriptor.getSRS(), row, COLUMN_CRS);
					tm.setValueAt(descriptor.isPrimaryKey(), row, COLUMN_PRIMARYKEY);
					tm.setValueAt(descriptor.isMandatory(), row, COLUMN_MANDATORY);
					*/
				}
				
				if (some_error) {
                    String tit = Messages.getText("_Default_fields");
                    String msg = Messages.getText("_Unable_to_add_fields");
                    JOptionPane.showMessageDialog(
                        jTable, msg, tit,
                        JOptionPane.ERROR_MESSAGE);
				}
			}

		}
		jTable.setEnabled(allowEditableFeatureType());
		return jTable;
	}

	/**
	 * This method initializes jPanelWest
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanelSouth() {
		if (jPanelEast == null) {
			jPanelEast = new JPanel();
			jPanelEast.setLayout(new BorderLayout());
			
			JPanel south_east = new JPanel(new GridBagLayout());
			// south_east.setPreferredSize(new java.awt.Dimension(170, 50));
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(8,10,8,0);  //top padding
			c.gridx = 0;       
			c.gridwidth = 1;   
			c.gridy = 0;       
			south_east.add(getJButtonAddField(), c);
            c.gridx = 1;       
			south_east.add(getJButtonDeleteField(), c);
			
			jPanelEast.add(south_east, BorderLayout.EAST);
		}
		return jPanelEast;
	}

	/**
	 * This method initializes jButtonAddField
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonAddField() {
		if (addFieldButton == null) {
		    addFieldButton = new JButton();
		    addFieldButton.setText(Messages.getText("add_field"));
		    addFieldButton.setLocation(new java.awt.Point(7, 5));
		    addFieldButton.setSize(new java.awt.Dimension(180, 23));
		    addFieldButton.setPreferredSize(new java.awt.Dimension(180, 23));
		    addFieldButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							DefaultTableModel tm = (DefaultTableModel) jTable
									.getModel();

							// Figure out a suitable field name
							ArrayList fieldNames = new ArrayList();
							for (int i = 0; i < jTable.getRowCount(); i++) {
								fieldNames.add(tm.getValueAt(i, COLUMN_FIELD));
							}
							String[] currentFieldNames = (String[]) fieldNames
									.toArray(new String[0]);
							String newField = Messages.getText("field")
									.replaceAll(" +", "_");
							int index = 0;
							for (int i = 0; i < currentFieldNames.length; i++) {
								if (currentFieldNames[i].startsWith(newField)) {
									try {
										index = Integer
												.parseInt(currentFieldNames[i]
														.replaceAll(newField,
																""));
									} catch (Exception ex) { /* we don't care */
									}
								}
							}
							String newFieldName = newField + (++index);

							// Esto lo a�ado aqu� porque si no tiene registros,
							// no hace caso. (Por eso no
							// lo pongo en getJTable()
							if (!addRowToFieldTable(
							    jTable,
							    newFieldName,
							    inProjection,
							    null)) {
							    
	                            String tit = Messages.getText("_Default_fields");
	                            String msg = Messages.getText("_Unable_to_add_fields");
	                            JOptionPane.showMessageDialog(
	                                jTable, msg, tit,
	                                JOptionPane.ERROR_MESSAGE);
							}
						}
					});

		}
		addFieldButton.setEnabled(allowEditableFeatureType());
		return addFieldButton;
	}
	
	private boolean allowEditableFeatureType(){
	    
	    if (!fieldsInited) {
	        initFields();
	    }

		DataManager dataManager = DALLocator.getDataManager();
		FeatureStoreProviderFactory factory =
		    (FeatureStoreProviderFactory) dataManager.
		    getStoreProviderFactory(inStoreName);
		if (factory.allowEditableFeatureType()==FeatureStoreProviderFactory.NO) {
			return false;
		}
		return true;
	}

	private class ComboItemDataType {
		private DataType dataType;

		public ComboItemDataType(DataType dataType) {
			this.dataType = dataType;
		}

		@Override
		public String toString() {
			return this.dataType.getName();
		}
	}

	private class ComboModelDataType implements ComboBoxModel {

		private List<ComboItemDataType> dataTypes;
		private ComboItemDataType selected;

		public ComboModelDataType(List<DataType> dataTypes, int sel_type) {
		    
			this.dataTypes = new ArrayList<ComboItemDataType>();
			ComboItemDataType item = null;
			for (Iterator iterator = dataTypes.iterator(); iterator.hasNext();) {
				DataType dataType = (DataType) iterator.next();
				item = new ComboItemDataType(dataType);
				this.dataTypes.add(item);
				if (sel_type == dataType.getType()) {
				    this.setSelectedItem(item);
				}
			}
		}

		public void addListDataListener(ListDataListener arg0) {
			// DO NOTHING
		}

		public Object getElementAt(int arg0) {
			return this.dataTypes.get(arg0);
		}

		public int getSize() {
			return this.dataTypes.size();
		}

		public void removeListDataListener(ListDataListener arg0) {
			// DO NOTHING
		}

		public Object getSelectedItem() {
			return this.selected;
		}

		public void setSelectedItem(Object anItem) {
			this.selected = (ComboItemDataType) anItem;
		}

	}

	class CrsRenderer extends JButton implements TableCellRenderer {
		private static final long serialVersionUID = -573793294070515376L;
		private JLabel label = null;
		
		public CrsRenderer() {
			setOpaque(true);
			label = new JLabel(Messages.getText("not_applicable"));
			this.setMargin(new Insets(2, 0, 2, 0));
			this.setFont(this.getFont().deriveFont(9f));
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			
		    ComboItemDataType item =
		        (ComboItemDataType) table.getValueAt(row, COLUMN_TYPE);
		    
		    if (item == null) {
		        // happens sometimes. perhaps an early rendering?
		        return new JLabel();
		    }
		    
		    DataType dataType = item.dataType;                       
                        
            if (dataType.getType() != DataTypes.GEOMETRY) {                
                return label;
            }
		    
		    if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(UIManager.getColor("Button.background"));
			}
			setText((value == null) ? "" : ((IProjection) value).getFullCode());
			return this;
		}
	}	
	   
    private class GeometryComboCellRenderer extends JLabel implements TableCellRenderer{

        public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus, int row,
            int column) {                     

            ComboItemDataType item = (ComboItemDataType) table.getValueAt(row, COLUMN_TYPE);
            DataType dataType = item.dataType;                       
                        
            if (dataType.getType() == DataTypes.GEOMETRY) {
                ComboItemGeomType geomType = (ComboItemGeomType)value;
                if (geomType != null) {            
                    this.setText(geomType.toString());
                }
            } else {
               this.setText(Messages.getText("not_applicable"));
            }
            
            return this;
        }
        
    }

	class ButtonEditor extends DefaultCellEditor {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2820892672026188802L;

		protected JButton button;

		private String label;

		private boolean isPushed;

		private Object value;

		public ButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.setOpaque(true);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			label = (value == null) ? "" : ((IProjection) value).getFullCode();// .toString();
			button.setText(label);
			isPushed = true;
			this.value = value;
			return button;
		}

		public Object getCellEditorValue() {
			IProjection curProj = (IProjection) this.value;
			if (isPushed) {
				ISelectCrsPanel csSelect = CRSSelectPanel.getUIFactory()
						.getSelectCrsPanel(curProj, true);
				WindowManager wm = ToolsSwingLocator.getWindowManager();
				wm.showWindow((JPanel) csSelect, "Projection",
						WindowManager.MODE.DIALOG);
				curProj = csSelect.getProjection();
			}
			isPushed = false;
			return curProj;
		}

		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
	}

	private class ComboItemGeomType {
		private int type;
		private String name;

		public ComboItemGeomType(String name, int type) {
			this.type = type;
			this.name = name;
		}

		@Override
		public String toString() {
			return this.name;
		}
		
		public int getGeomType() {
		    return type;
		}
	}

	private class ComboModelGeomType implements ComboBoxModel {

		private List<ComboItemGeomType> geomTypes;
		private ComboItemGeomType selected;

		public ComboModelGeomType(
		    List<ComboItemGeomType> geomTypes, int sel_geom_type) {
		    
			this.geomTypes = geomTypes;
			int len = geomTypes.size();
			ComboItemGeomType item = null;
			for (int i=0; i<len; i++) {
			    item = geomTypes.get(i);
			    if (item.getGeomType() == sel_geom_type) {
			        setSelectedItem(item);
			    }
			}
		}

		public void addListDataListener(ListDataListener arg0) {
			// DO NOTHING
		}

		public Object getElementAt(int arg0) {
			return this.geomTypes.get(arg0);
		}

		public int getSize() {
			return this.geomTypes.size();
		}

		public void removeListDataListener(ListDataListener arg0) {
			// DO NOTHING
		}

		public Object getSelectedItem() {
			return this.selected;
		}

		public void setSelectedItem(Object anItem) {
			this.selected = (ComboItemGeomType) anItem;
		}

	}

	private List<ComboItemGeomType> getGeomTypes() {
		List<ComboItemGeomType> geomTypes = new ArrayList<ComboItemGeomType>();
		geomTypes
				.add(new ComboItemGeomType("GEOMETRY", Geometry.TYPES.GEOMETRY));
		geomTypes.add(new ComboItemGeomType("POINT", Geometry.TYPES.POINT));
		geomTypes.add(new ComboItemGeomType("CURVE", Geometry.TYPES.CURVE));
		geomTypes.add(new ComboItemGeomType("SURFACE", Geometry.TYPES.SURFACE));
		geomTypes.add(new ComboItemGeomType("SOLID", Geometry.TYPES.SOLID));
		geomTypes.add(new ComboItemGeomType("AGGREGATE",
				Geometry.TYPES.AGGREGATE));
		geomTypes.add(new ComboItemGeomType("MULTIPOINT",
				Geometry.TYPES.MULTIPOINT));
		geomTypes.add(new ComboItemGeomType("MULTICURVE",
				Geometry.TYPES.MULTICURVE));
		geomTypes.add(new ComboItemGeomType("MULTISURFACE",
				Geometry.TYPES.MULTISURFACE));
		geomTypes.add(new ComboItemGeomType("MULTISOLID",
				Geometry.TYPES.MULTISOLID));

		return geomTypes;
	}

	private List<ComboItemGeomType> getGeomSubtypes() {
		List<ComboItemGeomType> geomSubtypes = new ArrayList<ComboItemGeomType>();
		geomSubtypes.add(new ComboItemGeomType("GEOM2D",
				Geometry.SUBTYPES.GEOM2D));
		geomSubtypes.add(new ComboItemGeomType("GEOM2DM",
				Geometry.SUBTYPES.GEOM2DM));
		geomSubtypes.add(new ComboItemGeomType("GEOM3D",
				Geometry.SUBTYPES.GEOM3D));
		geomSubtypes.add(new ComboItemGeomType("GEOM3DM",
				Geometry.SUBTYPES.GEOM3DM));
		geomSubtypes.add(new ComboItemGeomType("UNKNOWN",
				Geometry.SUBTYPES.UNKNOWN));
		return geomSubtypes;
	}

	class CheckBoxRenderer extends JCheckBox implements TableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5093448776058027505L;

		public CheckBoxRenderer() {
			setHorizontalAlignment(JLabel.CENTER);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}
			setSelected((value != null && ((Boolean) value).booleanValue()));
			return this;
		}
	}

	/**
	 * This method initializes jButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonDeleteField() {
		if (deleteFieldButton == null) {
		    deleteFieldButton = new JButton();
		    deleteFieldButton.setText(Messages.getText("delete_field"));
		    deleteFieldButton.setLocation(new java.awt.Point(7, 33));
		    deleteFieldButton.setSize(new java.awt.Dimension(180, 23));
		    deleteFieldButton.setPreferredSize(new java.awt.Dimension(180, 23));
		    deleteFieldButton.setEnabled(false);
		    deleteFieldButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							int[] selecteds = jTable.getSelectedRows();
							DefaultTableModel tm = (DefaultTableModel) jTable
									.getModel();

							for (int i = selecteds.length - 1; i >= 0; i--) {
							    /*
							     * 
								removedFields.add((String) tm.getValueAt(i, 0));
                                 */
								tm.removeRow(selecteds[i]);
							}
						}
					});
		}
		deleteFieldButton.setEnabled(false);
		return deleteFieldButton;
	}

	public String getPanelTitle() {
		return Messages.getText("_Field_definitions");
	}

	public void nextPanel() throws NotContinueWizardException {
		if (allowEditableFeatureType()){
			fillFeatureType();
		}
	}

	private void fillFeatureType() throws NotContinueWizardException {
	    
	    if (!fieldsInited) {
	        initFields();
	    }

		/*
		 * Coge los datos del formulario y los guarda en
		 * this.service.getFeatureType()
		 */

	    EditableFeatureType edft = newLayer_Service.getFeatureType();

		// Clean and rebuild featureType everytime we click next
		removeAllFields(edft);
		edft.setDefaultGeometryAttributeName(null);

		GeometryManager gm = GeometryLocator.getGeometryManager();
		DefaultTableModel tm = (DefaultTableModel) jTable.getModel();
		
		for (int i = 0; i < tm.getRowCount(); i++) {
			DataType dataType = ((ComboItemDataType) tm.getValueAt(i, COLUMN_TYPE)).dataType;

			int fieldLength = 20;
			if (dataTypeNeedsSize(dataType.getType())) {
	            fieldLength = 
	                Integer.parseInt((String) tm.getValueAt(i, COLUMN_LENGTH));
			}

			EditableFeatureAttributeDescriptor efad1 =
			    edft.add(
					(String) tm.getValueAt(i, COLUMN_FIELD), dataType.getType(),
					fieldLength);

			if (dataType.getType() == DataTypes.DOUBLE ||
			    dataType.getType() == DataTypes.FLOAT) {
			    // TODO Improve this?
			    // Half of the size dedicated
			    // to precision (at least 1) 
	            efad1.setPrecision(Math.max(1, fieldLength/2));
			}

			if (dataType.getType() == DataTypes.GEOMETRY) {
			    
                int g_type = ((ComboItemGeomType) tm.getValueAt(i, COLUMN_GEOMETRYTYPE)).type;
                int g_subtype = ((ComboItemGeomType) tm.getValueAt(i, COLUMN_GEOMETRYSUBTYPE)).type;
			    
			    GeometryType gty = null;
			    
			    try {
                    gty = gm.getGeometryType(g_type, g_subtype);
                    efad1.setGeometryType(gty);
                    // efad1.setGeometryType(g_type);
			    } catch (Exception ex) {
                    String _msg = Messages.getText("_Invalid_geometry_type")
                        + " (" + g_type + ", " + g_subtype + ")";
			        throw new NotContinueWizardException(_msg, ex, this);
			    }
				
				efad1.setSRS((IProjection) tm.getValueAt(i, COLUMN_CRS));
				
				//If there is not a previous default geometry field, add it
				// this will set the first geometry field
				// as the default geometry field
				if (edft.getDefaultGeometryAttribute() == null){
				    edft.setDefaultGeometryAttributeName(efad1.getName());
				}	
			}
			
			// pk and mandatory
            efad1.setIsPrimaryKey(((Boolean) tm.getValueAt(i, COLUMN_PRIMARYKEY))
                .booleanValue());
            efad1.setAllowNull(!((Boolean) tm.getValueAt(i, COLUMN_MANDATORY))
                .booleanValue());

		}
	}

	/**
     * @param featureType
     */
    private void removeAllFields(EditableFeatureType ft) {
        
        FeatureAttributeDescriptor[] atts =  ft.getAttributeDescriptors();
        int len = atts.length;
        FeatureAttributeDescriptor item = null;
        for (int i=0; i<len; i++) {
            item = atts[i];
            ft.remove(item.getName());
        }
    }

    public void lastPanel() {
		// TODO Auto-generated method stub
		logger.info("FeatureTypePanel: lastPanel");
	}

	public void updatePanel() {
		initializeComponents();

	}

	public JPanel getJPanel() {
		return this;
	}
	
	/**
	 * If fad is null, it's an addition after user has clicked on 'Add field'.
	 * If fad is not null, it's the description of a default field and the
	 * field name will also be taken from there.
	 * 
	 * @param the_table
	 * @param new_fld_name
	 * @param the_projection
	 * @param fad
	 * 
	 * @return true if field was actually added, false otherwise
	 */
    private boolean addRowToFieldTable(
        JTable the_table,
        String new_fld_name,
        IProjection the_projection,
        FeatureAttributeDescriptor fad) {
        
        if (fad == null && new_fld_name == null) {
            logger.info("Error: unable to add field to table (no description and no name)");
            return false;
        }
        
        DefaultTableModel the_tm = (DefaultTableModel) the_table.getModel();
        Object[] newRow = new Object[the_tm.getColumnCount()];
        
        /*
        tm.setValueAt(descriptor.getName(), row, COLUMN_FIELD);
        tm.setValueAt(descriptor.getType(), row, COLUMN_TYPE);
        tm.setValueAt(descriptor.getSize(), row, COLUMN_LENGTH);
        tm.setValueAt(descriptor.getGeometryType(), row, COLUMN_GEOMETRYTYPE);
        tm.setValueAt(descriptor.getGeometrySubType(), row, COLUMN_GEOMETRYSUBTYPE);
        tm.setValueAt(descriptor.getSRS(), row, COLUMN_CRS);
        tm.setValueAt(descriptor.isPrimaryKey(), row, COLUMN_PRIMARYKEY);
        tm.setValueAt(descriptor.isMandatory(), row, COLUMN_MANDATORY);
        */
        
        // =============================== name
        if (fad == null) {
            newRow[COLUMN_FIELD] = new_fld_name;
        } else {
            newRow[COLUMN_FIELD] = fad.getName();
        }
        // ===================================== field type
        TableColumn typeColumn = the_table.getColumnModel().getColumn(COLUMN_TYPE);
        List supp_types = spFactory.getSupportedDataTypes();
        
        DataManager daman = DALLocator.getDataManager();
        List all_types = daman.getDataTypes();
        
        
        ComboModelDataType model = null;
        if (fad == null) {

            // this is a user defined column, remove geometry if in default feattype 
            if (defaultHasGeometry) {
                supp_types = removeGeometryType(supp_types);
                all_types = removeGeometryType(all_types);
            }
            
            if (supp_types != null && supp_types.size() > 0) {
                model = new ComboModelDataType(
                    supp_types,
                    ((DataType) supp_types.get(0)).getType());
            } else {
                model = new ComboModelDataType(
                    all_types,
                    ((DataType) all_types.get(0)).getType());
            }
        } else {
            model = new ComboModelDataType(supp_types, fad.getType());
        }
        
        JComboBox comboBox = new JComboBox(model);
        typeColumn.setCellEditor(new DefaultCellEditor(comboBox));
        newRow[COLUMN_TYPE] = model.getSelectedItem();
        
        // ============================= width
        TableColumn widthColumn =
            the_table.getColumnModel().getColumn(COLUMN_LENGTH);
        if (fad == null) {
            newRow[COLUMN_LENGTH] = "20";
        } else {
            newRow[COLUMN_LENGTH] = "" + fad.getSize();
        }
        
        SimpleTextCellRenderer stcr = new SimpleTextCellRenderer();
        widthColumn.setCellEditor(new DefaultCellEditor(new JTextField()));
        widthColumn.setCellRenderer(stcr);
        
        
        // ============================= geom type
        TableColumn geomTypeColumn =
            the_table.getColumnModel().getColumn(COLUMN_GEOMETRYTYPE);
        ComboModelGeomType geomTypeModel = null;
        
        List<ComboItemGeomType> supptd =
            leaveSupportedGeomTypes(getGeomTypes(),
                spFactory.getSupportedGeometryTypesSubtypes(), 0);
        
        if (fad == null) {
            geomTypeModel = new ComboModelGeomType(
                supptd, supptd.size() > 0 ? supptd.get(0).getGeomType() : -1);
        } else {
            geomTypeModel = new ComboModelGeomType(
                supptd, fad.getGeomType().getType());
        }
        
        JComboBox geomTypeComboBox = new JComboBox(geomTypeModel);
        geomTypeColumn.setCellEditor(new DefaultCellEditor(geomTypeComboBox));
        
        GeometryComboCellRenderer gccr = new GeometryComboCellRenderer();
        geomTypeColumn.setCellRenderer(gccr);
        newRow[COLUMN_GEOMETRYTYPE] = geomTypeModel.getSelectedItem();
        
        // ============================= geom sub type

        TableColumn geomSubTypeColumn =
            the_table.getColumnModel().getColumn(COLUMN_GEOMETRYSUBTYPE);
        ComboModelGeomType geomSubTypeModel = null;

        supptd =
            leaveSupportedGeomTypes(getGeomSubtypes(),
                spFactory.getSupportedGeometryTypesSubtypes(), 1);
        
        if (fad == null) {
            geomSubTypeModel = new ComboModelGeomType(
                supptd, supptd.size() > 0 ? supptd.get(0).getGeomType() : -1);
        } else {
            geomSubTypeModel = new ComboModelGeomType(
                supptd, fad.getGeomType().getSubType());
        }
        
        JComboBox geomSubTypeComboBox = new JComboBox(geomSubTypeModel);
        geomSubTypeColumn.setCellEditor(new DefaultCellEditor(
            geomSubTypeComboBox));
        geomSubTypeColumn.setCellRenderer(new GeometryComboCellRenderer());
        newRow[COLUMN_GEOMETRYSUBTYPE] = geomSubTypeModel.getSelectedItem();

        // ============================= CRS

        TableColumn crsColumn = the_table.getColumnModel().getColumn(COLUMN_CRS);
        crsColumn.setCellEditor(new ButtonEditor(new JCheckBox()));
        CrsRenderer crsButtonRenderer = new CrsRenderer();
        crsColumn.setCellRenderer(crsButtonRenderer);
        newRow[COLUMN_CRS] = the_projection;

        // ============================= PK

        TableColumn primaryKeyColumn =
            the_table.getColumnModel().getColumn(COLUMN_PRIMARYKEY);
        JCheckBox primaryKeyCheckBox = new JCheckBox();
        
        if (fad == null) {
            primaryKeyCheckBox.setSelected(false);
        } else {
            primaryKeyCheckBox.setSelected(fad.isPrimaryKey());
        }

        primaryKeyColumn.setCellEditor(
            new DefaultCellEditor(primaryKeyCheckBox));
        CheckBoxRenderer primaryKeyCheckBoxRenderer = new CheckBoxRenderer();
        primaryKeyColumn.setCellRenderer(primaryKeyCheckBoxRenderer);
        newRow[COLUMN_PRIMARYKEY] = primaryKeyCheckBox.isSelected();
            
        // ================================== mandatory
        
        TableColumn mandatoryColumn =
            the_table.getColumnModel().getColumn(COLUMN_MANDATORY);
        JCheckBox mandatoryCheckBox = new JCheckBox();
        
        if (fad == null) {
            mandatoryCheckBox.setSelected(false);
        } else {
            mandatoryCheckBox.setSelected(fad.isMandatory());
        }
        
        mandatoryColumn.setCellEditor(new DefaultCellEditor(mandatoryCheckBox));
        CheckBoxRenderer mandatoryCheckBoxRenderer = new CheckBoxRenderer();
        mandatoryColumn.setCellRenderer(mandatoryCheckBoxRenderer);
        newRow[COLUMN_MANDATORY] = mandatoryCheckBox.isSelected();

        // ==================================
        
        // Add a new row
        the_tm.addRow(newRow);
        return true;
    }

    /**
     * @param supp_types
     * @return
     */
    private List removeGeometryType(List dta_types) {
        List resp = new ArrayList();
        
        int len = dta_types.size();
        Object item_o = null;
        DataType item_dt = null;
        
        for (int i=0; i<len; i++) {
            item_o = dta_types.get(i);
            if (item_o instanceof DataType) {
                item_dt = (DataType) item_o;
                if (item_dt.getType() != DataTypes.GEOMETRY) {
                    resp.add(item_dt);
                }
            }
        }
        return resp;
    }

    private List<ComboItemGeomType> leaveSupportedGeomTypes(
        List<ComboItemGeomType> all_types,
        List<int[]> supported,
        int index /* 0 for geom types, 1 for geom subtypes */) {
        
        if (supported == null) {
            // all supported
            return all_types;
        }
        
        List<ComboItemGeomType> resp = new ArrayList<ComboItemGeomType>();
        int len = all_types.size();
        ComboItemGeomType item = null;
        for (int i=0; i<len; i++) {
            item = all_types.get(i);
            if (isInNthElement(item.getGeomType(), supported, index)) {
                resp.add(item);
            }
        }
        return resp;
    }


    private boolean isInNthElement(int val, List<int[]> arr_list, int index) {
        
        int len = arr_list.size();
        int[] item = null;
        for (int i=0; i<len; i++) {
            item = arr_list.get(i);
            if (item != null && item.length > index && item[index] == val) {
                return true;
            }
        }
        return false;
    }
    
    private boolean dataTypeNeedsSize(int data_type) {
        
        if (data_type == DataTypes.FLOAT
            || data_type == DataTypes.INT
            || data_type == DataTypes.STRING) {
            return true;
        } else {
            return false;
        }
    }
    
    private class SimpleTextCellRenderer extends DefaultTableCellRenderer {

        public SimpleTextCellRenderer() {
            super();
        }
        /* (non-Javadoc)
         * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
         */
        public Component getTableCellRendererComponent(
            JTable table,
            Object value, boolean isSelected, boolean hasFocus, int row,
            int column) {
            
            TableModel tm = table.getModel();
            Object new_val = "";            
            Object type_obj = tm.getValueAt(row, COLUMN_TYPE);
            if (type_obj instanceof ComboItemDataType) {
                ComboItemDataType type_item = (ComboItemDataType) type_obj;
                if (dataTypeNeedsSize(type_item.dataType.getType())) {
                    new_val = getCurrentOr(value, "");
                } else {
                    new_val = Messages.getText("not_applicable");
                }
                return super.getTableCellRendererComponent(
                    table, new_val, isSelected, hasFocus, row, column);
            } else {
                return super.getTableCellRendererComponent(
                    table, value, isSelected, hasFocus, row, column);
            }
        }
        
        private Object getCurrentOr(Object v, String d) {
            
            if (v instanceof String) {
                String parse_str = (String) v;
                try {
                    Integer.parseInt(parse_str);
                    return v;
                } catch (Exception ex) {
                    // not a valid integer
                    return d;
                }
            } else {
                return v;
            }
        }

    }

    private class EnableLengthItemListener implements ItemListener {
        
        private TableModel theTableModel = null;
        private JTable theTable = null;
        
        public EnableLengthItemListener(JTable t, boolean b) {
            theTable = t;
            theTableModel = t.getModel();
        }
        
        public void itemStateChanged(final ItemEvent e) {
            doUpdate(e);
        }

        public void doUpdate(ItemEvent e) {
            
            Object src_obj = e.getSource();
            if (src_obj instanceof JComboBox) {
                
                // one of the field type combos has changed
                int len = theTableModel.getRowCount();
                Object type_obj = null;
                ComboItemDataType type_item = null;
                for (int i=0; i<len; i++) {
                    type_obj = theTableModel.getValueAt(i, COLUMN_TYPE);
                    if (type_obj instanceof ComboItemDataType) {
                        type_item = (ComboItemDataType) type_obj;
                        if (dataTypeNeedsSize(type_item.dataType.getType())) {
                            theTableModel.setValueAt(
                                "20",
                                i, COLUMN_LENGTH);
                        } else {
                            theTableModel.setValueAt(
                                Messages.getText("not_applicable"),
                                i, COLUMN_LENGTH);
                        }
                    }
                }
                theTable.validate(); // .revalidate(); // .repaint();
            }
        }

    }
    
    
    
    
    
}
