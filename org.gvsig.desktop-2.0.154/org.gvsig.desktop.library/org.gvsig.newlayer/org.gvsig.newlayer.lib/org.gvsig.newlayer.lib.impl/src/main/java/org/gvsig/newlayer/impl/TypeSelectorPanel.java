/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.awt.BorderLayout;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerWizard;

public class TypeSelectorPanel extends JPanel implements OptionPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 419873365484240927L;
	private NewLayerWizard wizard;
    private NewLayerProviderFactory currentType;

	public TypeSelectorPanel(NewLayerWizard wizard){
		this.wizard = wizard;
		initializeComponents();
	}

	public String getPanelTitle() {
		return Messages.getText("_Output_format");
	}

	public void nextPanel() throws NotContinueWizardException {
		if (currentType!=null){
			this.wizard.setType(currentType);
			return;
		}
		throw new NotContinueWizardException("", null, false);
	}

	public void lastPanel() {
		// do nothing
		
	}

	public void updatePanel() {
		initializeComponents();
	}

	public JPanel getJPanel() {
		return this;
	}

    private void initializeComponents() {
        this.setLayout(new BorderLayout());

        JList types = new JList();     
        ListModel model =
            new org.gvsig.utils.DefaultListModel(this.getService()
                .getProviderFactories());
       
        types.setModel(model);
        types.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
        
        types.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false) {
					JList list = (JList) e.getSource();
					
			        if (list.getSelectedIndex() > -1) {
                        currentType =
                            (NewLayerProviderFactory) list.getSelectedValue();
                        
                        wizard.setNextButtonEnabled(true);
			        } else {
			            wizard.setNextButtonEnabled(false);
			        }
			    }
			}
		});
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(types);
        this.add(scrollPane, BorderLayout.CENTER);        
    }

	private NewLayerService getService() {
		return this.wizard.getService();
	} 
}
