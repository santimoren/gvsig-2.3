/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.newlayer.NewLayerProvider;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerServiceException;
import org.gvsig.tools.service.ServiceException;

/**
 * Default {@link NewLayerService} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultNewLayerService implements NewLayerService {

    private NewLayerManager newLayerManager;
    private NewLayerProviderFactory type;
    private NewLayerProvider provider;
    private boolean addLayerToView = true;
    private MapContext mapContext;

    /**
     * {@link DefaultNewLayerService} constructor
     */
    public DefaultNewLayerService(NewLayerManager manager, MapContext mapContext) {
        this.newLayerManager = manager;
        this.mapContext = mapContext;
    }

    public NewLayerProvider getProvider() {
        return provider;
    }

    public void createLayer() throws NewLayerServiceException {
        try {
            this.getExplorer().add(this.getStoreName(),
                this.getNewStoreParameters(), true);
        } catch (DataException e) {
            throw new CantCreateNewLayerException(e);
        }
    }

    public void loadLayer() throws NewLayerServiceException {
        try {
            String storeName = this.getStoreName();
            DataStoreParameters storeParameters =
                this.getOpenStoreParameters();
            DataManager dataManager = DALLocator.getDataManager();
            DataStore store = dataManager.openStore(storeName, storeParameters);
            String layerName = store.getName();

            MapContextManager mapContextManager =
                MapContextLocator.getMapContextManager();
            FLayer lyr = mapContextManager.createLayer(layerName, store);
            lyr.setActive(true);
            lyr.setVisible(true);
            mapContext.getLayers().addLayer(lyr);

            lyr.dispose();
        } catch (DataException e) {
            throw new CantOpenStoreException(e);
        } catch (ValidateDataParametersException e) {
            throw new CantOpenStoreException(e);
        } catch (LoadLayerException e) {
            throw new CantLoadNewLayerException(e);
        }

    }

    public class CantCreateNewLayerException extends NewLayerServiceException {

        /**
		 * 
		 */
        private static final long serialVersionUID = 4208215791054246118L;

        public CantCreateNewLayerException(Throwable cause) {
            super("Can't create the layer", cause, "_cant_create_the_layer",
                serialVersionUID);
        }
    }

    public class CantOpenStoreException extends NewLayerServiceException {

        /**
		 * 
		 */
        private static final long serialVersionUID = -2245228621032918630L;

        public CantOpenStoreException(Throwable cause) {
            super("Can't open store", cause, "_cant_open_store",
                serialVersionUID);
        }
    }

    public class CantLoadNewLayerException extends NewLayerServiceException {

        /**
		 * 
		 */
        private static final long serialVersionUID = -1711950651766745963L;

        public CantLoadNewLayerException(Throwable cause) {
            super("Can't load the new layer", cause,
                "_cant_load_the_new_layer", serialVersionUID);
        }
    }

    public void setType(String type) {
        try {
            setProviderFactory(this.newLayerManager
                .getNewLayerProviderFactory(type));
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
    }

    public DataServerExplorer getExplorer() {
        return getProvider().getExplorer();
    }

    public String getStoreName() {
        return getProvider().getStoreName();
    }

    public EditableFeatureType getFeatureType() {
        return getProvider().getFeatureType();
    }

    public NewFeatureStoreParameters getNewStoreParameters() {
        return getProvider().getNewStoreParameters();
    }

    public DataStoreParameters getOpenStoreParameters() {
        return getProvider().getOpenStoreParameters();
    }

    public List<String> getTypes() {
        List<String> types = new ArrayList<String>();
        List<NewLayerProviderFactory> providers = getProviderFactories();
        Iterator<NewLayerProviderFactory> it = providers.iterator();
        while (it.hasNext()) {
            NewLayerProviderFactory newLayerProviderFactory = it.next();
            types.add(newLayerProviderFactory.getName());
        }
        return types;
    }

    public String getType() {
        return this.type.getName();
    }

    public void addLayerToView(boolean b) {
        this.addLayerToView = b;
    }

    public MapContext getMapContext() {
        return mapContext;
    }

    public void setProviderFactory(NewLayerProviderFactory type) {
        this.type = type;
        this.provider = type.create(this);
    }

    public NewLayerProviderFactory getProviderFactory() {
        return type;
    }

    public List<NewLayerProviderFactory> getProviderFactories() {

        List<NewLayerProviderFactory> providers =
            new ArrayList<NewLayerProviderFactory>(
                this.newLayerManager
                    .getProviders(NewLayerManager.STORETYPE.ANY));

        for (Iterator<NewLayerProviderFactory> iterator = providers.iterator(); iterator
            .hasNext();) {
            NewLayerProviderFactory newLayerProviderFactory = iterator.next();
            if (!newLayerProviderFactory.isEnabled()) {
                iterator.remove();
            }
        }

        return providers;
    }

}
