/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.spi;

import org.gvsig.newlayer.NewLayerLocator;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderServices;

public abstract class AbstractNewLayerProviderFactory implements NewLayerProviderFactory{

	public final Provider doCreate(DynObject parameters, ProviderServices services) {
		throw new UnsupportedOperationException();
	}

	public DynObject createParameters() {
		throw new UnsupportedOperationException();
	}

	public void initialize() {
		throw new UnsupportedOperationException();
	}

	protected DynClass createParametersDynClass() {
		throw new UnsupportedOperationException();
	}

	public Provider create(DynObject parameters, ProviderServices services)
	throws ServiceException {
		throw new UnsupportedOperationException();
	}

    public boolean isEnabled() {
        return NewLayerLocator.getManager().isProviderEnabled(this);
    }

    public void setEnabled(boolean value) {
        NewLayerLocator.getManager().enableProvider(this, value);
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
