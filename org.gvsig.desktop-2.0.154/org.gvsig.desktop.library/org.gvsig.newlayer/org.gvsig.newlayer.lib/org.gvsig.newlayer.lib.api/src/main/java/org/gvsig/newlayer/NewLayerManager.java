/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer;

import java.util.List;

import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.newlayer.preferences.NewLayerPreferencesComponent;
import org.gvsig.tools.service.ServiceException;

/**
 * This class is responsible of the management of the library's business logic.
 * It is the library's main entry point, and provides all the services to manage
 * {@link NewLayerService}s.
 * 
 * @see NewLayerService
 * @author gvSIG team
 * @version $Id$
 */
public interface NewLayerManager {

    public enum STORETYPE{ANY,TABULAR,SPATIAL};
    

    /**
     * Create an instance of a {@link NewLayerService}.
     * @return {@link NewLayerService}
     * @throws ServiceException
     *             if there is an error getting the service
     */
    public NewLayerService createNewLayerService(MapContext mapContext);
    
    public void registerProvider(NewLayerProviderFactory factory);
    
    public List<NewLayerProviderFactory> getProviders(STORETYPE filter);

    public List<NewLayerProviderFactory> getProviders();
    
    public NewLayerWizard createNewLayerWizard(NewLayerService service);

    /**
     * Returns the provider factory with the given name.
     * 
     * @param name
     *            of the provider
     * @return the provider factory
     * @throws ServiceException
     *             if there is an error getting the provider factory
     */
    public NewLayerProviderFactory getNewLayerProviderFactory(
        String providerName) throws ServiceException;

    /**
     * Enables or disables a new layer provider.
     * 
     * @param factory
     *            of the provider to enable or disable
     * @param value
     *            if the provider must be enabled or disabled
     */
    public void enableProvider(NewLayerProviderFactory factory, Boolean false1);

    /**
     * Creates a preferences component to manage the export to properties.
     * 
     * @return a preferences component
     */
    public NewLayerPreferencesComponent createNewLayerProvidersPreferences();

    /**
     * Returns if the provider whose factory is provided is enabled.
     * 
     * @param factory
     *            of the provider to check
     * @return if the provider whose factory is provided is enabled
     */
    public boolean isProviderEnabled(NewLayerProviderFactory factory);

}
