/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer;

import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.service.ServiceException;

/**
 * Exception thrown when there is an error getting a NewLayer message.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class NewLayerServiceException extends ServiceException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4395786952326742397L;

	private static final String MESSAGE =
        "An error has been produced creating a store";

    private static final String KEY = "_NewLayerServiceException";
    
	/**
	 * @see BaseException#BaseException(String, String, long)
	 */
	protected NewLayerServiceException(String message, String key, long code) {
		super(message, key, code);
	}

	/**
	 * @see BaseException#BaseException(String, Throwable, String, long)
	 */
	protected NewLayerServiceException(String message, Throwable cause, String key,
			long code) {
		super(message, cause, key, code);
	}

    /**
     * Creates a new {@link NewLayerServiceException}.
     * 
     * @param cause
     *            the original cause
     */
    public NewLayerServiceException(Throwable cause) {
        super(MESSAGE, cause, KEY, serialVersionUID);
    }
    
}
