/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer;

import java.util.List;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.mapcontext.MapContext;

/**
 * <p>
 * This service is used to create a new {@link FeatureStore}.
 * </p>
 * 
 * @author gvSIG team
 * @version $Id$
 */
public interface NewLayerService {

    /**
     * @deprecated use {@link #setProviderFactory(NewLayerProviderFactory)}
     *             instead
     */
    public void setType(String type);

    /**
     * @deprecated use {@link #getProviderFactory()} instead
     */
    public String getType();

    /**
     * @deprecated use {@link #getProviderFactories()} instead
     */
    public List<String> getTypes();

    public void setProviderFactory(NewLayerProviderFactory type);
    
    public NewLayerProviderFactory getProviderFactory();

    public List<NewLayerProviderFactory> getProviderFactories();
    
    public void createLayer() throws NewLayerServiceException;

	public void loadLayer() throws NewLayerServiceException;

    public DataServerExplorer getExplorer();
    
    public String getStoreName();
    
    public EditableFeatureType getFeatureType();
    
    public NewFeatureStoreParameters getNewStoreParameters();
    
    public DataStoreParameters getOpenStoreParameters();
    
    public NewLayerProvider getProvider();
    
    public void addLayerToView(boolean b);
    
    public MapContext getMapContext();
    
}
