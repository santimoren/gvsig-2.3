/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing;



/**
 * Responsible of the window management to show the panels of the
 * Annotation swing library.
 * 
 * @see AnnotationSwingManager
 * @see JAnnotationCreationServicePanel
 * @author gvSIG Team
 * @version $Id$
 */
public interface AnnotationWindowManager {

    public static final int MODE_DIALOG = 1;
    public static final int MODE_WINDOW = 2;
    public static final int MODE_TOOL = 3;

    /**
     * Inserts a Panel in a window with a characteristic properties.
     * 
     * @param panel
     *            JPanel with the content of the window
     * @param title
     *            String with the title of the window
     * @param mode
     *            int that defines the type of window
     */
    public void showWindow(JAnnotationCreationServicePanel panel, String title, int mode);
    
	public boolean showFileExistsPopup(String title, String text);
	
	public void showPreferencesWindow(JAnnotationPreferencesPanel panel, String title, int mode);
    
}
