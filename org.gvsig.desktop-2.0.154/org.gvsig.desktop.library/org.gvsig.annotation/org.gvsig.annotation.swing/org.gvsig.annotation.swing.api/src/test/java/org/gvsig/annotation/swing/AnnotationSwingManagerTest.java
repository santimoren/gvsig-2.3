/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.service.ServiceException;

/**
 * API compatibility tests for {@link AnnotationSwingManager}
 * implementations.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AnnotationSwingManagerTest extends
    AbstractLibraryAutoInitTestCase {

    protected AnnotationManager manager;
    protected AnnotationSwingManager swingManager;

    @Override
    protected void doSetUp() throws Exception {
        manager = AnnotationLocator.getManager();
        swingManager = AnnotationSwingLocator.getSwingManager();
    }

    public void testGetManager() {
        assertEquals(manager, swingManager.getManager());
    }

    public void testCreationJAnnotationServicePanel()
        throws ServiceException, DataException {
        AnnotationCreationService annotationCreationService = manager.getAnnotationCreationService(null);

        JAnnotationCreationServicePanel panel1 =
            swingManager.createAnnotation(annotationCreationService);

        assertNotNull(panel1);
    }

}
