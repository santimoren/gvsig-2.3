/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing.impl;

import java.util.Locale;

import org.gvsig.annotation.swing.AnnotationSwingLibrary;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconThemeManager;

/**
 * Library for default swing implementation initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class AnnotationSwingDefaultImplLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsImplementationOf(AnnotationSwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
        AnnotationSwingLocator
            .registerSwingManager(DefaultAnnotationSwingManager.class);

        IconThemeManager themeManager = ToolsSwingLocator.getIconThemeManager();
        
        themeManager.getDefault().registerDefault(
        		this.getClass().getName(), 
        		"annotation", 
        		"wizard-annotation", 
        		null, 
        		this.getClass().getClassLoader().getResource("org/gvsig/annotation/swing/impl/images/wizard-icon.png")
        );

    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        AnnotationSwingLocator.getSwingManager().registerWindowManager(new DefaultAnnotationWindowManager());
        
        if (!Messages.hasLocales()) {
            Messages.addLocale(Locale.getDefault());
        }
        Messages.addResourceFamily("org.gvsig.annotation.swing.impl.i18n.text",
            AnnotationSwingDefaultImplLibrary.class.getClassLoader(),
            AnnotationSwingDefaultImplLibrary.class.getClass().getName());
    }

}
