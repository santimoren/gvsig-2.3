/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.annotation.swing.AnnotationWindowManager;
import org.gvsig.annotation.swing.AnnotationWizardPanelActionListener;
import org.gvsig.annotation.swing.JAnnotationCreationServicePanel;
import org.gvsig.annotation.swing.JAnnotationPreferencesPanel;

/**
 * Default implementation for the {@link AnnotationWindowManager}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultAnnotationWindowManager implements
    AnnotationWindowManager, ComponentListener {

    public void showWindow(JAnnotationCreationServicePanel panel, String title, int mode) {
        JFrame frame = showJPanel(panel, title, mode)   ;  
        panel.setAnnotationServicePanelActionListener(new FrameWizardListener(frame));             
    }
    

	public void showPreferencesWindow(JAnnotationPreferencesPanel panel, String title, int modeDialog) {
		showJPanel(panel, title, modeDialog);
	}
	
	private JFrame showJPanel(JPanel panel, String title, int modeDialog) {		       
		 JFrame frame = new JFrame();    
		 
        final Dimension s = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(s.width / 4, s.height / 4);

        frame.setLayout(new BorderLayout());
        frame.setTitle(title);
        frame.add(panel, BorderLayout.CENTER);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        panel.addComponentListener(this);

        frame.pack();
        frame.setVisible(true);	
        return frame;
	}
	
    public void componentHidden(ComponentEvent componentEvent) {
        JFrame frame =
            (JFrame) ((JPanel) componentEvent.getSource()).getRootPane()
                .getParent();
        frame.setVisible(false);
        frame.dispose();
    }

    public void componentMoved(ComponentEvent arg0) {
        // Do nothing
    }

    public void componentResized(ComponentEvent arg0) {
        // Do nothing
    }

    public void componentShown(ComponentEvent arg0) {
        // Do nothing
    }
    
    public class FrameWizardListener implements AnnotationWizardPanelActionListener{
    	private JFrame frame = null;
    	
    	public FrameWizardListener(JFrame frame) {
    		super();
    		this.frame = frame;
    	}

    	public void cancel(
    			JAnnotationCreationServicePanel annotationCreationServicePanel) {
    		frame.setVisible(false);
    		System.exit(0);		
    	}

    	public void finish(
    			JAnnotationCreationServicePanel annotationCreationServicePanel) {
    		frame.setVisible(false);
    		System.exit(0);		
    	}
    }

	public boolean showFileExistsPopup(String title, String text) {
		int resp = JOptionPane.showConfirmDialog(
				new JFrame(), text,
				title, JOptionPane.YES_NO_OPTION);
		if (resp == JOptionPane.YES_OPTION) {
			return true;
		}
		return false;
	}


}
