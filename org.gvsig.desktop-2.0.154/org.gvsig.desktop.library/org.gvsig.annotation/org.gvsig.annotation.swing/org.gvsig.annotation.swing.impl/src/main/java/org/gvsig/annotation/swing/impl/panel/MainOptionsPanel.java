/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
 
package org.gvsig.annotation.swing.impl.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.fmap.dal.exception.DataException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class MainOptionsPanel extends JPanel{
	private JLabel annotationPositionCalculatorLabel;
	private JComboBox annotationPositionCalculatorCombo;
	private JLabel textValueLabel;
	private FeatuteAttributeCombo textValueCombo;
	private JLabel descriptionLabel;
	
	protected AnnotationManager annotationManager = null;
	protected AnnotationSwingManager annotationSwingManager = null;
	protected AnnotationCreationService annotationCreationService = null;
	
	public MainOptionsPanel(AnnotationCreationService annotationCreationService) throws DataException {
		super();	
		this.annotationCreationService = annotationCreationService;
		annotationManager = AnnotationLocator.getManager();
		annotationSwingManager = AnnotationSwingLocator.getSwingManager();
		initComponents();
		initCombos();
	}

	private void initCombos() {
		List<String> annotationPositionCalculators = annotationManager.getAnnotationPositionCalculatorList();
		for (int i=0 ; i<annotationPositionCalculators.size() ; i++){
			annotationPositionCalculatorCombo.addItem(annotationPositionCalculators.get(i));
		}	
	}

	private void initComponents() throws DataException {
		java.awt.GridBagConstraints gridBagConstraints;
		
		annotationPositionCalculatorLabel = new JLabel();
		textValueCombo = new FeatuteAttributeCombo(annotationCreationService.getFeatureStore(), false);
		annotationPositionCalculatorCombo = new JComboBox();
		textValueLabel = new JLabel();	
		descriptionLabel = new JLabel();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		setLayout(gridBagLayout);
		
		descriptionLabel.setText(annotationSwingManager.getTranslation("descripcion_de_crear_capa_de_anotaciones_nueva"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 30, 2);
		add(descriptionLabel, gridBagConstraints);		
		
		annotationPositionCalculatorLabel.setText(annotationSwingManager.getTranslation("duplicate"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(annotationPositionCalculatorLabel, gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 0.5;
		gridBagConstraints.insets = new Insets(2, 2, 10, 2);
		add(annotationPositionCalculatorCombo, gridBagConstraints);
		
		textValueLabel.setText(annotationSwingManager.getTranslation("seleccione_el_campo_de_texto_que_desea_que_se_utilize_para_mostrar_la_nueva_capa"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(textValueLabel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;	
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;	
		gridBagConstraints.weightx = 0.5;
		gridBagConstraints.insets = new Insets(2, 2, 10, 2);
		add(textValueCombo, gridBagConstraints);
	}

	public String getAnnotationPositionCalculatorName() {
		return annotationPositionCalculatorCombo.getSelectedItem().toString();
	}

	public String getTextValueAttribute() {
		return textValueCombo.getSelectedAttribute();
	}	
}

