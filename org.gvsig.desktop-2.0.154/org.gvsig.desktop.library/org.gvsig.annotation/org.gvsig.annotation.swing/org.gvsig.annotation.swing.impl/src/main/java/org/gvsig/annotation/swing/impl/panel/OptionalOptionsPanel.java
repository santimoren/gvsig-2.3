/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
 
package org.gvsig.annotation.swing.impl.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationDataTypes;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.fmap.dal.exception.DataException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class OptionalOptionsPanel extends JPanel{
	private JLabel rotationLabel;
	private FeatuteAttributeCombo rotationCombo;
	private JLabel colorLabel;
	private FeatuteAttributeCombo colorCombo;
	private JLabel heightLabel;
	private FeatuteAttributeCombo heightCombo;
	private JLabel fontLabel;
	private JLabel fontUnitsLabel;
	private FeatuteAttributeCombo fontCombo;
	private JLabel descriptionLabel;
	
	protected AnnotationCreationService annotationCreationService = null;
	protected AnnotationManager annotationManager = null;
	protected AnnotationSwingManager annotationSwingManager = null;
	
	public OptionalOptionsPanel(AnnotationCreationService annotationCreationService) throws DataException {
		super();	
		this.annotationCreationService = annotationCreationService;
		annotationManager = AnnotationLocator.getManager();
		annotationSwingManager = AnnotationSwingLocator.getSwingManager();
		initComponents();
		initCombos();
	}

	private void initCombos() {
		
	}

	private void initComponents() throws DataException {
		java.awt.GridBagConstraints gridBagConstraints;
		
		rotationLabel = new JLabel();
		rotationCombo = new FeatuteAttributeCombo(annotationCreationService.getFeatureStore(), AnnotationDataTypes.FONTROTATION, true);
		colorLabel = new JLabel();
		colorCombo = new FeatuteAttributeCombo(annotationCreationService.getFeatureStore(), AnnotationDataTypes.FONTCOLOR, true);
		heightLabel = new JLabel();
		heightCombo = new FeatuteAttributeCombo(annotationCreationService.getFeatureStore(), AnnotationDataTypes.FONTHEIGHT, true);
		fontLabel = new JLabel();
		fontCombo = new FeatuteAttributeCombo(annotationCreationService.getFeatureStore(), AnnotationDataTypes.FONTSTYLE, true);
		descriptionLabel = new JLabel();
		fontUnitsLabel = new JLabel();		
		
		GridBagLayout gridBagLayout = new GridBagLayout();			
		setLayout(gridBagLayout);
		
		descriptionLabel.setText(annotationSwingManager.getTranslation("descripcion_de_configuracion_capa_de_anotaciones"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 30, 2);
		add(descriptionLabel, gridBagConstraints);			
		
		rotationLabel.setText(annotationSwingManager.getTranslation("seleccione_el_campo_angulo_de_la_capa_de_anotaciones"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(rotationLabel, gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.insets = new Insets(2, 2, 10, 2);
		add(rotationCombo, gridBagConstraints);
		
		colorLabel.setText(annotationSwingManager.getTranslation("seleccione_el_campo_color_de_la_capa_de_anotaciones"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(colorLabel, gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;	
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.insets = new Insets(2, 2, 10, 2);
		add(colorCombo, gridBagConstraints);
		
		heightLabel.setText(annotationSwingManager.getTranslation("seleccione_el_campo_tamano_de_la_capa_de_anotaciones"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(heightLabel, gridBagConstraints);			
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.insets = new Insets(2, 0, 10, 2);
		add(heightCombo, gridBagConstraints);
		
		fontUnitsLabel.setText(annotationSwingManager.getTranslation("en_unidades"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.insets = new Insets(2, 0, 10, 2);
		add(fontUnitsLabel, gridBagConstraints);				
		
		fontLabel.setText(annotationSwingManager.getTranslation("seleccione_el_campo_fuente_de_la_capa_de_anotaciones"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(fontLabel, gridBagConstraints);	
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.insets = new Insets(2, 2, 2, 2);
		add(fontCombo, gridBagConstraints);	
	}

	public String getRotationAttribute() {
		return rotationCombo.getSelectedAttribute();
	}

	public String getColorAttribute() {
		return colorCombo.getSelectedAttribute();
	}

	public String getHeightAttribute() {
		return heightCombo.getSelectedAttribute();
	}

	public String getFontAttribute() {
		return fontCombo.getSelectedAttribute();
	}	
}

