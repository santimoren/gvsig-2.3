/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.annotation.swing.impl;

import java.io.File;

import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatus;



/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class WizardListenerAdapter implements WizardPanelActionListener {
	private DefaultJAnnotationCreationServicePanel annotationCreationServicePanel;
	private AnnotationSwingManager annotationSwingManager = null;

	public WizardListenerAdapter(
			DefaultJAnnotationCreationServicePanel annotationCreationServicePanel) {
		super();
		this.annotationCreationServicePanel = annotationCreationServicePanel;
		this.annotationSwingManager = AnnotationSwingLocator.getSwingManager();
	}

	public void cancel(WizardPanel wizardPanel) {
		TaskStatus taskStatus = annotationCreationServicePanel.getAnnotationCreationService().getTaskStatus();
		if(taskStatus instanceof SimpleTaskStatus){
			((SimpleTaskStatus)taskStatus).cancel();
		}
		if (annotationCreationServicePanel.getAnnotationServicePanelActionListener() != null){
			annotationCreationServicePanel.getAnnotationServicePanelActionListener().cancel(annotationCreationServicePanel);	
		}
	}

	public void finish(WizardPanel wizardPanel) {
		if (annotationCreationServicePanel.getAnnotationServicePanelActionListener() != null){
			File file = new File(annotationCreationServicePanel.getDestinationShapeFile());

			boolean continueIfExists = true;

			if (file.exists()){
				continueIfExists = annotationSwingManager.getWindowManager().showFileExistsPopup(Messages.getText("guardar"),
						Messages.getText("fichero_ya_existe_seguro_desea_guardarlo"));
			}

			if (continueIfExists){
			    annotationCreationServicePanel.createAnnotation();
			}
		}
	}
}

