/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.swing.impl;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.annotation.swing.AnnotationWindowManager;
import org.gvsig.annotation.swing.JAnnotationCreationServicePanel;
import org.gvsig.annotation.swing.JAnnotationPreferencesPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.service.ServiceException;

/**
 * Default implementation of the {@link AnnotationSwingManager}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultAnnotationSwingManager implements
    AnnotationSwingManager {

    private AnnotationManager annotationManager;
    private AnnotationWindowManager windowManager;    

    public DefaultAnnotationSwingManager() {
        this.annotationManager = AnnotationLocator.getManager();
    }

    public JAnnotationCreationServicePanel createAnnotation(AnnotationCreationService annotationCreationService) throws ServiceException{
        return new DefaultJAnnotationCreationServicePanel(this, annotationCreationService);   
    }    
    
	public JAnnotationPreferencesPanel createAnnotationPreferences() {
		return new DefaultJAnnotationPreferencesPanel(annotationManager);
	}

    public AnnotationManager getManager() {
        return this.annotationManager;
    }

    public String getTranslation(String key) {
       return Messages.getText(key);       
    }

	public AnnotationWindowManager getWindowManager() {
		return windowManager;
	}

	public void registerWindowManager(AnnotationWindowManager windowManager) {
		this.windowManager = windowManager;		
	}
}
