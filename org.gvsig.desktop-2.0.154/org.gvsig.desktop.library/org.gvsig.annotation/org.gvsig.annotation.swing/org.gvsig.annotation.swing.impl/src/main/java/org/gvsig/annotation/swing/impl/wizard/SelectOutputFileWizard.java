/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
 
package org.gvsig.annotation.swing.impl.wizard;

import java.io.File;

import javax.swing.JPanel;

import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.annotation.swing.impl.DefaultJAnnotationCreationServicePanel;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.gui.beans.wizard.panel.SelectFileOptionPanel;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class SelectOutputFileWizard extends SelectFileOptionPanel implements OptionPanel{
	private static AnnotationSwingManager annotationSwingManager =  AnnotationSwingLocator.getSwingManager();
	private DefaultJAnnotationCreationServicePanel annotationCreationServicePanel = null;
	
	public SelectOutputFileWizard(DefaultJAnnotationCreationServicePanel annotationCreationServicePanel) {
		super(annotationSwingManager.getTranslation("seleccionar_fichero"));	
		this.annotationCreationServicePanel = annotationCreationServicePanel;
	}

	public JPanel getJPanel() {
		return this;
	}

	public String getPanelTitle() {
		return "";
	}

	public void lastPanel() {
		annotationCreationServicePanel.setNextButtonEnabled(true);
		
	}

	public void nextPanel() throws NotContinueWizardException {
		// TODO Auto-generated method stub
		
	}

	public void updatePanel() {
		annotationCreationServicePanel.setNextButtonEnabled(false);
		checkNextButtonEnabled();		
	}
	
	@Override
	protected void checkNextButtonEnabled() {
		if (annotationCreationServicePanel != null){		
			File file = getSelectedFile();
			if (file != null){
				if (file.getParentFile() != null){
					annotationCreationServicePanel.setFinishButtonEnabled(file.getParentFile().exists());
					return;
				}
			}
			annotationCreationServicePanel.setFinishButtonEnabled(false);
		}
	}

}

