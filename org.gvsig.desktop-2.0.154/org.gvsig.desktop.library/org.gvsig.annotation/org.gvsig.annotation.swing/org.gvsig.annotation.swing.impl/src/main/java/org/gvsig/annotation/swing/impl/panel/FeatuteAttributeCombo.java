/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2010 {Prodevelop}   {Task}
*/
 
package org.gvsig.annotation.swing.impl.panel;

import javax.swing.JComboBox;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dataTypes.DataTypes;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class FeatuteAttributeCombo extends JComboBox{
	private FeatureStore featureStore;
	private boolean isNullable = true;
	
	public FeatuteAttributeCombo(FeatureStore featureStore, boolean isNullable) throws DataException {
		super();
		this.featureStore = featureStore;
		setNullable(isNullable);
		addFields();
	}
	
	public FeatuteAttributeCombo(FeatureStore featureStore, int dataType, boolean isNullable) throws DataException {
		super();
		this.featureStore = featureStore;
		setNullable(isNullable);
		addFields(dataType);
	}
	
	private void setNullable(boolean isNullable){
		this.isNullable = isNullable;
		if (isNullable){
			this.addItem(new String("- " + Messages.getText("default") + " -"));
		}
	}	

	private void addFields() throws DataException {
		FeatureAttributeDescriptor[] featureAttributeDescriptors = featureStore.getDefaultFeatureType().getAttributeDescriptors();
		for (int i=0; i<featureAttributeDescriptors.length ; i++){
			this.addItem(featureAttributeDescriptors[i].getName());
		}
	}
	
	private void addFields(int dataType) throws DataException {
		FeatureAttributeDescriptor[] featureAttributeDescriptors = featureStore.getDefaultFeatureType().getAttributeDescriptors();
		for (int i=0; i<featureAttributeDescriptors.length ; i++){
			if (featureAttributeDescriptors[i].getDataType().getType() == dataType){
				this.addItem(featureAttributeDescriptors[i].getName());
			}
			//Double fields includes Integer and long
			if (DataTypes.DOUBLE == dataType){
				if (DataTypes.INT == featureAttributeDescriptors[i].getDataType().getType()){
					this.addItem(featureAttributeDescriptors[i].getName());
				}else if (DataTypes.LONG == featureAttributeDescriptors[i].getDataType().getType()){
					this.addItem(featureAttributeDescriptors[i].getName());
				}
			}				
		}
	}

	public String getSelectedAttribute() {
		if (isNullable){
			int index = super.getSelectedIndex();
			if (index == 0){
				return null;
			}else{
				return (String)super.getSelectedItem();
			}
		}else{
			return (String)super.getSelectedItem();
		}
	}	
}

