/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;


/**
 * API compatibility tests for {@link AnnotationCreationService} implementations.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AnnotationCreationServiceTest extends
    AbstractLibraryAutoInitTestCase {

    protected AnnotationManager manager;
    protected DataManager dataManager;

    @Override
    protected void doSetUp() throws Exception {
        manager = AnnotationLocator.getManager();
        dataManager = DALLocator.getDataManager();
    }

    /**
     * Returns an instance of the {@link AnnotationCreationService}.
     * 
     * @return a {@link AnnotationCreationService} instance
     * @throws Exception
     *             if there is any error creating the instance
     */
    protected AnnotationCreationService createService() throws Exception {
    	String sourceFileName = getClass().getClassLoader().getResource("org/gvsig/annotation/data/andalucia.shp").getFile();
    	DataStoreParameters sourceParameters = dataManager.createStoreParameters("Shape");
     	sourceParameters.setDynValue("shpfile", sourceFileName);
     	sourceParameters.setDynValue("crs", CRSFactory.getCRS("EPSG:23030"));
     	FeatureStore sourceStore = (FeatureStore) dataManager.openStore("Shape", sourceParameters);
    	return manager.getAnnotationCreationService(sourceStore);
    }

    /**
     * Test for the {@link AnnotationCreationService#getMessage()} method.
     * 
     * @throws Exception
     *             if there is any error in the tests
     */
    public void testAnnotationServiceMessage() throws Exception {
        AnnotationCreationService annotationCreationService = createService();
            
        File temporalFolder = new File(System.getProperty("java.io.tmpdir") +  File.separator + "tmp_gvsig_annotation");
        if (!temporalFolder.exists()){
        	if (!temporalFolder.mkdir()){
        		throw new Exception("Impossible to create the destination folder");
        	}
        }
        
        String destinationFileName =  temporalFolder.getAbsolutePath() + File.separator + Math.random() + ".shp";		
                    
        annotationCreationService.createAnnotationStore(destinationFileName, 1);
        
        DataStoreParameters destinationParameters = dataManager.createStoreParameters("Shape");
        destinationParameters.setDynValue("shpfile", destinationFileName);
        destinationParameters.setDynValue("crs", CRSFactory.getCRS("EPSG:23030"));
    	FeatureStore destinationStore = (FeatureStore) dataManager.openStore("Shape", destinationParameters);
        
    	assertNotNull(destinationStore);
    	
    	FeatureType featureType = destinationStore.getDefaultFeatureType();
    	assertEquals(7, featureType.getAttributeDescriptors().length);
    	
     	assertEquals(destinationStore.getFeatureCount(), annotationCreationService.getFeatureStore().getFeatureCount());
    	
    	assertNotNull(featureType.getDefaultGeometryAttribute());
    	assertEquals(featureType.getDefaultGeometryAttribute().getGeometryType(), Geometry.TYPES.POINT);
    	    
    	//Check the geometries
    	FeatureSet featureSet = destinationStore.getFeatureSet();
    	DisposableIterator iterator = featureSet.fastIterator();
    	
    	Feature feature;
    	TextPointPairList textPointPairList = getResult();
    	while (iterator.hasNext()){
    		feature = (Feature)iterator.next();
    		assertNotNull(feature.get(Messages.getText(AnnotationManager.TEXTVALUE_ATTRIBUTE_NAME)));
    		String text = feature.getString(AnnotationManager.TEXTVALUE_ATTRIBUTE_NAME);
    		TextPointPair textPointPair = textPointPairList.search(text);
    		if (textPointPair != null){
	    		//Check the geometry
	    		Point point = (Point)feature.getDefaultGeometry();
	    		assertEquals(point.getX(), textPointPair.getPoint().getX(), 0.01);
	    		assertEquals(point.getY(), textPointPair.getPoint().getY(), 0.01);
	    	}
    		int color = feature.getInt(AnnotationManager.FONTCOLOR_ATTRIBUTE_NAME);
    		assertEquals(color, manager.getDefaultFontColor()); 
    		
    		double heigth = feature.getDouble(AnnotationManager.FONTHEGTH_ATTRIBUTE_NAME);
    		assertEquals(heigth, manager.getDefaultFontHeight()); 
    		
    		double rotation = feature.getDouble(AnnotationManager.FONTROTATION_ATTRIBUTE_NAME);
    		assertEquals(rotation, manager.getDefaultFontRotation()); 
    		
    		String type = feature.getString(AnnotationManager.FONTTYPE_ATTRIBUTE_NAME);
    		assertEquals(type, manager.getDefaultFontType()); 
    		
    		String style = feature.getString(AnnotationManager.FONTSTYLE_ATTRIBUTE_NAME);
    		assertEquals(style, manager.getDefaultFontStyle());     		
    	}
    	
    	destinationStore.dispose();
    	annotationCreationService.getFeatureStore().dispose();
    }
    
    public abstract TextPointPairList getResult() throws CreateGeometryException;
    
    public class TextPointPairList{
    	List<TextPointPair> textPointPairs = new ArrayList<TextPointPair>();

		public TextPointPairList() {
			super();
		}
    	
		public void addPoint(String text, Point point){
			textPointPairs.add(new TextPointPair(text, point));
		}
		
		public TextPointPair search(String text){
			for (TextPointPair textPointPair : textPointPairs) {
				if (textPointPair.getText().equals(text)){
					return textPointPair;
				}
			}
			return null;
		}    	
    }
    
    
    
    private class TextPointPair{
    	private String text;
		private Point point;
		
    	public TextPointPair(String text, Point point) {
			super();
			this.text = text;
			this.point = point;
		}
    	
    	public String getText() {
			return text;
		}

		public Point getPoint() {
			return point;
		}    	
    }

    /**
     * Test for the {@link AnnotationCreationService#getManager()} method.
     * 
     * @throws Exception
     *             if there is any error in the tests
     */
    public void testAnnotationServiceManager() throws Exception {
        AnnotationCreationService annotationCreationService = createService();
        assertEquals(manager, annotationCreationService.getManager());
    }
}
