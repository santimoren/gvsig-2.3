/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation;

import java.awt.Color;
import java.util.List;

import org.gvsig.annotation.calculator.AnnotationPositionCalculator;
import org.gvsig.annotation.calculator.AnnotationPositionCalculatorCreationException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.service.ServiceException;

/**
 * This class is responsible of the management of the library's business logic.
 * It is the library's main entry point, and provides all the services to manage
 * {@link AnnotationCreationService}s.
 * 
 * @see AnnotationCreationService
 * @author gvSIG team
 * @version $Id$
 */
public interface AnnotationManager {
	/**
	 * Name of the datastore attribute that contains the text
	 * of the annotation.
	 */
	public static final String TEXTVALUE_ATTRIBUTE_NAME = "Text";
	
	/**
	 * Name of the datastore attribute that contains the font type
	 * of the annotation.
	 */
	public static final String FONTTYPE_ATTRIBUTE_NAME = "FontType";
	
	/**
	 * Name of the datastore attribute that contains the font style
	 * of the annotation.
	 */
	public static final String FONTSTYLE_ATTRIBUTE_NAME = "FontStyle";
	
	/**
	 * Name of the datastore attribute that contains the font color
	 * of the annotation.
	 */
	public static final String FONTCOLOR_ATTRIBUTE_NAME = "FontColor";
	
	/**
	 * Name of the datastore attribute that contains the font rotation
	 * of the annotation.
	 */
	public static final String FONTROTATION_ATTRIBUTE_NAME = "Rotation";
	
	/**
	 * Name of the datastore attribute that contains the font heigth
	 * of the annotation.
	 */
	public static final String FONTHEGTH_ATTRIBUTE_NAME = "Heigth";

	/**
	 * Create an instance of a {@link AnnotationCreationService}.
	 * 
	 * @param the feature store that is used like input.
	 * @return {@link AnnotationCreationService}
	 * @throws ServiceException
	 *             if there is an error getting the service
	 */
	public AnnotationCreationService getAnnotationCreationService(FeatureStore featureStore)
	throws ServiceException;

	/**
	 * It registers a class that can be used to caclulate the position of an
	 * annotation.
	 * @param name
	 * The name used to register the class.
	 * @param annotationPositionCalculatorClass
	 * The class that is able to calculate an annotation point.
	 */
	public void registerAnnotationPositionCalculator(String name, Class annotationPositionCalculatorClass);

	/**
	 * It registers the default implementation of an {@link AnnotationPositionCalculator}
	 * class. It will be used by default if the user don't specify any of them.
	 * @param annotationPositionCalculatorClass
	 * A class that is able to calculate an annotation point.
	 */
	public void registerDefaultAnnotationPositionCalculator(Class annotationPositionCalculatorClass);

	/**
	 * It returns an object that can be used to create the point
	 * where the annotation is displayed.
	 * @param name
	 * the name used to register the class.
	 * @return
	 * a class that is able to calculate an annotation point.
	 * @throws AnnotationPositionCalculatorCreationException
	 * if is not possible to create the object.
	 */
	public AnnotationPositionCalculator getAnnotationPositionCalculator(String name) throws AnnotationPositionCalculatorCreationException;

	/**
	 * It returns the default {@link AnnotationPositionCalculator}.
	 * @return
	 * the default {@link AnnotationPositionCalculator}.
	 * @throws AnnotationPositionCalculatorCreationException
	 * if is not possible to create the object.
	 */
	public AnnotationPositionCalculator getDefaultAnnotationPositionCalculator() throws AnnotationPositionCalculatorCreationException;

	/**
	 * @return
	 * the list of the names used to register the {@link AnnotationPositionCalculator} classes.
	 */
	public List<String> getAnnotationPositionCalculatorList();

	/**
	 * Value used like the default value for the text field.
	 * @return
	 * the default text value.
	 */
	public String getDefaultTextValue();

	/**
	 * Set the default value for the text field.
	 * @param textValue
	 * the default text value.
	 */
	public void setDefaultTextValue(String textValue);

	/**
	 * Value used like the default value for the font type field.
	 * @return
	 * the default font type value.
	 */
	public String getDefaultFontType();

	/**
	 * Set the default value for the font type field.
	 * @param fontType
	 * the default font type value.
	 */
	public void setDefaultFontType(String fontType);

	/**
	 * @return
	 * the list of possible values for the font type field.
	 */
	public List<String> getFontTypes();

	/**
	 * It adds a new value for the font type field.
	 * @param fontType
	 * the new font type.
	 */
	public void addFontType(String fontType);

	/**
	 * Value used like the default value for the font style field.
	 * @return
	 * the default font style value.
	 */
	public String getDefaultFontStyle();

	/**
	 * Set the default value for the font style field.
	 * @param fontStyle
	 * the default font style value.
	 */
	public void setDefaultFontStyle(String fontStyle);

	/**
	 * @return
	 * the list of possible values for the font style field.
	 */
	public List<String> getFontStyles();

	/**
	 * It adds a new value for the font style field.
	 * @param fontStyle
	 * the new font style.
	 */
	public void addFontStyle(String fontStyle);

	/**
	 * Value used like the default value for the font color field.
	 * @return
	 * the default font color value.
	 */
	public int getDefaultFontColor();

	/**
	 * Set the default value for the font color field.
	 * @param fontColor
	 * the default font color value.
	 */
	public void setDefaultFontColor(int fontColor);
	
	/**
	 * Set the default value for the font color field.
	 * @param fontColor
	 * the default font color value.
	 */
	public void setDefaultFontColor(Color fontColor);

	/**
	 * Value used like the default value for the font height field.
	 * @return
	 * the default font height value.
	 */
	public double getDefaultFontHeight();

	/**
	 * Set the default value for the font height field.
	 * @param fontHeight
	 * the default font height value.
	 */
	public void setDefaultFontHeight(double fontHeight);

	/**
	 * Value used like the default value for the font rotation field.
	 * @return
	 * the default font rotation value.
	 */
	public double getDefaultFontRotation();

	/**
	 * Set the default value for the font rotation field.
	 * @param fontRotation
	 * the default font rotation value.
	 */
	public void setDefaultFontRotation(double fontRotation);        
}
