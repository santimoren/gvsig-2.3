/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation;

import org.gvsig.annotation.calculator.AnnotationPositionCalculator;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.task.MonitorableTask;

/**
 * <p>
 * This service is used to create an annotation layer.
 * </p>
 * <p>
 * It inherits if {@link MonitorableTask}, and it means that the annotation 
 * process can be monitorized by one or more observers that can listen
 * all the export events.
 * <p>
 * @author gvSIG team
 * @version $Id$
 */
public interface AnnotationCreationService extends MonitorableTask {

	/**
	 * @return
	 * the store that is used like input.
	 */
	public FeatureStore getFeatureStore();
	
	/**
	 * This method creates a {@link FeatureStore} with the annotations. 
	 * @param destinationShapeFile 
	 * path there the shape this the annotation information is created.
	 * @param textValueAttributeIndex
	 * attribute index of the original {@link FeatureStore} that contains the text.
	 * @return
	 * a new store with the annotation information.
	 * @throws AnnotationCreationException
	 * if there is an error creating the annotation store. 
	 */
    public FeatureStore createAnnotationStore(String destinationShapeFile, int textValueAttributeIndex) throws AnnotationCreationException;
	
    /**
	 * This method creates a {@link FeatureStore} with the annotations. 
	 * @param destinationShapeFile 
	 * path there the shape this the annotation information is created.
	 * @param textValueAttributeName
	 * attribute name of the original {@link FeatureStore} that contains the text.
	 * @return
	 * a new store with the annotation information.
	 * @throws AnnotationCreationException
	 * if there is an error creating the annotation store. 
	 */
    public FeatureStore createAnnotationStore(String destinationShapeFile, String textValueAttributeName) throws AnnotationCreationException;   
    
    /**
     * Sets the {@link AnnotationCreationFinishAction} that is used at the end
     * of the annotation creation service. 
     * @param annotationCreationFinishAction
     * it contains an action that can be executed at the end of the annotation
     * creation process.
     */
    public void setAnnotationCreationFinishAction(AnnotationCreationFinishAction annotationCreationFinishAction);
    
    /**
     * @return
     * the {@link AnnotationCreationFinishAction} used at the end of the annotation
     * creation process.
     */
    public AnnotationCreationFinishAction getAnnotationCreationFinishAction();
    
    /**
     * Sets an {@link AnnotationPositionCalculator} used to calculate the
     * position of the 
     * @param annotationPositionCalculator
     */
    public void setAnnotationPositionCalculator(AnnotationPositionCalculator annotationPositionCalculator);
       
    public void setFontTypeAttribute(int index) throws DataException;
    
    public void setFontStyleAttribute(int index) throws DataException;
    			
    public void setFontColorAttribute(int index) throws DataException;
    
    public void setFontHeigthAttribute(int index) throws DataException;
    
    public void setFontRotationAttribute(int index) throws DataException;
    
    public void setFontTypeAttribute(String attributeName) throws DataException;
    
    public void setFontStyleAttribute(String attributeName) throws DataException;
    		
    public void setFontColorAttribute(String attributeName) throws DataException;
    
    public void setFontHeigthAttribute(String attributeName) throws DataException;
    
    public void setFontRotationAttribute(String attributeName) throws DataException;
    
    /**
     * Returns the {@link AnnotationManager}
     * 
     * @return {@link AnnotationManager}
     * @see {@link AnnotationManager}
     */
    public AnnotationManager getManager();
}
