/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.impl;

import java.awt.Color;

import org.gvsig.annotation.AnnotationLibrary;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.impl.calculator.CentroidPositionCalculatior;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Library for default implementation initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class AnnotationDefaultImplLibrary extends AbstractLibrary {
	  	
    @Override
    public void doRegistration() {
        registerAsImplementationOf(AnnotationLibrary.class);
    }

	@Override
    protected void doInitialize() throws LibraryException {
		AnnotationLocator.registerManager(DefaultAnnotationManager.class);           
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        AnnotationManager annotationManager = AnnotationLocator.getManager();
                
        if (annotationManager == null) {
            throw new ReferenceNotRegisteredException(
                AnnotationLocator.MANAGER_NAME, AnnotationLocator
                    .getInstance());
        }
        
        annotationManager.registerDefaultAnnotationPositionCalculator(CentroidPositionCalculatior.class);
       
        //Adding font types
        annotationManager.addFontType("Arial");
        annotationManager.addFontType("Dialog");
        annotationManager.addFontType("DialogInput");
        annotationManager.addFontType("Serif");
        annotationManager.addFontType("SansSerif");
        annotationManager.addFontType("Monospaced");
        annotationManager.addFontType("Courier");
        annotationManager.addFontType("TimesRoman");
        annotationManager.addFontType("Helvetica");
        
           //Adding font styles
        annotationManager.addFontStyle("Plain");
        annotationManager.addFontStyle("Italic");
        annotationManager.addFontStyle("Bold");
        
        //Setting the default values if there are not registered
        if (annotationManager.getDefaultTextValue() == null){
        	annotationManager.setDefaultTextValue("Text");
        }
        
        if (annotationManager.getDefaultFontColor() == -1){
        	annotationManager.setDefaultFontColor(Color.BLACK);
        }
        
        if (annotationManager.getDefaultFontHeight() == -1){
        	annotationManager.setDefaultFontHeight(10);
        }
        
        if (annotationManager.getDefaultFontRotation() == -1){
        	annotationManager.setDefaultFontRotation(0);
        }
        
        if (annotationManager.getDefaultFontStyle() == null){
        	annotationManager.setDefaultFontStyle("Plain");
        }
        
        if (annotationManager.getDefaultFontType() == null){
        	annotationManager.setDefaultFontType("Arial");
        }        
    }

}
