/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationCreationServiceException;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.calculator.AnnotationPositionCalculator;
import org.gvsig.annotation.calculator.AnnotationPositionCalculatorCreationException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.ServiceException;

/**
 * Default {@link AnnotationManager} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultAnnotationManager implements AnnotationManager {
	private static final String ANNOTATION_POSITION_CALCULATOR_EXTENSION_POINT = "AnnotationPositionCalculatorExtensionPoint";
	private ExtensionPointManager extensionPointManager = ToolsLocator.getExtensionPointManager();
	private static final String DEFAULT_ANNOTATION_POSITION_CALCULATOR = "DefaultAnnotationPositionCalculator";
	
	private String defaultTextValue = null;
	private int defaultFontColor = -1;
	private double defaultFontHeight = -1;
	private double defaultFontRotation = -1;
	private String defaultFontStyle = null;
	private String defaultFontType = null;
	
	private List<String> fontTypes = Collections.synchronizedList(new ArrayList<String>());
	private List<String> fontStyles = Collections.synchronizedList(new ArrayList<String>());
	
	
    public AnnotationCreationService getAnnotationCreationService(FeatureStore featureStore)
        throws ServiceException {
        AnnotationCreationService fc;
		try {
			fc = new DefaultAnnotationCreationService(featureStore, this);
		} catch (DataException e) {
			throw new AnnotationCreationServiceException("Impossible to create the annotation service", e);
		}
        return fc;
    }

	public AnnotationPositionCalculator getAnnotationPositionCalculator(
			String name) throws AnnotationPositionCalculatorCreationException {
		if (extensionPointManager.get(ANNOTATION_POSITION_CALCULATOR_EXTENSION_POINT).has(name)){
			try {
				return (AnnotationPositionCalculator)extensionPointManager.get(ANNOTATION_POSITION_CALCULATOR_EXTENSION_POINT).create(name);
			} catch (InstantiationException e) {
				throw new AnnotationPositionCalculatorCreationException(name, e);
			} catch (IllegalAccessException e) {
				throw new AnnotationPositionCalculatorCreationException(name, e);
			}
		}else{
			throw new IllegalArgumentException("There is not an annotation position calculator registered with this name"); 
		}		
	}

	public List<String> getAnnotationPositionCalculatorList() {
		return extensionPointManager.get(ANNOTATION_POSITION_CALCULATOR_EXTENSION_POINT).getNames();
	}

	public AnnotationPositionCalculator getDefaultAnnotationPositionCalculator() throws AnnotationPositionCalculatorCreationException {
		return getAnnotationPositionCalculator(DEFAULT_ANNOTATION_POSITION_CALCULATOR);
	}

	public void registerAnnotationPositionCalculator(String name,
			Class annotationPositionCalculatorClass) {
		if (!AnnotationPositionCalculator.class.isAssignableFrom(annotationPositionCalculatorClass)) {
			throw new IllegalArgumentException(annotationPositionCalculatorClass.getName()
					+ " must implement the AnnotationPositionCalculator interface");
		}
		
		ExtensionPoint extensionPoint = extensionPointManager.add(ANNOTATION_POSITION_CALCULATOR_EXTENSION_POINT, "");
		extensionPoint.append(name, name, annotationPositionCalculatorClass);		
	}

	public void registerDefaultAnnotationPositionCalculator(Class annotationPositionCalculatorClass) {
		registerAnnotationPositionCalculator(DEFAULT_ANNOTATION_POSITION_CALCULATOR,
				annotationPositionCalculatorClass);
	}

	public void addFontStyle(String fontStyle) {
		fontStyles.add(fontStyle);		
	}

	public void addFontType(String fontType) {
		fontTypes.add(fontType);		
	}

	public int getDefaultFontColor() {
		return defaultFontColor;
	}

	public double getDefaultFontHeight() {
		return defaultFontHeight;
	}

	public double getDefaultFontRotation() {
		return defaultFontRotation;
	}

	public String getDefaultFontStyle() {
		return defaultFontStyle;
	}

	public String getDefaultFontType() {
		return defaultFontType;
	}

	public String getDefaultTextValue() {
		return defaultTextValue;
	}

	public List<String> getFontStyles() {
		return fontStyles;
	}

	public List<String> getFontTypes() {
		return fontTypes;
	}

	public void setDefaultFontColor(int fontColor) {
		this.defaultFontColor = fontColor;		
	}
	
	public void setDefaultFontColor(Color fontColor) {
		this.defaultFontColor = fontColor.getRGB();		
	}

	public void setDefaultFontHeight(double fontHeight) {
		this.defaultFontHeight = fontHeight;		
	}

	public void setDefaultFontRotation(double fontRotation) {
		this.defaultFontRotation = fontRotation;		
	}

	public void setDefaultFontStyle(String fontStyle) {
		this.defaultFontStyle = fontStyle;		
	}

	public void setDefaultFontType(String fontType) {
		this.defaultFontType = fontType;		
	}

	public void setDefaultTextValue(String textValue) {
		this.defaultTextValue = textValue;		
	}


}
