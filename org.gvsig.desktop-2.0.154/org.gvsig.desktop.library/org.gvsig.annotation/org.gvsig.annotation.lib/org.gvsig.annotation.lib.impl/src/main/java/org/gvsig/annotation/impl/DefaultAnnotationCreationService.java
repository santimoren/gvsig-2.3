/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.gvsig.annotation.AnnotationCreationException;
import org.gvsig.annotation.AnnotationCreationFinishAction;
import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationDataTypes;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.calculator.AnnotationPositionCalculationException;
import org.gvsig.annotation.calculator.AnnotationPositionCalculator;
import org.gvsig.annotation.calculator.AnnotationPositionCalculatorCreationException;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatusManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default {@link AnnotationCreationService} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultAnnotationCreationService extends AbstractMonitorableTask implements AnnotationCreationService {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAnnotationCreationService.class);
	private static final String TEMPLATE_NAME = "template.gvslab";
	private static DataManager dataManager = DALLocator.getDataManager();

	private AnnotationManager manager;
	private FeatureStore sourceStore;

	private int sourceFontTypeAttribute = -1;
	private int sourceFontStyleAttribute = -1;
	private int sourceFontColorAttribute = -1;
	private int sourceRotationAttribute = -1;
	private int sourceHeigthAttribute = -1;
	private AnnotationPositionCalculator annotationPositionCalculator = null;

	private String destinationGeometryAttributeName = null;

	private AnnotationCreationFinishAction annotationCreationFinishAction = null; 

	/**
	 * {@link DefaultAnnotationCreationService} constructor with a
	 * {@link AnnotationManager}.
	 * 
	 * @param manager
	 *            to use in the service
	 * @throws DataException 
	 */
	public DefaultAnnotationCreationService(FeatureStore featureStore, AnnotationManager manager) throws DataException {
		super("annotation");
	    this.sourceStore = featureStore; 
		this.manager = manager;
		destinationGeometryAttributeName = featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName();
	}

	public AnnotationManager getManager() {
		return this.manager;
	}

	public FeatureStore createAnnotationStore(String destinationShapeFile, int textValueAttribute)
	throws AnnotationCreationException {		
		try {
			if (destinationShapeFile == null){
				throw new AnnotationCreationException("File can not be null");
			}	

			String destinationShapeFileWithoutExtension = destinationShapeFile.toLowerCase().replaceAll("\\.shp", "");
			if (destinationShapeFileWithoutExtension.length() == destinationShapeFile.length()-4){
				destinationShapeFileWithoutExtension =  destinationShapeFile.substring(0, destinationShapeFile.length()-4);
			}else{
				destinationShapeFileWithoutExtension = destinationShapeFile;
			}			

			NewFeatureStoreParameters newFeatureStoreParameters = (NewFeatureStoreParameters)dataManager.createNewStoreParameters("FilesystemExplorer", "Shape");
			newFeatureStoreParameters.setDynValue("shpfile", destinationShapeFileWithoutExtension + ".shp");
			newFeatureStoreParameters.setDynValue("dbffile", destinationShapeFileWithoutExtension + ".dbf");
			newFeatureStoreParameters.setDynValue("shxfile", destinationShapeFileWithoutExtension + ".shx");
			newFeatureStoreParameters.setDynValue("crs", sourceStore.getDefaultFeatureType().getDefaultSRS());

			EditableFeatureType editableFeatureType = sourceStore.getDefaultFeatureType().getEditable();//newFeatureStoreParameters.getDefaultFeatureType().getEditable();
			FeatureAttributeDescriptor[] featureAttributeDescriptors = editableFeatureType.getAttributeDescriptors();
			for (int i=featureAttributeDescriptors.length-1  ; i>=0 ; i--){
				editableFeatureType.remove(i);
			}
			initializeFeatureType(editableFeatureType);
			newFeatureStoreParameters.setDefaultFeatureType(editableFeatureType);

			dataManager.newStore("FilesystemExplorer", "Shape", newFeatureStoreParameters, true);

			//If there is not an annotationPositionCalculator it gets the default 
			if (annotationPositionCalculator == null){
				annotationPositionCalculator = manager.getDefaultAnnotationPositionCalculator();
			}	

			FeatureStore destinationStore = (FeatureStore) dataManager.openStore("Shape", newFeatureStoreParameters);

			copyFeatureStore(sourceStore, 
					destinationStore,
					textValueAttribute);	

			try {
				copyLegend(destinationStore);
			} catch (IOException e) {
				LOG.error("Error copying the legend");
			}

			if (annotationCreationFinishAction != null){
				annotationCreationFinishAction.finished(destinationStore);
			}

			return destinationStore;
		} catch (InitializeException e) {
			throw new AnnotationCreationException(e);
		} catch (ProviderNotRegisteredException e) {
			throw new AnnotationCreationException(e);
		} catch (ValidateDataParametersException e) {
			throw new AnnotationCreationException(e);
		} catch (DynFieldNotFoundException e) {
			throw new AnnotationCreationException(e);
		} catch (DataException e) {
			throw new AnnotationCreationException(e);
		} catch (AnnotationPositionCalculatorCreationException e) {
			throw new AnnotationCreationException(e);
		} 			
	}	

	public FeatureStore createAnnotationStore(String destinationShapeFile,
			String textValueAttributeName) throws AnnotationCreationException {
		try {
			return createAnnotationStore(destinationShapeFile, getIndex(textValueAttributeName));
		} catch (DataException e) {
			throw new AnnotationCreationException(e);
		}			
	}

	private void initializeFeatureType(EditableFeatureType editableFeatureType) throws DataException
	{		
		EditableFeatureAttributeDescriptor geometryType = editableFeatureType.add(destinationGeometryAttributeName, DataTypes.GEOMETRY);
		geometryType.setAllowNull(false);		
		geometryType.setGeometryType(Geometry.TYPES.POINT).setGeometrySubType(Geometry.SUBTYPES.GEOM2D);
		geometryType.setSRS(sourceStore.getDefaultFeatureType().getDefaultSRS());
		geometryType.setObjectClass(Geometry.class);
		editableFeatureType.setDefaultGeometryAttributeName(destinationGeometryAttributeName);

		editableFeatureType.add(AnnotationManager.TEXTVALUE_ATTRIBUTE_NAME, AnnotationDataTypes.TEXT, 256).setAllowNull(true);
		editableFeatureType.add(AnnotationManager.FONTTYPE_ATTRIBUTE_NAME, AnnotationDataTypes.FONTTYPE, 30).setAllowNull(true);
		editableFeatureType.add(AnnotationManager.FONTSTYLE_ATTRIBUTE_NAME, AnnotationDataTypes.FONTSTYLE, 30).setAllowNull(true);
		editableFeatureType.add(AnnotationManager.FONTCOLOR_ATTRIBUTE_NAME, AnnotationDataTypes.FONTCOLOR, 20).setAllowNull(true);
		editableFeatureType.add(AnnotationManager.FONTROTATION_ATTRIBUTE_NAME, AnnotationDataTypes.FONTROTATION, 5).setAllowNull(true);
		editableFeatureType.add(AnnotationManager.FONTHEGTH_ATTRIBUTE_NAME, AnnotationDataTypes.FONTHEIGHT, 5).setAllowNull(true);
	}

	private AttributeInserter createInserter(int attributePosition, Object defaultValue)
	{
		if (attributePosition > -1){
			return new StoreAttributeInserter(attributePosition);
		}else{
			return new DefaultAttributeInserter(defaultValue);
		}
	}

	private void copyFeatureStore(FeatureStore sourceStore,
			FeatureStore destinationStore, int textAttribute)
	throws AnnotationCreationException, DataException {		
		FeatureSet featureSet = null;
		DisposableIterator iterator = null;
		
		try{
			//Start the edition
			destinationStore.edit();

			//Copy data
			featureSet = sourceStore.getFeatureSet();
			iterator = featureSet.fastIterator();
			
			
			taskStatus.setRangeOfValues(0, featureSet.getSize());
	
			//Create the attribute inserter's
			AttributeInserter fontTypeAttributeInserter = createInserter(sourceFontTypeAttribute, manager.getDefaultFontType());		
			AttributeInserter fontStyleAttributeInserter = createInserter(sourceFontStyleAttribute, manager.getDefaultFontStyle());		
			AttributeInserter fontColorAttributeInserter = createInserter(sourceFontColorAttribute, manager.getDefaultFontColor());		
			AttributeInserter fontRotationAttributeInserter = createInserter(sourceRotationAttribute, manager.getDefaultFontRotation());		
			AttributeInserter fontHeigthAttributeInserter = createInserter(sourceHeigthAttribute, manager.getDefaultFontHeight());		
	
			Feature sourceFeature;
			long featureCount = 0;
			while (iterator.hasNext()) {			
				sourceFeature = (Feature) iterator.next();
	
				EditableFeature destinationFeature = destinationStore.createNewFeature().getEditable();
				try {
					destinationFeature.set(destinationGeometryAttributeName, annotationPositionCalculator.getAnnotationPosition(sourceFeature));
				} catch (AnnotationPositionCalculationException e) {
					LOG.error("Not possible to get the point for the geometry", e);				
				}
		        String s = (String) sourceFeature.get(textAttribute);
                if (StringUtils.isEmpty(s)) {
                    s = this.getManager().getDefaultTextValue();
                }
				destinationFeature.set(AnnotationManager.TEXTVALUE_ATTRIBUTE_NAME, s);
				destinationFeature.set(AnnotationManager.FONTTYPE_ATTRIBUTE_NAME, fontTypeAttributeInserter.getValue(sourceFeature));
				destinationFeature.set(AnnotationManager.FONTSTYLE_ATTRIBUTE_NAME, fontStyleAttributeInserter.getValue(sourceFeature));
				destinationFeature.set(AnnotationManager.FONTCOLOR_ATTRIBUTE_NAME, fontColorAttributeInserter.getValue(sourceFeature));
				destinationFeature.set(AnnotationManager.FONTROTATION_ATTRIBUTE_NAME, fontRotationAttributeInserter.getValue(sourceFeature));
				destinationFeature.set(AnnotationManager.FONTHEGTH_ATTRIBUTE_NAME, fontHeigthAttributeInserter.getValue(sourceFeature));
	
				destinationStore.insert(destinationFeature);
	    		featureCount++;  
	    		this.taskStatus.setCurValue(featureCount);
	    
	            if (this.taskStatus.isCancellationRequested()) {
	            	destinationStore.cancelEditing();
	            	this.taskStatus.cancel();
	                return;
	            }
			}
		}finally{

			//Finish the edition
			if(destinationStore.isEditing()){
				destinationStore.finishEditing();
			}
	
			//Dispose resources
			if(iterator != null){
				iterator.dispose();	
			}
			if(featureSet != null){
				featureSet.dispose();
			}
			
			if(this.taskStatus.isRunning()){
				this.taskStatus.terminate();
			}
	        //this.taskStatus.remove();
		}
	}

	private void copyLegend(FeatureStore destinationStore) throws ValidateDataParametersException, DataException, IOException {
		FilesystemServerExplorer filesystemServerExplorer = (FilesystemServerExplorer)destinationStore.getExplorer();
		File target = filesystemServerExplorer.getResourcePath(destinationStore, "gvslab");

		//Copy the template
		InputStream in = getClass().getClassLoader().getResourceAsStream(TEMPLATE_NAME);
		OutputStream out = null;

		out = new FileOutputStream(target);

		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) { 
			out.write(buf, 0, len); 
		} 
		in.close();
		out.close(); 		
	}

	private void checkAttribute(int index, int[] type) throws DataException{
		if (index >= sourceStore.getDefaultFeatureType().size()){
			throw new IllegalArgumentException("Attribute not found");
		}
		int datatype = sourceStore.getDefaultFeatureType().getAttributeDescriptor(index).getDataType().getType();
		
		boolean validType = false;
		if(type != null) {
			for (int i = 0; i < type.length; i++) {
				if(datatype == type[i]) {
					validType = true;
				}
			}
		}
		
		if (!validType) {
			throw new IllegalArgumentException("The Attribute has not have the fine type");
		}
	}


	public void setFontTypeAttribute(int index) throws DataException {
		checkAttribute(index, new int[]{AnnotationDataTypes.FONTTYPE});
		this.sourceFontTypeAttribute = index;		
	}

	public void setFontStyleAttribute(int index) throws DataException {
		checkAttribute(index, new int[]{AnnotationDataTypes.FONTSTYLE});
		this.sourceFontStyleAttribute = index;		
	}

	public void setFontColorAttribute(int index) throws DataException {
		checkAttribute(index, new int[]{AnnotationDataTypes.FONTCOLOR});
		this.sourceFontColorAttribute = index;		
	}

	public void setFontHeigthAttribute(int index) throws DataException {
		checkAttribute(index, new int[]{AnnotationDataTypes.FONTHEIGHT, DataTypes.INT, DataTypes.LONG, DataTypes.FLOAT});
		this.sourceHeigthAttribute = index;		
	}

	public void setFontRotationAttribute(int index) throws DataException {
		checkAttribute(index, new int[]{AnnotationDataTypes.FONTROTATION});
		this.sourceRotationAttribute = index;		
	}

	public void setAnnotationPositionCalculator(
			AnnotationPositionCalculator annotationPositionCalculator) {		
		this.annotationPositionCalculator = annotationPositionCalculator;		
	}

	public int getIndex(String attributeName) throws DataException{
		FeatureAttributeDescriptor featureAttributeDescriptor = sourceStore.getDefaultFeatureType().getAttributeDescriptor(attributeName);
		if (featureAttributeDescriptor != null){
			return featureAttributeDescriptor.getIndex();
		}
		return -1;
	}

	public void setFontColorAttribute(String attributeName) throws DataException {
		setFontColorAttribute(getIndex(attributeName));		
	}

	public void setFontHeigthAttribute(String attributeName) throws DataException {
		setFontHeigthAttribute(getIndex(attributeName));				
	}

	public void setFontRotationAttribute(String attributeName) throws DataException {
		setFontRotationAttribute(getIndex(attributeName));				
	}

	public void setFontStyleAttribute(String attributeName) throws DataException {
		setFontStyleAttribute(getIndex(attributeName));				
	}

	public void setFontTypeAttribute(String attributeName) throws DataException {
		setFontTypeAttribute(getIndex(attributeName));				
	}


	public FeatureStore getFeatureStore() {
		return sourceStore;
	}

	private interface AttributeInserter{
		public Object getValue(Feature feature);
	}


	private class StoreAttributeInserter implements AttributeInserter{
		private int attributePosition = -1;

		public StoreAttributeInserter(int attributePosition) {
			super();
			this.attributePosition = attributePosition;			
		}

		public Object getValue(Feature feature){
			return feature.get(attributePosition);
		}
	}

	private class DefaultAttributeInserter implements AttributeInserter{
		private Object defaultValue = null;

		public DefaultAttributeInserter(Object defaultValue) {
			super();	
			this.defaultValue = defaultValue;
		}

		public Object getValue(Feature feature){
			return defaultValue;
		}
	}

	public AnnotationCreationFinishAction getAnnotationCreationFinishAction() {
		return annotationCreationFinishAction;
	}


	public void setAnnotationCreationFinishAction(
			AnnotationCreationFinishAction annotationCreationFinishAction) {
		this.annotationCreationFinishAction = annotationCreationFinishAction;	
	}
}
