/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.annotation.swing.AnnotationWindowManager;
import org.gvsig.annotation.swing.JAnnotationPreferencesPanel;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Main executable class for testing the Annotation library.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class PreferencesMain {

    private static final Logger LOG = LoggerFactory.getLogger(PreferencesMain.class);

    private AnnotationManager annotationManager;
    private AnnotationSwingManager annotationSwingManager;
    private DataManager dataManager;

    public static void main(String args[]) throws Exception {
        new DefaultLibrariesInitializer().fullInitialize();
        PreferencesMain main = new PreferencesMain();
        main.show();
    }    
    
    @SuppressWarnings("serial")
    public void show() throws Exception {
        annotationManager = AnnotationLocator.getManager();
        annotationSwingManager = AnnotationSwingLocator.getSwingManager();
        dataManager = DALLocator.getDataManager();
        
        
        JAnnotationPreferencesPanel annotationPreferencesPanel = annotationSwingManager.createAnnotationPreferences();
        annotationSwingManager.getWindowManager().showPreferencesWindow(annotationPreferencesPanel, "Annotation Preferences. example", AnnotationWindowManager.MODE_DIALOG);
    }   
    
    /**
     * Returns an instance of the {@link AnnotationCreationService}.
     * 
     * @return a {@link AnnotationCreationService} instance
     * @throws Exception
     *             if there is any error creating the instance
     */
    protected AnnotationCreationService createService() throws Exception {
    	String sourceFileName = getClass().getClassLoader().getResource("org/gvsig/annotation/data/andalucia.shp").getFile();
    	DataStoreParameters sourceParameters = dataManager.createStoreParameters("Shape");
     	sourceParameters.setDynValue("shpfile", sourceFileName);
     	sourceParameters.setDynValue("crs", CRSFactory.getCRS("EPSG:23030"));
     	FeatureStore sourceStore = (FeatureStore) dataManager.openStore("Shape", sourceParameters);
    	return annotationManager.getAnnotationCreationService(sourceStore);
    }

}
