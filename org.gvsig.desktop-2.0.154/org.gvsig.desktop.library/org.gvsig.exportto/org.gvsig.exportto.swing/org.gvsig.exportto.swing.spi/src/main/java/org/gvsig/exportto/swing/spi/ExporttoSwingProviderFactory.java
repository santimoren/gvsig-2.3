/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ProviderFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public interface ExporttoSwingProviderFactory extends ProviderFactory {

    /**
     * Return if a provider supports a data type.
     * 
     * @param providerType
     *            the provider type. One of the constants defined in the
     *            {@link ExporttoSwingManager}.
     * @return
     *         if the provider supports a dataType.
     * @throws ServiceException
     */
    public boolean support(int providerType) throws ServiceException;

    /**
     * Returns a description of the provider.
     * 
     * @return a description for the provider
     */
    public String getDescription();

    /**
     * Returns a label or short description for the provider.
     * 
     * @return a label or short description for the provider
     */
    public String getLabel();

    /**
     * If the provider this factory creates is enabled. This option might
     * be used as a way to show or not some providers to the user.
     * 
     * @return if the provider is enabled
     */
    public boolean isEnabled();

    /**
     * Enable or disable this provider factory.
     * 
     * @param value
     *            if to enable or disable the factory
     */
    public void setEnabled(boolean value);
}
