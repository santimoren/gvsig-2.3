/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import javax.swing.JPanel;
import org.gvsig.tools.swing.api.Component;

/**
 * The panels returned by a {@link ExporttoSwingProvider} have to
 * implement this interface.
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public interface ExporttoSwingProviderPanel extends Component {

    /**
     * @return
     *         the panel title that will be displayed with
     *         this panel.
     */
    public String getPanelTitle();

    /**
     * Checks if the panel is valid. If is not valid and throws
     * an exception, the exception message is displayed and the
     * user can not continue. If the return value is false,
     * the user can not continue but any error message is shown
     * to the user.
     * 
     * @return
     *         if all the obligatory fields of the panel have been
     *         filled.
     * @throws ExporttoPanelValidationException
     *             if there is any erro that has to be shown to the user.
     */
    public boolean isValidPanel() throws ExporttoPanelValidationException;
    
    public void enterPanel();

}
