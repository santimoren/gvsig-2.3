/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import java.util.List;

import javax.print.attribute.standard.OutputDeviceAssigned;

import org.cresques.cts.IProjection;

import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.preferences.ExporttoSwingPreferencesComponent;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ProviderManager;

/**
 * Responsible of the management of the providers business logic.
 * 
 * @see ExporttoSwingProvider
 * @author gvSIG Team
 * @version $Id$
 */
public interface ExporttoSwingProviderManager extends ProviderManager {

    /**
     * @return
     *         the list of providers that has been registered.
     * @deprecated use {@link #getProviderFactories()} instead.
     */
    public List<String> getProviderNames();

    /**
     * Returns a list of providers that support all the
     * provider types.
     * 
     * @param a
     *            list of provider types, that are constants
     *            defined in the {@link ExporttoSwingManager}.
     * @return
     *         the list of providers that has been registered.
     * @deprecated use {@link #getProviderFactories(int[])} instead.
     */
    public List<String> getProviderNames(int[] providerTypes);

    /**
     * Returns all the available {@link ExporttoSwingProviderFactory}s.
     * 
     * @return all the available {@link ExporttoSwingProviderFactory}s
     */
    public List<ExporttoSwingProviderFactory> getProviderFactories();

    /**
     * Returns the {@link ExporttoSwingProviderFactory}s that support the
     * given factory types.
     * 
     * @see ExporttoSwingManager#VECTORIAL_TABLE_WITH_GEOMETRY
     * @see ExporttoSwingManager#VECTORIAL_TABLE_WITHOUT_GEOMETRY
     * @param providerTypes
     *            the types of factories
     * @return the list of factories for the given factory types
     */
    public List<ExporttoSwingProviderFactory> getProviderFactories(
        int[] providerTypes);

    /**
     * Returns the description for a concrete provider
     * 
     * @param providerName
     *            the exportto provider.
     * @return
     *         the description of the provider.
     */
    public String getDescription(String providerName);

    /**
     * Return if a provider supports a data type.
     * 
     * @param providerName
     *            the provider name.
     * @param providerType
     *            the provider type. One of the constants defined in the
     *            {@link ExporttoSwingManager}.
     * @return
     *         if the provider supports a dataType.
     * @throws ServiceException
     */
    public boolean support(String providerName, int providerType)
        throws ServiceException;

    /**
     * Creates a {@link ExporttoSwingProvider} that is used to export a
     * {@link FeatureSet}.
     * 
     * @param providerName
     *            the name of the provider that has to be created.
     * @param featureStore
     *            the source feature store. It can be used to fix some
     *            parameters like the {@link FeatureType} of the destination
     *            {@link FeatureStore}.
     * @param projection
     *            the projection of the {@link OutputDeviceAssigned} store.
     * @return
     *         a provider that has to be used to export a {@link FeatureSet}.
     */
    public ExporttoSwingProvider createExporttoSwingProvider(
        String providerName, FeatureStore featureStore, IProjection projection)
        throws ServiceException;
    
    
    /**
     * Creates a {@link ExporttoSwingProvider} that is used to export a
     * {@link FeatureSet}.
     * 
     * @param providerName
     *            the name of the provider that has to be created.
     * @param vlayer
     *            the source vector layer
     * @return
     *         a provider that has to be used to export a {@link FeatureSet}.
     */
    public ExporttoSwingProvider createExporttoSwingProvider(
        String providerName, FLyrVect vlayer)
        throws ServiceException;    

    /**
     * Returns if the provider whose factory is provided is enabled.
     * 
     * @param factory
     *            of the provider to check
     * @return if the provider whose factory is provided is enabled
     */
    public boolean isProviderEnabled(ExporttoSwingProviderFactory factory);

    /**
     * Enables or disables an exportto provider.
     * 
     * @param factory
     *            of the provider to enable or disable
     * @param value
     *            if the provider must be enabled or disabled
     */
    public void enableProvider(ExporttoSwingProviderFactory factory,
        boolean value);

    /**
     * Returns the provider factory with the given name.
     * 
     * @param name
     *            of the provider
     * @return the provider factory
     * @throws ServiceException
     *             if there is an error getting the provider factory
     */
    public ExporttoSwingProviderFactory getExporttoSwingProviderFactory(
        String name) throws ServiceException;

    /**
     * Creates a preferences component to manage the export to properties.
     * 
     * @return a preferences component
     */
    public ExporttoSwingPreferencesComponent createExporttoSwingProvidersPreferences();
}
