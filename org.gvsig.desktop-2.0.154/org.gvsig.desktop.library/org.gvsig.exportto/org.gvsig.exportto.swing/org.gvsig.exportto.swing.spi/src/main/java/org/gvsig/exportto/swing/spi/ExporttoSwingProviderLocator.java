/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * This locator is the entry point for the Exporto library SPI, providing
 * access to all Exporto provider services through the
 * {@link ExporttoSwingProviderManager} .
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class ExporttoSwingProviderLocator extends BaseLocator {

    /**
     * Exporto provider manager name.
     */
    public static final String MANAGER_NAME = "Exporto.provider.manager";

    /**
     * Exporto provider manager description.
     */
    public static final String MANAGER_DESCRIPTION = "Exporto Manager";

    private static final String LOCATOR_NAME = "Exporto.provider.locator";

    /**
     * Unique instance.
     */
    private static final ExporttoSwingProviderLocator INSTANCE =
        new ExporttoSwingProviderLocator();

    /**
     * Return the singleton instance.
     * 
     * @return the singleton instance
     */
    public static ExporttoSwingProviderLocator getInstance() {
        return INSTANCE;
    }

    /**
     * Return the Locator's name
     * 
     * @return a String with the Locator's name
     */
    public String getLocatorName() {
        return LOCATOR_NAME;
    }

    /**
     * Return a reference to the {@link ExporttoSwingProviderManager}
     * 
     * @return a reference to the {@link ExporttoSwingProviderManager}
     * @throws LocatorException
     *             if there is no access to the class or the class cannot be
     *             instantiated
     * @see Locator#get(String)
     */
    public static ExporttoSwingProviderManager getManager()
        throws LocatorException {
        return (ExporttoSwingProviderManager) getInstance().get(MANAGER_NAME);
    }

    /**
     * Registers the Class implementing the {@link ExporttoSwingProviderManager}
     * interface.
     * 
     * @param clazz
     *            implementing the {@link ExporttoSwingProviderManager}
     *            interface
     */
    public static void registerManager(
        Class<? extends ExporttoSwingProviderManager> clazz) {
        getInstance().register(MANAGER_NAME, MANAGER_DESCRIPTION, clazz);
    }

}
