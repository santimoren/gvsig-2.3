/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import org.gvsig.exportto.ExporttoException;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExporttoPanelValidationException extends ExporttoException {

    private static final long serialVersionUID = 236599587467526356L;

    private static final String MESSAGE = "The panel is not valid";

    private static final String KEY = "_ExporttoPanelValidationException";

    /**
     * @see BaseException#BaseException(String, Throwable, String, long).
     * @param message
     *            the default messageFormat to describe the exception
     * @param cause
     *            the original cause of the exception
     */
    public ExporttoPanelValidationException(String message, Throwable cause) {
        super(message, cause, KEY, serialVersionUID);
    }

    /**
     * @see BaseException#BaseException(String, Throwable, String, long).
     * @param message
     *            the default messageFormat to describe the exception
     */
    public ExporttoPanelValidationException(String message) {
        super(message, KEY, serialVersionUID);
    }

    public ExporttoPanelValidationException() {
        super(MESSAGE, KEY, serialVersionUID);
    }
}
