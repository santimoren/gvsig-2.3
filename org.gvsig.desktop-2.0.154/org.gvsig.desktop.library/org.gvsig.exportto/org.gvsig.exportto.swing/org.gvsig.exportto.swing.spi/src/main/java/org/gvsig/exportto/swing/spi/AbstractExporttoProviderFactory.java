/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;

/**
 * Abstract parent class for {@link ExporttoSwingProviderFactory} implementation
 * classes.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractExporttoProviderFactory implements
    ExporttoSwingProviderFactory {

    protected static final String PARAMETER_FEATURESTORE = "FeatureStore";
    protected static final String PARAMETER_PROJECTION = "Projection";
    
    protected DynClass parametersDefinition;
    protected int[] providerTypes = null;

    /**
     * Constructor.
     * 
     * @param providerTypes
     *            supported provider types
     */
    public AbstractExporttoProviderFactory(int[] providerTypes) {
        this.providerTypes = providerTypes;
    }

    public DynObject createParameters() {
        return ToolsLocator.getDynObjectManager().createDynObject(
            parametersDefinition);
    }

    public void initialize() {
        parametersDefinition =
            ToolsLocator.getDynObjectManager().createDynClass(getName(),
                getDescription());
        parametersDefinition.addDynField(PARAMETER_FEATURESTORE)
            .setType(DataTypes.OBJECT).setMandatory(true)
            .setClassOfValue(FeatureStore.class);
        parametersDefinition.addDynField(PARAMETER_PROJECTION)
            .setType(DataTypes.CRS).setMandatory(false);
    }

    public boolean support(int providerType) throws ServiceException {
        for (int i = 0; i < providerTypes.length; i++) {
            if (providerTypes[i] == providerType) {
                return true;
            }
        }
        return false;
    }

    public String getDescription() {
        return Messages.getText("general_exporter_description",
            new String[] { getName() });
    }

    public String getLabel() {
        return Messages.getText("general_exporter_label",
            new String[] { getName() });
    }

    @Override
    public String toString() {
        return getLabel();
    }

    public boolean isEnabled() {
        return ExporttoSwingProviderLocator.getManager().isProviderEnabled(this);
    }

    public void setEnabled(boolean value) {
        ExporttoSwingProviderLocator.getManager().enableProvider(this, value);
    }

}
