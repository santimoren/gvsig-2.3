/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.spi;

import org.cresques.cts.IProjection;
import org.gvsig.exportto.ExporttoService;

/**
 * A Exportto provider.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public interface ExporttoSwingProvider {

    /**
     * @return
     *         the number of panels that are used por this provider.
     */
    public int getPanelCount();

    /**
     * A {@link ExporttoSwingProvider} are composed of a set of panels. This
     * method
     * is used to retrieve all these panels.
     *
     * @param index
     *            position of the panel to retrieve.
     * @return
     *         the panel that is in a concrete position.
     */
    public ExporttoSwingProviderPanel getPanelAt(int index);

    /**
     * @return a {@link ExporttoService} that has been initialized this
     *         the values typed in the panels returned by this service.
     */
    public ExporttoService createExporttoService();

    /**
     * Sets the target projection to which should be exported
     * @param targetProjection
     */
    public void setTargetProjection(IProjection targetProjection);

    /**
     * Informs if it needs to ask for a target projection,
     * or if it is not needed or provided through its own wizard panel.
     * @return
     */
    public boolean needsPanelTargetProjection();

}
