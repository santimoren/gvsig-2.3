/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing;

import javax.swing.JPanel;
import org.cresques.cts.IProjection;

import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

/**
 * This class is responsible of the management of the library's swing user
 * interface. It is the swing library's main entry point, and provides all the
 * services to manage library swing components.
 * 
 * @see ExporttoWindowManager
 * @see JExporttoServicePanel
 * @author gvSIG team
 * @version $Id$
 */
public interface ExporttoSwingManager {

    public static final int VECTORIAL_TABLE_WITH_GEOMETRY = 0;
    public static final int VECTORIAL_TABLE_WITHOUT_GEOMETRY = 1;

    /**
     * Returns the panel associated to a {@link ExporttoService}.
     * 
     * @param featureStore
     *            the source feature store that is going to be copied.
     * @param projection
     *            the destination projection for the output.
     * @return a {@link JExporttoServicePanel} with the panel of the
     *         {@link ExporttoService}.
     * @see JExporttoServicePanel
     * @see ExporttoService
     */
    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection);


    /**
     * Returns the panel associated to a {@link ExporttoService}.
     *
     * @param vectorLayer The source vector layer
     * 
     * @param exporttoServiceFinishAction
     *            the listener to manage the panel events.
     * @param providerTypes
     *            a set of the types that have to be displayed.
     * 
     * @return a {@link JExporttoServicePanel} with the panel of the
     *         {@link ExporttoService}.
     * @see JExporttoServicePanel
     * @see ExporttoService
     */
    public JExporttoServicePanel createExportto(FLyrVect vectorLayer,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes);
    
    /**
     * Returns the panel associated to a {@link ExporttoService}.
     * 
     * @param featureStore
     *            the source feature store that is going to be copied.
     * @param projection
     *            the destination projection for the output.
     * @param exporttoServiceFinishAction
     *            the listener to manage the panel events.
     * @return a {@link JExporttoServicePanel} with the panel of the
     *         {@link ExporttoService}
     * @see JExporttoServicePanel
     * @see ExporttoService
     */
    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection,
        ExporttoServiceFinishAction exporttoServiceFinishAction);

    /**
     * Returns the panel associated to a {@link ExporttoService}.
     * 
     * @param featureStore
     *            the source feature store that is going to be copied.
     * @param projection
     *            the destination projection for the output.
     * @param exporttoServiceFinishAction
     *            the listener to manage the panel events.
     * @param providerTypes
     *            a set of the types that have to be displayed.
     * @return a {@link JExporttoServicePanel} with the panel of the
     *         {@link ExporttoService}
     * @see JExporttoServicePanel
     * @see ExporttoService
     */
    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes);

    /**
     * Returns the {@link ExporttoManager}.
     * 
     * @return {@link ExporttoManager}
     * @see {@link ExporttoManager}
     */
    public ExporttoManager getManager();

    /**
     * Returns the translation of a string.
     * 
     * @param key
     *            String to translate
     * @return a String with the translation of the string passed by parameter
     */
    public String getTranslation(String key);

    /**
     * Registers a new instance of a WindowManager which provides services to
     * the management of the application windows.
     * 
     * @param manager
     *            {@link ExporttoWindowManager} to register in the
     *            ScriptingUIManager.
     * @see ExporttoWindowManager
     */
    public void registerWindowManager(ExporttoWindowManager manager);

    /**
     * Returns the {@link ExporttoWindowManager}.
     * 
     * @return {@link ExporttoWindowManager}
     * @see {@link ExporttoWindowManager}
     */
    public ExporttoWindowManager getWindowManager();
    
    /**
     * Show a message dialog.
     * 
     * If feature is specified, it is dumped in a humman readable
     * maner to the user.
     * 
     * @param title
     * @param header
     * @param html text of message in HTML
     * @param feature 
     */
    public void showMessage(String title,
            String header, String html, Feature feature);

    /**
     * Show a error message dialog associated to the exception.
     * 
     * If feature is specified, it is dumped in a humman readable
     * maner to the user.
     * 
     * @param title
     * @param header
     * @param ex
     * @param feature 
     */
    public void showMessage(String title,
            String header, Exception ex, Feature feature);

}
