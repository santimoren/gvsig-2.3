/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.preferences;

import java.util.Set;

import org.gvsig.tools.swing.api.Component;

/**
 * API for the Export to swing preferences component panel.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public interface ExporttoSwingPreferencesComponent extends Component {

    /**
     * Returns the providers disabled by the user.
     * 
     * @return the providers disabled by the user
     */
    Set<?> getDisabledProviders();

    /**
     * Undoes all the user changes in the preferences.
     */
    void initializeDefaults();

    /**
     * Is the user has made any change in the preferences.
     * 
     * @return the user has made any change
     */
    boolean isValueChanged();

    /**
     * Tells the component the changes made by the user to the properties
     * had been performed.
     */
    void setChangesApplied();
}
