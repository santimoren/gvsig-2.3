/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.file;

import org.cresques.cts.IProjection;

import org.gvsig.exportto.swing.prov.file.panel.SelectFileOptionPanel;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * Exporto provider which gets Exporto from a file.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractExporttoFileProvider extends AbstractProvider
    implements ExporttoSwingProvider {

    protected SelectFileOptionPanel selectFileOptionPanel = null;

    protected FeatureStore featureStore;
    protected IProjection projection;

    /**
     * Constructor.
     *
     * @param providerServices
     *            the services for the provider
     * @param file
     *            to get the Exporto from
     */
    public AbstractExporttoFileProvider(ProviderServices providerServices,
        FeatureStore featureStore, IProjection projection) {
        this(providerServices, featureStore, projection,
            new SelectFileOptionPanel());
    }

    public AbstractExporttoFileProvider(ProviderServices providerServices,
        FeatureStore featureStore, IProjection projection,
        SelectFileOptionPanel selectFileOptionPanel) {
        super(providerServices);
        this.featureStore = featureStore;
        this.projection = projection;

        this.selectFileOptionPanel = selectFileOptionPanel;
    }

    public int getPanelCount() {
        return 1;
    }

    public ExporttoSwingProviderPanel getPanelAt(int index) {
        switch (index) {
        case 0:
            return selectFileOptionPanel;
        }
        return null;
    }

    /**
     * Sets the target projection to which should be exported
     * @param targetProjection
     */
    public void setTargetProjection(IProjection targetProjection){
        this.projection=targetProjection;
    }

    /**
     * Informs if it needs to ask for a target projection,
     * or if it is not needed or provided through its own wizard panel.
     * @return
     */
    public boolean needsPanelTargetProjection(){
        return true;
    }
}
