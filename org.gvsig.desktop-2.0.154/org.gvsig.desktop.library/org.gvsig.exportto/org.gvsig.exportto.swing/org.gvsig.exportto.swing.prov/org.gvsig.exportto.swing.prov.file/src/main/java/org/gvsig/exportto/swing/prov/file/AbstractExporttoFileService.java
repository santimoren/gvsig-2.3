/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.file;

import java.io.File;

import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.tools.task.TaskStatus;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public abstract class AbstractExporttoFileService implements ExporttoService {

    protected static final ExporttoManager EXPORTTO_MANAGER = ExporttoLocator
        .getManager();
    protected File file;
    protected FeatureStore featureStore;
    protected ExporttoService exporttoService;

    private NewFeatureStoreParameters newFeatureStoreParameters;
    private ExporttoServiceFinishAction exporttoServiceFinishAction;

    public AbstractExporttoFileService(File file, FeatureStore featureStore) {
        super();
        this.featureStore = featureStore;
        this.file = file;
    }

    public void open() throws ExporttoServiceException {
        String path = file.getAbsolutePath();

        String extension = "." + getFileExtension();

        if (!(path.toLowerCase().endsWith(extension))) {
            path = path + extension;
        }

        File newFile = new File(path);
        DataManager dataManager = DALLocator.getDataManager();

        FilesystemServerExplorerParameters explorerParams;
        try {
            explorerParams =
                (FilesystemServerExplorerParameters) dataManager
                    .createServerExplorerParameters(FilesystemServerExplorer.NAME);
        } catch (InitializeException e) {
            throw new ExporttoServiceException(e);
        } catch (ProviderNotRegisteredException e) {
            throw new ExporttoServiceException(e);
        }
        explorerParams.setRoot(newFile.getParent());

        FilesystemServerExplorer explorer;
        try {
            explorer =
                (FilesystemServerExplorer) dataManager.openServerExplorer(
                    "FilesystemExplorer", explorerParams);
        } catch (ValidateDataParametersException e) {
            throw new ExporttoServiceException(e);
        } catch (InitializeException e) {
            throw new ExporttoServiceException(e);
        } catch (ProviderNotRegisteredException e) {
            throw new ExporttoServiceException(e);
        }

        try {
            newFeatureStoreParameters =
                (NewFeatureStoreParameters) explorer.getAddParameters(newFile);
            addParameters(newFeatureStoreParameters);

            exporttoService =
                EXPORTTO_MANAGER.getExporttoService(explorer,
                    newFeatureStoreParameters);
        } catch (DataException e) {
            throw new ExporttoServiceException(e);
        }
    }

    public abstract void addParameters(
        NewFeatureStoreParameters newFeatureStoreParameters)
        throws DataException;

    public abstract String getFileExtension();

    public void export(FeatureSet featureSet) throws ExporttoServiceException {
        exporttoService.export(featureSet);
        if (exporttoServiceFinishAction != null) {
            exporttoServiceFinishAction.finished(file.getName(),
                newFeatureStoreParameters);
        }
    }

    public void setFinishAction(
        ExporttoServiceFinishAction exporttoServiceFinishAction) {
        this.exporttoServiceFinishAction = exporttoServiceFinishAction;
    }

    public TaskStatus getTaskStatus() {
        return exporttoService.getTaskStatus();
    }

    public boolean isCancellationRequested() {
        return exporttoService.isCancellationRequested();
    }

    public void cancelRequest() {
        exporttoService.cancelRequest();
    }
}
