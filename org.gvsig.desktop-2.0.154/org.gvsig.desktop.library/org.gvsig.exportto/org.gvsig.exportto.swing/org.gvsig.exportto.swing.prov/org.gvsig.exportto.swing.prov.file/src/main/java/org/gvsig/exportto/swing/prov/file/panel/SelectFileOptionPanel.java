/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.file.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import javax.swing.JComponent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.i18n.Messages;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class SelectFileOptionPanel extends JPanel implements ExporttoSwingProviderPanel {

    private static final long serialVersionUID = -7417782279157857962L;
    private JPanel optionsPanel;

    private static final ExporttoSwingManager EXPORTTO_SWING_MANAGER =
        ExporttoSwingLocator.getSwingManager();

    private org.gvsig.gui.beans.wizard.panel.SelectFileOptionPanel selectFileOptionPanel;

    /**
     * @param fileText
     */
    public SelectFileOptionPanel() {
        this(null);
    }

    public SelectFileOptionPanel(JPanel optionsPanel) {
        super();
        this.setLayout(new BorderLayout());
        selectFileOptionPanel =
            new org.gvsig.gui.beans.wizard.panel.SelectFileOptionPanel(null);
        add(selectFileOptionPanel, BorderLayout.NORTH);
        if (optionsPanel != null) {
            optionsPanel.setBorder(javax.swing.BorderFactory
                .createCompoundBorder(javax.swing.BorderFactory
                    .createTitledBorder(Messages.getText("options")),
                    javax.swing.BorderFactory.createEmptyBorder(10, 5, 5, 5)));
            add(optionsPanel, BorderLayout.CENTER);
            this.optionsPanel = optionsPanel;
        }
    }

    public String getPanelTitle() {
        return EXPORTTO_SWING_MANAGER.getTranslation("Seleccionar_fichero");
    }

    public File getSelectedFile() {
        return selectFileOptionPanel.getSelectedFile();
    }
    
    public void enterPanel() {
        // Default do nothing
    }
    
    public boolean isValidPanel() throws ExporttoPanelValidationException {
        File file = selectFileOptionPanel.getSelectedFile();
        if ((file == null) || (file.equals(""))) {
            throw new ExporttoPanelValidationException(
                EXPORTTO_SWING_MANAGER.getTranslation("_File_cannot_be_empty"));
        }
        if (file.exists()) {
            int resp =
                JOptionPane
                    .showConfirmDialog(
                        (Component) selectFileOptionPanel,
                        EXPORTTO_SWING_MANAGER
                            .getTranslation("fichero_ya_existe_seguro_desea_guardarlo"),
                        EXPORTTO_SWING_MANAGER.getTranslation("guardar"),
                        JOptionPane.YES_NO_OPTION);
            if (resp == JOptionPane.NO_OPTION) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the optionsPanel
     */
    public JPanel getOptionsPanel() {
        return optionsPanel;
    }

    public JComponent asJComponent() {
        return this;
    }
}
