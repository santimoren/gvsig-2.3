/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.dbf;

import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.swing.prov.dbf.panel.ExporttoDBFPanel;
import org.gvsig.exportto.swing.prov.file.AbstractExporttoFileProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.service.spi.ProviderServices;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */

/**
 * Exporto provider which gets Exporto from a file.
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExporttoDBFProvider extends AbstractExporttoFileProvider implements
    ExporttoSwingProvider {

    private String encoding;

    public ExporttoDBFProvider(ProviderServices providerServices,
        FeatureStore featureStore) {
        super(providerServices, featureStore, null, new ExporttoDBFPanel());
    }

    @Override
    public ExporttoService createExporttoService() {
        return new ExporttoDBFService(selectFileOptionPanel.getSelectedFile(),
            featureStore,
            ((ExporttoDBFPanel) selectFileOptionPanel).getEncoding());
    }
    
    @Override
    public boolean needsPanelTargetProjection(){
        return false;
    }
}
