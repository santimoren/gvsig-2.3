/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.dbf.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.i18n.Messages;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class EncodingPanel extends JPanel {

    private JLabel encodingLabel;
    private JComboBox encodingCombo;

    public EncodingPanel() {
        super();
        initComponents();
        initEncodingCombo();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());

        java.awt.GridBagConstraints gridBagConstraints;

        JPanel northPanel = new JPanel();
        encodingLabel = new JLabel();
        encodingCombo = new JComboBox();

        northPanel.setLayout(new GridBagLayout());

        encodingLabel.setText(Messages.getText("encoding ") + ": ");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        northPanel.add(encodingLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        northPanel.add(encodingCombo, gridBagConstraints);

        add(northPanel, BorderLayout.NORTH);
    }

    private void initEncodingCombo() {
        MetadataManager metadataManager = MetadataLocator.getMetadataManager();
        DynStruct dynStruct = metadataManager.getDefinition("DBF");
        DynField dynField = dynStruct.getDynField("Encoding");
        DynObjectValueItem[] dynObjectValueItem = dynField.getAvailableValues();
        for (int i = 0; i < dynObjectValueItem.length; i++) {
            encodingCombo.addItem(dynObjectValueItem[i].getValue());
        }
    }

    /**
     * @return
     */
    public String getEncoding() {
        String encoding = (String) encodingCombo.getSelectedItem();
        if ("".equals(encoding)) {
            return null;
        }
        return encoding;
    }
}
