

package org.gvsig.exportto.swing.prov.jdbc.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;


public class PostCreatingStatementPanel extends PostCreatingStatementPanelLayout implements ExporttoSwingProviderPanel {
    private static final long serialVersionUID = 7106687071248901180L;

    private ExporttoJDBCOptions provider;

    public PostCreatingStatementPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    private void initComponents() {
        this.chkUsePostCreatingStatement.setEnabled(true);
        this.chkUsePostCreatingStatement.setSelected(false);
        this.txtPostCreatingStatement.setText("");
        this.txtPostCreatingStatement.setEnabled(false);
        this.chkUsePostCreatingStatement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                doChangeUsePostCreatingStatement();
            }
        });
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_Post_creating_statement_header")+"</html>");
        this.chkUsePostCreatingStatement.setText(i18nManager.getTranslation("_Introducir_sentencia_a_ejecutar_tras_la_creacion_de_la_tabla"));
    }

    protected void doChangeUsePostCreatingStatement() {
        this.txtPostCreatingStatement.setEnabled(this.chkUsePostCreatingStatement.isSelected());
    }
    
    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_Post_creating_statement");    
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }
    
    public void enterPanel() {
        if( this.provider.canCreatetable() ) {
            this.chkUsePostCreatingStatement.setEnabled(true);
        } else {
            this.chkUsePostCreatingStatement.setEnabled(false);
        }
    }
    
    public JComponent asJComponent() {
        return this;
    }

    public String getPostCreatingStatement() {
        if( this.chkUsePostCreatingStatement.isEnabled() ) {
            String stmt = this.txtPostCreatingStatement.getText();
            if( StringUtils.isBlank(stmt) ) {
                return null;
            }
            return stmt.trim();
        }
        return null;
    }
}
