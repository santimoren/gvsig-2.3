package org.gvsig.exportto.swing.prov.jdbc.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import org.gvsig.exportto.swing.prov.jdbc.ExporrtoJDBCService;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckGeometriesPanel extends CheckGeometriesPanelLayout implements ExporttoSwingProviderPanel {

    private final static Logger logger = LoggerFactory.getLogger(CheckGeometriesPanel.class);
    
    private final ExporttoJDBCOptions provider;
    
    private final CheckAction[] checkActions = {
        new CheckAction("_Set_geometry_to_null", ExporrtoJDBCService.ACTION_SET_GEOMETRY_TO_NULL),
        new CheckAction("_Skip_entire_feature", ExporrtoJDBCService.ACTION_SKIP_FEATURE),
        new CheckAction("_Abort_process", ExporrtoJDBCService.ACTION_ABORT)
    };
    
    private static class CheckAction {

        private final String label;
        private final int code;

        public CheckAction(String label, int code) {
            this.code = code;
            this.label = ToolsLocator.getI18nManager().getTranslation(label);
        }
        @Override
        public String toString() {
            return this.label;
        }
        public int getCode() {
            return this.code;
        }
    }
        
    public CheckGeometriesPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }
    
    private void initComponents() {
        this.rdbCheckIsCorrupt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               onChangeRadioButtons();
            }
        });
        this.rdbCheckIsValid.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               onChangeRadioButtons();
            }
        });
        this.rdbCheckNone.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               onChangeRadioButtons();
            }
        });
        for (CheckAction checkAction : checkActions) {
            this.cboActionIsCorrupt.addItem(checkAction);
            this.cboActionIsValid.addItem(checkAction);            
        }
        this.rdbCheckNone.setSelected(true);
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
                    
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_check_geometries_before_export_header")+"</html>");
        this.rdbCheckNone.setText(i18nManager.getTranslation("_No_realizar_ninguna_comprobacion_sobre_las_geometrias"));
        this.rdbCheckIsCorrupt.setText(i18nManager.getTranslation("_Comprobar_que_la_geometria_no_este_corrupta"));
        this.rdbCheckIsValid.setText(i18nManager.getTranslation("_Comprobar_la_validez_de_las_geometrias_antes_de_insertarlas"));
        this.lblCheckIsCorrupt.setText(i18nManager.getTranslation("_Cuando_la_geometria_este_corrupta"));
        this.lblCheckIsvalid.setText(i18nManager.getTranslation("_Cuando_la_geometria_no_sea_valida"));
        this.chkTryFixGeometry.setText(i18nManager.getTranslation("_Intentar_reparar_la_geometria"));
    }
    
    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_check_geometries_before_export");    
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }
    
    public void enterPanel() {
        // Default do nothing
    }
    
    public JComponent asJComponent() {
        return this;
    }

    public int getGeometryChecks() {
        if( this.rdbCheckIsCorrupt.isSelected() ) {
            return ExporrtoJDBCService.CHECK_IF_CORRUPT;
        } else if( this.rdbCheckIsValid.isSelected()) {
            return ExporrtoJDBCService.CHECK_IF_VALID;
        } else {
            return ExporrtoJDBCService.CHECK_NONE;
        }
    }

    public int getGeometryChecksAction() {
        try {
            if( this.rdbCheckIsCorrupt.isSelected() ) {
                CheckAction checkAction = (CheckAction)(this.cboActionIsCorrupt.getSelectedItem());
                return checkAction.getCode();
            } else if( this.rdbCheckIsValid.isSelected()) {
                CheckAction checkAction = (CheckAction)(this.cboActionIsValid.getSelectedItem());
                return checkAction.getCode();
            } else {
                return ExporrtoJDBCService.ACTION_ABORT;
            }
        } catch(Exception ex) {
            logger.warn("Can't get action for geometry check in export to JDBC.",ex);
            return ExporrtoJDBCService.ACTION_ABORT;
        }
    }

    public boolean getTryToFixGeometry() {
        return this.chkTryFixGeometry.isSelected();
    }
    
    private void onChangeRadioButtons() {
        if( this.rdbCheckNone.isSelected() ) {
            this.cboActionIsCorrupt.setEnabled(false);
            this.cboActionIsValid.setEnabled(false);
            this.chkTryFixGeometry.setEnabled(false);
        } else if( this.rdbCheckIsCorrupt.isSelected() ) {
            this.cboActionIsCorrupt.setEnabled(true);
            this.cboActionIsValid.setEnabled(false);
            this.chkTryFixGeometry.setEnabled(false);
        } else if( this.rdbCheckIsValid.isSelected()) {
            this.cboActionIsCorrupt.setEnabled(false);
            this.cboActionIsValid.setEnabled(true);
            this.chkTryFixGeometry.setEnabled(true);
        }
    }
}
