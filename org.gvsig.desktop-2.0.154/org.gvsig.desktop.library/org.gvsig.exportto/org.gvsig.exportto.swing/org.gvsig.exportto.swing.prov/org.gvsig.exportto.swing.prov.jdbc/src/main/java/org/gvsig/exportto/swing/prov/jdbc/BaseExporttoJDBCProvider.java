/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import org.cresques.cts.IProjection;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.swing.prov.jdbc.panel.CheckGeometriesPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.GeometryIndexPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.IdentifiersOptionsPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.JDBCConnectionPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.UpdateTableStatisticsPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.PermissionsPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.PostCreatingStatementPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.SelectPkPanel;
import org.gvsig.exportto.swing.prov.jdbc.panel.SelectTableNamePanel;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorer;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exporto provider which gets Exporto from a file.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class BaseExporttoJDBCProvider extends AbstractProvider
        implements ExporttoJDBCOptions, ExporttoSwingProvider {

    private static Logger logger = LoggerFactory.getLogger(
            BaseExporttoJDBCProvider.class);

    protected List<ExporttoSwingProviderPanel> panels = new ArrayList<ExporttoSwingProviderPanel>();

    protected FeatureStore sourceStore;
    protected IProjection projection;
    private final JDBCConnectionPanel connectionPanel;
    private final SelectPkPanel selectPkPanel;
    private final SelectTableNamePanel selectTableNamePanel;
    private final CheckGeometriesPanel checkGeometriesPanel;
    private final IdentifiersOptionsPanel identifiersOptionsPanel;

    private final GeometryIndexPanel geometryIndexPanel;
    private final UpdateTableStatisticsPanel updateTableStatistics;
    private final PermissionsPanel permissionsPanel;
    private final PostCreatingStatementPanel postCreatingStatementPanel;

    private String storeName = null;

    public BaseExporttoJDBCProvider(ProviderServices providerServices,
            FeatureStore sourceStore, IProjection projection) {
        super(providerServices);
        this.sourceStore = sourceStore;
        this.projection = projection;

        FeatureType ftype = null;
        try {
            ftype = sourceStore.getDefaultFeatureType();
        } catch (Exception exc) {
            logger.warn("Can't retrieve the feature type to use in the export to JDBC panel.", exc);

        }
        this.connectionPanel = new JDBCConnectionPanel(this);
        this.selectTableNamePanel = new SelectTableNamePanel(this);
        this.selectPkPanel = new SelectPkPanel(this);
        this.checkGeometriesPanel = new CheckGeometriesPanel(this);
        this.identifiersOptionsPanel = new IdentifiersOptionsPanel(this);

        this.geometryIndexPanel = new GeometryIndexPanel(this);
        this.updateTableStatistics = new UpdateTableStatisticsPanel(this);
        this.permissionsPanel = new PermissionsPanel(this);
        this.postCreatingStatementPanel = new PostCreatingStatementPanel(this);

        this.panels.add(this.identifiersOptionsPanel);
        this.panels.add(this.connectionPanel);
        this.panels.add(this.selectTableNamePanel);
        this.panels.add(this.selectPkPanel);
        this.panels.add(this.geometryIndexPanel);
        this.panels.add(this.checkGeometriesPanel);

        this.panels.add(this.permissionsPanel);
        this.panels.add(this.updateTableStatistics);
        this.panels.add(this.postCreatingStatementPanel);

    }



    public int getPanelCount() {
        return this.panels.size();
    }

    public ExporttoSwingProviderPanel getPanelAt(int index) {
        return this.panels.get(index);
    }

    public FeatureStore getSource() {
        return this.sourceStore;
    }

    public ExporttoService createExporttoService() {
        JDBCServerExplorerParameters explorerParameters = this.getExplorerParameters();
        explorerParameters.setSchema(this.getSchema());
        return new ExporrtoJDBCService(this);
    }

    public boolean getTranslateIdentifiersToLowerCase() {
        return this.identifiersOptionsPanel.getTranslateToLowerCase();
    }

    public boolean getRemoveSpacesInIdentifiers() {
        return this.identifiersOptionsPanel.getRemoveSpacesInTableName();
    }

    public JDBCServerExplorerParameters getExplorerParameters() {
        return this.connectionPanel.getServerExplorerParameters();
    }

    public String getSchema() {
        return this.selectTableNamePanel.getSchema();
    }

    public String getTableName() {
        return this.selectTableNamePanel.getTableName();
    }

    public boolean canCreatetable() {
        return this.selectTableNamePanel.canCreateTable();
    }

    public String getPrimaryKey() {
        return this.selectPkPanel.getPrimaryKeyName();
    }

    public int getGeometryChecks() {
        return this.checkGeometriesPanel.getGeometryChecks();
    }

    public int getGeometryChecksAction() {
        return this.checkGeometriesPanel.getGeometryChecksAction();
    }

    public boolean getTryToFixGeometry() {
        return this.checkGeometriesPanel.getTryToFixGeometry();
    }

    public String getExplorerName() {
        return this.connectionPanel.getServerExplorerParameters().getExplorerName();
    }

    public String getStoreName() {
        if ( this.storeName == null ) {
            try {
                JDBCServerExplorerParameters explorerParameters = this.getExplorerParameters();
                if ( explorerParameters == null ) {
                    return null;
                }
                DataManager dataManager = DALLocator.getDataManager();

                JDBCServerExplorer explorer = (JDBCServerExplorer) dataManager.openServerExplorer(
                        explorerParameters.getExplorerName(),
                        explorerParameters
                );
                this.storeName = explorer.getStoreName();
            } catch (Exception ex) {
                return null;
            }
        }
        return this.storeName;
    }

    public IProjection getTargetProjection() {
        return this.projection;
    }

    public String getSelectRole() {
        return this.permissionsPanel.getSelectRole();
    }

    public String getInsertRole() {
        return this.permissionsPanel.getInsertRole();
    }

    public String getUpdateRole() {
        return this.permissionsPanel.getUpdateRole();
    }

    public String getDeleteRole() {
        return this.permissionsPanel.getDeleteRole();
    }

    public String getTruncateRole() {
        return this.permissionsPanel.getTruncateRole();
    }

    public String getReferenceRole() {
        return this.permissionsPanel.getReferenceRole();
    }

    public String getTriggerRole() {
        return this.permissionsPanel.getTriggerRole();
    }

    public String getAllRole() {
        return this.permissionsPanel.getAllRole();
    }

    public String getPostCreatingStatement() {
        return this.postCreatingStatementPanel.getPostCreatingStatement();
    }

    public boolean getCreateIndexInGeometryRow() {
        return this.geometryIndexPanel.getCreateIndexInGeometryRow();
    }

    public boolean getUpdateTableStatistics() {
        return this.updateTableStatistics.getUpdateTableStatistics();
    }

    /**
     * Sets the target projection to which should be exported
     * @param targetProjection
     */
    public void setTargetProjection(IProjection targetProjection){
        this.projection=targetProjection;
    }

    /**
     * Informs if it needs to ask for a target projection,
     * or if it is not needed or provided through its own wizard panel.
     * @return
     */
    public boolean needsPanelTargetProjection(){
        return true;
    }

}
