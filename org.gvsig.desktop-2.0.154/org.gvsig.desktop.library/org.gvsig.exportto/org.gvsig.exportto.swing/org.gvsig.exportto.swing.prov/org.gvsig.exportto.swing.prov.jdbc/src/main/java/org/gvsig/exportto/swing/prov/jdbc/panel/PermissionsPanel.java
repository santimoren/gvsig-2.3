
package org.gvsig.exportto.swing.prov.jdbc.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;


public class PermissionsPanel extends PermissionsPanelLayout implements ExporttoSwingProviderPanel {
    private static final long serialVersionUID = -1930610404012797310L;

    private ExporttoJDBCOptions provider;

    public PermissionsPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    private void initComponents() {
        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                doUpdateState();
            }
        };
        this.chkAll.addActionListener(listener);
        this.chkSelect.addActionListener(listener);
        this.chkUpdate.addActionListener(listener);
        this.chkInsert.addActionListener(listener);
        this.chkDelete.addActionListener(listener);
        this.chkTruncate.addActionListener(listener);
        this.chkTrigger.addActionListener(listener);
        this.chkReference.addActionListener(listener);
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_Permissions_header")+"</html>");
        this.chkAll.setText(i18nManager.getTranslation("_All"));
        
        // Nota:
        // No traducimos los nombres de los permisos ya que lo normal es que si
        // alguien los precisa usar los conozca en ingles.
        
        this.lblRoleAll.setText(i18nManager.getTranslation("_role"));
        this.lblRoleSelect.setText(i18nManager.getTranslation("_role"));
        this.lblRoleUpdate.setText(i18nManager.getTranslation("_role"));
        this.lblRoleInsert.setText(i18nManager.getTranslation("_role"));
        this.lblRoleDelete.setText(i18nManager.getTranslation("_role"));
        this.lblRoleTruncate.setText(i18nManager.getTranslation("_role"));
        this.lblRoleTrigger.setText(i18nManager.getTranslation("_role"));
        this.lblRoleReference.setText(i18nManager.getTranslation("_role"));

    }
    
    protected void doUpdateState() {
        this.txtRoleAll.setEnabled(this.chkAll.isSelected());
        this.txtRoleSelect.setEnabled(this.chkSelect.isSelected());
        this.txtRoleUpdate.setEnabled(this.chkUpdate.isSelected());
        this.txtRoleInsert.setEnabled(this.chkInsert.isSelected());
        this.txtRoleDelete.setEnabled(this.chkDelete.isSelected());
        this.txtRoleTruncate.setEnabled(this.chkTruncate.isSelected());
        this.txtRoleTrigger.setEnabled(this.chkTrigger.isSelected());
        this.txtRoleReference.setEnabled(this.chkReference.isSelected());
    }
    
    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_Permissions");    
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }
    
    public void enterPanel() {
        boolean enabled = this.provider.canCreatetable();
        this.chkAll.setEnabled(enabled);
        this.chkSelect.setEnabled(enabled);
        this.chkUpdate.setEnabled(enabled);
        this.chkInsert.setEnabled(enabled);
        this.chkDelete.setEnabled(enabled);
        this.chkTruncate.setEnabled(enabled);
        this.chkTrigger.setEnabled(enabled);
        this.chkReference.setEnabled(enabled);
    }
    
    public JComponent asJComponent() {
        return this;
    }

    public String getSelectRole() {
        if( !this.chkSelect.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleSelect.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getInsertRole() {
        if( !this.chkInsert.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleInsert.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getUpdateRole() {
        if( !this.chkUpdate.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleUpdate.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getDeleteRole() {
        if( !this.chkDelete.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleDelete.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getTruncateRole() {
        if( !this.chkTruncate.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleTruncate.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getReferenceRole() {
        if( !this.chkReference.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleReference.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getTriggerRole() {
        if( !this.chkTrigger.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleTrigger.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
    public String getAllRole() {
        if( !this.chkAll.isSelected() ) {
            return null;
        } 
        String role = this.txtRoleAll.getText();
        if( StringUtils.isBlank(role) ) {
            return null;
        }
        return role.trim();
    }
    
}
