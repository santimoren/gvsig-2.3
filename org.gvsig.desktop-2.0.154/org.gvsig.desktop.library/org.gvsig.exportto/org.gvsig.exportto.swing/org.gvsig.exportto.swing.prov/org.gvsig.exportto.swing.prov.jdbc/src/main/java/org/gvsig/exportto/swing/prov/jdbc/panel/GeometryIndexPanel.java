
package org.gvsig.exportto.swing.prov.jdbc.panel;

import javax.swing.JComponent;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GeometryIndexPanel extends GeometryIndexPanelLayout implements ExporttoSwingProviderPanel {
    private static final long serialVersionUID = -3995015914295698209L;
    
    private static final Logger logger = LoggerFactory.getLogger(GeometryIndexPanel.class);

    private ExporttoJDBCOptions provider;

    public GeometryIndexPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    private void initComponents() {
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_Create_spatial_index_header")+"</html>");
        this.chkCreateGeometryIndex.setText(i18nManager.getTranslation("_Crear_indice_espacial_sobre_las_columnas_de_tipo_geometria"));
    }

    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_Create_spatial_index");    
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }
    
    public void enterPanel() {
        if( this.provider.canCreatetable() ) {
            int index = -1;
            try {
                index = this.provider.getSource().getDefaultFeatureType().getDefaultGeometryAttributeIndex();
            } catch (DataException ex) {
                logger.warn("Problems determining if the tabla has a geometry column",ex);
            }
            if( index <0 ) {
                this.chkCreateGeometryIndex.setEnabled(false);
                this.chkCreateGeometryIndex.setSelected(false);
            } else {
                this.chkCreateGeometryIndex.setEnabled(true);
                this.chkCreateGeometryIndex.setSelected(true);
            }
        } else {
            this.chkCreateGeometryIndex.setEnabled(false);
            this.chkCreateGeometryIndex.setSelected(false);
        }
    }
    
    public JComponent asJComponent() {
        return this;
    }

    public boolean getCreateIndexInGeometryRow() {
        return this.chkCreateGeometryIndex.isSelected();
    }
}
