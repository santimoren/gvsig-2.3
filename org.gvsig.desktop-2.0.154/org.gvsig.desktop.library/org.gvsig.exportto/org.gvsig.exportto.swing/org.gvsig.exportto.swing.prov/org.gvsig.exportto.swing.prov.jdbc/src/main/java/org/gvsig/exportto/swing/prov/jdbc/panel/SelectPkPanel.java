/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.jdbc.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;

import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class SelectPkPanel extends SelectPkPanelLayout implements ExporttoSwingProviderPanel {

    private static final long serialVersionUID = 2652404227373508779L;

    private ExporttoJDBCOptions provider;

    public SelectPkPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    protected void initComponents() {
        this.rdoCreatePrimaryNewKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSelect();
            }
        });
        this.rdoUseExistingFieldAsPrimariKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSelect();
            }
        });
        this.rdoDoNotCreatePrimaryKey.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSelect();
            }
        });
        this.rdoCreatePrimaryNewKey.setSelected(true);
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_Primary_key_header")+"</html>");
        this.lblQuestion.setText(i18nManager.getTranslation("_Desea_crear_una_clave_primaria"));
        this.rdoCreatePrimaryNewKey.setText(i18nManager.getTranslation("_Generar_una_clave_primaria_con_un_serial"));
        this.lblPrimaryKeyName.setText(i18nManager.getTranslation("_Indique_el_nombre_a_usar_para_la_clave_primaria"));
        this.rdoUseExistingFieldAsPrimariKey.setText(i18nManager.getTranslation("_Utilizar_un_campo_existente_como_clave_primaria"));
        this.lblSelectFieldAsPrimaryKey.setText(i18nManager.getTranslation("_Seleccione_el_campo_a_usar_como_clave_primaria"));
        this.rdoDoNotCreatePrimaryKey.setText(i18nManager.getTranslation("_No_hacer_nada_en_relacion_a_la_creacion_de_la_clave_primaria"));
    }


    private void onSelect() {
        if (this.rdoCreatePrimaryNewKey.isSelected()) {
            this.txtPrimaryKeyName.setEnabled(true);
            this.lstFields.setEnabled(false);
        } else if (this.rdoUseExistingFieldAsPrimariKey.isSelected()) {
            this.txtPrimaryKeyName.setEnabled(false);
            this.lstFields.setEnabled(true);
        } else if (this.rdoDoNotCreatePrimaryKey.isSelected()) {
            this.txtPrimaryKeyName.setEnabled(false);
            this.lstFields.setEnabled(false);
        }
    }

    public String getPrimaryKeyName() {
        String pkname = null;
        if (this.rdoCreatePrimaryNewKey.isSelected()) {
            pkname = this.txtPrimaryKeyName.getText();

        } else if (this.rdoUseExistingFieldAsPrimariKey.isSelected()) {
            pkname = (String) this.lstFields.getSelectedValue();
        }
        if (StringUtils.isBlank(pkname)) {
            return null;
        }
        return pkname.trim();
    }

    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_Primary_key");
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }

    public void enterPanel() {
        this.fillListOfFields();
        if( !this.provider.canCreatetable() ) {
            this.rdoCreatePrimaryNewKey.setEnabled(false);
            this.rdoUseExistingFieldAsPrimariKey.setEnabled(false);
            this.rdoDoNotCreatePrimaryKey.setEnabled(false);
        
            this.rdoCreatePrimaryNewKey.setSelected(false);
            this.rdoUseExistingFieldAsPrimariKey.setSelected(false);
            this.rdoDoNotCreatePrimaryKey.setSelected(false);
        } else {
            this.rdoCreatePrimaryNewKey.setEnabled(true);
            this.rdoUseExistingFieldAsPrimariKey.setEnabled(true);
            this.rdoDoNotCreatePrimaryKey.setEnabled(true);
            
            if (! this.rdoCreatePrimaryNewKey.isSelected()
                && ! this.rdoUseExistingFieldAsPrimariKey.isSelected()
                && ! this.rdoDoNotCreatePrimaryKey.isSelected()) {
                this.rdoCreatePrimaryNewKey.setSelected(true);
            }
        }
    }

    public JComponent asJComponent() {
        return this;
    }

    private void fillListOfFields() {
        try {
            FeatureStore source = this.provider.getSource();
            FeatureType sourceFeatureType = source.getDefaultFeatureType();
            DefaultListModel model = new DefaultListModel();
            for (int i = 0; i < sourceFeatureType.size(); i++) {
                model.addElement(sourceFeatureType.getAttributeDescriptor(i).getName());
            }
            this.lstFields.setModel(model);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
