/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.jdbc.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import org.apache.commons.lang3.StringUtils;
//import org.gvsig.app.ApplicationLocator;
//import org.gvsig.app.ApplicationManager;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorer;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreParameters;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class SelectTableNamePanel extends SelectTableNamePanelLayout implements ExporttoSwingProviderPanel {

    private static final Logger logger = LoggerFactory.getLogger(SelectTableNamePanel.class);

    private static final long serialVersionUID = 6269512983586358017L;
    private final ExporttoJDBCOptions provider;

    private FillTablesListTask task = null;

    private static class TableItem {

        private JDBCStoreParameters params;
        private String label;

        public TableItem(String label, JDBCStoreParameters params) {
            this.params = params;
            this.label = label;
        }

        public TableItem(JDBCStoreParameters params) {
            this(params.getSchema() + "." + params.getTable(), params);
        }

        public String toString() {
            return this.label;
        }

        public JDBCStoreParameters getParams() {
            return this.params;
        }
    }

    public SelectTableNamePanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
        this.addAncestorListener(new AncestorListener() {

            public void ancestorAdded(AncestorEvent ae) {
            }

            public void ancestorRemoved(AncestorEvent ae) {
                cancelTask();
            }

            public void ancestorMoved(AncestorEvent ae) {
            }
        });
    }

    private void initComponents() {
        this.rdoCreateTable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onChangeRadioSelecion();
            }
        });
        this.rdoInsert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onChangeRadioSelecion();
            }
        });
        this.rdoCreateTable.setSelected(true);
        this.rdoInsert.setEnabled(false);
        this.lstTables.setEnabled(false);
        try {
            this.txtTableName.setText(this.provider.getSource().getName());
        } catch (Exception ex) {
            logger.warn("Can't set the default value for the table name", ex);
        }

        I18nManager i18nManager = ToolsLocator.getI18nManager();
        this.lblHeader.setText(i18nManager.getTranslation("_Indique_donde_desea_insertar_los_datos"));
        this.lblWarningUseExistingTable.setText(
                "<html>\n"
                + i18nManager.getTranslation("_Los_datos_se_insertaran_usando_los_nombres_de_columna_que_coincidan_con_la_tabla_origen_dejandose_al_valor_por_defecto_para_los_que_no_haya_valores_en_la_tabla_origen")
                + "\n</html>"
        );
        this.rdoInsert.setText(i18nManager.getTranslation("_Insertar_registros_en_una_tabla_existente"));
        this.lblSelectTableName.setText(i18nManager.getTranslation("_Seleccione_la_tabla_a_usar"));
        this.rdoCreateTable.setText(i18nManager.getTranslation("_Crear_una_tabla_nueva"));
        this.lblSchema.setText(i18nManager.getTranslation("_Indique_el_esquema_en_el_que_desea_crear_la_tabla"));
        this.lblTableName.setText(i18nManager.getTranslation("_Indique_el_nombre_de_la_tabla"));
    }

    private void cancelTask() {
        if (task != null) {
            task.cancelRequest();
            task.getSimpleTaskStatus().remove();
            task = null;
        }
    }

    public boolean canCreateTable() {
        return this.rdoCreateTable.isSelected();
    }

    public String getSchema() {
        if (this.canCreateTable()) {
            return StringUtils.defaultIfBlank(this.txtSchema.getText(), null);
        }
        TableItem item = (TableItem) this.lstTables.getSelectedValue();
        JDBCStoreParameters tableParameter = item.getParams();
        if (tableParameter == null) {
            return null;
        }
        return tableParameter.getSchema();
    }

    public String getTableName() {
        if (this.canCreateTable()) {
            return StringUtils.defaultIfBlank(this.txtTableName.getText(), null);
        }
        TableItem item = (TableItem) this.lstTables.getSelectedValue();
        if (item == null) {
            return null;
        }
        JDBCStoreParameters tableParameter = item.getParams();

        if (tableParameter == null) {
            return null;
        }
        return tableParameter.getTable();
    }

    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_Tablename");
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        String tablename = this.getTableName();
        if (tablename == null) {
            throw new ExporttoPanelValidationException(
                    i18nManager.getTranslation(
                            "_The_name_of_table_cannot_be_empty"
                    )
            );
        }
        String schema = this.getSchema();
        if (schema == null) {
            throw new ExporttoPanelValidationException(
                    i18nManager.getTranslation(
                            "_The_name_of_schema_cannot_be_empty"
                    )
            );
        }
        if (this.rdoCreateTable.isSelected()) {
            String tablename_tr = tablename;
            if( this.provider.getTranslateIdentifiersToLowerCase() ) {
                tablename_tr = tablename_tr.toLowerCase();
            }
            if( this.provider.getRemoveSpacesInIdentifiers() ) {
                tablename_tr = StringUtils.normalizeSpace(tablename_tr).replace(" ", "_");
            }
            if( !tablename_tr.equals(tablename) ) {
                String msg = i18nManager.getTranslation(
                        "Ha_utilizado_espacios_en_blanco_o_mayusculas_en_el_nombre_de_la_tabla_Desea_que_se_corrija_de_forma_automatica"
                );
                int resp = JOptionPane.showConfirmDialog(
                        this,
                        "Ha utilizado espacios en blanco o mayusculas en el nombre de la tabla.\n� Desea que se corrija de forma automatica ?", 
                        i18nManager.getTranslation("_Warning"),
                        JOptionPane.YES_NO_OPTION, 
                        JOptionPane.WARNING_MESSAGE
                );
                if( resp != JOptionPane.YES_OPTION ) {
                    msg = i18nManager.getTranslation(
                            "El_nombre_de_tabla_contiene_caracteres no_validos"
                    );
                    throw new ExporttoPanelValidationException(msg);
                }
                tablename = tablename_tr;
                this.txtTableName.setText(tablename);
            }
            ListModel model = this.lstTables.getModel();
            for (int i = 0; i < model.getSize(); i++) {
                TableItem item = (TableItem) model.getElementAt(i);
                if (schema.equals(item.getParams().getSchema())
                        && tablename.equals(item.getParams().getTable())) {
                    String msg = i18nManager.getTranslation(
                            "_La_tabla_{0}_{1}_ya_existe_en_la_base_de_datos_Seleccione_la_opcion_de_insertar_registros_en_una_tabla_existente_para_a�adir_los_datos_a_esta_o_indique_otro_nombre",
                            new String[]{schema, tablename}
                    );
                    throw new ExporttoPanelValidationException(msg);
                }
            }
        }
        return true;
    }

    public void enterPanel() {
        this.fillTablesList();
    }

    public JComponent asJComponent() {
        return this;
    }

    public void onChangeRadioSelecion() {
        if (this.rdoCreateTable.isSelected()) {
            this.txtSchema.setEnabled(true);
            this.txtTableName.setEnabled(true);
            this.lstTables.setEnabled(false);
        } else {
            this.txtSchema.setEnabled(false);
            this.txtTableName.setEnabled(false);
            this.lstTables.setEnabled(true);
        }
    }

    private void fillTablesList() {

        JDBCServerExplorerParameters explorerParameters = this.provider.getExplorerParameters();
        if (explorerParameters == null) {
            return;
        }
        cancelTask();
        this.task = new FillTablesListTask();
        task.setDaemon(true);
        task.start();
    }

    private class FillTablesListTask extends AbstractMonitorableTask {

        public FillTablesListTask() {
            super("Export");
        }

        protected SimpleTaskStatus getSimpleTaskStatus() {
            return (SimpleTaskStatus) this.getTaskStatus();
        }

        public void run() {

            JDBCServerExplorerParameters explorerParameters = provider.getExplorerParameters();
            if (provider.getExplorerParameters() == null) {
                return;
            }
            final SimpleTaskStatus status = this.getSimpleTaskStatus();
            try {
                status.setAutoremove(true);

                DataManager dataManager = DALLocator.getDataManager();

                this.getSimpleTaskStatus().message("Connecting server");
                explorerParameters.setShowInformationDBTables(false);
                JDBCServerExplorer explorer = (JDBCServerExplorer) dataManager.openServerExplorer(
                        explorerParameters.getExplorerName(),
                        explorerParameters
                );

                this.getSimpleTaskStatus().message("Retrieving tables");
                final List<JDBCStoreParameters> tables = explorer.list();

                this.getSimpleTaskStatus().message("Add tables");

                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        DefaultListModel lmodel = new DefaultListModel();
                        Iterator<JDBCStoreParameters> it = tables.iterator();
                        while (it.hasNext()) {
                            if (status.isCancelled()) {
                                status.cancel();
                                break;
                            }
                            JDBCStoreParameters table = it.next();
                            lmodel.addElement(new TableItem(table));
                        }
                        lstTables.setModel(lmodel);
                        //lstTables.setEnabled(true);
                        rdoInsert.setEnabled(true);
                    }
                });

                status.message("finish");
                status.terminate();

            } catch (final Exception ex) {
                logger.warn("Fail to fill tables list", ex);
                if (status.isCancellationRequested()) {
                    status.cancel();
                }
                if (status.isRunning()) {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                I18nManager i18nManager = ToolsLocator.getI18nManager();
                                ExporttoSwingManager manager = ExporttoSwingLocator.getSwingManager();

                                manager.showMessage(
                                        i18nManager.getTranslation("_Warning"),
                                        i18nManager.getTranslation("_There_have_been_problems_filling_data_in_panel")
                                        + " (" + getPanelTitle() + ")",
                                        ex,
                                        null
                                );
                            }
                        });
                    } catch (Exception ex2) {
                        logger.warn("Can't show error message", ex2);
                    }
                }
            } finally {
                status.terminate();
                status.remove();
            }

        }

    }

}
