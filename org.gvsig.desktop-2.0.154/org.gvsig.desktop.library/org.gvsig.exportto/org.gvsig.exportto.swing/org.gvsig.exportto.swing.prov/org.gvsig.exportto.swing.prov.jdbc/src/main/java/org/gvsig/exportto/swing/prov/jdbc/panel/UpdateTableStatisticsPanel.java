
package org.gvsig.exportto.swing.prov.jdbc.panel;

import javax.swing.JComponent;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;


public class UpdateTableStatisticsPanel extends UpdateTableStatisticsPanelLayout implements ExporttoSwingProviderPanel {
    private static final long serialVersionUID = -8450431632002084194L;

    private ExporttoJDBCOptions provider;

    public UpdateTableStatisticsPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    private void initComponents() {
        this.chkUpdateStatisticsTableAfterInserts.setEnabled(true);
        this.chkUpdateStatisticsTableAfterInserts.setSelected(true);
        this.translate();
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblHeader.setText("<html>"+i18nManager.getTranslation("_update_table_statistics_header")+"</html>");
        this.chkUpdateStatisticsTableAfterInserts.setText(i18nManager.getTranslation("_Recalcular_estadisticas_de_la_tabla_al_terminar"));
    }

    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("_update_table_statistics");    
    }

    public boolean isValidPanel() throws ExporttoPanelValidationException {
        return true;
    }
    
    public void enterPanel() {
        // Default do nothing
    }
    
    public JComponent asJComponent() {
        return this;
    }

    public boolean getUpdateTableStatistics() {
        return this.chkUpdateStatisticsTableAfterInserts.isSelected();
    }
}
