package org.gvsig.exportto.swing.prov.jdbc;

import org.gvsig.exportto.swing.ExporttoSwingLibrary;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.service.spi.ProviderManager;

public class ExporttoJDBCProviderLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(ExporttoSwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        i18nManager.addResourceFamily(
                "org.gvsig.exportto.swing.prov.jdbc",
                ExporttoJDBCProviderLibrary.class.getClassLoader(),
                ExporttoJDBCProviderLibrary.class.getClass().getName()
        );
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        ProviderManager providerManager = ExporttoSwingProviderLocator.getManager();
        providerManager.addProviderFactory(new BaseExporttoJDBCProviderFactory());
    }

}
