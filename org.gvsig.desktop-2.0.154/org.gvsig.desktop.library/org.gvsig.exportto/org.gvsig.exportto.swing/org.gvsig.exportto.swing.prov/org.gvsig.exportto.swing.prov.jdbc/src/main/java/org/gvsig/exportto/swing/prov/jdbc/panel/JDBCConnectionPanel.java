/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.jdbc.panel;

import javax.swing.JComponent;
import javax.swing.JPanel;
import org.gvsig.exportto.swing.prov.jdbc.ExporttoJDBCOptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorerParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class JDBCConnectionPanel  implements ExporttoSwingProviderPanel {

    private static final long serialVersionUID = -3278172717881233447L;

    private static final Logger LOG = LoggerFactory.getLogger(JDBCConnectionPanel.class);
    private org.gvsig.fmap.mapcontrol.dal.jdbc.JDBCConnectionPanel connectionPanel = null;
    private final ExporttoJDBCOptions provider;


    public JDBCConnectionPanel(ExporttoJDBCOptions provider) {
        this.provider = provider;
        initComponents();
    }

    private void initComponents() {
        this.connectionPanel = new org.gvsig.fmap.mapcontrol.dal.jdbc.JDBCConnectionPanel();
    }
    
    public void enterPanel() {
        // Default do nothing
    }
    
    public JDBCServerExplorerParameters getServerExplorerParameters() {
        return this.connectionPanel.getServerExplorerParameters();
    }
    
    public String getPanelTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("connection_params");
    }
    
    public boolean isValidPanel() throws ExporttoPanelValidationException {
        DBServerExplorerParameters connection = this.connectionPanel.getServerExplorerParameters();
        try {
            connection.validate();
            return true;
        } catch (ValidateDataParametersException ex) {
            throw new ExporttoPanelValidationException(ex.getMessageStack(),ex);
        } catch(Exception ex) {
            return false;
        }
    }

    public JComponent asJComponent() {
        return this.connectionPanel;
    }

}
