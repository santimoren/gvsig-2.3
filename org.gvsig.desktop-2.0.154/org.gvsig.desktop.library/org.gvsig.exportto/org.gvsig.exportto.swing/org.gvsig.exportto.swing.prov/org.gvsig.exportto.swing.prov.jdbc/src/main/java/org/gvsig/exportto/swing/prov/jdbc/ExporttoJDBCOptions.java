/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gvsig.exportto.swing.prov.jdbc;

import java.util.List;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;

/**
 *
 * @author jjdelcerro
 */
public interface ExporttoJDBCOptions  {

    public JDBCServerExplorerParameters getExplorerParameters();
    
    public String getSchema();
    
    public String getTableName();
    
    public boolean canCreatetable();
    
    public String getPrimaryKey();
    
    public int getGeometryChecks();
    
    public int getGeometryChecksAction();
    
    public String getStoreName();

    public String getExplorerName();
    
    public FeatureStore getSource();
    
    public boolean getTryToFixGeometry();

    public boolean getTranslateIdentifiersToLowerCase();

    public boolean getRemoveSpacesInIdentifiers();
    
    public IProjection getTargetProjection();
    
    public String getSelectRole();
    
    public String getInsertRole();
    
    public String getUpdateRole();
    
    public String getDeleteRole();
    
    public String getTruncateRole();
    
    public String getReferenceRole();
    
    public String getTriggerRole();
    
    public String getAllRole();
    
    public String getPostCreatingStatement();
    
    public boolean getCreateIndexInGeometryRow();
    
    public boolean getUpdateTableStatistics();
}
