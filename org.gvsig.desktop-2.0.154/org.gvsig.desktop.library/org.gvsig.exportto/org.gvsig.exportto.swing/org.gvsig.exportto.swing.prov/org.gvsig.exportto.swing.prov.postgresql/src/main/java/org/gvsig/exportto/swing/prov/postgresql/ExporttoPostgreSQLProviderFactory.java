/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.postgresql;

import org.cresques.cts.IProjection;

import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.spi.AbstractExporttoProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * Factory of file {@link ExportoProvider} objects.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporttoPostgreSQLProviderFactory extends
    AbstractExporttoProviderFactory {

    private static final String PROVIDER_NAME = "PostgreSQL";

    public ExporttoPostgreSQLProviderFactory() {
        super(new int[] {
            ExporttoSwingManager.VECTORIAL_TABLE_WITHOUT_GEOMETRY,
            ExporttoSwingManager.VECTORIAL_TABLE_WITH_GEOMETRY });
    }

    public Provider create(DynObject parameters, ProviderServices services)
        throws ServiceException {
        return new ExporttoPostgreSQLProvider(services,
            (FeatureStore) parameters.getDynValue(PARAMETER_FEATURESTORE),
            (IProjection) parameters.getDynValue(PARAMETER_PROJECTION));
    }

    public String getName() {
        return PROVIDER_NAME;
    }
}
