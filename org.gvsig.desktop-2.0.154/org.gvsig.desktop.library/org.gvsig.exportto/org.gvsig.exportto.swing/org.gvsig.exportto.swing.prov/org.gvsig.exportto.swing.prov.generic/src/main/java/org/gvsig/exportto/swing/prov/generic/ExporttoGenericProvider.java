/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.swing.prov.generic.panel.DataServerExplorerPanel;
import org.gvsig.exportto.swing.prov.generic.panel.DataServerExplorerParamsPanel;
import org.gvsig.exportto.swing.prov.generic.panel.DataStorePanel;
import org.gvsig.exportto.swing.prov.generic.panel.DataStoreParamsPanel;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.dynobject.DynObjectSwingManager;
import org.gvsig.tools.swing.api.dynobject.JDynObjectComponent;

/**
 * Exporto provider which gets Exporto from a file.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporttoGenericProvider extends AbstractProvider implements
    ExporttoSwingProvider {

    private static final Logger LOG = LoggerFactory
        .getLogger(ExporttoGenericProvider.class);

    private static final DataManager DATA_MANAGER = DALLocator.getDataManager();
    private static final DynObjectSwingManager DYN_OBJECT_SWING_MANAGER =
        ToolsSwingLocator.getDynObjectSwingManager();
    private static final ExporttoManager EXPORTTO_MANAGER = ExporttoLocator
        .getManager();

    private DataServerExplorerPanel dataServerExplorerPanel = null;
    private DataServerExplorerParamsPanel dataServerExplorerParamsPanel = null;
    private DataStorePanel dataStorePanel = null;
    private DataStoreParamsPanel dataStoreParamsPanel = null;

    private FeatureStore featureStore;
    private IProjection projection;
    private DataServerExplorer dataServerExplorer = null;

    /**
     * Constructor.
     * 
     * @param providerServices
     *            the services for the provider
     * @param file
     *            to get the Exporto from
     */
    public ExporttoGenericProvider(ProviderServices providerServices,
        FeatureStore featureStore, IProjection projection) {
        super(providerServices);
        this.featureStore = featureStore;
        this.projection = projection;

        dataServerExplorerPanel = new DataServerExplorerPanel(this);
        dataServerExplorerParamsPanel = new DataServerExplorerParamsPanel(this);
        dataStorePanel = new DataStorePanel(this);
        dataStoreParamsPanel = new DataStoreParamsPanel(this);
    }

    public int getPanelCount() {
        return 4;
    }

    public ExporttoSwingProviderPanel getPanelAt(int index) {
        switch (index) {
        case 0:
            return dataServerExplorerPanel;
        case 1:
            return dataServerExplorerParamsPanel;
        case 2:
            return dataStorePanel;
        case 3:
            return dataStoreParamsPanel;
        }
        return null;
    }

    public void setDataServerExplorerSelected(String explorerName)
        throws ExporttoPanelValidationException {
        DataServerExplorerParameters dataServerExplorerParameters;
        try {
            dataServerExplorerParameters =
                DATA_MANAGER.createServerExplorerParameters(explorerName);
            JDynObjectComponent dynObjectComponent =
                DYN_OBJECT_SWING_MANAGER.createJDynObjectComponent(
                    dataServerExplorerParameters, true);
            this.dataServerExplorerParamsPanel
                .setDynObjectComponent(dynObjectComponent);
        } catch (InitializeException e1) {
            throw new ExporttoPanelValidationException(
                "Error creating the explorer params panel", e1);
        } catch (ProviderNotRegisteredException e1) {
            throw new ExporttoPanelValidationException(
                "Error creating the explorer params panel", e1);
        } catch (ServiceException e1) {
            throw new ExporttoPanelValidationException(
                "Error creating the explorer params panel", e1);
        }
    }

    public void setDataServerExplorerParameters(
        DataServerExplorerParameters dataServerExplorerParameters)
        throws ExporttoPanelValidationException {
        String explorerName =
            this.dataServerExplorerPanel.getSelectedProvider();
        if (explorerName != null) {
            if (dataServerExplorerParameters != null) {
                try {
                    dataServerExplorer =
                        DATA_MANAGER.openServerExplorer(explorerName,
                            dataServerExplorerParameters);
                    this.dataStorePanel
                        .setDataServerExplorer(dataServerExplorer);
                } catch (ValidateDataParametersException e) {
                    throw new ExporttoPanelValidationException(
                        "error creating the explorer", e);
                } catch (InitializeException e) {
                    throw new ExporttoPanelValidationException(
                        "error creating the explorer", e);
                } catch (ProviderNotRegisteredException e) {
                    throw new ExporttoPanelValidationException(
                        "error creating the explorer", e);
                }
            }
        }
    }

    public DataServerExplorer getDataServerExplorer() {
        return dataServerExplorer;
    }

    public void setDataStoreSelected(String storeName)
        throws ExporttoPanelValidationException {
        try {
            NewDataStoreParameters newDataStoreParameters =
                dataServerExplorer.getAddParameters(storeName);

            try {
                newDataStoreParameters.setDynValue("CRS", projection);
            } catch (DynFieldNotFoundException e) {
                LOG.info("The provider {} doesn't support the CRS parameter",
                    storeName);
            }
            try {
                newDataStoreParameters.setDynValue("featureType", featureStore
                    .getDefaultFeatureType().getEditable());
            } catch (DynFieldNotFoundException e) {
                LOG.info(
                    "The provider {} doesn't support the featureType parameter",
                    storeName);
            }

            JDynObjectComponent dynObjectComponent =
                DYN_OBJECT_SWING_MANAGER.createJDynObjectComponent(
                    newDataStoreParameters, true);
            this.dataStoreParamsPanel.setDynObjectComponent(dynObjectComponent);
        } catch (Exception e1) {
            throw new ExporttoPanelValidationException(
                "Error creating the store params panel", e1);
        }
    }

    private NewDataStoreParameters getNewDataStoreParameters() {
        return dataStoreParamsPanel.getNewDataStoreParameters();
    }

    public ExporttoService createExporttoService() {
        return EXPORTTO_MANAGER.getExporttoService(dataServerExplorer,
            getNewDataStoreParameters());

    }
}
