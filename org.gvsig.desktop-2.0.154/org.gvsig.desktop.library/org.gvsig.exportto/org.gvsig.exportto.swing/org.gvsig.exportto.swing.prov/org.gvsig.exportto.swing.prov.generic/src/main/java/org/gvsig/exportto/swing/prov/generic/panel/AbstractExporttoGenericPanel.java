/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic.panel;

import javax.swing.JComponent;
import javax.swing.JPanel;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.prov.generic.ExporttoGenericProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public abstract class AbstractExporttoGenericPanel extends JPanel implements
    ExporttoSwingProviderPanel {

    private static final long serialVersionUID = -7994157431819798158L;

    protected ExporttoGenericProvider exporttoGenericProvider;
    protected static final ExporttoSwingManager EXPORTTO_SWING_MANAGER =
        ExporttoSwingLocator.getSwingManager();

    public AbstractExporttoGenericPanel(
        ExporttoGenericProvider exporttoGenericProvider) {
        super();
        this.exporttoGenericProvider = exporttoGenericProvider;
    }

    public JComponent asJComponent() {
        return this;
    }

    public void enterPanel() {
        // Default do nothing
    }
    
    
    
}
