/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic.panel;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionListener;

import org.gvsig.exportto.swing.prov.generic.ExporttoGenericProvider;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public abstract class AbstractProviderPanel extends
    AbstractExporttoGenericPanel {

    private static final long serialVersionUID = -3344421488964340688L;
    private JList dalProviderList = null;
    private JScrollPane scrollPane = null;

    public AbstractProviderPanel(
        ExporttoGenericProvider exporttoGenericProvider, ListModel listModel) {
        super(exporttoGenericProvider);
        initializeComponents(listModel);
    }

    private void initializeComponents(ListModel listModel) {
        this.setLayout(new BorderLayout());

        // Create the list
        dalProviderList = new JList();
        dalProviderList.setModel(listModel);

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(dalProviderList);

        this.add(scrollPane, BorderLayout.CENTER);
    }

    protected void setModel(ListModel listModel) {
        dalProviderList.setModel(listModel);
    }

    public String getSelectedProvider() {
        Object obj = dalProviderList.getSelectedValue();
        if (obj != null) {
            return (String) obj;
        }
        return null;
    }

    public void AddListSelectionListener(
        ListSelectionListener listSelectionListener) {
        this.dalProviderList.addListSelectionListener(listSelectionListener);
    }

    @Override
    public boolean isValidPanel() throws ExporttoPanelValidationException {
        if (getSelectedProvider() == null) {
            throw new ExporttoPanelValidationException(
                "A provider has to be selected");
        }
        return true;
    }
}
