/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic;

import java.util.Locale;

import org.gvsig.exportto.swing.ExporttoSwingLibrary;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Library to initialize and register the file Exporto provider
 * implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporttoGenericProviderLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(ExporttoSwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
        if (!Messages.hasLocales()) {
            Messages.addLocale(Locale.getDefault());
        }
        Messages.addResourceFamily(
            "org.gvsig.exportto.swing.prov.generic.i18n.text",
            ExporttoGenericProviderLibrary.class.getClassLoader(),
            ExporttoGenericProviderLibrary.class.getClass().getName());
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        ExporttoSwingProviderManager providerManager =
            ExporttoSwingProviderLocator.getManager();
        ExporttoGenericProviderFactory providerFactory =
            new ExporttoGenericProviderFactory();
        providerManager.addProviderFactory(providerFactory);

        // Disable this provider by default
        providerManager.enableProvider(providerFactory, false);
    }

}
