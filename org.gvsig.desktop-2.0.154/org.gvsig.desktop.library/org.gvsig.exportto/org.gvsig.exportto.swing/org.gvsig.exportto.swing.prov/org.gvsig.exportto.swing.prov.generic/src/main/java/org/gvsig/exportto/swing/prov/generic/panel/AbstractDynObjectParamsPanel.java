/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic.panel;

import java.awt.BorderLayout;

import org.gvsig.exportto.swing.prov.generic.ExporttoGenericProvider;
import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public abstract class AbstractDynObjectParamsPanel extends
    AbstractExporttoGenericPanel {

    private static final long serialVersionUID = -8274240222613051224L;
    protected JDynForm dynform = null;

    public AbstractDynObjectParamsPanel(
        ExporttoGenericProvider exporttoGenericProvider) {
        super(exporttoGenericProvider);
        this.setLayout(new BorderLayout());
    }

    /**
     * @param dynform
     *            the dynObjectComponent to set
     */
    public void setDynObjectComponent(JDynForm dynform) {
        this.dynform = dynform;
        this.removeAll();
        this.add(dynform.asJComponent(), BorderLayout.CENTER);
    }

    @Override
    public boolean isValidPanel() throws ExporttoPanelValidationException {
//        DynObject dynObject = this.dynform.getDynObject();
//        
//        try {
//            dynObject.getDynClass().validate(dynObject);
//        } catch (DynObjectValidateException e) {
//            throw new ExporttoPanelValidationException(
//                "Error validating the panel", e);
//        }
        return true;
    }
}
