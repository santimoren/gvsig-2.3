/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.generic;

import org.cresques.cts.IProjection;

import org.gvsig.exportto.swing.spi.AbstractExporttoProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * Factory of generic {@link ExportoProvider} objects.
 * This factory add support for any DAL provider in a generic way.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporttoGenericProviderFactory extends
    AbstractExporttoProviderFactory implements ExporttoSwingProviderFactory {

    private static final String PROVIDER_NAME = "Generic";

    /**
     * Constructor.
     */
    public ExporttoGenericProviderFactory() {
        super(null);
    }

    public Provider create(DynObject parameters, ProviderServices services)
        throws ServiceException {
        return new ExporttoGenericProvider(services,
            (FeatureStore) parameters.getDynValue(PARAMETER_FEATURESTORE),
            (IProjection) parameters.getDynValue(PARAMETER_PROJECTION));
    }

    public String getName() {
        return PROVIDER_NAME;
    }

    public boolean support(int providerType) throws ServiceException {
        return true;
    }

    public String getDescription() {
        return Messages.getText("exportto_generic_description");
    }

    public String getLabel() {
        return Messages.getText("exportto_generic_label");
    }
}
