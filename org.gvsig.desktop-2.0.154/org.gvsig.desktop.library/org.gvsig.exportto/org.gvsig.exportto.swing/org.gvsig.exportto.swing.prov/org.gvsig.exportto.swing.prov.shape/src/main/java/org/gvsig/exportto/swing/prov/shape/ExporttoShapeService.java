/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.prov.shape;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.exportto.swing.prov.file.panel.SelectFileOptionPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.task.AbstractMonitorableTask;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class ExporttoShapeService extends AbstractMonitorableTask implements
    ExporttoService {

    private static Logger logger = LoggerFactory.getLogger(ExporttoShapeService.class);

    public static final int MAX_FIELD_NAME_LENGTH = 10;

    private File theShapeFile;
    private IProjection projection;
    private FeatureStore featureStore;
    private Map<String, String> origNameToDbfName;

    private int geometryType = -1;
    private NewFeatureStoreParameters newFeatureStoreParameters;
    private FilesystemServerExplorer filesystemServerExplorer;
    private SelectFileOptionPanel filePanel = null;

    private static GeometryManager geoManager = GeometryLocator.getGeometryManager();

    private ExporttoServiceFinishAction exporttoServiceFinishAction;

    public ExporttoShapeService(
    		SelectFileOptionPanel fPanel,
    		FeatureStore featureStore,
    		IProjection projection) {

        super("Export to shape");
        this.featureStore = featureStore;
        this.filePanel = fPanel;
        this.theShapeFile = fPanel.getSelectedFile();
        this.projection = projection;

        try {
			origNameToDbfName = getNames(featureStore.getDefaultFeatureType());
		} catch (DataException e) {
			logger.error("While getting def feat type.", e);
		}
    }

    private Map<String, String> getNames(FeatureType ft) {

    	Map<String, String> resp = new HashMap<String, String>();
    	FeatureAttributeDescriptor[] atts = ft.getAttributeDescriptors();
    	for (int i=0; i<atts.length; i++) {
    		//if (atts[i].getName().length() > MAX_FIELD_NAME_LENGTH) {
    			String new_name = getNewName(atts[i].getName(), resp.values());
    			resp.put(atts[i].getName(), new_name);
            //}
    	}
		return resp;
	}

	private String getNewName(String name, Collection<String> values) {

		int len = name.length();
		if (len <= MAX_FIELD_NAME_LENGTH) {
			/*
			 * Should not happen
			 */
			return name;
		}

		String resp = null;
		/*
		 * Composes a new name with start and end of old name and an index
		 * THISISAVERYLONGNAME => THISI_AME_1, THISI_AME_2 ... THISI_AME_9
		 */
		/*
		String pref = name.substring(0, 6) + "_" + name.substring(len - 2) + "_";
		for (int i=0; i<10; i++) {
			resp = pref + i;
			if (!values.contains(resp)) {
				return resp;
			}
		}
		*/
		/*
		 * Very strange to get here...
		 * THISISAVERYLONGNAME => THISISA_1, THISISA_2 ... THISISA_999
		 */
		/*
		String pref = name.substring(0, 7) + "_";
		for (int i=0; i<1000; i++) {
			resp = pref + i;
			if (!values.contains(resp)) {
				return resp;
			}
		}
		*/
		/*
		 * GDAL field name truncation method (extended from 100 to 255)
		 * THISISAVERYLONGNAME => THISISAVER, THISISAV_1 ... THISISAV_9,
		 * THISISAV10 ... THISISAV99, THISISA100 ... THISISA255
		 * (255 = max number of fields in a SHP)
		 */
		//String resp = null;
		for (int i=0; i<255; i++) {
		    if (i==0)
		    	resp = name.substring(0, 10);
		    else if (i<=9)
		        resp = name.substring(0, 8) + "_" + i;
		    else if (i<=99)
		    	resp = name.substring(0, 8) + i;
		    else
		    	resp = name.substring(0, 7) + i;
		    if (!values.contains(resp))
		        return resp;
		}
		/*
		 * Should not get here
		 */
		return name.substring(0, 4) + "_" + (System.currentTimeMillis() % 1000000);
	}

	public void export(FeatureSet featureSet) throws ExporttoServiceException {

        ExporttoServiceException throw_exp = null;
        File item_shp = null;
        String pathFile = theShapeFile.getAbsolutePath();
        String withoutShp = pathFile.replaceAll("\\.shp", "");

        File single_file = new File(withoutShp + ".shp");

        String layer_name = single_file.getName();
        int lastp = layer_name.lastIndexOf(".shp");
        if (lastp > -1) {
            layer_name = layer_name.substring(0, lastp);
        }

        initializeParams(featureSet, single_file);

        if (geometryType == Geometry.TYPES.GEOMETRY) {
            // SURFACE
            String fileName = withoutShp + "_surface" + ".shp";
            item_shp = new File(fileName);
            initializeParams(featureSet, item_shp);
            newFeatureStoreParameters.setDynValue("shpfile", item_shp);
            taskStatus.setTitle("Exporting surfaces");

            try {
                export(filesystemServerExplorer, newFeatureStoreParameters,
                    featureSet, Geometry.TYPES.SURFACE, true);
            } catch (ExporttoServiceException ee) {
                throw_exp = ee;
            }
            finishAction(layer_name + " (surface)", item_shp, projection);

            // CURVE
            fileName = withoutShp + "_curve" + ".shp";
            item_shp = new File(fileName);
            initializeParams(featureSet, item_shp);
            newFeatureStoreParameters.setDynValue("shpfile", item_shp);
            taskStatus.setTitle("Exporting curves");

            try {
                export(filesystemServerExplorer, newFeatureStoreParameters,
                    featureSet, Geometry.TYPES.CURVE, true);
            } catch (ExporttoServiceException ee) {
                throw_exp = ee;
            }

            finishAction(layer_name + " (curve)", item_shp, projection);

            // POINT
            fileName = withoutShp + "_point" + ".shp";
            item_shp = new File(fileName);
            initializeParams(featureSet, item_shp);

            newFeatureStoreParameters.setDynValue("shpfile", item_shp);
            taskStatus.setTitle("Exporting points");

            try {
                export(filesystemServerExplorer, newFeatureStoreParameters,
                    featureSet, Geometry.TYPES.POINT, true);
            } catch (ExporttoServiceException ee) {
                throw_exp = ee;
            }

            finishAction(layer_name + " (point)", item_shp, projection);


        } else {

            // params already initialized
            newFeatureStoreParameters.setDynValue("shpfile", single_file);
            try {
                export(filesystemServerExplorer, newFeatureStoreParameters,
                    featureSet, geometryType, false);
            } catch (ExporttoServiceException ee) {
                throw_exp = ee;
            }
            finishAction(layer_name, single_file, projection);
        }
        this.taskStatus.terminate();
        this.taskStatus.remove();

        if (throw_exp != null) {
            throw throw_exp;
        } else {
        	if (origNameToDbfName.size() > 0) {
        		showNewNamesDialog(origNameToDbfName);
        	}
        }
    }

    private void showNewNamesDialog(Map<String, String> map) {

    	Iterator<String> iter = map.keySet().iterator();

    	String msg = Messages.getText("_Some_field_names_too_long_automatically_changed");
    	msg = msg + ":\n";
    	String k, v;
    	boolean hasModifiedNames = false;
    	while (iter.hasNext()) {
    		k = iter.next();
    		v = map.get(k);
    		if(!k.equalsIgnoreCase(v)){
    		msg = msg + "\n" + k + " > " + v;
    			hasModifiedNames = true;
    		}
    	}

    	if(hasModifiedNames){
    	JOptionPane.showMessageDialog(
    			this.filePanel,
    			msg,
    			Messages.getText("export_progress"),
    			JOptionPane.WARNING_MESSAGE);
	}
	}

	private void initializeParams(
        FeatureSet featureSet, File out_shp_file)
        throws ExporttoServiceException {

        DataManager dataManager = DALLocator.getDataManager();

        FilesystemServerExplorerParameters explorerParams;
        try {
            explorerParams =
                (FilesystemServerExplorerParameters) dataManager
                    .createServerExplorerParameters(FilesystemServerExplorer.NAME);
        } catch (InitializeException e) {
            throw new ExporttoServiceException(e);
        } catch (ProviderNotRegisteredException e) {
            throw new ExporttoServiceException(e);
        }
        explorerParams.setRoot(out_shp_file.getParent());

        try {
            filesystemServerExplorer =
                (FilesystemServerExplorer) dataManager.openServerExplorer(
                    "FilesystemExplorer", explorerParams);
        } catch (ValidateDataParametersException e) {
            throw new ExporttoServiceException(e);
        } catch (InitializeException e) {
            throw new ExporttoServiceException(e);
        } catch (ProviderNotRegisteredException e) {
            throw new ExporttoServiceException(e);
        }

        try {
            newFeatureStoreParameters =
                (NewFeatureStoreParameters) filesystemServerExplorer
                    .getAddParameters(out_shp_file);
        } catch (DataException e) {
            throw new ExporttoServiceException(e);
        }

        newFeatureStoreParameters.setDynValue("CRS", projection);

        geometryType =
            featureSet.getDefaultFeatureType().getDefaultGeometryAttribute()
                .getGeomType().getType();

    }

    private void export(FilesystemServerExplorer explorer,
        NewFeatureStoreParameters params, FeatureSet featureSet,
        int geometryType, boolean checkType) throws ExporttoServiceException {

        String providerName = params.getDataStoreName();
        String explorerName = explorer.getProviderName();
        boolean there_was_error = false;

        DisposableIterator it = null;
        try {
            EditableFeatureType type =
                featureStore.getDefaultFeatureType().getCopy().getEditable();

            FeatureAttributeDescriptor fad =
                (FeatureAttributeDescriptor) type.get(type
                    .getDefaultGeometryAttributeName());

            type.remove(fad.getName());
            EditableFeatureAttributeDescriptor efad =
                type.add(fad.getName(), fad.getType(), fad.getSize());
            efad.setDefaultValue(fad.getDefaultValue());

            int gsubtype = fad.getGeomType().getSubType();
            // El shp solo soporta los subtipos 2D y 3D
            switch( gsubtype ) {
            case Geometry.SUBTYPES.GEOM2D:
                break;
            case Geometry.SUBTYPES.GEOM3D:
                break;
            default:
                // Forzaremos las geometrias a 3D
                gsubtype = Geometry.SUBTYPES.GEOM3D;
                break;
            }
            GeometryType gty = null;
            try {
                gty = geoManager.getGeometryType(geometryType, gsubtype);
            } catch (Exception e) {
                throw new ExporttoServiceException(e);
            }

            efad.setGeometryType(gty);

            efad.setName(origNameToDbfName.get(fad.getName()));
            efad.setPrecision(fad.getPrecision());


            type.setDefaultGeometryAttributeName(efad.getName());

            // ==========================
            fixNames(type);
            // ==========================

            params.setDefaultFeatureType(type.getNotEditableCopy());

            params.setDynValue("geometryType", null);

            DataManager manager = DALLocator.getDataManager();

            manager.newStore(explorerName, providerName, params, true);
            FeatureStore target =
                (FeatureStore) manager.openStore(providerName, params);

            FeatureType targetType = target.getDefaultFeatureType();

            taskStatus.setRangeOfValues(0, featureSet.getSize());

            target.edit(FeatureStore.MODE_APPEND);
            it = featureSet.fastIterator();
            int featureCount = 0;

            // ================================================
            // Reprojection stuff
            Geometry reproj_geom = null;
            EditableFeature edit_feat = null;
            IProjection sourceProjection =
                featureStore.getDefaultFeatureType().getDefaultGeometryAttribute().getSRS();

            ICoordTrans coord_trans = null;
            // this comparison is perhaps too preventive
            // we could  have two instances of same projection
            // so we would do more computations than needed
            if (sourceProjection != null && sourceProjection != this.projection) {
                coord_trans = sourceProjection.getCT(this.projection);
            }
            // ================================================

            List<Geometry> extracted = null;
            Geometry gitem = null;

            while (it.hasNext()) {

                Feature feature = (Feature) it.next();
                gitem = feature.getDefaultGeometry();

                if (checkType) {
                    extracted = getGeometriesFrom(gitem, geometryType);
                    if (extracted.size() == 0) {
                        // found no geometries of correct type
                        continue;
                    } else {
                        if (geometryType != Geometry.TYPES.POINT) {
                            // If not points, merge geometries
                            // (curves or surfaces)
                            try {
                                gitem = union(extracted);
                                extracted = new ArrayList<Geometry>();
                                extracted.add(gitem);
                            } catch (Exception ex) {
                                there_was_error = true;
                                logger.info("Error in union.", ex);
                            }
                        } else {
                            // only in the case of points, we can have several
                            // geometries if source is multipoint
                        }
                    }
                } else {
                    extracted = new ArrayList<Geometry>();
                    extracted.add(gitem);
                }

                for (int i=0; i<extracted.size(); i++) {
                    gitem = extracted.get(i);

                    if( gsubtype==Geometry.SUBTYPES.GEOM2D ) {
                        gitem = force2D(gitem, geometryType);
                    } else {
                        gitem = force3D(gitem, geometryType);
                    }
                    edit_feat = target.createNewFeature(true);
                    there_was_error = there_was_error |
                    		/*
                    		 * Accumulate error in boolean.
                    		 * This also fixes field names (using origNameToDbfName)
                    		 */
                    		setNonNulls(featureStore.getDefaultFeatureType(),
                    				targetType, feature, edit_feat);
                    edit_feat.setDefaultGeometry(gitem);
                    // ================================================
                    // Reprojection stuff
                    if (coord_trans != null) {
                        reproj_geom = edit_feat.getDefaultGeometry();
                        reproj_geom = reproj_geom.cloneGeometry();
                        reproj_geom.reProject(coord_trans);
                        edit_feat.setDefaultGeometry(reproj_geom);
                    }
                    // ================================================
                    target.insert(edit_feat);
                }

                featureCount++;
                this.taskStatus.setCurValue(featureCount);

                if (this.taskStatus.isCancellationRequested()) {
                    return;
                }
            }
            target.finishEditing();
            target.dispose();
        } catch (Exception e) {
            throw new ExporttoServiceException(e);
        } finally {
            DisposeUtils.dispose(it);
        }

        if (there_was_error) {
            Exception cause = new Exception(
                "_Issues_with_attributes_or_geometries");
            throw new ExporttoServiceException(cause);
        }
    }


    private void fixNames(EditableFeatureType eft) {

    	FeatureAttributeDescriptor[] atts = eft.getAttributeDescriptors();
    	EditableFeatureAttributeDescriptor efad = null;
    	for (int i=0; i<atts.length; i++) {
    		String new_name = origNameToDbfName.get(atts[i].getName());
    		if (new_name != null) {
    			eft.remove(atts[i].getName());
    			efad = eft.add(new_name, atts[i].getType(), atts[i].getSize());
    			efad.setPrecision(atts[i].getPrecision());
    			if(atts[i].getDataType().getType() == DataTypes.GEOMETRY){
    				efad.setGeometryType(atts[i].getGeomType());
    			}
    		}
    	}
	}

	/**
     * @param gitem
     * @param geometryType2
     * @return
     */
    private Geometry force2D(Geometry ge, int gt) throws CreateGeometryException {

        if (ge.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM2D) {
            return ge;
        } else {
            switch (gt) {
            case Geometry.TYPES.POINT:
                Point p = (Point) ge;
                Point point = geoManager.createPoint(
                    p.getX(), p.getY(),
                    Geometry.SUBTYPES.GEOM2D);
                return point;
            case Geometry.TYPES.CURVE:
                return geoManager.createCurve(ge.getGeneralPath(), Geometry.SUBTYPES.GEOM2D);
            case Geometry.TYPES.SURFACE:
                return geoManager.createSurface(ge.getGeneralPath(), Geometry.SUBTYPES.GEOM2D);
            default:
                return ge;
            }
        }
    }

    /**
     * @param gitem
     * @param geometryType2
     * @return
     */
    private Geometry force3D(Geometry ge, int gt) throws CreateGeometryException {
        // Los try catch que hay abajo, son para asegurarse de que la geometrķa original
        // tiene las 3 dimensiones necesarias. A veces nos llegan geometrķas que aunque son de subtipo GEOM3D
        // los puntos del generalPath que contienen no son 3D y no tienen la coordenada Z
            Point point;
            switch (gt) {
            case Geometry.TYPES.POINT:
                Point p = (Point) ge;
                point = geoManager.createPoint(
                    p.getX(), p.getY(),
                    Geometry.SUBTYPES.GEOM3D);
                try {
                    point.setCoordinateAt(2, p.getCoordinateAt(2));
                } catch (Exception e) {
                    point.setCoordinateAt(2, 0);
                }
                return point;
            case Geometry.TYPES.CURVE:
            case Geometry.TYPES.LINE:
                Line line = geoManager.createLine(Geometry.SUBTYPES.GEOM3D);
                for (int i = 0; i < ((OrientablePrimitive)ge).getNumVertices(); i++) {
                    Point vertex = ((OrientablePrimitive)ge).getVertex(i);
                    point = geoManager.createPoint(vertex.getX(), vertex.getY(), Geometry.SUBTYPES.GEOM3D);
                    try {
                        point.setCoordinateAt(2, vertex.getCoordinateAt(2));
                    } catch (Exception e) {
                        point.setCoordinateAt(2, 0);
                    }
                    line.addVertex(point);
                };
                return line;

            case Geometry.TYPES.SURFACE:
            case Geometry.TYPES.POLYGON:
                Polygon polygon = geoManager.createPolygon(Geometry.SUBTYPES.GEOM3D);
                for (int i = 0; i < ((OrientablePrimitive)ge).getNumVertices(); i++) {
                    Point vertex = ((OrientablePrimitive)ge).getVertex(i);
                    point = geoManager.createPoint(vertex.getX(), vertex.getY(), Geometry.SUBTYPES.GEOM3D);
                    try {
                        point.setCoordinateAt(2, vertex.getCoordinateAt(2));
                    } catch (Exception e) {
                        point.setCoordinateAt(2, 0);
                    }
                    polygon.addVertex(point);
                };
                return polygon;
            default:
                return ge;
            }
    }
    private void createPoint(int geom3d) {
        // TODO Auto-generated method stub

    }

    /**
     * @param feature
     * @param edit_feat
     */
    private boolean setNonNulls(
        FeatureType src_ft, FeatureType target_ft,
        Feature feat, EditableFeature edit_f) {

        boolean error = false;
        FeatureAttributeDescriptor[] atts = src_ft.getAttributeDescriptors();
        String orig_name = null;
        String dbf_name = null;
        for (int i=0; i<atts.length; i++) {
            if (atts[i].getType() != org.gvsig.fmap.geom.DataTypes.GEOMETRY) {

                Object val = null;

                try {
                	orig_name = atts[i].getName();
                	dbf_name = origNameToDbfName.get(orig_name);
                	if (dbf_name == null) {
                		dbf_name = orig_name;
                	}
                    val = feat.get(orig_name);
                    if (val != null) {
                        edit_f.set(dbf_name, val);
                    }
                } catch (Exception ex) {
                    logger.info("Error while getting/setting value", ex);
                    error = true;
                }
            }
        }
        return error;
    }

    private Geometry union(List<Geometry> geoms)
        throws GeometryOperationNotSupportedException, GeometryOperationException {

        if (geoms == null || geoms.size() == 0) {
            return null;
        }

        if (geoms.size() == 1) {
            return geoms.get(0);
        }

        Geometry resp = geoms.get(0);
        for (int i=1; i<geoms.size(); i++) {
            resp = resp.union(geoms.get(i));
        }
        return resp;
    }
    /**
     * @param feat_geom_type
     * @param type must be POINT, LINE or POLYGON (Geometry.TYPES)
     * @return
     */
    private List<Geometry> getGeometriesFrom(Geometry in_geom, int type) {

        List<Geometry> resp = new ArrayList<Geometry>();
        Aggregate agg = null;

        /*
         * If input geometry is aggregate, search its
         * primitives
         */
        if (in_geom instanceof Aggregate) {
            agg = (Aggregate) in_geom;
            Geometry item = null;
            List<Geometry> add_parts = new ArrayList<Geometry>();
            for (int i=0; i<agg.getPrimitivesNumber(); i++) {
                item = agg.getPrimitiveAt(i);
                add_parts = getGeometriesFrom(item, type);
                resp.addAll(add_parts);
            }
            return resp;
        }

        // ============================================

        switch (type) {
        case Geometry.TYPES.POINT:
            if (in_geom instanceof Point) {
                resp.add(in_geom);
            }
            // =======================================================
            break;
        case Geometry.TYPES.CURVE:
            if (in_geom instanceof Curve) {
                resp.add(in_geom);
            }
            // =======================================================
            break;
        case Geometry.TYPES.SURFACE:
            if (in_geom instanceof Surface) {
                resp.add(in_geom);
            }
            // =======================================================
            break;
        }
        return resp;
    }

    private void finishAction(String layerName, File shp_file, IProjection proj)
    throws ExporttoServiceException {

        if (exporttoServiceFinishAction != null) {

            /*
             * Export is done. We notify with a SHPStoreParameters,
             * not with the NewSHPParameters we have used:
             */
        	DataStoreParameters shp_params = null;
            try {
	            shp_params = DALLocator.getDataManager().createStoreParameters("Shape");
	            shp_params.setDynValue("shpfile", shp_file);
	            shp_params.setDynValue("CRS", proj);

                shp_params.validate();
            } catch (Exception e) {
                throw new ExporttoServiceException(e);
            }
            exporttoServiceFinishAction.finished(layerName, shp_params);
        }
    }

    public void setFinishAction(
        ExporttoServiceFinishAction exporttoServiceFinishAction) {
        this.exporttoServiceFinishAction = exporttoServiceFinishAction;
    }
}
