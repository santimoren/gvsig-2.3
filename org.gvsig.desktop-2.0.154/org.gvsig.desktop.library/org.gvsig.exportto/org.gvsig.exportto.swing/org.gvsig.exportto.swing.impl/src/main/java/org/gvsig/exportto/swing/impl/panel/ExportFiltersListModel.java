/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExportFiltersListModel implements ListModel {

    private static final ExporttoManager EXPORTTO_MANAGER = ExporttoLocator
        .getManager();
    @SuppressWarnings("rawtypes")
    private List filters = null;
    protected DefaultJExporttoServicePanel exporttoServicePanel = null;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ExportFiltersListModel(
        DefaultJExporttoServicePanel exporttoServicePanel) {
        super();
        this.exporttoServicePanel = exporttoServicePanel;
        filters = new ArrayList();
        Iterator it = EXPORTTO_MANAGER.getPredefinedFilters();
        while (it.hasNext()) {
            filters.add(new Filter((String) it.next()));
        }
    }

    public int getSize() {
        return filters.size();
    }

    public Object getElementAt(int index) {
        return filters.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        // TODO Auto-generated method stub

    }

    public void removeListDataListener(ListDataListener l) {
        // TODO Auto-generated method stub

    }

    class Filter {

        private String filterKey;
        private String filterTanslation;

        public Filter(String filterKey) {
            super();
            this.filterKey = filterKey;
            this.filterTanslation =
                exporttoServicePanel.getExporttoSwingManager().getTranslation(
                    filterKey);
        }

        /**
         * @return the filterKey
         */
        public String getFilterKey() {
            return filterKey;
        }

        public String toString() {
            return filterTanslation;
        }

    }

}
