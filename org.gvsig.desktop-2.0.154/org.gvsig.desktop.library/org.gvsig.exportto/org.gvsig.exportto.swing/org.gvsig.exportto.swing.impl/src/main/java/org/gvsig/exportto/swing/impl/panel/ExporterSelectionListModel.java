/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;

/**
 * {@link ListModel} with the list of exportto provider factories.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporterSelectionListModel implements ListModel {

    private List<ExporttoSwingProviderFactory> exporters = null;

    public ExporterSelectionListModel(int[] providerTypes) {
        super();
        ExporttoSwingProviderManager exporttoProviderManager =
            ExporttoSwingProviderLocator.getManager();
        List<ExporttoSwingProviderFactory> allExporters =
            exporttoProviderManager.getProviderFactories(providerTypes);
        exporters = new ArrayList<ExporttoSwingProviderFactory>();
        for (int i = 0; i < allExporters.size(); i++) {
            ExporttoSwingProviderFactory factory = allExporters.get(i);
            if (factory.isEnabled()) {
                exporters.add(factory);
            }
        }
    }

    public int getSize() {
        return exporters.size();
    }

    public Object getElementAt(int index) {
        return exporters.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        // Nothing to do
    }

    public void removeListDataListener(ListDataListener l) {
        // Nothing to do
    }
}
