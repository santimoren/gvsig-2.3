/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import javax.swing.JPanel;
import org.cresques.cts.IProjection;

import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.ExporttoWindowManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;
import org.gvsig.exportto.swing.impl.panel.MessagePanel;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

import static org.gvsig.exportto.swing.impl.panel.MessagePanel.showMessage;

/**
 * Default implementation of the {@link ExporttoSwingManager}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultExporttoSwingManager implements ExporttoSwingManager {

    private ExporttoManager manager;
    // private I18nManager i18nmanager = null;
    private ExporttoWindowManager windowManager;

    public DefaultExporttoSwingManager() {
        this.manager = ExporttoLocator.getManager();
        // this.i18nmanager = ToolsLocator.getI18nManager();
        this.windowManager = new DefaultExporttoWindowManager();
    }

    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection) {
        JExporttoServicePanel panel =
            new DefaultJExporttoServicePanel(this, featureStore, projection,
                null, new int[0]);
        return panel;
    }

    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection,
        ExporttoServiceFinishAction exporttoServiceFinishAction) {
        JExporttoServicePanel panel =
            new DefaultJExporttoServicePanel(this, featureStore, projection,
                exporttoServiceFinishAction, new int[0]);
        return panel;
    }

    public JExporttoServicePanel createExportto(FeatureStore featureStore,
        IProjection projection,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes) {
        return new DefaultJExporttoServicePanel(this, featureStore, projection,
            exporttoServiceFinishAction, providerTypes);
    }
    
    public JExporttoServicePanel createExportto(FLyrVect vlayer,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes) {
        
        return new DefaultJExporttoServicePanel(this, vlayer,
            exporttoServiceFinishAction, providerTypes);
    }
    

    public ExporttoManager getManager() {
        return this.manager;
    }

    public String getTranslation(String key) {
        return Messages.getText(key);
    }

    public void registerWindowManager(ExporttoWindowManager manager) {
        this.windowManager = manager;
    }

    public ExporttoWindowManager getWindowManager() {
        return this.windowManager;
    }

    public void showMessage(String title, String header, String html, Feature feature) {
        MessagePanel.showMessage(title, header, html, feature);
    }

    public void showMessage(String title, String header, Exception ex, Feature feature) {
        MessagePanel.showMessage(title, header, ex, feature);
    }
}
