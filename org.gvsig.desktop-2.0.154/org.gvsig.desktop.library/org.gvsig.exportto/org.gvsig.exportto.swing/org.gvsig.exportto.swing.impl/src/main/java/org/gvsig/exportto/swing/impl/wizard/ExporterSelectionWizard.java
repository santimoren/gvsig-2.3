/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.wizard;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.exportto.swing.impl.panel.ProviderSelectionPanel;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.tools.service.ServiceException;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExporterSelectionWizard extends ProviderSelectionPanel implements
    OptionPanel, ListSelectionListener {

    private static final long serialVersionUID = -2818813233378801919L;
    private static final ExporttoSwingManager EXPORTTO_SWING_MANAGER =
        ExporttoSwingLocator.getSwingManager();
    private static final ExporttoSwingProviderManager EXPORTTO_SWING_PROVIDER_MANAGER =
        ExporttoSwingProviderLocator.getManager();
    private static final Logger LOG = LoggerFactory
        .getLogger(ExporterSelectionWizard.class);

    public ExporterSelectionWizard(
        DefaultJExporttoServicePanel exporttoServicePanel, int[] providerTypes) {
        super(exporttoServicePanel, providerTypes);
        this.exporttoProviderList.addListSelectionListener(this);
    }

    public String getPanelTitle() {
        return exporttoServicePanel.getExporttoSwingManager().getTranslation(
            "select_format_to_export");
    }

    public void nextPanel() throws NotContinueWizardException {
        ExporttoSwingProviderFactory providerFactory = this.getSelectedProvider();

        if (providerFactory != null) {
            try {
                exporttoServicePanel.selectExporttoSwingProvider(providerFactory);
            } catch (ServiceException e) {
                LOG.error("Not possible to create a exporto swing provider", e);
                throw new NotContinueWizardException(
                    EXPORTTO_SWING_MANAGER
                        .getTranslation("execute_adding_error"),
                    e, this);
            }
        }
    }

    public void lastPanel() {
        // Nothing to do
    }

    public void updatePanel() {
        // Nothing to do
    }

    public JPanel getJPanel() {
        return this;
    }

    public void valueChanged(ListSelectionEvent e) {
        ExporttoSwingProviderFactory providerFactory = this.getSelectedProvider();

        if (providerFactory != null) {
            descriptionText.setText(EXPORTTO_SWING_PROVIDER_MANAGER
                .getDescription(providerFactory.getName()));

            exporttoServicePanel.setNextButtonEnabled(true);
        }
    }
}
