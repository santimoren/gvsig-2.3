/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.swing.impl.preferences.DefaultExporttoPreferencesComponent;
import org.gvsig.exportto.swing.preferences.ExporttoSwingPreferencesComponent;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.AbstractProviderManager;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderFactory;
import org.gvsig.tools.service.spi.ProviderServices;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultExporttoSwingProviderManager extends
    AbstractProviderManager implements ExporttoSwingProviderManager {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultExporttoSwingProviderManager.class);

    private static final String PROVIDERS_NAME = "Exportto.swing.providers";
    private static final String PROVIDERS_DESCRIPTION =
        "Exportto swing providers";

    private static final String PARAMETER_FEATURESTORE = "FeatureStore";
    private static final String PARAMETER_PROJECTION = "Projection";
    private static final String PARAMETER_LAYER = "FLyrVect";

    private Map<ExporttoSwingProviderFactory, Boolean> providerStatus;

    public DefaultExporttoSwingProviderManager() {
        super();
        providerStatus = new HashMap<ExporttoSwingProviderFactory, Boolean>();
    }

    public ProviderServices createProviderServices(Service service) {
        return new DefaultExporttoSwingProviderServices();
    }

    public List<String> getProviderNames() {
        return getProviderNames(new int[0]);
    }

    public List<String> getProviderNames(int[] providerTypes) {
        ExtensionPointManager extensionPointManager =
            ToolsLocator.getExtensionPointManager();
        ExtensionPoint extensionPoint =
            extensionPointManager.get(this.getRegistryKey());
        List<String> providers = new ArrayList<String>();
        if (extensionPoint != null) {
            @SuppressWarnings("unchecked")
            Iterator<Extension> it = extensionPoint.iterator();
            while (it.hasNext()) {
                Extension extension = it.next();
                try {
                    ExporttoSwingProviderFactory exporttoSwingProviderFactory =
                        (ExporttoSwingProviderFactory) extension.create();
                    if (providerTypes.length == 0) {
                        providers.add(exporttoSwingProviderFactory.getName());
                    } else {
                        boolean areSupported = true;
                        for (int i = 0; i < providerTypes.length; i++) {
                            if (!exporttoSwingProviderFactory
                                .support(providerTypes[i])) {
                                areSupported = false;
                                break;
                            }
                        }
                        if (areSupported) {
                            providers.add(exporttoSwingProviderFactory
                                .getName());
                        }
                    }
                } catch (InstantiationException e) {
                    LOG.error(
                        "Not possible to create the exportto provider factory",
                        e);
                } catch (IllegalAccessException e) {
                    LOG.error(
                        "Not possible to create the exportto provider factory",
                        e);
                } catch (ServiceException e) {
                    LOG.error(
                        "Not possible to check if the provider support a format",
                        e);
                }
            }
        }
        return providers;
    }

    public List<ExporttoSwingProviderFactory> getProviderFactories() {
        return getProviderFactories(null);
    }

    public List<ExporttoSwingProviderFactory> getProviderFactories(
        int[] providerTypes) {
        ExtensionPointManager extensionPointManager =
            ToolsLocator.getExtensionPointManager();
        ExtensionPoint extensionPoint =
            extensionPointManager.get(this.getRegistryKey());
        List<ExporttoSwingProviderFactory> providers =
            new ArrayList<ExporttoSwingProviderFactory>();
        if (extensionPoint != null) {
            @SuppressWarnings("unchecked")
            Iterator<Extension> it = extensionPoint.iterator();
            while (it.hasNext()) {
                Extension extension = it.next();
                try {
                    ExporttoSwingProviderFactory exporttoSwingProviderFactory =
                        (ExporttoSwingProviderFactory) extension.create();
                    if (providerTypes == null || providerTypes.length == 0) {
                        providers.add(exporttoSwingProviderFactory);
                    } else {
                        boolean areSupported = true;
                        for (int i = 0; i < providerTypes.length; i++) {
                            if (!exporttoSwingProviderFactory
                                .support(providerTypes[i])) {
                                areSupported = false;
                                break;
                            }
                        }
                        if (areSupported) {
                            providers.add(exporttoSwingProviderFactory);
                        }
                    }
                } catch (InstantiationException e) {
                    LOG.error(
                        "Not possible to create the exportto provider factory",
                        e);
                } catch (IllegalAccessException e) {
                    LOG.error(
                        "Not possible to create the exportto provider factory",
                        e);
                } catch (ServiceException e) {
                    LOG.error(
                        "Not possible to check if the provider support a format",
                        e);
                }
            }
        }
        return providers;
    }

    protected String getRegistryDescription() {
        return PROVIDERS_DESCRIPTION;
    }

    protected String getRegistryKey() {
        return PROVIDERS_NAME;
    }

    public ExporttoSwingProvider createExporttoSwingProvider(
        String providerName, FeatureStore featureStore, IProjection projection)
        throws ServiceException {
        DynObject serviceParameters = createServiceParameters(providerName);
        serviceParameters.setDynValue(PARAMETER_FEATURESTORE, featureStore);
        try {
            serviceParameters.setDynValue(PARAMETER_PROJECTION, projection);
        } catch (DynFieldNotFoundException e) {
            LOG.info("This provider doesn't accept projection", e);
        }
        Provider provider =
            createProvider(serviceParameters,
                new DefaultExporttoSwingProviderServices());
        if (!(provider instanceof ExporttoSwingProvider)) {
            throw new NotExporttoSwingProviderException(providerName);
        }
        return (ExporttoSwingProvider) provider;
    }
    
    
    public ExporttoSwingProvider createExporttoSwingProvider(
        String providerName, FLyrVect vlayer)
        throws ServiceException {
        
        FeatureStore fstore = vlayer.getFeatureStore();
        
        IProjection proj = null;
        ICoordTrans ct = vlayer.getCoordTrans();
        if (ct == null) {
            // Layer is not reprojected on the fly
            proj = vlayer.getProjection(); 
        } else {
            /*
             * If there is on-the-fly reprojection, then
             * the projection of the export process is the
             * destination projection of the transformation:
             */
            proj = ct.getPDest();
        }
            
        DynObject serviceParameters = createServiceParameters(providerName);
        serviceParameters.setDynValue(PARAMETER_FEATURESTORE, fstore);
        try {
            serviceParameters.setDynValue(PARAMETER_PROJECTION, proj);
        } catch (DynFieldNotFoundException e) {
            LOG.info("This provider doesn't accept projection");
        }
        try {
            serviceParameters.setDynValue(PARAMETER_LAYER, vlayer);
        } catch (DynFieldNotFoundException e) {
            LOG.info("This provider doesn't accept layer");
        }
        // ===========================================================
        Provider provider =
            createProvider(serviceParameters,
                new DefaultExporttoSwingProviderServices());
        if (!(provider instanceof ExporttoSwingProvider)) {
            throw new NotExporttoSwingProviderException(providerName);
        }
        return (ExporttoSwingProvider) provider;
    }

    
    
    

    public boolean support(String providerName, int providerType)
        throws ServiceException {
        ExtensionPointManager epm = ToolsLocator.getExtensionPointManager();
        ExtensionPoint ep = epm.get(this.getRegistryKey());
        ExporttoSwingProviderFactory exporttoSwingProviderFactory = null;

        if (ep != null) {
            try {
                exporttoSwingProviderFactory =
                    ((ExporttoSwingProviderFactory) ep.create(providerName));
                return exporttoSwingProviderFactory.support(providerType);
            } catch (InstantiationException e) {
                LOG.error(
                    "Not possible to create the exportto provider factory", e);
            } catch (IllegalAccessException e) {
                LOG.error(
                    "Not possible to create the exportto provider factory", e);
            }
        }
        return false;
    }

    public void addProviderFactory(ProviderFactory providerFactory) {
        if (providerFactory == null) {
            throw new IllegalArgumentException(
                "ProviderFactory cannot be null.");
        }

        if (!(providerFactory instanceof ExporttoSwingProviderFactory)) {
            throw new IllegalArgumentException(providerFactory.getName()
                + " must implement the ExporttoSwingProviderFactory interface");
        }
        super.addProviderFactory(providerFactory);
    }

    public String getDescription(String providerName) {
        try {
            return getExporttoSwingProviderFactory(providerName)
                .getDescription();
        } catch (ServiceException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isProviderEnabled(ExporttoSwingProviderFactory factory) {
        Boolean status = providerStatus.get(factory);
        return status == null ? true : status.booleanValue();
    }

    public void enableProvider(ExporttoSwingProviderFactory factory, boolean value) {
        providerStatus.put(factory, Boolean.valueOf(value));
    }

    public ExporttoSwingProviderFactory getExporttoSwingProviderFactory(
        String name) throws ServiceException {
        return (ExporttoSwingProviderFactory) getProviderFactory(name);
    }

    public ExporttoSwingPreferencesComponent createExporttoSwingProvidersPreferences() {
        return new DefaultExporttoPreferencesComponent();
    }
}
