/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExportFilterPanel extends ExportFilterPanelLayout {
    private static final long serialVersionUID = -2450969034747572624L;
    
    private static final Logger logger = LoggerFactory.getLogger(ExportFilterPanel.class);

    private FeatureStore store; 
    
    public ExportFilterPanel(DefaultJExporttoServicePanel exporttoServicePanel) {
        this.store = exporttoServicePanel.getFeatureStore();
        this.initComponents();
    }

    private static class FilterItem {
        private final String label;
        private final String value;
        public FilterItem(String label, String value) {
            this.label=label;
            this.value=value;
        }
        @Override
        public String toString() {
            return this.label;
        }
    }
    
    private void initComponents() {
        ExporttoManager manager = ExporttoLocator.getManager();
        
        DefaultListModel model = new DefaultListModel();
        Iterator filterNames = manager.getPredefinedFilters();
        while( filterNames.hasNext() ) {
            String name = (String) filterNames.next();
            String expresion = manager.getFilter(name);
            model.addElement(new FilterItem(name, expresion));
        }
        this.lstFilters.setModel(model);
        this.lstFilters.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent lse) {
                doSelectFilter();
            }
        });
        
        ActionListener changeOptionAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doChangeOption();
            }
        };
        this.rdbAllRows.addActionListener(changeOptionAction);
        this.rdbFilteredRows.addActionListener(changeOptionAction);
        this.rdbSelectedRows.addActionListener(changeOptionAction);
        this.rdoUseNewExpresion.addActionListener(changeOptionAction);
        this.rdoUseSavedExpresion.addActionListener(changeOptionAction);
        
        this.butTest.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doTest();
            }
        });
        this.rdbAllRows.setSelected(true);
        doChangeOption();
        this.butFilterDialog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doShowFilterDialog();
            }
        });
        this.translate();
    }

    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.rdbAllRows.setText(i18nManager.getTranslation("_Todos_los_registros"));
        this.rdbSelectedRows.setText(i18nManager.getTranslation("_Los_registros_seleccionados"));
        this.rdbFilteredRows.setText(i18nManager.getTranslation("_Los_registros_que_cumplan_el_critrio_seleccionado"));
        this.lblName.setText(i18nManager.getTranslation("_Nombre_de_la_expresion_opcinal"));
        this.lblExpresion.setText(i18nManager.getTranslation("_Expresion"));
        this.txtName.setToolTipText(i18nManager.getTranslation("_Type_here_the_name_of_the_expression_if_you_want_to_save_it_otherwise_leave_it_blank"));
        this.butTest.setText(i18nManager.getTranslation("_Test"));
        this.lblHeader.setText(i18nManager.getTranslation("_Indique_que_registros_desea_exportar"));
        this.rdoUseSavedExpresion.setText(i18nManager.getTranslation("_Usar_una_expresion_guardada"));
        this.rdoUseNewExpresion.setText(i18nManager.getTranslation("_Nueva_expresion"));
        this.butFilterDialog.setText(i18nManager.getTranslation("_Filtro"));        
    }
    
    private void doShowFilterDialog() {
        
    }

    protected void doSelectFilter() {
        FilterItem item = (FilterItem) this.lstFilters.getSelectedValue();
        if( item!=null ) {
            this.txtExpresion.setText(item.value);
            this.txtName.setText(item.label);
        }
    }
    
    private static class Data implements EvaluatorData {
        private final DynObject data;
        
        public Data(DynObject data) {
            this.data = data;
        }
        public Object getDataValue(String name) {
            if( this.data.getDynClass().getDynField(name)!=null ) {
                return this.data.getDynValue(name);
            }
            if( "defaultgeometry".equalsIgnoreCase(name) ) {
                // FIXME: deberia crear una geometris de agun tipo
                return null;
            }
            throw new RuntimeException("Identifier '"+name+"'not defined.");
        }

        public Object getContextValue(String name) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Iterator getDataValues() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Iterator getDataNames() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean hasDataValue(String name) {
            if( "defaultgeometry".equalsIgnoreCase(name) ) {
                return true;
            }
            return  this.data.hasDynValue(name);
        }

        public boolean hasContextValue(String name) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
            
    protected void doTest() {
        try {
            this.checkPanel(false);
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            JOptionPane.showMessageDialog(this,
                    i18nManager.getTranslation("_Expresion_correcta"),
                    i18nManager.getTranslation("_Filter"),
                    JOptionPane.INFORMATION_MESSAGE
            );
        } catch (NotContinueWizardException ex) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(), 
                    i18nManager.getTranslation("_Warning"),
                    JOptionPane.WARNING_MESSAGE
            );
        }
    }
    
    protected void doChangeOption(){
        if( this.rdbFilteredRows.isSelected() ) {
            this.rdoUseNewExpresion.setEnabled(true);
            this.rdoUseSavedExpresion.setEnabled(true);
        
            if( this.rdoUseSavedExpresion.isSelected() ) {
                this.lstFilters.setEnabled(true);
                this.txtName.setEnabled(false);
                this.txtExpresion.setEnabled(false);
                this.butTest.setEnabled(false);
                this.butFilterDialog.setEnabled(false);
            } else {
                this.rdoUseNewExpresion.setSelected(false);
                
                this.lstFilters.setEnabled(false);
                this.txtName.setEnabled(true);
                this.txtExpresion.setEnabled(true);
                this.butTest.setEnabled(true);
                this.butFilterDialog.setEnabled(true);
            }

        } else if( this.rdbSelectedRows.isSelected() ) {
            this.lstFilters.setEnabled(false);
            this.txtName.setEnabled(false);
            this.txtExpresion.setEnabled(false);
            this.butTest.setEnabled(false);
            this.butFilterDialog.setEnabled(false);
            this.rdoUseNewExpresion.setEnabled(false);
            this.rdoUseSavedExpresion.setEnabled(false);

        } else {
            this.rdbAllRows.setSelected(true);
            
            this.lstFilters.setEnabled(false);
            this.txtName.setEnabled(false);
            this.txtExpresion.setEnabled(false);
            this.butTest.setEnabled(false);
            this.butFilterDialog.setEnabled(false);
            this.rdoUseNewExpresion.setEnabled(false);
            this.rdoUseSavedExpresion.setEnabled(false);
            
        }
    } 
    
    public boolean isFullLayerSelected() {
        return this.rdbAllRows.isSelected();
    }

    public boolean isSelectedFeaturesSelected() {
        return this.rdbSelectedRows.isSelected();
    }

    public boolean isPersonalizedFilterSelected() {
        return this.rdbFilteredRows.isSelected();
    }

    public void checkPanel()  throws NotContinueWizardException {
        checkPanel(true);
    }
    
    public void checkPanel(boolean save)  throws NotContinueWizardException {
        if( this.rdbFilteredRows.isSelected() ) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            String expresion = this.getFilterExpresion();
            if( expresion == null ) {
                throw new NotContinueWizardException(
                        i18nManager.getTranslation("_The_expresion_is_empty_Check_other_option_or_enter_a_expresion"), 
                        this, true
                );
            }
            Evaluator filter = this.getFilter();
            if( filter == null ) {
                throw new NotContinueWizardException(
                        i18nManager.getTranslation("_Problems_compiling_the_expesion_Check_the_sintax_Remember_use_SQL_expresion_sintax"), 
                        this, true
                );
            }
            DynObjectManager dynobjmanager = ToolsLocator.getDynObjectManager();
            try {
                DynObject values = dynobjmanager.createDynObject(this.store.getDefaultFeatureType());
                filter.evaluate(new Data(values));
            } catch (EvaluatorException ex) {
                throw new NotContinueWizardException(
                        i18nManager.getTranslation("_Check_the_sintax_Remember_use_SQL_expresion_sintax")
                        + "\n"
                        + ex.getCause().getMessage(), 
                        this, true
                );
            } catch (DataException ex) {
                logger.warn("Can't test expresion",ex);
                throw new NotContinueWizardException(
                        i18nManager.getTranslation("_Problems_to_create_a_data_set_to_test_the_expresion_See_register_for_more_information"), 
                        this, true
                );
            }
            if( save ) {
                String filterName = this.getFilterName();
                if( filterName!=null ) {
                    ExporttoManager manager = ExporttoLocator.getManager();
                    manager.addFilter(filterName, expresion);
                }
            }
        }
    }

    public Evaluator getFilter() {
        String filter = this.getFilterExpresion();
        if( filter == null ) {
            return null;
        }
        DataManager datamanager = DALLocator.getDataManager();
        try {
            Evaluator evaluator = datamanager.createExpresion(filter);
            return evaluator;
        } catch (InitializeException ex) {
            // Error de sintaxis en la expresion ???
            return null;
        }
    }

    public String getFilterExpresion() {
        String s = this.txtExpresion.getText();
        if( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.trim();
    }

    public String getFilterName() {
        String s = this.txtName.getText();
        if( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.trim();
    }

}
