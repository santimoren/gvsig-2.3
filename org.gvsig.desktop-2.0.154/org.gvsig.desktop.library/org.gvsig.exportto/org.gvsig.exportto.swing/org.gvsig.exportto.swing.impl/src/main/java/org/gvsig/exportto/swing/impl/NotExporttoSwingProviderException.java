/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.exportto.swing.impl;

import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.tools.service.ServiceException;

/**
 * Exception thrown when the provider doesn't implements the
 * {@link ExporttoSwingProvider} interface
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class NotExporttoSwingProviderException extends ServiceException {

    private static final long serialVersionUID = -2159095938700834790L;

    private final static String MESSAGE_KEY =
        "_ExporttoProviderNotRegisteredException";
    private final static String MESSAGE_FORMAT =
        "The provider '%(providerName)' does not implement the ExporttoSwingProvider interface";

    /**
     * Creates a new {@link NotExporttoSwingProviderException}.
     * 
     * @param name
     *            the name of the provider that doesn't implements the
     *            {@link ExporttoSwingProvider} interface
     */
    public NotExporttoSwingProviderException(String name) {
        super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
        setValue("providerName", name);
    }
}
