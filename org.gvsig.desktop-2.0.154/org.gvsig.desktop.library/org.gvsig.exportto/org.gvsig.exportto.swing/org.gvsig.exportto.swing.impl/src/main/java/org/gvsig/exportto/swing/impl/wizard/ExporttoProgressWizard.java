/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.wizard;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.task.JTaskStatus;
import org.gvsig.tools.swing.api.task.TaskStatusSwingManager;
import org.gvsig.tools.task.TaskStatus;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExporttoProgressWizard extends JPanel implements OptionPanel {

    private static final long serialVersionUID = 8235569247892299856L;
    private static final TaskStatusSwingManager TASK_STATUS_SWING_MANAGER =
        ToolsSwingLocator.getTaskStatusSwingManager();

    private DefaultJExporttoServicePanel exporttoServicePanel = null;
    private JTaskStatus jTaskStatus = null;

    public ExporttoProgressWizard(
        DefaultJExporttoServicePanel exporttoServicePanel) {
        super();
        this.exporttoServicePanel = exporttoServicePanel;
        jTaskStatus = TASK_STATUS_SWING_MANAGER.createJTaskStatus();
        this.setLayout(new BorderLayout());
        this.add(jTaskStatus, BorderLayout.NORTH);
    }

    public String getPanelTitle() {
        return exporttoServicePanel.getExporttoSwingManager().getTranslation(
            "export_progress");
    }

    public void nextPanel() throws NotContinueWizardException {
        // TODO Auto-generated method stub

    }

    public void lastPanel() {
        exporttoServicePanel.setCancelButtonText(exporttoServicePanel
            .getExporttoSwingManager().getTranslation("cancel"));
    }

    public void updatePanel() {
        exporttoServicePanel.setExportButtonEnabled(false);
        exporttoServicePanel.setCancelButtonText(exporttoServicePanel
            .getExporttoSwingManager().getTranslation("close"));
    }

    public JPanel getJPanel() {
        return this;
    }

    public void bind(TaskStatus taskStatus) {
        this.jTaskStatus.bind(taskStatus);
    }
}
