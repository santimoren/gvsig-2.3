/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.wizard;

import javax.swing.JPanel;

import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.exportto.swing.impl.panel.ExportFilterPanel;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExportFilterWizard extends ExportFilterPanel implements
    OptionPanel {

    private static final long serialVersionUID = 8801849211055890046L;
    private DefaultJExporttoServicePanel exporttoServicePanel;

    public ExportFilterWizard(DefaultJExporttoServicePanel exporttoServicePanel) {
        super(exporttoServicePanel);
        this.exporttoServicePanel = exporttoServicePanel;
    }

    public String getPanelTitle() {
        return exporttoServicePanel.getExporttoSwingManager().getTranslation(
            "exportto_options");
    }

    public void nextPanel() throws NotContinueWizardException {
        this.checkPanel();
    }

    public void lastPanel() {
        exporttoServicePanel.setExportButtonEnabled(false);
        exporttoServicePanel.setNextButtonEnabled(true);
    }

    public void updatePanel() {
        exporttoServicePanel.setExportButtonEnabled(true);
        exporttoServicePanel.setNextButtonEnabled(false);
    }

    public JPanel getJPanel() {
        return this;
    }

}
