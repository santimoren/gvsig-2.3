/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.app.gui.panels.CRSSelectPanelFactory;
import org.gvsig.exportto.swing.impl.DefaultJExporttoServicePanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class ExportTargetProjectionPanel extends ExportTargetProjectionPanelLayout {

    /**
     *
     */
    private static final long serialVersionUID = -2164424159712152267L;

    private static final Logger logger = LoggerFactory.getLogger(ExportTargetProjectionPanel.class);

    private IProjection originalProjection;
    private IProjection viewProjection;
    private IProjection userProjection;
    private IProjection selectedProjection;
    private CRSSelectPanel crsSelectPanel;

    /**
     * @param exporttoServicePanel
     * @param vectorLayer
     */
    public ExportTargetProjectionPanel(DefaultJExporttoServicePanel exporttoServicePanel) {
        viewProjection=null;
        originalProjection=null;
        if (originalProjection==null){
            FeatureStore store = exporttoServicePanel.getFeatureStore();
            try {
                this.originalProjection = store.getDefaultFeatureType().getDefaultSRS();
            } catch (DataException e) {
                logger.warn("Couldn't get original projection from feature store");
            }
        }
        this.initComponents();
    }

    /**
     * Sets the vector layer and sets its projections
     * @param vectorLayer
     */
    public void setVectorLayer(FLyrVect vectorLayer){
        if (vectorLayer!=null){
            ICoordTrans ct = vectorLayer.getCoordTrans();
            if (ct!=null){
                viewProjection=ct.getPDest();
                originalProjection=ct.getPOrig();
            }
        }
        initComponents();
    }

    private void initComponents() {
        selectedProjection=null;

        if (originalProjection==null){
            rdbtnOriginalProjection.setEnabled(false);
            lblRdbtnOriginalProjection.setEnabled(false);
            lblOriginalProjection.setText("");
        }else{
            rdbtnOriginalProjection.setEnabled(true);
            lblRdbtnOriginalProjection.setEnabled(true);
            lblOriginalProjection.setText(originalProjection.getAbrev());
            this.rdbtnOriginalProjection.setSelected(true);
        }

        if (viewProjection==null){
            rdbtnViewProjection.setEnabled(false);
            lblRdbtnViewProjection.setEnabled(false);
            lblViewProjection.setText("");
        }else{
            rdbtnViewProjection.setEnabled(true);
            lblRdbtnViewProjection.setEnabled(true);
            lblViewProjection.setText(viewProjection.getAbrev());
            this.rdbtnViewProjection.setSelected(true);
        }

        ActionListener changeOptionAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doChangeOption();
            }
        };
        this.rdbtnViewProjection.addActionListener(changeOptionAction);
        this.rdbtnOriginalProjection.addActionListener(changeOptionAction);
        this.rdbtnUserProjection.addActionListener(changeOptionAction);

        this.crsSelectPanel=getJPanelProj();
        this.jPanelProj.add(crsSelectPanel);

        doChangeOption();
        this.translate();
    }

    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        this.lblSelectTargetProjection.setText(i18nManager.getTranslation(lblSelectTargetProjection.getText()));
        this.lblSelectTargetProjection.setToolTipText(i18nManager.getTranslation(lblSelectTargetProjection.getToolTipText()));

        this.lblRdbtnOriginalProjection.setText(i18nManager.getTranslation(lblRdbtnOriginalProjection.getText()));
        this.lblRdbtnOriginalProjection.setToolTipText(i18nManager.getTranslation(lblRdbtnOriginalProjection.getToolTipText()));
        this.rdbtnOriginalProjection.setToolTipText(i18nManager.getTranslation(rdbtnOriginalProjection.getToolTipText()));

        this.lblRdbtnViewProjection.setText(i18nManager.getTranslation(lblRdbtnViewProjection.getText()));
        this.lblRdbtnViewProjection.setToolTipText(i18nManager.getTranslation(lblRdbtnViewProjection.getToolTipText()));
        this.rdbtnViewProjection.setToolTipText(i18nManager.getTranslation(rdbtnViewProjection.getToolTipText()));

        this.lblRdbtnUserProjection.setText(i18nManager.getTranslation(lblRdbtnUserProjection.getText()));
        this.lblRdbtnUserProjection.setToolTipText(i18nManager.getTranslation(lblRdbtnUserProjection.getToolTipText()));
        this.rdbtnUserProjection.setToolTipText(i18nManager.getTranslation(rdbtnUserProjection.getToolTipText()));

        this.jPanelProj.setToolTipText(i18nManager.getTranslation(jPanelProj.getToolTipText()));
        this.crsSelectPanel.getJLabel().setText(i18nManager.getTranslation("selected_projection"));
    }

    protected void doChangeOption(){
        if( this.rdbtnOriginalProjection.isSelected() ) {
            this.jPanelProj.setEnabled(false);
            selectedProjection=originalProjection;

        } else if( this.rdbtnViewProjection.isSelected() ) {
            this.jPanelProj.setEnabled(false);
            selectedProjection=viewProjection;

        } else if( this.rdbtnUserProjection.isSelected() ) {
            this.jPanelProj.setEnabled(true);
            selectedProjection=userProjection;
        }
    }

    protected boolean checkPanel(){
        return selectedProjection!=null;
    }

    private CRSSelectPanel getJPanelProj() {
        if (crsSelectPanel == null) {
            if (viewProjection!=null){
                crsSelectPanel = CRSSelectPanelFactory.getPanel(viewProjection);
            }else{
                crsSelectPanel = CRSSelectPanelFactory.getPanel(originalProjection);
            }
            userProjection=crsSelectPanel.getCurProj();
            crsSelectPanel.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (crsSelectPanel.isOkPressed()) {
                        userProjection=crsSelectPanel.getCurProj();
                        selectedProjection=userProjection;
                        rdbtnUserProjection.setSelected(true);
                    }
                }
            });
        }
        return crsSelectPanel;
    }

    /**
     * Returns the target projection to export
     * @return
     */
    public IProjection getTargetProjection(){
        return selectedProjection;
    }

}
