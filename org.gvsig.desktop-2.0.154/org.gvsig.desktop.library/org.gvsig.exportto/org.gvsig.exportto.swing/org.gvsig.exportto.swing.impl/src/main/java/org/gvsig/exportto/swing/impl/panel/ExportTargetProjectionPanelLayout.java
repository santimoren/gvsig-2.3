package org.gvsig.exportto.swing.impl.panel;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class ExportTargetProjectionPanelLayout extends JPanel
{
   JLabel lblSelectTargetProjection = new JLabel();
   JRadioButton rdbtnOriginalProjection = new JRadioButton();
   ButtonGroup btngrpSelection = new ButtonGroup();
   JLabel lblRdbtnOriginalProjection = new JLabel();
   JLabel lblOriginalProjection = new JLabel();
   JRadioButton rdbtnViewProjection = new JRadioButton();
   JLabel lblRdbtnViewProjection = new JLabel();
   JLabel lblViewProjection = new JLabel();
   JRadioButton rdbtnUserProjection = new JRadioButton();
   JLabel lblRdbtnUserProjection = new JLabel();
   JPanel jPanelProj = new JPanel();

   /**
    * Default constructor
    */
   public ExportTargetProjectionPanelLayout()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:GROW(1.0),FILL:4DLU:NONE","CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,FILL:DEFAULT:NONE,CENTER:2DLU:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblSelectTargetProjection.setName("lblSelectTargetProjection");
      lblSelectTargetProjection.setText("select_target_projection");
      lblSelectTargetProjection.setToolTipText("select_target_projection");
      jpanel1.add(lblSelectTargetProjection,cc.xywh(2,2,4,1));

      rdbtnOriginalProjection.setActionCommand("original_projection	");
      rdbtnOriginalProjection.setName("rdbtnOriginalProjection");
      rdbtnOriginalProjection.setToolTipText("projection_from_source_layer_before_view_reprojection");
      btngrpSelection.add(rdbtnOriginalProjection);
      jpanel1.add(rdbtnOriginalProjection,cc.xy(3,4));

      lblRdbtnOriginalProjection.setName("lblRdbtnOriginalProjection");
      lblRdbtnOriginalProjection.setText("original_projection");
      lblRdbtnOriginalProjection.setToolTipText("projection_from_source_layer_before_view_reprojection");
      jpanel1.add(lblRdbtnOriginalProjection,cc.xy(5,4));

      lblOriginalProjection.setName("lblOriginalProjection");
      jpanel1.add(lblOriginalProjection,cc.xy(5,5));

      rdbtnViewProjection.setActionCommand("original_projection	");
      rdbtnViewProjection.setName("rdbtnViewProjection");
      rdbtnViewProjection.setToolTipText("projection_used_in_view");
      btngrpSelection.add(rdbtnViewProjection);
      jpanel1.add(rdbtnViewProjection,cc.xy(3,7));

      lblRdbtnViewProjection.setName("lblRdbtnViewProjection");
      lblRdbtnViewProjection.setText("view_projection");
      lblRdbtnViewProjection.setToolTipText("projection_used_in_view");
      jpanel1.add(lblRdbtnViewProjection,cc.xy(5,7));

      lblViewProjection.setName("lblViewProjection");
      jpanel1.add(lblViewProjection,cc.xy(5,8));

      rdbtnUserProjection.setActionCommand("original_projection	");
      rdbtnUserProjection.setName("rdbtnUserProjection");
      rdbtnUserProjection.setToolTipText("projection_selected_by_user");
      btngrpSelection.add(rdbtnUserProjection);
      jpanel1.add(rdbtnUserProjection,cc.xy(3,10));

      lblRdbtnUserProjection.setName("lblRdbtnUserProjection");
      lblRdbtnUserProjection.setText("user_selected_projection");
      lblRdbtnUserProjection.setToolTipText("projection_selected_by_user");
      jpanel1.add(lblRdbtnUserProjection,cc.xy(5,10));

      jPanelProj.setName("jPanelProj");
      jPanelProj.setToolTipText("projection_selector");
      jpanel1.add(jPanelProj,new CellConstraints(5,11,1,1,CellConstraints.LEFT,CellConstraints.DEFAULT));

      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5,6 },new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
