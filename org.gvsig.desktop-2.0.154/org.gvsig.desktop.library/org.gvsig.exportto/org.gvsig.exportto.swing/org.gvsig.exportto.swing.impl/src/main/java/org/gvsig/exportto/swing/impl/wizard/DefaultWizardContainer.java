/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.wizard;

import javax.swing.JPanel;
import org.gvsig.exportto.swing.impl.panel.MessagePanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.swing.spi.ExporttoPanelValidationException;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderPanel;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class DefaultWizardContainer implements OptionPanel {

    private static final Logger LOG = LoggerFactory
            .getLogger(DefaultWizardContainer.class);

    private ExporttoSwingProviderPanel exporttoSwingProviderPanel = null;

    public DefaultWizardContainer(
            ExporttoSwingProviderPanel exporttoSwingProviderPanel) {
        super();
        this.exporttoSwingProviderPanel = exporttoSwingProviderPanel;
    }

    public String getPanelTitle() {
        return exporttoSwingProviderPanel.getPanelTitle();
    }

    public void nextPanel() throws NotContinueWizardException {
        try {
            if (!this.exporttoSwingProviderPanel.isValidPanel()) {
                throw new NotContinueWizardException(
                        Messages.getText("_Invalid_values_in_form"),
                        exporttoSwingProviderPanel.asJComponent(),
                        false);
            }
        } catch (ExporttoPanelValidationException e) {
            LOG.info("Invalid form values.");
            throw new NotContinueWizardException(
                    Messages.getText("_Invalid_values_in_form"),
                    e, exporttoSwingProviderPanel.asJComponent());
        }
    }

    public void lastPanel() {
        // TODO Auto-generated method stub

    }

    public void updatePanel() {
        try {
            this.exporttoSwingProviderPanel.enterPanel();
        } catch (Exception ex) {
            LOG.warn("Fail the call to enterPanel.", ex);
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            MessagePanel.showMessage(
                    i18nManager.getTranslation("_Warning"),
                    i18nManager.getTranslation("_There_have_been_problems_filling_data_in_panel") +
                            " (" + this.exporttoSwingProviderPanel.getPanelTitle() +")" ,
                    ex,
                    null
            );
        }
    }

    public JPanel getJPanel() {
        return (JPanel) exporttoSwingProviderPanel.asJComponent();
    }

}
