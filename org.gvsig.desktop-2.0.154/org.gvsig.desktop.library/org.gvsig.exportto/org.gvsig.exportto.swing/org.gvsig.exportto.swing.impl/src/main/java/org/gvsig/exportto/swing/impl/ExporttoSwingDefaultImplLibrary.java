/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import java.util.Locale;

import org.gvsig.exportto.swing.ExporttoSwingLibrary;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.swing.api.ToolsSwingLibrary;

/**
 * Library for default swing implementation initialization and configuration.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class ExporttoSwingDefaultImplLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsImplementationOf(ExporttoSwingLibrary.class);
        this.require(ToolsSwingLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
        ExporttoSwingLocator
            .registerSwingManager(DefaultExporttoSwingManager.class);
        ExporttoSwingProviderLocator
            .registerManager(DefaultExporttoSwingProviderManager.class);

        if (!Messages.hasLocales()) {
            Messages.addLocale(Locale.getDefault());
        }
        Messages.addResourceFamily("org.gvsig.exportto.swing.impl.i18n.text",
            ExporttoSwingDefaultImplLibrary.class.getClassLoader(),
            ExporttoSwingDefaultImplLibrary.class.getClass().getName());
        IconThemeHelper.registerIcon("export-to", "wizard-export-to", this);
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        // Nothing to do
    }

}
