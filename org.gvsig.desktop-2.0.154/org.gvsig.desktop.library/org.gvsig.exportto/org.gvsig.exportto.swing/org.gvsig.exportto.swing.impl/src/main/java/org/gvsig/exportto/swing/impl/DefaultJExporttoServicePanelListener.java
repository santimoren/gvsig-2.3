/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.swing.JExporttoServicePanelListener;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultJExporttoServicePanelListener implements
    WizardPanelActionListener {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultJExporttoServicePanelListener.class);

    private DefaultJExporttoServicePanel exporttoServicePanel;
    private JExporttoServicePanelListener exporttoServicePanelListener = null;

    public DefaultJExporttoServicePanelListener(
        DefaultJExporttoServicePanel exporttoServicePanel) {
        super();
        this.exporttoServicePanel = exporttoServicePanel;
    }

    public void cancel(WizardPanel wizardPanel) {
        if (exporttoServicePanelListener != null) {
            exporttoServicePanelListener.close();
        }
        exporttoServicePanel.setStatus(JOptionPane.CANCEL_OPTION);
    }

    public void finish(WizardPanel wizardPanel) {
        // Start the export process
        try {
            exporttoServicePanel.export();
            exporttoServicePanel.setStatus(JOptionPane.OK_OPTION);
        } catch (ExporttoServiceException e) {
            exporttoServicePanel.setStatus(JOptionPane.CANCEL_OPTION);
            String error =
                exporttoServicePanel.getExporttoSwingManager().getTranslation(
                    "_Error_exportto");
            LOG.info(error, e);
            JOptionPane.showMessageDialog(exporttoServicePanel, error);
        } catch (DataException e) {
            exporttoServicePanel.setStatus(JOptionPane.CANCEL_OPTION);
            String error =
                exporttoServicePanel.getExporttoSwingManager().getTranslation(
                    "_Error_data_export");
            LOG.info(error, e);
            JOptionPane.showMessageDialog(exporttoServicePanel, error);
        }
    }


    /**
     * @param exporttoServicePanelListener
     */
    public void setExporttoServicePanelListener(
        JExporttoServicePanelListener exporttoServicePanelListener) {
        this.exporttoServicePanelListener = exporttoServicePanelListener;
    }
}
