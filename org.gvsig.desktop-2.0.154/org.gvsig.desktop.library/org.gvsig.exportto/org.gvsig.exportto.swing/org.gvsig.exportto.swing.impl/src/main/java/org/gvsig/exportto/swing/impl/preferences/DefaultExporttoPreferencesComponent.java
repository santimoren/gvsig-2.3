/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.preferences;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gvsig.exportto.swing.preferences.ExporttoSwingPreferencesComponent;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;

/**
 * Default implementation for the {@link ExporttoSwingPreferencesComponent}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultExporttoPreferencesComponent extends JPanel implements
    ExporttoSwingPreferencesComponent, ChangeListener {

    private static final long serialVersionUID = -4392838062470171181L;

    private JCheckBox[] providerChecks;
    private List<ExporttoSwingProviderFactory> providers;
    private boolean valueChanged = false;

    /**
     * Creates a new DefaultExporttoPreferencesComponent.
     * 
     * @see JPanel#JPanel()
     */
    public DefaultExporttoPreferencesComponent() {
        this(true);
    }

    /**
     * Creates a new DefaultExporttoPreferencesComponent.
     * 
     * @param isDoubleBuffered
     *            a boolean, true for double-buffering, which
     *            uses additional memory space to achieve fast, flicker-free
     *            updates
     * @see JPanel#JPanel(boolean)
     */
    public DefaultExporttoPreferencesComponent(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        initialize();
    }

    private void initialize() {
        this.setLayout(new GridBagLayout());

        Insets insets = new Insets(1, 0, 1, 0);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.weightx = 1.0f;
        gbc.insets = insets;

        providers =
            new ArrayList<ExporttoSwingProviderFactory>(
                ExporttoSwingProviderLocator.getManager()
                    .getProviderFactories());
        providerChecks = new JCheckBox[providers.size()];
        for (int i = 0; i < providers.size(); i++) {
            ExporttoSwingProviderFactory factory = providers.get(i);
            providerChecks[i] =
                new JCheckBox(factory.getLabel(), factory.isEnabled());
            providerChecks[i].addChangeListener(this);
            providerChecks[i].setToolTipText(factory.getDescription());
            add(providerChecks[i], gbc);
            gbc.gridy++;
        }
    }

    public JComponent asJComponent() {
        return this;
    }

    public void initializeDefaults() {
        for (int i = 0; i < providers.size(); i++) {
            ExporttoSwingProviderFactory factory = providers.get(i);
            providerChecks[i].setSelected(factory.isEnabled());
        }
        valueChanged = false;
        validate();
    }

    public Set<?> getDisabledProviders() {
        Set<ExporttoSwingProviderFactory> disabledFactories =
            new HashSet<ExporttoSwingProviderFactory>();

        for (int i = 0; i < providerChecks.length; i++) {
            if (!providerChecks[i].isSelected()) {
                disabledFactories.add(providers.get(i));
            }
        }

        return disabledFactories;
    }

    public boolean isValueChanged() {
        return valueChanged;
    }

    public void setChangesApplied() {
        valueChanged = false;
    }

    public void stateChanged(ChangeEvent e) {
        valueChanged = true;
    }
}
