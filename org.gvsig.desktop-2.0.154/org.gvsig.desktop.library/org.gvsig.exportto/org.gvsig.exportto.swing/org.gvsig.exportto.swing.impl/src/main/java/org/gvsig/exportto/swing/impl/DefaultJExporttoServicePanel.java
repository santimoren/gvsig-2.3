/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import jwizardcomponent.DefaultJWizardComponents;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.exportto.swing.JExporttoServicePanel;
import org.gvsig.exportto.swing.JExporttoServicePanelListener;
import org.gvsig.exportto.swing.impl.panel.MessagePanel;
import org.gvsig.exportto.swing.impl.wizard.DefaultWizardContainer;
import org.gvsig.exportto.swing.impl.wizard.ExportFilterWizard;
import org.gvsig.exportto.swing.impl.wizard.ExportTargetProjectionWizard;
import org.gvsig.exportto.swing.impl.wizard.ExporterSelectionWizard;
import org.gvsig.exportto.swing.impl.wizard.ExporttoProgressWizard;
import org.gvsig.exportto.swing.spi.ExporttoSwingProvider;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.task.SimpleTaskStatus;

/**
 * Default implementation for the {@link JExporttoServicePanel}.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultJExporttoServicePanel extends JExporttoServicePanel
    implements WizardPanel {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultJExporttoServicePanel.class);

    private static final ExporttoSwingProviderManager EXPORTTO_SWING_PROVIDER_MANAGER =
        ExporttoSwingProviderLocator.getManager();
    private static final DataManager DATA_MANAGER = DALLocator.getDataManager();

    private static final long serialVersionUID = -7026467582252285238L;

    private FeatureStore featureStore;
    private IProjection projection;
    private FLyrVect vectorLayer = null;
    private DefaultExporttoSwingManager uimanager;

    // Used to get the new feature store parameters
    private ExporttoSwingProvider exporttoSwingProvider;

    // Provider types that will be displayed
    private int[] providerTypes;

    // Wizards used to create the main wizard
    private ExporterSelectionWizard exporterSelectionWizard = null;
    private ExportFilterWizard exportFilterWizard = null;
    private ExportTargetProjectionWizard exportTargetProjectionWizard  = null;
    private ExporttoProgressWizard exporttoProgressWizard = null;

    // Listener for the finish and cancell button
    private WizardPanelActionListener exporttoServicePanelListener;

    private WizardPanelWithLogo wizardPanelWithLogo = null;

    // Action to execute at the end of the export process
    ExporttoServiceFinishAction exporttoServiceFinishAction;

    private int status;

    public DefaultJExporttoServicePanel(DefaultExporttoSwingManager uimanager,
        FLyrVect vlayer,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes) {

        this(
            uimanager,
            vlayer.getFeatureStore(),
            vlayer.getProjection(),
            exporttoServiceFinishAction,
            providerTypes);
        vectorLayer = vlayer;
    }

    public DefaultJExporttoServicePanel(DefaultExporttoSwingManager uimanager,
        FeatureStore featureStore, IProjection projection,
        ExporttoServiceFinishAction exporttoServiceFinishAction,
        int[] providerTypes) {
        this.featureStore = featureStore;
        this.projection = projection;
        this.uimanager = uimanager;
        this.exporttoServiceFinishAction = exporttoServiceFinishAction;
        this.providerTypes = providerTypes;
        this.status = JOptionPane.UNDEFINED_CONDITION;

        ImageIcon ii = IconThemeHelper.getImageIcon("wizard-export-to");
        wizardPanelWithLogo = new WizardPanelWithLogo(ii);

        // Initialize the wizards
        exporterSelectionWizard =
            new ExporterSelectionWizard(this, providerTypes);
        exportTargetProjectionWizard = new ExportTargetProjectionWizard(this);
        exportFilterWizard = new ExportFilterWizard(this);
        exporttoProgressWizard = new ExporttoProgressWizard(this);

        // Adding the wizards to the main wizard
        wizardPanelWithLogo.addOptionPanel(exporterSelectionWizard);

        // The finish button is called "export"
        this.wizardPanelWithLogo.getWizardComponents().getFinishButton()
            .setText(uimanager.getTranslation("export"));

        this.setLayout(new BorderLayout());
        this.add(wizardPanelWithLogo, BorderLayout.CENTER);

        // Disable the nextbutton
        setNextButtonEnabled(false);
        setExportButtonEnabled(false);

        // Set the listener for the finish and cancell events
        this.wizardPanelWithLogo.setWizardListener(this);
        this.setPreferredSize(new Dimension(700, 350));
    }

    public FeatureStore getFeatureStore() {
        return this.featureStore;
    }

    public void setNextButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setNextButtonEnabled(isEnabled);
    }

    public void setExportButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setFinishButtonEnabled(isEnabled);
    }

    public void setCancelButtonText(String text) {
        wizardPanelWithLogo.getWizardComponents().getCancelButton()
            .setText(text);
    }

    /**
     * @return the uimanager
     */
    public DefaultExporttoSwingManager getExporttoSwingManager() {
        return uimanager;
    }

    /**
     * @param exporttoSwingProvider
     * @throws ServiceException
     */
    public void selectExporttoSwingProvider(
        ExporttoSwingProviderFactory provider)
        throws ServiceException {

        if (vectorLayer == null) {
            exporttoSwingProvider =
                EXPORTTO_SWING_PROVIDER_MANAGER.createExporttoSwingProvider(
                    provider.getName(), featureStore, projection);
        } else {
            exporttoSwingProvider =
                EXPORTTO_SWING_PROVIDER_MANAGER.createExporttoSwingProvider(
                    provider.getName(), vectorLayer);
        }

        DefaultJWizardComponents wizardComponents =
            wizardPanelWithLogo.getWizardComponents();
        for (int i = wizardComponents.getWizardPanelList().size() - 1; i >= 1; i--) {
            wizardComponents.removeWizardPanel(i);
        }
        for (int i = 0; i < exporttoSwingProvider.getPanelCount(); i++) {
            wizardPanelWithLogo.addOptionPanel(new DefaultWizardContainer(
                exporttoSwingProvider.getPanelAt(i)));
        }
        exportTargetProjectionWizard.setVectorLayer(vectorLayer);
        if (exporttoSwingProvider.needsPanelTargetProjection()){
            wizardPanelWithLogo.addOptionPanel(exportTargetProjectionWizard);
        }
        wizardPanelWithLogo.addOptionPanel(exportFilterWizard);
        wizardPanelWithLogo.addOptionPanel(exporttoProgressWizard);
    }

    public Dimension getPreferredSize() {
        return new Dimension(800, 550);
    }

    public void export() throws DataException, ExporttoServiceException {
        this.lastWizard();

        // Create the feature Set...
        FeatureSet featureSet = null;
        if ( exportFilterWizard.isFullLayerSelected() ) {
            featureSet = featureStore.getFeatureSet();

        } else if ( exportFilterWizard.isSelectedFeaturesSelected() ) {
            featureSet = (FeatureSet) featureStore.getSelection();

        } else {
            Evaluator filter = exportFilterWizard.getFilter();
            if( filter!=null ) {
                FeatureQuery featureQuery = featureStore.createFeatureQuery();
                featureQuery.setFilter(filter);
                featureSet = featureStore.getFeatureSet(featureQuery);
            }
        }
        if( exporttoSwingProvider.needsPanelTargetProjection()) {
            exporttoSwingProvider.setTargetProjection(exportTargetProjectionWizard.getTargetProjection());
        }

        Export export = new Export(featureSet);
        export.start();
    }

    public void lastWizard() {
        this.wizardPanelWithLogo.getWizardComponents().getNextButton()
            .getActionListeners()[0].actionPerformed(null);
    }

    private class Export extends Thread {

        private FeatureSet featureSet;

        public Export(FeatureSet featureSet) {
            super();
            this.featureSet = featureSet;
        }

        public synchronized void run() {
            ExporttoService exportService =
                exporttoSwingProvider.createExporttoService();
            exporttoProgressWizard.bind(exportService.getTaskStatus());
            exportService.setFinishAction(exporttoServiceFinishAction);
            String layerName = "(unknow)";
            String layerFullName = "(unknow)";
            try {
                layerName = featureStore.getName();
                layerFullName = featureStore.getFullName();
                LOG.info("Exporting '"+layerName+"' using '"+exportService.getClass().getName()+"' ("+layerFullName+").");
                ((SimpleTaskStatus)(exportService.getTaskStatus())).setTitle("Exporting "+layerName+"...");
                exportService.export(featureSet);
            } catch (ExporttoServiceException e) {
                LOG.warn("Problems exporting the data (layer="+layerName+").", e);
                showError(e);
            } finally {
                ((SimpleTaskStatus)(exportService.getTaskStatus())).terminate();
            }
        }
    }

    private void showError(ExporttoServiceException e) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        MessagePanel.showMessage(
                i18nManager.getTranslation("_Warning"),
                i18nManager.getTranslation("_There_have_been_problems_exporting_data"),
                e,
                e.getFeature()
        );
    }

    public void setWizardPanelActionListener(
        WizardPanelActionListener wizardActionListener) {
        this.exporttoServicePanelListener = wizardActionListener;
    }

    public WizardPanelActionListener getWizardPanelActionListener() {
        if (exporttoServicePanelListener == null) {
            exporttoServicePanelListener =
                new DefaultJExporttoServicePanelListener(this);
        }
        return exporttoServicePanelListener;
    }

    @Override
    public void setExporttoServicePanelListener(
        JExporttoServicePanelListener exporttoServicePanelListener) {
        ((DefaultJExporttoServicePanelListener) getWizardPanelActionListener())
            .setExporttoServicePanelListener(exporttoServicePanelListener);
    }

    @Override
    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
        if( this.status == JOptionPane.CANCEL_OPTION ) {
            this.setVisible(false);
        }
    }
}
