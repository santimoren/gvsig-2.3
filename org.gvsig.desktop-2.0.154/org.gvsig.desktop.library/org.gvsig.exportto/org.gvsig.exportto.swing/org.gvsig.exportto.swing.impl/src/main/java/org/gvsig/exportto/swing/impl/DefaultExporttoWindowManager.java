/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.gvsig.exportto.swing.ExporttoWindowManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;

/**
 * Default implementation for the {@link ExporttoWindowManager}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultExporttoWindowManager implements ExporttoWindowManager,
    ComponentListener {

    public void showWindow(JExporttoServicePanel panel, String title, int mode) {
        JFrame frame = new JFrame();
        // frame.setAlwaysOnTop(true);

        final Dimension s = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(s.width / 4, s.height / 4);

        frame.setLayout(new BorderLayout());
        frame.setTitle(title);
        frame.add(panel, BorderLayout.CENTER);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        panel.addComponentListener(this);

        frame.pack();
        frame.setVisible(true);
    }

    public void componentHidden(ComponentEvent componentEvent) {
        JFrame frame =
            (JFrame) ((JPanel) componentEvent.getSource()).getRootPane()
                .getParent();
        frame.setVisible(false);
        frame.dispose();
    }

    public void componentMoved(ComponentEvent arg0) {
        // Do nothing
    }

    public void componentResized(ComponentEvent arg0) {
        // Do nothing
    }

    public void componentShown(ComponentEvent arg0) {
        // Do nothing
    }
}
