/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.ServiceException;

import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessagePanel extends JPanel {

    private static Logger logger = LoggerFactory.getLogger(MessagePanel.class);

    private static final long serialVersionUID = 2579894035021915221L;

    private JButton accept = null;
    private JTextPane text = null;

    public static void showMessage(String title,
            String header, String html, Feature feature) {
        JPanel panel = new MessagePanel(header, html, feature);
        WindowManager wm = ToolsSwingLocator.getWindowManager();
        wm.showWindow(panel, title, WindowManager.MODE.DIALOG);
    }

    public static void showMessage(String title,
            String header, Exception ex, Feature feature) {
        showMessage(title, header, getMessageAsHTML(ex), feature);
    }

    private static class ExceptionIterator implements Iterator {

        Throwable exception;

        ExceptionIterator(Throwable exception) {
            this.exception = exception;
        }

        public boolean hasNext() {
            return this.exception != null;
        }

        public Object next() {
            Throwable exception = this.exception;
            this.exception = exception.getCause();
            return exception;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static String getMessageAsHTML(Exception ex) {
        Iterator exceptions = new ExceptionIterator(ex);
        StringBuilder html = new StringBuilder();
        String lastmsg = "";

        html.append("<ul>\n");
        while (exceptions.hasNext()) {
            Throwable ex1 = ((Throwable) exceptions.next());
            String message = null;
            if (ex1 instanceof UnknownHostException) {
                message = "Unknown Host " + ex1.getMessage();
            } else {
                message = ex1.getMessage();
            }
            if (message != null) {
                if( !message.equalsIgnoreCase(lastmsg) ) {
                    lastmsg = message;
                    html.append("<li>");
                    if (message.toLowerCase().contains(" create ")) {
                        message = message.replaceFirst(" create ", "<br>\nCREATE ");
                        message = message.replaceAll(",", ",<br>\n");
                    } else if (message.toLowerCase().contains(" insert ")) {
                        message = message.replaceFirst(" insert ", "<br>\nINSERT ");
                        message = message.replaceAll(",", ",<br>\n");
                    }
                    html.append(message);
                    html.append("</li>\n");
                }
            }
        }
        html.append("</ul>\n");
        return html.toString();
    }

    public MessagePanel(final String header, final String html, Feature feature) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(500, 300));
        this.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

        JLabel headerlabel = new JLabel();
        headerlabel.setText(header);

        JTabbedPane tabs = new JTabbedPane();

        text = new JTextPane();
        text.setContentType("text/html");
        text.setEditable(false);
        text.setText(html);
        text.setCaretPosition(0);

        JScrollPane scrollPane = new JScrollPane(text);
        scrollPane.setPreferredSize(new Dimension(500, 220));

        tabs.addTab(i18nManager.getTranslation("_Problem"), scrollPane);
        if (feature != null) {
            try {
                DynObject data = feature.getAsDynObject();
                JDynForm form = DynFormLocator.getDynFormManager().createJDynForm(data);
                form.setLayoutMode(form.USE_TABS);
                tabs.addTab(i18nManager.getTranslation("_Row_values"), form.asJComponent());
            } catch (ServiceException ex) {
                logger.warn("Can't show feature associated with the problem.", ex);
            }
        }

        accept = new JButton(i18nManager.getTranslation("_Accept"));
        accept.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setVisible(false);
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel,
                BoxLayout.LINE_AXIS));
        buttonsPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

        buttonsPanel.add(Box.createHorizontalGlue());
        buttonsPanel.add(accept);

        this.add(headerlabel, BorderLayout.NORTH);
        this.add(tabs, BorderLayout.CENTER);
        this.add(buttonsPanel, BorderLayout.SOUTH);
        this.setVisible(true);
    }

}
