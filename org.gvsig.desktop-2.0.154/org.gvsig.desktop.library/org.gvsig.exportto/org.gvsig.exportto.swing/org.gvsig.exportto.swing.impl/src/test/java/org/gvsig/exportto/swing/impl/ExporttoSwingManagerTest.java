/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.swing.impl;

//import org.gvsig.exportto.ExporttoLocator;
//import org.gvsig.exportto.ExporttoManager;
//import org.gvsig.exportto.swing.ExporttoSwingLocator;
//import org.gvsig.exportto.swing.ExporttoSwingManager;
//import org.gvsig.exportto.swing.ExporttoWindowManager;
//import org.gvsig.exportto.swing.JExporttoServicePanel;
//import org.gvsig.fmap.crs.CRSFactory;
//import org.gvsig.fmap.dal.DALLocator;
//import org.gvsig.fmap.dal.DataManager;
//import org.gvsig.fmap.dal.DataStoreParameters;
//import org.gvsig.fmap.dal.exception.InitializeException;
//import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
//import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
//import org.gvsig.fmap.dal.feature.FeatureStore;
//import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
//import org.gvsig.tools.service.ServiceException;
//import org.gvsig.tools.swing.api.ToolsSwingLocator;
//import org.gvsig.tools.swing.impl.dynobject.DefaultDynObjectSwingManager;

/**
 * API compatibility tests for {@link ExporttoSwingManager} implementations.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class ExporttoSwingManagerTest {
//extends AbstractLibraryAutoInitTestCase {
//
//    protected ExporttoManager manager;
//    protected ExporttoSwingManager swingManager;
//    protected DataManager dataManager = null;
//
//    @Override
//    protected void doSetUp() throws Exception {
//        
//        // A Class or Factory has not been registered for the reference
//        // Tools.dynobject.swing.manager in the Locator Tools.swing.locator
//        ToolsSwingLocator.registerDynObjectSwingManager(
//            DefaultDynObjectSwingManager.class);
//
//        manager = ExporttoLocator.getManager();
//        swingManager = ExporttoSwingLocator.getSwingManager();
//        dataManager = DALLocator.getDataManager();
//        
//        IconThemeHelper.registerIcon("export-to", "wizard-export-to", this);
//    }
//
//    public void testGetManager() {
//        assertEquals(manager, swingManager.getManager());
//    }
//
//    public void testCreateWindowManager() {
//        ExporttoWindowManager windowManager = null;
//        swingManager.registerWindowManager(windowManager);
//        assertEquals(swingManager.getWindowManager(), windowManager);
//    }
//
//    public void testCreationJExporttoServicePanel() throws ServiceException,
//        ValidateDataParametersException, InitializeException,
//        ProviderNotRegisteredException {
//
//        JExporttoServicePanel panel1 =
//            swingManager.createExportto(createMemoryFeatureStore(),
//                CRSFactory.getCRS("EPSG:4326"));
//
//        assertNotNull(panel1);
//    }
//
//    private FeatureStore createMemoryFeatureStore() throws InitializeException,
//        ProviderNotRegisteredException, ValidateDataParametersException {
//        DataStoreParameters memoryParameters =
//            dataManager.createStoreParameters("Memory");
//        return (FeatureStore) dataManager.openStore("Memory", memoryParameters);
//    }

}
