/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoLocator;
import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.ExporttoWindowManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;
import org.gvsig.exportto.swing.preferences.ExporttoSwingPreferencesComponent;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Main executable class for testing the Exportto library.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private ExporttoManager manager;
    private ExporttoSwingManager swingManager;
    private DataManager dataManager;

    public static void main(String args[]) {
        new DefaultLibrariesInitializer().fullInitialize();
        Main main = new Main();
        main.show();
    }

    @SuppressWarnings("serial")
    public void show() {
        manager = ExporttoLocator.getManager();
        swingManager = ExporttoSwingLocator.getSwingManager();
        dataManager = DALLocator.getDataManager();

        Action showCookie = new AbstractAction("Get a Exportto") {

            public void actionPerformed(ActionEvent e) {
                showExportto(manager);
            }
        };

        Action preferences = new AbstractAction("Exportto preferences") {

            public void actionPerformed(ActionEvent e) {
                showExporttoPreferences();
            }

        };

        Action exit = new AbstractAction("Exit") {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        };

        JFrame frame = new JFrame("Exportto example app");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Create the menu bar.
        JMenuBar menuBar = new JMenuBar();

        // Build the menu.
        JMenu menuFile = new JMenu("File");
        menuFile.add(new JMenuItem(showCookie));
        menuFile.add(new JMenuItem(preferences));
        menuFile.add(new JMenuItem(exit));

        menuBar.add(menuFile);

        JToolBar toolBar = new JToolBar();
        toolBar.add(new JButton(showCookie));
        toolBar.add(new JButton(exit));

        frame.setPreferredSize(new Dimension(200, 100));
        frame.setJMenuBar(menuBar);
        frame.add(toolBar, BorderLayout.PAGE_START);

        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    private void showExporttoPreferences() {
        ExporttoSwingPreferencesComponent preferences =
            ExporttoSwingProviderLocator.getManager()
                .createExporttoSwingProvidersPreferences();

        JFrame preferencesFrame = new JFrame("Export to preferences");
        preferencesFrame.add(preferences.asJComponent());

        preferencesFrame.pack();
        preferencesFrame.setVisible(true);
    }

    public void showExportto(ExporttoManager manager) {
        try {
            JExporttoServicePanel panel =
                swingManager.createExportto(createFeatureStore(),
                    CRSFactory.getCRS("EPSG:23030"));
            panel.setPreferredSize(new Dimension(800, 400));
            swingManager.getWindowManager().showWindow(panel, "Exportto",
                ExporttoWindowManager.MODE_WINDOW);
        } catch (ValidateDataParametersException e) {
            LOG.error("Error showing a Exportto", e);
        } catch (InitializeException e) {
            LOG.error("Error showing a Exportto", e);
        } catch (ProviderNotRegisteredException e) {
            LOG.error("Error showing a Exportto", e);
        }
    }

    private FeatureStore createFeatureStore() throws InitializeException,
        ProviderNotRegisteredException, ValidateDataParametersException {
        String sourceFileName =
            getClass().getClassLoader()
                .getResource("org/gvsig/exportto/data/andalucia.shp").getFile();
        DataStoreParameters sourceParameters =
            dataManager.createStoreParameters("Shape");
        sourceParameters.setDynValue("shpfile", sourceFileName);
        sourceParameters.setDynValue("crs", CRSFactory.getCRS("EPSG:23030"));
        return (FeatureStore) dataManager.openStore("Shape", sourceParameters);
    }
}
