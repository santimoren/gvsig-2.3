Put into this folder the resources needed by your classes.

This folder is added to the classpath, so you can load any resources 
through the ClassLoader.

By default, in this folder you can find an example of log4j configuration,
prepared to log messages through the console, so logging works when you
run your classes.