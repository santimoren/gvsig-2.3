/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto;

import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.task.MonitorableTask;

/**
 * <p>
 * This service is used to export a source {@link FeatureStore} to a target
 * {@link FeatureStore}.
 * </p>
 * <p>
 * It inherits if {@link MonitorableTask}, and it means that the export process
 * can be monitorized by one or more observers that can listen all the export
 * events.
 * <p>
 * 
 * @author gvSIG team
 * @version $Id$
 */
public interface ExporttoService extends MonitorableTask {

    /**
     * This method export a {@link FeatureSet} to other {@link FeatureSet}.
     * The other necessary parameters to make this conversion are specific
     * for every {@link ExporttoService}. e.g: a shape exporter needs a
     * file and a projection, a database exporter needs a connection
     * parameters...
     * 
     * @param featureSet
     *            the set of features hat have to be exported.
     * @throws ExporttoServiceException
     *             if there is any exception in the exporting process
     */
    public void export(FeatureSet featureSet) throws ExporttoServiceException;

    /**
     * Sets the {@link ExporttoServiceFinishAction} that is used at the end
     * of the {@link ExporttoService}.
     * 
     * @param exporttoServiceFinishAction
     *            it contains an action that can be executed at the end of the
     *            export process
     * 
     */
    public void setFinishAction(
        ExporttoServiceFinishAction exporttoServiceFinishAction);

}
