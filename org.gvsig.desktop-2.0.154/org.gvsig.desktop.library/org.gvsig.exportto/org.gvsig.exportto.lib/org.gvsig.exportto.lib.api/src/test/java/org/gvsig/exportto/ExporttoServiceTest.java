/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;

/**
 * API compatibility tests for {@link ExporttoService} implementations.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class ExporttoServiceTest extends
    AbstractLibraryAutoInitTestCase {

    protected ExporttoManager manager;
    protected DataManager dataManager = null;

    @Override
    protected void doSetUp() throws Exception {
        manager = ExporttoLocator.getManager();
        dataManager = DALLocator.getDataManager();
    }

    /**
     * Returns an instance of the {@link ExporttoService}.
     * 
     * @return a {@link ExporttoService} instance
     * @throws ValidateDataParametersException
     * @throws DataException
     * @throws Exception
     *             if there is any error creating the instance
     */
    protected ExporttoService createService()
        throws ValidateDataParametersException, DataException {
        DataServerExplorerParameters dataServerExplorerParameters =
            dataManager.createServerExplorerParameters("FilesystemExplorer");
        DataServerExplorer dataServerExplorer =
            dataManager.openServerExplorer("FilesystemExplorer",
                dataServerExplorerParameters);
        NewDataStoreParameters newDataStoreParameters =
            dataServerExplorer.getAddParameters("Shape");
        return manager.getExporttoService(dataServerExplorer,
            newDataStoreParameters);
    }

    /**
     * Test for the
     * {@link ExporttoService#export(org.gvsig.fmap.dal.feature.FeatureSet)}
     * method.
     */
    public void testExporttoServiceCreation() {
        ExporttoService exportService;
        try {
            exportService = createService();
            assertNotNull(exportService);
            // TODO a test for the export service
            // exportService.export(featureSet);

        } catch (ValidateDataParametersException e) {
            assertNull(e);
        } catch (DataException e) {
            assertNull(e);
        }
    }

    @SuppressWarnings("unused")
    private FeatureStore createMemoryFeatureStore()
        throws ValidateDataParametersException, DataException {
        DataStoreParameters memoryParameters =
            dataManager.createStoreParameters("Memory");
        FeatureStore featureStore =
            (FeatureStore) dataManager.openStore("Memory", memoryParameters);
        return (FeatureStore) featureStore.getFeatureSet();
    }
}
