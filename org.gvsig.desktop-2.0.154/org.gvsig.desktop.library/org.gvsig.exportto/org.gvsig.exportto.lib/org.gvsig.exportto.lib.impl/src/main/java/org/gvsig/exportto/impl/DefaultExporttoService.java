/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.impl;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;

import org.gvsig.exportto.ExporttoService;
import org.gvsig.exportto.ExporttoServiceException;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.task.AbstractMonitorableTask;

/**
 * Default {@link ExporttoService} implementation.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultExporttoService extends AbstractMonitorableTask implements
        ExporttoService {

    private DataServerExplorer dataServerExplorer;
    private NewDataStoreParameters newDataStoreParameters;

    private ExporttoServiceFinishAction exporttoServiceFinishAction;

    /**
     * {@link DefaultExporttoService} constructor
     */
    public DefaultExporttoService(DataServerExplorer dataServerExplorer,
            NewDataStoreParameters newDataStoreParameters) {
        super(ToolsLocator.getI18nManager().getTranslation("Exportto"));
        this.dataServerExplorer = dataServerExplorer;
        this.newDataStoreParameters = newDataStoreParameters;
    }

    public void export(FeatureStore featureStore, IProjection projection,
            FeatureSet featureSet) throws ExporttoServiceException {
        // TODO Auto-generated method stub

    }

    public void export(FeatureSet featureSet) throws ExporttoServiceException {

        String providerName = newDataStoreParameters.getDataStoreName();
        String explorerName = dataServerExplorer.getProviderName();

        DisposableIterator it = null;
        FeatureStore target = null;
        EditableFeature newfeature = null;
        try {
            taskStatus.setRangeOfValues(0, featureSet.getSize());

            DataManager dataManager = DALLocator.getDataManager();

            dataManager.newStore(explorerName, providerName,
                    newDataStoreParameters, true);
            target
                    = (FeatureStore) dataManager.openStore(providerName,
                            newDataStoreParameters);

            FeatureType targetType = target.getDefaultFeatureType();

            target.edit(FeatureStore.MODE_APPEND);
            it = featureSet.fastIterator();

            IProjection targetProjection;
            FeatureAttributeDescriptor defaultGeom = target.getDefaultFeatureType().getDefaultGeometryAttribute();
            if (defaultGeom!=null) {
            	targetProjection = defaultGeom.getSRS();
            }
            else {
            	targetProjection = null;
            }

            long featureCount = 0;
            while (it.hasNext()) {
                Feature feature = (Feature) it.next();
                newfeature = target.createNewFeature(targetType, feature);
                // ================================================
                // Reprojection stuff
                Geometry reproj_geom = null;
                IProjection sourceProjection = feature.getDefaultSRS();

                ICoordTrans coord_trans = null;
                // this comparison is perhaps too preventive
                // we could  have two instances of same projection
                // so we would do more computations than needed
                if (sourceProjection != null
                        && targetProjection != null
                        && sourceProjection != targetProjection) {

                    coord_trans = sourceProjection.getCT(targetProjection);
                    reproj_geom = feature.getDefaultGeometry();
                    reproj_geom = reproj_geom.cloneGeometry();
                    reproj_geom.reProject(coord_trans);
                    newfeature.setDefaultGeometry(reproj_geom);
                }
                // ================================================

                target.insert(newfeature);

                featureCount++;
                this.taskStatus.setCurValue(featureCount);

                if (this.taskStatus.isCancellationRequested()) {
                    return;
                }
            }
            target.finishEditing();

            this.taskStatus.terminate();
            this.taskStatus.remove();
        } catch (Exception e) {
            throw new ExporttoServiceException(e, newfeature);
        } finally {
            if (target != null) {
                target.dispose();
            }
            if (it != null) {
                it.dispose();
            }
            if (featureSet != null) {
                featureSet.dispose();
            }
            DisposeUtils.dispose(it);
        }

        if (exporttoServiceFinishAction != null) {
            exporttoServiceFinishAction.finished(
                    newDataStoreParameters.getDataStoreName(),
                    newDataStoreParameters);
        }
    }

    public void setFinishAction(
            ExporttoServiceFinishAction exporttoServiceFinishAction) {
        this.exporttoServiceFinishAction = exporttoServiceFinishAction;
    }

}
