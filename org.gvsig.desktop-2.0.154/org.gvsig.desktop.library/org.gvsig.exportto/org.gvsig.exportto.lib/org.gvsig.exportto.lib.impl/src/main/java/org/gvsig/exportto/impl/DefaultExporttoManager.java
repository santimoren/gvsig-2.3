/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.exportto.ExporttoManager;
import org.gvsig.exportto.ExporttoService;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.NewDataStoreParameters;

/**
 * Default {@link ExporttoManager} implementation.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultExporttoManager implements ExporttoManager {

    private static final Logger LOG = LoggerFactory
            .getLogger(DefaultExporttoManager.class);

    private Map<String, String> filters = null;
    private File homeFolder = null;

    public DefaultExporttoManager() {
        super();
        filters = new HashMap<String, String>();
    }

    public ExporttoService getExporttoService(
            DataServerExplorer dataServerExplorer,
            NewDataStoreParameters newDataStoreParameters) {
        return new DefaultExporttoService(dataServerExplorer,
                newDataStoreParameters);
    }

    public Iterator getPredefinedFilters() {
        if (this.filters == null) {
            if (this.homeFolder != null) {
                Properties properties = new Properties();
                File filtersFile = new File(this.homeFolder, "filters");
                if (filtersFile.exists()) {
                    try {
                        properties.load(new FileInputStream(filtersFile));
                        Iterator it = properties.keySet().iterator();
                        while (it.hasNext()) {
                            String key = (String) it.next();
                            filters.put(key, (String) properties.get(key));
                        }
                    } catch (IOException ex) {
                        LOG.warn("Can't load filters file '" + filtersFile.getAbsolutePath() + "'.", ex);
                    }
                }
            }
        }
        return filters.keySet().iterator();
    }

    public String getFilter(String filterName) {
        if (filters.containsKey(filterName)) {
            return (String) filters.get(filterName);
        }
        return null;
    }

    public void addFilter(String name, String filter) {
        this.filters.put(name, filter);
        if (this.homeFolder != null) {
            Properties properties = new Properties();
            Iterator<Map.Entry<String, String>> it = this.filters.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> entry = it.next();
                properties.setProperty(entry.getKey(), entry.getValue());
            }
            File filtersFile = new File(this.homeFolder, "filters");
            try {
                properties.store(new FileOutputStream(filtersFile), "");
            } catch (IOException ex) {
                LOG.warn("Can't load filters file '" + filtersFile.getAbsolutePath() + "'.", ex);
            }
        }
    }

    public void setHomeFolder(File homeFolder) {
        this.homeFolder = homeFolder;
    }

}
