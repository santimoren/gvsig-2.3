/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.personaldb.main;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.personaldb.PersonalDBException;
import org.gvsig.personaldb.PersonalDBLocator;
import org.gvsig.personaldb.PersonalDBManager;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Main executable class for testing the PersonalDB library.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String args[]) {
        Main main = new Main();
        try {
            main.show();
        } catch (Throwable e) {
            e.printStackTrace();
            System.exit(-1);
        }
        System.exit(0);
    }

    public void show() throws PersonalDBException, SQLException {

        new DefaultLibrariesInitializer().fullInitialize();
        PersonalDBManager manager = PersonalDBLocator.getManager();

        Connection connection = manager.getConnection();
        DatabaseMetaData metadata = connection.getMetaData();
        LOG.info("DatabaseMetadata: {}", metadata);
    }

}
