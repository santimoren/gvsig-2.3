/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.personaldb;


/**
 * Exception thrown when there is an error creating a connection to the
 * personal database.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DataServerExplorerParametersException extends PersonalDBException {

    private static final long serialVersionUID = 8918999379397938916L;

    private final static String MESSAGE_FORMAT =
        "Error creating a DataServerExplorerParameters for the personal "
            + "database of the type: %(name)";
    private final static String MESSAGE_KEY =
        "_DataServerExplorerParametersException";

    /**
     * Constructor with a message and a cause
     */
    public DataServerExplorerParametersException(String name,
        Throwable cause) {
        super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
        setValue("name", name);
    }
}
