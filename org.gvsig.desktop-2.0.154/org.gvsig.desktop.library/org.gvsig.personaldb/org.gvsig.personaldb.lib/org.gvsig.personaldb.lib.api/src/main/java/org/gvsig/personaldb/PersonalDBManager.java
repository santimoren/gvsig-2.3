/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.personaldb;

import java.sql.Connection;

import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;

/**
 * This class is responsible of the management of the library's business logic.
 * It is the library's main entry point, and provides all the services to manage
 * {@link Connection}s and {@link DataParameters} to connect to the personal
 * database.
 * 
 * @see PersonalDBService
 * @author gvSIG team
 * @version $Id$
 */
public interface PersonalDBManager {

    /**
     * Returns a {@link Connection} the personal database.
     * 
     * @return a connection to the personal database
     * @throws PersonalDBException
     *             if there is an error getting the connection
     */
    public Connection getConnection() throws PersonalDBException;

    /**
     * Returns a {@link DataStoreParameters} to connect to the personal database
     * through the DAL library.
     * 
     * @return the DAL data store parameters to connect to connect to the
     *         database
     * @throws PersonalDBException
     *             if there is an error creating the parameters
     */
    public DataStoreParameters getDataStoreParameters()
        throws PersonalDBException;

    /**
     * Returns a {@link DataStoreParameters} to connect to the personal database
     * through the DAL library.
     * 
     * @param schema
     *            the schema of the table to load data from
     * @param table
     *            the name of the table to load data from
     * @return the DAL data store parameters to connect to connect to the
     *         database
     * @throws PersonalDBException
     *             if there is an error creating the parameters
     */
    public DataStoreParameters getDataStoreParameters(String schema,
        String table) throws PersonalDBException;

    /**
     * Returns a {@link DataServerExplorerParameters} to connect to the personal
     * database
     * through the DAL library.
     * 
     * @return the DAL data server explorer parameters to connect to connect to
     *         the
     *         database
     * @throws PersonalDBException
     *             if there is an error creating the parameters
     */
    public DataServerExplorerParameters getDataServerExplorerParameters()
        throws PersonalDBException;

    /**
     * Returns a {@link DataServerExplorerParameters} to connect to the personal
     * database
     * through the DAL library.
     * 
     * @param schema
     *            the schema of the database to connect to
     * @return the DAL data server explorer parameters to connect to connect to
     *         the
     *         database
     * @throws PersonalDBException
     *             if there is an error creating the parameters
     */
    public DataServerExplorerParameters getDataServerExplorerParameters(
        String schema) throws PersonalDBException;

}
