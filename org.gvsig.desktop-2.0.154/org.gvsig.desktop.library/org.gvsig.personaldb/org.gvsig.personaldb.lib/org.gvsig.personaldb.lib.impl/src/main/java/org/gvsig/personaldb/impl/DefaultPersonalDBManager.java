/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.personaldb.impl;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.store.jdbc.JDBCResourceParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorer;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreProvider;
import org.gvsig.personaldb.DBAccessException;
import org.gvsig.personaldb.DataServerExplorerParametersException;
import org.gvsig.personaldb.PersonalDBException;
import org.gvsig.personaldb.PersonalDBManager;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.exception.BaseException;

/**
 * Default {@link PersonalDBManager} implementation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DefaultPersonalDBManager extends AbstractDisposable implements
    PersonalDBManager {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultPersonalDBManager.class);

    private BasicDataSource dataSource;
    private JDBCResourceParameters resourceParameters;

    public Connection getConnection() throws PersonalDBException {

        try {
            return getDataSource().getConnection();
        } catch (SQLException e) {
            throw new DBAccessException(resourceParameters, e);
        }
    }

    private synchronized DataSource getDataSource() {

        if (dataSource == null) {
            dataSource = new BasicDataSource();
            dataSource.setDriverClassName(resourceParameters
                .getJDBCDriverClassName());
            dataSource.setUsername(resourceParameters.getUser());
            dataSource.setPassword(resourceParameters.getPassword());
            dataSource.setUrl(resourceParameters.getUrl());

            dataSource.setMaxWait(60L * 1000); // FIXME

            dataSource.setMaxActive(20);
            dataSource.setMaxIdle(4);
            dataSource.setMinIdle(1);
            dataSource.setInitialSize(1);

            // FIXME Set Pool parameters:
            /*
             * dataSource.setMaxOpenPreparedStatements(maxActive);
             * dataSource.setMaxWait(maxActive);
             * dataSource.setDefaultReadOnly(defaultReadOnly);
             * dataSource.setDefaultTransactionIsolation(defaultTransactionIsolation
             * )
             * ;
             * dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis
             * );
             * dataSource.setTestOnBorrow(testOnBorrow);
             * dataSource.setTestOnReturn(testOnReturn);
             * dataSource.setTestWhileIdle(testOnReturn);
             * dataSource
             * .setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
             * 
             * dataSource.setAccessToUnderlyingConnectionAllowed(allow);
             * dataSource.setLoginTimeout(seconds);
             * dataSource.setLogWriter(out);
             */
        }

        return dataSource;
    }

    public DataStoreParameters getDataStoreParameters()
        throws PersonalDBException {

        return getJDBCStoreParameters();
    }

    public DataStoreParameters getDataStoreParameters(String schema,
        String table) throws PersonalDBException {

        JDBCStoreParameters params = getJDBCStoreParameters();
        params.setSchema(schema);
        params.setTable(table);
        return params;
    }

    private JDBCStoreParameters getJDBCStoreParameters()
        throws PersonalDBException {
        try {
            JDBCStoreParameters params =
                (JDBCStoreParameters) DALLocator.getDataManager()
                    .createStoreParameters(JDBCStoreProvider.NAME);

            params.setUrl(resourceParameters.getUrl());
            params.setJDBCDriverClassName(resourceParameters
                .getJDBCDriverClassName());

            return params;
        } catch (InitializeException e) {
            throw new DataServerExplorerParametersException(
                JDBCServerExplorer.NAME, e);
        } catch (ProviderNotRegisteredException e) {
            throw new DataServerExplorerParametersException(
                JDBCServerExplorer.NAME, e);
        }
    }

    public DataServerExplorerParameters getDataServerExplorerParameters()
        throws PersonalDBException {
        return getJDBCServerExplorerParameters();
    }

    public DataServerExplorerParameters getDataServerExplorerParameters(
        String schema) throws PersonalDBException {
        JDBCServerExplorerParameters params = getJDBCServerExplorerParameters();
        params.setSchema(schema);
        return params;
    }

    private JDBCServerExplorerParameters getJDBCServerExplorerParameters()
        throws PersonalDBException {
        try {
            JDBCServerExplorerParameters params =
                (JDBCServerExplorerParameters) DALLocator.getDataManager()
                    .createServerExplorerParameters(JDBCServerExplorer.NAME);

            params.setUrl(resourceParameters.getUrl());
            params.setJDBCDriverClassName(resourceParameters
                .getJDBCDriverClassName());

            return params;
        } catch (InitializeException e) {
            throw new DataServerExplorerParametersException(
                JDBCServerExplorer.NAME, e);
        } catch (ProviderNotRegisteredException e) {
            throw new DataServerExplorerParametersException(
                JDBCServerExplorer.NAME, e);
        }
    }

    public void open(JDBCResourceParameters resourceParameters) {
        LOG.debug(
            "Creating connection to personal database with parameters: {}",
            resourceParameters);

        this.resourceParameters = resourceParameters;
    }

    @Override
    protected void doDispose() throws BaseException {
        try {
            dataSource.close();
        } catch (SQLException e) {
            throw new DBAccessException(resourceParameters, e);
        }
    }
}
