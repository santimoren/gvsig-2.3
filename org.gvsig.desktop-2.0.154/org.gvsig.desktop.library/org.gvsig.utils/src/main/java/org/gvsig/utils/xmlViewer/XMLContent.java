/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.xmlViewer;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;


/**
 * Interfaz que surte de datos al control XMLViewer
 *
 * @author Fernando Gonz�lez Cort�s
 */
public interface XMLContent {
    /**
     * Mediante este m�todo el control se registra como handler de los eventos
     * SAX disparados en el evento parse
     *
     * @param handler Handler de los eventos del m�todo parse que meter� toda
     *        la informaci�n en el control
     */
    public void setContentHandler(ContentHandler handler);

    /**
     * Debe de lanzar los eventos SAX del contenido XML que representa
     *
     * @throws SAXException Si se produce alg�n error relacionado con los
     *         eventos
     */
    public void parse() throws SAXException;
}
