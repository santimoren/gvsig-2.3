/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.utils.console;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.gvsig.utils.console.jedit.ConsoleInputHandler;
import org.gvsig.utils.console.jedit.JEditTextArea;
import org.gvsig.utils.console.jedit.TextAreaDefaults;
import org.gvsig.utils.console.jedit.TokenMarker;



/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class JConsole extends JPanel {
	private JEditTextArea txt;
	private int startingCaretPosition = 0;
	private ArrayList entries = new ArrayList();
	private int currentEntry = -1;
	private ResponseListenerSupport listenerSupport = new ResponseListenerSupport();
	public static int MESSAGE=0;
	public static int COMMAND=1;
	public static int INSERT=2;
	public static int ERROR=3;
	private static TextAreaDefaults defaults;
	private JConsole theContainer = null;
	
	private boolean redispatch_key_events = false;
	/**
	 * This is the default constructor
	 */
	public JConsole() {
		super();
		initialize();
		defaults=TextAreaDefaults.getDefaults();
		((ConsoleInputHandler)defaults.inputHandler).addConsoleListener(this);
		theContainer = this;
	}
	

	/**
	 * With this constructor, we decide whether the key events
	 * are re-dispatched to the parent of this component.
	 * This is necessary to prevent the console from consuming
	 * key events (short-cuts) while editing. the component
	 * which originally receives the key event is the JEditTextArea
	 * but this console is receiving also key-pressed notifications.
	 * 
	 * @param redisp_key_events
	 */
	public JConsole(boolean redisp_key_events) {
	    this();
	    redispatch_key_events = redisp_key_events;
	}


	/**
	 * This method initializes this
	 */
	private void initialize() {
		this.setLayout(new BorderLayout());
		this.setSize(300, 200);
		this.setPreferredSize(new Dimension(300,200));
		this.add(getTxt(), java.awt.BorderLayout.CENTER);
	}

	public void setTokenMarker(TokenMarker tm){
		txt.setTokenMarker(tm);
	}
	/**
	 * Obtiene una referencia al JTextArea de la consola
	 *
	 * @return javax.swing.JTextArea
	 */
	public JEditTextArea getTxt() {
		if (txt == null) {
			txt = new JEditTextArea();
			//txt.setTokenMarker(new JavaTokenMarker());
			txt.addCaretListener(new CaretListener() {
					public void caretUpdate(CaretEvent e) {
						if (txt.getCaretPosition() < startingCaretPosition) {
							if (startingCaretPosition <= txt.getText().length()) {
								txt.setCaretPosition(startingCaretPosition);
							}
						}
					}
				}
			);
		}

		return txt;
	}

	/**
	 * A�ade un texto a la consola
	 *
	 * @param text Texto que se a�ade a la consola
	 */
	public void addText(String text,int type) {
		txt.setText(txt.getText() + text);
		txt.setCaretPosition(txt.getText().length());
		startingCaretPosition = txt.getText().length();

	}
	/**
	 * A�ade un texto a la consola que es tomado como si lo
	 * hubiese escrito el usuario. Formar� parte de la respuesta
	 *
	 * @param text Texto que se a�ade a la consola
	 */
	public void addResponseText(String text) {
		txt.setText(txt.getText() + text);
		txt.setCaretPosition(txt.getText().length());
	}

	public void addResponseListener(ResponseListener listener) {
		listenerSupport.addResponseListener(listener);
	}
	public void removeResponseListener(ResponseListener listener) {
		listenerSupport.removeResponseListener(listener);
	}

	/**
	 * Useful to know from where it comes a key event. See CADExtension
	 * @param name
	 */
	public void setJTextName(String name)
	{
		txt.setName(name);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			if (startingCaretPosition >= txt.getCaretPosition()) {
				int caretPos = txt.getCaretPosition();
				String text = txt.getText();
				text = text.substring(0, caretPos) + " " +
					text.substring(caretPos);
				txt.setText(text);

				txt.setCaretPosition(caretPos);

			}else{
			}
		}
		
		if (redispatch_key_events) {
	        Container cont = this.getParent();
	        if (cont != null && cont instanceof Component) {
	            Component comp = (Component) cont;
	            comp.dispatchEvent(e);
	        }
		}
	}

	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			String texto = txt.getText();
			String response = texto.substring(startingCaretPosition);

			listenerSupport.callAcceptResponse(response);

			if (response.trim().length() > 0) {
				entries.add(response.trim());
			}

			currentEntry = -1;

		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			if (entries.size() == 0) {
				return;
			}

			if (currentEntry == -1) {
				currentEntry = entries.size() - 1;
			} else {
				currentEntry--;

				if (currentEntry < 0) {
					currentEntry = 0;
				}
			}

			putEntry();
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			if (entries.size() == 0) {
				return;
			}

			if (currentEntry != -1) {
				currentEntry++;

				if (currentEntry >= entries.size()) {
					currentEntry = entries.size() - 1;
				}
			}

			putEntry();
		} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			txt.setText(txt.getText());
			listenerSupport.callAcceptResponse(null);
			
		} else if (e.getKeyCode() == KeyEvent.VK_V) {
			if ((e.getModifiers() & InputEvent.CTRL_MASK) != 0) {
				String clipb = "";
				try {
		            clipb = (String) Toolkit.getDefaultToolkit()
		            .getSystemClipboard()
		            .getContents(null)
		            .getTransferData(DataFlavor.stringFlavor);
		        } catch (UnsupportedFlavorException ex) {
		        } catch (IOException ex) {
		            // TODO Auto-generated catch block
		        }
				
				
				if (clipb != null && clipb.length() > 0) {
					String[] parts = clipb.split("\n");
					for (int i = 0; i < parts.length; i++) {
						String part = parts[i];
						if (part != null && part.length() > 0) {
							txt.setText(txt.getText()+part);
							String texto = txt.getText();
							String response = texto.substring(startingCaretPosition);

							listenerSupport.callAcceptResponse(response);

							if (response.trim().length() > 0) {
								entries.add(response.trim());
							}

							currentEntry = -1;
						}
					}		
				}
			}
		}
	}
	private void putEntry() {
		String anterior = txt.getText();
		anterior = anterior.substring(0, startingCaretPosition);
		txt.setText(anterior + entries.get(currentEntry));
	}


}
