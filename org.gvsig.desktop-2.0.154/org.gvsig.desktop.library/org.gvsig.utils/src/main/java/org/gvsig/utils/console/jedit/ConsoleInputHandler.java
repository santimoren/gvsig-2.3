/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.console.jedit;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.gvsig.utils.console.JConsole;


public class ConsoleInputHandler extends DefaultInputHandler {

	private ArrayList listeners=new ArrayList();
	public void keyPressed(KeyEvent evt) {
		super.keyPressed(evt);
		callConsolePressed(evt);
	}

	public void keyTyped(KeyEvent evt) {
		super.keyTyped(evt);
	}

	public void keyReleased(KeyEvent arg0) {
		callConsoleReleased(arg0);
	}

	public void addConsoleListener(JConsole console) {
		listeners.add(console);
	}
	private void callConsolePressed(KeyEvent e){
		for (int i=0;i<listeners.size();i++){
			((JConsole)listeners.get(i)).keyPressed(e);
		}
	}
	private void callConsoleReleased(KeyEvent e){
		for (int i=0;i<listeners.size();i++){
			((JConsole)listeners.get(i)).keyReleased(e);
		}
	}
}
