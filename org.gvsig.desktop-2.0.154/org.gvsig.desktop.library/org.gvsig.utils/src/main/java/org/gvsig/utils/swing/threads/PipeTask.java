/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * Created on 10-nov-2006 by azabala
 *
 */
package org.gvsig.utils.swing.threads;


/**
 * @author alzabord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PipeTask implements IMonitorableTask {

	private IPipedTask task1;
	private IPipedTask task2;
	private boolean canceled = false;
	
	private IPipedTask currentTask = null;
	
	
	public PipeTask(IPipedTask task1, IPipedTask task2){
		this.task1 = task1;
		this.task2 = task2;
		this.currentTask = task1;
	}
	
	
	public int getInitialStep() {
		return 0;
	}

	public int getFinishStep() {
		return currentTask.getFinishStep();
	}

	public int getCurrentStep() {
		return currentTask.getCurrentStep();
	}

	public String getStatusMessage() {
		return currentTask.getStatusMessage();
	}

	public String getNote() {
		if(currentTask != null){
			return currentTask.getNote();
		}else{
			return "";
		}
	}

	public boolean isDefined() {
		return currentTask.isDefined();
	}

	public void cancel() {
		currentTask.cancel();
		canceled = true;
	}

	public void run() throws Exception {
		currentTask.run();
		if(currentTask.isCanceled())
			return;
		Object result = currentTask.getResult();
		currentTask = task2;
		currentTask.setEntry(result);
		currentTask.run();
	}

	public boolean isCanceled() {
		return currentTask.isCanceled();
	}

	public boolean isFinished() {
		return task1.isFinished() && task2.isFinished();
	}

	public void finished() {
		
	}
}
