/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @author fjp
 *
 * Clase que recibe las llamadas que tiene que hacer cuando termine
 * la lectura de proyecto, por ejemplo. Recibir�a algo as� como Clase,
 * Nombre de funci�n a invocar, Par�metros y Prioridad.
 * Lo guarda en un array, ordena por prioridad y empieza a invocar.
 */
public class PostProcessSupport
{
    private static ArrayList callList = new ArrayList();
    private static class Call
    {
        private Object obj;
        private Method method;
        private Object[] parameters;
        private int priority;
        public Call(Object obj, String strMethod, Object[] parameters, int priority)
        {
            this.obj = obj;
            this.parameters = parameters;
            Class[] classParams = new Class[parameters.length];
            for (int i=0; i< parameters.length; i++)
                classParams[i] = parameters[i].getClass();
            try {
                method = obj.getClass().getMethod(strMethod, classParams);
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                // Resulta que con getMethod no pilla bien el superinterface VectorialLegend,
                // as� que lo hacemos manual
                Method[] list = obj.getClass().getMethods();
                for (int i=0; i< list.length; i++)
                {
                    // System.err.println(list[i].toString());
                    if (list[i].getName().compareTo(strMethod) == 0)
                    {
                        boolean encontrado = true;
                        Class[] params = list[i].getParameterTypes();
                        // En cada par�metro del m�todo, miramos si
                        // el par�metro correspondiente implementa la interfaz
                        // adecuada.
                        for (int j=0; j < params.length; j++) // for de los par�metros del m�todo.
                        {
                        	// Miramos primero si es de la clase que buscamos, y si no lo es, pasamos
                        	// a revisar sus interfaces.
                        	boolean bParamOK = false;
//                        	if (params[j].getName().compareTo(parameters[j].getClass().getName()) == 0)
                        	if ( params[j].isInstance(parameters[j]) )
                        	{
                        		bParamOK = true;
                        		continue;
                        	}
                            // Interfaces que implementa el par�metro j-�simo que le pasamos.
                            Class[] theInterfaces = parameters[j].getClass().getInterfaces();

                            for (int k=0; k< theInterfaces.length; k++)
                            {
                                // Si alguno de estos interfaces cuadra con lo que
                                // espera el m�todo, podemos comprobar el siguiente par�metro.
                                // Si al salir del for externo, encontrado sigue siendo true,
                                // hemos encontrado el m�todo que busc�bamos.
                                if (theInterfaces[k].getName().compareTo(params[j].getName()) == 0)
                                {
                                    bParamOK = true;
                                    break;
                                }
                            }
                            if (bParamOK == false)
                            {
                                encontrado = false;
                                break; // Vamos a por otro m�todo
                            }
                        }
                        if (encontrado)
                        {
                            method = list[i];
                            return;
                        }
                    }
                }
                e.printStackTrace();
            }
        }

        public Object executeMethod() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
        {
            return method.invoke(obj,parameters);
        }
    }
    public static void addToPostProcess(Object obj, String strMethod, Object[] parameters, int priority)
    {
        Call newCall = new Call(obj, strMethod, parameters, priority);
        callList.add(newCall);
    }
    public static void addToPostProcess(Object obj, String strMethod, Object parameter, int priority)
    {
        Object[] parameters = new Object[1];
        parameters[0] = parameter;
        Call newCall = new Call(obj, strMethod, parameters, priority);
        callList.add(newCall);
    }

    private static void orderByPriority()
    {
        // TODO
    }
    public static void executeCalls()
    {
        // TODO: Primero deber�amos ordenar por prioridad
        // por ahora no lo hago.
        orderByPriority();

        for (int i=0; i < callList.size(); i++)
        {
            Call call = (Call) callList.get(i);
            try {
                call.executeMethod();
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        clearList();
    }
    public static void clearList()
    {
        callList.clear();
    }
}
