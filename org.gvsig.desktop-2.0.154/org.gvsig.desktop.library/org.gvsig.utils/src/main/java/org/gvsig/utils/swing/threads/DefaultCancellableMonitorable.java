/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: DefaultCancellableMonitorable.java 29631 2009-06-29 16:56:19Z jpiera $
* $Log$
* Revision 1.1  2006-05-22 10:31:55  fjp
* Monitorable tasks easy
*
* Revision 1.4  2006/05/15 14:56:23  azabala
* A�adida la posibilidad de modificar el paso actual (para que en tareas que constan de varios pasos, para cada paso se pueda mostrar una barra llenandose)
*
* Revision 1.3  2006/03/14 19:29:15  azabala
* *** empty log message ***
*
* Revision 1.2  2006/03/09 18:43:29  azabala
* *** empty log message ***
*
* Revision 1.1  2006/03/09 18:41:32  azabala
* *** empty log message ***
*
*
*/
package org.gvsig.utils.swing.threads;
/**
 * Default very easy implementation of 
 * CancellableMonitorable
 * @author azabala
 *
 */
public class DefaultCancellableMonitorable implements CancellableMonitorable {
	
	private boolean canceled = false;
	private int currentStep = 0;
	private int initialStep = 0;
	private int lastStep = 0;
	private boolean determinated = false;
	
	
	
	
	public boolean isCanceled() {
		return canceled;
	}

	public void reportStep() {
		currentStep++;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setInitialStep(int step) {
		initialStep = step;
	}

	public void setFinalStep(int step) {
		lastStep = step;
	}

	public void setDeterminatedProcess(boolean determinated) {
		this.determinated = determinated;
	}

	public boolean isDeterminatedProcess() {
		return determinated;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public void reset() {
		canceled = false;
		currentStep = 0;
		initialStep = 0;
		lastStep = 0;
		determinated = false;
	}

	public int getInitialStep() {
		return initialStep;
	}

	public int getFinalStep() {
		return lastStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

}

