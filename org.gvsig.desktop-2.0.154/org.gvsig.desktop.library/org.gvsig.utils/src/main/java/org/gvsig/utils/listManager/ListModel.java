/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.listManager;

import java.util.Vector;

/**
 * Modelo del control ListManager
 *
 * @author Fernando Gonz�lez Cort�s
 */
public interface ListModel extends javax.swing.ListModel {
	/**
	 * Removes the element at the specified position in this Vector. shifts any
	 * subsequent elements to the left (subtracts one from their indices).
	 * Returns the element that was removed from the Vector.
	 *
	 * @param i �ndice del elemento que se quiere eliminar
	 *
	 * @return Objeto eliminado
	 *
	 * @throws ArrayIndexOutOfBoundsException Si el �ndice est� fuera del array
	 */
	public Object remove(int i) throws ArrayIndexOutOfBoundsException;

	/**
	 * Inserts the specified element at the specified position in this Vector.
	 * Shifts the element currently at that position (if any) and any
	 * subsequent elements to the right (adds one to their indices).
	 *
	 * @param i index at which the specified element is to be inserted.
	 * @param o element to be inserted.
	 */
	public void insertAt(int i, Object o);

	/**
	 * Appends the specified element to the end of this Vector
	 *
	 * @param o element to be appended to this Vector
	 */
	public void add(Object o);

	/**
	 * Obtains the objects that are in the model
	 * @return Vector with the objetos
	 */
	public Vector getObjects();
}
