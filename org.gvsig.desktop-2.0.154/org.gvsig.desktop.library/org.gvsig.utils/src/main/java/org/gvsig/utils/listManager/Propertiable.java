/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.listManager;

/**
 * Indica si este objeto admite alg�n tipo de propiedad. De los objetos que se
 * a�aden al listManager, los que implementan esta interfaz tendr�n el boton
 * "Propiedades" activo
 *
 * @author Fernando Gonz�lez Cort�s
 */
public interface Propertiable {
    /**
     * Establece la propiedad del elemento de la lista. Como esta propiedad se
     * establece por medio de la implementaci�n de la interfaz
     * ListManagerListener, que implementa el usuario, en la implementaci�n de
     * esta interfaz se sabr� qu� clase tiene el objeto que se pasa como
     * par�metro.
     *
     * @param o El objeto que se devuelve en el m�todo getProperties de
     *        ListManagerListener
     */
    public void setProperties(Object o);
}
