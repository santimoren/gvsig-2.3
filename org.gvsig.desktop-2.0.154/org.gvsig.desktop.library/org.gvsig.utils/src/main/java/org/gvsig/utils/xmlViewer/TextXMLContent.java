/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.xmlViewer;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


/**
 * Clase que sirve de XMLContent al control XMLViewer a partir de un string con
 * un fichero XML
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class TextXMLContent implements XMLContent {
    private String text;
    private ContentHandler handler;

    /**
     * Crea un nuevo TextXMLContent.
     *
     * @param text Texto con el XML
     */
    public TextXMLContent(String text) {
        this.text = text;
    }

    /**
     * @see org.gvsig.utils.xmlViewer.XMLContent#setContentHandler(org.xml.sax.ContentHandler)
     */
    public void setContentHandler(ContentHandler handler) {
        this.handler = handler;
    }

    /**
     * @see org.gvsig.utils.xmlViewer.XMLContent#parse()
     */
    public void parse() throws SAXException {
    	XMLReader reader = XMLReaderFactory.createXMLReader();
    	reader.setFeature("http://xml.org/sax/features/namespaces", false);
        reader.setContentHandler(handler);

        if (text == null) {
            text = "<?xml version='1.0'?>";
        }

        try {
            reader.parse(new InputSource(
                    new ByteArrayInputStream(text.getBytes())));
        } catch (IOException e) {
            //Una IO exception en un array de bytes???
        }
    }
    
    public String toString(){
    	return text;
    }
}
