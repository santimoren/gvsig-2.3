/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.swing.wizard;

/**
 * Evento con la informaci�n del estado del asistente
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class WizardEvent {
	public WizardControl wizard;
	public int currentStep;

	/**
	 * Crea un nuevo WizardEvent.
	 *
	 * @param w referencia al asistente donde se gener� el evento
	 * @param currentStep paso actual del asistente
	 */
	public WizardEvent(Wizard w, int currentStep) {
		this.wizard = w;
		this.currentStep = currentStep;
	}
}
