/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.console;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class ResponseListenerSupport {
	private ArrayList listeners = new ArrayList();

	/**
	 * DOCUMENT ME!
	 *
	 * @param listener DOCUMENT ME!
	 */
	public void addResponseListener(ResponseListener listener) {
		listeners.add(listener);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param listener DOCUMENT ME!
	 */
	public void removeResponseListener(ResponseListener listener) {
		listeners.remove(listener);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param arg0 DOCUMENT ME!
	 *
	 * @throws InvalidResponseException DOCUMENT ME!
	 */
	public void callAcceptResponse(java.lang.String arg0) {
		Iterator i = listeners.iterator();

		while (i.hasNext()) {
			((ResponseListener) i.next()).acceptResponse(arg0);
		}
	}
}
