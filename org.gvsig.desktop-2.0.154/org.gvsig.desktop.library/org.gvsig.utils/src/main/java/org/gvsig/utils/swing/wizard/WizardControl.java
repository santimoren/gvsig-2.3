/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.swing.wizard;

/**
 * Visi�n que tienen los escuchadores de eventos sobre el Wizard
 *
 * @author Fernando Gonz�lez Cort�s
 */
public interface WizardControl {
	/**
	 * Activa el paso al siguiente paso del asistente
	 *
	 * @param enabled si se habilita o no
	 */
	public abstract void enableNext(boolean enabled);

	/**
	 * Activa el paso al paso anterior del asistente
	 *
	 * @param enabled si se habilita o no
	 */
	public abstract void enableBack(boolean enabled);

	/**
	 * Muestra el panel del siguiente paso del asistente
	 */
	public abstract void nextStep();

	/**
	 * Muestra el panel del paso anterior del asistente
	 */
	public abstract void backStep();

	/**
	 * Se cancela el asistente. Esta operaci�n no tiene ning�n efecto, salvo
	 * que se disparar� el evento de cancelado. El resultado de esto depender�
	 * de las implementaciones que haya escuchando el evento. Generalmente
	 * deber� haber un objeto que al escuchar este evento cerrar� el
	 * asistente.
	 */
	public abstract void cancel();

	/**
	 * Se finaliza el asistente. Esta operaci�n no tiene ning�n efecto, salvo
	 * que se disparar� el evento de finalizaci�n. El resultado de esto
	 * depender� de las implementaciones que haya escuchando el evento.
	 * Generalmente deber� haber un objeto que al escuchar este evento cerrar�
	 * el asistente.
	 */
	public abstract void finish();

	/**
	 * Obtiene el paso actual del asistente
	 *
	 * @return Paso actual del asistente
	 */
	public abstract Step getCurrentStep();
}
