/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils;

/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class JEPUtilities {
    /**
     * Transforma una expresion en funcion de variables con nombre de la forma
     * xix  (donde i es un n�mero cualquiera) en una expresi�n donde dichas
     * variables han sido sustituidas por los valores que se pasan como
     * par�metros.
     *
     * @param expression Expresion en funcion de xi
     * @param values Valores que toman las distintas variables x
     * @param cadena cadena[i] = true si xi es alfanum�rica y false en caso
     *        contrario
     *
     * @return La nueva expresion
     */
    public static String fillExpression(String expression, String[] values,
        boolean[] cadena) {
        String expresionReal = expression;

        for (int i = 0; i < values.length; i++) {
            if (cadena[i]) {
                expresionReal = expresionReal.replaceAll("x" + i + "x",
                        "\"" + values[i] + "\"");
            } else {
                expresionReal = expresionReal.replaceAll("x" + i + "x",
                        values[i]);
            }
        }

        return expresionReal;
    }

    /*
     * DOCUMENT ME!
     *
     * @param o DOCUMENT ME!
     * @param type DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws IllegalStateException DOCUMENT ME!
     *
       public static String getOrdinalValue(Object o, int type) {
               switch (type) {
               case FRecordset.STRING:
                       return (String) o;
               case FRecordset.DECIMAL:
               case FRecordset.INTEGER:
                       return ((Number) o).toString();
               case FRecordset.DATE:
                       return new Long(((Date) o).getTime()).toString();
               case FRecordset.BOOLEAN:
                       if (((Boolean) o).booleanValue()) {
                               return "1";
                       } else {
                               return "0";
                       }
               }
               throw new IllegalStateException("No se conoce el tipo del campo");
       }
     */
}
