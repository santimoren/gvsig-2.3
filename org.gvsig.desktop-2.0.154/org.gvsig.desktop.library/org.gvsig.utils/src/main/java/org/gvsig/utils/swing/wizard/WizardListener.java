/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.swing.wizard;

/**
 * Listener de los eventos del asistente
 *
 * @author Fernando González Cortés
 */
public interface WizardListener {
	/**
	 * Evento disparado cuando se cancela el asistente
	 *
	 * @param w objeto con la información del evento
	 */
	public void cancel(WizardEvent w);

	/**
	 * Evento disparado cuando se finaliza el asistente
	 *
	 * @param w objeto con la información del evento
	 */
	public void finished(WizardEvent w);

	/**
	 * Evento disparado cuando se avanza un paso el asistente
	 *
	 * @param w objeto con la información del evento
	 */
	public void next(WizardEvent w);

	/**
	 * Evento disparado cuando se da un paso atrás en el asistente
	 *
	 * @param w objeto con la información del evento
	 */
	public void back(WizardEvent w);
}
