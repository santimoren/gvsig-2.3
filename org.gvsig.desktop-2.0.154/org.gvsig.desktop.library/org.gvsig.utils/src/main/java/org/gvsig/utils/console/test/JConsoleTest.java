/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.utils.console.test;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;

import org.gvsig.utils.console.JConsole;
import org.gvsig.utils.console.JDockPanel;
import org.gvsig.utils.console.ResponseListener;

public class JConsoleTest extends JFrame {

	private javax.swing.JPanel jContentPane = null;

	private JConsole jConsole = null;
	private JButton jButton = null;
	/**
	 * This is the default constructor
	 */
	public JConsoleTest() {
		super();
		initialize();
	}
	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setSize(300,200);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}
	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private javax.swing.JPanel getJContentPane() {
		if(jContentPane == null) {
			jContentPane = new javax.swing.JPanel();
			jContentPane.setLayout(new java.awt.BorderLayout());
			JDockPanel dock = new JDockPanel(getJConsole());
			jContentPane.add(dock, java.awt.BorderLayout.SOUTH);
		}
		return jContentPane;
	}
	/**
	 * This method initializes jConsole
	 *
	 * @return com.iver.utiles.console.JConsole
	 */
	private JConsole getJConsole() {
		if (jConsole == null) {
			jConsole = new JConsole();
			// jConsole.add(getJButton(), java.awt.BorderLayout.NORTH);
		}
		return jConsole;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setPreferredSize(new java.awt.Dimension(34,25));
		}
		return jButton;
	}

	public static void main(String[] args) {
		MyBoolean responsed = new MyBoolean();
		
		//Se pone el lookAndFeel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {

		}


		JConsoleTest ct = new JConsoleTest();
		ct.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ct.show();
		ResponseHandler listener = new ResponseHandler(responsed, ct);
		ct.getJConsole().addResponseListener(listener);
		while (true){
			responsed.value = false;
			ct.getJConsole().addText("Como vas? ",JConsole.MESSAGE);

			while (!responsed.value){

			}
		}
	}

	private static class MyBoolean{
		public boolean value;
	}

	private static class ResponseHandler implements ResponseListener {

		private MyBoolean responsed;
		private JConsoleTest ct;

		public ResponseHandler(MyBoolean responsed, JConsoleTest ct){
			this.responsed = responsed;
			this.ct = ct;
		}

		/**
		 * @throws InvalidResponseException
		 * @see org.gvsig.utils.console.ResponseListener#acceptResponse(java.lang.String)
		 */
		public void acceptResponse(String response){
			ct.jButton.setText(response);
			responsed.value = true;
		}

	}
  }
