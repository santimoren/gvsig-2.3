/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils;

import java.util.ArrayList;
import java.util.HashSet;


/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class DefaultCharSet implements SymbolSet {
    private HashSet caracteres = new HashSet();
    private ArrayList intervalos = new ArrayList();

    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     */
    public void addCharacter(char c) {
        caracteres.add(new Character(c));
    }

    /**
     * DOCUMENT ME!
     *
     * @param c1 DOCUMENT ME!
     * @param c2 DOCUMENT ME!
     */
    public void addInterval(char c1, char c2) {
        intervalos.add(new Interval(c1, c2));
    }

    /**
     * @see org.gvsig.utils.SymbolSet#contains(char)
     */
    public boolean contains(char c) {
        if (caracteres.contains(new Character(c))) {
            return true;
        }

        for (int i = 0; i < intervalos.size(); i++) {
            if (((Interval) intervalos.get(i)).contains(c)) {
                return true;
            }
        }

        return false;
    }

    /**
     * DOCUMENT ME!
     *
     * @author Fernando Gonz�lez Cort�s
     */
    public class Interval implements SymbolSet {
        public int ini;
        public int fin;

        /**
         * Crea un nuevo Interval.
         *
         * @param c1 DOCUMENT ME!
         * @param c2 DOCUMENT ME!
         */
        public Interval(char c1, char c2) {
            ini = c1;
            fin = c2;
        }

        /**
         * @see org.gvsig.utils.SymbolSet#contains(char)
         */
        public boolean contains(char c) {
            if ((ini <= c) && (c <= fin)) {
                return true;
            }

            return false;
        }
    }
}
