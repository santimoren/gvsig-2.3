/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.utils.swing.objectSelection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.utils.swing.JComboBox;



/**
 * Control consistente en un texto y un combo autocompletable
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class ObjectSelection extends JPanel {
	private JLabel jLabel = null;
	private JComboBox combo = null;
	private ObjectSelectionModel model;
	private DefaultComboBoxModel cmbModel = new DefaultComboBoxModel();
	private Object selected;
	/**
	 * This is the default constructor
	 */
	public ObjectSelection() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		jLabel = new JLabel();
		this.setSize(504, 265);
		this.add(jLabel, null);
		this.add(getCombo(), null);
	}

	/**
	 * Obtiene una referencia al combo
	 *
	 * @return com.iver.utiles.swing.JComboBox
	 */
	protected JComboBox getCombo() {
		if (combo == null) {
			combo = new JComboBox();
			combo.setModel(cmbModel);
			combo.setEditable(true);
			combo.addItemListener(new java.awt.event.ItemListener() {
					public void itemStateChanged(java.awt.event.ItemEvent e) {
						seleccion();
					}
				});
		}

		return combo;
	}

	/**
	 * M�todo invocado cuando cambia la selecci�n del combo
	 */
	protected void seleccion() {
	}

	/**
	 * Establece el modelo del control
	 *
	 * @param model The model to set.
	 *
	 * @throws SelectionException Si hay alg�n error leyendo los
	 * datos del modelo
	 */
	public void setModel(ObjectSelectionModel model) throws SelectionException {
		this.model = model;

		Object[] objects = model.getObjects();
		cmbModel.removeAllElements();

		for (int i = 0; i < objects.length; i++) {
			cmbModel.addElement(objects[i]);
		}
		
		getCombo().setSelectedIndex(-1);

		jLabel.setText(model.getMsg());
	}

	/**
	 * Obtiene el elemento actualmente seleccionado
	 *
	 * @return Returns the selected item.
	 */
	public Object getSelected() {
		return cmbModel.getSelectedItem();
	}
} //  @jve:decl-index=0:visual-constraint="10,10"
