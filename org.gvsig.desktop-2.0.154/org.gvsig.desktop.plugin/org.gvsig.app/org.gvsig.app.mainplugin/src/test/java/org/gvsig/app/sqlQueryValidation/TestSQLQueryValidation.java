/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.sqlQueryValidation;

import junit.framework.TestCase;

/**
 * Tests the methods of the class SQLQueryValidation
 *    (This class is made without inner static methods for don't create problems to Zql, in this way,
 *     it's possible to add more tests, with or without long queries.)
 * 
 * @author Pablo Piqueras Bartolom� (p_queras@hotmail.com)
 */
 public class TestSQLQueryValidation extends TestCase{
	
	/*
	 *  (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 *  (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * A test (Correct)
	 */
	public void test1() {
		String query = "SELECT r.name, f.id FROM room r, flat f WHERE (r.user_name LIKE 'P%') AND (r.flat = f.id) AND (r.color_wall LIKE 'white') AND (r.height < 2.20);";
		
		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, false);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test2() {
		String query = "SELECT * FROM House;";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, false);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());
			fail();
		}
	}

	/**
	 * A test (Incorrect)
	 */
	public void test3() {
		String query = "SELECT a* FROM House;";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, false);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test4() {
		String query = "SELECT * FROM House;";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, false);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());
			fail();
		}
	}

	/**
	 * A test (Correct)
	 */
	public void test5() {
		String query = "r.level = f.level AND r.user_name LIKE \'P%\';";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());
			fail();
		}
	}

	/**
	 * A test (Incorrect)
	 */
	public void test6() {
		String query = "r.level = f.level a e3 w 	q3 	 �32	9'}97AND r.user_name LIKE \'P%\';";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test7() {
		String query = "\"codigo\" = 'Canal d'Elx'";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test8() {
		String query = "\"codigo\" = 'Canal de la M�nega'";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test9() {
		String query = "\"codigo\" = 'Espa�a'";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test10() {
		String query = "\"codigo\" = ('Canal d'Elx')";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test11() {
		String query = "\"codigo\" = ( 'Canal d'Elx' )";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test12() {
		String query = "\"codigo\" = ( 'Canal d'Elx')";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test13() {
		String query = "\"codigo\" = ('Canal d'Elx' )";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}	
	
	/**
	 * A test (Incorrect)
	 */
	public void test14() {
		String query = "\"codigo\" = ('Canal d' Elx' )";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
		
	/**
	 * A test (Incorrect)
	 */
	public void test15() {
		String query = "\"codigo\" = ('Canal d'Elx d'Alacant' )";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}	
	
	/**
	 * A test (Incorrect)
	 */
	public void test16() {
		String query = "\"fecha\" = Date(11-ene-2004)";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
	
	/**
	 * A test (Correct)
	 */
	public void test17() {
		String query = "\"fecha\" = 11-ene-2004";

		System.out.println("�Es v�lida '" + query + "' ?");
		SQLQueryValidation sqlQueryValidation = new SQLQueryValidation(query, true);

		if (sqlQueryValidation.validateQuery()) {
			System.out.println("Yes.");
		}
		else {
			System.out.println("No.");
			System.out.println(sqlQueryValidation.getErrorPositionAsMessage());
			System.out.println(sqlQueryValidation.getErrorMessage());			
			fail();
		}
	}
 }
