/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import java.util.HashMap;
import java.util.Map;

import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * @author cesar
 * 
 */
public class WindowLayout implements Persistent {

    Map<String, String> data = null;

    public WindowLayout() {
        data = new HashMap<String, String>();
    }

    public void set(String key, String value) {
        data.put(key, value);
    }

    public String get(String key) {
        return (String) data.get(key);
    }

    @SuppressWarnings("unchecked")
    public void loadFromState(PersistentState state)
        throws PersistenceException {
        this.data.clear();
        this.data.putAll((Map<String, String>) state.get("data"));
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("data", data);
    }

}
