/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.swing.JComponent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class ThemeManagerWindowNew extends JPanel implements IWindow, ActionListener {
	private static final Logger logger = LoggerFactory.getLogger(ThemeManagerWindow.class);
			
	private static final long serialVersionUID = 4650656815369149211L;
	private static int selectedTabIndex = 0;
	private static ArrayList<Class<? extends AbstractThemeManagerPage>> pages =
			new ArrayList<Class<? extends AbstractThemeManagerPage>>();
	private FLayer layer;
	//private Legend legend; // Le asignaremos la leyenda del primer tema activo.
	private JTabbedPane tabs = new JTabbedPane();  //  @jve:decl-index=0:
	private JPanel panelButtons;
	private static Hashtable<Class<? extends AbstractThemeManagerPage>, ArrayList<Class<? extends FLayer>>> s =
		new Hashtable<Class<? extends AbstractThemeManagerPage>, ArrayList<Class<? extends FLayer>>>();


	/**
	 * Sobrecarga del constructor. Este se utiliza cuando se llama a este
	 * di�logo desde la barra de comandos.
	 *
	 */
	public ThemeManagerWindowNew(FLayer l) {
		this.layer = l;

		// TODO falta definir leyenda para cualquier tipo de capa
		// y decidir entonces qu� opciones soporta cada una para
		// que el di�logo se autorrellene
		initialize();
	}

	private  void initialize() {
		StringBuffer msgerr = new StringBuffer(); 
		for (int i = 0; i < pages.size(); i++) {
			Class<? extends AbstractThemeManagerPage> pageClass = pages.get(i);
			AbstractThemeManagerPage tab = null;
			try {
				tab = pageClass.newInstance();
			} catch (Exception e) {
				msgerr.append(translate("_Cant_add_property_page_related_to_{0}", pageClass.getName()))
					.append("\n");
				logger.info("Can't instance propety page from class "+pageClass.getName(),e);
				continue;
			}
			if (tab!=null){
                            if( tab.isTabEnabledForLayer(layer) ) {
			    	String tabName = null;
			    	try {
			    		tab.setModel(layer);
			    		tabName = tab.getName();
			    	} catch( Throwable ex) {
						msgerr.append(translate("_Cant_initialice_property_page_{0}", tabName))
							.append("\n");
			    		logger.info("Can't set model of property page '"+tabName+"' from class '"+pageClass.getName()+"'.");
			    	}
			        tabs.addTab(tabName, tab);
			    }
			}
		}

		if (tabs.getComponentCount()>selectedTabIndex) {
			tabs.setSelectedIndex(selectedTabIndex);
		}
		tabs.setPreferredSize(new java.awt.Dimension(640,390));


		setLayout(new BorderLayout());
		add(tabs, java.awt.BorderLayout.CENTER);

		// The listener must be added after the tabs are added to the window
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent e) {
				//remember the visible tab
				selectedTabIndex = tabs.getSelectedIndex();
				if (selectedTabIndex<0) {
					selectedTabIndex = 0;
				}
			};
		});
		add(getPanelButtons(), java.awt.BorderLayout.SOUTH);
		setSize(new Dimension(780, 540));
		if( msgerr.length()>0 ) {
			PluginServices.getMainFrame().messageDialog(msgerr.toString(), "_Warning", JOptionPane.WARNING_MESSAGE);
		}
	}

	private String translate(String msg, String arg1) {
		String[] args = null;
		String translation = null;
        if (msg == null) {
            return "";
        }
        if( arg1 != null ) {
        	args = new String[] { arg1 };
        }
        translation = org.gvsig.i18n.Messages.getText(msg, args);
        if (translation == null) {
        	return "_"+msg.replace("_", " ");
        }
        return translation;
	}
	
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			panelButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5,5));

			JButton btnAceptar = new JButton(PluginServices.getText(this,"Aceptar"));
			btnAceptar.setActionCommand("OK");
			btnAceptar.addActionListener(this);

			JButton btnApply = new JButton(PluginServices.getText(this,"Apply"));
			btnApply.setActionCommand("APPLY");
			btnApply.addActionListener(this);


			JButton btnCancelar = new JButton(PluginServices.getText(this,"Cerrar"));
			btnCancelar.setActionCommand("CANCEL");
			btnCancelar.addActionListener(this);
			panelButtons.setPreferredSize(new java.awt.Dimension(493,33));
			panelButtons.add(btnCancelar);
			panelButtons.add(btnApply);
			panelButtons.add(btnAceptar);
		}
		return panelButtons;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "OK") {
			/* Causes all the tabs in the ThemeManagerWindow to perform THEIR apply-action
			 * then fires the LegendChanged event that causes the view to be refreshed.
			 * After that, it closes the window.
			 */
			actionPerformed(new ActionEvent(e.getSource(), e.getID(), "APPLY"));

			close();
		} else if (e.getActionCommand() == "CANCEL") {
			/* Causes all the tabs in the ThemeManagerWindow to perform THEIR cancel-action
			 * then closes the window.
			 */
			for (int i = 0; i < tabs.getTabCount(); i++) {
				AbstractThemeManagerPage tab = (AbstractThemeManagerPage) tabs.getComponentAt(i);
				tab.cancelAction();
			}
			close();
		} else if (e.getActionCommand() == "APPLY") {
			/* Causes the current visible tab in the ThemeManagerWindow to perform
			 * ITS specific apply-action.
			 */
			for (int i = 0; i < tabs.getTabCount(); i++) {
				AbstractThemeManagerPage tab = (AbstractThemeManagerPage) tabs.getComponentAt(i);
				tab.applyAction();
			}
			layer.getMapContext().callLegendChanged();
		} else {}
//		 Lots of temporary objects were create.
		// let's try some memory cleanup
		System.gc();
	}

	private void close() {
		PluginServices.getMDIManager().closeWindow(this);
	}

	public WindowInfo getWindowInfo() {
		WindowInfo viewInfo = new WindowInfo(WindowInfo.MODELESSDIALOG|WindowInfo.RESIZABLE);
		viewInfo.setWidth(getWidth());
		viewInfo.setHeight(getHeight());
		viewInfo.setTitle(PluginServices.getText(this,"propiedades_de_la_capa"));
		return viewInfo;
	}

        private static class AbstractThemeManagerPageAdapter implements PropertiesPage, PropertiesPageFactory {

            AbstractThemeManagerPage x;
            
            AbstractThemeManagerPageAdapter(AbstractThemeManagerPage x) {
                this.x = x;
            }
            
            public String getTitle() {
                return this.x.getName();
            }

            public int getPriority() {
                return 100;
            }

            public boolean whenAccept() {
                this.x.acceptAction();
                return true;
            }

            public boolean whenApply() {
                this.x.applyAction();
                return true;
            }

            public boolean whenCancel() {
                this.x.cancelAction();
                return true;
            }

            public JComponent asJComponent() {
                return this.x;
            }

            public String getGroupID() {
                return "Layer";
            }

            public boolean isVisible(Object obj) {
                return this.x.isTabEnabledForLayer((FLayer)obj);
            }

            public PropertiesPage create(Object obj) {
                this.x.setModel((FLayer)obj);
                return this;
            }

        }
        
	public static void addPage(Class<? extends AbstractThemeManagerPage> abstractThemeManagerPageClass) {
            
		pages.add(abstractThemeManagerPageClass);
	}

	public static void setTabEnabledForLayer(Class<? extends AbstractThemeManagerPage> abstractThemeManagerPageClass, Class<? extends FLayer> fLayerClazz, boolean enabled) {
		ArrayList<Class<? extends FLayer>> enabledLayers;
		if (enabled == true) {
			if (!s.containsKey(abstractThemeManagerPageClass)) {
				enabledLayers = new ArrayList<Class<? extends FLayer>> ();
				enabledLayers.add(fLayerClazz);
				s.put(abstractThemeManagerPageClass, enabledLayers);
			} else {
				enabledLayers = s.get(abstractThemeManagerPageClass);
				enabledLayers.add(fLayerClazz);
			}
		} else {
			if (!s.containsKey(abstractThemeManagerPageClass)) {
				return;
			}
				enabledLayers = s.get(abstractThemeManagerPageClass);
			enabledLayers.remove(fLayerClazz);
		}
	};

        public static boolean isTabEnabledForLayer(AbstractThemeManagerPage page, FLayer layer) {
            try {
                return s.get(page.getClass()).contains(layer.getClass());            
            } catch( Exception ex) {
                return false;
            }
        }
        
        public Object getWindowProfile() {
		return WindowInfo.TOOL_PROFILE;
	};
}  //  @jve:decl-index=0:visual-constraint="10,10"
