package org.gvsig.app.project.documents.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;

public class ProjectGeneralPropertiesPage extends JPanel implements PropertiesPage {

    private Project project = null;

    private JPanel jPanel = null;
    private JLabel jLabel = null;
    private JLabel jLabel1 = null;
    private JLabel jLabel2 = null;
    private JLabel jLabel3 = null;
    private JLabel jLabel4 = null;
    private JTextField txtName = null;
    private JTextField txtPath = null;
    private JTextField txtCreationDate = null;
    private JTextField txtModificationDate = null;
    private JTextField txtOwner = null;
    private JLabel jLabel5 = null;
    private JTextArea txtComments = null;
    private JLabel jLabel6 = null;
    private JLabel lblColor = null;
    private JButton btnColor = null;
    private JScrollPane jScrollPane = null;

    public ProjectGeneralPropertiesPage(Project project) {
        this.project = project;
        initComponents();
    }

    public boolean whenAccept() {
        return whenApply();
    }

    public boolean whenApply() {
        project.setName(txtName.getText());
        project.setOwner(txtOwner.getText());
        project.setComments(txtComments.getText());
        project.setSelectionColor(lblColor.getBackground());

        return true;
    }

    public boolean whenCancel() {
        return true;
    }

    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("General");
    }

    public int getPriority() {
        return 1000;
    }

    public JComponent asJComponent() {
        return this;
    }

    private void initComponents() {
        setLayout(new GridLayout(1, 1, 0, 0));
        add(getJPanel());

        getTxtName().setText(project.getName());

        String path = ProjectExtension.getPath();

        if ( path != null ) {
            File f = new File(path);
            getTxtPath().setText(f.toString());
        } else {
            getTxtPath().setText("");
        }

        getTxtOwner().setText(project.getOwner());
        getTxtComments().setText(project.getComments());
        getTxtCreationDate().setText(project.getCreationDate());
        getTxtModificationDate().setText(project.getModificationDate());

        getLblColor().setBackground(project.getSelectionColor());
    }

    /**
     * This method initializes jPanel
     *
     * @return JPanel
     */
    private JPanel getJPanel() {
        if ( jPanel == null ) {

            jPanel = new JPanel(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.insets = new Insets(2, 5, 0, 5);
            c.gridy = -1;

            addRow(c, getJLabel(), getTxtName());
            addRow(c, getJLabel1(), getTxtPath());
            addRow(c, getJLabel2(), getTxtCreationDate());
            addRow(c, getJLabel3(), getTxtModificationDate());
            addRow(c, getJLabel4(), getTxtOwner());

            c.anchor = GridBagConstraints.WEST;
            c.weightx = 0.0d;
            c.gridx = 0;
            c.gridy++;
            jPanel.add(getJLabel6(), c);

            JPanel colorPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            colorPanel.add(getLblColor());
            colorPanel.add(getBtnColor());

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0d;
            c.gridx = 1;
            jPanel.add(colorPanel, c);

            c.anchor = GridBagConstraints.WEST;
            c.weightx = 0.0d;
            c.gridx = 0;
            c.gridy++;
            jPanel.add(getJLabel5(), c);

            c.fill = GridBagConstraints.BOTH;
            c.weightx = 1.0d;
            c.gridy++;
            c.gridwidth = 2;
            jPanel.add(getJScrollPane(), c);

            c.anchor = GridBagConstraints.EAST;
            c.gridx = 0;
            c.gridwidth = 2;
            c.gridy++;
            c.weightx = 1.0d;
        }

        return jPanel;
    }

    private void addRow(GridBagConstraints c, JComponent label, JComponent text) {
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 0.0d;
        c.gridx = 0;
        c.gridy++;
        jPanel.add(label, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.gridx = 1;
        jPanel.add(text, c);
    }

    /**
     * This method initializes jLabel
     *
     * @return JLabel
     */
    private JLabel getJLabel() {
        if ( jLabel == null ) {
            jLabel = new JLabel();
            jLabel.setText(PluginServices.getText(this, "nombre_sesion") + ":");
        }

        return jLabel;
    }

    /**
     * This method initializes jLabel1
     *
     * @return JLabel
     */
    private JLabel getJLabel1() {
        if ( jLabel1 == null ) {
            jLabel1 = new JLabel();
            jLabel1.setText(PluginServices.getText(this, "path") + ":");
        }

        return jLabel1;
    }

    /**
     * This method initializes jLabel2
     *
     * @return JLabel
     */
    private JLabel getJLabel2() {
        if ( jLabel2 == null ) {
            jLabel2 = new JLabel();
            jLabel2
                    .setText(PluginServices.getText(this, "creation_date") + ":");
        }

        return jLabel2;
    }

    /**
     * This method initializes jLabel3
     *
     * @return JLabel
     */
    private JLabel getJLabel3() {
        if ( jLabel3 == null ) {
            jLabel3 = new JLabel();
            jLabel3.setText(PluginServices.getText(this, "modification_date")
                    + ":");
        }

        return jLabel3;
    }

    /**
     * This method initializes jLabel4
     *
     * @return JLabel
     */
    private JLabel getJLabel4() {
        if ( jLabel4 == null ) {
            jLabel4 = new JLabel();
            jLabel4.setText(PluginServices.getText(this, "owner") + ":");
        }

        return jLabel4;
    }

    /**
     * This method initializes txtName
     *
     * @return JTextField
     */
    private JTextField getTxtName() {
        if ( txtName == null ) {
            txtName = new JTextField(20);
        }

        return txtName;
    }

    /**
     * This method initializes txtPath
     *
     * @return JTextField
     */
    private JTextField getTxtPath() {
        if ( txtPath == null ) {
            txtPath = new JTextField(20);
            txtPath.setEditable(false);
            txtPath.setBackground(java.awt.Color.white);
        }

        return txtPath;
    }

    /**
     * This method initializes txtCreationDate
     *
     * @return JTextField
     */
    private JTextField getTxtCreationDate() {
        if ( txtCreationDate == null ) {
            txtCreationDate = new JTextField(20);
            txtCreationDate.setEditable(false);
            txtCreationDate.setBackground(java.awt.Color.white);
        }

        return txtCreationDate;
    }

    /**
     * This method initializes txtModificationDate
     *
     * @return JTextField
     */
    private JTextField getTxtModificationDate() {
        if ( txtModificationDate == null ) {
            txtModificationDate = new JTextField(20);
            txtModificationDate.setEditable(false);
            txtModificationDate.setBackground(java.awt.Color.white);
        }

        return txtModificationDate;
    }

    /**
     * This method initializes txtOwner
     *
     * @return JTextField
     */
    private JTextField getTxtOwner() {
        if ( txtOwner == null ) {
            txtOwner = new JTextField(20);
        }

        return txtOwner;
    }

    /**
     * This method initializes jLabel5
     *
     * @return JLabel
     */
    private JLabel getJLabel5() {
        if ( jLabel5 == null ) {
            jLabel5 = new JLabel();
            jLabel5.setText(PluginServices.getText(this, "comentarios") + ":");
        }

        return jLabel5;
    }

    /**
     * This method initializes txtComments
     *
     * @return JTextArea
     */
    private JTextArea getTxtComments() {
        if ( txtComments == null ) {
            txtComments = new JTextArea();
            txtComments.setRows(4);
            txtComments.setColumns(20);
        }

        return txtComments;
    }

    /**
     * This method initializes jLabel6
     *
     * @return JLabel
     */
    private JLabel getJLabel6() {
        if ( jLabel6 == null ) {
            jLabel6 = new JLabel();
            jLabel6.setText(PluginServices.getText(this, "selection_color")
                    + ":");
        }

        return jLabel6;
    }

    /**
     * This method initializes lblColor
     *
     * @return JLabel
     */
    private JLabel getLblColor() {
        if ( lblColor == null ) {
            lblColor = new JLabel();

            lblColor.setPreferredSize(getBtnColor().getPreferredSize());
            lblColor.setOpaque(true);
        }

        return lblColor;
    }

    /**
     * This method initializes btnColor
     *
     * @return JButton
     */
    private JButton getBtnColor() {
        if ( btnColor == null ) {
            btnColor
                    = ToolsSwingLocator.getUsabilitySwingManager().createJButton(
                            "...");

            btnColor.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    Color ret
                            = JColorChooser.showDialog(ProjectGeneralPropertiesPage.this,
                                    PluginServices.getText(this, "selection_color"),
                                    lblColor.getBackground());

                    if ( ret != null ) {
                        lblColor.setBackground(ret);
                    }
                }
            });
        }

        return btnColor;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return JScrollPane
     */
    private JScrollPane getJScrollPane() {
        if ( jScrollPane == null ) {
            jScrollPane = new JScrollPane(getTxtComments());
            Dimension dim = getTxtComments().getPreferredSize();
            jScrollPane
                    .setMinimumSize(new Dimension(dim.width, dim.height + 10));
        }

        return jScrollPane;
    }

}
