/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.gui.styling.SymbolSelector;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.legend.gui.ISymbolSelector;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocItemLeaf;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.IClassifiedVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.impl.MultiLayerFillSymbol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Directly opens the Symbol Selector if it is invoked from the TOC avoiding
 * by-passing the Layer Properties window
 */
public class ChangeSymbolTocMenuEntry extends AbstractTocContextMenuAction {
    private static final Logger logger = LoggerFactory
            .getLogger(ChangeSymbolTocMenuEntry.class);

	public String getGroup() {
		return "group1"; //FIXME
	}

	public int getGroupOrder() {
		return 10;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "change_symbol");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return true;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		return isTocItemLeaf(item);
	}

	public void execute(ITocItem item, FLayer[] selectedItems) {

		boolean showDialog = isTocItemLeaf(item);

		if (!showDialog) {
			return;
		}

		try {
			if(!(selectedItems[0] instanceof FLyrVect))
				return;
			FLyrVect layer = (FLyrVect) selectedItems[0];
			ISymbol oldSymbol = ((TocItemLeaf) item).getSymbol();
			// patch, figure out an ellegant way to solve this
			if (oldSymbol instanceof MultiLayerFillSymbol) {
				MultiLayerFillSymbol ms = (MultiLayerFillSymbol) oldSymbol;
				for (int i = 0; i < ms.getLayerCount(); i++) {
					if (ms.getLayer(i).getClass().getName().equals("org.gvsig.symbology.symbols.DotDensityFillSymbol")) {
						/*
						 * since dot density symbol works together with the
						 * legend, there is no way to edit it from the symbol selector and editor
						 * we have to break now!
						 */
						return;
					}
				}
			}
			// end patch


			int shapeType = ((IVectorLegend) layer.getLegend()).getShapeType();
			if (shapeType == 0) {
				logger.debug("Error legend " + layer.getLegend()
                        + " does not have shapetype initialized");
				shapeType = layer.getShapeType();
			}

			ISymbolSelector symSel = null;

			try {
				symSel = SymbolSelector.createSymbolSelector(oldSymbol, shapeType);
			} catch (IllegalArgumentException iaEx) {
				/*
				 * this usually happens when the Legend has a composed
				 * symbol that collides with the normal operation and
				 * it only can be changed from the panel of the legend
				 */
				// TODO throw a signal and show a warning message box telling
				// that the operation cannot be performed
				return;
			}
			PluginServices.getMDIManager().addWindow(symSel);
			ISymbol newSymbol = (ISymbol) symSel.getSelectedObject();

			if (newSymbol == null) {
				return;
			}

			newSymbol.setDescription(oldSymbol.getDescription());

			for (int i = 0; i < selectedItems.length; i++) {
				FLyrVect theLayer = ((FLyrVect) selectedItems[i]);

				try {
					ILegend legend = theLayer.getLegend();
					if (legend instanceof IClassifiedVectorLegend) {
						IClassifiedVectorLegend cv = (IClassifiedVectorLegend) legend;
						cv.replace(oldSymbol, newSymbol);
					} else if (legend instanceof ISingleSymbolLegend) {
						ISingleSymbolLegend ss = (ISingleSymbolLegend) legend;
						ss.setDefaultSymbol(newSymbol);
					}
				} catch (Exception e) {
					NotificationManager.addWarning(PluginServices.getText(this, "skipped_symbol_changed_for_layer")+": "+theLayer.getName(), e);
				}

			}
		     // refresh view treak
			MapContext mc = layer.getMapContext();
			mc.invalidate();
			Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
			project.setModified(true);
			mc.callLegendChanged();

		} catch (ReadException e) {
			NotificationManager.addError(PluginServices.getText(this, "getting_shape_type"), e);
		}
	}
}

