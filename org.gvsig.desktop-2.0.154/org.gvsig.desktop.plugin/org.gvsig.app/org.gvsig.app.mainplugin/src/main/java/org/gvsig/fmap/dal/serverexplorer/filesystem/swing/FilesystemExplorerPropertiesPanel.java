
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.swing.api.Component;


public interface FilesystemExplorerPropertiesPanel extends Component {

    /**
     * Sets the value of parameters in the panel.
     * 
     * @param parameters to get the values and set in the panel
     */
    public void putParameters(DynObject parameters);

    /**
     * Gets the values of from the panel and set in the passed parameters.
     * 
     * @param parameters to put in the panel values
     */
    public void fetchParameters(DynObject parameters);
    
    public void setExcludeGeometryOptions(boolean exclude);
    
    public boolean getExcludeGeometryOptions();
}
