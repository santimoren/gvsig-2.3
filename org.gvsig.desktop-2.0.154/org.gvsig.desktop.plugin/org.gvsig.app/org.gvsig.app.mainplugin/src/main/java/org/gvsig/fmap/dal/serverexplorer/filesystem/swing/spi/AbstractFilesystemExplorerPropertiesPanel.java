
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing.spi;

import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanel;


public abstract class AbstractFilesystemExplorerPropertiesPanel implements FilesystemExplorerPropertiesPanel {

    private boolean excludeGeometryOptions = false;
    
    public boolean getExcludeGeometryOptions() {
        return excludeGeometryOptions;
    }    
    
    @Override
    public void setExcludeGeometryOptions(boolean excludeGeometryOptions) {
        this.excludeGeometryOptions = excludeGeometryOptions;
    }
    
    
}
