/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;


public class EditingListenerExtension extends Extension {
    private EditingListenerPanel win = null;

    public void initialize() {
    }

    public void execute(String actionCommand) {
        if( "tools-devel-show-editing-listener".equalsIgnoreCase(actionCommand) ) {
            if( this.win == null || this.win.isClosed() ) {
                this.win = new EditingListenerPanel();
                WindowManager winmgr = ToolsSwingLocator.getWindowManager();
                winmgr.showWindow(win, "Editing monitor", WindowManager.MODE.TOOL);
            }
        }
    }

    public boolean isEnabled() {
        if( this.win == null || this.win.isClosed() ) {
            return true;
        }
        return false;
    }

    public boolean isVisible() {
        return true;
    }
    
}
