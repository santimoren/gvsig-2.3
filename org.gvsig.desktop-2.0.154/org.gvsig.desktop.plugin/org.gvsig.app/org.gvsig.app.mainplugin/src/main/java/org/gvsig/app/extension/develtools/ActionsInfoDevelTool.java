/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.util.Iterator;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class ActionsInfoDevelTool {

	public void showActions() {
		String html = "<html>\n<body>\n"+ this.getActionsInformation(null)+"</body>\n</html>\n";
		
		InfoPanel.save2file("actions-report", html);
		InfoPanel.showPanel("Actions information", WindowManager.MODE.WINDOW, html);
	}

	public void showReportOfPlugin() {
		ApplicationManager application = ApplicationLocator.getManager();
		String pluginName = null;
		
		pluginName = application.inputDialog("Introduzca el nombre del plugin del que desea el informe", "Plugin actions report");
		if( pluginName == null ) {
			return;
		}
		String actionsHtml = this.getActionsInformation(pluginName);
		String menusHtml = new MenusDevelTool().getMenusOfPlugin(pluginName);
		String iconsHtml = new IconThemeDevelTool().getIconThemeInformationOfPlugin(pluginName);
		
		String html = "<html>\n<body>\n"+actionsHtml+"<br>"+menusHtml+"<br>"+iconsHtml+"</body>\n</html>\n";
		
		InfoPanel.save2file("plugin-report", html);
		InfoPanel.showPanel("Plugin report", WindowManager.MODE.WINDOW, html);
	}

	
	
	public String getActionsInformation(String pluginName) {
		
		String previousPluginName = null;
		
		String warning_open = "<b>";
		String warning_close = "</b>";
		
		String error_open = "<b><font color=\"red\">";
		String error_close = "</font></b>";

		StringBuffer buffer = new StringBuffer();

		buffer.append("<html>\n");
		buffer.append("<body>\n");
		
		buffer.append("<div>\n");
		buffer.append("<h2>Actions information </h2>\n");
		buffer.append("<br>\n");

		buffer.append("<table border=\"0\">\n");
		buffer.append("  <tr>\n");
		buffer.append("    <td>Plugin</td>\n");
		buffer.append("    <td>Extension</td>\n");
		buffer.append("    <td>Name</td>\n");
		buffer.append("    <td>Position</td>\n");
		buffer.append("    <td>Text</td>\n");
		buffer.append("    <td>Command</td>\n");
		buffer.append("    <td>Icon</td>\n");
		buffer.append("    <td>Tip</td>\n");
		buffer.append("    <td>Accelerator</td>\n");
		buffer.append("  </tr>\n");
		Iterator<ActionInfo> actions = PluginsLocator.getActionInfoManager().getActions(); 
		while (actions.hasNext()) {
			ActionInfo action = actions.next();
			if( pluginName!=null && !pluginName.equalsIgnoreCase(action.getPluginName()) ) {
				continue;
			}
			if( previousPluginName!=null && !action.getPluginName().equals(previousPluginName) ) {
				buffer
					.append("  <tr>\n")
					.append("    <td colspan=\"9\"><hr></td>\n")
					.append("  </tr>\n");
				previousPluginName = action.getPluginName();
			}
			buffer.append("  <tr valign=\"top\">\n");
			buffer.append("    <td>")
				.append(action.getPluginName())
				.append("</td>\n");
			buffer.append("    <td>")
				.append(action.getExtensionName())
				.append("</td>\n");
			if( action.getName().startsWith("anonymous__") ) {
				buffer.append("    <td title=\"Missing action name, asigned one by default\">")
					.append(error_open)
					.append(action.getName())
					.append(error_close)
					.append("</td>\n");
			} else if( !action.getName().contains("-") ) {
				buffer.append("    <td title=\"Name don't have recomended syntax\">")
					.append(warning_open)
					.append(action.getName())
					.append(warning_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(action.getName())
					.append("</td>\n");
			}  
			if( action.getPosition()<100000000 ) {
				buffer.append("    <td title=\"Incorrect position\">")
					.append(error_open)
					.append(MenusDevelTool.formatPosition(action.getPosition()))
					.append(error_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(MenusDevelTool.formatPosition(action.getPosition()))
					.append("</td>\n");
			}
			if( action.getLabel()==null ) {
				buffer.append("    <td title=\"Missing label of action\">")
					.append(error_open)
					.append("(missing)")
					.append(error_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(action.getLabel())
					.append("</td>\n");
			}
			if( action.getCommand()==null ) {
				buffer.append("    <td title=\"Missing command of action\">")
					.append(error_open)
					.append(action.getCommand())
					.append(error_close)
					.append("</td>\n");
				
			} else if(!action.getCommand().contains("-") && !action.getCommand().equals(action.getName())) {
				buffer.append("    <td title=\"Command don't have recomended syntax and differ fron action name\">")
					.append(warning_open)
					.append(action.getCommand())
					.append(warning_close)
					.append("</td>\n");
				
			} else if(!action.getCommand().contains("-") ) {
				buffer.append("    <td title=\"Command don't have recomended syntax\">")
					.append(warning_open)
					.append(action.getCommand())
					.append(warning_close)
					.append("</td>\n");
				
			} else if(!action.getCommand().equals(action.getName())) {
				buffer.append("    <td title=\"Command differ fron action name\">")
					.append(warning_open)
					.append(action.getCommand())
					.append(warning_close)
					.append("</td>\n");
				
			} else {
				buffer.append("    <td>")
					.append(action.getCommand())
					.append("</td>\n");
			}
			if( action.getIconName()==null ) {
				buffer.append("    <td title=\"Missing icon name\">")
					.append(error_open)
					.append("(missing)")
					.append(error_close)
					.append("</td>\n");
			} else if( action.getIcon()==null ) {
				buffer.append("    <td title=\"Missing icon\">")
					.append(error_open)
					.append(action.getIconName())
					.append(error_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(action.getIconName())
					.append("</td>\n");
			}
			if( action.getTooltip()==null ) {
				buffer.append("    <td title=\"Missing tooltip\">")
					.append(error_open)
					.append(action.getTooltip())
					.append(error_close)
					.append("</td>\n");
				
			} else {
				buffer.append("    <td>")
					.append(action.getTooltip())
					.append("</td>\n");
			}
			if( action.getAccelerator()!=null && action.getKeyStroke()==null ) {
				buffer.append("    <td title=\"Incorrect syntax of the accelerator key\">")
					.append(error_open)
					.append(action.getAccelerator())
					.append(error_close)
					.append("</td>\n");
				
			} else {
				buffer.append("    <td>")
					.append(action.getAccelerator())
					.append("</td>\n");
			}
			buffer.append("  </tr>\n");
			
		}
		buffer.append("</table>\n");
		buffer.append("</div>\n");

		return buffer.toString();
	}


}
