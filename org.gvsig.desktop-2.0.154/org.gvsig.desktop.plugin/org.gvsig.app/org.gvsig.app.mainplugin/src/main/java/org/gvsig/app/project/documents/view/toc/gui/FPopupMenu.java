/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.IContextMenuAction;
import org.gvsig.app.project.documents.view.IContextMenuActionWithIcon;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.app.project.documents.view.toc.actions.ChangeNameTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.ChangeSymbolTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.CopyLayersTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.CutLayersTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.EliminarCapaTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.FirstLayerTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.LayersGroupTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.LayersUngroupTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.OldTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.actions.PasteLayersTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.ReloadLayerTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.ShowLayerErrorsTocMenuEntry;
import org.gvsig.app.project.documents.view.toc.actions.ZoomAlTemaTocMenuEntry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;

/**
 * Menu de bot�n derecho para el TOC.
 * Se pueden a�adir entradas facilmente desde una extensi�n,
 * creando una clase derivando de TocMenuEntry, y a�adiendola en
 * est�tico (o en tiempo de carga de la extensi�n) a FPopupMenu.
 * (Las entradas actuales est�n hechas de esa manera).
 *
 * @author vcn To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 *         Comments
 */

public class FPopupMenu extends JPopupMenu {

    private static final long serialVersionUID = -7420923229618207434L;
    private DefaultMutableTreeNode nodo;
    protected MapContext mapContext;
    private ExtensionPoint extensionPoint;
    private FLayer[] selecteds;

    public static void registerExtensionPoint() {
    	ProjectManager projectManager = ProjectManager.getInstance();
    	ViewManager viewManager = (ViewManager) projectManager.getDocumentManager(ViewManager.TYPENAME);

    	viewManager.addTOCContextAction("FSymbolChangeColor", new ChangeSymbolTocMenuEntry());
        viewManager.addTOCContextAction("ChangeName", new ChangeNameTocMenuEntry());
//        viewManager.addTOCContextAction("FLyrVectEditProperties", new FLyrVectEditPropertiesTocMenuEntry());
        viewManager.addTOCContextAction("ZoomAlTema", new ZoomAlTemaTocMenuEntry());
        viewManager.addTOCContextAction("EliminarCapa", new EliminarCapaTocMenuEntry());
        viewManager.addTOCContextAction("VerErroresCapa", new ShowLayerErrorsTocMenuEntry());
        viewManager.addTOCContextAction("ReloadLayer", new ReloadLayerTocMenuEntry());
        viewManager.addTOCContextAction("LayersGroup", new LayersGroupTocMenuEntry());
        viewManager.addTOCContextAction("LayersUngroup", new LayersUngroupTocMenuEntry());
        viewManager.addTOCContextAction("FirstLayer", new FirstLayerTocMenuEntry());

        viewManager.addTOCContextAction("Copy", new CopyLayersTocMenuEntry());
        viewManager.addTOCContextAction("Cut", new CutLayersTocMenuEntry());
        viewManager.addTOCContextAction("Paste", new PasteLayersTocMenuEntry());
    }

    /**
     * Creates a new FPopupMenu object.
     *
     * @param nodo
     *            DOCUMENT ME!
     * @param vista
     *            DOCUMENT ME!
     */
    public FPopupMenu(MapContext mc, DefaultMutableTreeNode node) {
        super();
        this.initialize(mc, node);
    }

    private void initialize(MapContext mc, DefaultMutableTreeNode node) {
        this.mapContext = mc;
        this.nodo = node;

        this.extensionPoint =
            ToolsLocator.getExtensionPointManager().get("View_TocActions");
        this.selecteds = this.mapContext.getLayers().getActives();

        List<IContextMenuAction> actions = this.getActionList();
        if (actions == null) {
            return;
        }
        this.createMenuElements(actions);
    }

    public MapContext getMapContext() {
        return mapContext;
    }

    public ITocItem getNodeUserObject() {
        if (nodo == null) {
            return null;
        }
        return (ITocItem) nodo.getUserObject();
    }

    public DefaultMutableTreeNode getNode() {
        return this.nodo;
    }

    private List<IContextMenuAction> getActionList() {
        List<IContextMenuAction> actions =  new ArrayList<IContextMenuAction>();
        @SuppressWarnings("unchecked")
        Iterator<Extension> iter = this.extensionPoint.iterator();
        IContextMenuAction action;
        boolean contains = false;
        ITocItem tocItem = this.getNodeUserObject();
        if (tocItem instanceof TocItemBranch) {
            for (int i = 0; i < this.selecteds.length; i++) {
                if (this.selecteds[i].equals(((TocItemBranch) tocItem)
                    .getLayer())) {
                    contains = true;
                    break;
                }
            }
        } else {
            contains = true;
        }
        if (contains) {
            while (iter.hasNext()) {
                action = null;
                try {
                    action = (IContextMenuAction) iter.next().create();
                } catch (Exception e) {
                    NotificationManager.addError(e);
                }
                if (action != null ) {
                	if( action instanceof AbstractTocContextMenuAction
                		&& !(action instanceof OldTocContextMenuAction)) {
                		((AbstractTocContextMenuAction)action).setMapContext(this.mapContext);
                	}
                    if (action.isVisible(this.getNodeUserObject(),
                        this.selecteds)) {
                        actions.add(action);
                    }
                }

            }
            Collections.sort(actions, new CompareAction());
            return actions;
        }
        return null;
    }

    public class CompareAction implements Comparator<IContextMenuAction> {

        public int compare(IContextMenuAction o1, IContextMenuAction o2) {
            // FIXME: flata formatear los enteros!!!!
            NumberFormat formater = NumberFormat.getInstance();
            formater.setMinimumIntegerDigits(6);
            String key1 =
                "" + formater.format(o1.getGroupOrder()) + o1.getGroup()
                    + formater.format(o1.getOrder());
            String key2 =
                "" + formater.format(o2.getGroupOrder()) + o2.getGroup()
                    + formater.format(o2.getOrder());
            return key1.compareTo(key2);
        }
    }

    private void createMenuElements(List<IContextMenuAction> actions) {
        String group = null;
        for (IContextMenuAction action : actions) {
            MenuItem item = new MenuItem(action.getText(), action);
            item.setEnabled(action.isEnabled(this.getNodeUserObject(),
                this.selecteds));
            if( action.getGroup()!=null ) {
	            if ( !action.getGroup().equals(group)) {
	                if (group != null) {
	                    this.addSeparator();
	                }
	                group = action.getGroup();
	            }
            }
            this.add(item);
        }

    }

    public class MenuItem extends JMenuItem implements ActionListener {

        private static final long serialVersionUID = 6858724610206253411L;
        private IContextMenuAction action;

        public MenuItem(String text, IContextMenuAction documentAction) {
            super(text);
            this.action = documentAction;
            if( this.action instanceof IContextMenuActionWithIcon ) {
                this.setIcon(((IContextMenuActionWithIcon)this.action).getIcon());
            }
            String tip = this.action.getDescription();
            if (tip != null && tip.length() > 0) {
                this.setToolTipText(tip);
            }
            this.addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            this.action.execute(FPopupMenu.this.getNodeUserObject(),
                FPopupMenu.this.selecteds);
            if (PluginServices.getMainFrame() != null) {
                PluginServices.getMainFrame().enableControls();
            }
        }
    }

}
