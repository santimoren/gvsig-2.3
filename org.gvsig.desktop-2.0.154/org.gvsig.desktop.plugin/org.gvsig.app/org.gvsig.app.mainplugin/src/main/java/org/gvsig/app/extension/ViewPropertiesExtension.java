/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.gui.preferencespage.LayerOrderPage;
import org.gvsig.app.gui.preferencespage.ViewBehaviorPage;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.app.project.documents.view.gui.ViewProperties;

public class ViewPropertiesExtension extends Extension implements IPreferenceExtension {

    private ViewBehaviorPage vb = new ViewBehaviorPage();
    private LayerOrderPage lo_page = new LayerOrderPage();

    public void initialize() {
        IconThemeHelper.registerIcon("preferences", "layer-order-preferences", this);
    }

    public void execute(String s) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();

        if (s.equalsIgnoreCase("view-properties")) {
            ViewProperties viewProperties = new ViewProperties(document);
            application.getUIManager().addWindow(viewProperties);
            if (viewProperties.isAcceppted()) {
                document.setModified(true);
            }
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        return application.getActiveComponent(ViewDocument.class) != null;
    }

    public IPreference[] getPreferencesPages() {
        return new IPreference[]{vb, lo_page};
    }
}
