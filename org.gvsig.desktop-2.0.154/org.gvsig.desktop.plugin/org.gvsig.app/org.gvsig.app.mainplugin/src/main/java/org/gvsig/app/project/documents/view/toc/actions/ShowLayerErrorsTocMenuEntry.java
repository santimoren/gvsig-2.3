/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: ShowLayerErrorsTocMenuEntry.java 31496 2009-11-04 12:53:20Z jjdelcerro $
 * $Log$
 * Revision 1.4  2007-03-06 16:37:08  caballero
 * Exceptions
 *
 * Revision 1.3  2007/02/20 15:52:17  caballero
 * no modified
 *
 * Revision 1.2  2007/01/04 07:24:31  caballero
 * isModified
 *
 * Revision 1.1  2006/09/21 18:26:13  azabala
 * first version in cvs
 *
 *
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class ShowLayerErrorsTocMenuEntry extends AbstractTocContextMenuAction {

    @Override
    public void execute(ITocItem item, FLayer[] selectedItems) {
        FLayer layer = getNodeLayer(item);
        LayerErrorsPanel panel = new LayerErrorsPanel(layer);
        WindowManager wm = ToolsSwingLocator.getWindowManager();
        wm.showWindow(panel, "Errors in layer '"+layer.getName()+"'", WindowManager.MODE.WINDOW);
    }

    @Override
    public String getText() {
        return PluginServices.getText(this, "ver_error_capa");
    }

    @Override
    public String getGroup() {
        return "group3"; //FIXME
    }

    @Override
    public int getGroupOrder() {
        return 30;
    }

    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
        return !getNodeLayer(item).isOk();
    }

    @Override
    public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
        return isTocItemBranch(item);

    }
}
