/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import java.awt.Dimension;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.i18n.Messages;
import org.gvsig.propertypage.BasePropertiesPageDialog;

/**
 * Dialogo donde se muestran las propiedades de una vista
 * 
 * @author gvSIG Team
 */
public class ViewProperties extends BasePropertiesPageDialog implements SingletonWindow {

    private static final long serialVersionUID = 5328865370833315385L;
    private ViewDocument viewDocument;
    
    /**
     * This is the default constructor
     * 
     * @param viewDocument
     *            Vista que se representa
     */
    public ViewProperties(ViewDocument viewDocument) {
        this.viewDocument = viewDocument;
        this.init(ViewDocument.VIEW_PROPERTIES_PAGE_GROUP,viewDocument);
    }

    @Override
    protected boolean useTabsAlwais() {
        return super.useTabsAlwais();
        // return true;
    }

    
    public boolean isAcceppted() {
        return this.getUserAction()==BasePropertiesPageDialog.ACTION_ACCEPT;
    }

    public Object getWindowModel() {
        return viewDocument;
    }

    public Object getWindowProfile() {
        return WindowInfo.PROPERTIES_PROFILE;
    }
    
    public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo = new WindowInfo(
                WindowInfo.RESIZABLE |
                WindowInfo.MAXIMIZABLE |
                WindowInfo.MAXIMIZABLE |
                WindowInfo.ICONIFIABLE
        );
        
        this.validate();
        m_viewinfo.setTitle(Messages.getText("propiedades_vista"));
        Dimension dim = getPreferredSize();
        m_viewinfo.setWidth(dim.width);
        m_viewinfo.setHeight(dim.height);
        return m_viewinfo;
    }    

    @Override
    protected void closeDialog() {
        PluginServices.getMDIManager().closeWindow(this);
    }
    
    
}
