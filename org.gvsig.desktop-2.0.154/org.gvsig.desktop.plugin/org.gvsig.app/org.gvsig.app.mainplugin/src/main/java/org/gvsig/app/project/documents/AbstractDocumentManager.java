/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.app.project.documents;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.DocumentsContainer;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.extensionpoint.ExtensionBuilder;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.identitymanagement.UnauthorizedException;
import org.gvsig.tools.observer.BaseNotification;
import org.gvsig.tools.observer.Notification;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.swing.api.Component;



/**
 * Factory of ProjectDocument.
 * 
 */

public abstract class AbstractDocumentManager implements DocumentManager, Observer {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDocumentManager.class);

    
    private Map<String, JComponent> mainComponents = new HashMap<String, JComponent>();

    private DelegateWeakReferencingObservable observableHelper = null;

    protected AbstractDocumentManager() {
        this.observableHelper = new DelegateWeakReferencingObservable(this);
    }
    
    /**
     * Returns the type of document priority.
     *
     * This priority is used when order document factories in UI.
     * 
     * @return Priority.
     */
    public int getPriority() {
        return 10;
    }

    /**
     * Returns the icon for the type of document.
     *
     * @return Image.
     */
    public ImageIcon getIcon() {
        return PluginServices.getIconTheme().get("document-icon-sel");
    }

    /**
     * Returns the icon for the type of document when is selected
     *
     * @return Image.
     */
    public ImageIcon getIconSelected() {
        return PluginServices.getIconTheme().get("document-icon");
    }

    /**
     * Returns the title of type of document.
     *
     * @return String title for type of document
     */
    public String getTitle() {
        return PluginServices.getText(this, "documento");
    }

    public AbstractDocument createDocumentByUser() {
        return createDocument();
    }

    public Iterator<? extends Document> createDocumentsByUser() {
        Set<AbstractDocument> doc = new HashSet<AbstractDocument>(1);
        doc.add(createDocumentByUser());
        return doc.iterator();
    }

    /**
     * @see ExtensionBuilder
     */
    public Object create() {
        return this;
    }

    /**
     * @see ExtensionBuilder
     */
    public Object create(Object[] args) {
        return this;
    }

    /**
     * @see ExtensionBuilder
     */
    @SuppressWarnings("rawtypes")
    public Object create(Map args) {
        return this;
    }

    @Override
    public IWindow getMainWindow(Document doc) {
        return getMainWindow(doc, null);
    }

    @Override
    public IWindow getMainWindow(Document doc, WindowLayout layout) {
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        if( ! identityManager.getCurrentIdentity().isAuthorized(Document.ACCESS_DOCUMENT_AUTHORIZATION, this, this.getTypeName()) ) {
            throw new UnauthorizedException(Document.ACCESS_DOCUMENT_AUTHORIZATION, this, this.getTypeName());
        }   
        IDocumentWindow win;

        win = (IDocumentWindow) PluginServices.getMDIManager().getSingletonWindow(getMainWindowClass(), doc);
        return win;
    }

    @Override
    public IWindow getPropertiesWindow(Document doc) {
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        if( ! identityManager.getCurrentIdentity().isAuthorized(Document.ACCESS_DOCUMENT_AUTHORIZATION, this, this.getTypeName()) ) {
            throw new UnauthorizedException(Document.ACCESS_DOCUMENT_AUTHORIZATION, this, this.getTypeName());
        }   
        return null;
    }
    
    
    
    @Override
    public JComponent getMainComponent(Document doc) {
        ApplicationManager application = ApplicationLocator.getManager();
        Document activeDoc = application.getActiveDocument();
        if( activeDoc == doc ) { // Ojo, hay que cerciorarse de que esta comparacion funciona.
            IWindow win = this.getMainWindow(doc);
            if( win instanceof Component ) {
                return ((Component)win).asJComponent();
            }
            return (JComponent)win;
        }
        if( activeDoc instanceof DocumentsContainer ) {
            DocumentsContainer container = (DocumentsContainer)activeDoc;
            return this.getMainComponent(container, doc);
        }
        return null;
    }
    
    public JComponent getMainComponent(DocumentsContainer container, Document doc) {
        Formatter f = new Formatter();
        f.format("08X",container.hashCode());
        f.format("08X",doc.hashCode());
        String key = f.toString();
        f.close();
        return mainComponents.get(key);
    }

    public void unregisterMainComponent(DocumentsContainer container, Document doc) {
        Formatter f = new Formatter();
        f.format("08X",container.hashCode());
        f.format("08X",doc.hashCode());
        String key = f.toString();
        f.close();
        mainComponents.remove(key);
    }

    public void registerMainComponent(DocumentsContainer container, Document doc, JComponent component) {
        Formatter f = new Formatter();
        f.format("08X",container.hashCode());
        f.format("08X",doc.hashCode());
        String key = f.toString();
        f.close();
        mainComponents.put(key,component);
    }
    
    /**
     * Return true if the name exists to another document.
     *
     * @param project
     * @param documentName
     *
     * @return True if the name exists.
     * @deprecated use instead  project.getProjectDocument
     */
    public boolean existName(Project project, String documentName) {
        return project.getDocument(documentName, getTypeName())!=null;
    }

    /**
     * Return the class or interface for the documents managed by this factory.
     * @return The document class;
     */
    @SuppressWarnings("rawtypes")
    abstract protected Class getDocumentClass();

    public Object createFromState(PersistentState state) throws PersistenceException {
        Document doc = this.createDocument();
        return doc;
    }

    public void loadFromState(PersistentState state, Object object) throws PersistenceException {
        Document doc = (Document) object;
        doc.loadFromState(state);
    }

    public void saveToState(PersistentState state, Object obj) throws PersistenceException {
        Document doc = (Document) obj;
        doc.saveToState(state);

    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean manages(Class theClass) {
        return this.getDocumentClass().isAssignableFrom(theClass);
    }

    @SuppressWarnings("rawtypes")
    public boolean manages(PersistentState state) {
        try {
            Class theClass;
            theClass = (Class) Class.forName(state.getTheClassName());
            return manages(theClass);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public List<DynStruct> getDefinitions() {
        DynStruct definition = this.getDefinition(this.getDocumentClass().getName());
        List<DynStruct> definitions = new ArrayList<DynStruct>();
        definitions.add(definition);
        return definitions;
    }

    public String getDomainName() {
        return PersistenceManager.DEFAULT_DOMAIN_NAME;
    }

    public String getDomainURL() {
        return PersistenceManager.DEFAULT_DOMAIN_URL;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List getManagedClasses() {
        List classes = new ArrayList();
        classes.add(this.getDocumentClass());
        return classes;
    }

    @SuppressWarnings("rawtypes")
    public Class getManagedClass(Object object) {
        return this.getDocumentClass();
    }

    @SuppressWarnings("rawtypes")
    public Class getManagedClass(PersistentState state) {
        return this.getDocumentClass();
    }

    @SuppressWarnings("rawtypes")
    public Class getManagedClass(String name) {
        return this.getDocumentClass();
    }

    @SuppressWarnings("rawtypes")
    public String getManagedClassName(Object object) {
        Class clazz = this.getManagedClass(object);
        if (clazz != null){
            return clazz.getName();
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    protected IDocumentWindow createDocumentWindow(Document document) {
        IDocumentWindow documentWindow = null;
        Class windowClass = getMainWindowClass();

        try {

            try {
                Constructor constructor;
                constructor =
                    windowClass.getConstructor(new Class[] { Document.class });
                documentWindow =
                    (IDocumentWindow) constructor
                        .newInstance(new Object[] { document });
            } catch (NoSuchMethodException e1) {
                documentWindow = (IDocumentWindow) windowClass.newInstance();
                documentWindow.setDocument(document);
            }
        } catch (Exception e) {
            logger.warn("Can't create the document window.", e);
            return null;
        }
        return documentWindow;
    }

    public void update(Observable observable, Object notification) {
        // Do nothing
    }
    

    public void addObserver(Observer o) {
        this.observableHelper.addObserver(o);
    }

    public void deleteObserver(Observer o) {
        this.observableHelper.deleteObserver(o);
    }

    public void deleteObservers() {
        this.observableHelper.deleteObservers();
    }
    
    protected Notification notifyObservers(String type, Document doc) {
        Notification notification = new BaseNotification(type, new Object[] {doc});
        this.observableHelper.notifyObservers(notification);
        return notification;
    }

    protected Notification notifyObservers(String type, IWindow doc) {
        Notification notification = new BaseNotification(type, new Object[] {doc});
        this.observableHelper.notifyObservers(notification);
        return notification;
    }
}
