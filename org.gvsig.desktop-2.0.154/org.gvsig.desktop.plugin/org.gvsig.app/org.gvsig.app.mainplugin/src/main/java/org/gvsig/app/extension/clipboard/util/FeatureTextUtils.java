/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.clipboard.util;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.tools.dispose.DisposableIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureTextUtils {

	private static final Logger logger = LoggerFactory.getLogger(FeatureTextUtils.class);

	/**
	 * The format of the clipboard content will be:
	 *
	 * <FEATURE><CLIPBOARD_SEPARATOR_1><FEATURE><CLIPBOARD_SEPARATOR_1><FEATURE>...
	 *
	 * <FEATURE> = <WKT><CLIPBOARD_SEPARATOR_2><FIELD><CLIPBOARD_SEPARATOR_2><FIELD>...
	 *
	 * <FIELD> = <NAME><CLIPBOARD_SEPARATOR_3><TYPE><CLIPBOARD_SEPARATOR_3><VALUE>
	 *
	 *  Fields are optional, so a WKT string alone will also be valid.
	 */
	public static final String CLIPBOARD_SEPARATOR_1 = "_GVSIGCOPYPASTESEPARATOR1_";
	public static final String CLIPBOARD_SEPARATOR_2 = "_GVSIGCOPYPASTESEPARATOR2_";
	public static final String CLIPBOARD_SEPARATOR_3 = "_GVSIGCOPYPASTESEPARATOR3_";

	private static GeometryManager gm = null;
	private static GeometryOperationContext wktctxt = new GeometryOperationContext();


	public static GeometryManager geoman() {
		if (gm == null) {
			gm = GeometryLocator.getGeometryManager();
		}
		return gm;
	}

	public static boolean clipboardSeemsToHaveValidFeatures() {

		if (!textInClipboard()) {
			return false;
		}

		String clipb = PluginServices.getFromClipboard();

		if (clipb == null || clipb.length() < 5) {
			return false;
		}

		String[] feats = clipb.split(CLIPBOARD_SEPARATOR_1);
		String[] feat_parts = feats[0].split(CLIPBOARD_SEPARATOR_2);

		if (feat_parts[0].length() > 10000) {
			logger.info("Clipboard: First part (wkt) of feature description is long ("
					+ feat_parts[0].length() +
					"). Not parsed. Returned 'true' meaning 'perhaps'.");
			/*
			 * 10000 characters is about 400 vertices or more.
			 * This seems to be a large geometry and we are not
			 * going to parse it here because this method must be fast.
			 * We will say "true" so the user will try to paste it, and perhaps it will
			 * not work (a dialog message will show, for example)
			 */
			return true;
		}

		Object geom_obj = null;

		try {
			geom_obj = geoman().createFrom(feat_parts[0]);
		} catch (Exception e) {
			logger.info("Unable to parse short WKT: " + e.getMessage());
			/*
			 * We discard the content of the clipboard after trying to parse only the
			 * first geometry (perhaps there were other geometries, but this method
			 * has to be very fast)
			 */
			return false;
		}

		return geom_obj instanceof Geometry;
	}

	public static boolean textInClipboard() {

		return Toolkit.getDefaultToolkit().getSystemClipboard(
				).isDataFlavorAvailable(DataFlavor.stringFlavor);
	}

	public static StringBuilder toString(
			FeatureSet fset,
			FeatureType fty,
			boolean include_atts) throws DataException {

		StringBuilder strb = new StringBuilder();
		Feature feat = null;
		DisposableIterator diter = fset.fastIterator();
		while (diter.hasNext()) {
			feat = (Feature) diter.next();
			if (strb.length() > 0) {
				strb.append(CLIPBOARD_SEPARATOR_1);
			}
			try {
				appendFeatureText(strb, feat, fty, include_atts);
			} catch (Exception e) {
				logger.info("One of the features in clipboard was not valid.", e);
			}
		}
		diter.dispose();
		return strb;
	}

	private static void appendFeatureText(
			StringBuilder strb,
			Feature feat,
			FeatureType fty,
			boolean include_atts) throws Exception {

		String aux = feat.getDefaultGeometry().convertToWKT();
		strb.append(aux);

		if (!include_atts) {
			return;
		}

		FeatureAttributeDescriptor[] atts = fty.getAttributeDescriptors();
		for (int i=0; i<atts.length; i++) {
			if (atts[i].getType() != DataTypes.GEOMETRY) {
				strb.append(CLIPBOARD_SEPARATOR_2);
				appendAttribute(strb, feat, atts[i].getName(), atts[i].getType());
			}
		}

	}

	private static void appendAttribute(
			StringBuilder strb,
			Feature feat,
			String name,
			int type) {

		strb.append(name);
		strb.append(CLIPBOARD_SEPARATOR_3);
		strb.append(type);
		strb.append(CLIPBOARD_SEPARATOR_3);
		strb.append(feat.get(name).toString());
	}

	public static List<EditableFeature> fromString(String str, FeatureStore fsto) {

		List<EditableFeature> resp = new ArrayList<EditableFeature>();

		if (!textInClipboard()) {
			return resp;
		}

		if (str == null || str.length() < 5) {
			return resp;
		}

		String[] feats = str.split(CLIPBOARD_SEPARATOR_1);
		EditableFeature item = null;

		for (int i=0; i<feats.length; i++) {
			item = stringToFeature(feats[i], fsto);
			if (item != null) {
				resp.add(item);
			}
		}
		return resp;
	}

	/**
	 * Returns null if feature was not created
	 *
	 * @param str
	 * @param fsto
	 * @return
	 */
	private static EditableFeature stringToFeature(String str, FeatureStore fsto) {

		String[] feat_parts = str.split(CLIPBOARD_SEPARATOR_2);
		Object geom_obj = null;
		try {
			geom_obj = geoman().createFrom(feat_parts[0]);
		} catch (Exception e) {
			logger.info("Unable to parse WKT: " + e.getMessage());
			return null;
		}

		if (!(geom_obj instanceof Geometry)) {
			// Parse issue, no feature
			return null;
		}

		EditableFeature resp = null;
		try {
			resp = fsto.createNewFeature(true);
		} catch (DataException e) {
			logger.error("Unable to create feature in clipboard operation.", e);
			return null;
		}

		// set geometry
		resp.setDefaultGeometry((Geometry) geom_obj);
		for (int i=1; i<feat_parts.length; i++) {
			setAttribute(resp, feat_parts[i]);
		}
		return resp;
	}

	private static void setAttribute(EditableFeature resp, String att_desc) {

		String[] att_parts = att_desc.split(CLIPBOARD_SEPARATOR_3);
		if (att_parts.length != 3) {
			// must be: name - type - value
			return;
		}

		FeatureType fty = null;
		fty = resp.getType();

		if (fty.get(att_parts[0]) == null) {
			// name not in feature type
			return;
		}

		try {
			resp.set(att_parts[0], att_parts[2]);
		} catch (Exception ex) {
			/*
			logger.info("Did not set value for field '" + att_parts[0]
					+ "': " + ex.getMessage());
			*/
		}
	}





}
