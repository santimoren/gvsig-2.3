/*
 * Copyright 2015 DiSiD Technologies S.L.L. All rights reserved.
 *
 * Project  : DiSiD org.gvsig.app.mainplugin
 * SVN Id   : $Id$
 */
package org.gvsig.app.extension.selectioncount;

import javax.swing.border.BevelBorder;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.andami.ui.mdiFrame.NewStatusBar;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlCreationListener;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.gui.beans.controls.IControl;
import org.gvsig.gui.beans.controls.label.Label;

/**
 * Controls the extension Selection count, which shows the selected features in the status bar
 * @author Daniel Martinez
 *
 */
public class SelectionCountExtension extends Extension {
    private IControl control;
    private SelectionCount selectionCount;

    @Override
    public void initialize() {
        // Do nothing

    }

    @Override
    public void execute(String actionCommand) {
     // Do nothing

    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        IWindow window = getActiveWindow();
        if (window instanceof IView) {
            IView view = (IView)window;
            ViewDocument viewDocument = view.getViewDocument();
            return viewDocument.getMapContext().hasActiveVectorLayers();
        }
        return false;
    }

    private IView getActiveView() {
        ApplicationManager application = ApplicationLocator.getManager();
        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        return view;
    }

    private IWindow getActiveWindow() {
        ApplicationManager application = ApplicationLocator.getManager();
        return application.getActiveWindow();
    }

    @Override
    public void postInitialize() {
        if (this.control==null){
            this.control=createLabel();
        }
        selectionCount = new SelectionCount(this.control);
    }

    private IControl createLabel(){
        MainFrame mainFrame = PluginsLocator.getMainFrame();
        Label label=new Label();
        label.setName("selectionCount");
        String tooltip=PluginServices.getText(this,"_Selected_features");
        label.setToolTipText(tooltip);
        label.setBorder(new BevelBorder(BevelBorder.LOWERED));
        mainFrame.addStatusBarControl(this.getClass(), label);
        return label;
    }
}
