/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.app.project.documents.Document;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * This class provides utility methods to manage the clipboard
 * for documents (it's file-based, it's not the system clipboard)
 * which will contain one or more documents for copy/cut/paste
 * operations. It is currently implemented using persistence
 * 
 * Each document type uses it's own "clipboard". Some actions
 * can affect all those clipboards at the same time
 * 
 * @author jldominguez
 * 
 */
public class CopyPasteDocsUtils {

    private static Logger logger = LoggerFactory
        .getLogger(CopyPasteDocsUtils.class);

    /**
     * The folder where docs are temporarily persisted
     */
    private static File clipboardFolder = null;
    private static String CLIPBOARD_FILE_NAME_PREFIX = "gvSIG_docs_clipboard_";
    private static String CLIPBOARD_FILE_NAME_EXTENSION = "tmp";
    private static int MAX_FILE_COUNT = 1000;

    private CopyPasteDocsUtils() {
    }
    
    private static PersistenceManager getPersMan() {
        return ToolsLocator.getPersistenceManager();
    }
    
    private static File getClipboardFolder() {
        if (clipboardFolder == null) {
            File appf = PluginsLocator.getManager().getApplicationHomeFolder();
            clipboardFolder = new File(appf, "clipboard-documents");
            clipboardFolder.mkdirs();
        }
        return clipboardFolder;
    }
    
    /**
     * Gets all if parameter is null
     * 
     */
    public static List<File> getClipboardFiles(String typeName) {
        
        List<File> resp = new ArrayList<File>();
        
        String filebase = CLIPBOARD_FILE_NAME_PREFIX;
        if (typeName != null) {
            filebase = filebase + typeName;
        }
        
        File folder = getClipboardFolder();
        
        if (!folder.exists() || !folder.isDirectory()) {
            return resp;
        }
        
        File[] ff = folder.listFiles();
        for (int i=0; i<ff.length; i++) {
            if (ff[i].getName().startsWith(filebase)) {
                resp.add(ff[i]);
            }
        }
        return resp;
    }
    
    
    private static File getFirstUnusedClipboardFile(String docTypeName)
    throws IOException {
        
        if (docTypeName == null || docTypeName.length() == 0) {
            throw new IOException("Doc type name cannot be empty");
        }
        
        String filename = null;
        for (int i=0; i<MAX_FILE_COUNT; i++) {
            filename = CLIPBOARD_FILE_NAME_PREFIX + docTypeName + "_" +
                Integer.toString(i)
                + "." + CLIPBOARD_FILE_NAME_EXTENSION;
            File resp = new File(getClipboardFolder(), filename);
            if (!resp.exists()) {
                /*
                 * Removed when JVM terminates
                 */
                resp.deleteOnExit();
                return resp;
            }
        }
        // This should never happen
        throw new IOException("Clipboard is full for documents: " + docTypeName);
    }
    
    /**
     * Clears clipboard of the provided doc type,
     * or clears all document clipboard if parameter is null.
     * 
     * @param docTypeName
     */
    public static void clearClipboard(String docTypeName) throws IOException {
        
        String filebase = CLIPBOARD_FILE_NAME_PREFIX;
        if (docTypeName != null) {
            filebase = filebase + docTypeName;
        }
        
        File folder = getClipboardFolder();
        
        if (!folder.exists() || !folder.isDirectory()) {
            /*
             * Nothing to do
             */
            return;
        }
        
        File[] ff = folder.listFiles();
        Exception exc = null;
        for (int i=0; i<ff.length; i++) {
            if (ff[i].getName().startsWith(filebase)) {
                try {
                    ff[i].delete();
                } catch (Exception e) {
                    // Keep first exception and try to go on
                    if (exc == null) {
                        exc = e;
                    }
                }
            }
        }
        // =================================
        if (exc != null) {
            throw new IOException(exc.getMessage());
        }
    }



    /**
     * Returns the content of the clipboard as a list of
     * documents or null if clipboard is empty or does not
     * contain documents of the specified type
     * 
     * 
     * 
     * @return
     * @throws PersistenceException
     */
    public static List<Document> getClipboardDocuments(String typeName)
        throws IOException, PersistenceException {

        List<Document> resp = new ArrayList<Document>();
        
        List<File> ff = getClipboardFiles(typeName);
        File infile = null;
        InputStream is = null;
        Object obj = null;
        Document doc = null;
        for (int i=0; i<ff.size(); i++) {
            infile = ff.get(i);
            is = new FileInputStream(infile);
            obj = getPersMan().getObject(is);
            is.close();
            if (obj instanceof Document) {
                doc = (Document) obj;
                // Checking, but it should not be necessary
                if (typeName == null || doc.getTypeName().compareTo(typeName) == 0) {
                    resp.add(doc);
                }
            }
        }
        return resp;
    }

    /**
     * Stores doc list to their clipboard
     * to clipboard
     * 
     * @param st
     * @throws PersistenceException
     */
    public static void saveToClipboard(List<Document> docs)
        throws IOException, PersistenceException {
        
        if (docs == null || docs.size() == 0) {
            return;
        }
        
        Map<String, List<Document>> separ =
            new HashMap<String, List<Document>>();
        Document doc = null;
        List<Document> list = null;
        for (int i=0; i<docs.size(); i++) {
            doc = docs.get(i);
            if (doc != null && doc.getTypeName() != null) {
                list = separ.get(doc.getTypeName());
                if (list == null) {
                    list = new ArrayList<Document>();
                    list.add(doc);
                    separ.put(doc.getTypeName(), list);
                } else {
                    list.add(doc);
                }
            }
        }
        
        Iterator<String> iter = separ.keySet().iterator();
        String k = null;
        while (iter.hasNext()) {
            k = iter.next();
            saveToClipboard(k, separ.get(k));
        }
    }
    
    /**
     * docs in list are of same type
     * @param typeName
     * @param docs
     */
    private static void saveToClipboard(String typeName, List<Document> docs)
    throws IOException, PersistenceException {
        
        clearClipboard(typeName);
        File f = null;
        OutputStream os = null;
        PersistentState state = null;
            
        for (int i=0; i<docs.size(); i++) {
            state = getPersMan().getState(docs.get(i), true);
            if (state.getContext().getErrors() != null) {
                throw state.getContext().getErrors();
            }
            f = getFirstUnusedClipboardFile(typeName);
            os = new FileOutputStream(f);
            getPersMan().saveState(state, os);
            os.close();
        }
    }
    
    

    public static String getLastMessage(Throwable ex) {
        
        if (ex == null) {
            return "-";
        }
        
        Throwable p = ex;
        while (p.getCause() != null && p.getCause() != p) {
            p = p.getCause();
        }
        return p.getMessage();
    }





}
