/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.toolListeners.InfoListener;
import org.gvsig.app.util.BaseOpenErrorHandler;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.OpenErrorHandler;
import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.fmap.dal.resource.exception.DisposeResorceManagerException;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.evaluator.sqljep.SQLJEPEvaluator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitializeApplicationExtension extends Extension {

    private static final Logger logger = LoggerFactory
            .getLogger(InitializeApplicationExtension.class);

    private OpenErrorHandler openErrorHandler = null;

    public void initialize() {

        DALLocator.getDataManager().registerDefaultEvaluator(SQLJEPEvaluator.class);
        DALLocator.registerFeatureTypeDefinitionsManager(DefaultFeatureTypeDefinitionsManager.class);

        InfoListener.initializeExtensionPoint();

        registerIcons();

    }

    public void postInitialize() {
        openErrorHandler = new BaseOpenErrorHandler();
        ApplicationLocator.getManager().getDataManager().setOpenErrorHandler(openErrorHandler);

        DALLocator.getResourceManager().startResourceCollector(
                3 * (60 * 1000), // minutes --> miliseconds
                null);
        // showAddOnManagerOnStartIfNeed();
    }

    private void showAddOnManagerOnStartIfNeed() {
        PluginsManager pluginManager = PluginsLocator.getManager();
        PluginServices plugin = pluginManager.getPlugin(this);
        DynObject pluginProperties = plugin.getPluginProperties();
        Map<String,Boolean> showAddOnmanagerOnStart = null;
        try {
            showAddOnmanagerOnStart = (Map<String,Boolean>) pluginProperties.getDynValue("showAddOnmanagerOnStart");
        } catch(Exception ex) {
            // Do nothing
        }
        if( showAddOnmanagerOnStart==null ) {
            showAddOnmanagerOnStart = new HashMap<String, Boolean>();
            pluginProperties.setDynValue("showAddOnmanagerOnStart",showAddOnmanagerOnStart);
        }
        Boolean showAddOnmanagerOnStartForCurrentVersion = (Boolean)showAddOnmanagerOnStart.get(pluginManager.getApplicationVersion().fullFormat());
                
        if( showAddOnmanagerOnStartForCurrentVersion==null || showAddOnmanagerOnStartForCurrentVersion.booleanValue() ) {
            pluginManager.addStartupTask(
                    "showAddOnmanagerOnStart", 
                    new ShowAddOnManagerOnStart(),
                    true, 
                    500
            );
        }
    }
    
    private static class ShowAddOnManagerOnStart implements Runnable {

        public void run() {
            ApplicationManager application = ApplicationLocator.getManager();
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            PluginsManager pluginManager = PluginsLocator.getManager();
            PluginServices plugin = pluginManager.getPlugin(this);
            DynObject pluginProperties = plugin.getPluginProperties();

            String msg = i18nManager.getTranslation("_Instalar_complementos_adicionales_no_disponibles_durante_el_proceso_de_instalacion");
            int resp = application.confirmDialog(
                    msg, 
                    i18nManager.getTranslation("_Finalizando_proceso_de_instalacion"), 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE
            );
            if( resp == JOptionPane.YES_OPTION ) {
                ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
                actionManager.execute(
                        "tools-addonsmanager",
                        new Object[] {"skipBundleSelection"}
                );
            }
            Map<String,Boolean> showAddOnmanagerOnStart = (Map<String,Boolean>) pluginProperties.getDynValue("showAddOnmanagerOnStart");
            showAddOnmanagerOnStart.put(pluginManager.getApplicationVersion().fullFormat(), Boolean.FALSE);
        }
    }
    
    private void registerIcons() {
        IconThemeHelper.registerIcon("action", "edit-clear", this);
        IconThemeHelper.registerIcon("action", "edit-copy", this);
        IconThemeHelper.registerIcon("action", "edit-cut", this);
        IconThemeHelper.registerIcon("action", "edit-delete", this);
        IconThemeHelper.registerIcon("action", "edit-find", this);
        IconThemeHelper.registerIcon("action", "edit-find-replace", this);
        IconThemeHelper.registerIcon("action", "edit-paste", this);
        IconThemeHelper.registerIcon("action", "edit-redo", this);
        IconThemeHelper.registerIcon("action", "edit-select-all", this);
        IconThemeHelper.registerIcon("action", "edit-undo", this);
        IconThemeHelper.registerIcon("action", "edit-undo-redo-actions", this);
        IconThemeHelper.registerIcon("action", "document-print", this);

        IconThemeHelper.registerIcon("toolbar-go", "go-next", this);
        IconThemeHelper.registerIcon("toolbar-go", "go-previous", this);
        IconThemeHelper.registerIcon("toolbar-go", "go-next-fast", this);
        IconThemeHelper.registerIcon("toolbar-go", "go-previous-fast", this);
        IconThemeHelper.registerIcon("toolbar-go", "go-first", this);
        IconThemeHelper.registerIcon("toolbar-go", "go-last", this);

    }

    public void execute(String actionCommand) {

    }

    public boolean isEnabled() {
        return false;
    }

    public boolean isVisible() {
        return false;
    }

    private void addToLogInfo() {
        String info[] = this.getStringInfo().split("\n");
        for (int i = 0; i < info.length; i++) {
            logger.info(info[i]);
        }
    }

    public String getStringInfo() {
        ApplicationManager application = ApplicationLocator.getManager();
        PluginsManager pluginmgr = PluginsLocator.getManager();
        InstallerManager installmgr = InstallerLocator.getInstallerManager();

        StringWriter writer = new StringWriter();

        Properties props = System.getProperties();

        // OS information
        String osName = props.getProperty("os.name");
        writer.write("OS\n");
        writer.write("    name   : " + osName + "\n");
        writer.write("    arch   : " + props.get("os.arch") + "\n");
        writer.write("    version: " + props.get("os.version") + "\n");
        if (osName.startsWith("Linux")) {
            try {
                String[] command = {"lsb_release", "-a"};
                Process p = Runtime.getRuntime().exec(command);
                InputStream is = p.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.write("    " + line + "\n");
                }
            } catch (Exception ex) {
                writer.write("Can't get detailled os information (lsb_release -a).");
            }
        }

        // JRE information
        writer.write("JRE\n");
        writer.write("    vendor : " + props.get("java.vendor") + "\n");
        writer.write("    version: " + props.get("java.version") + "\n");
        writer.write("    home   : " + props.get("java.home") + "\n");

        writer.write("gvSIG\n");
        writer.write("    version                 : " + application.getVersion().getLongFormat() + "\n");
        writer.write("    locale language         : " + application.getLocaleLanguage() + "\n");
        writer.write("    application forlder     : " + pluginmgr.getApplicationFolder() + "\n");
        writer.write("    install forlder         : " + pluginmgr.getInstallFolder() + "\n");
        writer.write("    application home forlder: " + pluginmgr.getApplicationHomeFolder() + "\n");
        writer.write("    plugins forlder         : " + StringUtils.join(pluginmgr.getPluginsFolders()) + "\n");

        try {
            PackageInfo[] pkgs = installmgr.getInstalledPackages();
            writer.write("Installed packages\n");
            for (int i = 0; i < pkgs.length; i++) {
                writer.write("    ");
                writer.write(pkgs[i].toStringCompact());
                writer.write("\n");
            }
        } catch (Throwable e) {
            writer.write("Can't get installed package information.");
        }
        return writer.toString();
    }

    public void terminate() {
        ResourceManager resMan = DALLocator.getResourceManager();
        resMan.stopResourceCollector();
        try {
            resMan.dispose();
        } catch (DisposeResorceManagerException e) {
            logger.warn(e.getMessageStack());
        }
        super.terminate();
    }
}
