
package org.gvsig.app.extension.logviewer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.LoggerFactory;


public class LogViewer extends LogViewerView {
    
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LogViewer.class);
    
    private final File logfile;
    
    
    public LogViewer() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        this.logfile = FileUtils.getFile(
                pluginsManager.getApplicationHomeFolder(), 
                pluginsManager.getApplicationName() + ".log"
        );
        this.initComponents();
    }
    
    private void initComponents() {
        
        this.translateComponents();
        
        this.btnClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                doClose();
            }
        });
        this.btnUpdate.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                doUpdate();
            }
        });
        
        String s = this.logfile.getAbsolutePath();
        this.txtFilename.setText(s);
        this.txtFilename.setSelectionStart(s.length());
        this.txtFilename.setSelectionEnd(s.length());
        
        doUpdate();
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension preferredSize = super.getPreferredSize();
        preferredSize.width = Math.max(preferredSize.width, 400);
        preferredSize.height = Math.max(preferredSize.height, 300);
        return preferredSize;
    }
    
    
    private void translateComponents() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.btnClose.setText(i18nManager.getTranslation("_Close"));
        this.btnUpdate.setText(i18nManager.getTranslation("_Update"));
    }
    
    public void doClose() {
        this.setVisible(false);
    }
    
    public void doUpdate() {
        try {
            String s = FileUtils.readFileToString(logfile);
            this.txtLogContents.setText(s);
            this.txtLogContents.setSelectionStart(s.length());
            this.txtLogContents.setSelectionEnd(s.length());
        } catch (IOException ex) {
            logger.warn("Can't load log file '"+this.logfile.getAbsolutePath()+"'.",ex);
            this.txtLogContents.setText("Can't load log file\n\n"+ex.toString());
        }
    }
}
