/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JComponent;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;



/**
 * Clase base de los elementos del proyecto (mapas, tablas y vistas)
 *
 * @author 2006-2009 Jose Manuel Vivo
 * @author 2005-         Vicente Caballero
 * @author 2009-         Joaquin del Cerro
 *
 */
public abstract class AbstractDocument implements Serializable, Persistent, Document {

	/**
	 *
	 */
	private static final long serialVersionUID = 3335040973071555406L;

	public static final String PERSISTENCE_DEFINITION_NAME = "AbstractDocument";

	protected PropertyChangeSupport change;

	private Project project;

	private String name;
	private String creationDate;
	private String owner;
	private String comment;
	private boolean locked = false;
	private boolean isModified=false;

	private DocumentManager factory;

	private WindowLayout windowLayout = null;

	private List<ProjectDocumentListener> projectDocListener = new ArrayList<ProjectDocumentListener>();

	/**
	 * Creates a new ProjectElement object.
	 */
	public AbstractDocument() {
		creationDate = DateFormat.getDateInstance().format(new Date());
		change = new PropertyChangeSupport(this);
		this.factory = null;
		this.project = null;
		this.name = PluginServices.getText(this, "untitled");
	}

	public AbstractDocument(DocumentManager factory) {
		this();
		this.factory = factory;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return name;
	}

	/**
	 * return the name of the document
	 *
	 * @return name as String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the name of the type of documents
	 *
	 * @return name as String
	 */
	public String getTypeName() {
		return this.factory.getTypeName();
	}

	/**
	 * Sets the nane of the document
	 *
	 * @param name as string
	 */
	public void setName(String name) {
		if (isLocked()) {
			throw new RuntimeException("this document is locked");
		}
		if( name.length()==0) {
			name = null;
		}
		if( project != null ) {
			Document doc = project.getDocument(name, this.getTypeName());
			if( doc != null && !this.equals(doc)  ) {
				throw new RuntimeException("document name already exists in project");
			}
		}
		String previousName = this.name;
		this.name = name;
		change.firePropertyChange("name", previousName, name);
	}

	/**
	 * Get the creation date of the document.
	 *
	 * @return creation date as String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Get the creator name of the document.
	 *
	 * @return creator name as String
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Set the creation date of the document.
	 *
	 * @param string
	 */
	public void setCreationDate(String string) {
		creationDate = string;
		change.firePropertyChange("creationDate", creationDate, creationDate);
	}

	/**
	 * Sets the creator name of the document
	 *
	 * @param string
	 */
	public void setOwner(String string) {
		String oldOwner = owner;
		owner = string;
		change.firePropertyChange("owner", oldOwner, owner);
	}

	/**
	 * Gets the comments asociateds to the document
	 *
	 * @return comments as String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Gets the comments asociateds to the document
	 *
	 * @param string
	 */
	public void setComment(String string) {
		String oldComment = comment;
		comment = string;
		change.firePropertyChange("comment", oldComment, comment);
	}

	/**
	 * Add a listener to monitor the changes in the properties of the document
	 *
	 * @param listener
	 */
	public synchronized void addPropertyChangeListener(
		PropertyChangeListener listener) {
		change.addPropertyChangeListener(listener);
	}

	/**
	 * Register a  ProjectDocumentListener.
	 * @param  listener  ProjectDocumentListener
	 */
	public void addListener(ProjectDocumentListener listener) {
		if(this.projectDocListener.indexOf(listener) == -1)
			this.projectDocListener.add(listener);
	}


	public WindowLayout getWindowLayout() {
		return this.windowLayout;
	}

	public void setWindowLayout(WindowLayout layout) {
		this.windowLayout = layout;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.setComment( state.getString("comment") );
		this.setCreationDate( state.getString("creationDate") );
		this.setName( state.getString("name") );
		this.setOwner( state.getString("owner") );
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("comment", comment);
		state.set("creationDate", creationDate);
		state.set("name", name);
		state.set("owner", owner);
		state.set("locked", locked);
	}

	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		DynStruct definition = manager.getDefinition(PERSISTENCE_DEFINITION_NAME);
		if ( definition == null ){
		    definition = manager.addDefinition(
		        AbstractDocument.class,
		        PERSISTENCE_DEFINITION_NAME,
		        "Document persistence definition",
		        null,
		        null
		    );
		    definition.addDynFieldString("comment").setMandatory(false);
		    definition.addDynFieldString("creationDate").setMandatory(true);
		    definition.addDynFieldString("name").setMandatory(true);
		    definition.addDynFieldString("owner").setMandatory(false);
		    definition.addDynFieldBoolean("locked").setMandatory(true);
		}
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}


	/**
	 * Locks this project element protecting it from deleting from the project.
	 */
	public void lock() {
		boolean oldLocked = locked;
		locked = Boolean.TRUE;
		change.firePropertyChange("locked", oldLocked, locked);
	}

	/**
	 * Unlocks this element. So, from now on, it can be removed from the project.
	 */
	public void unlock() {
		boolean oldLocked = locked;
		locked = Boolean.FALSE;
		change.firePropertyChange("locked", oldLocked, locked);
	}

	/**
	 * Tells whether if this project's element is locked/protected or not. A protected
	 * element cannot be removed from the current project.
	 *
	 * @see <b>lock()</b> and <b>unlock()</b> methods.
	 *
	 * @return true if it is locked, false otherwise
	 */
	public boolean isLocked() {
		return locked;
	}

	public DocumentManager getFactory() {
		return factory;
	}

	public boolean isModified() {
		return isModified;
	}

	public void setModified(boolean modified) {
		isModified=modified;
	}

	/**
	 * Throw this event when a new window is created
	 * @param  window
	 *         IWindow created
	 */
	public void raiseEventCreateWindow(IWindow window) {
		for (int i = 0; i < projectDocListener.size(); i++)
			projectDocListener.get(i).windowCreated(window);
	}

	/**
	 * @deprecated use {link {@link #raiseEventCreateWindow(IWindow)}
	 */
	protected void callCreateWindow(IWindow window) {
		raiseEventCreateWindow(window);
	}

	public void afterAdd() {
		// do nothing by default
	}

	public void afterRemove() {
		// do nothing by default
	}

	public String exportDocumentAsText() {
		throw new UnsupportedOperationException();
	}

	public void setStateFromText(String text) {
		throw new UnsupportedOperationException();
	}

	public IWindow getPropertiesWindow() {
		return this.getFactory().getPropertiesWindow(this);
	}

	public IWindow getMainWindow() {
		return this.getFactory().getMainWindow(this);
	}

    public JComponent getMainComponent() {
		return this.getFactory().getMainComponent(this);
    }

    public boolean isTemporary() {
        return false;
    }

    public boolean isAvailable() {
        return true;
    }


}
