/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gvsig.app.project.documents.view.ViewDocument;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;
import org.gvsig.propertypage.PropertiesPageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @deprecated use PropertiesPageManager, see GeneralLayerPropertiesPage for an exemple
 */
public final class ThemeManagerWindow {

    private static final Logger logger = LoggerFactory.getLogger(ThemeManagerWindow.class);

    private static final long serialVersionUID = 4650656815369149211L;
    /*
     private static int selectedTabIndex = 0;
     private static ArrayList<Class<? extends AbstractThemeManagerPage>> pages =
     new ArrayList<Class<? extends AbstractThemeManagerPage>>();
     private FLayer layer;
     private JTabbedPane tabs = new JTabbedPane();  //  @jve:decl-index=0:
     private JPanel panelButtons;
     */
    private static Map<Class<? extends AbstractThemeManagerPage>, List<Class<? extends FLayer>>> s
            = new HashMap<Class<? extends AbstractThemeManagerPage>, List<Class<? extends FLayer>>>();

    private static class ThemeManagerPropertiesPageFactory implements PropertiesPageFactory {

        private Class<? extends AbstractThemeManagerPage> pageClass;
        private AbstractThemeManagerPage page = null;
        private FLayer layer = null;

        public ThemeManagerPropertiesPageFactory(Class<? extends AbstractThemeManagerPage> pageClass) {
            this.pageClass = pageClass;
        }

        private AbstractThemeManagerPage getPage(FLayer layer) throws InstantiationException, IllegalAccessException {
            if (this.layer == null || this.layer != layer) {
                AbstractThemeManagerPage p = pageClass.newInstance();
                this.page = p;
                this.layer = layer;
            }
            this.page.setModel(this.layer);
            return this.page;
        }

        public String getGroupID() {
            return ViewDocument.LAYER_PROPERTIES_PAGE_GROUP;
        }

        public boolean isVisible(Object obj) {
            try {
                AbstractThemeManagerPage page = this.getPage((FLayer) obj);
                return page.isTabEnabledForLayer((FLayer) obj);
            } catch (Throwable th) {
                logger.warn("can't determine if the page must be visible to the object '" + obj + "'", th);
            }
            return false;
        }

        public PropertiesPage create(Object obj) {
            try {
                return this.getPage((FLayer) obj);
            } catch (Exception ex) {
                logger.warn("Can't create the page '" + this.pageClass + "' asociated to '" + obj + "'.", ex);
                return null;
            }
        }

    }

    public ThemeManagerWindow() {
    }

    /**
     * @deprecated use PropertiesPageManager, see BasicSymbologyExtension
     */
    public static void addPage(Class<? extends AbstractThemeManagerPage> pageClass) {
        PropertiesPageManager manager = MapControlLocator.getPropertiesPageManager();
        manager.registerFactory(new ThemeManagerPropertiesPageFactory(pageClass));
    }

    public static void setTabEnabledForLayer(Class<? extends AbstractThemeManagerPage> abstractThemeManagerPageClass, Class<? extends FLayer> fLayerClazz, boolean enabled) {
        ArrayList<Class<? extends FLayer>> enabledLayers;
        if (enabled == true) {
            if (!s.containsKey(abstractThemeManagerPageClass)) {
                enabledLayers = new ArrayList<Class<? extends FLayer>>();
                enabledLayers.add(fLayerClazz);
                s.put(abstractThemeManagerPageClass, enabledLayers);
            } else {
                enabledLayers = (ArrayList<Class<? extends FLayer>>) s.get(abstractThemeManagerPageClass);
                enabledLayers.add(fLayerClazz);
            }
        } else {
            if (!s.containsKey(abstractThemeManagerPageClass)) {
                return;
            }
            enabledLayers = (ArrayList<Class<? extends FLayer>>) s.get(abstractThemeManagerPageClass);
            enabledLayers.remove(fLayerClazz);
        }
    }

    public static boolean isTabEnabledForLayer(AbstractThemeManagerPage page, FLayer layer) {
        try {
            return s.get(page.getClass()).contains(layer.getClass());
        } catch (Exception ex) {
            return false;
        }
    }

}
