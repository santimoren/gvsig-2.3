
package org.gvsig.app.extension;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.util.List;
import javax.swing.TransferHandler;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.app.project.documents.view.toc.DnDJTree;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.app.project.documents.view.toc.gui.TOC;
import org.gvsig.tools.observer.Notification;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AddLayerDropingFile extends Extension implements Runnable, Observer, DropTargetListener  {

    private static final Logger logger = LoggerFactory.getLogger(AddLayerDropingFile.class);
            
    private ActionInfo addLayerAction = null;
    
    public void initialize() {
    }

    public void execute(String actionCommand) {
    }

    public void postInitialize() {
        PluginsManager pluginManager = PluginsLocator.getManager();
        pluginManager.addStartupTask("RegisterDropFilesOnTheView", this, true, 1000);    
    }

    
    public boolean isEnabled() {
        return false;
    }

    public boolean isVisible() {
        return false;
    }

    public void run() {
        ActionInfoManager actionManager = PluginsLocator.getActionInfoManager();
        addLayerAction = actionManager.getAction("view-layer-add");
        if( addLayerAction==null ) {
            logger.warn("can't locate addLayer action, don't enable drop of files in the view.");
            return;
        }
        ApplicationManager application = ApplicationLocator.getManager();
        ProjectManager pm = application.getProjectManager();
        ViewManager viewManager = (ViewManager) pm.getDocumentManager(ViewManager.TYPENAME);
        viewManager.addObserver(this);
        
    }

    public void update(Observable observable, Object notificationArg) {
        Notification notification = (Notification)notificationArg;
        if( notification.isOfType(DocumentManager.NOTIFY_AFTER_GETMAINWINDOW) ) {
            enableDragInView((IView) notification.getValue());
        }
    }
    
    private void enableDragInView(IView view) {
        DropTarget dt = new DropTarget(view.getMapControl(),this);
    }

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent dtde) {
        Transferable tr = dtde.getTransferable();
        if (!tr.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
		dtde.rejectDrop();
                return;
	}
        dtde.acceptDrop(dtde.getDropAction());
        List<File> fileNames;
        try {
            fileNames = (List<File>)tr.getTransferData(DataFlavor.javaFileListFlavor);
        } catch (Exception e) { 
            try {
                dtde.rejectDrop();
            } catch(Exception ex) {
                
            }
            logger.warn("Can't retrieve droped files.",e);
            return; 
        }
        dtde.dropComplete(true);
        addLayerAction.execute(fileNames);
    }
}
