/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import javax.swing.JOptionPane;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extensi�n de zoom a lo seleccionado teniendo como ventana activa una vista.
 *
 */
public class ZoomToSelectExtension extends Extension {

    private static final Logger logger = LoggerFactory
            .getLogger(ZoomToSelectExtension.class);

    public void initialize() {
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-to-selection", this);
    }

    public void execute(String s) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        Envelope selectedExtent = null;
        try {
            selectedExtent = mapa.getSelectionBounds();
            mapa.getViewPort().setEnvelope(selectedExtent);
        } catch (BaseException e) {
            String msg = "Can't zoom to the seleccion.";
            logger.warn(msg, e);
            application.message(msg, JOptionPane.WARNING_MESSAGE);
        }
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        return document.getMapContext().getLayers().getLayersCount() > 0;
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        FLayer[] selected = document.getMapContext().getLayers().getActives();
        if (selected.length == 1 && selected[0] instanceof FLyrVect && selected[0].isAvailable()) {
            FLyrVect layer = (FLyrVect) selected[0];
            try {
                if (!layer.getFeatureStore().getFeatureSelection().isEmpty()) {
                    return true;
                }
            } catch (Exception e) {
                String msg = "Can't check if selection if empty in layer '" + layer.getName() + "'.";
                logger.warn(msg, e);
                application.message(msg, JOptionPane.WARNING_MESSAGE);
            }
        }
        return false;
    }

}
