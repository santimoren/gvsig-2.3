/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app;

import java.awt.Component;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.event.HyperlinkListener;
import javax.swing.tree.TreeModel;

import org.cresques.cts.IProjection;
import org.gvsig.about.AboutManager;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.andami.ui.mdiFrame.ThreadSafeDialogs;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.app.extension.Version;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.prepareAction.PrepareContextView;
import org.gvsig.app.prepareAction.PrepareDataStore;
import org.gvsig.app.prepareAction.PrepareDataStoreParameters;
import org.gvsig.app.prepareAction.PrepareLayer;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.gui.ColorTablesFactory;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dispose.DisposableManager;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.icontheme.IconThemeManager;


public interface ApplicationManager extends ThreadSafeDialogs {

    /**
     * Return the version of the application.
     * 
     * @return the version
     */
	public Version getVersion() ;

    /**
     * Return the active window, IWindow, in the application.
     * 
     * @return the active window
     */
	public IWindow getActiveWindow();

	/**
	 * Return the project that is loaded and in use in the application.
	 * 
	 * @return the project 
	 */
    public Project getCurrentProject();

    /**
     * Returns the active document of the application or null 
     * if there is no active document.
     * 
     * @return the active document
     */
    public Document getActiveDocument();

    /**
     * Returns the active document of the application that is of the 
     * requested type or null if there is no active document or the
     * active document is not of the requested type.
     * 
     * @param documentTypeName, type name of the document requested.
     * @return the active document
     */
    public Document getActiveDocument(String documentTypeName);

    /**
     * Returns the active document of the application that is of the 
     * requested type or null if there is no active document or the
     * active document is not of the requested type.
     * 
     * @param documentClass, class of the document requested.
     * @return the active document
     */
    public Document getActiveDocument(Class<? extends Document> documentClass);
    
    /**
     * Returns the JComponent associated to the active document for the
     * specified type of document.
     * If not is active the specified type of document return null.
     * 
     * @param documentClass
     * @return the component associated to the active document of type documentClass
     */
    public JComponent getActiveComponent(Class<? extends Document> documentClass);
    
    /**
     * Returns the window associated with the document passed as 
     * parameter or null if there is no window associated with 
     * the document.
     * 
     * @param document to search the window
     * @return window associated to the document
     */
    public IDocumentWindow getDocumentWindow(Document document);
    
    
    /**
     * Return the root of the nodes of preferences in the application.
     * 
     * @return the root of preferences.
     */
    PreferencesNode getPreferences();
	
    /**
     * Search and return the node of preferences required as nodeName.
     *  
     * @param nodeName, name of the node to return
     * @return the preferences node requested
     */
	PreferencesNode getPreferences(String nodeName);
	
	/**
	 * Return the arguments passed to the application in the command line
	 *  
	 * @return arguments as a string array
	 */
	public String[] getArguments();
	
	/**
	 * Return the value of the argument passed in the command line by
	 * the name indicated as the parameter. This is passed as:
	 * <code>
	 * application --name=value
	 * </code>
	 * @param name of the requested argument
	 * @return value of the requested argument
	 */
	public String getArgument(String name);
	
	/**
	 * Get the value of the system clipboard as a string.
	 * 
	 * @return the clipboard value
	 */
	public String getFromClipboard();
	
	/**
	 * Put the string value passed as parameter in the clipboard of
	 * the system.
	 * 
	 * @param data to put in the clipboard.
	 */
	public void putInClipboard(String data) ;

	/**
	 * Obtain the projection associated by the code of projection
	 * Passed as parameter. If passed code of projection it not
	 * a valid projection code null is returned.
	 * 
	 * @param code of the projection requested
	 * @return the requested projection or null
	 */
    public IProjection getCRS(String code);
    
	/**
     * Utility method to obtain the DataManager used in the application at this moment.
     * 
     * @return the DataManager
	 */
	public DataManager getDataManager();
    
    /**
     * Utility method to obtain the ProjectManager used in the application at this moment.
     * 
     * @return the ProjectManager
     */
    public ProjectManager getProjectManager();

    /**
     * Utility method to obtain the UIManager used in the application at this moment.
     * 
     * @return the UIManager
     */
    public MDIManager getUIManager();
    
    /**
     * Utility method to obtain the GeometryManager used in the application at this moment.
     * 
     * @return the GeometryManager
     */
    public GeometryManager getGeometryManager();
    
    /**
     * Utility method to obtain the PersistenceManager used in the application at this moment.
     * 
     * @return the PersistenceManager
     */
    public PersistenceManager getPersistenceManager();
    
    /**
     * Utility method to obtain the DisposableManager used in the application at this moment.
     * 
     * @return the DisposableManager
     */
    public DisposableManager getDisposableManager() ;

    /**
     * Utility method to obtain the DynObjectManager used in the application at this moment.
     * 
     * @return the DynObjectManager
     */
    public DynObjectManager getDynObjectManager() ;

    /**
     * Utility method to obtain the ExtensionPointManager used in the application at this moment.
     * 
     * @return the ExtensionPointManager
     */
    public ExtensionPointManager getExtensionPointManager();

    /**
     * Utility method to obtain the MapContextManager used in the application at this moment.
     * 
     * @return the MapContextManager
     */
    public MapContextManager getMapContextManager();
    
    /**
     * Utility method to obtain the AboutManager used in the application at this moment.
     * 
     * @return the AboutManager
     */
    public AboutManager getAbout();

    public DataTypesManager getDataTypesManager();
    
    /**
     * Utility method to obtain the IconThemeManager used in the application at this moment.
     * 
     * @return the IconThemeManager
     */
    IconThemeManager getIconThemeManager();
    
	void registerAddTableWizard(String name, String description,
			Class<? extends WizardPanel> wpClass);

	List<WizardPanel> getWizardPanels() throws Exception;



	void registerPrepareOpenDataStore(PrepareDataStore action);
	void registerPrepareOpenDataStoreParameters(	PrepareDataStoreParameters action);
	void registerPrepareOpenLayer(PrepareLayer action);

	DataStore pepareOpenDataSource(DataStore store,
			PrepareContext context)
			throws Exception;

	DataStoreParameters prepareOpenDataStoreParameters(
			DataStoreParameters storeParameters, PrepareContext context)
			throws Exception;
	
	List<DataStoreParameters> prepareOpenDataStoreParameters(
			List<DataStoreParameters> storeParameters, PrepareContext context)
			throws Exception;

	FLayer prepareOpenLayer(FLayer layer,
			PrepareContextView context)
			throws Exception;
	
	
	public ColorTablesFactory getColorTablesFactory();
	
	public void registerColorTablesFactory(ColorTablesFactory factory);

	public String getLocaleLanguage();

    /**
     * sets message in status bar
     * 
     * This method is thread safe.
     * 
     * @param message
     * @param message_type One of: JOptionPane.ERROR_MESSAGE,
     * JOptionPane.WARNING_MESSAGE, JOptionPane.INFORMATION_MESSAGE 
     */
    public void message(String message, int message_type);
    
    public String translate(String message, String... args) ;

    public Component getRootComponent();

    public void refreshMenusAndToolBars();

    public MainFrame getMainFrame();

    public void addMenu(ActionInfo action, String text);
    public void addTool(ActionInfo action, String toolBarName);
    public void addSelectableTool(ActionInfo action, String toolBarName);

    public void showTextDialog(final WindowManager.MODE mode, final String title, final String htmlText);
    public void showTextDialog(final WindowManager.MODE mode, final String title, final String htmlText, HyperlinkListener hyperlinkListener);

    public TreeModel createProjectLayersTreeModel(); 
}
