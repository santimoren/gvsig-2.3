/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.tools.persistence.Persistent;

public interface Document extends Persistent {

        public static final String ACCESS_DOCUMENT_AUTHORIZATION = "project-document-access";

	/**
	 * Obtiene el nombre del elemento
	 *
	 * @return
	 */
	public String getName();

	/**
	 * Establece el nombre del elemento
	 *
	 * @param string
	 */
	public void setName(String string);

	/**
	 * Obtiene la fecha de creaci�n del elemento
	 *
	 * @return
	 */
	public String getCreationDate();

	/**
	 * Obtiene el propietario del elemento
	 *
	 * @return
	 */
	public String getOwner();

	public String getTypeName();

	/**
	 * Establece la fecha de creaci�n del elemento.
	 *
	 * @param string
	 */
	public void setCreationDate(String string);

	/**
	 * Establece el propietario del elemento
	 *
	 * @param string
	 */
	public void setOwner(String string);

	/**
	 * Obtiene los comentarios del proyecto
	 *
	 * @return
	 */
	public String getComment();

	/**
	 * Establece los comentarios del proyecto
	 *
	 * @param string
	 */
	public void setComment(String string);

	public Project getProject();

	public void setProject(Project project);

	/**
	 * Locks this project element protecting it from deleting from the project.
	 */
	public abstract void lock();

	/**
	 * Unlocks this element. So, from now on, it can be removed from the project.
	 */
	public abstract void unlock();

	/**
	 * Tells whether if this project's element is locked/protected or not. A protected
	 * element cannot be removed from the current project.
	 *
	 * @see <b>lock()</b> and <b>unlock()</b> methods.
	 *
	 * @return true if it is locked, false otherwise
	 */
	public abstract boolean isLocked();

	public DocumentManager getFactory();

	public boolean isModified();

	public void setModified(boolean modified);

	/**
	 * Register a  ProjectDocumentListener.
	 * @param  listener  ProjectDocumentListener
	 */
	public void addListener(ProjectDocumentListener listener);

	/**
	 * A�ade un listener para los cambios en las bounded properties
	 *
	 * @param listener
	 */
	public void addPropertyChangeListener(
			PropertyChangeListener listener);

	public void afterRemove();

	public void afterAdd();

	public WindowLayout getWindowLayout();

	public void setWindowLayout(WindowLayout layout) ;

	public IWindow getPropertiesWindow();

        /**
         * Return the main window associated to this document.
         * Is a tutility method, is equivalent to:
         * <code>this.getFactory().getMainWindow(doc)</code>
         *
         * @return the IWindow associated to this document
         */
	public IWindow getMainWindow();

        /**
         * Return the main JComponent associated to this document.
         * Is a tutility method, is equivalent to:
         * <code>this.getFactory().getMainComponent(doc)</code>
         *
         * @return the JComponent associated to this document
         */
        public JComponent getMainComponent();

    public boolean isTemporary();

    public boolean isAvailable();

}