/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;

/**
 * Interfaz invocada por el FPanelExtentSelector cuando el usuario ha seleccionado al
 * menos un elemento y pulsa el bot�n aceptar
 *
 * @author $author$
 * @version $Revision: 29598 $
 */
public interface ListSelectorListener {
    /**
     * M�todo invocado cuando el usuario pulsa aceptar y hay algo seleccionado
     *
     * @param indices Array con los �ndices de los elementos seleccionados
     */
    public void indexesSelected(int[] indices);

    /**
     * DOCUMENT ME!
     *
     * @param indices DOCUMENT ME!
     */
    public void indexesRemoved(int[] indices);

    /**
     * DOCUMENT ME!
     *
     * @param name DOCUMENT ME!
     */
    public void newElement(String name);
}
