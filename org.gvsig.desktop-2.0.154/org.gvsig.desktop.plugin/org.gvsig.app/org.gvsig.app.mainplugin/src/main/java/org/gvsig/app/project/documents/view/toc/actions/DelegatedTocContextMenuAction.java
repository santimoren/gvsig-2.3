/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;


/**
 * Clase abstract para facilitar sobreescribir una accion sobre el TOC.
 *
 * @author Joaquin del Cerro
 */
public abstract class DelegatedTocContextMenuAction extends AbstractTocContextMenuAction {

	private AbstractTocContextMenuAction delegated = null; 
	
	public DelegatedTocContextMenuAction(AbstractTocContextMenuAction delegated) {
		this.delegated = delegated;
	}
	
	public void execute(ITocItem item, FLayer[] selectedItems) {
		delegated.execute(item,selectedItems);
	}

	public String getText() {
		return delegated.getText();
	}
	public String getGroup() {
		return delegated.getGroup();
	}

	public int getGroupOrder() {
		return delegated.getGroupOrder();
	}

	public int getOrder() {
		return delegated.getOrder();
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return delegated.isEnabled(item, selectedItems);
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		return delegated.isVisible(item, selectedItems);
		
	}
	
	public MapContext getMapContext() {
		return delegated.getMapContext();
	}
	
	public void setMapContext(MapContext mapContext) {
		delegated.setMapContext(mapContext);
	}

}
