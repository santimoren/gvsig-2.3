/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.persistence.PersistentState;


public class CopyLayersTocMenuEntry extends AbstractTocContextMenuAction {
    
    private static Logger logger =
        LoggerFactory.getLogger(CopyLayersTocMenuEntry.class);
    
	public String getGroup() {
		return "copyPasteLayer";
	}

	public int getGroupOrder() {
		return 60;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "copiar");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		if ( selectedItems.length >= 1 && isTocItemBranch(item)){
			for (int i=0;i< selectedItems.length;i++) {
				if (selectedItems[i].isEditing() ||
				    (!selectedItems[i].isAvailable())){
					return false;
				}
			}
			return true;
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selItems) {

	       PersistentState lyrs_state = null;
	        
	        try {
	            /*
	             * This prevents copying the same layer
	             * twice (because a parent is also copied)
	             */
	            FLayer[] tocopy = getLayersWithNoActiveAncestors(selItems);
	            
	            lyrs_state = CopyPasteLayersUtils.getAsFLayersPersistentState(
	                tocopy, this.getMapContext());
	            /*
	             * Saving layers data to clipboard.
	             * Files and URLs are not relativized
	             */
	            CopyPasteLayersUtils.saveToClipboard(lyrs_state);
	            
	            /*
	             * This code does not look right: why modified?
	             * 
	            Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
	            project.setModified(true);
	            */

	        } catch (Exception e) {
	            
	            JOptionPane.showMessageDialog(
                    (Component)PluginServices.getMainFrame(),
                    Messages.getText("No_ha_sido_posible_realizar_la_operacion")
                    + "\n\n" + e.getMessage(),
                    Messages.getText("copiar"),
                    JOptionPane.ERROR_MESSAGE
                    );
	            logger.info("While copying layers.", e);
	            return;
	        }
	}

    private FLayer[] getLayersWithNoActiveAncestors(FLayer[] selItems) {
        
        List<FLayer> resp = new ArrayList<FLayer>();
        FLayer lyr = null;
        for (int i=0; i<selItems.length; i++) {
            lyr = selItems[i];
            if (!hasActiveAncestor(lyr)) {
                resp.add(lyr);
            }
        }
        return resp.toArray(new FLayer[0]);
    }

    private boolean hasActiveAncestor(FLayer lyr) {
        
        FLayers lyrs = lyr.getParentLayer();
        while (lyrs != null) {
            if (lyrs.isActive()) {
                return true;
            } else {
                if (lyrs == lyrs.getParentLayer()) {
                    /*
                     * Prevent endless loop due to bad setting
                     */
                    return false;
                } else {
                    lyrs = lyrs.getParentLayer();
                }
            }
        }
        return false;
    }


}
