/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.app.imp;

import org.gvsig.app.ApplicationLibrary;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.gui.ProjectGeneralPropertiesPageFactory;
import org.gvsig.app.project.documents.view.gui.GeneralViewPropertiesPageFactory;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanelManager;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.impl.DefaultFilesystemExplorerPropertiesPanelManager;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.impl.FilesystemExplorerPropertiesDynObjectFactory;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.installer.lib.api.InstallerLibrary;
import org.gvsig.propertypage.PropertiesPageManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Registers the default implementation for the AppGvSigManager.
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class ApplicationLibraryImpl extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsImplementationOf(ApplicationLibrary.class);
        require(InstallerLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
		ApplicationLocator.registerDefaultApplicationManager(DefaultApplicationManager.class);
                ApplicationLocator.registerFilesystemExplorerPropertiesPanelManager(DefaultFilesystemExplorerPropertiesPanelManager.class);
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
            PropertiesPageManager propertiesPageManager = MapControlLocator.getPropertiesPageManager();
            propertiesPageManager.registerFactory(new ProjectGeneralPropertiesPageFactory());
            propertiesPageManager.registerFactory(new GeneralViewPropertiesPageFactory());
        
            FilesystemExplorerPropertiesPanelManager filesystemExplorerPropertiesPanelManager = ApplicationLocator.getFilesystemExplorerPropertiesPanelManager();
            filesystemExplorerPropertiesPanelManager.registerFactory(new FilesystemExplorerPropertiesDynObjectFactory());
        }

}