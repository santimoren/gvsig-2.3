package org.gvsig.app.project.documents.view.toc.actions;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.fmap.mapcontrol.swing.dynobject.DynObjectEditor;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LayerErrorsPanel extends LayerErrorsView {

    private static final Logger logger = LoggerFactory.getLogger(LayerErrorsPanel.class);
    private static final long serialVersionUID = -5972995402218954892L;
    private FLayer layer;

    public LayerErrorsPanel(FLayer layer) {
        this.initComponents();
        this.setLayer(layer);
    }

    public LayerErrorsPanel(String name, Exception ex) {
        this.initComponents();
        this.setName(name);
        this.setError(ex);
    }

    private void initComponents() {
        this.translate();
        this.txtMessage.setContentType("text/html");
        this.btnClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                doClose();
            }
        });
        this.btnViewParameters.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                doViewParameters();
            }
        });
        this.setPreferredSize(new Dimension(550, 400));
    }

    public void setName(String name){
        this.txtLayerName.setText(name);
    }

    public void setError(Exception e){
        List<Exception> exs = new ArrayList<Exception>();
        exs.add(e);
        this.txtMessage.setText(this.getMessageAsHTML(exs));
        this.btnViewParameters.setEnabled(false);
    }

    public void setLayer(FLayer layer) {
        this.layer = layer;
        setName(layer.getName());
        this.txtMessage.setText(this.getMessageAsHTML(layer.getErrors()));
        this.btnViewParameters.setEnabled(this.layer instanceof SingleLayer);
    }

    private String getMessageAsHTML(List<Exception> exs) {
        StringBuilder html = new StringBuilder();
        //html.append("<html>\n");
        for (Exception ex : exs) {
            String lastmsg = "";
            html.append("<ul>\n");
            for( Throwable cause : ExceptionUtils.getThrowableList(ex) ) {
                String message = cause.getMessage();
                if (message != null) {
                    if (!message.equalsIgnoreCase(lastmsg)) {
                        lastmsg = message;
                        html.append("<li>");
                        html.append(message.replace("\n", "<br>\n"));
                        html.append("</li>\n");
                    }
                }
            }
            html.append("</ul><br>\n");
        }
        //html.append("</html>\n");
        return html.toString();
    }

    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        this.lblLayer.setText(i18nManager.getTranslation("_Layer") + ":");
        this.lblProblem.setText(i18nManager.getTranslation("_Problems") + ":");
        this.btnClose.setText(i18nManager.getTranslation("_Close"));
        this.btnViewParameters.setText(i18nManager.getTranslation("_View_properties_of_data_source"));
    }

    private void doClose() {
        this.setVisible(false);
    }

    private void doViewParameters() {
        try {
            DynObjectEditor editor = new DynObjectEditor( ((SingleLayer)layer).getDataStore().getParameters() );
            editor.editObject(true);
        } catch (Throwable th) {
            logger.warn("Can't show standard properties dialog for this parameters.",th);
        }
    }

}
