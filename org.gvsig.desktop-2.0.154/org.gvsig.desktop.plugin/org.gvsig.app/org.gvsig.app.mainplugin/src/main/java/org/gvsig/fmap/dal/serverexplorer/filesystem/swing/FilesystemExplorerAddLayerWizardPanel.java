/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIDefaults;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.messages.Messages;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.prepareAction.PrepareContextView;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;

/**
 * @author jmvivo
 *
 */
public class FilesystemExplorerAddLayerWizardPanel extends
        FilesystemExplorerWizardPanel {

    private static final long serialVersionUID = -5054057255129168139L;

    private static final Logger LOG = LoggerFactory
            .getLogger(FilesystemExplorerAddLayerWizardPanel.class);

    private PrepareContext prepareContext;

    @Override
    public void initWizard() {

        super.initWizard();
        this.getFileList().setCellRenderer(
                new TwoColumnFileListCellRenderer(this.getMapCtrl()));
    }

    @Override
    public void execute() {
        if (this.getMapCtrl() == null) {
            throw new IllegalArgumentException("MapControl need");
        }
        String layerName;

        for (DataStoreParameters params : this.getParameters()) {
            IProjection proj = this.getMapCtrl().getProjection();

            // Buscamos por el parametro de la proyeccion
            // que sean obligatorios y estén a null
            // y le ponemos la proyeccion de la vista
            DynField[] fields = params.getDynClass().getDynFields();
            for (DynField field : fields) {
                if (field.getType() == DataTypes.CRS && field.isMandatory()) {
                    if (params.getDynValue(field.getName()) == null) {
                        params.setDynValue(field.getName(), proj);
                    }
                }
            }

            layerName
                    = ((FilesystemStoreParameters) params).getFile().getName();

            this.doAddLayer(this.getMapCtrl(), layerName, params);
        }
    }

    protected void showPropertiesDialog(final DynObject parameters) {
        // For store parameters with a CRS field, add the current view
        // CRS as default value when null.
        try {
            IProjection projection = (IProjection) parameters.getDynValue("CRS");
            if (projection == null) {
                projection = getDefaultProjection();
                parameters.setDynValue("CRS", projection);
            }
        } catch (DynFieldNotFoundException e1) {
            LOG.info("Loading a store whose parameters don't have a CRS field");
        }

        FilesystemExplorerPropertiesPanelManager manager = ApplicationLocator.getFilesystemExplorerPropertiesPanelManager();
        FilesystemExplorerPropertiesPanel panel = manager.createPanel(parameters);   
        panel.setExcludeGeometryOptions(false);
        manager.showPropertiesDialog(parameters, panel);
        
        refreshFileList();
    }


    /**
     * @return
     */
    private IProjection getDefaultProjection() {
        return getMapCtrl().getViewPort().getProjection();
    }

    @Override
    protected PrepareContext getPrepareDataStoreContext() {
        if (this.prepareContext == null) {
            this.prepareContext = new PrepareContextView() {

                @Override
                public Window getOwnerWindow() {
                    return null;
                }

                @Override
                public MapControl getMapControl() {
                    return FilesystemExplorerAddLayerWizardPanel.this
                            .getMapCtrl();
                }

                @Override
                public IProjection getViewProjection() {
                    return getMapControl().getProjection();
                }

            };
        }
        return this.prepareContext;
    }

    // =================
    private class TwoColumnFileListCellRenderer extends JPanel implements ListCellRenderer {

        private JLabel fileName = null;
        private JLabel crsName = null;

        private Color selBC = null;
        private Color selFC = null;
        private Color unselBC = null;
        private Color unselFC = null;

        private MapControl mapCtrl = null;

        public TwoColumnFileListCellRenderer(MapControl mct) {
            // 1 col for name, one for crs
            setLayout(new GridLayout(0, 2));
            fileName = new JLabel();
            crsName = new JLabel();

            Font fnt = crsName.getFont();
            fnt = fnt.deriveFont((float) (fnt.getSize() - 1));
            crsName.setFont(fnt);

            add(fileName);
            add(crsName);
            mapCtrl = mct;

            UIDefaults defaults = javax.swing.UIManager.getDefaults();
            selBC = defaults.getColor("List.selectionBackground");
            selFC = defaults.getColor("List.selectionForeground");
            unselBC = this.getBackground();
            unselFC = this.getForeground();
        }

        @Override
        public Component getListCellRendererComponent(
                JList list, Object _value,
                int index, boolean isSelected, boolean cellHasFocus) {

            if (isSelected) {
                setBackground(selBC);
                fileName.setForeground(selFC);
                crsName.setForeground(selFC);
            } else {
                setBackground(unselBC);
                fileName.setForeground(unselFC);
                crsName.setForeground(unselFC);
            }

            // ===========================
            fileName.setText(_value == null
                    ? "?" : fixString(_value.toString()));

            ListModel lm = list.getModel();
            String unkown_tag = Messages.get("_Unknown_CRS_so_assumed");
            String view_crs = getViewCRS();

            if (lm instanceof FilesystemStoreListModel) {

                FilesystemStoreListModel fsslm = (FilesystemStoreListModel) lm;
                DynObject dyno = fsslm.getDynObjectAt(index);

                try {
                    Object crs_obj = dyno.getDynValue(DataStore.METADATA_CRS);
                    if (crs_obj == null || crs_obj.toString().compareToIgnoreCase("null") == 0) {
                        crsName.setText(unkown_tag + " " + view_crs);
                    } else {
                        if (crs_obj instanceof IProjection) {
                            IProjection ipr = (IProjection) crs_obj;
                            if (ipr.getAbrev().compareToIgnoreCase(view_crs) == 0) {
                                // CRS set but same as view's
                                crsName.setText(view_crs);
                            } else {
                                // CRS set and not same as view's
                                crsName.setText(ipr.getAbrev()
                                        + " ("
                                        + Messages.get("_reprojected_on_the_fly")
                                        + ")");
                            }
                        } else {
                            // CRS parameter class not expected
                            crsName.setText(unkown_tag + " " + view_crs);
                        }

                    }
                } catch (Exception ex) {
                    // CRS parameter not found
                    crsName.setText(unkown_tag + " " + view_crs);
                }

            } else {
                // list model not of expected class
                crsName.setText(unkown_tag + " " + view_crs);
            }

            return this;
        }

        /**
         * @param string
         * @return
         */
        private String fixString(String str) {

            int len = str.length();
            String resp = str;
            if (len < 32) {
                // return str;
            } else {
                int cut = len - 14;
                resp = resp.substring(0, 14)
                        + "..." + resp.substring(cut);
            }
            return resp + "    ";
        }

        private String getViewCRS() {
            if (this.mapCtrl == null) {
                return "CRS?";
            } else {
                return mapCtrl.getProjection().getAbrev();
            }
        }

    }

}
