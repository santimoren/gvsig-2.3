/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.Component;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.addlayer.AddLayerDialog;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerAddLayerWizardPanel;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerWizardPanel;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extensi�n que abre un di�logo para seleccionar la capa o capas que se
 * quieren anadir a la vista.
 *
 */
public class AddLayer extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(AddLayer.class);

    private static ArrayList<Class<? extends WizardPanel>> wizardStack = null;

    static {
        AddLayer.wizardStack = new ArrayList<>();
        // Anadimos el panel al wizard de cargar capa. 
        AddLayer.addWizard(FilesystemExplorerAddLayerWizardPanel.class);
    }

    public static void addWizard(Class<? extends WizardPanel> wpClass) {
        AddLayer.wizardStack.add(wpClass);
    }

    private static WizardPanel getInstance(int i, MapControl mapControl)
            throws IllegalArgumentException, SecurityException,
            InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        Class<? extends WizardPanel> wpClass = AddLayer.wizardStack.get(i);
        Object[] params = {};
        WizardPanel wp = wpClass.getConstructor()
                .newInstance(params);
        wp.setMapCtrl(mapControl);
        wp.initWizard();

        return wp;
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        return application.getActiveComponent(ViewDocument.class) != null;
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
    }

    public static void checkProjection(FLayer lyr, ViewPort viewPort) {
        if (lyr instanceof FLayers) {
            FLayers layers = (FLayers) lyr;
            for (int i = 0; i < layers.getLayersCount(); i++) {
                checkProjection(layers.getLayer(i), viewPort);
            }
        }
        if (lyr instanceof FLyrVect) {
            FLyrVect lyrVect = (FLyrVect) lyr;
            IProjection proj = lyr.getProjection();
            // Comprobar que la projecci�n es la misma que la vista
            if (proj == null) {
                // SUPONEMOS que la capa est� en la proyecci�n que
                // estamos pidiendo (que ya es mucho suponer, ya).
                lyrVect.setProjection(viewPort.getProjection());
                return;
            }
            int option;
            if (proj != viewPort.getProjection()) {
                option = JOptionPane.showConfirmDialog((Component) PluginServices.getMainFrame(), PluginServices
                        .getText(AddLayer.class, "reproyectar_aviso") + "\n" + PluginServices.getText(AddLayer.class, "Capa") + ": " + lyrVect.getName(), PluginServices
                        .getText(AddLayer.class, "reproyectar_pregunta"),
                        JOptionPane.YES_NO_OPTION);

                if (option != JOptionPane.OK_OPTION) {
                    return;
                }

                ICoordTrans ct = proj.getCT(viewPort.getProjection());
                lyrVect.setCoordTrans(ct);
                System.err.println("coordTrans = " + proj.getAbrev() + " "
                        + viewPort.getProjection().getAbrev());
            }
        }

    }

    @Override
    public void execute(String actionCommand) {
        this.execute(actionCommand, null);
    }

    @Override
    public void execute(String command, Object[] args) {
        List<File> files = null;
        if (args != null && args.length >= 1 && args[0] instanceof List) {
            files = (List<File>) args[0];
        }
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();

        MapControl mapControl = view.getMapControl();
        this.doAddLayers(mapControl, mapControl.getMapContext(), files);
        mapControl.getMapContext().callLegendChanged();
        document.setModified(true);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     * Creates FOpenDialog, and adds file tab, and additional registered tabs
     *
     * @return FOpenDialog
     */
    private AddLayerDialog createFOpenDialog(MapControl mapControl, MapContext mapContext, List<File> files) {
        ApplicationManager application = ApplicationLocator.getManager();
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        AddLayerDialog fopen = new AddLayerDialog();
        for (Class<? extends WizardPanel> wpClass : wizardStack) {
            WizardPanel wp;
            try {
                Object[] params = {};
                wp = wpClass.getConstructor()
                        .newInstance(params);
                application.message(
                        i18nManager.getTranslation("Adding tab...") + wp.getTabName(), 
                        JOptionPane.INFORMATION_MESSAGE
                );
                wp.setMapCtrl(mapControl);
                wp.setMapContext(mapContext);
                wp.initWizard();
                if (wp instanceof FilesystemExplorerWizardPanel && files != null && !files.isEmpty()) {
                    FilesystemExplorerWizardPanel fswp = (FilesystemExplorerWizardPanel) wp;
                    fswp.addFiles(files);
                }
                fopen.addWizardTab(wp.getTabName(), wp);
            } catch (Exception e) {
                logger.warn("Can't create layer open dialog.", e);
            }
        }
        application.message(null,JOptionPane.INFORMATION_MESSAGE);
        fopen.updateOkButtonState();
        return fopen;
    }

    /**
     * Adds to mapcontrol all layers selected by user in the specified
     * WizardPanel.
     *
     * @param mapControl MapControl on which we want to load user selected
     * layers.
     * @param wizardPanel WizardPanel where user selected the layers to load
     * @return
     */
//    private boolean loadGenericWizardPanelLayers(MapContext mapContext, WizardPanel wp, boolean mapControlAvailable) {
//        wp.setMapContext(mapContext);
//        wp.executeWizard();
//        return true;
//    }
    /**
     * Opens the AddLayer dialog, and adds the selected layers to the provided
     * MapContext.
     *
     * This method is useful when we want to add a layer but we don't have an
     * associated View. If a View or a MapControl is available, you will usually
     * prefer the {@link #addLayers(MapControl)} method.
     *
     * @param mapContext The MapContext to add the layers to
     * @return <code>true</code> if any layer has been added.
     */
    public boolean addLayers(MapContext mapContext) {
        return doAddLayers(null, mapContext, null);
    }

    /**
     * Opens the AddLayer dialog, and adds the selected layers to the provided
     * mapControl.
     *
     * @param mapControl The MapControl to add the layers to
     *
     * @return <code>true</code> if any layer has been added.
     */
    public boolean addLayers(MapControl mapControl) {
        return doAddLayers(mapControl, mapControl.getMapContext(), null);
    }

    private boolean doAddLayers(MapControl mapControl, MapContext mapContext, List<File> files) {
        AddLayerDialog fopen = null;
        try {
            fopen = createFOpenDialog(mapControl, mapContext, files);
            PluginServices.getMDIManager().addWindow(fopen);

            if (fopen.isAccepted()) {
                if (fopen.getSelectedTab() instanceof WizardPanel) {
                    WizardPanel wp = (WizardPanel) fopen.getSelectedTab();
                    wp.execute();
                    return true;
                } else {
                    JOptionPane.showMessageDialog((Component) PluginServices
                            .getMainFrame(), PluginServices.getText(this, "ninguna_capa_seleccionada"));
                }
            }
            return false;
        } finally {
            DisposeUtils.disposeQuietly(fopen);
        }
    }

    @Override
    public void initialize() {
        IconThemeHelper.registerIcon("action", "view-layer-add", this);
    }
}
