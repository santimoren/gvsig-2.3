/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners;

import java.awt.GridBagConstraints;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.info.gui.FInfoDialog;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.operations.InfoByPoint;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.extensionpoint.ExtensionSingleton;

/**
 * Listener that looks for information at the point selected by one
 * click of any mouse's button, in the active layers of the associated
 * <code>MapControl</code>, and displays that data on a {@link FInfoDialog
 * FInfoDialog} dialog.
 * 
 * @author -2009 Vicente Caballero Navarro
 * @author 2009- gvSIG Team
 */
public class InfoListener implements PointListener {

    /**
     * Object used to log messages for this listener.
     */
//    
    private static final Logger logger = LoggerFactory.getLogger(InfoListener.class);

    private static final String EP_INFOTOOL_NAME = "org.gvsig.app.infotool"; 
    private static final String EP_INFOTOOL_RENDERER = "renderer"; 

    public static void initializeExtensionPoint() {
        ExtensionPointManager manager = ToolsLocator.getExtensionPointManager();
        ExtensionPoint point = manager.add(EP_INFOTOOL_NAME, "Register of data relative to infotool");
        point.append(
            EP_INFOTOOL_RENDERER, 
            "Renderer used to show in a panel the info by point tool.", 
            new DefaultInfoByPointRenderer()
        );
    }
    
    /**
     * The image to display when the cursor is active.
     */
    private final Image img = PluginServices.getIconTheme()
        .get("cursor-info-by-point").getImage();

    /**
     * Reference to the <code>MapControl</code> object that uses.
     */
    private MapControl mapCtrl;

    /**
     * Radius as tolerance around the selected point, the area will be used to
     * look for information.
     */
    private static int TOL = 7;

    /**
     * <p>
     * Creates a new <code>InfoListener</code> object.
     * </p>
     * 
     * @param mc
     *            the <code>MapControl</code> where will be applied the changes
     */
    public InfoListener(MapControl mc) {
        this.mapCtrl = mc;
    }

    public void point(PointEvent event) throws BehaviorException {
        try {
            ApplicationManager application = ApplicationLocator.getManager();
            int numLayersInfoable = 0;
            Point point = event.getMapPoint();
            double tolerance = mapCtrl.getViewPort().toMapDistance(TOL);

            FLayer[] sel = mapCtrl.getMapContext().getLayers().getActives();
            Map<String, DynObjectSet> layer2info =
                new HashMap<String, DynObjectSet>(sel.length);

            for (int i = 0; i < sel.length; i++) {
                FLayer laCapa = sel[i];
                if (laCapa instanceof InfoByPoint) {
                    InfoByPoint layer = (InfoByPoint) laCapa;
                    DynObjectSet info = layer.getInfo(point, tolerance);
                    layer2info.put(laCapa.getName(), info);
                    numLayersInfoable++;
                }
            }
            
            if (numLayersInfoable == 0) {
                return;
            }

            ExtensionPointManager extensionPoints = ToolsLocator.getExtensionPointManager();
            InfoByPointRenderer renderer = (InfoByPointRenderer)extensionPoints.get(EP_INFOTOOL_NAME).create(EP_INFOTOOL_RENDERER);
            if( renderer instanceof DefaultInfoByPointRenderer ) {
            	((DefaultInfoByPointRenderer)renderer).setMapContext(this.mapCtrl.getMapContext());
            }
            IWindow window = renderer.getPanel(layer2info);

            if (window == null) {
                logger.info("Error. Unable to create info panel.");
                return;
            }

            PluginServices.getMDIManager().addWindow(window, GridBagConstraints.LAST_LINE_END);

        } catch (Exception e) {
            NotificationManager.addError("Info by Point", e);
            e.printStackTrace();
        }
    }

    /**
     * @param window
     * @return whether the window is currently one the app windows
     */
    private boolean isCurrentlyAdded(IWindow iw) {
        
        IWindow[] iws = PluginServices.getMDIManager().getAllWindows();
        for (int i=0; i<iws.length; i++) {
            if (iws[i] == iw) {
                return true;
            }
        }
        return false;
    }

    public Image getImageCursor() {
        return img;
    }

    public boolean cancelDrawing() {
        return false;
    }

    public void pointDoubleClick(PointEvent event) throws BehaviorException {
        // Nothing to do
    }
    
    public interface InfoByPointRenderer {
        
       public SingletonWindow getPanel(Map<String, DynObjectSet> layersInfo);
       
    }
    
    public static class DefaultInfoByPointRenderer implements InfoByPointRenderer, ExtensionSingleton {
        
        private FInfoDialog dlgInfo = null;
		private MapContext mapContext;
        
        public void setMapContext(MapContext mapContext) {
        	this.mapContext = mapContext;
        }
        
        public SingletonWindow getPanel(Map<String, DynObjectSet> layersInfo) {
            // TODO: set the writable parameter to true to activate
            // edition of the info by point information.
            LayersDynObjectSetComponent infoComponent =
                MapControlLocator.getMapControlManager()
                    .createLayersDynObjectSetComponent(layersInfo, false);
            infoComponent.setMapContext(mapContext);
            if (dlgInfo == null) {
                dlgInfo = new FInfoDialog(infoComponent);
            } else {
                dlgInfo.setInfo(infoComponent);
            }
            return dlgInfo;
        }
       
    }
    
    
}
