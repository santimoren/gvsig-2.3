/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.selectionByTheme;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.utils.swing.JComboBox;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;

public class SelectionByTheme extends JPanel implements IWindow {

    /**
     * 
     */
    private static final long serialVersionUID = -5094042907941800674L;

    public static final int EQUALS = 0;

    public static final int DISJOINT = 1;

    public static final int INTERSECTS = 2;

    public static final int TOUCHES = 3;

    public static final int CROSSES = 4;

    public static final int WITHIN = 5;

    public static final int CONTAINS = 6;

    public static final int OVERLAPS = 7;

    private I18nManager i18nManager = ToolsLocator.getI18nManager();

    public final String[] textosAcciones = new String[] {
            i18nManager.getTranslation("Sean_iguales_a"), // "sean iguales a",
            i18nManager.getTranslation("Sean_disjuntos_a"), // "sean disjuntos a",
            i18nManager.getTranslation("Intersecten_con"), // "intersecten con",
            i18nManager.getTranslation("Toquen"), // "toquen",
            i18nManager.getTranslation("Crucen_con"), // "crucen con",
            i18nManager.getTranslation("Contengan"), // "contengan",
            i18nManager.getTranslation("Esten_contenidos_en"), // "est�n contenidos en",
            i18nManager.getTranslation("Se_superponen_a"), // "se superponen a"
    };

    private SelectionByThemeModel dataSource = null;

    private ArrayList listeners = new ArrayList();

    private JPanel jPanel = null;

    private JPanel jPanel1 = null;

    private JLabel jLabel = null;

    private JComboBox cmbAction = null;

    private JLabel jLabel1 = null;

    private JComboBox cmbCapas = null;

    private JButton btnNew = null;

    private JButton btnAdd = null;

    private JButton btnFrom = null;

    private JButton btnCancel = null;

    /**
     * This is the default constructor
     */
    public SelectionByTheme() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     *
     * @return void
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(3, 10, 3, 10);
        
        // Labels and Comboboxes
        c.weightx = 1;

        c.gridx = 0;
        c.gridy = 0;
        this.add(getJLabel(), c);

        c.gridy = 1;
        this.add(getCmbAction(), c);

        c.gridy = 2;
        this.add(getJLabel1(), c);

        c.gridy = 3;
        this.add(getCmbCapas(), c);
        

        // Buttons
        c.weightx = 0;
        c.gridx = 1;
        
        c.gridy = 0;
        this.add(getBtnNew(), c);

        c.gridy = 1;
        this.add(getBtnAdd(), c);

        c.gridy = 2;
        this.add(getBtnFrom(), c);

        c.gridy = 3;
        this.add(getBtnCancel(), c);
        

        this.setSize(540, 95);
    }

    /**
     * This method initializes jLabel
     *
     * @return JLabel
     */
    private JLabel getJLabel() {
        if (jLabel == null) {
            jLabel = new JLabel();
            jLabel.setText(i18nManager
                    .getTranslation("Seleccionar_de_las_capas_activas_los_elementos_que"));
            jLabel.setVerticalAlignment(SwingConstants.BOTTOM);
            jLabel.setToolTipText(i18nManager.getTranslation("select_from_the_active_layers"));
        }
        return jLabel;
    }

    /**
     * This method initializes cmbAction
     *
     * @return JComboBox
     */
    private JComboBox getCmbAction() {
        if (cmbAction == null) {
            cmbAction = new JComboBox();
            cmbAction.setPreferredSize(new java.awt.Dimension(200, 20));
            DefaultComboBoxModel model = new DefaultComboBoxModel(
                    textosAcciones);
            cmbAction.setModel(model);
        }
        return cmbAction;
    }

    /**
     * This method initializes jLabel1
     *
     * @return JLabel
     */
    private JLabel getJLabel1() {
        if (jLabel1 == null) {
            jLabel1 = new JLabel();
            jLabel1.setText(i18nManager.getTranslation("Elementos_seleccionados_de_la_capa").concat(":"));
            jLabel1.setVerticalAlignment(SwingConstants.BOTTOM);
            jLabel1.setToolTipText(i18nManager.getTranslation("layer_to_which_belong_the_elements_selected_previously"));
        }
        return jLabel1;
    }

    /**
     * This method initializes cmbCapas
     *
     * @return JComboBox
     */
    private JComboBox getCmbCapas() {
        if (cmbCapas == null) {
            cmbCapas = new JComboBox();
            cmbCapas.setPreferredSize(new java.awt.Dimension(200, 20));
        }
        return cmbCapas;
    }

    /**
     * This method initializes btnNew
     *
     * @return JButton
     */
    private JButton getBtnNew() {
        if (btnNew == null) {
            btnNew = new JButton();
            btnNew.setText(i18nManager.getTranslation("Nuevo_conjunto"));
            btnNew.setMargin(new java.awt.Insets(3, 3, 3, 3));
            btnNew.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    callNewListeners(cmbCapas.getSelectedIndex(),
                            cmbAction.getSelectedIndex());
                }
            });
        }
        return btnNew;
    }

    /**
     * This method initializes btnAdd
     *
     * @return JButton
     */
    private JButton getBtnAdd() {
        if (btnAdd == null) {
            btnAdd = new JButton();
            btnAdd.setText(i18nManager.getTranslation("Anadir_al_conjunto"));
            btnAdd.setMargin(new java.awt.Insets(3, 3, 3, 3));
            btnAdd.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    callAddToListeners(cmbCapas.getSelectedIndex(),
                            cmbAction.getSelectedIndex());
                }
            });
        }
        return btnAdd;
    }

    /**
     * This method initializes btnFrom
     *
     * @return JButton
     */
    private JButton getBtnFrom() {
        if (btnFrom == null) {
            btnFrom = new JButton();
            btnFrom.setText(i18nManager
                    .getTranslation("Seleccionar_del_conjunto"));
            btnFrom.setMargin(new java.awt.Insets(3, 3, 3, 3));
            btnFrom.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    callFromListeners(cmbCapas.getSelectedIndex(),
                            cmbAction.getSelectedIndex());
                }
            });
        }
        return btnFrom;
    }

    /**
     * This method initializes btnCancel
     *
     * @return JButton
     */
    private JButton getBtnCancel() {
        if (btnCancel == null) {
            btnCancel = new JButton();
            btnCancel.setText(i18nManager.getTranslation("Cancel"));
            btnCancel.setMargin(new java.awt.Insets(3, 3, 3, 3));
            btnCancel.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    PluginServices.getMDIManager().closeWindow(
                            SelectionByTheme.this);
                }
            });
        }
        return btnCancel;
    }

    /**
     * @return
     */
    public SelectionByThemeModel getModel() {
        return dataSource;
    }

    /**
     * @param source
     */
    public void setModel(SelectionByThemeModel source) {
        dataSource = source;
        String[] nameLayers = new String[dataSource.getLayers()
                .getLayersCount()];
        for (int i = 0; i < nameLayers.length; i++) {
            nameLayers[i] = dataSource.getLayers().getLayer(i).getName();
        }
        DefaultComboBoxModel model = new DefaultComboBoxModel(nameLayers);
        cmbCapas.setModel(model);
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#getWindowInfo()
     */
    public WindowInfo getWindowInfo() {
        WindowInfo vi = new WindowInfo(WindowInfo.MODALDIALOG);
        vi.setWidth(this.getWidth());
        vi.setHeight(this.getHeight());
        vi.setTitle(i18nManager.getTranslation("Seleccion_por_capa"));
        return vi;
    }

    private void callNewListeners(int selection, int actionCode) {
        for (int i = 0; i < listeners.size(); i++) {
            SelectionByThemeListener l = (SelectionByThemeListener) listeners
                    .get(i);
            l.newSet(dataSource.getLayers().getActives(), dataSource
                    .getLayers().getLayer(selection), actionCode);
        }

    }

    private void callAddToListeners(int selection, int actionCode) {
        for (int i = 0; i < listeners.size(); i++) {
            SelectionByThemeListener l = (SelectionByThemeListener) listeners
                    .get(i);
            l.addToSet(dataSource.getLayers().getActives(), dataSource
                    .getLayers().getLayer(selection), actionCode);
        }

    }

    private void callFromListeners(int selection, int actionCode) {
        for (int i = 0; i < listeners.size(); i++) {
            SelectionByThemeListener l = (SelectionByThemeListener) listeners
                    .get(i);
            l.fromSet(dataSource.getLayers().getActives(), dataSource
                    .getLayers().getLayer(selection), actionCode);
        }

    }

    /**
     * DOCUMENT ME!
     *
     * @param arg0
     * @return
     */
    public boolean addSelectionListener(SelectionByThemeListener arg0) {
        return listeners.add(arg0);
    }

    /**
     * DOCUMENT ME!
     *
     * @param arg0
     * @return
     */
    public boolean removeSelectionListener(SelectionByThemeListener arg0) {
        return listeners.remove(arg0);
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#windowActivated()
     */
    public void viewActivated() {}

    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }

} // @jve:visual-info decl-index=0 visual-constraint="10,10"
