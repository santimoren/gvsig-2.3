/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.extension.develtools.ActionsInfoDevelTool;
import org.gvsig.app.extension.develtools.IconThemeDevelTool;
import org.gvsig.app.extension.develtools.MenusDevelTool;
import org.gvsig.app.extension.develtools.PersistenceDevelTool;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowDevelInfoExtension extends Extension {

	private static Logger logger = LoggerFactory
			.getLogger(ShowDevelInfoExtension.class);

	public void initialize() {
		// Do nothing
	}

	public void postInitialize() {
		super.postInitialize();
//		ActionInfoManager actionMgr = PluginsLocator.getActionInfoManager();
//		actionMgr.redirect("tools-devel-disposables-show-pendings", "layer-info-by-point");
		
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		
		try {
			manager.setAutoValidation(PersistenceManager.MANDATORY_IF_DECLARED);
		} catch (PersistenceException e) {
			logger.warn(
					"Error modificando el modo de autovalidacion de persistencia.",
					e);
		}

	}

	public void execute(String actionCommand) {
		if ("tools-devel-show-persistencefactories".equalsIgnoreCase(actionCommand)) {
			new PersistenceDevelTool().showPersistenceFactories();
		} else if ("tools-devel-show-icontheme".equalsIgnoreCase(actionCommand)) {
			new IconThemeDevelTool().showDefaultIconTheme();
		} else if ("tools-devel-show-actions".equalsIgnoreCase(actionCommand)) {
			new ActionsInfoDevelTool().showActions();
		} else if ("tools-devel-show-menus".equalsIgnoreCase(actionCommand)) {
			new MenusDevelTool().showAllMenus();
		} else if ("tools-devel-show-menus-by-plugin".equalsIgnoreCase(actionCommand)) {
			new MenusDevelTool().showAllMenusByPlugin();
		} else if ("tools-devel-show-plugin-report".equalsIgnoreCase(actionCommand)) {
			new ActionsInfoDevelTool().showReportOfPlugin();
		}
	}


	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		return true;
	}
}
