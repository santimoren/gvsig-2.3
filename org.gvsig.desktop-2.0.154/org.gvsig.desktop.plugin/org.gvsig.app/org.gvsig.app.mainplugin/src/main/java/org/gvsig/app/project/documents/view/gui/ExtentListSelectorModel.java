/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.gvsig.app.project.Project;

public class ExtentListSelectorModel implements ListModel,
    PropertyChangeListener {

    private Project project;
    private List<ListDataListener> listeners =
        new ArrayList<ListDataListener>();

    public ExtentListSelectorModel(Project p) {
        project = p;
    }

    public int getSize() {
        return project.getExtents().length;
    }

    public Object getElementAt(int arg0) {
        return project.getExtents()[arg0];
    }

    public void addListDataListener(ListDataListener arg0) {
        listeners.add(arg0);
    }

    public void removeListDataListener(ListDataListener arg0) {
        listeners.remove(arg0);
    }

    public void propertyChange(PropertyChangeEvent change) {
        if (change.getPropertyName().equals("addExtent")) {
            ListDataEvent event =
                new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0,
                    getSize());
            for (int i = 0; i < listeners.size(); i++) {
                listeners.get(i).intervalAdded(event);
            }
        } else {
            ListDataEvent event =
                new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0,
                    getSize());
            for (int i = 0; i < listeners.size(); i++) {
                listeners.get(i).intervalRemoved(event);
            }
        }
    }

}
