/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extension that handles the info by point tool for a selected layer set
 *
 */
public class InfoToolExtension extends Extension {

    private static final Logger logger = LoggerFactory
            .getLogger(InfoToolExtension.class);

    public void initialize() {
        IconThemeHelper.registerIcon("action", "layer-info-by-point", this);
    }

    public void execute(String s) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();

        if (s.compareTo("layer-info-by-point") == 0) {
            MapControl mapCtrl = view.getMapControl();
            mapCtrl.setTool("info");
            document.setModified(true);
        }
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        FLayer[] layers = mapa.getLayers().getActives();
        for (int i = 0; i < layers.length; i++) {
            if (layers[i].isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

}
