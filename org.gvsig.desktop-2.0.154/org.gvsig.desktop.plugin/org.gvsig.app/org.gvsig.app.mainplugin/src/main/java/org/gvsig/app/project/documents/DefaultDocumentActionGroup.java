/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents;

public class DefaultDocumentActionGroup implements DocumentActionGroup  {

	private String title;
	private String description;
	private int order;
	private String id;

	public DefaultDocumentActionGroup(String id, String title, String description, int order) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.order= order;
	}
	
	public String getDescription() {
		return this.description;
	}

	public int getOrder() {
		return this.order;
	}

	public String getTitle() {
		return this.title;
	}

	public String getId() {
		return this.id;
	}

}
