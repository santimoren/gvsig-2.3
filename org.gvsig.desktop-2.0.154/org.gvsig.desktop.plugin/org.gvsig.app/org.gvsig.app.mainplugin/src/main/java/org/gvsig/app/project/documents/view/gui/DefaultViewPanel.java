/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiFrame.NewStatusBar;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.DefaultProject;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.view.MapOverview;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.toc.gui.TOC;
import org.gvsig.app.project.documents.view.toolListeners.AreaListener;
import org.gvsig.app.project.documents.view.toolListeners.InfoListener;
import org.gvsig.app.project.documents.view.toolListeners.MeasureListener;
import org.gvsig.app.project.documents.view.toolListeners.PanListener;
import org.gvsig.app.project.documents.view.toolListeners.PointSelectListener;
import org.gvsig.app.project.documents.view.toolListeners.PolygonSelectListener;
import org.gvsig.app.project.documents.view.toolListeners.RectangleSelectListener;
import org.gvsig.app.project.documents.view.toolListeners.StatusBarListener;
import org.gvsig.app.project.documents.view.toolListeners.ZoomInListener;
import org.gvsig.app.project.documents.view.toolListeners.ZoomOutListener;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.events.ColorEvent;
import org.gvsig.fmap.mapcontext.events.ExtentEvent;
import org.gvsig.fmap.mapcontext.events.ProjectionEvent;
import org.gvsig.fmap.mapcontext.events.listeners.ViewPortListener;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlCreationException;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.tools.Behavior.Behavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.MouseMovementBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.MouseWheelBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.MoveBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PointBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PolygonBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PolylineBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.RectangleBehavior;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.utils.console.JConsole;
import org.gvsig.utils.console.JDockPanel;
import org.gvsig.utils.console.ResponseListener;
import org.gvsig.utils.console.jedit.JEditTextArea;

/**
 * <p>
 * <b>Class View</b>. This class represents the gvSIG specific internal window
 * where the maps are displayed and where the events coming from the user are
 * captured.
 * </p>
 * <p>
 * It is composed by three main visual areas:
 * </p>
 * <ol>
 * <li>
 * <b>Map control</b>: the map area located in the right area of the window. It
 * takes up the biggest part of the window.</li>
 * <li>
 * <b>Table of contents (TOC)</b>: is a list of layers displayed in the view.
 * The TOC is located on the left-top corner of the View and is the place where
 * the user can modify the order, the legends, the visibility and other
 * properties of the layers.</li>
 * <li>
 * <b>Map overview</b>: is a small MapControl located in the left-bottom corner
 * of the View where the user can put some layers which summarizes the view. It
 * is used to make the navigation easier and faster.</li>
 * </ol>
 *
 * @author 2005- Vicente Caballero
 * @author 2009- Joaquin del Cerro
 *
 */
public class DefaultViewPanel extends AbstractViewPanel implements Observer {

    /**
     *
     */
    private static final long serialVersionUID = -4044661458841786519L;

    private JConsole console;
    private JDockPanel dockConsole = null;
    protected ResponseAdapter consoleResponseAdapter = new ResponseAdapter();
    protected boolean isShowConsole = false;
    private ViewPortListener viewPortListener;

    private static MapControlManager mapControlManager = MapControlLocator
            .getMapControlManager();

    /**
     * Creates a new View object. Before being used, the object must be
     * initialized.
     *
     * @see initialize()
     */
    public DefaultViewPanel() {
        super();
        this.setName("View");
        // TODO Remove this when the system lo load libraries is finished
        if (mapControlManager == null) {
            mapControlManager = MapControlLocator.getMapControlManager();
        }
    }

    public DefaultViewPanel(Document document) {
        this();
        this.initialize(((ViewDocument) document).getMapContext());
        this.setDocument(document);
    }

    /**
     * Create the internal componentes and populate the window with them. If the
     * layout properties were set using the
     * <code>setWindowData(WindowData)</code> method, the window will be
     * populated according to this properties.
     */
    protected void initialize(MapContext mapContext) {
        super.initialize();
        initComponents(mapContext);
        hideConsole();
        getConsolePanel().addResponseListener(consoleResponseAdapter);
    }

    public void setDocument(Document document) {
        setModel((ViewDocument) document);
    }

    public Document getDocument() {
        return this.modelo;
    }

    public void setModel(ViewDocument model) {
        this.modelo = model;
        // Se registra como listener de cambios en FMap
        MapContext fmap = modelo.getMapContext();

        FLayers layers = fmap.getLayers();
        for (int i = 0; i < layers.getLayersCount(); i++) {
            if (layers.getLayer(i).isEditing()
                    && layers.getLayer(i) instanceof FLyrVect) {
                this.showConsole();
            }
        }

        // Se configura el mapControl
        m_TOC.setMapContext(fmap);
        m_MapControl.getMapContext().getLayers().addLegendListener(m_TOC);

        m_MapControl.setBackground(new Color(255, 255, 255));
        if (modelo.getMapOverViewContext() != null) {
            m_MapLoc.setModel(modelo.getMapOverViewContext());
        }
        model.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("name")) {
                    PluginServices.getMDIManager()
                            .getWindowInfo(DefaultViewPanel.this)
                            .setTitle(
                                    PluginServices.getText(this, "Vista") + ": "
                                    + (String) evt.getNewValue());
                }
            }
        });
        if (m_MapControl.getViewPort() != null) {
            viewPortListener = new ViewPortListener() {
                public void extentChanged(ExtentEvent e) {
                    if (PluginServices.getMainFrame() != null) {
                        PluginServices
                                .getMainFrame()
                                .getStatusBar()
                                .setControlValue(
                                        "view-change-scale",
                                        String.valueOf(m_MapControl
                                                .getMapContext().getScaleView()));
                        PluginServices
                                .getMainFrame()
                                .getStatusBar()
                                .setMessage(
                                        "projection",
                                        getMapControl().getViewPort()
                                        .getProjection().getAbrev());
                    }
                }

                public void backColorChanged(ColorEvent e) {
                    // Do nothing
                }

                public void projectionChanged(ProjectionEvent e) {
                    m_MapLoc.setProjection(e.getNewProjection());
                }
            };
            m_MapControl.getViewPort().addViewPortListener(viewPortListener);
        }
    }

    public JConsole getConsolePanel() {
        if (console == null) {
            console = new JConsole(true);
            // Para distinguir cuando se est� escribiendo sobre la consola y
            // cuando no.
            console.setJTextName("CADConsole");
        }
        return console;
    }

    private JDockPanel getDockConsole() {
        if (dockConsole == null) {
            dockConsole = new JDockPanel(getConsolePanel());
        }
        return dockConsole;
    }

    public void addConsoleListener(String prefix, ResponseListener listener) {
        consoleResponseAdapter.putSpaceListener(prefix, listener);

    }

    public void removeConsoleListener(ResponseListener listener) {
        consoleResponseAdapter.deleteListener(listener);

    }

    public void focusConsole(String text) {
        getConsolePanel().addResponseText(text);

        JEditTextArea jeta = getConsolePanel().getTxt();
        jeta.requestFocusInWindow();
        jeta.setCaretPosition(jeta.getText().length());

    }

    public void hideConsole() {
        isShowConsole = false;
        getDockConsole().setVisible(false);

    }

    public void showConsole() {
        if (isShowConsole || disableConsole) {
            return;
        }
        isShowConsole = true;
        getMapControl().remove(getDockConsole());
        getMapControl().setLayout(new BorderLayout());
        getMapControl().add(getDockConsole(), BorderLayout.SOUTH);
        getDockConsole().setVisible(true);

    }

    static class ResponseAdapter implements ResponseListener {

        private HashMap<String, ResponseListener> spaceListener
                = new HashMap<String, ResponseListener>();

        public void putSpaceListener(String namespace, ResponseListener listener) {
            spaceListener.put(namespace, listener);
        }

        public void acceptResponse(String response) {
            boolean nameSpace = false;
            int n = -1;
            if (response != null) {
                if ((n = response.indexOf(':')) != -1) {
                    nameSpace = true;
                }
            }

            if (nameSpace) {
                ResponseListener listener
                        = spaceListener.get(response.substring(0, n));
                if (listener != null) {
                    listener.acceptResponse(response.substring(n + 1));
                }
            } else {
                Iterator<ResponseListener> i
                        = spaceListener.values().iterator();
                while (i.hasNext()) {
                    ResponseListener listener = i.next();
                    listener.acceptResponse(response);
                }
            }
        }

        /**
         * @param listener
         */
        public void deleteListener(ResponseListener listener) {
            Iterator<String> i = spaceListener.keySet().iterator();
            while (i.hasNext()) {
                String namespace = i.next();
                ResponseListener l = spaceListener.get(namespace);
                if (l == listener) {
                    spaceListener.remove(namespace);
                }
            }
        }

    }

    protected void initComponents(MapContext mapContext) { // GEN-BEGIN:initComponents
        // Remember to activate it
        try {
            m_MapControl = mapControlManager.createJMapControlPanel(mapContext);
            m_MapControl.setMapControlDrawer(mapControlManager
                    .createDefaultMapControlDrawer());
        } catch (MapControlCreationException e) {
            NotificationManager.addError(e);
        }

        m_MapControl.addExceptionListener(mapControlExceptionListener);
        m_TOC = new TOC();

        // Ponemos el localizador
        m_MapLoc = new MapOverview(m_MapControl);
        try {
            m_MapLoc.setMapControlDrawer(mapControlManager
                    .createDefaultMapControlDrawer());
        } catch (MapControlCreationException e) {
            NotificationManager.addError(e);
        }
        removeAll();
        tempMainSplit = new ViewSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        if (windowLayout == null) {
            m_MapLoc.setPreferredSize(new Dimension(150, 200));
            tempMainSplit.setPreferredSize(new Dimension(500, 300));
        }

        if (!isPalette()) {
            tempSplitToc = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            tempSplitToc.setTopComponent(m_TOC);
            tempSplitToc.setBottomComponent(m_MapLoc);
            tempSplitToc.setResizeWeight(0.7);
            tempMainSplit.setLeftComponent(tempSplitToc);
        } else {
            tempMainSplit.setLeftComponent(m_TOC);
        }
        m_TOC.setVisible(true);
        tempMainSplit.setRightComponent(m_MapControl);
        this.setLayout(new BorderLayout());
        this.add(tempMainSplit, BorderLayout.CENTER);

        if (windowLayout != null) {
            try {
                tempMainSplit.setDividerLocation(Integer.valueOf(
                        windowLayout.get("MainDivider.Location")).intValue());
                if (windowLayout.get("TOCDivider.Location") != null) {
                    tempSplitToc.setDividerLocation(Integer.valueOf(
                            windowLayout.get("TOCDivider.Location")).intValue());
                }
            } catch (NumberFormatException ex) {
                PluginServices.getLogger().error(
                        "Error restoring View properties");
            }
        }

        // Zoom out (pinchas y el mapa se centra y te muestra m�s).
        // No es dibujando un rect�ngulo, es solo pinchando.
        ZoomOutListener zoomOutListener = new ZoomOutListener(m_MapControl);
        m_MapControl.addBehavior("zoomOut", new PointBehavior(zoomOutListener));

        // Zoom por rect�ngulo
        ZoomInListener zoomInListener = new ZoomInListener(m_MapControl);
        m_MapControl.addBehavior("zoomIn", new Behavior[]{
            new RectangleBehavior(zoomInListener), 
            new PointBehavior(zoomOutListener, Behavior.BUTTON_RIGHT)});

        // pan
        PanListener panListener = new PanListener(m_MapControl);
        m_MapControl.addBehavior("pan", new MoveBehavior(panListener,Behavior.BUTTON_LEFT));

        // Medir
        MeasureListener mli = new MeasureListener(m_MapControl);
        m_MapControl.addBehavior("medicion", new PolylineBehavior(mli));

        // Area
        AreaListener ali = new AreaListener(m_MapControl);
        m_MapControl.addBehavior("area", new PolygonBehavior(ali));

        // Info por punto
        InfoListener il = new InfoListener(m_MapControl);
        m_MapControl.addBehavior("info", new PointBehavior(il));

        // Seleccion por punto
        PointSelectListener psl = new PointSelectListener(m_MapControl);
        m_MapControl.addBehavior("pointSelection", new PointBehavior(psl));

        // Selecci�n por rect�ngulo
        RectangleSelectListener rsl = new RectangleSelectListener(m_MapControl);
        m_MapControl.addBehavior("rectSelection", new RectangleBehavior(rsl));

        // Selecci�n por pol�gono
        PolygonSelectListener poligSel = new PolygonSelectListener(m_MapControl);
        m_MapControl.addBehavior("polSelection", new PolygonBehavior(poligSel));
        
        m_MapControl.setTool("zoomIn");

        // Listener de eventos de movimiento que pone las coordenadas del rat�n
        // en la barra de estado
        StatusBarListener statusBarListener = new StatusBarListener(m_MapControl);
        
        m_MapControl.addCombinedBehavior(new MouseMovementBehavior(statusBarListener));
        m_MapControl.addCombinedBehavior(new MouseWheelBehavior());
        m_MapControl.addCombinedBehavior(new MoveBehavior(panListener, Behavior.BUTTON_MIDDLE));
    }

    public void windowActivated() {
        super.windowActivated();

        NewStatusBar statusbar = PluginServices.getMainFrame().getStatusBar();
        MapContext mapContext = this.getMapControl().getMapContext();

        statusbar.setMessage("units",
                PluginServices.getText(this, mapContext.getDistanceName()));
        statusbar.setControlValue("view-change-scale",
                String.valueOf(mapContext.getScaleView()));
        IProjection proj = getMapControl().getViewPort().getProjection();
        if (proj != null) {
            statusbar.setMessage("projection", proj.getAbrev());
        } else {
            statusbar.setMessage("projection", "");
        }
    }

    public void windowClosed() {
        super.windowClosed();
        if (viewPortListener != null) {
            getMapControl().getViewPort().removeViewPortListener(
                    viewPortListener);
        }
        if (getMapOverview() != null) {
            getMapOverview().getViewPort().removeViewPortListener(
                    getMapOverview());
        }

    }

    public void toPalette() {
        isPalette = true;
        m_MapLoc.setPreferredSize(new Dimension(200, 150));
        m_MapLoc.setSize(new Dimension(200, 150));
        movp = new MapOverViewPalette(m_MapLoc, this);
        PluginServices.getMDIManager().addWindow(movp);
        FLayer[] layers
                = getViewDocument().getMapContext().getLayers().getActives();
        if (layers.length > 0 && layers[0] instanceof FLyrVect) {
            if (((FLyrVect) layers[0]).isEditing()) {
                showConsole();
                return;
            }
        }
        hideConsole();

    }

    public void restore() {
        isPalette = false;
        PluginServices.getMDIManager().closeWindow(movp);
        FLayer[] layers
                = getViewDocument().getMapContext().getLayers().getActives();
        if (layers.length > 0 && layers[0] instanceof FLyrVect) {
            if (((FLyrVect) layers[0]).isEditing()) {
                showConsole();
                return;
            }
        }
        hideConsole();
        JSplitPane tempSplitToc = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        tempSplitToc.setTopComponent(m_TOC);
        tempSplitToc.setBottomComponent(m_MapLoc);
        tempSplitToc.setResizeWeight(0.7);
        tempMainSplit.setLeftComponent(tempSplitToc);
    }

    /**
     * Sets the default map overview background color that will be used in
     * subsequent projects.
     *
     * @param color
     * @deprecated use instead
     * Project.getPreferences().setDefaultMapoverViewBackColor
     */
    public static void setDefaultMapOverViewBackColor(Color color) {
        DefaultProject.getPreferences().setDefaultOverviewBackColor(color);
    }

    /**
     * Returns the current default map overview background color defined which
     * is the color defined when the user does not define any other one
     *
     * @return java.awt.Color
     * @deprecated use instead
     * Project.getPreferences().setDefaultMapoverViewBackColor
     */
    public static Color getDefaultMapOverViewBackColor() {
        return DefaultProject.getPreferences().getDefaultOverviewBackColor();
    }

    /**
     * Returns the current default view background color defined which is the
     * color defined when the user does not define any other one
     *
     * @return java.awt.Color
     * @deprecated use instead Project.getPreferences().getDefaultViewBackColor
     */
    public static Color getDefaultBackColor() {
        return DefaultProject.getPreferences().getDefaultViewBackColor();
    }

    /**
     * @deprecated use instead Project.getPreferences().setDefaultViewBackColor
     */
    public static void setDefaultBackColor(Color color) {
        DefaultProject.getPreferences().setDefaultViewBackColor(color);
    }

    public Object getWindowProfile() {
        return WindowInfo.EDITOR_PROFILE;
    }

    /* (non-Javadoc)
     * @see org.gvsig.tools.observer.Observer#update(org.gvsig.tools.observer.Observable, java.lang.Object)
     */
    public void update(final Observable observable, final Object notification) {

        if (notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification event
                    = (FeatureStoreNotification) notification;
            if (event.getType() == FeatureStoreNotification.AFTER_CANCELEDITING
                    || event.getType() == FeatureStoreNotification.AFTER_FINISHEDITING) {

                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            update(observable, notification);
                        }
                    });
                    return;
                }
                getMapControl().setTool("zoomIn");
                hideConsole();
                repaintMap();
            }
        }
    }

    private static boolean disableConsole = false;

    public static void setDisableConsole(boolean disable) {
        disableConsole = disable;
    }

}
