

package org.gvsig.app.project.documents.view.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.JComboBoxUnits;
import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.ProjectPreferences;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.i18n.Messages;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author gvSIG team
 *
 */
public class GeneralViewPropertiesPage extends JPanel implements PropertiesPage {

    /**
     *
     */
    private static final long serialVersionUID = 1356416393907133043L;

    private JPanel propertiesPanel;

    private javax.swing.JLabel jLabelName = null;
    private javax.swing.JTextField txtName = null;
    private javax.swing.JLabel jLabelDate = null;
    private javax.swing.JTextField txtDate = null;
    private javax.swing.JLabel jLabelOwner = null;
    private javax.swing.JTextField txtOwner = null;
    private javax.swing.JLabel jLabelMapUnits = null;
    private javax.swing.JComboBox cmbMapUnits = null;
    private javax.swing.JLabel jLabelDistanceUnits = null;
    private javax.swing.JComboBox cmbDistanceUnits = null;
    private javax.swing.JLabel jLabelAreaUnits = null;
    private javax.swing.JTextArea txtComments = null;
    private javax.swing.JLabel jLabelColor = null;
    private javax.swing.JLabel jLabelComments = null;
    private javax.swing.JPanel lblColor = null;

    private JCheckBox setAsDefCrsChk = null;

    private Color backColor = null;

    private JButton btnColor = null;
    private ViewDocument view = null;
    private javax.swing.JScrollPane jScrollPane = null;
    protected CRSSelectPanel jPanelProj = null;
    private boolean isAcceppted = false;
    private JComboBox cmbDistanceArea = null;

    /**
     * @param view
     */
    public GeneralViewPropertiesPage(ViewDocument view) {
        super(new BorderLayout());
        this.view = view;
        initComponents();
    }

    public boolean whenAccept() {
        isAcceppted = true;
        return whenApply();
    }

    public boolean whenApply() {
        String name = txtName.getText();
        if (name == null || name.length() == 0) {
            return false;
        }
        Project project =
            ProjectManager.getInstance().getCurrentProject();
        List<Document> views =
            project.getDocuments(ViewManager.TYPENAME);
        for (Document theView : views) {
            if (view.equals(theView)) {
                continue;
            }
            if (theView.getName().equals(name)) {
                JOptionPane.showMessageDialog(
                    (Component) PluginServices.getMainFrame(),
                    Messages.getText("elemento_ya_existe"));
                return false;
            }
        }

        //FIXME: Parche provisional para permitir guardar otras propiedades de la vista en edici�n
        if (!view.isLocked()) {
            view.setName(name);
            view.setCreationDate(txtDate.getText());
            view.setOwner(txtOwner.getText());
            view.setComment(txtComments.getText());
            view.getMapContext().getViewPort()
                .setMapUnits(cmbMapUnits.getSelectedIndex());
            view.getMapContext().getViewPort()
                .setDistanceUnits(cmbDistanceUnits.getSelectedIndex());
            view.getMapContext().getViewPort()
                .setDistanceArea(cmbDistanceArea.getSelectedIndex());
            view.setBackColor(backColor);

            // Select the projection
            if (jPanelProj.isOkPressed()) {
                if (!jPanelProj.getCurProj().isProjected()) {
                    getCmbMapUnits()
                        .setSelectedItem(Messages.getText("Grados"));
                    getCmbMapUnits().setEnabled(false);
                } else {
                    if (getCmbMapUnits().getSelectedItem().equals(
                        Messages.getText("Grados"))) {
                        getCmbMapUnits().setSelectedIndex(1);
                    }
                    getCmbMapUnits().setEnabled(true);
                }
                view.setProjection(jPanelProj.getCurProj());
                if (getSetAsDefaultCrsCheckbox().isSelected()) {
                    setAppDefaultCRS(jPanelProj.getCurProj().getAbrev());
                }
            }
        }

        return true;
    }

    public boolean whenCancel() {
        isAcceppted = false;
        return true;
    }

    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("General");
    }

    public int getPriority() {
        return 1000;
    }

    public JComponent asJComponent() {
        return this;
    }

    private void initComponents() {
        this.add(getCenterPanel(), BorderLayout.NORTH);
    }

    private Component getCenterPanel() {
        if (propertiesPanel == null) {
            propertiesPanel = new JPanel(new GridBagLayout());

            propertiesPanel.setLayout(new GridBagLayout());
            propertiesPanel.setBorder(BorderFactory.createEmptyBorder(4, 5, 4, 5));

            GridBagConstraints c = new GridBagConstraints();
            c.insets = new Insets(2, 5, 2, 5);
            c.gridy = -1;

            addRow(propertiesPanel, c, getJLabelName(), getTxtName());
            addRow(propertiesPanel, c, getJLabelDate(), getTxtDate());
            addRow(propertiesPanel, c, getJLabelOwner(), getTxtOwner());
            addRow(propertiesPanel, c, getJLabelMapUnits(), getCmbMapUnits());
            addRow(propertiesPanel, c, getJLabelDistanceUnits(), getCmbDistanceUnits());
            addRow(propertiesPanel, c, getJLabelAreaUnits(), getCmbDistanceArea());

            // some extra space
            addRow(propertiesPanel, c, new JLabel(" "), new JLabel(" "));

            GridLayout gl = new GridLayout(1,2);
            JPanel colorPanel = new JPanel(gl);
            colorPanel.add(getJLabelColor());
            JPanel secondHalfPanel = new JPanel(new GridBagLayout());

            GridBagConstraints c2 = new GridBagConstraints();

            c2.fill = GridBagConstraints.BOTH;
            c2.weightx = 1.0;   //request any extra hor space
            c2.gridx = 0;
            c2.gridwidth = 1;
            c2.gridy = 0;

            JPanel auxPanel = new JPanel(new GridLayout(1,2));
            auxPanel.add(getLblColor(secondHalfPanel.getBackground()));
            auxPanel.add(new JLabel(""));
            secondHalfPanel.add(auxPanel, c2);

            c2.fill = GridBagConstraints.NONE;
            c2.weightx = 0.0;
            c2.gridx = 1;
            secondHalfPanel.add(getBtnColor(), c2);

            colorPanel.add(secondHalfPanel);

            c.gridx = 0;
            c.gridwidth = 2;
            c.gridy++;
            propertiesPanel.add(colorPanel, c);
            c.gridwidth = 1;

            c.anchor = GridBagConstraints.WEST;
            c.weightx = 0.0d;
            c.gridx = 0;
            c.gridwidth = GridBagConstraints.REMAINDER;
            c.gridy++;
            c.weightx = 1.0d;
            c.fill = GridBagConstraints.HORIZONTAL;
            propertiesPanel.add(getJPanelProj(), c);

            // ============ set current as app default CRS
            c.anchor = GridBagConstraints.CENTER;
            c.gridy++;
            JPanel auxp = new JPanel(new BorderLayout());
            auxp.add(getSetAsDefaultCrsCheckbox(), BorderLayout.CENTER);
            propertiesPanel.add(auxp, c);
            // =============================

            // some extra space
            addRow(propertiesPanel, c, new JLabel(" "), new JLabel(" "));

            c.anchor = GridBagConstraints.WEST;
            c.weightx = 0.0d;
            c.gridx = 0;
            c.gridy++;
            propertiesPanel.add(getJLabelComments(), c);

            c.fill = GridBagConstraints.BOTH;
            c.weightx = 1.0d;
            c.gridy++;
            c.gridwidth = 2;
            propertiesPanel.add(getJScrollPaneComments(), c);
        }
        return propertiesPanel;
    }


    /**
     * @return
     */
    private JCheckBox getSetAsDefaultCrsCheckbox() {

        if (setAsDefCrsChk == null) {
            setAsDefCrsChk = new JCheckBox(Messages.getText(
                "_Set_this_CRS_as_app_default"));
            updateSetAsDefaultCRSChk();
        }
        return setAsDefCrsChk;
    }


    private String getAppDefaultCRS() {
        ProjectPreferences pp = new ProjectPreferences();
        IProjection curr_def = pp.getDefaultProjection();
        return curr_def.getAbrev();
    }

    private void setAppDefaultCRS(String abbrev) {
        IProjection proj = CRSFactory.getCRS(abbrev);
        ProjectPreferences projectPreferences = ApplicationLocator.getProjectManager().getProjectPreferences();
        projectPreferences.setDefaultProjection(proj);
    }

    private void updateSetAsDefaultCRSChk() {

        IProjection view_proj = this.getJPanelProj().getCurProj();
        String view_abbrev = view_proj.getAbrev();

        String curr_app_crs_def = getAppDefaultCRS();
        if (view_abbrev.compareToIgnoreCase(curr_app_crs_def) == 0) {
            // same as curr default
            this.getSetAsDefaultCrsCheckbox().setSelected(true);
            this.getSetAsDefaultCrsCheckbox().setEnabled(false);
        } else {
            this.getSetAsDefaultCrsCheckbox().setEnabled(true);
            this.getSetAsDefaultCrsCheckbox().setSelected(false);
        }
    }

    private void addRow(JComponent panel, GridBagConstraints c, JComponent label, JComponent text) {
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 0.0d;
        c.gridx = 0;
        c.gridy++;
        panel.add(label, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.gridx = 1;
        panel.add(text, c);
    }



    /**
     * This method initializes jLabel
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelName() {
        if (jLabelName == null) {
            jLabelName =
                new javax.swing.JLabel(Messages.getText("nombre") + ":");
        }

        return jLabelName;
    }

    /**
     * This method initializes txtName
     *
     * @return javax.swing.JTextField
     */
    private javax.swing.JTextField getTxtName() {
        if (txtName == null) {
            txtName = new javax.swing.JTextField(view.getName());
        }

        return txtName;
    }

    /**
     * This method initializes jLabel1
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelDate() {
        if (jLabelDate == null) {
            jLabelDate =
                new javax.swing.JLabel(Messages.getText("creation_date") + ":");
        }

        return jLabelDate;
    }

    /**
     * This method initializes txtDate
     *
     * @return javax.swing.JTextField
     */
    private javax.swing.JTextField getTxtDate() {
        if (txtDate == null) {
            txtDate = new javax.swing.JTextField(view.getCreationDate());
            txtDate.setEditable(false);
            txtDate.setBackground(java.awt.Color.white);
        }

        return txtDate;
    }

    /**
     * This method initializes jLabel2
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelOwner() {
        if (jLabelOwner == null) {
            jLabelOwner =
                new javax.swing.JLabel(Messages.getText("owner") + ":");
        }

        return jLabelOwner;
    }

    /**
     * This method initializes txtOwner
     *
     * @return javax.swing.JTextField
     */
    private javax.swing.JTextField getTxtOwner() {
        if (txtOwner == null) {
            txtOwner = new javax.swing.JTextField(view.getOwner());
        }

        return txtOwner;
    }

    /**
     * This method initializes jLabel4
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelMapUnits() {
        if (jLabelMapUnits == null) {
            jLabelMapUnits =
                new javax.swing.JLabel(Messages.getText("map_units") + ":");
        }

        return jLabelMapUnits;
    }

    /**
     * This method initializes cmbMapUnits
     *
     * @return javax.swing.JComboBox
     */
    private javax.swing.JComboBox getCmbMapUnits() {
        if (cmbMapUnits == null
            || MapContext.getDistanceNames().length > cmbMapUnits
                .getItemCount()) {
            cmbMapUnits = new JComboBoxUnits(false);

            IProjection proj = view.getProjection();
            if (!proj.isProjected()) {
                cmbMapUnits.setSelectedItem(Messages.getText("Grados")); // deegree
                cmbMapUnits.setEnabled(false);
            } else {
                if (!(cmbMapUnits.getItemCount() == MapContext
                    .getDistanceNames().length)) {
                    cmbMapUnits.removeItem(Messages.getText("Grados")); // deegree
                    view.getMapContext().getViewPort().setMapUnits(1);
                }
                cmbMapUnits.setSelectedIndex(view.getMapContext().getViewPort()
                    .getMapUnits());
                cmbMapUnits.setEnabled(true);
            }
        }

        return cmbMapUnits;
    }

    /**
     * This method initializes jLabel5
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelDistanceUnits() {
        if (jLabelDistanceUnits == null) {
            jLabelDistanceUnits =
                new javax.swing.JLabel(Messages.getText("distance_units") + ":");
        }

        return jLabelDistanceUnits;
    }

    /**
     * This method initializes jLabel6
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelAreaUnits() {
        if (jLabelAreaUnits == null) {
            jLabelAreaUnits =
                new javax.swing.JLabel(Messages.getText("unidades_area") + ":");
        }

        return jLabelAreaUnits;
    }

    /**
     * This method initializes cmbDistanceUnits
     *
     * @return javax.swing.JComboBox
     */
    private javax.swing.JComboBox getCmbDistanceUnits() {
        if (cmbDistanceUnits == null
            || MapContext.getDistanceNames().length > cmbDistanceUnits
                .getItemCount()) {
            cmbDistanceUnits = new JComboBoxUnits(false);
            cmbDistanceUnits.setSelectedIndex(view.getMapContext()
                .getViewPort().getDistanceUnits());
        }

        return cmbDistanceUnits;
    }

    /**
     * This method initializes jLabel6
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelComments() {
        if (jLabelComments == null) {
            jLabelComments =
                new javax.swing.JLabel(Messages.getText("comentarios") + ":");
            jLabelComments
                .setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            jLabelComments
                .setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        }

        return jLabelComments;
    }

    /**
     * This method initializes txtComments
     *
     * @return javax.swing.JTextArea
     */
    private javax.swing.JTextArea getTxtComments() {
        if (txtComments == null) {
            txtComments = new javax.swing.JTextArea(view.getComment());
            txtComments.setRows(3);
            txtComments.setColumns(20);
        }

        return txtComments;
    }

    /**
     * This method initializes jLabel7
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabelColor() {
        if (jLabelColor == null) {
            jLabelColor =
                new javax.swing.JLabel(Messages.getText("background_color")
                    + ":");
        }

        return jLabelColor;
    }

    /**
     * This method initializes lblColor
     *
     * @return javax.swing.JLabel
     */
    private javax.swing.JPanel getLblColor(Color surround_color) {
        if (lblColor == null) {
            lblColor = new javax.swing.JPanel();
            lblColor.setPreferredSize(new java.awt.Dimension(42, 21));
            Color theColor = view.getMapContext().getViewPort().getBackColor();
            backColor = theColor;
            if (theColor == null) {
                theColor = Color.WHITE;
            }
            lblColor.setBackground(theColor);
            // lblColor.setBorder(BorderFactory.createLineBorder(surround_color, 3));
            lblColor.setOpaque(true);
        }

        return lblColor;
    }

    /**
     * This method initializes btnColor
     *
     * @return javax.swing.JButton
     */
    private JButton getBtnColor() {
        if (btnColor == null) {
            btnColor = new JButton();
            // ToolsSwingLocator.getUsabilitySwingManager().createJButton();

            btnColor.setText("...");

            btnColor.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    Color ret =
                        JColorChooser.showDialog(GeneralViewPropertiesPage.this,
                            Messages.getText("background_color"),
                            lblColor.getBackground());

                    if (ret != null) {
                        lblColor.setBackground(ret);
                        backColor = ret;
                    } else {
                        lblColor.setBackground(Color.WHITE);
                    }
                }
            });
        }

        return btnColor;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private javax.swing.JScrollPane getJScrollPaneComments() {
        if (jScrollPane == null) {
            jScrollPane = new javax.swing.JScrollPane();
            jScrollPane.setViewportView(getTxtComments());
            Dimension dim = getTxtComments().getPreferredSize();
            jScrollPane
                .setMinimumSize(new Dimension(dim.width, dim.height + 10));
        }

        return jScrollPane;
    }

    /**
     * This method initializes jPanel4
     *
     * @return javax.swing.JPanel
     */
    private CRSSelectPanel getJPanelProj() {
        if (jPanelProj == null) {
            IProjection proj = view.getProjection();
            jPanelProj = CRSSelectPanel.getPanel(proj);
            jPanelProj.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (jPanelProj.isOkPressed()) {
                    	int numberOfLayers = view.getMapContext().getLayers().getLayersCount();
                    	if(numberOfLayers > 0) {
                    		warningViewProjectionChange();
                    	}

                        if (!jPanelProj.getCurProj().isProjected()) {
                            getCmbMapUnits().setSelectedItem(
                                PluginServices
                                    .getText(this, "Grados"));
                            getCmbMapUnits().setEnabled(false);
                        } else {
                            if (getCmbMapUnits().getSelectedItem().equals(
                                PluginServices
                                    .getText(this, "Grados"))) {
                                getCmbMapUnits().setSelectedIndex(1);
                            }
                            getCmbMapUnits().setEnabled(true);
                        }
                        updateSetAsDefaultCRSChk();
                    }
                }
            });
        }
        return jPanelProj;
    }

    private void warningViewProjectionChange() {
    	JOptionPane.showMessageDialog(
    			(Component)PluginServices.getMainFrame(),
    			Messages.getText("view_contain_layers"));
    }

    /**
     * @return ViewDocument
     * @see org.gvsig.andami.ui.mdiManager.SingletonWindow#getWindowModel()
     */
    public Object getWindowModel() {
        return view;
    }


    /**
     * @return boolean
     */
    public boolean isAcceppted() {
        return isAcceppted;
    }

    /**
     * This method initializes jComboBox
     *
     * @return javax.swing.JComboBox
     */
    private JComboBox getCmbDistanceArea() {
        String[] names = MapContext.getAreaNames();
        if (cmbDistanceArea == null
            || names.length > cmbDistanceArea.getItemCount()) {
            for (int i = 0; i < names.length; i++) {
                names[i] =
                    Messages.getText(names[i]) + MapContext.getOfLinear(i);
            }
            cmbDistanceArea = new javax.swing.JComboBox(names);
            cmbDistanceArea.setEditable(false);
            cmbDistanceArea.setSelectedIndex(view.getMapContext().getViewPort()
                .getDistanceArea());
            cmbDistanceArea
                .addActionListener(new java.awt.event.ActionListener() {

                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        // view.getMapContext().getViewPort().setDistanceUnits(cmbDistanceUnits.getSelectedIndex());
                    }
                });
        }

        return cmbDistanceArea;

    }




}
