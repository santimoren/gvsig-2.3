/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.gui.ChangeName;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Realiza una agrupación de capas, a partir de las capas que se encuentren activas.
 *
 * @author Vicente Caballero Navarro
 */
public class LayersGroupTocMenuEntry extends AbstractTocContextMenuAction {
	/**
	 * Useful for debug the problems during the implementation.
	 */
	private static Logger logger = LoggerFactory.getLogger(LayersGroupTocMenuEntry.class);

	public String getGroup() {
		return "group4"; //FIXME
	}

	public int getGroupOrder() {
		return 40;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "agrupar_capas");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length > 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (selectedItems.length < 2) {
			return false;
		}
		FLayers parent = selectedItems[0].getParentLayer();
		for (int i = 1; i < selectedItems.length;i++){
			if (parent != selectedItems[i].getParentLayer()){
				return false;
			}
		}
		return true;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
		//ITocItem tocItem = (ITocItem) getNodeUserObject();
		ChangeName changename=new ChangeName(null);
		PluginServices.getMDIManager().addWindow(changename);
		if (!changename.isAccepted())
			return;
		String nombre=changename.getName();

		if (nombre != null){

			getMapContext().beginAtomicEvent();
			FLayers parent = selectedItems[0].getParentLayer();
			FLayers newGroup = getMapContext().getNewGroupLayer(parent);
			
			newGroup.setProjection(getMapContext().getProjection());
			newGroup.setName(nombre);
			
			int pos=0;
			for (int i=0;i<parent.getLayersCount();i++){
				if (parent.getLayer(i).equals(selectedItems[0])){
					pos=i;
					continue;
				}
			}
			for (int j = 0; j < selectedItems.length; j++){
				FLayer layer = selectedItems[j];
				parent.join(layer, newGroup);
			}
			parent.addLayer(pos,newGroup);
			newGroup.dispose();
			getMapContext().endAtomicEvent();
			// TRUCO PARA REFRESCAR.
			getMapContext().invalidate();
			Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
			project.setModified(true);
		}
	}
}
