/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersistenceDevelTool {
	private static Logger logger = LoggerFactory.getLogger(PersistenceDevelTool.class);

	public void showPersistenceFactories() {
		String html = this.getPersistenceFactories();
		InfoPanel.save2file("persistenceinfo", html);
		InfoPanel.showPanel("Persistence factories", WindowManager.MODE.WINDOW, html);
	}
	

	public class PersistenceInfo implements Comparable<PersistenceInfo> {
		PersistenceFactory factory;
		@SuppressWarnings("rawtypes")
		Class theClass;
		DynStruct definition;

		PersistenceInfo(PersistenceFactory factory,
				@SuppressWarnings("rawtypes") Class theClass,
				DynStruct definition) {
			this.factory = factory;
			this.theClass = theClass;
			this.definition = definition;
		}

		String getDefinitionName() {
			if (definition == null) {
				return "";
			}
			return definition.getFullName();
		}

		String getDefinitionDescription() {
			if (definition == null) {
				return "";
			}
			return definition.getDescription();
		}

		String getClassName() {
			if (theClass == null) {
				return "";
			}
			return theClass.getName();
		}

		String getFactoryName() {
			if (factory == null) {
				return "";
			}
			return factory.getClass().getName();
		}

		public int compareTo(PersistenceInfo other) {
			int r = this.factory.getClass().getName()
					.compareTo(other.factory.getClass().getName());
			if (r == 0) {
				return this.theClass.getName().compareTo(
						other.theClass.getName());
			}
			return r;
		}
	}

	@SuppressWarnings("rawtypes")
	public class ClassComparator implements Comparator {

		public int compare(Object o1, Object o2) {
			if (o1 == null || o2 == null || ((Class) o1).getName() == null) {
				logger.warn("Esto no deberia estar pasando.");
				return 0; // FIXME
			}
			return ((Class) o1).getName().compareTo(((Class) o2).getName());
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String getPersistenceFactories() {
		int warningsCounter = 0;
		List<PersistenceInfo> classes = new ArrayList<PersistenceInfo>();
		Set<Class> referencedClasses = new TreeSet<Class>(new ClassComparator());

		PersistenceManager manager = ToolsLocator.getPersistenceManager();

		List<PersistenceFactory> factories = manager.getFactories();
		for (PersistenceFactory factory : factories) {
			List<Class> theClasses = factory.getManagedClasses();
			for (Class theClass : theClasses) {

				DynStruct definition = manager.getDefinition(theClass);
				List defs = factory.getDefinitions();
				if (definition == null && defs != null && !defs.isEmpty()) {
					for (int i = 0; i < defs.size(); i++) {
						definition = (DynStruct) defs.get(i);
						classes.add(new PersistenceInfo(factory, theClass,
								definition));
					}
				} else {
					classes.add(new PersistenceInfo(factory, theClass,
							definition));
				}
			}
		}
		Collections.sort(classes);

		StringBuffer buffer = new StringBuffer();
		StringBuffer warnings = new StringBuffer();

		buffer.append("<html>\n");
		buffer.append("<body>\n");
		buffer.append("<h2>Supported persistent classes</h2>\n");
		buffer.append("<br>\n");

		buffer.append("<ol>\n");
		for (PersistenceInfo classInfo : classes) {
			StringBuffer classBuffer = new StringBuffer();
			boolean warning = false;
			classBuffer.append("  <li>\n    ");
			classBuffer.append("Class: <i>");
			classBuffer.append(classInfo.getClassName());
			classBuffer.append("</i><br>\n    Persistent: <i>");
			if (Persistent.class.isAssignableFrom(classInfo.theClass)) {
				classBuffer.append(" yes.");
			} else {
				classBuffer.append(" through a factory.");
			}
			classBuffer.append("<br>\n    Factory: <i>");
			classBuffer.append(classInfo.getFactoryName()).append(
					"</i><br>\n    Definition name: <i>");
			classBuffer.append(classInfo.getDefinitionName()).append(
					"</i><br>\n    Description: \n");
			classBuffer.append(classInfo.getDefinitionDescription()).append(
					"<br>\n    ");
			DataTypesManager dataTypesManager = ToolsLocator
					.getDataTypesManager();
			DynStruct definition = classInfo.definition;
			if (definition == null) {
				classBuffer.append("Definition for ")
						.append(classInfo.getClassName())
						.append(" is null.<br>\n    ");
				warningsCounter++;
				warning = true;
			} else {
				DynField[] fields = definition.getDynFields();
				for (int i = 0; i < fields.length; i++) {
					DynField field = fields[i];
					if (dataTypesManager.isContainer(field.getType())) {
						if (field.getClassOfItems() == null) {
							classBuffer
									.append("Field <b>")
									.append(field.getName())
									.append("</b>, container,  can't has class for value of items.<br>\n    ");
							warningsCounter++;
							warning = true;
						} else {
							classBuffer.append("Field ")
									.append(field.getName())
									.append(", container of '")
									.append(field.getClassOfItems().getName())
									.append("'.<br>\n    ");
							referencedClasses.add(field.getClassOfItems());
						}
					} else if (dataTypesManager.isObject(field.getType())) {
						if (field.getClassOfValue() == null) {
							classBuffer
									.append("Field <b>")
									.append(field.getName())
									.append("</b> can't has class of value.<br>\n    ");
							warningsCounter++;
							warning = true;
						} else {
							classBuffer.append("Field ")
									.append(field.getName()).append(" of '")
									.append(field.getClassOfValue())
									.append("'.<br>\n    ");
							referencedClasses.add(field.getClassOfValue());
						}
					}
				}
			}
			classBuffer.append("<br>\n  </li>\n");
			buffer.append(classBuffer);
			if (warning) {
				warnings.append(classBuffer);
			}
		}
		buffer.append("</ol>\n");
		buffer.append("<br>\n");
		buffer.append("<br>\n");

		buffer.append("<h2>Persistent classes with problems</h2>\n");
		buffer.append("<ol>\n");
		buffer.append(warnings);
		buffer.append("</ol>\n");
		buffer.append("<br>\n<p>Total warnigs: ").append(warningsCounter)
				.append("</p>");

		buffer.append("<h2>Not persistents used classes</h2>\n");
		buffer.append("<ol>\n");
		for (Class theClass : referencedClasses) {
			// if( manager.canPersist(theClass) ) {
			// continue;
			// }
			if (Persistent.class.isAssignableFrom(theClass)) {
				continue;
			}
			if (manager.getFactories().get(theClass) != null) {
				continue;
			}
			buffer.append("  <li>\n");
			buffer.append("    <i>").append(theClass.getName())
					.append("</i><br>\n");
			buffer.append("  </li>\n");
			warningsCounter++;
		}
		buffer.append("</ol>\n");

		buffer.append("</body>\n");
		buffer.append("</html>\n");

		return buffer.toString();
	}


}
