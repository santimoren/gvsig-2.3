/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app;

import java.awt.Color;


public interface PreferencesNode {

	public String name() ;

	public void clear() throws Exception ;
	
	public void flush() throws Exception;

	public String[] keys() throws Exception ;

	public String get(String arg0, String arg1) ;

	public void remove(String arg0) ;

	public void sync() throws Exception ;

	
	
	public Color getColor(String name, Color value) ;

	public boolean getBoolean(String name, boolean value) ;

	public byte[] getByteArray(String name, byte[] value) ;

	public double getDouble(String name, double value) ;

	public float getFloat(String name, float value) ;

	public int getInt(String name, int value) ;

	public long getLong(String name, long value) ;


	public void put(String name, String value) ;

	public void putBoolean(String name, boolean value) ;

	public void putByteArray(String name, byte[] value) ;

	public void putDouble(String name, double value) ;

	public void putFloat(String name, float value) ;

	public void putInt(String name, int value) ;

	public void putLong(String name, long value) ;


}
