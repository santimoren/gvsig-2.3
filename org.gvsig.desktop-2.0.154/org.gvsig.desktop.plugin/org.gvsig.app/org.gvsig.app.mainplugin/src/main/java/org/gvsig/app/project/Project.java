/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cresques.cts.IProjection;

import org.gvsig.app.project.documents.Document;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesSupport;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.exception.PersistenceException;

public interface Project extends DocumentsContainer,  Persistent, ExtendedPropertiesSupport {

    public static final String PROJECT_PROPERTIES_PAGE_GROUP = "Proyect";

    public static final String FILE_EXTENSION = ".gvsproj";

	public void addPropertyChangeListener(PropertyChangeListener arg0);

	/**
	 * Return the creation date of the project
	 *
	 * @return
	 */
	public String getCreationDate();

	/**
	 * Return the name of the project
	 *
	 * @return
	 */
	public String getName();

	/**
	 * Set the name of he project.
	 *
	 * @param string
	 */
	public void setName(String name);

	/**
	 * Return the comments associateds with the project
	 *
	 * @return comments
	 */
	public String getComments();

	/**
	 * Set the comments associateds with the project
	 *
	 * @param comments as string
	 */
	public void setComments(String string);

	/**
	 * Retuen the modification date of the project.
	 *
	 * @return modification date as string
	 */
	public String getModificationDate();

	/**
	 * Return the author of the project,
	 *
	 * @return author as string
	 */
	public String getOwner();

	/**
	 * Sets the author of the project
	 *
	 * @param author name as string
	 */
	public void setOwner(String owner);

	/**
	 * Obtiene el color de selecci�n que se usar� en el proyecto
	 *
	 * @return
	 */
	public Color getSelectionColor();

	/**
	 * Sets the selecction color
	 *
	 * @param selection color as string
	 */
	public void setSelectionColor(String selectionColor);

	/**
	 * Sets the selecction color
	 *
	 * @param selection color as Color
	 */
	public void setSelectionColor(Color selectionColor);

	public IProjection getProjection();

	public void setProjection(IProjection projection);

	/**
	 * Sets the modified state of project.
	 *
	 * Can't set to not modified.
	 *
	 * @param modified as boolean
	 */
	public void setModified(boolean modified);

	public boolean hasChanged();


	/**
	 * Adds a document to the project
	 *
	 * @param document as IProjectDocument
         * @deprecated use addDocument
	 */
	public void add(Document document);

	/**
	 * Remove a document of the project
	 *
	 * @param document as Document
         * @deprecated use removeDocument
	 */
	public void remove(Document document);

	/**
	 * Return the view that contains the especified layer.
	 *
	 * @param layer
	 *
	 * @return name of the view that contains the layer
	 *
	 * @throws RuntimeException
	 *             Si la capa que se pasa como par�metro no se encuentra en
	 *             ninguna vista
	 */
	public String getViewName(FLayer layer);

	public void addExtent(ProjectExtent arg1);

	public ProjectExtent removeExtent(int arg0);

	public ProjectExtent[] getExtents();

	public void saveState(File out) throws PersistenceException;

	public void saveState(OutputStream out) throws PersistenceException;

	public void loadState(InputStream in);

	public void loadState(File in);

	public String exportDocumentsAsText(List<Document> documents);

	public void importDocuments(String data, String doctype);

	public boolean canImportDocuments(String data, String doctype);

    public File getFile();

    public Set<String> getUnloadedObjects();

}