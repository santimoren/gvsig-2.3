
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import org.gvsig.tools.dynobject.DynObject;


public interface FilesystemExplorerPropertiesPanelFactory {
    
    /**
     * Create the properties panel associated to the parameters.
     * @param parameters
     * @return the properties panel.
     */
    public FilesystemExplorerPropertiesPanel create(DynObject parameters);

    /**
     * Return true if this factory can apply to the parameters.
     * 
     * @param parameters
     * @return true if this factory can apply to the parameters
     */
    public boolean canBeApplied(DynObject parameters);
    
    /**
     * The priority of this factory.
     * Cuando hay mas de una factoria aplicable a unos parametros es cogida la
     * de prioridad mas alta.
     * 
     * @return the priority of this factory
     */
    public int getPriority();    
}
