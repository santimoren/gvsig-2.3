/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.undo.RedoException;



/**
 * Extensi�n encargada de gestionar el rehacer un comando anteriormente
 * deshecho.
 *
 * @author Vicente Caballero Navarro
 */
public class RedoViewExtension extends Extension {
	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
        IconThemeHelper.registerIcon("action", "edit-redo", this);
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String s) {
		DefaultViewPanel vista = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapControl = vista.getMapControl();

		if (s.compareTo("edit-redo-layer") == 0) {
			try {
					FLayers layers=mapControl.getMapContext().getLayers();
					for (int i=0;i<layers.getLayersCount();i++){
						if (layers.getLayer(i) instanceof FLyrVect && layers.getLayer(i).isEditing() && layers.getLayer(i).isActive()){
							((FLyrVect)layers.getLayer(i)).getFeatureStore().redo();
							mapControl.drawMap(false);
							ApplicationLocator.getManager().refreshMenusAndToolBars();
						}

					}
			} catch (RedoException e) {
				NotificationManager.addError(e.getMessage(),e);
			}
		}
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
		DefaultViewPanel vista = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapControl = vista.getMapControl();
		FLayers layers=mapControl.getMapContext().getLayers();
		for (int i=0;i<layers.getLayersCount();i++){
//			try {
				if (layers.getLayer(i) instanceof FLyrVect && ((FLyrVect)layers.getLayer(i)).getFeatureStore().isEditing() && layers.getLayer(i).isActive()){
					return ((FLyrVect)layers.getLayer(i)).getFeatureStore().canRedo();
				}
//			} catch (ReadException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (DataException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

		}
		return false;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
															 .getActiveWindow();

		if (f == null) {
			return false;
		}

		if (f instanceof DefaultViewPanel) {
			MapControl mapControl = ((DefaultViewPanel)f).getMapControl();
			FLayer[] layers=mapControl.getMapContext().getLayers().getActives();
			FLayer layer;
			for (int i=0;i<layers.length;i++){
				layer = layers[i];
				if (!layer.isAvailable()){
					continue;
				}
//				try {
					if (layer instanceof FLyrVect && ((FLyrVect)layer).getFeatureStore().isEditing()){
						return true;
					}
//				} catch (ReadException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		}
		return false;
	}
}
