/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;


import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowTransform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class PaletteViewInfoExtension extends Extension{
    private static final Logger logger = LoggerFactory
            .getLogger(PaletteViewInfoExtension.class);
    
	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String s) {
        if (s.equals("window-convert-to-tool")){
    		IWindow view = PluginServices.getMDIManager().getActiveWindow();
        	IWindowTransform vt=(IWindowTransform)view;
        	vt.toPalette();
            PluginServices.getMainFrame().enableControls();
        }
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
       return true;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		IWindow f = PluginServices.getMDIManager().getFocusWindow();
		if (f == null) {
			return false;
		}
		if (f instanceof IWindowTransform) {
			IWindowTransform vt=(IWindowTransform)f;
			if (!vt.isPalette())
				return true;
		}
		return false;
	}

}
