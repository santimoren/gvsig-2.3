/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.preferencespage.CartographicSupportPage;
import org.gvsig.app.gui.preferencespage.SymbologyPage;
import org.gvsig.app.gui.styling.MarkerFill;
import org.gvsig.app.gui.styling.PictureFill;
import org.gvsig.app.gui.styling.PictureLine;
import org.gvsig.app.gui.styling.PictureMarker;
import org.gvsig.app.gui.styling.SimpleFill;
import org.gvsig.app.gui.styling.SimpleLine;
import org.gvsig.app.gui.styling.SimpleMarker;
import org.gvsig.app.project.documents.view.legend.gui.AttrInTableLabeling;
import org.gvsig.app.project.documents.view.legend.gui.Categories;
import org.gvsig.app.project.documents.view.legend.gui.Features;
import org.gvsig.app.project.documents.view.legend.gui.General;
import org.gvsig.app.project.documents.view.legend.gui.GeneralLayerPropertiesPage.GeneralLayerPropertiesPageFactory;
import org.gvsig.app.project.documents.view.legend.gui.LabelingManager;
import org.gvsig.app.project.documents.view.legend.gui.LegendManager;
import org.gvsig.app.project.documents.view.legend.gui.MultipleAttributes;
import org.gvsig.app.project.documents.view.legend.gui.Quantities;
import org.gvsig.app.project.documents.view.legend.gui.SingleSymbol;
import org.gvsig.app.project.documents.view.legend.gui.ThemeManagerWindow;
import static org.gvsig.app.project.documents.view.legend.gui.ThemeManagerWindow.addPage;
import org.gvsig.app.project.documents.view.legend.gui.VectorialInterval;
import org.gvsig.app.project.documents.view.legend.gui.VectorialUniqueValue;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.propertypage.PropertiesPageManager;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;

/**
 * Extension for enable the symbology. It only installs the core symbology.
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class BasicSymbologyExtension extends Extension implements IPreferenceExtension{

    public void initialize() {
        // modules for symbol editor
        SymbologySwingManager symbologySwingManager = SymbologySwingLocator.getSwingManager();

        symbologySwingManager.registerSymbolEditor(SimpleFill.class, Geometry.TYPES.SURFACE);
        symbologySwingManager.registerSymbolEditor(SimpleMarker.class, Geometry.TYPES.POINT);
        symbologySwingManager.registerSymbolEditor(SimpleLine.class, Geometry.TYPES.CURVE);
        symbologySwingManager.registerSymbolEditor(SimpleFill.class, Geometry.TYPES.MULTISURFACE);
        symbologySwingManager.registerSymbolEditor(SimpleMarker.class, Geometry.TYPES.MULTIPOINT);
        symbologySwingManager.registerSymbolEditor(SimpleLine.class, Geometry.TYPES.MULTICURVE);

        //FIXME 
        symbologySwingManager.registerSymbolEditor(PictureMarker.class, Geometry.TYPES.POINT);
        symbologySwingManager.registerSymbolEditor(PictureLine.class, Geometry.TYPES.CURVE);
        symbologySwingManager.registerSymbolEditor(PictureFill.class, Geometry.TYPES.SURFACE);
        symbologySwingManager.registerSymbolEditor(MarkerFill.class, Geometry.TYPES.SURFACE);

        symbologySwingManager.registerSymbolEditor(PictureMarker.class, Geometry.TYPES.MULTIPOINT);
        symbologySwingManager.registerSymbolEditor(PictureLine.class, Geometry.TYPES.MULTICURVE);
        symbologySwingManager.registerSymbolEditor(PictureFill.class, Geometry.TYPES.MULTISURFACE);
        symbologySwingManager.registerSymbolEditor(MarkerFill.class, Geometry.TYPES.MULTISURFACE);

        // legends available 
        symbologySwingManager.registerLegendEditor(Quantities.class);
        symbologySwingManager.registerLegendEditor(Features.class);
        symbologySwingManager.registerLegendEditor(Categories.class);
        symbologySwingManager.registerLegendEditor(MultipleAttributes.class);

        symbologySwingManager.registerLegendEditor(SingleSymbol.class);
        symbologySwingManager.registerLegendEditor(VectorialInterval.class);
        symbologySwingManager.registerLegendEditor(VectorialUniqueValue.class);

        // labeling strategies (inverse order to the wanted to be shown)
        symbologySwingManager.registerLabelingEditor(AttrInTableLabeling.class);

        // Registry property page of layers
        PropertiesPageManager propertiesPageManager = MapControlLocator.getPropertiesPageManager();
        propertiesPageManager.registerFactory(new GeneralLayerPropertiesPageFactory());
        
        // Registry old style property page of layers
        // These must be updated to use "PropertiesPage" instead of "ThemeManagerWindow"
        // Do not use this procedure more!!!
        ThemeManagerWindow.addPage(LegendManager.class);
        ThemeManagerWindow.addPage(LabelingManager.class);

        ThemeManagerWindow.setTabEnabledForLayer(LegendManager.class, FLyrVect.class, true);
        ThemeManagerWindow.setTabEnabledForLayer(LabelingManager.class, FLyrVect.class, true);

        registerIcons();
    }

	@Override
	public void postInitialize() {
		super.postInitialize();
		
		SymbologySwingLocator.getSwingManager().setColorTablesFactory(
				ApplicationLocator.getManager().getColorTablesFactory());
	}

	private void registerIcons(){
    	IconThemeHelper.registerIcon("preferences", "symbology-preferences", this);
	}

	public void execute(String actionCommand) {

	}

	public boolean isEnabled() {
		return false;
	}

	public boolean isVisible() {
		return false;
	}

	public IPreference[] getPreferencesPages() {
		return new IPreference[] {
			new CartographicSupportPage(), new SymbologyPage()
		};
	}
}