/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;

import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectExtent;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.mapcontext.MapContext;



public class Encuadrator implements ListSelectorListener {

	private Project project;
//	private View vista;
	private MapContext mapa;

	public Encuadrator(Project project, MapContext map, IView vista){
		this.project = project;
//		this.vista = vista;
		mapa = map;
	}

	/**
	 * @see com.iver.utiles.ListSelectorListener#indexesSelected(int[])
	 */
	public void indexesSelected(int[] indices) {
		mapa.getViewPort().setEnvelope(project.getExtents()[indices[0]].getExtent());
	}

	/**
	 * @see com.iver.utiles.ListSelectorListener#indexesRemoved(int[])
	 */
	public void indexesRemoved(int[] indices) {
		for (int i = 0; i < indices.length; i++){
			project.removeExtent(indices[i]);		
		}
	}

	/* (non-Javadoc)
	 * @see com.iver.utiles.ListSelectorListener#newElement(java.lang.String)
	 */
	public void newElement(String name) {
		ProjectExtent extent = ProjectManager.getInstance().createExtent();
		extent.setDescription(name);
		extent.setExtent(mapa.getViewPort().getEnvelope());
		project.addExtent(extent);
	}

}
