/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import org.cresques.cts.IProjection;

import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.Document;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesSupport;
import org.gvsig.fmap.mapcontext.layers.FLayer;


public interface ViewDocument extends Document, ExtendedPropertiesSupport {

        public static final String VIEW_PROPERTIES_PAGE_GROUP = "ViewDocument";
        public static final String LAYER_PROPERTIES_PAGE_GROUP = "LayerPropertiesPageGroup";

        /**
	 * Gets the Map contexts of the main map in the view.
	 *
	 * @return MapContext
	 */
	public MapContext getMapContext();

	/**
	 * Gets the Map context from the locator, which is the
	 * small map in the left-bottom corner of the View.
	 *
	 * @return
	 */
	public MapContext getMapOverViewContext();

	public void setMapContext(MapContext fmap);

	public void setMapOverViewContext(MapContext fmap);

        /**
         * Gets the name of the View
         * @return name of the view
         */
	public String getName();

	public Project getProject();

	public void addPropertyChangeListener(PropertyChangeListener listener);
        
        
        /**
         * Gets the CRS of the view
         * 
         * @return CRS of the view 
         */
        public IProjection getProjection();
        
        public void setProjection (IProjection proj);
        
        public void setBackColor(Color c) ;
	
        /**
         * Get the first level of layer in the view TOC.
         * 
         * @return layers in TOC
         */
        public Iterator<FLayer> iterator();
        
        /**
         * Get all layer in the view TOC.
         * 
         * @return layers in TOC
         */
        public Iterator<FLayer> deepiterator();
 
        /**
         * Return true if layer is in this view.
         * 
         * @param layer
         * @return 
         */
        public boolean contains(FLayer layer);

        /**
         * Return true if this view has a layer with this store.
         * @param store
         * @return 
         */
        public boolean contains(DataStore store);
        
        /**
         * Center view in the envelope.
         * 
         * @param envelope 
         */
        public void center(Envelope envelope);
}