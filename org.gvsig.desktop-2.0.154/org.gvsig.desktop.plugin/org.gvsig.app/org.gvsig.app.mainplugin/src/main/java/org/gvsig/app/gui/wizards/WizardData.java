/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.wizards;

/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class WizardData {
    private String title;
    private String Abstract;
    private LayerInfo layer;
    private String[] formats;

    /**
     * DOCUMENT ME!
     *
     * @return
     */
    public String getAbstract() {
        return Abstract;
    }

    /**
     * DOCUMENT ME!
     *
     * @return
     */
    public String[] getFormats() {
        return formats;
    }

    /**
     * DOCUMENT ME!
     *
     * @return
     */
    public LayerInfo getLayer() {
        return layer;
    }

    /**
     * DOCUMENT ME!
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * DOCUMENT ME!
     *
     * @param string
     */
    public void setAbstract(String string) {
        Abstract = string;
    }

    /**
     * DOCUMENT ME!
     *
     * @param strings
     */
    public void setFormats(String[] strings) {
        formats = strings;
    }

    /**
     * DOCUMENT ME!
     *
     * @param info
     */
    public void setLayer(LayerInfo info) {
        layer = info;
    }

    /**
     * DOCUMENT ME!
     *
     * @param string
     */
    public void setTitle(String string) {
        title = string;
    }
}
