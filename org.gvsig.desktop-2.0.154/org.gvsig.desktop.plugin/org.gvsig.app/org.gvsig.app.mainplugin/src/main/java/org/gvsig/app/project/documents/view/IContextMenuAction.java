/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;


/**
 * Interface que deben de cumplir una acci�n aplicable dede un
 * Menu contextual o popup.
 * <br>
 * Las acciones se deben registrar en el punto de extensi�n adecuado * 
 * <br><br>
 * Por lo general extender de la clase AbstractDocumentAction
 * 
 *  
 * 
 * @author Jose Manuel Viv� (Chema)
 */

public interface IContextMenuAction {
	
	/**
	 * Dice si la acci�n es visible 
	 * segun los documentos seleccionados
	 * <br>
	 * @param item elemento sobre el que se ha pulsado
	 * @param selectedItems elementos seleccionados en el momento de pulsar
	 * 
	 */
	public boolean isVisible(Object item, Object[] selectedItems);
	
	/**
	 * Dice si la acci�n esta habilitada 
	 * segun los documentos seleccionados
	 * <br>
	 * @param item elemento sobre el que se ha pulsado
	 * @param selectedItems elementos seleccionados en el momento de pulsar
	 * 
	 */
	public boolean isEnabled(Object item, Object[] selectedItems);
	
	/**
	 * Ejecuta la acci�n sobre los documentos seleccionados
	 * <br>
	 * @param item elemento sobre el que se ha pulsado
	 * @param selectedItems elementos seleccionados en el momento de pulsar
	 * 
	 */
	public void execute(Object item, Object[] selectedItems);
	
	/**
	 * Nombre del grupo al que pertenece la accion
	 */
	public String getGroup();
	
	/**
	 * Orden del grupo al que pertenece la acci�n
	 */
	public int getGroupOrder();
	
	/**
	 * Orden del elemento dentro del grupo
	 */
	public int getOrder();
	
	/**
	 * Texto del elemento
	 */
	public String getText();
	
	/**
	 * Descripci�n mas detallada de la acci�n
	 * (se utilizar� como Tooltip)
	 */
	public String getDescription();	
	
}
