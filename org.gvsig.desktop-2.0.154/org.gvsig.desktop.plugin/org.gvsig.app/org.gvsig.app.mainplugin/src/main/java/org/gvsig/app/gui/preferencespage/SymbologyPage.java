/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.preferencespage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.gui.beans.swing.JComboBoxFontSizes;
import org.gvsig.gui.beans.swing.JComboBoxFonts;
import org.gvsig.gui.beans.swing.JFileChooser;
import org.gvsig.i18n.Messages;



/**
 * This class extends AbstractPreferencesPage. This component is a preferences page for
 * symbology and allows select default fore color, fill color, font and size text and
 * the path where the images that compound symbols are located.
 *
 */
public class SymbologyPage extends AbstractPreferencePage {

	private static final long serialVersionUID = 1L;
	
	private SymbolPreferences preferences = null;

	private ColorChooserPanel defaultColor = null;
	private ColorChooserPanel defaultFillColor = null;
	private JSlider jsDefaultSelectionAlpha = null;
	private JSlider jsFillSelelctionAlpha = null;
	protected String id;
	private boolean panelStarted = false;
	private JButton btnSelectProjectsFolder=null;
	private ActionListener btnFileChooserAction=null;
	private JTextField txtProjectsFolder=null;
	private JComboBoxFonts fonts= null;
	private JComboBoxFontSizes sizes= null;
	private ImageIcon icon=null;
	private JCheckBox aleatoryFillColor;

	public SymbologyPage(){
		super();
		id = this.getClass().getName();
	}
	
	public SymbolPreferences getPreferences() {
		if(preferences == null) 	
			preferences = MapContextLocator.getSymbolManager().getSymbolPreferences();
		return preferences;
	}

	@Override
	public void setChangesApplied() {
		setChanged(false);

	}

	public String getID() {
		return id;
	}

	public ImageIcon getIcon() {
		if (icon == null){
			icon = IconThemeHelper.getImageIcon("symbology-preferences");
		}
		return icon;
	}

	public JPanel getPanel() {
		if(panelStarted)return this;
		panelStarted=true;
		addComponent(new JLabel(" "));

		GridBagLayoutPanel selectionDefaultColorPanel = new GridBagLayoutPanel();
		selectionDefaultColorPanel.setBorder(new TitledBorder(
				Messages.getText("_Default_color")));
		selectionDefaultColorPanel.setLayout(new GridBagLayout());
		selectionDefaultColorPanel.add(new JLabel(
				Messages.getText("fill")));
		selectionDefaultColorPanel.add(defaultColor = new ColorChooserPanel());

		selectionDefaultColorPanel.add(new JLabel(
				Messages.getText("alpha")));
		selectionDefaultColorPanel.add(jsDefaultSelectionAlpha = new JSlider(0,255));
		selectionDefaultColorPanel.add(new JBlank(50,50));

		jsDefaultSelectionAlpha.setPreferredSize(new Dimension(100,30));
		jsDefaultSelectionAlpha.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e) {
				defaultColor.setAlpha(((JSlider)e.getSource()).getValue());
		}});
		addComponent(new JLabel(" "));
		addComponent(selectionDefaultColorPanel);

		GridBagLayoutPanel selectionFillColor = new GridBagLayoutPanel();
		selectionFillColor.setBorder(new TitledBorder(
				Messages.getText("_Default_fill_color")));
		selectionFillColor.setLayout(new GridBagLayout());
		selectionFillColor.add(new JLabel(
				Messages.getText("fill")));
		selectionFillColor.add(defaultFillColor = new ColorChooserPanel());

		selectionFillColor.add(new JLabel(
				Messages.getText("alpha")));
		selectionFillColor.add(jsFillSelelctionAlpha = new JSlider(0,255));

		jsFillSelelctionAlpha.setPreferredSize(new Dimension(100,30));
		jsFillSelelctionAlpha.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e) {
				defaultFillColor.setAlpha(((JSlider)e.getSource()).getValue());
		}});

		selectionFillColor.add(new JBlank(50,50));
		selectionFillColor.add(aleatoryFillColor = new JCheckBox());
		selectionFillColor.add(new JLabel("   " +
				Messages.getText("_Random")));


		aleatoryFillColor.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == aleatoryFillColor){
					defaultFillColor.setEnabled(!aleatoryFillColor.isSelected());
					jsFillSelelctionAlpha.setEnabled(!aleatoryFillColor.isSelected());
				}
			}

		});

		addComponent(new JLabel(" "));
		addComponent(selectionFillColor);

		btnFileChooserAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String path;
				if (e.getSource().equals(btnSelectProjectsFolder)) {
					path = txtProjectsFolder.getText();


				FileFilter def =  new FileFilter(){
					public boolean accept(File f) {
						return (f.isDirectory());
					}

					public String getDescription() {
						return null;
					}
				};

				File file = new File(path);
				JFileChooser fc;
				if (file.exists()) {
					fc = new JFileChooser("SYMBOLOGY_PREFERENCE_PAGE_FILECHOOSER", file);
				} else {
					fc= new JFileChooser("SYMBOLOGY_PREFERENCE_PAGE_FILECHOOSER",JFileChooser.getLastPath("SYMBOLOGY_PREFERENCE_PAGE_FILECHOOSER", file));
				}


				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fc.setMultiSelectionEnabled(false);
                fc.setAcceptAllFileFilterUsed(false);
                fc.addChoosableFileFilter(def);
                int result = fc.showOpenDialog(SymbologyPage.this);

                if (result == JFileChooser.APPROVE_OPTION && (file = fc.getSelectedFile()) != null)
                	if (e.getSource().equals(btnSelectProjectsFolder))
    					txtProjectsFolder.setText(file.getAbsolutePath());
                }
			}

		};
		btnSelectProjectsFolder = new JButton(
				Messages.getText("browse"));
		btnSelectProjectsFolder.addActionListener(btnFileChooserAction);

		JPanel panelBrowser = new JPanel();
		panelBrowser.setBorder(new TitledBorder(
				Messages.getText("_Symbols_folder")));

		panelBrowser.add(txtProjectsFolder = new JTextField(30));
		panelBrowser.add(btnSelectProjectsFolder);

		addComponent(panelBrowser);

		fonts= new JComboBoxFonts();
		sizes = new JComboBoxFontSizes();

		JPanel panelFont = new JPanel(new FlowLayout());
		panelFont.setBorder(new TitledBorder(
				Messages.getText("_Default_font")));
		panelFont.add(fonts);
		panelFont.add(sizes);

		addComponent(panelFont);
		initializeValues();
		return this;
	}

	public String getTitle() {
		return Messages.getText("Simbologia");
	}
	
	public void initializeValues() {
		if (!panelStarted) 
			getPanel();

		defaultColor.setColor(getPreferences().getDefaultSymbolColor());
		defaultColor.setAlpha(getPreferences().getDefaultSymbolColor().getAlpha());
		jsDefaultSelectionAlpha.setValue(getPreferences().getDefaultSymbolColor().getAlpha());
		
		aleatoryFillColor.setSelected(getPreferences().isDefaultSymbolFillColorAleatory());

		defaultFillColor.setColor(getPreferences().getDefaultSymbolFillColor());
		defaultFillColor.setAlpha(getPreferences().getDefaultSymbolFillColor().getAlpha());
		jsFillSelelctionAlpha.setValue(getPreferences().getDefaultSymbolFillColor().getAlpha());
		
		txtProjectsFolder.setText(getPreferences().getSymbolLibraryPath());

		fonts.setSelectedItem(getPreferences().getDefaultSymbolFont().getFamily());
		sizes.setSelectedItem(getPreferences().getDefaultSymbolFont().getSize());
		
		defaultFillColor.setEnabled(!aleatoryFillColor.isSelected());
		jsFillSelelctionAlpha.setEnabled(!aleatoryFillColor.isSelected());

	}

	public void initializeDefaults() {

	    defaultColor.setColor(Color.DARK_GRAY);
	    defaultColor.setAlpha(255);
	    jsDefaultSelectionAlpha.setValue(255);
	    
	    aleatoryFillColor.setSelected(true);

	    defaultFillColor.setColor(new Color(60, 235, 235));
	    defaultFillColor.setAlpha(255);
	    jsFillSelelctionAlpha.setValue(255);
	    
	    String path = PluginServices.getPluginServices(this).
	        getPluginHomeFolder().getAbsolutePath() + File.separator + "Symbols";
	    txtProjectsFolder.setText(path);
	    
	    fonts.setSelectedItem("SansSerif");
	    sizes.setSelectedItem(14);
	    
	    defaultFillColor.setEnabled(!aleatoryFillColor.isSelected());
        jsFillSelelctionAlpha.setEnabled(!aleatoryFillColor.isSelected());
	}
	
	public boolean isValueChanged() {
		return super.hasChanged();
	}

	@Override
	public void storeValues() throws StoreException {
		persistPreferences();
	}

	private void persistPreferences() {
		if(defaultColor.getColor() != null) {
			Color color = defaultColor.getColor();
			color = new Color(color.getRed(), 
					color.getGreen(), 
					color.getBlue(), 
					jsDefaultSelectionAlpha.getValue());
			getPreferences().setDefaultSymbolColor(color);
		}

		if(aleatoryFillColor != null) {
			getPreferences().setDefaultSymbolFillColorAleatory(aleatoryFillColor.isSelected());
		}
		
		if (defaultFillColor.getColor()  !=  null) {
			Color color = defaultFillColor.getColor();
			color = new Color(color.getRed(),color.getGreen(),color.getBlue(),jsFillSelelctionAlpha.getValue());
			getPreferences().setDefaultSymbolFillColor(color);
		}

		if (txtProjectsFolder.getText() != null) {
			getPreferences().setSymbolLibraryPath(txtProjectsFolder.getText());
		}

		if(fonts.getFont() != null && sizes.getSelectedItem() != null){
			String f = (String)fonts.getSelectedItem();
			Font font = new Font(f, Font.PLAIN, (Integer)sizes.getSelectedItem());
			getPreferences().setDefaultSymbolFont(font);
		}
	}
}
