/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents;

import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.DocumentsContainer;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.tools.extensionpoint.ExtensionBuilder;
import org.gvsig.tools.observer.WeakReferencingObservable;
import org.gvsig.tools.persistence.PersistenceFactory;

public interface DocumentManager extends ExtensionBuilder, PersistenceFactory, WeakReferencingObservable {

        public static String NOTIFY_AFTER_CREATEDOCUMENT = "DocumentManager.CreateDocument";
        public static String NOTIFY_AFTER_GETMAINWINDOW = "DocumentManager.getMainWindow";
        public static String NOTIFY_AFTER_GETPROPERTIESWINDOW = "DocumentManager.getPropertiesWindow";
        
	/**
	 * Returns the type of document priority.
	 *
	 * @return Priority.
	 */
	public int getPriority();

	/**
	 * Returns tdocumenthe icon for the type of document.
	 *
	 * @return Image.
	 */
	public ImageIcon getIcon();

	/**
	 * Returns the icon for the type of document when is selected
	 *
	 * @return Image.
	 */
	public ImageIcon getIconSelected();

	/**
	 * Create a new ProjectDocument.
	 *
	 * @param project Opened project.
	 *
	 * @return ProjectDocument.
	 */
	public AbstractDocument createDocument();

	        /**
     * Uses a gui to be able from the characteristics that we want a
     * ProjectDocument
     * 
     * @return a new Document
     * 
     * @deprecated user {@link #createDocumentsByUser()} instead
     */
	public AbstractDocument createDocumentByUser();

    /**
     * Uses a gui to be able from the characteristics that we want a
     * ProjectDocument
     * 
     * @return the created documents
     */
    public Iterator<? extends Document> createDocumentsByUser();

	/**
	 * Returns the title of type of document.
	 *
	 * @return String title for type of document
	 */
	public String getTitle();

	/**
	 * Returns the type name of the document factory.
	 *
	 * @return Name of registration
	 */
	public String getTypeName();

	/**
	 * Return the main window asociated to the passed document. 
	 * If the window alreade exists return it. If not, a new
	 * window is created.
	 * 
	 * @param doc
	 * @return Window asociated to document
	 */
	public IWindow getMainWindow(Document doc) ;
	
	/**
	 * Return the main window asociated to the document.
	 * 
	 * @param doc, layout
	 * @return Window asociated to document
	 */
	public IWindow getMainWindow(Document doc, WindowLayout layout) ;
        
        /**
         * Create a new JComponent for the document or if already exists
         * in the specified container, return this.
         * 
         * @param container
         * @param doc
         * @return 
         */
        public JComponent getMainComponent(DocumentsContainer container, Document doc);
	
        /**
         * Devuelbe el JComponent que se corresponde con el documento indicado.
         * Si la ventana activa se corresponde con este documento, devuelbe el 
         * JComponent asociado a esa ventana. Si no se corresponde con la ventana
         * activa, pero esta es un contenedor de documentos le pide a este el componente
         * asociado al documento requerido y si lo encuentra lo devuelve. En 
         * otro caso develbe null.
         * @param doc
         * @return el JComponent asociado a doc
         */
        public JComponent getMainComponent(Document doc);

        public void unregisterMainComponent(DocumentsContainer container, Document document);
        
        public void registerMainComponent(DocumentsContainer container, Document document, JComponent component);
        
	/**
	 * Return the windows properties asociated to the document.
	 * @param doc
	 * @return
	 */
	public IWindow getPropertiesWindow(Document doc) ;
	
	/**
	 * Return true if the name exists to another document.
	 *
	 * @param project
	 * @param documentName
	 *
	 * @return True if the name exists.
	 * @deprecated use instead  project.getDocument
	 */
	public boolean existName(Project project, String documentName);
	
	/**
	 * Return the class that implement the main window for this type of
	 * documents.
	 * 
	 * @return the class of main window 
	 */
    public Class<? extends IDocumentWindow> getMainWindowClass();

}