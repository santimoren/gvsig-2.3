
package org.gvsig.app.project.documents.view.gui;

import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;


public class GeneralViewPropertiesPageFactory implements PropertiesPageFactory {

    public boolean isVisible(Object obj) {
        // Always if enabled this page
        return true;
    }

    public PropertiesPage create(Object obj) {
        return new GeneralViewPropertiesPage((ViewDocument)obj);
    }

    public String getGroupID() {
        return ViewDocument.VIEW_PROPERTIES_PAGE_GROUP;
    }
    
}
