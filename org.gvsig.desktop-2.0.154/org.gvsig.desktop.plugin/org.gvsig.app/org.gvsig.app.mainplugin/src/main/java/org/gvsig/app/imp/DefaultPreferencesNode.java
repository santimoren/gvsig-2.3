/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.imp;

import java.awt.Color;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.PreferencesNode;
import org.gvsig.utils.StringUtilities;
import org.gvsig.utils.XMLEntity;

public class DefaultPreferencesNode implements PreferencesNode {

	private String name;
	private XMLEntity node;
	
	public DefaultPreferencesNode() {
		this.name = null;
	}
	
	public DefaultPreferencesNode(String nodeName) {
		this.name = nodeName;
	}
	
	protected XMLEntity getRootNode() {
		XMLEntity node = PluginServices.getPluginServices("org.gvsig.app")	.getPersistentXML();
		return node;
	}
	
	protected XMLEntity getNode() {
		if( this.node == null ) {
			if( this.name == null ) {
				this.node = this.getRootNode();
			} else {
		        this.node = this.getRootNode().firstChild(this.name);		
			}
		}
        return this.node;
	}
	
	public String name() {
		return this.name;
	}

	public String[] keys() throws Exception {
		XMLEntity node = this.getNode();
		int count = node.getChildrenCount();
		String[] keys = new String[count];
		for(int  i=0 ; i<count ; i++ ) {
			keys[i] = node.getChild(i).getName();
		}
		return keys;
	}

	public void clear() throws Exception {
		XMLEntity node = this.getNode();
		node.removeAllChildren();
	}

	public void flush() throws Exception {
		// do nothing
	}

	public String get(String name, String defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		String value = node.getStringProperty(name);
		return value;
	}

	public boolean getBoolean(String name, boolean defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		boolean value = node.getBooleanProperty(name);
		return value;
	}

	public byte[] getByteArray(String name, byte[] defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		byte[] value = node.getByteArrayProperty(name);
		return value;
	}

	public double getDouble(String name, double defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		double value = node.getDoubleProperty(name);
		return value;
	}

	public float getFloat(String name, float defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		float value = node.getFloatProperty(name);
		return value;
	}

	public int getInt(String name, int defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		int value = node.getIntProperty(name);
		return value;
	}

	public long getLong(String name, long defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		long value = node.getLongProperty(name);
		return value;
	}

	public Color getColor(String name, Color defaultValue) {
		XMLEntity node = this.getNode();
		
		if (!node.contains(name)) {
			return defaultValue;
		}
		String value = node.getStringProperty(name);
		return  StringUtilities.string2Color(value);
	}

	public void put(String name, String value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putBoolean(String name, boolean value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putByteArray(String name, byte[] value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putDouble(String name, double value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putFloat(String name, float value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putInt(String name, int value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void putLong(String name, long value) {
		XMLEntity node = this.getNode();
		
		node.putProperty(name, value);
	}

	public void remove(String name) {
		XMLEntity node = this.getNode();

		if (!node.contains(name)) {
			return;
		}
		node.remove(name);
	}

	public void sync() throws Exception {
		// do nothing
	}

}
