/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.extension.Version;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.exceptions.SaveException;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.ProjectWindow;
import org.gvsig.app.project.documents.view.DefaultViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesHelper;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.observer.ObservableHelper;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentContext;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.utils.StringUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que representa un proyecto de gvSIG
 *
 */
public class DefaultProject implements Serializable, PropertyChangeListener,
        Project {

    private Logger LOG = LoggerFactory.getLogger(DefaultProject.class);
    /**
     * @deprecated see ApplicationLocator.getManager().getVersion()
     */
    public static String VERSION = Version.format();

    private ExtendedPropertiesHelper propertiesHelper = new ExtendedPropertiesHelper();

    private ObservableHelper observableHelper = new ObservableHelper();

    /**
     *
     */
    private static final long serialVersionUID = -4449622027521773178L;

    private static final Logger logger = LoggerFactory.getLogger(Project.class);

    private static ProjectPreferences preferences = new ProjectPreferences();

    /**
     * Index used by the generator of unique names of documents.
     */
    private Map<String, Integer> nextDocumentIndexByType = new HashMap<String, Integer>();

    private PropertyChangeSupport change;

    private boolean modified = false;

    private String name = null;

    private String creationDate = null;

    private String modificationDate = null;

    private String owner = null;

    private String comments = null;

    private Color selectionColor = null;

    private List<Document> documents = null;

    private List<ProjectExtent> extents = null;

    private IProjection projection;

    private File fname = null;

    private Set<String> unloadedObjects;

    /**
     * Creates a new Project object.
     */
    DefaultProject() {
        this.change = new PropertyChangeSupport(this);
        this.clean();
    }

    protected void clean() {
        this.owner = "";
        this.comments = "";
        this.name = PluginServices.getText(this, "untitled");
        this.creationDate = DateFormat.getDateInstance().format(new Date());
        this.modificationDate = this.creationDate;

        this.documents = new ArrayList<Document>();
        this.extents = new ArrayList<ProjectExtent>();

        this.setSelectionColor(getPreferences().getDefaultSelectionColor());

        this.projection = null; // se inicializa en el getProjection()
    }

    public static ProjectPreferences getPreferences() {
        return preferences;
    }

    public void propertyChange(PropertyChangeEvent evt) {
        change.firePropertyChange(evt);
    }

    public synchronized void addPropertyChangeListener(
            PropertyChangeListener arg0) {
        change.addPropertyChangeListener(arg0);
    }

    /**
     * Return the creation date of the project
     *
     * @return
     */
    public String getCreationDate() {
        return creationDate;
    }

    protected void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
        change.firePropertyChange("setCreationDate", null, null);
    }

    public Document createDocument(String type) {
        logger.info("createDocument('{}')", type);
        return ProjectManager.getInstance().createDocument(type);
    }

    /**
     * Return the name of the project
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of he project.
     *
     * @param string
     */
    public void setName(String name) {
        this.name = name;
        change.firePropertyChange("setName", null, null);
    }

    /**
     * Return the comments associateds with the project
     *
     * @return comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * Set the comments associateds with the project
     *
     * @param comments as string
     */
    public void setComments(String string) {
        comments = string;
        change.firePropertyChange("setComments", null, null);
    }

    /**
     * Retuen the modification date of the project.
     *
     * @return modification date as string
     */
    public String getModificationDate() {
        return modificationDate;
    }

    protected void setModificationDate(String string) {
        modificationDate = string;
        change.firePropertyChange("setModificationDate", null, null);
    }

    /**
     * Return the author of the project,
     *
     * @return author as string
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the author of the project
     *
     * @param author name as string
     */
    public void setOwner(String owner) {
        this.owner = owner;
        change.firePropertyChange("setOwner", null, null);
    }

    /**
     * Obtiene el color de selecci�n que se usar� en el proyecto
     *
     * @return
     */
    public Color getSelectionColor() {
        if (selectionColor == null) {
            selectionColor = getPreferences().getDefaultSelectionColor();
        }
        return selectionColor;
    }

    /**
     * Sets the selecction color
     *
     * @param selection color as string
     */
    public void setSelectionColor(String selectionColor) {
        this.setSelectionColor(StringUtilities.string2Color(selectionColor));
    }

    /**
     * Sets the selecction color
     *
     * @param selection color as Color
     */
    public void setSelectionColor(Color selectionColor) {
        this.selectionColor = selectionColor;
        MapContext.setSelectionColor(selectionColor);
        change.firePropertyChange("selectionColor", null, selectionColor);
    }

    public IProjection getProjection() {
        if (projection == null) {
            projection = getPreferences().getDefaultProjection();
        }
        return projection;
    }

    public void setProjection(IProjection projection) {
        this.projection = projection;
    }

    /**
     * Sets the modified state of project.
     *
     * Can't set to not modified.
     *
     * @param modified as boolean
     */
    public void setModified(boolean modified) {
        this.modified = modified;
        if (modified == false) {
            List<Document> documents = this.getDocuments();
            for (int i = 0; i < documents.size(); i++) {
                documents.get(i).setModified(false);
            }
        }
    }

    public boolean hasChanged() {
		// we return true if the project is not empty (until we have a better
        // method...)
        if ((this.getDocuments().size() != 0) || modified) {
            return true;
        }
        return false;
    }

    /**
     * Return a list of documents in the project.
     *
     * @return documents as List of IProjectDocument
     */
    public List<Document> getDocuments() {
        return Collections.unmodifiableList(documents);
    }

    /**
     * Return a list with all documents of especified type.
     *
     * @param type of document
     *
     * @return List of IProjectDocument
     */
    public List<Document> getDocuments(String type) {
        List<Document> docs = new ArrayList<Document>();
        if (type != null) {
            for (Document document : this.documents) {
                if (type.equalsIgnoreCase(document.getTypeName())) {
                    docs.add(document);
                }
            }
        }
        return Collections.unmodifiableList(docs);
    }

    /**
     * Adds a document to the project
     *
     * @param document as Document
     */
    public void add(Document document) {
        this.addDocument(document);
    }

    public void addDocument(Document document) {
        logger.info("add('{}')", document.toString());

        if (notifyObservers(ProjectNotification.BEFORE_ADD_DOCUMENT, document).isProcessCanceled()) {
            return;
        }
        document.addPropertyChangeListener(this);
        document.setProject(this);
        document.setName(this.getUniqueNameForDocument(document.getTypeName(),
                document.getName()));
        documents.add(document);
        document.afterAdd();
        this.setModified(true);
        notifyObservers(ProjectNotification.AFTER_ADD_DOCUMENT, document);
        change.firePropertyChange("addDocument", null, document);
    }

    public void remove(Document doc) {
        this.removeDocument(doc);
    }

    public void removeDocument(Document doc) {
        logger.info("remove('{}')", doc.toString());
        if (notifyObservers(ProjectNotification.BEFORE_REMOVE_DOCUMENT, doc).isProcessCanceled()) {
            return;
        }
        documents.remove(doc);
        this.setModified(true);
        change.firePropertyChange("delDocument", doc, null);
        doc.afterRemove();
        notifyObservers(ProjectNotification.AFTER_REMOVE_DOCUMENT, doc);
    }

    public Iterator<Document> iterator() {
        return documents.iterator();
    }

    public boolean isEmpty() {
        return documents.isEmpty();
    }

    /**
     * Return the view that contains the especified layer.
     *
     * @param layer
     *
     * @return name of the view that contains the layer
     *
     * @throws RuntimeException Si la capa que se pasa como par�metro no se
     * encuentra en ninguna vista
     */
    public String getViewName(FLayer layer) {
        List<Document> views = getDocuments(ViewManager.TYPENAME);
        for (int v = 0; v < views.size(); v++) {
            DefaultViewDocument pView = (DefaultViewDocument) views.get(v);
            FLayers layers = pView.getMapContext().getLayers();
            if (isView(layers, layer)) {
                return pView.getName();
            }
        }

        throw new RuntimeException(MessageFormat.format(
                "The layer '{1}' is not in a view", layer.getName()));
    }

    private boolean isView(FLayers layers, FLayer layer) {
        for (int i = 0; i < layers.getLayersCount(); i++) {
            if (layers.getLayer(i) instanceof FLayers) {
                if (isView((FLayers) layers.getLayer(i), layer)) {
                    return true;
                }
            }
            if (layers.getLayer(i) == layer) {
                return true;
            }
        }
        return false;
    }

    public void addExtent(ProjectExtent arg1) {
        extents.add(arg1);
        change.firePropertyChange("addExtent", null, null);
    }

    public ProjectExtent removeExtent(int arg0) {
        change.firePropertyChange("delExtent", null, null);
        return extents.remove(arg0);
    }

    public ProjectExtent[] getExtents() {
        return (ProjectExtent[]) extents.toArray(new ProjectExtent[0]);
    }

    /**
     * Obtiene un documento a partir de su nombre y el nombre de registro en el
     * pointExtension, este �ltimo se puede obtener del
     * Project****Factory.registerName.
     *
     * @param name Nombre del documento
     * @param type nombre de registro en el extensionPoint
     *
     * @return Documento
     */
    public Document getDocument(String name, String type) {
        if (type == null) {
            for (int i = 0; i < documents.size(); i++) {
                Document document = documents.get(i);
                if ( name.equalsIgnoreCase(document.getName())) {
                    return document;
                }
            }
        } else {
            for (int i = 0; i < documents.size(); i++) {
                Document document = documents.get(i);
                if (type.equalsIgnoreCase(document.getTypeName())
                        && name.equalsIgnoreCase(document.getName())) {
                    return document;
                }
            }
        }
        return null;
    }

    public Document getDocument(String name) {
        return this.getDocument(name, null);
    }

    public String getUniqueNameForDocument(String type, String name) {
        Document document = getDocument(name, type);
        if (document == null) {
            return name;
        }

        String newName = null;
        int num = this.getNextDocumentIndex(type);
        while (document != null) {
            newName = name + " - " + num++;
            document = getDocument(newName, type);
        }
        this.setNextDocumentIndex(type, num);
        return newName;
    }

    private int getNextDocumentIndex(String type) {
        if (nextDocumentIndexByType.get(type) == null) {
            nextDocumentIndexByType.put(type, new Integer(1));
            return 1;
        }
        return nextDocumentIndexByType.get(type).intValue();
    }

    private void setNextDocumentIndex(String type, int newIndex) {
        if (nextDocumentIndexByType.get(type) == null) {
            nextDocumentIndexByType.put(type, new Integer(newIndex));
        } else {
            nextDocumentIndexByType.put(type, new Integer(newIndex));
        }
    }

    public void saveState(File out) throws PersistenceException {
        FileOutputStream fout;
        if (notifyObservers(ProjectNotification.BEFORE_SAVE_TO_FILE, out).isProcessCanceled()) {
            return;
        }
        try {
            fout = new FileOutputStream(out);
            saveState(fout, new File(out.getParent()));
        } catch (FileNotFoundException e) {
            throw new PersistenceException(e);
        }
        this.fname = out;
        notifyObservers(ProjectNotification.AFTER_SAVE_TO_FILE, out);
    }

    public File getFile() {
        return this.fname;
    }

    public void saveState(OutputStream out) throws PersistenceException {
        saveState(out, null);
    }

    public void saveState(OutputStream out, File rootFolder)
            throws PersistenceException {
        if (notifyObservers(ProjectNotification.BEFORE_SAVE_TO_STREAM).isProcessCanceled()) {
            return;
        }
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        PersistentState state = null;
        state = manager.getState(this, true);
        try {
            if (rootFolder != null) {
                state.relativizeFiles(rootFolder);
            }
        } catch (Exception ex) {
            state.getContext().addError(ex);
        }
        manager.saveState(state, out);
        if (state.getContext().getErrors() != null) {
            throw state.getContext().getErrors();
        }
        this.fname = null;
        notifyObservers(ProjectNotification.AFTER_SAVE_TO_STREAM);
    }

    public void loadState(InputStream in) {
        loadState(in, null);
    }

    public void loadState(InputStream in, File rootFolder) {
        if (notifyObservers(ProjectNotification.BEFORE_LOAD_FROM_STREAM).isProcessCanceled()) {
            return;
        }
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        try {
            PersistentState state = manager.loadState(in);
            try {
                if (rootFolder != null) {
                    state.derelativizeFiles(rootFolder);
                }
            } catch (Exception ex) {
                state.getContext().addError(ex);
            }
            this.loadFromState(state);
            this.unloadedObjects = getUnloadedObjects(state.getContext());
        } catch (PersistenceException e) {
            LOG.info("Can't load project to stream", e);
        }
        this.fname = null;
        notifyObservers(ProjectNotification.AFTER_LOAD_FROM_STREAM);

    }



    /**
     * @param context
     * @return
     */
    private Set<String> getUnloadedObjects(PersistentContext context) {
        Set unloadedObjects = new HashSet();

        Iterator statesIterator = context.iterator();
        String className = null;

        while (statesIterator.hasNext()) {
            PersistentState aState = (PersistentState) statesIterator.next();
            try {
                className = aState.getTheClassName();
                DynStruct definition = aState.getDefinition();
                if (definition == null) {
                    unloadedObjects.add(className);
                }
            } catch (Throwable e) {
                // do nothing
            }
        }

        if(unloadedObjects.isEmpty()){
            return null;
        }

        return unloadedObjects;
    }

    public Set<String> getUnloadedObjects() {
        return this.unloadedObjects;
    }

    public void loadState(File in) {
        if (notifyObservers(ProjectNotification.BEFORE_LOAD_FROM_FILE, in).isProcessCanceled()) {
            return;
        }
        FileInputStream fin;
        try {
            fin = new FileInputStream(in);
            loadState(fin, new File(in.getParent()));
        } catch (FileNotFoundException e) {
            LOG.info("Can't load project to stream", e);
        }
        this.fname = in;
        notifyObservers(ProjectNotification.AFTER_LOAD_FROM_FILE, in);
    }

    @SuppressWarnings("unchecked")
    public void loadFromState(PersistentState state)
            throws PersistenceException {
        this.clean();

        this.setComments(state.getString("comments"));
        this.setCreationDate(state.getString("creationDate"));
        this.setModificationDate(state.getString("modificationDate"));
        this.setName(state.getString("name"));
        this.setOwner(state.getString("owner"));
        this.setSelectionColor((Color) state.get("selectionColor"));
        this.setProjection((IProjection) state.get("projection"));

        this.propertiesHelper = (ExtendedPropertiesHelper) state.get("propertiesHelper");

        List<ProjectExtent> extents = (List<ProjectExtent>) state
                .get("extents");
        for (int i = 0; i < extents.size(); i++) {
            this.addExtent(extents.get(i));
        }

        List<AbstractDocument> documents = (List<AbstractDocument>) state
                .get("documents");
        for (int i = 0; i < documents.size(); i++) {
            this.add(documents.get(i));
        }

        List<DocumentWindowInfo> persistentWindows = (List<DocumentWindowInfo>) state.get("documentWindowsInformation");

        for (int i = 0; i < persistentWindows.size(); i++) {
            DocumentWindowInfo persistentWindow = persistentWindows.get(i);
            String docName = persistentWindow.getDocumentName();
            String docType = persistentWindow.getDocumentType();
            Document doc = this.getDocument(docName, docType);
            IWindow win = doc.getFactory().getMainWindow(doc);
            if(win!=null){
                win.getWindowInfo().setWindowInfo(persistentWindow.getWindowInfo());
                PluginServices.getMDIManager().addWindow(win);
            }
        }

        if (state.hasValue("projectWindowInfo")) {
            WindowInfo projectWindowInfo = (WindowInfo) state.get("projectWindowInfo");
            ProjectExtension pe = (ProjectExtension) PluginServices.getExtension(org.gvsig.app.extension.ProjectExtension.class);
            pe.setProject(this);
            pe.showProjectWindow(projectWindowInfo);
        }

    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("version", VERSION);
        state.set("comments", getComments());
        state.set("creationDate", this.getCreationDate());

        state.set("modificationDate", this.getModificationDate());
        state.set("name", this.getName());
        state.set("owner", this.getOwner());
        state.set("selectionColor", this.getSelectionColor());

        state.set("projection", this.getProjection());

        state.set("extents", this.extents);
        List<Document> docs = this.getDocuments();
        List<Document> noTempDocs = new ArrayList<Document>();
        for (Iterator iterator = docs.iterator(); iterator.hasNext();) {
            Document document = (Document) iterator.next();
            if(!document.isTemporary()){
                noTempDocs.add(document);
            }
        }
        state.set("documents", noTempDocs);

        state.set("propertiesHelper",propertiesHelper);

        List<DocumentWindowInfo> persistentWindows = new ArrayList<DocumentWindowInfo>();
        MDIManager mdiMan = PluginServices.getMDIManager();
        IWindow[] windows = mdiMan.getOrderedWindows();
        for (int i = windows.length - 1; i >= 0; i--) {
            IWindow window = windows[i];
            if (window instanceof IDocumentWindow) {
                WindowInfo wi = mdiMan.getWindowInfo(window);
                DocumentWindowInfo dwi = new DocumentWindowInfo(
                        wi,
                        ((IDocumentWindow) window).getDocument().getTypeName(),
                        ((IDocumentWindow) window).getDocument().getName());
                persistentWindows.add(dwi);
            } else if (window instanceof ProjectWindow) {
                state.set("projectWindowInfo", mdiMan.getWindowInfo(window));
            }
        }
        state.set("documentWindowsInformation", persistentWindows);

    }

    public Object getProperty(Object key) {
        return this.propertiesHelper.getProperty(key);
    }

    public void setProperty(Object key, Object obj) {
        this.propertiesHelper.setProperty(key, obj);
    }

    public Map getExtendedProperties() {
        return this.propertiesHelper.getExtendedProperties();
    }

    public static class DocumentWindowInfo implements Persistent {

        public static final String PERSISTENCE_DEFINITION_NAME = "DocumentWindowInfo";

        private WindowInfo windowInfo;
        private String documentType;
        private String documentName;

        public DocumentWindowInfo() {
        }

        DocumentWindowInfo(WindowInfo wi, String docType, String docName) {
            windowInfo = wi;
            documentType = docType;
            documentName = docName;
        }

        public WindowInfo getWindowInfo() {
            return windowInfo;
        }

        public String getDocumentType() {
            return documentType;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void saveToState(PersistentState state)
                throws PersistenceException {
            state.set("windowInfo", this.windowInfo);
            state.set("documentType", this.documentType);
            state.set("documentName", this.documentName);
        }

        public void loadFromState(PersistentState state)
                throws PersistenceException {
            this.windowInfo = (WindowInfo) state.get("windowInfo");
            this.documentType = state.getString("documentType");
            this.documentName = state.getString("documentName");
        }

        public static void registerPersistent() {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            DynStruct definition = manager.getDefinition(PERSISTENCE_DEFINITION_NAME);
            if (definition == null) {
                definition = manager.addDefinition(
                        DocumentWindowInfo.class,
                        PERSISTENCE_DEFINITION_NAME,
                        "DocumentWindowInfo persistence definition",
                        null,
                        null
                );
                definition.addDynFieldObject("windowInfo").setMandatory(true).setClassOfValue(WindowInfo.class);
                definition.addDynFieldString("documentType").setMandatory(true);
                definition.addDynFieldString("documentName").setMandatory(true);
            }

        }
    }

    public static void registerPersistent() {
        AbstractDocument.registerPersistent();
        DocumentWindowInfo.registerPersistent();
        ProjectExtent.registerPersistent();

        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynStruct definition = manager.addDefinition(DefaultProject.class,
                "Project", "Project Persistence definition", null, null);
        definition.addDynFieldString("version").setMandatory(true);
        definition.addDynFieldString("comments").setMandatory(true);
        definition.addDynFieldString("creationDate").setMandatory(true);
        definition.addDynFieldString("modificationDate").setMandatory(true);
        definition.addDynFieldString("name").setMandatory(true);
        definition.addDynFieldString("owner").setMandatory(true);

        definition.addDynFieldObject("selectionColor")
                .setClassOfValue(Color.class).setMandatory(true);
        definition.addDynFieldObject("projection")
                .setClassOfValue(IProjection.class).setMandatory(true);

        definition.addDynFieldList("extents")
                .setClassOfItems(ProjectExtent.class).setMandatory(true);

        definition.addDynFieldList("documents").setClassOfItems(Document.class)
                .setMandatory(true);

        definition.addDynFieldObject("projectWindowInfo").setClassOfValue(WindowInfo.class).setMandatory(false);

        definition.addDynFieldList("documentWindowsInformation").setClassOfItems(WindowInfo.class).setMandatory(false);


        definition.addDynFieldObject("propertiesHelper").setClassOfValue(ExtendedPropertiesHelper.class)
                        .setMandatory(false);

    }

    /**
     * @deprecated use getPreferences().setDefaultSelectionColor()
     */
    public static void setDefaultSelectionColor(Color color) {
        getPreferences().setDefaultSelectionColor(color);
    }

    /**
     * @deprecated use getPreferences().getDefaultSelectionColor()
     */
    public static Color getDefaultSelectionColor() {
        return getPreferences().getDefaultSelectionColor();
    }

    /**
     * @deprecated use getPreferences().getDefaultMapUnits()
     */
    public static int getDefaultMapUnits() {
        return getPreferences().getDefaultMapUnits();
    }

    /**
     * @deprecated use getPreferences().getDefaultDistanceUnits()
     */
    public static int getDefaultDistanceUnits() {
        return getPreferences().getDefaultDistanceUnits();
    }

    /**
     * @deprecated use getPreferences().getDefaultDistanceArea()
     */
    public static int getDefaultDistanceArea() {
        return getPreferences().getDefaultDistanceArea();
    }

    /**
     * @deprecated use getPreferences().setDefaultMapUnits()
     */
    public static void setDefaultMapUnits(int mapUnits) {
        getPreferences().setDefaultMapUnits(mapUnits);
    }

    /**
     * @deprecated use getPreferences().setDefaultDistanceUnits()
     */
    public static void setDefaultDistanceUnits(int distanceUnits) {
        getPreferences().setDefaultDistanceUnits(distanceUnits);
    }

    /**
     * @deprecated use getPreferences().setDefaultDistanceArea()
     */
    public static void setDefaultDistanceArea(int distanceArea) {
        getPreferences().setDefaultDistanceArea(distanceArea);
    }

    /**
     * @deprecated use getPreferences().setDefaultProjection()
     */
    public static void setDefaultProjection(IProjection defaultProjection) {
        getPreferences().setDefaultProjection(defaultProjection);
    }

    /**
     * @deprecated use getPreferences().getDefaultProjection()
     */
    public static IProjection getDefaultProjection() {
        return getPreferences().getDefaultProjection();
    }

    /**
     * @deprecated see {@link #setSelectionColor(String)}, to be remove in gvSIG
     * 2.1.0
     */
    public void setColor(String color) {
        this.setSelectionColor(StringUtilities.string2Color(color));
    }

    /**
     * Return the selection color
     *
     * @return selection color as string
     * @deprecated use {@link #getSelectionColor()}
     */
    public String getColor() {
        return StringUtilities.color2String(selectionColor);
    }

    /**
     * Return the list of views of the project
     *
     * @return views as ArrayList of ProjectDocument
     *
     * @deprecated see {@link #getDocumentsByType(String)}
     */
    public List<Document> getViews() {
        return getDocuments(ViewManager.TYPENAME);
    }

    /**
     * Add a view to the project
     *
     * @deprecated see {@link #add(AbstractDocument)}
     */
    public void addView(DefaultViewDocument v) {
        add(v);
    }

    /**
     * Remove a view of the project
     *
     * @param index of the view as integer
     *
     * @deprecated see {@link #remove(AbstractDocument)}
     */
    public void delView(int i) {
        List<Document> list = getDocuments(ViewManager.TYPENAME);
        remove(list.get(i));
    }

    /**
     * @deprecated see {@link #getDocument(String, String)}
     */
    public Document getProjectDocumentByName(String name, String type) {
        return this.getDocument(name, type);
    }

    /**
     * @deprecated see {@link #getDocuments(String)}
     */
    public List<Document> getDocumentsByType(String type) {
        return this.getDocuments(type);
    }

    /**
     * @deprecated aun por decidir que API darle al copy/paste
     */
    public String exportToXML(AbstractDocument[] selectedItems)
            throws SaveException {
        // FIXME jjdc:hay que decirdir que API darle al copy/paste
        throw new UnsupportedOperationException("This method is not supported");
    }

    /**
     * @deprecated aun por decidir que API darle al copy/paste
     */
    public void importFromXML(String sourceString, String docType) {
        // FIXME jjdc:hay que decirdir que API darle al copy/paste
        throw new UnsupportedOperationException("This method is not supported");
    }

    /**
     * @deprecated aun por decidir que API darle al copy/paste
     */
    public boolean isValidXMLForImport(String sourceString, String docType) {
        // FIXME jjdc:hay que decirdir que API darle al copy/paste
        throw new UnsupportedOperationException("This method is not supported");
    }

    public boolean canImportDocuments(String data, String doctype) {
        // TODO Auto-generated method stub
        return false;
    }

    public String exportDocumentsAsText(List<Document> documents) {
        // TODO Auto-generated method stub
        return null;
    }

    public void importDocuments(String data, String doctype) {
        // TODO Auto-generated method stub

    }

    public Document getActiveDocument() {
        return this.getActiveDocument((Class<? extends Document>)null);
    }

    public Document getActiveDocument(String documentTypeName) {
        ApplicationManager application = ApplicationLocator.getManager();

        Document document = null;
        IWindow[] windows = application.getUIManager().getOrderedWindows();
        IWindow window = null;
        for (int i = 0; i < windows.length; i++) {
            window = windows[i];
            if (window instanceof SingletonWindow) {
		// Cogemos no la primera ventana, si no la primera
                // ventana de tipo documento (SingletonWindow).
                // Y por si las mosca no es un documento, atrapamos
                // los errores y continuamos si no puede hacer un cast
                // del Model a Document
                try {
                    document = (Document) ((SingletonWindow) window).getWindowModel();
                    if (documentTypeName == null) {
                        return document;
                    }
                    if( document.getTypeName().equalsIgnoreCase(documentTypeName) ) {
                        return document;
                    }
                    if( document instanceof DocumentsContainer ) {
                        Document subdoc = ((DocumentsContainer)document).getActiveDocument(documentTypeName);
                        return subdoc;
                    }

                } catch (ClassCastException e) {
                    // Do nothing, skip this window
                }
            }
        }
        return null;
    }

    public Document getActiveDocument(Class<? extends Document> documentClass) {
        ApplicationManager application = ApplicationLocator.getManager();

        Document document = null;
        IWindow[] windows = application.getUIManager().getOrderedWindows();
        IWindow window = null;
        for (int i = 0; i < windows.length; i++) {
            window = windows[i];
            if (window instanceof SingletonWindow && window instanceof IDocumentWindow) {
		// Cogemos no la primera ventana, si no la primera
                // ventana de tipo documento (SingletonWindow).
                // Y por si las mosca no es un documento, atrapamos
                // los errores y continuamos si no puede hacer un cast
                // del Model a Document
                try {
                    document = (Document) ((SingletonWindow) window).getWindowModel();
                    if (documentClass == null) {
                        return document;
                    }
                    if (documentClass.isAssignableFrom(document.getClass())) {
                        return document;
                    }
                    if( document instanceof DocumentsContainer ) {
                        Document subdoc = ((DocumentsContainer)document).getActiveDocument(documentClass);
                        return subdoc;
                    }

                } catch (ClassCastException e) {
                    // Do nothing, skip this window
                }
            }
        }
        return null;
    }

    public void addObserver(Observer o) {
        observableHelper.addObserver(o);
    }

    public void deleteObserver(Observer o) {
        observableHelper.deleteObserver(o);
    }

    public void deleteObservers() {
        observableHelper.deleteObservers();
    }

//	private void notifyObservers(ProjectNotifycation notifycation) {
//		observableHelper.notifyObservers(this, notifycation);
//	}
    private ProjectNotification notifyObservers(int type) {
        DefaultProjectNotification notifycation
                = new DefaultProjectNotification(type, null, null);
        try {
            observableHelper.notifyObservers(this, notifycation);
        } catch (Exception ex) {
            LOG.info("Can't notify observers", ex);
        }
        return notifycation;
    }

    private ProjectNotification notifyObservers(int type, Document document) {
        DefaultProjectNotification notifycation
                = new DefaultProjectNotification(type, document, null);
        try {
            observableHelper.notifyObservers(this, notifycation);
        } catch (Exception ex) {
            LOG.info("Can't notify observers", ex);
        }
        return notifycation;
    }

    private ProjectNotification notifyObservers(int type, File file) {
        DefaultProjectNotification notifycation
                = new DefaultProjectNotification(type, null, file);
        try {
            observableHelper.notifyObservers(this, notifycation);
        } catch (Exception ex) {
            LOG.info("Can't notify observers", ex);
        }
        return notifycation;
    }

    class DefaultProjectNotification implements ProjectNotification {

        private int type;
        private Document document;
        private File file;
        private boolean processCanceled = false;

        DefaultProjectNotification(int type, Document document, File file) {
            this.type = type;
            this.document = document;
            this.file = file;
        }

        public int getNotificationType() {
            return type;
        }

        public Document getDocument() {
            return document;
        }

        public void cancelProcess() {
            processCanceled = true;
        }

        public boolean isProcessCanceled() {
            return processCanceled;
        }

        public File getFile() {
            return file;
        }

    }

}
