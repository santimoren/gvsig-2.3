/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
*
* $Id: CreateSpatialIndexMonitorableTask.java 29596 2009-06-29 16:02:00Z jpiera $
* $Log$
* Revision 1.4  2007-07-30 12:56:04  jaume
* organize imports, java 5 code downgraded to 1.4 and added PictureFillSymbol
*
* Revision 1.3  2007/05/15 07:22:56  cesar
* Add the finished method for execution from Event Dispatch Thread
*
* Revision 1.2  2007/03/06 16:37:08  caballero
* Exceptions
*
* Revision 1.1  2006/09/15 10:41:30  caballero
* extensibilidad de documentos
*
* Revision 1.2  2006/05/22 10:35:41  fjp
* Monitorable tasks easy
*
* Revision 1.1  2006/05/08 15:41:06  azabala
* added rtree spatial indexing capabilities
*
*
*/
package org.gvsig.app.project.documents.view.legend;

import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.utils.swing.threads.CancellableMonitorable;
import org.gvsig.utils.swing.threads.DefaultCancellableMonitorable;
import org.gvsig.utils.swing.threads.IMonitorableTask;


public class CreateSpatialIndexMonitorableTask implements IMonitorableTask{

	String MAIN_MESSAGE;
	String HULL_MESSAGE = PluginServices.getText(this, "Indexando");
	String OF = "de";

	FLyrVect layer;
	/**
	 * It monitors advance of indexing process
	 */
	private CancellableMonitorable cancelMonitor = null;
	/**
	 * flag to tell if the process is finished
	 */
	private boolean finished = false;

	/**
	 * Default constructor
	 * @throws DriverIOException
	 * @throws DriverException
	 */
	public CreateSpatialIndexMonitorableTask(FLyrVect layer)
			throws DataException {
		this.layer = layer;
		MAIN_MESSAGE = PluginServices.getText(this, "Indexando_espacialmente") +
								layer.getName();
//		cancelMonitor = createCancelMonitor();
	}

	/**
	 * Creates a CancellableMonitorable instance to monitor
	 * progress and to cancel the process.
	 * @return
	 * @throws DriverIOException
	 * @throws DriverException
	 */
//	private CancellableMonitorable createCancelMonitor()
//				throws ReadException {
//		DefaultCancellableMonitorable monitor =
//			new DefaultCancellableMonitorable();
//		monitor.setInitialStep(0);
//		monitor.setDeterminatedProcess(true);
//		int numSteps = ((FLyrVect)layer).getFeatureStore().getDataCollection().size();
//		monitor.setFinalStep(numSteps);
//		return monitor;
//	}

	public void run() throws Exception {
		FeatureStore fs=(layer).getFeatureStore();
		//TODO comentado para que compile
//		fs.createIndex(fs.getDefaultFeatureType());
		finished = true;
	}

	public int getInitialStep() {
		return cancelMonitor.getInitialStep();
	}

	public int getFinishStep() {
		return cancelMonitor.getFinalStep();
	}

	public int getCurrentStep() {
		return cancelMonitor.getCurrentStep();
	}

	public String getStatusMessage() {
		return MAIN_MESSAGE;
	}

	public String getNote() {
		return HULL_MESSAGE + " " +
		getCurrentStep()+ " " +
		OF  + " "+
		getFinishStep();
	}
	//FIXME Borrar los ficheros de indice en caso de cancelacion
	public void cancel() {
		((DefaultCancellableMonitorable) cancelMonitor).setCanceled(true);
	}
	/**
	 * Tells if it is a defined process (we know its number of steps)
	 */
	public boolean isDefined() {
		return true;
	}
	public boolean isCanceled() {
		return cancelMonitor.isCanceled();
	}

	public boolean isFinished() {
		return finished;
	}

	/* (non-Javadoc)
	 * @see com.iver.utiles.swing.threads.IMonitorableTask#finished()
	 */
	public void finished() {
		// TODO Auto-generated method stub

	}
}//CreateSpatialIndexMonitorableTask


