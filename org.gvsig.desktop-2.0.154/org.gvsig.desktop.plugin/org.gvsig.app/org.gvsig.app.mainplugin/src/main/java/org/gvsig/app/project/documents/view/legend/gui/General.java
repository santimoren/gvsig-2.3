/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLyrDefault;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.tools.locator.LocatorException;

/**
 * @deprecated This tab is outdated and not used. It has been replaced by "GeneralLayerPropertiesPage"
 * @see GeneralLayerPropertiesPage

 */
public class General extends AbstractThemeManagerPage {

    private static final long serialVersionUID = 1L;
    private FLayer layer;
    private NumberFormat nf = NumberFormat.getInstance();
    private JPanel pnlLayerName = null;
    private GridBagLayoutPanel pnlScale = null;
    private JPanel pnlProperties = null;
    private JLabel lblLayerName = null;
    private JTextField txtLayerName = null;
    private JTextField txtMaxScale = null;
    private JTextArea propertiesTextArea = null;
    private JRadioButton rdBtnShowAlways = null;
    private JRadioButton rdBtnDoNotShow = null;
    private JTextField txtMinScale = null;
    private JScrollPane scrlProperties;

    /**
     * This is the default constructor.
     */
    public General() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     *
     * @return void
     */
    private void initialize() {
        this.setLayout(new BorderLayout());
        GridBagLayoutPanel aux = new GridBagLayoutPanel();
        aux.addComponent(getPnlLayerName());
        aux.addComponent(new JBlank(10, 10));
        aux.addComponent("", getPnlScale());
        JPanel aux2 = new JPanel(new GridLayout(1, 2));
        aux2.add(getPnlProperties());
        aux.addComponent(aux2);

        aux.setPreferredSize(getPreferredSize());
        this.add(aux, BorderLayout.CENTER);
        this.add(new JBlank(5, 10), BorderLayout.WEST);
        this.add(new JBlank(5, 10), BorderLayout.EAST);

    }

    @Override
    public int getPriority() {
        return 1000;
    }

    /**
     * Sets the necessary properties in the panel. This properties are extracted
     * from the layer. With this properties fills the TextFields, ComboBoxes and
     * the rest of GUI components.
     *
     * @param FLayer layer,
     */
    public void setModel(FLayer layer) {
        this.layer = layer;

        if (layer.getMinScale() != -1) {
            getTxtMaxScale().setText(nf.format(layer.getMinScale()));
        }
        if (layer.getMaxScale() != -1) {
            getTxtMinScale().setText(nf.format(layer.getMaxScale()));
        }
        if (layer.getMinScale() == -1 && layer.getMaxScale() == -1) {
            getRdBtnShowAlways().setSelected(true);
            txtMaxScale.setEnabled(false);
            txtMinScale.setEnabled(false);

        } else {
            getRdBtnDoNotShowWhen().setSelected(true);
            txtMaxScale.setEnabled(true);
            txtMinScale.setEnabled(true);
        }
        txtLayerName.setText(layer.getName());
        showLayerInfo();

    }

    /**
     * This method initializes jPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlLayerName() {
        if (pnlLayerName == null) {
            lblLayerName = new JLabel();
            pnlLayerName = new JPanel();
            lblLayerName.setText(PluginServices.getText(this, "Nombre") + ":");
            lblLayerName.setComponentOrientation(ComponentOrientation.UNKNOWN);
            pnlLayerName.setComponentOrientation(ComponentOrientation.UNKNOWN);
            pnlLayerName.add(lblLayerName, null);
            pnlLayerName.add(getTxtLayerName(), null);
        }
        return pnlLayerName;
    }

    /**
     * This method initializes jPanel1
     *
     * @return javax.swing.JPanel
     */
    private GridBagLayoutPanel getPnlScale() {
        if (pnlScale == null) {
            pnlScale = new GridBagLayoutPanel();
            pnlScale.setBorder(BorderFactory.createTitledBorder(
                    BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                    PluginServices.getText(this, "rango_de_escalas"),
                    TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null)
            );
            ButtonGroup buttonGroup = new ButtonGroup();
            buttonGroup.add(getRdBtnShowAlways());
            buttonGroup.add(getRdBtnDoNotShowWhen());
            pnlScale.addComponent(getRdBtnShowAlways());
            pnlScale.addComponent(getRdBtnDoNotShowWhen());
            JPanel aux;

            aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
            aux.add(getTxtMaxScale());
            aux.add(new JLabel("(" + PluginServices.getText(this, "escala_maxima") + ")"));

            GridBagLayoutPanel aux2;
            aux2 = new GridBagLayoutPanel();
            aux2.addComponent(PluginServices.getText(
                    this, "este_por_encima_de") + " 1:",
                    aux);
            aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
            aux.add(getTxtMinScale());
            aux.add(new JLabel("(" + PluginServices.getText(this, "escala_minima") + ")"));

            aux2.addComponent(PluginServices.getText(
                    this, "este_por_debajo_de_") + " 1:",
                    aux);

            pnlScale.addComponent(new JBlank(20, 1), aux2);

            pnlScale.addComponent(new JBlank(20, 1), aux2);

        }
        return pnlScale;
    }

    /**
     * This method initializes jPanel2, this contains the ScrollPane with the
     * properies.
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlProperties() {
        if (pnlProperties == null) {
            pnlProperties = new JPanel();
            pnlProperties.setBorder(BorderFactory.createTitledBorder(
                    BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                    PluginServices.getText(this, "propiedades"),
                    TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null)
            );
            pnlProperties.add(getScrlProperties(), null);
        }
        return pnlProperties;
    }

    /**
     * This method initializes jTextField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTxtLayerName() {
        if (txtLayerName == null) {
            txtLayerName = new JTextField(25);
            txtLayerName.setEditable(false);
        }
        return txtLayerName;
    }

    /**
     * This method initializes TxtMaxScale
     *
     * @return jTextField1
     */
    private JTextField getTxtMaxScale() {
        if (txtMaxScale == null) {
            txtMaxScale = new JTextField(15);
            txtMaxScale.setEnabled(false);
        }
        return txtMaxScale;
    }

    /**
     * This method initilizes TxtArea, in this TextArea sets the text with the
     * properties of the layer
     *
     * @return
     */
    private JTextArea getPropertiesTextArea() {
        if (propertiesTextArea == null) {
            propertiesTextArea = new JTextArea();
            propertiesTextArea.setEditable(false);
            propertiesTextArea.setBackground(SystemColor.control);

        }
        return propertiesTextArea;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getScrlProperties() {
        if (scrlProperties == null) {
            scrlProperties = new JScrollPane();
            scrlProperties.setViewportView(getPropertiesTextArea());
            scrlProperties.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            scrlProperties.setPreferredSize(new Dimension(350, 180));
        }
        return scrlProperties;
    }

    /**
     * This method initializes jRadioButton
     *
     * @return javax.swing.JRadioButton
     */
    private JRadioButton getRdBtnShowAlways() {
        if (rdBtnShowAlways == null) {
            rdBtnShowAlways = new JRadioButton();
            rdBtnShowAlways.setText(PluginServices.getText(this, "Mostrar_siempre"));
            rdBtnShowAlways.setSelected(true);
            rdBtnShowAlways.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    txtMaxScale.setEnabled(false);
                    txtMinScale.setEnabled(false);
                }
            });
        }
        return rdBtnShowAlways;
    }

    /**
     * This method initializes jRadioButton1
     *
     * @return javax.swing.JRadioButton
     */
    private JRadioButton getRdBtnDoNotShowWhen() {
        if (rdBtnDoNotShow == null) {
            rdBtnDoNotShow = new JRadioButton();
            rdBtnDoNotShow.setText(PluginServices.getText(this, "No_mostrar_la_capa_cuando_la_escala"));
            rdBtnDoNotShow.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    txtMaxScale.setEnabled(true);
                    txtMinScale.setEnabled(true);
                }
            });
        }
        return rdBtnDoNotShow;
    }

    /**
     * This method initializes jTextField2
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTxtMinScale() {
        if (txtMinScale == null) {
            txtMinScale = new JTextField(15);
            txtMinScale.setEnabled(false);
        }
        return txtMinScale;
    }

    private String getLayerName() {
        return txtLayerName.getText().toString();
    }

    /**
     * Add the information of the layer to the textArea
     */
    private void showLayerInfo() {
        try {
            String info = ((FLyrDefault) layer).getInfoString();
            if (info == null) {
                StringBuffer buff = new StringBuffer();
                Envelope fullExtentViewPort = layer.getFullEnvelope();
                IProjection viewPortProj = layer.getMapContext().getProjection();
                buff.append(PluginServices.getText(this, "Extent"));
                buff.append(" ");
                buff.append(viewPortProj.getAbrev());
                buff.append(" (" + PluginServices.getText(this, "view_projection") + "):\n\t");
                buff.append(PluginServices.getText(this, "Superior") + ":\t" + fullExtentViewPort.getMaximum(1) + "\n\t");
                buff.append(PluginServices.getText(this, "Inferior") + ":\t" + fullExtentViewPort.getMinimum(1) + "\n\t");
                buff.append(PluginServices.getText(this, "Izquierda") + ":\t" + fullExtentViewPort.getMinimum(0) + "\n\t");
                buff.append(PluginServices.getText(this, "Derecha") + ":\t" + fullExtentViewPort.getMaximum(0) + "\n");
                // show layer native projection
                if (layer.getProjection() != null
                        && !layer.getProjection().getAbrev().equals(
                                viewPortProj.getAbrev())) {
                    IProjection nativeLayerProj = layer.getProjection();
                    ICoordTrans ct = viewPortProj.getCT(nativeLayerProj);
                    Envelope nativeLayerExtent = fullExtentViewPort.convert(ct);
                    buff.append(PluginServices.getText(this, "Extent") + " ");
                    buff.append(nativeLayerProj.getAbrev());
                    buff.append(" (" + PluginServices.getText(this, "layer_native_projection") + "):\n\t");
                    buff.append(PluginServices.getText(this, "Superior")
                            + ":\t" + nativeLayerExtent.getMaximum(1) + "\n\t");
                    buff.append(PluginServices.getText(this, "Inferior")
                            + ":\t" + nativeLayerExtent.getMinimum(1) + "\n\t");
                    buff.append(PluginServices.getText(this, "Izquierda")
                            + ":\t" + nativeLayerExtent.getMinimum(0) + "\n\t");
                    buff.append(PluginServices.getText(this, "Derecha") + ":\t"
                            + nativeLayerExtent.getMaximum(0) + "\n");

                }
                if (layer instanceof FLyrVect) {
                    FeatureStore fStore = ((FLyrVect) layer).getFeatureStore();

                    buff.append(PluginServices.getText(this, "Origen_de_datos") + ": ");
                    buff.append(fStore.getName());
                    info = buff.toString();

                    DataStoreParameters parameters = fStore.getParameters();
                    if (parameters instanceof FilesystemStoreParameters) {
                        info = info + "\n"
                                + PluginServices.getText(this, "fichero")
                                + ": "
                                + ((FilesystemStoreParameters) parameters)
                                .getFile();
                    } else {
                        info = info + "\n" + fStore.getName() + "\n";
                    }
                    String sGeomType = "Unknow";

                    try {
                        GeometryType geomType = ((FLyrVect) layer).getTypeVectorLayer();
                        sGeomType = geomType.getName();
                    } catch (LocatorException e) {
                        NotificationManager.addError(e);
                    } catch (GeometryTypeNotSupportedException e) {
                        NotificationManager.showMessageWarning("Not supported GeometryType", e);
                    } catch (GeometryTypeNotValidException e) {
                        NotificationManager.showMessageWarning("Not valid GeometryType", e);
                    }

                    info += "\n" + PluginServices.getText(this, "type") + ": " + sGeomType + "\n";

                } else {
                    info = buff.toString();
                    info = info + PluginServices.getText(this, "Origen_de_datos") + ": " + layer.getName();
                }

            }
            getPropertiesTextArea().setText(info);

        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        }

    }

    /**
     * Returns true or false if the scale is activa
     */
    private boolean isScaleActive() {
        return getRdBtnDoNotShowWhen().isSelected();
    }

    public void acceptAction() {

    }

    public void cancelAction() {
        // does nothing
    }

    /**
     * When we press the apply button, sets the new properties of the layer thar
     * the user modified using the UI components
     */
    public void applyAction() {
        if (isScaleActive()) {
            try {
                layer.setMinScale((nf.parse(getTxtMaxScale().getText())).doubleValue());
            } catch (ParseException ex) {
                if (getTxtMaxScale().getText().compareTo("") == 0) {
                    layer.setMinScale(-1);
                } else {
                    System.err.print(ex.getLocalizedMessage());
                }
            }

            try {
                layer.setMaxScale((nf.parse(getTxtMinScale().getText())).doubleValue());
            } catch (ParseException ex) {
                if (getTxtMinScale().getText().compareTo("") == 0) {
                    layer.setMaxScale(-1);
                } else {
                    System.err.print(ex.getLocalizedMessage());
                }
            }

        } else {
            layer.setMinScale(-1);
            layer.setMaxScale(-1);
        }

        if (!getLayerName().equals(layer.getName())) {
            layer.setName(getLayerName());
        }

    }

    /*
     *  (non-Javadoc)
     * @see java.awt.Component#getName()
     */
    public String getName() {
        return PluginServices.getText(this, "General");
    }
}
