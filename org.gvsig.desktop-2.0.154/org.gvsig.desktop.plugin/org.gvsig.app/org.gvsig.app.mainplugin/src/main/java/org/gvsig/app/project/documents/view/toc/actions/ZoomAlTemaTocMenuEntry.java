/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZoomAlTemaTocMenuEntry extends AbstractTocContextMenuAction {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(ZoomAlTemaTocMenuEntry.class);
	
	public String getGroup() {
		return "group2"; //FIXME
	}

	public int getGroupOrder() {
		return 20;
	}

	public int getOrder() {
		return 1;
	}

	public String getText() {
		return PluginServices.getText(this, "Zoom_a_la_capa");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
	    
	    if (isVisible(item, selectedItems)) {
	        
	        for (int i=0; i<selectedItems.length; i++) {
	            if (!selectedItems[i].isAvailable()) {
	                return false;
	            }
	        }
	        return true;
	        
	    } else {
	        return false;
	    }
		
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item) && !(selectedItems == null || selectedItems.length <= 0)) {
			return true;
		}
		return false;

	}

    public void execute(ITocItem ite, FLayer[] selectedItems) {

        if (selectedItems.length == 1) {
            try {
                if (!selectedItems[0].isAvailable()) {
                    return;
                }
                getMapContext().zoomToEnvelope(selectedItems[0].getFullEnvelope());
            } catch (ReadException e1) {
                e1.printStackTrace();
            }
        } else {
            try {
                Envelope maxExtent = setMaxExtent(selectedItems);
                getMapContext().zoomToEnvelope(maxExtent);
            } catch (ReadException e1) {
                e1.printStackTrace();
            }
        }
        Project project = ((ProjectExtension) PluginServices.getExtension(ProjectExtension.class)).getProject();
        project.setModified(true);
    }

    private Envelope setMaxExtent(FLayer[] actives)
            throws ReadException {

        Envelope extRef = null;
        try {
            extRef = geomManager.createEnvelope(Geometry.SUBTYPES.GEOM2D);
        } catch (Exception ex) {
            logger.warn("Can't create new envelope", ex);
            return null;
        }
        for (int i = 0; i < actives.length; i++) {
            if (actives[i].isAvailable()) {
                Envelope extVar = actives[i].getFullEnvelope();
                try {
                    extRef.add(extVar);
                } catch (Exception e) {
                    logger.warn("Error creating the envelope", e);
                }
            }
        }
        return extRef;
    }

    
}
