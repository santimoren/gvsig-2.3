/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.gvsig.about.AboutLocator;
import org.gvsig.about.AboutManager;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.installer.lib.api.PackageInfo;

/**
 * Extensión que abre una nueva ventana mostrando la información sobre el gvSIG.
 *
 */
public class About extends Extension {

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /**
     * @see com.iver.mdiApp.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        return true;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    }

    private URL getResource(String name) {
        URL resource = this.getClass().getClassLoader().getResource(name);
        return resource;
    }

    public void postInitialize() {
        super.postInitialize();

        PluginsManager pmanager = PluginsLocator.getManager();

        ApplicationManager application = ApplicationLocator.getManager();

        PackageInfo pinfo = pmanager.getPackageInfo(About.class);

        AboutManager about = application.getAbout();

        about.setProject("gvSIG desktop", getResource("about/about.htm"),
                getResource("about/gvsig-icon16x16.png"));
        about.getProject().set("version", pinfo.getVersion().toString());
        about.getProject().set("state", pinfo.getState());
        about.getProject().set("java.version",
                System.getProperty("java.version"));

        about.addSponsor(
                "CITMA GVA", //"Conselleria d'infraestructures i transport de la Generalitat Valenciana",
                getResource("about/gva-cit.html"),
                1
        );
        about.addDeveloper(
                "PRODEVELOP",
                getResource("about/prodevelop.html"),
                2
        );

        about.addDeveloper(
                "Software Colaborativo",
                getResource("about/scolab.html"),
                3
        );

        about.addDeveloper(
                "DISID",
                getResource("about/disid.html"),
                4
        );
    }

    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();

        application.getUIManager().addCentredWindow(new AboutWindow());
    }

    class AboutWindow extends JPanel implements IWindow {

        /**
         *
         */
        private static final long serialVersionUID = -3577229971045886621L;

        AboutWindow() {
            AboutManager aboutManager = AboutLocator.getManager();

            this.setLayout(new BorderLayout());
            this.add(aboutManager.getAboutPanel(), BorderLayout.CENTER);
            this.add(new JPanelButtons(), BorderLayout.SOUTH);
        }

        public WindowInfo getWindowInfo() {
            WindowInfo winfo = new WindowInfo(
                    WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE
            );
            winfo.setTitle(PluginServices.getText(this, "acerca_de"));
            winfo.setHeight(500);
            winfo.setWidth(750);
            return winfo;
        }

        public Object getWindowProfile() {
            return WindowInfo.DIALOG_PROFILE;
        }

        public void closeWindow() {
            PluginServices.getMDIManager().closeWindow(this);
        }

        class JPanelButtons extends JPanel {

            /**
             *
             */
            private static final long serialVersionUID = 1529755877776747074L;
            JButton close = null;
            JButton installedPlugins = null;

            JPanelButtons() {
                this.setLayout(new FlowLayout(FlowLayout.RIGHT));
                this.close = getCloseButton();
//				this.installedPlugins = getInstalledPluginsButton();
                this.add(this.close);
//				this.add(this.installedPlugins);
            }

            private JButton getCloseButton() {
                JButton button = new JButton(
                        PluginServices.getText(this, "Close"));
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        closeWindow();
                    }
                });
                return button;
            }

            private JButton getInstalledPluginsButton() {
                JButton button = new JButton(PluginServices.getText(this,
                        "Installed plugins"));
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        // TODO: Need implementation
                    }
                });
                return button;
            }

        }
    }
}
