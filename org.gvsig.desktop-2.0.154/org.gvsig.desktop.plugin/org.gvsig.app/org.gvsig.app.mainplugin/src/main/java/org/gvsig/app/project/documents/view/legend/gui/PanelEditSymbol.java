/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;



/**
 * Panel creado para editar el s�mbolo seleccionado.
 *
 * @author Vicente Caballero Navarro
 */
public class PanelEditSymbol extends JPanel implements IWindow {
	private SingleSymbol panel = null;
	private JButton bAceptar = null;  //  @jve:decl-index=0:visual-constraint="198,310"
	private JPanel panelbutton = null;
	private boolean ok=false;
	//private FSymbol symbol;


	public PanelEditSymbol(){
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        this.setLayout(new BorderLayout());
        this.setSize(320, 160);
        this.add(getPanel(), java.awt.BorderLayout.NORTH);
		this.add(getPanelbutton(),BorderLayout.SOUTH);
	}
	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo = new WindowInfo(WindowInfo.MODALDIALOG);
		m_viewinfo.setTitle(PluginServices.getText(this, "simbolo"));
		m_viewinfo.setWidth(getWidth()+10);
		m_viewinfo.setHeight(getHeight());
		return m_viewinfo;
	}

	/**
	 * This method initializes panel
	 *
	 * @return javax.swing.JPanel
	 */
	private SingleSymbol getPanel() {
		if (panel == null) {
			panel = new SingleSymbol();
			panel.setPreferredSize(new Dimension(320, 160));
			panel.setVisible(true);
		}
		return panel;
	}

	/**
	 * This method initializes bAceptar
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBAceptar() {
	  I18nManager i18nManager = ToolsLocator.getI18nManager();
		if (bAceptar == null) {
			bAceptar = new JButton();
			bAceptar.setPreferredSize(new java.awt.Dimension(80,20));
			bAceptar.setText(i18nManager.getTranslation("Aceptar"));
			bAceptar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ok=true;
					PluginServices.getMDIManager().closeWindow(PanelEditSymbol.this);
				}
			});
			bAceptar.setSize(35, 20);
		}
		return bAceptar;
	}

	public ISymbol getSymbol(){
		return panel.getSymbol();
	}

	public void setSymbol(ISymbol s){
		ok=false;
		panel.setSymbol(s);
	}

	public boolean isOK(){
		return ok;
	}
	/**
	 * This method initializes panelbutton
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPanelbutton() {
		if (panelbutton == null) {
			panelbutton = new JPanel();
			panelbutton.add(getBAceptar(), null);
		}
		return panelbutton;
	}

	public void setShapeType(int shapeType) {
		getPanel().setShapeType(shapeType);
	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
