/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui;

import org.gvsig.fmap.mapcontext.layers.FLayer;

/**
 * @author fjp
 *
 * Interfaz que se usa por las nuevas capas que necesiten definir
 * una capa que todav�a no existe en gvSIG.
 * Para usarlo, en una extensi�n aparter, se define un JPanel
 * que implemente este interfaz, y luego se a�ade al Wizard 
 * correspondiente.
 * Por ejemplo, cuando se crea una nueva capa, existe un Wizard
 * del tipo JWizardComponents en la clase NewTheme de extCAD
 * (o extEdition, como quiera que se llame al final)
 */
public interface ILayerPanel {
	/**
	 * Creates a FLyrWFS.
	 * 
	 * @return the layer
	 */
	public FLayer getLayer();
}