/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.utils;


import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.jfree.ui.StandardDialog;

/**
 * A dialog for choosing a font from the available system fonts.
 * Allows hiding size and/or style (bold, etc)
 *
 * @author jldominguez
 */
public class PartialFontChooserDialog extends StandardDialog {

    /** The panel within the dialog that contains the font selection controls. */
    private PartialFontChooserPanel fontChooserPanel;
    
    private boolean allowStyle = true;
    private boolean allowSize = true;

    /**
     * Standard constructor - builds a font chooser dialog owned by another dialog.
     *
     * @param owner  the dialog that 'owns' this dialog.
     * @param title  the title for the dialog.
     * @param modal  a boolean that indicates whether or not the dialog is modal.
     * @param font  the initial font displayed.
     */
    public PartialFontChooserDialog(
        final Dialog owner,
        final String title,
        final boolean modal,
        final Font font,
        boolean style,
        boolean size) {
        
        super(owner, title, modal);
        allowStyle = style;
        allowSize = size;
        setContentPane(createContent(font));
    }

    /**
     * Standard constructor - builds a font chooser dialog owned by a frame.
     *
     * @param owner  the frame that 'owns' this dialog.
     * @param title  the title for the dialog.
     * @param modal  a boolean that indicates whether or not the dialog is modal.
     * @param font  the initial font displayed.
     */
    public PartialFontChooserDialog(
        final Frame owner,
        final String title,
        final boolean modal,
        final Font font,
        boolean style,
        boolean size) {
        
        super(owner, title, modal);
        allowStyle = style;
        allowSize = size;
        setContentPane(createContent(font));
    }
    

    /**
     * Returns the selected font.
     *
     * @return the font.
     */
    public Font getSelectedFont() {
        return this.fontChooserPanel.getSelectedFont();
    }

    /**
     * Returns the panel that is the user interface.
     *
     * @param font  the font.
     *
     * @return the panel.
     */
    private JPanel createContent(Font font) {
        final JPanel content = new JPanel(new BorderLayout());
        content.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        if (font == null) {
            font = new Font("Dialog", 10, Font.PLAIN);
        }
        this.fontChooserPanel =
            new PartialFontChooserPanel(font, allowStyle, allowSize);
        content.add(this.fontChooserPanel);

        final JPanel buttons = createButtonPanel();
        buttons.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));
        content.add(buttons, BorderLayout.SOUTH);

        return content;
    }

}