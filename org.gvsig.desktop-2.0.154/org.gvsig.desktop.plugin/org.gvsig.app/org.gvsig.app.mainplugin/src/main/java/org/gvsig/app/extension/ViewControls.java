/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.gui.preferencespage.ViewPage;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.Encuadrator;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.ExtentListSelectorModel;
import org.gvsig.app.project.documents.view.gui.FPanelExtentSelector;
import org.gvsig.app.project.documents.view.gui.FPanelLocConfig;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.app.project.documents.view.toc.gui.FPopupMenu;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;

/**
 * Extensi�n que controla las operaciones b�sicas realizadas sobre la vista.
 *
 */
public class ViewControls extends Extension implements IPreferenceExtension {

    private static final Logger logger = LoggerFactory
            .getLogger(ViewInvertSelection.class);

    private static ViewPage viewPropertiesPage = null;

    public void execute(String s) {
        execute(s, null);
    }

    public void execute(String command, Object[] args) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();

        MapContext mapa = document.getMapContext();
        MapControl mapCtrl = view.getMapControl();

        if (command.equalsIgnoreCase("view-navigation-zoom-all")) {
            Envelope all = mapa.getLayers().getFullEnvelope();
            if( all!=null && !all.isEmpty() ) {
                mapa.getViewPort().setEnvelope(all);
                document.setModified(true);
            }

        } else if (command.equalsIgnoreCase("view-navigation-frame-manager")) {
            FPanelExtentSelector l = new FPanelExtentSelector();

            ProjectExtension p = (ProjectExtension) PluginServices.getExtension(org.gvsig.app.extension.ProjectExtension.class);
            Project project = p.getProject();
            ExtentListSelectorModel modelo = new ExtentListSelectorModel(project);
            project.addPropertyChangeListener(modelo);
            l.setModel(modelo);
            l.addSelectionListener(new Encuadrator(project, mapa, view));
            document.setModified(true);
            PluginServices.getMDIManager().addWindow(l);

        } else if (command.equalsIgnoreCase("view-locator-setup")) {
            FPanelLocConfig m_panelLoc = new FPanelLocConfig(view.getMapOverview());
            PluginServices.getMDIManager().addCentredWindow(m_panelLoc);
            document.setModified(true);

        } else if (command.equalsIgnoreCase("view-navigation-pan")) {
            mapCtrl.setTool("pan");
            document.setModified(true);

        } else if (command.equalsIgnoreCase("view-navigation-zoom-in-topoint")) {
            mapCtrl.setTool("zoomIn");
            document.setModified(true);

        } else if (command.equalsIgnoreCase("view-navigation-zoom-out-topoint")) {
            mapCtrl.setTool("zoomOut");
            document.setModified(true);

        } else if (command.equalsIgnoreCase("layer-set-visible")) {
            setVisibles(true, mapa.getLayers());
            document.setModified(true);
            application.refreshMenusAndToolBars();

        } else if (command.equalsIgnoreCase("layer-set-hide")) {
            setVisibles(false, mapa.getLayers());
            document.setModified(true);
            application.refreshMenusAndToolBars();

        } else if (command.equalsIgnoreCase("layer-set-active")) {
            setActives(true, mapa.getLayers());
            document.setModified(true);
            application.refreshMenusAndToolBars();

        } else if (command.equalsIgnoreCase("layer-set-inactive")) {
            setActives(false, mapa.getLayers());
            document.setModified(true);
            application.refreshMenusAndToolBars();

        } else if (command.equalsIgnoreCase("view-navigation-zoom-in-center")) {
            mapCtrl.zoomIn();
            document.setModified(true);

        } else if (command.equalsIgnoreCase("view-navigation-zoom-out-center")) {
            mapCtrl.zoomOut();
            document.setModified(true);

        } else if (command.equalsIgnoreCase("view-change-scale")) {
            try {
                Long scale = (Long) ToolsLocator.getDataTypesManager().coerce(DataTypes.LONG, args[0]);
                mapa.setScaleView(scale.longValue());
                document.setModified(true);
            } catch (Throwable ex) {
                logger.info("Can't change scale of view.", ex);
                application.message("Can't change scale of view.", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private ViewPage getViewPropertiesPage() {
        if (viewPropertiesPage == null) {
            viewPropertiesPage = new ViewPage();
        }
        return viewPropertiesPage;
    }

    /**
     * Pone todas las capas visibles o no visibles.
     *
     * @param visible true si que quieren poner a visibles.
     * @param mapa FMap sobre el que actuar.
     */
    private void setVisibles(boolean visible, FLayers layers) {
        int layerCount = layers.getLayersCount();
        for (int i = 0; i < layerCount; i++) {
            FLayer layer = layers.getLayer(i);
            layer.setVisible(visible);
            if (layer instanceof FLayers) {
                setVisibles(visible, (FLayers) layer);
            }
        }
    }

    /**
     * Pone todas las capas activas o no activas.
     *
     * @param active true si que quieren poner a activas.
     * @param mapa FMap sobre el que actuar.
     */
    private void setActives(boolean active, FLayers layers) {
        int layerCount = layers.getLayersCount();
        for (int i = 0; i < layerCount; i++) {
            FLayer layer = layers.getLayer(i);
            layer.setActive(active);
            if (layer instanceof FLayers) {
                setActives(active, (FLayers) layer);
            }
        }
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

    public void initialize() {
        registerIcons();
    }

    public void postInitialize() {
        FPopupMenu.registerExtensionPoint();
        ProjectManager projectManager = ApplicationLocator.getProjectManager();
        ViewManager viewManager = (ViewManager) projectManager.getDocumentManager(ViewManager.TYPENAME);
        viewManager.addTOCContextAction("view-layer-add");
    }

    private void registerIcons() {
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-in-topoint", this);
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-out-topoint", this);
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-all", this);
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-in-center", this);
        IconThemeHelper.registerIcon("action", "view-navigation-zoom-out-center", this);
        IconThemeHelper.registerIcon("action", "view-navigation-pan", this);
        IconThemeHelper.registerIcon("action", "view-navigation-frame-manager", this);

        IconThemeHelper.registerIcon("action", "edit-undo-redo-actions-modify", this);
        IconThemeHelper.registerIcon("action", "edit-undo-redo-actions-add", this);
        IconThemeHelper.registerIcon("action", "edit-undo-redo-actions-delete", this);

    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        FLayers layers = mapa.getLayers();
        for (int i = 0; i < layers.getLayersCount(); i++) {
            if (layers.getLayer(i).isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public IPreference[] getPreferencesPages() {
        IPreference[] preferences = new IPreference[1];
        preferences[0] = getViewPropertiesPage();
//        GridPage gridPage = new GridPage();
//        preferences[1] = gridPage;
        return preferences;
    }
}
