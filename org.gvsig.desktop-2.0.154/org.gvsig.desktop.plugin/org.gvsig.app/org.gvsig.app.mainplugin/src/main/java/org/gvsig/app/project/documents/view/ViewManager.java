/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;

import java.util.Map;

import javax.swing.ImageIcon;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.AbstractDocumentManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.actions.CopyDocumentAction;
import org.gvsig.app.project.documents.actions.CutDocumentAction;
import org.gvsig.app.project.documents.actions.PasteDocumentAction;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.app.project.documents.view.gui.ViewProperties;
import org.gvsig.app.project.documents.view.toc.AbstractActionInfoAdapterToTocContextMenuAction;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesHelper;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.extensionpoint.ExtensionBuilder;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.persistence.PersistenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory of View.
 *
 * @author 2005-         Vicente Caballero
 * @author 2009-         Joaquin del Cerro
 * 
 */
public class ViewManager extends AbstractDocumentManager {
    private static final Logger logger = LoggerFactory.getLogger(ViewManager.class);
    
    private static final String PERSISTENCE_VIEW_DOCUMENT_DEFINITION_NAME =
        "DefaultViewDocument";
    public static String TYPENAME = "project.document.view2d";
    private DynStruct persistenceDefinition;
    
    public ViewManager() {
    	// Do nothing
    }
    
    public int getPriority() {
        return 0;
    }

    public ImageIcon getIcon() {
		return PluginServices.getIconTheme().get("document-view-icon");
    }

    public ImageIcon getIconSelected() {
		return PluginServices.getIconTheme().get("document-view-icon-sel");
    }

    public String getTitle() {
        return PluginServices.getText(this, "Vista");
    }

    public String getTypeName() {
        return TYPENAME;
    }
    
    public Class getMainWindowClass() {
        return DefaultViewPanel.class;
    }
    
    public AbstractDocument createDocument() {
        AbstractDocument doc = new DefaultViewDocument(this);
        if( this.notifyObservers(NOTIFY_AFTER_CREATEDOCUMENT,doc).isCanceled() ) {
            return null;
        }
    	return doc;        
        
    }

    @Override
    public IWindow getMainWindow(Document doc, WindowLayout layout) {
        IDocumentWindow win = (IDocumentWindow) super.getMainWindow(doc, layout);
        if ( win == null ) {
            win = this.createDocumentWindow(doc);
            if ( layout != null && win != null ) {
                win.setWindowLayout(layout);
            }
        }
        if( this.notifyObservers(NOTIFY_AFTER_GETMAINWINDOW,win).isCanceled() ) {
            return null;
        }
        ((AbstractDocument) doc).raiseEventCreateWindow(win);
        return win;
    }

    @Override
    public IWindow getPropertiesWindow(Document doc) {
        IWindow win = super.getPropertiesWindow(doc);
        if( win == null ) {
            win = new ViewProperties((DefaultViewDocument) doc);
        }
        if( this.notifyObservers(NOTIFY_AFTER_GETPROPERTIESWINDOW,win).isCanceled() ) {
            return null;
        }
        return win;
    }

    public void addTOCContextAction(String theAction) {
    	ActionInfoManager actionManager = PluginsLocator.getActionInfoManager(); 
    	ActionInfo action = actionManager.getAction(theAction);
    	if( action==null ) {
    		String errmsg = "Action '"+theAction+"' not found";
    		logger.info(errmsg);
    		throw new RuntimeException(errmsg);
    	}
    	this.addTOCContextAction(action);
    }
    
    public void addTOCContextAction(String theAction, String group, int groupOrder, int order) {
    	ActionInfoManager actionManager = PluginsLocator.getActionInfoManager(); 
    	ActionInfo action = actionManager.getAction(theAction);
    	if( action==null ) {
    		String errmsg = "Action '"+theAction+"' not found";
    		logger.info(errmsg);
    		throw new RuntimeException(errmsg);
    	}
    	this.addTOCContextAction(
                action.getName(), 
                new ActionInfoAdapterToContextMenuAction(action, group, groupOrder,0)
        );
    }
    
    public void addTOCContextAction(ActionInfo action) {
    	this.addTOCContextAction(action.getName(), new ActionInfoAdapterToContextMenuAction(action, "default", 9000));
    }
    
    public void addTOCContextAction(ActionInfo action, String group, int groupOrder) {
    	this.addTOCContextAction(action.getName(), new ActionInfoAdapterToContextMenuAction(action, group, groupOrder));
    }
    
    class ActionInfoAdapterToContextMenuAction extends AbstractActionInfoAdapterToTocContextMenuAction {
    	
    	ActionInfoAdapterToContextMenuAction(ActionInfo action, String group, int groupOrder) {
            super(action, group, groupOrder);
    	}
    	ActionInfoAdapterToContextMenuAction(ActionInfo action, String group, int groupOrder, int order) {
            super(action, group, groupOrder,order);
    	}
    }
    
    /**
     * @deprecated use addTOCContextAction(ActionInfo action, String group, int groupOrder)
     * @param id
     * @param action 
     */
    public void addTOCContextAction(String id, IContextMenuAction action) {
    	initializeRegisterTOCActions();
    	ExtensionPoint exPoint = ToolsLocator.getExtensionPointManager().add(
    			"View_TocActions", "");
    	if( action instanceof ExtensionBuilder ) {
    		exPoint.append(id, "", (ExtensionBuilder)action);
    		return;
    	}
		exPoint.append(id, "", new ContextMenuActionAdapterToExtensionBuilder(action));
    }

    class ContextMenuActionAdapterToExtensionBuilder implements ExtensionBuilder {
    	IContextMenuAction menuAction = null;
    	ContextMenuActionAdapterToExtensionBuilder(IContextMenuAction menuAction) {
    		this.menuAction = menuAction;
    	}
		public Object create() {
			return this.menuAction;
		}
		public Object create(Object[] args) {
			return this.menuAction;
		}
		public Object create(Map args) {
			return this.menuAction;
		}
    }
    
    private static void initializeRegisterTOCActions() {
		ExtensionPointManager epManager = ToolsLocator.getExtensionPointManager();

		if (!epManager.has("View_TocActions")) {
			epManager.add(
					"View_TocActions",
					"Context menu options of the TOC " +
						" in the view window "+
						"(register instances of " +
						"org.gvsig.app.gui.toc.AbstractTocContextMenuAction)"
			);
		}
    }
    
    /**
     * Registers in the points of extension the Factory with alias.
     *
     */
    public static void register() {
        ViewManager factory = new ViewManager();
    	ProjectManager.getInstance().registerDocumentFactory(factory);
    	
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        manager.registerFactory(factory);

        initializeRegisterTOCActions();
        
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new CopyDocumentAction());
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new CutDocumentAction());
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new PasteDocumentAction());

	   	IconThemeHelper.registerIcon("document", "document-view-icon", ViewManager.class);
	   	IconThemeHelper.registerIcon("document", "document-view-icon-sel", ViewManager.class);
	   	
	   	IconThemeHelper.registerIcon("cursor", "cursor-crux", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-info-by-point", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-pan", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-query-area", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-select-by-point", ViewManager.class);
        IconThemeHelper.registerIcon("cursor", "cursor-select-by-polygon", ViewManager.class);
        IconThemeHelper.registerIcon("cursor", "cursor-select-by-rectangle", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-zoom-in", ViewManager.class);
	   	IconThemeHelper.registerIcon("cursor", "cursor-zoom-out", ViewManager.class);

	   	IconThemeHelper.registerIcon("layer", "layer-icon", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-group", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-vectorial", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-dgn", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-dxf", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-postgresql", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-mysql", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-icon-jdbc", ViewManager.class);
	   	//IconThemeHelper.registerIcon("layer", "layer-icon-unavailable", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-chk-unavailable", ViewManager.class);
	   	IconThemeHelper.registerIcon("layer", "layer-chk-temporary", ViewManager.class);

	   	IconThemeHelper.registerIcon("legend", "legend-overview-single-symbol", ViewManager.class);
	   	IconThemeHelper.registerIcon("legend", "legend-overview-vectorial-interval", ViewManager.class);
	   	IconThemeHelper.registerIcon("legend", "legend-overview-vectorial-unique-value", ViewManager.class);

	   	MapContextManager mapContextMgr = MapContextLocator.getMapContextManager();
	   	mapContextMgr.registerIconLayer("DGN", "layer-icon-dgn");
	   	mapContextMgr.registerIconLayer("DXF", "layer-icon-dxf");
	   	mapContextMgr.registerIconLayer("jdbc", "layer-icon-jdbc");
	   	mapContextMgr.registerIconLayer("PostgreSQL", "layer-icon-postgresql");
	   	mapContextMgr.registerIconLayer("MySQL", "layer-icon-mysql");
	   	
        if (factory.persistenceDefinition == null){
            factory.persistenceDefinition = manager.addDefinition(
                ViewDocument.class,
                PERSISTENCE_VIEW_DOCUMENT_DEFINITION_NAME,
                "Default view document persistence definition",
                null, 
                null
            );
            factory.persistenceDefinition.extend(manager.getDefinition(AbstractDocument.PERSISTENCE_DEFINITION_NAME));

            factory.persistenceDefinition.addDynFieldBoolean("useMapOverview").setMandatory(true);
            factory.persistenceDefinition.addDynFieldObject("mainMapContext").setClassOfValue(MapContext.class).setMandatory(true);
            factory.persistenceDefinition.addDynFieldObject("overviewMapContext").setClassOfValue(MapContext.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldObject("propertiesHelper").setClassOfValue(ExtendedPropertiesHelper.class)
                .setMandatory(false);
        
        }


    }

    @SuppressWarnings("rawtypes")
    public DynStruct getDefinition(String className) {

        if( this.persistenceDefinition.getName().equalsIgnoreCase(className)) {
            return this.persistenceDefinition;
        }
        if( this.persistenceDefinition.getFullName().equalsIgnoreCase(className)) {
            return this.persistenceDefinition;
        }
        if( this.getDocumentClass().getName().equals(className) ) {
            return this.persistenceDefinition;
        }

        return null;
    }

    @SuppressWarnings("rawtypes")
    protected Class getDocumentClass() {
        return DefaultViewDocument.class;
    }

    public boolean manages(Object object) {
        return object instanceof ViewDocument;
    }
}
