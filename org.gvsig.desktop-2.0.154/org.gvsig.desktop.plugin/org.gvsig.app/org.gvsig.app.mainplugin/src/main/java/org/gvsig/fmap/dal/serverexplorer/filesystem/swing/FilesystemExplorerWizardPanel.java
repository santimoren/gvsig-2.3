/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemFileFilter;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.gui.beans.swing.JFileChooser;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jmvivo
 * 
 */
public abstract class FilesystemExplorerWizardPanel extends WizardPanel
    implements ActionListener, ListSelectionListener, Disposable {

    /**
	 *
	 */
    private static final long serialVersionUID = -3371957786521876903L;

    private static final Logger LOG = LoggerFactory
        .getLogger(FilesystemExplorerWizardPanel.class);

    public static final String OPEN_LAYER_FILE_CHOOSER_ID =
        "OPEN_LAYER_FILE_CHOOSER_ID";

    protected static final String ADD_COMMAND = "ADD";
    protected static final String EDIT_COMMAND = "PROPERTIES";
    protected static final String REMOVE_COMMAND = "REMOVE";
    protected static final String UP_COMMAND = "UP";
    protected static final String DOWN_COMMAND = "DOWN";

    private static String lastPath = null;
    private static MyFileFilter lastFilter = null;
    private static final String DEFAULT_FILTER = "All_supported";

    private JList fileList;
    private JScrollPane fileListScroll;
    private JPanel buttonsPanel;
    private JButton addButton;
    private JButton propertiesButton;
    private JButton removeButton;
    private JButton upButton;
    private JButton downButton;

    protected FilesystemServerExplorer explorer;
    private ArrayList<MyFileFilter> filters;
    
    public FilesystemExplorerWizardPanel() {
        super();
        this.addAncestorListener(new AncestorListener() {
            public void ancestorAdded(AncestorEvent ae) {
                initExplorer();
                initFilters();
            }
            public void ancestorRemoved(AncestorEvent ae) {
                dispose();
            }
            public void ancestorMoved(AncestorEvent ae) {
            }
        });
    }
    
    public void setTabName(String name) {
        super.setTabName(name);
    }

    public DataStoreParameters[] getParameters() {
        return ((FilesystemStoreListModel) getFileList().getModel())
            .getParameters();
    }

    public void initWizard() {
        setTabName(PluginServices.getText(this, "Fichero"));
        if (lastPath == null) {
            Preferences prefs = Preferences.userRoot().node("gvsig.foldering");
            lastPath = prefs.get("DataFolder", null);
        }
        initExplorer();
        initFilters();
        initUI();
    }

    private void initExplorer() {
        if (this.explorer == null) {
            DataManager dm = DALLocator.getDataManager();
            FilesystemServerExplorerParameters param;
            try {
                param
                        = (FilesystemServerExplorerParameters) dm
                        .createServerExplorerParameters(FilesystemServerExplorer.NAME);
                param.setInitialpath(lastPath);
                explorer
                        = (FilesystemServerExplorer) dm.openServerExplorer(
                                FilesystemServerExplorer.NAME, param);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void initFilters() {
        if (this.filters == null) {
            int mode = FilesystemServerExplorer.MODE_ALL;
            this.filters = new ArrayList<MyFileFilter>();
            if (this.getMapCtrl() == null) {
                mode = FilesystemServerExplorer.MODE_FEATURE;
            } else {
                mode
                        = FilesystemServerExplorer.MODE_GEOMETRY
                        | FilesystemServerExplorer.MODE_RASTER;
            }

            // First filter in list will be 'All formats' filter
            this.filters.add(new MyFileFilter(explorer.getFilter(mode,
                    Messages.get(DEFAULT_FILTER))));

            @SuppressWarnings("unchecked")
            Iterator<FilesystemFileFilter> iter = explorer.getFilters(mode);
            while (iter.hasNext()) {
                this.filters.add(new MyFileFilter(iter.next()));
            }
        }
    }

    private void initUI() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints constr = new GridBagConstraints();

        constr.gridwidth = GridBagConstraints.RELATIVE;
        constr.gridheight = GridBagConstraints.RELATIVE;
        constr.fill = GridBagConstraints.BOTH;
        constr.anchor = GridBagConstraints.FIRST_LINE_START;
        constr.weightx = 1;
        constr.weighty = 1;
        constr.ipadx = 3;
        constr.ipady = 3;

        this.add(getFileListScroll(), constr);

        constr.gridwidth = GridBagConstraints.REMAINDER;
        constr.gridheight = GridBagConstraints.RELATIVE;
        constr.fill = GridBagConstraints.NONE;
        constr.anchor = GridBagConstraints.FIRST_LINE_END;
        constr.weightx = 0;
        constr.weighty = 0;
        this.add(getButtonsPanel(), constr);

        this.updateButtons();
        this.updateContainingWizardAcceptButton();

    }

    protected class FilesystemStoreListModel extends AbstractListModel {

        private static final long serialVersionUID = -726119349962990665L;
        private ArrayList<FilesystemStoreParameters> theList;

        public FilesystemStoreListModel() {
            theList = new ArrayList<FilesystemStoreParameters>();
        }

        public DataStoreParameters[] getParameters() {
            return theList.toArray(new DataStoreParameters[0]);
        }

        public Object getElementAt(int index) {
            return theList.get(index).getFile().getName();
        }

        public FilesystemStoreParameters getStoreParameterAt(int index) {
            return theList.get(index);
        }

        public int getSize() {
            return theList.size();
        }

        public DynObject getDynObjectAt(int index) {
            return (DynObject) theList.get(index);
        }

        public void add(DynObject dynObject) {
            this.theList.add((FilesystemStoreParameters) dynObject);
            this.fireIntervalAdded(this, this.theList.size() - 1,
                this.theList.size() - 1);
        }

        public void addAll(List<FilesystemStoreParameters> toAdd) {
            int index0 = this.getSize() - 1;
            if (index0 < 0) {
                index0 = 0;
            }
            this.theList.addAll(toAdd);
            this.fireIntervalAdded(this, index0, this.theList.size() - 1);
        }

        public void remove(int i) {
            this.theList.remove(i);
            this.fireIntervalRemoved(this, i, i);

        }

        public void up(FilesystemStoreParameters item) {
            int curIndex = this.theList.indexOf(item);
            if (curIndex < 1) {
                return;
            }
            this.theList.remove(item);
            this.theList.add(curIndex - 1, item);
            this.fireContentsChanged(this, curIndex, curIndex - 1);
        }

        public void down(FilesystemStoreParameters item) {
            int curIndex = this.theList.indexOf(item);
            if (curIndex < 0) {
                return;
            } else
                if (curIndex == this.theList.size() - 1) {
                    return;
                }
            this.theList.remove(item);
            this.theList.add(curIndex + 1, item);
            this.fireContentsChanged(this, curIndex, curIndex + 1);
        }
        
        public void forceUpdate(int index) {
            this.fireContentsChanged(this, index, index);
        }

    }

    protected JList getFileList() {
        if (fileList == null) {
            fileList = new JList(new FilesystemStoreListModel());

            fileList.addListSelectionListener(this);
        }
        return fileList;
    }

    private JScrollPane getFileListScroll() {
        if (fileListScroll == null) {
            fileListScroll = new JScrollPane();
            fileListScroll.setViewportView(getFileList());
            fileListScroll
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            fileListScroll
                .setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        }
        return fileListScroll;
    }

    private Component getButtonsPanel() {
        if (buttonsPanel == null) {
            buttonsPanel = new JPanel();

            buttonsPanel.setLayout(new GridBagLayout());

            GridBagConstraints constr = new GridBagConstraints();

            constr.anchor = GridBagConstraints.CENTER;
            constr.fill = GridBagConstraints.NONE;
            constr.ipadx = 3;
            constr.ipady = 3;
            constr.insets = new Insets(3, 3, 3, 3);
            constr.gridwidth = GridBagConstraints.REMAINDER;

            buttonsPanel.add(getAddButton(), constr);
            buttonsPanel.add(getPropertiesButton(), constr);
            buttonsPanel.add(getRemoveButton(), constr);
            buttonsPanel.add(getUpButton(), constr);
            buttonsPanel.add(getDownButton(), constr);
            // buttonsPanel.add(new JLabel(), constrLbl);
        }
        return buttonsPanel;
    }

    private JButton getAddButton() {
        if (addButton == null) {
            addButton =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            addButton.setText(getLocalizedText("add"));
            addButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
            addButton.setActionCommand(ADD_COMMAND);
            addButton.addActionListener(this);
        }
        return addButton;
    }

    private JButton getPropertiesButton() {
        if (propertiesButton == null) {
            propertiesButton =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            propertiesButton.setText(getLocalizedText("properties"));
            propertiesButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
            propertiesButton.setActionCommand(EDIT_COMMAND);
            propertiesButton.addActionListener(this);
        }
        return propertiesButton;
    }

    private JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            removeButton.setText(getLocalizedText("remove"));
            removeButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
            removeButton.setActionCommand(REMOVE_COMMAND);
            removeButton.addActionListener(this);
        }
        return removeButton;
    }

    private JButton getUpButton() {
        if (upButton == null) {
            upButton =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            upButton.setText(getLocalizedText("up"));
            upButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
            upButton.setActionCommand(UP_COMMAND);
            upButton.addActionListener(this);
        }
        return upButton;
    }

    private JButton getDownButton() {
        if (downButton == null) {
            downButton =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            downButton.setText(getLocalizedText("down"));
            downButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
            downButton.setActionCommand(DOWN_COMMAND);
            downButton.addActionListener(this);
        }
        return downButton;
    }

    private String getLocalizedText(String txt) {
        try {
            return PluginServices.getText(this, txt);
        } catch (Exception e) {
            return txt;
        }
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        FilesystemStoreListModel model =
            (FilesystemStoreListModel) getFileList().getModel();

        if (command == ADD_COMMAND) {
            this.addFilesFromParameters(this.chooseFilesParameters());
        } else
            if (command == EDIT_COMMAND) {
                showPropertiesDialog(model.getDynObjectAt(getFileList().getSelectedIndex()));

            } else
                if (command == REMOVE_COMMAND) {
                    int[] selecteds = getFileList().getSelectedIndices();
                    for (int i = selecteds.length - 1; i > -1; i--) {
                        model.remove(selecteds[i]);
                    }
                    getFileList().setSelectedIndex(-1);
                    
                    this.updateContainingWizardAcceptButton();

                } else
                    if (command == UP_COMMAND) {
                        List<FilesystemStoreParameters> items =
                            new ArrayList<FilesystemStoreParameters>();
                        int[] selecteds = getFileList().getSelectedIndices();
                        if (selecteds.length == 0 || selecteds[0] == 0) {
                            return;
                        }
                        for (int i = 0; i < selecteds.length; i++) {
                            items.add(model.getStoreParameterAt(selecteds[i]));
                            selecteds[i]--;
                        }
                        Iterator<FilesystemStoreParameters> iter =
                            items.iterator();
                        while (iter.hasNext()) {
                            model.up(iter.next());
                        }
                        getFileList().setSelectedIndices(selecteds);

                    } else
                        if (command == DOWN_COMMAND) {
                            List<FilesystemStoreParameters> items =
                                new ArrayList<FilesystemStoreParameters>();
                            int[] selecteds =
                                getFileList().getSelectedIndices();
                            if (selecteds.length == 0
                                || selecteds[selecteds.length - 1] == model
                                    .getSize() - 1) {
                                return;
                            }
                            for (int i = selecteds.length - 1; i > -1; i--) {
                                items.add(model
                                    .getStoreParameterAt(selecteds[i]));
                                selecteds[i]++;
                            }
                            Iterator<FilesystemStoreParameters> iter =
                                items.iterator();
                            while (iter.hasNext()) {
                                model.down(iter.next());
                            }
                            getFileList().setSelectedIndices(selecteds);

                        } else {
                            throw new IllegalArgumentException(command);
                        }

    }

    protected void showPropertiesDialog(final DynObject parameters) {
        FilesystemExplorerPropertiesPanelManager manager = ApplicationLocator.getFilesystemExplorerPropertiesPanelManager();
        FilesystemExplorerPropertiesPanel panel = manager.createPanel(parameters);
        manager.showPropertiesDialog(parameters,panel);
        
        refreshFileList();
    }

    protected void refreshFileList() {
        // force repaint
        ListModel lm = this.getFileList().getModel();
        if (lm instanceof FilesystemStoreListModel) {
            FilesystemStoreListModel mm = (FilesystemStoreListModel) lm;
            mm.forceUpdate(getFileList().getSelectedIndex());
        }
    }
    
    public void addFiles(List<File> files) {
        File[] filesArray = files.toArray(new File[files.size()]);
        this.addFilesFromParameters(this.getParametersList(null, filesArray, null));
    }

    private void addFilesFromParameters(List<FilesystemStoreParameters> filesParams) {
        if ( filesParams.isEmpty() ) {
            return;
        }
        FilesystemStoreListModel model = (FilesystemStoreListModel) getFileList().getModel();
        model.addAll(filesParams);
        getFileList().setModel(model);

        this.updateContainingWizardAcceptButton();
        try {
            fileList.setSelectedIndex(fileList.getModel().getSize() - 1);
        } catch (Exception ex) {
            // Ignore errors
        }
    }

    /**
     * check number of files already selected for adding
     */
    private void updateContainingWizardAcceptButton() {
        int curr_add_file_count = getFileList().getModel().getSize();
        this.callStateChanged(curr_add_file_count > 0);
    }

    
    public boolean areSettingsValid() {
        int curr_add_file_count = getFileList().getModel().getSize();
        boolean r = curr_add_file_count > 0;
        return r;
    }

    public List<DynObject> getSelecteds() {
        ArrayList<DynObject> list = new ArrayList<DynObject>();
        JList fList = this.getFileList();
        int[] selecteds = fList.getSelectedIndices();
        FilesystemStoreListModel model =
            (FilesystemStoreListModel) fList.getModel();

        for (int index : selecteds) {
            list.add((DynObject) model.getStoreParameterAt(index));
        }
        return list;
    }

    public void valueChanged(ListSelectionEvent e) {
        this.updateButtons();
    }

    public class MyFileFilter extends FileFilter {

        public FilesystemFileFilter filter = null;

        public MyFileFilter(FilesystemFileFilter params) {
            this.filter = params;
        }

        /**
         * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
         */
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            return filter.accept(f);

        }

        /**
         * @see javax.swing.filechooser.FileFilter#getDescription()
         */
        public String getDescription() {
            return filter.getDescription();
        }

        public String getName() {
            return filter.getDataStoreProviderName();
        }
    }

    private List<FilesystemStoreParameters> chooseFilesParameters() {
        
        // this.callStateChanged(true);
        
        JFileChooser fileChooser =
            new JFileChooser(OPEN_LAYER_FILE_CHOOSER_ID,
                explorer.getCurrentPath());
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setAcceptAllFileFilterUsed(false);

        Iterator<MyFileFilter> iter = this.filters.iterator();
        while (iter.hasNext()) {
            fileChooser.addChoosableFileFilter(iter.next());
        }
        
        if (filters.size() > 0) {
            MyFileFilter firstf = filters.get(0);
            // Set first filter
            fileChooser.setFileFilter(firstf);
        }

        // If there is a last filter, use it
        if (lastFilter != null && filters.contains(lastFilter)) {
            fileChooser.setFileFilter(lastFilter);
        }

        int result = fileChooser.showOpenDialog(this);

        List<FilesystemStoreParameters> toAdd = 
            new ArrayList<FilesystemStoreParameters>();

        if (result == JFileChooser.APPROVE_OPTION) {
            lastFilter = (MyFileFilter) fileChooser.getFileFilter();
            lastPath = fileChooser.getCurrentDirectory().getAbsolutePath();
            JFileChooser.setLastPath(OPEN_LAYER_FILE_CHOOSER_ID, fileChooser.getCurrentDirectory());
            String providerName = null;
            if( lastFilter!=null ) {
                if( !DEFAULT_FILTER.equalsIgnoreCase(lastFilter.getDescription()) ) {
                    providerName = lastFilter.getName();
                }
            }
            toAdd = this.getParametersList(
                    fileChooser.getCurrentDirectory(), 
                    fileChooser.getSelectedFiles(), 
                    providerName
            );
        }
        return toAdd;
    }

    private List<FilesystemStoreParameters> getParametersList(File folder, File[] files, String providerName) {
        ApplicationManager application = ApplicationLocator.getManager();
        
        PrepareContext context = this.getPrepareDataStoreContext();
        
        List<DataStoreParameters> params = new ArrayList<DataStoreParameters>();
        List<DataStoreParameters> preparedParams = null;
        List<FilesystemStoreParameters> returnParams = new ArrayList<FilesystemStoreParameters>();
        
        for ( File aFile : files ) {
            String currentProvider = providerName;
            DataStoreParameters param;
            try {
                if( folder == null) {
                    explorer.setCurrentPath(aFile.getParentFile());
                } else {
                    explorer.setCurrentPath(folder);
                }
                if( currentProvider == null ) {
                    for( int i=1; i<this.filters.size(); i++) {
                        MyFileFilter filter = this.filters.get(i);
                        if( filter.accept(aFile)) {
                            currentProvider = filter.getName();
                            break;
                        }
                    }
                }
                if( currentProvider == null ) {
                    // TODO: ask user
                    continue;
                }
                param = explorer.createStoreParameters(aFile, currentProvider);
                if ( param == null ) {
                    // TODO show warning
                    continue;
                }
                params.add(param);

            } catch (DataException e) {
                NotificationManager.addError(e);
                return null;
            }
        }

        try {
            preparedParams
                    = application.prepareOpenDataStoreParameters(params, context);
        } catch (Exception e) {
            NotificationManager.addError(e);
        }

        if ( preparedParams != null ) {
            for ( int i = 0; i < preparedParams.size(); i++ ) {
                returnParams.add((FilesystemStoreParameters) preparedParams.get(i));
            }
        }
        return returnParams;
    }

    protected abstract PrepareContext getPrepareDataStoreContext();

    /**
     * Refers to up/down/remove buttons etc. Called when selection changes
     */
    private void updateButtons() {
        int selectedIndex = this.getFileList().getSelectedIndex();
        int size = this.getFileList().getModel().getSize();
        int[] selecteds = this.getFileList().getSelectedIndices();
        if (size < 1) {
            this.getPropertiesButton().setEnabled(false);
            this.getRemoveButton().setEnabled(false);
            this.getUpButton().setEnabled(false);
            this.getDownButton().setEnabled(false);
            return;
        } else
            if (size == 1) {
                this.getUpButton().setEnabled(false);
                this.getDownButton().setEnabled(false);
            } else {
                this.getUpButton().setEnabled(selectedIndex > 0);

                this.getDownButton().setEnabled(
                    selectedIndex > -1
                        && selecteds[selecteds.length - 1] < size - 1);
            }
        this.getPropertiesButton().setEnabled(
            selectedIndex > -1
                && this.getFileList().getSelectedIndices().length == 1);
        this.getRemoveButton().setEnabled(selectedIndex > -1);
    }

    public void close() {
        this.dispose();
    }

    public void dispose() {
        if (explorer != null) {
            explorer.dispose();
            explorer = null;
        }
        if (filters != null) {
            filters.clear();
            filters = null;
        }
    }
    
    class RefreshThread extends Thread {
        
        private int waitt = 0;
        private JList comp = null;
        
        public RefreshThread(JList c, int t) {
            waitt = t;
            comp = c;
        }
        public void run() {
            
            try {
                /*
                 * We might need to wait a bit because the GUI thread needs
                 * some time to update (this is because we have used a modal dialog
                 * and we refresh after that dialog is closed)
                 */
                Thread.sleep(waitt);
            } catch (Exception exc) {
                
            }
            
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                        // System.out.println("REFRESH");
                        comp.clearSelection();
                    } catch (Exception exc) {
                        LOG.info("Error while refreshing components.", exc);
                    }
                }
            });
        }
    }    
    
    protected void fixParametersFromContext(final DynObject parameters) {
        // By default do nothing
    }
}
