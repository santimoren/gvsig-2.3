/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.persistence.impl.exception.ObjectNotFoundException;

/**
 * This class provides utility methods to manage the TOC clipboard
 * which will contain one or more layers for copy/cut/paste
 * operations. It is currently implemented using a persistence
 * file where an instance of FLayers is stored.
 * 
 * @author jldominguez
 * 
 */
public class CopyPasteLayersUtils {

    private static Logger logger = LoggerFactory
        .getLogger(CopyPasteLayersUtils.class);

    /**
     * The file which is used to save the layers
     */
    private static String CLIPBOARD_FILE_NAME = "gvSIG_layers_clipboard.tmp";
    private static File clipboardFolder = null;

    private CopyPasteLayersUtils() {
    }
    
    private static PersistenceManager getPersMan() {
        return ToolsLocator.getPersistenceManager();
    }
    
    private static File getClipboardFolder() {
        if (clipboardFolder == null) {
            File appf = PluginsLocator.getManager().getApplicationHomeFolder();
            clipboardFolder = new File(appf, "clipboard-layers");
            clipboardFolder.mkdirs();
        }
        return clipboardFolder;
        // 
        
    }
    /**
     * Gets the outputstream for writing contents to the
     * clipboard
     * 
     * @return
     * @throws IOException
     */
    private static OutputStream getClipboardOStream() throws IOException {
        
        File f = new File(getClipboardFolder(), CLIPBOARD_FILE_NAME);
        if (f.exists()) {
            /*
             * If file exists, it is removed
             * (clipboard content overwritten)
             */
            f.delete();
        }
        f.createNewFile();
        /*
         * File will be removed on exit
         */
        f.deleteOnExit();
        return new FileOutputStream(f);
    }
    
    public static void clearClipboard() throws IOException {

        File f = new File(getClipboardFolder(), CLIPBOARD_FILE_NAME);
        if (f.exists()) {
            f.delete();
        }
    }

    /**
     * Gets the input stream to read the clipboard or null
     * if clipboard is empty
     * 
     * @return
     * @throws IOException
     */
    private static InputStream getClipboardIStream() throws IOException {

        File f = new File(getClipboardFolder(), CLIPBOARD_FILE_NAME);
        if (f.exists()) {
            return new FileInputStream(f);
        } else {
            return null;
        }

    }
    
    public static boolean isClipboardEmpty() {
        File f = new File(getClipboardFolder(), CLIPBOARD_FILE_NAME);
        return !f.exists();
    }

    /**
     * Returns the content of the clipboard as an instance
     * of FLayers or null if clipboard is empty or does not
     * contain instance of FLayers
     * 
     * @return
     * @throws PersistenceException
     */
    public static FLayers getClipboardAsFLayers() throws PersistenceException {

        InputStream is = null;

        try {
            is = getClipboardIStream();
            
            if (is == null) {
                return null;
            }
            
            Object obj = getPersMan().getObject(is);
            is.close();
            if (obj instanceof FLayers) {
                return (FLayers) obj;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.info("While getting object from clipboard: ", e);
            throw new PersistenceException(e);
        }

    }

    /**
     * Stores the given {@link PersistentState}
     * to clipboard
     * 
     * @param st
     * @throws PersistenceException
     */
    public static void saveToClipboard(PersistentState st)
        throws PersistenceException {

        OutputStream os = null;
        try {
            os = getClipboardOStream();
            getPersMan().saveState(st, os);
            os.close();
            if (st.getContext().getErrors() != null) {
                throw st.getContext().getErrors();
            }
        } catch (Exception ex) {
            throw new PersistenceException(ex);
        }
    }

    /**
     * Gets an array of layers as a {@link PersistentState}
     * of an instance of FLayers
     * 
     * @param actives
     * @param ctxt
     * @return
     * @throws PersistenceException
     */
    public static PersistentState getAsFLayersPersistentState(FLayer[] actives,
        MapContext ctxt) throws PersistenceException {

        FLayers lyrs = new FLayers();
        lyrs.setMapContext(ctxt);
        lyrs.setName("copy-paste-root");

        FLayer item = null;
        if (actives != null) {
            for (int i = 0; i < actives.length; i++) {
                if(actives[i] instanceof FLayers){
                	FLayers aux = (FLayers) actives[i];
                	 FLayers copyaux = new FLayers();
                	 copyaux.setMapContext(ctxt);
                	 copyaux.setName(aux.getName());
                	 for(int j=0; j<aux.getLayersCount(); j++){
                		 copyaux.addLayer(aux.getLayer(j));
                	 }
                	 lyrs.addLayer(copyaux);
                }else{
	            	item = actives[i];
	                try {
	                    item = item.cloneLayer();
	                } catch (Exception ex) {
	                    throw new PersistenceException(ex);
	                }
	                lyrs.addLayer(item);
                }
            }
        }

        PersistentState state = null;
        state = getPersMan().getState(lyrs, true);

        if (state.getContext().getErrors() != null) {
            throw state.getContext().getErrors();
        }
        return state;
    }

    /**
     * Remosves the layers from the their parent
     * @param actives
     * @param mc
     * @return
     */
    public static boolean removeLayers(FLayer[] actives, MapContext mc) {

        if (actives == null || actives.length == 0) {
            return false;
        }

        for (int i = 0; i < actives.length; i++) {
            if (actives[i].isEditing() && actives[i].isAvailable()) {
                JOptionPane.showMessageDialog(
                    (Component) PluginServices.getMainFrame(),
                    Messages.getText("no_se_puede_borrar_una_capa_en_edicion"),
                    Messages.getText("eliminar_capa"),
                    JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }

        mc.beginAtomicEvent();
        for (int i = actives.length - 1; i >= 0; i--) {
            try {
                // actives[i].getParentLayer().removeLayer(actives[i]);
                // FLayers lyrs=getMapContext().getLayers();
                // lyrs.addLayer(actives[i]);
            	FLayers parent = actives[i].getParentLayer();
            	if(actives[i] instanceof FLayers){
            		FLayers grouped = (FLayers) actives[i];
            		parent.removeLayer(grouped);
            		while(grouped.getLayersCount()>0){
            			FLayer lay = grouped.getLayer(0);
            			grouped.removeLayer(lay);
            		}
            	}else{
            		parent.removeLayer(actives[i]);
            	}
                
                // Cierra todas las ventanas asociadas a la capa
                IWindow[] wList =
                    PluginServices.getMDIManager().getAllWindows();
                for (int j = 0; j < wList.length; j++) {
                    String name = wList[j].getWindowInfo().getAdditionalInfo();
                    for (int k = 0; k < actives.length; k++) {
                        if (name != null && actives != null
                            && actives[k] != null
                            && actives[k].getName() != null
                            && name.compareTo(actives[k].getName()) == 0)
                            PluginServices.getMDIManager()
                                .closeWindow(wList[j]);
                    }
                }

            } catch (CancelationException e1) {
                JOptionPane.showMessageDialog((Component) PluginServices
                    .getMainFrame(), Messages
                    .getText("No_ha_sido_posible_realizar_la_operacion"),
                    Messages.getText("eliminar_capa"),
                    JOptionPane.WARNING_MESSAGE);
                logger.info("Error while removing layers.", e1);
                return false;
            }
        }
        mc.endAtomicEvent();
        Project project =
            ((ProjectExtension) PluginServices
                .getExtension(ProjectExtension.class)).getProject();
        project.setModified(true);
        PluginServices.getMainFrame().enableControls();
        return true;

    }

    /**
     * Adds layers to target {@link FLayers}
     * 
     * @param clipboard_root
     * @param target_root
     * @return
     */
    public static boolean addLayers(FLayers clipboard_root, FLayers target_root) {

        if (clipboard_root == null || clipboard_root.getLayersCount() == 0) {
            return false;
        }

        int n = clipboard_root.getLayersCount();
        FLayer item = null;
        for (int i = 0; i < n; i++) {
            item = clipboard_root.getLayer(i);
            target_root.addLayer(item);
        }
        return true;
    }

}
