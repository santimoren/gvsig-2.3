package org.gvsig.app.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.CRC32;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultFeatureTypeDefinitionsManager implements FeatureTypeDefinitionsManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultFeatureTypeDefinitionsManager.class);

    private File definitionsFolder = null;
    private final Map<String, DynClass> dynClasses;

    public DefaultFeatureTypeDefinitionsManager() {
        this.dynClasses = new HashMap<>();
    }

    private File getDefinitionsFolder() throws IOException {
        if (this.definitionsFolder == null) {
            PluginsManager pluginManager = PluginsLocator.getManager();
            PluginServices plugin = pluginManager.getPlugin(CreateDefaultFormDefinitionExtension.class);
            File homeFolder = plugin.getPluginHomeFolder();
            File definitionsFolder = new File(homeFolder, "schemas");
            if (!definitionsFolder.exists()) {
                FileUtils.forceMkdir(definitionsFolder);
            }
            this.definitionsFolder = definitionsFolder;
        }
        return this.definitionsFolder;
    }

    private long getCRC(FeatureType type) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < type.size(); i++) {
            FeatureAttributeDescriptor x = type.getAttributeDescriptor(i);
            buffer.append(x.getName());
            buffer.append(x.getDataTypeName());
            buffer.append(x.getSize());
        }
        CRC32 crc = new CRC32();
        byte[] data = buffer.toString().getBytes();
        crc.update(data);
        return crc.getValue();
    }

    private String getKey(FeatureStore store, FeatureType featureType) {
        return store.getName() + "_" + Long.toHexString(getCRC(featureType));
    }

    private File getDefinitionFile(String key) {
        File folder;
        try {
            folder = getDefinitionsFolder();
        } catch (IOException ex) {
            return null;
        }
        File f = new File(folder, key + ".xml");
        if (f.exists()) {
            return f;
        }
        Properties prop = new Properties();
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(new File(folder, "index.properties"));
            prop.load(fin);
        } catch (IOException ex) {
            return null;
        } finally {
            IOUtils.closeQuietly(fin);
        }
        String s = prop.getProperty(key, null);
        if (s == null) {
            return null;
        }
        f = new File(folder, s);
        if (f.exists()) {
            return f;
        }
        f = new File(s);
        if (f.exists()) {
            return f;
        }
        return null;
    }

    @Override
    public DynClass get(FeatureStore store, FeatureType featureType) {
        String key = this.getKey(store, featureType);
        DynClass dynClass = this.dynClasses.get(key);
        if (dynClass != null) {
            return dynClass;
        }
        File definitionFile = getDefinitionFile(key);
        if (definitionFile == null) {
            DataServerExplorer explorer = null;
            try {
                explorer = store.getExplorer();
                definitionFile = explorer.getResourcePath(store, key+".xml");
            } catch(Exception ex) {
                // Do nothing, leave definitionFile to null
            } finally {
                DisposeUtils.disposeQuietly(explorer);
            }
            if( definitionFile == null || !definitionFile.exists()) {
                return featureType;
            }
        }
        DynObjectManager dynObjectManager = ToolsLocator.getDynObjectManager();
        try {
            String xml = FileUtils.readFileToString(definitionFile);
            xml = xml.replaceAll("[$][{]CWD[}]",definitionFile.getParentFile().getAbsolutePath());

            InputStream is = IOUtils.toInputStream(xml, Charset.forName("UTF-8"));
            Map<String, DynClass> dynClasses = dynObjectManager.importDynClassDefinitions(is, this.getClass().getClassLoader());
            for (DynClass aDynClass : dynClasses.values()) {
                this.dynClasses.put(aDynClass.getName(), aDynClass);
            }
        } catch (Exception ex) {
            logger.warn("Can't parse xml definition.", ex);
            return null;
        }

        dynClass = this.dynClasses.get(key);
        if (dynClass != null) {
            return dynClass;
        }
        return featureType;
    }

    @Override
    public boolean contains(FeatureStore store, FeatureType featureType) {
        String key = this.getKey(store, featureType);
        return this.dynClasses.containsKey(key);
    }

    @Override
    public void add(FeatureStore store, FeatureType featureType, DynClass dynClass) {
        try {
            String key = this.getKey(store, featureType);
            File f = new File(getDefinitionsFolder(), key + ".xml");
            DynObjectManager dynObjectManager = ToolsLocator.getDynObjectManager();
            String xml = dynObjectManager.exportSimpleDynClassDefinitions(dynClass);
            FileUtils.write(f, xml);
            this.dynClasses.put(key, dynClass);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void remove(FeatureStore store, FeatureType featureType) {
        String key = this.getKey(store, featureType);
        this.dynClasses.remove(key);
    }

    public void addModel(File model) {
    	  DynObjectManager dynObjectManager = ToolsLocator.getDynObjectManager();
          try {
              String xml = FileUtils.readFileToString(model);
              InputStream is = IOUtils.toInputStream(xml, Charset.forName("UTF-8"));
              Map<String, DynClass> dynClasses = dynObjectManager.importDynClassDefinitions(is, this.getClass().getClassLoader());

              File folder;
              try {
                  folder = getDefinitionsFolder();
              } catch (IOException ex) {
                  return ;
              }
              Properties prop = new Properties();
              FileInputStream fin = null;
              try {
                  fin = new FileInputStream(new File(folder, "index.properties"));
                  prop.load(fin);
              } catch (IOException ex) {
              } finally {
                  IOUtils.closeQuietly(fin);
              }
              for (DynClass aDynClass : dynClasses.values()) {
            	  String className = aDynClass.getName();
            	  prop.setProperty(className, model.getAbsolutePath());
              }
              FileOutputStream fout = null;
              try {
            	  fout = new FileOutputStream(new File(folder, "index.properties"));
                  prop.store(fout, "");
              } catch (IOException ex) {
              } finally {
                  IOUtils.closeQuietly(fout);
              }

          } catch (Exception ex) {
              logger.warn("Can't parse xml definition.", ex);
          }
    }

}
