/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocumentAction;
import org.gvsig.app.project.documents.Document;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.persistence.exception.PersistenceException;


public class CopyDocumentAction extends AbstractDocumentAction {

    private static Logger logger = LoggerFactory.getLogger(
        CopyDocumentAction.class);
    
	public CopyDocumentAction() {
		super("copy");
		this.order = 0;
		this.title = Messages.getText("copiar");
		this.group = ProjectManager.getInstance().addDocumentActionGroup("ClipboardActions", "Clipboard actions", null, 0);
	}
	
	public void execute(Document document, List<Document> documents) {
	    
		List<Document> docs = new ArrayList<Document>(documents);
		if(document != null &&  !docs.contains(document)) {
			docs.add(document);
		}
		// Now "docs" has all docs involved
		try {
            CopyPasteDocsUtils.saveToClipboard(docs);
        } catch (Exception e) {
            
            logger.info("While copying docs to clipboard.", e);
            JOptionPane.showMessageDialog(
                ApplicationLocator.getManager().getRootComponent(),
                Messages.getText("_Clipboard_error") + ":\n"
                + CopyPasteDocsUtils.getLastMessage(e),
                title,
                JOptionPane.ERROR_MESSAGE);
            /*
             * Clear clipboard after error
             */
            try {
                CopyPasteDocsUtils.clearClipboard(null);
            } catch (Exception exc) {
                logger.error("While clearing clipboard.", exc);
            }
            
        }
	}
	
	public boolean isAvailable(Document document, List<Document> selectedItems) {
	    return selectedItems.size() > 0;
	}

	public boolean isVisible(Document document, List<Document> documents) {
		return true;
	}

}
