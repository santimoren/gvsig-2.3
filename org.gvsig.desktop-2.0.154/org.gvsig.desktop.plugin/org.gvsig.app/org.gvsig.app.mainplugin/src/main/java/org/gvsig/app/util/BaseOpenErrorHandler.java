/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.util;

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.JOptionPane;

import org.apache.commons.io.FilenameUtils;
import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.OpenErrorHandler;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerWizardPanel;
import org.gvsig.gui.beans.swing.JFileChooser;
import org.gvsig.i18n.Messages;
import org.gvsig.utils.SimpleFileFilter;

public class BaseOpenErrorHandler implements OpenErrorHandler{
	
	
	private DataParameters parameters;
//	private Exception exception;

	public BaseOpenErrorHandler() {
//		exception=null;
		parameters=null;
		
	}

	public boolean canRetryOpen(Exception e, DataParameters parameters) {
//		this.exception=e;
		this.parameters=parameters;
//		Throwable cause = null;
		boolean retry = false;

		/*if (this.exception instanceof InvocationTargetException){
			cause = this.exception.getCause();
		} else {
			cause = this.exception;
		}*/
		//if (cause instanceof InitializeException){
			//if (cause.getCause() instanceof OpenException){
				if ( this.parameters instanceof FilesystemStoreParameters) {
					FilesystemStoreParameters filesystemStoreParameters = (FilesystemStoreParameters)this.parameters;
					File searchedFile = filesystemStoreParameters.getFile();
					File file = null;
					if( searchedFile != null && !searchedFile.exists() ) {
						file = chooseAlternateFile(searchedFile);
						if( file!=null){
							filesystemStoreParameters.setFile(file);
							retry = true;
						} else {
							return false;
						}
					}

					if (this.parameters.getDynClass().getName().equalsIgnoreCase("SHPStoreParameters") ) {
						//SHPStoreParameters shpStoreParameters = (SHPStoreParameters)this.parameters;
//						File searchedDBFFile = shpStoreParameters.getDBFFile();
						File searchedDBFFile =  (File) this.parameters.getDynValue("dbfFile");
						if (searchedDBFFile!=null && !searchedDBFFile.exists()) {
							File possibleDbfFile = null;
							if (file != null){
//								possibleDbfFile = SHP.getDbfFile(file);
								possibleDbfFile = findFile(file, "dbf");
							}
							if (possibleDbfFile != null && possibleDbfFile.exists()) {
//								shpStoreParameters.setDBFFile(possibleDbfFile);
								this.parameters.setDynValue("dbfFile", possibleDbfFile);
								retry = true;
							} else {
								File dbfFile = chooseAlternateFile(searchedDBFFile);
								if (dbfFile != null){
//									shpStoreParameters.setDBFFile(dbfFile);
									this.parameters.setDynValue("dbfFile", dbfFile);
									retry = true;
								} else {
									return false;
								}
							}
						}
//						File searchedSHXFile = shpStoreParameters.getSHXFile();
						File searchedSHXFile = (File) this.parameters.getDynValue("shxfile");;
						if (searchedDBFFile!=null && !searchedSHXFile.exists()) {
							File possibleShxFile = null;
							if (file != null){
//								possibleShxFile = SHP.getShxFile(file);
								possibleShxFile = findFile(file, "shx");
							}
							if (possibleShxFile != null && possibleShxFile.exists()) {
//								shpStoreParameters.setSHXFile(possibleShxFile);
								this.parameters.setDynValue("shxfile", possibleShxFile);
								retry = true;
							} else {
								File shxFile = chooseAlternateFile(searchedSHXFile);
								if (shxFile != null) {
//									shpStoreParameters.setSHXFile(shxFile);
									this.parameters.setDynValue("shxfile", shxFile);
									retry = true;
								} else {
									return false;
								}
							}
						}
					}
				}
			//}
		//}
		return retry;
	}
	
	private File chooseAlternateFile(File searchedFile){
			int resp = JOptionPane.showConfirmDialog(
					null,
					Messages.getText("dont_find_the_file")+"\n"+
					searchedFile.getName()+"\n"+
					Messages.getText("do_you_want_to_locate_the_file"),
					Messages.getText("dont_find_the_file"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (resp == JOptionPane.OK_OPTION){
				JFileChooser fileChooser = new JFileChooser(FilesystemExplorerWizardPanel.OPEN_LAYER_FILE_CHOOSER_ID,
						searchedFile.getParentFile());
				File parentFile = searchedFile.getParentFile();
				if (parentFile.exists()){
					fileChooser.setLastPath(parentFile);
				}
				fileChooser.setMultiSelectionEnabled(false);
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.setDialogTitle("dont_find_the_file: "+searchedFile.getAbsolutePath());

				String searchedFileName = searchedFile.getName();
				String ext = searchedFileName.substring(searchedFileName.lastIndexOf(".")+1);
				SimpleFileFilter fileFilter = new SimpleFileFilter(ext, ext);
				fileChooser.addChoosableFileFilter(fileFilter);
				fileChooser.setFileFilter(fileFilter);
				
				int result = fileChooser.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
		            File file = fileChooser.getSelectedFile();
		            return file;
		        }
			}
		return null;
	}
	
	private File findFile(File file, String extension) {
		File[] files = file.getParentFile().listFiles(new ExtensionFilenameFilter(file, extension));
		if( files == null || files.length<1 ) {
			return new File(file.getParentFile(),FilenameUtils.getBaseName(file.getName())+"."+extension);
		}
		return files[0];
	}
	
	private class ExtensionFilenameFilter implements FilenameFilter {
		private String extension;
		private String baseName;
		ExtensionFilenameFilter(File f, String extension) {
			this.extension = extension;
			this.baseName = FilenameUtils.getBaseName(f.getName());
		}
		public boolean accept(File arg0, String filename) {
			if( FilenameUtils.getBaseName(filename).equals(baseName) ) {
				if( FilenameUtils.getExtension(filename).equalsIgnoreCase(extension) ) {
					return true;
				}
			}
			return false;
		}
	}

}


