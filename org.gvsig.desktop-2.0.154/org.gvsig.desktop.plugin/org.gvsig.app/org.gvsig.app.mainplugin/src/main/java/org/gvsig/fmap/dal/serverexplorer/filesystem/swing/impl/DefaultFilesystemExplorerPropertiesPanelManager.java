package org.gvsig.fmap.dal.serverexplorer.filesystem.swing.impl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanel;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanelFactory;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanelManager;
import org.gvsig.fmap.mapcontrol.swing.dynobject.DynObjectEditor;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.Dialog;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager_v2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultFilesystemExplorerPropertiesPanelManager implements FilesystemExplorerPropertiesPanelManager {

    private static Logger logger = LoggerFactory.getLogger(DefaultFilesystemExplorerPropertiesPanelManager.class);
            
    private List<FilesystemExplorerPropertiesPanelFactory> factories = null;
    
    public DefaultFilesystemExplorerPropertiesPanelManager() {
        this.factories = new ArrayList<>();
    }
    
    @Override
    public void registerFactory(FilesystemExplorerPropertiesPanelFactory factory) {
        if( this.factories.contains(factory) ) {
            return;
        }
        this.factories.add(factory);
    }

    @Override
    public FilesystemExplorerPropertiesPanelFactory getFactory(DynObject parameters) {
        FilesystemExplorerPropertiesPanelFactory factory = null;
        for (FilesystemExplorerPropertiesPanelFactory currentFactory : factories) {
            if( currentFactory.canBeApplied(parameters) ) {
                if( factory == null ) {
                    factory = currentFactory;
                } else if( factory.getPriority() < currentFactory.getPriority() ) {
                    factory = currentFactory;
                }
            }
        }
        return factory;
    }

    @Override
    public FilesystemExplorerPropertiesPanel createPanel(DynObject parameters) {
        FilesystemExplorerPropertiesPanelFactory factory = this.getFactory(parameters);
        if( factory == null ) {
            return null;
        }
        return factory.create(parameters);
    }
    
    @Override
    public void showPropertiesDialog(final DynObject parameters) {
        
        final FilesystemExplorerPropertiesPanel panel = this.createPanel(parameters);
        this.showPropertiesDialog(parameters, panel);
    }
    
    @Override
    public void showPropertiesDialog(final DynObject parameters, final FilesystemExplorerPropertiesPanel panel) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        String title = parameters.getDynClass().getDescription();
        if( !StringUtils.isEmpty(title) ) {
        	title = parameters.getDynClass().getName();
        }
        
        WindowManager_v2 winmanager = (WindowManager_v2) ToolsSwingLocator.getWindowManager();
        Dialog dialog = winmanager.createDialog(
                panel.asJComponent(),
                title,
                null, 
                WindowManager_v2.BUTTONS_APPLY_OK_CANCEL
        );
        if( panel instanceof FilesystemExplorerPropertiesDynObjectPanel ) {
            dialog.setButtons(WindowManager_v2.BUTTONS_OK_CANCEL);
        } else {
            dialog.setButtonLabel(
                    WindowManager_v2.BUTTON_APPLY, 
                    i18nManager.getTranslation("Avanzado")
            );
        }
        dialog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                switch(ae.getID() ) {
                    case WindowManager_v2.BUTTON_APPLY:
                        showAdvancedPropertiesDialog(parameters);
                        break;
                    case WindowManager_v2.BUTTON_OK:
                        panel.fetchParameters(parameters);
                        break;
                    case WindowManager_v2.BUTTON_CANCEL:
                        break;
                }
            }
        });
        dialog.show(WindowManager.MODE.DIALOG);
        
    }
    
    private void showAdvancedPropertiesDialog(DynObject parameters) {

        try {
            DynObjectEditor editor = new DynObjectEditor(parameters);
            editor.editObject(true);
        } catch (Throwable th) {
            logger.warn("Can't show standard properties dialog for this parameters.",th);
        }

   }    
}
