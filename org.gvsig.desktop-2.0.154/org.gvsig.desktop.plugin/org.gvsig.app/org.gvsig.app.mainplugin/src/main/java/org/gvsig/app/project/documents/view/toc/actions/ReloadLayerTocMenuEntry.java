/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.FLayer;


/**
*
* @author Jose Manuel Viv� (Chema)
*
* Entrada de men� para recargar una capa.
*/
public class ReloadLayerTocMenuEntry extends AbstractTocContextMenuAction{
    
    private static Logger logger =
        LoggerFactory.getLogger(ReloadLayerTocMenuEntry.class);
        
	public String getGroup() {
		return "group3"; //FIXME
	}

	public int getGroupOrder() {
		return 30;
	}

	public int getOrder() {
		return 1;
	}

	public String getText() {
		return PluginServices.getText(this, "recargar");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return !getNodeLayer(item).isAvailable();
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			return true;
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
		try {
                    FLayer layer = getNodeLayer(item);
                    layer.reload();
                    layer.getMapContext().invalidate();
		} catch (Exception ex) {
            logger.info("While reloading layer", ex);
            getNodeLayer(item).setAvailable(false);
		}
	}
}

