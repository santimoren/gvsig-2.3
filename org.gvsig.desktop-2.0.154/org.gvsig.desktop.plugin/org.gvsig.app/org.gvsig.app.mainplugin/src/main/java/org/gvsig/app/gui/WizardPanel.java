/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui;

import java.awt.Window;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.cresques.cts.IProjection;

import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.gui.wizards.WizardListener;
import org.gvsig.app.gui.wizards.WizardListenerSupport;
import org.gvsig.app.prepareAction.PrepareContextView_v1;
import org.gvsig.app.project.documents.view.toc.actions.LayerErrorsPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlCreationException;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.identitymanagement.UnauthorizedException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class WizardPanel extends JPanel {

    private static final Logger logger = LoggerFactory.getLogger(WizardPanel.class);
    private static final long serialVersionUID = 5317749209668850863L;
    private String tabName = "TabName";
    private MapControl mapCtrl = null;
    private final WizardListenerSupport listenerSupport = new WizardListenerSupport();
    private MapContext mapContext;
    private boolean b_isMapControlAvailable = false;

    public void addWizardListener(WizardListener listener) {
        listenerSupport.addWizardListener(listener);
    }

    public void callError(Exception descripcion) {
        listenerSupport.callError(descripcion);
    }

    public void removeWizardListener(WizardListener listener) {
        listenerSupport.removeWizardListener(listener);
    }

    public void callStateChanged(boolean finishable) {
        listenerSupport.callStateChanged(finishable);
    }

    protected void setTabName(String name) {
        tabName = name;
    }

    public String getTabName() {
        return tabName;
    }

    abstract public void initWizard();

    /**
     * @deprecated use {@link #executeWizard()} instead.
     */
    abstract public void execute();

    /**
     * Executes the wizard and returns anything created in the process.
     *
     * @return anything created in the process
     */
    public Object executeWizard() {
        execute();
        return null;
    }

    abstract public void close();

    abstract public DataStoreParameters[] getParameters();

    /**
     * You can use it to interact with the MapControl component that will
     * receive the new layer, in order to get user feedback (for instance a
     * bounding box). Check the {@link #isMapControlAvailable()} method before
     * accessing the MapControl because it may not be available (for instance
     * when adding layers to a MapContext not associated with a View).
     *
     * For the moment, this method will return a non-null MapControl for
     * compatibility reasons, but you should still check
     * {@link #isMapControlAvailable()} to be sure it is a valid one, as it
     * could only be a fake MapControl.
     *
     * It is recommended to use {@link #getMapContext()} method when no
     * interaction is needed with the map user interface (for instance to get
     * the active projection, visible extent, etc)
     *
     * @return Returns the mapCtrl.
     */
    public MapControl getMapCtrl() {
        if (mapCtrl != null) {
            return mapCtrl;
        } else if (mapContext != null) {
    		// if MapContext has been set, create a fake MapControl
            // for compatibility purposes
            MapControl mc;
            try {
                mc = MapControlLocator.getMapControlManager().createJMapControlPanel(mapContext);
            } catch (MapControlCreationException ex) {
                logger.warn("Can't create a MapControl.", ex);
                throw new RuntimeException(ex);
            }
            mapCtrl = mc;
        }
        return mapCtrl;
    }

    /**
     * Sets the MapControl that will receive the new layer
     *
     * @param mapCtrl The mapCtrl to set.
     */
    public void setMapCtrl(MapControl mapCtrl) {
        this.mapCtrl = mapCtrl;
        b_isMapControlAvailable = (mapCtrl != null);
    }

    /**
     * You can use it to extract information from the MapContext that will
     * receive the new layer. For example, projection to use, or visible extent.
     *
     * @return Returns the MapContext.
     */
    public MapContext getMapContext() {
        if (this.mapContext != null || this.mapCtrl == null) {
            return this.mapContext;
        } else {
            return this.mapCtrl.getMapContext();
        }

    }

    /**
     * Sets the MapContext that will receive the new layer
     *
     * @param mapContext The mapContext to set.
     */
    public void setMapContext(MapContext mapContext) {
        this.mapContext = mapContext;
    }

    /**
     * Checks whether the MapControl is available. The MapControl may not be
     * available in some circumstances, for instance when adding layers to a
     * MapContext not associated with a View.
     *
     * A MapContext should always be available on the {@link #getMapContext()}
     * method.
     *
     * @return true if the MapControl is available, false otherwise
     */
    public boolean isMapControlAvailable() {
        return b_isMapControlAvailable;
    }

    protected void doAddLayer(
            final String layerName, final DataStoreParameters parameters) {
        final boolean b_isMapControlAvail = this.isMapControlAvailable();
        final MapControl mapControl = this.getMapCtrl();
        final MapContext mapContext = this.getMapContext();
        final ApplicationManager application = ApplicationLocator.getManager();
        final MapContextManager manager
                = MapContextLocator.getMapContextManager();

        logger.info("addLayer('{}',...)", layerName);
        Thread task = new Thread(new Runnable() {

            @Override
            public void run() {
                FLayer layer = null;
                FLayer preparedLayer = null;
                try {
                    DataManager dataManager = DALLocator.getDataManager();
                    DataStore dataStore = dataManager.openStore(parameters.getDataStoreName(), parameters);
                    String layerName = dataStore.getName();
                    layer = manager.createLayer(layerName, dataStore);
                    DisposeUtils.disposeQuietly(dataStore);
                    preparedLayer
                            = application.prepareOpenLayer(layer,
                                    new PrepareContextView_v1() {

                                        @Override
                                        public Window getOwnerWindow() {
                                            return null;
                                        }

                                        @Override
                                        public MapControl getMapControl() {
                                            return mapControl;
                                        }

                                        @Override
                                        public IProjection getViewProjection() {
                                            return mapContext.getProjection();
                                        }

                                        @Override
                                        public MapContext getMapContext() {
                                            return mapContext;
                                        }

                                        @Override
                                        public boolean isMapControlAvailable() {
                                            return b_isMapControlAvail;
                                        }
                                    });
                    if (preparedLayer != null) {
                        mapContext.getLayers().addLayer(preparedLayer);
                    }
                } catch (UnauthorizedException e) {
                    I18nManager i18nManager = ToolsLocator.getI18nManager();
                    ApplicationManager application = ApplicationLocator.getManager();
                    String resource = "";
                    if (e.getResource() instanceof FilesystemStoreParameters) {
                        resource = ((FilesystemStoreParameters) e.getResource()).getFile().getPath();
                    }
                    application.messageDialog(
                            i18nManager.getTranslation("_User_0_is_not_authorized_to_1_on_resource_2_3",
                                    new String[]{
                                        e.getIdentity().getID(),
                                        e.getActionName(),
                                        e.getResourceName(),
                                        resource
                                    }),
                            i18nManager.getTranslation("_Unauthorized_access"),
                            JOptionPane.WARNING_MESSAGE
                    );
                    logger.warn("Unauthorized access to layer '" + layerName + "'.", e);

                } catch (Exception e) {
                    LayerErrorsPanel panel = new LayerErrorsPanel(layerName, e);
                    if (preparedLayer != null) {
                        panel.setLayer(preparedLayer);
                    } else if(layer!=null){
                        panel.setLayer(layer);
                    }
                    I18nManager i18nManager = ToolsLocator.getI18nManager();
                    ToolsSwingLocator.getWindowManager().showWindow(
                        panel,
                        i18nManager.getTranslation("_Problems_loading_the_layer"),
                        WindowManager.MODE.WINDOW
                    );
                    logger.warn("Can't load layer '" + layerName + "'.", e);

                } finally {
                    if (preparedLayer != layer) {
                        DisposeUtils.disposeQuietly(preparedLayer);
                    }
                    DisposeUtils.disposeQuietly(layer);
                }
            }
        });
        task.start();

    }

    /**
     *
     * @param mapControl
     * @param layerName
     * @param parameters
     * @deprecated Use {@link #doAddLayer(String, DataStoreParameters)} in
     * combination with {@link #setMapCtrl(MapControl)} if you need to set the
     * MapControl. Note that MapControl is automatically initialized when
     * creating the panel from the AddLayer extension.
     */
    protected void doAddLayer(final MapControl mapControl,
            final String layerName, final DataStoreParameters parameters) {
        this.setMapCtrl(mapControl);
        doAddLayer(layerName, parameters);
    }

    /**
     * This method is called for example when user changes tab in add layer
     * dialog (new tab's settings are valid?)
     *
     * @return whether current wizard settings are enough (for example, to
     * enable an Accept button in a container)
     */
    public boolean areSettingsValid() {
        return true;
    }

}
