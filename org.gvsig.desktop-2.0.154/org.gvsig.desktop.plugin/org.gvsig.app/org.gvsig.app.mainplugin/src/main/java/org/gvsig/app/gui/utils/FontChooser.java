/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.gui.utils;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import org.jfree.ui.FontChooserDialog;



public class FontChooser {
    
    public static Font showDialog(String title, Font font) {
    	
    	FontChooserDialog dlg = new FontChooserDialog(
    	    (Dialog)null,title, true,font);
    	
    	dlg.pack();
    	centerComponent(dlg);
    	
    	dlg.setVisible(true);
    	if( dlg.isCancelled() ) {
    		return null;
    	}
    	return dlg.getSelectedFont();
    }
    
    public static Font showDialog(
        String title,
        Font font,
        boolean allowStyle, boolean allowSize) {
        
        PartialFontChooserDialog dlg = new PartialFontChooserDialog(
            (Dialog)null,title, true,font,allowStyle,allowSize);
        
        dlg.pack();
        centerComponent(dlg);
        
        dlg.setVisible(true);
        if( dlg.isCancelled() ) {
            return null;
        }
        return dlg.getSelectedFont();
    }    
    
    private static void centerComponent(Component comp) {
        
        Dimension scr_dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension comp_dim = comp.getSize();
        
        comp.setLocation(
            (scr_dim.width - comp_dim.width) / 2,
            (scr_dim.height - comp_dim.height) / 2
            );
    }
}
