/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicArrowButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.extension.AddLayer;
import org.gvsig.app.project.documents.view.MapOverview;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.operations.Classifiable;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

/**
 * @author FJP
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Generation - Code and Comments
 */
public class FPanelLocConfig extends JPanel implements ActionListener, IWindow {

    private static Logger logger = LoggerFactory.getLogger(FPanelLocConfig.class);
    private static final long serialVersionUID = -3331364698732098356L;
    private javax.swing.JLabel jLabel = null;
    private javax.swing.JList jList = null; // @jve:decl-index=0:visual-constraint="390,10"
    private JButton jBtnAddLayer = null; //
    private JButton jBtnRemoveLayer = null;
    private JButton jBtnEditLegend = null;
    private JButton jBtnCancel = null;

    private MapControl mapCtrl;
    private WindowInfo m_viewinfo = null;
    private JPanel pnlButtons = null; // @jve:decl-index=0:visual-constraint="10,159"
    private BasicArrowButton jBtnUp;
    private BasicArrowButton jBtnDown;

    /**
     * This is the default constructor
     */
    public FPanelLocConfig(MapControl mc) {
        super();
        mapCtrl = mc;
        initialize();
        refreshList();
        updateControls(null);
    }

    private void refreshList() {
        
        DefaultListModel listModel = (DefaultListModel) getJList().getModel();
        listModel.removeAllElements();

        int index = 0;
        for (int i = mapCtrl.getMapContext().getLayers().getLayersCount() - 1; i >= 0; i--) {
            FLayer lyr = mapCtrl.getMapContext().getLayers().getLayer(i);
            listModel.add(index++, new LayerListItem(lyr));
        }
    }
    

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        
        this.setLayout(new BorderLayout());
        
        this.add(getJLabel(), BorderLayout.NORTH);
        this.add(getJList(), BorderLayout.CENTER);
        
        JPanel eastp = new JPanel(new BorderLayout());
        eastp.add(getJBtnUp(), BorderLayout.NORTH);
        eastp.add(getJBtnDown(), BorderLayout.SOUTH);
        this.add(eastp, BorderLayout.EAST);
        
        this.add(getJPanel(), BorderLayout.SOUTH);
    }

    /**
     * This method initializes jLabel
     * 
     * @return javax.swing.JLabel
     */
    private javax.swing.JLabel getJLabel() {
        if (jLabel == null) {
            jLabel = new javax.swing.JLabel();
            
            jLabel.setBorder(BorderFactory.createEmptyBorder(8,4,8,8));
            
            jLabel.setText(PluginServices
                .getText(this, "Capas_del_localizador") + ":");
        }
        return jLabel;
    }

    /**
     * This method initializes jList
     * 
     * @return javax.swing.JList
     */
    public javax.swing.JList getJList() {
        if (jList == null) {
            
            jList = new javax.swing.JList(new DefaultListModel());            
            jList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            jList.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {

                    public void valueChanged(
                        javax.swing.event.ListSelectionEvent e) {
                        updateControls(e);
                    }
                });

        }
        return jList;
    }

    private void updateControls(javax.swing.event.ListSelectionEvent e) {

        DefaultListModel lstModel = (DefaultListModel) getJList().getModel();
        
        int selIndex = jList.getSelectedIndex();
        jBtnDown.setEnabled(false);
        jBtnUp.setEnabled(false);

        if (selIndex != -1) {
            if (lstModel.getSize() > 1) {
                if (selIndex < (lstModel.getSize() - 1)) {
                    jBtnDown.setEnabled(true);
                }

                if (selIndex > 0) {
                    jBtnUp.setEnabled(true);
                }
            }
        }
        
        jBtnRemoveLayer.setEnabled(selIndex != -1);
        jBtnEditLegend.setEnabled(selIndex != -1);
    }

    /**
     * This method initializes jBtnUp
     * 
     * @return JButton
     */
    private BasicArrowButton getJBtnUp() {
        if (jBtnUp == null) {
            jBtnUp =
                new javax.swing.plaf.basic.BasicArrowButton(
                    javax.swing.SwingConstants.NORTH);
            jBtnUp.setToolTipText(PluginServices.getText(this, "Subir_capa"));
            jBtnUp.addActionListener(this);
            jBtnUp.setActionCommand("UP");

        }
        return jBtnUp;
    }

    /**
     * This method initializes jBtnDown
     * 
     * @return JButton
     */
    private BasicArrowButton getJBtnDown() {
        if (jBtnDown == null) {
            jBtnDown =
                new javax.swing.plaf.basic.BasicArrowButton(
                    javax.swing.SwingConstants.SOUTH);
            jBtnDown.setToolTipText(PluginServices.getText(this, "Bajar_capa"));
            jBtnDown.setActionCommand("DOWN");
            jBtnDown.addActionListener(this);
        }
        return jBtnDown;
    }

    /**
     * This method initializes jButton2
     * 
     * @return JButton
     */
    private JButton getJBtnAddLayer() {
        if (jBtnAddLayer == null) {
            jBtnAddLayer =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            jBtnAddLayer.setText(PluginServices.getText(this, "Anadir_capa")
                + "...");
            jBtnAddLayer.addActionListener(this);
            jBtnAddLayer.setActionCommand("ADD_LAYER");
        }
        return jBtnAddLayer;
    }

    /**
     * This method initializes jBtnRemoveLayer
     * 
     * @return JButton
     */
    private JButton getJBtnRemoveLayer() {
        if (jBtnRemoveLayer == null) {
            jBtnRemoveLayer =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            jBtnRemoveLayer.setText(PluginServices.getText(this, "Quitar_capa"));
            jBtnRemoveLayer.addActionListener(this);
            jBtnRemoveLayer.setActionCommand("REMOVE_LAYER");

        }
        return jBtnRemoveLayer;
    }

    /**
     * This method initializes jBtnEditLegend
     * 
     * @return JButton
     */
    private JButton getJBtnEditLegend() {
        if (jBtnEditLegend == null) {
            jBtnEditLegend =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            jBtnEditLegend.setText(PluginServices.getText(this,
                "Editar_leyenda") + "...");
            jBtnEditLegend.addActionListener(this);
            jBtnEditLegend.setActionCommand("EDIT_LEGEND");
        }
        return jBtnEditLegend;
    }

    /**
     * This method initializes jBtnCancel
     * 
     * @return JButton
     */
    private JButton getJBtnCancel() {
        if (jBtnCancel == null) {
            jBtnCancel =
                ToolsSwingLocator.getUsabilitySwingManager().createJButton();
            jBtnCancel.setText(PluginServices.getText(this, "Cerrar"));
            jBtnCancel.setActionCommand("CANCEL");
            jBtnCancel.addActionListener(this);

        }
        return jBtnCancel;
    }

    public void actionPerformed(ActionEvent e) {
        DefaultListModel lstModel = (DefaultListModel) getJList().getModel();
        FLayers theLayers = mapCtrl.getMapContext().getLayers();

        int numLayers = theLayers.getLayersCount() - 1;

        if (e.getActionCommand() == "CANCEL") {
            if (PluginServices.getMainFrame() != null) {
                PluginServices.getMDIManager()
                    .closeWindow(FPanelLocConfig.this);
            } else {
                ((JDialog) (getParent().getParent().getParent().getParent()))
                    .dispose();
            }
        }
        if (e.getActionCommand() == "ADD_LAYER") {
            AddLayer addLayer =
                (AddLayer) PluginServices.getExtension(AddLayer.class);

            if (addLayer.addLayers(mapCtrl)) {
                RefreshThread st = new RefreshThread(500);
                st.start();
            }
            if (mapCtrl instanceof MapOverview) {
                ((MapOverview) mapCtrl).refreshExtent();
            }

        }
        if (e.getActionCommand() == "REMOVE_LAYER") {
            if (jList.getSelectedIndex() != -1) {
                
                LayerListItem lli = (LayerListItem)
                    lstModel.get(jList.getSelectedIndex());
                
                theLayers.removeLayer(lli.getLayer());
                
                lstModel.remove(jList.getSelectedIndex());

                updateControls(null);
                if (mapCtrl instanceof MapOverview) {
                    ((MapOverview) mapCtrl).refreshExtent();
                }
            }
        }
        if (e.getActionCommand() == "EDIT_LEGEND") {
            int idSelec = jList.getSelectedIndex();
            if (idSelec != -1) {
                
                LayerListItem lli = (LayerListItem) lstModel.get(idSelec);
                FLayer lyr = lli.getLayer();
                
                if (lyr instanceof Classifiable) {
                    LayerProperties dlg = new LayerProperties(lyr);
                    WindowManager wm = ToolsSwingLocator.getWindowManager();
                    wm.showWindow(
                            dlg.asJComponent(), 
                            ToolsLocator.getI18nManager().getTranslation("propiedades_de_la_capa"), 
                            WindowManager.MODE.DIALOG
                    );

                } else {
                    JOptionPane.showMessageDialog(
                        null,
                        PluginServices.getText(this,
                            "Solo_para_capas_vectoriales") + ".");
                }

            }

        }
        if (e.getActionCommand() == "UP") {
            int idSelec = jList.getSelectedIndex();
            int fromIndex = idSelec;
            int toIndex = idSelec - 1;
            
            LayerListItem lli = (LayerListItem) lstModel.get(fromIndex);
            
            try {
                theLayers.moveTo(numLayers - fromIndex, numLayers - toIndex);
            } catch (CancelationException e1) {
                e1.printStackTrace();
            }

            lstModel.remove(fromIndex);
            lstModel.add(toIndex, lli);

            jList.setSelectedIndex(toIndex);
        }
        if (e.getActionCommand() == "DOWN") {
            int idSelec = jList.getSelectedIndex();
            int fromIndex = idSelec;
            int toIndex = idSelec + 1;
            
            LayerListItem lli = (LayerListItem) lstModel.get(fromIndex);
            
            try {
                theLayers.moveTo(numLayers - fromIndex, numLayers - toIndex);
            } catch (CancelationException e1) {
                e1.printStackTrace();
            }

            lstModel.remove(fromIndex);
            lstModel.add(toIndex, lli);

            jList.setSelectedIndex(toIndex);
        }

    }

    public WindowInfo getWindowInfo() {
        if (m_viewinfo == null) {
            m_viewinfo = new WindowInfo(WindowInfo.MODELESSDIALOG);
            m_viewinfo.setTitle(PluginServices.getText(this,
                "Configurar_localizador"));
            
            m_viewinfo.setWidth(400);
            m_viewinfo.setHeight(200);
        }
        return m_viewinfo;
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#windowActivated()
     */
    public void viewActivated() {
    }

    /**
     * This method initializes jPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJPanel() {
        if (pnlButtons == null) {
            pnlButtons = new JPanel(new GridLayout(2, 3, 8, 4));
            pnlButtons.setBorder(BorderFactory.createEmptyBorder(8,8,8,8));
            
            pnlButtons.add(getJBtnAddLayer());
            pnlButtons.add(new JLabel());
            pnlButtons.add(new JLabel());
            
            pnlButtons.add(getJBtnRemoveLayer());
            pnlButtons.add(getJBtnEditLegend());
            pnlButtons.add(getJBtnCancel());
        }
        return pnlButtons;
    }

    /**
     * Obtiene el MapControl asociado al localizador
     * 
     * @return MapControl
     */
    public MapControl getMapCtrl() {
        return mapCtrl;
    }

    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }
    
    
    class RefreshThread extends Thread {
        
        private int waitt = 0;
        
        public RefreshThread(int t) {
            waitt = t;
        }
        public void run() {
            
            try {
                /*
                 * We might need to wait a bit because the GUI thread needs
                 * some time to update (this is because we have used a modal dialog
                 * and we refresh after that dialog is closed)
                 */
                Thread.sleep(waitt);
            } catch (Exception exc) {
                
            }
            
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                        refreshList();
                        updateControls(null);
                    } catch (Exception exc) {
                        logger.info("Error while refreshing components.", exc);
                    }
                }
            });
        }
    }

}

// @jve:visual-info decl-index=0 visual-constraint="10,10"
