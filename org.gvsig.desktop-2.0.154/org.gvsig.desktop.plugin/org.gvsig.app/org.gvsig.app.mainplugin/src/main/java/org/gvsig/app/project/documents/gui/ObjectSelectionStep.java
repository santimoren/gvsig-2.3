/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import org.gvsig.utils.swing.objectSelection.ObjectSelection;
import org.gvsig.utils.swing.wizard.Step;
import org.gvsig.utils.swing.wizard.WizardControl;

/**
 * Control ObjectSelection como paso de un asistente
 * 
 * @author Fernando Gonz�lez Cort�s
 */
public class ObjectSelectionStep extends ObjectSelection implements Step {

    private static final long serialVersionUID = 1721757595450180255L;
    private WizardControl w;

    /**
     * @see org.gvsig.utils.swing.wizard.Step#init(org.gvsig.utils.swing.wizard.Wizard)
     */
    public void init(WizardControl w) {
        this.w = w;
    }

    /**
     * @see org.gvsig.utils.swing.objectSelection.ObjectSelection#seleccion()
     */
    protected void seleccion() {
        if (w != null) {
            w.enableNext(getCombo().getSelectedIndex() != -1);
        }
    }

    /**
     * Obtiene el elemento seleccionado
     * 
     * @return
     */
    public Object getSelectedItem() {
        return getCombo().getSelectedItem();
    }
}
