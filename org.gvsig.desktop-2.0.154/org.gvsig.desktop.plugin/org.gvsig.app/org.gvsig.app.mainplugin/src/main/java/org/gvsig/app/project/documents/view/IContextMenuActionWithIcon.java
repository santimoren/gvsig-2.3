package org.gvsig.app.project.documents.view;

import javax.swing.ImageIcon;


public interface IContextMenuActionWithIcon extends IContextMenuAction {
 
    public ImageIcon getIcon();
}
