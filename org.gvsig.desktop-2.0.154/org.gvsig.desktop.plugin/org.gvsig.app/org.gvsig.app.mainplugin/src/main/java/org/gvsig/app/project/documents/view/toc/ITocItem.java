/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view.toc;

import java.awt.Dimension;
import java.awt.datatransfer.Transferable;

import javax.swing.Icon;

import org.gvsig.app.project.documents.view.IContextMenuAction;


/**
 * @author FJP
 * Siguiendo el Patr�n COMPOSITE: la rama ser� la capa (FLayer) y la
 * hoja ser� el s�mbolo (FStyle2D)
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public interface ITocItem extends Transferable {
	String getLabel();
	Icon getIcon();
	Dimension getSize();
	void setSize(Dimension sz);
	public IContextMenuAction getDoubleClickAction();
	
}
