/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.exceptions.ReloadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.tools.persistence.PersistentState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/* CVS MESSAGES:
 *
 * $Id: LayersUngroupTocMenuEntry.java 39575 2013-01-09 12:28:47Z jldominguez $
 * $Log$
 * Revision 1.6  2007-01-04 07:24:31  caballero
 * isModified
 *
 * Revision 1.5  2006/10/02 13:52:34  jaume
 * organize impots
 *
 * Revision 1.4  2006/09/25 15:24:26  jmvivo
 * * Modificada la implementacion.
 *
 * Revision 1.3  2006/09/20 12:01:24  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/09/20 11:41:20  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2006/09/15 10:41:30  caballero
 * extensibilidad de documentos
 *
 * Revision 1.1  2006/09/12 15:58:14  jorpiell
 * "Sacadas" las opcines del menú de FPopupMenu
 *
 *
 */
/**
 * Realiza una desagrupación de capas, a partir de las capas que se encuentren activas.
 *
 * @author Vicente Caballero Navarro
 */
public class LayersUngroupTocMenuEntry extends AbstractTocContextMenuAction {
	/**
	 * Useful for debug the problems during the implementation.
	 */
	private static Logger logger = LoggerFactory.getLogger(LayersUngroupTocMenuEntry.class);

	public String getGroup() {
		return "group4"; //FIXME
	}

	public int getGroupOrder() {
		return 40;
	}

	public int getOrder() {
		return 1;
	}

	public String getText() {
		return PluginServices.getText(this, "desagrupar_capas");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return true;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		FLayer lyr = getNodeLayer(item);
		if (!(lyr instanceof FLayers) || (lyr instanceof FLayers && lyr.getParentLayer() == null)){
			return false;
		}
		return true;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)){
			FLayers agrupa = (FLayers)getNodeLayer(item);
			FLayers parent=agrupa.getParentLayer();

			if (parent!=null){
				getMapContext().beginAtomicEvent();

				FLayer[] layers = new FLayer[agrupa.getLayersCount()];
				for(int i=0; i<agrupa.getLayersCount(); i++){
					layers[i] = agrupa.getLayer(i);
				}
				 try {
					 PersistentState lyrs_state = CopyPasteLayersUtils.getAsFLayersPersistentState(
							 layers, this.getMapContext());
					 CopyPasteLayersUtils.saveToClipboard(lyrs_state);
					 CopyPasteLayersUtils.removeLayers(
							 layers, this.getMapContext());
					 FLayers resp = CopyPasteLayersUtils.getClipboardAsFLayers();
					 CopyPasteLayersUtils.addLayers(resp, parent);
				 } catch (Exception e) {
					 logger.error("Error ungrouping layers. Can't copy layers outside the group");
				 }
				parent.removeLayer(agrupa);
				getMapContext().endAtomicEvent();

				// TRUCO PARA REFRESCAR.
				getMapContext().invalidate();
				Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
				project.setModified(true);
			}
		}
	}

    /**
     * Tells the starting position of children when FLayers provided is ungrouped
     * @param agrupa
     * @return
     */
    private int getPositionForUngrouped(FLayers lyr) {
        
        FLayers pare = lyr.getParentLayer();
        for (int i=0; i<pare.getLayersCount(); i++) {
            if (pare.getLayer(i) == lyr) {
                return i;
            }
        }
        return 0;
    }
}
