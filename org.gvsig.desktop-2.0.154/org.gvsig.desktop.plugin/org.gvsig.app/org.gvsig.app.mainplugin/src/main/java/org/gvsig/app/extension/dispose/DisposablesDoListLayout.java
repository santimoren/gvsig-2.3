package org.gvsig.app.extension.dispose;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;


public class DisposablesDoListLayout extends JPanel
{
   JButton disposeButton = new JButton();
   JButton closeButton = new JButton();
   JButton disposeAllButton = new JButton();
   JLabel messageLabel = new JLabel();
   JButton refreshButton = new JButton();
   JSplitPane splitPanel = new JSplitPane();

   /**
    * Default constructor
    */
   public DisposablesDoListLayout()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      JLabel jlabel1 = new JLabel();
      jlabel1.setText("Disposables do list");
      jpanel1.add(jlabel1,cc.xywh(2,2,8,1));

      disposeButton.setActionCommand("Dispose");
      disposeButton.setEnabled(false);
      disposeButton.setName("disposeButton");
      disposeButton.setText("Dispose");
      jpanel1.add(disposeButton,cc.xy(7,6));

      closeButton.setActionCommand("Close");
      closeButton.setName("closeButton");
      closeButton.setText("Close");
      jpanel1.add(closeButton,cc.xy(9,6));

      disposeAllButton.setActionCommand("Dispose all");
      disposeAllButton.setName("disposeAllButton");
      disposeAllButton.setText("Dispose all");
      jpanel1.add(disposeAllButton,cc.xy(5,6));

      messageLabel.setName("messageLabel");
      jpanel1.add(messageLabel,cc.xy(2,6));

      refreshButton.setActionCommand("Refresh");
      refreshButton.setName("refreshButton");
      refreshButton.setText("Refresh");
      jpanel1.add(refreshButton,cc.xy(3,6));

      splitPanel.setDividerLocation(120);
      splitPanel.setLastDividerLocation(-1);
      splitPanel.setName("splitPanel");
      splitPanel.setOneTouchExpandable(true);
      splitPanel.setOrientation(0);
      jpanel1.add(splitPanel,cc.xywh(2,4,8,1));

      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5,6,7,8,9,10 },new int[]{ 1,2,3,4,5,6,7 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
