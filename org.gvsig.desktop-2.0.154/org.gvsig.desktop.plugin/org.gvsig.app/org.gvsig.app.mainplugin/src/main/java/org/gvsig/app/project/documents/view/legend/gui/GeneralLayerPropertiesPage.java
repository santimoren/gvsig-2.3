package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import javax.swing.JComponent;
import org.apache.commons.lang3.StringUtils;
import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneralLayerPropertiesPage extends GeneralLayerPropertiesPageView implements PropertiesPage {

    private static final Logger logger = LoggerFactory.getLogger(GeneralLayerPropertiesPage.class);
    private FLayer layer = null;

    public static class GeneralLayerPropertiesPageFactory implements PropertiesPageFactory {

        @Override
        public String getGroupID() {
            return ViewDocument.LAYER_PROPERTIES_PAGE_GROUP;
        }

        @Override
        public boolean isVisible(Object obj) {
            return obj instanceof FLayer;
        }

        @Override
        public PropertiesPage create(Object obj) {
            return new GeneralLayerPropertiesPage((FLayer) obj);
        }

    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public GeneralLayerPropertiesPage(FLayer layer) {
        this.initComponents();
        this.setLayer(layer);
    }

    private void initComponents() {
        this.rdoShowAlways.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                txtIsGreaterThan.setEnabled(false);
                txtIsLessThan.setEnabled(false);
            }
        });
        this.rdoShowConditionedToTheScale.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                txtIsGreaterThan.setEnabled(true);
                txtIsLessThan.setEnabled(true);
            }
        });
        this.translate();
    }

    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        this.lblName.setText(i18nManager.getTranslation("Nombre") + ":");
        this.lblRangeOfScales.setText(i18nManager.getTranslation("rango_de_escalas"));
        this.lblIsGreaterThan.setText(i18nManager.getTranslation("este_por_encima_de"));
        this.lblIsLessThan.setText(i18nManager.getTranslation("este_por_debajo_de_"));
        this.lblFullScale.setText(i18nManager.getTranslation("escala_maxima"));
        this.lblMinumunScale.setText(i18nManager.getTranslation("escala_minima"));
        this.lblProperties.setText(i18nManager.getTranslation("propiedades"));
        this.rdoShowAlways.setText(i18nManager.getTranslation("Mostrar_siempre"));
        this.rdoShowConditionedToTheScale.setText(i18nManager.getTranslation("No_mostrar_la_capa_cuando_la_escala") + ":");

        this.lblSourceType.setText(i18nManager.getTranslation("Source_type"));
        this.lblSource.setText(i18nManager.getTranslation("origen"));
        this.lblDataSource.setText(i18nManager.getTranslation("Data_source"));
    }

    @Override
    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("General");
    }

    @Override
    public int getPriority() {
        return 1000;
    }

    @Override
    public boolean whenAccept() {
        this.whenApply();
        return true;
    }

    @Override
    public boolean whenApply() {
        NumberFormat nf = NumberFormat.getInstance();

        if (this.rdoShowConditionedToTheScale.isSelected()) {
            try {
                String s = this.txtIsGreaterThan.getText();
                if (StringUtils.isEmpty(s)) {
                    layer.setMinScale(-1);
                } else {
                    layer.setMinScale((nf.parse(s)).doubleValue());
                }
            } catch (ParseException ex) {
                this.txtIsGreaterThan.setText("");
            }
            try {
                String s = this.txtIsLessThan.getText();
                if (StringUtils.isEmpty(s)) {
                    layer.setMaxScale(-1);
                } else {
                    layer.setMaxScale((nf.parse(s)).doubleValue());
                }
            } catch (ParseException ex) {
                this.txtIsLessThan.setText("");
            }
        } else {
            layer.setMinScale(-1);
            layer.setMaxScale(-1);
        }
        if (!this.txtName.getText().equals(layer.getName())) {
            layer.setName(this.txtName.getText());
        }
        return true;
    }

    @Override
    public boolean whenCancel() {
        // Do nothing
        return true;
    }

    @Override
    public JComponent asJComponent() {
        return this;
    }

    public void setLayer(FLayer layer) {
        NumberFormat nf = NumberFormat.getInstance();

        this.layer = layer;

        this.txtName.setText(this.layer.getName());
        this.txtProperties.setText(this.getLayerInfo());

        if (layer.getMinScale() != -1) {
            this.txtIsGreaterThan.setText(nf.format(layer.getMinScale()));
        }
        if (layer.getMaxScale() != -1) {
            this.txtIsLessThan.setText(nf.format(layer.getMaxScale()));
        }
        if (layer.getMinScale() == -1 && layer.getMaxScale() == -1) {
            this.rdoShowConditionedToTheScale.setSelected(false);
        } else {
            this.rdoShowConditionedToTheScale.setSelected(true);
        }
        if (layer instanceof SingleLayer) {
            DataStore store = ((SingleLayer) layer).getDataStore();
            this.txtSourceType.setText(store.getProviderName());
            DataStoreParameters parameters = store.getParameters();
            if (parameters instanceof FilesystemStoreParameters) {
                this.txtSource.setText(
                        ((FilesystemStoreParameters) parameters).getFile().getAbsolutePath()
                );
            } else {
                this.txtSource.setText(store.getFullName());
            }
        } else {
            this.txtSourceType.setText("");
            this.txtSource.setText("");
        }
    }

    private String getLayerInfo() {
        String info;
        StringBuilder buff = new StringBuilder();
        try {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            info = layer.getInfoString();
            if (info == null) {
                Envelope fullExtentViewPort = layer.getFullEnvelope();
                IProjection mapProjection = layer.getMapContext().getProjection();
                buff.append(i18nManager.getTranslation("Extent"));
                buff.append(" ");
                buff.append(mapProjection.getAbrev());
                buff.append(" (")
                        .append(i18nManager.getTranslation("view_projection"))
                        .append("):\n\t");
                buff.append(i18nManager.getTranslation("Superior"))
                        .append(":\t")
                        .append(fullExtentViewPort.getMaximum(1))
                        .append("\n\t");
                buff.append(i18nManager.getTranslation("Inferior"))
                        .append(":\t")
                        .append(fullExtentViewPort.getMinimum(1))
                        .append("\n\t");
                buff.append(i18nManager.getTranslation("Izquierda"))
                        .append(":\t")
                        .append(fullExtentViewPort.getMinimum(0))
                        .append("\n\t");
                buff.append(i18nManager.getTranslation("Derecha"))
                        .append(":\t")
                        .append(fullExtentViewPort.getMaximum(0))
                        .append("\n");

                if (layer.getProjection() != null
                        && !layer.getProjection().getAbrev().equals(
                                mapProjection.getAbrev())) {
                    IProjection nativeLayerProj = layer.getProjection();
                    ICoordTrans ct = mapProjection.getCT(nativeLayerProj);
                    Envelope nativeLayerExtent = fullExtentViewPort.convert(ct);
                    buff.append(i18nManager.getTranslation("Extent"))
                            .append(" ");
                    buff.append(nativeLayerProj.getAbrev());
                    buff.append(" (")
                            .append(i18nManager.getTranslation("layer_native_projection"))
                            .append("):\n\t");
                    buff.append(i18nManager.getTranslation("Superior"))
                            .append(":\t")
                            .append(nativeLayerExtent.getMaximum(1))
                            .append("\n\t");
                    buff.append(i18nManager.getTranslation("Inferior"))
                            .append(":\t")
                            .append(nativeLayerExtent.getMinimum(1))
                            .append("\n\t");
                    buff.append(i18nManager.getTranslation("Izquierda"))
                            .append(":\t")
                            .append(nativeLayerExtent.getMinimum(0))
                            .append("\n\t");
                    buff.append(i18nManager.getTranslation("Derecha"))
                            .append(":\t")
                            .append(nativeLayerExtent.getMaximum(0))
                            .append("\n");
                }

                if (layer instanceof FLyrVect) {
                    try {
                        GeometryType geomType = ((FLyrVect) layer).getTypeVectorLayer();
                        buff.append("\n")
                                .append(i18nManager.getTranslation("type"))
                                .append(": ")
                                .append(geomType.getFullName())
                                .append("\n");
                    } catch (Exception e) {
                        logger.warn("Can't get gemetry type for layer '" + layer.getName() + "'.", e);
                        buff.append("\n")
                                .append(i18nManager.getTranslation("type"))
                                .append(": ")
                                .append("unknow")
                                .append("\n");
                    }

                }
                info = buff.toString();
            }
        } catch (Exception e) {
            logger.warn("Can't get layer information.", e);
            info = buff.toString();
        }
        return info;
    }

}
