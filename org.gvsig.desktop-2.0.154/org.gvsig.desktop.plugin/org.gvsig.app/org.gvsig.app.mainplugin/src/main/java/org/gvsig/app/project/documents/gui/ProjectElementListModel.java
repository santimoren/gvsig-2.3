/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import java.util.ArrayList;

import javax.swing.AbstractListModel;

/**
 * Modelo para presentar los ProjectElement's (vistas, mapas, tablas) en un
 * JList
 * 
 * @author Fernando Gonz�lez Cort�s
 */
public class ProjectElementListModel extends AbstractListModel {

    private static final long serialVersionUID = 5224594892273078623L;
    private ArrayList els;

    /**
     * Creates a new MyListModel object.
     * 
     * @param els
     *            Elementos del modelo
     */
    public ProjectElementListModel(ArrayList els) {
        this.els = els;
    }

    /**
     * Obtiene el tama�o de la lista
     * 
     * @return tama�o de la lista
     */
    public int getSize() {
        if (els == null) {
            return 0;
        }

        return els.size();
    }

    /**
     * @see javax.swing.ListModel#getElementAt(int)
     */
    public Object getElementAt(int index) {
        if (els == null) {
            return "";
        }

        return els.get(index);
    }
}
