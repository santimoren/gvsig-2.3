/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.app;

import org.gvsig.app.project.ProjectManager;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanelManager;
import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

public class ApplicationLocator extends BaseLocator {

	private static final String LOCATOR_NAME = "gvSIGApplicationLocator";

	public static final String APPGVSIG_MANAGER_NAME = LOCATOR_NAME+".manager";
	private static final String APPGVSIG_MANAGER_DESCRIPTION = "Manager of gvSIG application";
	
	public static final String FILESYSTEMEXPLORERPROPERTIESPANEL_MANAGER_NAME = "FilesystemExplorerPropertiesPanel.manager";
	private static final String FILESYSTEMEXPLORERPROPERTIESPANEL_MANAGER_DESCRIPTION = "Manager for FilesystemExplorerPropertiesPanel";
	
	/**
	 * Unique instance.
	 */
	private static final ApplicationLocator instance = new ApplicationLocator();

	/**
	 * Return the singleton instance.
	 *
	 * @return the singleton instance
	 */
	public static ApplicationLocator getInstance() {
		return instance;
	}

	public String getLocatorName() {
		return LOCATOR_NAME;
	}

	/**
	 * Return a reference to PersistenceManager.
	 *
	 * @return a reference to PersistenceManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static ApplicationManager getManager()
			throws LocatorException {
		return (ApplicationManager) getInstance().get(APPGVSIG_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the PersistenceManager interface.
	 *
	 * @param clazz
	 *            implementing the PersistenceManager interface
	 */
	@SuppressWarnings("unchecked")
	public static void registerApplicationManager(Class<? extends ApplicationManager> clazz) {
		getInstance().register(APPGVSIG_MANAGER_NAME,
				APPGVSIG_MANAGER_DESCRIPTION, clazz);
	}

	@SuppressWarnings("unchecked")
	public static void registerDefaultApplicationManager(Class<? extends ApplicationManager> clazz) {
		getInstance().registerDefault(APPGVSIG_MANAGER_NAME,
				APPGVSIG_MANAGER_DESCRIPTION, clazz);
	}
	
	public static ProjectManager getProjectManager() throws LocatorException {
		return ProjectManager.getInstance();
	}

	public static void registerFilesystemExplorerPropertiesPanelManager(Class<? extends FilesystemExplorerPropertiesPanelManager> clazz) {
		getInstance().register(FILESYSTEMEXPLORERPROPERTIESPANEL_MANAGER_NAME,
				FILESYSTEMEXPLORERPROPERTIESPANEL_MANAGER_DESCRIPTION, clazz);
	}

	public static FilesystemExplorerPropertiesPanelManager getFilesystemExplorerPropertiesPanelManager()
			throws LocatorException {
		return (FilesystemExplorerPropertiesPanelManager) getInstance().get(FILESYSTEMEXPLORERPROPERTIESPANEL_MANAGER_NAME);
	}

}
