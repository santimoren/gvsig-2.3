/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.exceptions;

import java.util.Map;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.utils.XMLException;


public class OpenException extends XMLException {
	private Exception e;
	private String c;
	public OpenException(Exception e,String c) {
		this.e=e;
		this.c=c;
	}
	public void showError(){
		NotificationManager.addError(PluginServices.getText(this,"abrir_proyecto"),e);
	}
	public void showInfo(){
		NotificationManager.addInfo(PluginServices.getText(this,"abrir_proyecto"),e);
	}
	public void showWarning(){
		NotificationManager.addWarning(PluginServices.getText(this,"abrir_proyecto"),e);
	}
	protected Map values() {
		// TODO Auto-generated method stub
		return null;
	}
	public void showMessageError(String name) {
		String message=PluginServices.getText(this,"error_opening_the_document")+": "+name;
		NotificationManager.showMessageError(message,e);

	}
}
