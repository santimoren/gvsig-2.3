/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.i18n.Messages;

/**
 * Di�logo para cambiar el nombre de una capa.
 * 
 * @author Vicente Caballero Navarro
 * @author gvSIG Team
 */
public class ChangeName extends JPanel implements IWindow {

    private static final long serialVersionUID = -1366865045507066336L;
    private JLabel lblName = null;
    private JTextField txtName = null;
    private String name;
    private AcceptCancelPanel jPanelAcceptCancel = null;
    private boolean isAccepted = false;

    /**
     * This is the default constructor
     * 
     * @param layout
     *            Referencia al Layout.
     * @param fframe
     *            Referencia al fframe de imagen.
     */
    public ChangeName(String n) {
        super();
        if (n == null) {
            name = PluginServices.getText(this, "agrupacion");
        } else {
            name = n;
        }
        initialize();
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder(4, 5, 4, 5));

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(2, 5, 2, 5);
        c.gridy = -1;

        c.anchor = GridBagConstraints.WEST;
        c.weightx = 0.0d;
        c.gridx = 0;
        c.gridy++;
        add(getLblName(), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.gridx = 1;
        add(getTxtName(), c);

        c.anchor = GridBagConstraints.EAST;
        c.gridx = 0;
        c.gridwidth = 2;
        c.gridy++;
        c.weightx = 1.0d;
        add(getJPanelAcceptCancel(), c);
    }

    /**
     * This method initializes lFichero
     * 
     * @return javax.swing.JLabel
     */
    private JLabel getLblName() {
        if (lblName == null) {
            lblName = new JLabel(Messages.getText("nombre"));
        }

        return lblName;
    }

    /**
     * This method initializes tFichero
     * 
     * @return javax.swing.JTextField
     */
    private JTextField getTxtName() {
        if (txtName == null) {
            txtName = new JTextField(name, 20);
            txtName.addKeyListener(new KeyListener() {

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        acceptAction();
                    } else
                        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                            cancelAction();
                        }
                }

                public void keyReleased(KeyEvent e) {
                    // Nothing to do
                }

                public void keyTyped(KeyEvent e) {
                    // Nothing to do
                }

            });
        }
        return txtName;
    }

    /**
     * Inserta el path al TFichero.
     * 
     * @param val
     *            path del fichero.
     */
    public void setText(String val) {
        getTxtName().setText(val);
    }

    public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo = new WindowInfo(WindowInfo.MODALDIALOG);
        m_viewinfo.setTitle(Messages.getText("cambio_nombre"));
        Dimension dim = getPreferredSize();
        m_viewinfo.setWidth(dim.width);
        m_viewinfo.setHeight(dim.height);
        return m_viewinfo;
    }

    public String getName() {
        return name;
    }

    /**
     * This method initializes jPanel1
     * 
     * @return javax.swing.JPanel
     */
    private AcceptCancelPanel getJPanelAcceptCancel() {
        if (jPanelAcceptCancel == null) {
            ActionListener okActionListener, cancelActionListener;
            okActionListener = new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    acceptAction();
                }
            };
            cancelActionListener = new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    cancelAction();
                }
            };
            jPanelAcceptCancel =
                new AcceptCancelPanel(okActionListener, cancelActionListener);
        }
        return jPanelAcceptCancel;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    protected void acceptAction() {
        name = getTxtName().getText();
        isAccepted = true;
        PluginServices.getMDIManager().closeWindow(ChangeName.this);

    }

    protected void cancelAction() {
        isAccepted = false;
        PluginServices.getMDIManager().closeWindow(ChangeName.this);

    }

    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }

}
