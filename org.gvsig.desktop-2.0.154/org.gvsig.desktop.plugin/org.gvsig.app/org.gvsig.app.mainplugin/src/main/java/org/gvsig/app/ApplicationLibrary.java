/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.app.extension.InitializeApplicationExtension;
import org.gvsig.app.gui.preferencespage.AppSymbolPreferences;
import org.gvsig.app.project.DefaultProject;
import org.gvsig.fmap.mapcontext.MapContextLibrary;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontrol.MapControlLibrary;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLibrary;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 2008-2009 jmvivo
 * @author 2009- jjdelcerro
 * 
 */
public class ApplicationLibrary extends AbstractLibrary {

    private static final Logger logger =
        LoggerFactory.getLogger(ApplicationLibrary.class);
    
    @Override
    public void doRegistration() {
        registerAsAPI(ApplicationLibrary.class);
        require(MapContextLibrary.class);
        require(MapControlLibrary.class);
        require(InstallerLibrary.class);
    }
	
	@Override
	protected void doInitialize() throws LibraryException {
		// Do nothing
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		ApplicationManager manager = ApplicationLocator.getManager();
		if (manager == null) {
			throw new ReferenceNotRegisteredException(
					ApplicationLocator.APPGVSIG_MANAGER_NAME, ApplicationLocator
							.getInstance());
		}
		DefaultProject.registerPersistent();

                MapContextManager mapContextManager = MapContextLocator.getMapContextManager();

                PluginServices plugin = PluginsLocator.getManager().getPlugin(InitializeApplicationExtension.class);
                mapContextManager.setColorTableLibraryFolder(new File(plugin.getPluginHomeFolder(),"colortable"));
                
		MapContextLocator.getSymbolManager().setSymbolPreferences(new AppSymbolPreferences());
		
		installBaseSymbols();
	}

	/**
	 * Copy symbol folders from this plugin's 'Symbols' folder
	 * to user's 'Symbols' folder unless a folder with same name
	 * exists.
	 */
	private void installBaseSymbols() {
		PluginServices ps = PluginsLocator.getManager().getPlugin(InitializeApplicationExtension.class);
		File from_folder = new File( ps.getPluginDirectory(), "Symbols");
		String to_folder = MapContextLocator.getSymbolManager().
			    getSymbolPreferences().getSymbolLibraryPath();
		try {
		    copyMissingFolders(from_folder, new File(to_folder));
		} catch (IOException ioe) {
		    ApplicationLocator.getManager().message(
		        Messages.getText("_Unable_to_copy_symbols_from_main_plugin"),
		        JOptionPane.ERROR_MESSAGE);
		}
	
	}
	
    private void copyMissingFolders(File fromf, File tof)
        throws IOException {
        
        if (fromf == null || tof == null) {
            return;
        }
        
        if (!fromf.exists() || !fromf.isDirectory() ||
            (tof.exists() && tof.isFile())) {
            return;
        }
        
        if (!tof.exists()) {
            tof.mkdirs();
        }
        
        File[] from_subs = fromf.listFiles();
        for (int i=0; i<from_subs.length; i++) {
            if (!folderIsOneIn(from_subs[i], tof)) {
                logger.info("Copying symbols subfolder: " + from_subs[i].getName());
                FileUtils.copyDirectoryToDirectory(from_subs[i], tof);
            } else {
                logger.info("Symbols subfolder already exists: " + from_subs[i].getName());
            }
        }
    }


    /**
     * Tells whether <folder> is a folder and <tofolder>
     * has a file or subfolder with the same short name as 
     * <folder>
     *  
     */
    private boolean folderIsOneIn(File folder, File tofolder) {
        
        if (!folder.isDirectory()) {
            return false;
        }
        
        String name = folder.getName();
        String tname = tofolder.getAbsolutePath() + File.separator + name;
        File tfile = new File(tname);
        return tfile.exists();
    }


}