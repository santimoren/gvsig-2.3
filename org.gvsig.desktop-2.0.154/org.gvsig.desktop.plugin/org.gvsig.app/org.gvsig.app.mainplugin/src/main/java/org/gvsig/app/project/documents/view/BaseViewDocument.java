/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.app.project.documents.view.info.gui.HTMLInfoToolPanel;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.events.ErrorEvent;
import org.gvsig.fmap.mapcontext.events.listeners.ErrorListener;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesHelper;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 *
 * @author 2005- Vicente Caballero
 * @author 2009- Joaquin del Cerro
 *
 */
public abstract class BaseViewDocument extends AbstractDocument implements ErrorListener,
        ViewDocument {

    /**
     *
     */
    private static final long serialVersionUID = -2621709089720665902L;

    public static final String PERSISTENCE_DEFINITION_NAME = "BaseViewDocument";

    protected MapContext mapOverViewContext;
    protected MapContext mapContext;

    private ExtendedPropertiesHelper propertiesHelper = new ExtendedPropertiesHelper();

    public BaseViewDocument() {
        super();
    }

    public BaseViewDocument(DocumentManager factory) {
        super(factory);
    }

    /**
     * Gets the MapContext of the main map in the view.
     *
     * @return the main MapContext
     */
    public MapContext getMapContext() {
        return mapContext;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        this.mapContext.getLayers().setName(name);
    }

    
    /**
     * Gets the MapContext from the locator, which is the small map in the
     * left-bottom corner of the View.
     *
     * @return the locator MapContext
     */
    public MapContext getMapOverViewContext() {
        return mapOverViewContext;
    }

    public void setMapContext(MapContext fmap) {
        mapContext = fmap;
        fmap.addErrorListener(this);
    }

    public void setMapOverViewContext(MapContext fmap) {
        mapOverViewContext = fmap;
        mapOverViewContext.setProjection(mapContext.getProjection());
    }

    public void showErrors() {
        if (mapContext.getLayersError().size() > 0) {
            String layersError = "";
            for (int i = 0; i < mapContext.getLayersError().size(); i++) {
                layersError = layersError + "\n" + (String) mapContext.getLayersError().get(i);
            }
            JOptionPane.showMessageDialog((Component) PluginServices.getMainFrame(),
                    PluginServices.getText(this, "fallo_capas") + " : \n"
                    + layersError);
        }
    }

    /**
     * Reports to the user a bundle of driver exceptions produced in the same
     * atomic MapContext transaction
     */
    @SuppressWarnings("rawtypes")
    public void reportDriverExceptions(String introductoryText, List driverExceptions) {
        HtmlWindow htmlPanel = new HtmlWindow(570, 600, PluginServices.getText(this, "driver_error"));
        String htmlText = "";
        if (introductoryText == null) {
            htmlText += "<h2 text=\"#000080\">" + PluginServices.getText(this, "se_han_producido_los_siguientes_errores_durante_la_carga_de_las_capas") + "</h2>";
        } else {
            htmlText += introductoryText;
        }
        int numErrors = driverExceptions.size();
        for (int i = 0; i < numErrors; i++) {
            //htmlText += "<br>\n";
            BaseException exception = (BaseException) driverExceptions.get(i);
            htmlText += "<p text=\"#550080\">_________________________________________________________________________________________</p>";
            htmlText += "<h3>" + PluginServices.getText(this, exception.getMessageKey()) + "</h3>";
            htmlText += "<p>" + exception.getMessage() + "</p>";
            htmlText += "<p text=\"#550080\">_________________________________________________________________________________________</p>";
        }

        System.out.println(htmlText);
        htmlPanel.show(htmlText);

        PluginServices.getMDIManager().addCentredWindow(htmlPanel);

    }

    /**
     * HtmlInfoToolPanel that implements IWindow
     *
     * @author azabala
     *
     */
    class HtmlWindow extends JPanel implements IWindow {

        /**
         *
         */
        private static final long serialVersionUID = 1151465547277478664L;

        private HTMLInfoToolPanel htmlPanel = new HTMLInfoToolPanel();
        WindowInfo viewInfo = null;

        public HtmlWindow(int width, int height, String windowTitle) {
            htmlPanel.setBackground(Color.white);
            JScrollPane scrollPane = new JScrollPane(htmlPanel);
            scrollPane.setPreferredSize(new Dimension(width - 30, height - 30));
            this.add(scrollPane);
            viewInfo = new WindowInfo(WindowInfo.MODELESSDIALOG);
            viewInfo.setTitle(windowTitle);
            viewInfo.setWidth(width);
            viewInfo.setHeight(height);
        }

        public void show(String htmlText) {
            htmlPanel.show(htmlText);
        }

        public WindowInfo getWindowInfo() {
            return viewInfo;
        }

        public Object getWindowProfile() {
            return WindowInfo.PROPERTIES_PROFILE;
        }

    }

    public IProjection getProjection() {
        return mapContext.getProjection();
    }

    public void setProjection(IProjection proj) {
        mapContext.setProjection(proj);
        mapOverViewContext.setProjection(proj);
    }

    public IProjection getOverViewProjection() {
        return mapOverViewContext.getProjection();
    }

    public void afterRemove() {
        // FIXME: Parece que no recorre correctamente el arbol de capas

        FLayers layers = this.getMapContext().getLayers();

        for (int i = layers.getLayersCount() - 1; i >= 0; i--) {
            try {
                layers.getLayer(i).getParentLayer().removeLayer(layers.getLayer(i));
            } catch (CancelationException e1) {
                e1.printStackTrace();
            }
        }
        getMapContext().dispose();
        getMapOverViewContext().dispose();
    }

    public void afterAdd() {
        // Do nothing
    }

    public void setBackColor(Color c) {
        getMapContext().getViewPort().setBackColor(c);
    }

    public void errorThrown(ErrorEvent e) {
        JOptionPane.showMessageDialog((Component) PluginServices.getMainFrame(),
                PluginServices.getText(this, "fallo_capas") + " : \n"
                + e.getMessage());

    }

    public boolean isLocked() {
        if (super.isLocked()) {
            return true;
        }
        FLayers layers = getMapContext().getLayers();
        for (int i = 0; i < layers.getLayersCount(); i++) {
            FLayer layer = layers.getLayer(i);
            if (layer.isEditing()) {
                return true;
            }
        }
        return false;
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        super.saveToState(state);
        state.set("mainMapContext", this.getMapContext());
        if (this.getMapOverViewContext() != null) {
            state.set("useMapOverview", true);
        } else {
            state.set("useMapOverview", false);
        }
        state.set("overviewMapContext", this.getMapOverViewContext());
        state.set("propertiesHelper", propertiesHelper);
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        super.loadFromState(state);
        this.mapContext = (MapContext) state.get("mainMapContext");
        if (state.getBoolean("useMapOverview")) {
            this.mapOverViewContext = (MapContext) state.get("overviewMapContext");
        } else {
            this.mapOverViewContext = null;
        }
        this.propertiesHelper = (ExtendedPropertiesHelper) state.get("propertiesHelper");
    }

    public Object getProperty(Object key) {
        return this.propertiesHelper.getProperty(key);
    }

    public void setProperty(Object key, Object obj) {
        this.propertiesHelper.setProperty(key, obj);
    }

    public Map getExtendedProperties() {
        return this.propertiesHelper.getExtendedProperties();
    }

    public Iterator<FLayer> iterator() {
        return this.mapContext.iterator();
    }

    public Iterator<FLayer> deepiterator() {
        return this.mapContext.deepiterator();
    }

    public boolean contains(FLayer layer) {
        if( !(layer instanceof SingleLayer) ) {
            return false;
        }
        return this.isInLayers(this.getMapContext().getLayers(), ((SingleLayer)layer).getDataStore());
    }
    
    public boolean contains(DataStore store) {
        return this.isInLayers(this.getMapContext().getLayers(), store);
    }
    
    private boolean isInLayers(FLayers layers, DataStore store) {
        for (int i = 0; i < layers.getLayersCount(); i++) {
            if (layers.getLayer(i) instanceof FLayers) {
                if (isInLayers((FLayers) layers.getLayer(i), store)) {
                    return true;
                }
            }
            FLayer layer = layers.getLayer(i);
            if( layer instanceof SingleLayer ) {
                if (((SingleLayer)layer).getDataStore() == store ) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void center(Envelope envelope) {
        this.getMapContext().getViewPort().setEnvelope(envelope);
    }
}
