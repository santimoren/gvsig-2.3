/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Utility class to deal with legend files (sld, gvsleg, etc)
 */
public class LegendFileFilter extends FileFilter {

    private String fileExt = "";

    private String description = "";

    private I18nManager i18n = ToolsLocator.getI18nManager();

    public LegendFileFilter(String ext, String desc) {
        fileExt = ext.toLowerCase();
        description = desc;
    }

    public LegendFileFilter(String ext) {

        StringBuilder sb = new StringBuilder();
        fileExt = ext.toLowerCase();

        description = i18n.getTranslation("format_for_legends");
        description = StringUtils.replace(description, "%(ext)",
                ext.toUpperCase());

        sb.append(description);
        sb.append(" (*." + ext.toLowerCase() + ")");
        description = sb.toString();
    }

    public String getFileExtension() {
        return fileExt;
    }

    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            return true;
        }
        else {
            return pathname.getAbsolutePath().toLowerCase()
                    .endsWith("." + fileExt);
        }
    }

    public String getDescription() {
        return description;
    }

}
