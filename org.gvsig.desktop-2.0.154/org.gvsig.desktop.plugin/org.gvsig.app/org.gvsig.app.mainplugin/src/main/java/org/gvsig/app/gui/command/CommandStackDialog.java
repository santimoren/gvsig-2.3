/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.command;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.gui.beans.DefaultBean;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.undo.UndoRedoInfo;
import org.gvsig.tools.undo.UndoRedoStack;


@SuppressWarnings("serial")
public class CommandStackDialog extends DefaultBean implements SingletonWindow, IWindowListener, Observer{

	private JTable commandTable = null;
	private JPanel topPanel = null;
	private UndoRedoStack undoRedoStack;
	private JSlider commandSlider = null;
	
	private int lowLimit;
	private int currentValue = -1;
	private int currentSliderValue = -1;
	private JPanel sliderPanel = null;
	protected boolean refreshing;
	private JPanel centerPanel = null;
	private JScrollPane jScrollPane = null;
	private JPanel tablePanel = null;
	
	private static final ImageIcon imodify = IconThemeHelper.getImageIcon("edit-undo-redo-actions-modify"); 
	private static final ImageIcon iadd = IconThemeHelper.getImageIcon("edit-undo-redo-actions-add");
	private static final ImageIcon idel = IconThemeHelper.getImageIcon("edit-undo-redo-actions-delete");

	private CommandTableModel commandTableModel = null;
	/**
	 * This is the default constructor
	 */
	public CommandStackDialog() {
		super();
		initialize();
	}
	
	public void setModel(UndoRedoStack cr1) {
        if (this.undoRedoStack != null) {
            if (this.undoRedoStack.equals(cr1)) {
                return;
            } else {
                this.undoRedoStack.deleteObserver(this);
            }
		}
		this.undoRedoStack = cr1;
		this.undoRedoStack.addObserver(this);
		initTable();
		initSlider();
		currentValue = commandTableModel.getPos();
    	refreshControls();
		refreshSliderSize();
	}
	
	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setLayout(new BorderLayout());
		this.setSize(328, 229);		
		this.add(getJScrollPane(), java.awt.BorderLayout.CENTER);
	}

	/**
	 * This method initializes jList
	 *
	 * @return javax.swing.JList
	 */
	private JTable getTable() {
		if (commandTable == null) {
			commandTable = new JTable();
		}
		return commandTable;
	}
	
	private void initTable(){
		commandTableModel = new CommandTableModel(undoRedoStack);
		commandTable.setModel(commandTableModel);
		commandTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		commandTable.setSelectionBackground(Color.orange);
		commandTable.setSelectionForeground(Color.black);
		commandTable.setShowGrid(false);
		commandTable.getTableHeader().setBackground(Color.white);
		TableColumn tc = commandTable.getColumnModel().getColumn(0);
		tc.setCellRenderer(new DefaultTableCellRenderer() {
			   public Component getTableCellRendererComponent(JTable table,
			                                               Object value,
			                                               boolean isSelected,
			                                               boolean hasFocus,
			                                               int row,
			                                               int column)
			      {
			         JLabel label = (JLabel)
			            super.getTableCellRendererComponent
			               (table, value, isSelected, hasFocus, row, column);
			            UndoRedoInfo info = (UndoRedoInfo) value;
			            switch (info.getType()) {
                            case UndoRedoInfo.INSERT:
                                label.setIcon(iadd);
                                break;
                            case UndoRedoInfo.DELETE:
                                label.setIcon(idel);
                                break;
                            default:
                                label.setIcon(imodify);
                        }
			         if (commandTableModel.getPos()<row){
			        	 label.setBackground(Color.lightGray);
			         }else {
			       		 label.setBackground(Color.orange);
			         }
			            return label;
			      }
			});

		commandTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent e) {
				int newpos = commandTable.getSelectedRow();	
				if (newpos >= 0){
    				commandTableModel.setPos(newpos);				
    				ApplicationLocator.getManager().refreshMenusAndToolBars();
				}
			}
		});
	}
	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getTopPanel() {
		if (topPanel == null) {
			topPanel = new JPanel();	
		}
		return topPanel;
	}

	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo = new WindowInfo(WindowInfo.ICONIFIABLE |
				WindowInfo.MODELESSDIALOG | WindowInfo.RESIZABLE | WindowInfo.PALETTE);
		m_viewinfo.setTitle(PluginServices.getText(this,
				"pila_de_comandos"));
		return m_viewinfo;
	}

	public Object getWindowModel() {
		return commandTableModel.getClass().getName();
	}

	public void windowActivated() {
		this.validate();
	}

	public void windowClosed() {
		
	}

	public void commandRepaint() {
		setValue(commandTableModel.getPos(), true);
		refreshSliderSize();
	}

    /**
     * Refreshes all the mutable controls in this component.
     */
    private void refreshControls() {
        int normalizedValue = (int) (((commandTableModel.getRowCount() - (currentValue + 1)) / (float)commandTableModel.getRowCount()) * 100);
        //Adding the 50% od the interval
        if (commandTableModel.getRowCount() > 0){
            normalizedValue = normalizedValue + (100 / (commandTableModel.getRowCount() * 2));
        }
        refreshSlider(normalizedValue);
        commandTable.repaint();
    }
    
    /**
     * Sets the slider to the correct (scaled) position.
     * @param normalizedValue
     */
    private void refreshSlider(int normalizedValue) {
        if (!refreshing){
            refreshing = true;
            getJSlider().setValue(normalizedValue);
            refreshing = false; 
        }
    }    
    
    private void refreshSliderSize(){
        if (!refreshing){
            Dimension size = new Dimension(commandSlider.getPreferredSize().width,
                ((commandTableModel.getRowCount() + 1) * getTable().getRowHeight()));
            JScrollBar verticalScrollBar = getJScrollPane().getVerticalScrollBar();
            verticalScrollBar.setValue(commandTableModel.getPos()*getTable().getRowHeight());
            commandSlider.setPreferredSize(size);
            commandSlider.setSize(size);
            validate();
        }
    }
    
	
	/**
	 * This method initializes jSlider
	 *
	 * @return javax.swing.JSlider
	 */
	private JSlider getJSlider() {
		if (commandSlider == null) {
			commandSlider = new JSlider(JSlider.VERTICAL, 0, 100, 0);	
		}
		return commandSlider;
	}
	
	private void initSlider(){		
		commandSlider.setPreferredSize(
		    new Dimension(commandSlider.getPreferredSize().width,
		        ((getTable().getRowCount())*getTable().getRowHeight())));

		commandSlider.addChangeListener(new javax.swing.event.ChangeListener() {
				public synchronized void stateChanged(javax.swing.event.ChangeEvent e) {
				    if (!refreshing){
				        int value = getTablePosFromSlider();  
				        //currentSliderValue controls the same event thrown by the
				        //slider. currentValue controls the event thrown by the table
				        if (currentSliderValue != value){
				            refreshing = true;	
				            currentSliderValue = value;
				            commandTableModel.setPos(value);
				            ApplicationLocator.getManager().refreshMenusAndToolBars();
				            refreshing = false;    
				        }
				    }
			    }
    		});
    	setValue(commandTableModel.getRowCount() - 1 - commandTableModel.getPos(),true);
	}
	
	private int getTablePosFromSlider(){	  
	    if (commandTableModel.getRowCount() == 0){
	        return -1;
	    }
	    
	    int value = getJSlider().getValue();
	    value = (int) ((value * 0.01) * commandTableModel.getRowCount());

	    //The bottom part of the slider starts in 100: take the reverse value
	    value = (commandTableModel.getRowCount() - 1) - value;
	    
	    if (value == -1){
	        return 0;
	    }
	    return value;
	}
	
    public void setValue(int number, boolean fireEvent) {
        int rowCount = commandTableModel.getRowCount();
               
        if (number < lowLimit) {
			number = lowLimit;
		}
        if (number > (rowCount - 1)) {
			number = rowCount;
		}
        if (number != currentValue) {
        	currentValue = number;
        	if (fireEvent) {
				callValueChanged(new Integer(currentValue));
			}
        }
        /*
         * This needs to be refreshed also when same number is set
         * Example: select one feature and click 'delete' key
         */
        refreshControls();
    }

	/**
	 * This method initializes jPanel1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getSliderPanel() {
		if (sliderPanel == null) {
			sliderPanel = new JPanel();
			sliderPanel.add(getJSlider());
		}
		return sliderPanel;
	}

	/**
	 * This method initializes pCenter
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout());			
			centerPanel.add(getTablePanel(), java.awt.BorderLayout.CENTER);
			centerPanel.add(getSliderPanel(), java.awt.BorderLayout.WEST);
		}
		return centerPanel;
	}
	
	private JPanel getTablePanel() {
	    if (tablePanel == null) {
	        tablePanel = new JPanel();
	        tablePanel.setLayout(new BorderLayout());
	        tablePanel.add(getTable(), java.awt.BorderLayout.CENTER);	        
	        tablePanel.add(getTopPanel(), java.awt.BorderLayout.NORTH);
	    }
	    return tablePanel;
	}

	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getCenterPanel());
		}
		return jScrollPane;
	}
	
	public void update(Observable observable, Object notification) {
		if (notification instanceof FeatureStoreNotification){
			FeatureStoreNotification featureStoreNotification =
					(FeatureStoreNotification) notification;
			//If the edition finish the command stack disappears
			String type = featureStoreNotification.getType();
			if (FeatureStoreNotification.AFTER_FINISHEDITING.equals(type) ||
					FeatureStoreNotification.AFTER_CANCELEDITING.equals(type)){							
				ApplicationLocator.getManager().getUIManager().closeWindow(this);				
				featureStoreNotification.getSource().deleteObserver(this);
				return;
			}
			//Only repaint if the event is a selection event or an edition event
			if (FeatureStoreNotification.AFTER_INSERT.equals(type) ||
                FeatureStoreNotification.AFTER_DELETE.equals(type) ||
                FeatureStoreNotification.AFTER_UPDATE.equals(type) ||
                FeatureStoreNotification.SELECTION_CHANGE.equals(type) ||
                FeatureStoreNotification.AFTER_REDO.equals(type) ||
                FeatureStoreNotification.AFTER_UNDO.equals(type)) {                   
			    commandRepaint();
			}			
		}
	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}
}  
