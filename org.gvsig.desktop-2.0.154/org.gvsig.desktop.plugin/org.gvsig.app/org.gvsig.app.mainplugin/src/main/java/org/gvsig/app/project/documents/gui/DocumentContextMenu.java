/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.DocumentAction;
import org.gvsig.app.project.documents.DocumentActionGroup;

public class DocumentContextMenu extends JPopupMenu {

    /**
	 *
	 */
    private static final long serialVersionUID = -8153469580038240864L;

    private Document item;
    private List<Document> seleteds;
    private List<DocumentAction> actions = null;

    public DocumentContextMenu(String doctype, Document document,
        Document[] seletedDocuments) {
        super();

        List<DocumentAction> allActions =
            ProjectManager.getInstance().getDocumentActions(doctype);
        List<DocumentAction> actions = new ArrayList<DocumentAction>();
        DocumentActionGroup group = null;

        this.item = document;
        this.seleteds = Arrays.asList(seletedDocuments);
        if (allActions != null) {
            for (DocumentAction action : allActions) {
                if (action.isVisible(document, this.seleteds)) {
                    actions.add(action);
                    // Create menu item
                    MenuItem item = new MenuItem(action.getTitle(), action);

                    item.setEnabled(action
                        .isAvailable(this.item, this.seleteds));
                    if (!action.getGroup().equals(group)) {
                        if (group != null) {
                            this.addSeparator();
                        }
                        group = action.getGroup();
                    }
                    this.add(item);

                }
            }
        }
        this.actions = actions;
    }

    public boolean hasActions() {
        return actions != null || actions.size() > 0;
    }

    private class MenuItem extends JMenuItem implements ActionListener {

        /**
		 *
		 */
        private static final long serialVersionUID = -752624469241001299L;
        private DocumentAction action;

        public MenuItem(String text, DocumentAction action) {
            super(text);
            this.action = action;
            String tip = this.action.getDescription();
            if (tip != null && tip.length() > 0) {
                this.setToolTipText(tip);
            }
            this.addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            this.action.execute(DocumentContextMenu.this.item,
                DocumentContextMenu.this.seleteds);
        }
    }

}
