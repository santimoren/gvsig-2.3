/*
 * Copyright 2015 DiSiD Technologies S.L.L. All rights reserved.
 *
 * Project  : DiSiD org.gvsig.app.mainplugin
 * SVN Id   : $Id$
 */
package org.gvsig.app.extension.selectioncount;

import javax.swing.JLabel;

import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.events.AtomicEvent;
import org.gvsig.fmap.mapcontext.events.listeners.AtomicEventListener;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerEvent;
import org.gvsig.fmap.mapcontext.layers.LayerListener;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlCreationListener;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.gui.beans.controls.IControl;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Counts total and selected features and shows them in the status bar
 * @author Daniel Martinez
 *
 */
public class SelectionCount implements LayerListener,Observer{
    private static Logger logger =
        LoggerFactory.getLogger(SelectionCount.class);

    private IControl control;

    /**
     * Listens the map control to show the count through the control in the status bar
     * @param mapControl
     * @param control
     */
    public SelectionCount (IControl control){
        this.control=control;
        MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
        mapControlManager.addMapControlCreationListener(new MapControlCreationListener() {
            @Override
            public MapControl mapControlCreated(final MapControl mapControl) {
                final MapContext mapContext = mapControl.getMapContext();
                mapContext.addAtomicEventListener(new AtomicEventListener() {
                    @Override
                    public void atomicEvent(AtomicEvent e) {
                        doAtomicEvent(mapContext, e);
                    }
                });
                return mapControl;
            }
        });
    }

    public void doAtomicEvent(MapContext mapContext, AtomicEvent e) {
        LayerCollectionEvent[] events = e.getLayerCollectionEvents();
        for( int i=0; i<events.length ; i++ ) {
            if( events[i].getEventType() == LayerCollectionEvent.LAYER_ADDED ) {
                FLayer fLayer = events[i].getAffectedLayer();
                fLayer.addLayerListener(this);
            }
            if( events[i].getEventType() == LayerCollectionEvent.LAYER_REMOVED ) {
                FLayer fLayer = events[i].getAffectedLayer();
                fLayer.removeLayerListener(this);
                showFeatureCount();
            }
        }
    }

    @Override
    public void visibilityChanged(LayerEvent e) {
        // Do nothing
    }

    @Override
    public void activationChanged(LayerEvent e) {
        if (e.getEventType()==LayerEvent.ACTIVATION_CHANGED){
            FLayer fLayer =e.getSource();
            if( fLayer.isAvailable() ) {
                showFeatureCount();
                if (fLayer.isActive()){
                    if (fLayer instanceof FLyrVect) {
                        FLyrVect lyrVect = (FLyrVect) fLayer;
                        lyrVect.getFeatureStore().addObserver(this);
                    }
                }else{
                    if (fLayer instanceof FLyrVect) {
                        FLyrVect lyrVect = (FLyrVect) fLayer;
                        lyrVect.getFeatureStore().deleteObserver(this);
                    }
                }
            }
        }
    }

    @Override
    public void nameChanged(LayerEvent e) {
     // Do nothing
    }

    @Override
    public void editionChanged(LayerEvent e) {
     // Do nothing
    }

    @Override
    public void drawValueChanged(LayerEvent e) {
     // Do nothing
    }

    @Override
    public void update(final Observable observable, final Object notification) {
        if (notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification event
                    = (FeatureStoreNotification) notification;
            if (event.getType() == FeatureStoreNotification.AFTER_DELETE ||
                event.getType() == FeatureStoreNotification.AFTER_INSERT ||
                event.getType() == FeatureStoreNotification.AFTER_FINISHEDITING ||
                event.getType() == FeatureStoreNotification.SELECTION_CHANGE ){

                if (event.getSource() instanceof FeatureStore){
                    showFeatureCount();
                }
            }
        }
    }

    /**
     * Gets the features from the selected layers and shows them through the control
     * @param mapControl
     */
    public void showFeatureCount(){
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument viewDoc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if( viewDoc == null ) {
            return;
        }
        MapContext mapContext = viewDoc.getMapContext();
        Long totalFeaturesCount=Long.valueOf(0);
        Long selectedFeaturesCount=Long.valueOf(0);;
        if (mapContext!=null){
            FLayer[] actives =mapContext.getLayers().getActives();
            if (actives!=null && actives.length>0){
                for(FLayer fLayer:actives){
                    if (fLayer instanceof FLyrVect) {
                        FLyrVect lyrVect = (FLyrVect) fLayer;
                        FeatureStore featureStore=lyrVect.getFeatureStore();
                        if (featureStore != null) {
                            try {
                                totalFeaturesCount +=
                                    featureStore.getFeatureCount();
                            } catch (DataException e) {
                                logger.warn(
                                    "Problem obtaining total features count");
                                totalFeaturesCount = 0L;
                            }
                            try {
                                FeatureSelection featureSelection =
                                    featureStore.getFeatureSelection();
                                if (featureSelection != null) {
                                    selectedFeaturesCount +=
                                        featureSelection.getSize();
                                }
                            } catch (DataException e) {
                                logger.warn(
                                    "Problem obtaining selected features count");
                                selectedFeaturesCount = 0L;
                            }
                        }
                    }
                }
                messageToStatusBar(totalFeaturesCount,selectedFeaturesCount);
            }else {
                clean();
            }
        }else {
            clean();
        }
    }

    /**
     * Cleans the text shown in the control
     */
    public void clean(){
        if (control!=null && control instanceof JLabel ){
            JLabel jlabel = (JLabel)control;
            jlabel.setText("");
        }
    }

    private void messageToStatusBar(Long totalFeaturesCount, Long selectedFeaturesCount){
        String strTotal;
        String strSelected;
        if (totalFeaturesCount!=null){
            strTotal=totalFeaturesCount.toString();
        }else{
            strTotal="N/A";
        }
        if (selectedFeaturesCount!=null){
            strSelected=selectedFeaturesCount.toString();
        }else{
            strSelected="N/A";
        }
        if (control!=null && control instanceof JLabel ){
            JLabel jlabel = (JLabel)control;
            jlabel.setText(strSelected+"/"+strTotal);
        }
    }

}
