/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocMenuEntry;
import org.gvsig.app.project.documents.view.toc.gui.FPopupMenu;
import org.gvsig.fmap.mapcontext.layers.FLayer;


public class OldTocContextMenuAction extends AbstractTocContextMenuAction {
	private TocMenuEntry entry;

	public int getGroupOrder() {
		return 999;
	}

	public String getGroup() {
		return "zzzOldMenus";
	}

	public void execute(ITocItem item, FLayer[] selectedItems) {
		return; //No se llega a ejecutar nunca
	}

	public String getText() {
		return null; //No se llega a ejecutar nunca
	}

	public void initializeElement(FPopupMenu menu) {
		this.entry.initialize(menu);
	}

	public void setEntry(TocMenuEntry entry) {
		this.entry = entry;
	}

	public TocMenuEntry getEntry() {
		return this.entry;
	}
}
