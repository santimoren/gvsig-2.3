package org.gvsig.app.gui.preferencespage;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.app.extension.AddLayer;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynobject.DynObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LayerOrderPage extends AbstractPreferencePage implements ActionListener {

	private static Logger logger = LoggerFactory.getLogger(LayerOrderPage.class);
	public static String PREFERENCES_ID = "current_layer_order_manager";
	
	private ImageIcon icon;
	private JComboBox orderCombo = null;
	private JTextArea descArea = null;
	
	public LayerOrderPage() {
		super();
		setParentID(ViewPage.id);
		initUI();
	}
	
	
	private void initUI() {
		
		JPanel panel = new JPanel(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 8, 5, 8);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(new JLabel(Messages.getText("_Order")), gbc);
		
		gbc.gridx = 1;
		panel.add(getOrderCombo(), gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(new JLabel(Messages.getText("_Description")), gbc);
		
		gbc.gridx = 1;
		panel.add(getDescArea(getOrderCombo().getFont(), panel.getBackground()), gbc);
		
		this.addComponent(panel);
		
	}


	private JTextArea getDescArea(Font fnt, Color bg) {
		
		if (descArea == null) {
			
			descArea = new JTextArea();
			descArea.setWrapStyleWord(true);
			descArea.setLineWrap(true);
			descArea.setPreferredSize(new Dimension(250, 80));
			if (fnt != null) {
				descArea.setFont(fnt);
			}
			if (bg != null) {
				descArea.setBackground(bg);
			}
		}
		return descArea;
	}


	private JComboBox getOrderCombo() {
		
		if (orderCombo == null) {
			orderCombo = new JComboBox();
			orderCombo.addActionListener(this);
			
		}
		return orderCombo;
	}


	public String getID() {
		return this.getClass().getName();
	}

	public String getTitle() {
		return Messages.getText("_Layer_order_in_TOC");
	}

	public JPanel getPanel() {
		return this;
	}

	public void initializeValues() {
		
		this.getOrderCombo().removeAllItems();
		loadAvailables();
		
		DynObject props = this.getPluginProperties();
		// current_layer_order_manager
		Object val_obj = null;
		if (props.hasDynValue(PREFERENCES_ID)) {
			val_obj = props.getDynValue(PREFERENCES_ID);
		}
		if (val_obj != null && val_obj instanceof LayerOrderManager) {
			LayerOrderManager lom = (LayerOrderManager) val_obj;
			String code = lom.getCode();
			setSelected(code);
			return;
		}
		// init to default
		LayerOrderManager lom = MapContextLocator.getDefaultOrderManager();
		if (lom != null) {
			setSelected(lom.getCode());
		} else {
			logger.warn("No default order manager set!");
		}
	}

	private void setSelected(String code) {
		
		int n = this.getOrderCombo().getItemCount();
		LayerOrderManager lom = null;
		for (int i=0; i<n; i++) {
			lom = ((LayerOrderComboItem) getOrderCombo().getItemAt(i)).getManager();
			if (lom.getCode().compareTo(code) == 0) {
				this.getOrderCombo().setSelectedIndex(i);
				return;
			}
		}
		
		logger.warn("Did not find code in list of order managers: " + code);
		getOrderCombo().setSelectedIndex(0);
	}


	private void loadAvailables() {
		
		List avs = MapContextLocator.getOrderManagers();
		LayerOrderManager lom = null;
		Iterator iter = avs.iterator();
		while (iter.hasNext()) {
			lom = (LayerOrderManager) iter.next();
			this.getOrderCombo().addItem(new LayerOrderComboItem(lom));
		}
		
	}


	public void initializeDefaults() {
		
		this.getOrderCombo().removeAllItems();
		loadAvailables();
		// init to default
		LayerOrderManager lom = MapContextLocator.getDefaultOrderManager();
		if (lom != null) {
			setSelected(lom.getCode());
		} else {
			logger.warn("No default order manager set!");
		}
		
	}

	public ImageIcon getIcon() {
		
		if (icon == null) {
			icon = IconThemeHelper.getImageIcon("layer-order-preferences");
		}
		return icon;
	}

	public boolean isValueChanged() {
		return true;
	}

	public void storeValues() throws StoreException {
		
		LayerOrderComboItem loci =
				(LayerOrderComboItem) getOrderCombo().getSelectedItem();
		
		DynObject props = this.getPluginProperties();
		props.setDynValue(PREFERENCES_ID, loci.getManager());
	}


	public void setChangesApplied() {
	}
	
	
	private DynObject getPluginProperties() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        return pluginsManager.getPlugin(AddLayer.class).getPluginProperties();
	}
	
	
	public class LayerOrderComboItem {
		
		private LayerOrderManager lom = null;
		
		public LayerOrderComboItem(LayerOrderManager _lom) {
			lom = _lom;
		}
		
		public String toString() {
			return lom.getName();
		}
		
		public LayerOrderManager getManager() {
			return lom;
		}
	}


	public void actionPerformed(ActionEvent e) {
		
		Object src = e.getSource();
		if (src == getOrderCombo()) {
			LayerOrderComboItem item = null;
			item = (LayerOrderComboItem) getOrderCombo().getSelectedItem(); 
			if (item == null) {
				return;
			}
			String desc = item.getManager().getDescription();
			this.getDescArea(null, null).setText(desc);
		}
		
	}

}
