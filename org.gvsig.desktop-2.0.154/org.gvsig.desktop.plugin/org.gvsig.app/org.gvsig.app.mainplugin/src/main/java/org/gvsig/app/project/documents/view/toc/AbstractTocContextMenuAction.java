/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc;

import java.util.Map;

import org.gvsig.app.project.documents.view.AbstractContextMenuAction;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.extensionpoint.ExtensionBuilder;


public abstract class AbstractTocContextMenuAction extends AbstractContextMenuAction implements ExtensionBuilder{
	private MapContext mapContext;

	public MapContext getMapContext() {
		return this.mapContext;
	}

	public void setMapContext(MapContext mapContext) {
		this.mapContext = mapContext;
	}

	/**
	 * @deprecated use public boolean isEnabled(ITocItem item, FLayer[] selectedItems)
	 */
	public boolean isEnabled(Object item, Object[] selectedItems) {
		return this.isEnabled((ITocItem)item, (FLayer[])selectedItems);
	}

	/**
	 * @deprecated use public boolean isVisible(ITocItem item, FLayer[] selectedItems)
	 */
	public boolean isVisible(Object item, Object[] selectedItems) {
		return this.isVisible((ITocItem)item, (FLayer[])selectedItems);
	}

	/**
	 * @deprecated use public void execute(ITocItem item, FLayer[] selectedItems)
	 */
	public void execute(Object item, Object[] selectedItems) {
		this.execute((ITocItem)item, (FLayer[])selectedItems);
	}

	public FLayer getNodeLayer(ITocItem node) {
		if (isTocItemBranch(node))
			return ((TocItemBranch) node).getLayer();
		return null;
	}
	public boolean isTocItemLeaf(ITocItem node) {
		return node instanceof TocItemLeaf;
	}

	public boolean isTocItemBranch(ITocItem node) {
		return node instanceof TocItemBranch;
	}


	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return true;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		return true;
	}

	public abstract void execute(ITocItem item, FLayer[] selectedItems);

	public Object create() {
		return this;
	}

	public Object create(Map args) {
		// TODO Auto-generated method stub
		return this;
	}

	public Object create(Object[] args) {
		// TODO Auto-generated method stub
		return this;
	}

}
