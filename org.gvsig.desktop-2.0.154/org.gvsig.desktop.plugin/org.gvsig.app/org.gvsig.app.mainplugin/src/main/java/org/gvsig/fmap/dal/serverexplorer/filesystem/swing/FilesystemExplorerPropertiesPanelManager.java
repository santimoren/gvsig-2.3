
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import org.gvsig.tools.dynobject.DynObject;


public interface FilesystemExplorerPropertiesPanelManager {
    /**
     * Register a new PropertiesPageFactory in this manager.
     * 
     * @param factory 
     */
    public void registerFactory(FilesystemExplorerPropertiesPanelFactory factory);
    
    /**
     * Return the factory associated to the passed parameters.
     * 
     * @param parameters
     * @return the factory for the parameters 
     */
    public FilesystemExplorerPropertiesPanelFactory getFactory(DynObject parameters);
    
    /**
     * Create and return the panel associated to the passed parameters,
     * 
     * @param parameters
     * @return the panel for the parameters  
     */
    public FilesystemExplorerPropertiesPanel createPanel(DynObject parameters);    
    
    public void showPropertiesDialog(DynObject parameters);
    
    public void showPropertiesDialog(final DynObject parameters, final FilesystemExplorerPropertiesPanel panel);

}
