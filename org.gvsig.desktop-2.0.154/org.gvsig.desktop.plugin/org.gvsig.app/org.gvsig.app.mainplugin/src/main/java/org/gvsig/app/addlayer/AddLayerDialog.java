/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.addlayer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cresques.cts.IProjection;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.gui.wizards.WizardListener;
import org.gvsig.app.project.DefaultProject;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Frame del cuadro de dialogo que contendra los tabs de aperturas de ficheros
 *
 * @version 04/09/2007
 * @author BorSanZa - Borja S�nchez Zamorano (borja.sanchez@iver.es)
 */
public class AddLayerDialog extends JPanel implements org.gvsig.andami.ui.mdiManager.IWindow, Disposable {

    private static Logger logger = LoggerFactory.getLogger(AddLayerDialog.class);
    static private IProjection proj = null;
    private JTabbedPane jTabbedPane = null;
    private AcceptCancelPanel jPanel = null;
    private boolean accepted = false;
    private String title = PluginServices.getText(this, "add_layer");
    private final WizardListener wizardListener = new DialogWizardListener();

    /**
     * Creates a new FOpenDialog object.
     */
    public AddLayerDialog() {
        initialize();
    }

    /**
     * Constructor con un nombre de Frame
     *
     * @param title
     */
    public AddLayerDialog(String title) {
        this.title = title;
        initialize();
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        ToolsLocator.getDisposableManager().bind(this);

        this.setLayout(new BorderLayout());
        this.setSize(523, 385);
        this.setPreferredSize(new Dimension(523, 385));
        this.add(getJTabbedPane(), BorderLayout.CENTER);
        this.add(getJPanel(), BorderLayout.SOUTH);
    }

    /**
     * This method initializes jTabbedPane
     *
     * @return javax.swing.JTabbedPane
     */
    private JTabbedPane getJTabbedPane() {
        if (jTabbedPane == null) {
            jTabbedPane = new JTabbedPane();
            jTabbedPane.setBounds(0, 0, getWindowInfo().getWidth() - 10, getWindowInfo().getHeight() - 10);
            jTabbedPane.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    JTabbedPane tabs = (JTabbedPane) e.getSource();
                    Component sel_tab = tabs.getSelectedComponent();
                    if (sel_tab instanceof WizardPanel) {
                        WizardPanel wipa = (WizardPanel) sel_tab;
                        wizardListener.wizardStateChanged(
                                wipa.areSettingsValid());
                    }
                    // getJPanel().setOkButtonEnabled(!(tabs.getSelectedComponent() instanceof WizardPanel));
                }
            });
        }

        return jTabbedPane;
    }

    /**
     * A�ade en una pesta�a un Jpanel con un titulo
     *
     * @param title
     * @param panel
     */
    public void addTab(String title, JPanel panel) {
        getJTabbedPane().addTab(title, panel);
    }

    /**
     * A�ade en una pesta�a un WizardPanel con un titulo
     *
     * @param title
     * @param panel
     */
    public void addWizardTab(String title, WizardPanel panel) {
        panel.addWizardListener(wizardListener);
        getJTabbedPane().addTab(title, panel);
    }

    /**
     * Devuelve el JPanel seleccionado en las pesta�as
     *
     * @return
     */
    public JPanel getSelectedTab() {
        return (JPanel) getJTabbedPane().getSelectedComponent();
    }

    /*
     * (non-Javadoc)
     * @see com.iver.andami.ui.mdiManager.IWindow#getWindowInfo()
     */
    @Override
    public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo = new WindowInfo(
                WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
        m_viewinfo.setTitle(this.title);
        m_viewinfo.setHeight(500);
        m_viewinfo.setWidth(530);
        return m_viewinfo;
    }

    public void updateOkButtonState() {
        if (this.getSelectedTab() instanceof WizardPanel) {
            WizardPanel wp = (WizardPanel) this.getSelectedTab();
            getJPanel().setOkButtonEnabled(wp.areSettingsValid());
        }
    }

    /**
     * This method initializes jPanel
     *
     * @return javax.swing.JPanel
     */
    private AcceptCancelPanel getJPanel() {
        if (jPanel == null) {
            ActionListener okAction = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    accepted = true;
                    closeWindow();
                }
            };
            ActionListener cancelAction = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    closeWindow();
                }
            };
            jPanel = new AcceptCancelPanel(okAction, cancelAction);
            jPanel.setOkButtonEnabled(false);
        }
        return jPanel;
    }

    /**
     * @return
     */
    public boolean isAccepted() {
        return accepted;
    }

    /**
     * Listener para el Wizard de apertura de fichero
     *
     * @version 05/09/2007
     * @author BorSanZa - Borja S�nchez Zamorano (borja.sanchez@iver.es)
     */
    public class DialogWizardListener implements WizardListener {
        /*
         * (non-Javadoc)
         * @see com.iver.cit.gvsig.gui.wizards.WizardListener#error(java.lang.Exception)
         */

        @Override
        public void error(Exception e) {
        }

        /*
         * (non-Javadoc)
         * @see com.iver.cit.gvsig.gui.wizards.WizardListener#wizardStateChanged(boolean)
         */
        @Override
        public void wizardStateChanged(boolean finishable) {
            getJPanel().setOkButtonEnabled(finishable);
        }
    }

    /**
     * Devuelve la ultima proyecci�n usada
     *
     * @return
     */
    public static IProjection getLastProjection() {
        if (proj == null) {
            proj = DefaultProject.getDefaultProjection();
        }
        return proj;
    }

    /**
     * Define la ultima proyeccion
     *
     * @param proj
     */
    public static void setLastProjection(IProjection proj) {
        AddLayerDialog.proj = proj;
    }

    @Override
    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }

    private void closeWindow() {
        if (PluginServices.getMainFrame() == null) {
            ((JDialog) (getParent().getParent().getParent().getParent())).dispose();
        } else {
            PluginServices.getMDIManager().closeWindow(AddLayerDialog.this);
        }
    }

    @Override
    public void dispose() {
        JTabbedPane tabbed = getJTabbedPane();
        for (int i = 0; i < tabbed.getTabCount(); i++) {
            Component component = tabbed.getComponentAt(i);
            if (component instanceof WizardPanel) {
                ((WizardPanel) component).close();
            }
        }
        ToolsLocator.getDisposableManager().release(this);
    }

}
