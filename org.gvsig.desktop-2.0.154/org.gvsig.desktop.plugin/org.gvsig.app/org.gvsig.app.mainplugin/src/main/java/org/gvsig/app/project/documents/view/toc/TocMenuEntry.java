/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.gvsig.app.project.documents.view.toc.gui.FPopupMenu;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;


/**
 * @author Luis W. Sevilla (sevilla_lui@gva.es)�
 * @deprecated
 */
public abstract class TocMenuEntry implements ActionListener {
	private FPopupMenu menu;
	//private Class [] layerTypes = null;
	public TocMenuEntry() {
	}

	/**
	 * Se utiliza para inicializar el menu. Dentro comprobar�
	 * encima de qu� tipo de tocItem hemos pinchado, y se
	 * mostrar� la opci�n de menu o no en funci�n de si
	 * es un branch o un leaf, o incluso de alg�n otro
	 * par�metro interno.
	 * @param menu
	 */
	public void initialize(FPopupMenu m) {
		menu = m;
	}

	/**
	 * C�digo que se ejecuta cuando selecciona una opci�n
	 * de menu. Recibe el tocItem sobre el que has pinchado.
	 * @param tocItem
	 */
	public abstract void actionPerformed(ActionEvent e);

	public FPopupMenu getMenu() { return menu; }

	public MapContext getMapContext() {
		return menu.getMapContext();
	}
	public Object getNodeUserObject() {
		if (menu.getNode() == null) {
			return null;
		}
		return menu.getNode().getUserObject();
	}

	public FLayer getNodeLayer() {
		if (isTocItemBranch()) {
			return ((TocItemBranch) getNodeUserObject()).getLayer();
		}
		return null;
	}
	public boolean isTocItemLeaf() {
		return getNodeUserObject() instanceof TocItemLeaf;
	}

	public boolean isTocItemBranch() {
		return getNodeUserObject() instanceof TocItemBranch;
	}
}
