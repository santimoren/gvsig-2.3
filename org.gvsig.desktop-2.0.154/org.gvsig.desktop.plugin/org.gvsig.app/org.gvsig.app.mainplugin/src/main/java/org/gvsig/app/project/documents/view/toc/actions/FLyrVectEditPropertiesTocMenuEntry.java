/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;


import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.gui.LayerProperties;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.operations.ClassifiableVectorial;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

/**
 * Muestra el men� de propiedades del tema.
 *
 * @author jmorell
 */


public class FLyrVectEditPropertiesTocMenuEntry extends AbstractTocContextMenuAction {
	public String getGroup() {
		return "group2"; //FIXME
	}

	public int getGroupOrder() {
		return 20;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "propiedades");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
	    
        if (item instanceof TocItemBranch) {
            TocItemBranch bra = (TocItemBranch) item; 
            return bra.getLayer().isAvailable();
        } else {
            return false;
        }
	        
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			FLayer lyr = getNodeLayer(item);
            if ((lyr instanceof ClassifiableVectorial)) {
            	if (!((lyr instanceof FLyrVect) &&
            			!((FLyrVect)lyr).isPropertiesMenuVisible())){
            		return true;
//            		if (!(lyr instanceof FLyrAnnotation))
//            			return true;
            	}

            }
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
//		long t1 = System.currentTimeMillis();

		FLayer lyr = getNodeLayer(item);

		if (lyr.isAvailable()) {
                    LayerProperties dlg = new LayerProperties(lyr);
                    WindowManager wm = ToolsSwingLocator.getWindowManager();
                    wm.showWindow(
                            dlg.asJComponent(),
                            ToolsLocator.getI18nManager().getTranslation("propiedades_de_la_capa"),
                            WindowManager.MODE.WINDOW
                    );
                }

		Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
		project.setModified(true);
	}

}
