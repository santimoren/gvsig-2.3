/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.imp;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.TreeModel;

import org.cresques.cts.IProjection;
import org.gvsig.about.AboutLocator;
import org.gvsig.about.AboutManager;
import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.ui.mdiFrame.MainFrame;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.PreferencesNode;
import org.gvsig.app.extension.Version;
import org.gvsig.app.extension.develtools.InfoPanel;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.prepareAction.PrepareContextView;
import org.gvsig.app.prepareAction.PrepareDataStore;
import org.gvsig.app.prepareAction.PrepareDataStoreParameters;
import org.gvsig.app.prepareAction.PrepareLayer;
import org.gvsig.app.project.DefaultProject;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.CompoundLayersTreeModel;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.gui.ColorTablesFactory;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dispose.DisposableManager;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 * @author jmvivo
 *
 */
public class DefaultApplicationManager implements ApplicationManager {

    @SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(DefaultApplicationManager.class);
    
	private static final String EPNAME_PREPARE_OPEN_DATASTORE = "org.gvsig.datastore.open.prepare"; //"PrepareOpenDataStore";
	private static final String EPNAME_PREPARE_OPEN_DATASTOREPARAMETERS = "org.gvsig.datastoreparameters.open.prepare"; //"PrepareOpenDataStoreParameters";
	private static final String EPNAME_PREPARE_OPEN_LAYER = "org.gvsig.layer.open.prepare"; // "PrepareOpenLayer";

	private static final String EPNAME_ADD_TABLE_WIZARD = "AddLayerWizard";

	private ExtensionPointManager epManager;

    private Version version;

	public DefaultApplicationManager() {
		epManager = ToolsLocator.getExtensionPointManager();
		epManager.add(EPNAME_PREPARE_OPEN_DATASTORE,
				"Actions to do when open a DataStore");
		epManager.add(EPNAME_PREPARE_OPEN_DATASTOREPARAMETERS,
				"Actions to do before open a DataStore with parameters");
		epManager.add(EPNAME_PREPARE_OPEN_LAYER,
			"Actions to do after create a Layer");
		epManager.add(EPNAME_ADD_TABLE_WIZARD,
				"Wizards to add new document table");

        version = new Version();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.appGvSigManager#pepareOpenDataSource(org.gvsig.fmap.dal.DataStore)
	 */
	@SuppressWarnings("unchecked")
	public DataStore pepareOpenDataSource(DataStore store,
			PrepareContext context) throws Exception {
		ExtensionPoint ep = epManager.get(EPNAME_PREPARE_OPEN_DATASTORE);
		if (ep.getCount() == 0) {
			return store;
		}
		DataStore result = store;
		Iterator<ExtensionPoint.Extension> iter = ep.iterator();
		PrepareDataStore prepare;
		while (iter.hasNext()) {
			prepare = (PrepareDataStore) iter.next().create();
			result = prepare.prepare(store, context);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.appGvSigManager#prepareOpenDataStoreParameters(org.gvsig.fmap.dal.DataStoreParameters)
	 */
	@SuppressWarnings("unchecked")
	public DataStoreParameters prepareOpenDataStoreParameters(
			DataStoreParameters storeParameters, PrepareContext context)
			throws Exception {

		ExtensionPoint ep = epManager
				.get(EPNAME_PREPARE_OPEN_DATASTOREPARAMETERS);
		if (ep.getCount() == 0) {
			return storeParameters;
		}
		DataStoreParameters result = storeParameters;
		Iterator<ExtensionPoint.Extension> iter = ep.iterator();
		PrepareDataStoreParameters prepare;
		while (iter.hasNext()) {
			prepare = (PrepareDataStoreParameters) iter.next().create();
			result = prepare.prepare(storeParameters, context);
		}

		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.app.ApplicationManager#prepareOpenDataStoreParameters(java.util.List, org.gvsig.app.prepareAction.PrepareContext)
	 */
	@SuppressWarnings("unchecked")
	public List<DataStoreParameters> prepareOpenDataStoreParameters(
			List<DataStoreParameters> storeParameters, PrepareContext context)
			throws Exception {

		ExtensionPoint ep = epManager
				.get(EPNAME_PREPARE_OPEN_DATASTOREPARAMETERS);
		if (ep.getCount() == 0) {
			return storeParameters;
		}
		
		List<DataStoreParameters> result = new ArrayList<DataStoreParameters>();
		
		Iterator<ExtensionPoint.Extension> iter = ep.iterator();
		List<PrepareDataStoreParameters> prepareList = new ArrayList<PrepareDataStoreParameters>();
		while (iter.hasNext()) {
			prepareList.add((PrepareDataStoreParameters) iter.next().create());
		}
		
		for (int i = 0; i < storeParameters.size(); i++) {
			DataStoreParameters param = storeParameters.get(i);
			for (int j = 0; j < prepareList.size(); j++) {
				prepareList.get(j).pre(param, context);
			}
		}
		
		for (int i = 0; i < storeParameters.size(); i++) {
			DataStoreParameters param = storeParameters.get(i);
			if(param != null) {
				for (int j = 0; j < prepareList.size(); j++) {
					param = prepareList.get(j).prepare(param, context);
					if(param == null)
						break;
				}
				if(param != null)
					result.add(param);
			}
		}
		
		for (int i = 0; i < storeParameters.size(); i++) {
			DataStoreParameters param = storeParameters.get(i);
			for (int j = 0; j < prepareList.size(); j++) {
				prepareList.get(j).post(param, context);
			}
		}

		return result;
	}

	public void registerPrepareOpenDataStore(PrepareDataStore action) {
		ExtensionPoint ep = epManager.get(EPNAME_PREPARE_OPEN_DATASTORE);
		ep.append(action.getName(), action.getDescription(), action);
	}

	public void registerPrepareOpenDataStoreParameters(
			PrepareDataStoreParameters action) {
		ExtensionPoint ep = epManager
				.get(EPNAME_PREPARE_OPEN_DATASTOREPARAMETERS);
		ep.append(action.getName(), action.getDescription(), action);


	}

	@SuppressWarnings("unchecked")
	public FLayer prepareOpenLayer(FLayer layer,
			PrepareContextView context)
			throws Exception {

		ExtensionPoint ep = epManager.get(EPNAME_PREPARE_OPEN_LAYER);

		if (ep.getCount() == 0) {
			return layer;
		}
		FLayer result = layer;
		Iterator<ExtensionPoint.Extension> iter = ep.iterator();
		PrepareLayer prepare;
		while (iter.hasNext()) {
			prepare = (PrepareLayer) iter.next().create();
			result = prepare.prepare(result, context);
		}

		return result;

	}

	public void registerPrepareOpenLayer(PrepareLayer action) {
		ExtensionPoint ep = epManager.get(EPNAME_PREPARE_OPEN_LAYER);
		ep.append(action.getName(), action.getDescription(), action);
	}

	public void registerAddTableWizard(String name, String description,
			Class<? extends WizardPanel> wpClass) {
		ExtensionPoint ep = epManager.get(EPNAME_ADD_TABLE_WIZARD);
		ep.append(name, description, wpClass);
	}

	@SuppressWarnings("unchecked")
	public List<WizardPanel> getWizardPanels() throws Exception {
		ExtensionPoint ep = epManager.get(EPNAME_ADD_TABLE_WIZARD);
		List<WizardPanel> result = new ArrayList<WizardPanel>();
		Iterator<Extension> iter = ep.iterator();
		while (iter.hasNext()) {
			result.add((WizardPanel) iter.next().create());
		}
		return result;
	}

	public DataManager getDataManager() {
		return DALLocator.getDataManager();
	}

	public GeometryManager getGeometryManager() {
		return GeometryLocator.getGeometryManager();
	}

	public PersistenceManager getPersistenceManager() {
		return ToolsLocator.getPersistenceManager();
	}

	public DisposableManager getDisposableManager() {
		return ToolsLocator.getDisposableManager();
	}

	public DynObjectManager getDynObjectManager() {
		return ToolsLocator.getDynObjectManager();
	}

	public ExtensionPointManager getExtensionPointManager() {
		return ToolsLocator.getExtensionPointManager();
	}

	public ProjectManager getProjectManager() {
		return ProjectManager.getInstance();
	}

	public MDIManager getUIManager() {
		return PluginServices.getMDIManager();
	}

	public MapContextManager getMapContextManager() {
		return MapContextLocator.getMapContextManager();
	}

	public DataTypesManager getDataTypesManager() {
		return ToolsLocator.getDataTypesManager();
	}
	
	public IProjection getCRS(String code) {
		return CRSFactory.getCRS(code);
	}

	public IconThemeManager getIconThemeManager() {
		return ToolsSwingLocator.getIconThemeManager();
	}

	public String getArgument(String name) {
		return PluginServices.getArgumentByName(name);
	}

	public String[] getArguments() {
		return PluginServices.getArguments();
	}

	public String getFromClipboard() {
		return PluginServices.getFromClipboard();
	}

	public void putInClipboard(String data) {
		PluginServices.putInClipboard(data);
	}

	public Project getCurrentProject() {
		return ProjectManager.getInstance().getCurrentProject();
	}

	public PreferencesNode getPreferences(String node) {
		if( node.equalsIgnoreCase("project")) {
			return DefaultProject.getPreferences();
		}
		return new DefaultPreferencesNode(node);
	}

	public PreferencesNode getPreferences() {
		return new DefaultPreferencesNode();
	}

	public Version getVersion() {
        return version;
	}
	
	public AboutManager getAbout() {
		AboutManager manager = AboutLocator.getManager();
		return manager;
	}

	public ColorTablesFactory getColorTablesFactory() {
		return SymbologySwingLocator.getSwingManager().getColorTablesFactory();
	}

	public void registerColorTablesFactory(ColorTablesFactory factory) {
		SymbologySwingLocator.getSwingManager().setColorTablesFactory(factory);
		
	}

    public IWindow getActiveWindow() {
        try {
            IWindow window = PluginServices.getMDIManager().getActiveWindow();
            return window;
        } catch (Exception ex) {
            return null;
        }
    }

    public Document getActiveDocument() {
    	return this.getActiveDocument((Class<? extends Document>)null);
    }
    
    public Document getActiveDocument(String documentTypeName) {
        Document document = this.getActiveDocument();
        if( document!= null && document.getTypeName().equalsIgnoreCase(documentTypeName) ) {
            return document;
        }
        return null;
    }
    
    public Document getActiveDocument(Class<? extends Document> documentClass) {
        Project project = this.getCurrentProject();
        if( project == null ) {
            return null;
        }
        return project.getActiveDocument(documentClass);
    }

    public JComponent getActiveComponent(Class<? extends Document> documentClass) {
        Document document = this.getActiveDocument(documentClass);
        if (document == null) {
            return null;
        }
        return document.getMainComponent();
    }
    
   
    public IDocumentWindow getDocumentWindow(Document document) {
        Class<? extends IDocumentWindow> defaultDocumentClass =
            document.getFactory().getMainWindowClass();
        return (IDocumentWindow) this.getUIManager().getSingletonWindow(defaultDocumentClass, document);
    }

	public String getLocaleLanguage() {
		return Launcher.getAndamiConfig().getLocaleLanguage();
	}

	public void message(String message, int message_type) {
		MainFrame main = PluginServices.getMainFrame();
		main.message(message, message_type);
	}

	public void messageDialog(String message, String title, int messageType) {
		MainFrame main = PluginServices.getMainFrame();
		main.messageDialog(message, title, messageType);
	}

	public void messageDialog(String message, String[] messageArgs,
			String title, int messageType) {
		MainFrame main = PluginServices.getMainFrame();
		main.messageDialog(message, messageArgs, title, messageType);
	}

	public int confirmDialog(String message, String title, int optionType,
			int messageType) {
		MainFrame main = PluginServices.getMainFrame();
		return main.confirmDialog(message, title, optionType, messageType);
	}

	public String inputDialog(String message, String title, int messageType,
			String initialValue) {
		MainFrame main = PluginServices.getMainFrame();
		return main.inputDialog(message, title, messageType, initialValue);
	}

	public String inputDialog(String message, String title) {
		MainFrame main = PluginServices.getMainFrame();
		return main.inputDialog(message, title);
	}

	public void showDialog(Component contents, String title) {
		MainFrame main = PluginServices.getMainFrame();
		main.showDialog(contents, title);
	}

	public Component createComponent(Class<? extends Component> theClass,
			Object... parameters) {
		MainFrame main = PluginServices.getMainFrame();
		return main.createComponentWithParams(theClass, parameters);
	}

	public Component createComponentWithParams(
			Class<? extends Component> theClass, Object[] parameters) {
		MainFrame main = PluginServices.getMainFrame();
		return main.createComponentWithParams(theClass, parameters);
	}


	public File[] showChooserDialog(
			final String title,
			final int type, // SAVE_DIALOG / OPEN_DIALOG
			final int selectionMode, //    JFileChooser.FILES_ONLY, JFileChooser.DIRECTORIES_ONLY, JFileChooser.FILES_AND_DIRECTORIES
			final boolean multiselection, 
			final File initialPath,
			final FileFilter filter,
			final boolean fileHidingEnabled
			) {
		MainFrame main = PluginServices.getMainFrame();
		return main.showChooserDialog(title, type, selectionMode, multiselection, initialPath, filter, fileHidingEnabled);
	}
	
	public File[] showOpenDirectoryDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.DIRECTORIES_ONLY, false, initialPath, null, false);
	}

	
	public File[] showOpenFileDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.OPEN_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
	}

	
	public File[] showSaveFileDialog(String title, File initialPath) {
		return showChooserDialog(title, JFileChooser.SAVE_DIALOG, JFileChooser.FILES_ONLY, false, initialPath, null, false);
	}
	
	public String translate(String message, String... args) {
		return org.gvsig.i18n.Messages.translate(message, args);
	}
	
	public Component getRootComponent() {
		return (Component) PluginServices.getMainFrame();
	}

	public void refreshMenusAndToolBars() {
		PluginServices.getMainFrame().refreshControls();
	}

    public MainFrame getMainFrame() {
        return PluginServices.getMainFrame();
    }
    
	public void addMenu(ActionInfo action, String text) {
		MainFrame f = this.getMainFrame();
		f.addMenu(action,text);
	}

        @Override
	public void addTool(ActionInfo action, String toolBarName) {
		MainFrame f = this.getMainFrame();
		f.addTool(action,toolBarName);
		f.refreshControls();
	}
	
        @Override
        public void addSelectableTool(ActionInfo action, String toolBarName) {
		MainFrame f = this.getMainFrame();
		f.addSelectableTool(action,toolBarName,"unico",false);
		f.refreshControls();
	}
                
        public void showTextDialog(final WindowManager.MODE mode, final String title, final String htmlText) {
            this.showTextDialog(mode, title, htmlText, null);
        }
        
        public void showTextDialog(final WindowManager.MODE mode, final String title, final String htmlText, final HyperlinkListener hyperlinkListener) {
            if (!SwingUtilities.isEventDispatchThread()) {
                if( mode == WindowManager.MODE.DIALOG ) {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                showTextDialog(mode, title, htmlText,hyperlinkListener);
                            }
                        });
                    } catch (Exception ex) {
                        logger.warn("Can't show text dialog:\n"+htmlText,ex);
                    }
                } else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            showTextDialog(mode, title, htmlText,hyperlinkListener);
                        }
                    });
                }
                return;
            }
            InfoPanel.showPanel(title, mode, htmlText,hyperlinkListener);
        }

        @Override
        public TreeModel createProjectLayersTreeModel() {
            CompoundLayersTreeModel model = (CompoundLayersTreeModel) MapControlLocator.getMapControlManager().createCompoundLayersTreeModel();
            Project project = this.getCurrentProject();
            List<Document> views = project.getDocuments(ViewManager.TYPENAME);
            for (Document view : views) {
                model.addLayers( ((ViewDocument)view).getMapContext().getLayers() );
            }
            return model;
        }
}
