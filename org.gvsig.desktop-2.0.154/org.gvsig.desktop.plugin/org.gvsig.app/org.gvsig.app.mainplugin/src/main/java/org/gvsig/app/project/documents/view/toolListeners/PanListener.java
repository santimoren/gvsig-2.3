/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners;

import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.PanListenerImpl;
import org.gvsig.fmap.mapcontrol.tools.Events.MoveEvent;



/**
 * <p>Inherits {@link PanListener PanListener} enabling/disabling special
 *  controls for managing the data.</p>
 *
 * @see PanListenerImpl
 *
 * @author Vicente Caballero Navarro
 */
public class PanListener extends PanListenerImpl {
	/**
	 * <p>Creates a new <code>PanListener</code> object.</p>
	 * 
	 * @param mapCtrl the <code>MapControl</code> where will be applied the changes
	 */
	public PanListener(MapControl mapCtrl) {
		super(mapCtrl);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.PanListenerImpl#move(com.iver.cit.gvsig.fmap.tools.Events.MoveEvent)
	 */
	public void move(MoveEvent event) {
		super.move(event);
		PluginServices.getMainFrame().enableControls();
	}
}
