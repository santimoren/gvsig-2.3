/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.prepareAction;

import org.gvsig.fmap.mapcontext.MapContext;

/**
 * Interface extending PrepareContextView. The methods defined
 * here are indended to be integrated on PrepareContextView, but
 * are maintained on this separate interface in order to mantain
 * compatibility with existing implementations.
 * 
 * This interface may be eventually integrated with PrepareContextView
 * on the next major refactoring.
 *
 * @author Cesar Martinez
 *
 */
public interface PrepareContextView_v1 extends PrepareContextView {

    /**
     * You can use it to extract information from
     * the MapContext that will receive the new layer.
     * For example, projection to use, or visible extent.
     * 
     * @return Returns the MapContext.
     */
    MapContext getMapContext();

    /**
     * Checks whether the MapControl is available.
     * The MapControl may not be available in some circumstances, for instance
     * when adding layers to a MapContext not associated with a View.
     * 
     * A MapContext should always be available on the {@link #getMapContext()}
     * method.
     * 
     * @return true if the MapControl is available, false otherwise
     */
    boolean isMapControlAvailable();

}



