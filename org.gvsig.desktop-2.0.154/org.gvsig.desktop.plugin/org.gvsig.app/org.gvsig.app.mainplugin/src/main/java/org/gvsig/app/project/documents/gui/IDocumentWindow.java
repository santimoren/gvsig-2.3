/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.app.project.documents.Document;

/**
 * @author Miguel �ngel Querol Carratal� <querol_mig@gva.es>
 * 
 *         Interfaz que tienen que implementar las clases de interfaz de
 *         usuario de los documentos que se quieran insertar en gvSIG. Estas
 *         clases se usar�n para cargar y guardar las propiedades graficas de la
 *         ventana
 *         del documento como tama�os y posiciones.
 */
public interface IDocumentWindow extends IWindow, IWindowListener,
    SingletonWindow {

    /**
     * M�todo para obtener un windowData con las propiedades de la
     * ventana del documento como pueden ser tama�os, posiciones y
     * estados de sliders, divisores etc. Este m�todo ser� llamado
     * para guardar los datos a disco. Lo llamar� la clase project para
     * obtener los datos y asi guardarlos.
     * 
     * @return un windowData con los datos de la ventana.
     */
    public WindowLayout getWindowLayout();

    /**
     * M�todo para cargar los datos de la ventana de proyecto. El widowData
     * guardado
     * se le pasa a la clase de interfaz de usuario correspondiente a la ventana
     * del documento.
     * 
     * @param winData
     */
    public void setWindowLayout(WindowLayout layout);

    public void setDocument(Document document);

    public Document getDocument();

}
