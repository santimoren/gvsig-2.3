/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.app.project.documents.view.gui.LayerProperties;
import org.gvsig.app.project.documents.view.toc.gui.FPopupMenu;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class LayerPropertiesExtension extends Extension {

    public void initialize() {
    }

    @Override
    public void postInitialize() {
        FPopupMenu.registerExtensionPoint();
        IconThemeHelper.registerIcon("action", "layer-properties", this);
        ProjectManager projectManager = ApplicationLocator.getProjectManager();
        ViewManager viewManager = (ViewManager) projectManager.getDocumentManager(ViewManager.TYPENAME);
        viewManager.addTOCContextAction("layer-properties","group-last",10000,900);    
    }

    public void execute(String s) {

        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();
        if (!document.getMapContext().hasActiveVectorLayers()) {
            return;
        }

        if (s.equalsIgnoreCase("layer-properties")) {
            FLayer layer = document.getMapContext().getLayers().getActives()[0];
            LayerProperties dlg = new LayerProperties(layer);
            WindowManager wm = ToolsSwingLocator.getWindowManager();
            wm.showWindow(
                    dlg.asJComponent(),
                    ToolsLocator.getI18nManager().getTranslation("propiedades_de_la_capa"),
                    WindowManager.MODE.WINDOW
            );
        }
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        return document.getMapContext().hasActiveVectorLayers();
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        return document.getMapContext().hasVectorLayers();
    }

}
