package org.gvsig.app.project.documents.view.toc;

import java.awt.Image;
import javax.swing.ImageIcon;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.app.project.documents.view.IContextMenuActionWithIcon;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractActionInfoAdapterToTocContextMenuAction extends AbstractTocContextMenuAction implements IContextMenuActionWithIcon  {

    private static final Logger logger = LoggerFactory.getLogger(AbstractActionInfoAdapterToTocContextMenuAction.class);
    protected ActionInfo action = null;
    protected String group = null;
    protected int groupOrder = 1;
    protected int order = 1;

    protected AbstractActionInfoAdapterToTocContextMenuAction(ActionInfo action, String group, int groupOrder, int order) {
        this.action = action;
        this.group = group;
        this.groupOrder = groupOrder;
        this.order = order;
    }

    protected AbstractActionInfoAdapterToTocContextMenuAction(ActionInfo action, String group, int groupOrder) {
        this(action, group, groupOrder, (int) action.getPosition());
    }

    protected AbstractActionInfoAdapterToTocContextMenuAction(String actionName, String group, int groupOrder, int order) {
        this(
                PluginsLocator.getActionInfoManager().getAction(actionName),
                group, groupOrder, order
        );
    }

    protected AbstractActionInfoAdapterToTocContextMenuAction(String actionName, String group, int groupOrder) {
        this(
                PluginsLocator.getActionInfoManager().getAction(actionName),
                group, groupOrder
        );
    }

    public String getGroup() {
        return this.group;
    }

    public int getGroupOrder() {
        return this.groupOrder;
    }

    public int getOrder() {
        return this.order;
    }

    public String getText() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation(this.action.getLabel());
    }

    public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
        return this.action.isEnabled();
    }

    public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
        boolean n = this.action.isVisible();
        return n;
    }

    public void execute(ITocItem item, FLayer[] selectedItems) {
        this.action.execute(selectedItems);
    }
    
    public ImageIcon getIcon() {
        return this.action.getIcon();
    }
}
