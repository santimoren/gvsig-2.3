/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.andami.actioninfo.ActionInfoManager;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.preferencespage.ViewPage;
import org.gvsig.app.project.documents.view.IContextMenuAction;
import org.gvsig.app.project.documents.view.toc.DnDJTree;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.ITocOrderListener;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.app.project.documents.view.toc.TocItemLeaf;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.events.AtomicEvent;
import org.gvsig.fmap.mapcontext.events.listeners.AtomicEventListener;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionListener;
import org.gvsig.fmap.mapcontext.layers.LayerPositionEvent;
import org.gvsig.fmap.mapcontext.layers.operations.Classifiable;
import org.gvsig.fmap.mapcontext.layers.operations.IHasImageLegend;
import org.gvsig.fmap.mapcontext.layers.operations.LayerCollection;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.IClassifiedLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendChangedEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendClearEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.listeners.LegendListener;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.utils.XMLEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fjp
 */
public class TOC extends JComponent implements ITocOrderListener,
    LegendListener, LayerCollectionListener, TreeExpansionListener,
    ComponentListener, LegendContentsChangedListener {

    /**
     *
     */
    private static final long serialVersionUID = 5689047685537359038L;

    /**
     * Useful for debug the problems during the implementation.
     */
    private static Logger logger = LoggerFactory.getLogger(TOC.class);

    private MapContext mapContext;

    private DnDJTree m_Tree;

    private DefaultTreeModel m_TreeModel;

    private DefaultMutableTreeNode m_Root;

    private TOCRenderer m_TocRenderer;

    private JScrollPane m_Scroller;

    // private ArrayList m_Listeners;
    private Map<String, Boolean> m_ItemsExpanded =
        new HashMap<String, Boolean>();

    private NodeSelectionListener nodeSelectionListener = null;

    /**
     * Crea un nuevo TOC.
     */
    public TOC() {
        this.setName("TOC");
        this.setLayout(new BorderLayout());
        this.setMinimumSize(new Dimension(100, 80));
        this.setPreferredSize(new Dimension(100, 80));

        // Construct the tree.
        m_Root = new DefaultMutableTreeNode(java.lang.Object.class);
        m_TreeModel = new DefaultTreeModel(m_Root);
        m_Tree = new DnDJTree(m_TreeModel);

        m_TocRenderer = new TOCRenderer(m_Tree.getBackground());
        m_Tree.setCellRenderer(m_TocRenderer);

        m_Tree.setRootVisible(false);

        // m_Tree.setExpandsSelectedPaths(true);
        // m_Tree.setAutoscrolls(true);
        m_Tree.setShowsRootHandles(true);

        // Posibilidad de seleccionar de forma aleatoria nodos de la leyenda.
        m_Tree.getSelectionModel().setSelectionMode(
            TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        nodeSelectionListener = new NodeSelectionListener(m_Tree);
        m_Tree.addMouseListener(nodeSelectionListener);
        // m_Tree.setBackground(UIManager.getColor("Button.background"));
        // m_Tree.setBorder(BorderFactory.createEtchedBorder());

        this.addComponentListener(this);

        m_Tree.addTreeExpansionListener(this);

        m_Tree.addOrderListener(this);

        m_Tree.setRowHeight(0); // Para que lo determine el renderer

        m_Scroller = new JScrollPane(m_Tree);
        m_Scroller.setBorder(BorderFactory.createEmptyBorder());

        // scrollPane.setPreferredSize(new Dimension(80,80));
        // Add everything to this panel.
        /*
         * GridBagLayout gridbag = new GridBagLayout(); GridBagConstraints c =
         * new GridBagConstraints(); setLayout(gridbag); c.fill =
         * GridBagConstraints.BOTH; c.weightx = 1.0;
         * gridbag.setConstraints(check,c);
         */
        add(m_Scroller); // , BorderLayout.WEST);

        // refresh();
    }

    /**
     * Elimina los listeners que actuan sobre el TOC, lo �nico que deja hacer es
     * desplegar la leyenda de las capas.
     */
    public void removeListeners() {
        m_Tree.removeMouseListener(nodeSelectionListener);
        m_Tree.invalidateListeners();
    }

    /**
     * Inserta el FMap.
     *
     * @param mc
     *            FMap.
     */
    public void setMapContext(MapContext mc) {
        mapContext = mc;
        mapContext.addAtomicEventListener(new AtomicEventListener() {

            /**
             * @see org.gvsig.fmap.mapcontext.events.listeners.AtomicEventListener#atomicEvent(org.gvsig.fmap.mapcontext.events.AtomicEvent)
             */
            public void atomicEvent(final AtomicEvent e) {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            atomicEvent(e);
                        }
                    });
                    return;
                }

                if ((e.getLayerCollectionEvents().length > 0)
                    || (e.getLegendEvents().length > 0)) {
                    refresh();
                }

                if (e.getLayerEvents().length > 0) {
                    repaint();
                }

                if (e.getExtentEvents().length > 0) {
                    repaint();
                }
                LayerCollectionEvent[] events = e.getLayerCollectionEvents();
                for( int i=0; i<events.length ; i++ ) {
                   if( events[i].getEventType() == LayerCollectionEvent.LAYER_ADDED ) {
                       if (PluginServices.getMainFrame() != null) {
                           PluginServices.getMainFrame().enableControls();
                       }
                   }
                   // ===================================================
                   // Change visibility of layer before adding, according to app preferences
                   if( events[i].getEventType() == LayerCollectionEvent.LAYER_ADDING ) {

                       if (invisibilityIsForced()) {
                           events[i].getAffectedLayer().setVisible(false);
                       }
                   }
                   // ===================================================
                }
            }
        });

        refresh();
    }

    /**
     * DOCUMENT ME!
     */
    private void setExpandedNodes(DefaultMutableTreeNode node) {
        // int i = 0;
        // Las claves sobrantes de m_ItemsExpanded (provocadas
        // por layerRemove, se quitan en el evento layerRemoved
        // de este TOC
        DefaultMutableTreeNode n;
        @SuppressWarnings("rawtypes")
        Enumeration enumeration = node.children();
        //Copia necesaria para que los cambios no adulteren el estado inicial del TOC
        Map<String, Boolean> itemsExpanded_copy = new HashMap<String, Boolean>(m_ItemsExpanded);

        while (enumeration.hasMoreElements()) {
            n = (DefaultMutableTreeNode) enumeration.nextElement();
            if (n.getChildCount() > 0) {
                setExpandedNodes(n);
            }
            TreePath path = new TreePath(m_TreeModel.getPathToRoot(n));
            ITocItem item = (ITocItem) n.getUserObject();
            Boolean b = (Boolean) itemsExpanded_copy.get(item.getLabel());

            if (b == null) // No estaba en el hash todav�a: valor por defecto
            {
                m_Tree.expandPath(path);

                return;
            }

            if (b.booleanValue()) {
                m_Tree.expandPath(path);
            } else {
                m_Tree.collapsePath(path);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.opensig.gui.IToc#refresh()
     */
    public void refresh() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    refresh();
                }
            });
            return;
        }
        LayerCollection theLayers = mapContext.getLayers();
        m_Root.removeAllChildren();
        m_Root.setAllowsChildren(true);
        doRefresh(theLayers, m_Root);

        m_TreeModel.reload();

        setExpandedNodes(m_Root);
    }

    private void doRefresh(LayerCollection theLayers,
        DefaultMutableTreeNode parentNode) {
        int width = m_Tree.getWidth();
        if (width == 0) {
            width = 300;
        }
        Dimension sizeLeaf = new Dimension(width, 15);
        // Get the tree font height
        Font font = m_Tree.getFont();
        FontMetrics metrics = this.getFontMetrics(font);
        Dimension sizeBranch = new Dimension(width, metrics.getHeight() + 4);

        for (int i = theLayers.getLayersCount() - 1; i >= 0; i--) {
            FLayer lyr = theLayers.getLayer(i);
            if (!lyr.isInTOC()) {
                continue;
            }
            TocItemBranch elTema = new TocItemBranch(lyr);
            elTema.setSize(sizeBranch);

            DefaultMutableTreeNode nodeLayer =
                new DefaultMutableTreeNode(elTema);

            m_TreeModel.insertNodeInto(nodeLayer, parentNode,
                parentNode.getChildCount());

            if (lyr instanceof LayerCollection) {
                LayerCollection group = (LayerCollection) lyr;
                doRefresh(group, nodeLayer);
            } else {
                if (lyr instanceof Classifiable) {// && !(lyr instanceof
                                                  // FLyrAnnotation)) {

                    Classifiable classifiable = (Classifiable) lyr;
                    ILegend legendInfo = classifiable.getLegend();

                    try {
                        if (legendInfo instanceof IClassifiedLegend) {
                            IClassifiedLegend cl =
                                (IClassifiedLegend) legendInfo;
                            String[] descriptions = cl.getDescriptions();
                            ISymbol[] symbols = cl.getSymbols();

                            for (int j = 0; j < descriptions.length; j++) {
                                TocItemLeaf itemLeaf;
                                itemLeaf =
                                    new TocItemLeaf(symbols[j],
                                        descriptions[j], classifiable.getShapeType());
                                itemLeaf.setSize(sizeLeaf);

                                DefaultMutableTreeNode nodeValue =
                                    new DefaultMutableTreeNode(itemLeaf);
                                m_TreeModel.insertNodeInto(nodeValue,
                                    nodeLayer, nodeLayer.getChildCount());

                            }
                        }

                        if (legendInfo instanceof ISingleSymbolLegend
                            && (legendInfo.getDefaultSymbol() != null)) {
                            TocItemLeaf itemLeaf;
                            itemLeaf =
                                new TocItemLeaf(legendInfo.getDefaultSymbol(),
                                    legendInfo.getDefaultSymbol()
                                        .getDescription(), classifiable.getShapeType());
                            itemLeaf.setSize(sizeLeaf);

                            DefaultMutableTreeNode nodeValue =
                                new DefaultMutableTreeNode(itemLeaf);
                            m_TreeModel.insertNodeInto(nodeValue, nodeLayer,
                                nodeLayer.getChildCount());
                        }

                        if (legendInfo instanceof IHasImageLegend) {
                            TocItemLeaf itemLeaf;
                            IHasImageLegend imageLegend = (IHasImageLegend) legendInfo;
                            Image image = imageLegend.getImageLegend();

                            int w = 0;
                            int h = 0;

                            if(image != null) {
                            	w = image.getWidth(null);
                            	h = image.getHeight(null);
                            }

                            if (image != null && w > 0 && h > 0) {
                                itemLeaf = new TocItemLeaf();
                                itemLeaf.setImageLegend(image, "", new Dimension(w, h));

                                DefaultMutableTreeNode nodeValue =
                                    new DefaultMutableTreeNode(itemLeaf);
                                m_TreeModel.insertNodeInto(nodeValue, nodeLayer,
                                    nodeLayer.getChildCount());
                            }
                        }
                    } catch (ReadException e) {
                        logger.error(MessageFormat.format(
                            "Can't add leyend of layer {0} to the TOC.", lyr),
                            e);
                    }
                }
            } // if instanceof layers
        }
    }

    /**
     * @see com.iver.cit.opensig.gui.toc.ITocOrderListener#orderChanged(int,
     *      int)
     */
    public void orderChanged(int oldPos, int newPos, FLayers lpd) {
        try {
            lpd.moveTo(oldPos, newPos);
        } catch (CancelationException e) {
            logger.error("Can't change order of layers in TOC", e);
        }
        mapContext.invalidate();
    }

    public void parentChanged(FLayers lpo, FLayers lpd, FLayer ls) {
        lpo.move(ls, lpd);
        mapContext.invalidate();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.
     * ComponentEvent)
     */
    public void componentHidden(ComponentEvent e) {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent
     * )
     */
    public void componentMoved(ComponentEvent e) {
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.ComponentListener#componentResized(java.awt.event.
     * ComponentEvent)
     */
    public void componentResized(ComponentEvent e) {
        DefaultMutableTreeNode n;
        @SuppressWarnings("rawtypes")
        Enumeration enumeration = m_Root.children();

        while (enumeration.hasMoreElements()) {
            n = (DefaultMutableTreeNode) enumeration.nextElement();

            if (n.getUserObject() instanceof TocItemBranch) {
                ITocItem item = (ITocItem) n.getUserObject();
                Dimension szAnt = item.getSize();
                item.setSize(new Dimension(this.getWidth() - 40, szAnt.height));
            }

        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent
     * )
     */
    public void componentShown(ComponentEvent e) {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerListener#legendChanged(com.iver.cit
     * .gvsig.fmap.rendering.LegendChangedEvent)
     */
    public void legendChanged(final LegendChangedEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    legendChanged(e);
                }
            });
            return;
        }
        refresh();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerAdded(com
     * .iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void layerAdded(final LayerCollectionEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    layerAdded(e);
                }
            });
            return;
        }
        FLayer layer = e.getAffectedLayer();
        if( layer instanceof FLyrVect ) {
            FLyrVect layerVect = (FLyrVect)layer;
            ILegend legend = layerVect.getLegend();
            switch( e.getEventType() ) {
                case LayerCollectionEvent.LAYER_ADDED :
                    legend.addLegendListener(this);
                    break;
                case LayerCollectionEvent.LAYER_REMOVED :
                    legend.removeLegendListener(this);
                    break;
            }
        }
        refresh();
    }

    public boolean symbolChanged(SymbolLegendEvent e) {
        refresh();
        return true;
    }

    public void legendCleared(LegendClearEvent event) {
        refresh();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerMoved(com
     * .iver.cit.gvsig.fmap.layers.LayerPositionEvent)
     */
    public void layerMoved(final LayerPositionEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    layerMoved(e);
                }
            });
            return;
        }
        refresh();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerRemoved(com
     * .iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void layerRemoved(final LayerCollectionEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    layerRemoved(e);
                }
            });
            return;
        }
        m_ItemsExpanded.remove(e.getAffectedLayer().getName());
        refresh();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerAdding(com
     * .iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void layerAdding(LayerCollectionEvent e) throws CancelationException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerMoving(com
     * .iver.cit.gvsig.fmap.layers.LayerPositionEvent)
     */
    public void layerMoving(LayerPositionEvent e) throws CancelationException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#layerRemoving(
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void layerRemoving(LayerCollectionEvent e)
        throws CancelationException {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#activationChanged
     * (com.iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void activationChanged(LayerCollectionEvent e)
        throws CancelationException {
        repaint();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.layers.LayerCollectionListener#visibilityChanged
     * (com.iver.cit.gvsig.fmap.layers.LayerCollectionEvent)
     */
    public void visibilityChanged(final LayerCollectionEvent e)
        throws CancelationException {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    visibilityChanged(e);
                }
            });
            return;
        }
        repaint();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.swing.event.TreeExpansionListener#treeCollapsed(javax.swing.event
     * .TreeExpansionEvent)
     */
    public void treeCollapsed(TreeExpansionEvent event) {
        TreePath path = event.getPath();
        DefaultMutableTreeNode n =
            (DefaultMutableTreeNode) path.getLastPathComponent();

        if (n.getUserObject() instanceof ITocItem) {
            ITocItem item = (ITocItem) n.getUserObject();
            Boolean b = Boolean.FALSE;

            m_ItemsExpanded.put(item.getLabel(), b);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.swing.event.TreeExpansionListener#treeExpanded(javax.swing.event
     * .TreeExpansionEvent)
     */
    public void treeExpanded(TreeExpansionEvent event) {
        TreePath path = event.getPath();
        DefaultMutableTreeNode n =
            (DefaultMutableTreeNode) path.getLastPathComponent();

        if (n.getUserObject() instanceof ITocItem) {
            ITocItem item = (ITocItem) n.getUserObject();
            Boolean b = Boolean.TRUE;

            m_ItemsExpanded.put(item.getLabel(), b);
        }
    }

    /**
     * Obtiene el JScrollPane que contiene el TOC
     *
     * @return JScrollPane que contiene el TOC
     */
    public JScrollPane getJScrollPane() {
        return this.m_Scroller;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public DnDJTree getTree() {
        return m_Tree;
    }

    /**
     * Clase Listener que reacciona al pulsar sobre el checkbox de un nodo y
     * crea un popupmenu al pulsar el bot�n derecho.
     */
    class NodeSelectionListener extends MouseAdapter implements ActionListener {

        JTree tree;

        JDialog dlg;

        JColorChooser colorChooser;

        FPopupMenu popmenu = null;

        DefaultMutableTreeNode node;

        /**
         * Crea un nuevo NodeSelectionListener.
         *
         * @param tree
         *            DOCUMENT ME!
         */
        NodeSelectionListener(JTree tree) {
            this.tree = tree;
        }

        /**
         * DOCUMENT ME!
         *
         * @param e
         *            DOCUMENT ME!
         */
        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            int row = tree.getRowForLocation(x, y);
            TreePath path = tree.getPathForRow(row);
            LayerCollection layers = mapContext.getLayers();

            if (path != null) {
                if (e.getClickCount() == 1) {
                    // this fixes a bug when double-clicking. JTree by default
                    // expands the tree when double-clicking, so we capture a
                    // different node in the second click than in the first
                    node = (DefaultMutableTreeNode) path.getLastPathComponent();
                }

                if (node != null
                    && node.getUserObject() instanceof TocItemBranch) {
                    // double click with left button ON A BRANCH/NODE (layer)
                    if (e.getClickCount() >= 2
                        && e.getButton() == MouseEvent.BUTTON1) {
                        e.consume();
                        PluginServices.getMDIManager().setWaitCursor();
                        try {
                            TocItemBranch leaf =
                                (TocItemBranch) node.getUserObject();

                            IContextMenuAction action =
                                leaf.getDoubleClickAction();
                            if (action != null) {
                                /*
                                 * if there is an action associated with the
                                 * double-clicked element it will be fired for
                                 * it and FOR ALL OTHER COMPATIBLES THAT HAVE
                                 * BEEN ACTIVATED.
                                 */
                                ArrayList<FLayer> targetLayers =
                                    new ArrayList<FLayer>();

                                TocItemBranch owner =
                                    (TocItemBranch) node.getUserObject();

                                FLayer masterLayer = owner.getLayer();
                                targetLayers.add(masterLayer);
                                FLayer[] actives =
                                    mapContext.getLayers().getActives();
                                for (int i = 0; i < actives.length; i++) {
                                    if (actives[i].getClass().equals(
                                        masterLayer.getClass())) {
                                        if (actives[i] instanceof FLyrVect) {
                                            FLyrVect vectorLayer =
                                                (FLyrVect) actives[i];
                                            FLyrVect vectorMaster =
                                                (FLyrVect) masterLayer;
                                            if (vectorLayer.getShapeType() == vectorMaster
                                                .getShapeType()) {
                                                targetLayers.add(vectorLayer);
                                            } else {
                                                vectorLayer.setActive(false);
                                            }
                                        }
                                        // TODO for the rest of layer types
                                        // (i.e. FLyrRaster)
                                    } else {
                                        actives[i].setActive(false);
                                    }
                                }

                                // Do nothing if there is a non-available layer
                                for (int k=0; k<targetLayers.size(); k++) {
                                    if (!targetLayers.get(k).isAvailable()) {
                                        return;
                                    }
                                }

                                action.execute(leaf,
                                    targetLayers.toArray(new FLayer[0]));
                            }
                        } catch (Exception ex) {
                            NotificationManager.addError(ex);
                        } finally {
                            PluginServices.getMDIManager().restoreCursor();
                        }
                        return;
                    }

                    TocItemBranch elTema = (TocItemBranch) node.getUserObject();
                    FLayer lyr = elTema.getLayer();
                    lyr.getMapContext().beginAtomicEvent();

                    if (((e.getModifiers() & InputEvent.SHIFT_MASK) != 0)
                        && (
                            e.getButton() == MouseEvent.BUTTON1 ||
                            e.getButton() == MouseEvent.BUTTON3
                            )
                       ) {
                        FLayer[] activeLayers = layers.getActives();
                        if (activeLayers.length > 0) {
                            selectInterval(layers, lyr);
                        } else {
                            updateActive(lyr, !lyr.isActive());
                        }

                    } else {
                        if (!((e.getModifiers() & InputEvent.CTRL_MASK) != 0)
                            && (
                                e.getButton() == MouseEvent.BUTTON1 ||
                                e.getButton() == MouseEvent.BUTTON3
                                )
                           ) {
                            layers.setAllActives(false);
                        }
                        if (e.getButton() == MouseEvent.BUTTON1
                            ||e.getButton() == MouseEvent.BUTTON3) {
                            // lyr.setActive(true);
                            updateActive(lyr, !lyr.isActive());
                        }
                    }
                    // Si pertenece a un grupo, lo ponemos activo tambi�n.
                    // FLayer parentLayer = lyr.getParentLayer();

                    /*
                     * if (parentLayer != null) { parentLayer.setActive(true); }
                     */
                    Point layerNodeLocation =
                        tree.getUI().getPathBounds(tree, path).getLocation();

                    // Rect�ngulo que representa el checkbox
                    Rectangle checkBoxBounds =
                        m_TocRenderer.getCheckBoxBounds();
                    checkBoxBounds.translate((int) layerNodeLocation.getX(),
                        (int) layerNodeLocation.getY());

                    if (checkBoxBounds.contains(e.getPoint())) {
                        updateVisible(lyr);
                    }

                    // }
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        // Boton derecho sobre un nodo del arbol
                        // if ((e.getModifiers() & InputEvent.META_MASK) != 0) {
                        //***** Change selection
//                        if(!lyr.isActive()){
//                            layers.setAllActives(false);
//                            updateActive(lyr, true);
//                        }
                        //*****
                        popmenu = new FPopupMenu(mapContext, node);
                        tree.add(popmenu);

                        popmenu.show(e.getComponent(), e.getX(), e.getY());

                        // }
                    }

                    lyr.getMapContext().endAtomicEvent();
                }

                if (node != null && node.getUserObject() instanceof TocItemLeaf) {
                    TocItemBranch owner =
                        (TocItemBranch) ((DefaultMutableTreeNode) node
                            .getParent()).getUserObject();

                    FLayer masterLayer = owner.getLayer();

                    if (((e.getModifiers() & InputEvent.SHIFT_MASK) != 0)
                        && (
                            e.getButton() == MouseEvent.BUTTON1 ||
                            e.getButton() == MouseEvent.BUTTON3
                            )
                       ) {
                        FLayer[] activeLayers = layers.getActives();
                        if (activeLayers.length > 0) {
                            selectInterval(layers, masterLayer);
                        } else {
                            updateActive(masterLayer, !masterLayer.isActive());
                        }

                    } else {
                        if (!((e.getModifiers() & InputEvent.CTRL_MASK) != 0)
                            && (
                                e.getButton() == MouseEvent.BUTTON1 ||
                                e.getButton() == MouseEvent.BUTTON3
                                )
                           ) {
                            layers.setAllActives(false);
                        }
                        if (e.getButton() == MouseEvent.BUTTON1
                            ||e.getButton() == MouseEvent.BUTTON3) {
                            // lyr.setActive(true);
                            updateActive(masterLayer, !masterLayer.isActive());
                        }
                    }

                    // double click with left button ON A LEAF (ISymbol)
                    if (e.getClickCount() >= 2
                        && e.getButton() == MouseEvent.BUTTON1) {
                        e.consume();

                        PluginServices.getMDIManager().setWaitCursor();
                        try {
                            TocItemLeaf leaf =
                                (TocItemLeaf) node.getUserObject();
                            IContextMenuAction action =
                                leaf.getDoubleClickAction();
                            if (action != null) {
                                /*
                                 * if there is an action associated with the
                                 * double-clicked element it will be fired for
                                 * it and FOR ALL OTHER COMPATIBLES THAT HAVE
                                 * BEEN ACTIVATED.
                                 */
                                /*
                                 * #3035: Symbology is applied to active layers too
                                 * Now it will be done only on the double-clicked one
                                 */
                                ArrayList<FLayer> targetLayers =
                                    new ArrayList<FLayer>();


                                targetLayers.add(masterLayer);
//                                FLayer[] actives =
//                                    mapContext.getLayers().getActives();
//                                for (int i = 0; i < actives.length; i++) {
//                                    if (actives[i].getClass().equals(
//                                        masterLayer.getClass())) {
//                                        if (actives[i] instanceof FLyrVect) {
//                                            FLyrVect vectorLayer =
//                                                (FLyrVect) actives[i];
//                                            FLyrVect vectorMaster =
//                                                (FLyrVect) masterLayer;
//                                            int masterLayerShapetypeOF_THE_LEGEND =
//                                                ((IVectorLegend) vectorMaster
//                                                    .getLegend())
//                                                    .getShapeType();
//                                            int anotherVectorLayerShapetypeOF_THE_LEGEND =
//                                                ((IVectorLegend) vectorLayer
//                                                    .getLegend())
//                                                    .getShapeType();
//                                            if (masterLayerShapetypeOF_THE_LEGEND == anotherVectorLayerShapetypeOF_THE_LEGEND) {
//                                                targetLayers.add(vectorLayer);
//                                            } else {
//                                                vectorLayer.setActive(false);
//                                            }
//                                        }
//                                        // TODO for the rest of layer types
//                                        // (i.e. FLyrRaster)
//                                    } else {
//                                        actives[i].setActive(false);
//                                    }
//                                }
                                action.execute(leaf,
                                    targetLayers.toArray(new FLayer[0]));
                            }
                        } catch (Exception ex) {
                            NotificationManager.addError(ex);
                        } finally {
                            PluginServices.getMDIManager().restoreCursor();
                        }
                        return;
                    }

                    // Boton derecho sobre un Simbolo
                    // TocItemLeaf auxLeaf = (TocItemLeaf) node.getUserObject();
                    // FSymbol theSym = auxLeaf.getSymbol();
                    if ((e.getModifiers() & InputEvent.META_MASK) != 0) {
                        //***** Change selection
                        if (!masterLayer.isActive()){
                            layers.setAllActives(false);
                            updateActive(masterLayer, true);
                        }
                        //*****
                        popmenu = new FPopupMenu(mapContext, node);
                        tree.add(popmenu);
                        popmenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }

                ((DefaultTreeModel) tree.getModel()).nodeChanged(node);

                if (row == 0) {
                    tree.revalidate();
                    tree.repaint();
                }

                //FIXME Is it really necessary?
                if (PluginServices.getMainFrame() != null) {
                    PluginServices.getMainFrame().enableControls();
                }
            } else {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    popmenu = new FPopupMenu(mapContext, null);
                    tree.add(popmenu);
                    popmenu.show(e.getComponent(), e.getX(), e.getY());
                }

            }
        }

        private void selectInterval(LayerCollection layers, FLayer lyr) {
            FLayer[] activeLayers = layers.getActives();
            // if (activeLayers[0].getParentLayer() instanceof FLayers &&
            // activeLayers[0].getParentLayer().getParentLayer()!=null) {
            // selectInterval((LayerCollection)activeLayers[0].getParentLayer(),lyr);
            // }
            for (int j = 0; j < layers.getLayersCount(); j++) {
                FLayer layerAux = layers.getLayer(j);
                // Si se cumple esta condici�n es porque la primera capa que nos
                // encontramos en el TOC es la que estaba activa
                if (activeLayers[0].equals(layerAux)) {
                    boolean isSelected = false;
                    for (int i = 0; i < layers.getLayersCount(); i++) {
                        FLayer layer = layers.getLayer(i);
                        if (!isSelected) {
                            isSelected = layer.isActive();
                        } else {
                            updateActive(layer, true);
                            if (lyr.equals(layer)) {
                                isSelected = false;
                            }
                        }
                    }
                    break;
                } else
                    // Si se cumple esta condici�n es porque la primera capa que
                    // nos
                    // encontramos en el TOC es la que acabamos de seleccionar
                    if (lyr.equals(layerAux)) {
                        boolean isSelected = false;
                        for (int i = layers.getLayersCount() - 1; i >= 0; i--) {
                            FLayer layer = layers.getLayer(i);
                            if (!isSelected) {
                                isSelected = layer.isActive();
                            } else {
                                updateActive(layer, true);
                                if (lyr.equals(layer)) {
                                    isSelected = false;
                                }
                            }
                        }
                        break;
                    }
            }

        }

        /**
         * DOCUMENT ME!
         *
         * @param lyr
         *            DOCUMENT ME!
         * @param active
         *            DOCUMENT ME!
         */
        private void updateActive(FLayer lyr, boolean active) {
            lyr.setActive(active);
            updateActiveChild(lyr);
        }

        /**
         * DOCUMENT ME!
         *
         * @param lyr
         *            DOCUMENT ME!
         */
        private void updateActiveChild(FLayer lyr) {
            if (lyr instanceof FLayers) { // Es la raiz de una rama o
                // cualquier nodo intermedio.

                FLayers layergroup = (FLayers) lyr;

                for (int i = 0; i < layergroup.getLayersCount(); i++) {
                    layergroup.getLayer(i).setActive(lyr.isActive());
                    updateActiveChild(layergroup.getLayer(i));
                }
            }
        }

        /**
         * Actualiza la visibilidad de la capas.
         *
         * @param lyr
         *            Capa sobre la que se est� clickando.
         */
        private void updateVisible(FLayer lyr) {
            if (lyr.isAvailable()) {
                lyr.setVisible(!lyr.visibleRequired());
                updateVisibleChild(lyr);
                updateVisibleParent(lyr);
            }
        }

        /**
         * Actualiza de forma recursiva la visibilidad de los hijos de la capa
         * que se pasa como par�metro.
         *
         * @param lyr
         *            Capa a actualizar.
         */
        private void updateVisibleChild(FLayer lyr) {
            if (lyr instanceof FLayers) { // Es la raiz de una rama o
                                          // cualquier nodo intermedio.

                FLayers layergroup = (FLayers) lyr;

                for (int i = 0; i < layergroup.getLayersCount(); i++) {
                    layergroup.getLayer(i).setVisible(lyr.visibleRequired());
                    updateVisibleChild(layergroup.getLayer(i));
                }
            }
        }

        /**
         * Actualiza de forma recursiva la visibilidad del padre de la capa que
         * se pasa como par�metro.
         *
         * @param lyr
         *            Capa a actualizar.
         */
        private void updateVisibleParent(FLayer lyr) {
            FLayers parent = lyr.getParentLayer();

            if (parent != null) {
                boolean parentVisible = false;

                for (int i = 0; i < parent.getLayersCount(); i++) {
                    if (parent.getLayer(i).visibleRequired()) {
                        parentVisible = true;
                    }
                }

                parent.setVisible(parentVisible);
                updateVisibleParent(parent);
            }
        }

        /**
         * DOCUMENT ME!
         *
         * @param arg0
         *            DOCUMENT ME!
         */
        public void actionPerformed(ActionEvent arg0) {
        }

        /**
         * DOCUMENT ME!
         *
         * @param arg0
         *            DOCUMENT ME!
         */
        public void mouseReleased(MouseEvent arg0) {
            super.mouseReleased(arg0);
        }

        /**
         * DOCUMENT ME!
         *
         * @param arg0
         *            DOCUMENT ME!
         */
        public void mouseEntered(MouseEvent arg0) {
            super.mouseEntered(arg0);
        }
    }

    private boolean invisibilityIsForced() {

        PluginServices ps = PluginServices.getPluginServices(this);
        XMLEntity xml = ps.getPersistentXML();

        if (xml.contains(ViewPage.ADD_NEW_LAYERS_IN_INVISIBLE_MODE_KEY_NAME)) {
            // Get invisibility value
            return xml.getBooleanProperty(ViewPage.ADD_NEW_LAYERS_IN_INVISIBLE_MODE_KEY_NAME);
        } else {
            // not found in preferences
            return false;
        }
    }

}
