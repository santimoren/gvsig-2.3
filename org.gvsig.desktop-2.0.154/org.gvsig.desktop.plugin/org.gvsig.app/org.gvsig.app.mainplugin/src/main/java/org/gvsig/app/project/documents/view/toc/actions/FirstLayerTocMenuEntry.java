/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;


/**
 * Cambia la posici�n actual del layer a la primera posici�n.
 *
 * @author Vicente Caballero Navarro
 */
public class FirstLayerTocMenuEntry extends AbstractTocContextMenuAction {
	public String getGroup() {
		return "group4"; //FIXME
	}

	public int getGroupOrder() {
		return 40;
	}

	public int getOrder() {
		return 2;
	}

	public String getText() {
		return PluginServices.getText(this, "colocar_delante");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
        return true;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)){
			return true;
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
      	if (isTocItemBranch(item)){
            if (getMapContext().getLayers().getActives().length == 1) {
                FLayer layer=((TocItemBranch)item).getLayer();
                FLayers layers=layer.getParentLayer();
                for (int i=0;i<layers.getLayersCount();i++){
                    if(layers.getLayer(i).equals(layer)){
                    	layers.moveTo(layers.getLayersCount()-1-i, 0);
                    }
                }
            } else if (selectedItems.length > 1) {
                FLayer[] actives =selectedItems;
                FLayers layers=actives[0].getParentLayer();
                for (int i=0;i<actives.length;i++){
                    for (int j=0;j<layers.getLayersCount();j++){
                        if(layers.getLayer(j).equals(actives[i])){
                        	layers.moveTo(layers.getLayersCount()-1-j, 0);
                        }
                    }
                }
            }
		}
		// TRUCO PARA REFRESCAR.
        getMapContext().invalidate();
        Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
		project.setModified(true);
		PluginServices.getMainFrame().enableControls();
	}
}
