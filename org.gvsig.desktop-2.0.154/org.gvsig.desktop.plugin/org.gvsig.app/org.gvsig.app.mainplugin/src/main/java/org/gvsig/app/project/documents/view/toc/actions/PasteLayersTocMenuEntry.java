/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.persistence.exception.PersistenceException;


public class PasteLayersTocMenuEntry extends AbstractTocContextMenuAction {
    
    private static Logger logger =
        LoggerFactory.getLogger(PasteLayersTocMenuEntry.class);

	public String getGroup() {
		return "copyPasteLayer";
	}

	public int getGroupOrder() {
		return 60;
	}

	public int getOrder() {
		return 2;
	}

	public String getText() {
		return PluginServices.getText(this, "pegar");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			FLayer lyr = getNodeLayer(item);
			if (lyr instanceof FLayers) {
			    /*
			     * We cannot parse it here because it can
			     * lead to bad error dialog. We accept anything.
			     * When user tries to paste, the error
			     * message will show if there is a problem.
			     */
			    return !CopyPasteLayersUtils.isClipboardEmpty();
			}

		} else if (!isTocItemLeaf(item)) {
			if (getNodeLayer(item) == null) {
                /*
                 * We cannot parse it here because it can
                 * lead to bad error dialog. We accept anything.
                 * When user tries to paste, the error
                 * message will show if there is a problem.
                 */
			    return !CopyPasteLayersUtils.isClipboardEmpty();
			}
		}
		return false;
	}

	/**
	 * This method can return null (if content of clipboard not useful)
	 * 
	 * @return
	 */
	private FLayers getFLayersFromClipboard() {
	    
		FLayers resp = null;
        try {
            resp = CopyPasteLayersUtils.getClipboardAsFLayers();
            // resp can be null here if clipboard content not useful
        } catch (PersistenceException e) {
            ApplicationLocator.getManager().message(
                Messages.getText("_Clipboard_error"),
                JOptionPane.ERROR_MESSAGE);
            resp = null;
        }
		return resp;
	}

	public void execute(ITocItem item, FLayer[] selectedItems) {
		FLayers target_root;

        FLayers clipboard_root = this.getFLayersFromClipboard();
        if (clipboard_root == null) {
            JOptionPane.showMessageDialog(
                (Component)PluginServices.getMainFrame(),
                Messages.getText("No_ha_sido_posible_realizar_la_operacion"),
                Messages.getText("pegar"),
                JOptionPane.ERROR_MESSAGE
                );
            logger.info("Unable to parse clipboard as flayers");
            return;
        }

		if (isTocItemBranch(item)) {
		    target_root = (FLayers)getNodeLayer(item);
		} else if (getNodeLayer(item) == null){
		    target_root = getMapContext().getLayers();
		} else {
			return;
		}
		
		getMapContext().beginAtomicEvent();

		boolean isOK = CopyPasteLayersUtils.addLayers(clipboard_root, target_root);

		getMapContext().endAtomicEvent();

		if (isOK) {
			getMapContext().invalidate();
			Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
			project.setModified(true);
		}
	}


}
