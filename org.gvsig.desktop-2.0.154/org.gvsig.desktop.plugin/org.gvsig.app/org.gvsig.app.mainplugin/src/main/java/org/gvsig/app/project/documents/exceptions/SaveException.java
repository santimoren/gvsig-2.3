/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.exceptions;

import java.util.Map;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.utils.XMLException;


public class SaveException extends XMLException {
	private Exception e;
	private String c;
	public SaveException(Exception e, String c) {
		this.e=e;
		this.c=c;
	}
	public SaveException() {
		// TODO Auto-generated constructor stub
	}
	public void showError(){
		NotificationManager.addError("Fallo guardando el Proyecto en : "+c,e);
	}
	public void showInfo(){
		NotificationManager.addInfo("Guardando el Proyecto en : "+c,e);
	}
	public void showWarning(){
		NotificationManager.addWarning("Guardando el Proyecto en : "+c,e);
	}
	protected Map values() {
		// TODO Auto-generated method stub
		return null;
	}
}
