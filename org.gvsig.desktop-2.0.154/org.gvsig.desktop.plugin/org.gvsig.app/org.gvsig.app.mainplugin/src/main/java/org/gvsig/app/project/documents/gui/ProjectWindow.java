/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.help.Help;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.usability.UsabilitySwingManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.utils.DefaultListModel;

/**
 * Clase principal del proyecto donde se puede operar para crear cualquier tipo
 * de documento.
 *
 * @author Vicente Caballero Navarro
 * @author gvSIG team
 */
public class ProjectWindow extends JPanel implements PropertyChangeListener,
    IWindow, SingletonWindow, Observer {

    private static final long serialVersionUID = 7315293357719796556L;

    public static final String PERSISTENCE_DEFINITION_NAME = "ProjectWindow";

    private JScrollPane documentTypes = null;
    private JPanel documentTypesList = null;
    private JRadioButton[] btnsDocuments = null;
    private ButtonGroup grupo = new ButtonGroup();
    private JPanel documentsPanel = null;
    private JList lstDocs = null;
    private JPanel jPanel2 = null;
    private JButton btnNuevo = null;
    private JButton btnPropiedades = null;
    private JButton btnAbrir = null;
    private JButton btnBorrar = null;
    private JButton btnRenombrar = null;
    private JPanel propertiesPanel = null;
    private JLabel jLabel = null;
    private JLabel lblNombreSesion = null;
    private JLabel jLabel1 = null;
    private JLabel lblGuardado = null;
    private JLabel jLabel2 = null;
    private JLabel lblFecha = null;
    private JButton btnImportar = null;
    private JButton btnExportar = null;
    private JButton btnEditar = null;
    // The properties of the Project Manager window (size, position, etc):
    private WindowInfo m_viewInfo = null;

    private Project project;
    private UsabilitySwingManager manager = ToolsSwingLocator
        .getUsabilitySwingManager();

    private JScrollPane documentsScrollPane = null;
    private JPanel projectButtonsPanel = null;

    /**
     * This is the default constructor
     *
     * @param project
     *            Extension
     */
    public ProjectWindow() {
        super();
        initialize();
        refreshControls();
        Help.getHelp().enableHelp(this, this.getClass().getName());
        // Observe document factory registration changes
        ProjectManager.getInstance().addObserver(this);
    }

    /**
     * Asigna el proyecto a la ventana
     *
     * @param project
     *            Proyecto con el que se operar� a trav�s de este di�logo
     */
    public void setProject(Project project) {
        this.project = project;
        project.addPropertyChangeListener(this);
        refreshControls();
    }

    /**
     * Activa los botones de la botonera del medio o los desactiva en funci�n de
     * que est� seleccionado o no un elemento de proyecto en la lista del medio
     */
    private void activarBotones() {
        if (lstDocs.getSelectedIndex() != -1) {
            btnAbrir.setEnabled(true);
            btnBorrar.setEnabled(true);
            if (lstDocs.getSelectedIndices().length == 1) {
                btnRenombrar.setEnabled(true);
                btnPropiedades.setEnabled(true);
            } else {
                btnRenombrar.setEnabled(false);
                btnPropiedades.setEnabled(false);
            }
        } else {
            btnAbrir.setEnabled(false);
            btnBorrar.setEnabled(false);
            btnRenombrar.setEnabled(false);
            btnPropiedades.setEnabled(false);
        }
    }

    /**
     * Refresca la lista de elementos de proyecto con vistas, mapas o tablas,
     * seg�n la opci�n activada
     */
    private void refreshList() {
        if (project != null) {
            DefaultListModel model = null;
            String tituloMarco =
                PluginServices.getText(this, "documentos_existentes");

            String doctype = getDocumentSelected();
            model = new DefaultListModel(project.getDocuments(doctype));
            tituloMarco = getDocumentSelectedName();
            Object selectedDoc = lstDocs.getSelectedValue();
            lstDocs.setModel(model);
            if (selectedDoc!=null) {
            	lstDocs.setSelectedValue(selectedDoc, false);
            }
            
            ((TitledBorder) getDocumentsPanel().getBorder())
                .setTitle(tituloMarco);
            getDocumentsPanel().repaint(1);
            activarBotones();
        }
    }

    /**
     * Devuelve el nombre del tipo de documento activo (el mismo que
     * ProjectDocumentFactory.getRegisterName)
     *
     *
     * @return
     */

    public String getDocumentSelected() {
        JRadioButton btnSelected = null;
        for (int i = 0; i < btnsDocuments.length; i++) {
            if (btnsDocuments[i].isSelected()) {
                btnSelected = btnsDocuments[i];
                return btnSelected.getName();
            }
        }

        return null;
    }

    private String getDocumentSelectedName() {
        JRadioButton btnSelected = null;
        for (int i = 0; i < btnsDocuments.length; i++) {
            if (btnsDocuments[i].isSelected()) {
                btnSelected = btnsDocuments[i];
                return btnSelected.getText();
            }
        }

        return null;
    }

    /**
     * Refresca las etiquetas con la informaci�n del proyecto
     */
    private void refreshProperties() {
        if (project != null) {
            lblNombreSesion.setText(project.getName());
            String path = ProjectExtension.getPath();
            if (path != null) {
                File f = new File(path);
                lblGuardado.setText(f.toString());
            } else {
                lblGuardado.setText("");
            }
            lblFecha.setText(project.getCreationDate());
        }
    }

    /**
     * Refresca todo el di�logo
     */
    public void refreshControls() {
        refreshList();
        refreshProperties();
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(2, 5, 2, 5);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.weighty = 0.0d;
        c.gridy = 0;
        add(getDocumentTypesPanel(), c);

        c.fill = GridBagConstraints.BOTH;
        c.gridy = 1;
        c.weightx = 1.0d;
        c.weighty = 1.0d;
        add(getDocumentsPanel(), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.weighty = 0.0d;
        c.gridy = 2;
        add(getPropertiesPanel(), c);

        this.setMinimumSize(this.getPreferredSize());
        refreshDocuments();
    }

    private void createDocumentsInProject(String doctype) {
        Iterator<? extends Document> documents =
            ProjectManager.getInstance().createDocumentsByUser(doctype);
        for (; (documents != null) && (documents.hasNext());) {
            try {
                Document document = documents.next();
                project.add(document);
                int index =
                    ((DefaultListModel) lstDocs.getModel()).indexOf(document);
                lstDocs.setSelectedIndex(index);
                lstDocs.requestFocus();
                openDocumentWindow(document);
            } catch (Exception e) {
                NotificationManager.addError(e);
            }
        }
    }

    /**
     * Crea un nuevo project element
     *
     * @throws Exception
     *             DOCUMENT ME!
     */
    private void newProjectDocument() throws Exception {
        String doctype = getDocumentSelected();
        createDocumentsInProject(doctype);
        lstDocs.setSelectedIndex(lstDocs.getModel().getSize()-1);
    }

    /**
     * Abre la ventana de un nuevo documento
     */
    private void abrir() {
        int[] indexes = lstDocs.getSelectedIndices();
        for (int i = indexes.length - 1; i >= 0; i--) {
            int index = indexes[i];
            if (index == -1) {
                return;
            }
            String doctype = getDocumentSelected();
            List<Document> documents = project.getDocuments(doctype);
            Document doc = documents.get(index);
            if(!doc.isAvailable()){
                I18nManager i18nManager = ToolsLocator.getI18nManager();
                JOptionPane.showMessageDialog(
                    this,
                    i18nManager.getTranslation("document_not_available"),
                    i18nManager.getTranslation("abrir"),
                    JOptionPane.WARNING_MESSAGE);
                return;
            }
            openDocumentWindow(doc);
        }
    }

    private void openDocumentWindow(Document doc) {
        if (doc != null) {
            IWindow window = doc.getFactory().getMainWindow(doc);
            if (window == null) {
                JOptionPane.showMessageDialog(
                    (Component) PluginServices.getMainFrame(),
                    PluginServices.getText(this, "error_opening_the_document"));
                return;
            }
            PluginServices.getMDIManager().addWindow(window,
                MDIManager.ALIGN_FIRST_LINE_END_CASCADE);
            project.setModified(true);
        }
    }

    /**
     * Cambia el nombre de un project element
     */
    private void renombrar() {
        int index = lstDocs.getSelectedIndex();

        if (index == -1) {
            return;
        }
        String doctype = getDocumentSelected();
        List<Document> documents = project.getDocuments(doctype);
        Document doc = documents.get(index);

        JOptionPane pane = new JOptionPane();
        pane.setMessage(PluginServices.getText(this, "introduce_nombre"));
        pane.setMessageType(JOptionPane.QUESTION_MESSAGE);
        pane.setWantsInput(true);
        pane.setInitialSelectionValue(doc.getName());
        pane.setInputValue(doc.getName());
        JDialog dlg =
            pane.createDialog((Component) PluginServices.getMainFrame(),
                PluginServices.getText(this, "renombrar"));
        dlg.setModal(true);
        dlg.setVisible(true);

        String nuevoNombre = pane.getInputValue().toString().trim();

        if (nuevoNombre.length() == 0) {
            return;
        }

        for (int i = 0; i < lstDocs.getModel().getSize(); i++) {
            if (i == index) {
                continue;
            }
            if (((AbstractDocument) lstDocs.getModel().getElementAt(i))
                .getName().equals(nuevoNombre)) {
                JOptionPane.showMessageDialog(
                    (Component) PluginServices.getMainFrame(),
                    PluginServices.getText(this, "elemento_ya_existe"));
                return;
            }

        }
        doc.setName(nuevoNombre);

        refreshList();
        project.setModified(true);
    }

    /**
     * Borra un project element
     */
    private void borrar() {
        int res =
            JOptionPane.showConfirmDialog(
                (Component) PluginServices.getMainFrame(),
                PluginServices.getText(this, "confirmar_borrar"),
                PluginServices.getText(this, "borrar"),
                JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

        int lastremoved = 0;
        if (res == JOptionPane.YES_OPTION) {
            int[] indexes = lstDocs.getSelectedIndices();
            for (int i = indexes.length - 1; i >= 0; i--) {
                int index = indexes[i];
                String s = getDocumentSelected();
                List<Document> documents = project.getDocuments(s);
                Document doc = documents.get(index);
                if (doc.isLocked()) {
                    JOptionPane.showMessageDialog(this, PluginServices.getText(
                        this, "locked_element_it_cannot_be_deleted"));
                    return;
                }
                PluginServices.getMDIManager().closeSingletonWindow(doc);
                project.remove(doc);
                lastremoved = index;
            }
            if (lastremoved > lstDocs.getModel().getSize()) {
                lastremoved = lstDocs.getModel().getSize();
            }
            if (lastremoved >= 0) {
                lstDocs.setSelectedIndex(lastremoved);
            }
            // refreshList();
            project.setModified(true);
        }
    }

    /**
     * Muestra el di�logo de propiedades de un project element
     */
    private void propiedades() {
        int index = lstDocs.getSelectedIndex();

        if (index == -1) {
            return;
        }

        IWindow dlg = null;
        String doctype = getDocumentSelected();
        List<Document> documents = project.getDocuments(doctype);
        Document doc = documents.get(index);
        if(!doc.isAvailable()){
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            JOptionPane.showMessageDialog(
                this,
                i18nManager.getTranslation("document_not_available"),
                i18nManager.getTranslation("properties"),
                JOptionPane.WARNING_MESSAGE);
            return;
        }
        dlg = doc.getFactory().getPropertiesWindow(doc);
        PluginServices.getMDIManager().addWindow(dlg);

        refreshControls();
        lstDocs.setSelectedIndex(index);
        project.setModified(true);
    }

    /**
     * This method initializes jPanel
     *
     * @return JPanel
     */
    private JComponent getDocumentTypesPanel() {
        if (documentTypes == null) {

            documentTypesList =
                new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
            documentTypesList.setName("tipoDocPanel");
            fillDocumentTypeButtons();

            documentTypes = new JScrollPane(documentTypesList);
            Dimension dim = documentTypesList.getPreferredSize();
            documentTypes.setMinimumSize(new Dimension(dim.width,dim.height + 35));
            documentTypes.setBorder(BorderFactory.createTitledBorder(null,
                PluginServices.getText(this, "tipos_de_documentos"),
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
            documentTypes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
            documentTypes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        }

        return documentTypes;
    }

    /**
     * Adds a button for each document type
     */
    private void fillDocumentTypeButtons() {
        JRadioButton[] btns = getBtnDocumentTypes();
        for (int i = 0; i < btns.length; i++) {
            documentTypesList.add(btns[i]);
        }
    }

    /**
     * This method initializes btnVistas
     *
     * @return JRadioButton
     */
    private JRadioButton[] getBtnDocumentTypes() {
        if (btnsDocuments == null) {

            List<JRadioButton> btns = new ArrayList<JRadioButton>();
            List<DocumentManager> factories =
                ProjectManager.getInstance().getDocumentManagers();
            Collections.sort(factories, new Comparator<DocumentManager>() {

                public int compare(DocumentManager arg0, DocumentManager arg1) {
                    return arg0.getPriority() - arg1.getPriority();
                }

            });
            SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
            for (DocumentManager documentFactory : factories) {
                if( ! identityManager.getCurrentIdentity().isAuthorized(Document.ACCESS_DOCUMENT_AUTHORIZATION, documentFactory, documentFactory.getTypeName()) ) {
                    // throw new UnauthorizedException(Document.ACCESS_DOCUMENT_AUTHORIZATION, documentFactory, documentFactory.getTypeName());
                    continue;
                }                
                JRadioButton rb = new JRadioButton();

                rb.setIcon(documentFactory.getIcon());
                rb.setSelectedIcon(documentFactory.getIconSelected());
                rb.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                rb.setText(documentFactory.getTitle());
                rb.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                rb.setName(documentFactory.getTypeName());
                rb.addChangeListener(new javax.swing.event.ChangeListener() {

                    public void stateChanged(javax.swing.event.ChangeEvent e) {
                        refreshList();
                    }
                });
                btns.add(rb);
            }
            btnsDocuments = btns.toArray(new JRadioButton[0]);
        }
        return btnsDocuments;
    }

    /**
     * This method initializes jPanel1
     *
     * @return JPanel
     */
    private JPanel getDocumentsPanel() {
        if (documentsPanel == null) {
            documentsPanel = new JPanel();

            GridBagLayout layout2 = new GridBagLayout();
            GridBagConstraints c = new GridBagConstraints();

            documentsPanel.setLayout(layout2);

            c.insets = new Insets(2, 5, 2, 5);
            c.anchor = GridBagConstraints.WEST;
            c.fill = GridBagConstraints.BOTH;
            c.weightx = 1.0;
            c.weighty = 1.0;
            documentsPanel.add(getDocumentsScrollPane(), c);

            c.anchor = GridBagConstraints.EAST;
            c.fill = GridBagConstraints.NONE;
            c.weightx = 0;
            c.weighty = 0;
            documentsPanel.add(getDocumentButtonsPanel(), c);

            documentsPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(null,
                    PluginServices.getText(this, "documentos_existentes"),
                    javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, null,
                    null));
        }

        return documentsPanel;
    }

    /**
     * This method initializes lstDocs
     *
     * @return JList
     */
    private JList getDocumentsList() {
        if (lstDocs == null) {
            lstDocs = new JList();
            lstDocs.addMouseListener(new java.awt.event.MouseAdapter() {

                public AbstractDocument[] getSelecteds() {
                    if (lstDocs.getSelectedIndex() < 0) {
                        return new AbstractDocument[0];
                    }
                    Object[] seleteds = lstDocs.getSelectedValues();
                    AbstractDocument[] returnValue =
                        new AbstractDocument[seleteds.length];
                    System.arraycopy(seleteds, 0, returnValue, 0,
                        seleteds.length);
                    return returnValue;
                }

                public AbstractDocument getItem(java.awt.event.MouseEvent e) {
                    if (lstDocs.getSelectedIndex() < 0) {
                        return null;
                    }

                    return null;
                }

                @Override
                public void mouseClicked(java.awt.event.MouseEvent e) {
                    if (e.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                        /*
                         * getDocumentSelected();//ProjectView
                         * getDocumentSelectedName();//Vista
                         */
                        String docType = getDocumentSelected();
                        AbstractDocument[] selecteds = this.getSelecteds();

                        if (selecteds == null) {
                            return;
                        }
                        DocumentContextMenu menu =
                            new DocumentContextMenu(docType, this.getItem(e),
                                selecteds);
                        if (!menu.hasActions()) {
                            return;
                        }
                        lstDocs.add(menu);
                        menu.show(e.getComponent(), e.getX(), e.getY());
                        return;
                    }

                    if (e.getClickCount() == 2) {
                        abrir();
                    }

                }
            });
            lstDocs
                .addListSelectionListener(new javax.swing.event.ListSelectionListener() {

                    public void valueChanged(
                        javax.swing.event.ListSelectionEvent e) {
                        activarBotones();
                    }
                });
        }

        return lstDocs;
    }

    /**
     * This method initializes jPanel2
     *
     * @return JPanel
     */
    private JPanel getDocumentButtonsPanel() {
        if (jPanel2 == null) {
            jPanel2 = new JPanel();

            // FlowLayout layout = new FlowLayout();
            GridLayout layout = new GridLayout(5, 1);
            layout.setVgap(7);

            jPanel2.setLayout(layout);
            jPanel2.add(getBtnNuevo(), null);
            jPanel2.add(getBtnAbrir(), null);
            jPanel2.add(getBtnRenombrar(), null);
            jPanel2.add(getBtnBorrar(), null);
            jPanel2.add(getBtnPropiedades(), null);
        }

        return jPanel2;
    }

    /**
     * This method initializes btnNuevo
     *
     * @return JButton
     */
    private JButton getBtnNuevo() {
        if (btnNuevo == null) {
            btnNuevo = manager.createJButton();
            btnNuevo.setName("btnNuevo");
            btnNuevo.setText(PluginServices.getText(this, "nuevo"));
            btnNuevo.setMargin(new java.awt.Insets(2, 2, 2, 2));
            btnNuevo.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    try {
                        newProjectDocument();
                    } catch (Exception e1) {
                        NotificationManager.addError(e1.getLocalizedMessage(),
                            e1);
                    }
                }
            });
        }

        return btnNuevo;
    }

    /**
     * This method initializes btnPropiedades
     *
     * @return JButton
     */
    private JButton getBtnPropiedades() {
        if (btnPropiedades == null) {
            btnPropiedades = manager.createJButton();
            ;
            btnPropiedades.setText(PluginServices.getText(this, "propiedades"));
            btnPropiedades.setName("btnPropiedades");
            btnPropiedades.setEnabled(false);
            btnPropiedades.setMargin(new java.awt.Insets(2, 2, 2, 2));
            btnPropiedades
                .addActionListener(new java.awt.event.ActionListener() {

                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        propiedades();
                    }
                });
        }

        return btnPropiedades;
    }

    /**
     * This method initializes btnAbrir
     *
     * @return JButton
     */
    private JButton getBtnAbrir() {
        if (btnAbrir == null) {
            btnAbrir = manager.createJButton();
            btnAbrir.setName("btnAbrir");
            btnAbrir.setText(PluginServices.getText(this, "abrir"));
            btnAbrir.setEnabled(false);
            btnAbrir.setMargin(new java.awt.Insets(2, 2, 2, 2));
            btnAbrir.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    abrir();
                }
            });
        }

        return btnAbrir;
    }

    /**
     * This method initializes btnBorrar
     *
     * @return JButton
     */
    private JButton getBtnBorrar() {
        if (btnBorrar == null) {
            btnBorrar = manager.createJButton();
            btnBorrar.setText(PluginServices.getText(this, "borrar"));
            btnBorrar.setName("btnBorrar");
            btnBorrar.setEnabled(false);
            btnBorrar.setMargin(new java.awt.Insets(2, 2, 2, 2));
            btnBorrar.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    borrar();
                }
            });
        }

        return btnBorrar;
    }

    /**
     * This method initializes btnRenombrar
     *
     * @return JButton
     */
    private JButton getBtnRenombrar() {
        if (btnRenombrar == null) {
            btnRenombrar = manager.createJButton();
            btnRenombrar.setName("btnRenombrar");
            btnRenombrar.setText(PluginServices.getText(this, "renombrar"));
            btnRenombrar.setEnabled(false);
            btnRenombrar.setMargin(new java.awt.Insets(2, 2, 2, 2));
            btnRenombrar.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    renombrar();
                }
            });
        }

        return btnRenombrar;
    }

    /**
     * This method initializes jPanel3
     *
     * @return JPanel
     */
    private JPanel getPropertiesPanel() {
        if (propertiesPanel == null) {
            propertiesPanel = new JPanel(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.insets = new Insets(2, 5, 0, 5);
            c.anchor = GridBagConstraints.WEST;
            c.gridx = 0;
            c.gridy = 0;
            propertiesPanel.add(getJLabel(), c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0;
            c.gridx = 1;
            propertiesPanel.add(getLblNombreSesion(), c);

            c.gridx = 0;
            c.gridy = 1;
            c.fill = GridBagConstraints.NONE;
            c.weightx = 0.0;
            propertiesPanel.add(getJLabel1(), c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0;
            c.gridx = 1;
            propertiesPanel.add(getLblGuardado(), c);

            c.gridx = 0;
            c.gridy = 2;
            c.fill = GridBagConstraints.NONE;
            c.weightx = 0.0;
            propertiesPanel.add(getJLabel2(), c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 1.0;
            c.gridx = 1;
            propertiesPanel.add(getLblFecha(), c);

            c.gridx = 0;
            c.gridwidth = 2;
            c.gridy = 4;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.anchor = GridBagConstraints.EAST;
            propertiesPanel.add(getProjectsButtonPanel(), c);

            propertiesPanel.setBorder(javax.swing.BorderFactory
                .createTitledBorder(null,
                    PluginServices.getText(this, "propiedades_sesion"),
                    javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, null,
                    null));
        }

        return propertiesPanel;
    }

    /**
     * This method initializes jLabel
     *
     * @return JLabel
     */
    private JLabel getJLabel() {
        if (jLabel == null) {
            jLabel = new JLabel();
            jLabel.setText(PluginServices.getText(this, "nombre_sesion") + ":");
        }

        return jLabel;
    }

    /**
     * This method initializes lblNombreSesion
     *
     * @return JLabel
     */
    private JLabel getLblNombreSesion() {
        if (lblNombreSesion == null) {
            lblNombreSesion = new JLabel();
            lblNombreSesion.setText("");
            lblNombreSesion.setName("lblNombreSesion");
            lblNombreSesion.setAutoscrolls(true);
        }

        return lblNombreSesion;
    }

    /**
     * This method initializes jLabel1
     *
     * @return JLabel
     */
    private JLabel getJLabel1() {
        if (jLabel1 == null) {
            jLabel1 = new JLabel();
            jLabel1.setText(PluginServices.getText(this, "guardado") + ":");
        }

        return jLabel1;
    }

    /**
     * This method initializes lblGuardado
     *
     * @return JLabel
     */
    private JLabel getLblGuardado() {
        if (lblGuardado == null) {
            lblGuardado = new JLabel();
            lblGuardado.setText("");
            lblGuardado.setAutoscrolls(true);
        }

        return lblGuardado;
    }

    /**
     * This method initializes jLabel2
     *
     * @return JLabel
     */
    private JLabel getJLabel2() {
        if (jLabel2 == null) {
            jLabel2 = new JLabel();
            jLabel2
                .setText(PluginServices.getText(this, "creation_date") + ":");
        }

        return jLabel2;
    }

    /**
     * This method initializes lblFecha
     *
     * @return JLabel
     */
    private JLabel getLblFecha() {
        if (lblFecha == null) {
            lblFecha = new JLabel();
            lblFecha.setText("");
            lblFecha.setAutoscrolls(true);
        }

        return lblFecha;
    }

    /**
     * This method initializes btnImportar
     *
     * @return JButton
     */
    private JButton getBtnImportar() {
        if (btnImportar == null) {
            btnImportar = manager.createJButton();
            btnImportar.setPreferredSize(new java.awt.Dimension(80, 23));
            btnImportar.setText(PluginServices.getText(this, "importar"));
            btnImportar.setName("btnImportar");
            btnImportar.setMargin(new java.awt.Insets(2, 2, 2, 2));
        }

        return btnImportar;
    }

    /**
     * This method initializes btnExportar
     *
     * @return JButton
     */
    private JButton getBtnExportar() {
        if (btnExportar == null) {
            btnExportar = manager.createJButton();
            btnExportar.setPreferredSize(new java.awt.Dimension(80, 23));
            btnExportar.setText(PluginServices.getText(this, "exportar"));
            btnExportar.setName("btnExportar");
            btnExportar.setMargin(new java.awt.Insets(2, 2, 2, 2));
        }

        return btnExportar;
    }

    /**
     * This method initializes btnEditar
     *
     * @return JButton
     */
    private JButton getBtnEditar() {
        if (btnEditar == null) {
            btnEditar = manager.createJButton();
            btnEditar.setPreferredSize(new java.awt.Dimension(80, 23));
            btnEditar.setText(PluginServices.getText(this, "propiedades"));

            btnEditar.setName("btnEditar");
            btnEditar.setMargin(new java.awt.Insets(2, 2, 2, 2));

            btnEditar.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    I18nManager i18nManager = ToolsLocator.getI18nManager();
                    WindowManager winmgr = ToolsSwingLocator.getWindowManager();
                    winmgr.showWindow(
                            new ProjectProperties(project),
                            i18nManager.getTranslation("project_properties"),
                            WindowManager.MODE.WINDOW
                    );
                    refreshProperties();
                }
            });
        }

        return btnEditar;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return JScrollPane
     */
    private JScrollPane getDocumentsScrollPane() {
        if (documentsScrollPane == null) {
            documentsScrollPane = new JScrollPane();
            documentsScrollPane.setViewportView(getDocumentsList());
            documentsScrollPane.setPreferredSize(new java.awt.Dimension(200,100));
            documentsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        }

        return documentsScrollPane;
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.SingletonWindow#getWindowModel()
     */
    public Object getWindowModel() {
        return project;
    }

    public Project getProject() {
        return project;
    }

    /**
     * This method is used to get <strong>an initial</strong> ViewInfo object
     * for this Project Manager window. It is not intended to retrieve the
     * ViewInfo object in a later time. <strong>Use
     * PluginServices.getMDIManager().getViewInfo(view) to retrieve the ViewInfo
     * object at any time after the creation of the object.
     *
     * @see com.iver.mdiApp.ui.MDIManager.IWindow#getWindowInfo()
     */
    public WindowInfo getWindowInfo() {
        if (m_viewInfo == null) {
            m_viewInfo =
                new WindowInfo(WindowInfo.MAXIMIZABLE | WindowInfo.RESIZABLE);
            m_viewInfo.setWidth(this.getWidth());
            m_viewInfo.setHeight(this.getHeight());
            int w = this.getPreferredSize().width;
            if( w < 450 ) {
                w = 450;
            }
            m_viewInfo.setNormalWidth(w);
            m_viewInfo.setNormalHeight(this.getPreferredSize().height);
            m_viewInfo.setMinimumSize(getPreferredSize());

            m_viewInfo.setTitle(PluginServices.getText(this, "_Project_manager"));
        }
        return m_viewInfo;
    }

    public void propertyChange(final PropertyChangeEvent evt) {
		if( !SwingUtilities.isEventDispatchThread() ) {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					propertyChange(evt);
				}
			});
			return;
		}
        refreshControls();
    }

    /**
     * This method initializes jPanel4
     *
     * @return JPanel
     */
    private JPanel getProjectsButtonPanel() {
        if (projectButtonsPanel == null) {
            projectButtonsPanel = new JPanel();

            projectButtonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
            projectButtonsPanel
                .setComponentOrientation(java.awt.ComponentOrientation.LEFT_TO_RIGHT);
            // TODO: implement export and import functions
            // projectButtonsPanel.add(getBtnImportar(), null);
            // projectButtonsPanel.add(getBtnExportar(), null);
            projectButtonsPanel.add(getBtnEditar(), null);
        }

        return projectButtonsPanel;
    }

    public Object getWindowProfile() {
        return WindowInfo.PROJECT_PROFILE;
    }

    public void update(Observable observable, Object notification) {
        if (observable instanceof ProjectManager) {
            refreshDocuments();
        }
    }

    private void refreshDocuments() {
        // Setting it to null will make it recreate in the next
        // getBtnDocuments() call
        this.btnsDocuments = null;

        // Clear previous document type buttons
        documentTypesList.removeAll();
        for (Enumeration<AbstractButton> buttons = grupo.getElements(); buttons
            .hasMoreElements();) {
            grupo.remove(buttons.nextElement());
        }

        JRadioButton[] btnDocuments = getBtnDocumentTypes();
        for (int i = 0; i < btnDocuments.length; i++) {
            grupo.add(btnDocuments[i]);
        }
        if (btnDocuments.length > 0) {
            btnDocuments[0].setSelected(true);
        }
        fillDocumentTypeButtons();
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);

        I18nManager i18nManager = ToolsLocator.getI18nManager();

        btnNuevo.setText(i18nManager.getTranslation( "nuevo"));
        btnPropiedades.setText(i18nManager.getTranslation("propiedades"));
        btnAbrir.setText(i18nManager.getTranslation("abrir"));
        btnBorrar.setText(i18nManager.getTranslation("borrar"));
        btnRenombrar.setText(i18nManager.getTranslation("renombrar"));

        jLabel.setText(i18nManager.getTranslation("nombre_sesion") + ":");
        jLabel1.setText(i18nManager.getTranslation("guardado") + ":");
        jLabel2.setText(i18nManager.getTranslation("creation_date") + ":");

        // btnImportar.setText(i18nManager.getTranslation("importar"));
        // btnExportar.setText(i18nManager.getTranslation("exportar"));
        btnEditar.setText(i18nManager.getTranslation("propiedades"));

        m_viewInfo.setTitle(i18nManager.getTranslation("_Project_manager"));

        TitledBorder border = null;

        border = (TitledBorder) propertiesPanel.getBorder();
        border.setTitle(i18nManager.getTranslation("propiedades_sesion"));

        border = (TitledBorder) documentTypes.getBorder();
        border.setTitle(i18nManager.getTranslation("tipos_de_documentos"));

        border = (TitledBorder) documentsPanel.getBorder();
        border.setTitle(i18nManager.getTranslation("documentos_existentes"));

        refreshList();
    }


}
