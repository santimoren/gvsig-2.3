/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners;

import java.awt.geom.Point2D;
import java.text.NumberFormat;
import java.util.Locale;

import org.cresques.cts.IProjection;
import org.geotools.measure.AngleFormat;
import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.MeasureListenerImpl;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;



/**
 * <p>Listener for calculating distances using vertexes of a polyline, defined in the associated {@link MapControl MapControl}
 *  object, and displaying that information at the status bar of the application's main frame.</p>
 *
 * <p>Calculates and displays:
 *  <ul>
 *   <li>Distance of the last segment of the polyline.</li>
 *   <li>Accumulated distance of all segments of the polyline.</li>
 *  </ul>
 * </p>
 *
 * @see MeasureListenerImpl
 *
 * @author Vicente Caballero Navarro
 */
public class MeasureListener extends MeasureListenerImpl {
	/**
	 * <p>Creates a new listener for calculating distances using points of a polyline.</p>
	 *
	 * @param mc the <code>MapControl</code> where is calculated the length
	 */
	public MeasureListener(MapControl mc) {
		super(mc);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.MeasureListenerImpl#points(com.iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
	 */
	public void points(MeasureEvent event) {
		double dist = 0;
		double distAll = 0;
		double angle = 0;
		double sin = 0;
		double cos = 0;
		double dm = 0;

		ViewPort vp = mapCtrl.getMapContext().getViewPort();

		IProjection proj = mapCtrl.getMapContext().getProjection();
		
		Point2D p = new Point2D.Double(event.getXs()[0].doubleValue(),event.getYs()[0].doubleValue());//vp.toMapPoint(new Point(event.getXs()[0].intValue(),event.getYs()[0].intValue()));
		for (int i = 1; i < (event.getXs().length); i++) {
			Point2D p2 = new Point2D.Double(event.getXs()[i].doubleValue(),	event.getYs()[i].doubleValue());// vp.toMapPoint(new Point(event.getXs()[i].intValue(),event.getYs()[i].intValue()));
			dist = vp.distanceWorld(p, p2);
			distAll += dist;
			
			if (proj.isProjected()) {
				dm = p.distance(p2);
				sin = (p2.getX() - p.getX()) / dm;
				cos = (p2.getY() - p.getY()) / dm;
				if (sin >= 0) {
					angle = Math.toDegrees(Math.acos(cos));
				} else {
					angle = 360 - Math.toDegrees(Math.acos(cos));
				}
			}
			p = p2;
		}

		NumberFormat nf = NumberFormat.getInstance();
		AngleFormat af = AngleFormat.getInstance(Locale.getDefault());
        nf.setMaximumFractionDigits(2);
        if (PluginServices.getMainFrame() != null)
        {
        	double[] trans2Meter=MapContext.getDistanceTrans2Meter();
            int distanceUnits = mapCtrl.getViewPort().getDistanceUnits();
			PluginServices.getMainFrame().getStatusBar().setMessage("4",
    			"Dist=" + nf.format(dist/trans2Meter[distanceUnits]) + " " + MapContext.getDistanceAbbr()[distanceUnits]);
    		PluginServices.getMainFrame().getStatusBar().setMessage("5",
    			"T=" + nf.format(distAll/trans2Meter[distanceUnits]) + " " + MapContext.getDistanceAbbr()[distanceUnits]);

    		//
    		// El calculo del azimut esta pensado para trabajar en coordenadas proyectadas (metros)
    		// y probablemente cuando se esten usando coordenadas en lat/lang se generen resultados
    		// erroneos, asi que solo se calcula y muestra el azimut si la proyeccion es proyectada.
    		//
    		if( proj.isProjected() ) {
	    		PluginServices.getMainFrame().getStatusBar().setMessage("azimut",
	        			"Azimut:" + af.format(angle) + "");
    		} else {
	    		PluginServices.getMainFrame().getStatusBar().setMessage("azimut",
	        			"Azimut: (not available)");
    		}
        }
	}
}
