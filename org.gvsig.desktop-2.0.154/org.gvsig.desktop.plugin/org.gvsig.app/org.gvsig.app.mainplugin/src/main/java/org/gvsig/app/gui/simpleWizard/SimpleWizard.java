/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.simpleWizard;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;

import jwizardcomponent.CancelAction;
import jwizardcomponent.DefaultJWizardComponents;
import jwizardcomponent.FinishAction;


public class SimpleWizard extends JPanel implements IWindow {
	WindowInfo viewInfo = null;
	WizardPanelWithLogo wizardPanel;

	// No deber�an necesitarse un FinishAction y un CancelAction, pero bueno,
	// lo mantengo por ahora.
	private class CloseAction extends FinishAction
	{
		IWindow v;
		public CloseAction(IWindow view)
		{
			super(wizardPanel.getWizardComponents());
			v = view;
		}
		public void performAction() {
			PluginServices.getMDIManager().closeWindow(v);
		}

	}
	private class CloseAction2 extends CancelAction
	{

		IWindow v;
		public CloseAction2(IWindow view)
		{
			super(wizardPanel.getWizardComponents());
			v = view;
		}
		public void performAction() {
			PluginServices.getMDIManager().closeWindow(v);
		}

	}


	public SimpleWizard(ImageIcon logo)
	{
		wizardPanel = new WizardPanelWithLogo(logo);
		CloseAction closeAction = new CloseAction(this);
		CloseAction2 closeAction2 = new CloseAction2(this);
		wizardPanel.getWizardComponents().setFinishAction(closeAction);
		wizardPanel.getWizardComponents().setCancelAction(closeAction2);

		this.setLayout(new BorderLayout());
		this.add(wizardPanel, BorderLayout.CENTER);
	}

	public DefaultJWizardComponents getWizardComponents()
	{
		return wizardPanel.getWizardComponents();
	}


	public WindowInfo getWindowInfo() {
		if (viewInfo == null)
		{
			viewInfo = new WindowInfo(WindowInfo.MODALDIALOG|WindowInfo.RESIZABLE);
		}
		return viewInfo;
	}
	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}
}
