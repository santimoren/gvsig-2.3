/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.wizards;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class WizardListenerSupport {
    private ArrayList listeners = new ArrayList();

    /**
     * DOCUMENT ME!
     *
     * @param listener DOCUMENT ME!
     */
    public void addWizardListener(WizardListener listener) {
        listeners.add(listener);
    }

    /**
     * DOCUMENT ME!
     *
     * @param listener DOCUMENT ME!
     */
    public void removeWizardListener(WizardListener listener) {
        listeners.remove(listener);
    }

	/**
	 * DOCUMENT ME!
	 * @param finishable TODO
	 */
	public void callStateChanged(boolean finishable) {
		Iterator i = listeners.iterator();

		while (i.hasNext()) {
			((WizardListener) i.next()).wizardStateChanged(finishable);
		}
	}

	/**
	 * DOCUMENT ME!
	 */
	public void callError(Exception descripcion) {
		Iterator i = listeners.iterator();

		while (i.hasNext()) {
			((WizardListener) i.next()).error(descripcion);
		}
	}
}
