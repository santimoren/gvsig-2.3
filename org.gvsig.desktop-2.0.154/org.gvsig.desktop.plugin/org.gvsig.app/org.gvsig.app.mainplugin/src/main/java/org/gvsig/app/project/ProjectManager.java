/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.documents.DefaultDocumentActionGroup;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.DocumentAction;
import org.gvsig.app.project.documents.DocumentActionGroup;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.app.project.documents.gui.ProjectWindow;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;
import org.gvsig.propertypage.PropertiesPageManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.observer.impl.BaseWeakReferencingObservable;

public class ProjectManager extends BaseWeakReferencingObservable {

    private static final Logger logger = LoggerFactory
        .getLogger(ProjectManager.class);
    private static ProjectManager factory = null;

    // private static final String KEY_PROJECT = "app.project";
    // private static final String KEY_DOCUMENTS = "app.project.documents";
    private static final String KEY_DOCUMENTS_FACTORIES =
        "app.project.documents.factories";
    private static final String KEY_DOCUMENTS_ACTIONS =
        "app.project.documents.actions";

    private Map<String, DocumentActionGroup> documentActionGroups;

    public static ProjectManager getInstance() {
        if (factory == null) {
            factory = new ProjectManager();
        }
        return factory;
    }

    private ProjectManager() {
        this.documentActionGroups = new HashMap<String, DocumentActionGroup>();
    }

    
    public Project getCurrentProject() {
        return ((ProjectExtension) PluginServices
            .getExtension(ProjectExtension.class)).getProject();
    }

    public ProjectWindow getCurrentProjectWindow() {
        ProjectExtension projectExtension =
            (ProjectExtension) PluginServices
                .getExtension(ProjectExtension.class);
        ProjectWindow window =
            (ProjectWindow) projectExtension.getProjectWindow();
        return window;
    }

    /**
     * @deprecated use {@link #getDocumentManagers()} instead.
     */
    public List<DocumentManager> getDocumentManager() {
        return getDocumentManagers();
    }

    @SuppressWarnings("unchecked")
    public List<DocumentManager> getDocumentManagers() {
        Iterator<Extension> iterator =
            ToolsLocator.getExtensionPointManager()
                .get(KEY_DOCUMENTS_FACTORIES).iterator();
        List<DocumentManager> factories = new ArrayList<DocumentManager>();
        while (iterator.hasNext()) {
            Extension extension = iterator.next();
            try {
                factories.add((DocumentManager) extension.create());
            } catch (InstantiationException e) {
                logger.error("Can't access to project document factory ("
                    + extension.getName() + ").");
            } catch (IllegalAccessException e) {
                logger.error("Can't access to project document factory ("
                    + extension.getName() + ").");
            }
        }
        return factories;
    }

    /**
     * @deprecated use {@link #getDocumentManager(String)} instead.
     */
    public DocumentManager getDocumentManagers(String type) {
        return getDocumentManager(type);
    }

    public DocumentManager getDocumentManager(String type) {
        DocumentManager factory = null;
        try {
            factory =
                (DocumentManager) ToolsLocator.getExtensionPointManager()
                    .get(KEY_DOCUMENTS_FACTORIES).create(type);
        } catch (Exception ex) {
            logger
                .warn(
                    MessageFormat.format(
                        "Unable to locate factory for documents of type {0}",
                        type), ex);
        }
        return factory;
    }

    public Document createDocument(String type) {
        Document doc = getDocumentManager(type).createDocument();
        return doc;
    }

    public Document createDocument(String type, String name) {
        Document doc = createDocument(type);
        doc.setName(name);
        return doc;
    }

    /**
     * @deprecated use {@link #createDocumentsByUser(String)} instead
     */
    public Document createDocumentByUser(String type) {
        Document doc = getDocumentManager(type).createDocumentByUser();
        return doc;
    }

    /**
     * Creates a group of documents of a given type through the user interface.
     * 
     * @param type
     *            the type of documents to create
     * @return the created documents
     */
    public Iterator<? extends Document> createDocumentsByUser(String type) {
        logger.info("createDocumentsByUser('{}')", type);
        return getDocumentManager(type).createDocumentsByUser();
    }

    public Project createProject() {
        return new DefaultProject();
    }

    public ProjectExtent createExtent() {
        return new ProjectExtent();
    }

    public void registerDocumentFactory(DocumentManager documentManager) {
        ExtensionPointManager manager = ToolsLocator.getExtensionPointManager();
        manager.add(KEY_DOCUMENTS_FACTORIES).append(
            documentManager.getTypeName(), null, documentManager);
        notifyObservers();
    }

    public void registerDocumentFactoryAlias(String typeName, String alias) {
        ExtensionPointManager manager = ToolsLocator.getExtensionPointManager();

        manager.get(KEY_DOCUMENTS_FACTORIES).addAlias(typeName, alias);
    }

    public void registerDocumentAction(String typeName, DocumentAction action) {
        ExtensionPointManager manager = ToolsLocator.getExtensionPointManager();

        String key = KEY_DOCUMENTS_ACTIONS + "." + typeName;
        String description =
            MessageFormat.format("Actions for {1} documents ", typeName);

        manager.add(key, description).append(action.getId(),
            action.getDescription(), action);
    }

    /**
     * Gets a list of actions for the document type especified.
     * 
     * La lista esta ordenada deacuerdo al orden especificado en
     * las acciones y grupos de acciones involucrados.
     * 
     * @param doctype
     * @return list of actions as List<DocumentAction>
     */
    @SuppressWarnings("unchecked")
    public List<DocumentAction> getDocumentActions(String doctype) {

        ExtensionPointManager manager = ToolsLocator.getExtensionPointManager();

        String key = KEY_DOCUMENTS_ACTIONS + "." + doctype;
        ExtensionPoint extensionPoint = manager.get(key);
        if (extensionPoint == null) {
            // No hay acciones registradas para ese tipo de documento
            return null;
        }
        List<DocumentAction> actions = new ArrayList<DocumentAction>();
        Iterator<Extension> it = extensionPoint.iterator();
        while (it.hasNext()) {
            try {
                DocumentAction action;
                action = (DocumentAction) it.next().create();
                actions.add(action);
            } catch (InstantiationException e) {
                logger.warn("Can't retrieve document action", e);
            } catch (IllegalAccessException e) {
                logger.warn("Can't retrieve document action", e);
            }
        }
        if (actions.size() < 1) {
            return null;
        }
        Collections.sort(actions, new CompareAction());
        return actions;
    }

    private class CompareAction implements Comparator<DocumentAction> {

        public int compare(DocumentAction action1, DocumentAction action2) {
            // FIXME: flata formatear los enteros!!!!
            String key1 =
                MessageFormat.format("{1}.{2}.{3}", action1.getGroup()
                    .getOrder(), action1.getGroup().getTitle(), action1
                    .getOrder());
            String key2 =
                MessageFormat.format("{1}.{2}.{3}", action2.getGroup()
                    .getOrder(), action2.getGroup().getTitle(), action2
                    .getOrder());
            return key1.compareTo(key2);
        }
    }

    /**
     * Create, add and return a new action for documents.
     * 
     * If action already exists don't create and return this.
     * 
     * @param unique
     *            identifier for the action
     * @param title
     * @param description
     * @param order
     * @return
     */
    public DocumentActionGroup addDocumentActionGroup(String id, String title,
        String description, int order) {
        DocumentActionGroup group = this.documentActionGroups.get(id);
        if (group != null) {
            return group;
        }
        group = new DefaultDocumentActionGroup(id, title, description, order);
        this.documentActionGroups.put(id, group);
        return group;
    }

    public ProjectPreferences getProjectPreferences() {
        return DefaultProject.getPreferences();
    }
    
}
