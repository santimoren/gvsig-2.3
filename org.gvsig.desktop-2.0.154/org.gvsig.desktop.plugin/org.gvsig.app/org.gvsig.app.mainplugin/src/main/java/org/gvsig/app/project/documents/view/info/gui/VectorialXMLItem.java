/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.app.project.documents.view.info.gui;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.dispose.DisposeUtils;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class VectorialXMLItem implements XMLItem {

	private FeatureSet selection;
	private FLayer layer;

	public VectorialXMLItem(FeatureSet selection, FLayer layer) {
		this.selection = selection;
		this.layer = layer;
	}

	public FLayer getLayer(){
		return layer;
	}
	/**
	 * @see org.gvsig.app.project.documents.view.info.gui.iver.cit.gvsig.gui.toolListeners.InfoListener.XMLItem#parse(org.xml.sax.ContentHandler)
	 */
	public void parse(ContentHandler handler) throws SAXException {
		AttributesImpl aii = new AttributesImpl();
		handler.startElement("", "", (layer).getName(), aii);
		DisposableIterator iterator = null;
		try {

//			FeatureStore ds = ((FLyrVect) layer).getFeatureStore();
			iterator = selection.fastIterator();
			int j=0;
			FeatureAttributeDescriptor attr;
			Object value;
			String strValue;
			while (iterator.hasNext()) {
				Feature feature = (Feature) iterator.next();
				AttributesImpl ai = new AttributesImpl();
				FeatureType featureType=((FLyrVect) layer).getFeatureStore().getDefaultFeatureType();
				for (int k = 0; k < featureType.size(); k++) {
					attr = (FeatureAttributeDescriptor) featureType
							.get(k);
					value = feature.get(k);
					if (value == null) {
						strValue = "{null}";
					} else {
						strValue = value.toString();
					}
					ai.addAttribute("", attr.getName(), attr.getName(),
					"xs:string",
							strValue);
				}
				handler.startElement("", "", String.valueOf(j), ai);
				handler.endElement("", "", String.valueOf(j));
				j++;
			}

			//TODO
//			ds.start();
//
//			for (int j = bitset.nextSetBit(0); j >= 0; j = bitset
//					.nextSetBit(j + 1)) {
//				AttributesImpl ai = new AttributesImpl();
//
//				for (int k = 0; k < ds.getFieldCount(); k++) {
//					ai.addAttribute("", ds.getFieldName(k), "",
//							"xs:string", ds.getFieldValue(j, k).toString());
//				}
//				handler.startElement("", "", String.valueOf(j), ai);
//				handler.endElement("", "", String.valueOf(j));
//			}
//
//			ds.stop();

		} catch (ReadException e) {
			throw new SAXException(e);
		} catch (DataException e) {
			throw new SAXException(e);
		}
		finally {
			DisposeUtils.dispose(iterator);
		}
		handler.endElement("", "", (layer).getName());
	}
}


