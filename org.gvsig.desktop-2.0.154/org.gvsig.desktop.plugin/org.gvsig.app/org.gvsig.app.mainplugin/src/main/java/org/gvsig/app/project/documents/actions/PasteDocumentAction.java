/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.actions;

import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocumentAction;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.gui.ProjectWindow;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.persistence.exception.PersistenceException;


public class PasteDocumentAction extends AbstractDocumentAction {

    private static Logger logger = LoggerFactory.getLogger(
        PasteDocumentAction.class);

	public PasteDocumentAction() {
		super("paste");
		this.order = 0;
		this.title =PluginServices.getText(this, "pegar");
		this.group = ProjectManager.getInstance().addDocumentActionGroup("ClipboardActions", "Clipboard actions", null, 0);
	}
	
	public void execute(Document document, List<Document> documents) {
	    
	    List<Document> docs = null;
	    
	    try {
            docs = CopyPasteDocsUtils.getClipboardDocuments(null);
        } catch (Exception e) {
            
            logger.info("While getting docs from clipboard.", e);
            JOptionPane.showMessageDialog(
                ApplicationLocator.getManager().getRootComponent(),
                Messages.getText("_Clipboard_error") + ":\n"
                + CopyPasteDocsUtils.getLastMessage(e),
                title,
                JOptionPane.ERROR_MESSAGE);
            return;
        }
	    
	    if (docs.size() == 0) {
            JOptionPane.showMessageDialog(
                ApplicationLocator.getManager().getRootComponent(),
                Messages.getText("_There_are_no_documents_to_paste"),
                title,
                JOptionPane.INFORMATION_MESSAGE);
            return;
	    }
	    
	    Project project = ProjectManager.getInstance().getCurrentProject();
	    
	    for (int i=0; i<docs.size(); i++) {
	        project.add(docs.get(i));
	    }

	}
	
	public boolean isAvailable(Document document, List<Document> documents) {
	    return CopyPasteDocsUtils.getClipboardFiles(null).size() > 0;
	}

	public boolean isVisible(Document document, List<Document> documents) {
		return true;
	}

	
}
