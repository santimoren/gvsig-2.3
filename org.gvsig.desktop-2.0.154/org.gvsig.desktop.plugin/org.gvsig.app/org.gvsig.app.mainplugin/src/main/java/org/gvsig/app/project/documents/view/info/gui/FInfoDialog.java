/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view.info.gui;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.fmap.mapcontrol.swing.dynobject.LayersDynObjectSetComponent;



/**
 * Dialog that contains the generic Feature Info Viewer
 *
 * @author laura
 *
 */
public class FInfoDialog extends JPanel implements SingletonWindow,
    IWindowListener, ComponentListener {

    private static final long serialVersionUID = -6707094091114785970L;
    
    private LayersDynObjectSetComponent infoViewer;

    public FInfoDialog(LayersDynObjectSetComponent info) {
        super(new BorderLayout());
        this.infoViewer = info;
        add(infoViewer.asJComponent(), BorderLayout.CENTER);
		setSize(450, 400);
		infoViewer.asJComponent().addComponentListener(this);
	}
    
    public void setInfo(LayersDynObjectSetComponent info) {
        this.setEnabled(false);
		info.asJComponent().addComponentListener(this);
        this.removeAll();
        if (infoViewer != null) {
            infoViewer.dispose();
        }
        this.infoViewer = info;
        add(infoViewer.asJComponent(), BorderLayout.CENTER);
        this.setEnabled(true);
}

	public WindowInfo getWindowInfo() {

		WindowInfo m_viewinfo = new WindowInfo(WindowInfo.MODELESSDIALOG |
				WindowInfo.RESIZABLE | WindowInfo.PALETTE);
		m_viewinfo.setWidth(getWidth()+8);
		m_viewinfo.setHeight(getHeight());
		m_viewinfo.setTitle(PluginServices.getText(this,
				"Identificar_Resultados"));

		return m_viewinfo;
	}

	public Object getWindowModel() {
		return "FInfoDialog";
	}

	public Object getWindowProfile() {
		return WindowInfo.PROPERTIES_PROFILE;
	}

    public void windowActivated() {
        // Nothing to do
    }

    public void windowClosed() {
        infoViewer.dispose();
    }

	public void componentHidden(ComponentEvent arg0) {
		ApplicationManager application = ApplicationLocator.getManager();
		application.getUIManager().closeWindow(this);
	}

	public void componentMoved(ComponentEvent arg0) {
		// Do nothing
	}

	public void componentResized(ComponentEvent arg0) {
		// Do nothing
	}

	public void componentShown(ComponentEvent arg0) {
		// Do nothing
	}
}
