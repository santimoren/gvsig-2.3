/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project;

import java.io.File;


public interface ProjectNotification extends DocumentsContainerNotification {
	
	public static final int BEFORE_SAVE_TO_FILE = 9;
	public static final int AFTER_SAVE_TO_FILE = 10;
	
	public static final int BEFORE_SAVE_TO_STREAM = 11;
	public static final int AFTER_SAVE_TO_STREAM = 12;

	public static final int BEFORE_LOAD_FROM_FILE = 13;
	public static final int AFTER_LOAD_FROM_FILE = 14;
	
	public static final int BEFORE_LOAD_FROM_STREAM = 15;
	public static final int AFTER_LOAD_FROM_STREAM = 16;
	
	public File getFile();
	
}
