/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.actions;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocumentAction;
import org.gvsig.app.project.documents.Document;
import org.gvsig.i18n.Messages;


public class CutDocumentAction extends AbstractDocumentAction {
	
    private static Logger logger = LoggerFactory.getLogger(
        CutDocumentAction.class);
    
	public CutDocumentAction() {
		super("cut");
		this.order = 0;
		this.title =PluginServices.getText(this, "cortar");
		this.group = ProjectManager.getInstance().addDocumentActionGroup("ClipboardActions", "Clipboard actions", null, 0);
	}
	

	public int getOrder() {
		return 1;
	}

	public boolean isVisible(Document item, List<Document> selectedItems) {
		return true;
	}

	public boolean isAvailable(Document item, List<Document> selectedItems) {
		return selectedItems.size() > 0;
	}

	public void execute(Document document, List<Document> documents) {

        List<Document> docs = new ArrayList<Document>(documents);
        if(document != null &&   !docs.contains(document)) {
            docs.add(document);
        }
        // Now "docs" has all docs involved
        try {
            CopyPasteDocsUtils.saveToClipboard(docs);
        } catch (Exception e) {
            
            logger.info("While copying docs to clipboard in cut operation.", e);
            JOptionPane.showMessageDialog(
                ApplicationLocator.getManager().getRootComponent(),
                Messages.getText("_Clipboard_error") + ":\n"
                + CopyPasteDocsUtils.getLastMessage(e),
                title,
                JOptionPane.ERROR_MESSAGE);
            /*
             * Clear clipboard after error
             */
            try {
                CopyPasteDocsUtils.clearClipboard(null);
            } catch (Exception exc) {
                logger.error("While clearing clipboard.", exc);
            }
        }
        
	    // ==================================================
	    
		Project project = ProjectManager.getInstance().getCurrentProject();
		for (Document doc : docs) {
			if (doc.isLocked()) {
				JOptionPane.showMessageDialog(
						(Component)PluginServices.getMainFrame(),
						PluginServices.getText(this, "locked_element_it_cannot_be_deleted") + ": " +doc.getName()
				);
				return;
			}
		}

    	int option=JOptionPane.showConfirmDialog(
    			(Component)PluginServices.getMainFrame(),
    			PluginServices.getText(this,"desea_borrar_el_documento")
    	);
    	if (option!=JOptionPane.OK_OPTION) {
    		return;
    	}

		this.removeDocuments(docs,project);

	}

	private boolean removeDocuments(List<Document> selectedItems,Project project) {
		for (Document document : selectedItems) {
			if (document.isLocked()) {
				JOptionPane.showMessageDialog(
						(Component)PluginServices.getMainFrame(),
						PluginServices.getText(this, "locked_element_it_cannot_be_deleted") + ": " +document.getName()
				);
			} else {
				PluginServices.getMDIManager().closeSingletonWindow(document);
				project.remove(document);
			}
		}
		return true;
	}


}
