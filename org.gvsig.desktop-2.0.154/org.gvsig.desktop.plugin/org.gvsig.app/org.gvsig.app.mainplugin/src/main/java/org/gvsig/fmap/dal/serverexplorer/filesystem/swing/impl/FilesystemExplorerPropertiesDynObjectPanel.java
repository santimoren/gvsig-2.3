
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing.impl;

import java.awt.Dimension;
import javax.swing.JComponent;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.spi.AbstractFilesystemExplorerPropertiesPanel;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.ServiceException;


public class FilesystemExplorerPropertiesDynObjectPanel extends AbstractFilesystemExplorerPropertiesPanel {
    private final DynObject paramters;
    private JDynForm form;

    public FilesystemExplorerPropertiesDynObjectPanel(DynObject paramters) {
     	this.paramters = paramters;
        try {
            this.form = DynFormLocator.getDynFormManager().createJDynForm(this.paramters);
        } catch (ServiceException ex) {
            throw new RuntimeException("Can't create JDynForm from '"+this.paramters.getDynClass().getFullName()+"'.", ex);
        }
    	this.form.setLayoutMode(this.form.USE_TABS);

    }

    @Override
    public void putParameters(DynObject parameters) {
        this.form.setValues(paramters);
    }

    @Override
    public void fetchParameters(DynObject parameters) {
        this.form.getValues(paramters);
    }

    @Override
    public JComponent asJComponent() {
        JComponent x = this.form.asJComponent();
        x.setPreferredSize(new Dimension(500,250));
        return x;
    }
    
}
