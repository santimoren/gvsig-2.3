/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents;

import java.util.List;

import org.gvsig.tools.extensionpoint.ExtensionSingleton;


/**
 * Interface que deben den cumplir las  acciones asociadas
 * a un documento
 * <br>
 *  Las acciones de un documento se registraran  a traves
 *  del metodo registerDocumentAction del ProjectFactory
 *  <br><br>
 * Por lo general extender de la clase AbstractDocumentAction
 * 
 *  
 * 
 */

public interface DocumentAction extends ExtensionSingleton {

	/**
	 * Gets the unique identifier for this action.
	 * 
	 * @return id as String
	 */
	public String getId();

	public boolean isVisible(Document document, List<Document> documents);
	
	public boolean isAvailable(Document document, List<Document> documents);
	
	/**
	 * 
	 * @param document Optionally a document with a special role or 
	 * null if there is not a document with a special role
	 *  
	 * @param documents Documents to manage
	 */
	public void execute(Document document, List<Document> documents);
	
	/**
	 * Gets the group name for this action.
	 * 
	 * @return group name as String
	 */
	public DocumentActionGroup getGroup();
	
	/**
	 * Representa el orden en el que se presentara la accion entre
	 * elresto de acciones de este tipo documento alla donde estas se
	 * presenten.
	 * 
	 * @return title as String 
	 */
	public int getOrder();
	
	/**
	 * Returns a  phrase with the action title for this document.type.
	 * 
	 * @return description as String 
	 */
	public String getTitle();
	
	/**
	 * Returns a  paragraph with the action description for this document 
	 * type.
	 * 
	 * @return description as String 
	 */
	public String getDescription();	
	
}
