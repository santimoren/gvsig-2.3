/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JOptionPane;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.swing.dynobject.DynObjectEditor;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EditingListenerPanel extends EditingListenerPanelLayout implements Observer {
    private static final long serialVersionUID = -4106763914259136033L;
    private static final Logger logger = LoggerFactory.getLogger(EditingListenerPanel.class);
    private boolean askToAceptNotification;
    private boolean closed = true;
    private StringBuilder buffer = new StringBuilder();
    
    public EditingListenerPanel() {
        initComponents();
        startObservation();
    }
    
    private void initComponents() {
        this.btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                onClear();
            }
        });
        this.btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                setVisible(false);
            }
        });

        this.chkAskToAceptNotification.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                onAskToAceptNotification();
            }
        });
        this.chkSkipFeatureValidation.setSelected(false);

        this.chkAskToAceptNotification.setSelected(false);
        this.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent ce) {
            }
            public void componentMoved(ComponentEvent ce) {
            }
            public void componentShown(ComponentEvent ce) {
                closed = false;
            }
            public void componentHidden(ComponentEvent ce) {
                onClose();
            }
        });
        this.setPreferredSize(new Dimension(300, 400));
    }
    
    public boolean isClosed() {
        return closed;
    }

    private void onClose() {
        this.stopObservation();
        this.closed = true;
    }
    
    private void onClear() {
        this.txtNotifications.setText("");
        this.buffer = new StringBuilder();
    }
    
    private void onAskToAceptNotification() {
        this.askToAceptNotification = this.chkAskToAceptNotification.isSelected();
    }

    private void addText(String text) {
        this.buffer.append(text);
        if( this.buffer.length()> 30000) {
            this.buffer.delete(0, 20000);
        }
        String s = "<html>\n"+buffer.toString()+"</html>\n";
        this.txtNotifications.setText(s);
        this.txtNotifications.setSelectionStart(s.length());
    }
    
    public void update(Observable observable, Object notification) {
        EditingNotification n = (EditingNotification) notification;
        StringBuilder b = new StringBuilder();
        b.append("<b>Notification type</b>: ");
        b.append(n.getType());
        b.append("<br>\n");
        if( n.getSource() == null ) {
            b.append("<b>Source</b>: (unknow)<br>\n");
        } else {
            b.append("<b>Source</b>: <i>");
            b.append(n.getSource().getClass().getName());
            b.append("</i> (");
            b.append(n.getSource().hashCode());
            b.append(") <br>\n");
        }
        if( n.getDocument() == null ) {
            b.append("<b>Document</b>: (unknow)<br>\n");
        } else {
            b.append("<b>Document</b>: <i>");
            b.append(n.getDocument().toString());
            b.append("</i> (");
            b.append(n.getDocument().hashCode());
            b.append(", ");
            b.append(n.getDocument().getClass().getName());
            b.append(") <br>\n");
        }
        if( n.getAuxData()== null ) {
            b.append("<b>Layer</b>: (unknow)<br>\n");
        } else {
            b.append("<b>Layer</b>: <i>");
            b.append(((FLayer)(n.getAuxData())).getName());
            b.append("</i><br>\n");
        }
        if( n.getStore()== null ) {
            b.append("<b>Store</b>: (unknow)<br>\n");
        } else {
            b.append("<b>Store</b>: <i>");
            b.append(n.getStore().getName());
            b.append("</i> (");
            b.append(n.getStore().hashCode());
            b.append(", ");
            b.append(n.getStore().getClass().getName());
            b.append(") <br>\n");
        }
        if( n.getFeature()== null ) {
            b.append("<b>Feature</b>: (unknow)<br>\n");
        } else {
            b.append("<b>Feature</b>: ");
            b.append(n.getFeature().toString());
            b.append("<br>\n");
        }

        b.append("<hr><br>\n");
        this.addText(b.toString());

        n.setSkipFeatureValidation(this.chkSkipFeatureValidation.isSelected());
        
        if( n.isCancelable() ) {
            if( this.chkShowFeatureInForm.isSelected() && n.getFeature()!=null ) {
                try {
                    DynObject data = n.getFeature().getAsDynObject();
                    DynObjectEditor editor = new DynObjectEditor(data);
                    editor.setTitle("Editing monitor - "+n.getFeature().getType().getName());
                    editor.editObject(true);
                    if( editor.isCanceled() ) {
                        if( this.askToAceptNotification ) {
                                n.cancel();
                        }
                    } else {
                         editor.getData(data);
                    }
                } catch (Exception ex) {
                    logger.warn("Problems showing the feature in a form.",ex);
                }
            } else if( this.askToAceptNotification ) {
                ApplicationManager application = ApplicationLocator.getManager();
                int r = application.confirmDialog(
                        "Do you want to cancel the process that generated this notification?", 
                        "Cancel process", 
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if( r == JOptionPane.YES_OPTION ) {
                    n.cancel();
                }
            }
        }
    }

    private void startObservation() {
        EditingNotificationManager editingNotificationManager = DALSwingLocator.getEditingNotificationManager();
        editingNotificationManager.addObserver(this);
    }

    private void stopObservation() {
        EditingNotificationManager editingNotificationManager = DALSwingLocator.getEditingNotificationManager();
        editingNotificationManager.deleteObserver(this);
    }
}
