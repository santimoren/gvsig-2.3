/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.clipboard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.clipboard.util.FeatureTextUtils;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.i18n.Messages;


public class PasteFeaturesFromClipboardExtension extends Extension {

    private static Logger logger = LoggerFactory.getLogger(
    		PasteFeaturesFromClipboardExtension.class);

	public void initialize() {

		IconThemeHelper.registerIcon("action", "layer-modify-clipboard-paste", this);
	}

	public void execute(String actionCommand) {

		if (actionCommand.compareToIgnoreCase("layer-modify-clipboard-paste") != 0) {
			return;
		}

		IWindow actw = actWin();

		if (actw instanceof IView) {

			IView vw = (IView) actw;
			FLayer[] act_lyr = vw.getMapControl().getMapContext().getLayers().getActives();
			if (act_lyr == null || act_lyr.length != 1
					|| !(act_lyr[0] instanceof FLyrVect)) {

			} else {
				FLyrVect vect = (FLyrVect) act_lyr[0];
				if (vect.isEditing()) {

					// Discard if no text data or empty:
					if (!FeatureTextUtils.textInClipboard()) {
						JOptionPane.showMessageDialog(
								ApplicationLocator.getManager().getRootComponent(),
								Messages.getText("_Clipboard_has_no_valid_info_or_is_empty"),
								Messages.getText("_Pasting_features_from_clipboard"),
								JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					String clipb = PluginServices.getFromClipboard();
					if (clipb == null || clipb.length() == 0) {
						JOptionPane.showMessageDialog(
								ApplicationLocator.getManager().getRootComponent(),
								Messages.getText("_Clipboard_has_no_valid_info_or_is_empty"),
								Messages.getText("_Pasting_features_from_clipboard"),
								JOptionPane.INFORMATION_MESSAGE);
						return;
					}

					// ============================================
					// ============================================

					FeatureStore fsto = vect.getFeatureStore();
					List<EditableFeature> try_feats = FeatureTextUtils.fromString(clipb, fsto);
					/*
					 * This method will check geometry type
					 */
					List<EditableFeature> added_feats = addFeatures(fsto, try_feats);

					int n_orig = try_feats.size();
					int n_added = added_feats.size();
					int n_disc = n_orig - n_added;

					String msg = "";

					if (n_orig == 0) {
						msg = Messages.getText("_No_features_found_in_clipboard");
					} else {
						msg = Messages.getText("_Number_of_features_pasted_from_clipboard");
						msg = msg + ":   " + n_added + "    \n";
						msg = msg + Messages.getText("_Number_of_features_from_clipboard_discarded_due_to_bad_format");
						msg = msg + ":   " + n_disc + "    ";
					}


					if (n_added > 0) {

						msg = msg + "\n\n" +
								Messages.getText("_Zoom_to_added_features_question");
						int user_opt = JOptionPane.showConfirmDialog(
								ApplicationLocator.getManager().getRootComponent(),
								msg,
								Messages.getText("_Pasting_features_from_clipboard"),
								JOptionPane.YES_NO_OPTION);
						if (user_opt == JOptionPane.YES_OPTION) {
							try {
								zoomToExtent(added_feats,
										vw.getMapControl().getMapContext());
							} catch (Exception ex) {
								JOptionPane.showMessageDialog(
										ApplicationLocator.getManager().getRootComponent(),
										Messages.getText("_Unable_to_zoom_to_features")
										+ ": " + ex.getMessage(),
										Messages.getText("_Pasting_features_from_clipboard"),
										JOptionPane.ERROR_MESSAGE);
							}

						} else {
							// Repaint view
							vw.getMapControl().getMapContext().invalidate();
						}

					} else {
						JOptionPane.showMessageDialog(
								ApplicationLocator.getManager().getRootComponent(),
								msg,
								Messages.getText("_Pasting_features_from_clipboard"),
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}

		}


	}

	private void zoomToExtent(
			List<EditableFeature> added_feats,
			MapContext mco) throws Exception {

		Envelope env = null;
		Envelope itemenv = null;
		Iterator<EditableFeature> iter = added_feats.iterator();
		Feature feat = null;
		while (iter.hasNext()) {
			feat = (Feature) iter.next();
			itemenv = feat.getDefaultGeometry().getEnvelope();
			if (itemenv != null) {
				if (env == null) {
					itemenv = (Envelope) itemenv.clone();
					env = itemenv;
				} else {
					env.add(itemenv);
				}
			}
		}
		if (env == null) {
			throw new GeometryException(new Exception("All envelopes are null."));
		}
		mco.getViewPort().setEnvelope(env);
	}

	/**
	 * Add features if geometry is of right type.
	 * Returns number of features added.
	 * If returned number is negative, it means there were errors.
	 * Example: -5 means 5 were added and there were errors with other geometries.
	 *
	 * @param fsto
	 * @param add_feats
	 * @throws DataException
	 */
	private List<EditableFeature> addFeatures(
			FeatureStore fsto,
			List<EditableFeature> add_feats) {

		List<EditableFeature> really_added = new ArrayList<EditableFeature>();

		Iterator<EditableFeature> iter = add_feats.iterator();
		EditableFeature item = null;
		Geometry geom = null;
		GeometryType good_gt = null;
		int good_dimensions = 0;

		try {
			good_gt = fsto.getDefaultFeatureType().getDefaultGeometryAttribute().getGeomType();
			good_dimensions = FeatureTextUtils.geoman().create(good_gt).getDimension();
		} catch (Exception e) {
			logger.error("While getting geom type.", e);
			return really_added;
		}

		while (iter.hasNext()) {
			item = iter.next();
			geom = item.getDefaultGeometry();
			/*
			 * Same number of dimensions and compatible types
			 */
			if ((geom.getDimension() == good_dimensions)
					&&
					(geom.getGeometryType().isTypeOf(good_gt)
					|| simpleTypeOf(geom.getGeometryType(), good_gt))
					) {
				try {
					fsto.insert(item);
					really_added.add(item);
				} catch (DataException e) {
					/*
					 * This error will cause that "really_added"
					 * will be shorter than "add_feats" and the user
					 * must be notified of that when the process ends.
					 */
					logger.info("Error while inserting feature from clipboard: " + e.getMessage());
				}
			}
		}

		return really_added;
	}

	private boolean simpleTypeOf(GeometryType simplet, GeometryType multit) {

		return (multit.getType() == Geometry.TYPES.MULTISURFACE
				&& simplet.isTypeOf(Geometry.TYPES.SURFACE))
				||
				(multit.getType() == Geometry.TYPES.MULTICURVE
				&& simplet.isTypeOf(Geometry.TYPES.CURVE))
				||
				(multit.getType() == Geometry.TYPES.MULTIPOINT
				&& simplet.getType() == Geometry.TYPES.POINT);
	}

	public boolean isEnabled() {

		/*
		 * I think it's better to make it enabled always when the active layer
		 * is in editing mode, so it's not necessary to refresh the state of the button
		 * (with a zoom or changing window, etc). The content of the clipboard can change
		 * and gvSIG does not know. Of course, if the user clicks the button,
		 * a dialog will tell if the clipboard is empty or has invalid data.
		 */
		return isVisible();

		/*
		 * Discarded by now:
		 *
		 * It's enabled if the active layer is a vector layer
		 * in editing mode and the clipboard seems to have valid features
		 *
		IWindow actw = actWin();
		if (actw instanceof IView) {

			IView vw = (IView) actw;
			FLayer[] act_lyr = vw.getMapControl().getMapContext().getLayers().getActives();
			if (act_lyr == null || act_lyr.length != 1
					|| !(act_lyr[0] instanceof FLyrVect)) {
				return false;

			} else {
				FLyrVect vect = (FLyrVect) act_lyr[0];
				if (vect.isEditing()) {
					return FeatureTextUtils.clipboardSeemsToHaveValidFeatures();
				} else {
					return false;
				}
			}

		} else {
			return false;
		}
		*/
	}


	public boolean isVisible() {

        org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
        .getActiveWindow();
        if (f == null)
            return false;

        if (f instanceof DefaultViewPanel) {
            DefaultViewPanel vista = (DefaultViewPanel) f;
            ViewDocument model = vista.getViewDocument();
            MapContext mapa = model.getMapContext();

            FLayers capas = mapa.getLayers();

            int numActiveVectorial = 0;
            int numActiveVectorialEditable = 0;

            LayersIterator iter = new LayersIterator(capas);

            FLayer capa;
            while (iter.hasNext()) {
                capa = iter.nextLayer();
                if (capa instanceof FLyrVect &&
                        capa.isActive() && capa.isAvailable()) {
                    numActiveVectorial++;
                    if (capa.isEditing())
                        numActiveVectorialEditable++;
                }

            }

            if (numActiveVectorialEditable == 1 && numActiveVectorial == 1)
			return true;
		}
		return false;
	}

	/**
	 * Gets active window
	 * @return
	 */
	private IWindow actWin() {
		return ApplicationLocator.getManager().getActiveWindow();
	}

}
