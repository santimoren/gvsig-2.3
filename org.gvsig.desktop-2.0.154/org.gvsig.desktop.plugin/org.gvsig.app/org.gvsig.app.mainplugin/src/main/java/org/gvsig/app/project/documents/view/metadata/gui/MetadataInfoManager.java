/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.metadata.gui;

import java.awt.BorderLayout;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.project.documents.view.legend.gui.AbstractThemeManagerPage;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.metadata.swing.basic.api.JMetadataPanel;
import org.gvsig.metadata.swing.basic.api.MetadataSwingLocator;
import org.gvsig.metadata.swing.basic.api.MetadataSwingManager;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;

public class MetadataInfoManager extends AbstractThemeManagerPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7432962479048597376L;
	private JMetadataPanel metadataPanel;

	/**
	 * This is the default constructor.
	 */
	public MetadataInfoManager() {
		super();
		initialize();
	}

	private void initialize() {
		this.setLayout(new BorderLayout());
		this.add(new JBlank(5, 10), BorderLayout.WEST);
		this.add(new JBlank(5, 10), BorderLayout.EAST);
	}

	/**
	 * Sets the necessary properties in the panel. This properties are extracted
	 * from the layer. With this properties fills the TextFields, ComboBoxes and
	 * the rest of GUI components.
	 * 
	 * @param FLayer
	 *            layer,
	 */
	public void setModel(FLayer layer) {
		try {
			MetadataSwingManager manager = MetadataSwingLocator
					.getMetadataSwingManager();
			this.metadataPanel = manager.createJMetadataPanel(layer);			
			this.add(metadataPanel, BorderLayout.CENTER);

		} catch (Exception e) {
			NotificationManager.addError("Can't assign model", e);
		}
	}

	public void acceptAction() {
		if (metadataPanel == null) {
			return;
		}
		saveMetadata();
	}

	private void saveMetadata() {
		try {
			this.metadataPanel.saveMetadata();
		} catch (DynObjectValidateException e) {
			JOptionPane.showMessageDialog(null, getInfoString(e),
					"Metadata Validation Errors",
					 JOptionPane.WARNING_MESSAGE);
		}
	}

	private String getInfoString(DynObjectValidateException e) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<html>");
		buffer.append("<body>");
		buffer.append("<h2>Metadata values could not be stored due to the following errors:'")
				.append(this.metadataPanel.getMetadata().getDynClass()
						.getName()).append("'</h2>");
		buffer.append("<p>");
		buffer.append("<br>");
		buffer.append("<ol>");
		buffer.append("  " + e.getLocalizedMessageStack());
		buffer.append("</ol>");
		buffer.append("</p>");
		buffer.append("</body>");
		buffer.append("</html>");

		return buffer.toString().replace("\n", "<br/>");
	}

	public void cancelAction() {
		// does nothing
	}

	/**
	 * When we press the apply button, sets the new properties of the layer that
	 * the user modified using the UI components
	 */
	public void applyAction() {
		if (metadataPanel == null) {
			return;
		}
		saveMetadata();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Component#getName()
	 */
	public String getName() {
		return PluginServices.getText(this, "_Metadata");
	}

}
