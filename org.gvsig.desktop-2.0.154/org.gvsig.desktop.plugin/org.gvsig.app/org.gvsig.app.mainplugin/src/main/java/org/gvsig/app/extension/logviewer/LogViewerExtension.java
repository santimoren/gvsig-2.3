package org.gvsig.app.extension.logviewer;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;


public class LogViewerExtension extends Extension {
    
    
    public LogViewerExtension() {
        
    }

    @Override
    public void initialize() {
    }

    @Override
    public void execute(String actionCommand) {
        if( "log-view".equalsIgnoreCase(actionCommand) ) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            WindowManager winmgr = ToolsSwingLocator.getWindowManager();
            winmgr.showWindow(
                    new LogViewer(), 
                    i18nManager.getTranslation("_Log viewer"),
                    WindowManager.MODE.WINDOW
            );
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        return true;
    }
}
