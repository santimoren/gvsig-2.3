/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.actions;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.extension.ProjectExtension;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;


public class EliminarCapaTocMenuEntry extends AbstractTocContextMenuAction {
	public String getGroup() {
		return "group3"; //FIXME
	}

	public int getGroupOrder() {
		return 30;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "eliminar_capa");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return true;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			return true;
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
    	// 050209, jmorell: Para poder borrar todas las capas seleccionadas desde el FPopUpMenu.
		//					Es necesario pulsar May�sculas a la vez que el bot�n derecho.

    	FLayer[] actives = selectedItems;

    	int i;
    	for (i= 0; i < actives.length;i++){
    		if (actives[i].isEditing() && actives[i].isAvailable()){
    			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(), PluginServices.getText(this,"no_se_puede_borrar_una_capa_en_edicion"), PluginServices.getText(this, "eliminar_capa"), JOptionPane.WARNING_MESSAGE);
    			return;
    		}
    	}

    	int option=-1;
    	if (actives.length>0) {
    		option=JOptionPane.showConfirmDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"desea_borrar_la_capa"));
    	} else {
    		return;
    	}
    	if (option!=JOptionPane.OK_OPTION)
    		return;
    	getMapContext().beginAtomicEvent();
    	for (i = actives.length-1; i>=0; i--){
        	try {
				//actives[i].getParentLayer().removeLayer(actives[i]);
				//FLayers lyrs=getMapContext().getLayers();
				//lyrs.addLayer(actives[i]);
				actives[i].getParentLayer().removeLayer(actives[i]);

                //Cierra todas las ventanas asociadas a la capa
        		IWindow[] wList = PluginServices.getMDIManager().getAllWindows();
        		for (int j = 0; j < wList.length; j++) {
        			String name = wList[j].getWindowInfo().getAdditionalInfo();
        			for (int k = 0; k < actives.length; k++) {
        				if( name != null && actives != null && actives[k] != null &&
                			actives[k].getName() != null &&
                			name.compareTo(actives[k].getName()) == 0)
                			PluginServices.getMDIManager().closeWindow(wList[j]);
					}
        		}

    		} catch (CancelationException e1) {
    			e1.printStackTrace();
    		}
    	}
        // ===============================================
    	/*
    	 * If the removed layer is in a group, the TOC does not received a
    	 * notification to refresh, so we force a refreshment unless the TOC is empty
    	 */
    	if (getMapContext().getLayers().getLayersCount() > 0) {
    	    getMapContext().getLayers().moveTo(0, 0);
    	}
        // ===============================================
    	getMapContext().endAtomicEvent();
    	Project project=((ProjectExtension)PluginServices.getExtension(ProjectExtension.class)).getProject();
		project.setModified(true);
    	PluginServices.getMainFrame().enableControls();
		// 050209, jmorell: As� solo borra una capa (sobre la que pulsas).
    	/*FLayer lyr = getNodeLayer();
        try {
        	getMapContext().getLayers().removeLayer(lyr);
        	if (getMapContext().getLayers().getLayersCount()==0)PluginServices.getMainFrame().enableControls();
		} catch (CancelationException e1) {
			e1.printStackTrace();
		}*/
    }
}
