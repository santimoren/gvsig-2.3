/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners;


import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.PointSelectionListener;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;



/**
 * <p>Inherits {@link PointSelectListener PointSelectListener} enabling/disabling special
 *  controls for managing the data selected.</p>
 *
 * @see PointSelectionListener
 *
 * @author Vicente Caballero Navarro
 */
public class PointSelectListener extends PointSelectionListener {
	/**
	 * <p>Creates a new <code>PointSelectListener</code> object.</p>
	 * 
	 * @param mapCtrl the <code>MapControl</code> where will be applied the changes
	 */
	public PointSelectListener(MapControl mapCtrl) {
		super(mapCtrl);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.PointSelectionListener#point(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
	 */
	public void point(PointEvent event) throws BehaviorException {
		super.point(event);
		PluginServices.getMainFrame().enableControls();
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.PointSelectionListener#pointDoubleClick(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
	 */
	public void pointDoubleClick(PointEvent event) throws BehaviorException {
	/*	try {
			FLayer[] actives = mapCtrl.getMapContext()
            .getLayers().getActives();
            for (int i=0; i < actives.length; i++){
                if (actives[i] instanceof FLyrAnnotation && actives[i].isEditing()) {
                    FLyrAnnotation lyrAnnotation = (FLyrAnnotation) actives[i];

                    	lyrAnnotation.setSelectedEditing();
                    	lyrAnnotation.setInEdition(lyrAnnotation.getRecordset().getSelection().nextSetBit(0));
                    	FLabel fl=lyrAnnotation.getLabel(lyrAnnotation.getInEdition());
        				if (fl!=null){

        					View vista=(View)PluginServices.getMDIManager().getActiveView();
        					TextFieldEdit tfe=new TextFieldEdit(lyrAnnotation);

        					tfe.show(vista.getMapControl().getViewPort().fromMapPoint(fl.getOrig()),vista.getMapControl());
        				}


                }
            }

		} catch (DriverLoadException e) {
			e.printStackTrace();
			throw new BehaviorException("Fallo con el recordset");
		} catch (com.iver.cit.gvsig.fmap.DriverException e) {
			e.printStackTrace();
			throw new BehaviorException("Fallo con el recordset");
		}
*/
	}
}
