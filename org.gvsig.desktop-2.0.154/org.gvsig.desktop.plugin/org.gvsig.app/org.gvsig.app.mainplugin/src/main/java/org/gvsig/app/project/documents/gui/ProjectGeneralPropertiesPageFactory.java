
package org.gvsig.app.project.documents.gui;

import org.gvsig.app.project.Project;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;

public class ProjectGeneralPropertiesPageFactory implements PropertiesPageFactory {

    public String getGroupID() {
        return Project.PROJECT_PROPERTIES_PAGE_GROUP;
    }
    
    public boolean isVisible(Object obj) {
        // Always if enabled this page
        return true;
    }

    public PropertiesPage create(Object obj) {
        return new ProjectGeneralPropertiesPage((Project)obj);
    }
    
}
