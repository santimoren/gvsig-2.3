/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.info.gui;

import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLEditorKit;

import org.gvsig.utils.BrowserControl;


/**
 * JPanel to show the feature information return in HTML code
 * @author laura
 *
 */
public class HTMLInfoToolPanel extends JPanel implements IInfoToolPanel{
		
	private boolean initialized = false;
	private JEditorPane editor = null;
	private JScrollPane scrollPane = null;

	public HTMLInfoToolPanel() throws HeadlessException {
		super();
	}
	
	private void init() {
		if (this.initialized ) return;
				
		this.setAutoscrolls(true);
		this.setLocation(0,0);

		scrollPane = new JScrollPane();		
		scrollPane.setAutoscrolls(true);
				
		editor = new JEditorPane();
		editor.setContentType("text/html");
		editor.setAutoscrolls(true);
		editor.setEditable(false);
		editor.addHyperlinkListener(new javax.swing.event.HyperlinkListener() { 
	          public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
	           if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
	           {
	        	   BrowserControl.displayURL(e.getURL().toString());
	           }
	          }
		});
		
//azabala		this.add(editor);
		
		
		this.setSize(new Dimension(640, 400));
		editor.setSize(new Dimension(640, 400));
		
		
		editor.setEditorKit(new HTMLEditorKit());
        scrollPane.setViewportView(editor);
//azabalA		
		this.add(scrollPane);
		
//azabala		scrollPane.setLocation(0,0);
	}
	
	public void show(String text) 
	{
		this.init();
		this.setVisible(true);	
		editor.setText(text.replaceFirst("Content-Type","Content-Typex"));		
	}

	public void refreshSize() {
		// TODO Auto-generated method stub
		
	}

	public void show(XMLItem item) {
		// TODO Auto-generated method stub
		
	}
}
