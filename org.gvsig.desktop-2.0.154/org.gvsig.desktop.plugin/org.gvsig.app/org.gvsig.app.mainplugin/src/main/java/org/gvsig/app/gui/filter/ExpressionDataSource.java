/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.filter;



/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision: 29596 $
 */
public interface ExpressionDataSource {
	/**
	 * DOCUMENT ME!
	 *
	 * @param row DOCUMENT ME!
	 * @param idField DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws FilterException DOCUMENT ME!
	 */
	public Object getFieldValue(int row, int idField) throws FilterException;

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws FilterException DOCUMENT ME!
	 */
	public int getFieldCount() throws FilterException;

	/**
	 * DOCUMENT ME!
	 *
	 * @param idField DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws FilterException DOCUMENT ME!
	 */
	public String getFieldName(int idField) throws FilterException;

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 *
	 * @throws FilterException DOCUMENT ME!
	 */
	public int getRowCount() throws FilterException;

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public String getDataSourceName();

//    public void start() throws ReadDriverException;
//    public void stop() throws ReadDriverException;

}
