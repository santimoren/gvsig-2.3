/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.gui;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.Messages;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.operations.Classifiable;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.propertypage.PropertiesPage;


public abstract class AbstractThemeManagerPage extends JPanel implements PropertiesPage {
	/**
	 * Cuando hay varios capas vectoriales seleccionados, devolver� el �ltimo.
	 *
	 * @param layers Grupo de layers.
	 *
	 * @return la primera flayer seleccionada.
	 */
	protected FLayer getFirstActiveLayerVect(FLayers layers) {
		// Comprobar en openLegendManager que hay alg�n capa activo!
		FLayer[] activeLyrs = layers.getActives();

		if (activeLyrs.length == 0) {
			JOptionPane.showMessageDialog(null,
				Messages.getString("necesita_una_capa_activa"), "",
				JOptionPane.ERROR_MESSAGE);

			return null;
		}

		FLayer lyr = null;

		for (int i = 0; i < activeLyrs.length; i++) {
			if (activeLyrs[i] instanceof FLayers) {
				lyr = getFirstActiveLayerVect((FLayers) activeLyrs[i]);
			}

			if (activeLyrs[i] instanceof Classifiable) {
				Classifiable auxC = (Classifiable) activeLyrs[i];
				ILegend theLegend = auxC.getLegend();

				if (theLegend instanceof IVectorLegend) {
					lyr = (FLayer) auxC;
				}
			}
		}

		if (lyr == null) {
			JOptionPane.showMessageDialog(null,
				Messages.getString(
						PluginServices.getText(this, "necesita_una_capa_vectorial_activa") +
						"\n\n"+
						PluginServices.getText(this, "Por_favor_active_la_capa") + "."),
				"",
				JOptionPane.ERROR_MESSAGE);
			return null;
		}

		return lyr;
	}

        public boolean isTabEnabledForLayer(FLayer layer) {
            return ThemeManagerWindow.isTabEnabledForLayer(this, layer);
        }
        
        /**
         * Retorna la prioridad usada para determinar la posicion de la pesta�a.
         * 
         * Cuanto mas alta sea esta mas a la izquierda estara la pesta�a.
         * @return La prioridad 
         */
        public int getPriority() {
            return 50;
        }
        
	/**
	 * Returns the name of this ThemeManagerPage's tab, the text returned by this
	 * method will be shown in the text of this panel's tab.
	 */
	public abstract String getName();

	/**
	 * <p>
	 * Method invoked when the Ok button is pressed from the ThemeManagerWindow.
	 * It will cause the changes performed by the user to take effect into the
	 * layer if the Apply button wasn't pressed yet. In case Apply button was
	 * pressed, then the programmer can choose between apply the changes again or
	 * not.<br>
	 * </p>
	 * <p>
	 * It shouldn't be a problem rather than the potential consumption of time
	 * required in when applying such changes.<br>
	 * </p>
	 * <p>
	 * <b>Notice</b> that after the call of this method the ThemeManagerWindow will be closed.
	 * </p>
	 */
	public abstract void acceptAction();

	/**
	 * <p>
	 * Method invoked when the Cancel button is pressed from the ThemeManagerWindow.
	 * It will cause that the changes performed will be discarded.
	 * </p>
	 */
	public abstract void cancelAction();

	/**
	 * Method invoked when the Apply button is pressed from the ThemeManagerWindow.
	 * It will cause the changes performed by the user to take effect inmediately
	 * into the the layer.
	 */
	public abstract void applyAction();

	/**
	 * This method is invoked during the initialization of the ThemeManagerWindow
	 * and causes the dialog to be updated to reflect the current settings of
	 * the layer in the context that this panel was designed for.
	 * @param layer, the target FLayer
	 */
	public abstract void setModel(FLayer layer);

    void setModel(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getTitle() {
        return this.getName();
    }
    
    public boolean whenAccept()  {
        acceptAction();
        return true;
    }

    public boolean whenApply()  {
        applyAction();
        return true;
    }

    public boolean whenCancel()  {
        cancelAction();
        return true;
    }

    public JComponent asJComponent() {
        return this;
    }
    
    
    
}
