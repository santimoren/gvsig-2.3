/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import javax.swing.JPanel;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.installer.translations.TranslationsInstallerFactory;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.gvsig.tools.swing.api.Component;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TranslationsPackageExtension extends Extension {

    private static Logger logger = LoggerFactory
            .getLogger(TranslationsPackageExtension.class);

    public void initialize() {
        // Do nothing
    }

    public void postInitialize() {
        super.postInitialize();
    }

    public void execute(String actionCommand) {
        if ( "tools-devel-translations-package".equalsIgnoreCase(actionCommand) ) {
            ApplicationManager application = ApplicationLocator.getManager();
            PluginsManager pluginsManager = PluginsLocator.getManager();
            SwingInstallerManager installerManager = SwingInstallerLocator.getSwingInstallerManager();
            Component packager = installerManager.createPackagerPanel(
                    TranslationsInstallerFactory.PROVIDER_NAME,
                    pluginsManager.getApplicationI18nFolder(),
                    pluginsManager.getInstallFolder()
            );
            application.getUIManager().showWindow(
                    (JPanel)packager.asJComponent(),
                    application.translate("_Translations_package"),
                    WindowManager.MODE.WINDOW
            );
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return true;
    }
}
