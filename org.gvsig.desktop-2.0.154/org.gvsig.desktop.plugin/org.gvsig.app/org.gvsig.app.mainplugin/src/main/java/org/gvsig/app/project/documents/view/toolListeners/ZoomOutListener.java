/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners;

import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.ZoomOutListenerImpl;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;



/**
 * <p>Inherits {@link ZoomOutListenerImpl ZoomOutListenerImpl} enabling/disabling special
 *  controls for managing the data selected.</p>
 *
 * @see ZoomOutListenerImpl
 *
 * @author Vicente Caballero Navarro
 */
public class ZoomOutListener extends ZoomOutListenerImpl {
	/**
	 * <p>Creates a new <code>ZoomOutListener</code> object.</p>
	 * 
	 * @param mapCtrl the <code>MapControl</code> where be applied the changes
	 */
	public ZoomOutListener(MapControl mapCtrl) {
		super(mapCtrl);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.tools.ZoomOutListenerImpl#point(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
	 */
	public void point(PointEvent event) {
		super.point(event);
		PluginServices.getMainFrame().enableControls();
	}
}
