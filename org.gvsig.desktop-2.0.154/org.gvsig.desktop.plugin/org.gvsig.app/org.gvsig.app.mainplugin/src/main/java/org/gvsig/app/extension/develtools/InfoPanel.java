/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.gvsig.app.ApplicationLocator;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InfoPanel extends JPanel {
	private static Logger logger = LoggerFactory.getLogger(InfoPanel.class);


	private static final long serialVersionUID = 7164653790859770568L;

	private JButton accept = null;
	private JButton copy = null;
	private JTextPane text = null;

	public static void showPanel(String title, WindowManager.MODE mode,
			String html) {
		JPanel panel = new InfoPanel(html, null);
		WindowManager wm = ToolsSwingLocator.getWindowManager();
		wm.showWindow(panel, title, mode);
	}
        

	public static void showPanel(String title, WindowManager.MODE mode,
			String html, HyperlinkListener hyperlinkListener) {
		JPanel panel = new InfoPanel(html,hyperlinkListener);
		WindowManager wm = ToolsSwingLocator.getWindowManager();
		wm.showWindow(panel, title, mode);
	}
        

	public static void save2file(String name, String contents) {
		File file;
		try {
			file = File.createTempFile("gvsig-" + name + "-", ".html");
			FileWriter fwriter = new FileWriter(file);
			fwriter.append(contents);
			fwriter.close();
		} catch (IOException e) {
			logger.warn("Can't save contents to temp file gvsig-" + name, e);
		}
	}
        
	public InfoPanel(final String html, HyperlinkListener hyperlinkListener) {
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(500, 300));
		this.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		text = new JTextPane();
		text.setContentType("text/html");
		text.setEditable(false);
		text.setText(html);
		text.setCaretPosition(0);
                if( hyperlinkListener!=null ) {
                    text.addHyperlinkListener(hyperlinkListener);
                }

		JScrollPane scrollPane = new JScrollPane(text);
		scrollPane.setPreferredSize(new Dimension(500, 220));

		accept = new JButton("Accept");
		accept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		copy = new JButton("Copy to clipboard");
		copy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ApplicationLocator.getManager().putInClipboard(
						text.getText());
			}
		});

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel,
				BoxLayout.LINE_AXIS));
		buttonsPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		buttonsPanel.add(Box.createHorizontalGlue());
		buttonsPanel.add(copy);
		buttonsPanel.add(accept);

		this.add(scrollPane, BorderLayout.CENTER);
		this.add(buttonsPanel, BorderLayout.SOUTH);
		this.setVisible(true);
	}

}
