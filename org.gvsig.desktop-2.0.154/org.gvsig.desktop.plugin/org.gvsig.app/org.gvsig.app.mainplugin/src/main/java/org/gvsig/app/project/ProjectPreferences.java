/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.app.project;

import java.awt.Color;
import org.apache.commons.lang.StringUtils;

import org.cresques.cts.IProjection;
import org.gvsig.andami.PluginServices;
import org.gvsig.app.imp.DefaultPreferencesNode;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.utils.StringUtilities;
import org.gvsig.utils.XMLEntity;

/**
 * Clase que representa las preferencias para el proyecto
 *
 * @author 2009-         Joaquin del Cerro
 * 
 */

public class ProjectPreferences extends DefaultPreferencesNode {

    public static final String DEFAULT_PROJECTION_KEY_NAME = "DefaultProjection";
    
	private IProjection defaultProjection = null;

	private IProjection defaultFactoryProjection =
	    MapContextLocator.getMapContextManager().getDefaultCRS();

	private Color defaultSelectionColor = Color.YELLOW;
	
	private Color defaultViewBackColor = Color.WHITE;

	private Color defaultOverviewBackColor = Color.WHITE;	

	private int defaultMapUnits = -1;

	private int defaultDistanceUnits = -1;

	private int defaultDistanceArea = -1;

	
	public ProjectPreferences() {
	}

        /**
	 * Sets the projection used when no projection is defined
	 *
	 * @param defaultProjection
	 */
	public void setDefaultProjection(IProjection defaultProjection) {
            this.defaultProjection = defaultProjection;
            if (defaultProjection != null){
                PluginServices plugin = PluginServices.getPluginServices("org.gvsig.app");
                XMLEntity xml = plugin.getPersistentXML();
                xml.putProperty(DEFAULT_PROJECTION_KEY_NAME, defaultProjection.getAbrev());
            }
	}
	
	public void setDefaultProjection(String defaultProjection) {
            if( !StringUtils.isEmpty(defaultProjection) ) {
                this.setDefaultProjection(CRSFactory.getCRS(defaultProjection));
            }
        }
        
	public IProjection getDefaultProjection() {
            if (this.defaultProjection == null){
                PluginServices plugin = PluginServices.getPluginServices("org.gvsig.app");
                XMLEntity xml = plugin.getPersistentXML();
                if (xml.contains(DEFAULT_PROJECTION_KEY_NAME)) {
                        String projCode = xml.getStringProperty(DEFAULT_PROJECTION_KEY_NAME);
                        this.defaultProjection = CRSFactory.getCRS(projCode);
                } else {
                        this.defaultProjection = defaultFactoryProjection;
                }
            }
            return this.defaultProjection;
	}

	public int getInt(String name, int defaultValue) {
		if( name.equalsIgnoreCase("DefaultDistanceUnits")) {
			return this.getDefaultDistanceUnits();
		} else if( name.equalsIgnoreCase("DefaultDistanceArea")) {
			return this.getDefaultDistanceArea();
		} else if( name.equalsIgnoreCase("DefaultMapUnits")) {
			return this.getDefaultMapUnits();
		}
		
		return super.getInt(name, defaultValue);
	}

	/**
	 * Sets the default selection color that will be used in subsequent
	 * projects.
	 *
	 * @param color
	 */
	public void setDefaultSelectionColor(Color color) {
		defaultSelectionColor = color;
	}

	/**
	 * Returns the current default selection color defined which is the color
	 * defined when the user does not define any other one
	 *
	 * @return java.awt.Color
	 */
	public Color getDefaultSelectionColor() {
		XMLEntity xml = PluginServices.getPluginServices("org.gvsig.app")
				.getPersistentXML();
		if (xml.contains("DefaultSelectionColor")) {
			defaultSelectionColor = StringUtilities.string2Color(xml
					.getStringProperty("DefaultSelectionColor"));
		}
		return defaultSelectionColor;
	}

	public Color getDefaultViewBackColor() {
		XMLEntity xml = PluginServices.getPluginServices("org.gvsig.app").getPersistentXML();
		if (xml.contains("DefaultViewBackColor")) {
			defaultViewBackColor = StringUtilities.
				string2Color(xml.getStringProperty("DefaultViewBackColor"));
		}
		return defaultViewBackColor;
	}

	public void setDefaultViewBackColor(Color color) {
		defaultViewBackColor = color;
	}

	public Color getDefaultOverviewBackColor() {
		XMLEntity xml = PluginServices.getPluginServices("org.gvsig.app")
				.getPersistentXML();
		if (xml.contains("defaultOverviewBackColor")) {
			defaultOverviewBackColor = StringUtilities.string2Color(xml
					.getStringProperty("defaultOverviewBackColor"));
		}
		return defaultOverviewBackColor;

	}

	public void setDefaultOverviewBackColor(Color color) {
		defaultOverviewBackColor = color;
	}

	/**
	 * Returns the user's default map units. This is the cartography data
	 * distance units.
	 *
	 * @return int (index of the <b>Attributes.NAMES array</b>)
	 */
	public int getDefaultMapUnits() {
		if (defaultMapUnits == -1) {
			XMLEntity xml = PluginServices.getPluginServices(
					"org.gvsig.app").getPersistentXML();
			if (xml.contains("DefaultMapUnits")) {
				defaultMapUnits = xml.getIntProperty("DefaultMapUnits");
			} else {
				// first app run case
				String[] unitNames = MapContext.getDistanceNames();
				for (int i = 0; i < unitNames.length; i++) {
					// meter is the factory default's map unit
					if (unitNames[i].equals("Metros")) {
						defaultMapUnits = i;
						break;
					}
				}
			}
			if (defaultMapUnits == -1 || defaultMapUnits >= MapContext.getDistanceNames().length){
				defaultMapUnits = MapContext.getDistancePosition("Metros");
			}
		}
		return defaultMapUnits;
	}

	/**
	 * Returns the user's default view units for measuring distances. This is
	 * the units that the user will see in the status bar of the view.
	 *
	 * @return int (index of the <b>Attributes.NAMES array</b>)
	 */
	public int getDefaultDistanceUnits() {
		if (defaultDistanceUnits == -1) {
			XMLEntity xml = PluginServices.getPluginServices(
					"org.gvsig.app").getPersistentXML();
			if (xml.contains("DefaultDistanceUnits")) {
				defaultDistanceUnits = xml
						.getIntProperty("DefaultDistanceUnits");
			} else {
				// first app run case
				String[] unitNames = MapContext.getDistanceNames();
				for (int i = 0; i < unitNames.length; i++) {
					// meter is the factory default's distance unit
					if (unitNames[i].equals("Metros")) {
						defaultDistanceUnits = i;
						break;
					}
				}
			}
			if (defaultDistanceUnits == -1 || defaultDistanceUnits >= MapContext.getDistanceNames().length){
				defaultDistanceUnits = MapContext.getDistancePosition("Metros");
			}
		}
		return defaultDistanceUnits;
	}
	/**
	 * Returns the user's default view units for measuring areas. This is
	 * the units that the user will see in the status bar of the view.
	 *
	 * @return int (index of the <b>Attributes.NAMES array</b>)
	 */
	public int getDefaultDistanceArea() {
		if (defaultDistanceArea == -1) {
			XMLEntity xml = PluginServices.getPluginServices(
					"org.gvsig.app").getPersistentXML();
			if (xml.contains("DefaultDistanceArea")) {
				defaultDistanceArea = xml
						.getIntProperty("DefaultDistanceArea");
			} else {
				// first app run case
				String[] unitNames = MapContext.getAreaNames();
				for (int i = 0; i < unitNames.length; i++) {
					// meter is the factory default's distance unit
					if (unitNames[i].equals("Metros")) {
						defaultDistanceArea = i;
						break;
					}
				}
			}
			if (defaultDistanceArea == -1 || defaultDistanceArea >= MapContext.getAreaNames().length){
				defaultDistanceArea=getDefaultDistanceUnits();
			}
		}
		return defaultDistanceArea;
	}
	/**
	 * Sets the default map unit (the units used by the data).
	 *
	 * @param mapUnits
	 */
	public void setDefaultMapUnits(int mapUnits) {
		defaultMapUnits = mapUnits;
	}

	/**
	 * Sets the default distance units (the units shown in the status bar)
	 *
	 * @param distanceUnits
	 */
	public void setDefaultDistanceUnits(int distanceUnits) {
		defaultDistanceUnits = distanceUnits;
	}
	/**
	 * Sets the default distance area (the units shown in the status bar)
	 *
	 * @param distanceUnits
	 */
	public void setDefaultDistanceArea(int distanceArea) {
		defaultDistanceArea = distanceArea;
	}

}
