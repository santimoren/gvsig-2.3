package org.gvsig.app.extension;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACTION_COMMAND_KEY;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.JOptionPane;
import org.cresques.cts.IProjection;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.featureform.swing.CreateJFeatureFormException;
import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.DataSwingManager;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsGeometryEvaluator;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PointBehavior;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PointListener;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowLayerAsForm extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(ShowLayerAsForm.class);

    @Override
    public void initialize() {
        IconThemeHelper.registerIcon("action", "layer-selectforediting", this);
    }

    @Override
    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();
        if ("layer-show-form".equalsIgnoreCase(actionCommand)) {
            ViewDocument doc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
            if (doc == null) {
                return;
            }
            FLayer[] layers = doc.getMapContext().getLayers().getActives();
            for (int i = 0; i < layers.length; i++) {
                FLayer layer = layers[i];
                if (layer.isAvailable() && layer instanceof FLyrVect) {
                    try {
                        FLyrVect vectLayer = (FLyrVect) layer;
                        JFeaturesForm form = this.createform(vectLayer.getFeatureStore());
                        form.addAction(new ZoomToCurrentAction(doc, form));
                        form.addAction(new SelectFeatureInTheViewAction(doc, form, layer));
                        form.showForm(WindowManager.MODE.WINDOW);
                    } catch (Exception ex) {
                        String msg = "Can't show form for layer '" + layer.getName() + "'.";
                        logger.warn(msg, ex);
                        application.messageDialog(msg + "\n\n(See the error log for more information)", "Warning", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        }
    }

    private static class SelectFeatureInTheViewAction extends AbstractAction implements PointListener {

        private final JFeaturesForm form;
        private final ViewDocument doc;
        private MapControl mapControl = null;
        private String previousTool = null;
        private final FLyrVect layer;

        public SelectFeatureInTheViewAction(ViewDocument doc, JFeaturesForm form, FLayer layer) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getDefault();

            this.doc = doc;
            this.form = form;
            this.putValue(NAME, null);
            this.putValue(SHORT_DESCRIPTION, i18nManager.getTranslation("_Select_feature_in_the_View"));
            this.putValue(SMALL_ICON, iconTheme.get("layer-selectforediting"));
            this.putValue(ACTION_COMMAND_KEY, "selectInTheView");
            if( layer instanceof FLyrVect ) {
                this.layer = (FLyrVect) layer;
                this.setEnabled(true);
            } else {
                this.layer = null;
                this.setEnabled(false);
            }
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            ApplicationManager application = ApplicationLocator.getManager();
            if( this.layer == null ) {
                return;
            }
            if (this.mapControl == null) {
                IView view = (IView) application.getActiveComponent(ViewDocument.class);
                if (view != null) {
                    MapControl mapControl = view.getMapControl();
                    this.previousTool = mapControl.getCurrentTool();
                    mapControl.addBehavior("layer-selectforediting", new PointBehavior(this));
                    mapControl.setTool("layer-selectforediting");
                    this.mapControl = mapControl;
                    mapControl.requestFocus();
                }
            } else {
                this.mapControl.setTool(this.previousTool);
                this.mapControl = null;
                this.previousTool = null;
            }
        }

        @Override
        public void point(PointEvent event) throws BehaviorException {
            ApplicationManager application = ApplicationLocator.getManager();
            try {
                Point point = event.getMapPoint();
                double tolerance = mapControl.getViewPort().toMapDistance(7);
                GeometryManager manager = GeometryLocator.getGeometryManager();
                Circle circle = (Circle) manager.create(
                        Geometry.TYPES.CIRCLE,
                        Geometry.SUBTYPES.GEOM2D
                );
                circle.setPoints(point, tolerance);

                FeatureStore featureStore = this.form.getFeatureStore();
                FeatureType featureType = featureStore.getDefaultFeatureType();
                FeatureQuery featureQuery = featureStore.createFeatureQuery();
                String geomName = featureType.getDefaultGeometryAttributeName();
                featureQuery.setFeatureType(featureType);

                Geometry query_geo = this.layer.transformToSourceCRS(circle, true);
                IProjection query_proj = this.mapControl.getMapContext().getProjection();
                if (this.layer.getCoordTrans() != null) {
                    query_proj = this.layer.getCoordTrans().getPOrig();
                }

                IntersectsGeometryEvaluator iee
                        = new IntersectsGeometryEvaluator(
                                query_geo,
                                query_proj,
                                featureStore.getDefaultFeatureType(),
                                geomName);
                featureQuery.setFilter(iee);
                featureQuery.setAttributeNames(null);

                this.form.setQuery(featureQuery);

                this.mapControl.setTool(this.previousTool);
                this.mapControl = null;
                this.previousTool = null;
            } catch (Exception e) {
                application.message("Can't filter form.", JOptionPane.WARNING_MESSAGE);
                throw new RuntimeException("Can't create filter", e);
            }
        }

        @Override
        public void pointDoubleClick(PointEvent event) throws BehaviorException {
            this.point(event);
        }

        @Override
        public Image getImageCursor() {
            return IconThemeHelper.getImage("cursor-info-by-point");
        }

        @Override
        public boolean cancelDrawing() {
            return false;
        }

    }

    private static class ZoomToCurrentAction extends AbstractAction {

        private final JFeaturesForm form;
        private final ViewDocument doc;

        public ZoomToCurrentAction(ViewDocument doc, JFeaturesForm form) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getDefault();

            this.doc = doc;
            this.form = form;
            this.putValue(NAME, null);
            this.putValue(SHORT_DESCRIPTION, i18nManager.getTranslation("_Zoom"));
            this.putValue(SMALL_ICON, iconTheme.get("view-navigation-zoom-to-selection"));
            this.putValue(ACTION_COMMAND_KEY, "zoomToCurrent");

            this.setEnabled(doc != null);
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            long index = this.form.getCurrentIndex();
            if (index < 0) {
                return;
            }
            Feature f = this.form.get(index);
            if (f == null) {
                return;
            }
            Geometry g = f.getDefaultGeometry();
            if (g != null) {
                doc.getMapContext().getViewPort().setEnvelope(g.getEnvelope());
            }
        }

    }

    private JFeaturesForm createform(FeatureStore featureStore) throws CreateJFeatureFormException, ServiceException, DataException {
        final DataSwingManager swingManager = DALSwingLocator.getSwingManager();
        final JFeaturesForm form = swingManager.createJFeaturesForm(featureStore);
        form.setPreferredSize(new Dimension(400, 300));
        return form;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument doc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if (doc == null) {
            return false;
        }
        return doc.getMapContext().hasActiveVectorLayers();
    }
}
