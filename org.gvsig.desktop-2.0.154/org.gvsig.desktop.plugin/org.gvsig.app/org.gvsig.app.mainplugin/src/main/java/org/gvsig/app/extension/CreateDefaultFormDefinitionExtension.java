
package org.gvsig.app.extension;

import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.DataSwingManager;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;

/**
 *
 * @author jjdelcerro
 */
public class CreateDefaultFormDefinitionExtension extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(CreateDefaultFormDefinitionExtension.class);


    @Override
    public void initialize() {
    }

    @Override
    public void execute(String actionCommand) {
        if( "layer-create-default-form-definition".equalsIgnoreCase(actionCommand) ) {
            ApplicationManager application = ApplicationLocator.getManager();
            ViewDocument viewDoc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
            if( viewDoc == null ) {
                return;
            }
            MapContext mapContext = viewDoc.getMapContext();
            if( !mapContext.hasActiveVectorLayers() ) {
                return;
            }
            FeatureTypeDefinitionsManager featureTypeDefinitionsManager = DALLocator.getFeatureTypeDefinitionsManager();
            FLayer[] layers = mapContext.getLayers().getActives();
            for (FLayer layer1 : layers) {
                if (layer1 instanceof FLyrVect) {
                    FLyrVect layer = (FLyrVect) layer1;
                    try {
                        FeatureStore store = layer.getFeatureStore();
                        FeatureType featureType = store.getDefaultFeatureType();
                        featureTypeDefinitionsManager.add(store, featureType, featureType); 
                        application.messageDialog(
                                "Se creado la definicion de formulario asociada a la capa '"+layer.getName()+"'.",
                                "Definicion de formulario", 
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    } catch (Exception ex) {
                        logger.warn("Can't save form definition for layer '"+layer.getName()+"'.",ex);
                        application.messageDialog(
                                "No se ha podido crear la definicion de formulario asociada a la capa '"+layer.getName()+"'.\n\n(Consulte el registro de errores si desea mas informacion)",
                                "Definicion de formulario", 
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    }
                }
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument viewDoc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if( viewDoc == null ) {
            return false;
        }
        MapContext mapContext = viewDoc.getMapContext();
        return mapContext.hasActiveVectorLayers();
    }

}
