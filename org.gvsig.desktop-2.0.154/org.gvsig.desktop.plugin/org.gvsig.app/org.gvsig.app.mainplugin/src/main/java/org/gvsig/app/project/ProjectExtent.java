/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project;

import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author 2006-2009 Jose Manuel Vivo
 * @author 2005-         Vicente Caballero
 * @author 2009-         Joaquin del Cerro
 * 
 */

public class ProjectExtent implements Persistent {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(ProjectExtent.class);
	
    public static final String PERSISTENCE_DEFINITION_NAME = "org.gvsig.app.project.ProjectExtent";

	private Envelope extent;
    private String description;

    public ProjectExtent() {
    	this.extent = null;
    	this.description = null;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String string) {
        description = string;
    }

    public Envelope getExtent() {
    	if( this.extent == null ) {
    		try {
				this.extent = geomManager.createEnvelope(Geometry.SUBTYPES.GEOM2D);
			} catch (CreateEnvelopeException e) {
				logger.warn("Can't create envelop.", e);
			}
    	}
    	return this.extent;
    }

    public void setExtent(Envelope envelope) {
        this.extent = envelope;
    }

    /**
     * @deprecated use {link {@link #setExtent(Envelope)}
     */
    public void setExtent(Rectangle2D rectangle2D) {
        try {
			this.extent = geomManager.createEnvelope(
					rectangle2D.getMinX(),
					rectangle2D.getMinY(),
					rectangle2D.getMaxX(),
					rectangle2D.getMaxY(),
					Geometry.SUBTYPES.GEOM2D);
		} catch (CreateEnvelopeException e) {
			logger.warn("Can't create envelope.", e);
			throw new RuntimeException("Can't create envelope.",e);
		}
    }

    public String getEncuadre() {
    	Point lower = extent.getLowerCorner();
    	Point upper = extent.getUpperCorner();
    	return lower.getX() + "," + lower.getY() + "," + upper.getX() + "," + upper.getY(); 
    }

    public void setEncuadre(String encuadre) {
        String[] coords = encuadre.split(",");
        try {
			this.extent = geomManager.createEnvelope(
					Double.parseDouble(coords[0]),
					Double.parseDouble(coords[1]),
					Double.parseDouble(coords[2]),
					Double.parseDouble(coords[3]),
					Geometry.SUBTYPES.GEOM2D);
		} catch (NumberFormatException e) {
			logger.warn("Incorrect format string.", e);
			throw new RuntimeException("Incorrect format string.",e);
		} catch (CreateEnvelopeException e) {
			logger.warn("Can't create envelope.", e);
			throw new RuntimeException("Can't create envelope.",e);
		}
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return description;
    }

    public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.extent = (Envelope) state.get("envelope");
		this.description = state.getString("description");
	}

	public void saveToState(PersistentState state) throws PersistenceException {
        state.set("description", description);
        state.set("envelope", extent);
	}

    /**
     * 
     */
    public static void registerPersistent() {
        
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynStruct definition = manager.getDefinition(PERSISTENCE_DEFINITION_NAME);
        if ( definition == null ){
            definition = manager.addDefinition(
                ProjectExtent.class,
                PERSISTENCE_DEFINITION_NAME,
                PERSISTENCE_DEFINITION_NAME + "  persistence definition",
                    null, 
                    null
            );
            definition.addDynFieldObject("envelope").setMandatory(true).setClassOfValue(Envelope.class);
            definition.addDynFieldString("description").setMandatory(true);
        }        
    }

}
