/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.AddLayer;
import org.gvsig.app.gui.preferencespage.LayerOrderPage;
import org.gvsig.app.project.ProjectPreferences;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Clase que representa una vista del proyecto
 *
 * @author 2004-2005 Fernando Gonz�lez Cort�s
 * @author 2006-2009 Jose Manuel Vivo
 * @author 2005-         Vicente Caballero
 * @author 2009-         Joaquin del Cerro
 * 
 */

public class DefaultViewDocument extends BaseViewDocument {

	private static Logger logger = LoggerFactory.getLogger(DefaultViewDocument.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 676711512482334764L;

	public static final String PERSISTENCE_DEFINITION_NAME = "DefaultViewDocument";
	

	public DefaultViewDocument() {
		this(null);
	}
	
	public DefaultViewDocument(DocumentManager factory) {
		super(factory); 

		ViewPort vp;
	
		ProjectPreferences preferences = (ProjectPreferences) ApplicationLocator.getManager().getPreferences("project");
    	
    	MapContext viewMapContext = new MapContext(
				new ViewPort( preferences.getDefaultProjection() )
		);
		vp = viewMapContext.getViewPort();
		vp.setBackColor(preferences.getDefaultViewBackColor());
		vp.setDistanceUnits(preferences.getDefaultDistanceUnits());
		vp.setDistanceArea(preferences.getDefaultDistanceArea());
		
		/*
		 * Another user preference: the order manager
		 */
		LayerOrderManager lom = getCurrentOrderManager();
		viewMapContext.setOrderManager(lom);
		
		
		/*
		 * Not very elegant but we are forcing meters if projected
		 */
		if (viewMapContext.getProjection().isProjected()) {
		    vp.setMapUnits(1);
		} else {
            vp.setMapUnits(8);
		}
		
		
		this.setMapContext(viewMapContext);
		
		MapContext overviewMapContext = new MapContext(
				new ViewPort( viewMapContext.getProjection() )
		);
		vp = viewMapContext.getViewPort();
		vp.setBackColor(preferences.getDefaultViewBackColor());
		this.setMapOverViewContext(overviewMapContext);
	}
	
	/**
	 * Get current order manager from persistence
	 * 
	 * @return
	 */
	private LayerOrderManager getCurrentOrderManager() {
		
		DynObject props = this.getPluginProperties();
		// current_layer_order_manager
		Object val_obj = null;
		if (props.hasDynValue(LayerOrderPage.PREFERENCES_ID)) {
			val_obj = props.getDynValue(LayerOrderPage.PREFERENCES_ID);
		}
		if (val_obj != null && val_obj instanceof LayerOrderManager) {
			LayerOrderManager lom = (LayerOrderManager) val_obj;
			return lom;
		} else {
			logger.info("No order manager found in persistence.");
			return MapContextLocator.getDefaultOrderManager();
		}
		
	}
	
	private DynObject getPluginProperties() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        return pluginsManager.getPlugin(AddLayer.class).getPluginProperties();
	}
	
	
	public void saveToState(PersistentState state) throws PersistenceException {
		super.saveToState(state);
	}
	
	public void loadFromState(PersistentState state) throws PersistenceException {
		super.loadFromState(state);
	}

	public String exportDocumentAsText() {
		// FIXME: jjdc PersistentManager getStateAsText o similar
//		 PersistenceManager manager = ToolsLocator.getPersistenceManager();
//		 PersistentState state = manager.getState(this);
//		 return manager.getStateAsText(state);
		return null;
	}

	public void setStateFromText(String text) {
		// FIXME: jjdc PersistentManager getStateFromText o similar
//		 PersistenceManager manager = ToolsLocator.getPersistenceManager();
//		 PersistentState state = manager.getStateFromText(text);
//		 this.loadFromState(state);
	}

	
}
