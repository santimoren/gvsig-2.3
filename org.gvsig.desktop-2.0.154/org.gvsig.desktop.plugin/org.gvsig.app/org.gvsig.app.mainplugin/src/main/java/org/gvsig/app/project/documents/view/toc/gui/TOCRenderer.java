/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toc.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SpringLayout;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.app.project.documents.view.toc.TocItemBranch;
import org.gvsig.fmap.mapcontext.layers.FLyrDefault;

/**
 * Renderer que actua sobre el TOC.
 * 
 * @author vcn
 */
public class TOCRenderer extends JPanel implements TreeCellRenderer {

    private static final long serialVersionUID = -6733445768959238193L;
    private JCheckBox check;
    private JLabel label;
    private static int MARGIN  = 100;

    private final Color editingColor;
    
    /**
     * toc background color
     */
    private final Color tocBgColor;

    /**
     * Creates a new TOCRenderer object.
     */
    public TOCRenderer(Color tocBackground) {
        this(Color.RED, tocBackground);
    }

    /**
     * Creates a new TOCRenderer object.
     */
    public TOCRenderer(Color editingColor, Color tocBackground) {
        
        this.editingColor = editingColor;
        this.tocBgColor = tocBackground;

        check = new JCheckBox();
        label = new JLabel();

        SpringLayout theLayout = new SpringLayout();
        this.setLayout(theLayout);

        // Adjust constraints for the text field so it's at
        // (<label's right edge> + 5, 5).
        this.add(check);
        this.add(label);

        theLayout.putConstraint(SpringLayout.WEST, label, 5, SpringLayout.EAST,
            check);
    }
    
    /**
     * Gets the dimension of the node.  
     * @param item
     * @param font
     * @return
     */
    private Dimension getNodeDimension(ITocItem item, Font font) {
    	int textWidth = 0;
        int textHeight = 0;
        if(font != null && item.getLabel() != null) {
        	FontMetrics metrics = this.getFontMetrics(font);
        	textWidth = metrics.stringWidth(item.getLabel());
        	textHeight = metrics.getHeight();
        }
        Dimension itemSize = item.getSize();
        
        int w = (int)Math.max(textWidth, itemSize.getWidth());
        int h = (int)Math.max(textHeight, itemSize.getHeight());
        
        return new Dimension(w + MARGIN, h);
    }

    /**
     * M�todo llamado una vez por cada nodo, y todas las veces que se redibuja
     * en pantalla el TOC.
     * 
     * @param tree
     * @param value
     * @param isSelected
     * @param expanded
     * @param leaf
     * @param row
     * @param hasFocus
     * 
     * @return
     */
    public Component getTreeCellRendererComponent(JTree tree, Object value,
        boolean isSelected, boolean expanded, boolean leaf, int row,
        boolean hasFocus) {

        DefaultMutableTreeNode n = (DefaultMutableTreeNode) value;
        Color foreground = tree.getForeground();
        this.label.setFont(tree.getFont());
        
        this.label.setBackground(this.tocBgColor);
        this.setBackground(this.tocBgColor);

        if (n.getUserObject() instanceof ITocItem) {

            ITocItem item = (ITocItem) n.getUserObject();
            label.setText(item.getLabel());
            Icon icono = item.getIcon();
            if (icono != null) {
                label.setIcon(icono);
            }

            this.validate();
            
            Dimension sizeNode = getNodeDimension(item, tree.getFont());
            
            this.setPreferredSize(sizeNode);

            if (item instanceof TocItemBranch) {
                TocItemBranch branch = (TocItemBranch) item;
                FLyrDefault lyr = (FLyrDefault) branch.getLayer();
                check.setVisible(true);
                check.setSelected(lyr.visibleRequired());
                check.setBackground(this.tocBgColor);
                if (!lyr.isAvailable()) {
                    check.setEnabled(false);
                } else {
                    check.setEnabled(true);
                    if (!lyr.isWithinScale(lyr.getMapContext().getScaleView())) {
                        check.setEnabled(false);
                    }

                    if (lyr.isEditing()) {
                        this.label.setForeground(editingColor);
                    } else {
                        this.label.setForeground(foreground);
                    }
                }
                if (lyr.isActive()) {
                    this.label.setFont(label.getFont().deriveFont(Font.BOLD));
                }
            } else {
                check.setVisible(false);
            }
        }

        if (leaf) {
            // label.setIcon(UIManager.getIcon("Tree.leafIcon"));
        } else if (expanded) {
            // label.setIcon(UIManager.getIcon("Tree.openIcon"));
        } else {
            // label.setIcon(UIManager.getIcon("Tree.closedIcon"));
        }

        return this;
    }

    public Rectangle getCheckBoxBounds() {
        return check.getBounds();
    }

}
