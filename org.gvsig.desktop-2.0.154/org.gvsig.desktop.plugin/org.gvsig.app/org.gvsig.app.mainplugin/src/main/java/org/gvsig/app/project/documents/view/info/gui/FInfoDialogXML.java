/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view.info.gui;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.utils.xmlViewer.XMLViewer;



/**
 * Diálogo utilizado para mostrar la infromación.
 *
 * @author fernando To change the template for this generated type comment go
 * 		   to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * 		   Comments
 */
public class FInfoDialogXML extends XMLViewer implements IWindow {


	public FInfoDialogXML(){
		super();
		setNamesColumn(new String[]{PluginServices.getText(this, "Atributo"), PluginServices.getText(this, "Valor")});
		setSize(500, 375);
	}
	/**
	 * @see com.iver.mdiApp.ui.MDIManager.IWindow#windowActivated()
	 */
	public void viewActivated() {
	}

	/* (non-Javadoc)
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo = new WindowInfo(WindowInfo.MODELESSDIALOG |
				WindowInfo.RESIZABLE | WindowInfo.PALETTE);
		m_viewinfo.setWidth(getWidth()+8);
		m_viewinfo.setHeight(getHeight());
		m_viewinfo.setTitle(PluginServices.getText(this,
				"Identificar_Resultados"));

		return m_viewinfo;
	}

	/**
	 * @see com.iver.mdiApp.ui.MDIManager.SingletonWindow#getWindowModel()
	 */
	public Object getViewModel() {
		return "FInfoDialogXML";
	}
	public Object getWindowProfile() {
		return WindowInfo.PROPERTIES_PROFILE;
	}
}
