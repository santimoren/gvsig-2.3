/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontrol.MapControl;

/**
 * Extensi�n que controla las operaciones de medida realizadas sobre la vista.
 *
 * @author Vicente Caballero Navarro
 */
public class MeasureExtension extends Extension {

    private void registerDistanceUnits() {
        MapContext.addDistanceUnit("Millas_Nauticas", "nmi", 1852);
        MapContext.addDistanceUnit("Decimetros", "dm", 0.1);
    }

    private void registerAreaUnits() {
        MapContext.addAreaUnit("Areas", "a", false, 10);
        MapContext.addAreaUnit("Hectareas", "ha", false, 100);
        MapContext.addAreaUnit("HanegadasV", "hgV", false, 28.8287);
        MapContext.addAreaUnit("HanegadasC", "hgC", false, 80.2467);
        MapContext.addAreaUnit("Decimetros", "dm", true, 0.1);
        MapContext.addAreaUnit("Acres", "acre", false, 63.6149);
        MapContext.addAreaUnit("Millas_Nauticas", "nmi", true, 1852);
    }
    /* (non-Javadoc)
     * @see com.iver.andami.plugins.IExtension#initialize()
     */

    public void initialize() {
        registerDistanceUnits();
        registerAreaUnits();
        IconThemeHelper.registerIcon("action", "view-query-distance", this);
        IconThemeHelper.registerIcon("action", "view-query-area", this);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String s) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        MapControl mapCtrl = view.getMapControl();

        if (s.equals("view-query-distance")) {
            mapCtrl.setTool("medicion");
        } else if (s.equals("view-query-area")) {
            mapCtrl.setTool("area");
        }
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        return true;
//        ViewDocument document = view.getViewDocument();
//        return document.getMapContext().hasActiveVectorLayers();
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        return true;
//        ViewDocument document = view.getViewDocument();
//        MapContext mapa = document.getMapContext();
//        return mapa.getLayers().getLayersCount() > 0;
    }
}
