/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {}  {{Task}}
*/
package org.gvsig.app.gui.preferencespage;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.app.extension.InitializeApplicationExtension;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.tools.dynobject.DynObject;

/**
 * Default {@link SymbolPreferences} implementation based on object attributes.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class AppSymbolPreferences implements SymbolPreferences {
	private static final String SYMBOL_COLOR                   = "defaultSymbolColor";
	private static final String SYMBOL_FILL_COLOR              = "defaultSymbolFillColor";
	private static final String SYMBOL_FILL_COLOR_ALEATORY     = "defaultSymbolFillColorAleatory";
	private static final String SYMBOL_FONT                    = "defaultSymbolFont";
	private static final String SYMBOL_LIBRARY_PATH            = "symbolLibraryPath";
	
	private DynObject           values                         = null;
	
	private String symbolFileExtension = ".gvssym";
	private Color defaultSymbolColor;
	private Color defaultSymbolFillColor;
	private boolean defaultSymbolFillColorAleatory;
	private Font defaultSymbolFont;
	private String symbolLibraryPath;

	/**
	 * The unit that will be used when creating new symbols with Cartographic
	 * support.
	 */
	private int defaultCartographicSupportMeasureUnit = -1;

	/**
	 * The reference system that will be used when creating new symbols with
	 * Cartographic support.
	 */
	private int defaultCartographicSupportReferenceSystem = CartographicSupport.WORLD;

	public AppSymbolPreferences() {
		PluginsManager pluginsManager = (PluginsManager) PluginsLocator.getManager();
		PluginServices plugin = pluginsManager.getPlugin(InitializeApplicationExtension.class);
		values = plugin.getPluginProperties();
		initDefaults();
	}
	
	private void initDefaults() {
		resetDefaultSymbolColor();
		resetDefaultSymbolFillColor();
		resetDefaultSymbolFillColorAleatory();
		resetSymbolLibraryPath();
		resetDefaultSymbolFont();
		
		if(getDefaultSymbolFont() == null)
			setDefaultSymbolFont(defaultSymbolFont);
		if(getDefaultSymbolColor() == null)
			setDefaultSymbolColor(defaultSymbolColor);
		if(getDefaultSymbolFillColor() == null)
			setDefaultSymbolFillColor(defaultSymbolFillColor);
		if(getSymbolLibraryPath() == null)
			setSymbolLibraryPath(symbolLibraryPath);
		setSymbolFileExtension(symbolFileExtension);
		setDefaultCartographicSupportMeasureUnit(defaultCartographicSupportMeasureUnit);
		setDefaultCartographicSupportReferenceSystem(defaultCartographicSupportReferenceSystem);
	}

	public String getSymbolFileExtension() {
		return symbolFileExtension;
	}

	public void setSymbolFileExtension(String symbolFileExtension) {
		this.symbolFileExtension = symbolFileExtension;
	}
	
	public String getSymbolLibraryPath() {
		String spath = (String)values.getDynValue(SYMBOL_LIBRARY_PATH);
                if( spath==null ) {
                    return this.symbolLibraryPath;
                }
                File path = new File(spath);
                if( !path.exists() ) {
                    return this.symbolLibraryPath;
                }
		return spath;
	}

	public void setSymbolLibraryPath(String symbolLibraryPath) {
		values.setDynValue(SYMBOL_LIBRARY_PATH, symbolLibraryPath);
		PluginServices.getPluginServices(this).savePluginProperties();
	}

	public Color getDefaultSymbolColor() {
		return (Color)values.getDynValue(SYMBOL_COLOR);
	}

	public void setDefaultSymbolColor(Color symbolColor) {
		values.setDynValue(SYMBOL_COLOR, symbolColor);
		PluginServices.getPluginServices(this).savePluginProperties();
	}

	public Color getDefaultSymbolFillColor() {
		return (Color)values.getDynValue(SYMBOL_FILL_COLOR);
	}

	public void setDefaultSymbolFillColor(Color symbolFillColor) {
		values.setDynValue(SYMBOL_FILL_COLOR, symbolFillColor);
		PluginServices.getPluginServices(this).savePluginProperties();
	}

	public boolean isDefaultSymbolFillColorAleatory() {
		if(values.getDynValue(SYMBOL_FILL_COLOR_ALEATORY) == null) {
			setDefaultSymbolFillColorAleatory(defaultSymbolFillColorAleatory);
		}
		return (Boolean)values.getDynValue(SYMBOL_FILL_COLOR_ALEATORY);
	}

	public void setDefaultSymbolFillColorAleatory(boolean defaultSymbolFillColorAleatory) {
		values.setDynValue(SYMBOL_FILL_COLOR_ALEATORY, defaultSymbolFillColorAleatory);
		PluginServices.getPluginServices(this).savePluginProperties();
	}

	public Font getDefaultSymbolFont() {
		return (Font)values.getDynValue(SYMBOL_FONT);
	}

	public void setDefaultSymbolFont(Font defaultSymbolFont) {
		values.setDynValue(SYMBOL_FONT, defaultSymbolFont);
		PluginServices.getPluginServices(this).savePluginProperties();
	}


	public int getDefaultCartographicSupportMeasureUnit() {
		return defaultCartographicSupportMeasureUnit;
	}

	public void setDefaultCartographicSupportMeasureUnit(
			int defaultCartographicSupportMeasureUnit) {
		this.defaultCartographicSupportMeasureUnit =
				defaultCartographicSupportMeasureUnit;
	}

	public int getDefaultCartographicSupportReferenceSystem() {
		return defaultCartographicSupportReferenceSystem;
	}

	public void setDefaultCartographicSupportReferenceSystem(
			int defaultCartographicSupportReferenceSystem) {
		this.defaultCartographicSupportReferenceSystem =
				defaultCartographicSupportReferenceSystem;
	}

	public void resetDefaultSymbolColor() {
		defaultSymbolColor = Color.DARK_GRAY;
	}

	public void resetDefaultSymbolFillColor() {
		defaultSymbolFillColor = new Color(60, 235, 235);
	}

	public void resetDefaultSymbolFillColorAleatory() {
		defaultSymbolFillColorAleatory = true;
	}

	public void resetDefaultSymbolFont() {
		defaultSymbolFont = new Font("SansSerif", Font.PLAIN, 14);
	}

	public void resetSymbolLibraryPath() {
		PluginsManager pluginsManager = (PluginsManager) PluginsLocator.getManager();
		PluginServices plugin = pluginsManager.getPlugin(InitializeApplicationExtension.class);
		
		symbolLibraryPath = new File(plugin.getPluginHomeFolder(),"Symbols").getAbsolutePath();
		
	}
}