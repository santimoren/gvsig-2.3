/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelable;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;
import org.gvsig.utils.swing.JComboBox;


/**
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class LabelingManager extends AbstractThemeManagerPage implements ActionListener {
	private static final long serialVersionUID = 856162295985695717L;
//	private static ArrayList<Class<? extends ILabelingStrategyPanel>> installedPanels = new ArrayList<Class<? extends ILabelingStrategyPanel>>();
	private Comparator<Class<?>> comparator=new Comparator<Class<?>>(){

		public int compare(Class<?> o1, Class<?> o2) {
			return o1.getSimpleName().compareTo(o2.getSimpleName());
		}

	};
	private TreeMap<Class<?>, ILabelingStrategyPanel> strategyPanels =
			new TreeMap<Class<?>, ILabelingStrategyPanel>(comparator);

	private JCheckBox chkApplyLabels;
	private ILabelable layer;
	private JPanel content;
	private JPanel pnlNorth;
	private JComboBox cmbStrategy;


	public LabelingManager() {
		super();
		initialize();
	}

	private class LabelingStrategyItem {
		private ILabelingStrategyPanel strategyPanel;

		private LabelingStrategyItem(ILabelingStrategyPanel strategyPanel) {
			this.strategyPanel = strategyPanel;
		}

		public String toString() {
			return strategyPanel.getLabelingStrategyName();
		}

		public boolean equals(Object obj) {
			if (obj instanceof LabelingStrategyItem) {
				LabelingStrategyItem item = (LabelingStrategyItem) obj;
				return this.strategyPanel.getClass().equals(item.strategyPanel.getClass());

			}
			return super.equals(obj);
		}

	}

	 @Override
	    public int getPriority() {
	    	return 600;
	    }

	private void initialize() {
		setLayout(new BorderLayout());
                SymbologySwingManager symbologySwingManager = SymbologySwingLocator.getSwingManager();

                Iterator<ILabelingStrategyPanel> it = symbologySwingManager.getLabelingEditors().iterator();
                while( it.hasNext() ) {
                    ILabelingStrategyPanel pnl = it.next();
                    strategyPanels.put(pnl.getLabelingStrategyClass(), pnl);
		}
		content = new JPanel(new BorderLayout());
		content.setBorder(BorderFactory.createEtchedBorder());
		add(getPnlNorth(), BorderLayout.NORTH);
		add(content, BorderLayout.SOUTH);

	}

	private JPanel getPnlNorth() {
		if (pnlNorth == null) {
			pnlNorth = new JPanel(new BorderLayout(5,5));
			JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEFT));

			aux.add(getChkApplyLabels());
			pnlNorth.add(aux, BorderLayout.NORTH);
			aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
			aux.add(new JLabel(PluginServices.getText(this, "general")+":"));
			aux.add(getCmbStrategy());
			pnlNorth.add(aux, BorderLayout.CENTER);

		}
		return pnlNorth;
	}

	private JComboBox getCmbStrategy() {
		if (cmbStrategy == null) {
			Iterator<ILabelingStrategyPanel> it = strategyPanels.values().iterator();
			final ArrayList<LabelingStrategyItem> aux = new ArrayList<LabelingStrategyItem>();
			while (it.hasNext()) {
				aux.add(new LabelingStrategyItem(it.next()));
			}
			final LabelingStrategyItem items[] = aux.toArray(new LabelingStrategyItem[aux.size()]);

			cmbStrategy = new JComboBox(items) {
				private static final long serialVersionUID = 7506754097091500846L;

				@Override
				public void setSelectedItem(Object anObject) {
					if (anObject == null)
						return;
					if (anObject instanceof ILabelingStrategy) {
						ILabelingStrategy st = (ILabelingStrategy) anObject;
						for (ILabelingStrategyPanel pnl : strategyPanels.values()) {
							if (pnl.getLabelingStrategyClass() != null &&
								pnl.getLabelingStrategyClass().equals(st.getClass())) {
								super.setSelectedItem(new LabelingStrategyItem(pnl));
								return;
							}
						}
					} else {
						super.setSelectedItem(anObject);
					}
				}
			};

			cmbStrategy.setName("CMBMODE");
			cmbStrategy.addActionListener(this);
		}

		return cmbStrategy;
	}

	private JCheckBox getChkApplyLabels() {
		if (chkApplyLabels == null) {
			chkApplyLabels = new JCheckBox(PluginServices.getText(this, "enable_labeling"));
			chkApplyLabels.setName("CHKAPPLYLABELS");
			chkApplyLabels.addActionListener(this);
		}
		return chkApplyLabels;
	}

        /**
         *
         * @deprecated use {#SymbolSwingManger.
         */
	public static void addLabelingStrategy(Class<? extends ILabelingStrategyPanel> iLabelingStrategyPanelClass) {
            SymbologySwingManager symbologySwingManager = SymbologySwingLocator.getSwingManager();
            symbologySwingManager.registerLabelingEditor(iLabelingStrategyPanelClass);
	}

	private void setComponentEnabled(Component c, boolean b) {
		c.setEnabled(b);
	}


	public void setModel(FLayer layer) throws IllegalArgumentException {
		if (layer instanceof ILabelable) {
			// get the labeling strategy
			this.layer = (ILabelable) layer;
			for (ILabelingStrategyPanel p : strategyPanels.values()) {
				p.setModel(layer,((ILabelable) layer).getLabelingStrategy() );
			}

			setComponentEnabled(this, true);
			refreshControls();


			ActionEvent evt = new ActionEvent(chkApplyLabels, 0, null);
			evt.setSource(chkApplyLabels);
			actionPerformed(evt);

			getCmbStrategy().setSelectedItem(this.layer.getLabelingStrategy());
			evt.setSource(getCmbStrategy());
			actionPerformed(evt);
		} else {
			setComponentEnabled(this, false);
		}
	}



	private void refreshControls() {
		if (layer == null) return;

		// enables labeling
		JCheckBox applyLabels = getChkApplyLabels();
		applyLabels.setSelected(layer.isLabeled());
	}




	public void actionPerformed(ActionEvent e) {
		JComponent c = (JComponent)e.getSource();

		if (c.equals(chkApplyLabels)) {
			boolean b = chkApplyLabels.isSelected();
			// enables/disables all components
			getCmbStrategy().setEnabled(b);
			for (int i = 0; i < content.getComponentCount(); i++) {
				Component c1 = content.getComponent(i);
				if (!c1.equals(c))
					setComponentEnabled(c1, b);
			}

		} else if (c.equals(cmbStrategy)) {
			if (c.equals(cmbStrategy)) {
				ILabelingStrategyPanel panel = ((LabelingStrategyItem) cmbStrategy.getSelectedItem()).strategyPanel;
				if (panel!=null) {
					try {
						remove(content);

						content.removeAll();
						content.add((Component) panel);
						add(content, BorderLayout.CENTER);
						actionPerformed(new ActionEvent(chkApplyLabels, 0, null));
						revalidate();
						paintImmediately(getBounds());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	public void acceptAction() {
	    applyAction();
	}

	public void cancelAction() {

	}

	public void applyAction() {
		if (layer != null) { // in other case the layer is not labelable
			ILabelingStrategyPanel panel = ((LabelingStrategyItem) getCmbStrategy().getSelectedItem()).strategyPanel;
			ILabelingStrategy strategy=panel.getLabelingStrategy();
			layer.setLabelingStrategy(strategy);
			layer.setIsLabeled(getChkApplyLabels().isSelected());
		}
	}


	public String getName() {
		return PluginServices.getText(this,"Etiquetados");
	}
}
