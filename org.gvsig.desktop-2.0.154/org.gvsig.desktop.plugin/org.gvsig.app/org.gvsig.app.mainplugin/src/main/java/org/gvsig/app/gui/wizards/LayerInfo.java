/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.wizards;

import java.util.ArrayList;


//	Nodo[] nodos;
public class LayerInfo {
    public String text;
    public String name;
    public boolean queryable;
    private ArrayList srs = new ArrayList();
    
    public ArrayList hijos = new ArrayList();
    public LayerInfo padre;

    /**
     * DOCUMENT ME!
     *
     * @param srs DOCUMENT ME!
     */
    public void addSRS(String srs) {
    	String[] srsArray = srs.split(" ");
    	for (int i = 0; i < srsArray.length; i++){
			this.srs.add(srsArray[i]);
    	}
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ArrayList getSRSs() {
		ArrayList ret = new ArrayList();
		ret.addAll(srs);

        if (padre != null) {
            ret.addAll(padre.getSRSs());
        }

        return ret;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        return text;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
    	try{
	        LayerInfo objeto = (LayerInfo) obj;
			return this.name.equals(objeto.name);
		}catch(ClassCastException e){
			e.printStackTrace();
			return false;
		}catch (NullPointerException e) {
			return false;
		}
    }
}
