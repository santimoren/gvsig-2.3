/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.app.extension.dispose;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dispose.DisposableInfo;
import org.gvsig.tools.dispose.DisposableManager;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An extension to view and manage {@link Disposable} objects which are still
 * bound.
 *
 * TODO: remove this extension and replace with a better one, maybe based on
 * scripting.
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class DisposableManagementExtension extends Extension {

    public static String SHOW_COMMAND = "tools-devel-disposables-show-pendings";
    public static String DISPOSE_ALL_COMMAND = "tools-devel-disposables-free-all";

    private static final Logger LOG
            = LoggerFactory.getLogger(DisposableManagementExtension.class);

    public void initialize() {
        // Nothing to do
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
    }

    public void execute(String actionCommand) {

        if (DISPOSE_ALL_COMMAND.equals(actionCommand)) {
            disposeAll();
        } else {
            if (SHOW_COMMAND.equals(actionCommand)) {
                DisposablesDoList panel = new DisposablesDoList();
                panel.showPanel();
            }
        }
    }

    private void disposeAll() {
        DisposableManager manager;
        manager = ToolsLocator.getDisposableManager();
        try {
            manager.releaseAll();
        } catch (BaseException ex) {
            LOG.error("Error disposing all bound disposable objects", ex);
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return true;
    }

    class DisposablesDoList extends DisposablesDoListLayout {

        private DisposableManager manager;
        private JList disposablesList = null;
        private JTextPane infoTextArea = null;

        public DisposablesDoList() {
            manager = ToolsLocator.getDisposableManager();
            initComponents();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    fillList();
                }
            });
        }

        private void initComponents() {
            this.setPreferredSize(new Dimension(600, 550));
            this.disposablesList = new JList();
            this.infoTextArea = new JTextPane();
            this.infoTextArea.setContentType("text/html");
            JScrollPane listScroller = new JScrollPane(this.disposablesList);
            JScrollPane infoScroller = new JScrollPane(this.infoTextArea);
            this.splitPanel.setLeftComponent(listScroller);
            this.splitPanel.setRightComponent(infoScroller);

            this.closeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    closeWindow();
                }
            });
            this.closeButton.setEnabled(true);
            this.disposeAllButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    disposeAll();
                    refreshList();
                }
            });
            this.disposeAllButton.setEnabled(true);
            this.disposeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    disposeSelecteds();
                    refreshList();
                }
            });
            this.disposeButton.setEnabled(true);
            this.disposablesList.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent arg0) {
                    ListItemSelected();
                }
            });
            this.disposablesList.setEnabled(true);
            this.refreshButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    refreshList();
                }
            });
            this.refreshButton.setEnabled(true);
        }

        private void refreshList() {
            fillList();
        }

        private void ListItemSelected() {
            DisposableInfoItem item = (DisposableInfoItem) disposablesList.getSelectedValue();
            if (item == null) {
                this.infoTextArea.setText("");
            } else {
                this.infoTextArea.setText(item.getAllInformation());
                this.infoTextArea.setCaretPosition(0);
            }
        }

        private void fillList() {
            Set<DisposableInfo> disposables
                    = (Set<DisposableInfo>) manager.getBoundDisposables();

            this.messageLabel.setText("Pending " + disposables.size());
            DefaultListModel listmodel = new DefaultListModel();
            for (Iterator<DisposableInfo> iterator = disposables.iterator(); iterator.hasNext();) {
                DisposableInfo disposableInfo = iterator.next();
                listmodel.addElement(new DisposableInfoItem(disposableInfo));
            }
            this.disposablesList.setModel(listmodel);
            this.infoTextArea.setText("");
        }

        class DisposableInfoItem {

            DisposableInfo disposableInfo = null;

            DisposableInfoItem(DisposableInfo disposableInfo) {
                this.disposableInfo = disposableInfo;
            }

            public Disposable getDisposable() {
                return this.disposableInfo.getDisposable();
            }

            public DisposableInfo getDisposableInfo() {
                return this.disposableInfo;
            }

            public String toString() {
                if (this.disposableInfo == null) {
                    return "(null)";
                }
                Disposable d = this.disposableInfo.getDisposable();
                if (d == null) {
                    return "(null - disposable)";
                }
                String msg = d.getClass().getName() + " - " + d.toString();
                return msg;
            }

            public String getAllInformation() {
                StringBuffer buffer = new StringBuffer();
                Disposable disposable = this.disposableInfo.getDisposable();

                buffer.append("<b>Class</b>: ").append(disposable.getClass().getName()).append("<br>\n");
                buffer.append("<b>References</b>: ").append(this.disposableInfo.getReferencesCount()).append("<br>\n");
                buffer.append("<b>toString</b>: ").append(disposable.toString()).append("<br>\n");

                buffer.append("<b>Binded from (stack)</b>:<br>\n");
                try {
                    StackTraceElement[] stackTrace = disposableInfo
                            .getBindDisposableStackTrace();
                    for (int i = 0; i < stackTrace.length; i++) {
                        buffer.append("&nbsp;&nbsp;").append(stackTrace[i].toString().replaceAll("[<]", "&lt;").replace("[>]", "&gt;")).append("<br>\n");
                    }
                } catch (Exception ex) {
                    buffer.append("<br>\n<br>\nError showing stack.<br>\n").append(ex.getMessage());
                }
                return buffer.toString();
            }
        }

        public void closeWindow() {
            this.setVisible(false);
        }

        public void disposeAll() {
            try {
                manager.releaseAll();
            } catch (BaseException ex) {
                LOG.error("Error disposing all bound disposable objects", ex);
            }
        }

        public void disposeSelecteds() {
            Object[] selection = this.disposablesList.getSelectedValues();
            for (int i = 0; i < selection.length; i++) {
                DisposableInfoItem item = (DisposableInfoItem) selection[i];
                this.manager.release(item.getDisposable());
            }
            refreshList();
        }

        public void showPanel() {
            WindowManager wm = ToolsSwingLocator.getWindowManager();
            wm.showWindow(this, "Disposable do list", WindowManager.MODE.WINDOW);
        }
    }

}
