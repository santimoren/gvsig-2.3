/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.gui;

import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowTransform;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.documents.view.MapOverview;

public class MapOverViewPalette extends JPanel implements IWindow,
    IWindowTransform {

    private static final long serialVersionUID = 3254895082290314502L;
    private MapOverview mov;
    private DefaultViewPanel view;

    public MapOverViewPalette(MapOverview mapOverView, IView view) {
        super();
        mov = mapOverView;
        this.view = (DefaultViewPanel) view;
        initialize();
        this.add(mov);
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        this.setSize(300, 200);
        this.setBackground(java.awt.SystemColor.control);
        this.setBorder(javax.swing.BorderFactory.createLineBorder(
            java.awt.Color.gray, 0));
    }

    public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo =
            new WindowInfo(WindowInfo.ICONIFIABLE | WindowInfo.MODELESSDIALOG
                | WindowInfo.PALETTE);
        m_viewinfo.setTitle(PluginServices.getText(this, "localizador"));
        m_viewinfo.setWidth(mov.getWidth() + 20);
        m_viewinfo.setHeight(mov.getHeight());

        return m_viewinfo;
    }

    public void toPalette() {
        view.toPalette();

    }

    public void restore() {
        view.restore();

    }

    public boolean isPalette() {
        return true;
    }

    public Object getWindowProfile() {
        return WindowInfo.TOOL_PROFILE;
    }

}
