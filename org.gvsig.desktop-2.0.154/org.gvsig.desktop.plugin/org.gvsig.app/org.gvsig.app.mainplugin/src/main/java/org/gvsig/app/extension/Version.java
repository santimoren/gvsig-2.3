/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.util.StringTokenizer;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.creation.MakePluginPackageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * gvSIG application version information.
 * 
 * @author gvSIG Team
 * @version $Id: Version.java 39065 2012-10-16 11:26:12Z jldominguez $
 */
public class Version {

    private static final Logger LOG = LoggerFactory.getLogger(Version.class);

    private org.gvsig.installer.lib.api.Version version;

    @Deprecated
    public static String format() {
        return ApplicationLocator.getManager().getVersion().getFormat();
    }

    @Deprecated
    public static String longFormat() {
        return ApplicationLocator.getManager().getVersion().getLongFormat();
    }

    @Deprecated
    public static String getBuild() {
        return ApplicationLocator.getManager().getVersion().getBuildId();
    }

    /**
     * Constructor.
     * Loads version information from the application installation information.
     */
    public Version() {
        
        PackageInfo pinfo = PluginsLocator.getManager().getPackageInfo(InitializeApplicationExtension.class);
        this.version = pinfo.getVersion();
        LOG.debug("Loaded version information: {}", getLongFormat());
    }

    public int getMinor() {
        return this.version.getMinor();
    }

    public int getMajor() {
        return this.version.getMajor();
    }

    public int getRelease() {
        return this.version.getRevision();
    }

    public String getBuildId() {
        return Integer.toString(this.version.getBuild());
    }

    public String toString() {
        return getLongFormat();
    }

    public String getFormat() {
        return getMajor() + "." + getMinor() + "." + getRelease();
    }

    public String getLongFormat() {
        return version.toString(); 
    }
    
    public org.gvsig.installer.lib.api.Version asInstallerVersion() {
        return version;
    }
}
