/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import javax.swing.JOptionPane;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.toc.gui.FPopupMenu;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extensi�n que a�ade una capa al locator
 *
 */
public class AddToLocatorExtension extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(AddToLocatorExtension.class);

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if (view != null) {
            FLayer[] layers = view.getMapContext().getLayers().getActives();
            return layers != null && layers.length > 0;
        }
        return false;
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
        ProjectManager projectManager = ApplicationLocator.getProjectManager();
        ViewManager viewManager = (ViewManager) projectManager.getDocumentManager(ViewManager.TYPENAME);
        viewManager.addTOCContextAction("layer-addtolocator");
    }

    @Override
    public void execute(String actionCommand) {
        this.execute(actionCommand, null);
    }

    @Override
    public void execute(String command, Object[] args) {
        ApplicationManager application = ApplicationLocator.getManager();
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        if ("layer-addtolocator".equalsIgnoreCase(command)) {
            ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
            if (view == null) {
                return;
            }
            MapContext mapOverViewContext = view.getMapOverViewContext();
            FLayer[] layers = view.getMapContext().getLayers().getActives();
            for (int i = 0; i < layers.length; i++) {
                FLayer layer = layers[i];
                try {
                    mapOverViewContext.getLayers().addLayer(layer.cloneLayer());
                    mapOverViewContext.invalidate();
                    view.setModified(true);
                } catch (Exception ex) {
                    logger.warn("Can't add layer '" + layer.getName() + "' to the overview.", ex);
                    application.messageDialog(
                            i18nManager.getTranslation("_Add_to_locator"),
                            i18nManager.getTranslation("_Cant_add_layer_{0}_to_locator", new String[] {layer.getName()})
                                + "\n\n"
                                + i18nManager.getTranslation("_See_the_error_log_for_more_information"),
                            JOptionPane.WARNING_MESSAGE
                    );
                }
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void initialize() {
        IconThemeHelper.registerIcon("action", "layer-addtolocator", this);
    }
}
