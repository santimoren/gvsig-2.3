/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.command;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.tools.undo.RedoException;
import org.gvsig.tools.undo.UndoException;
import org.gvsig.tools.undo.UndoRedoStack;
import org.gvsig.tools.undo.command.Command;


@SuppressWarnings("serial")
public class CommandTableModel extends AbstractTableModel{
    private static final Logger LOG = 
        LoggerFactory.getLogger(CommandTableModel.class);
    
    private UndoRedoStack undoRedoStack;
        
    public CommandTableModel(UndoRedoStack undoRedoStack) {
        this.undoRedoStack = undoRedoStack;
	}

	public int getPos() {
		if ((undoRedoStack == null) || undoRedoStack.getUndoInfos() == null){
		    return 0;
		}
	    return undoRedoStack.getUndoInfos().size() - 1;
	}
	
	public int getColumnCount() {
		return 1;
	}
	
	public int getRowCount() {
		if ((undoRedoStack == null) || undoRedoStack.getUndoInfos() == null || undoRedoStack.getRedoInfos() == null){
		    return 0;
		}
		return undoRedoStack.getRedoInfos().size() + undoRedoStack.getUndoInfos().size();
	}
	
	@SuppressWarnings("unchecked")
    public Object getValueAt(int i, int j) {
		Command[] undos=(Command[])undoRedoStack.getUndoInfos().toArray(new Command[0]);
		Command[] redos=(Command[])undoRedoStack.getRedoInfos().toArray(new Command[0]);
		if (i<undos.length){
			//System.out.println("undo i=" + i + " index=" + (undos.length-1-i));
			return undos[undos.length-1-i];
		}else{
			//System.out.println("redo i=" + i + " index=" + (i-undos.length));
			return redos[i-undos.length];
		}
	}
	
	public void setPos(int newpos) {
		try {
		    int currentPos = getPos();
			if (newpos > currentPos) {
				undoRedoStack.redo(newpos - currentPos);
			}else if (newpos < getPos()) {
				undoRedoStack.undo(currentPos - newpos);
			}
			
		} catch (RedoException e) {
			LOG.error("Error executing the command", e);
		} catch (UndoException e) {
		    LOG.error("Error executing the command", e);
		}
	}
}
