/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.legend.edition.gui;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class PreviewIcon extends JPanel {
	private ImageIcon icon;
	private boolean isSelected;

	/**
	 *
	 */
	public PreviewIcon() {
		super();
			initialize();
	}


	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
        this.setSize(70, 52);

	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (icon == null)
			return;

		Graphics2D g2 = (Graphics2D) g;
		//g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g.setClip(0,0,getWidth(), getHeight());
		//Rectangle r = g.getClipBounds(); //getClipRect();
		if (isSelected)
			g2.setColor(Color.blue);
		else
			g2.setColor(Color.white);

		//g2.setColor(color);
		g2.fillRect(0,0,getWidth(),getHeight());
		g2.drawImage(icon.getImage(),0,0,this);

	}
	public void setSelected(boolean b) {
		isSelected=b;
	}
	public void setIcon(ImageIcon icon) {
		this.icon=icon;
		repaint();
	}
	public ImageIcon getIcon() {
		return icon;
	}

	/* (non-Javadoc)
	 * @see java.awt.Component#isShowing()
	 */
	public boolean isShowing() {
		return true; // super.isShowing();
	}


	public boolean isSelected() {
		return isSelected;
	}


	public synchronized void addMouseListener(MouseListener arg0) {
		super.addMouseListener(arg0);

	}
}  //  @jve:visual-info  decl-index=0 visual-constraint="10,10"
