/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.gvsig.tools.swing.icontheme.IconThemeManager;

public class IconThemeDevelTool {

	public void showDefaultIconTheme() {
		String html = "<html>\n<body>\n"+ this.getIconThemeInformationByGroup()+"</body>\n</html>\n";
		InfoPanel.save2file("icontheme-report", html);
		InfoPanel.showPanel("Icon theme information", WindowManager.MODE.WINDOW, html);
	}

	public void showDefaultIconThemeByPlugin() {
		String html = "<html>\n<body>\n"+ this.getIconThemeInformationByPlugin()+"</body>\n</html>\n";
		InfoPanel.save2file("icontheme-report", html);
		InfoPanel.showPanel("Icon theme information", WindowManager.MODE.WINDOW, html);
	}

	private String getIconThemeInformationByPlugin() {
		IconThemeManager manager = ToolsSwingLocator.getIconThemeManager();
		IconTheme theme = manager.getDefault();
		
		List<IconTheme.Icon>themeIcons = new ArrayList<IconTheme.Icon>();
		Iterator<IconTheme.Icon> themeIconsIt = theme.getThemeIcons();
		while (themeIconsIt.hasNext()) {
			themeIcons.add(themeIconsIt.next());
		}
		Collections.sort(themeIcons, new Comparator<IconTheme.Icon>() {
			public int compare(IconTheme.Icon o1, IconTheme.Icon o2) {
				String s1 = String.format("%s:%s:%s", o1.getProviderName(), o1.getGroup(), o1.getName());
				String s2 = String.format("%s:%s:%s", o2.getProviderName(), o2.getGroup(), o2.getName());
				return s1.compareTo(s2);
			}
		});
		themeIconsIt = themeIcons.iterator();
		return getIconThemeInformation(theme, themeIconsIt);
	}
	
	public String getIconThemeInformationOfPlugin(String pluginName) {
		if( pluginName == null ) {
			return "";
		}
		IconThemeManager manager = ToolsSwingLocator.getIconThemeManager();
		IconTheme theme = manager.getDefault();
		
		List<IconTheme.Icon>themeIcons = new ArrayList<IconTheme.Icon>();
		Iterator<IconTheme.Icon> themeIconsIt = theme.getThemeIcons();
		while (themeIconsIt.hasNext()) {
			IconTheme.Icon iconTheme = (IconTheme.Icon) themeIconsIt.next();
			if( !pluginName.equalsIgnoreCase(iconTheme.getProviderName()) ) {
				continue;
			}
			themeIcons.add(iconTheme);
		}
		Collections.sort(themeIcons, new Comparator<IconTheme.Icon>() {
			public int compare(IconTheme.Icon o1, IconTheme.Icon o2) {
				String s1 = String.format("%s:%s:%s", o1.getProviderName(), o1.getGroup(), o1.getName());
				String s2 = String.format("%s:%s:%s", o2.getProviderName(), o2.getGroup(), o2.getName());
				return s1.compareTo(s2);
			}
		});
		themeIconsIt = themeIcons.iterator();
		return getIconThemeInformation(theme, themeIconsIt);
	}
	
	private String getIconThemeInformationByGroup() {
		IconThemeManager manager = ToolsSwingLocator.getIconThemeManager();
		IconTheme theme = manager.getDefault();
		
		List<IconTheme.Icon>themeIcons = new ArrayList<IconTheme.Icon>();
		Iterator<IconTheme.Icon> themeIconsIt = theme.getThemeIcons();
		while (themeIconsIt.hasNext()) {
			themeIcons.add(themeIconsIt.next());
		}
		Collections.sort(themeIcons, new Comparator<IconTheme.Icon>() {
			public int compare(IconTheme.Icon o1, IconTheme.Icon o2) {
				String s1 = String.format("%s:%s:%s", o1.getGroup(), o1.getName(), o1.getProviderName());
				String s2 = String.format("%s:%s:%s", o2.getGroup(), o2.getName(), o2.getProviderName());
				return s1.compareTo(s2);
			}
		});
		themeIconsIt = themeIcons.iterator();
		return getIconThemeInformation(theme,themeIconsIt);
	}

	private String getIconThemeInformation(IconTheme theme, Iterator<IconTheme.Icon> themeIconsIt) {
		Set<String> actionIcons = new HashSet<String>();
		Iterator<ActionInfo> actions = PluginsLocator.getActionInfoManager().getActions(); 
		while (actions.hasNext()) {
			ActionInfo action = actions.next();
			if( action.getIconName()!=null ) {
				actionIcons.add(action.getIconName());
			}
		}
		Set<String> tangoIconNames = getTangoIconNames();
		
		StringBuffer buffer = new StringBuffer();

		String warning_open = "<b>";
		String warning_close = "</b>";
		
		String error_open = "<b><font color=\"red\">";
		String error_close = "</font></b>";

		String tangoicon_open = "<b><font color=\"green\">";
		String tangoicon_close = "</font></b>";
		
		String previousPluginName = null;

		buffer.append("<div>\n");
		buffer.append("<h2>Icon theme information</h2>\n");
		buffer.append("<br>\n");
		buffer.append("Theme: ");
		buffer.append(theme.getName());
		buffer.append("<br>\n");
		buffer.append("Description: ");
		buffer.append(theme.getDescription());
		buffer.append("<br>\n");

		buffer.append("<table border=\"0\">\n");
		buffer.append("  <tr>\n");
		buffer.append("    <td>Preview</td>\n");
		buffer.append("    <td>Group</td>\n");
		buffer.append("    <td>Name</td>\n");
		buffer.append("    <td>Provider/Plugin</td>\n");
		buffer.append("    <td>Resource</td>\n");
		buffer.append("  </tr>\n");
		
		
		while (themeIconsIt.hasNext()) {
			IconTheme.Icon themeIcon = themeIconsIt.next();
			if( previousPluginName!=null && !themeIcon.getProviderName().equals(previousPluginName) ) {
				buffer
					.append("  <tr>\n")
					.append("    <td colspan=\"4\"><hr></td>\n")
					.append("  </tr>\n");
				previousPluginName = themeIcon.getProviderName();
			}
			
			buffer.append("  <tr valign=\"top\">\n");
			
			// Preview			
			if( themeIcon.getLabel().length()>0 && themeIcon.getImageIcon()!=null ) {
				try {
					ImageIcon img = themeIcon.getImageIcon();
					if( img.getIconHeight() > 48 || img.getIconWidth() > 48 ) {
						buffer
							.append("    <td nowrap>")
							.append("<img width=\"48\" height=\"48\" src=\"")
							.append(themeIcon.getLabel())
							.append("\">(*)</td>");
					} else {
						buffer
							.append("    <td>")
							.append("<img src=\"")
							.append(themeIcon.getLabel())
							.append("\"></td>");
					}
				} catch(Exception ex) {
					buffer
						.append("    <td>")
						.append("<img src=\"")
						.append(themeIcon.getLabel())
						.append("\"></td>");
				}
			} else {
				buffer.append("    <td></td>");
			}
		
			// Group
			if( themeIcon.getGroup()==null ) {
				buffer.append("    <td title=\"missing recomended group\">")
					.append(warning_open)
					.append(themeIcon.getGroup())
					.append(warning_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(themeIcon.getGroup())
					.append("</td>\n");
			}

			// Name
			if( "action".equals(themeIcon.getGroup()) && !actionIcons.contains(themeIcon.getName())) {
				buffer.append("    <td title=\"Don't exists an action associated to this icon\">")
					.append(error_open)
					.append(themeIcon.getName())
					.append(error_close)
					.append("</td>\n");
			} else if( !themeIcon.getName().contains("-") ) {
				buffer.append("    <td title=\"Name don't have recomended format\">")
					.append(warning_open)
					.append(themeIcon.getName())
					.append(warning_close)
					.append("</td>\n");
			} else if( tangoIconNames.contains(themeIcon.getName()) ){
				buffer.append("    <td>")
					.append(tangoicon_open)
					.append(themeIcon.getName())
					.append(tangoicon_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(themeIcon.getName())
					.append("</td>\n");
			}

			// Plugin
			if( themeIcon.getProviderName()== null ) {
				buffer.append("    <td title=\"missing recomended associated plugin\">")
					.append(warning_open)
					.append(themeIcon.getProviderName())
					.append(warning_close)
					.append("</td>\n");
				
			}  else {
				buffer.append("    <td>")
					.append(themeIcon.getProviderName())
					.append("</td>\n");
			}			
			
			// Resource
			if( themeIcon.getLabel().length()<1 ) {
				buffer.append("    <td title=\"Resource icon not specified\">")
					.append(error_open)
					.append("(missing)")
					.append(error_close)
					.append("</td>\n");
			} else if( themeIcon.getImageIcon()==null ) {
				buffer.append("    <td title=\"Can't locate icon\">")
					.append(error_open)
					.append(themeIcon.getLabel())
					.append(error_close)
					.append("</td>\n");
			} else {
				buffer.append("    <td>")
					.append(themeIcon.getLabel())
					.append("</td>\n");

			}
			buffer.append("  </tr>\n");
		}
		buffer.append("</table>\n");
		buffer.append("</div>\n");

		return buffer.toString();
	}

	private Set<String> getTangoIconNames() {
		String[] iconNames = new String[] {
				"address-book-new",
				"application-exit",
				"appointment-new",
				"call-start",
				"call-stop",
				"contact-new",
				"document-new",
				"document-open",
				"document-open-recent",
				"document-page-setup",
				"document-print",
				"document-print-preview",
				"document-properties",
				"document-revert",
				"document-save",
				"document-save-as",
				"document-send",
				"edit-clear",
				"edit-copy",
				"edit-cut",
				"edit-delete",
				"edit-find",
				"edit-find-replace",
				"edit-paste",
				"edit-redo",
				"edit-select-all",
				"edit-undo",
				"folder-new",
				"format-indent-less",
				"format-indent-more",
				"format-justify-center",
				"format-justify-fill",
				"format-justify-left",
				"format-justify-right",
				"format-text-direction-ltr",
				"format-text-direction-rtl",
				"format-text-bold",
				"format-text-italic",
				"format-text-underline",
				"format-text-strikethrough",
				"go-bottom",
				"go-down",
				"go-first",
				"go-home",
				"go-jump",
				"go-last",
				"go-next",
				"go-previous",
				"go-top",
				"go-up",
				"help-about",
				"help-contents",
				"help-faq",
				"insert-image",
				"insert-link",
				"insert-object",
				"insert-text",
				"list-add",
				"list-remove",
				"mail-forward",
				"mail-mark-important",
				"mail-mark-junk",
				"mail-mark-notjunk",
				"mail-mark-read",
				"mail-mark-unread",
				"mail-message-new",
				"mail-reply-all",
				"mail-reply-sender",
				"mail-send",
				"mail-send-receive",
				"media-eject",
				"media-playback-pause",
				"media-playback-start",
				"media-playback-stop",
				"media-record",
				"media-seek-backward",
				"media-seek-forward",
				"media-skip-backward",
				"media-skip-forward",
				"object-flip-horizontal",
				"object-flip-vertical",
				"object-rotate-left",
				"object-rotate-right",
				"process-stop",
				"system-lock-screen",
				"system-log-out",
				"system-run",
				"system-search",
				"system-reboot",
				"system-shutdown",
				"tools-check-spelling",
				"view-fullscreen",
				"view-refresh",
				"view-restore",
				"view-sort-ascending",
				"view-sort-descending",
				"window-close",
				"window-new",
				"zoom-fit-best",
				"zoom-in",
				"zoom-original",
				"zoom-out",
				"process-working",
				"accessories-calculator",
				"accessories-character-map",
				"accessories-dictionary",
				"accessories-text-editor",
				"help-browser",
				"multimedia-volume-control",
				"preferences-desktop-accessibility",
				"preferences-desktop-font",
				"preferences-desktop-keyboard",
				"preferences-desktop-locale",
				"preferences-desktop-multimedia",
				"preferences-desktop-screensaver",
				"preferences-desktop-theme",
				"preferences-desktop-wallpaper",
				"system-file-manager",
				"system-software-install",
				"system-software-update",
				"utilities-system-monitor",
				"utilities-terminal",
				"applications-accessories",
				"applications-development",
				"applications-engineering",
				"applications-games",
				"applications-graphics",
				"applications-internet",
				"applications-multimedia",
				"applications-office",
				"applications-other",
				"applications-science",
				"applications-system",
				"applications-utilities",
				"preferences-desktop",
				"preferences-desktop-peripherals",
				"preferences-desktop-personal",
				"preferences-other",
				"preferences-system",
				"preferences-system-network",
				"system-help",
				"audio-card",
				"audio-input-microphone",
				"battery",
				"camera-photo",
				"camera-video",
				"camera-web",
				"computer",
				"drive-harddisk",
				"drive-optical",
				"drive-removable-media",
				"input-gaming",
				"input-keyboard",
				"input-mouse",
				"input-tablet",
				"media-flash",
				"media-floppy",
				"media-optical",
				"media-tape",
				"modem",
				"multimedia-player",
				"network-wired",
				"network-wireless",
				"pda",
				"phone",
				"printer",
				"scanner",
				"video-display",
				"emblem-default",
				"emblem-documents",
				"emblem-downloads",
				"emblem-favorite",
				"emblem-important",
				"emblem-mail",
				"emblem-photos",
				"emblem-readonly",
				"emblem-shared",
				"emblem-symbolic-link",
				"emblem-synchronized",
				"emblem-system",
				"emblem-unreadable",
				"face-angel",
				"face-angry",
				"face-cool",
				"face-crying",
				"face-devilish",
				"face-embarrassed",
				"face-kiss",
				"face-laugh",
				"face-monkey",
				"face-plain",
				"face-raspberry",
				"face-sad",
				"face-sick",
				"face-smile",
				"face-smile-big",
				"face-smirk",
				"face-surprise",
				"face-tired",
				"face-uncertain",
				"face-wink",
				"face-worried",
				"flag-aa",
				"application-x-executable",
				"audio-x-generic",
				"font-x-generic",
				"image-x-generic",
				"package-x-generic",
				"text-html",
				"text-x-generic",
				"text-x-generic-template",
				"text-x-script",
				"video-x-generic",
				"x-office-address-book",
				"x-office-calendar",
				"x-office-document",
				"x-office-presentation",
				"x-office-spreadsheet",
				"folder",
				"folder-remote",
				"network-server",
				"network-workgroup",
				"start-here",
				"user-bookmarks",
				"user-desktop",
				"user-home",
				"user-trash",
				"appointment-missed",
				"appointment-soon",
				"audio-volume-high",
				"audio-volume-low",
				"audio-volume-medium",
				"audio-volume-muted",
				"battery-caution",
				"battery-low",
				"dialog-error",
				"dialog-information",
				"dialog-password",
				"dialog-question",
				"dialog-warning",
				"folder-drag-accept",
				"folder-open",
				"folder-visiting",
				"image-loading",
				"image-missing",
				"mail-attachment",
				"mail-unread",
				"mail-read",
				"mail-replied",
				"mail-signed",
				"mail-signed-verified",
				"media-playlist-repeat",
				"media-playlist-shuffle",
				"network-error",
				"network-idle",
				"network-offline",
				"network-receive",
				"network-transmit",
				"network-transmit-receive",
				"printer-error",
				"printer-printing",
				"security-high",
				"security-medium",
				"security-low",
				"software-update-available",
				"software-update-urgent",
				"sync-error",
				"sync-synchronizing",
				"task-due",
				"task-past-due",
				"user-available",
				"user-away",
				"user-idle",
				"user-offline",
				"user-trash-full",
				"weather-clear",
				"weather-clear-night",
				"weather-few-clouds",
				"weather-few-clouds-night",
				"weather-fog",
				"weather-overcast",
				"weather-severe-alert",
				"weather-showers",
				"weather-showers-scattered",
				"weather-snow",
				"weather-storm"
		};
		Set<String> icons = new HashSet<String>();
		for (int i = 0; i < iconNames.length; i++) {
			icons.add(iconNames[i]);
		}
		return icons;
	}

}
