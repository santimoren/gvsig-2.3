/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.develtools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.Launcher.PluginMenuItem;
import org.gvsig.andami.actioninfo.ActionInfo;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class MenusDevelTool {
	public void showAllMenus() {
		String html = "<html>\n<body>\n"+ this.getAllMenus()+"</body>\n</html>\n";
		InfoPanel.save2file("menus-report", html);
		InfoPanel.showPanel("Menu information", WindowManager.MODE.WINDOW, html);
	}
	public void showAllMenusByPlugin() {
		String html = "<html>\n<body>\n"+ this.getAllMenusByPlugin()+"</body>\n</html>\n";
		InfoPanel.save2file("menus-report-sorted-by-plugin", html);
		InfoPanel.showPanel("Menu by plugin information", WindowManager.MODE.WINDOW, html);
	}
	
	public String getAllMenus() {
		Launcher launcher = Launcher.getInstance();
		List<PluginMenuItem> menuItems = launcher.getPluginMenuItems();
		
		return this.getAllMenus(menuItems);
	}
	
	public String getMenusOfPlugin(String pluginName) {
		Launcher launcher = Launcher.getInstance();
		List<PluginMenuItem> menuItems = launcher.getPluginMenuItems();
		List<PluginMenuItem> pluginMenus = new ArrayList<Launcher.PluginMenuItem>();
		
		for (PluginMenuItem pluginMenuItem : menuItems) {
			if( pluginName.equalsIgnoreCase(pluginMenuItem.getPluginName())) {
				pluginMenus.add(pluginMenuItem);
			}
		}
		if( pluginMenus.isEmpty() ) {
			return "";
		}
		return this.getAllMenus(pluginMenus);
	}
	

	public String getAllMenusByPlugin() {
		Launcher launcher = Launcher.getInstance();
		List<PluginMenuItem> menuItems = launcher.getPluginMenuItems();
		
		Collections.sort(menuItems, new Comparator<PluginMenuItem>() {
			public int compare(PluginMenuItem o1, PluginMenuItem o2) {
				String s1 = String.format("%s:%012d:%s", o1.getPluginName(), o1.getPosition(), o1.getText());
				String s2 = String.format("%s:%012d:%s", o2.getPluginName(), o2.getPosition(), o2.getText());
				return s1.compareTo(s2);
			}
		});
		return this.getAllMenus(menuItems);
	}
	
	private String getAllMenus(List<PluginMenuItem> menuItems) {
		StringBuffer buffer = new StringBuffer();

//		String warning_open = "<b>";
//		String warning_close = "</b>";
		
		String error_open = "<b><font color=\"red\">";
		String error_close = "</font></b>";
		
		buffer.append("<div>\n");
		buffer.append("<h2>Menu information </h2>\n");
		buffer.append("<br>\n");
		
		buffer.append("<table border=\"0\">\n");
		buffer.append("  <tr>\n");
		buffer.append("    <td>Position</td>\n");
		buffer.append("    <td>Text</td>\n");
		buffer.append("    <td>Action</td>\n");
		buffer.append("    <td>Plugin</td>\n");
		buffer.append("  </tr>\n");

		
		for (PluginMenuItem menu : menuItems) {
			buffer.append("  <tr valign=\"top\">\n");
			buffer
				.append("    <td>")
				.append(formatPosition( menu.getPosition()))
				.append("</td>\n");			
			buffer
				.append("    <td>")
				.append(menu.getText())
				.append("</td>\n");		
			if( menu.isParent() ) {
				buffer
					.append("    <td>")
					.append("----")
					.append("</td>\n");
				buffer
					.append("    <td>")
					.append(menu.getPluginName())
					.append("</td>\n");			
			} else {
				ActionInfo action = menu.getAction();
				if( action == null ) {
					buffer
						.append("    <td>")
						.append(error_open)
						.append(menu.getName())
						.append(error_close)
						.append("</td>\n");
					buffer
						.append("    <td>")
						.append(menu.getPluginName())
						.append("</td>\n");			
				} else {
					buffer
						.append("    <td>")
						.append(action.getName())
						.append("</td>\n");
					buffer
						.append("    <td>")
						.append(action.getPluginName())
						.append("</td>\n");			
				}
			}
			buffer.append("  </tr>\n");
		}
		buffer.append("</table>\n");
		buffer.append("</div>\n");

		return buffer.toString();
	}
	
	static String formatPosition(long position) {
		String pos = String.format("%012d",position);
		if( pos.length() != 12) {
			return pos;
		}
		String posf = pos.substring(0, 5) + "-" +
			pos.substring(5,8) + "-" +
			pos.substring(8,11) + "-" +
			pos.substring(11);
		return posf;
	}
}
