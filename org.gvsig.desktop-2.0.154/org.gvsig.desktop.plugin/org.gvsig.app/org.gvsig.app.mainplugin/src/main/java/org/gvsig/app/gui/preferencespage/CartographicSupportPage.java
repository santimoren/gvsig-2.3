/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.preferencespage;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.BasicSymbologyExtension;
import org.gvsig.app.gui.JComboBoxUnits;
import org.gvsig.app.gui.styling.JComboBoxUnitsReferenceSystem;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.impl.CartographicSupportToolkit;
import org.gvsig.tools.dynobject.DynObject;


/**
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class CartographicSupportPage extends AbstractPreferencePage {
    
	private static final String DefaultMeasureUnitKey = "default_measure_units";
	private static final String DefaultUnitReferenceSystemKey = "default_measure_units_reference_system";
	
	private JComboBoxUnits cmbUnits;
	private JComboBoxUnitsReferenceSystem cmbReferenceSystem;
	
	public CartographicSupportPage() {
		super();
		initialize();
	}
	
	private void initialize() {
		addComponent(PluginServices.getText(this, "_Default_measure_units"),
				cmbUnits = new JComboBoxUnits(true));
		addComponent(PluginServices.getText(this, "_Default_measure_units_reference_system"),
				cmbReferenceSystem = new JComboBoxUnitsReferenceSystem());
		addComponent(new JSeparator(JSeparator.HORIZONTAL));
	}

	public void setChangesApplied() {
		setChanged(false);
	}

	public String getID() {
		return getClass().getName();
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	public JPanel getPanel() {
		return this;
	}

	public String getTitle() {
		return Messages.getText("_Cartographic_support_panel_title");
	}

	public void initializeValues() {
	    
	    DynObject props = this.getPluginProperties();
	    int val = 0;
	    Object val_obj = null;

	    val_obj = props.getDynValue(CartographicSupportPage.DefaultMeasureUnitKey);
	    if (val_obj != null) {
	        val = ((Integer) val_obj).intValue(); 
	        cmbUnits.setSelectedUnitIndex(val);
	    } 
	    
	    val_obj = props.getDynValue(CartographicSupportPage.DefaultUnitReferenceSystemKey);
	    if (val_obj != null) {
	        val = ((Integer) val_obj).intValue(); 
	        cmbReferenceSystem.setSelectedIndex(val);
	    }
	}

	public void initializeDefaults() {
        cmbUnits.setSelectedUnitIndex(CartographicSupportToolkit.DefaultMeasureUnit);
        cmbReferenceSystem.setSelectedIndex(CartographicSupportToolkit.DefaultReferenceSystem);
	}
	
	public void storeValues() throws StoreException {
	    
	    DynObject props = this.getPluginProperties();
        props.setDynValue(CartographicSupportPage.DefaultMeasureUnitKey,
            new Integer(cmbUnits.getSelectedUnitIndex()));
        props.setDynValue(CartographicSupportPage.DefaultUnitReferenceSystemKey,
            new Integer(cmbReferenceSystem.getSelectedIndex()));
	}

	public boolean isValueChanged() {
		return super.hasChanged();
	}
	
	private DynObject getPluginProperties() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        return pluginsManager.getPlugin(BasicSymbologyExtension.class).getPluginProperties();
	}

}
