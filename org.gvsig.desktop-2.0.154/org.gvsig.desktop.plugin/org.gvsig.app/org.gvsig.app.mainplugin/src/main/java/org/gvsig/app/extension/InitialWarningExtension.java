/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.Color;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.border.TitledBorder;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.WindowInfo;

public class InitialWarningExtension extends Extension {

    private DlgWaring dlgWarning;

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public void initialize() {

        if (isEnabled() && isVisible()) {
            dlgWarning = new DlgWaring();
            dlgWarning.setText(PluginServices.getText(this, "initial_warning"));
            dlgWarning.setVisible(true);
        }

    }

    @Override
    public void execute(String actionCommand) {

    }

    private class DlgWaring extends JWindow {

        private JButton btnOk = null;
        private final JCheckBox chkBoxShowAgain = null;
        private JLabel lblText = null;
        private JPanel panel = null;
        private JPanel panel2;

        /**
         * This is the default constructor
         */
        public DlgWaring() {
            super((Window) PluginServices.getMainFrame());
            Window window = (Window) PluginServices.getMainFrame();
            setLocation(window.getWidth() / 2 + window.getX() - 150, window.getHeight() / 2 + window.getY() - 150);
            initialize();
            this.setAlwaysOnTop(true);
        }

        public void setText(String string) {
            lblText.setText(string);
        }

        /**
         * This method initializes this
         *
         * @return void
         */
        private void initialize() {
            panel = new JPanel();
            panel.setBackground(Color.black);
            panel.setLayout(null);
            panel.setBounds(0, 0, 300, 300);
            panel2 = new JPanel();
            panel2.setLayout(null);
            panel2.setBounds(2, 2, 298, 298);
            lblText = new JLabel();
            lblText.setBounds(15, 15, 271, 200);
            lblText.setText("JLabel");
            lblText.setBorder(new TitledBorder(PluginServices.getText(this, "warning")));
            panel2.add(lblText);
            panel2.setSize((int) (panel.getSize().getWidth() - 4), (int) (panel.getSize().getHeight() - 4));
            panel.add(panel2);
            this.setLayout(null);
            this.setSize(300, 300);
            this.add(getBtnOk(), null);
            this.add(panel, null);
        }

        /**
         * This method initializes btnOk
         *
         * @return javax.swing.JButton
         */
        private JButton getBtnOk() {
            if (btnOk == null) {
                btnOk = new JButton();
                btnOk.setBounds(100, 250, 100, 20);
                btnOk.setText(PluginServices.getText(this, "aceptar"));
                btnOk.addActionListener(new java.awt.event.ActionListener() {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        dlgWarning.setVisible(false);
                        dlgWarning.dispose();
                        //						PluginServices.getMDIManager().closeWindow(dlgWarning);
                    }
                });
            }
            return btnOk;
        }

        public WindowInfo getWindowInfo() {
            WindowInfo vi = new WindowInfo(WindowInfo.MODALDIALOG);
            vi.setWidth(300 + 8);
            vi.setHeight(250);
            vi.setTitle(PluginServices.getText(this, "warning"));
            return vi;
        }

    }

}
