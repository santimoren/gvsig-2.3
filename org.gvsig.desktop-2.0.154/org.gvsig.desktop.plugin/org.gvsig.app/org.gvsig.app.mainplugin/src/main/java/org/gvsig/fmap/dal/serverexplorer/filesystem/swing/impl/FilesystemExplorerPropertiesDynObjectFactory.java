
package org.gvsig.fmap.dal.serverexplorer.filesystem.swing.impl;

import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanel;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerPropertiesPanelFactory;
import org.gvsig.tools.dynobject.DynObject;


public class FilesystemExplorerPropertiesDynObjectFactory implements FilesystemExplorerPropertiesPanelFactory {

    @Override
    public FilesystemExplorerPropertiesPanel create(DynObject parameters) {
        return new FilesystemExplorerPropertiesDynObjectPanel(parameters);
    }

    @Override
    public boolean canBeApplied(DynObject parameters) {
        return true;
    }

    @Override
    public int getPriority() {
        return 0;
    }
    
}
