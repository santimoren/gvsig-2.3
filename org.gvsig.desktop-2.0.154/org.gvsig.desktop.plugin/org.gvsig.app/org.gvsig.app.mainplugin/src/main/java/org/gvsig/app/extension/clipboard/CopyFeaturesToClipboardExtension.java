/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension.clipboard;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.clipboard.util.FeatureTextUtils;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.i18n.Messages;


public class CopyFeaturesToClipboardExtension extends Extension {

    private static Logger logger = LoggerFactory.getLogger(
    		CopyFeaturesToClipboardExtension.class);

	public void initialize() {

		IconThemeHelper.registerIcon("action", "layer-modify-clipboard-copy", this);
	}

	public void execute(String actionCommand) {

		if (actionCommand.compareToIgnoreCase("layer-modify-clipboard-copy") != 0) {
			return;
		}

		IWindow actw = actWin();

		if (actw instanceof IView) {

			IView vw = (IView) actw;
			FLayer[] act_lyr = vw.getMapControl().getMapContext().getLayers().getActives();
			if (act_lyr == null || act_lyr.length != 1
					|| !(act_lyr[0] instanceof FLyrVect)) {

			} else {

				int usr_opt = JOptionPane.showConfirmDialog(
						ApplicationLocator.getManager().getRootComponent(),
						Messages.getText("_Include_alphanumeric_attributes_Question"),
						Messages.getText("_Copy_selected_features_to_clipboard"),
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE);


				if (usr_opt == JOptionPane.CANCEL_OPTION) {
					return;
				}

				FLyrVect vect = (FLyrVect) act_lyr[0];
				FeatureSelection sele = null;
				FeatureStore fsto = null;
				try {
					fsto = vect.getFeatureStore();
					sele = (FeatureSelection) fsto.getSelection();

					StringBuilder strb = FeatureTextUtils.toString(
							sele, fsto.getDefaultFeatureType(),
							usr_opt == JOptionPane.YES_OPTION);
					/*
					 * Put selected features in clipboard.
					 */
					PluginServices.putInClipboard(strb.toString());

					JOptionPane.showMessageDialog(
							ApplicationLocator.getManager().getRootComponent(),
							Messages.getText("_Number_of_features_copied_to_clipboard")
							+ ":   " + sele.getSize() + "    ",
							Messages.getText("_Copy_selected_features_to_clipboard"),
							JOptionPane.INFORMATION_MESSAGE);

				} catch (DataException e) {
					logger.error("While getting store and selection. ", e);
				}
			}
		}

	}

	public boolean isEnabled() {

		/*
		 * It's enabled if there is exactly one vector layer in the active view
		 * and it has a selection
		 */

		IWindow actw = actWin();

		if (actw instanceof IView) {

			IView vw = (IView) actw;
			FLayer[] act_lyr = vw.getMapControl().getMapContext().getLayers().getActives();
			if (act_lyr == null || act_lyr.length != 1
					|| !(act_lyr[0] instanceof FLyrVect)) {
				return false;

			} else {
				FLyrVect vect = (FLyrVect) act_lyr[0];
				if (!vect.isAvailable()) {
				    /*
				     * This can happen when opening a persisted project
				     * and there is a "slow" layer (GeoDB)
				     */
				    return false;
				}
				FeatureSelection sele = null;
				long sele_count = 0;
				try {
					sele = (FeatureSelection) vect.getFeatureStore().getSelection();
					sele_count = sele.getSize();
				} catch (DataException e) {
					logger.error("While getting store. ", e);
					return false;
				}
				return sele_count > 0;
			}

		} else {
			return false;
		}
	}

	public boolean isVisible() {

		return actWin() instanceof IView;
	}

	/**
	 * Gets active window
	 * @return
	 */
	private IWindow actWin() {
		return ApplicationLocator.getManager().getActiveWindow();
	}


}
