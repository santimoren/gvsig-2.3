/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project;

import java.util.Iterator;
import java.util.List;


import org.gvsig.app.project.documents.Document;
import org.gvsig.tools.observer.Observable;

public interface DocumentsContainer extends Iterable<Document>,  Observable {

	/**
	 * Return a list of documents in the project.
	 *
	 * @return documents as List of IProjectDocument
	 */
	public List<Document> getDocuments();

	/**
	 * Return a list with all documents of especified type.
	 *
	 * @param type of document
	 *
	 * @return List of IProjectDocument
	 */
	public List<Document> getDocuments(String type);

	/**
	 * Adds a document to the project
	 *
	 * @param document as Document
	 */
	public void addDocument(Document document);

	/**
	 * Remove a document of the project
	 *
	 * @param document as Document
	 */
	public void removeDocument(Document document);

	public Iterator<Document> iterator();

	public boolean isEmpty();
        
	/**
         * Returns the document of the type indicated and the name specified by name.
         * If documnt don't exists return null
	 *
	 * @param name name of the document to retrieve
	 * @param type name of the type of document to retrieve
	 *
	 * @return Documento
	 */
	public Document getDocument(String name, String type);

        /**
         * Returns the document with the name specified by name.
         * If documnt don't exists return null
         * 
         * @param name
         * @return Document with the name specified by name
         */
        public Document getDocument(String name);
	
	public Document getActiveDocument();

	public Document getActiveDocument(Class<? extends Document> documentClass);

        public Document getActiveDocument(String documentTypeName);

	public Document createDocument(String type);
}