/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.apache.commons.io.FileUtils;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class CheckSOAndArquitectureExtension extends Extension implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(CheckSOAndArquitectureExtension.class);

    @Override
    public void initialize() {
        // Do nothing
    }

    @Override
    public void execute(String actionCommand) {
        // Do nothing

    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    @Override
    public void postInitialize() {
        PluginsManager pluginManager = PluginsLocator.getManager();
        pluginManager.addStartupTask("CheckOsAndArquitectre", this, true, 100);
    }

    @Override
    public void run() {
        ApplicationManager application = ApplicationLocator.getManager();
        InstallerManager installmgr = InstallerLocator.getInstallerManager();
        PluginsManager pluginmgr = PluginsLocator.getManager();

        Set<PackageInfo> mismatchs = new HashSet<>();
        String recommendedArchitecture = null;
        try {
            PackageInfo[] pkgs = installmgr.getInstalledPackages();
            for (PackageInfo pkg : pkgs) {
                if( !InstallerManager.ARCH.ALL.equalsIgnoreCase(pkg.getArchitecture()) 
                    && !installmgr.getArchitecture().equalsIgnoreCase(pkg.getArchitecture())) {
                    recommendedArchitecture = pkg.getArchitecture();
                    mismatchs.add(pkg);
                }
                if (!InstallerManager.OS.ALL.equalsIgnoreCase(pkg.getOperatingSystemFamily()) && 
                    !installmgr.getOperatingSystemFamily().equalsIgnoreCase(pkg.getOperatingSystemFamily())) {
                    mismatchs.add(pkg);
                }
            }
        } catch (Throwable e) {
            logger.info("Can't get installed packages.", e);
        }
        if (mismatchs.isEmpty()) {
            return;
        }

        StringBuilder buffer = new StringBuilder();
        Iterator<PackageInfo> it = mismatchs.iterator();
        while (it.hasNext()) {
            PackageInfo pkg = it.next();
            buffer.append(pkg.getName());
            buffer.append(" (");
            buffer.append(pkg.getOperatingSystem());
            buffer.append("/");
            buffer.append(pkg.getArchitecture());
            buffer.append(")");
            buffer.append("<br>\n");
        }
        String template = "<html>"
                + "<p>gvSIG is running on a platform $(CURRENT_ARCHITECTURE)</p>\n" 
                + "<br>\n"
                + "<p>Packages are installed that are not compatible with your system.</p>\n"
                + "<br>\n"
                + "$(PACKAGES)"
                + "<br>\n"
                + "<p>Some are not specific to your system or architecture.</p>\n"
                + "<br>\n"
                + "<p>You probably need to configure a $(RECOMMENDED_ARCHITECTURE) Java environment\n"
                + "for the proper functioning of gvSIG.</p>\n"
                + "</html>\n";

        try {
            String fname = "i18n/" + application.translate("_filename_warning_architecture_or_os_mismatch");
            URL res = this.getClass().getClassLoader().getResource(fname);
            template = FileUtils.readFileToString(new File(res.getFile()));
        } catch (Throwable e) {
            logger.info("Can't get template, use default.", e);
        }

        String msg = template.replaceAll("[$][(]PACKAGES[)]", buffer.toString());
        msg = msg.replaceAll("[%]PACKAGES[%]", buffer.toString());
        msg = msg.replaceAll("[$]PACKAGES", buffer.toString());

        msg = msg.replaceAll("[$][(]RECOMMENDED_ARCHITECTURE[)]", recommendedArchitecture);
        msg = msg.replaceAll("[%]RECOMMENDED_ARCHITECTURE[%]", recommendedArchitecture);
        msg = msg.replaceAll("[$]RECOMMENDED_ARCHITECTURE", recommendedArchitecture);

        msg = msg.replaceAll("[$][(]CURRENT_ARCHITECTURE[)]", installmgr.getArchitecture());
        msg = msg.replaceAll("[%]CURRENT_ARCHITECTURE[%]", installmgr.getArchitecture());
        msg = msg.replaceAll("[$]CURRENT_ARCHITECTURE", installmgr.getArchitecture());
        
        application.showDialog(new ShowMessageControler(msg), "_Warning");
    }

    public class ShowMessageControler extends ShowMessageView {

        /**
         *
         */
        private static final long serialVersionUID = 2641062720310466029L;

        public ShowMessageControler(String message) {
            super();
            this.messaje.setContentType("text/html");
            this.messaje.setText(message);
            this.closeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    close();
                }
            });
            ApplicationManager application = ApplicationLocator.getManager();
            this.closeButton.setText(application.translate("Close"));
        }

        public void close() {
            this.setVisible(false);
        }

    }

    public class ShowMessageView extends JPanel {

        private static final long serialVersionUID = 8291970039773969840L;
        JTextPane messaje = new JTextPane();
        JButton closeButton = new JButton();

        /**
         * Default constructor
         */
        public ShowMessageView() {
            initializePanel();
        }

        /**
         * Adds fill components to empty cells in the first row and first column
         * of the grid. This ensures that the grid spacing will be the same as
         * shown in the designer.
         *
         * @param cols an array of column indices in the first row where fill
         * components should be added.
         * @param rows an array of row indices in the first column where fill
         * components should be added.
         */
        private void addFillComponents(Container panel, int[] cols, int[] rows) {
            Dimension filler = new Dimension(10, 10);

            boolean filled_cell_11 = false;
            CellConstraints cc = new CellConstraints();
            if (cols.length > 0 && rows.length > 0) {
                if (cols[0] == 1 && rows[0] == 1) {
                    /**
                     * add a rigid area
                     */
                    panel.add(Box.createRigidArea(filler), cc.xy(1, 1));
                    filled_cell_11 = true;
                }
            }

            for (int index = 0; index < cols.length; index++) {
                if (cols[index] == 1 && filled_cell_11) {
                    continue;
                }
                panel.add(Box.createRigidArea(filler), cc.xy(cols[index], 1));
            }

            for (int index = 0; index < rows.length; index++) {
                if (rows[index] == 1 && filled_cell_11) {
                    continue;
                }
                panel.add(Box.createRigidArea(filler), cc.xy(1, rows[index]));
            }

        }

        /**
         * Method for recalculating the component orientation for right-to-left
         * Locales.
         *
         * @param orientation the component orientation to be applied
         */
        @Override
        public void applyComponentOrientation(ComponentOrientation orientation) {
			// Not yet implemented...
            // I18NUtils.applyComponentOrientation(this, orientation);
            super.applyComponentOrientation(orientation);
        }

        public JPanel createPanel() {
            JPanel jpanel1 = new JPanel();
            FormLayout formlayout1 = new FormLayout(
                    "FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE",
                    "CENTER:DEFAULT:NONE,CENTER:DEFAULT:GROW(1.0),CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
            CellConstraints cc = new CellConstraints();
            jpanel1.setLayout(formlayout1);

            JScrollPane jscrollpane1 = new JScrollPane();
            jscrollpane1.setViewportView(messaje);
            jscrollpane1
                    .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            jscrollpane1
                    .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            jpanel1.add(jscrollpane1, new CellConstraints(2, 2, 3, 1,
                    CellConstraints.DEFAULT, CellConstraints.FILL));

            closeButton.setActionCommand("JButton");
            closeButton.setText("JButton");
            jpanel1.add(closeButton, cc.xy(3, 4));

            addFillComponents(jpanel1, new int[]{1, 2, 3, 4, 5}, new int[]{
                1, 2, 3, 4, 5});
            return jpanel1;
        }

        /**
         * Initializer
         */
        protected void initializePanel() {
            setLayout(new BorderLayout());
            add(createPanel(), BorderLayout.CENTER);
        }

    }

}
