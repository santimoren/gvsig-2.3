/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.view.toc;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.IContextMenuAction;
import org.gvsig.app.project.documents.view.toc.actions.FLyrVectEditPropertiesTocMenuEntry;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;


/**
 * @author FJP
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class TocItemBranch implements ITocItem {

	private ImageIcon icolayer = null;

	private ImageIcon finalIcon = null;

	private BufferedImage unavailableImg = null;
        
	private BufferedImage temporaryLayerImg = null;
        
	private final String defaultIcon = "images/icolayer.PNG";

	private FLayer lyr;

	private Dimension sz;

    final public static DataFlavor INFO_FLAVOR =
	    new DataFlavor(TocItemBranch.class, "ItemBranch");
    static DataFlavor flavors[] = {INFO_FLAVOR };

	public TocItemBranch(FLayer lyr)
	{
		this.lyr = lyr;
	}
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.ITocItem#getLabel()
	 */
	public String getLabel() {
		/*if (lyr instanceof FLyrVect && ((FLyrVect)lyr).isBroken())
		{
			return lyr.getName() + "(broken)";
		}
		else*/
			return lyr.getName();
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.ITocItem#getIcon()
	 */
	public Icon getIcon() {
		// TODO Pedirle el icono a la capa. Por defecto habr� un icono
		// para vectoriales y otro para raster, pero se podr�n cambiar.

		if (finalIcon == null) {
			ImageIcon icon = (ImageIcon) PluginServices.getIconTheme().get(lyr.getTocImageIcon());
			this.setIcon(icon);
		}
		updateStateIcon();

		return finalIcon;
	}

	private void setIcon(String path) {
		File f = new File(path);
		if (f.exists()) {
			icolayer = new ImageIcon(f.getAbsolutePath());
		} else {
			URL url =PluginServices.getPluginServices("org.gvsig.app").getClassLoader().getResource(path);
			if (url!=null) {
				icolayer = new ImageIcon(url);
				return;
			}
		}
		updateStateIcon();

	}
	private void updateStateIcon() {
		if (icolayer == null) {
                    return;
                }
        	BufferedImage newImage = new BufferedImage(icolayer.getIconWidth(),icolayer.getIconHeight(),BufferedImage.TYPE_INT_ARGB);
                Graphics2D grp = newImage.createGraphics();
                grp.drawImage(icolayer.getImage(),0,0,null);
		if (lyr.getTocStatusImage() != null) {
			Image img = lyr.getTocStatusImage();
			grp.drawImage(img,0,icolayer.getIconHeight()-img.getHeight(null),null);
		}

                if (!this.lyr.isAvailable()) {
                        BufferedImage img = this.getUnavailableImage();
                        grp.drawImage(img,0,icolayer.getIconHeight()-img.getHeight(),null);
                }
                
                if( this.lyr.isTemporary() ) {
                        BufferedImage img = this.getTemporaryLayerImage();
                        grp.drawImage(img,icolayer.getIconWidth()-img.getWidth(),icolayer.getIconHeight()-img.getHeight(),null);
                }
                
		this.finalIcon = new ImageIcon(newImage);
        }

	private void setIcon(ImageIcon icon) {
		if (icon!=null) {
			icolayer = icon;
		} else {
			this.setIcon(defaultIcon);

		}
		updateStateIcon();

	}

	public FLayer getLayer() {
		return lyr;
	}

	/* (non-Javadoc)
	 * @see java.awt.datatransfer.Transferable#getTransferDataFlavors()
	 */
	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	/* (non-Javadoc)
	 * @see java.awt.datatransfer.Transferable#isDataFlavorSupported(java.awt.datatransfer.DataFlavor)
	 */
	public boolean isDataFlavorSupported(DataFlavor dF) {
		return dF.equals(INFO_FLAVOR);
	}

	/* (non-Javadoc)
	 * @see java.awt.datatransfer.Transferable#getTransferData(java.awt.datatransfer.DataFlavor)
	 */
	public Object getTransferData(DataFlavor dF) throws UnsupportedFlavorException, IOException {
	    if (dF.equals(INFO_FLAVOR)) {
	        return this;
	      }
	      else throw new UnsupportedFlavorException(dF);
	}
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.ITocItem#getSize()
	 */
	public Dimension getSize() {
		return sz;
	}
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.ITocItem#setSize(java.awt.Dimension)
	 */
	public void setSize(Dimension sz) {
		this.sz = sz;
	}

	private BufferedImage getUnavailableImage() {
		if (this.unavailableImg == null) {
			ImageIcon uIcon = IconThemeHelper.getImageIcon("layer-chk-unavailable");
			if (uIcon != null) {
				this.unavailableImg = new BufferedImage(uIcon.getIconWidth(), uIcon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
				this.unavailableImg.getGraphics().drawImage(uIcon.getImage(), 0, 0, null);
			}
		}
		return this.unavailableImg;
	}
        
	private BufferedImage getTemporaryLayerImage() {
		if (this.temporaryLayerImg == null) {
			ImageIcon uIcon = IconThemeHelper.getImageIcon("layer-chk-temporary");
			if (uIcon != null) {
				this.temporaryLayerImg = new BufferedImage(uIcon.getIconWidth(), uIcon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
				this.temporaryLayerImg.getGraphics().drawImage(uIcon.getImage(), 0, 0, null);
			}
		}
		return this.temporaryLayerImg;
	}

	public IContextMenuAction getDoubleClickAction() {
		// TODO
		// THIS IS A PATCH; IT WILL BE REMOVED WHEN ALL THE PROPERTIES
		// FOR ALL LAYERS WILL BE MERGED IN THE SAME DIALOG
		if (!(getLayer() instanceof FLyrVect)) {
			return null;
		}
		return new FLyrVectEditPropertiesTocMenuEntry();
	}
}
