/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb.vectorialdb.wizard;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.jdbc.JDBCConnectionPanel;
import org.gvsig.fmap.mapcontrol.swing.dynobject.DynObjectEditor;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class VectorialDBConnectionParamsDialog extends VectorialDBConnectionParamsDialogView {

    private static final long serialVersionUID = -5493563028200403559L;

    private static final Logger LOG = LoggerFactory
        .getLogger(VectorialDBConnectionParamsDialog.class);

    private boolean isCanceled = false;
    private JDBCConnectionPanel jdbcServerExplorer;

    public VectorialDBConnectionParamsDialog() {
        initComponents();
    }
    
    private void initComponents() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        this.btnAdvanced.setText(i18nManager.getTranslation("advanced"));
        this.btnAdvanced.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doAdvanced();
            }
        });
        this.btnAcept.setText(i18nManager.getTranslation("ok"));
        this.btnAcept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doAcept();
            }
        });
        this.btnCancel.setText(i18nManager.getTranslation("cancel"));
        this.btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doCancel();
            }
        });
        
        this.btnDelete.setText(i18nManager.getTranslation("del"));
        this.btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
            	doDelete();
            }
        });
        this.jdbcServerExplorer = DALSwingLocator.getSwingManager().createJDBCConnectionPanel();
        this.containerJDBCConnectionPanel.setLayout(new BorderLayout());
        this.containerJDBCConnectionPanel.add(
                this.jdbcServerExplorer.asJComponent(),
                BorderLayout.CENTER
        );
    }
    
    protected void doAdvanced() {
        JDBCServerExplorerParameters myParams = this.getServerExplorerParameters();
        try {
            myParams.validate();
        } catch (Exception e) {
            // ignore... only for fill default values
        }

        try {
            DynObjectEditor editor = new DynObjectEditor(myParams);
            editor.editObject(true);
            this.jdbcServerExplorer.setServerExplorerParameters(myParams);
        } catch (ServiceException ex) {
            LOG.error("Error creating a Swing component for the DynObject: "
                    + myParams, ex);
        }
    }
    
    protected void doAcept() {
        this.isCanceled = false;
        this.setVisible(false);
    }
    
    protected void doCancel() {
        this.isCanceled = true;
        this.setVisible(false);
    }
    
    protected void doDelete() {
    	this.jdbcServerExplorer.clear();
    	this.jdbcServerExplorer.delete();
    }
    
    public void showDialog() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        WindowManager winmgr = ToolsSwingLocator.getWindowManager();
        winmgr.showWindow(
                this, 
                i18nManager.getTranslation("_Connection_parameters"), 
                WindowManager.MODE.DIALOG
        );
    }
    
    public boolean isCanceled() {
        return this.isCanceled;
    }
    
    public JDBCServerExplorerParameters getServerExplorerParameters() {
        return this.jdbcServerExplorer.getServerExplorerParameters();
    }
    
    public String getConnectionName() {
        return this.jdbcServerExplorer.getConnectionName();
    }
   
}

