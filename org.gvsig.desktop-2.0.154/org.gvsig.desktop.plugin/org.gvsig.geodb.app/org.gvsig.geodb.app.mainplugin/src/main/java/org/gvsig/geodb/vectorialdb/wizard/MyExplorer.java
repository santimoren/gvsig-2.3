/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.geodb.vectorialdb.wizard;

import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorerParameters;


/**
 * @author Vicente Caballero Navarro
 */
public class MyExplorer {
    private DBServerExplorerParameters dbExplorerParameters;
    private String name;

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public DBServerExplorerParameters getDbSeverExplorerParameters() {
        return dbExplorerParameters;
    }

    /**
     * DOCUMENT ME!
     *
     * @param dbExplorerParameters DOCUMENT ME!
     */
    public void setDbExplorerParameters(
        DBServerExplorerParameters dbExplorerParameters) {
        this.dbExplorerParameters = dbExplorerParameters;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getName() {
        return name;
    }

    /**
     * DOCUMENT ME!
     *
     * @param name DOCUMENT ME!
     */
    public void setName(String name) {
        this.name = name;
    }

	public String toString() {
		return getName();
	}

}
