/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb.vectorialdb.wizard;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.collections.map.HashedMap;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerPool;
import org.gvsig.fmap.dal.DataServerExplorerPoolEntry;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorerParameters;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.geodb.ExtDB_Spatial;
import org.gvsig.gui.beans.swing.JButton;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynobject.DynObject;

public class WizardDB extends WizardPanel implements ActionListener,
        ListSelectionListener {

    /**
     *
     */
    private static final long serialVersionUID = -7045762275505941695L;
    private static final String WIZARD_TAB_NAME = "DB";
    private static Logger logger = LoggerFactory.getLogger(WizardDB.class
            .getName());

    private JPanel namePanel = null;
    private JPanel tablesPanel = null;
    private JScrollPane tablesScrollPane = null;
    private AvailableTablesCheckBoxList tablesList = null;
    private JComboBox datasourceComboBox = null;
    private UserSelectedFieldsPanel fieldsPanel = null;
    private UserSelectedFieldsPanel emptyFieldsPanel = null;
    private JButton dbButton = null;
    private DBServerExplorerParameters dbExplorerParameters;
    private DBServerExplorer dbExplorer = null;

    private UserTableSettingsPanel settingsPanel = null;
    protected UserTableSettingsPanel emptySettingsPanel = null;
    private PrepareContext prepareDSContext;

    public WizardDB() {
        super();
        initialize();
        this.addAncestorListener(new AncestorListener() {
            public void ancestorAdded(AncestorEvent ae) {
            }

            public void ancestorRemoved(AncestorEvent ae) {
                if ( dbExplorer != null ) {
                    dbExplorer.dispose();
                    dbExplorer = null;
                }
            }

            public void ancestorMoved(AncestorEvent ae) {
            }
        });
    }

    public DBServerExplorer getServerExplorer() {
        return this.dbExplorer;
    }

    protected void initialize() {
        setTabName(WIZARD_TAB_NAME);
        setLayout(null);
        setSize(512, 478);

        emptyFieldsPanel = new UserSelectedFieldsPanel(null, true, this);
        add(emptyFieldsPanel);

        add(getNamePanel(), null);
        loadVectorialDBDatasourcesCombo(null);

        add(getTablesPanel(), null);

        emptySettingsPanel = createSettingsPanel(null);
        add(emptySettingsPanel);

    }

    @SuppressWarnings("rawtypes")
    private void loadVectorialDBDatasourcesCombo(MyExplorer sel) {
        DataManager dataManager = DALLocator.getDataManager();
        DataServerExplorerPool explorersPool = dataManager.getDataServerExplorerPool();
        Iterator it = explorersPool.iterator();
        getDatasourceComboBox().removeAllItems();
        getDatasourceComboBox().addItem("");
        while ( it.hasNext() ) {
            DataServerExplorerPoolEntry entry = (DataServerExplorerPoolEntry) it.next();
            MyExplorer myExplorer = new MyExplorer();
            myExplorer.setDbExplorerParameters((DBServerExplorerParameters) entry.getExplorerParameters());
            myExplorer.setName(entry.getName());
            getDatasourceComboBox().addItem(myExplorer);
            if ( sel != null && sel.getName().equalsIgnoreCase(myExplorer.getName()) ) {
                getDatasourceComboBox().setSelectedItem(myExplorer);
            }
        }

    }

    public void initWizard() {
    }

    @Override
    public void execute() {
        executeWizard();
    }

    @Override
    public Object executeWizard() {
        TablesListItem[] tables = getSelectedTables();

        DataManager man = DALLocator.getDataManager();
        FeatureStore store;

        String docName;
        TableDocument document;
        Project project = ProjectManager.getInstance().getCurrentProject();

        ApplicationManager appGvSIGMan = ApplicationLocator.getManager();
        PrepareContext context = this.getPrepareDataStoreContext();
        DBStoreParameters storeParams;
        List<TableDocument> tabledocs
                = new ArrayList<TableDocument>(tables.length);
        for ( TablesListItem table : tables ) {
            storeParams = getParameterForTable(table);

            try {
                storeParams = (DBStoreParameters) appGvSIGMan
                        .prepareOpenDataStoreParameters(storeParams, context);
            } catch (Exception e2) {
                NotificationManager.addError(e2);
                continue;
            }

            UserTableSettingsPanel userTableSettingsPanel = table
                    .getUserTableSettingsPanel();

            docName = userTableSettingsPanel.getUserLayerName();
            try {
                store = (FeatureStore) man.openStore(storeParams.getDataStoreName(), storeParams);
            } catch (Exception e) {
                NotificationManager.addError(e);
                return null;
            }

            try {
                appGvSIGMan.pepareOpenDataSource(
                        store, context);
            } catch (Exception e) {
                NotificationManager.addError(e);
                store.dispose();
                return null;
            }

            document = (TableDocument) ProjectManager.getInstance().createDocument(TableManager.TYPENAME, docName);
            document.setStore(store);
            // project.add(document);
            tabledocs.add(document);
        }
        return tabledocs;
    }

    protected DBStoreParameters getParameterForTable(TablesListItem table) {
        DBStoreParameters parameters = table.getParameters();

        UserTableSettingsPanel userTableSettingsPanel = table
                .getUserTableSettingsPanel();

        String fidField = userTableSettingsPanel.getIdFieldName();
        //IF is a multiple PK, remove the {} symbols
        if ( fidField.startsWith("{") && fidField.endsWith("}") ) {
            fidField = fidField.substring(1, fidField.length() - 1);
        }
        String[] pkFields = fidField.split(",");

        String[] fields = table.getUserSelectedFieldsPanel()
                .getUserSelectedFields(pkFields, null);

        if ( userTableSettingsPanel.isSqlActive() ) {
            String whereClause = userTableSettingsPanel
                    .getWhereClause();
            parameters.setBaseFilter(whereClause);
        } else {
            parameters.setBaseFilter("");
        }

        parameters.setFields(fields);

        return parameters;

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected TablesListItem[] getSelectedTables() {
        int count = tablesList.getModel().getSize();
        ArrayList resp = new ArrayList();

        for ( int i = 0; i < count; i++ ) {
            TablesListItem item = (TablesListItem) tablesList.getModel()
                    .getElementAt(i);

            if ( item.isSelected() ) {
                resp.add(item);
            }
        }

        return (TablesListItem[]) resp.toArray(new TablesListItem[0]);
    }

    /**
     * This method initializes namePanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getNamePanel() {
        if ( namePanel == null ) {
            namePanel = new JPanel();
            namePanel.setLayout(null);
            namePanel.setBounds(new java.awt.Rectangle(5, 5, 501, 51));
            namePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(
                    null, PluginServices.getText(this, "choose_connection"),
                    javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, null,
                    null));
            namePanel.add(getDatasourceComboBox(), null);
            namePanel.add(getJdbcButton(), null);
        }

        return namePanel;
    }

    /**
     * This method initializes tablesPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getTablesPanel() {
        if ( tablesPanel == null ) {
            tablesPanel = new JPanel();
            tablesPanel.setLayout(new BorderLayout());
            tablesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(
                    null, PluginServices.getText(this, "choose_table"),
                    javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, null,
                    null));
            tablesPanel.setBounds(new java.awt.Rectangle(5, 55, 246, 166));
            tablesPanel
                    .add(getTablesScrollPane(), java.awt.BorderLayout.CENTER);
        }

        return tablesPanel;
    }

    /**
     * This method initializes settingsPanel
     *
     * @return javax.swing.JPanel
     */
    /**
     * This method initializes tablesScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getTablesScrollPane() {
        if ( tablesScrollPane == null ) {
            tablesScrollPane = new JScrollPane();
            tablesScrollPane.setViewportView(getTablesList());
        }

        return tablesScrollPane;
    }

    /**
     * This method initializes tablesList
     *
     * @return javax.swing.JList
     */
    protected AvailableTablesCheckBoxList getTablesList() {
        if ( tablesList == null ) {
            tablesList = new AvailableTablesCheckBoxList(this);
            tablesList.addListSelectionListener(this);
            tablesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }

        return tablesList;
    }

    /**
     * This method initializes layerNameTextField
     *
     * @return javax.swing.JTextField
     */
    /**
     * This method initializes jComboBox
     *
     * @return javax.swing.JComboBox
     */
    private JComboBox getDatasourceComboBox() {
        if ( datasourceComboBox == null ) {
            datasourceComboBox = new JComboBox();
            datasourceComboBox
                    .setBounds(new java.awt.Rectangle(10, 20, 446, 21));
            datasourceComboBox.addActionListener(this);
        }

        return datasourceComboBox;
    }

    public void actionPerformed(ActionEvent arg0) {
        if ( datasourceComboBox.getItemCount() == 0 ) {
            setEmptyPanels();
        }
        Object src = arg0.getSource();

        if ( src == datasourceComboBox ) {
            Object selected = datasourceComboBox.getSelectedItem();
            if ( selected instanceof MyExplorer ) {
                MyExplorer sel_obj = (MyExplorer) selected;

                getDatasourceComboBox().repaint();
                dbExplorerParameters = sel_obj.getDbSeverExplorerParameters();
                updateTableList(dbExplorerParameters);
            }

        } else if ( src == dbButton ) {
            MyExplorer sel = addNewConnection();

            if ( sel != null ) {
                dbExplorerParameters = sel.getDbSeverExplorerParameters();
                loadVectorialDBDatasourcesCombo(sel);
                getDatasourceComboBox().setSelectedItem(sel);

            }
        }

    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private MyExplorer addNewConnection() {
        MyExplorer myExplorer = new MyExplorer();
        DBServerExplorerParameters resp = null;

        
        VectorialDBConnectionParamsDialog newco = new VectorialDBConnectionParamsDialog();
        newco.showDialog();

        if ( !newco.isCanceled()) {
            myExplorer.setDbExplorerParameters(newco.getServerExplorerParameters());
            myExplorer.setName(newco.getConnectionName());
            loadVectorialDBDatasourcesCombo(myExplorer);
            return myExplorer;
        } else {
            return null;
        }
    }

    protected TablesListItem createTabeListItem(DBStoreParameters param) {
        return new TablesListItem(param, this);
    }

    /**
     * Subclasses of this wizard will return a filtered list
     * if necessary
     *
     * @param explorer
     * @return
     * @throws DataException
     */
    protected List getTableList(DBServerExplorer explorer) throws DataException {
        return explorer.list();
    }

    @SuppressWarnings("rawtypes")
    protected void updateTableList(
            DBServerExplorerParameters dbSeverExplorerParameters2) {
        if ( dbSeverExplorerParameters2 == null ) {
            return;
        }
        DataManager dm = DALLocator.getDataManager();
        String err_msg = Messages.getText("connection_error");
        try {
            dbExplorer = (DBServerExplorer) dm.openServerExplorer(dbSeverExplorerParameters2.getExplorerName(), dbSeverExplorerParameters2);

            List parameters = getTableList(dbExplorer);

            DefaultListModel lmodel = new DefaultListModel();

            Iterator iter = parameters.iterator();
            DBStoreParameters param;
            int count = 0;
            while ( iter.hasNext() ) {
                param = (DBStoreParameters) iter.next();
                lmodel.addElement(createTabeListItem(param));
                count++;
            }

            getTablesList().setModel(lmodel);
            getTablesScrollPane().setViewportView(tablesList);
            tablesScrollPane.updateUI();
        } catch (Exception e) {
            logger.info("Error while getting table names: " + e.getMessage(),e);

            String remove_question = Messages.getText(
                    "_Remove_connection_parameters_from_list_question");

            int opt = JOptionPane.showConfirmDialog(
                    ApplicationLocator.getManager().getRootComponent(),
                    err_msg + ": " + getLastMessage(e) + "\n" + remove_question,
                    err_msg,
                    JOptionPane.YES_NO_OPTION);

            if ( opt == JOptionPane.YES_OPTION ) {
                removeDBPArameters(dbSeverExplorerParameters2);
                // dbExplorer.
                loadVectorialDBDatasourcesCombo(null);
            }
        }

    }

    /**
     * Removes DB parameters from list of pre-configured DB parameters
     *
     * @param dbSeverExplorerParameters2
     */
    private void removeDBPArameters(
            DBServerExplorerParameters dbparams) {

        PluginsManager manager = PluginsLocator.getManager();
        DynObject values = manager.getPlugin(ExtDB_Spatial.class).getPluginProperties();
        Map connections = (Map) values.getDynValue("db_connections");
        if ( connections == null ) {
            return;
        }

        DBServerExplorerParameters item = null;
        Iterator it = connections.entrySet().iterator();
        List<String> toremove = new ArrayList();

        while ( it.hasNext() ) {
            Map.Entry entry = (Entry) it.next();
            item = (DBServerExplorerParameters) entry.getValue();
            if ( dbparams.equals(item) ) {
                toremove.add((String) entry.getKey());
            }
        }

        if ( toremove.size() == 0 ) {
            return;
        }

        for ( int i = 0; i < toremove.size(); i++ ) {
            connections.remove(toremove.get(i));
        }
        values.setDynValue("db_connections", connections);

    }

    public void valueChanged(ListSelectionEvent arg0) {
        Object src = arg0.getSource();

        if ( src == tablesList ) {
            TablesListItem selected = (TablesListItem) tablesList
                    .getSelectedValue();

            setSettingsPanels(selected);
            checkFinishable();
        }
    }

    public boolean areSettingsValid() {
        int count = tablesList.getModel().getSize();

        boolean at_least_one = false;
        boolean resp = true;

        for ( int i = 0; i < count; i++ ) {
            TablesListItem item = (TablesListItem) tablesList.getModel()
                    .getElementAt(i);

            if ( item.isSelected() ) {
                at_least_one = true;
            }

            if ( item.disturbsWizardValidity() ) {
                resp = false;
            }
        }

        return (at_least_one && resp);
    }

    public void checkFinishable() {
        boolean finishable = areSettingsValid();
        callStateChanged(finishable);
    }

    /**
     * This method initializes jdbcButton
     *
     * @return javax.swing.JButton
     */
    private JButton getJdbcButton() {
        if ( dbButton == null ) {
            dbButton = new JButton();
            dbButton.addActionListener(this);
            dbButton.setToolTipText(PluginServices.getText(this,
                    "add_connection"));
            dbButton.setBounds(new java.awt.Rectangle(465, 20, 26, 21));

            dbButton.setIcon(IconThemeHelper.getImageIcon("geodb-connection-add"));
        }

        return dbButton;
    }

    private void showConnectionErrorMessage(String _msg) {
        String msg = (_msg.length() > 300) ? "" : (": " + _msg);
        String title = PluginServices.getText(this, "connection_error");
        JOptionPane.showMessageDialog(this, title + msg, title,
                JOptionPane.ERROR_MESSAGE);
    }

    private java.net.URL createResourceUrl(String path) {
        return getClass().getClassLoader().getResource(path);
    }

    public void setSettingsPanels(TablesListItem actTable) {
        if ( actTable == null ) {
            setEmptyPanels();

            return;
        }
        fieldsPanel = actTable.getUserSelectedFieldsPanel();

        removeFieldPanels();
        add(fieldsPanel);
        fieldsPanel.repaint();
        removeSettingsPanels();
        add(emptySettingsPanel);

        settingsPanel = createSettingsPanel(actTable);

        removeSettingsPanels();
        add(settingsPanel);
        settingsPanel.repaint();

        repaint();
    }

    protected UserTableSettingsPanel createSettingsPanel(TablesListItem actTable) {
        if ( actTable == null ) {
            return new UserTableSettingsPanel(null, "", true, this, null);
        }

        return actTable.getUserTableSettingsPanel();
    }

    protected void setEmptyPanels() {
        removeFieldPanels();
        add(emptyFieldsPanel);
        fieldsPanel = emptyFieldsPanel;

        repaint();
    }

    private void removeFieldPanels() {
        for ( int i = 0; i < getComponentCount(); i++ ) {
            if ( getComponent(i) instanceof UserSelectedFieldsPanel ) {
                remove(i);
            }
        }
    }

    public DataStoreParameters[] getParameters() {
        try {
            TablesListItem[] selected = getSelectedTables();
            int count = selected.length;
            DBStoreParameters[] dbParameters = new DBStoreParameters[count];

            for ( int i = 0; i < count; i++ ) {
                TablesListItem item = selected[i];

                dbParameters[i] = getParameterForTable(item);
            }

            return dbParameters;// layerArrayToGroup(all_layers, groupName);
        } catch (Exception e) {
            logger.info("Error while creating jdbc layer: " + e.getMessage(), e);
            NotificationManager.addError("Error while loading layer: "
                    + e.getMessage(), e);
        }

        return null;
    }

    /**
     * This method process the errors found in a layer
     *
     * @param lyr
     * @param mapControl
     */
    protected void processErrorsOfLayer(FLayer lyr, MapControl mapControl) {
        this.getMapContext().callNewErrorEvent(null);
    }

    private void removeSettingsPanels() {
        for ( int i = 0; i < getComponentCount(); i++ ) {
            if ( getComponent(i) instanceof UserTableSettingsPanel ) {
                remove(i);
            }
        }
    }

    protected PrepareContext getPrepareDataStoreContext() {
        if ( this.prepareDSContext == null ) {
            this.prepareDSContext = new PrepareContext() {
                public Window getOwnerWindow() {
                    return null;
                }

                public IProjection getViewProjection() {
                    return WizardDB.this.getMapContext().getProjection();
                }

            };
        }
        return this.prepareDSContext;
    }

    @Override
    public void close() {

    }

    protected String getLastMessage(Throwable ex) {

        Throwable p = ex;
        while ( p.getCause() != null && p.getCause() != p ) {
            p = p.getCause();
        }
        return p.getMessage();
    }

} // @jve:decl-index=0:visual-constraint="10,10"
