/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb;

import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.extension.AddLayer;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.geodb.vectorialdb.wizard.WizardDB;
import org.gvsig.geodb.vectorialdb.wizard.WizardVectorialDB;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.fmap.dal.DataServerExplorerPool;

/**
 * This extension adds the export-to-oracle button.
 *
 * @author jldominguez
 *
 */
public class ExtDB_Spatial extends Extension {

    public void initialize() {
        IconThemeHelper.registerIcon("geodb", "geodb-connection-add", this);
        IconThemeHelper.registerIcon("geodb", "geodb-advanced-properties", this);
        IconThemeHelper.registerIcon("geodb", "geodb-get-view-extent", this);
        
        DataManager dataManager = DALLocator.getDataManager();
        PluginsManager manager = PluginsLocator.getManager();
        DynObject pluginProperties = manager.getPlugin(ExtDB_Spatial.class).getPluginProperties();
        if( pluginProperties.getDynValue("explorersPool")==null ) {
            pluginProperties.setDynValue("explorersPool", dataManager.getDataServerExplorerPool());
        } else {
            dataManager.setDataServerExplorerPool((DataServerExplorerPool)pluginProperties.getDynValue("explorersPool"));
        }
        
    }

    public void execute(String actionCommand) {
        //do nothing
    }

    public boolean isEnabled() {
        return isVisible();

        // return true;
    }

    public boolean isVisible() {
        return false;
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
        addAboutInfo();

        ApplicationLocator.getManager().registerAddTableWizard("DB",
                "DB Tables", WizardDB.class);
        AddLayer.addWizard(WizardVectorialDB.class);
    }

    private void addAboutInfo() {
        ApplicationManager application = ApplicationLocator.getManager();

        AboutManager about = application.getAbout();
        about.addDeveloper("PRODEVELOP", getClass().getClassLoader()
                .getResource("about/prodevelop.html"), 2);

        AboutParticipant participant = about.getDeveloper("PRODEVELOP");
        participant.addContribution(
                "GeoDB",
                "Soporte para pool de conexiones con BBDD",
                2006, 1, 1,
                2007, 12, 31
        );
    }

}
