/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.geodb.vectorialdb.wizard;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.addlayer.AddLayerDialog;
import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.i18n.Messages;


public class UserTableSettingsVectorialPanel extends UserTableSettingsPanel {
    private static Logger logger = LoggerFactory
			.getLogger(UserTableSettingsPanel.class.getName());

    private FieldComboItem[] geos;
	private JComboBox geomComboBox = null;
    private JLabel geomLabel = null;
	private JLabel waLabel = null;
	private JLabel topLabel = null;
	private JTextField topTextField = null;
	private JTextField bottomTextField = null;
	private JTextField rightTextField = null;
	private JTextField leftTextField = null;
	private JLabel bottomLabel = null;
	private JLabel rightLabel = null;
	private JLabel leftLabel = null;
	private JButton getviewButton = null;
	private JCheckBox activateWACheckBox = null;
    private MapControl mControl = null;
	private CRSSelectPanel panelProj;
	
    private IProjection proposedProjection = null;
    // private IProjection currentProj;
	
	private JPanel this_panel = null;

    public UserTableSettingsVectorialPanel(FieldComboItem[] idComboItems,
			FieldComboItem[] geoComboItems, String initialLayerName,
			MapControl mapc, boolean empty, WizardVectorialDB _p,
			DBStoreParameters parameters,
			IProjection proposed_proj) {
    	super();
    	
    	this_panel = this;
		setInitValues(idComboItems, initialLayerName, empty, _p, parameters);

        mControl = mapc;
		geos = geoComboItems;
		proposedProjection = proposed_proj;
		initialize(empty);
	}

    protected void initialize(boolean _empty) {
    	super.initialize(_empty);
        setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				PluginServices.getText(this, "_Table_settings"),
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
		leftLabel = new JLabel();
		// leftLabel.setBounds(new java.awt.Rectangle(375, 175, 111, 16));
		leftLabel.setBounds(new java.awt.Rectangle(80+70, 200, 70, 16));
		leftLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 10));
		leftLabel.setText(PluginServices.getText(this, "xmin"));
		rightLabel = new JLabel();
		rightLabel.setBounds(new java.awt.Rectangle(75+150, 200, 70, 16));
		rightLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 10));
		rightLabel.setText(PluginServices.getText(this, "xmax"));
		bottomLabel = new JLabel();
		bottomLabel.setBounds(new java.awt.Rectangle(70+230, 200, 70, 16));
		bottomLabel
				.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 10));
		bottomLabel.setText(PluginServices.getText(this, "ymin"));
		topLabel = new JLabel();
		topLabel.setBounds(new java.awt.Rectangle(65+310, 200, 70, 16));
		topLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 10));
		topLabel.setText(PluginServices.getText(this, "ymax"));
		waLabel = new JLabel();
		// waLabel.setBounds(new java.awt.Rectangle(40, 145, 131, 21));
		waLabel.setBounds(new java.awt.Rectangle(30, 215, 125, 21));
		waLabel.setText(PluginServices.getText(this, "_Working_area"));
		geomLabel = new JLabel();
		// geomLabel.setBounds(new java.awt.Rectangle(240, 55, 111, 21));
		geomLabel.setBounds(new java.awt.Rectangle(8, 140, 160, 21));
		geomLabel.setText(PluginServices.getText(this, "_Geo_field"));

		add(getGeomComboBox(), null);
		add(geomLabel, null);
	    add(waLabel, null);
		add(topLabel, null);
		add(getTopTextField(), null);
		add(getBottomTextField(), null);
		add(getRightTextField(), null);
		add(getLeftTextField(), null);
		add(bottomLabel, null);
		add(rightLabel, null);
		add(leftLabel, null);
		add(getGetviewButton(), null);
		add(getActivateWACheckBox(), null);
		loadValues(_empty);
	}

    private CRSSelectPanel getJPanelProj(IProjection pro) {
		if (panelProj == null) {
			panelProj = CRSSelectPanel.getPanel(pro);

			panelProj.setTransPanelActive(true);
			panelProj.setBounds(8, 165, 477, 21);
			panelProj.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (panelProj.isOkPressed()) {
						AddLayerDialog
								.setLastProjection(panelProj.getCurProj());
					}
				}
			});
		}
		return panelProj;
	}


    public boolean hasValidValues() {
		if (!super.hasValidValues()) {
			return false;
		}

		if ((activateWACheckBox.isSelected()) && (getWorkingArea() == null)) {
			return false;
		}

		return true;
	}

    private JComboBox getGeomComboBox() {
		if (geomComboBox == null) {
			geomComboBox = new JComboBox();
			// geomComboBox.setBounds(new java.awt.Rectangle(355, 55, 131, 21));
			geomComboBox.setBounds(new java.awt.Rectangle(150, 140, 335, 21));
			geomComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    
                    if (panelProj == null) {
                        return;
                    }
                    /*
                     * Set the proj of the field in the 
                     * SRS chooser
                     */
                    FieldComboItem fci = (FieldComboItem) geomComboBox.getSelectedItem();
                    
                    if (fci == null) {
                        return;
                    }
                    
                    IProjection iproj = fci.getProjection();
                    if (iproj != null) {
                        /*
                         * Remove and recreate crs selection panel
                         */
                        this_panel.remove(panelProj);
                        panelProj = null;
                        add(getJPanelProj(iproj), null);
                    }
                }
			});
		}

		return geomComboBox;
	}

	private JTextField getTopTextField() {
		if (topTextField == null) {
			topTextField = new JTextField();
			topTextField.addKeyListener(this);
			// topTextField.setBounds(new java.awt.Rectangle(15, 190, 111, 21));
			topTextField.setBounds(new java.awt.Rectangle(65+310, 215, 70, 21));
		}

		return topTextField;
	}

	private JTextField getBottomTextField() {
		if (bottomTextField == null) {
			bottomTextField = new JTextField();
			bottomTextField.addKeyListener(this);
			bottomTextField
					.setBounds(new java.awt.Rectangle(70+230, 215, 70, 21));
		}

		return bottomTextField;
	}

	private JTextField getRightTextField() {
		if (rightTextField == null) {
			rightTextField = new JTextField();
			rightTextField.addKeyListener(this);
			rightTextField.setBounds(new java.awt.Rectangle(75+150, 215, 70, 21));
		}

		return rightTextField;
	}

	private JTextField getLeftTextField() {
		if (leftTextField == null) {
			leftTextField = new JTextField();
			leftTextField.addKeyListener(this);
			leftTextField.setBounds(new java.awt.Rectangle(80+70, 215, 70, 21));
		}

		return leftTextField;
	}

	private JButton getGetviewButton() {
		if (getviewButton == null) {
			getviewButton = new org.gvsig.gui.beans.swing.JButton();
			getviewButton.addActionListener(this);
			//getviewButton.setBounds(new java.awt.Rectangle(195, 145, 111, 26));
			getviewButton.setBounds(new java.awt.Rectangle(455, 205, 31, 31));
            ImageIcon ii = IconThemeHelper.getImageIcon("geodb-get-view-extent"); 
            // ImageIcon ii = IconThemeHelper.getImageIcon("geodb-connection-add"); 
			
            getviewButton.setMargin(new Insets(1,1,1,1));
	        getviewButton.setIcon(ii);
	        getviewButton.setToolTipText(
	                Messages.getText("_Get_view_extent"));
	        getviewButton.setEnabled(false);
		}

		return getviewButton;
	}

	private JCheckBox getActivateWACheckBox() {
		if (activateWACheckBox == null) {
			activateWACheckBox = new JCheckBox();
			activateWACheckBox.addActionListener(this);
//			activateWACheckBox
//					.setBounds(new java.awt.Rectangle(15, 145, 21, 21));
			activateWACheckBox
					.setBounds(new java.awt.Rectangle(5, 215, 21, 21));

		}

		return activateWACheckBox;
	}

    protected void loadValues(boolean is_empty) { 
		super.loadValues(is_empty);
		if (is_empty) {
			enableAlphaControls(false);
			enableSpatialControls(false);
			getActivateWACheckBox().setSelected(false);
		} else {
			enableAlphaControls(true);
			enableSpatialControls(true);

			getGeomComboBox().removeAllItems();

			for (int i = 0; i < geos.length; i++) {
				getGeomComboBox().addItem(geos[i]);
			}
			add(getJPanelProj(proposedProjection), null);
		}
	}

    public void enableSpatialControls(boolean enable) {
    	super.enableSpatialControls(enable);
		getGeomComboBox().setEnabled(enable);

		getActivateWACheckBox().setEnabled(enable);

		boolean there_is_view = ((mControl != null) &&
		    (mControl.getViewPort().getAdjustedEnvelope() != null));

		getGetviewButton().setEnabled(enable && there_is_view);
		getTopTextField().setEnabled(enable);
		getBottomTextField().setEnabled(enable);
		getRightTextField().setEnabled(enable);
		getLeftTextField().setEnabled(enable);
	}

    public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();

        if (src == getviewButton) {
			getViewIntoFourBounds();
			parent.checkFinishable();
		}

		if (src == activateWACheckBox) {
			enableWASettings(activateWACheckBox.isSelected());
			parent.checkFinishable();
		}
	}

    private void enableWASettings(boolean b) {
		getviewButton.setEnabled(b
				&& (mControl.getViewPort().getAdjustedEnvelope() != null));
		rightTextField.setEnabled(b);
		leftTextField.setEnabled(b);
		topTextField.setEnabled(b);
		bottomTextField.setEnabled(b);
	}

    private void getViewIntoFourBounds() {
		Envelope rect = mControl.getViewPort().getAdjustedEnvelope();
		
		IProjection viewProj = mControl.getViewPort().getProjection();
		IProjection srcProj = this.getProjection();
		if (viewProj == null || srcProj == null) {
		    messageDialog(Messages.getText(
		        "_Unable_to_reproject_viewport_Default_bounds_used"),
		        JOptionPane.WARNING_MESSAGE);
		    setLargeArea(srcProj);
		} else {
		    try {
		        ICoordTrans ct = null;
		        if (viewProj.getAbrev().compareToIgnoreCase(
		            srcProj.getAbrev()) != 0) {
		            /*
		             * Reprojection needed
		             */
		            ct = viewProj.getCT(srcProj);
	                rect = rect.convert(ct);
		        }
		        topTextField.setText(getFormattedDouble(rect.getMaximum(1)));
		        bottomTextField.setText(getFormattedDouble(rect.getMinimum(1)));
		        rightTextField.setText(getFormattedDouble(rect.getMaximum(0)));
		        leftTextField.setText(getFormattedDouble(rect.getMinimum(0)));
		        if (ct != null) {
	                messageDialog(Messages.getText(
	                    "_Viewport_bounds_reprojected_to_datasource_CRS"),
	                    JOptionPane.WARNING_MESSAGE);
		        }
		    } catch (Exception exc) {
	            messageDialog(Messages.getText(
	                "_Unable_to_reproject_viewport_Default_bounds_used"),
	                JOptionPane.WARNING_MESSAGE);
	            setLargeArea(srcProj);
		    }
		}
		
	}

    private void setLargeArea(IProjection proj) {
        
        if (proj == null || proj.isProjected()) {
            topTextField.setText(getFormattedDouble(20000000d));
            bottomTextField.setText(getFormattedDouble(-20000000d));
            rightTextField.setText(getFormattedDouble(20000000d));
            leftTextField.setText(getFormattedDouble(-20000000d));
        } else {
            topTextField.setText(getFormattedDouble(90d));
            bottomTextField.setText(getFormattedDouble(-90d));
            rightTextField.setText(getFormattedDouble(180d));
            leftTextField.setText(getFormattedDouble(-180d));
        }
    }

    private void messageDialog(String msg, int msgType) {
        
        JOptionPane.showMessageDialog(
            ApplicationLocator.getManager().getRootComponent(),
            msg,
            Messages.getText("_Working_area"),
            msgType);
    }

    public Envelope getWorkingArea() {
		if (!activateWACheckBox.isSelected()) {
			return null;
		}

		double maxx;
		double maxy;
		double minx;
		double miny;

		try {
			maxx = Double.parseDouble(rightTextField.getText());
			miny = Double.parseDouble(bottomTextField.getText());
			minx = Double.parseDouble(leftTextField.getText());
			maxy = Double.parseDouble(topTextField.getText());
		} catch (NumberFormatException nfe) {
			logger.info("Not valid value in working area: " + nfe.getMessage());
			return null;
		}
		GeometryManager geoMan = GeometryLocator.getGeometryManager();

		try {
			return geoMan.createEnvelope(minx, miny, maxx, maxy,
					SUBTYPES.GEOM2D);
		} catch (CreateEnvelopeException e) {
			// FIXME Exception
			throw new RuntimeException(e);
		}
	}

    public boolean combosHaveItems() {
		if (!super.combosHaveItems()) {
			return false;
		}

		if (getGeomComboBox().getItemCount() == 0) {
			return false;
		}

		return true;
	}

    public void repaint() {
		super.repaint();
		getGeomComboBox().updateUI();
	}

    public String getGeoFieldName() {
		if (getGeomComboBox().getSelectedItem() == null) {
			return null;
		}
		return getGeomComboBox().getSelectedItem().toString();
	}
    public IProjection getProjection() {
		return panelProj.getCurProj();
	}

}
