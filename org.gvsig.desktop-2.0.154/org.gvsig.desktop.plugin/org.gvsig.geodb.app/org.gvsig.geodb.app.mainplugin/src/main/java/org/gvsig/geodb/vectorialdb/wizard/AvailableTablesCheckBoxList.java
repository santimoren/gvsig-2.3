/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb.vectorialdb.wizard;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;



/**
 * Utility class to keep the list of available tables.
 *
 * @author jldominguez
 *
 */
public class AvailableTablesCheckBoxList extends JList {
    protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

    private static final Logger LOG = LoggerFactory
        .getLogger(AvailableTablesCheckBoxList.class);
    private WizardDB parent = null;
    private TablesListItem actingTable = null;

    public AvailableTablesCheckBoxList(WizardDB p) {
        parent = p;

        setCellRenderer(new CellRenderer());

        addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    int index = locationToIndex(e.getPoint());

                    if (index == -1) {
                        return;
                    }

                    actingTable = (TablesListItem) getModel().getElementAt(index);

                    if ((e.getClickCount() == 2) || (e.getX() < 15)) {
                        if (!actingTable.isActivated()) {
                            actingTable.activate();
                        }

                        actingTable.setSelected(!actingTable.isSelected());

                        if (actingTable.isSelected()) {
                            actingTable.setEnabledPanels(true);
                        }
                        else {
                            actingTable.setEnabledPanels(false);
                        }

                        repaint();
                    }

                    try {
                        parent.setSettingsPanels(actingTable);
                        parent.checkFinishable();
                    }
                    catch (Exception e1) {
                        actingTable.setEnabledPanels(false);
                        actingTable.setSelected(false);
                    LOG.info(
                        "Error while setting selected table: " +
                            e1.getMessage(), e1);
                        showConnectionErrorMessage(e1.getMessage());
                    }
                }
            });

        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    private void showConnectionErrorMessage(String _msg) {
    	if (_msg==null) {
			_msg="";
		}
        String msg = (_msg.length() > 300) ? "" : (": " + _msg);
        String title = PluginServices.getText(this, "connection_error");
        JOptionPane.showMessageDialog(this, title + msg, title,
            JOptionPane.ERROR_MESSAGE);
    }

    protected class CellRenderer implements ListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
            TablesListItem checkbox = (TablesListItem) value;
            checkbox.setBackground(isSelected ? getSelectionBackground()
                                              : getBackground());
            checkbox.setForeground(isSelected ? getSelectionForeground()
                                              : getForeground());
            checkbox.setEnabled(isEnabled());
            checkbox.setFont(getFont());
            checkbox.setFocusPainted(false);
            checkbox.setBorderPainted(true);
            checkbox.setBorder(isSelected
                ? UIManager.getBorder("List.focusCellHighlightBorder")
                : noFocusBorder);

            return checkbox;
        }
    }
}
