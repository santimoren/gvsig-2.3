/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.geodb.vectorialdb.wizard;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.i18n.Messages;


public class TablesListItemVectorial extends TablesListItem {
	private MapControl mc;


	public TablesListItemVectorial(DBStoreParameters param,
			MapControl _mc, WizardVectorialDB _parent) {
		super(param, _parent);
		mc = _mc;
	}

    public UserTableSettingsPanel getUserTableSettingsPanel(String espView) {
		if (tableSettingsPanel == null) {

			String[] ids = new String[0];
			FeatureType ft = null;
			FeatureAttributeDescriptor dbattr = null;

			try {
				ft = this.parent.getServerExplorer().getFeatureType(parameters);
			} catch (DataException e) {
				NotificationManager.addError(e);
				return null;
			}

			ArrayList auxAll = new ArrayList();

			ArrayList geo_atts = new ArrayList();
			ArrayList auxId = new ArrayList();
			Iterator iter = ft.iterator();
			while (iter.hasNext()) {
				dbattr = (FeatureAttributeDescriptor) iter.next();
				if (dbattr.getDataType().getType() == DataTypes.GEOMETRY) {
				    geo_atts.add(dbattr);
				}
				if (dbattr.isPrimaryKey()) {
					auxId.add(dbattr.getName());
				}
				auxAll.add(dbattr.getName());

			}

			if (auxId.size() > 0) {
				StringBuilder strb = new StringBuilder();
				strb.append('{');
				for (int i = 0; i < auxId.size() - 1; i++) {
					strb.append(auxId.get(i));
					strb.append(',');
				}
				strb.append(auxId.get(auxId.size() - 1));

				strb.append('}');
				/*
				 * Only the true PK from the DB
				 */
				auxAll.clear();
				auxAll.add(strb.toString());
			} else {
			    /*
			     * If the table does not hace PK declared in the DB,
			     * we let the user choose any field and show a warning dialog
			     */
			    showNoPkWarning(this.tableName);
			}
			
			// ===============================
			
			ids = (String[]) auxAll.toArray(new String[auxAll.size()]);
			int ids_size = ids.length;
			FieldComboItem[] ids_ci = new FieldComboItem[ids_size];

			for (int i = 0; i < ids_size; i++) {
				ids_ci[i] = new FieldComboItem(ids[i]);
			}


			int geos_size = geo_atts.size();
			FieldComboItem[] geos_ci = new FieldComboItem[geos_size];

			for (int i = 0; i < geos_size; i++) {
			    dbattr = (FeatureAttributeDescriptor) geo_atts.get(i);
				geos_ci[i] = new FieldComboItem(dbattr.getName(), dbattr.getSRS());
			}

			tableSettingsPanel = new UserTableSettingsVectorialPanel(ids_ci,
					geos_ci,
					tableName, mc, false, (WizardVectorialDB) parent,
					getParameters(),
					currentProjection(espView,
							ids_ci, geos_ci));
		}

		return tableSettingsPanel;
	}

    private void showNoPkWarning(String tableName) {
        
        String msg = Messages.getText(
            "_Table_NAME_does_not_have_declared_PK", new String[] { tableName } );
        msg = msg + "\n\n";
        msg = msg + Messages.getText(
            "_If_layer_edited_saving_edits_will_not_be_possible");
        msg = msg + "\n";
        msg = msg + Messages.getText(
            "_Strange_behavior_if_selected_ID_not_unique_values");
        msg = msg + "      ";
        
        JOptionPane.showMessageDialog(
            ApplicationLocator.getManager().getRootComponent(),
            msg,
            Messages.getText("_Id_field"),
            JOptionPane.WARNING_MESSAGE);
        
    }

    public void setEnabledPanels(boolean b) {
    	super.setEnabledPanels(b);
		tableSettingsPanel.enableSpatialControls(b);
		tableSettingsPanel.enableAlphaControls(b);
	}

}
