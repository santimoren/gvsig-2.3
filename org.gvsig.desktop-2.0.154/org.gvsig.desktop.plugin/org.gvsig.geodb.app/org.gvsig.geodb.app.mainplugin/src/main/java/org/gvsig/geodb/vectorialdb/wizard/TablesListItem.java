/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geodb.vectorialdb.wizard;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JCheckBox;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.addlayer.AddLayerDialog;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCConnectionParameters;



/**
 * Utility class that represents a table list item as a selectable check box.
 *
 * @author jldominguez
 *
 */
public class TablesListItem extends JCheckBox {
    
    private static Logger logger = LoggerFactory.getLogger(TablesListItem.class);
    
    protected String tableName = "";
    protected String schemaName = "";
    
    private UserSelectedFieldsPanel selectedFieldsPanel = null;
    protected WizardDB parent = null;
    private boolean activated = false;
    // private CRSSelectPanel jPanelProj;
	protected DBStoreParameters parameters;
	protected UserTableSettingsPanel tableSettingsPanel = null;

    public TablesListItem(DBStoreParameters param,
			WizardDB _parent) {
        
        tableName = param.getTable();
        schemaName = getSchema(param);
        
        setText(toString());
        this.parameters = param;
        parent = _parent;
    }
    


    /**
     * @param param
     * @return
     */
    private String getSchema(DBStoreParameters dynobj) {
        
        String resp = null;
        try {
            resp = (String) dynobj.getDynValue(
                JDBCConnectionParameters.SCHEMA_PARAMTER_NAME);
        } catch (Exception ex) {
            // logger.info("did not find schema in DB parameters.");
        }
        return resp;
    }

    public void activate() {
        activated = true;
        selectedFieldsPanel.loadValues();
        tableSettingsPanel.loadValues();
    }

    public boolean isActivated() {
        return activated;
    }

    /**
     * Tells whether this item prevents the wizard from being in a valid final state.
     * @return whether this item prevents the wizard from being in a valid final state.
     */
    public boolean disturbsWizardValidity() {
        if (isSelected()) {
            return (!hasValidValues());
        }
        else {
            return false;
        }
    }

    private boolean hasValidValues() {
        return tableSettingsPanel.hasValidValues();
    }

    public String toString() {
        if (schemaName == null || schemaName.length() == 0) {
            return tableName;
        } else {
            return schemaName + "." + tableName;
        }
        
    }

    public String getTableName() {
        return tableName;
    }

    public void setEnabledPanels(boolean b) {
        selectedFieldsPanel.enableControls(b);
        tableSettingsPanel.enableAlphaControls(b);
        tableSettingsPanel.enableSpatialControls(b);

    }

    public UserSelectedFieldsPanel getUserSelectedFieldsPanel() {
        if (selectedFieldsPanel == null) {
        	FeatureType ft=null;
			try {
				ft = this.parent.getServerExplorer().getFeatureType(parameters);
			} catch (DataException e) {
				NotificationManager.addError(e);
				return null;
			}
			ArrayList<FeatureAttributeDescriptor> attList = new ArrayList<FeatureAttributeDescriptor>();

			Iterator<FeatureAttributeDescriptor> iter = ft.iterator();
			while (iter.hasNext()) {
				attList.add(iter.next());
			}

        	FeatureAttributeDescriptor[] allf = attList
					.toArray(new FeatureAttributeDescriptor[0]);


            selectedFieldsPanel = new UserSelectedFieldsPanel(allf, false,
                    parent);
        }

        return selectedFieldsPanel;
    }

    public UserTableSettingsPanel getUserTableSettingsPanel() {
		if (tableSettingsPanel == null) {

			String[] ids = new String[0];
			FeatureType ft = null;

			try {
				ft = this.parent.getServerExplorer().getFeatureType(parameters);
			} catch (DataException e) {
				NotificationManager.addError(e);
				return null;
			}

			ArrayList auxAll = new ArrayList();
			ArrayList auxId = new ArrayList();
			Iterator iter = ft.iterator();
			while (iter.hasNext()) {
				FeatureAttributeDescriptor dbattr = (FeatureAttributeDescriptor) iter
						.next();
				if (dbattr.isPrimaryKey()) {
					auxId.add(dbattr.getName());
				}
				auxAll.add(dbattr.getName());

			}

			if (auxId.size() > 0) {
				StringBuilder strb = new StringBuilder();
				strb.append('{');
				for (int i = 0; i < auxId.size()-1; i++) {
					strb.append(auxId.get(i));
					strb.append(',');
				}
				strb.append(auxId.get(auxId.size() - 1));

				strb.append('}');
				if (auxId.size() == 1) {
					auxAll.remove(auxId.get(0));
				}
				auxAll.add(0, strb.toString());
			}
			ids = (String[]) auxAll.toArray(new String[auxAll.size()]);
			int ids_size = ids.length;
			FieldComboItem[] ids_ci = new FieldComboItem[ids_size];

			for (int i = 0; i < ids_size; i++) {
				ids_ci[i] = new FieldComboItem(ids[i]);
			}



			tableSettingsPanel = new UserTableSettingsPanel(ids_ci, tableName,
					true, parent, getParameters());
		}

		return tableSettingsPanel;
	}


    public IProjection currentProjection(String espView,
			FieldComboItem[] ids_ci, FieldComboItem[] geos_ci) {
		IProjection proj = AddLayerDialog.getLastProjection();
		try {
			ArrayList list = new ArrayList(1);
			list.add(espView);
			FeatureType ft = null;
			try {
				ft = this.parent.getServerExplorer().getFeatureType(parameters);
			} catch (DataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {
			NotificationManager.addInfo("Incorrect projection", e);
		}
		return proj;
	}

    public DBStoreParameters getParameters() {
    	return parameters;
    }
}
