/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.snappers;

import java.awt.geom.Point2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.CuadrantHandler;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapperVectorial;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.impl.AbstractSnapper;
import org.gvsig.i18n.Messages;


/**
 * Quadrant point snapper.
 *
 * @author Vicente Caballero Navarro
 */
public class QuadrantPointSnapper extends AbstractSnapper
    implements ISnapperVectorial {
	/* (non-Javadoc)
     * @see com.iver.cit.gvsig.gui.cad.snapping.ISnapper#getSnapPoint(Point2D point,
     * IGeometry geom,double tolerance, Point2D lastPointEntered)
     */
    public Point2D getSnapPoint(Point2D point, Geometry geom,
        double tolerance, Point2D lastPointEntered) {
        Point2D resul = null;

        Handler[] handlers = geom.getHandlers(Geometry.SELECTHANDLER);

        double minDist = tolerance;

        for (int j = 0; j < handlers.length; j++) {
            if (handlers[j] instanceof CuadrantHandler) {
                Point2D handlerPoint = handlers[j].getPoint();
                double dist = handlerPoint.distance(point);

                if ((dist < minDist)) {
                    resul = handlerPoint;
                    minDist = dist;
                }
            }
        }

        return resul;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.gui.cad.snapping.ISnapper#getToolTipText()
     */
    public String getToolTipText() {
        return Messages.getText("Quadrant_point");
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.gui.cad.snapping.ISnapper#draw(java.awt.Graphics, java.awt.geom.Point2D)
     */
    public void draw(PrimitivesDrawer primitivesDrawer, Point2D pPixels) {
    	primitivesDrawer.setColor(getColor());

        int half = getSizePixels() / 2;
        int x1 = (int) (pPixels.getX() - half);
        int x2 = (int) (pPixels.getX() + half);
        int x3 = (int) pPixels.getX();
        int y1 = (int) (pPixels.getY() - half);
        int y2 = (int) (pPixels.getY() + half);
        int y3 = (int) pPixels.getY();

        primitivesDrawer.drawLine(x1, y3, x3, y1);
        primitivesDrawer.drawLine(x1, y3, x3, y2);
        primitivesDrawer.drawLine(x2, y3, x3, y1);
        primitivesDrawer.drawLine(x2, y3, x3, y2);
    }
}
