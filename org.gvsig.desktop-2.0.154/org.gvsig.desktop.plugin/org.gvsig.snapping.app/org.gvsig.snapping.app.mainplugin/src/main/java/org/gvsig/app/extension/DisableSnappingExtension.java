/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.dynobject.DynObject;


/**
 * @author jldominguez
 *
 */
public class DisableSnappingExtension extends Extension {

    public void initialize() {
        // TODO Auto-generated method stub

    }

    public void execute(String actionCommand) {
        if (actionCommand.equals("edit-disable-snapping")) {

            PluginsManager pluginManager = PluginsLocator.getManager();
            PluginServices plugin = pluginManager.getPlugin(this);
            DynObject pluginProperties = plugin.getPluginProperties();

            IView view = getActiveView();
            MapControl mc = view.getMapControl();
            mc.setRefentEnabled(false);
            pluginProperties.setDynValue("applySnappers", mc.isRefentEnabled());
            plugin.savePluginProperties();
            ApplicationLocator.getManager().refreshMenusAndToolBars();
        }

    }

    public boolean isEnabled() {
        IView view = getActiveView();
        if (view != null) {
            MapControl mc = view.getMapControl();
            if (mc != null) {
                FLyrVect layer = getActiveLayer(view);
                return (mc.isRefentEnabled() && layer != null && layer.isEditing());
            }
        }
        return false;
    }

    public boolean isVisible() {
        return isEnabled();
    }

    private IView getActiveView() {
        ApplicationManager application = ApplicationLocator.getManager();
        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        return view;
    }

    private FLyrVect getActiveLayer(IView vista) {
        if (vista != null) {
            ViewDocument viewDocument = vista.getViewDocument();
            FLayer[] actives =
                viewDocument.getMapContext().getLayers().getActives();

            if ((actives.length == 1) && (actives[0] instanceof FLyrVect)) {
                return (FLyrVect) actives[0];
            }
        }
        return null;
    }

}
