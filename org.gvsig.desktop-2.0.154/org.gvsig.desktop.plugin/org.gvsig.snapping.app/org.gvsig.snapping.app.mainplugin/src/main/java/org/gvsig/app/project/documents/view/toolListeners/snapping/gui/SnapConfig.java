/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.gvsig.andami.PluginServices;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author fjp
 *
 * Necesitamos un sitio donde est�n registrados todos los snappers que
 * se pueden usar. ExtensionPoints es el sitio adecuado.
 * Este di�logo recuperar� esa lista para que el usuario marque los
 * snappers con los que desea trabajar.
 */
public class SnapConfig extends JPanel {

	/**
     *
     */
    private static final long serialVersionUID = 3539711525292306297L;
    private JCheckBox jChkBoxRefentActive = null;
	private JTable jListSnappers = null;
	private JPanel jPanel = null;
	private JScrollPane jScrollPane = null;
    private JPanel jPanelTolerance = null;
    private JTextField jTxtTolerance = null;
    private JLabel jLabelTolerance = null;
    private JLabel jLabelTolerance1 = null;

	private static MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

	/**
	 * @author fjp
	 * primera columna editable con un check box para habilitar/deshabilitar el snapper
	 * segunda columna con el s�mbolo del snapper
	 * tercera con el tooltip
	 * cuarta con un bot�n para configurar el snapper si es necesario.
	 */
	class MyTableModel extends AbstractTableModel {

		/**
         *
         */
        private static final long serialVersionUID = 6262877908586753756L;

        public MyTableModel()
		{

		}

		public int getColumnCount() {
			return 5;
		}

		public int getRowCount() {
			return mapControlManager.getSnapperCount();
		}

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			if (columnIndex == 0 || columnIndex == 3)
				return true;
			return false;
		}

		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			ISnapper snap = mapControlManager.getSnapperAt(rowIndex);
			switch (columnIndex)
			{
			case 0://CheckBox
				snap.setEnabled(((Boolean)aValue).booleanValue());
				break;
			case 3://Prioridad
				snap.setPriority(((Integer)aValue).intValue());
				break;
			}
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			ISnapper snap = mapControlManager.getSnapperAt(rowIndex);
			switch (columnIndex)
			{
			case 0:
				return new Boolean(snap.isEnabled());
			case 1:
				return snap.getClass().getName();
			case 2:
				return snap.getToolTipText();
			case 3:
				return new Integer(snap.getPriority());
			case 4:
				return new JButton();
			}
			return null;
		}

		public Class getColumnClass(int columnIndex) {
			switch (columnIndex)
			{
			case 0:
				return Boolean.class;
			case 1:
				return String.class;
			case 2:
				return String.class;
			case 3:
				return Integer.class;
			case 4:
				return JButton.class;
			}
			return null;
		}

		public String getColumnName(int columnIndex) {
			switch (columnIndex){
			case 0:
				return PluginServices.getText(this,"aplicar");
			case 1:
				return PluginServices.getText(this,"simbolo");
			case 2:
				return PluginServices.getText(this,"tipo");
			case 3:
				return PluginServices.getText(this,"prioridad");
			case 4:
				return PluginServices.getText(this,"propiedades");
			}
			return null;
		}

	}

	class MyCellRenderer extends JCheckBox implements ListCellRenderer {

		// This is the only method defined by ListCellRenderer.
		// We just reconfigure the JLabel each time we're called.

		public Component getListCellRendererComponent(
				JList list,
				Object value,            // value to display
				int index,               // cell index
				boolean isSelected,      // is the cell selected
				boolean cellHasFocus)    // the list and the cell have the focus
		{
			ISnapper snapper = (ISnapper) value;
			String s = snapper.getToolTipText();
			setText(s);

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			}
			else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			setEnabled(list.isEnabled());
			setFont(list.getFont());
			setOpaque(true);
			return this;
		}

		public void doClick() {
			super.doClick();
			System.out.println("Click");
		}


	}


    //FIXME: sobra el par�metro mc
	/**
	 * This method initializes
	 *
	 */
	public SnapConfig(MapControl mc) {
		super();
//		this.mc=mc;
		initialize();
	}


	//FIXME: sobra el par�metro mc
	/**
	 * This constructor allows an aditional parameter
	 * which will be notified of the mouse events
	 * in the relevant components of this panel
	 *
	 * @param mc
	 * @param mouseListener the mouse listener to be notified
	 */
	public SnapConfig(MapControl mc, MouseListener mouseListener) {
	    this(mc);
	    getJListSnappers().addMouseListener(mouseListener);
	    getJChkBoxRefentActive().addMouseListener(mouseListener);
	}

	   /**
     * This constructor allows two additional parameters
     * which will be notified of the mouse or key events
     * in the relevant components of this panel
     *
     * @param mouseListener the mouse listener to be notified
     * @param keyListener the key listener to be notified
     */
    public SnapConfig(MouseListener mouseListener, KeyListener keyListener) {
        this(null, mouseListener);
        getJTxtTolerance().addKeyListener(keyListener);
    }

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.setSize(new java.awt.Dimension(463,239));
		this.setPreferredSize(new java.awt.Dimension(463,239));
		//FIXME: crear un panel NORTH para meter estos los controles JChkBoxRefentActive y JPanelTolerance
		this.add(getJChkBoxRefentActive(), BorderLayout.NORTH);
		jLabelTolerance = new JLabel();
		jLabelTolerance.setText(i18nManager.getTranslation("snap_tolerance"));
		jLabelTolerance.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		jLabelTolerance.setName("jLabel");
		jLabelTolerance.setBounds(new java.awt.Rectangle(15, 8, 122, 15));
		jLabelTolerance.setPreferredSize(new java.awt.Dimension(28, 20));
		jLabelTolerance.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jLabelTolerance1 = new JLabel();
        jLabelTolerance1.setText(i18nManager.getTranslation("pixels"));
        jLabelTolerance1.setBounds(new java.awt.Rectangle(195, 8, 207, 15));
        jLabelTolerance1.setPreferredSize(new java.awt.Dimension(28, 20));
        jLabelTolerance1.setName("jLabel1");
        this.add(getJPanelTolerance(), BorderLayout.SOUTH);
        this.add(getJPanel(), BorderLayout.CENTER);

	}

	/**
	 * This method initializes jChkBoxRefentActive
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getJChkBoxRefentActive() {
		if (jChkBoxRefentActive == null) {
                        I18nManager i18nManager = ToolsLocator.getI18nManager();
			jChkBoxRefentActive = new JCheckBox();
			jChkBoxRefentActive.setText(i18nManager.getTranslation("activate_reference_to_objects"));
			jChkBoxRefentActive.setBounds(new java.awt.Rectangle(26,10,418,23));
		}
		return jChkBoxRefentActive;
	}

	   /**
     * This method initializes jTxtTolerance
     *
     * @return javax.swing.JTextField
     */
    private JTextField getJTxtTolerance() {
        if (jTxtTolerance == null) {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            if (jLabelTolerance == null) {
                jLabelTolerance = new JLabel();
                jLabelTolerance.setText(i18nManager
                    .getTranslation("snap_tolerance"));
                jLabelTolerance
                    .setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
                jLabelTolerance.setName("jLabel");
                jLabelTolerance
                    .setBounds(new java.awt.Rectangle(15, 8, 122, 15));
                jLabelTolerance
                    .setPreferredSize(new java.awt.Dimension(28, 20));
                jLabelTolerance
                    .setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
            }
            if (jLabelTolerance1 == null) {
                jLabelTolerance1 = new JLabel();
                jLabelTolerance1.setText(i18nManager.getTranslation("pixels"));
                jLabelTolerance1.setBounds(new java.awt.Rectangle(195, 8, 207,
                    15));
                jLabelTolerance1
                    .setPreferredSize(new java.awt.Dimension(28, 20));
                jLabelTolerance1.setName("jLabel1");
            }

            jTxtTolerance = new JTextField();
            jTxtTolerance.setPreferredSize(new java.awt.Dimension(28, 20));
            jTxtTolerance.setName("jTxtTolerance");
            jTxtTolerance.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
            jTxtTolerance.setBounds(new java.awt.Rectangle(142, 8, 39, 15));
        }
        return jTxtTolerance;
    }

    /**
     * This method initializes jPanelNord
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJPanelTolerance() {
        if (jPanelTolerance == null) {
            jPanelTolerance = new JPanel();
            jPanelTolerance.setLayout(null);
            jPanelTolerance
                    .setComponentOrientation(java.awt.ComponentOrientation.UNKNOWN);
            jPanelTolerance.setPreferredSize(new java.awt.Dimension(30, 30));
            jPanelTolerance.add(jLabelTolerance, null);
            jPanelTolerance.add(getJTxtTolerance(), null);
            jPanelTolerance.add(jLabelTolerance1, null);

        }
        return jPanelTolerance;
    }


	/**
	 * This method initializes jListSnappers
	 *
	 * @return javax.swing.JList
	 */
	private JTable getJListSnappers() {
		if (jListSnappers == null) {
			jListSnappers = new JTable();
			// jListSnappers.setCellRenderer(new MyCellRenderer());
		}
		return jListSnappers;
	}

	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BorderLayout());
			jPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
			jPanel.setBounds(new java.awt.Rectangle(19,40,423,181));
			jPanel.add(getJScrollPane(), BorderLayout.CENTER);
		}
		return jPanel;
	}

	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(new java.awt.Rectangle(9,9,402,163));
			jScrollPane.setViewportView(getJListSnappers());
		}
		return jScrollPane;
	}

	public void setSnappers() {
		MyTableModel listModel = new MyTableModel();
		getJListSnappers().setModel(listModel);
		TableColumn tc=getJListSnappers().getColumnModel().getColumn(0);
		setUpSymbolColumn(getJListSnappers().getColumnModel().getColumn(1));
		setUpPropertyColumn(getJListSnappers().getColumnModel().getColumn(4));
		getJListSnappers().setCellSelectionEnabled(false);
		tc.setMaxWidth(40);
		tc.setMinWidth(20);
	}
	public TableModel getTableModel() {
		return getJListSnappers().getModel();
	}
	public boolean applySnappers() {
		return getJChkBoxRefentActive().isSelected();
	}


	public void selectSnappers() {
		for (int i=0;i<mapControlManager.getSnapperCount();i++) {
			getTableModel().setValueAt(mapControlManager.getSnapperAt(i).isEnabled(),i,0);
		}

	}

	public Integer getSnapTolerance() {
        return Integer.valueOf(getJTxtTolerance().getText());
	}

	public void setSnapTolerance(Integer snapTolerance) {
	        getJTxtTolerance().setText(snapTolerance.toString());
	    }

	public void setApplySnappers(boolean applySnappers) {
		getJChkBoxRefentActive().setSelected(applySnappers);
	}

	public void setUpSymbolColumn(TableColumn column) {
		DrawSnapCellRenderer symbolCellRenderer = new DrawSnapCellRenderer();
		column.setCellRenderer(symbolCellRenderer);
	}
	public void setUpPropertyColumn(TableColumn column) {

		PropertySnapCellEditor propertyeditor = new PropertySnapCellEditor();
		column.setCellEditor(propertyeditor);

		PropertySnapCellRenderer renderer = new PropertySnapCellRenderer();
		column.setCellRenderer(renderer);
	}
}


