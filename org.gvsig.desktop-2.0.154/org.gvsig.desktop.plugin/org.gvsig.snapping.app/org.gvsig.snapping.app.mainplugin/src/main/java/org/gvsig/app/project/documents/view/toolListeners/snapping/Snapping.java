/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping;


import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.CentralPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.FinalPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.IntersectionPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.MediumPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.NearestPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.PerpendicularPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.PixelSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.QuadrantPointSnapper;
import org.gvsig.app.project.documents.view.toolListeners.snapping.snappers.TangentPointSnapper;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.impl.DefaultMapControlLibrary;
import org.gvsig.fmap.mapcontrol.impl.DefaultMapControlManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;


/**
 * DOCUMENT ME!
 *
 * @author Vicente Caballero Navarro
 */
public class Snapping {
    /**
     * DOCUMENT ME!
     */
    public static void register() {    	//TODO delete when the system to load libraries is finished
    	if (MapControlLocator.getMapControlManager() == null){
    		MapControlLocator.registerMapControlManager(DefaultMapControlManager.class);
    	}
    	MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
    	    	
    	mapControlManager.registerSnapper("FinalPointSnapper", FinalPointSnapper.class);
    	mapControlManager.registerSnapper("NearestPointSnapper", NearestPointSnapper.class);
    	mapControlManager.registerSnapper("PixelSnapper", PixelSnapper.class);
    	mapControlManager.registerSnapper("CentralPointSnapper", CentralPointSnapper.class);
    	mapControlManager.registerSnapper("QuadrantPointSnapper", QuadrantPointSnapper.class);

    	// mapControlManager.registerSnapper("InsertPointSnapper", new InsertPointSnapper.class);
    	
        mapControlManager.registerSnapper("IntersectionPointSnapper", IntersectionPointSnapper.class);
    	mapControlManager.registerSnapper("MediumPointSnapper", MediumPointSnapper.class);
    	mapControlManager.registerSnapper("PerpendicularPointSnapper", PerpendicularPointSnapper.class);
    	mapControlManager.registerSnapper("TangentPointSnapper", TangentPointSnapper.class);
    }
}
