/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.geom.Point2D;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.gvsig.fmap.mapcontrol.impl.DefaultPrimitivesDrawer;

/**
 * Cell Renderer del s�mbolo de cada uno de los ISnapper.
 *
 * @author Vicente Caballero Navarro
 */
public class DrawSnapCellRenderer extends JPanel implements TableCellRenderer {
	private static Point2D position = new Point2D.Double(7,7);
	private static MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
	private PrimitivesDrawer primitivesDrawer = new DefaultPrimitivesDrawer();
	
	private int row;
	public DrawSnapCellRenderer() {
		this.setBackground(Color.white);
	}
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		this.row=row;
		return this;
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		primitivesDrawer.setGraphics(g);
		
		(mapControlManager.getSnapperAt(row)).draw(primitivesDrawer, position);
	}


}
