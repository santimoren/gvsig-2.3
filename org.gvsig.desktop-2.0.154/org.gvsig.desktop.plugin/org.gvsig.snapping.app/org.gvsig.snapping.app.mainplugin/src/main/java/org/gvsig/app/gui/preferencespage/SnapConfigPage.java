/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.preferencespage;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.app.project.documents.view.toolListeners.snapping.gui.SnapConfig;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.i18n.I18nManager;




/**
 * Preferencias de snapping.
 *
 * @author Vicente Caballero Navarro
 */
public class SnapConfigPage extends AbstractPreferencePage {
    /**
     *
     */
    private static final long serialVersionUID = -3288863201776046550L;

    private static final Logger logger = LoggerFactory.getLogger(SnapConfigPage.class);

    public static final String ID = SnapConfigPage.class.getName();


    private SnapConfig snapConfig;
	private static boolean applySnappers=true;
    private static MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

    private DynObject pluginProperties;
    private PluginServices plugin;

    public SnapConfigPage() {
//        super();
        PluginsManager pluginManager = PluginsLocator.getManager();
        this.plugin = pluginManager.getPlugin(this);
        this.pluginProperties = this.plugin.getPluginProperties();

        this.snapConfig = new SnapConfig(new MouseListener() {
            public void mouseReleased(MouseEvent e) {
            }
            public void mousePressed(MouseEvent e) {
                setChanged(true);
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            }
        }, new KeyListener() {
            public void keyTyped(KeyEvent e) {
                setChanged(true);
            }
            public void keyReleased(KeyEvent e) {
                setChanged(true);
            }
            public void keyPressed(KeyEvent e) {
                setChanged(true);
            }
        });

        this.setLayout(new BorderLayout());
        this.add(this.snapConfig, BorderLayout.NORTH);
        this.snapConfig.setSnappers();
        initializeValues();
    }



    /**
     * DOCUMENT ME!
     *
     * @throws StoreException DOCUMENT ME!
     */
    public void storeValues() throws StoreException {
    	for (int n = 0; n < mapControlManager.getSnapperCount(); n++) {
            Boolean b = (Boolean) snapConfig.getTableModel().getValueAt(n, 0);
            ISnapper snp = (ISnapper) mapControlManager.getSnapperAt(n);
            String nameClass=snp.getClass().getName();
            nameClass=nameClass.substring(nameClass.lastIndexOf('.')+1);
            this.pluginProperties.setDynValue("snapperActivated" + nameClass, b.booleanValue());

            if (b.booleanValue()){
            	snp.setEnabled(b);
            }
            this.pluginProperties.setDynValue("snapperPriority" + nameClass, snp.getPriority());
        }
    	Boolean b = (Boolean)snapConfig.applySnappers();
        this.pluginProperties.setDynValue("applySnappers", b.booleanValue());
        applySnappers=b;

        Integer snapTolerance = snapConfig.getSnapTolerance();
        this.pluginProperties.setDynValue("snapTolerance", snapTolerance.intValue());
        mapControlManager.setTolerance(snapTolerance);

        this.plugin.savePluginProperties();
    }

    /**
     * DOCUMENT ME!
     */
    public void setChangesApplied() {
    	setChanged(false);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getID() {
        return ID;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("Snapping");
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public JPanel getPanel() {
        return this;
    }

    /**
     * DOCUMENT ME!
     */
    public void initializeValues() {
        //FIXME: �Es necesario esto? Creo que no, la gestion de preferencias nueva ya incorpora los valores por defecto
        try {
            Boolean applySnappers = (Boolean) pluginProperties.getDynValue("applySnappers");
        } catch (DynFieldNotFoundException e){
            initializeDefaults();
        }

        snapConfig.setApplySnappers(applySnappers);

        for (int n = 0; n < mapControlManager.getSnapperCount(); n++) {
            // Boolean b = (Boolean) snapConfig.getTableModel().getValueAt(n, 0);
            ISnapper snp = (ISnapper) mapControlManager.getSnapperAt(n);
            String nameClass = snp.getClass().getName();
            nameClass = nameClass.substring(nameClass.lastIndexOf('.')+1);
            Boolean active = (Boolean) pluginProperties.getDynValue("snapperActivated"+nameClass);
            snapConfig.getTableModel().setValueAt(active.booleanValue(),n,0);
            Integer prior = (Integer) pluginProperties.getDynValue("snapperPriority"+nameClass);
            snapConfig.getTableModel().setValueAt(prior, n, 3);
        }
      Integer snapTolerance = (Integer) pluginProperties.getDynValue("snapTolerance");
      snapConfig.setSnapTolerance(snapTolerance);

    }

    /**
     * DOCUMENT ME!
     */
    public void initializeDefaults() {
    	for (int n = 0; n < mapControlManager.getSnapperCount(); n++) {
            ISnapper snp = mapControlManager.getSnapperAt(n);
            String nameClass=snp.getClass().getName();
            nameClass=nameClass.substring(nameClass.lastIndexOf('.')+1);
            if (nameClass.equals(".FinalPointSnapper")){
            	int priority = 1;
            	//FIXME: �Por qu� hay que a�adir esta l�nea?
//                this.pluginProperties.setDynValue("snapperPriority" + nameClass, priority);
            	snp.setEnabled(true);
            	snp.setPriority(priority);
            }else if (nameClass.equals(".NearestPointSnapper")){
            	int priority = 2;
                //FIXME: �Por qu� hay que a�adir esta l�nea?
//                this.pluginProperties.setDynValue("snapperPriority" + nameClass, priority);
            	snp.setEnabled(true);
            	snp.setPriority(priority);
            }else{
            	int priority = 3;
                //FIXME: �Por qu� hay que a�adir esta l�nea?
//                this.pluginProperties.setDynValue("snapperPriority" + nameClass, priority);
            	snp.setEnabled(false);
            	snp.setPriority(priority);
            }
        }
        applySnappers = true;
        snapConfig.setApplySnappers(applySnappers);
        snapConfig.selectSnappers();

        this.pluginProperties.setDynValue("snapTolerance", 4);
        snapConfig.setSnapTolerance(4);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ImageIcon getIcon() {
        return IconThemeHelper.getImageIcon("preferences-snapper");
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isValueChanged() {
    	return super.hasChanged();
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.andami.preferences.AbstractPreferencePage#isResizeable()
     */
	public boolean isResizeable() {
		return true;
	}



}
