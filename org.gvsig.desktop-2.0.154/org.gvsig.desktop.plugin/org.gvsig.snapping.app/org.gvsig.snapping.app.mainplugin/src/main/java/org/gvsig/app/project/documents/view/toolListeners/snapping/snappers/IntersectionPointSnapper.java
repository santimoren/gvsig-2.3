/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.snappers;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.Point;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapperGeometriesVectorial;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.impl.AbstractSnapper;
import org.gvsig.i18n.Messages;


/**
 * Intersection point snapper.
 *
 * @author Vicente Caballero Navarro
 */
public class IntersectionPointSnapper extends AbstractSnapper
    implements ISnapperGeometriesVectorial {
    private static final Logger LOG = LoggerFactory.getLogger(IntersectionPointSnapper.class);
    
    public static final int MAX_GEOMETRIES_IN_RELAXED_LIST = 30;
    public static final int MAX_GEOMETRIES_WITHIN_TOLERANCE = 5;
    
	private List relaxedList = null;
	private GeometryFactory jtsGeoFact = null;
	private long errorMsgCount = 0;

    public Point2D getSnapPoint(
        Point2D point,
        Geometry geom,
        double tolerance,
        Point2D lastPointEntered) {
        
        if (geom == null) {
            return null;
        }
        
        if (relaxedList == null
            || relaxedList.size() < 2 /* We need 2 to intersect */
            || relaxedList.size() > MAX_GEOMETRIES_IN_RELAXED_LIST) {
            /*
             * If there are a lot of geometries, we assume the user
             * is not interested in snapping now. The relaxed list
             * might be long because the spatial index used perhaps
             * is "lazy" so the value of MAX_GEOMETRIES_IN_RELAXED_LIST
             * has to be not very small
             */
            return null;
        }
        
        List<Geometry> refinedList = null;
        try {
            refinedList = refineList(relaxedList, point, tolerance);
        } catch (Exception e1) {
            errorMsgCount++;
            if (errorMsgCount % 100 == 0) {
                /*
                 * Prevents too many lines in logger. If this happens,
                 * it will happen many times, so we'll see it in the log file.
                 */
                LOG.info("Error while refining list: " + e1.getMessage());
                errorMsgCount = 0;
            }
            return null;
        }
        
        if (refinedList.size() > MAX_GEOMETRIES_WITHIN_TOLERANCE) {
            /*
             * This refined list contains the geometries that are
             * close to the current mouse. Again we use an upper limit
             * to prevent the case where the user is in a zoom where
             * he is not interested in snapping.
             */
            return null;
        }
        
        List<Point> interPoints = getInterPoints(refinedList);
        
        if (interPoints.size() == 0) {
            return null;
        }
        
        com.vividsolutions.jts.geom.Geometry jtsg = null;
        try {
            jtsg = (com.vividsolutions.jts.geom.Geometry) geom.invokeOperation("toJTS", null);
        } catch (Exception e) {
            errorMsgCount++;
            if (errorMsgCount % 100 == 0) {
                /*
                 * Prevents too many lines in logger. If this happens,
                 * it will happen many times, so we'll see it in the log file.
                 */
                LOG.info("Error while refining list: " + e.getMessage());
                errorMsgCount = 0;
            }
            return null;
        }
        
        double zerodist = 0.001 * tolerance;
        int n = interPoints.size();
        Point po = null;
        for (int i=0; i<n; i++) {
            po = interPoints.get(i);
            if (point.distance(po.getX(), po.getY()) < tolerance) {
                // This is a candidate, now we check that it belongs to the
                // current geometry
                if (jtsg.distance(po) < zerodist) {
                    return new Point2D.Double(po.getX(), po.getY());
                }
            }
        }
        return null;
    }


    private List<Geometry> refineList(
        List geomlist,
        Point2D point,
        double tolerance) throws Exception {
        
        List<Geometry> resp = new ArrayList<Geometry>();
        if (geomlist == null || geomlist.size() == 0) {
            return resp;
        }
        
        double point_tol_north = point.getY() + tolerance; 
        double point_tol_south = point.getY() - tolerance;
        double point_tol_east = point.getX() + tolerance;
        double point_tol_west = point.getX() - tolerance;
        
        Geometry item = null;
        Envelope env = null;
        Iterator iter = geomlist.iterator();
        while (iter.hasNext()) {
            item = (Geometry) iter.next();
            
            if (item.intersects(
                point_tol_west, point_tol_south,
                point_tol_east - point_tol_west,
                point_tol_north - point_tol_south)) {
                
                resp.add(item);
            }
        }
        return resp;
    }


    public void draw(PrimitivesDrawer primitivesDrawer, Point2D pPixels) {
    	primitivesDrawer.setColor(getColor());

        int half = getSizePixels() / 2;
        int x1 = (int) (pPixels.getX() - half);
        int x2 = (int) (pPixels.getX() + half);
        int y1 = (int) (pPixels.getY() - half);
        int y2 = (int) (pPixels.getY() + half);

        primitivesDrawer.drawLine(x1, y1, x2, y2);
        primitivesDrawer.drawLine(x1, y2, x2, y1);
    }

    public String getToolTipText() {
        return Messages.getText("Intersection_point_snapper");
    }

	public void setGeometries(List geoms) {
	    relaxedList = leaveUsefulGeometries(geoms);
	}
	
	private List<Point> getInterPoints(List geomes) {
	    
	    List<Point> resp = new ArrayList<Point>();
	    List borders = getJTSBorders(geomes);
	    com.vividsolutions.jts.geom.Geometry jtsg1 = null;
	    com.vividsolutions.jts.geom.Geometry jtsg2 = null;
	    com.vividsolutions.jts.geom.Geometry jtsinter = null;
	    int n = borders.size();
	    Point jtsp = null;
	    MultiPoint jtsmp = null;
	    
	    for (int i=0; i<n; i++) {
	        for (int j=0; j<i; j++) {
	            if (i != j) {
	                jtsg1 = (com.vividsolutions.jts.geom.Geometry) borders.get(i);
	                jtsg2 = (com.vividsolutions.jts.geom.Geometry) borders.get(j);
	                jtsinter = jtsg1.intersection(jtsg2);
	                if (jtsinter instanceof Point) {
	                    jtsp = (Point) jtsinter;
	                    resp.add(jtsp);
	                } else {
	                    if (jtsinter instanceof MultiPoint) {
	                        
	                        jtsmp = (MultiPoint) jtsinter;
	                        int m = jtsmp.getNumGeometries();
	                        for (int k=0; k<m; k++) {
	                            jtsp = (Point) jtsmp.getGeometryN(k);
	                            resp.add(jtsp);
	                        }
	                    }
	                }
	            }
	        }
	    }
	    return resp;
	}
	

    private List getJTSBorders(List gvsigGeoms) {
        
        List resp = new ArrayList();
        com.vividsolutions.jts.geom.Geometry jtsborder = null;
        
        if (gvsigGeoms != null && gvsigGeoms.size() > 0) {
            
            Iterator iter = gvsigGeoms.iterator();
            Object itemobj = null;
            
            while (iter.hasNext()) {
                itemobj = iter.next();
                if (itemobj instanceof Geometry) {
                    jtsborder = getJTSBorder((Geometry) itemobj);
                    if (jtsborder != null) {
                        resp.add(jtsborder);
                    }
                }
            }
        }
        return resp;
    }

    private com.vividsolutions.jts.geom.Geometry getJTSBorder(Geometry geom) {
        
        com.vividsolutions.jts.geom.Geometry resp = null;
        
        if (geom instanceof Curve || geom instanceof MultiCurve) {
            
            try {
                resp = (com.vividsolutions.jts.geom.Geometry)
                    geom.invokeOperation("toJTS", null);
            } catch (Exception e) {
                errorMsgCount++;
                if (errorMsgCount % 100 == 0) {
                    /*
                     * Prevents too many lines in logger. If this happens,
                     * it will happen many times, so we'll see it in the log file.
                     */
                    LOG.info("Error while refining list: " + e.getMessage());
                    errorMsgCount = 0;
                }
                return null;
            }
            return resp;
            
        } else {
            if (geom instanceof Surface || geom instanceof MultiSurface) {
                
                try {
                    resp = (com.vividsolutions.jts.geom.Geometry)
                        geom.invokeOperation("toJTS", null);
                    return resp.getBoundary();
                } catch (Exception e) {
                    
                    errorMsgCount++;
                    if (errorMsgCount % 100 == 0) {
                        /*
                         * Prevents too many lines in logger. If this happens,
                         * it will happen many times, so we'll see it in the log file.
                         */
                        LOG.info("Error while refining list: " + e.getMessage());
                        errorMsgCount = 0;
                    }
                    return null;
                }
                
            } else {
                return null;
            }
        }
        
    }
  
    
    private List<Geometry> leaveUsefulGeometries(List geoms) {
        
        List resp = new ArrayList();
        if (geoms == null || geoms.size() == 0) {
            return resp;
        }
        
        Object item = null;
        Iterator iter = geoms.iterator();
        while (iter.hasNext()) {
            item = iter.next();
            if (item instanceof Curve || item instanceof Surface
                || item instanceof MultiCurve || item instanceof MultiSurface) {
                resp.add(item);
            }
        }
        return resp;
    }        

}
