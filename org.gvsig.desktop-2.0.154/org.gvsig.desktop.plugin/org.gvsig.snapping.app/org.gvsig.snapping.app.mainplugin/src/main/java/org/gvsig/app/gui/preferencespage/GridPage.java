/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.preferencespage;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.grid.Grid;
import org.gvsig.tools.dynobject.DynObject;


public class GridPage extends AbstractPreferencePage {
    public static final String ID = GridPage.class.getName();

	private org.gvsig.fmap.mapcontrol.MapControl mapControl;
	private JCheckBox chkShowGrid;
	private JCheckBox chkAdjustGrid;
	private JTextField txtDistanceX;
	private JTextField txtDistanceY;
	private JLabel lblUnits=new JLabel();
	private ImageIcon icon;

    private DynObject pluginProperties;
    private PluginServices plugin;



	public GridPage() {
		super();
        PluginsManager pluginManager = PluginsLocator.getManager();
        this.plugin = pluginManager.getPlugin(this);
        this.pluginProperties = this.plugin.getPluginProperties();

		icon = IconThemeHelper.getImageIcon("grid-preferences");

		chkShowGrid=new JCheckBox(PluginServices.getText(this,"mostrar_rejilla"));
		addComponent(chkShowGrid);
		chkAdjustGrid=new JCheckBox(PluginServices.getText(this,"ajustar_rejilla"));
		addComponent(chkAdjustGrid);
		addComponent(lblUnits);

		// distance x
		addComponent(PluginServices.getText(this, "distance_x") + ":",
			txtDistanceX = new JTextField("", 15));
		// distance y
		addComponent(PluginServices.getText(this, "distance_y") + ":",
			txtDistanceY = new JTextField("", 15));
//		setParentID(ViewPage.id);

	}

	public void initializeValues() {
        boolean showGrid = ((Boolean)pluginProperties.getDynValue("showgrid")).booleanValue();
        boolean adjustGrid = ((Boolean)pluginProperties.getDynValue("adjustgrid")).booleanValue();

		double dx = ((Double)pluginProperties.getDynValue("griddistancex")).doubleValue();
        double dy = ((Double)pluginProperties.getDynValue("griddistancey")).doubleValue();

		Grid.SHOWGRID=showGrid;
		Grid.ADJUSTGRID=adjustGrid;
		Grid.GRIDSIZEX=dx;
		Grid.GRIDSIZEY=dy;

		chkShowGrid.setSelected(showGrid);
		chkAdjustGrid.setSelected(adjustGrid);
		txtDistanceX.setText(String.valueOf(dx));
		txtDistanceY.setText(String.valueOf(dy));
	}

	public String getID() {
		return ID;
	}

	public String getTitle() {
		return PluginServices.getText(this, "Grid");
	}

	public JPanel getPanel() {
		return this;
	}

	public void storeValues() throws StoreException {
		boolean showGrid;
		boolean adjustGrid;
		double dx;
		double dy;

			showGrid=chkShowGrid.isSelected();
			adjustGrid=chkAdjustGrid.isSelected();
		try{
			dx=Double.parseDouble(txtDistanceX.getText());
			dy=Double.parseDouble(txtDistanceY.getText());
		}catch (Exception e) {
			throw new StoreException(PluginServices.getText(this,"distancia_malla_incorrecta"));
		}

        this.pluginProperties.setDynValue("showgrid", showGrid);
        this.pluginProperties.setDynValue("adjustgrid", adjustGrid);
        this.pluginProperties.setDynValue("griddistancex", dx);
        this.pluginProperties.setDynValue("griddistancey", dy);


		Grid.SHOWGRID=showGrid;
		Grid.ADJUSTGRID=adjustGrid;
		Grid.GRIDSIZEX=dx;
		Grid.GRIDSIZEY=dy;

		IWindow window=PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof DefaultViewPanel){
			MapControl mc=((DefaultViewPanel)window).getMapControl();
			Grid cadGrid=mc.getGrid();
			cadGrid.setShowGrid(showGrid);
			cadGrid.setAdjustGrid(adjustGrid);
			cadGrid.setGridSizeX(dx);
			cadGrid.setGridSizeY(dy);
		}

	}

	public void initializeDefaults() {
		chkShowGrid.setSelected(Grid.DefaultShowGrid);
		chkAdjustGrid.setSelected(Grid.DefaultAdjustGrid);
		txtDistanceX.setText(String.valueOf(Grid.DefaultGridSizeX));
		txtDistanceY.setText(String.valueOf(Grid.DefaultGridSizeY));
	}

	public ImageIcon getIcon() {
            return icon;
        }

	public boolean isValueChanged() {
		return super.hasChanged();
	}

	public void setChangesApplied() {
		setChanged(false);
	}
}
