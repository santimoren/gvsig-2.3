/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.snappers;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapperVectorial;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.impl.AbstractSnapper;
import org.gvsig.i18n.Messages;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;


/**
 * Perpendicular point snapper.
 *
 * @author Vicente Caballero Navarro
 */
public class PerpendicularPointSnapper extends AbstractSnapper
    implements ISnapperVectorial {
	/* (non-Javadoc)
     * @see com.iver.cit.gvsig.gui.cad.snapping.ISnapper#getSnapPoint(Point2D point,
     * IGeometry geom,double tolerance, Point2D lastPointEntered)
     */
    public Point2D getSnapPoint(Point2D point, Geometry geom,
        double tolerance, Point2D lastPointEntered) {
        Point2D resul = null;
        Coordinate c = new Coordinate(point.getX(), point.getY());

        if (lastPointEntered == null) {
            return null;
        }

        Coordinate cLastPoint = new Coordinate(lastPointEntered.getX(),
                lastPointEntered.getY());
        PathIterator theIterator = geom.getPathIterator(null,
                geomManager.getFlatness()); //polyLine.getPathIterator(null, flatness);
        double[] theData = new double[6];
        double minDist = tolerance;
        Coordinate from = null;
        Coordinate first = null;

        while (!theIterator.isDone()) {
            //while not done
            int theType = theIterator.currentSegment(theData);

            switch (theType) {
            case PathIterator.SEG_MOVETO:
                from = new Coordinate(theData[0], theData[1]);
                first = from;

                break;

            case PathIterator.SEG_LINETO:

                // System.out.println("SEG_LINETO");
                Coordinate to = new Coordinate(theData[0], theData[1]);
                LineSegment line = new LineSegment(from, to);
                Coordinate closestPoint = line.closestPoint(cLastPoint);
                double dist = c.distance(closestPoint);

                if (!(line.getCoordinate(0).equals2D(closestPoint) ||
                        line.getCoordinate(1).equals2D(closestPoint))) {
                    if ((dist < minDist)) {
                        resul = new Point2D.Double(closestPoint.x,
                                closestPoint.y);
                        minDist = dist;
                    }
                }

                from = to;

                break;

            case PathIterator.SEG_CLOSE:
                line = new LineSegment(from, first);
                closestPoint = line.closestPoint(cLastPoint);
                dist = c.distance(closestPoint);

                if (!(line.getCoordinate(0).equals2D(closestPoint) ||
                        line.getCoordinate(1).equals2D(closestPoint))) {
                    if ((dist < minDist)) {
                        resul = new Point2D.Double(closestPoint.x,
                                closestPoint.y);
                        minDist = dist;
                    }
                }

                from = first;

                break;
            } //end switch

            theIterator.next();
        }

        return resul;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.gui.cad.snapping.ISnapper#getToolTipText()
     */
    public String getToolTipText() {
        return Messages.getText("Perpendicular_point");
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper#draw(org.gvsig.fmap.mapcontrol.PrimitivesDrawer, java.awt.geom.Point2D)
     */
    public void draw(PrimitivesDrawer primitivesDrawer, Point2D pPixels) {
    	primitivesDrawer.setColor(getColor());

        int half = getSizePixels() / 2;
        int x1 = (int) (pPixels.getX() - half);
        int x2 = (int) (pPixels.getX() + half);
        int x3 = (int) pPixels.getX();
        int y1 = (int) (pPixels.getY() - half);
        int y2 = (int) (pPixels.getY() + half);
        int y3 = (int) pPixels.getY();

        primitivesDrawer.drawLine(x1, y2, x2, y2);
        primitivesDrawer.drawLine(x1, y2, x1, y1);
        primitivesDrawer.drawLine(x1, y3, x3, y3);
        primitivesDrawer.drawLine(x3, y3, x3, y2);
    }

}
