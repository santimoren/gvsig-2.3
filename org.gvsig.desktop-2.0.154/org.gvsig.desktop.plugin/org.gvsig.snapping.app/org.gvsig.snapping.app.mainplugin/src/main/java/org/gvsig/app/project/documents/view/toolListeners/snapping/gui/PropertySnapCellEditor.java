/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;


public class PropertySnapCellEditor extends JButton implements TableCellEditor{
	private int row;
	private IWindow panel;
	private static MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
	
	public PropertySnapCellEditor() {		
		this.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount()==2) {
					if (panel!=null)
						openConfigurePanel();
				}
			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});
	}
//	class WinConfigure extends JPanel implements IWindow {
//
//		private WindowInfo wi=null;
//
//		public WindowInfo getWindowInfo() {
//			if (wi==null) {
//				wi=new WindowInfo(WindowInfo.MODALDIALOG|WindowInfo.RESIZABLE);
//				wi.setWidth(panel.getWidth());
//				wi.setHeight(panel.getHeight());
//				wi.setTitle(PluginServices.getText(this,"propiedades"));
//			}
//			return wi;
//		}
//	}
	private void openConfigurePanel() {
		//IWindow window=new WinConfigure();
		((DefaultConfigurePanel)panel).setSnapper(mapControlManager.getSnapperAt(row));
		PluginServices.getMDIManager().addWindow(panel);
	}
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		this.row=row;
		panel=(IWindow)(mapControlManager.getSnapperAt(row)).getConfigurator();
		if (panel!=null) {
			this.setEnabled(true);
		}else {
			this.setEnabled(false);
			this.setBackground(Color.white);
		}
		return this;
	}

	public void cancelCellEditing() {
		// TODO Auto-generated method stub

	}

	public boolean stopCellEditing() {
		// TODO Auto-generated method stub
		return false;
	}

	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	public boolean shouldSelectCell(EventObject anEvent) {
		// TODO Auto-generated method stub
		return false;
	}

	public void addCellEditorListener(CellEditorListener l) {
		// TODO Auto-generated method stub

	}

	public void removeCellEditorListener(CellEditorListener l) {
		// TODO Auto-generated method stub

	}

}
