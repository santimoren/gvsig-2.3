/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.view.toolListeners.snapping.snappers;

import java.awt.geom.Point2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.mapcontrol.PrimitivesDrawer;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapperVectorial;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.impl.AbstractSnapper;
import org.gvsig.i18n.Messages;


public class FinalPointSnapper extends AbstractSnapper implements ISnapperVectorial {
	public Point2D getSnapPoint(Point2D point, Geometry geom, double tolerance, Point2D lastPointEntered) {
		Point2D resul = null;
		Handler[] handlers = geom.getHandlers(Geometry.SELECTHANDLER);
		//		Point2D initPoint=handlers[0].getPoint();
		//		Point2D endPoint=handlers[handlers.length-1].getPoint();
		//		if (initPoint.equals(endPoint))
		//			return resul;

		double minDist = tolerance;
		//		double dist = initPoint.distance(point);
		//		if (dist < minDist) {
		//			resul = initPoint;
		//			minDist = dist;
		//		}
		//
		//		dist = endPoint.distance(point);
		//		if (dist < minDist) {
		//			resul = endPoint;
		//		}

		for (int j = 0; j < handlers.length; j++) {
			Point2D handlerPoint = handlers[j].getPoint();
			double dist = handlerPoint.distance(point);
			if ((dist < minDist)) {
				resul = handlerPoint;
				minDist = dist;
			}
		}

		return resul;
	}

	public String getToolTipText() {
	    return Messages.getText("Final_point");
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper#draw(org.gvsig.fmap.mapcontrol.PrimitivesDrawer, java.awt.geom.Point2D)
	 */
	public void draw(PrimitivesDrawer primitivesDrawer, Point2D pPixels) {
		primitivesDrawer.setColor(getColor());

		int half = getSizePixels() / 2;
		primitivesDrawer.drawRect((int) (pPixels.getX() - half),
				(int) (pPixels.getY() - half),
				getSizePixels(), getSizePixels());
	}
}
