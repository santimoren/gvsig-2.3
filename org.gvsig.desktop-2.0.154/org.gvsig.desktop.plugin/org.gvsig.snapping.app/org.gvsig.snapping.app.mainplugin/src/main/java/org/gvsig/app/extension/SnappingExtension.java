/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.app.gui.preferencespage.GridPage;
import org.gvsig.app.gui.preferencespage.SnapConfigPage;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.app.project.documents.view.gui.ViewSnappingPropertiesPageFactory;
import org.gvsig.app.project.documents.view.toolListeners.snapping.Snapping;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.fmap.mapcontrol.tools.snapping.snappers.ISnapper;
import org.gvsig.propertypage.PropertiesPageManager;

/**
 * Extension for config the snapping
 *
 * @author fdiaz
 *
 */
public class SnappingExtension extends Extension implements IPreferenceExtension {

    private IPreference[] preferencePages = null;
    
    public void initialize() {
        Snapping.register();

        registerIcons();

        MapControlManager mapControlManager = MapControlLocator.getMapControlManager();
        for (int n = 0; n < mapControlManager.getSnapperCount(); n++) {
            ISnapper snp = (ISnapper) mapControlManager.getSnapperAt(n);
            String nameClass = snp.getClass().getName();
            nameClass = nameClass.substring(nameClass.lastIndexOf('.') + 1);
            snp.setEnabled((Boolean) getPlugin().getPluginProperties().getDynValue("snapperActivated" + nameClass));
            snp.setPriority((Integer) getPlugin().getPluginProperties().getDynValue("snapperPriority" + nameClass));
        }

        Integer snapTolerance
                = (Integer) getPlugin().getPluginProperties().getDynValue("snapTolerance");
        mapControlManager.setTolerance(snapTolerance.intValue());

        Boolean applySnappers
                = (Boolean) getPlugin().getPluginProperties().getDynValue("applySnappers");
        //TODO: si se a�ade al MapControlManager para habilitar/deshabilitar de manera global los snappers activos, invocarlo aqu�
//        mapControlManager.applySnappers(applySnappers);

        PropertiesPageManager manager = MapControlLocator.getPropertiesPageManager();
        manager.registerFactory(new ViewSnappingPropertiesPageFactory());

    }

    @Override
    public void postInitialize() {

    }

    private void registerIcons() {
        IconThemeHelper.registerIcon("preferences", "preferences-snapper", this);
        IconThemeHelper.registerIcon("preferences", "grid-preferences", this);
        IconThemeHelper.registerIcon("actions", "enable-snapping", this);
        IconThemeHelper.registerIcon("actions", "disable-snapping", this);
    }

    public void execute(String actionCommand) {
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {

        org.gvsig.andami.ui.mdiManager.IWindow f
                = PluginServices.getMDIManager().getActiveWindow();

        if (f != null && f instanceof DefaultViewPanel) {
            DefaultViewPanel vista = (DefaultViewPanel) f;
            ViewDocument model = vista.getViewDocument();
            MapContext mapa = model.getMapContext();

            FLayers capas = mapa.getLayers();

            int numActiveVectorial = 0;
            int numActiveVectorialEditable = 0;

            LayersIterator iter = new LayersIterator(capas);

            FLayer capa;
            while (iter.hasNext()) {
                capa = iter.nextLayer();
                if (capa instanceof FLyrVect && capa.isActive()
                        && capa.isAvailable()) {
                    numActiveVectorial++;
                    if (capa.isEditing()) {
                        numActiveVectorialEditable++;
                    }
                }

            }

            if (numActiveVectorialEditable == 1 && numActiveVectorial == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public IPreference[] getPreferencesPages() {
        if( this.preferencePages == null ) {
            this.preferencePages = new IPreference[] {
                new SnapConfigPage(),
                new GridPage()
            };
        }
        return this.preferencePages;
    }
}
