/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.Color;
import java.awt.event.InputEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.cresques.cts.ICoordTrans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.IEditionManager;
import org.gvsig.editing.gui.cad.CADTool;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.SelectionCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.SelectionCADToolContext.SelectionCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class SelectionCADTool extends DefaultCADTool {

    private static final Logger LOG = LoggerFactory
        .getLogger(SelectionCADTool.class);

    protected SelectionCADToolContext _fsm;

    protected Point2D firstPoint;

    protected String nextState;
    
    // These two arrays are used to save the current selected features and
    // geometries. The selected geometries contains an instance of the
    // geometry that can be reprojected.
    protected ArrayList selectedGeometries = new ArrayList();
    protected ArrayList selectedFeatures = new ArrayList(); 
    
    protected String type = PluginServices.getText(this, "simple");

    protected boolean multipleSelection = false;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new SelectionCADToolContext(this);
        setNextTool("selection");
        setType(PluginServices.getText(this, "simple"));
    }

    public void transition(double x, double y, InputEvent event) {
        LOG.debug("TRANSICION DESDE ESTADO {}", _fsm.getState() + " x= " + x
            + " y=" + y);
        try {
            _fsm.addPoint(x, y, event);
        } catch (Exception e) {
            init();
            PluginServices.getMDIManager().restoreCursor();
        }
        LOG.debug("ESTADO ACTUAL: ", getStatus());
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    public String getNextState() {
        return nextState;
    }

    protected void pointDoubleClick(MapContext map) throws ReadException {
        FLayer[] actives = map.getLayers().getActives();
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param selection
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        SelectionCADToolState actualState =
            (SelectionCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        LOG.debug("PREVIOUSSTATE = {}", status);
        VectorialLayerEdited vle = getVLE();
        try {
            LOG.debug("STATUS ACTUAL = {}", _fsm.getTransition());
            if (status.equals("Selection.FirstPoint")) {
                firstPoint = new Point2D.Double(x, y);
            } else
                if (status.equals("Selection.SecondPoint")) {
                } else
                    if (status.equals("Selection.WithFeatures")) {
                    } else
                        if (status.equals("Selection.WithHandlers")) {
                            addPointWithHandlers(x, y, vle.getFeatureStore(),
                                vle.getSelectedHandler());
                        }
        } catch (DataException e) {
            LOG.error("Error reding the store", e);
        }
    }

    /**
     * AddPoint method for the WithHandlers option
     * 
     * @param x
     *            selected x coordinate.
     * @param y
     *            selected y coordinate.
     * @param featureStore
     *            the selected feature store.
     * @param selectedHandlers
     *            the selected handlers
     */
    protected void addPointWithHandlers(double x, double y,
        FeatureStore featureStore, ArrayList selectedHandlers) {
        String description = PluginServices.getText(this, "move_handlers");
        DisposableIterator iterator = null;
        
        IEditionManager ed_man = this.getEditionManager();
        try {
            featureStore.beginEditingGroup(description);
            ICoordTrans coordTrans = getVLE().getLayer().getCoordTrans();
            for (int i=0 ; i<selectedFeatures.size() ; i++){
                Feature feature = (Feature) selectedFeatures.get(i);
                
                Geometry changing_geometry = (Geometry)selectedGeometries.get(i);
                Geometry old_geometry = changing_geometry.cloneGeometry();
                
                // Draw the moved geometry. It has been reprojected in the
                // select handler method
                for (int k = 0; k < selectedHandlers.size(); k++) {
                    Handler h = (Handler) selectedHandlers.get(k);
                    h.set(x, y);
                }                            
                             
                //If the layer is reprojected, It is necessary
                //to reproject the geometry
                if (coordTrans != null) {
                    changing_geometry = changing_geometry.cloneGeometry();
                    changing_geometry.reProject(coordTrans.getInverted());
                    // ==========================
                    // this is already an independent clone
                    old_geometry.reProject(coordTrans.getInverted());
                }              
                
                // restore old geometry in feature
                // this is because selectedGeometries and
                // selectedFeature point
                // at the same geometries at least sometimes
                EditableFeature eFeature = feature.getEditable();
                eFeature.setDefaultGeometry(old_geometry);
                
                Feature old_geom_feature = eFeature.getNotEditableCopy();
                
                // set new (old remains as source for undoing)
                eFeature = old_geom_feature.getEditable();
                eFeature.setDefaultGeometry(changing_geometry);
                
                ed_man.updateFeature(featureStore, eFeature);
            }

            firstPoint = new Point2D.Double(x, y);

            featureStore.endEditingGroup();
        } catch (DataException e1) {
            LOG.error("Error reading the store", e1);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    /**
     * Receives second point
     * 
     * @param x
     * @param y
     * @return numFeatures selected
     */
    public long selectWithSecondPoint(double x, double y, InputEvent event) {
        VectorialLayerEdited vle = getVLE();
        PluginServices.getMDIManager().setWaitCursor();
        vle.selectWithSecondPoint(x, y);
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) vle.getFeatureStore().getSelection();
            PluginServices.getMDIManager().restoreCursor();
            long countSel = selection.getSize();
            if (countSel > 0) {
                nextState = "Selection.WithSelectedFeatures";
            } else {
                nextState = "Selection.FirstPoint";
            }
            return countSel;
        } catch (ReadException e) {
            e.printStackTrace();
            return 0;
        } catch (DataException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        SelectionCADToolState actualState = _fsm.getState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        if (vle == null) {
            return;
        }
        ArrayList selectedHandler = vle.getSelectedHandler();
        ViewPort vp = vle.getLayer().getMapContext().getViewPort();
        if (status.equals("Selection.SecondPoint")) {
            // Dibuja el rect�ngulo de selecci�n
            Curve curve = createCurve();
            curve.addMoveToVertex(createPoint(firstPoint.getX(),
                firstPoint.getY()));
            curve.addVertex(createPoint(x, firstPoint.getY()));
            curve.addVertex(createPoint(x, y));
            curve.addVertex(createPoint(firstPoint.getX(), y));
            curve.addVertex(createPoint(firstPoint.getX(), firstPoint.getY()));

            renderer
                .draw(curve, mapControlManager.getGeometrySelectionSymbol());

            return;
        } else
            //If there is a handler to move...
            if (status.equals("Selection.WithHandlers")) {
                drawWithHandlers(renderer, x, y, selectedHandler);
            }
    }
    
    /**
     * Draw method to draw a geometry with handlers
     * 
     * @param mapControlDrawer
     *            object used to draw.
     * @param x
     *            selected x coordinate.
     * @param y
     *            selected y coordinate.
     * @param selectedHandlers
     *            the selected handlers
     */
    protected void drawWithHandlers(MapControlDrawer mapControlDrawer, double x,
        double y, ArrayList selectedHandlers) {
        // Moving the handlers that have been selected
        // in the selectHandlers method
        double xPrev = 0;
        double yPrev = 0;
        for (int k = 0; k < selectedHandlers.size(); k++) {
            Handler h = (Handler) selectedHandlers.get(k);
            xPrev = h.getPoint().getX();
            yPrev = h.getPoint().getY();
            h.set(x, y);
        }
       
        // Draw the moved geometry. It has been reprojected in the
        // select handler method
        for (int i = 0; i < selectedGeometries.size(); i++) {
            Geometry geom = (Geometry) selectedGeometries.get(i);

            mapControlDrawer.setColor(Color.gray);
            mapControlDrawer.draw(geom,
                mapControlManager.getAxisReferenceSymbol());
        }
        for (int k = 0; k < selectedHandlers.size(); k++) {
            Handler h = (Handler) selectedHandlers.get(k);
            h.set(xPrev, yPrev);
        } 
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        SelectionCADToolState actualState =
            (SelectionCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        LOG.debug("PREVIOUSSTATE = {}", status);
        LOG.debug("STATUS ACTUAL = {}", _fsm.getTransition());
        if (s.equals(PluginServices.getText(this, "cancel"))) {
            init();
            return;
        }
        if (status.equals("Selection.FirstPoint")) {
            setType(s);
            return;
        }
        init();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    public String getStatus() {
        try {
            SelectionCADToolState actualState =
                (SelectionCADToolState) _fsm.getPreviousState();
            String status = actualState.getName();

            return status;
        } catch (NullPointerException e) {
            return "Selection.FirstPoint";
        }
    }

    public void end() {
        if (!getNextTool().equals("selection")) {
            CADExtension.setCADTool(getNextTool(), false);
        }
    }

    public String getName() {
        return PluginServices.getText(this, "selection_");
    }

    public boolean selectFeatures(double x, double y, InputEvent event) {
        SelectionCADToolState actualState = _fsm.getState();

        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        multipleSelection = event.isControlDown();

        if ((status.equals("Selection.FirstPoint"))
            || (status.equals("Selection.WithSelectedFeatures"))) {
            PluginServices.getMDIManager().setWaitCursor();
            firstPoint = new Point2D.Double(x, y);
            try {
                vle.getFeatureStore().beginEditingGroup(getName());
                vle.selectWithPoint(x, y, multipleSelection);
                vle.getFeatureStore().endEditingGroup();
            } catch (NeedEditingModeException e) {
                NotificationManager.showMessageError(getName(), e);
            } catch (ReadException e) {
                NotificationManager.showMessageError(getName(), e);
            }
            PluginServices.getMDIManager().restoreCursor();
        }
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) vle.getFeatureStore().getSelection();
            long countSel = selection.getSize();
            if (countSel > 0) {
                nextState = "Selection.WithSelectedFeatures";
                return true;
            } else {
                {
                    nextState = "Selection.SecondPoint";
                    return true;
                }
            }
        } catch (ReadException e) {
            LOG.error("Error selecting the features", e);
            return false;
        } catch (DataException e) {
            LOG.error("Error selecting the features", e);
            return false;
        }
    }

    public int selectHandlers(double x, double y, InputEvent event) {
        Point2D auxPoint = new Point2D.Double(x, y);

        VectorialLayerEdited vle = getVLE();
        
        //Gets the current selected handlers 
        ArrayList selectedHandler = vle.getSelectedHandler();
        FeatureSet selection = null;
        DisposableIterator iterator = null;
        try {
            selection = (FeatureSet) vle.getFeatureStore().getSelection();

            long countSel = selection.getSize();
            LOG.debug("DENTRO DE selectHandlers. selectedRow.size = {}",
                countSel);
            selectedHandler.clear();

            //Clear the previous selection
            clearCurrentSelection();
            
            // Se comprueba si se pincha en una gemometr�a
            PluginServices.getMDIManager().setWaitCursor();

            double tam =
                getCadToolAdapter().getMapControl().getViewPort()
                    .toMapDistance(mapControlManager.getTolerance());

            Handler[] handlers = null;
            
            
            selectedGeometries.clear();
            iterator = selection.fastIterator();
            ICoordTrans coordTrans = getVLE().getLayer().getCoordTrans();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                
                //If the layer is reprojected, the tolerance is not applicable. It is necessary
                //to reproject the geometry
                Geometry geometry = feature.getDefaultGeometry();
                if (coordTrans != null){
                    geometry = geometry.cloneGeometry();
                    geometry.reProject(coordTrans);
                }
                
                handlers = geometry.getHandlers(Geometry.SELECTHANDLER);

                // y miramos los handlers de cada entidad seleccionada
                double min = tam;

                for (int j = 0; j < handlers.length; j++) {
                    Point2D handlerPoint = handlers[j].getPoint();
                    double distance = auxPoint.distance(handlerPoint);
                    if (distance <= min) {
                        min = distance;
                        selectedHandler.add(handlers[j]);
                        selectedGeometries.add(geometry);
                        selectedFeatures.add(feature);
                    }
                }
            }
            PluginServices.getMDIManager().restoreCursor();
        } catch (DataException e) {
            LOG.error("Error reading the store", e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }

        return selectedHandler.size();
    }
    
    protected void clearCurrentSelection(){
        selectedFeatures.clear();
        selectedGeometries.clear();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type.equals("S") || type.equals("s")) {
            this.type = PluginServices.getText(this, "simple");
        } else {
            this.type = type;
        }
    }

    public String toString() {
        return "_selection";
    }

    public void multipleSelection(boolean b) {
        multipleSelection = b;

    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }
    
    /**
     * Tells whether the tool provided is an instance of this class
     * 
     * @param tool
     * @param exactly_this requires that the object is an instance of
     * this class and not an instance of a subclass
     *  
     * @return 
     */
    public static boolean isInstance(CADTool tool, boolean exactly_this) {
        
        if (exactly_this) {
            return tool.getClass() == SelectionCADTool.class;
        } else {
            return (tool instanceof SelectionCADTool);
        }
        
    }
}
