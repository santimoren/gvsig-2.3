/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.awt.KeyEventPostProcessor;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.text.JTextComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.gui.cad.CADTool;
import org.gvsig.editing.gui.cad.CADToolAdapter;
import org.gvsig.editing.gui.cad.tools.CopyCADTool;
import org.gvsig.editing.gui.cad.tools.RotateCADTool;
import org.gvsig.editing.gui.cad.tools.ScaleCADTool;
import org.gvsig.editing.gui.cad.tools.SymmetryCADTool;
import org.gvsig.editing.gui.preferences.EditingPage;
import org.gvsig.editing.project.documents.view.toc.MenuEntry;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.utils.console.JConsole;
import org.gvsig.utils.console.ResponseListener;
import org.gvsig.utils.console.jedit.JEditTextArea;


/**
 * Extensi�n dedicada a controlar las diferentes operaciones sobre el editado de
 * una capa.
 *
 * @author Vicente Caballero Navarro
 */
public class CADExtension extends Extension implements IPreferenceExtension{
	private static Logger logger = LoggerFactory.getLogger(CADExtension.class);

	private static DefaultViewPanel view;
	private MapControl mapControl;
	private EditingPage editingPage = null;


	/**
     * Gets the CADToolAdapter of the view where the layer is
     * contained or null if the layer is not found
     * in any view
	 *
	 * @param layer
	 * @return
	 *
     * @deprecated Use same method in {@link EditionLocator}
	 */
	public static CADToolAdapter getCADToolAdapter(FLayer layer) {

	    return EditionLocator.getCADToolAdapter(layer);
	}

	/**
	 * Gets the CADToolAdapter of the current active view
	 * or the CADToolAdapter of the view that was active more
	 * recently (if there is not an active view)
	 * or null if there never was an active view
	 *
	 * @return
	 *
     * @deprecated Use same method in {@link EditionLocator}
	 */
	public static CADToolAdapter getCADToolAdapter() {

	    return EditionLocator.getCADToolAdapter();
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {

		// Registramos los Popup menus:
        MenuEntry.register();

		CopyCADTool copy = new CopyCADTool();
		RotateCADTool rotate = new RotateCADTool();
		ScaleCADTool scale = new ScaleCADTool();
		SymmetryCADTool symmetry=new SymmetryCADTool();
		addCADTool("_copy", copy);
		addCADTool("_rotate", rotate);
		addCADTool("_scale", scale);
		addCADTool("_symmetry", symmetry);

		KeyboardFocusManager kfm = KeyboardFocusManager
				.getCurrentKeyboardFocusManager();
		kfm.addKeyEventPostProcessor(new myKeyEventPostProcessor());

		registerIcons();
	}

	@Override
    public void postInitialize() {
        //Register the edition symbology
        registerEditionSymbology();
    }

    /**
     * It registers the default symbology used in edition
     */
    private void registerEditionSymbology() {
        PluginsManager pluginsManager = PluginsLocator.getManager();
        DynObject dynObject = pluginsManager.getPlugin(getClass()).getPluginProperties();

        MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

        if (dynObject.hasDynValue(EditingPage.DEFAULT_AXIS_REFERENCES_SYMBOL)){
            mapControlManager.setAxisReferenceSymbol(
                (ISymbol)dynObject.getDynValue(EditingPage.DEFAULT_AXIS_REFERENCES_SYMBOL));
        }

        if (dynObject.hasDynValue(EditingPage.DEFAULT_RECTANGLE_SELECTION_SYMBOL)) {
            mapControlManager.setGeometrySelectionSymbol(
                (ISymbol)dynObject.getDynValue(EditingPage.DEFAULT_RECTANGLE_SELECTION_SYMBOL));
        }

        if (dynObject.hasDynValue(EditingPage.DEFAULT_HANDLER_SYMBOL)) {
            mapControlManager.setHandlerSymbol(
                (ISymbol)dynObject.getDynValue(EditingPage.DEFAULT_HANDLER_SYMBOL));
        }
    }

    private void registerIcons(){
    	IconThemeHelper.registerIcon("action", "layer-modify-copy", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-symmetry", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-rotate", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-scale", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-edit-vertex", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-join", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-matrix", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-move", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-rotate", this);
    	IconThemeHelper.registerIcon("action", "layer-modify-stretch", this);


    	IconThemeHelper.registerIcon("preferences", "preferences-snapper", this);
    	IconThemeHelper.registerIcon("preferences", "editing-properties", this);

	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String s) {
		initFocus();
    	if( "layer-modify-copy".equalsIgnoreCase(s) ) {
    		setCADTool("_copy", true);
    	} else if( "layer-modify-symmetry".equalsIgnoreCase(s) ) {
    		setCADTool("_symmetry", true);
    	} else if( "layer-modify-rotate".equalsIgnoreCase(s) ) {
    		setCADTool("_rotate", true);
    	} else if( "layer-modify-scale".equalsIgnoreCase(s) ) {
    		setCADTool("_scale", true);

    		// -------------------------------
    		// The following action commands are not declared
    		// in the section of config.xml dedicated to CADExtension.
    		// They are used in other extensions of this plugin.
    		// -------------------------------
    		/*
    	} else if( "layer-modify-edit-vertex".equalsIgnoreCase(s) ) {
    		setCADTool("_editvertex", true);
    	} else if( "layer-modify-join".equalsIgnoreCase(s) ) {
    		setCADTool("_join", true);
    	} else if( "layer-modify-matrix".equalsIgnoreCase(s) ) {
    		setCADTool("_matriz", true);
    	} else if( "layer-modify-move".equalsIgnoreCase(s) ) {
    		setCADTool("_move", true);
    	} else if( "layer-modify-stretch".equalsIgnoreCase(s) ) {
    		setCADTool("_stretch", true);
    		*/

    	} else {
    		logger.info("Action command '"+s+"' not supportted in "+this.getClass().getName());
    	}
		CADToolAdapter cta=getCADToolAdapter();
		cta.configureMenu();
	}

	public static void addCADTool(String name, CADTool c) {
		CADToolAdapter.addCADTool(name, c);
	}

	public static void setCADTool(String text, boolean showCommand) {
		CADToolAdapter cta=getCADToolAdapter();
		CADTool ct=cta.getCADTool(text);

		if (ct == null)
			throw new RuntimeException("No such cad tool");
		cta.initializeFlatness();
		cta.setCadTool(ct);
		ct.init();
		if (showCommand) {
			if (PluginServices.getMDIManager().getActiveWindow() instanceof DefaultViewPanel) {
				DefaultViewPanel vista = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
				vista.getConsolePanel().addText("\n" + ct.getName(),
					JConsole.COMMAND);
				cta.askQuestion();
			}
		}
		// PluginServices.getMainFrame().setSelectedTool("SELECT");
		// PluginServices.getMainFrame().enableControls();
	}

	public static CADTool getCADTool() {
		CADToolAdapter cta=getCADToolAdapter();
		return cta.getCadTool();
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
		// initFocus();
		return true;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE) {
			view = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
			mapControl = view.getMapControl();
			FLayer[] layers = mapControl.getMapContext().getLayers()
					.getActives();
//			if (!(layers[0] instanceof FLyrAnnotation)) {
//				return true;
//			}
			return true;
		}
		return false;
	}

	public MapControl getMapControl() {
		return getEditionManager().getMapControl();
	}

	class KeyAction extends AbstractAction {

		private String key;

		public KeyAction(String key) {
			this.key = key;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			view.focusConsole(key);
		}

	}

	class MyAction extends AbstractAction {
		private String actionCommand;

		public MyAction(String command) {
			actionCommand = command;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			CADToolAdapter cta=getCADToolAdapter();
			cta.keyPressed(actionCommand);
		}

	}

	/**
	 * @author fjp
	 *
	 * La idea es usar esto para recibir lo que el usuario escribe y enviarlo a
	 * la consola de la vista para que salga por all�.
	 */
	private class myKeyEventPostProcessor implements KeyEventPostProcessor {

		public boolean postProcessKeyEvent(KeyEvent e) {
			// System.out.println("KeyEvent e = " + e);
			CADToolAdapter cta=getCADToolAdapter();
			if ((cta == null) || (view == null))
				return false;

			if (cta.getMapControl() == null){
				return false;
			}

			if (e.getID() != KeyEvent.KEY_RELEASED)
				return false;
			if (!(e.getComponent() instanceof JTextComponent)) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE)
					cta.keyPressed("eliminar");
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					cta.keyPressed("escape");
				else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					// TODO: REVISAR ESTO CUANDO VIENE UN INTRO DESDE UN
					// JTEXTAREA
					// QUE NO ES EL DE CONSOLA
					if (!(e.getSource() instanceof JTable))
						view.focusConsole("");
				}
				else if ((!e.isActionKey() && e.getKeyCode()!=KeyEvent.VK_TAB)) {
						//if (Character.isLetterOrDigit(e.getKeyChar())) {
							Character keyChar = new Character(e.getKeyChar());
							if (e.getComponent().getName() != null) {
							    /*
								System.out
										.println("Evento de teclado desde el componente "
												+ e.getComponent().getName());
								*/
								if (!e.getComponent().getName().equals(
										"CADConsole")) {
									view.focusConsole(keyChar + "");
								}
							} else {
							    /*
							     * Send content of key event to the editing console
							     * unless it comes from a JTable (probably the attribute
							     * table)
							     */
								if (!(e.getComponent() instanceof JTable)) {
									view.focusConsole(keyChar + "");
								}
							}
						//}
					}
				}
			return false;
		}

	}

	/*
	 * private void registerKeyStrokes(){ for (char key = '0'; key <= '9';
	 * key++){ Character keyChar = new Character(key);
	 * mapControl.getInputMap(MapControl.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(key),
	 * keyChar); mapControl.getActionMap().put(keyChar, new
	 * KeyAction(keyChar+"")); } for (char key = 'a'; key <= 'z'; key++){
	 * Character keyChar = new Character(key);
	 * mapControl.getInputMap(MapControl.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(key),
	 * keyChar); mapControl.getActionMap().put(keyChar, new
	 * KeyAction(keyChar+"")); } for (char key = 'A'; key <= 'Z'; key++){
	 * Character keyChar = new Character(key);
	 * mapControl.getInputMap(MapControl.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(key),
	 * keyChar); mapControl.getActionMap().put(keyChar, new
	 * KeyAction(keyChar+"")); }
	 * //this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,
	 * 0), "enter"); //this.getActionMap().put("enter", new MyAction("enter"));
	 * Character keyChar = new
	 * Character(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0).getKeyChar());
	 * mapControl.getInputMap(MapControl.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,
	 * 0),keyChar); mapControl.getActionMap().put(keyChar, new KeyAction(""));
	 *  // El espacio como si fuera INTRO Character keyCharSpace = new
	 * Character(' ');
	 * mapControl.getInputMap(MapControl.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('
	 * '), keyCharSpace); mapControl.getActionMap().put(keyCharSpace, new
	 * KeyAction(""));
	 *
	 *  }
	 */
	private static JPopupMenu popup = new JPopupMenu();

	public static void clearMenu() {
		popup.removeAll();
	}

	public static void addMenuEntry(String text) {
		JMenuItem menu = new JMenuItem(text);
		menu.setActionCommand(text);
		menu.setEnabled(true);
		menu.setVisible(true);
		menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CADToolAdapter cta=getCADToolAdapter();
				cta.transition(e.getActionCommand());
			}
		});

		popup.add(menu);
	}

	public static void showPopup(MouseEvent e) {
		popup.show(e.getComponent(), e.getX(), e.getY());
	}

	public static DefaultViewPanel getView() {
		return view;
	}

	public static void clearView() {
		view = null;
	}

	/**
	 *
     * Gets the edition manager of the currently active view
     * or the edition manager of the view that was active more
     * recently (if there is not an active view)
     * or null if there never was an active view
     *
	 * @return Returns the editionManager.
	 *
	 * @deprecated Use same method in {@link EditionLocator}
	 */
	public static IEditionManager getEditionManager() {
	    return EditionLocator.getEditionManager();
	}

	public static CADTool[] getCADTools() {
		return CADToolAdapter.getCADTools();
	}

	public static void initFocus() {
		view = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapControl = view.getMapControl();
		CADToolAdapter cta=getCADToolAdapter();
		if (!mapControl.getNamesMapTools().containsKey("cadtooladapter")){
			// StatusBarListener sbl=new StatusBarListener(view.getMapControl());
			// mapControl.addMapTool("cadtooladapter",  new Behavior[]{adapter,new MouseMovementBehavior(sbl)});
			mapControl.addBehavior("cadtooladapter", cta);
		}
		// view.getMapControl().setTool("cadtooladapter");
		JEditTextArea jeta=view.getConsolePanel().getTxt();
		jeta.requestFocusInWindow();
		jeta.setCaretPosition(jeta.getText().length());

		view.addConsoleListener("cad", new ResponseListener() {
			public void acceptResponse(String response) {
				CADToolAdapter cta=getCADToolAdapter();
				cta.textEntered(response);
				// TODO:
				// FocusManager fm=FocusManager.getCurrentManager();
				// fm.focusPreviousComponent(mapControl);
				/*
				 * if (popup.isShowing()){ popup.setVisible(false); }
				 */

			}
		});
		cta.getEditionManager().setMapControl(mapControl);
		view.getMapControl().setTool("cadtooladapter");
	}

	public IPreference[] getPreferencesPages() {
		IPreference[] preferences=new IPreference[1];
		preferences[0] = getPreferencesPage();
		return preferences;
	}

	private IPreference getPreferencesPage() {
		if (editingPage == null) {
			editingPage = new EditingPage();
		}
		return editingPage;
	}


}
