/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.SplineCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.SplineCADToolContext.SplineCADToolState;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * CADTool Spline
 * 
 * @author Vicente Caballero Navarro
 */
public class SplineCADTool extends AbstractCurveCADTool {

    protected SplineCADToolContext _fsm;
    protected ArrayList list = new ArrayList();
    protected boolean close = false;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new SplineCADToolContext(this);
    }

    public void endGeometry() {
        try {
            int geometryType = ((FLyrVect) getVLE().getLayer()).getShapeType();
            if (((geometryType == Geometry.TYPES.SURFACE) || (geometryType == Geometry.TYPES.MULTISURFACE))
                && !close) {
                closeGeometry();
            }
        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        }

        // No queremos guardar FGeometryCollections:
        Geometry newGeom =
            createSpline((Point2D[]) list.toArray(new Point2D[0]));
        insertAndSelectGeometry(newGeom);
        _fsm = new SplineCADToolContext(this);
        list.clear();
        clearTemporalCache();
        close = false;
    }

    public void closeGeometry() {
        close = true;
        Point2D endPoint =
            new Point2D.Double(((Point2D) list.get(0)).getX(),
                ((Point2D) list.get(0)).getY());
        list.add(endPoint);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        SplineCADToolState actualState =
            (SplineCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        if (status.equals("Spline.NextPoint")
            || status.equals("Spline.FirstPoint")) {
            list.add(new Point2D.Double(x, y));
            Geometry spline =
                createSpline((Point2D[]) list.toArray(new Point2D[0]));
            
            if (spline != null) {
                addTemporalCache(spline);
            }
        }
    }

    /**
     * M�todo para dibujar lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        SplineCADToolState actualState = _fsm.getState();
        String status = actualState.getName();
        if (status.equals("Spline.NextPoint")
            || status.equals("Spline.FirstPoint")) {
            // ArrayList points=new ArrayList();
            Point2D[] points = new Point2D[list.size() + 1];
            Point2D[] auxPoints = (Point2D[]) list.toArray(new Point2D[0]);
            for (int i = 0; i < auxPoints.length; i++) {
                points[i] = (Point2D) auxPoints[i].clone();
            }
            points[points.length - 1] = new Point2D.Double(x, y);
            Geometry spline = createSpline(points);
            ViewPort vp = getCadToolAdapter().getMapControl().getViewPort();

            renderer.draw(spline, mapControlManager.getSelectionSymbol());
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    public void cancel() {
        list.clear();
        clearTemporalCache();
        close = false;
    }

    public void end() {
        // Nothing to do
    }

    public String getName() {
        return PluginServices.getText(this, "Spline_");
    }

    public String toString() {
        return "_Spline";
    }

    public void endTransition(double x, double y, MouseEvent event) {
        _fsm.endPoint(x, y, event);
    }

    @Override
    protected int getSupportedPrimitiveGeometryType() {
        return SPLINE;
    }
}
