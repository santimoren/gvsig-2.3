/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.MultiPointCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.MultiPointCADToolContext.MultiPointCADToolState;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class MultiPointCADTool extends DefaultCADTool {

    private static final Logger LOG = LoggerFactory
        .getLogger(MultiPointCADTool.class);

    protected MultiPointCADToolContext _fsm;
    protected ArrayList points = new ArrayList();

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new MultiPointCADToolContext(this);
    }

    /**
     * DOCUMENT ME!
     * 
     * @param x
     *            DOCUMENT ME!
     * @param y
     *            DOCUMENT ME!
     * @param sel
     *            DOCUMENT ME!
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        MultiPointCADToolState actualState =
            (MultiPointCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("MultiPoint.InsertPoint")) {
            points.add(new double[] { x, y });
            // addGeometry(ShapeFactory.createPoint2D(x, y));
        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        try {
            int num = points.size();
            Point[] pointsAux = new Point[num];
            for (int i = 0; i < num; i++) {
                double[] p = (double[]) points.get(i);
                pointsAux[i] =
                    (Point) geomManager.create(TYPES.POINT, SUBTYPES.GEOM2D);
                pointsAux[i].setX(p[0]);
                pointsAux[i].setY(p[1]);
            }

            MultiPoint multiPoint =
                (MultiPoint) geomManager.create(TYPES.MULTIPOINT,
                    SUBTYPES.GEOM2D);
            for (int i = 0; i < pointsAux.length; i++) {
                multiPoint.addPoint(pointsAux[i]);
            }
            renderer.draw(multiPoint,
                mapControlManager.getGeometrySelectionSymbol());

            renderer.draw(createPoint(x, y),
                mapControlManager.getGeometrySelectionSymbol());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error drawing with renderer: " + renderer + ", x=" + x
                + ",y=" + y, new CreateGeometryException(TYPES.MULTIPOINT,
                SUBTYPES.GEOM2D, e));
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        // Nothing to do
    }

    public void addValue(double d) {
        // Nothing to do
    }

    public String getName() {
        return PluginServices.getText(this, "multipoint_");
    }

    public String toString() {
        return "_multipoint";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { MULTIPOINT };
    }

    public void endGeometry() {
        try {
            int num = points.size();
            Point[] pointsAux = new Point[num];
            for (int i = 0; i < num; i++) {
                double[] p = (double[]) points.get(i);
                pointsAux[i] =
                    (Point) geomManager.create(TYPES.POINT, SUBTYPES.GEOM2D);
                pointsAux[i].setX(p[0]);
                pointsAux[i].setY(p[1]);
            }

            MultiPoint multiPoint = createMultiPoint();

            for (int i = 0; i < pointsAux.length; i++) {
                multiPoint.addPoint(pointsAux[i]);
            }
            insertAndSelectGeometry(multiPoint);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error ending geometry", new CreateGeometryException(
                TYPES.MULTIPOINT, SUBTYPES.GEOM2D, e));
        }
        end();
    }

    public void end() {
        points.clear();
        super.end();
    }
}
