/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import org.gvsig.andami.PluginServices;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.panels.matrix.MatrixOperations;
import org.gvsig.editing.gui.cad.panels.matrix.MatrixProperty;
import org.gvsig.editing.gui.cad.tools.smc.MatrixCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.MatrixCADToolContext.MatrixCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.SpatialCache;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Herramienta para crear una matriz de geometr�as.
 * 
 * @author Vicente Caballero Navarro
 */
public class MatrixCADTool extends DefaultCADTool {

    protected MatrixCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D secondPoint;
    protected MatrixProperty matrixProperty = null;
    protected MatrixOperations operations = null;
    protected String option;

    /**
     * Crea un nuevo MatrixCADTool.
     */
    public MatrixCADTool() {
        matrixProperty = new MatrixProperty();
        operations = new MatrixOperations();
    }

    /**
     * M�todo de inicio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new MatrixCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_matrix");
            } else {
                // init();
                matrixPropeties();
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void matrixPropeties() {
        matrixProperty.setMatrixCADTool(this);
        PluginServices.getMDIManager().addWindow(matrixProperty);
        endMatrix();

    }

    public void endMatrix() {
        if (operations.isAccepted()) {
            PluginServices.getMDIManager().setWaitCursor();

            // ArrayList selectedRow = getSelectedRows();
            ArrayList selectedRowAux = new ArrayList();
            VectorialLayerEdited vle = getVLE();
            FeatureStore featureStore = null;
            DisposableIterator iterator = null;
            try {
                featureStore = vle.getFeatureStore();

                featureStore.beginEditingGroup(getName());
                iterator =
                    ((FeatureSelection) featureStore.getSelection()).iterator();
                while (iterator.hasNext()) {
                    Feature feature = (Feature) iterator.next();
                    // Object[] attributes=new Object[feature.size()];
                    // for (int j = 0; j < feature.size(); j++) {
                    // attributes[j]=feature.get(j);
                    // }
                    if (operations.isRectangular()) {// Si es rectangular la
                                                     // matriz

                        for (int columns = 0; columns < operations
                            .getNumColumns(); columns++) {

                            for (int rows = 0; rows < operations.getNumRows(); rows++) {
                                if (columns == 0 && rows == 0) {
                                    continue;
                                }

                                // DefaultFeature feaCloned = (DefaultFeature)
                                // fea
                                // .cloneRow();
                                Geometry geom =
                                    (feature.getDefaultGeometry())
                                        .cloneGeometry();
                                Envelope original_env = geom.getEnvelope();
                                Geometry g =
                                    createPoint(
                                        original_env.getMinimum(0)
                                            + operations.getDistColumns()
                                            * columns, original_env.getMinimum(1)
                                            + operations.getDistRows() * rows);
                                AffineTransform at = new AffineTransform();
                                at.rotate(
                                    Math.toRadians(operations.getRotation()),
                                    original_env.getMinimum(0),
                                    original_env.getMinimum(1));
                                g.transform(at);
                                Envelope new_env = g.getEnvelope();
                                Point2D pDest =
                                    new Point2D.Double(
                                        new_env.getMinimum(0),
                                        new_env.getMinimum(1));

                                double difX = pDest.getX() - original_env.getMinimum(0);
                                double difY = pDest.getY() - original_env.getMinimum(1);
                                Handler[] handlers =
                                    geom.getHandlers(Geometry.SELECTHANDLER);
                                for (int j = 0; j < handlers.length; j++) {
                                    Handler h = handlers[j];
                                    Point2D p = h.getPoint();
                                    h.set(p.getX() + (difX), p.getY() + (difY));
                                }
                                EditableFeature eFeature =
                                    featureStore.createNewFeature(
                                        feature.getType(), feature);
                                eFeature.setGeometry(featureStore
                                    .getDefaultFeatureType()
                                    .getDefaultGeometryAttributeName(), geom);
                                selectedRowAux.add(eFeature);
                            }

                        }

                    } else { // Polar

                        double rotation = 360 / operations.getNum();

                        for (int numElem = 0; numElem < operations.getNum(); numElem++) {
                            System.out.println("numElem = " + numElem);
                            if (numElem == 0) {
                                continue;
                            }

                            // DefaultFeature feaCloned = (DefaultFeature) fea
                            // .cloneRow();
                            Geometry geom =
                                (feature.getDefaultGeometry()).cloneGeometry();

                            if (!operations.isRotateElements()) {
                                Envelope orig_env = geom.getEnvelope();
                                Geometry g =
                                    createPoint(
                                        orig_env.getMinimum(0),
                                        orig_env.getMinimum(1));
                                AffineTransform at = new AffineTransform();
                                at.rotate(Math.toRadians(rotation * numElem),
                                    operations.getPositionX(),
                                    operations.getPositionY());
                                g.transform(at);
                                
                                Envelope new_env = g.getEnvelope();
                                
                                Point2D pDest =
                                    new Point2D.Double(
                                        new_env.getMinimum(0),
                                        new_env.getMinimum(1));

                                double difX = pDest.getX() - orig_env.getMinimum(0);
                                double difY = pDest.getY() - orig_env.getMinimum(1);
                                Handler[] handlers =
                                    geom.getHandlers(Geometry.SELECTHANDLER);
                                for (int j = 0; j < handlers.length; j++) {
                                    Handler h = handlers[j];
                                    Point2D p = h.getPoint();
                                    h.set(p.getX() + (difX), p.getY() + (difY));
                                }
                            } else {// Cuando los elemtos rotan al mismo tiempo
                                    // que se van a�adiendo.

                                Envelope orig_env = geom.getEnvelope();
                                AffineTransform at = new AffineTransform();
                                at.rotate(Math.toRadians(rotation * numElem),
                                    operations.getPositionX(),
                                    operations.getPositionY());
                                geom.transform(at);
                                
                                Envelope new_env = geom.getEnvelope();
                                
                                Point2D pDest =
                                    new Point2D.Double(
                                        new_env.getMinimum(0),
                                        new_env.getMinimum(1)
                                        );

                                double difX = pDest.getX() - orig_env.getMinimum(0);
                                double difY = pDest.getY() - orig_env.getMinimum(1);
                                Handler[] handlers =
                                    geom.getHandlers(Geometry.SELECTHANDLER);
                                for (int j = 0; j < handlers.length; j++) {
                                    Handler h = handlers[j];
                                    Point2D p = h.getPoint();
                                    h.set(p.getX() + (difX), p.getY() + (difY));
                                }
                            }
                            EditableFeature eFeature =
                                featureStore.createNewFeature(
                                    feature.getType(), feature);
                            eFeature.setGeometry(featureStore
                                .getDefaultFeatureType()
                                .getDefaultGeometryAttributeName(), geom);

                            selectedRowAux.add(eFeature);
                        }
                    }
                }
                
                // =================================================
                /*
                 * Inserting geometries of matrix
                 */
//                SpatialCache spatialCache =
//                    ((FLyrVect) vle.getLayer()).getSpatialCache();
                
                for (int i=0; i<selectedRowAux.size(); i++) {
                	super.insertFeature((Feature) selectedRowAux.get(i));
//                	
//                    EditableFeature efeat = (EditableFeature) selectedRowAux.get(i);
//                    featureStore.insert(efeat);
//                    Geometry ngeom = efeat.getDefaultGeometry();
//                    Envelope envelope = ngeom.getEnvelope();
//                    /*
//                     * Updating spatial cache
//                     */
//                    spatialCache.insert(envelope, ngeom);
                }
                
                // =================================================
                
                featureStore.endEditingGroup();
                // vle.setSelectionCache(VectorialLayerEdited.SAVEPREVIOUS,
                // selectedRowAux);
                PluginServices.getMDIManager().restoreCursor();
                end();
            } catch (DataException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (iterator != null) {
                    iterator.dispose();
                }
            }
        } else {// Cancelado

        }

    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        // MatrixCADToolState actualState = _fsm.getState();
        MatrixCADToolState previousState =
            (MatrixCADToolState) _fsm.getPreviousState();
        String status = previousState.getName();
        if (status.equals("Matrix.Start") || status.equals("Matrix.FirstPoint")) {
            firstPoint = new Point2D.Double(x, y);
        } else
            if (status.equals("Matrix.SecondPoint")) {
                secondPoint = new Point2D.Double(x, y);
                if (option.equals("lagX") || option.equals("lagXY")) {
                    operations.setDistColumns(secondPoint.getX()
                        - firstPoint.getX());
                    matrixProperty.refreshLagX();
                }
                if (option.equals("lagY") || option.equals("lagXY")) {
                    operations.setDistRows(secondPoint.getY()
                        - firstPoint.getY());
                    matrixProperty.refreshLagY();
                }
                if (option.equals("rotation")) {

                    double w;
                    double h;
                    w = secondPoint.getX() - firstPoint.getX();
                    h = secondPoint.getY() - firstPoint.getY();
                    operations.setRotation((-Math.atan2(w, h) + (Math.PI / 2))
                        * 180 / Math.PI);
                    matrixProperty.refreshRotation();
                }
                firstPoint = null;
                PluginServices.getMDIManager().addWindow(matrixProperty);
            }
    }

    /**
     * M�todo para dibujar lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        if (_fsm == null || firstPoint == null) {
            return;
        }
        GeneralPathX gpx = new GeneralPathX();
        gpx.moveTo(firstPoint.getX(), firstPoint.getY());
        gpx.lineTo(x, y);
        VectorialLayerEdited vle = getVLE();
        ViewPort vp = vle.getLayer().getMapContext().getViewPort();

        Curve curve = createCurve(gpx);
        renderer.draw(curve, mapControlManager.getAxisReferenceSymbol());
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        option = s;
        PluginServices.getMDIManager().closeWindow(matrixProperty);
        /*
         * MatrixCADToolState actualState = _fsm .getState(); String status =
         * actualState.getName();
         * 
         * if (status.equals("Matrix.LagXY")) {
         * 
         * }else if (status.equals("Matrix.LagX")) {
         * 
         * }else if (status.equals("Matrix.LagY")) {
         * }
         */
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {

    }

    public String getName() {
        return PluginServices.getText(this, "matrix_");
    }

    public String toString() {
        return "_matrix";
    }

    public MatrixOperations getOperations() {
        return operations;
    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }
}
