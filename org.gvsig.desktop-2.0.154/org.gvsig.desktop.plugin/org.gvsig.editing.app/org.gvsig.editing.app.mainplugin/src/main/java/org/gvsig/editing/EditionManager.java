/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.layers.FactoryLayerEdited;
import org.gvsig.editing.layers.ILayerEdited;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.exceptions.CancelEditingLayerException;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionListener;
import org.gvsig.fmap.mapcontext.layers.LayerEvent;
import org.gvsig.fmap.mapcontext.layers.LayerListener;
import org.gvsig.fmap.mapcontext.layers.LayerPositionEvent;
import org.gvsig.fmap.mapcontext.layers.SpatialCache;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EditionManager implements LayerListener, LayerCollectionListener, IEditionManager {

    private static final Logger logger = LoggerFactory
            .getLogger(EditionManager.class);

    final private List<ILayerEdited> editedLayers = new ArrayList<ILayerEdited>();
    final private GeometryManager geomManager = GeometryLocator.getGeometryManager();
    final private MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

    private MapControl mapCtrl = null;
    private ILayerEdited ile = null;

    public ILayerEdited getLayerEdited(FLayer lyr) {
        ILayerEdited aux = null;
        for ( int i = 0; i < editedLayers.size(); i++ ) {
            aux = editedLayers.get(i);
            if ( aux.getLayer() == lyr ) {
                return aux;
            }
        }
        return null;
    }

    public void editLayer(FLyrVect lv, DefaultViewPanel vista)
            throws DataException {

        EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

        EditingNotification notification = editingNotificationManager.notifyObservers(
                this,
                EditingNotification.BEFORE_ENTER_EDITING_STORE,
                vista == null ? null : vista.getDocument(),
                lv);

        if ( notification.isCanceled() ) {
            logger.info("Edit layer '" + lv.getName() + "' canceled by somme observer.");
            throw new CancelException("Edit layer '" + lv.getName() + "' canceled.");
        }

        CADExtension.initFocus();
        vista.showConsole();
        MapControl mapControl = vista.getMapControl();
        this.setMapControl(mapControl);

        lv.addLayerListener(this);

        // This line also enables the spatial cache for snapping:
        lv.getFeatureStore().edit();
        lv.getFeatureStore().addObserver(vista);

        ApplicationLocator.getManager().refreshMenusAndToolBars();

        lv.getFeatureStore().addObserver(mapControl);
        StartEditing.startCommandsApplicable(vista, lv);

        // vista.repaintMap();
        // ----------------------------
        // this forces a true repaint:
        lv.drawValueChanged(LayerEvent.createDrawValuesChangedEvent(lv, ""));

        editingNotificationManager.notifyObservers(
                this,
                EditingNotification.AFTER_ENTER_EDITING_STORE,
                vista == null ? null : vista.getDocument(),
                lv);
    }

    public void visibilityChanged(LayerEvent e) {
    }

    public void activationChanged(LayerEvent e) {
        if ( e.getSource().isActive() ) {
            ile = getLayerEdited(e.getSource());
        }
        if ( ile == null || ile.getLayer().equals(e.getSource()) ) {

            if ( ile != null && !ile.getLayer().isActive() ) {
                VectorialLayerEdited lastVLE = (VectorialLayerEdited) ile;
                lastVLE.activationLost(e);
            }
            if ( e.getSource() instanceof FLyrVect ) {
                VectorialLayerEdited vle = null;
                vle = (VectorialLayerEdited) getLayerEdited(e.getSource());
                ile = vle;
                if ( getMapControl() != null && vle != null
                        && vle.getLayer().isActive() ) {
                    getMapControl().setTool("cadtooladapter");
                    vle.activationGained(e);
                    return;
                }
            }
            if ( getMapControl() != null ) {
                getMapControl().setTool("zoomIn");
                PluginServices.getMainFrame().setSelectedTool("ZOOM_IN");
            }
        }
    }

    public void nameChanged(LayerEvent e) {
    }

    public void editionChanged(LayerEvent e) {
        ILayerEdited lyrEdit = getLayerEdited(e.getSource());

        // Si no est� en la lista, comprobamos que est� en edici�n
        // y lo a�adimos
        if ( (lyrEdit == null) && e.getSource().isEditing() ) {
            lyrEdit
                    = FactoryLayerEdited.createLayerEdited(e.getSource(), mapCtrl);
            editedLayers.add(lyrEdit);
            if ( getMapControl() != null ) {
                getMapControl().setTool("cadtooladapter");
                CADExtension.setCADTool("_selection", true);
            }
            PluginServices.getMainFrame().setSelectedTool("_selection");
            // idActiveLayer = editedLayers.size() - 1;
            ile = getLayerEdited(e.getSource());
            logger.debug("NUEVA CAPA EN EDICION: {}", lyrEdit.getLayer().getName());

            // Ponemos el resto de temas desactivados
            if ( mapCtrl != null ) {
                mapCtrl.getMapContext().getLayers().setActive(false);
            }
            // y activamos el nuevo.
            e.getSource().setActive(true);

            if ( e.getSource() instanceof FLyrVect ) {
                FLyrVect fLyrVect = (FLyrVect) e.getSource();
                ((VectorialLayerEdited) lyrEdit)
                        .setEditionChangeManager(new EditionChangeManager(fLyrVect));
            }
        } else {
            for ( int i = 0; i < editedLayers.size(); i++ ) {
                VectorialLayerEdited vle
                        = (VectorialLayerEdited) editedLayers.get(i);
                if ( vle.equals(lyrEdit) ) {
                    editedLayers.remove(i);
                    ile = null;
                    return;
                }
            }
        }

    }

    public ILayerEdited getActiveLayerEdited() {
        return ile;
    }


    public MapControl getMapControl() {
        return mapCtrl;
    }

    public void setMapControl(MapControl mapCtrl) {
        if ( mapCtrl != null ) {
            this.mapCtrl = mapCtrl;
            mapCtrl.getMapContext().getLayers().addLayerListener(this);
            mapCtrl.getMapContext().getLayers()
                    .addLayerCollectionListener(this);
        }
    }

    public void layerAdded(LayerCollectionEvent e) {
        // Nothing to do

    }

    public void layerMoved(LayerPositionEvent e) {
        // Nothing to do
    }

    public void layerRemoved(LayerCollectionEvent e) {
        VectorialLayerEdited vle
                = (VectorialLayerEdited) getActiveLayerEdited();
        if ( vle != null && vle.getLayer().isActive() ) {
            try {
                vle.clearSelection();
            } catch (DataException e1) {
                NotificationManager.addError(e1);
            }
            editedLayers.remove(vle);
            getMapControl().setTool("zoomIn");
            FLyrVect lv = (FLyrVect) vle.getLayer();
            if ( e.getAffectedLayer().equals(lv) ) {
                IWindow window
                        = PluginServices.getMDIManager().getActiveWindow();
                if ( window instanceof DefaultViewPanel ) {
                    DefaultViewPanel view = (DefaultViewPanel) window;
                    view.hideConsole();
                    view.validate();
                    view.repaint();
                }
            }
        }
        PluginServices.getMainFrame().enableControls();
    }

    public void layerAdding(LayerCollectionEvent e) throws CancelationException {
        // Nothing to do
    }

    public void layerMoving(LayerPositionEvent e) throws CancelationException {
        // Nothing to do
    }

    public void layerRemoving(LayerCollectionEvent e)
            throws CancelationException {
        // Nothing to do
    }

    public void activationChanged(LayerCollectionEvent e)
            throws CancelationException {
        // Nothing to do
    }

    public void visibilityChanged(LayerCollectionEvent e)
            throws CancelationException {
        // Nothing to do
    }

    public void drawValueChanged(LayerEvent e) {
        // Nothing to do
    }

//    protected boolean validateFeature(EditableFeature feature, FeatureStore featureStore) {
//        while( true ) {
//            if( !needAskUser(feature, featureStore) ) {
//                // La feature se puede validar y no precisa de intervencion del
//                // usuario. Retornamos true.
//                return true;
//            }
//            // No se habia podido validar la feature, se piden al
//            // usuario los datos que faltan.
//            AskRequiredAttributtes ask = new AskRequiredAttributtes();
//            ask.showDialog(feature, featureStore);
//            if( ask.userCancel() ) {
//                // No se habia podido validar la feature, se le han pedido al
//                // usuario los datos que faltan y este ha pulsado en cancel,
//                // asi que la feature no se puede validar, y retornamos
//                // false.
//                return false;
//            }
//        }
//    }
//    
//    private boolean needAskUser(EditableFeature feature, FeatureStore featureStore) {
//        FeatureType featureType = feature.getType();
//        FeatureAttributeDescriptor[] attributeDescriptors = featureType.getAttributeDescriptors();
//
//        for ( int i = 0; i < attributeDescriptors.length; i++ ) {
//            FeatureAttributeDescriptor attrDesc = attributeDescriptors[i];
//            if ( attrDesc.isAutomatic() ) {
//                break;
//            }
//            if ( (attrDesc.isPrimaryKey() || !attrDesc.allowNull())
//                    && feature.get(attrDesc.getName()) == null ) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private class AskRequiredAttributtes {
//
//        private boolean userCancelValue = true;
//
//        public boolean userCancel() {            
//            return this.userCancelValue;
//        }
//        
//        public void showDialog(final EditableFeature feature,
//                final FeatureStore store) {
//
//            if ( !SwingUtilities.isEventDispatchThread() ) {
//                try {
//                    SwingUtilities.invokeAndWait(new Runnable() {
//                        public void run() {
//                            showDialog(feature, store);
//                        }
//                    }
//                    );
//                } catch (Exception e1) {
//                    message("Can't show form to fill need data.",e1);
//                    this.userCancelValue = true;
//                    return;
//                }
//            }
//
//            try {
//                DynObject data = feature.getAsDynObject();
//                DynObjectEditor editor;
//                editor = new DynObjectEditor(data);
//                editor.editObject(true);
//                if ( editor.isCanceled() ) {
//                    this.userCancelValue = true;
//                    message("User canceled.", null);
//                } else {
//                    editor.getData(data);
//                    this.userCancelValue = false;
//                }
//            } catch (Exception ex) {
//                message("Can't show form to fill need data.",ex);
//                this.userCancelValue = true;
//            }
//        }
//        
//        private void message(String msg, Throwable ex) {
//            msg = msg + "\nSee to application log for more information.";
//            if( ex==null ) {
//                logger.warn(msg);
//            } else {
//                logger.warn(msg,ex);
//            }
//            ApplicationManager application = ApplicationLocator.getManager();
//            application.message(msg,JOptionPane.WARNING_MESSAGE);
//        }
//    }

    private VectorialLayerEdited getVLE() {
        return (VectorialLayerEdited) getActiveLayerEdited();
    }

    private void insertSpatialCache(Geometry geom)
            throws CreateEnvelopeException {
        VectorialLayerEdited vle = getVLE();
        SpatialCache spatialCache = ((FLyrVect) vle.getLayer())
                .getSpatialCache();
        Envelope r = geom.getEnvelope();
        if ( geom.getType() == Geometry.TYPES.POINT ) {
            r = geomManager.createEnvelope(r.getMinimum(0), r.getMinimum(1),
                    r.getMinimum(0) + 1, r.getMinimum(1) + 1, SUBTYPES.GEOM2D);
        }
        spatialCache.insert(r, geom);

    }

    private void updateSpatialCache(Geometry geom) {
        // TODO: no se actualizaban la cache espacial cuando se modifican las geometrias,
        // habria que hacerlo.
        logger.info("TODO: need update spatial cache");
    }

    private void removeSpatialCache(Geometry geom) {
        // TODO: no se actualizaban la cache espacial cuando se modifican las geometrias,
        // habria que hacerlo.
        logger.info("TODO: need update spatial cache");
    }

    private void draw(Geometry geometry, Feature feature) {
        if ( geometry != null ) {
            getMapControl().getMapControlDrawer()
                    .draw(geometry, mapControlManager.getGeometrySelectionSymbol());
        }
    }

    public Feature insertGeometry(Geometry geometry) {
        VectorialLayerEdited vle = getVLE();
        try {
            FeatureStore featureStore
                    = ((FLyrVect) vle.getLayer()).getFeatureStore();
            EditableFeature eFeature = featureStore.createNewFeature(true);

            //Reproject the geometry
            Geometry insertedGeometry = geometry;
            if ( getVLE().getLayer().getCoordTrans() != null ) {
                insertedGeometry = insertedGeometry.cloneGeometry();
                insertedGeometry.reProject(getVLE().getLayer().getCoordTrans().getInverted());
            }

            eFeature.setGeometry(featureStore.getDefaultFeatureType()
                    .getDefaultGeometryAttributeName(), insertedGeometry);

            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );
            if ( notification.isCanceled() ) {
                return null;
            }
            if( !notification.shouldValidateTheFeature() ) {
                if ( !editingNotificationManager.validateFeature(eFeature) ) {
                    return null;
                }
            }

            featureStore.insert(eFeature);
            insertSpatialCache(insertedGeometry);
            draw(insertedGeometry, eFeature);

            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );

            return eFeature;
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
            return null;
        }
    }

    public Feature insertGeometry(Geometry geometry, Feature feature) {
        VectorialLayerEdited vle = getVLE();

        try {
            FeatureStore featureStore
                    = ((FLyrVect) vle.getLayer()).getFeatureStore();
            EditableFeature eFeature
                    = featureStore.createNewFeature(
                            featureStore.getDefaultFeatureType(), feature);
            eFeature.setGeometry(featureStore.getDefaultFeatureType()
                    .getDefaultGeometryAttributeName(), geometry);

            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );
            if ( notification.isCanceled() ) {
                return null;
            }
            if( notification.shouldValidateTheFeature() ) {
                if ( !editingNotificationManager.validateFeature(eFeature) ) {
                    return null;
                }
            }

            featureStore.insert(eFeature);
            insertSpatialCache(geometry);
            draw(geometry, eFeature);

            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );

            return eFeature;
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
            return null;
        }
    }

    public Feature insertFeature(Feature feature) {
        VectorialLayerEdited vle = getVLE();

        try {

            FeatureStore featureStore
                    = ((FLyrVect) vle.getLayer()).getFeatureStore();
            EditableFeature eFeature
                    = featureStore.createNewFeature(
                            featureStore.getDefaultFeatureType(), feature);
            Geometry geometry = feature.getDefaultGeometry();

            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );
            if ( notification.isCanceled() ) {
                return null;
            }
            if( notification.shouldValidateTheFeature() ) {
                if ( !editingNotificationManager.validateFeature(eFeature) ) {
                    return null;
                }
            }

            featureStore.insert(eFeature);
            insertSpatialCache(geometry);
            draw(geometry, eFeature);

            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_INSERT_FEATURE,
                    null,
                    vle.getLayer(),
                    featureStore,
                    eFeature
            );

            return eFeature;
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
            return null;
        }
    }

    public Feature insertAndSelectGeometry(String toolName, Geometry geometry) {
        Feature feature = null;
        try {
            FeatureStore featureStore = getVLE().getFeatureStore();
            featureStore.beginComplexNotification();
            featureStore.beginEditingGroup(toolName);
            FeatureSelection newSelection
                    = featureStore.createFeatureSelection();
            feature = insertGeometry(geometry);
            if ( feature != null ) {
                newSelection.select(feature);
                featureStore.setSelection(newSelection);
            }
            featureStore.endEditingGroup();
            featureStore.endComplexNotification();

        } catch (DataException e) {
            NotificationManager.showMessageError("insertAndSelectGeoemtry", e);
        }
        return feature;
    }

    public EditableFeature updateGeometry(FeatureStore store, Feature feature, Geometry geometry) {
        try {
            EditableFeature eFeature = feature.getEditable();
            eFeature.setGeometry(feature.getType()
                    .getDefaultGeometryAttributeName(), geometry);
            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_UPDATE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    eFeature
            );
            if ( notification.isCanceled() ) {
                return null;
            }
            if( notification.shouldValidateTheFeature() ) {
                if ( !editingNotificationManager.validateFeature(eFeature) ) {
                    return null;
                }
            }

            store.update(eFeature);
            updateSpatialCache(geometry);

            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_UPDATE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    eFeature
            );

            return eFeature;
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
            return null;
        }

    }

    public EditableFeature updateFeature(FeatureStore store, Feature feature) {
        try {
            EditableFeature eFeature = feature.getEditable();
            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_UPDATE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    eFeature
            );
            if ( notification.isCanceled() ) {
                return null;
            }
            if( notification.shouldValidateTheFeature() ) {
                if ( !editingNotificationManager.validateFeature(eFeature) ) {
                    return null;
                }
            }

            store.update(eFeature);
            updateSpatialCache(feature.getDefaultGeometry());
            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_UPDATE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    eFeature
            );

            return eFeature;
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
            return null;
        }

    }

    public void removeFeature(FeatureStore store, Feature feature) {
        try {
            EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

            EditingNotification notification = editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.BEFORE_REMOVE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    feature
            );
            if ( notification.isCanceled() ) {
                return;
            }
            store.delete(feature);
            removeSpatialCache(feature.getDefaultGeometry());

            editingNotificationManager.notifyObservers(
                    this,
                    EditingNotification.AFTER_REMOVE_FEATURE,
                    null,
                    getVLE().getLayer(),
                    store,
                    feature
            );
        } catch (Exception e) {
            NotificationManager.addError(e.getMessage(), e);
        }

    }

    public void stopEditLayer(DefaultViewPanel view, FLyrVect lv, int mode)
            throws CancelEditingLayerException, ReadException {

        EditingNotificationManager editingNotificationManager = MapControlLocator.getEditingNotificationManager();

        EditingNotification notification = editingNotificationManager.notifyObservers(
                this,
                EditingNotification.BEFORE_EXIT_EDITING_STORE,
                view == null ? null : view.getDocument(),
                lv);
        if ( notification.isCanceled() ) {
            logger.info("Stop edit layer '" + lv.getName() + "' canceled by somme observer.");
            throw new CancelException("Stop edit layer '" + lv.getName() + "' canceled.");
        }

        VectorialLayerEdited lyrEd = null;
        FeatureStore featureStore = null;

        switch (mode) {

        case CANCEL_EDITING:
            lyrEd = (VectorialLayerEdited) this.getActiveLayerEdited();
            lv.getFeatureStore().deleteObserver(lyrEd);

            try {
                featureStore = lv.getFeatureStore();

                featureStore.cancelEditing();
            } catch (ReadException e) {
                throw new CancelEditingLayerException(lv.getName(), e);
            } catch (DataException e) {
                throw new CancelEditingLayerException(lv.getName(), e);
            }
            lv.getFeatureStore().deleteObserver(view);
            lv.removeLayerListener(this);

            if ( view != null ) {
                view.getMapControl().getMapContext().removeLayerDrawListener(lyrEd);
            }
            break;
        case ACCEPT_EDITING:
            featureStore = lv.getFeatureStore();
            try {

                /*
                 * This will throw a WriteException
                 * also if the provider does not accept
                 * the changes (added/updated/deleted/updated featuretype)  
                 */
                featureStore.finishEditing();

            } catch (DataException e) {
                throw new ReadException(lv.getName(), e);
            }
            lyrEd = (VectorialLayerEdited) this.getActiveLayerEdited();
            featureStore.deleteObserver(lyrEd);
            featureStore.deleteObserver(view);
            lv.removeLayerListener(this);
            if ( view != null ) {
                view.getMapControl().getMapContext().removeLayerDrawListener(lyrEd);
            }

            break;
        case CONTINUE_EDITING:
            break;
        }

        editingNotificationManager.notifyObservers(
                this,
                EditingNotification.AFTER_EXIT_EDITING_STORE,
                view == null ? null : view.getDocument(),
                lv);

    }

}
