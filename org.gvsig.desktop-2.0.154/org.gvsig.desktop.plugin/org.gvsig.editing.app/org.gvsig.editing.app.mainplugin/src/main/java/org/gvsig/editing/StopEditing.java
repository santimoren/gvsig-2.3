/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.IExtensionExecuteWithArgs;
import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.andami.plugins.status.UnsavedData;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.DefaultViewDocument;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.exportto.app.extension.ExporttoLayerExtension;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.utils.swing.threads.IMonitorableTask;

/**
 * @author Francisco Jos�
 *
 * Cuando un tema se pone en edici�n, puede que su driver implemente
 * ISpatialWriter. En ese caso, es capaz de guardarse sobre s� mismo. Si
 * no lo implementa, esta opci�n estar� deshabilitada y la �nica
 * posibilidad de guardar este tema ser� "Guardando como..."
 *
 */
public class StopEditing extends Extension implements IExtensionExecuteWithArgs {

    private final static Logger logger = LoggerFactory.getLogger(StopEditing.class);

    private DefaultViewPanel vista;

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    }

    public void execute(String s) {
        this.execute(s, null);
    }

    public void execute(String s, Object[] args) {
        org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices
                .getMDIManager().getActiveWindow();

        vista = (DefaultViewPanel) f;
        boolean isStop = false;
        ViewDocument model = vista.getModel();
        MapContext mapa = model.getMapContext();
        FLayers layers = mapa.getLayers();
        IEditionManager edMan = EditionLocator.getEditionManager();

        if ( s.equals("layer-stop-editing") ) {

            FLayer[] selectedItems = null;
            // Desde el menu contextual del TOC se le pasan las capas seleccionadas como primer argumento.
            if ( args != null && args.length > 0 && args[0] instanceof FLayer[] ) {
                selectedItems = (FLayer[]) args[0];
            }

            vista.getMapControl().getCanceldraw().setCanceled(true);
            FLayer[] actives = layers.getActives();
			// TODO: Comprobar que solo hay una activa, o al menos
            // que solo hay una en edici�n que est� activa, etc, etc
            for ( int i = 0; i < actives.length; i++ ) {
                if ( actives[i] instanceof FLyrVect && actives[i].isEditing() ) {
                    FLyrVect lv = (FLyrVect) actives[i];
                    MapControl mapControl = vista.getMapControl();

                    int user_opt = confirmStop(lv, mapControl);
                    if ( user_opt != IEditionManager.CONTINUE_EDITING ) {
                        isStop = true;
                    }

                    try {
                        edMan.stopEditLayer(vista, lv, user_opt);
                    } catch (CancelException ex) {
                        // Do nothing
                        isStop = false;
                    } catch (Exception ex) {
                        logger.warn("Can't stop layer editing.", ex);
                        /*
                         * Unable to end editing, inform user
                         * of the problem. Editing session will continue.
                         */
                        showUnableToEndEditingDialog(ex);
                        isStop = false;
                    }
                }
            }

            if ( isStop ) {
                CADExtension.clearView();
                /*
                 * Make grid not visible and remove snapping to grid
                 * and disable snappers
                 */
                disableGridAndSnappers(vista.getMapControl());
            }
        }
        PluginServices.getMainFrame().enableControls();
    }

    private void disableGridAndSnappers(MapControl mco) {
        mco.setGridVisibility(false);
        mco.setAdjustGrid(false);
//        Preferences prefs = GridPage.prefs;
//        prefs.putBoolean("grid.showgrid", false);
//        prefs.getBoolean("grid.adjustgrid", false);
        // Snapping to geometries disabled
        mco.setRefentEnabled(false);
        /*
         * Remove all layers to snap to (unless they are being edited).
         * If we don't do this, geometries will still be added
         * to the spatial cache of those layers when editing session ends
         */
        List removelist = new ArrayList();
        FLyrVect item = null;
        Iterator snapiter = mco.getMapContext().getLayersToSnap().iterator();
        while ( snapiter.hasNext() ) {
            item = (FLyrVect) snapiter.next();
            if ( !item.isEditing() ) {
                /*
                 * Get list of layers to be removed from the list
                 */
                removelist.add(item);
            }
        }
        /*
         * Remove layers from list
         */
        mco.getMapContext().getLayersToSnap().removeAll(removelist);

    }

    private void showUnableToEndEditingDialog(Exception ex) {

        String msg = Messages.getText(
                "_Unable_to_save_edits_The_cause_is");
        msg = msg + ":\n\n" + EditionUtilities.getLastMessage(ex);
        JOptionPane.showMessageDialog(
                ApplicationLocator.getManager().getRootComponent(),
                msg,
                Messages.getText("_Guardar"),
                JOptionPane.ERROR_MESSAGE);

    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        FLayer[] lyrs = EditionUtilities.getActiveAndEditedLayers();
        if ( lyrs == null ) {
            return false;
        }
        FLyrVect lyrVect = (FLyrVect) lyrs[0];
        if ( lyrVect.isEditing() ) {
            return true;
        }
        return false;
    }

    /**
     * DOCUMENT ME!
     */
    /*
     public boolean stopEditing(FLyrVect layer, MapControl mapControl) {
     int resp = JOptionPane.CANCEL_OPTION;

     try {
     if (layer.isWritable()) {
     Object[] options = {
     PluginServices.getText(this, "_Guardar"),
     "       " + PluginServices.getText(this, "_Descartar") + "       ",
     PluginServices.getText(this, "_Continuar") };

     JPanel explanation_panel = getExplanationPanel(layer.getName());

     resp = JOptionPane
     .showOptionDialog(
     (Component) PluginServices.getMainFrame(),
     explanation_panel,
     PluginServices.getText(this, "stop_edition"),
     JOptionPane.YES_NO_CANCEL_OPTION,
     JOptionPane.QUESTION_MESSAGE, null, options,
     options[2]);

     if (resp == JOptionPane.YES_OPTION) {
     // SAVE
     saveLayer(layer);
     // layer.setEditing(false);
     return true;
     } else if (resp == JOptionPane.NO_OPTION) {
     // CANCEL EDITING
     cancelEdition(layer);
     // layer.setEditing(false);
     return true;
     } else if (resp == JOptionPane.CANCEL_OPTION) {
     // CONTINUE EDITING
     return false;
     } else {
     // This happens when user clicks on [x]
     // to abruptly close previous JOptionPane dialog
     // We make it equivalent to CONTINUE EDITING
     return false;
     }

     }
     // ========================================
     // Layer cannot save changes:

     Object[] options = {
     PluginServices.getText(this, "export"),
     PluginServices.getText(this,
     "finish_editing_without_saving_changes"),
     PluginServices.getText(this, "continue_editing") };

     resp = JOptionPane
     .showOptionDialog(
     (Component) PluginServices.getMainFrame(),
     PluginServices
     .getText(
     this,
     "no_existe_writer_para_este_formato_de_capa_o_no_tiene_permisos_de_escritura_los_datos_que_desea_hacer")
     + " : " + layer.getName(), PluginServices
     .getText(this, "stop_edition"),
     JOptionPane.YES_NO_CANCEL_OPTION,
     JOptionPane.QUESTION_MESSAGE, null, options,
     options[2]);
     if (resp == JOptionPane.NO_OPTION) { // CANCEL EDITING
     cancelEdition(layer);
     // layer.setEditing(false);
     return true;
     } else if (resp == JOptionPane.YES_OPTION) {
     int status = exportLayer(layer);
     if (status == JOptionPane.OK_OPTION) {
     cancelEdition(layer);
     // layer.setEditing(false);
     }
     }

     } catch (ReadException e) {
     NotificationManager.addError(e);
     } catch (CancelEditingLayerException e) {
     NotificationManager.addError(e);
     }
     return false;

     }
     */
    public int confirmStop(FLyrVect layer, MapControl mapControl) {
        int resp = JOptionPane.CANCEL_OPTION;

        try {
            if ( layer.isWritable() ) {
                Object[] options = {
                    PluginServices.getText(this, "_Guardar"),
                    "       " + PluginServices.getText(this, "_Descartar") + "       ",
                    PluginServices.getText(this, "_Continuar")};

                String question = Messages.getText("realmente_desea_guardar_la_capa");
                question = question + " '" + layer.getName() + "'?";
                String firstLabel = Messages.getText("_Guardar");
                String firstDesc = Messages.getText("_Save_changes_performed");

                JPanel explanation_panel = getExplanationPanel(
                        question, firstLabel, firstDesc);

                resp = JOptionPane
                        .showOptionDialog(
                                (Component) PluginServices.getMainFrame(),
                                explanation_panel,
                                PluginServices.getText(this, "stop_edition"),
                                JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options,
                                options[2]);

                if ( resp == JOptionPane.YES_OPTION ) {
                    return IEditionManager.ACCEPT_EDITING;
                } else if ( resp == JOptionPane.NO_OPTION ) {
                    return IEditionManager.CANCEL_EDITING;
                } else if ( resp == JOptionPane.CANCEL_OPTION ) {
                    return IEditionManager.CONTINUE_EDITING;
                } else {
	                    // This happens when user clicks on [x]
                    // to abruptly close previous JOptionPane dialog
                    // We make it equivalent to CONTINUE EDITING
                    return IEditionManager.CONTINUE_EDITING;
                }

            }
	            // ========================================
            // Layer cannot save changes:

            Object[] options = {
                PluginServices.getText(this, "export"),
                "       " + PluginServices.getText(this, "_Descartar") + "       ",
                PluginServices.getText(this, "_Continuar")};

            String question = Messages.getText(
                    "no_existe_writer_para_este_formato_de_capa_o_no_tiene_permisos_de_escritura_los_datos_que_desea_hacer");
            String firstLabel = Messages.getText("export");
            String firstDesc = Messages.getText("_Export_to_another_format");

            JPanel explanation_panel = getExplanationPanel(
                    question, firstLabel, firstDesc);

            resp = JOptionPane
                    .showOptionDialog(
                            (Component) PluginServices.getMainFrame(),
                            explanation_panel,
                            PluginServices.getText(this, "stop_edition"),
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options,
                            options[2]);
            if ( resp == JOptionPane.NO_OPTION ) { // CANCEL EDITING
                return IEditionManager.CANCEL_EDITING;
            } else if ( resp == JOptionPane.YES_OPTION ) {
                int status = exportLayer(layer);
                if ( status == JOptionPane.OK_OPTION ) {
                    return IEditionManager.CANCEL_EDITING;
                }
            }

        } catch (ReadException e) {
            NotificationManager.addError(e);
        }
        return IEditionManager.CONTINUE_EDITING;

    }

    private JPanel getExplanationPanel(
            String translatedQuestion,
            String translatedFirstTag,
            String translatedFirstDescription) {

        JPanel resp = new JPanel(new BorderLayout(10, 10));

        JLabel topLabel = new JLabel(translatedQuestion);

        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints cc = new GridBagConstraints();

        cc.gridx = 0;
        cc.gridy = 0;
        cc.anchor = GridBagConstraints.WEST;

        cc.insets = new Insets(3, 6, 3, 6);

        Font boldf = mainPanel.getFont().deriveFont(Font.BOLD);

        JLabel lbl = new JLabel(translatedFirstTag);
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(translatedFirstDescription), cc);

        cc.gridx = 0;
        cc.gridy = 1;
        lbl = new JLabel(Messages.getText("_Descartar"));
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(Messages.getText("_Discard_and_lose_changes")), cc);

        cc.gridx = 0;
        cc.gridy = 2;
        lbl = new JLabel(Messages.getText("_Continuar"));
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(Messages.getText("_Do_not_save_yet_Stay_in_editing_mode")), cc);

        resp.add(mainPanel, BorderLayout.CENTER);
        resp.add(topLabel, BorderLayout.NORTH);
        return resp;
    }


    /*
     private void saveLayer(FLyrVect layer) throws ReadException {
     FeatureStore featureStore = layer.getFeatureStore();
     try {
     featureStore.finishEditing();
     } catch (WriteException e) {
     throw new ReadException(featureStore.getName(), e);
     } catch (DataException e) {
     throw new ReadException(featureStore.getName(), e);
     }
     }

     private void cancelEdition(FLyrVect layer)
     throws CancelEditingLayerException {
     FeatureStore featureStore = null;
     try {
     featureStore = layer.getFeatureStore();

     featureStore.cancelEditing();
     } catch (ReadException e) {
     throw new CancelEditingLayerException(layer.getName(), e);
     } catch (DataException e) {
     throw new CancelEditingLayerException(layer.getName(), e);
     }
     }
     */
    private int exportLayer(FLyrVect layer) throws ReadException {
        ViewDocument model = vista.getModel();
        MapContext mapContext = model.getMapContext();
        IProjection projection = mapContext.getProjection();

        ExporttoLayerExtension extension = (ExporttoLayerExtension) PluginsLocator
                .getManager().getExtension(ExporttoLayerExtension.class);
        return extension.showExportto(layer, projection, mapContext, WindowManager.MODE.DIALOG);
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        if ( EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE ) {
            return true;
        }
        return false;

    }

    public IExtensionStatus getStatus() {
        return new StopEditingStatus();
    }

    /**
     * Show the dialogs to save the layer without ask if don't like to save.
     *
     * @param layer
     * Layer to save.
     */
    public boolean executeSaveLayer(FLyrVect layer) {

        IEditionManager edMan = EditionLocator.getEditionManager(layer);

        VectorialLayerEdited lyrEd = (VectorialLayerEdited) edMan
                .getLayerEdited(layer);
        boolean isStop = false;
        try {
            lyrEd.clearSelection();

            if ( layer.isWritable() ) {

                try {
                    edMan.stopEditLayer(null, layer, IEditionManager.ACCEPT_EDITING);
                    isStop = true;
                } catch (CancelException ex) {
                    isStop = false;

                } catch (Exception ex) {
                    logger.error("While stopping layer editing.", ex);
                    isStop = false;
                }

            } else {
                // Si no existe writer para la capa que tenemos en edici�n
                int resp = JOptionPane
                        .showConfirmDialog(
                                (Component) PluginServices.getMainFrame(),
                                PluginServices
                                .getText(
                                        this,
                                        "no_existe_writer_para_este_formato_de_capa_o_no_tiene_permisos_de_escritura_los_datos_no_se_guardaran_desea_continuar")
                                + " : " + layer.getName(),
                                PluginServices
                                .getText(this, "cancelar_edicion"),
                                JOptionPane.YES_NO_OPTION);
                if ( resp == JOptionPane.YES_OPTION ) { // CANCEL EDITING

                    try {
                        edMan.stopEditLayer(null, layer, IEditionManager.CANCEL_EDITING);
                        isStop = true;
                    } catch (CancelException ex) {
                        isStop = false;

                    } catch (Exception ex) {
                        isStop = false;
                        logger.error("While stopping layer editing.", ex);
                    }

                }

            }
        } catch (ReadException e1) {
            NotificationManager.showMessageError(e1.getMessage(), e1);
        } catch (DataException e) {
            NotificationManager.showMessageError(e.getMessage(), e);
        }
        return isStop;
    }

    private class UnsavedLayer extends UnsavedData {

        private FLayer layer;

        public UnsavedLayer(IExtension extension) {
            super(extension);
        }

        public String getDescription() {
            return PluginServices.getText(this, "editing_layer_unsaved");
        }

        public String getResourceName() {
            return layer.getName();
        }

        public boolean saveData() {
            return executeSaveLayer((FLyrVect) layer);
        }

        public void setLayer(FLayer layer) {
            this.layer = layer;

        }

        public String getIcon() {
            return layer.getTocImageIcon();
        }

    }

    /**
     * <p>
     * This class provides the status of extensions. If this extension has some
     * unsaved editing layer (and save them), and methods to check if the
     * extension has some associated background tasks.
     *
     * @author Vicente Caballero Navarro
     *
     */
    private class StopEditingStatus implements IExtensionStatus {

        /**
         * This method is used to check if this extension has some unsaved
         * editing layer.
         *
         * @return true if the extension has some unsaved editing layer, false
         * otherwise.
         */
        public boolean hasUnsavedData() {
            Project project = ProjectManager.getInstance().getCurrentProject();
            DefaultViewDocument[] views = project.getDocuments(
                    ViewManager.TYPENAME).toArray(new DefaultViewDocument[0]);
            for ( int i = 0; i < views.length; i++ ) {
                FLayers layers = views[i].getMapContext().getLayers();
                LayersIterator iter = getEditingLayer(layers);
                if ( iter.hasNext() ) {
                    return true;
                }
            }
            return false;
        }

        /**
         * This method is used to check if the extension has some associated
         * background process which is currently running.
         *
         * @return true if the extension has some associated background process,
         * false otherwise.
         */
        public boolean hasRunningProcesses() {
            return false;
        }

        /**
         * <p>
         * Gets an array of the traceable background tasks associated with this
         * extension. These tasks may be tracked, canceled, etc.
         * </p>
         *
         * @return An array of the associated background tasks, or null in case
         * there is no associated background tasks.
         */
        public IMonitorableTask[] getRunningProcesses() {
            return null;
        }

        /**
         * <p>
         * Gets an array of the UnsavedData objects, which contain information
         * about the unsaved editing layers and allows to save it.
         * </p>
         *
         * @return An array of the associated unsaved editing layers, or null in
         * case the extension has not unsaved editing layers.
         */
        public IUnsavedData[] getUnsavedData() {
            Project project = ProjectManager.getInstance().getCurrentProject();
            DefaultViewDocument[] views = project.getDocuments(
                    ViewManager.TYPENAME).toArray(new DefaultViewDocument[0]);
            ArrayList unsavedLayers = new ArrayList();
            for ( int i = 0; i < views.length; i++ ) {
                FLayers layers = views[i].getMapContext().getLayers();
                LayersIterator iter = getEditingLayer(layers);
                while ( iter.hasNext() ) {
                    UnsavedLayer ul = new UnsavedLayer(StopEditing.this);
                    ul.setLayer(iter.nextLayer());
                    unsavedLayers.add(ul);
                }
            }
            return (IUnsavedData[]) unsavedLayers.toArray(new IUnsavedData[0]);
        }
    }

    private LayersIterator getEditingLayer(FLayers layers) {
        return new LayersIterator(layers) {
            public boolean evaluate(FLayer layer) {
                return layer.isEditing();
            }
        };
    }
}
