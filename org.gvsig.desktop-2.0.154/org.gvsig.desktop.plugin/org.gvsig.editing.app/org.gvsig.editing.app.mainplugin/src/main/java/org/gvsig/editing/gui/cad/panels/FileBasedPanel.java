/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.panels;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import jwizardcomponent.JWizardComponents;
import jwizardcomponent.JWizardPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.addlayer.AddLayerDialog;
import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.utils.SimpleFileFilter;


public class FileBasedPanel extends JWizardPanel {

	private static final long serialVersionUID = -1431370928697152515L;
	private JLabel jLabel = null;
	private JTextField jTextFieldPath = null;
	private JButton jButtonSelectPath = null;
	private CRSSelectPanel crsSelectPanel = null;
	private String fileExt;

//	private class MyInputEventListener implements CaretListener
//	{
//		public void caretUpdate(CaretEvent arg0) {
//			if (jTextFieldPath.getText().length() > 0)
//				setFinishButtonEnabled(true);
//			else
//				setFinishButtonEnabled(false);
//
//		}
//
//	}


	public FileBasedPanel(JWizardComponents wizardComponents) {
		super(wizardComponents);
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        jLabel = new JLabel();
        jLabel.setText(PluginServices.getText(this,"enter_path_to_file"));
        jLabel.setBounds(new java.awt.Rectangle(12,17,319,15));
        this.setLayout(null);
        this.setSize(new java.awt.Dimension(380,214));
        this.add(jLabel, null);
        this.add(getJTextFieldPath(), null);
        this.add(getJButtonSelectPath(), null);

        this.add(getChooserPanel(), null);
        setFinishButtonEnabled(false);
	}

	/**
	 * This method initializes jTextFieldPath
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldPath() {
		if (jTextFieldPath == null) {
			jTextFieldPath = new JTextField();
			jTextFieldPath.setPreferredSize(new java.awt.Dimension(210,20));
			jTextFieldPath.setBounds(new java.awt.Rectangle(12,38,319,23));
			jTextFieldPath.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyReleased(KeyEvent arg0) {
					if (!jTextFieldPath.getText().equals(""))
						setFinishButtonEnabled(true);
					else
						setFinishButtonEnabled(false);
				}

			});
		}
		return jTextFieldPath;
	}

	/**
	 * This method initializes jButtonSelectPath
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonSelectPath() {
		if (jButtonSelectPath == null) {
			jButtonSelectPath = new JButton();
			jButtonSelectPath.setText("...");
			jButtonSelectPath.setBounds(new java.awt.Rectangle(332,38,32,22));
			jButtonSelectPath.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
		            JFileChooser jfc = new JFileChooser();
		            SimpleFileFilter filterShp = new SimpleFileFilter(fileExt, PluginServices.getText(this,"file")+" "+fileExt);
		            jfc.setFileFilter(filterShp);
		            if (jfc.showSaveDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
		        		    File newFile = jfc.getSelectedFile();
		        		    String path = newFile.getAbsolutePath();
		        		    if (!(path.toLowerCase().endsWith("." + fileExt)))
		        		    {
		        		    	path = path + "." + fileExt;
		        		    }
		        		    jTextFieldPath.setText(path);
		        		    setFinishButtonEnabled(true);
		            }else{
		            	setFinishButtonEnabled(false);
		            }

				}
			});
		}
		return jButtonSelectPath;
	}

	public String getPath() {
		return jTextFieldPath.getText();
	}

	/**
	 * Use it to set the extension of the file you want to receive.
	 * (Without . : Example: for shps: shp for dxfs: dxf)
	 * @param extension
	 */
	public void setFileExtension(String extension)
	{
		this.fileExt = extension;
	}

	/**
	 * This method initializes chooserPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private CRSSelectPanel getChooserPanel() {
		if (crsSelectPanel == null) {
			crsSelectPanel = CRSSelectPanel.getPanel(AddLayerDialog.getLastProjection());
			crsSelectPanel.setBounds(new java.awt.Rectangle(16,98,348,44));
			IWindow view= PluginServices.getMDIManager().getActiveWindow();
			if (view instanceof org.gvsig.app.project.documents.view.gui.DefaultViewPanel){
				if (((DefaultViewPanel)view).getMapControl().getMapContext().getLayers().getLayersCount()!=0){
					crsSelectPanel.getJBtnChangeProj().setEnabled(false);
				}
			}
			crsSelectPanel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
			        if (crsSelectPanel.isOkPressed()) {
			        	AddLayerDialog.setLastProjection(crsSelectPanel.getCurProj());
			        }
				}
			});
		}
		return crsSelectPanel;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
