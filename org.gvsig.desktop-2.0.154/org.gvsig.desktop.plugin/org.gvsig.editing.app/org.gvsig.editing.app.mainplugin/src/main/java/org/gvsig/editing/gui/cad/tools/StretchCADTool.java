/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.StretchCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.StretchCADToolContext.StretchCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Herramienta para estirar los handlers que seleccionemos previamente.
 * 
 * @author Vicente Caballero Navarro
 */
public class StretchCADTool extends DefaultCADTool {

    protected StretchCADToolContext _fsm;
    protected Point2D selfirstPoint;
    protected Point2D sellastPoint;
    protected Point2D movefirstPoint;
    protected Point2D movelastPoint;
    protected Rectangle2D rect = null;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new StretchCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_stretch");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        StretchCADToolState actualState =
            (StretchCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Stretch.SelFirstPoint")) {
            selfirstPoint = new Point2D.Double(x, y);
        } else
            if (status.equals("Stretch.SelLastPoint")) {
                sellastPoint = new Point2D.Double(x, y);

                double x1;
                double y1;
                double w1;
                double h1;

                if (selfirstPoint.getX() < sellastPoint.getX()) {
                    x1 = selfirstPoint.getX();
                    w1 = sellastPoint.getX() - selfirstPoint.getX();
                } else {
                    x1 = sellastPoint.getX();
                    w1 = selfirstPoint.getX() - sellastPoint.getX();
                }

                if (selfirstPoint.getY() < sellastPoint.getY()) {
                    y1 = selfirstPoint.getY();
                    h1 = sellastPoint.getY() - selfirstPoint.getY();
                } else {
                    y1 = sellastPoint.getY();
                    h1 = selfirstPoint.getY() - sellastPoint.getY();
                }

                rect = new Rectangle2D.Double(x1, y1, w1, h1);
            } else
                if (status.equals("Stretch.MoveFirstPoint")) {
                    movefirstPoint = new Point2D.Double(x, y);
                } else
                    if (status.equals("Stretch.MoveLastPoint")) {
                        VectorialLayerEdited vle = getVLE();
                        FeatureStore featureStore = null;
                        DisposableIterator iterator = null;
                        try {
                            featureStore = vle.getFeatureStore();

                            // VectorialEditableAdapter vea=vle.getVEA();
                            featureStore.beginEditingGroup(getName());
                            // ArrayList selectedRow=getSelectedRows();
                            // ArrayList selectedRowAux = new ArrayList();
                            // PluginServices.getMDIManager().setWaitCursor();
                            movelastPoint = new Point2D.Double(x, y);

                            Handler[] handlers = null;

                            // for (int i = selectedGeometries.nextSetBit(0); i
                            // >= 0;
                            // i = selectedGeometries.nextSetBit(i + 1)) {
                            iterator =
                                ((FeatureSelection) featureStore.getSelection())
                                    .iterator();
                            while (iterator.hasNext()) {
                                Feature feature = (Feature) iterator.next();

                                // }
                                // for (int i =0;i<selectedRow.size(); i++) {
                                // IRowEdited edRow = (IRowEdited)
                                // selectedRow.get(i);
                                // DefaultFeature fea = (DefaultFeature)
                                // edRow.getLinkedRow().cloneRow();
                                Geometry geometry = null;
                                geometry =
                                    (feature.getDefaultGeometry())
                                        .cloneGeometry();

                                handlers =
                                    geometry
                                        .getHandlers(Geometry.STRETCHINGHANDLER);

                                for (int j = 0; j < handlers.length; j++) {
                                    if (rect.contains(handlers[j].getPoint())) {
                                        handlers[j].move(movelastPoint.getX()
                                            - movefirstPoint.getX(),
                                            movelastPoint.getY()
                                                - movefirstPoint.getY());
                                    }
                                }
                                super.updateGeometry(featureStore, feature, geometry);
//                                EditableFeature eFeature =
//                                    feature.getEditable();
//                                eFeature.setGeometry(featureStore
//                                    .getDefaultFeatureType()
//                                    .getDefaultGeometryAttributeName(),
//                                    geometry);
//                                // vea.modifyRow(edRow.getIndex(),fea,getName(),EditionEvent.GRAPHIC);
//                                featureStore.update(eFeature);
//                                // selectedRowAux.add(feature);
                            }
                            featureStore.endEditingGroup();
                            // vle.setSelectionCache(VectorialLayerEdited.NOTSAVEPREVIOUS,
                            // selectedRowAux);

                            // PluginServices.getMDIManager().restoreCursor();
                        } catch (DataException e) {
                            NotificationManager.addError(e.getMessage(), e);
                        } finally {
                            if (iterator != null) {
                                iterator.dispose();
                            }
                        }
                    }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        StretchCADToolState actualState = (_fsm).getState();
        String status = actualState.getName();

        // ArrayList selectedRow = getSelectedRows();
        DisposableIterator iterator = null;
        try {
            iterator =
                ((FeatureSelection) getVLE().getFeatureStore().getSelection())
                    .iterator();
            if (status.equals("Stretch.SelLastPoint")) {
                GeneralPathX elShape =
                    new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 4);
                elShape.moveTo(selfirstPoint.getX(), selfirstPoint.getY());
                elShape.lineTo(x, selfirstPoint.getY());
                elShape.lineTo(x, y);
                elShape.lineTo(selfirstPoint.getX(), y);
                elShape.lineTo(selfirstPoint.getX(), selfirstPoint.getY());

                renderer.draw(createCurve(elShape),
                    mapControlManager.getAxisReferenceSymbol());
            } else
                if (status.equals("Stretch.MoveFirstPoint")) {

                    Handler[] handlers = null;
                    while (iterator.hasNext()) {
                        Feature feature = (Feature) iterator.next();

                        Geometry geometry = null;
                        geometry =
                            (feature.getDefaultGeometry()).cloneGeometry();

                        handlers =
                            geometry.getHandlers(Geometry.STRETCHINGHANDLER);

                        for (int j = 0; j < handlers.length; j++) {
                            if (rect.contains(handlers[j].getPoint())) {
                                renderer.drawHandlers(handlers,
                                    getCadToolAdapter().getMapControl()
                                        .getViewPort().getAffineTransform(),
                                    mapControlManager.getHandlerSymbol());
                            }
                        }
                    }
                } else
                    if (status.equals("Stretch.MoveLastPoint")) {
                        Handler[] handlers = null;
                        while (iterator.hasNext()) {
                            Feature feature = (Feature) iterator.next();
                            // for (int i = 0;i<selectedRow.size();i++) {
                            // IRowEdited edRow = (IRowEdited)
                            // selectedRow.get(i);
                            // DefaultFeature fea = (DefaultFeature)
                            // edRow.getLinkedRow().cloneRow();
                            Geometry geometry = null;
                            geometry =
                                (feature.getDefaultGeometry()).cloneGeometry();

                            handlers =
                                geometry
                                    .getHandlers(Geometry.STRETCHINGHANDLER);

                            for (int j = 0; j < handlers.length; j++) {
                                if (rect.contains(handlers[j].getPoint())) {
                                    handlers[j].move(x - movefirstPoint.getX(),
                                        y - movefirstPoint.getY());
                                }
                            }
                            renderer.draw(geometry,
                                mapControlManager.getAxisReferenceSymbol());
                        }
                    }
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }

    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    public String getName() {
        return PluginServices.getText(this, "stretch_");
    }

    public String toString() {
        return "_stretch";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { CURVE, SURFACE, AGGREGATE, MULTICURVE,
            MULTISURFACE, CIRCLE, ARC, ELLIPSE, SPLINE, ELLIPTICARC };
    }
}
