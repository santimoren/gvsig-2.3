/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.SymmetryCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.SymmetryCADToolContext.SymmetryCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.handler.Handler;
//import org.gvsig.fmap.geom.operation.perpendicular.Perpendicular;
//import org.gvsig.fmap.geom.operation.perpendicular.PerpendicularOperationContext;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Herramienta para crear una geometr�a sim�trica a otra, con la posibilidad de
 * borrar la original.
 * 
 * @author Vicente Caballero Navarro
 */
public class SymmetryCADTool extends DefaultCADTool {

    protected SymmetryCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D secondPoint;

    /**
     * M�todo de inicio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new SymmetryCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet,
     * java.lang.String)
     */
    public void transition(String s) throws CommandException {
        // if (!super.changeCommand(s)) {
        _fsm.addOption(s);
        // }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_symmetry");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        SymmetryCADToolState actualState =
            (SymmetryCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        if (status.equals("Symmetry.FirstPoint")) {
            firstPoint = new Point2D.Double(x, y);
        } else
            if (status.equals("Symmetry.SecondPoint")) {
                secondPoint = new Point2D.Double(x, y);
            }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        SymmetryCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Symmetry.SecondPoint")) {
            Point2D pAux = new Point2D.Double(x, y);
            drawSymmetry(renderer, pAux);
        } else
            if (status.equals("Symmetry.CutOrCopy")) {
                drawSymmetry(renderer, secondPoint);
            }
    }

    private void drawSymmetry(MapControlDrawer renderer, Point2D pAux) {

        VectorialLayerEdited vle = getVLE();
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) vle.getFeatureStore().getSelection();
        } catch (ReadException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ViewPort vp = vle.getLayer().getMapContext().getViewPort();

        GeneralPathX gpx = new GeneralPathX();
        gpx.moveTo(firstPoint.getX(), firstPoint.getY());
        gpx.lineTo(pAux.getX(), pAux.getY());

        renderer.draw(createCurve(gpx),
            mapControlManager.getAxisReferenceSymbol());

        DisposableIterator iterator = null;
        try {
            iterator = selection.iterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();

                // }
                // for (int i = 0; i < selectedRow.size(); i++) {
                // DefaultRowEdited row=(DefaultRowEdited) selectedRow.get(i);
                // DefaultFeature fea = (DefaultFeature) row.getLinkedRow();

                Geometry geom = (feature.getDefaultGeometry()).cloneGeometry();
                Handler[] handlers = geom.getHandlers(Geometry.SELECTHANDLER);

                for (int j = 0; j < handlers.length; j++) {
                    Handler h = handlers[j];
                    Point2D p = h.getPoint();
                    Point2D[] ps =
                        UtilFunctions.getPerpendicular(firstPoint, pAux, p);
                    Point2D inter =
                        UtilFunctions.getIntersection(ps[0], ps[1], firstPoint,
                            pAux);
                    if (inter != null) {
                        Point2D dif =
                            new Point2D.Double(inter.getX() - p.getX(),
                                inter.getY() - p.getY());
                        h.set(inter.getX() + dif.getX(),
                            inter.getY() + dif.getY());
                    }
                }
                renderer.draw(geom, mapControlManager.getAxisReferenceSymbol());
            }
        } catch (DataException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        
        if (s.equals(Messages.getText("cancel"))) {
            refresh();
            /*
             * Nothing to do
             */
            return;
        }
        
        SymmetryCADToolState actualState =
            (SymmetryCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        FeatureSelection selection = null;
        try {
            featureStore = vle.getFeatureStore();
            selection = featureStore.getFeatureSelection();
        } catch (DataException e1) {
            NotificationManager.addError(e1.getMessage(), e1);
            return;
        }
        // ArrayList selectedRowAux=new ArrayList();
        // VectorialEditableAdapter vea = vle.getVEA();
        if (status.equals("Symmetry.CutOrCopy")) {
            PluginServices.getMDIManager().setWaitCursor();
            DisposableIterator iterator = null;
            try {
                featureStore.beginEditingGroup(getName());
                // If not moved, deselect the previous features to select the new ones
                try {
                	FeatureSelection newSelection =
                            featureStore.createFeatureSelection();
                    iterator = selection.iterator();
                    while (iterator.hasNext()) {
                        Feature feature = (Feature) iterator.next();

                        Geometry geom =
                            feature.getDefaultGeometry().cloneGeometry();
                        Handler[] handlers =
                            geom.getHandlers(Geometry.SELECTHANDLER);

                        for (int j = 0; j < handlers.length; j++) {
                            Handler h = handlers[j];
                            Point2D p = h.getPoint();
                            Point2D[] ps = getPerpendicularLine(
                                firstPoint, secondPoint, p);
                            Point2D inter =
                                UtilFunctions.getIntersection(ps[0], ps[1],
                                    firstPoint, secondPoint);
                            Point2D dif =
                                new Point2D.Double(inter.getX() - p.getX(),
                                    inter.getY() - p.getY());
                            h.set(inter.getX() + dif.getX(),
                                inter.getY() + dif.getY());
                        }
                        
                        // geom.invokeOperation(index, ctx)

                        String yes_low_case =
                            Messages.getText("SymmetryCADTool.yes").toLowerCase();
                        String yes_up_case = yes_low_case.toUpperCase(); 

                        if (s.equals(PluginServices.getText(this, "cut"))
                            || s.equals(yes_low_case) || s.equals(yes_up_case)) {
                        	super.updateGeometry(featureStore, feature, geom);
//                            EditableFeature eFeature = feature.getEditable();
//                            eFeature.setGeometry(featureStore
//                                .getDefaultFeatureType()
//                                .getDefaultGeometryAttributeName(), geom);
//                            featureStore.update(eFeature);
                            newSelection.select(feature);
                        } else {
                            Feature newFeature = insertGeometry(geom);
                            newSelection.select(newFeature);
                        }
                        refresh();

                    }
                    featureStore.setSelection(newSelection);
                } finally {
                    featureStore.endEditingGroup();
                }
            } catch (DataException e) {
                NotificationManager.addError(e.getMessage(), e);
            } finally {
                if (iterator != null) {
                    iterator.dispose();
                }
                PluginServices.getMDIManager().restoreCursor();
            }

        }
    }

    public void addValue(double d) {

    }

    public String getName() {
        return PluginServices.getText(this, "symmetry_");
    }

    public String toString() {
        return "_symmetry";
    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }
    
    private Point2D[] getPerpendicularLine(
        Point2D p1, Point2D p2, Point2D pp) throws CreateGeometryException {
        
        try {
            Point g_p1 = geomManager.createPoint(
                p1.getX(), p1.getY(), Geometry.SUBTYPES.GEOM2D); 
            Point g_p2 = geomManager.createPoint(
                p2.getX(), p2.getY(), Geometry.SUBTYPES.GEOM2D); 
            Point g_pp = geomManager.createPoint(
                pp.getX(), pp.getY(), Geometry.SUBTYPES.GEOM2D); 
            
            Point[] res = UtilFunctions.perpendicular(g_p1, g_p2, g_pp);

            Point2D[] resp = new Point2D[2];
            resp[0] = new Point2D.Double(res[0].getX(), res[0].getY());
            resp[1] = new Point2D.Double(res[1].getX(), res[1].getY());
            return resp;
            
        } catch (Exception ex) {
            throw new CreateGeometryException(
                Geometry.TYPES.POINT,
                Geometry.SUBTYPES.GEOM2D,
                ex);
        }
    }


}
