/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.ArcCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.ArcCADToolContext.ArcCADToolState;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;

/**
 * 
 * @author Vicente Caballero Navarro
 */
public class ArcCADTool extends AbstractCurveCADTool {

    private ArcCADToolContext _fsm;
    private Point p1;
    private Point p2;
    private Point p3;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new ArcCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        ArcCADToolState actualState = (ArcCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Arc.FirstPoint")) {
            p1 = createPoint(x, y);
        } else
            if (status.equals("Arc.SecondPoint")) {
                p2 = createPoint(x, y);
            } else
                if (status.equals("Arc.ThirdPoint")) {
                    p3 = createPoint(x, y);
                    Geometry ig = createArc(p1, p2, p3);
                    if (ig != null) {
                        insertAndSelectGeometry(ig);
                    } else {
                        ApplicationLocator.getManager().message(
                            Messages.getText("_Unable_to_create_arc"),
                            JOptionPane.ERROR_MESSAGE);
                    }
                }
    }

    /**
     * M�todo para dibujar lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        ArcCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Arc.SecondPoint")) {
            renderer.drawLine(new Point2D.Double(p1.getX(), p1.getY()),
                new Point2D.Double(x, y),
                mapControlManager.getGeometrySelectionSymbol());
        } else
            if (status.equals("Arc.ThirdPoint")) {
                Point current = createPoint(x, y);

                Geometry ig = createArc(p1, p2, current);
                
                if (ig != null) {
                    /*
                     * sometims it's not possible to create arc
                     * (example: aligned points)
                     */
                    renderer.draw(ig,
                        mapControlManager.getGeometrySelectionSymbol());

                    Point2D p =
                        getCadToolAdapter().getMapControl().getViewPort()
                            .fromMapPoint(p1.getX(), p1.getY());
                    renderer.drawRect((int) p.getX(), (int) p.getY(), 1, 1);
                    p =
                        getCadToolAdapter().getMapControl().getViewPort()
                            .fromMapPoint(p2.getX(), p2.getY());
                    renderer.drawRect((int) p.getX(), (int) p.getY(), 1, 1);
                }
            }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        // Nothing to do
    }

    public void addValue(double d) {
        // Nothing to do
    }

    public String getName() {
        return PluginServices.getText(this, "arc_");
    }

    public String toString() {
        return "_arc";
    }

    @Override
    protected int getSupportedPrimitiveGeometryType() {
        return ARC;
    }

}
