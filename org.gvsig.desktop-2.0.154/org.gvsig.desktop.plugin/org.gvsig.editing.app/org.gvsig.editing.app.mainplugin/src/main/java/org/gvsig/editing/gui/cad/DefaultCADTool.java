/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad;

import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.cresques.cts.ICoordTrans;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.EditionLocator;
import org.gvsig.editing.IEditionManager;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Arc;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Ellipse;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Spline;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.layers.SpatialCache;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.utils.console.JConsole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Vicente Caballero Navarro
 */
public abstract class DefaultCADTool implements CADTool, Geometry.TYPES,
    Geometry.SUBTYPES {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultCADTool.class);

    /**
     * Reference to the MapControl library manager, used to manage
     * tools.
     */
    protected MapControlManager mapControlManager = MapControlLocator
        .getMapControlManager();
    protected GeometryManager geomManager = GeometryLocator
        .getGeometryManager();

    private CADToolAdapter cadToolAdapter;

    private String question;

    private String[] currentdescriptions;

    private String tool = "selection";

    private DefaultCADTool previousTool;

    private List<Geometry> temporalCache = new ArrayList<Geometry>();

    private GeometryType[] supportedTypes;

    /**
     * Crea un nuevo DefaultCADTool.
     */
    public DefaultCADTool() {
        loadGeometryTypes();
    }

    protected void loadGeometryTypes() {
        int[] types = getSupportedGeometryTypes();
        if (types == null) {
            supportedTypes = new GeometryType[0];
        }
        else {
            supportedTypes = new GeometryType[types.length];
            for (int i = 0; i < types.length; i++) {
                supportedTypes[i] = loadGeometryType(types[i], GEOM2D);
            }
        }
    }

    public void addTemporalCache(Geometry geom) {
        
        if (geom == null) {
            try {
                throw new IllegalArgumentException("Null geometry");
            } catch (Exception ex) {
                LOG.info("Tried to add null geometry to temporal cache", ex);
            }
            return;
        }
        
        temporalCache.add(geom);
        try {
            insertSpatialCache(geom);
        } catch (CreateEnvelopeException e) {
            LOG.error("Error adding the spatial cache", e);
        }
    }

    public void clearTemporalCache() {
        Geometry[] geoms =
            temporalCache.toArray(new Geometry[temporalCache.size()]);
        for (int i = 0; i < geoms.length; i++) {
            try {
                removeSpatialCache(geoms[i]);
            } catch (CreateEnvelopeException e) {
                LOG.error("Error removing the temporal cache", e);
            }
        }
        temporalCache.clear();
    }

    private void insertSpatialCache(Geometry geom)
        throws CreateEnvelopeException {
        VectorialLayerEdited vle = getVLE();
        SpatialCache spatialCache =
            ((FLyrVect) vle.getLayer()).getSpatialCache();
        Envelope r = geom.getEnvelope();
        if (geom.getType() == Geometry.TYPES.POINT) {
            r =
                geomManager.createEnvelope(r.getMinimum(0), r.getMinimum(1),
                    r.getMinimum(0) + 1, r.getMinimum(1) + 1, SUBTYPES.GEOM2D);
        }
        spatialCache.insert(r, geom);

    }

    private void removeSpatialCache(Geometry geom)
        throws CreateEnvelopeException {
        VectorialLayerEdited vle = getVLE();
        SpatialCache spatialCache =
            ((FLyrVect) vle.getLayer()).getSpatialCache();
        Envelope r = geom.getEnvelope();
        if (geom.getType() == Geometry.TYPES.POINT) {
            r =
                geomManager.createEnvelope(r.getMinimum(0), r.getMinimum(1),
                    r.getMinimum(0) + 1, r.getMinimum(1) + 1, SUBTYPES.GEOM2D);// Rectangle2D.Double(r.getX(),r.getY(),1,1);
        }
        spatialCache.remove(r, geom);

    }

    /**
     * DOCUMENT ME!
     * 
     * @param feature
     */
    public void draw(Geometry geometry, Feature feature) {
        if (geometry != null) {
            getCadToolAdapter().getMapControl().getMapControlDrawer()
                .draw(geometry, mapControlManager.getGeometrySelectionSymbol());
        }
    }

    /**
     * It draw the selected geometries from the initial point until a new
     * point.
     * 
     * @param mapControlDrawer
     *            The drawer.
     * @param firstPoint
     *            The initial point.
     * @param x
     *            The X coordinate of the mouse.
     * @param y
     *            The Y coordinate of the mouse.
     */
    protected void drawSelectedGeometries(MapControlDrawer mapControlDrawer,
        Point2D firstPoint, double x, double y) {
        FeatureStore featureStore = null;
        DisposableIterator iterator = null;
        ICoordTrans ct = getVLE().getLayer().getCoordTrans();

        try {
            featureStore = getVLE().getFeatureStore();
            FeatureSet selection = (FeatureSet) featureStore.getSelection();

            iterator = selection.fastIterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Geometry geometry =
                    (feature.getDefaultGeometry()).cloneGeometry();

                if (geometry == null) {
                    continue;
                }

                Point2D currentPoint = new Point2D.Double(x, y);
                if (ct != null) {
                    currentPoint = ct.getInverted().convert(currentPoint, null);
                }

                geometry.move(currentPoint.getX() - firstPoint.getX(),
                    currentPoint.getY() - firstPoint.getY());

                mapControlDrawer.draw(geometry,
                    mapControlManager.getGeometrySelectionSymbol());
            }
        } catch (Exception e) {
            LOG.error("Retrieving the selection", e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    /**
     * It draw the selected geometries from the initial point until a new
     * point.
     * 
     * @param mapControlDrawer
     *            The drawer.
     * @param firstPoint
     *            The initial point.
     * @param x
     *            The X coordinate of the mouse.
     * @param y
     *            The Y coordinate of the mouse.
     * @param affineTransform
     *            The transformation to apply
     */
    protected void drawAndRotateSelectedGeometries(
        MapControlDrawer mapControlDrawer, Point2D firstPoint, double x,
        double y) {
        FeatureStore featureStore = null;
        DisposableIterator iterator = null;

        try {
            Point2D lastPoint = new Point2D.Double(x, y);

            double w = lastPoint.getX() - firstPoint.getX();
            double h = lastPoint.getY() - firstPoint.getY();

            featureStore = getVLE().getFeatureStore();
            FeatureSet selection = (FeatureSet) featureStore.getSelection();

            iterator = selection.fastIterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Geometry geometry =
                    (feature.getDefaultGeometry()).cloneGeometry();

                if (geometry == null) {
                    continue;
                }

                geometry.rotate(-Math.atan2(w, h) + (Math.PI / 2),
                    firstPoint.getX(), firstPoint.getY());

                mapControlDrawer.draw(geometry,
                    mapControlManager.getGeometrySelectionSymbol());
            }
        } catch (Exception e) {
            LOG.error("Retrieving the selection", e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    public void setCadToolAdapter(CADToolAdapter cta) {
        cadToolAdapter = cta;
    }

    public CADToolAdapter getCadToolAdapter() {
        return cadToolAdapter;
    }

    public VectorialLayerEdited getVLE() {
        
        return (VectorialLayerEdited) EditionLocator.getEditionManager()
            .getActiveLayerEdited();
    }

    public Feature insertGeometry(Geometry geometry, Feature feature) {
    	return this.getEditionManager().insertGeometry(geometry,feature);
//
//    	VectorialLayerEdited vle = getVLE();
//
//        try {
//            FeatureStore featureStore =
//                ((FLyrVect) vle.getLayer()).getFeatureStore();
//            EditableFeature eFeature =
//                featureStore.createNewFeature(
//                    featureStore.getDefaultFeatureType(), feature);
//            eFeature.setGeometry(featureStore.getDefaultFeatureType()
//                .getDefaultGeometryAttributeName(), geometry);
//            featureStore.insert(eFeature);
//            // drawToImage(featureStore, vle, eFeature);
//            insertSpatialCache(geometry);
//
//            getCadToolAdapter().getMapControl().getMapControlDrawer()
//                .draw(geometry, mapControlManager.getGeometrySelectionSymbol());
//
//            return eFeature;
//        } catch (ReadException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        } catch (DataException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        } catch (CreateEnvelopeException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        }
    }

    public Feature insertFeature(Feature feature) {
    	return this.getEditionManager().insertFeature(feature);
    }
    
    public Feature insertAndSelectGeometry(Geometry geometry) {
    	return this.getEditionManager().insertAndSelectGeometry(getName(),geometry);
//
//    	Feature feature = null;
//        try {
//            FeatureStore featureStore = getVLE().getFeatureStore();
//            featureStore.beginComplexNotification();
//            featureStore.beginEditingGroup(getName());
//            FeatureSelection newSelection =
//                featureStore.createFeatureSelection();
//            feature = insertGeometry(geometry);
//            newSelection.select(feature);
//            featureStore.setSelection(newSelection);
//            featureStore.endEditingGroup();
//            featureStore.endComplexNotification();
//        } catch (DataException e) {
//            NotificationManager.showMessageError("insertAndSelectGeoemtry", e);
//        }
//        return feature;
    }

    public void updateGeometry(FeatureStore store, Feature feature, Geometry geometry) {
    	this.getEditionManager().updateGeometry(store, feature, geometry);
    }
    
    protected IEditionManager getEditionManager() {
    	return this.getCadToolAdapter().getEditionManager();
    }
    
    public Feature insertGeometry(Geometry geometry) {
    	return this.getEditionManager().insertGeometry(geometry);
//    	
//        VectorialLayerEdited vle = getVLE();
//
//        try {
//            FeatureStore featureStore =
//                ((FLyrVect) vle.getLayer()).getFeatureStore();
//            EditableFeature eFeature = featureStore.createNewFeature(true);
//           
//            //Reproject the geometry
//            Geometry insertedGeometry = geometry;
//            if (getVLE().getLayer().getCoordTrans() != null){
//                insertedGeometry = insertedGeometry.cloneGeometry();
//                insertedGeometry.reProject(getVLE().getLayer().getCoordTrans().getInverted());
//            }
//            
//            eFeature.setGeometry(featureStore.getDefaultFeatureType()
//                .getDefaultGeometryAttributeName(), insertedGeometry);
//            featureStore.insert(eFeature);
//
//            insertSpatialCache(insertedGeometry);
//
//            draw(insertedGeometry, eFeature);
//            return eFeature;
//        } catch (ReadException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        } catch (DataException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        } catch (CreateEnvelopeException e) {
//            NotificationManager.addError(e.getMessage(), e);
//            return null;
//        }

    }

    /**
     * Devuelve la cadena que corresponde al estado en el que nos encontramos.
     * 
     * @return Cadena para mostrar por consola.
     */
    public String getQuestion() {
        return question;
    }

    /**
     * Actualiza la cadena que corresponde al estado actual.
     * 
     * @param s
     *            Cadena que aparecer� en consola.
     */
    public void setQuestion(String s) {
        question = s;
    }

    /**
     * Provoca un repintado "soft" de la capa activa en edici�n. Las capas por
     * debajo de ella no se dibujan de verdad, solo se dibuja la que est� en
     * edici�n y las que est�n por encima de ella en el TOC.
     */
    public void refresh() {
        getCadToolAdapter().getMapControl().rePaintDirtyLayers();
    }

    public void drawHandlers(MapControlDrawer renderer, ArrayList selectedRows,
        AffineTransform at) {
        FeatureSet selection = null;
        DisposableIterator iterator = null;
        try {
            selection =
                (FeatureSet) ((FLyrVect) getVLE().getLayer()).getFeatureStore()
                    .getSelection();

            iterator = selection.fastIterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();

                Geometry ig = (feature.getDefaultGeometry()).cloneGeometry();
                if (ig == null) {
                    continue;
                }
                Handler[] handlers = ig.getHandlers(Geometry.SELECTHANDLER);
                renderer.drawHandlers(handlers, at,
                    mapControlManager.getGeometrySelectionSymbol());
            }

        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }

    }

    public void setDescription(String[] currentdescriptions) {
        this.currentdescriptions = currentdescriptions;
    }

    public String[] getDescriptions() {
        return currentdescriptions;
    }

    public void end() {
        CADExtension.setCADTool("_selection", true);
        PluginServices.getMainFrame().setSelectedTool("_selection");
        CADTool cadtool = CADExtension.getCADTool();
        cadtool.setPreviosTool(this);
        cadtool.getVLE().getLayer().getMapContext().invalidate();
    }

    public void init() {
        // Nothing to do
    }

    protected ArrayList getSelectedHandlers() {
        VectorialLayerEdited vle = getVLE();
        ArrayList selectedHandlers = vle.getSelectedHandler();
        return selectedHandlers;
    }

    public void clearSelection() throws DataException {
        VectorialLayerEdited vle = getVLE();
        FeatureSelection selection = null;
        selection =
            (FeatureSelection) ((FLyrVect) vle.getLayer()).getFeatureStore()
                .getSelection();
        // ArrayList selectedRow = vle.getSelectedRow();
        ArrayList selectedHandlers = vle.getSelectedHandler();
        selection.deselectAll();
        selectedHandlers.clear();
        // VectorialEditableAdapter vea = vle.getVEA();
        // FBitSet selection = vea.getSelection();
        // selection.clear();
        vle.setSelectionImage(null);
        vle.setHandlersImage(null);

    }

    public String getNextTool() {
        return tool;
    }

    public void setNextTool(String tool) {
        this.tool = tool;
    }

    public boolean changeCommand(String name) throws CommandException {
        CADTool[] cadtools = CADExtension.getCADTools();
        for (int i = 0; i < cadtools.length; i++) {
            CADTool ct = cadtools[i];
            if (name.equalsIgnoreCase(ct.getName())
                || name.equalsIgnoreCase(ct.toString())) {
                int type = Geometry.TYPES.POINT;
                try {
                    type = ((FLyrVect) getVLE().getLayer()).getShapeType();
                } catch (ReadException e) {
                    throw new CommandException(e);
                }
                if (ct.isApplicable(type)) {
                    getCadToolAdapter().setCadTool(ct);
                    ct.init();
                    DefaultViewPanel vista =
                        (DefaultViewPanel) PluginServices.getMDIManager()
                            .getActiveWindow();
                    vista.getConsolePanel().addText("\n" + ct.getName(),
                        JConsole.COMMAND);
                    String question = ct.getQuestion();
                    vista.getConsolePanel().addText(
                        "\n" + "#" + question + " > ", JConsole.MESSAGE);
                    return true;
                }
                throw new CommandException(name);
            }
        }
        return false;
    }

    public boolean isApplicable(int shapeType) {
        GeometryType type = loadGeometryType(shapeType, GEOM2D);
        return isApplicable(type);
    }

    public boolean isApplicable(GeometryType geometryType) {
        if (supportedTypes != null) {
            for (int i = 0; i < supportedTypes.length; i++) {
                if (supportedTypes[i].isTypeOf(geometryType)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the geometry types supported by the tool.
     * 
     * @return the supported geometry types
     */
    protected GeometryType[] getSupportedTypes() {
        return supportedTypes;
    }

    /**
     * Returns the list of Geometry types supported by this tool. If any
     * Geometry type is supported, just return null.
     * 
     * @return the list of Geometry types supported by this tool
     */
    protected int[] getSupportedGeometryTypes() {
        return null;
    }

    protected GeometryType loadGeometryType(int type, int subtype) {
        try {
            return GeometryLocator.getGeometryManager().getGeometryType(type,
                subtype);
        } catch (GeometryTypeNotSupportedException e) {
            throw new RuntimeException(
                "Error getting the Geometry type with type = " + type
                    + ", subtype = " + subtype, e);
        } catch (GeometryTypeNotValidException e) {
            throw new RuntimeException(
                "Error getting the Geometry type with type = " + type
                    + ", subtype = " + subtype, e);
        }
    }

    public abstract String toString();

    public void throwValueException(String s, double d) {
        IWindow window = PluginServices.getMDIManager().getActiveWindow();
        if (window instanceof DefaultViewPanel) {
            ((DefaultViewPanel) window).getConsolePanel().addText(
                s + " : " + d, JConsole.ERROR);
        }
    }

    public void throwOptionException(String s, String o) {
        IWindow window = PluginServices.getMDIManager().getActiveWindow();
        if (window instanceof DefaultViewPanel) {
            ((DefaultViewPanel) window).getConsolePanel().addText(
                s + " : " + o, JConsole.ERROR);
        }
    }

    public void throwPointException(String s, double x, double y) {
        IWindow window = PluginServices.getMDIManager().getActiveWindow();
        if (window instanceof DefaultViewPanel) {
            ((DefaultViewPanel) window).getConsolePanel().addText(
                s + " : " + " X = " + x + ", Y = " + y, JConsole.ERROR);
        }
    }

    public void setPreviosTool(DefaultCADTool tool) {
        previousTool = tool;
    }

    public void restorePreviousTool() {
        CADExtension.setCADTool(previousTool.toString(), true);
        PluginServices.getMainFrame().setSelectedTool(previousTool.toString());
    }

    public void endTransition(double x, double y, MouseEvent e) {
        // Nothing to do
    }

    /**
     * Create a curve. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @return
     *         The Curve
     */
    protected Curve createCurve() {
        try {
            return (Curve) geomManager.create(TYPES.CURVE, getSubType());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating curve", new CreateGeometryException(
                TYPES.CURVE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create a multicurve. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @return
     *         The MultiCurve
     */
    protected MultiCurve createMultiCurve() {
        try {
            return (MultiCurve) geomManager.create(TYPES.MULTICURVE,
                getSubType());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating multicurve", new CreateGeometryException(
                TYPES.MULTICURVE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create an {@link OrientablePrimitive}. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param geometryType
     *            a type of a OrientablePrimitive.
     * @return
     *         The {@link OrientablePrimitive}
     */
    protected OrientablePrimitive createOrientablePrimitive(int geometryType) {
        try {
            return (OrientablePrimitive) geomManager.create(geometryType,
                getSubType());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating curve", new CreateGeometryException(
                TYPES.CURVE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create an envelope like a curve from two points.
     * If there is an error return <code>null</code> and add the error to the
     * log.
     * 
     * @param firstPoint
     *            first point
     * @param secondPoint
     *            second point
     * @return
     *         the curve
     */
    protected Curve createEnvelopeLikeCurve(Point2D firstPoint,
        Point2D secondPoint) {
        try {
            Curve curve = (Curve) geomManager.create(TYPES.CURVE, getSubType());
            curve.addMoveToVertex(createPoint(firstPoint.getX(),
                firstPoint.getY()));
            curve.addVertex(createPoint(secondPoint.getX(), firstPoint.getY()));
            curve
                .addVertex(createPoint(secondPoint.getX(), secondPoint.getY()));
            curve.addVertex(createPoint(firstPoint.getX(), secondPoint.getY()));
            curve.addVertex(createPoint(firstPoint.getX(), firstPoint.getY()));
            return curve;
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating curve", new CreateGeometryException(
                TYPES.CURVE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create a curve from a GeneralPath. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param gpx
     *            The GeneralPath
     * @return
     *         The Curve
     */
    protected Curve createCurve(GeneralPathX gpx) {
        Curve curve = null;
        try {
            curve = (Curve) geomManager.create(TYPES.CURVE, getSubType());
            curve.setGeneralPath(gpx);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating curve", new CreateGeometryException(
                TYPES.CURVE, getSubType(), e));
        }
        return curve;
    }

    /**
     * Create a surface. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @return
     *         The Surface
     */
    protected Surface createSurface() {
        try {
            return (Surface) geomManager.create(TYPES.SURFACE, getSubType());

        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating surface", new CreateGeometryException(
                TYPES.SURFACE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create a multisurface. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @return
     *         The MultiSurface
     */
    protected MultiSurface createMultiSurface() {
        try {
            return (MultiSurface) geomManager.create(TYPES.MULTISURFACE,
                getSubType());

        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error(
                "Error creating multisurface",
                new CreateGeometryException(TYPES.MULTISURFACE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create a surface from a GeneralPath. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param gpx
     *            The general path
     * @return
     *         The Surface
     */
    protected Surface createSurface(GeneralPathX gpx) {
        Surface surface = null;
        try {
            surface = (Surface) geomManager.create(TYPES.SURFACE, getSubType());
            surface.setGeneralPath(gpx);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating surface", new CreateGeometryException(
                TYPES.SURFACE, getSubType(), e));
        }
        return surface;
    }

    /**
     * Create a curve point. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     *            The AWT point
     * @return
     *         The gvSIG point
     */
    protected Point createPoint(Point2D p1) {
        return createPoint(p1.getX(), p1.getY());
    }

    /**
     * Create point. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param x
     *            The X coordinate
     * @param y
     *            The y coordinate
     * @return
     *         The Point
     */
    protected Point createPoint(double x, double y) {
        Point point = null;
        try {
            point = (Point) geomManager.create(TYPES.POINT, getSubType());
            point.setX(x);
            point.setY(y);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating point with x=" + x + ", y=" + y,
                new CreateGeometryException(TYPES.POINT, getSubType(), e));
        }
        return point;
    }

    /**
     * Create a multipoint. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param x
     *            The X coordinate
     * @param y
     *            The y coordinate
     * @return
     *         The MultiPoint
     */
    protected MultiPoint createMultiPoint() {
        try {
            return (MultiPoint) geomManager.create(TYPES.MULTIPOINT,
                getSubType());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating multipoint", new CreateGeometryException(
                TYPES.MULTIPOINT, getSubType(), e));
            return null;
        }
    }

    /**
     * Create an Arc. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @param p3
     * @return
     *         The arc
     */
    protected Arc createArc(Point2D p1, Point2D p2, Point2D p3) {
        return createArc(createPoint(p1), createPoint(p2), createPoint(p3));
    }

    /**
     * Create an arc. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @param p3
     * @return
     *         The arc
     */
    protected Arc createArc(Point p1, Point p2, Point p3) {
        Arc arc = null;
        if (p1.equals(p2) || p2.equals(p3) || p1.equals(p3)) {
            return null;
        }
        try {
            arc = (Arc) geomManager.create(TYPES.ARC, getSubType());
            arc.setPoints(p1, p2, p3);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating arc with p1=" + p1, ", p2=" + p2
                + ", p3=" + p3, new CreateGeometryException(TYPES.ARC,
                getSubType(), e));
        } catch (IllegalArgumentException ex) {
            LOG.info("Warning: unable to create arc from points: "
                + p1.getX() + " " + p1.getY() + " :: "
                + p2.getX() + " " + p2.getY() + " :: "
                + p3.getX() + " " + p3.getY());
            arc = null;
        }
        return arc;
    }

    /**
     * Create a circle. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @param p3
     * @return
     *         The Circle
     */
    protected Circle createCircle(Point p1, Point p2, Point p3) {
        Circle circle = null;
        try {
            circle = (Circle) geomManager.create(TYPES.CIRCLE, getSubType());
            circle.setPoints(p1, p2, p3);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating circle with p1=" + p1 + " p2=" + p2
                + ", p3=" + p3, new CreateGeometryException(TYPES.CIRCLE,
                getSubType(), e));
        }
        return circle;
    }

    /**
     * Create a circle from a GeneralPath. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @return
     *         The circle
     */
    protected Circle createCircle(Point2D p1, Point2D p2) {
        return createCircle(createPoint(p1), createPoint(p2));
    }

    /**
     * Create a circle. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @return
     *         The circle
     */
    protected Circle createCircle(Point p1, Point p2) {
        Circle circle = null;
        try {
            circle = (Circle) geomManager.create(TYPES.CIRCLE, getSubType());
            circle.setPoints(p1, p2);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating circle with p1=" + p1 + " p2=" + p2,
                new CreateGeometryException(TYPES.CIRCLE, getSubType(), e));
        }
        return circle;
    }

    /**
     * Create a circle. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param radious
     * @return
     *         The Circle
     */
    protected Circle createCircle(Point2D p1, double radious) {
        return createCircle(createPoint(p1), radious);
    }

    /**
     * Create a circle. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param radious
     * @return
     *         The Circle
     */
    protected Circle createCircle(Point p1, double radious) {
        Circle circle = null;
        try {
            circle = (Circle) geomManager.create(TYPES.CIRCLE, getSubType());
            circle.setPoints(p1, radious);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating circle with p1=" + p1 + " radious="
                + radious, new CreateGeometryException(TYPES.CIRCLE,
                getSubType(), e));
        }
        return circle;
    }

    /**
     * Create an Ellipse. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @param d
     * @return
     *         The Ellipse
     */
    protected Ellipse createEllipse(Point2D p1, Point2D p2, double d) {
        return createEllipse(createPoint(p1), createPoint(p2), d);
    }

    /**
     * Create an Ellipse. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param p1
     * @param p2
     * @param d
     * @return
     *         The Ellipse
     */
    protected Ellipse createEllipse(Point p1, Point p2, double d) {
        Ellipse ellipse = null;
        try {
            ellipse = (Ellipse) geomManager.create(TYPES.ELLIPSE, getSubType());
            ellipse.setPoints(p1, p2, d);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating ellipse with p1=" + p1 + " p2=" + p2
                + ", d=" + d, new CreateGeometryException(TYPES.ELLIPSE,
                getSubType(), e));
        }
        return ellipse;
    }

    /**
     * Create a Spline from a GeneralPath. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param points
     * @return
     *         The Spline
     */
    protected Spline createSpline(Point2D[] points) {
        Spline spline = null;
        try {
            spline = (Spline) geomManager.create(TYPES.SPLINE, getSubType());
            for (int i = 0; i < points.length; i++) {
                spline.addVertex(createPoint(points[i]));
            }
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating spline", new CreateGeometryException(
                TYPES.SPLINE, getSubType(), e));
        }
        return spline;
    }

    /**
     * Create a MultiPrimitive. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @return
     */
    protected MultiPrimitive createMultiPrimitive() {
        try {
            return (MultiPrimitive) geomManager.create(TYPES.AGGREGATE,
                getSubType());
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating MultiPrimitive",
                new CreateGeometryException(TYPES.SPLINE, getSubType(), e));
        }
        return null;
    }

    /**
     * Create a MultiPrimitive. If there is an
     * error return <code>null</code> and add the error
     * to the log
     * 
     * @param geometries
     * @return
     */
    protected MultiPrimitive createMultiPrimitive(Geometry[] geometries) {
        MultiPrimitive multiPrimitive = null;
        try {
            multiPrimitive =
                (MultiPrimitive) geomManager.create(TYPES.AGGREGATE,
                    getSubType());
            for (int i = 0; i < geometries.length; i++) {
                multiPrimitive.addPrimitive((Primitive) geometries[i]);
            }
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating MultiPrimitive",
                new CreateGeometryException(TYPES.SPLINE, getSubType(), e));
        }
        return multiPrimitive;
    }

    /**
     * @return the subtype of the default geometry.
     */
    protected int getSubType() {
        FeatureStore featureStore =
            ((FLyrVect) getVLE().getLayer()).getFeatureStore();
        try {
            return featureStore.getDefaultFeatureType()
                .getDefaultGeometryAttribute().getGeometrySubType();
        } catch (DataException e) {
            LOG.error(
                "Error getting subtype of the default feature type of the store: "
                    + featureStore, e);
            return SUBTYPES.GEOM3D;
        }
    }

    /**
     * Returns the type of the geometries to create.
     * 
     * @return the type of the geometries to create
     */
    protected GeometryType getGeometryType() {
        return getCadToolAdapter().getActiveLayerGeometryType();
    }

}
