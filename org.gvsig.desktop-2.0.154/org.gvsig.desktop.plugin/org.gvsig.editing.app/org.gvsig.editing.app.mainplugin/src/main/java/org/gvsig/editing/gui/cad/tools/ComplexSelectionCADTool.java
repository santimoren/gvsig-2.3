/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.ComplexSelectionCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.ComplexSelectionCADToolContext.ComplexSelectionCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ComplexSelectionCADTool extends SelectionCADTool {

    private static final Logger LOG = LoggerFactory
        .getLogger(ComplexSelectionCADTool.class);

    protected ComplexSelectionCADToolContext _fsm;
    protected List pointsPolygon = new ArrayList();

    /**
     * Crea un nuevo ComplexSelectionCADTool.
     */
    public ComplexSelectionCADTool() {
        type = "";
    }

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new ComplexSelectionCADToolContext(this);
        setNextTool("complex_selection");

        setType("");
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param selection
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        if (event != null && ((MouseEvent) event).getClickCount() == 2) {
            try {
                pointDoubleClick(((MapControl) event.getComponent())
                    .getMapContext());
            } catch (ReadException e) {
                NotificationManager.addError(e.getMessage(), e);
            }
            return;
        }
        ComplexSelectionCADToolState actualState =
            (ComplexSelectionCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        LOG.info("PREVIOUSSTATE =" + status);
        LOG.info("STATUS ACTUAL = " + _fsm.getTransition());
        if (status.equals("Selection.FirstPoint")) {
            firstPoint = new Point2D.Double(x, y);
            pointsPolygon.add(firstPoint);
        } else if (status.equals("Selection.SecondPoint")) {

        } else if (status.equals("Selection.WithSelectedFeatures")) {

        } else if (status.equals("Selection.WithHandlers")) {
            FeatureStore featureStore = null;
            try {
                VectorialLayerEdited vle = getVLE();
                featureStore = vle.getFeatureStore();
                addPointWithHandlers(x, y, featureStore,
                    vle.getSelectedHandler());
            } catch (ReadException e) {
                LOG.error("Error getting the feature store");
            }
        } else if (status.equals("Selection.NextPointPolygon")) {
            pointsPolygon.add(new Point2D.Double(x, y));
        } else if (status.equals("Selection.SecondPointCircle")) {
            selectWithCircle(x, y, event);
        }
    }

    /**
     * Receives second point
     * 
     * @param x
     * @param y
     * @return numFeatures selected
     */
    public long selectWithSecondPointOutRectangle(double x, double y,
        InputEvent event) {
        Point2D lastPoint = new Point2D.Double(x, y);
        Surface surface = createSurface();
        surface.addMoveToVertex(createPoint(firstPoint.getX(),
            firstPoint.getY()));
        surface.addVertex(createPoint(lastPoint.getX(), firstPoint.getY()));
        surface.addVertex(createPoint(lastPoint.getX(), lastPoint.getY()));
        surface.addVertex(createPoint(firstPoint.getX(), lastPoint.getY()));
        surface.closePrimitive();
        return selectWithPolygon(surface);
    }

    /**
     * Receives second point
     * 
     * @param x
     * @param y
     * @return numFeatures selected
     */
    public long selectWithCircle(double x, double y, InputEvent event) {
        Geometry circle = createCircle(firstPoint, new Point2D.Double(x, y));
        return selectWithPolygon(circle);
    }

    public long selectWithPolygon(Geometry polygon) {
        VectorialLayerEdited vle = getVLE();
        PluginServices.getMDIManager().setWaitCursor();

        if (getType().equals(PluginServices.getText(this, "inside_circle"))
            || getType().equals(PluginServices.getText(this, "inside_polygon"))) {
            vle.selectContainsSurface(polygon);
        } else
            if (getType().equals(PluginServices.getText(this, "cross_circle"))
                || getType().equals(
                    PluginServices.getText(this, "cross_polygon"))) {
                vle.selectIntersectsSurface(polygon);
            } else
                if (getType()
                    .equals(PluginServices.getText(this, "out_circle"))
                    || getType().equals(
                        PluginServices.getText(this, "out_polygon"))
                    || getType().equals(
                        PluginServices.getText(this, "out_rectangle"))) {
                    vle.selectOutPolygon(polygon);
                }
        long countSelection = 0;
        try {
            countSelection =
                ((FeatureSelection) vle.getFeatureStore().getSelection())
                    .getSize();
        } catch (ReadException e) {
            LOG.error("Error reading the store", e);
        } catch (DataException e) {
            LOG.error("Error reading the store", e);
        }
        PluginServices.getMDIManager().restoreCursor();
        if (countSelection > 0) {
            nextState = "Selection.WithSelectedFeatures";
            end();
        } else {
            nextState = "Selection.FirstPoint";
        }
        return countSelection;
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        ComplexSelectionCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Selection.SecondPoint")
            || status.equals("Selection.SecondPointOutRectangle")) {
            drawRectangle(renderer, x, y);
        } else
            if (status.equals("Selection.SecondPointCircle")) {
                drawSecondPointCircle(renderer, x, y);
            } else
                if (status.equals("Selection.NextPointPolygon")) {
                    drawNextPointPolygon(renderer, x, y);
                } else
                    if (status.equals("Selection.WithHandlers")) {
                        VectorialLayerEdited vle = getVLE();
                        drawWithHandlers(renderer, x, y,
                            vle.getSelectedHandler());
                    }
    }

    /**
     * Draw method for the nexp point of a polygon.
     * 
     * @param mapControlDrawer
     *            object used to draw.
     * @param x
     *            selected x coordinate.
     * @param y
     *            selected y coordinate.
     */
    private void drawNextPointPolygon(MapControlDrawer mapControlDrawer,
        double x, double y) {
        // Dibuja el pol�gono de selecci�n
        Geometry curve =
            createOrientablePrimitive(new Point2D.Double(x, y),
                Geometry.TYPES.CURVE);
        mapControlDrawer.draw(curve,
            mapControlManager.getGeometrySelectionSymbol());
    }

    /**
     * Draw method for the second point of a "rectangle".
     * 
     * @param mapControlDrawer
     *            object used to draw.
     * @param x
     *            selected x coordinate.
     * @param y
     *            selected y coordinate.
     */
    private void drawRectangle(MapControlDrawer mapControlDrawer, double x,
        double y) {
        Curve curve =
            createEnvelopeLikeCurve(firstPoint, new Point2D.Double(x, y));
        mapControlDrawer.draw(curve);
    }

    /**
     * Draw method for the second point of the "circle" option
     * 
     * @param mapControlDrawer
     *            object used to draw.
     * @param x
     *            selected x coordinate.
     * @param y
     *            selected y coordinate.
     */
    private void drawSecondPointCircle(MapControlDrawer mapControlDrawer,
        double x, double y) {
        Geometry circle = createCircle(firstPoint, new Point2D.Double(x, y));
        GeneralPathX gpx = new GeneralPathX();
        gpx.append(circle.getInternalShape().getPathIterator(null), true);
        Geometry circleSel = createCurve(gpx);
        // Draw the circle
        mapControlDrawer.draw(circleSel,
            mapControlManager.getGeometrySelectionSymbol());
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        ComplexSelectionCADToolState actualState =
            (ComplexSelectionCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        LOG.info("PREVIOUSSTATE =" + status);
        LOG.info("STATUS ACTUAL = " + _fsm.getTransition());
        if (s.equals(PluginServices.getText(this, "cancel"))) {
            init();
            try {
                clearSelection();
            } catch (DataException e) {
                LOG.error("Error canceling the selection", e);
            }
            return;
        } else
            if (s.equals(PluginServices.getText(this, "select_all"))) {
                // The selct all is made in the context
                init();
                return;
            }
        if (status.equals("Selection.FirstPoint")) {
            setType(s);
            return;
        } else
            if (status.equals("Selection.NextPointPolygon")) {
                if (s.equals(PluginServices.getText(this, "end_polygon"))
                    || s.equalsIgnoreCase(PluginServices.getText(this,
                        "ComplexSelectionCADTool.end"))) {
                    selectCurrentSurface();
                    return;
                }
            }
        init();
    }

    public long selectCurrentSurface() {
        Geometry surface =
            createOrientablePrimitive(null, Geometry.TYPES.SURFACE);
        GeneralPathX gpx = new GeneralPathX();
        gpx.append(surface.getPathIterator(null), true);
        if (gpx.isCCW()) {
            gpx.flip();
            surface = createSurface(gpx);
        }
        pointsPolygon.clear();
        return selectWithPolygon(surface);
    }

    public long selectAll() {
        VectorialLayerEdited vle = getVLE();
        PluginServices.getMDIManager().setWaitCursor();
        vle.selectAll();
        long countSelection = 0;
        try {
            countSelection =
                ((FeatureSelection) vle.getFeatureStore().getSelection())
                    .getSize();
        } catch (ReadException e) {
            LOG.error("Error reading the store", e);
        } catch (DataException e) {
            LOG.error("Error reading the store", e);
        }
        PluginServices.getMDIManager().restoreCursor();
        if (countSelection > 0) {
            nextState = "Selection.WithSelectedFeatures";
        } else {
            nextState = "Selection.FirstPoint";
        }
        end();
        return countSelection;
    }

    private OrientablePrimitive createOrientablePrimitive(Point2D p,
        int geometryType) {
        Point2D[] points = (Point2D[]) pointsPolygon.toArray(new Point2D[0]);
        OrientablePrimitive orientablePrimitive =
            createOrientablePrimitive(geometryType);

        for (int i = 0; i < points.length; i++) {
            if (i == 0) {
                orientablePrimitive.addMoveToVertex(createPoint(
                    points[i].getX(), points[i].getY()));
            } else {
                orientablePrimitive.addVertex(createPoint(points[i].getX(),
                    points[i].getY()));
            }
        }
        if (p != null) {
            orientablePrimitive.addVertex(createPoint(p.getX(), p.getY()));
        }
        orientablePrimitive.closePrimitive();
        return orientablePrimitive;
    }

    public void addValue(double d) {

    }

    public void end() {
        if (!getNextTool().equals("complex_selection")) {
            CADExtension.setCADTool(getNextTool(), false);
        }
    }

    public String getName() {
        return PluginServices.getText(this, "complex_selection_");
    }

    public boolean selectFeatures(double x, double y, InputEvent event) {
        ComplexSelectionCADToolState actualState = _fsm.getState();

        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();

        if ((status.equals("Selection.FirstPoint"))
            || (status.equals("Selection.WithSelectedFeatures"))) {
            PluginServices.getMDIManager().setWaitCursor();
            firstPoint = new Point2D.Double(x, y);
            vle.selectWithPoint(x, y, multipleSelection);
            PluginServices.getMDIManager().restoreCursor();
        }
        long countSelection = 0;
        try {
            countSelection =
                ((FeatureSelection) vle.getFeatureStore().getSelection())
                    .getSize();
        } catch (ReadException e) {
            LOG.error("Error reading the store", e);
        } catch (DataException e) {
            LOG.error("Error reading the store", e);
        }
        if (countSelection > 0) {
            nextState = "Selection.WithSelectedFeatures";
            return true;
        } else {
            {
                nextState = "Selection.SecondPoint";
                return true;
            }
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type.equalsIgnoreCase(PluginServices.getText(this,
            "ComplexSelectionCADTool.outrectangle"))) {
            this.type = PluginServices.getText(this, "out_rectangle");
        } else
            if (type.equalsIgnoreCase(PluginServices.getText(this,
                "ComplexSelectionCADTool.intropolygon"))) {
                this.type = PluginServices.getText(this, "inside_polygon");
            } else
                if (type.equalsIgnoreCase(PluginServices.getText(this,
                    "ComplexSelectionCADTool.crosspolygon"))) {
                    this.type = PluginServices.getText(this, "cross_polygon");
                } else
                    if (type.equalsIgnoreCase(PluginServices.getText(this,
                        "ComplexSelectionCADTool.outpolygon"))) {
                        this.type = PluginServices.getText(this, "out_polygon");
                    } else
                        if (type.equalsIgnoreCase(PluginServices.getText(this,
                            "ComplexSelectionCADTool.introcircle"))) {
                            this.type =
                                PluginServices.getText(this, "inside_circle");
                        } else
                            if (type.equalsIgnoreCase(PluginServices.getText(
                                this, "ComplexSelectionCADTool.crosscircle"))) {
                                this.type =
                                    PluginServices
                                        .getText(this, "cross_circle");
                            } else
                                if (type.equalsIgnoreCase(PluginServices
                                    .getText(this,
                                        "ComplexSelectionCADTool.outcircle"))) {
                                    this.type =
                                        PluginServices.getText(this,
                                            "out_circle");
                                } else
                                    if (type.equals(PluginServices.getText(
                                        this, "select_all"))) {
                                        selectAll();
                                        init();
                                    } else {
                                        this.type = type;
                                    }
        pointsPolygon.clear();
    }

    public void transition(double x, double y, InputEvent event) {
        LOG.info("TRANSITION FROM STATE " + _fsm.getState() + " x= " + x
            + " y=" + y);
        try {
            _fsm.addPoint(x, y, event);
        } catch (Exception e) {
            init();
        }
        LOG.info("CURRENT STATE: " + getStatus());
    }

    public String getStatus() {
        try {
            ComplexSelectionCADToolState actualState =
                (ComplexSelectionCADToolState) _fsm.getPreviousState();
            String status = actualState.getName();

            return status;
        } catch (NullPointerException e) {
            return "Selection.FirstPoint";
        }
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {

            _fsm.addOption(s);

        }
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public String toString() {
        return "_complex_selection";
    }

    public String getNextState() {
        return nextState;
    }
}
