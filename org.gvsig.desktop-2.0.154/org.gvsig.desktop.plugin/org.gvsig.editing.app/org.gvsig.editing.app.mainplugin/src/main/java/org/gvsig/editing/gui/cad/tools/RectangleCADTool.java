/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import org.gvsig.andami.PluginServices;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.RectangleCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.RectangleCADToolContext.RectangleCADToolState;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class RectangleCADTool extends AbstractCurveSurfaceCADTool {

    protected RectangleCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D lastPoint;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new RectangleCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        RectangleCADToolState actualState =
            (RectangleCADToolState) _fsm.getPreviousState();

        String status = actualState.getName();

        if (status.equals("Rectangle.FirstPoint")) {
            firstPoint = new Point2D.Double(x, y);
        } else
            if (status == "Rectangle.SecondPointOrSquare") {
                lastPoint = new Point2D.Double(x, y);

                OrientablePrimitive orientablePrimitive =
                    createOrientablePrimitive();

                orientablePrimitive.addMoveToVertex(createPoint(
                    firstPoint.getX(), firstPoint.getY()));
                orientablePrimitive.addVertex(createPoint(lastPoint.getX(),
                    firstPoint.getY()));
                orientablePrimitive.addVertex(createPoint(lastPoint.getX(),
                    lastPoint.getY()));
                orientablePrimitive.addVertex(createPoint(firstPoint.getX(),
                    lastPoint.getY()));
                orientablePrimitive.closePrimitive();

                insertAndSelectGeometry(orientablePrimitive);

                firstPoint = (Point2D) lastPoint.clone();
            } else
                if (status == "Rectangle.SecondPointSquare") {
                    lastPoint = new Point2D.Double(x, y);

                    OrientablePrimitive orientablePrimitive =
                        createOrientablePrimitive();

                    orientablePrimitive.addMoveToVertex(createPoint(
                        firstPoint.getX(), firstPoint.getY()));
                    orientablePrimitive.addVertex(createPoint(lastPoint.getX(),
                        firstPoint.getY()));

                    if (((lastPoint.getY() <= firstPoint.getY()) && (lastPoint
                        .getX() <= firstPoint.getX()))
                        || ((lastPoint.getY() > firstPoint.getY()) && (lastPoint
                            .getX() > firstPoint.getX()))) {
                        orientablePrimitive.addVertex(createPoint(
                            lastPoint.getX(),
                            firstPoint.getY()
                                + (lastPoint.getX() - firstPoint.getX())));
                        orientablePrimitive.addVertex(createPoint(
                            firstPoint.getX(),
                            firstPoint.getY()
                                + (lastPoint.getX() - firstPoint.getX())));
                    } else {
                        orientablePrimitive.addVertex(createPoint(
                            lastPoint.getX(),
                            firstPoint.getY()
                                - (lastPoint.getX() - firstPoint.getX())));
                        orientablePrimitive.addVertex(createPoint(
                            firstPoint.getX(),
                            firstPoint.getY()
                                - (lastPoint.getX() - firstPoint.getX())));
                    }
                    orientablePrimitive.closePrimitive();

                    insertAndSelectGeometry(orientablePrimitive);

                    firstPoint = (Point2D) lastPoint.clone();
                }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        RectangleCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status == "Rectangle.SecondPointOrSquare") {
            GeneralPathX elShape =
                new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 4);
            elShape.moveTo(firstPoint.getX(), firstPoint.getY());
            elShape.lineTo(x, firstPoint.getY());
            elShape.lineTo(x, y);
            elShape.lineTo(firstPoint.getX(), y);
            elShape.lineTo(firstPoint.getX(), firstPoint.getY());

            renderer.draw(createCurve(elShape),
                mapControlManager.getGeometrySelectionSymbol());
        } else
            if (status == "Rectangle.SecondPointSquare") {
                GeneralPathX elShape =
                    new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 4);
                elShape.moveTo(firstPoint.getX(), firstPoint.getY());
                elShape.lineTo(x, firstPoint.getY());

                if (((y <= firstPoint.getY()) && (x <= firstPoint.getX()))
                    || ((y > firstPoint.getY()) && (x > firstPoint.getX()))) {
                    elShape.lineTo(x,
                        firstPoint.getY() + (x - firstPoint.getX()));
                    elShape.lineTo(firstPoint.getX(), firstPoint.getY()
                        + (x - firstPoint.getX()));
                    elShape.lineTo(firstPoint.getX(), firstPoint.getY());
                } else {
                    elShape.lineTo(x,
                        firstPoint.getY() - (x - firstPoint.getX()));
                    elShape.lineTo(firstPoint.getX(), firstPoint.getY()
                        - (x - firstPoint.getX()));
                    elShape.lineTo(firstPoint.getX(), firstPoint.getY());
                }
                renderer.draw(createCurve(elShape),
                    mapControlManager.getGeometrySelectionSymbol());
            }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        RectangleCADToolState actualState =
            (RectangleCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status == "Rectangle.SecondPointOrSquare") {
            if (s.equalsIgnoreCase(PluginServices.getText(this,
                "RectangleCADTool.square"))) {
                // Opci�n correcta
            }
        }
    }

    public void addValue(double d) {
    }

    public String getName() {
        return PluginServices.getText(this, "rectangle_");
    }

    public String toString() {
        return "_rectangle";
    }

}
