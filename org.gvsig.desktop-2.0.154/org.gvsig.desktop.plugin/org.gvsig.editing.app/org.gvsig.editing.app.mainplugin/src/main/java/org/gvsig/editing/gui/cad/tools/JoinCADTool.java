/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.JoinCADToolContext;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class JoinCADTool extends DefaultCADTool {

    private static final Logger LOG = LoggerFactory
        .getLogger(JoinCADTool.class);

    private JoinCADToolContext _fsm;

    /**
     * M�todo de inicio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new JoinCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_join");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {

    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {

    }

    public void join() {
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        try {
            featureStore = vle.getFeatureStore();
        } catch (ReadException e1) {
            NotificationManager.addError(e1.getMessage(), e1);
        }
        DisposableIterator iterator = null;
        try {
            featureStore.beginEditingGroup(getName());

            FeatureSelection selection = featureStore.createFeatureSelection();
            selection.select((FeatureSet) featureStore.getSelection());

            Geometry geomTotal = null;
            iterator = selection.fastIterator();
            boolean first = true;
            Feature firstFeature = null;
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                if (first) {
                    firstFeature = feature;
                    first = false;
                }

                Geometry currentGeometry =
                    feature.getDefaultGeometry().cloneGeometry();
                featureStore.delete(feature);
                if (geomTotal == null) {
                    geomTotal = currentGeometry;
                } else {
                    geomTotal = geomTotal.union(currentGeometry);
                }
            }

            super.insertGeometry(geomTotal,firstFeature);
//            EditableFeature editableFeature =
//                featureStore.createNewFeature(firstFeature.getType(),
//                    firstFeature);
//            editableFeature.setGeometry(featureStore.getDefaultFeatureType()
//                .getDefaultGeometryAttributeName(), geomTotal);
//            featureStore.insert(editableFeature);
            refresh();
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (GeometryOperationNotSupportedException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (GeometryOperationException e) {
            NotificationManager.addError(e.getMessage(), e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
            try {
                featureStore.endEditingGroup();
            } catch (NeedEditingModeException e) {
                LOG.error("Exception endEditingGroup", e);
            }
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {

    }

    public String getName() {
        return PluginServices.getText(this, "join_");
    }

    public String toString() {
        return "_join";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] {
            CURVE,
            MULTICURVE,
            SURFACE,
            MULTISURFACE,
            };
    }
}
