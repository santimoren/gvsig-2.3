/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.awt.Component;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.Messages;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.IExtensionExecuteWithArgs;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.gui.cad.CADTool;
import org.gvsig.editing.gui.tokenmarker.ConsoleToken;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.utils.console.jedit.KeywordMap;
import org.gvsig.utils.console.jedit.Token;


public class StartEditing extends Extension implements IExtensionExecuteWithArgs {

    private static Logger logger = LoggerFactory.getLogger(StartEditing.class);

	public void initialize() {
	}

    public void execute(String actionCommand) {
		this.execute(actionCommand, null);
	}

	public void execute(String actionCommand, Object[] args) {

        if (actionCommand.compareTo("layer-start-editing") == 0) {

        	FLayer[] selectedItems = null;
    		// Desde el menu contextual del TOC se le pasan las capas seleccionadas como primer argumento.
        	if( args!=null && args.length>0 && args[0] instanceof FLayer[] ) {
        		selectedItems = (FLayer[]) args[0];
        	}

            org.gvsig.andami.ui.mdiManager.IWindow f =
                PluginServices.getMDIManager().getActiveWindow();

            if (f instanceof DefaultViewPanel) {
                DefaultViewPanel vista = (DefaultViewPanel) f;

                MapControl mapControl = vista.getMapControl();
                ViewDocument model = vista.getModel();
                FLayer[] actives =
                    model.getMapContext().getLayers().getActives();

                if (actives.length == 1 && actives[0] instanceof FLyrVect) {

                    FLyrVect lv = (FLyrVect) actives[0];

                    if (!lv.getFeatureStore().getTransforms().isEmpty()) {

                        // cannot edit transformed
                        JOptionPane
                            .showMessageDialog(
                                (Component) f,
                                Messages
                                    .get("_Cannot_start_edition_in_transformed_layer")
                                    + ": '" + lv.getName() + "'",
                                PluginServices.getText(this, "warning_title"),
                                JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }

                    if (!lv.isWritable()) {
                        JOptionPane.showMessageDialog((Component) f,
                            PluginServices.getText(this,
                                "this_layer_is_not_self_editable"),
                            PluginServices.getText(this, "warning_title"),
                            JOptionPane.WARNING_MESSAGE);
                    }

                    IEditionManager editionManager =
                        EditionLocator.getEditionManager();

                    // ==============================================
                    // Set visibility and snapping of grid according to
                    // grid preferences.
                    //FIXME: Coger los valores de las preferencias del plugin de snapping y pasárselos al mapControl
//                    Preferences prefs = GridPage.prefs;
//                    boolean b = prefs.getBoolean("grid.showgrid", false);
//                    mapControl.setGridVisibility(b);
//                    b = prefs.getBoolean("grid.adjustgrid", false);
//                    mapControl.setAdjustGrid(b);
                    // Enable snapping to geometries
                    // (this enables snappers which are active
                    mapControl.setRefentEnabled(true);
                    /*
                     * Add if not present in list of layers to snap to:
                     */
                    List snaplist = mapControl.getMapContext().getLayersToSnap();
                    if (!snaplist.contains(lv)) {
                    	snaplist.add(lv);
                    }
                    // ==============================================

                    try {
                        editionManager.editLayer(lv, vista);
                    } catch ( CancelException e) {
                    	// Do nothing
                    } catch (DataException e) {

                        logger.info(
                            "Error while starting edition: " + e.getMessage(), e);

                        ApplicationLocator.getManager().message(
                            Messages.get("_Unable_to_start_edition_in_layer")
                                + ": " + lv.getName(),
                            JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
    }



	public static void startCommandsApplicable(DefaultViewPanel vista,FLyrVect lv) {
	    if (vista==null)
	        vista=(DefaultViewPanel)PluginServices.getMDIManager().getActiveWindow();

	    CADTool[] cadtools = CADExtension.getCADTools();
		KeywordMap keywordMap = new KeywordMap(true);
		for (int i = 0; i < cadtools.length; i++) {
			try {
				if (cadtools[i].isApplicable(lv.getShapeType())){
					keywordMap.add(cadtools[i].getName(), Token.KEYWORD2);
					keywordMap.add(cadtools[i].toString(), Token.KEYWORD3);
				}
			} catch (ReadException e) {
				NotificationManager.addError(e.getMessage(),e);
			}

		}
		ConsoleToken consoletoken = new ConsoleToken(keywordMap);
		vista.getConsolePanel().setTokenMarker(consoletoken);

	}


	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
				.getActiveWindow();

		if (f == null) {
			return false;
		}
		if (f instanceof DefaultViewPanel){
			DefaultViewPanel view=(DefaultViewPanel)f;
			FLayer[] selected = view.getModel().getMapContext().getLayers()
			.getActives();
			if (selected.length == 1 && selected[0].isAvailable() && selected[0] instanceof FLyrVect) {
				if (selected[0].isEditing())
					return false;
				return true;
			}
		}
		return false;
	}
}
