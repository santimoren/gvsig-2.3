/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.gui.cad.tools.CircleCADTool;
import org.gvsig.editing.gui.cad.tools.EditVertexCADTool;
import org.gvsig.editing.gui.cad.tools.EllipseCADTool;
import org.gvsig.editing.gui.cad.tools.PolygonCADTool;
import org.gvsig.editing.gui.cad.tools.RectangleCADTool;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;


/**
 * Extensi�n que gestiona la inserci�n de poligonos en edici�n.
 *
 * @author Vicente Caballero Navarro
 */
public class InsertPolygonExtension extends Extension {
	private DefaultViewPanel view;

	private MapControl mapControl;
	private PolygonCADTool polygon;

	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
		polygon = new PolygonCADTool();
		CircleCADTool circle=new CircleCADTool();
        RectangleCADTool rectangle=new RectangleCADTool();
        EllipseCADTool ellipse=new EllipseCADTool();
        EditVertexCADTool editvertex=new EditVertexCADTool();
		CADExtension.addCADTool("_polygon", polygon);
		CADExtension.addCADTool("_circle",circle);
	    CADExtension.addCADTool("_rectangle", rectangle);
	    CADExtension.addCADTool("_ellipse", ellipse);
	    CADExtension.addCADTool("_editvertex",editvertex);

	    registerIcons();
	}

	private void registerIcons(){
		IconThemeHelper.registerIcon("action", "layer-modify-edit-vertex", this);
		IconThemeHelper.registerIcon("action", "layer-insert-ellipse", this);
		IconThemeHelper.registerIcon("action", "layer-insert-circle", this);
		IconThemeHelper.registerIcon("action", "layer-insert-rectangle", this);
		IconThemeHelper.registerIcon("action", "layer-insert-polygon", this);
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String s) {
		CADExtension.initFocus();
		if ("layer-modify-edit-vertex".equalsIgnoreCase(s)) {
			CADExtension.setCADTool("_editvertex",true);
			
		} else if ("layer-insert-ellipse".equalsIgnoreCase(s)) {
			CADExtension.setCADTool("_ellipse",true);
		
		} else if ("layer-insert-circle".equalsIgnoreCase(s)) {
			CADExtension.setCADTool("_circle",true);
		
		} else if ("layer-insert-rectangle".equalsIgnoreCase(s)) {
			CADExtension.setCADTool("_rectangle",true);
		
		} else if ("layer-insert-polygon".equalsIgnoreCase(s)) {
			CADExtension.setCADTool("_polygon",true);
	    
		}
		CADExtension.getEditionManager().setMapControl(mapControl);
		CADExtension.getCADToolAdapter().configureMenu();
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {

		try {
			if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE) {
				view = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
				mapControl = view.getMapControl();
				if (CADExtension.getEditionManager().getActiveLayerEdited()==null)
					return false;
				FLyrVect lv=(FLyrVect)CADExtension.getEditionManager().getActiveLayerEdited().getLayer();
				if (polygon.isApplicable(lv.getShapeType())){
					return true;
				}
			}
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(),e);
		}
		return false;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE)
			return true;
		return false;
	}
}
