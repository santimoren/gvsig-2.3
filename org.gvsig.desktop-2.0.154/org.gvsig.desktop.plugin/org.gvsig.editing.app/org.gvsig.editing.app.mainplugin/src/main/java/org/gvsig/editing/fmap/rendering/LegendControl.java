/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.fmap.rendering;

import java.util.BitSet;

import org.gvsig.symbology.fmap.mapcontext.rendering.legend.impl.VectorialUniqueValueLegend;

public class LegendControl extends VectorialUniqueValueLegend{

	private BitSet isBlocked=new BitSet();
	private BitSet isActivated=new BitSet();
	private BitSet isDisabled=new BitSet();
	private BitSet isFilled=new BitSet();
	private int present=0;

	public LegendControl(int shapeType) {
		super(shapeType);
	}
	public boolean isBlocked(int i) {
		return isBlocked.get(i);
	}
	public boolean isActivated(int i) {
		return isActivated.get(i);
	}
	public boolean isDisabled(int i) {
		return isDisabled.get(i);
	}
	public boolean isFilled(int i) {
		return isFilled.get(i);
	}
	public void setBlocked(int i,boolean b) {
		isBlocked.set(i,b);
	}
	public void setActivated(int i,boolean b) {
		isActivated.set(i,b);
	}
	public void setDisabled(int i,boolean b) {
		isDisabled.set(i,b);
	}
	public void setFilled(int i,boolean b) {
		isFilled.set(i,b);
	}
	public void setPresent(int i) {
		present=i;
	}
	public int getPresent() {
		return present;
	}
}
