/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoCompletePolygon extends PolylineCADTool {

	private static Logger logger =
			LoggerFactory.getLogger(AutoCompletePolygon.class);
    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     *
     * @param g Graphics sobre el que dibujar.
     * @param selectedGeometries BitSet con las geometr�as seleccionadas.
     * @param x par�metro x del punto que se pase para dibujar.
     * @param y par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        Geometry geom = getGeometry();
        if (geom.getHandlers(Geometry.SELECTHANDLER).length == 0
            && firstPoint != null) {
            GeneralPathX gpx = new GeneralPathX();
            gpx.moveTo(firstPoint.getX(), firstPoint.getY());
            gpx.lineTo(x, y);
            renderer.draw(createCurve(gpx),
                mapControlManager.getGeometrySelectionSymbol());
        } else
            if (geom.getHandlers(Geometry.SELECTHANDLER).length > 1) {
                GeneralPathX gpxGeom = new GeneralPathX();
                gpxGeom
                    .append(
                        geom.getPathIterator(null, geomManager.getFlatness()),
                        true);
                gpxGeom.lineTo(x, y);
                gpxGeom.closePath();
                renderer.draw(createCurve(gpxGeom),
                    mapControlManager.getGeometrySelectionSymbol());
            }
    }

    private Geometry autoComplete(Geometry digitizedGeom)
        throws CreateGeometryException {
        FeatureSet selected = null;
        DisposableIterator iterator = null;
        try {
            FLyrVect lyrVect = (FLyrVect) getVLE().getLayer();
            // Se supone que debe ser r�pido, ya que est� indexado

            selected =
                lyrVect.queryByGeometry(digitizedGeom, lyrVect
                    .getFeatureStore().getDefaultFeatureType());
            iterator = selected.fastIterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Geometry aux = feature.getDefaultGeometry();
                digitizedGeom = digitizedGeom.difference(aux);
            }
        } catch (Exception e) {
        	
        	throw new CreateGeometryException(
        			digitizedGeom.getGeometryType().getType(),
        			digitizedGeom.getGeometryType().getSubType(), e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
            if (selected != null) {
                selected.dispose();
            }
        }

        return digitizedGeom;
    }
    
    public Feature insertGeometry(Geometry geometry) {
        Geometry newGeom;
        try {
            newGeom = autoComplete(geometry);
            return super.insertGeometry(newGeom);
        } catch (CreateGeometryException e) {
            logger.info("Error in autocomplete polygon.", e);
        	JOptionPane.showMessageDialog(
        			ApplicationLocator.getManager().getRootComponent(),
        			Messages.getText(
        					"_Unable_to_complete_operation_Probably_invalid_polygon"),
        			Messages.getText("autocomplete-polygon"),
        			JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public Feature insertAndSelectGeometry(Geometry geometry) {
        Geometry newGeom;
        try {
            newGeom = autoComplete(geometry);
            return super.insertAndSelectGeometry(newGeom);
        } catch (CreateGeometryException e) {
        	
            logger.info("Error in autocomplete polygon.", e);
        	JOptionPane.showMessageDialog(
        			ApplicationLocator.getManager().getRootComponent(),
        			Messages.getText(
        					"_Unable_to_complete_operation_Probably_invalid_polygon"),
        			Messages.getText("autocomplete-polygon"),
        			JOptionPane.ERROR_MESSAGE);
        	
        }
        return null;
    }
    
}
