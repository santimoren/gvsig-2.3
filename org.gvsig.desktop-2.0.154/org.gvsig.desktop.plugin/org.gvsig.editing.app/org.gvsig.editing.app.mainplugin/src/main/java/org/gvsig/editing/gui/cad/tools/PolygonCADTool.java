/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.exception.ValueException;
import org.gvsig.editing.gui.cad.tools.smc.PolygonCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.PolygonCADToolContext.PolygonCADToolState;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.locator.LocatorException;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class PolygonCADTool extends AbstractCurveSurfaceCADTool {

    protected PolygonCADToolContext _fsm;
    protected Point2D center;
    protected int numLines = 5;
    protected boolean isI = true;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new PolygonCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        PolygonCADToolState actualState =
            (PolygonCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Polygon.NumberOrCenterPoint")) {
            center = new Point2D.Double(x, y);
        } else
            if (status.equals("Polygon.CenterPoint")) {
                center = new Point2D.Double(x, y);
            } else
                if (status.equals("Polygon.OptionOrRadiusOrPoint")
                    || status.equals("Polygon.RadiusOrPoint")) {
                    Point2D point = new Point2D.Double(x, y);
                    // Pol�gono a partir de la circunferencia.
                    if (isI) {
                        insertAndSelectGeometry(getIPolygon(point,
                            point.distance(center)));
                    } else {
                        insertAndSelectGeometry(getCPolygon(point,
                            point.distance(center)));
                    }
                }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        PolygonCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Polygon.OptionOrRadiusOrPoint")
            || status.equals("Polygon.RadiusOrPoint")) {
            Point2D point = new Point2D.Double(x, y);
            renderer.drawLine(center, point,
                mapControlManager.getGeometrySelectionSymbol());

            Geometry help_geom = null;
            Geometry help_geom_perim = null;
            
            /*
             * Get helping polygon
             */
            if (isI) {
                help_geom = getIPolygon(point, point.distance(center));
            } else {
                help_geom = getCPolygon(point, point.distance(center));
            }

            /*
             * Get and draw perimeter of polygon
             */
            try {
                help_geom_perim = GeometryLocator.
                    getGeometryManager().createCurve(
                        help_geom.getGeneralPath(),
                        Geometry.SUBTYPES.GEOM2D);
                renderer.draw(help_geom_perim,
                    mapControlManager.getGeometrySelectionSymbol());
            } catch (Exception e) {
                ApplicationLocator.getManager().message(
                    Messages.getText("_Unable_to_draw_help_geometry"),
                    JOptionPane.ERROR_MESSAGE);
            }


            /*
             * Create circle
             */
            help_geom = createCircle(center, point.distance(center));
            
            /*
             * Get and draw perimeter of circle
             */
            try {
                help_geom_perim = GeometryLocator.
                    getGeometryManager().createCurve(
                        help_geom.getGeneralPath(),
                        Geometry.SUBTYPES.GEOM2D);
                renderer.draw(help_geom_perim,
                    mapControlManager.getAxisReferenceSymbol());
            } catch (Exception e) {
                ApplicationLocator.getManager().message(
                    Messages.getText("_Unable_to_draw_help_geometry"),
                    JOptionPane.ERROR_MESSAGE);
            }

        }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        PolygonCADToolState actualState =
            (PolygonCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Polygon.OptionOrRadiusOrPoint")) {
            if (s.equalsIgnoreCase(PluginServices.getText(this,
                "PolygonCADTool.circumscribed"))
                || s.equals(PluginServices.getText(this, "circumscribed"))) {
                isI = false;
            } else
                if (s.equalsIgnoreCase(PluginServices.getText(this,
                    "PolygonCADTool.into_circle"))
                    || s.equals(PluginServices.getText(this, "into_circle"))) {
                    isI = true;
                }
        }
    }

    public void addValue(double d) {
        PolygonCADToolState actualState =
            (PolygonCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Polygon.NumberOrCenterPoint")) {
            numLines = (int) d;
        } else
            if (status.equals("Polygon.OptionOrRadiusOrPoint")
                || status.equals("Polygon.RadiusOrPoint")) {
                double radio = d;

                // Pol�gono a partir de radio.
                Point2D point =
                    UtilFunctions.getPoint(center,
                        new Point2D.Double(center.getX(), center.getY() + 10),
                        radio);

                if (isI) {
                    insertAndSelectGeometry(getIPolygon(point, radio));
                } else {
                    insertAndSelectGeometry(getCPolygon(point, radio));
                }
            }
    }

    /**
     * Devuelve la geometr�a con el poligono regular circunscrito a la
     * circunferencia formada por el punto central y el radio que se froma
     * entre este y el puntero del rat�n..
     * 
     * @param point
     *            Puntero del rat�n.
     * @param radio
     *            Radio
     * 
     * @return GeometryCollection con las geometr�as del pol�gono.
     */
    private Geometry getCPolygon(Point2D point, double radio) {
        Point2D p1 = UtilFunctions.getPoint(center, point, radio);

        double initangle = UtilFunctions.getAngle(center, point);
        Point2D antPoint = p1;
        Point2D antInter = null;
        double an = (Math.PI * 2) / numLines;
        boolean firstTime = true;

        OrientablePrimitive orientablePrimitive = createOrientablePrimitive();

        for (int i = numLines - 1; i >= 0; i--) {
            Point2D p2 =
                UtilFunctions.getPoint(center, (an * i) + initangle, radio);
            Point2D[] ps1 =
                UtilFunctions.getPerpendicular(antPoint, center, antPoint);
            Point2D[] ps2 = UtilFunctions.getPerpendicular(p2, center, p2);
            Point2D inter =
                UtilFunctions.getIntersection(ps1[0], ps1[1], ps2[0], ps2[1]);

            if (antInter != null) {

                if (firstTime) {
                    orientablePrimitive.addMoveToVertex(createPoint(
                        antInter.getX(), antInter.getY()));
                    firstTime = false;
                }
                orientablePrimitive.addVertex(createPoint(inter.getX(),
                    inter.getY()));
            }

            antInter = inter;
            antPoint = p2;
        }
        orientablePrimitive.closePrimitive();

        return orientablePrimitive;
    }

    /**
     * Devuelve la geometr�a con el poligono regular inscrito en la
     * circunferencia.
     * 
     * @param point
     *            Puntero del rat�n.
     * @param radio
     *            Radio
     * 
     * @return GeometryCollection con las geometr�as del pol�gono.
     * @throws ValueException
     */
    private Geometry getIPolygon(Point2D point, double radio) {
        Point2D p1 = UtilFunctions.getPoint(center, point, radio);
        double initangle = UtilFunctions.getAngle(center, point);
        Point2D antPoint = p1;
        double an = (Math.PI * 2) / numLines;
        boolean firstTime = true;

        OrientablePrimitive orientablePrimitive = createOrientablePrimitive();

        for (int i = numLines - 1; i > 0; i--) {
            Point2D p2 =
                UtilFunctions.getPoint(center, (an * i) + initangle, radio);

            if (firstTime) {
                orientablePrimitive.addMoveToVertex(createPoint(
                    antPoint.getX(), antPoint.getY()));
                firstTime = false;
            }

            orientablePrimitive.addVertex(createPoint(p2.getX(), p2.getY()));

            antPoint = p2;
        }
        orientablePrimitive.closePrimitive();

        return orientablePrimitive;
    }

    public String getName() {
        return PluginServices.getText(this, "polygon_");
    }

    /**
     * Devuelve la cadena que corresponde al estado en el que nos encontramos.
     * 
     * @return Cadena para mostrar por consola.
     */
    public String getQuestion() {
        PolygonCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Polygon.NumberOrCenterPoint")) {
            return super.getQuestion() + "<" + numLines + ">";
        }
        return super.getQuestion();

    }

    public String toString() {
        return "_polygon";
    }

}
