/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * Parent class for tools which create Curve or Surface geometries, depending
 * on the layer geometry type.
 * 
 * @author gvSIG team
 */
public abstract class AbstractCurveSurfaceCADTool extends DefaultCADTool {

    private static final int TYPE_CURVE = 0;
    private static final int TYPE_MULTICURVE = 1;
    private static final int TYPE_MULTISURFACE = 3;

    @Override
    public Feature insertAndSelectGeometry(Geometry geometry) {
        Geometry finalGeometry = geometry;
        GeometryType type = getGeometryType();

        GeometryType[] types = getSupportedTypes();

        if (types[TYPE_MULTICURVE].isTypeOf(type)) {
            MultiCurve mcurve = createMultiCurve();
            mcurve.addCurve((Curve) geometry);
            finalGeometry = mcurve;
        } else
            if (types[TYPE_MULTISURFACE].isTypeOf(type)) {
                MultiSurface msurface = createMultiSurface();
                msurface.addSurface((Surface) geometry);
                finalGeometry = msurface;
            }

        return super.insertAndSelectGeometry(finalGeometry);
    }

    protected OrientablePrimitive createOrientablePrimitive() {
        GeometryType type = getGeometryType();

        GeometryType[] types = getSupportedTypes();

        if (types[TYPE_CURVE].isTypeOf(type)
            || types[TYPE_MULTICURVE].isTypeOf(type)) {
            return createCurve();
        } else { // SURFACE or MULTISURFACE
            return createSurface();
        }
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { CURVE, MULTICURVE, SURFACE, MULTISURFACE };
    }
}
