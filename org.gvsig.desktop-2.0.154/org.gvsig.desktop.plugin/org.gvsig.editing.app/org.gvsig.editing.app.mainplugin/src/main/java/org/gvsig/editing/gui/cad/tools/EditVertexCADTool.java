/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.IEditionManager;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.EditVertexCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.EditVertexCADToolContext.EditVertexCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class EditVertexCADTool extends DefaultCADTool {

    protected EditVertexCADToolContext _fsm;
    protected int numSelect = 0;
    protected int numHandlers;
    protected boolean addVertex = false;
    
    private static Logger logger =
        LoggerFactory.getLogger(EditVertexCADTool.class);

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new EditVertexCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_editvertex");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        selectHandler(x, y);
        addVertex = false;
    }

    private Geometry getSelectedGeometry() {
        FeatureSet selection = null;
        DisposableIterator iterator = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            Feature feature = null;
            Geometry ig = null;
            if (selection.getSize() == 1) {
                iterator = selection.iterator();
                feature = (Feature) iterator.next();
                ig = (feature.getDefaultGeometry()).cloneGeometry();
                return ig;
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            DisposeUtils.dispose(iterator);
        }

        return null;
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        drawVertex(renderer, getCadToolAdapter().getMapControl().getViewPort());
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        EditVertexCADToolState actualState =
            (EditVertexCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        DisposableIterator iterator = null;
        try {
            featureStore = vle.getFeatureStore();

            FeatureSet selection = (FeatureSet) featureStore.getSelection();
            Feature feature = null;
            Geometry ig = null;
            Handler[] handlers = null;
            if (selection.getSize() == 1) {
                iterator = selection.iterator();
                feature = (Feature) iterator.next();
                ig = (feature.getDefaultGeometry()).cloneGeometry();
                handlers = ig.getHandlers(Geometry.SELECTHANDLER);
                numHandlers = handlers.length;
                if (numHandlers == 0) {
                    try {
                        featureStore.delete(feature);
                    } catch (ReadException e) {
                        NotificationManager.addError(e.getMessage(), e);
                    } catch (DataException e) {
                        NotificationManager.addError(e.getMessage(), e);
                    }
                }
            } else {
                JOptionPane.showMessageDialog((Component) PluginServices
                    .getMainFrame(), PluginServices.getText(this,
                    "hay_mas_de_una_geometria_seleccionada"));
            }

            int diff = 1;// En el caso de ser pol�gono.
            /*
            if (ig instanceof MultiPrimitive) {
                diff = 2;
            }
            */

            if (status.equals("EditVertex.SelectVertexOrDelete")) {
                if (s.equalsIgnoreCase(PluginServices.getText(this,
                    "EditVertexCADTool.nextvertex"))
                    || s.equals(PluginServices.getText(this, "next"))) {
                    
                    numSelect = numSelect + diff;
                    if (numSelect > (numHandlers - 1)) {
                        numSelect = numSelect - (numHandlers);
                    }
                    
                } else
                    if (s.equalsIgnoreCase(PluginServices.getText(this,
                        "EditVertexCADTool.previousvertex"))
                        || s.equals(PluginServices.getText(this, "previous"))) {

                        numSelect = numSelect - diff;
                        if (numSelect < 0) {
                            numSelect = numHandlers - 1 + (numSelect + 1);
                        }

                    } else
                        if (s.equalsIgnoreCase(PluginServices.getText(this,
                            "EditVertexCADTool.delvertex"))
                            || s.equals(PluginServices.getText(this, "del"))) {
                            if (handlers != null) {
                                Geometry newGeometry = null;
                                if (ig instanceof MultiPrimitive) {
                                    newGeometry =
                                        removeVertexGC((MultiPrimitive) ig,
                                            handlers[numSelect]);
                                } else {
                                    newGeometry =
                                        removeVertex(ig, handlers[numSelect]);
                                }
                                super.updateGeometry(featureStore, feature, newGeometry);
//                                try {
//                                    EditableFeature eFeature =
//                                        feature.getEditable();
//                                    eFeature.setGeometry(featureStore
//                                        .getDefaultFeatureType()
//                                        .getDefaultGeometryAttributeName(),
//                                        newGeometry);
//                                    featureStore.update(eFeature);
//
//                                } catch (ReadException e) {
//                                    NotificationManager.addError(
//                                        e.getMessage(), e);
//                                } catch (DataException e) {
//                                    // TODO Auto-generated catch block
//                                    e.printStackTrace();
//                                }
                            }
                        } else
                            if (s.equalsIgnoreCase(PluginServices.getText(this,
                                "EditVertexCADTool.addvertex"))
                                || s.equals(PluginServices.getText(this, "add"))) {
                                addVertex = true;
                            }
            }
        } catch (Exception e1) {
            
            ApplicationLocator.getManager().message(
                e1.getMessage(), JOptionPane.ERROR_MESSAGE);
            logger.info("Error in add option.", e1);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    private void drawVertex(MapControlDrawer renderer, ViewPort vp) {
        VectorialLayerEdited vle = getVLE();
        DisposableIterator iterator = null;
        try {
            iterator =
                ((FeatureSelection) vle.getFeatureStore().getSelection())
                    .iterator();
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();

                Geometry ig = (feature.getDefaultGeometry()).cloneGeometry();
                // renderer.draw(ig,
                // mapControlManager.getGeometrySelectionSymbol());

                Handler[] handlers = ig.getHandlers(Geometry.SELECTHANDLER);
                if (numSelect >= handlers.length) {
                    numSelect = 0;
                }
                if (handlers.length == 0)
                    continue;
                renderer.drawHandler(handlers[numSelect],
                    vp.getAffineTransform());
            }
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    private Geometry removeVertex(Geometry gp, Handler hand) {
        GeneralPathX newGp = new GeneralPathX();
        double[] theData = new double[6];

        PathIterator theIterator;
        int theType;
        int numParts = 0;

        Point2D ptSrc = new Point2D.Double();
        boolean bFirst = false;

        theIterator = gp.getPathIterator(null, geomManager.getFlatness());
        int numSegmentsAdded = 0;
        while (!theIterator.isDone()) {
            theType = theIterator.currentSegment(theData);
            if (bFirst) {
                newGp.moveTo(theData[0], theData[1]);
                numSegmentsAdded++;
                bFirst = false;
                theIterator.next();
                continue;
            }
            switch (theType) {

            case PathIterator.SEG_MOVETO:
                numParts++;
                ptSrc.setLocation(theData[0], theData[1]);
                if (ptSrc.equals(hand.getPoint())) {
                    numParts--;
                    bFirst = true;
                    break;
                }
                newGp.moveTo(ptSrc.getX(), ptSrc.getY());
                numSegmentsAdded++;
                bFirst = false;
                break;

            case PathIterator.SEG_LINETO:
                ptSrc.setLocation(theData[0], theData[1]);
                if (ptSrc.equals(hand.getPoint())) {
                    break;
                }
                newGp.lineTo(ptSrc.getX(), ptSrc.getY());
                bFirst = false;
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_QUADTO:
                newGp.quadTo(theData[0], theData[1], theData[2], theData[3]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CUBICTO:
                newGp.curveTo(theData[0], theData[1], theData[2], theData[3],
                    theData[4], theData[5]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CLOSE:
                if (numSegmentsAdded < 3) {
                    newGp.lineTo(theData[0], theData[1]);
                }
                newGp.closePath();

                break;
            } // end switch

            theIterator.next();
        } // end while loop
        Geometry shp = null;
        switch (gp.getType()) {
        case Geometry.TYPES.POINT: // Tipo punto
            shp = createPoint(ptSrc.getX(), ptSrc.getY());
            break;

        case Geometry.TYPES.CURVE:
            shp = createCurve(newGp);
            break;
        case Geometry.TYPES.SURFACE:
            shp = createSurface(newGp);
            break;
        }
        Geometry ig = shp;
        int dif = 1;// En el caso de ser pol�gono.
        numSelect = numSelect - dif;
        if (numSelect < 0) {
            numSelect = numHandlers - 1 + (numSelect + 1);
        }
        return ig;
    }
    
    private Geometry removeVertexGC(MultiPrimitive gc, Handler handler)
            throws GeometryOperationNotSupportedException,
            GeometryOperationException, CreateGeometryException {

            Primitive prim = null;
            int np = gc.getPrimitivesNumber();
            
            for (int i = 0; i < np; i++) {
                prim = gc.getPrimitiveAt(i);
                Handler[] hh = prim.getHandlers(Geometry.SELECTHANDLER);
                int nh = hh.length;
                for (int j=0; j<nh; j++) {
                    if (hh[j].equalsPoint(handler)) {
                        /*
                         * Find the first primitive which has a handler
                         * matching received handler
                         */
                        Geometry resp = this.removeVertex(prim, handler);
                        for (int k=0; k<np; k++) {
                            if (k != i) {
                                resp = resp.union(gc.getPrimitiveAt(k));
                            }
                        }
                        if (resp.getType() == Geometry.TYPES.SURFACE){
                        	MultiSurface geom = createMultiSurface();
                        	geom.addSurface((Surface)resp);
                        	return geom;
                        }else if (resp.getType() == Geometry.TYPES.POINT){
                        	MultiPoint geom = createMultiPoint();
                        	geom.addPoint((Point)resp);
                        	return geom;
                        }else if (resp.getType() == Geometry.TYPES.CURVE){
                        	MultiCurve geom = createMultiCurve();
                        	geom.addCurve((Curve)resp);
                        	return geom;
                    	}
                        return resp;
                    }
                }
            }
            throw new CreateGeometryException(
                new Exception("Unable to remove vertex"));
        
    }
    


    private Geometry addVertex(Geometry geome, Point2D p, Rectangle2D rect) {
        Geometry geometryCloned = geome.cloneGeometry();
        Geometry geom1 = null;
        GeneralPathX gpxAux;
        boolean finish = false;
        // FGeometry geom2=null;

        // if (geometryCloned.getGeometryType() == FShape.POLYGON){
        // ///////////////

        GeneralPathX newGp = new GeneralPathX();
        double[] theData = new double[6];

        PathIterator theIterator;
        int theType;
        int numParts = 0;
        Point2D pLast = new Point2D.Double();
        Point2D pAnt = new Point2D.Double();
        Point2D firstPoint = null;
        theIterator = geome.getPathIterator(null, geomManager.getFlatness()); // ,
                                                                              // flatness);
        int numSegmentsAdded = 0;
        while (!theIterator.isDone()) {
            theType = theIterator.currentSegment(theData);
            switch (theType) {
            case PathIterator.SEG_MOVETO:
                pLast.setLocation(theData[0], theData[1]);
                if (numParts == 0) {
                    firstPoint = (Point2D) pLast.clone();
                }
                numParts++;

                gpxAux = new GeneralPathX();
                gpxAux.moveTo(pAnt.getX(), pAnt.getY());
                gpxAux.lineTo(pLast.getX(), pLast.getY());
                geom1 = createCurve(gpxAux);
                if (geom1.intersects(rect)) {
                    finish = true;
                    newGp.moveTo(pLast.getX(), pLast.getY());
                    // newGp.lineTo(pLast.getX(),pLast.getY());
                } else {
                    newGp.moveTo(pLast.getX(), pLast.getY());
                }
                pAnt.setLocation(pLast.getX(), pLast.getY());
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_LINETO:
                pLast.setLocation(theData[0], theData[1]);
                gpxAux = new GeneralPathX();
                gpxAux.moveTo(pAnt.getX(), pAnt.getY());
                gpxAux.lineTo(pLast.getX(), pLast.getY());
                geom1 = createCurve(gpxAux);
                if (geom1.intersects(rect)) {
                    newGp.lineTo(p.getX(), p.getY());
                    newGp.lineTo(pLast.getX(), pLast.getY());
                } else {
                    newGp.lineTo(pLast.getX(), pLast.getY());
                }
                pAnt.setLocation(pLast.getX(), pLast.getY());
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_QUADTO:
                newGp.quadTo(theData[0], theData[1], theData[2], theData[3]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CUBICTO:
                newGp.curveTo(theData[0], theData[1], theData[2], theData[3],
                    theData[4], theData[5]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CLOSE:
                // if (numSegmentsAdded < 3){
                gpxAux = new GeneralPathX();
                gpxAux.moveTo(pAnt.getX(), pAnt.getY());
                gpxAux.lineTo(firstPoint.getX(), firstPoint.getY());
                geom1 = createCurve(gpxAux);
                if (geom1.intersects(rect) || finish) {
                    newGp.lineTo(p.getX(), p.getY());
                    // newGp.lineTo(pLast.getX(), pLast.getY());
                } else {
                    newGp.lineTo(pLast.getX(), pLast.getY());
                }
                // }
                newGp.closePath();
                break;
            } // end switch

            theIterator.next();
        } // end while loop
        Geometry shp = null;
        switch (geometryCloned.getType()) {
        case POINT: // Tipo punto
            shp = createPoint(pLast.getX(), pLast.getY());
            break;

        case CURVE:
            shp = createCurve(newGp);
            break;
        case SURFACE:
        case CIRCLE:
        case ELLIPSE:
            shp = createSurface(newGp);
            break;
        }
        return shp;
    }

    private Geometry addVertexGC(
        MultiPrimitive mpri,
        Point2D p,
        Rectangle2D rect)
            throws GeometryOperationNotSupportedException,
            GeometryOperationException, CreateGeometryException
            {
        
        int n = mpri.getPrimitivesNumber();
        Primitive item = null;
        for (int i=0; i<n; i++) {
            item = mpri.getPrimitiveAt(i);
            if (item.intersects(rect)) {
                /*
                 * Only the first primitive that intersects
                 * with rect will be treated
                 */
                Geometry newg = this.addVertex(item, p, rect);
                for (int k=0; k<n; k++) {
                    if (k != i) {
                        newg = newg.union(mpri.getPrimitiveAt(k));
                    }
                    return newg;
                }
            }
        }
        throw new CreateGeometryException(
            new Exception("Unable to add vertex"));

        /*
    	Geometry[] geoms = new Geometry[gc.getPrimitivesNumber()];
    	for( int i=0; i<gc.getPrimitivesNumber(); i++) {
    		geoms[i] = gc.getPrimitiveAt(i);
    	}
  	
        int pos = -1;
        for (int i = 0; i < geoms.length; i++) {
            if (geoms[i].intersects(rect)) {
                pos = i;
            }
        }
        ArrayList newGeoms = new ArrayList();
        for (int i = 0; i < pos; i++) {
            newGeoms.add(geoms[i]);
        }
        if (pos != -1) {
            GeneralPathX gpx1 = new GeneralPathX();
            GeneralPathX gpx2 = new GeneralPathX();
            Handler[] handlers = geoms[pos].getHandlers(Geometry.SELECTHANDLER);
            Point2D p1 = handlers[0].getPoint();
            Point2D p2 = p;
            Point2D p3 = handlers[handlers.length - 1].getPoint();
            gpx1.moveTo(p1.getX(), p1.getY());
            gpx1.lineTo(p2.getX(), p2.getY());
            gpx2.moveTo(p2.getX(), p2.getY());
            gpx2.lineTo(p3.getX(), p3.getY());
            newGeoms.add(createCurve(gpx1));
            newGeoms.add(createCurve(gpx2));
            for (int i = pos + 1; i < geoms.length; i++) {
                newGeoms.add(geoms[i]);
            }
            return createMultiPrimitive((Geometry[]) newGeoms
                .toArray(new Geometry[0]));
        } else {
            return null;
        }
        */
    }

    public String getName() {
        return PluginServices.getText(this, "edit_vertex_");
    }

    private void selectHandler(double x, double y) {
        Point2D firstPoint = new Point2D.Double(x, y);
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        DisposableIterator iterator = null;
        DisposableIterator selectedIterator = null;
        
        IEditionManager ed_man = this.getEditionManager();

        try {
            featureStore = vle.getFeatureStore();

            iterator =
                ((FeatureSelection) featureStore.getSelection()).iterator();
            double tam =
                getCadToolAdapter().getMapControl().getViewPort()
                    .toMapDistance(mapControlManager.getTolerance());
            Rectangle2D rect =
                new Rectangle2D.Double(firstPoint.getX() - tam,
                    firstPoint.getY() - tam, tam * 2, tam * 2);
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();

                boolean isSelectedHandler = false;
                Geometry geometry = getSelectedGeometry();
                if (geometry != null) {
                    Handler[] handlers =
                        geometry.getHandlers(Geometry.SELECTHANDLER);
                    for (int h = 0; h < handlers.length; h++) {
                        if (handlers[h].getPoint().distance(firstPoint) < tam) {
                            numSelect = h;
                            isSelectedHandler = true;
                            break;
                        }
                    }

                    if (!isSelectedHandler) {
                        boolean isSelectedGeometry = false;
                        if (geometry.intersects(rect)) { // , 0.1)){
                            isSelectedGeometry = true;
                        }
                        if (isSelectedGeometry && addVertex) {
                            try {
                                selectedIterator =
                                    featureStore.getFeatureSelection()
                                        .iterator();
                                Feature feat =
                                    (Feature) selectedIterator.next();
                                Point2D posVertex = new Point2D.Double(x, y);
                                Geometry geom1 =
                                    (feat.getDefaultGeometry()).cloneGeometry();
                                Geometry geom = null;
                                if (geom1 instanceof MultiPrimitive) {
                                    geom =
                                        addVertexGC((MultiPrimitive) geom1,
                                            posVertex, rect);
                                } else {
                                    geom = addVertex(geom1, posVertex, rect);
                                }
                                if (geom != null) {
                                    EditableFeature eFeature =
                                        feature.getEditable();
                                    eFeature.setGeometry(featureStore
                                        .getDefaultFeatureType()
                                        .getDefaultGeometryAttributeName(),
                                        geom);
                                    
                                    ed_man.updateFeature(featureStore, eFeature);
                                    
                                    Handler[] newHandlers =
                                        geom.getHandlers(Geometry.SELECTHANDLER);
                                    for (int h = 0; h < newHandlers.length; h++) {
                                        if (newHandlers[h].getPoint().distance(
                                            posVertex) < tam) {
                                            numSelect = h;
                                            isSelectedHandler = true;
                                        }
                                    }

                                    // clearSelection();
                                }
                            } finally {
                                if (selectedIterator != null) {
                                    selectedIterator.dispose();
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            
            ApplicationLocator.getManager().message(
                e.getMessage(), JOptionPane.ERROR_MESSAGE);
            logger.info("Error in select handler.", e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }

    }

    public String toString() {
        return "_editvertex";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { POINT, MULTIPOINT, CURVE, MULTICURVE, CIRCLE,
            ELLIPSE, SURFACE, MULTISURFACE };
    }

}
