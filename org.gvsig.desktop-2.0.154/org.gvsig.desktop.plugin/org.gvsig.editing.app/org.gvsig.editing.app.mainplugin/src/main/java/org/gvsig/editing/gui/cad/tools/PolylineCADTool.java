/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.PolylineCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.PolylineCADToolContext.PolylineCADToolState;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class PolylineCADTool extends AbstractCurveSurfaceCADTool {

    private static final Logger LOG = LoggerFactory.getLogger(PolylineCADTool.class);

    protected PolylineCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D antPoint;
    protected Point2D antantPoint;
    protected Point2D antCenter;
    protected Point2D antInter;
    protected List<Curve> list = new ArrayList<Curve>();
    protected boolean close = false;
    protected GeometryManager geomManager = GeometryLocator.getGeometryManager();

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new PolylineCADToolContext(this);
    }

    public Geometry getGeometry() {
        Geometry newGeom = null;
        newGeom = concatenateCurves(list, false);
        return newGeom;
    }



    public int getLinesCount() {
        return list.size();
    }

    public boolean isPolygonLayer() {
        try {
            return (((FLyrVect) getVLE().getLayer()).getShapeType() == Geometry.TYPES.SURFACE);
        } catch (ReadException e) {
            return false;
        }
    }

    public void endGeometry() {
        
        /*
        if (gpx == null) {
            gpx = new GeneralPathX();
            Geometry[] geoms = (Geometry[]) list.toArray(new Geometry[0]);
            MultiPrimitive fgc = createMultiPrimitive(geoms);
            // No queremos guardar FGeometryCollections:
            gpx.append(fgc.getPathIterator(null, geomManager.getFlatness()),
                true);
        }
        
        try {
            if (((FLyrVect) getVLE().getLayer()).getShapeType() == Geometry.TYPES.SURFACE
                && !close) {
                closeGeometry();
            }
        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        }
        */
        

        Geometry newGeom = null;
        int type = getCadToolAdapter().getActiveLayerType();
        
        if ((type == Geometry.TYPES.SURFACE)
            || (type == Geometry.TYPES.MULTISURFACE)) {
            
            newGeom = concatenateCurves(list, true);
            closeSurfaceIfNecessary((Surface) newGeom);
            // newGeom = createSurface(gpx);
        } else {
            newGeom = concatenateCurves(list, false);
            // newGeom = createCurve(gpx);
        }
        insertAndSelectGeometry(newGeom);
        _fsm = new PolylineCADToolContext(this);
        list.clear();
        clearTemporalCache();
        close = false;
        antantPoint = antCenter = antInter = antPoint = firstPoint = null;
    }

    /**
     * @param newGeom
     */
    private void closeSurfaceIfNecessary(Surface geo) {
        
        if (!close && geo != null && geo.getNumVertices() > 1) {
            Point firstp = geo.getVertex(0);
            firstp = (Point) firstp.cloneGeometry();
            geo.addVertex(firstp);
        }
        // TODO Auto-generated method stub
        
    }

    public void closeGeometry() {
        
        int ng = list.size();
        if (ng > 0) {
            Curve firstcurve = list.get(0);
            Curve lastcurve = list.get(ng - 1);
            Point firstp = firstcurve.getVertex(0);
            firstp = (Point) firstp.cloneGeometry();
            lastcurve.addVertex(firstp);
        }
        close = true;
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        PolylineCADToolState actualState =
            (PolylineCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        if (status.equals("Polyline.NextPointOrArcOrClose")
            || status.equals("Polyline.FirstPoint")) {

            if (firstPoint == null) {
                firstPoint = new Point2D.Double(x, y);
            }
            Point2D point = new Point2D.Double(x, y);

            if (antPoint != null
                && (antPoint.getX() != point.getX() || antPoint.getY() != point
                    .getY())) {
                GeneralPathX elShape =
                    new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 2);
                elShape.moveTo(antPoint.getX(), antPoint.getY());
                elShape.lineTo(point.getX(), point.getY());
                Curve geom = createCurve(elShape);
                if (geom != null) {
                    list.add(geom);
                    addTemporalCache(geom);
                }
            }
            if (antPoint == null)
                antPoint = (Point2D) firstPoint.clone();

            if (antPoint != null) {
                antantPoint = antPoint;
            }

            antPoint = point;
        } else
            if (status.equals("Polyline.NextPointOrLineOrClose")) {
                Point2D point = new Point2D.Double(x, y);
                Point2D lastp = antPoint; // (Point2D)points.get(i-1);

                if (antantPoint == null) {
                    antantPoint =
                        new Point2D.Double(lastp.getX() + (point.getX() / 2),
                            lastp.getY() + (point.getY() / 2));
                }

                if (((point.getY() == lastp.getY()) && (point.getX() < lastp
                    .getX()))
                    || ((point.getX() == lastp.getX()) && (point.getY() < lastp
                        .getY()))) {
                } else {
                    if (point.getX() == lastp.getX()) {
                        point =
                            new Point2D.Double(point.getX() + 0.00000001,
                                point.getY());
                    } else
                        if (point.getY() == lastp.getY()) {
                            point =
                                new Point2D.Double(point.getX(),
                                    point.getY() + 0.00000001);
                        }

                    if (point.getX() == antantPoint.getX()) {
                        point =
                            new Point2D.Double(point.getX() + 0.00000001,
                                point.getY());
                    } else
                        if (point.getY() == antantPoint.getY()) {
                            point =
                                new Point2D.Double(point.getX(),
                                    point.getY() + 0.00000001);
                        }

                    if (!(list.size() > 0)
                        || (((Geometry) list.get(list.size() - 1)).getType() == Geometry.TYPES.CURVE)) {
                        Point2D[] ps1 =
                            UtilFunctions.getPerpendicular(antantPoint, lastp,
                                lastp);
                        Point2D mediop =
                            new Point2D.Double(
                                (point.getX() + lastp.getX()) / 2,
                                (point.getY() + lastp.getY()) / 2);
                        Point2D[] ps2 =
                            UtilFunctions
                                .getPerpendicular(lastp, point, mediop);
                        Point2D interp =
                            UtilFunctions.getIntersection(ps1[0], ps1[1],
                                ps2[0], ps2[1]);
                        antInter = interp;

                        double radio = interp.distance(lastp);

                        if (UtilFunctions.isLowAngle(antantPoint, lastp,
                            interp, point)) {
                            radio = -radio;
                        }

                        Point2D centerp =
                            UtilFunctions.getPoint(interp, mediop, radio);
                        antCenter = centerp;

                        Curve ig = createArc(lastp, centerp, point);

                        if (ig != null) {
                            list.add(ig);
                            addTemporalCache(ig);
                        } else {
                            
                            ApplicationLocator.getManager().message(
                                Messages.getText("_Unable_to_create_arc"),
                                JOptionPane.ERROR_MESSAGE);

                        }
                    } else {
                        Point2D[] ps1 =
                            UtilFunctions.getPerpendicular(lastp, antInter,
                                lastp);
                        double a1 = UtilFunctions.getAngle(ps1[0], ps1[1]);
                        double a2 = UtilFunctions.getAngle(ps1[1], ps1[0]);
                        double angle = UtilFunctions.getAngle(antCenter, lastp);
                        Point2D ini1 = null;
                        Point2D ini2 = null;

                        if (UtilFunctions.absoluteAngleDistance(angle, a1) > UtilFunctions
                            .absoluteAngleDistance(angle, a2)) {
                            ini1 = ps1[0];
                            ini2 = ps1[1];
                        } else {
                            ini1 = ps1[1];
                            ini2 = ps1[0];
                        }

                        Point2D unit = UtilFunctions.getUnitVector(ini1, ini2);
                        Point2D correct =
                            new Point2D.Double(lastp.getX() + unit.getX(),
                                lastp.getY() + unit.getY());
                        Point2D[] ps =
                            UtilFunctions.getPerpendicular(lastp, correct,
                                lastp);
                        Point2D mediop =
                            new Point2D.Double(
                                (point.getX() + lastp.getX()) / 2,
                                (point.getY() + lastp.getY()) / 2);
                        Point2D[] ps2 =
                            UtilFunctions
                                .getPerpendicular(lastp, point, mediop);
                        Point2D interp =
                            UtilFunctions.getIntersection(ps[0], ps[1], ps2[0],
                                ps2[1]);
                        antInter = interp;

                        double radio = interp.distance(lastp);

                        if (UtilFunctions.isLowAngle(correct, lastp, interp,
                            point)) {
                            radio = -radio;
                        }

                        Point2D centerp =
                            UtilFunctions.getPoint(interp, mediop, radio);
                        antCenter = centerp;
                        Curve geom = createArc(lastp, centerp, point);
                        
                        if (geom != null) {
                            list.add(geom);
                            addTemporalCache(geom);
                        }
                    }

                    antantPoint = antPoint;
                    antPoint = point;
                }
            }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        PolylineCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if (status.equals("Polyline.NextPointOrArcOrClose")
            || status.equals("Polyline.FirstPoint")) {
            for (int i = 0; i < list.size(); i++) {
                renderer.draw((Geometry) list.get(i),
                    mapControlManager.getGeometrySelectionSymbol());
            }
            if (antPoint != null)
                renderer.drawLine(antPoint, new Point2D.Double(x, y),
                    mapControlManager.getGeometrySelectionSymbol());

        } else
            if ((status.equals("Polyline.NextPointOrLineOrClose"))) {
                for (int i = 0; i < list.size(); i++) {
                    renderer.draw((Geometry) list.get(i),
                        mapControlManager.getGeometrySelectionSymbol());
                }

                Point2D point = new Point2D.Double(x, y);
                Point2D lastp = antPoint;

                if (!(list.size() > 0)
                    || (((Geometry) list.get(list.size() - 1)).getType() == Geometry.TYPES.CURVE)) {
                    if (antantPoint == null) {
                        drawArc(point, lastp, new Point2D.Double(lastp.getX()
                            + (point.getX() / 2), lastp.getY()
                            + (point.getY() / 2)), renderer);
                    } else {
                        drawArc(point, lastp, antantPoint, renderer);
                    }
                } else {
                    if (antInter != null) {
                        Point2D[] ps1 =
                            UtilFunctions.getPerpendicular(lastp, antInter,
                                lastp);
                        double a1 = UtilFunctions.getAngle(ps1[0], ps1[1]);
                        double a2 = UtilFunctions.getAngle(ps1[1], ps1[0]);
                        double angle = UtilFunctions.getAngle(antCenter, lastp);
                        Point2D ini1 = null;
                        Point2D ini2 = null;

                        if (UtilFunctions.absoluteAngleDistance(angle, a1) > UtilFunctions
                            .absoluteAngleDistance(angle, a2)) {
                            ini1 = ps1[0];
                            ini2 = ps1[1];
                        } else {
                            ini1 = ps1[1];
                            ini2 = ps1[0];
                        }

                        Point2D unit = UtilFunctions.getUnitVector(ini1, ini2);
                        Point2D correct =
                            new Point2D.Double(lastp.getX() + unit.getX(),
                                lastp.getY() + unit.getY());
                        drawArc(point, lastp, correct, renderer);
                    }
                }
            }
        try {
            if (((FLyrVect) getVLE().getLayer()).getShapeType() == Geometry.TYPES.SURFACE
                && !list.isEmpty()) {
                Handler handler1 =
                    ((Geometry) list.get(0))
                        .getHandlers(Geometry.SELECTHANDLER)[0];
                GeneralPathX gpx = new GeneralPathX();
                gpx.moveTo(x, y);
                Point2D p1 = handler1.getPoint();
                gpx.lineTo(p1.getX(), p1.getY());
                Curve curve = createCurve(gpx);
                renderer.draw(curve,
                    mapControlManager.getGeometrySelectionSymbol());
            }
        } catch (ReadException e) {
            e.printStackTrace();
        }
    }

    /**
     * Dibuja el arco sobre el graphics.
     * 
     * @param point
     *            Puntero del rat�n.
     * @param lastp
     *            �ltimo punto de la polilinea.
     * @param antp
     *            Punto antepenultimo.
     * @param g
     *            Graphics sobre el que se dibuja.
     */
    private void drawArc(Point2D point, Point2D lastp, Point2D antp,
        MapControlDrawer renderer) {
        if (((point.getY() == lastp.getY()) && (point.getX() < lastp.getX()))
            || ((point.getX() == lastp.getX()) && (point.getY() < lastp.getY()))) {
        } else {
            if (point.getX() == lastp.getX()) {
                point =
                    new Point2D.Double(point.getX() + 0.00000001, point.getY());
            } else
                if (point.getY() == lastp.getY()) {
                    point =
                        new Point2D.Double(point.getX(),
                            point.getY() + 0.00000001);
                }

            if (point.getX() == antp.getX()) {
                point =
                    new Point2D.Double(point.getX() + 0.00000001, point.getY());
            } else
                if (point.getY() == antp.getY()) {
                    point =
                        new Point2D.Double(point.getX(),
                            point.getY() + 0.00000001);
                }

            Point2D[] ps1 = UtilFunctions.getPerpendicular(lastp, antp, lastp);
            Point2D mediop =
                new Point2D.Double((point.getX() + lastp.getX()) / 2,
                    (point.getY() + lastp.getY()) / 2);
            Point2D[] ps2 =
                UtilFunctions.getPerpendicular(lastp, point, mediop);
            Point2D interp =
                UtilFunctions.getIntersection(ps1[0], ps1[1], ps2[0], ps2[1]);

            double radio = interp.distance(lastp);

            if (UtilFunctions.isLowAngle(antp, lastp, interp, point)) {
                radio = -radio;
            }

            Point2D centerp = UtilFunctions.getPoint(interp, mediop, radio);
            renderer.drawLine(lastp, point,
                mapControlManager.getGeometrySelectionSymbol());

            Geometry ig = createArc(lastp, centerp, point);

            if (ig != null) {
                renderer.draw(ig,
                    mapControlManager.getGeometrySelectionSymbol());
            }
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        // Nothing to do
    }

    public void addValue(double d) {
        // Nothing to do
    }

    public void cancel() {
        list.clear();
        clearTemporalCache();
        antantPoint = antCenter = antInter = antPoint = firstPoint = null;
    }

    public void end() {
        // Nothing to do
    }

    public String getName() {
        return PluginServices.getText(this, "polyline_");
    }

    public String toString() {
        return "_polyline";
    }

    public void endTransition(double x, double y, MouseEvent event) {
        _fsm.endPoint(x, y, event);
    }
    
    private Point create3DPoint(Point p) {
    	try {
			return geomManager.createPoint(p.getX(), p.getY(), Geometry.SUBTYPES.GEOM3D);
		} catch (CreateGeometryException e) {
			return p;
		}
    }
    
    /**
     * @param geoms
     * @return
     * @throws CreateGeometryException 
     */
    private Geometry concatenateCurves(List<Curve> cus, boolean surf) {
        
        Curve _curv = null;
        Surface _surf = null;
        int subtype = cus.size() > 0 && cus.get(0) != null ? cus.get(0).getGeometryType().getSubType() : Geometry.SUBTYPES.GEOM2D;
		boolean is3D = subtype == 1 || subtype == 3;
        
        try {
            if (surf) {
                _surf = (Surface) geomManager.create(Geometry.TYPES.SURFACE, subtype);
            } else {
                _curv = (Curve) geomManager.create(Geometry.TYPES.CURVE, subtype);
            }
        } catch (CreateGeometryException e) {
            LOG.info("Unable to create geometry: " + e.getMessage());
            ApplicationLocator.getManager().message(
                Messages.getText("_Unable_to_create_geometry"),
                JOptionPane.ERROR_MESSAGE);
            return null;
        }
        
        
        if (cus == null || cus.size() == 0) {
            if (surf) {
                return _surf;
            } else {
                return _curv;
            }
        }
        
        Curve item_cu = cus.get(0);
        int len = item_cu.getNumVertices();
        Point po = null;
        /*
         * All from first curve
         */
        for (int i = 0; i < len; i++) {
        	po = item_cu.getVertex(i);
        	if(is3D) {
        		po = create3DPoint(po);
        	} 
        	if (surf) {
        		_surf.addVertex(po);
        	} else {
        		_curv.addVertex(po);
        	}

        }
        
        for (int n = 1; n < cus.size(); n++) {
            
            item_cu = cus.get(n);
            len = item_cu.getNumVertices();
            for (int i = 0; i < len; i++) {
                po = item_cu.getVertex(i);
                if(is3D) {
            		po = create3DPoint(po);
            	} 
                
                /*
                 * Add only if not same coords
                 */
                if (surf) {
                    if (differentCoordinates(po,
                        _surf.getVertex(
                        _surf.getNumVertices()-1))) {
                        _surf.addVertex(po);
                    }
                } else {
                    if (differentCoordinates(po,
                        _curv.getVertex(
                        _curv.getNumVertices()-1))) {
                        _curv.addVertex(po);
                    }
                }
            }
        }
        
        if (surf) {
            return _surf;
        } else {
            return _curv;
        }
        
    }

    /**
     * @param po
     * @param vertex
     * @return
     */
    private boolean differentCoordinates(Point p1, Point p2) {
        return !p1.equals(p2);
    }

}
