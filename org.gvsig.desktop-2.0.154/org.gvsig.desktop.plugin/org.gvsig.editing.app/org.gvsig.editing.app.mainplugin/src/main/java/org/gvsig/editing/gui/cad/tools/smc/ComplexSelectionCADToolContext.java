/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

//
// Vicente Caballero Navarro


package org.gvsig.editing.gui.cad.tools.smc;

import java.awt.event.InputEvent;

import org.gvsig.editing.gui.cad.tools.ComplexSelectionCADTool;
import org.gvsig.i18n.Messages;


public final class ComplexSelectionCADToolContext
extends statemap.FSMContext
{
    private static String[] DEFAULT_OPTIONS = new String[]{"out_rectangle", "inside_polygon", "cross_polygon", 
        "out_polygon", "inside_circle", "cross_circle", "out_circle", "select_all", "cancel"};
    private static String[] CLOSE_POLYGON_OPTIONS = new String[]{"end_polygon", "out_rectangle", "inside_polygon", "cross_polygon", 
        "out_polygon", "inside_circle", "cross_circle", "out_circle", "select_all", "cancel"};
    private static String SELECTION_QUESTION =
        
        Messages.getText("inside_circle")
        +"["+Messages.getText("ComplexSelectionCADTool.introcircle")+"], "
        +Messages.getText("out_rectangle")
        +"["+Messages.getText("ComplexSelectionCADTool.outrectangle")+"], "
        +Messages.getText("inside_polygon")
        +"["+Messages.getText("ComplexSelectionCADTool.intropolygon")+"]"
        +"\n#"
        +Messages.getText("cross_polygon")
        +"["+Messages.getText("ComplexSelectionCADTool.crosspolygon")+"], "
        +Messages.getText("out_polygon")
        +"["+Messages.getText("ComplexSelectionCADTool.outpolygon")+"], "
        +Messages.getText("cross_circle")
        +"["+Messages.getText("ComplexSelectionCADTool.crosscircle")+"]"
        +"\n#"
        +Messages.getText("out_circle")
        +"["+Messages.getText("ComplexSelectionCADTool.outcircle")+"]";

    //---------------------------------------------------------------
    // Member methods.
    //

    public ComplexSelectionCADToolContext(ComplexSelectionCADTool owner)
    {
        super();

        _owner = owner;
        setState(Selection.FirstPoint);
        Selection.FirstPoint.Entry(this);
    }

    public void addOption(String s)
    {
        _transition = "addOption";
        getState().addOption(this, s);
        _transition = "";
        return;
    }

    public void addPoint(double pointX, double pointY, InputEvent event)
    {
        _transition = "addPoint";
        getState().addPoint(this, pointX, pointY, event);
        _transition = "";
        return;
    }

    public void addValue(double d)
    {
        _transition = "addValue";
        getState().addValue(this, d);
        _transition = "";
        return;
    }

    public ComplexSelectionCADToolState getState()
    throws statemap.StateUndefinedException
    {
        if (_state == null)
        {
            throw(
                new statemap.StateUndefinedException());
        }

        return ((ComplexSelectionCADToolState) _state);
    }

    protected ComplexSelectionCADTool getOwner()
    {
        return (_owner);
    }

    //---------------------------------------------------------------
    // Member data.
    //

    transient private ComplexSelectionCADTool _owner;

    //---------------------------------------------------------------
    // Inner classes.
    //

    public static abstract class ComplexSelectionCADToolState
    extends statemap.State
    {
        //-----------------------------------------------------------
        // Member methods.
        //

        protected ComplexSelectionCADToolState(String name, int id)
        {
            super (name, id);
        }

        protected void Entry(ComplexSelectionCADToolContext context) {}
        protected void Exit(ComplexSelectionCADToolContext context) {}

        protected void addOption(ComplexSelectionCADToolContext context, String s)
        {
            Default(context);
        }

        protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
        {
            Default(context);
        }

        protected void addValue(ComplexSelectionCADToolContext context, double d)
        {
            Default(context);
        }

        protected void Default(ComplexSelectionCADToolContext context)
        {
            throw (
                new statemap.TransitionUndefinedException(
                    "State: " +
                    context.getState().getName() +
                    ", Transition: " +
                    context.getTransition()));
        }

        //-----------------------------------------------------------
        // Member data.
        //
    }

    /* package */ static abstract class Selection
    {
        //-----------------------------------------------------------
        // Member methods.
        //

        //-----------------------------------------------------------
        // Member data.
        //

        //-------------------------------------------------------
        // Statics.
        //
        /* package */ static Selection_Default.Selection_FirstPoint FirstPoint;
        /* package */ static Selection_Default.Selection_SecondPoint SecondPoint;
        /* package */ static Selection_Default.Selection_WithSelectedFeatures WithSelectedFeatures;
        /* package */ static Selection_Default.Selection_WithHandlers WithHandlers;
        /* package */ static Selection_Default.Selection_SecondPointOutRectangle SecondPointOutRectangle;
        /* package */ static Selection_Default.Selection_SecondPointCircle SecondPointCircle;
        /* package */ static Selection_Default.Selection_NextPointPolygon NextPointPolygon;
        private static Selection_Default Default;

        static
        {
            FirstPoint = new Selection_Default.Selection_FirstPoint("Selection.FirstPoint", 0);
            SecondPoint = new Selection_Default.Selection_SecondPoint("Selection.SecondPoint", 1);
            WithSelectedFeatures = new Selection_Default.Selection_WithSelectedFeatures("Selection.WithSelectedFeatures", 2);
            WithHandlers = new Selection_Default.Selection_WithHandlers("Selection.WithHandlers", 3);
            SecondPointOutRectangle = new Selection_Default.Selection_SecondPointOutRectangle("Selection.SecondPointOutRectangle", 4);
            SecondPointCircle = new Selection_Default.Selection_SecondPointCircle("Selection.SecondPointCircle", 5);
            NextPointPolygon = new Selection_Default.Selection_NextPointPolygon("Selection.NextPointPolygon", 6);
            Default = new Selection_Default("Selection.Default", -1);
        }

    }

    protected static class Selection_Default
    extends ComplexSelectionCADToolState
    {
        //-----------------------------------------------------------
        // Member methods.
        //

        protected Selection_Default(String name, int id)
        {
            super (name, id);
        }

        protected void addOption(ComplexSelectionCADToolContext context, String s)
        {
            ComplexSelectionCADTool ctxt = context.getOwner();

            if (s.equals(Messages.getText("cancel")))
            {
                boolean loopbackFlag =
                    context.getState().getName().equals(
                        Selection.FirstPoint.getName());

                if (loopbackFlag == false)
                {
                    (context.getState()).Exit(context);
                }

                context.clearState();
                try
                {
                    ctxt.end();
                }
                finally
                {
                    context.setState(Selection.FirstPoint);

                    if (loopbackFlag == false)
                    {
                        (context.getState()).Entry(context);
                    }

                }
            }
            else
            {
                boolean loopbackFlag =
                    context.getState().getName().equals(
                        Selection.FirstPoint.getName());

                if (loopbackFlag == false)
                {
                    (context.getState()).Exit(context);
                }

                context.clearState();
                try
                {
                    ctxt.throwOptionException(Messages.getText("incorrect_option"), s);
                }
                finally
                {
                    context.setState(Selection.FirstPoint);

                    if (loopbackFlag == false)
                    {
                        (context.getState()).Entry(context);
                    }

                }
            }

            return;
        }

        protected void addValue(ComplexSelectionCADToolContext context, double d)
        {
            ComplexSelectionCADTool ctxt = context.getOwner();

            boolean loopbackFlag =
                context.getState().getName().equals(
                    Selection.FirstPoint.getName());

            if (loopbackFlag == false)
            {
                (context.getState()).Exit(context);
            }

            context.clearState();
            try
            {
                ctxt.throwValueException(Messages.getText("incorrect_value"), d);
            }
            finally
            {
                context.setState(Selection.FirstPoint);

                if (loopbackFlag == false)
                {
                    (context.getState()).Entry(context);
                }

            }
            return;
        }

        protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
        {
            ComplexSelectionCADTool ctxt = context.getOwner();

            boolean loopbackFlag =
                context.getState().getName().equals(
                    Selection.FirstPoint.getName());

            if (loopbackFlag == false)
            {
                (context.getState()).Exit(context);
            }

            context.clearState();
            try
            {
                ctxt.throwPointException(Messages.getText("incorrect_point"), pointX, pointY);
            }
            finally
            {
                context.setState(Selection.FirstPoint);

                if (loopbackFlag == false)
                {
                    (context.getState()).Entry(context);
                }

            }
            return;
        }

        //-----------------------------------------------------------
        // Inner classse.
        //


        private static final class Selection_FirstPoint
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_FirstPoint(String name, int id)
            {
                super (name, id);
            }

            protected void Entry(ComplexSelectionCADToolContext context)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.getType().equals(Messages.getText("inside_polygon")) || ctxt.getType().equals(Messages.getText("cross_polygon")) || ctxt.getType().equals(Messages.getText("out_polygon"))){
                    ctxt.setQuestion(Messages.getText("insert_first_point"));
                }else if(ctxt.getType().equals(Messages.getText("inside_circle")) || ctxt.getType().equals(Messages.getText("cross_circle")) || ctxt.getType().equals(Messages.getText("out_circle"))){
                    ctxt.setQuestion(Messages.getText("insert_first_point"));
                }else if (ctxt.getType().equals(Messages.getText("out_rectangle"))){
                    ctxt.setQuestion(Messages.getText("insert_first_point"));
                }else{
                    ctxt.setQuestion(SELECTION_QUESTION);     
                }
                ctxt.setDescription(DEFAULT_OPTIONS);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                ComplexSelectionCADToolState endState = context.getState();

                context.clearState();
                try
                {
                    if (s.equals(Messages.getText("inside_polygon")) || s.equals(Messages.getText("cross_polygon")) || s.equals(Messages.getText("out_polygon"))){
                        ctxt.setQuestion(Messages.getText("insert_first_point"));
                    }else if(s.equals(Messages.getText("inside_circle")) || s.equals(Messages.getText("cross_circle")) || s.equals(Messages.getText("out_circle"))){
                        ctxt.setQuestion(Messages.getText("insert_first_point"));
                    }else if (s.equals(Messages.getText("out_rectangle"))){
                        ctxt.setQuestion(Messages.getText("insert_first_point"));
                    }else if (s.equals(Messages.getText("select_all"))){
                        if (ctxt.selectAll() > 0){ 
                            try
                            {                                           
                                ctxt.setQuestion(Messages.getText("select_handlers"));
                                ctxt.setDescription(DEFAULT_OPTIONS);
                                ctxt.end();
                            }
                            finally
                            {
                                endState = Selection.WithSelectedFeatures;                                 
                                return;
                            }                          

                        }
                    }
                    else{
                        ctxt.setQuestion(SELECTION_QUESTION);    
                    }
                    ctxt.setDescription(DEFAULT_OPTIONS);
                    ctxt.addOption(s);
                }
                finally
                {
                    context.setState(endState);
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.getType().equals(Messages.getText("out_rectangle")))
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("insert_second_point_selection"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.SecondPointOutRectangle);
                        (context.getState()).Entry(context);
                    }
                }
                else if (ctxt.getType().equals(Messages.getText("inside_circle")) || ctxt.getType().equals(Messages.getText("cross_circle")) || ctxt.getType().equals(Messages.getText("out_circle")))
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("insert_radius_or_second_point"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.SecondPointCircle);
                        (context.getState()).Entry(context);
                    }
                }
                else if (ctxt.getType().equals(Messages.getText("inside_polygon")) || ctxt.getType().equals(Messages.getText("cross_polygon")) || ctxt.getType().equals(Messages.getText("out_polygon")))
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("insert_next_point_selection_or_end_polygon")+
                            "["+Messages.getText("ComplexSelectionCADTool.end")+"]");
                        ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.NextPointPolygon);
                        (context.getState()).Entry(context);
                    }
                }
                else if (ctxt.getType().equals(Messages.getText("simple")) && ctxt.selectFeatures(pointX,pointY, event) && ctxt.getNextState().equals("Selection.SecondPoint"))
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("insert_second_point_selection"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.SecondPoint);
                        (context.getState()).Entry(context);
                    }
                }                else
                {
                    super.addPoint(context, pointX, pointY, event);
                }

                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_SecondPoint
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_SecondPoint(String name, int id)
            {
                super (name, id);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();


                (context.getState()).Exit(context);
                context.clearState();
                try
                {
                    ctxt.setQuestion(SELECTION_QUESTION);    
                    ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                    ctxt.setType(s);
                }
                finally
                {
                    context.setState(Selection.FirstPoint);
                    (context.getState()).Entry(context);
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.selectWithSecondPoint(pointX,pointY, event) > 0)
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("select_handlers"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                        ctxt.end();
                    }
                    finally
                    {
                        context.setState(Selection.WithSelectedFeatures);
                        (context.getState()).Entry(context);
                    }
                }
                else
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(SELECTION_QUESTION);    
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.FirstPoint);
                        (context.getState()).Entry(context);
                    }
                }

                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_WithSelectedFeatures
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_WithSelectedFeatures(String name, int id)
            {
                super (name, id);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();


                (context.getState()).Exit(context);
                context.clearState();
                try
                {
                    ctxt.setQuestion(SELECTION_QUESTION);    
                    ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                    ctxt.setType(s);
                }
                finally
                {
                    context.setState(Selection.FirstPoint);
                    (context.getState()).Entry(context);
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.selectHandlers(pointX, pointY, event)>0)
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("insert_destination_point"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.WithHandlers);
                        (context.getState()).Entry(context);
                    }
                }
                else if (ctxt.selectFeatures(pointX,pointY, event) && ctxt.getNextState().equals("Selection.WithSelectedFeatures"))
                {
                    ComplexSelectionCADToolState endState = context.getState();

                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("select_handlers"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(endState);
                    }
                }
                else
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(SELECTION_QUESTION);    
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.FirstPoint);
                        (context.getState()).Entry(context);
                    }
                }

                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_WithHandlers
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_WithHandlers(String name, int id)
            {
                super (name, id);
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();


                (context.getState()).Exit(context);
                context.clearState();
                try
                {
                    ctxt.setQuestion(Messages.getText("select_handlers"));
                    ctxt.setDescription(DEFAULT_OPTIONS);
                    ctxt.addPoint(pointX, pointY, event);
                    ctxt.refresh();
                }
                finally
                {
                    context.setState(Selection.WithSelectedFeatures);
                    (context.getState()).Entry(context);
                }
                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_SecondPointOutRectangle
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_SecondPointOutRectangle(String name, int id)
            {
                super (name, id);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();


                (context.getState()).Exit(context);
                context.clearState();
                try
                {
                    ctxt.setQuestion(SELECTION_QUESTION);    
                    ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                    ctxt.setType(s);
                }
                finally
                {
                    context.setState(Selection.FirstPoint);
                    (context.getState()).Entry(context);
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.selectWithSecondPointOutRectangle(pointX,pointY, event) > 0)
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("select_handlers"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                        ctxt.end();
                    }
                    finally
                    {
                        context.setState(Selection.WithSelectedFeatures);
                        (context.getState()).Entry(context);
                    }
                }
                else
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(SELECTION_QUESTION);    
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.FirstPoint);
                        (context.getState()).Entry(context);
                    }
                }

                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_SecondPointCircle
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_SecondPointCircle(String name, int id)
            {
                super (name, id);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();


                (context.getState()).Exit(context);
                context.clearState();
                try
                {
                    ctxt.setQuestion(SELECTION_QUESTION);    
                    ctxt.setDescription(DEFAULT_OPTIONS);
                    ctxt.setType(s);
                }
                finally
                {
                    context.setState(Selection.FirstPoint);
                    (context.getState()).Entry(context);
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                if (ctxt.selectWithCircle(pointX,pointY, event) > 0)
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(Messages.getText("select_handlers"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                        ctxt.end();
                    }
                    finally
                    {
                        context.setState(Selection.WithSelectedFeatures);
                        (context.getState()).Entry(context);
                    }
                }
                else
                {

                    (context.getState()).Exit(context);
                    context.clearState();
                    try
                    {
                        ctxt.setQuestion(SELECTION_QUESTION);    
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.addPoint(pointX, pointY, event);
                    }
                    finally
                    {
                        context.setState(Selection.FirstPoint);
                        (context.getState()).Entry(context);
                    }
                }

                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        private static final class Selection_NextPointPolygon
        extends Selection_Default
        {
            //-------------------------------------------------------
            // Member methods.
            //

            private Selection_NextPointPolygon(String name, int id)
            {
                super (name, id);
            }

            protected void addOption(ComplexSelectionCADToolContext context, String s)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                ComplexSelectionCADToolState state = context.getState();


                (context.getState()).Exit(context);
                context.clearState();



                if (ctxt.selectCurrentSurface() > 0){
                    try
                    {                      
                        ctxt.setQuestion(Messages.getText("select_handlers"));
                        ctxt.setDescription(DEFAULT_OPTIONS);
                        ctxt.end();
                    }
                    finally
                    {
                        context.setState(Selection.WithSelectedFeatures);
                        (context.getState()).Entry(context);
                    }
                }else{
                    try
                    {
                        ctxt.setQuestion(SELECTION_QUESTION);    
                        ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                        ctxt.setType(s);
                    }
                    finally
                    {
                        context.setState(Selection.FirstPoint);
                        (context.getState()).Entry(context);
                    }
                }
                return;
            }

            protected void addPoint(ComplexSelectionCADToolContext context, double pointX, double pointY, InputEvent event)
            {
                ComplexSelectionCADTool ctxt = context.getOwner();

                ComplexSelectionCADToolState endState = context.getState();

                context.clearState();
                try
                {
                    ctxt.setQuestion(Messages.getText("insert_next_point_selection_or_end_polygon")+
                        "["+Messages.getText("ComplexSelectionCADTool.end")+"]");
                    ctxt.setDescription(CLOSE_POLYGON_OPTIONS);
                    ctxt.addPoint(pointX, pointY, event);
                }
                finally
                {
                    context.setState(endState);
                }
                return;
            }

            //-------------------------------------------------------
            // Member data.
            //
        }

        //-----------------------------------------------------------
        // Member data.
        //
    }
}
