/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.accelerators;

import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.mapcontrol.MapControl;


/**
 * @author fjp
 *
 */
public class GridAccelerator implements KeyEventDispatcher {

	public boolean dispatchKeyEvent(KeyEvent e) {
		if (e.getID() == KeyEvent.KEY_PRESSED)
			return false;

		IWindow v = PluginServices.getMDIManager().getActiveWindow();
		if (!(v instanceof org.gvsig.app.project.documents.view.gui.DefaultViewPanel))
			return false;

		MapControl mc=((DefaultViewPanel)v).getMapControl();
		mc.setGridVisibility(!mc.getGrid().isShowGrid());

		System.err.println("Ponemos GRID a " + mc.getGrid().isShowGrid());

		return false;
	}

}


