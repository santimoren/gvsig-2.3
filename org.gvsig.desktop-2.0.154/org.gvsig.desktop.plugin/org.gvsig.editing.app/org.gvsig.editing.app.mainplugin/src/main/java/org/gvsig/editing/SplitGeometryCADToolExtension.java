/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id:
 * $Log:
 */
package org.gvsig.editing;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.gui.cad.tools.SplitGeometryCADTool;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;


/**
 * CAD extension to split geometries from a digitized linear geometry.
 *
 * @author Alvaro Zabala
 *
 */
public class SplitGeometryCADToolExtension extends Extension {

    private DefaultViewPanel view;
    private MapControl mapControl;
    private SplitGeometryCADTool cadTool;


    public void execute(String actionCommand) {
        CADExtension.initFocus();
        if (actionCommand.equals(SplitGeometryCADTool.SPLIT_GEOMETRY_ACTION_NAME)) {
            CADExtension.setCADTool(SplitGeometryCADTool.SPLIT_GEOMETRY_TOOL_NAME, true);
        }
        CADExtension.getEditionManager().setMapControl(mapControl);
        CADExtension.getCADToolAdapter().configureMenu();
    }

    public void initialize() {
        cadTool = new SplitGeometryCADTool();
        CADExtension.addCADTool(SplitGeometryCADTool.SPLIT_GEOMETRY_TOOL_NAME, cadTool);
        registerIcons();
    }

    private void registerIcons(){
		IconThemeHelper.registerIcon("action", "layer-modify-split", this);
    }

    /**
     * Returns if this Edit CAD tool is visible.
     * For this, there must be an active vectorial editing lyr in the TOC, which geometries'
     * dimension would must be linear or polygonal, and with at least one selected geometry.
     *
     */
    public boolean isEnabled() {
        try {
            if (EditionUtilities.getEditionStatus() ==
                EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE) {
                this.view = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
                mapControl = view.getMapControl();
                if (CADExtension.getEditionManager().getActiveLayerEdited() == null)
                    return false;
                FLyrVect lv = (FLyrVect) CADExtension.
                getEditionManager().
                getActiveLayerEdited().
                getLayer();
                if (isPoint(lv.getShapeType())){             
                    return false;
                }

                return !lv.getFeatureStore().getFeatureSelection().isEmpty();
            }
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(),e);
            return false;
        }
        return true;
    }
   
    private boolean isPoint(int shapeType) {
        switch (shapeType) {        
        case Geometry.TYPES.MULTIPOINT:
        case Geometry.TYPES.POINT:      
            return true;
        default:
            return false;
        }
    }

    public boolean isVisible() {
        if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE)
            return true;
        return false;
    }
}
