/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.panels;

import jwizardcomponent.JWizardComponents;
import jwizardcomponent.JWizardPanel;
public class PostGISpanel extends JWizardPanel {

	public PostGISpanel(JWizardComponents wizardComponents) {
		super(wizardComponents);
        // initialize();

	}

    // /**
    // *
    // */
    // private static final long serialVersionUID = 1L;
    // private ConnectionPanel jPanelConex = null;
    //
    // /**
    // * This method initializes this
    // *
    // */
    // private void initialize() {
    // this.setSize(new java.awt.Dimension(408,284));
    // this.add(getJPanelConex(), null);
    //
    // }
    //
    // /**
    // * This method initializes jPanelConex
    // *
    // * @return javax.swing.JPanel
    // */
    // private ConnectionPanel getJPanelConex() {
    // if (jPanelConex == null) {
    // jPanelConex = new ConnectionPanel();
    // String[] drvAux = new String[1];
    // drvAux[0] = "PostGIS JDBC Driver";
    // jPanelConex.setDrivers(drvAux);
    // jPanelConex.setPreferredSize(new java.awt.Dimension(400,300));
    // }
    // return jPanelConex;
    // }
    //
    // public ConnectionSettings getConnSettings() {
    // return getJPanelConex().getConnectionSettings();
    // }

}  //  @jve:decl-index=0:visual-constraint="10,10"
