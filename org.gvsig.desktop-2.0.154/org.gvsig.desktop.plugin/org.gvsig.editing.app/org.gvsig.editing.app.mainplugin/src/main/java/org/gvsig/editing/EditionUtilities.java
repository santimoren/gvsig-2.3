/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.util.ArrayList;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;


/**
 * @author fjp
 *
 * Clase con m�todos muy �tiles a la hora de hacer nuevas extensiones, y otras
 * cosas que puedan ser gen�ricas para este plugin.
 *
 */
public class EditionUtilities {

	public static final int EDITION_STATUS_NO_EDITION = 0;
	public static final int EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE = 1;
	public static final int EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE = 2;
	public static final int EDITION_STATUS_MULTIPLE_VECTORIAL_LAYER_ACTIVE = 3;
	public static final int EDITION_STATUS_MULTIPLE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE = 4;
	public static int getEditionStatus()
	{
		int status = EDITION_STATUS_NO_EDITION;
        org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
        .getActiveWindow();
        if (f == null)
        	return status;

        if (f instanceof DefaultViewPanel) {
        	DefaultViewPanel vista = (DefaultViewPanel) f;
        	ViewDocument model = vista.getModel();
        	MapContext mapa = model.getMapContext();

        	FLayers capas = mapa.getLayers();

        	int numActiveVectorial = 0;
        	int numActiveVectorialEditable = 0;

        	LayersIterator iter = new LayersIterator(capas);

        	FLayer capa;
        	while (iter.hasNext()) {
        		capa = iter.nextLayer();
        		if (capa instanceof FLyrVect &&
        				capa.isActive() && capa.isAvailable()) {
        			numActiveVectorial++;
        			if (capa.isEditing())
        				numActiveVectorialEditable++;
        		}

        	}

        	if (numActiveVectorialEditable == 1 && numActiveVectorial == 1)
        		return EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE;
        	if (numActiveVectorialEditable > 1 && numActiveVectorial == numActiveVectorialEditable)
        		return EDITION_STATUS_MULTIPLE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE;
        	if (numActiveVectorial == 1)
        		return EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE;
        	if (numActiveVectorial > 1)
        		return EDITION_STATUS_MULTIPLE_VECTORIAL_LAYER_ACTIVE;

        }

		return status;
	}

	public static FLayer[] getActiveAndEditedLayers()
	{
		int status = EDITION_STATUS_NO_EDITION;
        org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
        .getActiveWindow();
        if (f == null)
        	return null;

        if (f instanceof DefaultViewPanel) {
        	DefaultViewPanel vista = (DefaultViewPanel) f;
        	ViewDocument model = vista.getModel();
        	MapContext mapa = model.getMapContext();

        	ArrayList resul = new ArrayList();

        	FLayers capas = mapa.getLayers();

        	int numActiveVectorial = 0;
        	int numActiveVectorialEditable = 0;

        	LayersIterator iter = new LayersIterator(capas);
        	FLayer capa;
        	while (iter.hasNext()) {
        		capa = iter.nextLayer();
        		if (capa instanceof FLyrVect &&
        				capa.isActive()) {
        			numActiveVectorial++;
        			if (capa.isEditing())
        			{
        				numActiveVectorialEditable++;
        				resul.add(capa);
        			}
        		}

        	}
        	if (resul.isEmpty())
        		return null;

       		return (FLayer[]) resul.toArray(new FLayer[0]);

        }

		return null;
	}

//	public static ILayerDefinition createLayerDefinition(FLyrVect layer) throws ReadDriverException {
//		LayerDefinition lyrDef;
//		if (layer.getSource().getDriver() instanceof IVectorialDatabaseDriver)
//		{
//			VectorialEditableAdapter vea = (VectorialEditableAdapter)layer.getSource();
//			IVectorialDatabaseDriver dbDriver = (IVectorialDatabaseDriver) vea.getDriver();
//
//			DBLayerDefinition dbldef=dbDriver.getLyrDef();
//			dbldef.setFieldsDesc(vea.getFieldsDescription());
//			return dbldef;
//		}
//		lyrDef = new LayerDefinition();
//		lyrDef.setShapeType(layer.getShapeType());
//		lyrDef.setProjection(layer.getProjection());
//		lyrDef.setName(layer.getName());
//			lyrDef.setFieldsDesc(layer.getRecordset().getFieldsDescription());
//		return lyrDef;
//	}


	public static String getLastMessage(Throwable ex) {
        
        Throwable p = ex;
        while (p.getCause() != null && p.getCause() != p) {
            p = p.getCause();
        }
        return p.getMessage();
    }
}
