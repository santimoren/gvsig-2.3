package org.gvsig.editing;

public class CancelException extends RuntimeException {

	public CancelException(String msg) {
		super(msg);
	}
}
