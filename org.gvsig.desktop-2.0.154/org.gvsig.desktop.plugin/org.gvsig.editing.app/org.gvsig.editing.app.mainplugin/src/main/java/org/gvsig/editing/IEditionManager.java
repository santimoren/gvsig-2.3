/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.layers.ILayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.exceptions.CancelEditingLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.tools.observer.Observable;

public interface IEditionManager  {

    
    public static int CANCEL_EDITING = 0;
    public static int ACCEPT_EDITING = 1;
    public static int CONTINUE_EDITING = 2;

	/**
	 * @param lyr
	 * @return
	 */
	public ILayerEdited getLayerEdited(FLayer lyr);

	public void editLayer(FLyrVect lv, DefaultViewPanel vista)
			throws DataException;

	/**
	 * @return Returns the activeLayerEdited. Null if there isn't any active AND
	 *         edited
	 */
	public ILayerEdited getActiveLayerEdited();

	/**
	 * @return Returns the mapCtrl.
	 */
	public MapControl getMapControl();

	/**
	 * @param mapCtrl
	 *            The mapCtrl to set.
	 */
	public void setMapControl(MapControl mapCtrl);

	public Feature insertGeometry(Geometry geometry);
	
	public Feature insertFeature(Feature feature);
	
	public Feature insertGeometry(Geometry geometry, Feature feature) ;
	
	public Feature insertAndSelectGeometry(String toolName, Geometry geometry);
	
	public EditableFeature updateGeometry(FeatureStore store, Feature feature, Geometry geometry);
	
    public EditableFeature updateFeature(FeatureStore store, Feature feature);
    
    public void removeFeature(FeatureStore store, Feature feature);
    
    public void stopEditLayer(DefaultViewPanel view, FLyrVect lv, int mode)
        throws CancelEditingLayerException, ReadException;

}