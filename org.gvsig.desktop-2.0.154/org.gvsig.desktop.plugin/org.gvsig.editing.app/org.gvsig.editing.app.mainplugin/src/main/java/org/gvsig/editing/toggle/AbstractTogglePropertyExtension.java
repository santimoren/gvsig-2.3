/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.toggle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.EditionUtilities;
import org.gvsig.fmap.mapcontrol.MapControl;


/**
 * @author jldominguez
 *
 */
public abstract class AbstractTogglePropertyExtension extends Extension {

    private static Logger logger =
        LoggerFactory.getLogger(AbstractTogglePropertyExtension.class);
    
    public void initialize() {
    }

    public void execute(String actionCommand) {
        
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (!(v instanceof org.gvsig.app.project.documents.view.gui.DefaultViewPanel)) {
            logger.info("Executing toggle extension when window is not view (?)");
            return;
        }
        
        if (actionCommand.compareTo(getActionCommand()) != 0) {
            logger.info("Executing toggle extension. Unexpected command: " + actionCommand
                + ". Expected: " + getActionCommand());
            return;
        }
            
        MapControl mc=((DefaultViewPanel)v).getMapControl();
        doToggleValue(mc);
    }


    public boolean isEnabled() {
        return isVisible();
    }

    public boolean isVisible() {
        return optionMustBeAvailable();
    }
    
    // ========================
    
    protected boolean optionMustBeAvailable() {
        return (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE);
    }
    
    protected abstract String getActionCommand();
    protected abstract void doToggleValue(MapControl mc);

}
