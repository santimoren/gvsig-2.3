/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.project.documents.view.legend.edition.gui;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.gvsig.editing.fmap.rendering.EditionManagerLegend;




/**
 * Cell Editor de iconos. Controla los eventos de edici�n que se realicen
 * sobre la columna de iconos.
 *
 * @author Vicente Caballero Navarro
 */
public class BlockedCellEditor extends IconOptionCellEditor implements TableCellEditor {
	public BlockedCellEditor(EditionManagerLegend el,JTable tab,ImageIcon sel, ImageIcon notSel) {
		super(el,tab,sel,notSel);
		addMouseListener(new MouseListener(){

			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount()==2){
					int index=table.getSelectedRow();
					if (eml.isBlocked(index)) {
						setIcon(getNotSel());
						eml.setBlocked(index,false);

					}else {
						eml.setBlocked(index,true);
						setIcon(getSel());
					}
					stopCellEditing();
//				}
				table.repaint();
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
			}

			public void mouseReleased(MouseEvent e) {
			}

		});
	}

	//Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(JTable table, Object value,
		boolean isSelected, int row, int column) {
//		if (table.getSelectedRow()==row)
//		setSelected(true);
//	else
		setSelected(isSelected);
		if (eml.isBlocked(row)) {
			setIcon(getSel());
		}else {
			setIcon(getNotSel());
		}
		return this;
	}
}
