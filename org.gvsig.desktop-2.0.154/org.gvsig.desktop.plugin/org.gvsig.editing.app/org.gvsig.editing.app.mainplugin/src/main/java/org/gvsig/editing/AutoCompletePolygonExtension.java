/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.gui.cad.tools.AutoCompletePolygon;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;


public class AutoCompletePolygonExtension extends Extension {
	private DefaultViewPanel view;

	private MapControl mapControl;

	private AutoCompletePolygon theTool;

	public void initialize() {
		theTool = new AutoCompletePolygon();
		CADExtension.addCADTool("_autocompletepolygon", theTool);
		registerIcons();
	}
	private void registerIcons(){
		IconThemeHelper.registerIcon("action", "layer-insert-autopolygon", this);
	}
	public void execute(String actionCommand) {
		CADExtension.initFocus();
		if (actionCommand.equals("layer-insert-autopolygon")) {
			CADExtension.setCADTool("_autocompletepolygon", true);
			CADExtension.getEditionManager().setMapControl(mapControl);
		}
		CADExtension.getCADToolAdapter().configureMenu();

	}

	public boolean isEnabled() {
		try {
			if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE) {
				view = (DefaultViewPanel) PluginServices.getMDIManager().getActiveWindow();
				mapControl = view.getMapControl();
				if (CADExtension.getEditionManager().getActiveLayerEdited() == null)
					return false;
				FLyrVect lv = (FLyrVect) CADExtension.getEditionManager()
						.getActiveLayerEdited().getLayer();
				if (theTool.isApplicable(lv.getShapeType())) {
					return true;
				}
			}
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		}
		return false;
	}

	public boolean isVisible() {
		if (EditionUtilities.getEditionStatus() == EditionUtilities.EDITION_STATUS_ONE_VECTORIAL_LAYER_ACTIVE_AND_EDITABLE)
			return true;
		return false;
	}

}