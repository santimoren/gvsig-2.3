/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;


/**
 * Cuando un tema se pone en edici�n se le debe asociar
 * un listener de este tipo, que se dispar� cuando se produzca
 * un evento de edici�n (borrado, modificaci�n,... sobre la capa.
 *
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class EditionChangeManager implements Observer{
	private FLayer fLayer = null;

	/**
	 * Constructor
	 * @param fLayer
	 * Tema que se est� editando
	 */
	public EditionChangeManager(FLayer fLayer){
		this.fLayer = fLayer;
	}
	/*
	 *  (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.edition.IEditionListener#afterRowEditEvent(com.iver.cit.gvsig.fmap.edition.AfterRowEditEvent)
	 */
//	public void afterRowEditEvent(IRow feat, AfterRowEditEvent e) {
//		IWindow[] views = PluginServices.getMDIManager().getAllWindows();
//
//		for (int i=0 ; i<views.length ; i++){
//			if (views[i] instanceof Table){
////				Table table=(Table)views[i];
//				///VCN Creo que no hace falta refrescar la tabla aqu�
////				if (table.getModel().getAssociatedTable()!=null && table.getModel().getAssociatedTable().equals(fLayer))
////					table.refresh();
//			}else if (views[i] instanceof com.iver.cit.gvsig.project.documents.view.gui.View){
//				com.iver.cit.gvsig.project.documents.view.gui.View view=(com.iver.cit.gvsig.project.documents.view.gui.View)views[i];
//
//				if (e.getChangeType() == EditionEvent.CHANGE_TYPE_ADD) {
//					// No redraw, just image paint
//					view.getMapControl().repaint();
//				}else if (e.getChangeType() == EditionEvent.CHANGE_TYPE_DELETE){
//					EditionManager em=CADExtension.getEditionManager();
//					if (em.getActiveLayerEdited()!=null){
//						VectorialLayerEdited vle=(VectorialLayerEdited)em.getActiveLayerEdited();
//						try {
//							vle.clearSelection(false);
//						} catch (ReadException e1) {
//							NotificationManager.addError(e1);
//						}
//					}
//				}else{
//					fLayer.setDirty(true);
//					view.getMapControl().rePaintDirtyLayers();
//				}
//
//				/* FLayers layers=view.getMapControl().getMapContext().getLayers();
//				for (int j=0;j<layers.getLayersCount();j++){
//					if (layers.getLayer(j).equals(fLayer)){
//						view.repaintMap();
//					}
//				} */
//			}
//		}
//
//	}
	public void update(Observable observable, Object notification) {
		FeatureStoreNotification dfsn=(FeatureStoreNotification)notification;
		String type=dfsn.getType();
		IWindow[] views = PluginServices.getMDIManager().getAllWindows();

		for (int i=0 ; i<views.length ; i++){
			if (views[i] instanceof org.gvsig.app.project.documents.view.gui.DefaultViewPanel){
				org.gvsig.app.project.documents.view.gui.DefaultViewPanel view=(org.gvsig.app.project.documents.view.gui.DefaultViewPanel)views[i];
				if (type.equals(FeatureStoreNotification.AFTER_DELETE)){
					IEditionManager em = EditionLocator.getEditionManager();
					if (em.getActiveLayerEdited()!=null){
						VectorialLayerEdited vle=(VectorialLayerEdited)em.getActiveLayerEdited();
						try {
							vle.clearSelection();
						} catch (DataException e1) {
							NotificationManager.addError(e1);
						}
					}
				}
				if (type.equals(FeatureStoreNotification.AFTER_INSERT)){
					view.getMapControl().repaint();
				}
				if (type.equals(FeatureStoreNotification.AFTER_UPDATE)
				    || type.equals(FeatureStoreNotification.AFTER_REDO)
                    || type.equals(FeatureStoreNotification.AFTER_UNDO)) {
					view.getMapControl().rePaintDirtyLayers();
				}
			}
		}
	}

}
