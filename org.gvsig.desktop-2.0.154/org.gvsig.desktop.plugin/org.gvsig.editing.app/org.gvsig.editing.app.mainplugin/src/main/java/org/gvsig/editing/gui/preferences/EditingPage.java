/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* CVS MESSAGES:
 *
 * $Id: EditingPage.java 36005 2011-08-08 11:15:31Z jpiera $
 * $Log$
 * Revision 1.4  2007-09-19 16:01:06  jaume
 * removed unnecessary imports
 *
 * Revision 1.3  2007/05/02 16:55:13  caballero
 * Editing colors
 *
 * Revision 1.1.2.2  2007/05/02 15:49:56  caballero
 * Editing colors
 *
 * Revision 1.1.2.1  2007/05/02 07:50:56  caballero
 * Editing colors
 *
 * Revision 1.4.4.9  2007/02/16 10:30:36  caballero
 * factor 0 incorrecto
 *
 * Revision 1.4.4.8  2006/11/22 01:45:47  luisw2
 * Recuperados cambios de la RC2 que normalizan la gesti�n de CRSs
 *
 * Revision 1.4.4.7  2006/11/15 00:08:09  jjdelcerro
 * *** empty log message ***
 *
 * Revision 1.21  2006/11/08 10:57:55  jaume
 * remove unecessary imports
 *
 * Revision 1.20  2006/10/25 08:34:06  jmvivo
 * LLamado al PluginServices.getText para las unidades de medida del los combo
 *
 * Revision 1.19  2006/10/04 07:23:31  jaume
 * refactored ambiguous methods and field names and added some more features for preference pages
 *
 * Revision 1.18  2006/10/03 09:52:38  jaume
 * restores to meters
 *
 * Revision 1.17  2006/10/03 09:19:12  jaume
 * *** empty log message ***
 *
 * Revision 1.16  2006/10/03 07:26:08  jaume
 * *** empty log message ***
 *
 * Revision 1.15  2006/10/02 15:30:29  jaume
 * *** empty log message ***
 *
 * Revision 1.14  2006/10/02 13:52:34  jaume
 * organize impots
 *
 * Revision 1.13  2006/10/02 13:38:23  jaume
 * *** empty log message ***
 *
 * Revision 1.12  2006/10/02 11:49:23  jaume
 * *** empty log message ***
 *
 * Revision 1.11  2006/09/28 12:04:21  jaume
 * default selection color now configurable
 *
 * Revision 1.10  2006/09/25 10:17:15  caballero
 * Projection
 *
 * Revision 1.9  2006/09/15 10:41:30  caballero
 * extensibilidad de documentos
 *
 * Revision 1.8  2006/09/14 15:43:48  jaume
 * *** empty log message ***
 *
 * Revision 1.7  2006/09/14 15:42:38  jaume
 * *** empty log message ***
 *
 * Revision 1.6  2006/09/14 06:57:18  jaume
 * *** empty log message ***
 *
 * Revision 1.5  2006/09/12 15:56:50  jaume
 * Default Projection now customizable
 *
 * Revision 1.4  2006/08/29 07:21:08  cesar
 * Rename com.iver.cit.gvsig.fmap.Fmap class to com.iver.cit.gvsig.fmap.MapContext
 *
 * Revision 1.3  2006/08/22 12:30:59  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/08/22 07:36:04  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2006/08/04 11:41:05  caballero
 * poder especificar el zoom a aplicar en las vistas
 *
 * Revision 1.3  2006/07/31 10:02:31  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/06/13 07:43:08  fjp
 * Ajustes sobre los cuadros de dialogos de preferencias
 *
 * Revision 1.1  2006/06/12 16:04:28  caballero
 * Preferencias
 *
 * Revision 1.11  2006/06/06 10:26:31  jaume
 * *** empty log message ***
 *
 * Revision 1.10  2006/06/05 17:07:17  jaume
 * *** empty log message ***
 *
 * Revision 1.9  2006/06/05 17:00:44  jaume
 * *** empty log message ***
 *
 * Revision 1.8  2006/06/05 16:57:59  jaume
 * *** empty log message ***
 *
 * Revision 1.7  2006/06/05 14:45:06  jaume
 * *** empty log message ***
 *
 * Revision 1.6  2006/06/05 11:00:09  jaume
 * *** empty log message ***
 *
 * Revision 1.5  2006/06/05 10:39:02  jaume
 * *** empty log message ***
 *
 * Revision 1.4  2006/06/05 10:13:40  jaume
 * *** empty log message ***
 *
 * Revision 1.3  2006/06/05 10:06:08  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/06/05 09:51:56  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2006/06/02 10:50:18  jaume
 * *** empty log message ***
 *
 *
 */
package org.gvsig.editing.gui.preferences;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.editing.CADExtension;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ILineSymbol;
import org.gvsig.tools.dynobject.DynObject;



/**
 *  Editing configuration page.
 *  <b><b>
 *  Here the user can establish what settings wants to use by default regarding to
 *  editing.
 *
 *
 * @author Vicente Caballero Navarro
 */
public class EditingPage extends AbstractPreferencePage {
    private static final long serialVersionUID = -6126280933781398092L;

    public static String DEFAULT_HANDLER_SYMBOL = "default_editing_handler_symbol"; 
    public static String DEFAULT_AXIS_REFERENCES_SYMBOL = "default_editing_axis_references_symbol";
    public static String DEFAULT_RECTANGLE_SELECTION_SYMBOL = "default_editing_rectangle_selection_symbol";

    protected String id;
    private ImageIcon icon;   
    private ColorChooserPanel jccDefaultAxisReferencesColor;
    private ColorChooserPanel jccDefaultGeometrySelectionColor;
    private ColorChooserPanel jccDefaultHandlerColor;

    private boolean panelStarted = false; 
    private JSlider jsDefaultAxisReferencesAlpha;
    private JSlider jsDefaultGeometrySelectionAlpha;
    private JSlider jsDefaultHandlerAlpha;

    private SymbolManager symbolManager = MapContextLocator.getSymbolManager();
    private MapControlManager mapControlManager = MapControlLocator.getMapControlManager();

    /**
     * Creates a new panel containing View preferences settings.
     *
     */
    public EditingPage() {
        super();
        id = this.getClass().getName();
        icon = IconThemeHelper.getImageIcon("editing-properties");
    }

    public void initializeValues() {
        if (!panelStarted) getPanel();

        //The values has beed retrieved in the CADExtension.

        // Default axis references color
        ISymbol axisReferenceSymbol = mapControlManager.getAxisReferenceSymbol();
        jccDefaultAxisReferencesColor.setColor(axisReferenceSymbol.getColor());
        jccDefaultAxisReferencesColor.setAlpha(axisReferenceSymbol.getColor().getAlpha());
        jsDefaultAxisReferencesAlpha.setValue(axisReferenceSymbol.getColor().getAlpha());

        // Default geometry selection color
        ISymbol geometrySelectionSymbol = mapControlManager.getGeometrySelectionSymbol();
        jccDefaultGeometrySelectionColor.setColor(geometrySelectionSymbol.getColor());
        jccDefaultGeometrySelectionColor.setAlpha(geometrySelectionSymbol.getColor().getAlpha());
        jsDefaultGeometrySelectionAlpha.setValue(geometrySelectionSymbol.getColor().getAlpha());

        // Default handler color
        ISymbol handlerSymbol = mapControlManager.getHandlerSymbol();
        jccDefaultHandlerColor.setColor(((IFillSymbol)handlerSymbol).getColor());
        jccDefaultHandlerColor.setAlpha(((IFillSymbol)handlerSymbol).getColor().getAlpha());
        jsDefaultHandlerAlpha.setValue(geometrySelectionSymbol.getColor().getAlpha());

    }

    public String getID() {
        return id;
    }

    public String getTitle() {
        return PluginServices.getText(this, "editing");
    }

    public JPanel getPanel() {
        if (panelStarted) return this;
        panelStarted = true;

        // just a separator
        addComponent(new JLabel(" "));

        addComponent(new JLabel(PluginServices.getText(this,"change_the_editing_colors")));


        // default selection color chooser
        JPanel modifyPanel = new JPanel();
        modifyPanel.setBorder(new TitledBorder(PluginServices.getText(this, "options.editing.default_axis_references_color")));
        modifyPanel.setLayout(new GridBagLayout());
        modifyPanel.add(new JLabel(PluginServices.getText(this,"fill")));
        modifyPanel.add(jccDefaultAxisReferencesColor = new ColorChooserPanel());

        //		JPanel alphaModifyPanel= new JPanel();
        modifyPanel.add(new JLabel(PluginServices.getText(this,"alpha")));
        modifyPanel.add(jsDefaultAxisReferencesAlpha = new JSlider(0,255));
        jsDefaultAxisReferencesAlpha.setPreferredSize(new Dimension(100,30));

        jsDefaultAxisReferencesAlpha.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) {
                jccDefaultAxisReferencesColor.setAlpha(((JSlider)e.getSource()).getValue());

            }});

        addComponent(modifyPanel);
        addComponent(new JLabel(" "));

        // default drawing color chooser
        JPanel drawingPanel = new JPanel();
        drawingPanel.setBorder(new TitledBorder(PluginServices.getText(this, "options.editing.default_rectangle_selection_color")));
        drawingPanel.setLayout(new GridBagLayout());
        drawingPanel.add(new JLabel(PluginServices.getText(this,"fill")));
        drawingPanel.add(jccDefaultGeometrySelectionColor = new ColorChooserPanel());

        //		JPanel alphaDrawingPanel= new JPanel();
        drawingPanel.add(new JLabel(PluginServices.getText(this,"alpha")));
        drawingPanel.add(jsDefaultGeometrySelectionAlpha = new JSlider(0,255));
        jsDefaultGeometrySelectionAlpha.setPreferredSize(new Dimension(100,30));

        jsDefaultGeometrySelectionAlpha.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) {
                jccDefaultGeometrySelectionColor.setAlpha(((JSlider)e.getSource()).getValue());
            }});

        addComponent(drawingPanel);
        addComponent(new JLabel(" "));

        // default selection color chooser
        JPanel handlerPanel = new JPanel();
        handlerPanel.setBorder(new TitledBorder(PluginServices.getText(this, "options.editing.default_handler_color")));
        handlerPanel.setLayout(new GridBagLayout());
        handlerPanel.add(new JLabel(PluginServices.getText(this,"fill")));
        handlerPanel.add(jccDefaultHandlerColor = new ColorChooserPanel());      

        handlerPanel.add(new JLabel(PluginServices.getText(this,"alpha")));
        handlerPanel.add(jsDefaultHandlerAlpha = new JSlider(0,255));
        jsDefaultHandlerAlpha.setPreferredSize(new Dimension(100,30));

        jsDefaultHandlerAlpha.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e) {
                jccDefaultHandlerColor.setAlpha(((JSlider)e.getSource()).getValue());   
            }});

        addComponent(handlerPanel);
        addComponent(new JLabel(" "));

        initializeValues();
        return this;
    }

    public void storeValues() throws StoreException {
        Color axisReferencesColor, geometrySelectionColor, handlerColor, handlerOutLineColor;
        axisReferencesColor = jccDefaultAxisReferencesColor.getColor();
        geometrySelectionColor = jccDefaultGeometrySelectionColor.getColor();
        handlerColor = jccDefaultHandlerColor.getColor();       

        PluginsManager pluginsManager = PluginsLocator.getManager();
        DynObject dynObject = pluginsManager.getPlugin(CADExtension.class).getPluginProperties();

        //Symbol for the AxisReferences
        ISymbol axisReferencesSymbol =
            symbolManager.createSymbol(Geometry.TYPES.GEOMETRY, axisReferencesColor);
        if (axisReferencesSymbol instanceof IFillSymbol){
            ((IFillSymbol)axisReferencesSymbol).setFillColor(axisReferencesColor);
        }
        mapControlManager.setAxisReferenceSymbol(axisReferencesSymbol);
        dynObject.setDynValue(DEFAULT_AXIS_REFERENCES_SYMBOL, axisReferencesSymbol);

        //Symbol for the geometry used in the edition selection
        ISymbol geometrySelectionSymbol =
            symbolManager.createSymbol(Geometry.TYPES.GEOMETRY, geometrySelectionColor);
        if (geometrySelectionSymbol instanceof IFillSymbol){
            ((IFillSymbol)geometrySelectionSymbol).setFillColor(geometrySelectionColor);           
        }
        if (geometrySelectionSymbol instanceof ILineSymbol){
            ((ILineSymbol)geometrySelectionSymbol).setLineColor(geometrySelectionColor);           
        }
        mapControlManager.setGeometrySelectionSymbol(geometrySelectionSymbol);
        dynObject.setDynValue(DEFAULT_RECTANGLE_SELECTION_SYMBOL, geometrySelectionSymbol);

        //Symbol for the handlers
        ISymbol handlerSymbol =
            symbolManager.createSymbol(Geometry.TYPES.GEOMETRY, handlerColor);       
        if (axisReferencesSymbol instanceof IFillSymbol){
            ((IFillSymbol)handlerSymbol).setFillColor(handlerColor);
        }       
        mapControlManager.setHandlerSymbol(handlerSymbol);
        dynObject.setDynValue(DEFAULT_HANDLER_SYMBOL, handlerSymbol);
    }

    public void initializeDefaults() {    
        jccDefaultAxisReferencesColor.setColor(new Color(100, 100, 100, 100));
        jsDefaultAxisReferencesAlpha.setValue(100);

        jccDefaultGeometrySelectionColor.setColor(new Color(255, 0,0, 100));
        jsDefaultGeometrySelectionAlpha.setValue(100);

        jccDefaultHandlerColor.setColor(new Color(255, 0,0, 100));       
        jsDefaultHandlerAlpha.setValue(100);
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public boolean isValueChanged() {
        return super.hasChanged();
    }

    public void setChangesApplied() {
        setChanged(false);
    }
}
