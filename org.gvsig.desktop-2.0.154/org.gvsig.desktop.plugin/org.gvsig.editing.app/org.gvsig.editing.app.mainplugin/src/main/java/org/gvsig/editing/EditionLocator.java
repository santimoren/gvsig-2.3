/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.editing.gui.cad.CADToolAdapter;
import org.gvsig.fmap.mapcontext.layers.FLayer;

public class EditionLocator {

	private static CADToolAdapter adapter=null;
	private static Map<ViewDocument,CADToolAdapter> adapters=new HashMap<ViewDocument,CADToolAdapter>();
	
    /**
     * Gets the CADToolAdapter of the view where the layer is
     * contained or null if the layer is not found
     * in any view
     *  
     * @param layer
     * @return CADToolAdapter of the layer
     */
	public static CADToolAdapter getCADToolAdapter(FLayer layer) {
		Project project = ProjectManager.getInstance().getCurrentProject();
		List<Document> docs = project.getDocuments();
		for( int i=0; i<docs.size(); i++ ) {
			Document doc = docs.get(i);
			if( doc instanceof ViewDocument ) {
				ViewDocument view = (ViewDocument)doc;
				if (view.getMapContext() == layer.getMapContext() ) {
					CADToolAdapter cta = adapters.get(doc);
					if ( cta == null ) {
						cta = new CADToolAdapter();
						adapters.put(view,cta);
					}
					return cta;
				}
			}
		}
		return null;
	}
	
	/**
     * Gets the CADToolAdapter of the view
     * or null if the view not found in the current project
     * 
     * @return CADToolAdapter of the view
     */
	public static CADToolAdapter getCADToolAdapter(ViewDocument view) {
		CADToolAdapter cta = adapters.get(view);
		if (cta == null) {
			cta = new CADToolAdapter();
			adapters.put(view, cta);
		}
		return cta;
	}

	/**
     * Gets the CADToolAdapter of the currently active view
     * or the CADToolAdapter of the view that was active more
     * recently (if there is not an active view)
     * or null if there never was an active view
     * 
     * @return CADToolAdapter of current view
     */
	public static CADToolAdapter getCADToolAdapter() {
		Project prj = ProjectManager.getInstance().getCurrentProject();
		
		if(prj == null)
			return null;
		
		ViewDocument view = (ViewDocument) prj.getActiveDocument(ViewDocument.class);
		if( view != null ) {
			adapter = adapters.get(view);
			if( adapter==null ) {
				adapter = new CADToolAdapter();
				adapters.put(view, adapter);
			}
			return adapter;
		}
		return adapter;
	}
	
	/**
	 * 
     * Gets the edition manager of the currently active view
     * or the edition manager of the view that was active more
     * recently (if there is not an active view)
     * or null if there never was an active view.
     * 
	 * @return
	 */
	public static IEditionManager getEditionManager() {
		CADToolAdapter cta=getCADToolAdapter();
		if (cta == null) {
		    return null;
		} else {
		    return cta.getEditionManager();
		}
		
	}
	
	

	/**
     * Gets the EditionManager of the view where the layer is
     * contained or null if the layer is not found
     * in any view
	 * 
	 * @param layer
	 * @return
	 */
    public static IEditionManager getEditionManager(FLayer layer) {
        CADToolAdapter cta=getCADToolAdapter(layer);
        if (cta == null) {
            return null;
        } else {
            return cta.getEditionManager();
        }
        
    }

	/**
     * Gets the EditionManager of the ViewDocument or null if the view is not found
     * in the project
	 * 
	 * @param viewDocument
	 * @return
	 */
    public static IEditionManager getEditionManager(ViewDocument view) {
        CADToolAdapter cta=getCADToolAdapter(view);
        if (cta == null) {
            return null;
        } else {
            return cta.getEditionManager();
        }
        
    }

}
