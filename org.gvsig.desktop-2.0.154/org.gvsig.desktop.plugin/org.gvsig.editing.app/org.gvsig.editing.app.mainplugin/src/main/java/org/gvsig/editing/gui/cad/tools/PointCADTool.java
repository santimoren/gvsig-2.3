/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;

import org.gvsig.andami.PluginServices;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.PointCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.PointCADToolContext.PointCADToolState;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * 
 * @author Vicente Caballero Navarro
 */
public class PointCADTool extends DefaultCADTool {

    protected PointCADToolContext _fsm;

    private static final int TYPE_MULTIPOINT = 1;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new PointCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        PointCADToolState actualState =
            (PointCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Point.FirstPoint")) {
            insertAndSelectGeometry(createPoint(x, y));
        }
    }

    @Override
    public Feature insertAndSelectGeometry(Geometry geometry) {
        if (getSupportedTypes()[TYPE_MULTIPOINT].isTypeOf(getGeometryType())) {
            MultiPoint multiPoint = createMultiPoint();
            multiPoint.addPoint((Point) geometry);
            return super.insertAndSelectGeometry(multiPoint);
        } else {
            return super.insertAndSelectGeometry(geometry);
        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        // Nothing to do
    }

    public void addValue(double d) {
        // Nothing to do
    }

    public String getName() {
        return PluginServices.getText(this, "point_");
    }

    public String toString() {
        return "_point";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { POINT, MULTIPOINT };
    }

}
