/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.IEditionManager;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.SplitGeometryCADToolContext;
import org.gvsig.editing.gui.cad.tools.split.SplitStrategy;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.ISimpleFillSymbol;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.locator.LocatorException;

import statemap.State;
import statemap.StateUndefinedException;

/**
 * CAD Tool which splits the selected geometries of a vectorial editing
 * layer with a digitized polyline.
 * 
 * 
 * @author Alvaro Zabala
 * 
 */
public class SplitGeometryCADTool extends DefaultCADTool {

    private static Logger logger =
        LoggerFactory.getLogger(SplitGeometryCADTool.class);
    /**
     * String representation of this tool (used for example to active the tool
     * in mapcontrol)
     */
    public static final String SPLIT_GEOMETRY_ACTION_NAME = "layer-modify-split";
    public static final String SPLIT_GEOMETRY_TOOL_NAME = "_split_geometry";

    /**
     * finite state machine for this CAD tool
     */
    protected SplitGeometryCADToolContext _fsm;

    /**
     * Flag to mark if the digitized line has been finished.
     */
    protected boolean digitizingFinished = false;

    /**
     * Collection of digitized geometries
     */
    protected List<Point2D> clickedPoints;

    /**
     * used to draw helping rectangle (bbox)
     */
    protected ISymbol rectSymbol = null;
    /**
     * Initialization method.
     */
    public void init() {
        digitizingFinished = false;
        _fsm = new SplitGeometryCADToolContext(this);
        setNextTool(SplitGeometryCADTool.SPLIT_GEOMETRY_TOOL_NAME);
    }

    public boolean isDigitizingFinished() {
        return digitizingFinished;
    }

    public String toString() {
        return SplitGeometryCADTool.SPLIT_GEOMETRY_TOOL_NAME;
    }

    public void finishDigitizedLine() {
    }

    public Coordinate[] getPoint2DAsCoordinates(Point2D[] point2d) {
        Coordinate[] solution = new Coordinate[point2d.length];
        for (int i = 0; i < point2d.length; i++) {
            solution[i] = new Coordinate(point2d[i].getX(), point2d[i].getY());
        }
        return solution;
    }

    public void splitSelectedGeometryWithDigitizedLine() {
        
        IEditionManager ed_man = this.getEditionManager();
        
        // comprobar bucle, probar interseccion?
        Point2D[] clickedPts = new Point2D[this.clickedPoints.size()];
        clickedPoints.toArray(clickedPts);
        Coordinate[] digitizedCoords = getPoint2DAsCoordinates(clickedPts);
        LineString splittingLs =
            new GeometryFactory(new PrecisionModel(10000))
                .createLineString(digitizedCoords);
        DisposableIterator selected = null;
        try {
            VectorialLayerEdited vle = getVLE();
            FeatureStore store = vle.getFeatureStore();
            selected = store.getFeatureSelection().iterator();

            getCadToolAdapter().getMapControl().getMapContext()
                .beginAtomicEvent();
            store.beginEditingGroup(getName());
            while (selected.hasNext()) {
                Feature feature = (Feature) selected.next();
                org.gvsig.fmap.geom.Geometry ig = feature.getDefaultGeometry();
                Geometry jtsGeo = (Geometry)ig.invokeOperation("toJTS", null);
                if (jtsGeo == null)
                    return;
                Geometry splitGeo = SplitStrategy.splitOp(jtsGeo, splittingLs);
                if (splitGeo instanceof GeometryCollection
                    && ((GeometryCollection) splitGeo).getNumGeometries() > 1) {

                    GeometryCollection gc = (GeometryCollection) splitGeo;
                    ArrayList<Geometry> geoms0 = new ArrayList<Geometry>();
                    ArrayList<Geometry> geoms1 = new ArrayList<Geometry>();
                    if (gc.getNumGeometries() > 2) {
                        Geometry[] splitGroups =
                            createSplitGroups(jtsGeo, splittingLs);
                        
                        if (splitGroups == null) {
                            
                            String _tit = Messages.getText("split_geometry");
                            String _msg = Messages.getText("_Split_line_must_start_and_end_outside_bbox");
                            Component parent = getCurrentWindow();
                            JOptionPane.showMessageDialog(
                                parent,
                                _msg,
                                _tit,
                                JOptionPane.WARNING_MESSAGE);
                            // cancel splitting
                            break;
                        }
                        
                        if (splitGroups.length == 0) {
                            continue;
                        }

                        for (int j = 0; j < gc.getNumGeometries(); j++) {
                            Geometry g = gc.getGeometryN(j);
                            
                            if (isRatherInside(g, splitGroups[0])) {
                                geoms0.add(g);
                            } else {
                                geoms1.add(g);
                            }
                        }
                    } else {
                        if (gc.getNumGeometries() == 2) {
                            geoms0.add(gc.getGeometryN(0));
                            geoms1.add(gc.getGeometryN(1));
                        } else {
                            continue;
                        }
                    }
                    GeometryCollection gc0 =
                        createMulti(geoms0, gc.getFactory());

                    // for(int j = 0; j < gc.getNumGeometries(); j++){
                    // Geometry g = gc.getGeometryN(j);
                    org.gvsig.fmap.geom.Geometry fmapGeo =
                        UtilFunctions.jtsToGeometry(gc0);

                    // if (j==0){
                    EditableFeature eFeature = feature.getEditable();
                    eFeature.setGeometry(store.getDefaultFeatureType()
                        .getDefaultGeometryAttributeName(), fmapGeo);
                    
                    ed_man.updateFeature(store, eFeature);

                    // }else{
                    GeometryCollection gc1 =
                        createMulti(geoms1, gc.getFactory());
                    fmapGeo = UtilFunctions.jtsToGeometry(gc1);

                    super.insertGeometry(fmapGeo,feature);
//                    EditableFeature newFeature =
//                        store.createNewFeature(store.getDefaultFeatureType(),
//                            feature);
//                    newFeature.setGeometry(store.getDefaultFeatureType()
//                        .getDefaultGeometryAttributeName(), fmapGeo);
//                    store.insert(newFeature);
//                    
//                    SpatialCache spatialCache =
//                        ((FLyrVect) vle.getLayer()).getSpatialCache();
//                    Envelope envelope = fmapGeo.getEnvelope();
//                    spatialCache.insert(envelope, fmapGeo);

                    // }
                    // }//for j
                }// if splitGeo

            }

            store.endEditingGroup();

            getCadToolAdapter().getMapControl().getMapContext()
                .endAtomicEvent();
        } catch (Exception ex) {
            PluginServices.getLogger().error("Error splitting geom", ex);
        } finally {
            DisposeUtils.dispose(selected);
        }
    }

    /**
     * @return
     */
    private Component getCurrentWindow() {
        
        IWindow iw = PluginServices.getMDIManager().getActiveWindow();
        if (iw instanceof Component) {
            return (Component) iw;
        } else {
            return null;
        }
    }

    private GeometryCollection createMulti(ArrayList<Geometry> geoms,
        GeometryFactory factory) {
        if (geoms.size() == 0)
            return null;
        if (geoms.get(0) instanceof Polygon) {
            return new MultiPolygon((Polygon[]) geoms.toArray(new Polygon[0]),
                factory);
        } else
            if (geoms.get(0) instanceof LineString) {
                return new MultiLineString(
                    (LineString[]) geoms.toArray(new LineString[0]), factory);
            } else
                if (geoms.get(0) instanceof Point) {
                    return new MultiPoint(
                        (Point[]) geoms.toArray(new Point[0]), factory);
                }
        return null;
    }

    private Geometry[] createSplitGroups(Geometry splitGeo,
        LineString splittingLs) {
        try {
            Geometry[] geomsJTS = new Geometry[2];
            Geometry r = splitGeo.getEnvelope();
            Geometry splitG = SplitStrategy.splitOp(r, splittingLs);
            if (splitG instanceof GeometryCollection
                && ((GeometryCollection) splitG).getNumGeometries() > 1) {
                GeometryCollection gc = (GeometryCollection) splitG;
                for (int j = 0; j < gc.getNumGeometries(); j++) {
                    geomsJTS[j] = gc.getGeometryN(j);
                }
            }
            return geomsJTS;
        } catch (Exception e) {
            logger.info("Warning: Found split line which does not leave bbox. User will be prompted.");
        }
        return null;
    }

    public void end() {
        getCadToolAdapter().refreshEditedLayer();
        init();
    }

    public void addOption(String s) {
        State actualState = _fsm.getPreviousState();
        String status = actualState.getName();
        if (s.equals(PluginServices.getText(this, "cancel"))) {
            init();
            return;
        }
        if (status.equals("TopologicalEdition.FirstPoint")) {
            return;
        }
        init();

    }

    public void addPoint(double x, double y, InputEvent event) {

        State actualState = _fsm.getPreviousState();
        String status = actualState.getName();
        if (status.equals("SplitGeometry.FirstPoint")) {
            clickedPoints = new ArrayList<Point2D>();
            clickedPoints.add(new Point2D.Double(x, y));
        } else
            if (status.equals("SplitGeometry.DigitizingLine")) {
                clickedPoints.add(new Point2D.Double(x, y));
                if (event != null && ((MouseEvent) event).getClickCount() == 2) {
                    digitizingFinished = true;
                    finishDigitizedLine();
                    splitSelectedGeometryWithDigitizedLine();
                    end();
                }
            }
    }

    public void addValue(double d) {
    }

    /**
     * Draws a polyline with the clicked digitized points in the specified
     * graphics.
     * 
     * @param g2
     *            graphics on to draw the polyline
     * @param x
     *            last x mouse pointer position
     * @param y
     *            last y mouse pointer position
     */
    protected void drawPolyLine(MapControlDrawer renderer, double x, double y) {
        GeneralPathX gpx =
            new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, clickedPoints.size());
        Point2D firstPoint = clickedPoints.get(0);
        gpx.moveTo(firstPoint.getX(), firstPoint.getY());
        for (int i = 1; i < clickedPoints.size(); i++) {
            Point2D clickedPoint = clickedPoints.get(i);
            gpx.lineTo(clickedPoint.getX(), clickedPoint.getY());

        }
        gpx.lineTo(x, y);
        org.gvsig.fmap.geom.Geometry geom;
        try {
            geom =
                GeometryLocator.getGeometryManager().createCurve(gpx,
                    SUBTYPES.GEOM2D);
            renderer.draw(geom, mapControlManager.getGeometrySelectionSymbol());
        } catch (LocatorException e) {
            e.printStackTrace();
        } catch (CreateGeometryException e) {
            e.printStackTrace();
        }
    }

    private void drawRectangleOfSplit(MapControlDrawer renderer)
        throws GeometryOperationNotSupportedException,
        GeometryOperationException, DataException,
        CreateGeometryException {
        VectorialLayerEdited vle = getVLE();
        FeatureStore store = vle.getFeatureStore();
        DisposableIterator selected = null;

        try {
            selected = store.getFeatureSelection().iterator();
            while (selected.hasNext()) {
                Feature feature = (Feature) selected.next();
                org.gvsig.fmap.geom.Geometry ig = feature.getDefaultGeometry();
                Geometry jtsG = (Geometry)ig.invokeOperation("toJTS", null);
                if (jtsG != null && jtsG instanceof GeometryCollection
                    && jtsG.getNumGeometries() > 1) {
                    
                    /*
                    org.gvsig.fmap.geom.Geometry r =
                        ig.getEnvelope().getGeometry();
                        */
                    // get perimeter of envelope to prevent
                    // opaque rectangle
                    org.gvsig.fmap.geom.Geometry geom =
                        GeometryLocator.getGeometryManager().createCurve(
                            ig.getEnvelope().getGeometry().getGeneralPath(),
                            SUBTYPES.GEOM2D);
                    
                    renderer.draw(geom, getRectangleOfSplitSymbol());
                }
            }
        } finally {
            DisposeUtils.dispose(selected);
        }
    }

    /**
     * @return
     */
    private ISymbol getRectangleOfSplitSymbol() {
        
        if (rectSymbol == null) {
            SymbologyManager sm = SymbologyLocator.getSymbologyManager();
            ISimpleFillSymbol resp = sm.createSimpleFillSymbol();
            resp.setColor(Color.RED);
            resp.setFillColor(new Color(0,0,0, 100));
            rectSymbol = resp;
        }
        return rectSymbol;
    }

    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        
        // decide whether user line must be drawn
        boolean draw_user_poly_line = false;
        try {
            State currentState = _fsm.getState();
            if (currentState != null) {
                String status = currentState.getName();
                if (status != null && status.equals("SplitGeometry.DigitizingLine")) {
                    draw_user_poly_line = true;
                }
            }        
        } catch (StateUndefinedException sue) {
            // this happens when state is null
            // because the line has been finished and we must not draw it
            draw_user_poly_line = false;
        }
        
        // =======================================================
        
        try {
            drawRectangleOfSplit(renderer);
            
            // possibly draw splitting line
            if (draw_user_poly_line) {
                drawPolyLine(renderer, x, y);
            }

            // draw selection
            Image imgSel = getVLE().getSelectionImage();
            renderer.drawImage(imgSel, 0, 0);
        } catch (Exception e) {
            
            logger.info("Error while drawing split tool.", e);
            ApplicationLocator.getManager().message(
                Messages.getText("_Drawing_error") + e.getMessage(),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    public String getName() {
        return PluginServices.getText(this, "split_geometry_shell");
    }

    public void transition(double x, double y, InputEvent event) {
        try {
            _fsm.addPoint(x, y, event);
        } catch (Exception e) {
            init();
        }

    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }

    /**
     * This method decides if geometrya  is inside geometry b 
     * by more than 50%. This is useful because it will be
     * used to assign each geometry to one of two possible containers
     * 
     * @param a
     * @param b must be a multi(polygon)
     * @return
     */
    private static boolean isRatherInside(Geometry a, Geometry b) {
        
        if (a instanceof Polygon || a instanceof MultiPolygon) {
            return polygonIsRatherInside(a, b);
        } else {
            if (a instanceof LineString || a instanceof MultiLineString) {
                return lineIsRatherInside(a, b);
            } else {
                if (a instanceof MultiPoint) {
                    
                    MultiPoint mp = (MultiPoint) a;
                    int n = mp.getNumPoints();
                    int inn = 0;
                    Geometry itemg = null;
                    for (int i=0; i<n; i++) {
                        itemg = mp.getGeometryN(i);
                        if (isRatherInside(itemg, b)) {
                            inn++;
                        }
                    }
                    return inn > (n/2);
                } else {
                    // last case: should be a point
                    return b.contains(a);
                }
            }
        }
        
    }

    
    
    /**
     * This method decides if a polygon is inside another
     * by more than 50%. This is useful because it will be
     * used to assign each polygon to one of two possible containers
     * 
     * @param a
     * @param b
     * @return whether a is 'rather' contained by b
     */
    private static boolean polygonIsRatherInside(Geometry a, Geometry b) {
        
        if (a == null || b == null || a.isEmpty() || b.isEmpty()) {
            return false;
        }
        
        double area_a = a.getArea();
        
        if (area_a == 0) {
            return false;
        }
        
        Geometry a_inters_b = null;
        
        a_inters_b = a.intersection(b);
        if (a_inters_b == null || a_inters_b.isEmpty()) {
            return false;
        }

        double area_aib = a_inters_b.getArea();
        
        if (area_aib == 0) {
            return false;
        }
        
        return area_aib > (0.5d * area_a);
    }
    
    /**
     * 
     * @param a assumed to be a (multi)linestring
     * @param b the (possibly) containing polygon
     * @return
     */
    private static boolean lineIsRatherInside(Geometry a, Geometry b) {
        
        if (a == null || b == null || a.isEmpty() || b.isEmpty()) {
            return false;
        }
        
        double len_a = a.getLength();
        
        if (len_a == 0) {
            return false;
        }
        
        Geometry a_inters_b = null;
        
        a_inters_b = a.intersection(b);
        if (a_inters_b == null || a_inters_b.isEmpty()) {
            return false;
        }

        double len_aib = a_inters_b.getLength();
        
        if (len_aib == 0) {
            return false;
        }
        
        return len_aib > (0.5d * len_a);
    }
}
