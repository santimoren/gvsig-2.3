/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.layers;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.EmptyStackException;

import javax.swing.JOptionPane;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.extension.ViewCommandStackExtension;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.EditionChangeManager;
import org.gvsig.editing.StartEditing;
import org.gvsig.editing.gui.cad.CADTool;
import org.gvsig.editing.gui.cad.tools.SelectionCADTool;
import org.gvsig.editing.gui.cad.tools.select.SelectRowPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayerDrawEvent;
import org.gvsig.fmap.mapcontext.layers.LayerDrawingListener;
import org.gvsig.fmap.mapcontext.layers.LayerEvent;
import org.gvsig.fmap.mapcontext.layers.vectorial.ContainsGeometryEvaluator;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsGeometryEvaluator;
import org.gvsig.fmap.mapcontext.layers.vectorial.OutGeometryEvaluator;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.MapControlManager;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;

public class VectorialLayerEdited extends DefaultLayerEdited implements
LayerDrawingListener, Observer {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger LOG = LoggerFactory.getLogger(VectorialLayerEdited.class);
	private static final MapControlManager MAP_CONTROL_MANAGER = MapControlLocator.getMapControlManager();
	private ArrayList selectedHandler = new ArrayList();
	private Point2D lastPoint;
	private Point2D firstPoint;
	private CADTool cadtool = null;

	private ILegend legend;
	private Image imageSelection;
	private Image imageHandlers;
	private EditionChangeManager echm=null;
	private MapControl mapControl = null;

	public VectorialLayerEdited(FLayer lyr, MapControl mapControl) {
		super(lyr);
		this.mapControl = mapControl;
		lyr.getMapContext().addLayerDrawingListener(this);
		// Por defecto, siempre hacemos snapping sobre la capa en edici�n.
		lyr.getMapContext().getLayersToSnap().add(lyr);
		((FLyrVect) lyr).getFeatureStore().addObserver(this);
	}

	public ArrayList getSelectedHandler() {
		return selectedHandler;
	}

	public void clearSelection() throws DataException {
		if (getFeatureStore() == null) {
			return;
		}
		FeatureSelection selection = getFeatureStore().getFeatureSelection();
		selectedHandler.clear();
		selection.deselectAll();
	}

	public void selectWithPoint(double x, double y, boolean multipleSelection) {
		FeatureSet set = null;
		try {
			firstPoint = new Point2D.Double(x, y);
			FeatureStore featureStore = getFeatureStore();
			// Se comprueba si se pincha en una gemometr�a
			ViewPort viewPort = getLayer().getMapContext().getViewPort();
			double tol = viewPort.toMapDistance(MAP_CONTROL_MANAGER.getTolerance());

			GeometryManager manager = GeometryLocator.getGeometryManager();
			Point center = (org.gvsig.fmap.geom.primitive.Point)manager.create(TYPES.POINT, SUBTYPES.GEOM2D);
			center.setX(x);
			center.setY(y);
			Circle circle = (Circle)geomManager.create(TYPES.CIRCLE, SUBTYPES.GEOM2D);
			circle.setPoints(center, tol);

			FeatureQuery featureQuery = featureStore.createFeatureQuery();
			featureQuery.setAttributeNames(new String[]{featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName()});

			FLayer the_layer = this.getLayer();
	        Geometry query_geo = FLyrVect.fromViewPortCRSToSourceCRS(
	            the_layer, circle, true);

	        IProjection query_proj = this.getLayer().getMapContext().getProjection();
	        if (the_layer.getCoordTrans() != null) {
	            query_proj = the_layer.getCoordTrans().getPOrig();
	        }

			Evaluator evaluator = new IntersectsGeometryEvaluator(
			    query_geo,
			    query_proj,
			    featureStore.getDefaultFeatureType(),
			    featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());

			featureQuery.setFilter(evaluator);
			set = featureStore.getFeatureSet(featureQuery);
			if (!multipleSelection && set.getSize()>1){
				SelectRowPanel selectionPanel = new SelectRowPanel(set, this);
				PluginServices.getMDIManager().addCentredWindow(selectionPanel);
			}
			else {
			    selectGeometries(featureStore, set, viewPort, multipleSelection);
			}
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (CreateGeometryException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			set.dispose();
		}
	}

	public void selectWithSecondPoint(double x, double y) {
		FeatureSet set = null;
		try {
			FeatureStore featureStore = getFeatureStore();
			lastPoint = new Point2D.Double(x, y);
			ViewPort viewPort = getLayer().getMapContext().getViewPort();
			double x1;
			double y1;
			double w1;
			double h1;

			if (firstPoint.getX() < lastPoint.getX()) {
				x1 = firstPoint.getX();
				w1 = lastPoint.getX() - firstPoint.getX();
			} else {
				x1 = lastPoint.getX();
				w1 = firstPoint.getX() - lastPoint.getX();
			}

			if (firstPoint.getY() < lastPoint.getY()) {
				y1 = firstPoint.getY();
				h1 = lastPoint.getY() - firstPoint.getY();
			} else {
				y1 = lastPoint.getY();
				h1 = firstPoint.getY() - lastPoint.getY();
			}

			Envelope envelope = geomManager.createEnvelope(x1, y1, x1 + w1, y1 + h1, SUBTYPES.GEOM2D);

			FeatureQuery featureQuery = featureStore.createFeatureQuery();
			featureQuery.setAttributeNames(new String[]{featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName()});
			Evaluator evaluator=null;
			if (firstPoint.getX() < lastPoint.getX()) {
				evaluator = new IntersectsGeometryEvaluator(envelope.getGeometry(), viewPort.getProjection(), featureStore.getDefaultFeatureType(), featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());
			}else{
				evaluator = new IntersectsGeometryEvaluator(envelope.getGeometry(), viewPort.getProjection(), featureStore.getDefaultFeatureType(), featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());
			}
			featureQuery.setFilter(evaluator);
			set = featureStore.getFeatureSet(featureQuery);
			selectGeometries(featureStore, set, viewPort, false);
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (CreateEnvelopeException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			DisposeUtils.dispose(set);
		}
	}

	public void selectContainsSurface(org.gvsig.fmap.geom.Geometry polygon) {
		FeatureSet set = null;
		try {
			FeatureStore featureStore = getFeatureStore();
			ViewPort viewPort = getLayer().getMapContext().getViewPort();
			FeatureQuery featureQuery = featureStore.createFeatureQuery();
			featureQuery.setAttributeNames(new String[]{featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName()});
			Evaluator evaluator = new ContainsGeometryEvaluator(polygon, viewPort.getProjection(), featureStore.getDefaultFeatureType(), featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());
			featureQuery.setFilter(evaluator);
			set = featureStore.getFeatureSet(featureQuery);
			selectGeometries(featureStore, set, viewPort, false);
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			DisposeUtils.dispose(set);
		}
	}

	private void selectGeometries(FeatureStore featureStore, FeatureSet set, ViewPort vp, boolean multipleSelection) throws DataException, GeometryOperationNotSupportedException, GeometryOperationException{
		FeatureSelection featureSelection = null;
		if (multipleSelection) {
			featureSelection = (FeatureSelection) featureStore.getSelection();
			featureSelection.select(set);
		} else {
			featureSelection = featureStore.createFeatureSelection();
			featureSelection.select(set);
			featureStore.setSelection(featureSelection);
		}
		//Draw the geometries with the edition symbology

	}

	public void selectIntersectsSurface(org.gvsig.fmap.geom.Geometry polygon) {
		FeatureSet set = null;
		try {
			FeatureStore featureStore = getFeatureStore();
			ViewPort vp = getLayer().getMapContext().getViewPort();
			FeatureQuery featureQuery = featureStore.createFeatureQuery();
			featureQuery.setAttributeNames(new String[]{featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName()});
			Evaluator evaluator = new IntersectsGeometryEvaluator(polygon, vp.getProjection(), featureStore.getDefaultFeatureType(), featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());
			featureQuery.setFilter(evaluator);
			set = featureStore.getFeatureSet(featureQuery);
			selectGeometries(featureStore,set,vp,false);
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			DisposeUtils.dispose(set);
		}
	}

	public void selectOutPolygon(org.gvsig.fmap.geom.Geometry polygon) {
		FeatureSet set = null;
		try {
			FeatureStore featureStore = getFeatureStore();
			ViewPort vp = getLayer().getMapContext().getViewPort();
			FeatureQuery featureQuery = featureStore.createFeatureQuery();
			featureQuery.setAttributeNames(new String[]{featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName()});
			Evaluator evaluator=new OutGeometryEvaluator(polygon, vp.getProjection(), featureStore.getDefaultFeatureType(), featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName());
			featureQuery.setFilter(evaluator);
			set = featureStore.getFeatureSet(featureQuery);
			selectGeometries(featureStore, set, vp, false);

		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			DisposeUtils.dispose(set);
		}
	}

	public void selectAll() {
		FeatureSet set = null;
		try {
			FeatureStore featureStore = getFeatureStore();
			ViewPort viewPort = getLayer().getMapContext().getViewPort();
			set = featureStore.getFeatureSet();
			selectGeometries(featureStore, set, viewPort, false);
		} catch (ReadException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (DataException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationNotSupportedException e) {
			NotificationManager.addError(e.getMessage(), e);
		} catch (GeometryOperationException e) {
			NotificationManager.addError(e.getMessage(), e);
		} finally {
			DisposeUtils.dispose(set);
		}
	}

	public void drawHandlers(org.gvsig.fmap.geom.Geometry geom,
			ViewPort vp) {
	    Geometry drawnGeometry = geom;
	    //If the layer is reprojected, reproject the geometry to draw its handlers in the fine position
	    if (getLayer().getCoordTrans() != null){
	        drawnGeometry = drawnGeometry.cloneGeometry();
	        drawnGeometry.reProject(getLayer().getCoordTrans());
        }
		Handler[] handlers = drawnGeometry.getHandlers(org.gvsig.fmap.geom.Geometry.SELECTHANDLER);
		mapControl.getMapControlDrawer().drawHandlers(handlers, vp.getAffineTransform(),
				MAP_CONTROL_MANAGER.getHandlerSymbol());
	}

	public Image getSelectionImage() {
		return imageSelection;
	}

	public void setSelectionImage(Image image) {
		imageSelection = image;
	}

	public Image getHandlersImage() {
		return imageHandlers;
	}

	public void setHandlersImage(Image image) {
		imageHandlers = image;
	}

	public void beforeLayerDraw(LayerDrawEvent e) throws CancelationException {

	}

	public void afterLayerDraw(LayerDrawEvent e) throws CancelationException {
		//Draw the selected geometries...
		FeatureStore featureStore = null;
		DisposableIterator iterator = null;
		try {
			featureStore = getFeatureStore();
			if (featureStore.isEditing()) {
				ViewPort viewPort = getLayer().getMapContext().getViewPort();
				iterator = featureStore.getFeatureSelection().fastIterator();
				MapControlDrawer mapControlDrawer = mapControl.getMapControlDrawer();
				try {
					while (iterator.hasNext()) {
						Feature feature = (Feature) iterator.next();
						Geometry geometry = feature.getDefaultGeometry();
						mapControlDrawer.startDrawing(this);
						mapControlDrawer.setGraphics(e.getGraphics());
					    drawHandlers(geometry.cloneGeometry(), viewPort);
						mapControlDrawer.stopDrawing(this);
					}
				} catch (ConcurrentModificationException e1) {
					// throw new CancelationException(e1);
					// A mitad de pintado se cambia la selecci�n y por tanto no
					// se puede seguir recorriendo la anterior.
					return;
				}
			}
		} catch (Exception e2) {
	          LOG.info("afterLayerDraw error. ", e2);
	          ApplicationLocator.getManager().message(
	              "_Unable_to_draw_geometry_handlers",
	                JOptionPane.ERROR_MESSAGE);
			// LOG.error("Error drawing the selected geometry", e2);
		} finally {
			if (iterator != null) {
				iterator.dispose();
			}
		}
	}

    public void beforeGraphicLayerDraw(LayerDrawEvent e)
	throws CancelationException {
	}

	public void afterLayerGraphicDraw(LayerDrawEvent e)
	throws CancelationException {
	}

	public void activationGained(LayerEvent e) {
		if (ViewCommandStackExtension.csd != null) {
			ViewCommandStackExtension.csd.setModel(((FLyrVect) getLayer())
					.getFeatureStore());

		}
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof DefaultViewPanel) {
			DefaultViewPanel view = (DefaultViewPanel) window;
			if (e.getSource().isEditing()) {
				view.showConsole();
			}
		}
		if (cadtool != null) {
			CADExtension.getCADToolAdapter().setCadTool(cadtool);
			PluginServices.getMainFrame().setSelectedTool(cadtool.toString());
			StartEditing.startCommandsApplicable(null, (FLyrVect) getLayer());
			CADExtension.initFocus();
		}

	}

	public void activationLost(LayerEvent e) {
		try {
			cadtool = CADExtension.getCADTool();
			IWindow window = PluginServices.getMDIManager().getActiveWindow();
			if (window instanceof DefaultViewPanel) {
				DefaultViewPanel view = (DefaultViewPanel) window;
				view.hideConsole();
			}
		} catch (EmptyStackException e1) {
			cadtool = new SelectionCADTool();
			cadtool.init();
		}

	}

	public void setLegend(ILegend legend) {
		this.legend = legend;
	}

	public ILegend getLegend() {
		return legend;
	}

	public FeatureStore getFeatureStore() throws ReadException {
		return ((FLyrVect) getLayer()).getFeatureStore();
	}

	public void update(Observable observable, Object notification) {
		echm.update(observable, notification);
	}

	public EditionChangeManager getEditionChangeManager() {
		return echm;
	}

	public void setEditionChangeManager(EditionChangeManager echm) {
		this.echm = echm;
	}
}
