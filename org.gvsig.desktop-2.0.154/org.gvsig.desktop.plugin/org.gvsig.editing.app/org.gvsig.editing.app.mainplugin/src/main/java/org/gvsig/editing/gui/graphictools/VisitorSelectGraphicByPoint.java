/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.graphictools;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.geom.Geometry;
//import org.gvsig.fmap.mapcontext.layers.FBitSet;

import com.vividsolutions.jts.index.ItemVisitor;

/**
 * @author fjp
 *
 * @deprecated Use queryByRect
 */
public class VisitorSelectGraphicByPoint implements ItemVisitor{

	private Point2D mapPoint;
	private double tol;
//	private FBitSet selection = new FBitSet();
	private int numReg;
	Rectangle2D recPoint;

	public VisitorSelectGraphicByPoint(Point2D mapPoint, double tolerance)
	{
		this.mapPoint = mapPoint;
		this.tol = tolerance;
		this.numReg = 0;

        recPoint = new Rectangle2D.Double(mapPoint.getX() - (tolerance / 2),
        		mapPoint.getY() - (tolerance / 2), tolerance, tolerance);

	}

	/* (non-Javadoc)
	 * @see com.vividsolutions.jts.index.ItemVisitor#visitItem(java.lang.Object)
	 * TODO: VENDRIA BIEN SABER EL NUMERO DE REGISTRO PARA PODER MARCARLO COMO SELECCIONADO
	 */
	public void visitItem(Object item) {
//		FGraphic graf = (FGraphic) item;
//		Geometry geom = graf.getGeom();
//		if (geom.intersects(recPoint))
//		{
//			selection.set(numReg);
//		}
//		numReg++;

	}

//	public FBitSet getSelection() {
//		return selection;
//	}

}


