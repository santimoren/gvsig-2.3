/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.vividsolutions.jts.geom.GeometryCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.IEditionManager;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.InternalPolygonCADToolContext;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class InternalPolygonCADTool extends DefaultCADTool {
    private static final Logger LOG = LoggerFactory
        .getLogger(InternalPolygonCADTool.class);

    protected InternalPolygonCADToolContext _fsm;
    protected List<Point> points = new ArrayList<Point>();
    protected Geometry geometry = null;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new InternalPolygonCADToolContext(this);
        points.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_internalpolygon");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        if (((MouseEvent) event).getClickCount() == 2) {
            addOption(PluginServices
                .getText(this, "InternalPolygonCADTool.end"));
            return;
        }
        VectorialLayerEdited vle = getVLE();
        FeatureSet featureCollection = null;
        DisposableIterator iterator = null;
        try {
            featureCollection =
                (FeatureSet) vle.getFeatureStore().getSelection();
            if (featureCollection.getSize() == 1) {
                iterator = featureCollection.iterator();
                Feature feature = (Feature) iterator.next();
                geometry = (feature.getDefaultGeometry()).cloneGeometry();
                if (geometry.contains(x, y)) {
                    points.add(createPoint(x, y));
                } else {
                    JOptionPane
                        .showMessageDialog(
                            ((Component) PluginServices.getMainFrame()),
                            PluginServices
                                .getText(this,
                                    "_Debe_insertar_el_punto_dentro_de_los_limites_del_poligono"));
                }
            }
        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        } finally {
            DisposeUtils.dispose(iterator);
        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        GeneralPathX gpx = new GeneralPathX();
        GeneralPathX gpx1 = new GeneralPathX();

        if (points.size() > 0) {
            for (int i = 0; i < points.size(); i++) {
                if (i == 0) {
                    gpx.moveTo(points.get(i));
                    gpx1.moveTo(points.get(i));
                } else {
                    gpx.lineTo(points.get(i));
                    gpx1.lineTo(points.get(i));
                }

            }
            gpx.lineTo(createPoint(x, y));
            gpx.closePath();
            gpx1.closePath();

            Geometry geom = createSurface(gpx);
            Geometry geom1 = createSurface(gpx1);

            renderer.draw(geom1, mapControlManager.getSelectionSymbol());
            renderer.draw(geom, mapControlManager.getGeometrySelectionSymbol());
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        VectorialLayerEdited vle = getVLE();
        
        IEditionManager ed_man = this.getEditionManager();
        
        FeatureStore featureStore = null;
        try {
            featureStore = vle.getFeatureStore();
        } catch (ReadException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        DisposableIterator iterator = null;
        try {
            iterator =
                ((FeatureSelection) featureStore.getSelection()).fastIterator();
            if (s.equals(PluginServices.getText(this, "end"))
                || s.equalsIgnoreCase(PluginServices.getText(this,
                    "InternalPolygonCADTool.end"))) {
                if (points.size() > 0) {
                    Feature feature = (Feature) iterator.next();
                    geometry = (feature.getDefaultGeometry()).cloneGeometry();
                    if (geometry instanceof GeometryCollection) {
                        MultiPrimitive gc = (MultiPrimitive) geometry;
                        geometry = createNewPolygonGC(gc, points);
                    } else {
                        geometry = createNewPolygon(geometry, points);
                    }
                    try {
                        EditableFeature eFeature = feature.getEditable();
                        eFeature.setGeometry(featureStore
                            .getDefaultFeatureType()
                            .getDefaultGeometryAttributeName(), geometry);
                        
                        ed_man.updateFeature(featureStore, eFeature);

                    } catch (ReadException e) {
                        NotificationManager.addError(e.getMessage(), e);
                    } catch (DataException e) {
                        NotificationManager.addError(e.getMessage(), e);
                    }
                    ArrayList rows = new ArrayList();
                    rows.add(feature);
                    // vle.setSelectionCache(VectorialLayerEdited.NOTSAVEPREVIOUS,
                    // rows);
                }
                points.clear();
                refresh();

            } else
                if (s.equals(PluginServices.getText(this, "cancel"))) {
                    points.clear();
                }
        } catch (DataException e1) {
            e1.printStackTrace();
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    private Geometry createNewPolygon(Geometry geometry, List<Point> points) {        
        Geometry newGeometry = geometry.cloneGeometry();
                  
        GeneralPathX gpxInternal = new GeneralPathX();
        gpxInternal.moveTo(points.get(points.size() - 1));
        for (int i = points.size() - 2; i >= 0; i--) {
            gpxInternal.lineTo(points.get(i));
        }
        gpxInternal.closePath();

        if (!gpxInternal.isCCW()) {
            gpxInternal.flip();
        }
        
        if (newGeometry.getGeometryType().isTypeOf(Geometry.TYPES.SURFACE)){
            GeneralPathX newGp = newGeometry.getGeneralPath();
            newGp.append(gpxInternal.getPathIterator(null), false);
        }else if (newGeometry.getGeometryType().isTypeOf(Geometry.TYPES.MULTISURFACE)){
            MultiSurface multiSurface = (MultiSurface)newGeometry;
            for (int i=0 ; i<multiSurface.getPrimitivesNumber() ; i++){
                Surface surcafe = multiSurface.getSurfaceAt(i);
                try {
                    if (createSurface(gpxInternal).intersects(surcafe)){
                        GeneralPathX newGp = surcafe.getGeneralPath();
                        newGp.append(gpxInternal.getPathIterator(null), false);
                    }
                } catch (GeometryOperationNotSupportedException e) {
                    LOG.error("Erro calculating the intersection", e);
                } catch (GeometryOperationException e) {
                    LOG.error("Erro calculating the intersection", e);
                }
            }
        }

        return newGeometry;
    }

    private Geometry createNewPolygonGC(MultiPrimitive multiPrimitive,
        List<Point> points) {
        MultiPrimitive multiPrimitiveAux = createMultiPrimitive();

        for (int i = 0; i < multiPrimitive.getPrimitivesNumber(); i++) {
            multiPrimitiveAux.addPrimitive(multiPrimitive.getPrimitiveAt(i));
        }

        GeneralPathX gpx = new GeneralPathX();
        gpx.moveTo(points.get(points.size() - 1));
        for (int i = points.size() - 2; i >= 0; i--) {
            gpx.lineTo(points.get(i));
            multiPrimitiveAux.addPrimitive(createCurve(gpx));
            gpx = new GeneralPathX();
            gpx.moveTo(points.get(i));
        }
        gpx.closePath();
        multiPrimitiveAux.addPrimitive(createCurve(gpx));

        return multiPrimitiveAux;
    }

    public String getName() {
        return PluginServices.getText(this, "internal_polygon_");
    }

    public String toString() {
        return "_internalpolygon";
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { SURFACE, MULTISURFACE };
    }
}
