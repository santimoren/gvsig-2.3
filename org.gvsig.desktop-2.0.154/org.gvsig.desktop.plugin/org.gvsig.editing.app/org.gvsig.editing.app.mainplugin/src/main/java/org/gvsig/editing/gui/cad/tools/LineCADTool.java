/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import org.gvsig.andami.PluginServices;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.LineCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.LineCADToolContext.LineCADToolState;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class LineCADTool extends AbstractCurveCADTool {

    protected LineCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D lastPoint;
    protected double angle;
    protected double length;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new LineCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        LineCADToolState actualState =
            (LineCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Line.FirstPoint")) {
            firstPoint = new Point2D.Double(x, y);
        } else
            if (status == "Line.SecondPointOrAngle") {
                lastPoint = new Point2D.Double(x, y);

                GeneralPathX elShape =
                    new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 2);
                elShape.moveTo(firstPoint.getX(), firstPoint.getY());
                elShape.lineTo(lastPoint.getX(), lastPoint.getY());
                insertAndSelectGeometry(createCurve(elShape));
                firstPoint = (Point2D) lastPoint.clone();
            } else
                if (status == "Line.LenghtOrPoint") {
                    length = firstPoint.distance(x, y);

                    double w = (Math.cos(Math.toRadians(angle))) * length;
                    double h = (Math.sin(Math.toRadians(angle))) * length;
                    lastPoint =
                        new Point2D.Double(firstPoint.getX() + w,
                            firstPoint.getY() + h);

                    GeneralPathX elShape =
                        new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 2);
                    elShape.moveTo(firstPoint.getX(), firstPoint.getY());
                    elShape.lineTo(lastPoint.getX(), lastPoint.getY());
                    insertAndSelectGeometry(createCurve(elShape));

                    firstPoint = (Point2D) lastPoint.clone();
                }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        LineCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        if ((status != "Line.FirstPoint")) { // || (status == "5")) {

            if (firstPoint != null) {
                renderer.drawLine(firstPoint, new Point2D.Double(x, y),
                    mapControlManager.getGeometrySelectionSymbol());
            }
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        // Nothing to do
    }

    public void addValue(double d) {
        LineCADToolState actualState =
            (LineCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status == "Line.SecondPointOrAngle") {
            angle = d;
        } else
            if (status == "Line.LenghtOrPoint") {
                length = d;

                double w = Math.cos(Math.toRadians(angle)) * length;
                double h = Math.sin(Math.toRadians(angle)) * length;
                lastPoint =
                    new Point2D.Double(firstPoint.getX() + w, firstPoint.getY()
                        + h);

                GeneralPathX elShape =
                    new GeneralPathX(GeneralPathX.WIND_EVEN_ODD, 2);
                elShape.moveTo(firstPoint.getX(), firstPoint.getY());
                elShape.lineTo(lastPoint.getX(), lastPoint.getY());
                insertAndSelectGeometry(createCurve(elShape));
                firstPoint = (Point2D) lastPoint.clone();
            }
    }

    public String getName() {
        return PluginServices.getText(this, "line_");
    }

    public String toString() {
        return "_line";
    }

    @Override
    protected int getSupportedPrimitiveGeometryType() {
        return CURVE;
    }
}
