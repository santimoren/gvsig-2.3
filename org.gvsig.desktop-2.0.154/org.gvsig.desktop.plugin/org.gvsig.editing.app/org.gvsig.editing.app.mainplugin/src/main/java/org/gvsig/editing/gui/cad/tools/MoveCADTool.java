/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import org.cresques.cts.ICoordTrans;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.MoveCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.MoveCADToolContext.MoveCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class MoveCADTool extends DefaultCADTool {

    protected MoveCADToolContext _fsm;
    protected Point2D firstPoint;
    protected Point2D lastPoint;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new MoveCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_move");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        MoveCADToolState actualState =
            (MoveCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        try {
            featureStore = vle.getFeatureStore();
        } catch (ReadException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        DisposableIterator iterator = null;
        try {
            FeatureSet selection = (FeatureSet) featureStore.getSelection();// getSelectedRows();
            // ArrayList selectedRowAux=new ArrayList();
            ICoordTrans ct = getVLE().getLayer().getCoordTrans();
            if (status.equals("Move.FirstPointToMove")) {
                firstPoint = new Point2D.Double(x, y);
            } else
                if (status.equals("Move.SecondPointToMove")) {
                    PluginServices.getMDIManager().setWaitCursor();
                    lastPoint = new Point2D.Double(x, y);

                    featureStore.beginEditingGroup(getName());
                    try {

                        iterator = selection.iterator();
                        while (iterator.hasNext()) {
                            Feature feature = (Feature) iterator.next();

                            // }
                            // for (int i = 0; i < selection.size(); i++) {
                            // IRowEdited edRow = (IRowEdited)
                            // selectedRow.get(i);
                            // IFeature feat = (IFeature)
                            // edRow.getLinkedRow().cloneRow();
                            Geometry ig =
                                (feature.getDefaultGeometry()).cloneGeometry();
                            if (ig == null) {
                                continue;
                            }
                            if (ct != null) {
                                lastPoint =
                                    ct.getInverted().convert(lastPoint, null);
                                firstPoint =
                                    ct.getInverted().convert(firstPoint, null);

                            }
                            // if (ct!=null)
                            // ig.reProject(ct);
                            // Movemos la geometr�a
                            ig.move(lastPoint.getX() - firstPoint.getX(),
                                lastPoint.getY() - firstPoint.getY());

                            // if (ct!=null)
                            // ig.reProject(ct.getInverted());
                            
                            super.updateGeometry(featureStore, feature, ig);
//                            
//                            EditableFeature eFeature = feature.getEditable();
//                            eFeature.setGeometry(featureStore
//                                .getDefaultFeatureType()
//                                .getDefaultGeometryAttributeName(), ig);
//                            featureStore.update(eFeature);
//                            
                            
                            // vea.modifyRow(edRow.getIndex(),feat,getName(),EditionEvent.GRAPHIC);
                            // selectedRowAux.add(new
                            // DefaultRowEdited(feat,IRowEdited.STATUS_MODIFIED,edRow.getIndex()));
                        }
                    } finally {
                        featureStore.endEditingGroup();
                    }
                    // vle.setSelectionCache(VectorialLayerEdited.NOTSAVEPREVIOUS,
                    // selectedRowAux);
                    // clearSelection();
                    // selectedRow.addAll(selectedRowAux);
                }
        } catch (ReadException e) {
            NotificationManager.addError(e.getMessage(), e);
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
        }
        PluginServices.getMDIManager().restoreCursor();
    }

    /**
     * M�todo para dibujar lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        MoveCADToolState actualState = (_fsm).getState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();

        if (status.equals("Move.SecondPointToMove")) {
            drawSelectedGeometries(renderer, firstPoint, x, y);
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    public String getName() {
        return PluginServices.getText(this, "move_");
    }

    public String toString() {
        return "_move";
    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }

}
