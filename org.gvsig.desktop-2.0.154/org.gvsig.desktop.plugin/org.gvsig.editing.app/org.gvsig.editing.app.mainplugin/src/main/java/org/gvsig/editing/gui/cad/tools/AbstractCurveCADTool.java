/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.primitive.Curve;

/**
 * Parent class for tools which create Curve geometries.
 * 
 * @author gvSIG team
 */
public abstract class AbstractCurveCADTool extends DefaultCADTool {

    private static final int TYPE_MULTICURVE = 1;

    @Override
    public Feature insertAndSelectGeometry(Geometry geometry) {
        if (getSupportedTypes()[TYPE_MULTICURVE].isTypeOf(getGeometryType())) {
            MultiCurve multiCurve = createMultiCurve();
            multiCurve.addCurve((Curve) geometry);
            return super.insertAndSelectGeometry(multiCurve);
        } else {
            return super.insertAndSelectGeometry(geometry);
        }
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return new int[] { getSupportedPrimitiveGeometryType(), MULTICURVE };
    }

    protected abstract int getSupportedPrimitiveGeometryType();
}
