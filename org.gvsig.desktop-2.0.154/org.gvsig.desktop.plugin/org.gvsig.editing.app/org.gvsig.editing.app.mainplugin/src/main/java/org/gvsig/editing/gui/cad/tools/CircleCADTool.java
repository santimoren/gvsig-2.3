/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.CircleCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.CircleCADToolContext.CircleCADToolState;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.i18n.Messages;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class CircleCADTool extends AbstractSurfaceCADTool {

    protected CircleCADToolContext _fsm;
    protected Point2D center;
    protected Point2D firstPoint;
    protected Point2D secondPoint;
    protected Point2D thirdPoint;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new CircleCADToolContext(this);
    }

    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    public void transition(double d) {
        _fsm.addValue(d);
    }

    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par� metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param sel
     *            Bitset con las geometr�as que est�n seleccionadas.
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        CircleCADToolState actualState =
            (CircleCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status.equals("Circle.CenterPointOr3p")) {
            center = new Point2D.Double(x, y);
        } else
            if (status == "Circle.PointOrRadius") {
                insertAndSelectGeometry(createCircle(createPoint(center),
                    createPoint(x, y)));
            } else
                if (status == "Circle.FirstPoint") {
                    firstPoint = new Point2D.Double(x, y);
                } else
                    if (status == "Circle.SecondPoint") {
                        secondPoint = new Point2D.Double(x, y);
                    } else
                        if (status == "Circle.ThirdPoint") {
                            thirdPoint = new Point2D.Double(x, y);
                            insertAndSelectGeometry(createCircle(
                                createPoint(firstPoint),
                                createPoint(secondPoint),
                                createPoint(thirdPoint)));
                        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param selectedGeometries
     *            BitSet con las geometr�as seleccionadas.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        CircleCADToolState actualState = _fsm.getState();
        String status = actualState.getName();

        Geometry help_geom_perim = null; 
        
        if ((status == "Circle.CenterPointOr3p")) { // || (status == "5")) {

            if (firstPoint != null) {
                renderer.drawLine(firstPoint, new Point2D.Double(x, y),
                    mapControlManager.getGeometrySelectionSymbol());
            }
        }

        if (status == "Circle.PointOrRadius") {
            Point2D currentPoint = new Point2D.Double(x, y);
            Circle circle =
                createCircle(createPoint(center), createPoint(currentPoint));
            
            try {
                help_geom_perim = GeometryLocator.
                    getGeometryManager().createCurve(
                        circle.getGeneralPath(),
                        Geometry.SUBTYPES.GEOM2D);
                renderer.draw(help_geom_perim,
                    mapControlManager.getAxisReferenceSymbol());
            } catch (Exception e) {
                ApplicationLocator.getManager().message(
                    Messages.getText("_Unable_to_draw_help_geometry"),
                    JOptionPane.ERROR_MESSAGE);
            }            
            
            
        } else
            if (status == "Circle.SecondPoint") {
                renderer.drawLine(firstPoint, new Point2D.Double(x, y),
                    mapControlManager.getGeometrySelectionSymbol());
            } else
                if (status == "Circle.ThirdPoint") {
                    Point2D currentPoint = new Point2D.Double(x, y);
                    Geometry geom =
                        createCircle(createPoint(firstPoint),
                            createPoint(secondPoint), createPoint(currentPoint));

                    if (geom != null) {
                        
                        try {
                            help_geom_perim = GeometryLocator.
                                getGeometryManager().createCurve(
                                    geom.getGeneralPath(),
                                    Geometry.SUBTYPES.GEOM2D);
                            renderer.draw(help_geom_perim,
                                mapControlManager.getAxisReferenceSymbol());
                        } catch (Exception e) {
                            ApplicationLocator.getManager().message(
                                Messages.getText("_Unable_to_draw_help_geometry"),
                                JOptionPane.ERROR_MESSAGE);
                        }            
                    }
                }
    }

    /**
     * Add a diferent option.
     * 
     * @param sel
     *            DOCUMENT ME!
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
        CircleCADToolState actualState =
            (CircleCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status == "Circle.CenterPointOr3p") {
            if (s.equalsIgnoreCase(PluginServices.getText(this,
                "CircleCADTool.3p"))) {
                // Opci�n correcta.
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
        CircleCADToolState actualState =
            (CircleCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();

        if (status == "Circle.PointOrRadius") {
            insertAndSelectGeometry(createCircle(createPoint(center), d));
        }
    }

    public String getName() {
        return PluginServices.getText(this, "circle_");
    }

    public String toString() {
        return "_circle";
    }

    @Override
    protected int getSupportedPrimitiveGeometryType() {
        return CIRCLE;
    }
}
