/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.editing.gui.cad.tools;

import java.awt.event.InputEvent;
import java.awt.geom.Point2D;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.editing.CADExtension;
import org.gvsig.editing.gui.cad.DefaultCADTool;
import org.gvsig.editing.gui.cad.exception.CommandException;
import org.gvsig.editing.gui.cad.tools.smc.CopyCADToolContext;
import org.gvsig.editing.gui.cad.tools.smc.CopyCADToolContext.CopyCADToolState;
import org.gvsig.editing.layers.VectorialLayerEdited;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.util.UtilFunctions;
import org.gvsig.fmap.mapcontrol.MapControlDrawer;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class CopyCADTool extends DefaultCADTool {

    private CopyCADToolContext _fsm;
    private Point2D firstPoint;
    private Point2D lastPoint;
    private GeometryType[] gTypes;

    /**
     * M�todo de incio, para poner el c�digo de todo lo que se requiera de una
     * carga previa a la utilizaci�n de la herramienta.
     */
    public void init() {
        _fsm = new CopyCADToolContext(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double, double)
     */
    public void transition(double x, double y, InputEvent event) {
        _fsm.addPoint(x, y, event);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, double)
     */
    public void transition(double d) {
        _fsm.addValue(d);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.gui.cad.CADTool#transition(com.iver.cit.gvsig.fmap
     * .layers.FBitSet, java.lang.String)
     */
    public void transition(String s) throws CommandException {
        if (!super.changeCommand(s)) {
            _fsm.addOption(s);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void selection() {
        FeatureSet selection = null;
        try {
            selection = (FeatureSet) getVLE().getFeatureStore().getSelection();

            if (selection.getSize() == 0
                && !SelectionCADTool.isInstance(CADExtension.getCADTool(), true)) {
                CADExtension.setCADTool("_selection", false);
                ((SelectionCADTool) CADExtension.getCADTool())
                    .setNextTool("_copy");
            }
        } catch (ReadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Equivale al transition del prototipo pero sin pasarle como par�metro el
     * editableFeatureSource que ya estar� creado.
     * 
     * @param x
     *            par�metro x del punto que se pase en esta transici�n.
     * @param y
     *            par�metro y del punto que se pase en esta transici�n.
     */
    public void addPoint(double x, double y, InputEvent event) {
        CopyCADToolState actualState =
            (CopyCADToolState) _fsm.getPreviousState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        FeatureStore featureStore = null;
        try {
            featureStore = vle.getFeatureStore();
            // FeatureSelection selection =
            // featureStore.createFeatureSelection();
            // selection.select((FeatureSet) featureStore.getSelection());
            FeatureSelection selection =
                (FeatureSelection) featureStore.getSelection();
            if (status.equals("Copy.FirstPointToMove")) {
                firstPoint = new Point2D.Double(x, y);
            } else
                if (status.equals("Copy.SecondPointToMove")) {
                    PluginServices.getMDIManager().setWaitCursor();
                    lastPoint = new Point2D.Double(x, y);
                    featureStore.beginEditingGroup(getName());

                    FeatureSelection newSelection =
                        featureStore.createFeatureSelection();

                    DisposableIterator iterator = null;

                    try {
                        iterator = selection.iterator();

                        featureStore.beginComplexNotification();

                        while (iterator.hasNext()) {
                            Feature feature = (Feature) iterator.next();
                            // Movemos la geometr�a
                            Geometry geometry =
                                (feature.getDefaultGeometry()).cloneGeometry();
                            // EditableFeature
                            // eFeature=featureStore.createNewFeature(true);
                            geometry.move(lastPoint.getX() - firstPoint.getX(),
                                lastPoint.getY() - firstPoint.getY());
                            
                            // eFeature.setGeometry(featureStore.getDefaultFeatureType().getDefaultGeometryAttributeName(),geometry);
                            // featureStore.insert(eFeature);
                            newSelection.select(insertGeometry(geometry,
                                feature));
                        }
                        // clearSelection();
                        featureStore.endComplexNotification();
                        featureStore.setSelection(newSelection);
                        featureStore.endEditingGroup();
                        PluginServices.getMDIManager().restoreCursor();
                    } catch (DataException e) {
                        featureStore.endComplexNotification();
                        throw e;
                    } finally {
                        if (iterator != null) {
                            iterator.dispose();
                        }
                    }

                } else {
                }
        } catch (DataException e) {
            NotificationManager.addError(e.getMessage(), e);
        }
    }

    /**
     * M�todo para dibujar la lo necesario para el estado en el que nos
     * encontremos.
     * 
     * @param g
     *            Graphics sobre el que dibujar.
     * @param x
     *            par�metro x del punto que se pase para dibujar.
     * @param y
     *            par�metro x del punto que se pase para dibujar.
     */
    public void drawOperation(MapControlDrawer renderer, double x, double y) {
        CopyCADToolState actualState = _fsm.getState();
        String status = actualState.getName();
        VectorialLayerEdited vle = getVLE();
        if (status.equals("Copy.SecondPointToMove")) {
            drawSelectedGeometries(renderer, firstPoint, x, y);
        }
    }

    /**
     * Add a diferent option.
     * 
     * @param s
     *            Diferent option.
     */
    public void addOption(String s) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.gui.cad.CADTool#addvalue(double)
     */
    public void addValue(double d) {
    }

    public String getName() {
        return PluginServices.getText(this, "copy_");
    }

    public String toString() {
        return "_copy";
    }

    @Override
    public boolean isApplicable(GeometryType geometryType) {
        return true;
    }

    @Override
    protected int[] getSupportedGeometryTypes() {
        return null;
    }
}
