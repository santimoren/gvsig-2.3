/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension;

import org.cresques.cts.IProjection;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.util.ArrayUtils;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class ExporttoLayerExtension extends Extension {

    private ExporttoSwingManager swingManager;

    @Override
    public void initialize() {
        IconThemeHelper.registerIcon("action", "layer-export", this);
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
        // Asignamos el locator e iniciamos la instancia
        swingManager = ExporttoSwingLocator.getSwingManager();

        swingManager.registerWindowManager(new GvSIGExporttoWindowManager());

        ProjectManager projectManager = ApplicationLocator.getProjectManager();
        ViewManager viewManager = (ViewManager) projectManager.getDocumentManager(ViewManager.TYPENAME);
        viewManager.addTOCContextAction("layer-exportto");

    }

    @Override
    public void execute(String actionCommand) {
        this.execute(actionCommand, null);
    }

    @Override
    public void execute(String command, Object[] args) {
        if( !"exportto".equalsIgnoreCase(command) ) {
            return;
        }
        Object layer = ArrayUtils.get(args, 0);
        if( layer instanceof FLyrVect ) {
            WindowManager.MODE mode = WindowManager.MODE.fromInteger(
                    (Integer)ArrayUtils.get(args, 1,DataTypes.INT, 0)
            );
            FLyrVect layervect = (FLyrVect) layer;
            MapContext mapContext = layervect.getMapContext();
            this.showExportto(layervect, mapContext.getProjection(), mapContext, mode);
            return;
        }
        
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if ( view != null ) {
            MapContext mapContext = view.getMapContext();
            FLayers layers = mapContext.getLayers();
            FLayer[] actives = layers.getActives();
            try {
                if ( actives.length == 1 ) {
                    if ( actives[0] instanceof FLyrVect ) {
                        FLyrVect fLyrVect = (FLyrVect) actives[0];
                        showExportto(fLyrVect, mapContext.getProjection(),
                                mapContext, WindowManager.MODE.WINDOW);
                    }
                } else {
                    for (FLayer active : actives) {
                        if (active instanceof FLyrVect) {
                            FLyrVect fLyrVect = (FLyrVect) active;
                            showExportto(fLyrVect, mapContext.getProjection(),
                                    mapContext, WindowManager.MODE.DIALOG);
                        }
                    }
                }
            } catch (Exception e) {
                NotificationManager.showMessageError(e.getMessage(), e);
            }
        }
    }

    public int showExportto(FLyrVect fLyrVect, IProjection projection,
            MapContext mapContext) {
        return this.showExportto(fLyrVect, projection, mapContext, WindowManager.MODE.DIALOG);
    }
    
    public int showExportto(FLyrVect fLyrVect, IProjection projection,
            MapContext mapContext, WindowManager.MODE mode) {

        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        JExporttoServicePanel panel
                = swingManager
                .createExportto(
                        fLyrVect,
                        new LoadLayerAction(mapContext),
                        new int[]{ExporttoSwingManager.VECTORIAL_TABLE_WITH_GEOMETRY});

        WindowManager winmgr = ToolsSwingLocator.getWindowManager();
        winmgr.showWindow(panel, i18nManager.getTranslation("_Export_to"), mode);
        return panel.getStatus();

    }

    @Override
    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if ( view != null ) {
            FLayer[] actives = view.getMapContext().getLayers().getActives();
            for (FLayer active : actives) {
                if (active instanceof FLyrVect) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        return view != null;
    }

}
