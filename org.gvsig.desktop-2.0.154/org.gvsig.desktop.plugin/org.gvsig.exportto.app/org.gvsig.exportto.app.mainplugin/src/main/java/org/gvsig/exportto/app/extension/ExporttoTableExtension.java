/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.exportto.swing.ExporttoSwingLocator;
import org.gvsig.exportto.swing.ExporttoSwingManager;
import org.gvsig.exportto.swing.ExporttoWindowManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class ExporttoTableExtension extends Extension {

    private ExporttoSwingManager swingManager;
    private static final ApplicationManager APPLICATION_MANAGER =
        ApplicationLocator.getManager();

    public void initialize() {
        IconThemeHelper.registerIcon("action", "table-export", this);
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
        swingManager = ExporttoSwingLocator.getSwingManager();
    }

    public void execute(String actionCommand) {
        org.gvsig.andami.ui.mdiManager.IWindow f =
            PluginServices.getMDIManager().getActiveWindow();

        if (f instanceof FeatureTableDocumentPanel) {
            FeatureTableDocumentPanel featureTableDocumentPanel =
                (FeatureTableDocumentPanel) f;
            TableDocument tableDocument =
                (TableDocument) featureTableDocumentPanel.getDocument();
            showExportto(tableDocument.getStore());
        }
    }

    private void showExportto(FeatureStore featureStore) {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        JExporttoServicePanel panel =
            swingManager
                .createExportto(
                    featureStore,
                    null,
                    new LoadTableAction(),
                    new int[] { ExporttoSwingManager.VECTORIAL_TABLE_WITHOUT_GEOMETRY });

        swingManager.getWindowManager().showWindow(panel, i18nManager.getTranslation("_Export_to"),
            ExporttoWindowManager.MODE_WINDOW);

    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        IWindow window = APPLICATION_MANAGER.getUIManager().getActiveWindow();

        if (window == null) {
            return false;
        }

        if (window instanceof FeatureTableDocumentPanel) {
            return true;
        }
        return false;
    }
}
