/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension.preferences;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.exportto.app.extension.ExporttoPreferencesExtension;
import org.gvsig.exportto.swing.preferences.ExporttoSwingPreferencesComponent;

//import org.gvsig.exportto.swing.prov.generic.ExporttoGenericProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderFactory;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderLocator;
import org.gvsig.exportto.swing.spi.ExporttoSwingProviderManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynobject.DynObject;

/**
 * Exportto related preferences page.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class ExporttoPreferencesPage extends AbstractPreferencePage {

    private static final long serialVersionUID = -5751805016601819817L;

    //private ImageIcon icon;

    private ExporttoSwingProviderManager manager;

    private ExporttoSwingPreferencesComponent preferences;

    /**
     * Constructor.
     */
    public ExporttoPreferencesPage() {
        //icon = PluginServices.getIconTheme().get("exportto-properties");

        manager = ExporttoSwingProviderLocator.getManager();
        addComponent(new JLabel(
            Messages.getText("exportto_preferences_panel_description")));
        preferences = manager.createExporttoSwingProvidersPreferences();
        addComponent(preferences.asJComponent());
    }

    public String getID() {
        return getClass().getName();
    }

    public String getTitle() {
        return Messages.getText("exportto_preferences");
    }

    public JPanel getPanel() {
        return this;
    }

    public void initializeValues() {
        // Nothing to do
    }

    public void initializeDefaults() {

        ExporttoSwingProviderManager providerManager =
            ExporttoSwingProviderLocator.getManager();
        List<ExporttoSwingProviderFactory> list =  
        providerManager.getProviderFactories();
        
        ExporttoSwingProviderFactory item = null;
        /*
         * This is similar to the initialization of the
         * export-to library
         */
        for (int i=0; i<list.size(); i++) {
            item = list.get(i);
            // if (item.getClass() == ExporttoGenericProviderFactory.class) {
                // The generic one (advanced) is disabled
            //     providerManager.enableProvider(item, false);
            // } else {
                providerManager.enableProvider(item, true);
            // }
        }

        preferences.initializeDefaults();
    }

    public ImageIcon getIcon() {
        return IconThemeHelper.getImageIcon("export-to-preferences");
    }

    public boolean isValueChanged() {
        return preferences.isValueChanged();
    }

    @Override
    public void storeValues() throws StoreException {
        // Update manager values
        List<ExporttoSwingProviderFactory> providers =
            manager.getProviderFactories();
        @SuppressWarnings({ "unchecked" })
        Set<ExporttoSwingProviderFactory> disabledProviders =
            (Set<ExporttoSwingProviderFactory>) preferences
                .getDisabledProviders();
        Set<String> enabledNames = new HashSet<String>();
        Set<String> disabledNames =
            new HashSet<String>(disabledProviders.size());
        for (int i = 0; i < providers.size(); i++) {
            ExporttoSwingProviderFactory provider = providers.get(i);
            boolean disabled = disabledProviders.contains(provider);
            provider.setEnabled(!disabled);
            if (disabled) {
                disabledNames.add(provider.getName());
            } else {
                enabledNames.add(provider.getName());
            }
        }
        // Persist them
        DynObject preferences =
            PluginsLocator.getManager()
                .getPlugin("org.gvsig.exportto.app.extension")
                .getPluginProperties();
        preferences.setDynValue(
            ExporttoPreferencesExtension.PREFERENCE_ENABLED_PROVIDERS,
            enabledNames);
        preferences.setDynValue(
            ExporttoPreferencesExtension.PREFERENCE_DISABLED_PROVIDERS,
            disabledNames);
    }

    @Override
    public void setChangesApplied() {
        preferences.setChangesApplied();
    }

}
