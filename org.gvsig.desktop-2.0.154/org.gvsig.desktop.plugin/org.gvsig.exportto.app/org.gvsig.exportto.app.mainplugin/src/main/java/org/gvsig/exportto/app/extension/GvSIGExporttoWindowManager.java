/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.exportto.swing.ExporttoWindowManager;
import org.gvsig.exportto.swing.JExporttoServicePanel;

/**
 * {@link ExporttoWindowManager} implementation to show Exportto
 * windows as gvSIG windows
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class GvSIGExporttoWindowManager implements ExporttoWindowManager,
    ComponentListener {

    private Map<JPanel, IWindow> panelToWindow = new HashMap<JPanel, IWindow>();

    public void showWindow(JExporttoServicePanel panel, String title, int mode) {
        ExporttoWindow window = new ExporttoWindow(panel, title, mode);
        panel
            .setExporttoServicePanelListener(new ExporttoWindowListener(window));

        panel.addComponentListener(this);

        panelToWindow.put(panel, window);

        PluginServices.getMDIManager().addCentredWindow(window);
    }

    public void componentHidden(ComponentEvent componentEvent) {
        JPanel panel = (JPanel) componentEvent.getSource();
        IWindow window = panelToWindow.get(panel);
        PluginServices.getMDIManager().closeWindow(window);
        panelToWindow.remove(panel);
    }

    public void componentMoved(ComponentEvent e) {
        // Nothing to do
    }

    public void componentResized(ComponentEvent e) {
        // Nothing to do
    }

    public void componentShown(ComponentEvent e) {
        // Nothing to do
    }
}
