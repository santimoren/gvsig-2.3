/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class LoadTableAction implements ExporttoServiceFinishAction {

    private static final Logger LOG = LoggerFactory
        .getLogger(LoadLayerAction.class);
    private static final DataManager DATA_MANAGER = DALLocator.getDataManager();

    public LoadTableAction() {
        super();
    }

    public void finished(String tableName,
        DataStoreParameters dataStoreParameters) {

        int res =
            JOptionPane.showConfirmDialog((JComponent) PluginServices
                .getMDIManager().getActiveWindow(), PluginServices.getText(
                this, "add_table_to_project"), PluginServices.getText(this,
                "add_table"), JOptionPane.YES_NO_OPTION);

        if (res == JOptionPane.YES_OPTION) {
            try {
                FeatureStore featureStore =
                    (FeatureStore) DATA_MANAGER.openStore(
                        dataStoreParameters.getDataStoreName(),
                        dataStoreParameters);
                TableDocument tableDocument =
                    (TableDocument) ProjectManager.getInstance()
                        .createDocument(TableManager.TYPENAME, tableName);
                tableDocument.setStore(featureStore);

                ApplicationLocator.getManager().getProjectManager()
                    .getCurrentProject().add(tableDocument);
                // FeatureTableDocumentPanel featureTableDocumentPanel =
                // new FeatureTableDocumentPanel(tableDocument);
                //
                // MDI_MANAGER.addWindow(featureTableDocumentPanel);
            } catch (ValidateDataParametersException e) {
                LOG.error("Error opening the exported table", e);
            } catch (InitializeException e) {
                LOG.error("Error opening the exported table", e);
            } catch (ProviderNotRegisteredException e) {
                LOG.error("Error opening the exported table", e);
            } catch (DataException e) {
                LOG.error("Error opening the exported table", e);
            }
        }
    }
}
