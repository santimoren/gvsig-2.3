/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.exportto.app.extension;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.exportto.ExporttoServiceFinishAction;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class LoadLayerAction implements ExporttoServiceFinishAction {

    private static final Logger LOG = LoggerFactory
        .getLogger(LoadLayerAction.class);
    private static final MapContextManager MAP_CONTEXT_MANAGER =
        MapContextLocator.getMapContextManager();

    private MapContext mapContext = null;

    public LoadLayerAction(MapContext mapContext) {
        super();
        this.mapContext = mapContext;
    }

    public void finished(String layerName,
        DataStoreParameters dataStoreParameters) {
        try {
            int res =
                JOptionPane.showConfirmDialog((JComponent) PluginServices
                    .getMDIManager().getActiveWindow(), PluginServices.getText(
                    this, "insertar_en_la_vista_la_capa_creada"),
                    PluginServices.getText(this, "insertar_capa"),
                    JOptionPane.YES_NO_OPTION);

            if (res == JOptionPane.YES_OPTION) {
                FLayer layer =
                    MAP_CONTEXT_MANAGER.createLayer(layerName,
                        dataStoreParameters);
                mapContext.getLayers().addLayer(layer);
            }

        } catch (LoadLayerException e) {
            LOG.error("Error loading the exported layer", e);
        }
    }
}
