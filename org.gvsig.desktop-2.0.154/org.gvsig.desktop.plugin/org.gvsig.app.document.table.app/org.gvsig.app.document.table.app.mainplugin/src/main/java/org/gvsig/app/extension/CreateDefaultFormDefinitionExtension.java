package org.gvsig.app.extension;

import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;

/**
 *
 * @author jjdelcerro
 */
public class CreateDefaultFormDefinitionExtension extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(CreateDefaultFormDefinitionExtension.class);

    @Override
    public void initialize() {
    }

    @Override
    public void execute(String actionCommand) {
        if ("table-create-default-form-definition".equalsIgnoreCase(actionCommand)) {
            ApplicationManager application = ApplicationLocator.getManager();
            TableDocument tableDoc = (TableDocument) application.getActiveDocument(TableManager.TYPENAME);
            if (tableDoc == null) {
                return;
            }
            FeatureTypeDefinitionsManager featureTypeDefinitionsManager = DALLocator.getFeatureTypeDefinitionsManager();
            try {
                FeatureStore store = tableDoc.getFeatureStore();
                FeatureType featureType = store.getDefaultFeatureType();
                featureTypeDefinitionsManager.add(store, featureType, featureType); 
                application.messageDialog(
                        "Se creado la definicion de formulario asociada a la table '" + tableDoc.getName() + "'.",
                        "Definicion de formulario",
                        JOptionPane.INFORMATION_MESSAGE
                );
            } catch (Exception ex) {
                logger.warn("Can't save form definition for layer '" + tableDoc.getName() + "'.", ex);
                application.messageDialog(
                        "No se ha podido crear la definicion de formulario asociada a la capa '" + tableDoc.getName() + "'.\n\n(Consulte el registro de errores si desea mas informacion)",
                        "Definicion de formulario",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
       ApplicationManager application = ApplicationLocator.getManager();
       TableDocument tableDoc = (TableDocument) application.getActiveDocument(TableManager.TYPENAME);
       if (tableDoc == null) {
           return false;
       }
       return true;
    }

}
