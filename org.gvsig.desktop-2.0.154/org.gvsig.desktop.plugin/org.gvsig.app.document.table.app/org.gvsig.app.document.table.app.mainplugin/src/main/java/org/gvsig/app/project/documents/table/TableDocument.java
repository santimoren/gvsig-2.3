/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {TableDocument implementation based on the gvSIG DAL API}
 */
package org.gvsig.app.project.documents.table;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.VectorLayer;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureStoreModel;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class TableDocument extends AbstractDocument implements Observer {

    public static final String TABLE_PROPERTIES_PAGE_GROUP = "TableDocument";

    private static final long serialVersionUID = -1842181135614158881L;

    final static private Logger logger = LoggerFactory
        .getLogger(TableDocument.class);

    private FeatureStore store;

    private String featureTypeId;

    private String[] attributeNames;

    private List<TableLink> linkTable;

    private VectorLayer associatedLayer;

    private FeatureQuery query;

    private Evaluator baseFilter;

    private FeatureQueryOrder baseOrder;

    private FeatureStoreModel featureStoreModel;

    private Object lock = new Object();

    public TableDocument(DocumentManager factory) {
        super(factory);
        this.store = null;
        this.query = null;
        this.featureTypeId = null;
        this.baseFilter = null;
        this.baseOrder = null;
    }

    public TableDocument() {
        this(null);
    }

    public TableDocument(DocumentManager factory, FeatureStore store) {
        this(factory);
        setStore(store);
    }

    public FeatureStoreModel getFeatureStoreModel() {
        synchronized (lock) {
            if (this.featureStoreModel == null) {
                try {
                    this.featureStoreModel =
                        new FeatureStoreModel(getStore(), getQuery());
                } catch (BaseException e) {
                    NotificationManager.addError(e);
                }
            }
        }
        return this.featureStoreModel;
    }

    public void setStore(FeatureStore store) {
        if (this.store != null) {
            throw new UnsupportedOperationException(
                "can't set store. store already set.");
        }
        this.store = store;
        this.store.addObserver(this);
        this.query = null; // setQuery(store.createFeatureQuery());
    }

    @SuppressWarnings("unchecked")
    public FeatureQuery getQuery() {
        if (this.query == null) {
            try {
                FeatureType fType = null;
                this.query = this.store.createFeatureQuery();
                if (this.featureTypeId != null) {
                    Iterator<FeatureType> iter;
                    iter = this.store.getFeatureTypes().iterator();
                    while (iter.hasNext()) {
                        fType = iter.next();
                        if (this.featureTypeId.equals(fType.getId())) {
                            this.query.setFeatureType(fType);
                            break;
                        }
                    }
                    if (fType == null) {
                        throw new RuntimeException(MessageFormat.format(
                            "frature type {1} not found.", this.featureTypeId));
                    }

                } else {
                    fType = store.getDefaultFeatureType();
                }

                if (this.attributeNames != null) {
                    ArrayList<String> newNames = new ArrayList<String>();
                    for (String name : this.attributeNames) {
                        if (fType.getIndex(name) > -1) {
                            newNames.add(name);
                        }
                    }
                    if (newNames.size() > 0) {
                        this.query.setAttributeNames(newNames
                            .toArray(this.attributeNames));
                    }
                }

                this.query.setFilter(this.baseFilter); // TODO check is valid
                this.query.setOrder(this.baseOrder);

            } catch (DataException e) {
                NotificationManager.addError(e);
                return null;
            }

        }
        return this.query;
    }

    /**
     * @return the store
     */
    public FeatureStore getStore() {
        return store;
    }

    /**
     * @return the store
     */
    public FeatureStore getFeatureStore() {
        return store;
    }

    /**
     * Return information about the table links.
     *
     * @return List of TableLink information.
     */
    public List<TableLink> getLinks() {
        return this.linkTable;
    }

    /**
     * Returns if this table document has links with other tables.
     *
     * @return if this table document has links with other tables
     */
    public boolean hasLinks() {
        return this.linkTable != null && this.linkTable.size() > 0;
    }

    /**
     * Devuelve el identificador de la tabla que contiene el link.
     *
     * @return identificador �nico de la tabla.
     * @deprecated see {{@link #getLinks()}
     */
    public String getLinkTable() {
        if (linkTable == null || linkTable.isEmpty()) {
            return null;
        }
        return linkTable.get(0).getTargetTable().getName();
    }

    /**
     * Devuelve el nombre del campo de la tabla a enlazar.
     *
     * @return Nombre del campo de la tabla a enlazar.
     * @deprecated see {{@link #getLink()}
     */
    public String getField1() {
        if (linkTable.isEmpty()) {
            return null;
        }
        return this.linkTable.get(0).getSourceFieldName();
    }

    /**
     * Devuelve el nombre del campo de la tabla enlazada.
     *
     * @return Nombre del campo de la tabla enlazada.
     * @deprecated see {{@link #getLink()}
     */
    public String getField2() {
        if (linkTable.isEmpty()) {
            return null;
        }
        return this.linkTable.get(0).getTargetFieldName();
    }

    /**
     * Enlaza la seleccion de esta tabla con la de la tabla indicada
     *
     * @deprecated see {@link #addLinkTable(String, String, String)}
     */
    public void setLinkTable(String targetTable, String fieldSource,
        String fieldTarget) {
        this.addLinkTable(targetTable, fieldSource, fieldTarget);
    }

    /**
     * Add a table link to this document.
     *
     * @param targetTable
     * @param fieldSource
     * @param fieldTarget
     */
    public void addLinkTable(String targetTable, String fieldSource,
        String fieldTarget) {
        TableDocument target =
            (TableDocument) ProjectManager.getInstance().getCurrentProject()
                .getDocument(targetTable, TableManager.TYPENAME);
        TableLink link = new TableLink(this, target, fieldSource, fieldTarget);
        link.setEnabled(true);
        if (this.linkTable == null) {
            this.linkTable = new ArrayList<TableLink>();
        }
        this.linkTable.add(link);
    }

    /**
     * remove the last link to table added.
     *
     */
    public void removeLinkTable() {
        if (linkTable.isEmpty()) {
            return;
        }
        TableLink link = this.linkTable.remove(this.linkTable.size() - 1);
        link.setEnabled(false);
        this.linkTable = null;
    }

    /**
     * Remove the link to the table document.
     *
     * @param name
     *            of table document to remove.
     */
    public void removeLinkTable(String name) {
        for (TableLink link : this.linkTable) {
            if (name.equals(link.target.getName())) {
                link.setEnabled(false);
                this.linkTable.remove(link);
            }
        }
    }

    public VectorLayer getAssociatedLayer() {
        return associatedLayer;
    }

    public void setAssociatedLayer(VectorLayer associatedLayer) {
        this.associatedLayer = associatedLayer;
    }

    public void update(Observable arg0, Object arg1) {
        if (this.store.equals(arg0)) {
            if (arg1 instanceof FeatureStoreNotification) {
                FeatureStoreNotification event =
                    (FeatureStoreNotification) arg1;
                if (event.getType() == FeatureStoreNotification.TRANSFORM_CHANGE
                    || event.getType() == FeatureStoreNotification.RESOURCE_CHANGED) {
                    this.query = null;
                }
            }

        }

    }

    @SuppressWarnings("unchecked")
    public void loadFromState(PersistentState state)
        throws PersistenceException {
        try {
            super.loadFromState(state);

            this.store = (FeatureStore) state.get("store");
            this.featureTypeId = state.getString("featureTypeId");
            this.attributeNames =
                (String[]) state.getArray("attributeNames", String.class);
            this.linkTable = state.getList("linkTable");
            this.associatedLayer = (FLyrVect) state.get("associatedLayer");
            this.query = (FeatureQuery) state.get("query");
            this.baseFilter = (Evaluator) state.get("baseFilter");
            this.baseOrder = (FeatureQueryOrder) state.get("baseOrder");
        } catch (Throwable e) {
            String storeName = (store == null) ? "unknow" : store.getFullName();
            logger.warn("can't load table '" + this.getName() + "' (store="
                + storeName + ") from persisted state.", e);
//            this.setAvailable(false);
            return;
        }
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        super.saveToState(state);

        state.set("store", store);
        state.set("featureTypeId", featureTypeId);
        state.set("attributeNames", attributeNames);
        state.set("linkTable", linkTable);
        state.set("associatedLayer", associatedLayer);
        state.set("query", query);
        // state.set("baseFilter", baseFilter);
        state.set("baseOrder", baseOrder);
    }

    public static class TableLink implements Observer, Persistent {

        private TableDocument source;
        private FeatureStore storeSource;
        private int fieldSource;

        private TableDocument target;
        private FeatureStore storeTarget;
        private int fieldTarget;

        private boolean enabled;

        public TableLink() {
            this.source = null;
            this.target = null;
            this.fieldSource = -1;
            this.fieldTarget = -1;
            this.storeSource = null;
            this.storeTarget = null;
            this.enabled = false;
        }

        public TableLink(TableDocument source, TableDocument target,
            String fieldSource, String fieldTarget) {
            this();
            this.initialize(source, target, fieldSource, fieldTarget);
        }

        private void initialize(TableDocument source, TableDocument target,
            String fieldSource, String fieldTarget) {
            this.source = source;
            this.target = target;

            this.storeSource = this.source.getStore();
            this.storeTarget = this.target.getStore();
            try {
                this.fieldSource =
                    storeSource.getDefaultFeatureType().getIndex(fieldSource);
                this.fieldTarget =
                    storeTarget.getDefaultFeatureType().getIndex(fieldTarget);
            } catch (DataException ex) {
                logger.error("Can't initialize TableLink", ex);
                throw new RuntimeException("Can't initialize TableLink", ex);
            }
        }

        public void setEnabled(boolean enabled) {
            if (enabled) {
                this.storeSource.addObserver(this);
            } else {
                this.storeSource.deleteObserver(this);
            }
            this.enabled = enabled;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

        public TableDocument getTargetTable() {
            return this.target;
        }

        public TableDocument getSourceTable() {
            return this.source;
        }

        public String getSourceFieldName() {
            try {
                return ((FeatureAttributeDescriptor) this.storeSource
                    .getDefaultFeatureType().get(this.fieldSource)).getName();
            } catch (DataException e) {
                logger.warn("Can't get source field name.", e);
                return null;
            }
        }

        public String getTargetFieldName() {
            try {
                return ((FeatureAttributeDescriptor) this.storeTarget
                    .getDefaultFeatureType().get(this.fieldTarget)).getName();
            } catch (DataException e) {
                logger.warn("Can't get target field name.", e);
                return null;
            }
        }

        public void update(Observable arg0, Object arg1) {
            try {
                FeatureSet fCollection1 =
                    (FeatureSet) storeSource.getSelection();
                FeatureSelection fCollection2 =
                    (FeatureSelection) storeTarget.createSelection();
                List<Object> idx = new ArrayList<Object>();

                // Construimos el �ndice
                DisposableIterator iterator1 = null;
                try {
                    iterator1 = fCollection1.fastIterator();
                    while (iterator1.hasNext()) {
                        Feature feature = (Feature) iterator1.next();
                        Object obj = feature.get(fieldSource);
                        if (!idx.contains(obj)) {
                            idx.add(obj);
                        }
                    }
                } finally {
                    if (iterator1 != null) {
                        iterator1.dispose();
                    }
                }
                FeatureSet set = null;
                DisposableIterator iterator2 = null;

                try {
                    set = storeTarget.getFeatureSet();
                    iterator2 = set.fastIterator();
                    while (iterator2.hasNext()) {
                        Feature feature = (Feature) iterator2.next();
                        Object obj = feature.get(fieldTarget);
                        if (idx.contains(obj)) {
                            fCollection2.select(feature);
                        }
                    }
                } catch (DataException e1) {
                    NotificationManager.addError(e1);
                    return;
                } finally {
                    if (iterator2 != null) {
                        iterator2.dispose();
                    }
                    if (set != null) {
                        set.dispose();
                    }
                }

                // this applies the selection to the linked table
                if (storeSource != storeTarget) {
                    storeTarget.setSelection(fCollection2);
                }
            } catch (DataException e2) {
                NotificationManager.addError(e2);
                return;
            }
        }

        public void loadFromState(PersistentState state)
            throws PersistenceException {
            this.initialize((TableDocument) state.get("source"),
                (TableDocument) state.get("target"),
                state.getString("fieldSource"), state.getString("fieldTarget"));
            this.setEnabled(state.getBoolean("enabled"));
        }

        public void saveToState(PersistentState state)
            throws PersistenceException {
            state.set("source", this.source);
            state.set("target", this.target);
            state.set("fieldSource", this.getSourceFieldName());
            state.set("fieldTarget", this.getTargetFieldName());
            state.set("enabled", this.getEnabled());
        }

    }

    @Override
    public boolean isTemporary() {
        if(this.associatedLayer!=null && this.associatedLayer.isTemporary()){
            return true;
        }
        return false;
    }

    public boolean isAvailable() {
        if(this.store == null){
            return false;
        }
        return true;
    }

}
