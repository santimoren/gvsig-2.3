/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import javax.swing.JOptionPane;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;

public class TableRowsOperations extends Extension {

    public void initialize() {
        registerIcons();
    }

    private void registerIcons() {
    	IconThemeHelper.registerIcon("action", "selection-move-up", this);
    	IconThemeHelper.registerIcon("action", "selection-disable-move-up", this);
    	IconThemeHelper.registerIcon("action", "selection-reverse", this);    
    }

    public void execute(String actionCommand) {
        FeatureTableDocumentPanel tableDocument = getTableDocument();
        if (actionCommand.equalsIgnoreCase("selection-disable-move-up") ) {
            tableDocument.setSelectionUp(false);
        }
        if (actionCommand.equalsIgnoreCase("selection-move-up") ) {
//            if (thereIsSelection(tableDocument)) {
                tableDocument.setSelectionUp(true);
                tableDocument.getModel().setModified(true);
//            } else {
//                JOptionPane.showMessageDialog(
//                    ApplicationLocator.getManager().getRootComponent(),
//                    Messages.getText("_There_are_no_selected_rows"),
//                    Messages.getText("_Move_up_selection"),
//                    JOptionPane.INFORMATION_MESSAGE);
//            }
            
        }
        if (actionCommand.equalsIgnoreCase("selection-reverse") ) {
            invertSelection(tableDocument);
            tableDocument.getModel().setModified(true);
        }
        
    }

    /**
     * Flip the selection (inverts selection)
     * 
     * @param table
     */
    private void invertSelection(FeatureTableDocumentPanel table) {
        try {
            FeatureStore fs = getTableDocument().getModel().getStore();
            fs.getFeatureSelection().reverse();
        } catch (DataException e) {
            e.printStackTrace();
            NotificationManager.addError(e);
        }
    }

//    private void showsSelectedRows(FeatureTableDocumentPanel table) {
//        table.setSelectionUp(true);
//    }

    public boolean isEnabled() {
        return getTableDocument() != null;
    }

    public boolean isVisible() {
        return getTableDocument() != null;
    }

    private FeatureTableDocumentPanel getTableDocument() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (v != null && v instanceof FeatureTableDocumentPanel) {
            return (FeatureTableDocumentPanel) v;
        }
        return null;
    }
    
    private boolean thereIsSelection(FeatureTableDocumentPanel tabledoc) {
        
        if (tabledoc != null) {
            try {
                return !tabledoc.getModel().getStore()
                    .getFeatureSelection().isEmpty();
            } catch (DataException e) {
                NotificationManager.addError(e);
            }
        }
        return false;
        
    }
}
