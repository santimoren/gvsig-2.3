/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.table;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.TableModelListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.gui.CreateNewAttributePanel;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.StoreUpdateFeatureTypeException;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTable;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureTableModel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Feature Table Operations.
 *
 * @author Vicente Caballero Navarro
 *
 */
public class TableOperations {

    private static Logger logger = LoggerFactory.getLogger(TableOperations.class);

    public static final int MAX_FIELD_LENGTH = 254;
    private static TableOperations fto = null;
    private ArrayList<Feature> selectedFeatures = new ArrayList<Feature>();
    private boolean cutting = false;

    private FeatureTableDocumentPanel tablePanel = null;
    private FeatureStore featureStore;

    public static TableOperations getInstance() {
        if (fto == null) {
            fto = new TableOperations();
        }
        return fto;
    }

    public void setTablePanel(FeatureTableDocumentPanel tp) {
        tablePanel = tp;
        featureStore = tp.getModel().getStore();
    }

    public void copyFeatures() throws DataException {
        cutting = false;
        copy();
    }

    public boolean hasSelection() {
        return !selectedFeatures.isEmpty();
    }

    public void pasteFeatures() throws DataException {
        if (cutting) {
            delete();
            cutting = false;
        }
        Iterator<Feature> features = selectedFeatures.iterator();
        while (features.hasNext()) {
            Feature feature = features.next();
            if( !insertFeature(feature.getEditable()) ) {
                break;
            }
        }
    }

    public void cutFeatures() throws DataException {
        cutting = true;
        copy();
    }

    private void copy() throws DataException {
        DisposableIterator features = null;
        try {
            features =
                ((FeatureSelection) featureStore.getSelection()).fastIterator();
            selectedFeatures.clear();
            while (features.hasNext()) {
                Feature feature = (Feature) features.next();
                selectedFeatures.add(feature);
            }
        } finally {
            if (features != null) {
                features.dispose();
            }
        }
    }

    private void delete() throws DataException {
        Iterator<Feature> features = selectedFeatures.iterator();
        while (features.hasNext()) {
            Feature feature = features.next();
            if( !deleteFeature(feature) ) {
                break;
            }
        }
    }

    public void deleteFeatures() throws DataException {

        FeatureTableModel _ftm = this.tablePanel.getTablePanel().getTableModel();
        List<TableModelListener> tmll = removeTableModelListeners(_ftm);
        DisposableIterator feat_iter = null;
        Feature feat = null;
        try {


            FeatureSelection selection = featureStore.createFeatureSelection();
            selection.select((FeatureSet) featureStore.getSelection());
            feat_iter = selection.fastIterator();
            while (feat_iter.hasNext()) {
                feat = (Feature) feat_iter.next();
                if( !deleteFeature(feat) ) {
                    return;
                }
            }


        } finally {
            if (feat_iter != null) {
            	feat_iter.dispose();
            }

            addTableModelListeners(_ftm, tmll);
        }
    }

    /**
     * @param _ftm
     * @param tmll
     */
    private void addTableModelListeners(
        FeatureTableModel _model,
        List<TableModelListener> _list) {

        Iterator<TableModelListener> iter = _list.iterator();
        while (iter.hasNext()) {
            _model.addTableModelListener(iter.next());
        }
        _model.fireTableDataChanged();
    }

    /**
     * @param ftm
     * @param class1
     * @return
     */
    private List<TableModelListener> removeTableModelListeners(FeatureTableModel ftm) {

        TableModelListener[] ll = ftm.getListeners(TableModelListener.class);
        List<TableModelListener> resp = new ArrayList<TableModelListener>();

        int n = ll.length;
        for (int i=0; i<n; i++) {
            resp.add(ll[i]);
            ftm.removeTableModelListener(ll[i]);
        }

        return resp;
    }

    public void insertNewFeature() throws DataException {
        EditableFeature feature = featureStore.createNewFeature();
        insertFeature(feature);
    }

    /*
     * Return false if the operation is canceled.
     */
    private boolean insertFeature(EditableFeature feature) throws DataException {
        EditingNotificationManager editingNotificationManager = DALSwingLocator.getEditingNotificationManager();
        EditingNotification notification = editingNotificationManager.notifyObservers(
                this,
                EditingNotification.BEFORE_INSERT_FEATURE,
                tablePanel.getDocument(),
                featureStore,
                feature);
        if ( notification.isCanceled() ) {
            return false;
        }
        if( notification.shouldValidateTheFeature() ) {
            if ( !editingNotificationManager.validateFeature(feature) ) {
                return false;
            }
        }
        featureStore.insert(feature);
        editingNotificationManager.notifyObservers(
                this,
                EditingNotification.AFTER_INSERT_FEATURE,
                tablePanel.getDocument(),
                featureStore,
                feature);
        return true;
    }

    private boolean deleteFeature(Feature feature) throws DataException {
        EditingNotificationManager editingNotification = DALSwingLocator.getEditingNotificationManager();
        EditingNotification notification = editingNotification.notifyObservers(
                this,
                EditingNotification.BEFORE_REMOVE_FEATURE,
                tablePanel.getDocument(),
                featureStore,
                feature);
        if ( notification.isCanceled() ) {
            return false;
        }
        featureStore.delete(feature);
        editingNotification.notifyObservers(
                this,
                EditingNotification.AFTER_REMOVE_FEATURE,
                tablePanel.getDocument(),
                featureStore,
                feature);
        return true;
    }

    private boolean updateFeatureType(EditableFeatureType featureType) throws DataException {

        EditingNotificationManager editingNotification = DALSwingLocator.getEditingNotificationManager();
        EditingNotification notification = editingNotification.notifyObservers(
                this,
                EditingNotification.BEFORE_UPDATE_FEATURE_TYPE,
                tablePanel.getDocument(),
                featureStore,
                featureType);
        if ( notification.isCanceled() ) {
            return false;
        }
        featureStore.update(featureType);
        editingNotification.notifyObservers(
                this,
                EditingNotification.AFTER_UPDATE_FEATURE_TYPE,
                tablePanel.getDocument(),
                featureStore,
                featureType);
        return true;
    }

    public void deleteAttributes(FeatureTable table) throws DataException {
        EditableFeatureType eft =
            featureStore.getDefaultFeatureType().getEditable();
        FeatureAttributeDescriptor[] selecteds =
            table.getSelectedColumnsAttributeDescriptor();
        for (int i = 0; i < selecteds.length; i++) {
            eft.remove(selecteds[i].getName());
        }
        featureStore.update(eft);
    }

    public void insertAttributes(FeatureTable table) throws DataException {

		EditableFeatureType eft = featureStore.getDefaultFeatureType().getEditable();

		List<String> tmpfnames = new ArrayList<String>();
		int size = eft.size();
		for (int i = 0; i < size; i++) {
			FeatureAttributeDescriptor ad = (FeatureAttributeDescriptor) eft.get(i);
			tmpfnames.add(ad.getName());
		}

		CreateNewAttributePanel panelNewField = new CreateNewAttributePanel();
		panelNewField.setCurrentFieldNames(tmpfnames.toArray(new String[0]));
	    DataManager dataManager = DALLocator.getDataManager();
	    FeatureStoreProviderFactory factory =
	            (FeatureStoreProviderFactory) dataManager.
	            getStoreProviderFactory(featureStore.getProviderName());
		panelNewField.setMaxAttributeNameSize(factory.getMaxAttributeNameSize());
		panelNewField.setOkAction(new NewFieldActionListener(panelNewField, eft));
		ApplicationLocator.getManager().getUIManager().addWindow(panelNewField);
		featureStore.update(eft);
    }

    public void renameAttributes(FeatureTable table) throws DataException {

        FeatureType _ft = featureStore.getDefaultFeatureType();

        FeatureAttributeDescriptor[] selecteds =
            table.getSelectedColumnsAttributeDescriptor();

        for (int i = selecteds.length - 1; i >= 0; i--) {
            String newName =
                JOptionPane.showInputDialog((Component) PluginServices
                    .getMDIManager().getActiveWindow(),
                    PluginServices.getText(
                    this, "_Please_insert_new_field_name"),
                    selecteds[i]
                    .getName());
            if (newName == null || newName.length() == 0) {
                continue;
            }
            if (_ft.getIndex(newName) != -1) {
                NotificationManager.showMessageInfo(
                		Messages.getText("field_already_exists"), null);
                return;
            }

            renameAttribute(featureStore, selecteds[i].getName(), newName);
        }

        // featureStore.finishEditing();
        // featureStore.edit(FeatureStore.MODE_FULLEDIT);
    }

    /**
     * This method renames a field in three steps:
     *
     * (1) add new field using type and size of old field.
     * (2) copy value from old field to new field.
     * (3) remove old field.
     *
     * @param fs
     * @param name
     * @param newName
     * @return true if the change took place
     */
    private static boolean renameAttribute(FeatureStore fs, String name, String newName) {

        EditableFeatureType eft = null;
        FeatureType dft = null;
        try {
            dft = fs.getDefaultFeatureType();

            if (dft instanceof EditableFeatureType) {
                eft = (EditableFeatureType) dft;
            } else {
                eft = dft.getEditable();
            }

            EditableFeatureAttributeDescriptor efad =
                (EditableFeatureAttributeDescriptor) eft.getAttributeDescriptor(name);
            efad.setName(newName);
            fs.update(eft);

        } catch (DataException de) {

            Component root_comp =
                ApplicationLocator.getManager().getRootComponent();

            JOptionPane.showMessageDialog(
                root_comp,
                Messages.getText("_Unable_to_rename_attribute") +
                ": " + de.getMessage(),
                Messages.getText("_Unable_to_rename_attribute"),
                JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;

        /*
        try {
            // ========== add new field
            eft = fs.getDefaultFeatureType().getEditable();
            FeatureAttributeDescriptor fad = eft.getAttributeDescriptor(name);
            eft.add(newName, fad.getType(), fad.getSize());
            fs.update(eft);
        } catch (DataException ex) {
            logger.info("Unable to rename attribute (" + name + " --> " + newName + ")", ex);
            ApplicationLocator.getManager().message(
                Messages.getText("_Unable_to_rename_attribute"),
                JOptionPane.ERROR_MESSAGE);
            // did not even add new field
            return false;
        }

        boolean error_when_inserting = false;
        try {
            // ========== copy value old field -> new field
            FeatureSet fset = fs.getFeatureSet();
            DisposableIterator diter = fset.fastIterator();
            Feature feat = null;
            Object val = null;
            EditableFeature efeat = null;
            while (diter.hasNext()) {
                feat = (Feature) diter.next();
                val = feat.get(name);
                efeat = feat.getEditable();
                efeat.set(newName, val);
                fset.update(efeat);
            }
            diter.dispose();

            // Closing editing to check that store admits new field
            fs.finishEditing();
        } catch (DataException ex) {

            logger.info("Error while renaming att to: " + newName, ex);
            String final_msg = getLastMessage(ex);
            JOptionPane.showMessageDialog(
                root_comp,
                Messages.getText("_Unable_to_rename_attribute")
                + ": " + final_msg,
                Messages.getText("_Rename_column"),
                JOptionPane.ERROR_MESSAGE);
            error_when_inserting = true;
        }

        if (error_when_inserting) {
            try {
                // Trying to remove new field and leave table as it was
                eft.remove(newName);
                fs.update(eft);
            } catch (DataException ex) {
                // Unable to remove added field but user was
                // already notified that something went wrong
            }
            // Not changed
            return false;
        }


        try {
            // Finally reopen editing and delete old field
            fs.edit(FeatureStore.MODE_FULLEDIT);
            eft = fs.getDefaultFeatureType().getEditable();
            eft.remove(name);
            fs.update(eft);

        } catch (DataException ex) {
            logger.info("Unable to rename attribute (" + name + " --> " + newName + ")", ex);
            ApplicationLocator.getManager().message(
                Messages.getText("_Unable_to_rename_attribute"),
                JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
        */

    }



    /**
     * Renames field in feature store
     *
     * @param fs
     * @param oldname
     * @param newname
     * @return
     * @throws DataException
     */
    public static void renameColumn(FeatureStore fs,
        String oldname, String newname) throws DataException {

        FeatureType _ft = fs.getDefaultFeatureType();
        if (_ft.getIndex(newname) != -1) {
            throw new StoreUpdateFeatureTypeException(
                new Exception("Attribute name already existed."),
                fs.getName());
        }
        renameAttribute(fs, oldname, newname);
        // fs.finishEditing();
    }

    public class NewFieldActionListener implements ActionListener {

    	private CreateNewAttributePanel panel = null;
    	private EditableFeatureType eft = null;

    	public NewFieldActionListener(CreateNewAttributePanel p, EditableFeatureType t) {
    		eft = t;
    		panel = p;
    	}

		public void actionPerformed(ActionEvent e) {
            try {
                EditableFeatureAttributeDescriptor ead = panel.loadFieldDescription(eft);
                if (ead == null) {
                    return;
                }
                if (ead.getType() == DataTypes.STRING
                    && ead.getSize() > TableOperations.MAX_FIELD_LENGTH) {
                    NotificationManager.showMessageInfo(
                        PluginServices.getText(this,
                            "max_length_is")
                            + ":"
                            + TableOperations.MAX_FIELD_LENGTH,
                        null);
                    ead.setSize(TableOperations.MAX_FIELD_LENGTH);
                }
                PluginServices.getMDIManager().closeWindow(panel);
            } catch (ParseException e2) {
                NotificationManager.addError(e2);
            }

		}

    }

}
