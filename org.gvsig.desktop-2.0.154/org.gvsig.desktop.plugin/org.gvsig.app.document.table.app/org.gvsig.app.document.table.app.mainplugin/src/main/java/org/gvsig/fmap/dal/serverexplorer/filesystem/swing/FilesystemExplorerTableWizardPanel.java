/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.serverexplorer.filesystem.swing;

import java.awt.Window;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.prepareAction.PrepareContext;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldRequiredValueException;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;

public class FilesystemExplorerTableWizardPanel extends
    FilesystemExplorerWizardPanel {

    private static final long serialVersionUID = 8469934188826417698L;
    private static final Logger LOG = LoggerFactory.getLogger(
        FilesystemExplorerTableWizardPanel.class);

    private PrepareContext prepareDSContext = null;

    @Override
    public void execute() {
        executeWizard();
    }

    @Override
    public Object executeWizard() {
        FeatureStore store;
        TableDocument table;

        ApplicationManager manager = ApplicationLocator.getManager();
        Project project = manager.getCurrentProject();

        PrepareContext context = this.getPrepareDataStoreContext();
        DataStoreParameters[] parameters = this.getParameters();
        List<TableDocument> tabledocs =
            new ArrayList<>(parameters.length);
        
        Set<String> not_valid = new HashSet<>();
        
        for (DataStoreParameters params : parameters) {
            store = null;
            
            /*
             * Try to validate.
             */
            try {
                params.validate();
            } catch (ValidateDataParametersException ecx) {
                StringBuffer buffer = new StringBuffer();
                if( ecx.getCause() instanceof DynObjectValidateException ) {
                    DynObjectValidateException exceptions = (DynObjectValidateException) ecx.getCause();
                    for( int i=0; i<exceptions.size(); i++ ) {
                        if( exceptions.get(i) instanceof DynFieldRequiredValueException ) {
                            DynFieldRequiredValueException exx = (DynFieldRequiredValueException) exceptions.get(i);
                            if( buffer.length()>0 ) {
                                buffer.append(", ");
                            }
                            buffer.append(exx.getMessage());
                        }
                        
                    }
                }
                String msg = params.getDataStoreName();
                if( buffer.length()>0 ) {
                    msg = msg + ": "+buffer.toString();
                }
                LOG.info("Unable to validate params: " + msg);
                not_valid.add(msg);
                continue;
            }
            
            try {

                DataManager dataManager = DALLocator.getDataManager();
                store =
                    (FeatureStore) dataManager.openStore(
                        params.getDataStoreName(), params);
                manager.pepareOpenDataSource(store, context);

                table =
                    (TableDocument) ProjectManager.getInstance()
                        .createDocument(TableManager.TYPENAME);
                table.setName(store.getName());
                table.setStore(store);

                // project.add(table);
                tabledocs.add(table);
            } catch (Exception e) {
                if (store != null) {
                    store.dispose();
                }
                NotificationManager.addError(e);
            }

        }
        
        if (not_valid.size() > 0) {
            String not_str = not_valid.toString();
            JOptionPane.showMessageDialog(
                this,
                Messages.getText("_These_sources_were_not_loaded")
                + ": " + not_str, // not_valid,
                Messages.getText("_Load_error"),
                JOptionPane.WARNING_MESSAGE);
        }
        return tabledocs;
    }

    @Override
    protected PrepareContext getPrepareDataStoreContext() {
        if (this.prepareDSContext == null) {
            this.prepareDSContext = new PrepareContext() {

                @Override
                public Window getOwnerWindow() {
                    return null;
                }
                
                @Override
                public IProjection getViewProjection() {
                	return null;
                }

            };
        }
        return this.prepareDSContext;
    }

    @Override
    public String getTabName() {
        return PluginServices.getText(this, "File");
    }


    @Override
    protected void showPropertiesDialog(final DynObject parameters) {
        FilesystemExplorerPropertiesPanelManager manager = ApplicationLocator.getFilesystemExplorerPropertiesPanelManager();
        FilesystemExplorerPropertiesPanel panel = manager.createPanel(parameters);        
        panel.setExcludeGeometryOptions(true);
        manager.showPropertiesDialog(parameters, panel);
        
        refreshFileList();
    }
    
}
