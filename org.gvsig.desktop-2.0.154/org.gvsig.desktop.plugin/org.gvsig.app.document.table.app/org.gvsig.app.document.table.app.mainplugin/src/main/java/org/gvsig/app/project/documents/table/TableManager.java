/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.app.project.documents.table;

import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.addlayer.AddLayerDialog;
import org.gvsig.app.gui.WizardPanel;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.AbstractDocument;
import org.gvsig.app.project.documents.AbstractDocumentManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.actions.CopyDocumentAction;
import org.gvsig.app.project.documents.actions.CutDocumentAction;
import org.gvsig.app.project.documents.actions.PasteDocumentAction;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.app.project.documents.table.TableDocument.TableLink;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.table.gui.TableProperties;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerWizardPanel;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.VectorLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.persistence.PersistenceManager;

/**
 * Factory of Table.
 * 
 * @author 2005- Vicente Caballero
 * @author 2009- Joaquin del Cerro
 * 
 */
public class TableManager extends AbstractDocumentManager {

    public static final String PERSISTENCE_TABLE_DOCUMENT_DEFINITION_NAME =
        "TableDocument";
    public static final String PERSISTENCE_TABLELINK_DEFINITION_NAME =
        "TableLink";

    public static String TYPENAME = "project.document.table";

    private DynStruct persistenceDefinition = null;

    public ImageIcon getIcon() {
    	return IconThemeHelper.getImageIcon("document-table-icon");
    }

    public ImageIcon getIconSelected() {
        return IconThemeHelper.getImageIcon("document-table-icon-sel");
    }

    public String getTitle() {
        return PluginServices.getText(this, "Tabla");
    }

    public String getTypeName() {
        return TYPENAME;
    }

    public int getPriority() {
        return 1;
    }

    public Iterator<? extends Document> createDocumentsByUser() {
        AddLayerDialog fopen = null;
        try {
            fopen =
                new AddLayerDialog(PluginServices.getText(this, "Nueva_tabla"));
            List<WizardPanel> wizards =
                ApplicationLocator.getManager().getWizardPanels();
            WizardPanel panel;
            Iterator<WizardPanel> iter = wizards.iterator();
            while (iter.hasNext()) {
                panel = iter.next();
                fopen.addWizardTab(panel.getTabName(), panel);
                panel.initWizard();
            }
            PluginServices.getMDIManager().addWindow(fopen);
            if (fopen.isAccepted()) {
                panel = (WizardPanel) fopen.getSelectedTab();
                @SuppressWarnings("unchecked")
                List<TableDocument> docs =
                    (List<TableDocument>) panel.executeWizard();
                return docs.iterator();
            }
        } catch (Exception e) {
            NotificationManager.addError(e);
        } finally {
            if (fopen != null) {
                fopen.dispose();
                fopen = null;
            }
        }
        return null;
    }

    public AbstractDocument createDocumentByUser() {
        return (AbstractDocument) createDocumentsByUser().next();
    }

    /**
     * Registers in the points of extension the Factory with alias.
     * 
     */
    public static void register() {

        TableManager factory = new TableManager();
        // A�adimos nuestra extension para el tratamiento de la apertura de
        // ficheros
        // dentro de gvSIG
        ToolsLocator.getExtensionPointManager().add("FileTableOpenDialog", "")
            .append("FileOpenTable", "", FilesystemExplorerWizardPanel.class);

		IconThemeHelper.registerIcon("document", "document-table-icon", TableManager.class);
        IconThemeHelper.registerIcon("document", "document-table-icon-sel", TableManager.class);
        IconThemeHelper.registerIcon("document", "document-table-icon-small", TableManager.class);

        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        manager.registerFactory(factory);

        if (factory.persistenceDefinition == null) {
            DynObjectManager dynman = ToolsLocator.getDynObjectManager();
            factory.persistenceDefinition =
                dynman.createDynClass(PersistenceManager.PERSISTENCE_NAMESPACE,
                    PERSISTENCE_TABLE_DOCUMENT_DEFINITION_NAME,
                    "Table document Persistence definition");
            factory.persistenceDefinition.extend(manager
                .getDefinition(AbstractDocument.PERSISTENCE_DEFINITION_NAME));

            factory.persistenceDefinition.addDynFieldObject("store")
                .setClassOfValue(FeatureStore.class).setMandatory(true);
            factory.persistenceDefinition.addDynFieldString("featureTypeId")
                .setMandatory(false);
            factory.persistenceDefinition.addDynFieldArray("attributeNames")
                .setClassOfItems(String.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldObject("associatedLayer")
                .setClassOfValue(FLyrVect.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldObject("query")
                .setClassOfValue(FeatureQuery.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldObject("baseFilter")
                .setClassOfValue(Evaluator.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldObject("baseOrder")
                .setClassOfValue(FeatureQueryOrder.class).setMandatory(false);
            factory.persistenceDefinition.addDynFieldList("linkTable")
                .setClassOfItems(TableLink.class).setMandatory(false);          
        }
        
        
        //Register also the TableLink
        if (manager.getDefinition(PERSISTENCE_TABLELINK_DEFINITION_NAME) == null){
            DynStruct tableLinkDefinition =
                manager.addDefinition(TableLink.class,
                    PERSISTENCE_TABLELINK_DEFINITION_NAME,
                    "TableLink Persistence definition", null, null);                 
            
            tableLinkDefinition.addDynFieldObject("source")
                .setClassOfValue(TableDocument.class).setMandatory(true);
            tableLinkDefinition.addDynFieldObject("target")
                .setClassOfValue(TableDocument.class).setMandatory(true);
            tableLinkDefinition.addDynFieldString("fieldSource").setMandatory(true);
            tableLinkDefinition.addDynFieldString("fieldTarget").setMandatory(true);
            tableLinkDefinition.addDynFieldBoolean("enabled").setMandatory(true);
        }  
    
        ProjectManager.getInstance().registerDocumentFactory(factory);
        
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new CopyDocumentAction());
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new CutDocumentAction());
        ProjectManager.getInstance().registerDocumentAction(TYPENAME,new PasteDocumentAction());

    }

    /**
     * Create a new table document.
     * 
     * @return TableDocument.
     */
    public AbstractDocument createDocument() {
        AbstractDocument doc = new TableDocument(this);
        if( this.notifyObservers(NOTIFY_AFTER_CREATEDOCUMENT,doc).isCanceled() ) {
            return null;
        }
    	return doc;        
    }

    public Class<? extends IDocumentWindow> getMainWindowClass() {
        return FeatureTableDocumentPanel.class;
    }

    @Override
    public IWindow getMainWindow(Document doc, WindowLayout layout) {
       IDocumentWindow win = (IDocumentWindow) super.getMainWindow(doc, layout);
        if (win == null) {
            win = (IDocumentWindow) this.createDocumentWindow(doc);
            if (layout != null && win != null) {
                win.setWindowLayout(layout);
            }
        }
        if( this.notifyObservers(NOTIFY_AFTER_GETMAINWINDOW,win).isCanceled() ) {
            return null;
        }
        return win;
    }

    @Override
    public IWindow getPropertiesWindow(Document doc) {
        IWindow win = super.getPropertiesWindow(doc);
        if( win == null ) {
            win = new TableProperties((TableDocument) doc);
        }
        if( this.notifyObservers(NOTIFY_AFTER_GETPROPERTIESWINDOW,win).isCanceled() ) {
            return null;
        }
        return win;        
    }

    protected Class<? extends Document> getDocumentClass() {
        return TableDocument.class;
    }

    public DynStruct getDefinition(String className) {

        if (this.persistenceDefinition.getName().equalsIgnoreCase(className)) {
            return this.persistenceDefinition;
        }
        if (this.persistenceDefinition.getFullName()
            .equalsIgnoreCase(className)) {
            return this.persistenceDefinition;
        }
        if (this.getDocumentClass().getName().equals(className)) {
            return this.persistenceDefinition;
        }
        return null;
    }

    public boolean manages(Object object) {
        return object instanceof TableDocument;
    }

    public TableDocument getTableDocument(VectorLayer layer) {
        if (layer == null) {
            return null;
        }
        List<Document> tableDocs =
            getProject().getDocuments(TableManager.TYPENAME);
        for (Document document : tableDocs) {
            if (layer == ((TableDocument) document).getAssociatedLayer()) {
                return (TableDocument) document;
            }
        }
        return null;
    }

    public void removeTableDocument(VectorLayer layer) {
        TableDocument doc = getTableDocument(layer);
        // Only remove it if it exists
        if (doc != null) {
            PluginServices.getMDIManager().closeSingletonWindow(doc);
            getProject().remove(doc);
        }
    }

    private Project getProject() {
        return ProjectManager.getInstance().getCurrentProject();
    }
}
