/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.TableOperations;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTableEditExtension extends Extension {
    
    protected static final Logger logger = LoggerFactory.getLogger(AbstractTableEditExtension.class);

    protected FeatureTableDocumentPanel table = null;
    protected TableOperations featureTableOperations = null;

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
        featureTableOperations = TableOperations.getInstance();
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (v != null
            && v instanceof FeatureTableDocumentPanel
            && ((FeatureTableDocumentPanel) v).getModel().getStore()
                .isEditing()) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        }
        return false;
    }

}
