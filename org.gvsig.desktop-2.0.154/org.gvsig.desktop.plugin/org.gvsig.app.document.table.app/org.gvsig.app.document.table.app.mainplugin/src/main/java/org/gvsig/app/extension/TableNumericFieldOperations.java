/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.math.BigDecimal;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.table.gui.Statistics;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * @author 2004-2005 Fernando Gonz�lez Cort�s
 * @author 2005- Vicente Caballero
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class TableNumericFieldOperations extends Extension {

    private FeatureTableDocumentPanel table;

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
       IconThemeHelper.registerIcon("action", "table-statistics", this);
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
    	if( "table-statistics".equalsIgnoreCase(actionCommand)) {
	        IWindow v = PluginServices.getMDIManager().getActiveWindow();
	
	        if (v != null) {
	            if (v.getClass() == FeatureTableDocumentPanel.class) {
	
	                FeatureTableDocumentPanel table = (FeatureTableDocumentPanel) v;
	
	                doExecute(table);
	            }
	        }
    	}
    	
    }

    /**
     * "execute" method acction
     * 
     * @param actionCommand
     *            The acction command that executes this method
     * @param table
     *            Table to operate
     */
    protected void doExecute(FeatureTableDocumentPanel table) {
        FeatureSet featureSet = null;
        DisposableIterator iterator = null;
        try {
            FeatureAttributeDescriptor fad =
                table.getTablePanel().getTable()
                    .getSelectedColumnsAttributeDescriptor()[0];
            long numRows = 0;
            FeatureStore fs = table.getModel().getStore();

            // Get the iterator from the selection or all the data
            FeatureSelection selection = fs.getFeatureSelection();
            if (selection.isEmpty()) {
                FeatureQuery fq = fs.createFeatureQuery();
                fq.setAttributeNames(new String[] { fad.getName() });
                featureSet = fs.getFeatureSet();
                numRows = featureSet.getSize();
                iterator = featureSet.fastIterator();
            } else {
                numRows = selection.getSize();
                iterator = selection.fastIterator();
            }

            // Read all values and calculate the min, max and the sum.
            // Also, calculate what we can for the variance
            BigDecimal sum = BigDecimal.ZERO;
            double min = Double.MAX_VALUE;
            double max = -Double.MAX_VALUE;
            BigDecimal variance = BigDecimal.ZERO;
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Number valueNumber = (Number) feature.get(fad.getName());
                if (valueNumber == null) {
                    continue;
                }
                double value = valueNumber.doubleValue();
                BigDecimal valueBig = BigDecimal.valueOf(value);
                sum = sum.add(valueBig);
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }
                variance = variance.add(valueBig.pow(2));
            }

            // Calculate the average, the variance final value and the standard
            // desviation
            BigDecimal average;
            double desviation = 0.0d;
            if (numRows < 1l) {
                average = BigDecimal.ZERO;
            } else {
                BigDecimal numRowsBig = BigDecimal.valueOf(numRows);
                average = sum.divide(numRowsBig, BigDecimal.ROUND_HALF_DOWN);
                variance =
                    variance.divide(numRowsBig, BigDecimal.ROUND_HALF_DOWN)
                        .subtract(average.pow(2));
                desviation = Math.sqrt(variance.doubleValue());
            }

            // Create and shot the statistics window
            Statistics st = new Statistics();
            st.setStatistics(average.doubleValue(), max, min,
                variance.doubleValue(), desviation, numRows, max - min,
                sum.doubleValue());
            PluginServices.getMDIManager().addWindow(st);
        } catch (DataException e) {
            NotificationManager.addError(e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
            if (featureSet != null) {
                featureSet.dispose();
            }
        }
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        return doIsEnabled(table);
    }

    protected boolean doIsEnabled(FeatureTableDocumentPanel table) {
        // FIXME
        FeatureAttributeDescriptor[] fads = null;
        try {
            fads =
                table.getTablePanel().getTable()
                    .getSelectedColumnsAttributeDescriptor();
        } catch (DataException e) {
            e.printStackTrace();
        }

        if (fads.length == 1) {
            // int index=indices.nextSetBit(0);
            // if
            // (table.getModel().getStore().getDefaultFeatureType().size()<index+1)
            // {
            // return false;
            // }
            int type = fads[0].getType();
            if ((type == DataTypes.LONG) || (type == DataTypes.DOUBLE)
                || (type == DataTypes.FLOAT) || (type == DataTypes.INT)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (v != null && v instanceof FeatureTableDocumentPanel) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        }
        return false;
    }

}
