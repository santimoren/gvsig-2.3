/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.gui.toc.ShowAttributesTableTocMenuEntry;
import org.gvsig.fmap.dal.serverexplorer.filesystem.swing.FilesystemExplorerTableWizardPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;

/**
 * A gvSIG {@link Extension} to register the Table document type.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class TableExtension extends Extension {

    public void initialize() {
    	
        ExtensionPointManager epMan = ToolsLocator.getExtensionPointManager();
        epMan.add("DocumentActions_Table",
            "Context menu options of the table document list"
                + " in the project window " + "(register instances of "
                + "org.gvsig.app.project.AbstractDocumentContextMenuAction)");
        
        // Adding TOC menu entry
    	ExtensionPoint exPoint = ToolsLocator.getExtensionPointManager().add(
				"View_TocActions", "");
		exPoint.append(
				ShowAttributesTableTocMenuEntry.EXTENSION_POINT_NAME,
				"TOC popup menu to show vector layer's attributes table",
				new ShowAttributesTableTocMenuEntry());
    }

    public void postInitialize() {
        TableManager.register();
        ApplicationLocator.getManager().registerAddTableWizard("File",
            "File Table", FilesystemExplorerTableWizardPanel.class);
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return true;
    }

    public void execute(String actionCommand) {
        // Nothing to do
    }

}
