/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableOperations;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.table.gui.FeatureTypeEditingPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;

/**
 * Extensión que abre la ventana para cambiar la configuración de la estructura
 * de la tabla.
 * 
 * @author Vicente Caballero Navarro
 */
public class TableEditAttributes extends Extension {

    private FeatureTableDocumentPanel table = null;
    private TableOperations featureTableOperations = null;

    /**
     * @see com.iver.mdiApp.plugins.IExtension#updateUI(java.lang.String)
     */
    public void execute(String s) {
    	if( "table-column-manager".equalsIgnoreCase(s)) {
	        TableDocument pt = table.getModel();
	        FeatureStore fs = pt.getStore();
	        try {
	            PluginServices.getMDIManager().addWindow(
	                new FeatureTypeEditingPanel(fs));
	        } catch (DataException e) {
	            NotificationManager.addError(
	                PluginServices.getText(this, "create_editabletype"), e);
	        }
    	} else {

    		try {
                if ("table-add-column".equals(s)) {
                	featureTableOperations.setTablePanel(table);
                    featureTableOperations.insertAttributes(table
                        .getTablePanel().getTable());
                }
    		} catch (DataException de) {
                NotificationManager.showMessageError(
                		Messages.getText("update_featuretype_error"), null);
    		}
    		
    	}
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    	IconThemeHelper.registerIcon("action", "table-column-manager", this);
		IconThemeHelper.registerIcon("action", "table-add-column", this);
		featureTableOperations = TableOperations.getInstance();
    }

    public boolean isEnabled() {
        FeatureStore fs = table.getModel().getStore();
        return fs.isEditing();
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (v != null && v instanceof FeatureTableDocumentPanel) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        }
        return false;
    }

}
