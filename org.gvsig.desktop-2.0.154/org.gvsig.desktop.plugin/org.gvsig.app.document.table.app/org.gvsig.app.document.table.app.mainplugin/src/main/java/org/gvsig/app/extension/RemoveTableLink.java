/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;

/**
 * Extensi�n para borrar los link existentes entre tablas.
 * 
 * @author Vicente Caballero Navarro
 */
public class RemoveTableLink extends Extension {

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    	IconThemeHelper.registerIcon("action", "table-remove-link", this);
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
    	if( "table-remove-link".equalsIgnoreCase(actionCommand)) {
	        FeatureTableDocumentPanel t =
	            (FeatureTableDocumentPanel) PluginServices.getMDIManager()
	                .getActiveWindow();
	        TableDocument pt = t.getModel();
	        pt.removeLinkTable();// restoreDataSource();
	        t.getModel().setModified(true);
    	}
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        }

        if (v.getClass() == FeatureTableDocumentPanel.class) {
            FeatureTableDocumentPanel t = (FeatureTableDocumentPanel) v;

            if (t.getModel().hasLinks()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        }

        if (v instanceof FeatureTableDocumentPanel) {
            return true;
        } else {
            return false;
        }
    }
}
