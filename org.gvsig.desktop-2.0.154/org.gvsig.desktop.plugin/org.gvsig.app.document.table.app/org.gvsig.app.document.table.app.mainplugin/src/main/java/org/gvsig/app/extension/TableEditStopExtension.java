/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.andami.plugins.status.IExtensionStatus;
import org.gvsig.andami.plugins.status.IUnsavedData;
import org.gvsig.andami.plugins.status.UnsavedData;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.gui.beans.Messages;
import org.gvsig.tools.util.ArrayUtils;
import org.gvsig.utils.swing.threads.IMonitorableTask;

public class TableEditStopExtension extends AbstractTableEditExtension {

    private static Logger logger
            = LoggerFactory.getLogger(TableEditStopExtension.class);

    @Override
    public void initialize() {
        super.initialize();
        IconThemeHelper.registerIcon("action", "table-stop-editing", this);
    }

    public void execute(String actionCommand) {
        this.execute(actionCommand,null);
    }

    @Override
    public void execute(String actionCommand, Object[] args) {
        if ("table-stop-editing".equals(actionCommand)) {
            TableDocument doc = (TableDocument)ArrayUtils.get(args,0);
            if( doc == null ) {
                doc = (TableDocument) table.getDocument();
            }    
            EditingNotificationManager editingNotification = DALSwingLocator.getEditingNotificationManager();
            EditingNotification notification = editingNotification.notifyObservers(
                    this,
                    EditingNotification.BEFORE_ENTER_EDITING_STORE,
                    doc,
                    doc.getStore());
            if (notification.isCanceled()) {
                return;
            }
            stopEditing(table);
            ApplicationLocator.getManager().refreshMenusAndToolBars();
            editingNotification.notifyObservers(
                    this,
                    EditingNotification.AFTER_ENTER_EDITING_STORE,
                    doc,
                    doc.getStore());
        }
    }

    private void stopEditing(FeatureTableDocumentPanel table) {

        Object[] options = {
            PluginServices.getText(this, "_Guardar"),
            "       " + PluginServices.getText(this, "_Descartar") + "       ",
            PluginServices.getText(this, "_Continuar")};

        JPanel explanation_panel = getExplanationPanel(table.getModel().getName());

        int resp = JOptionPane
                .showOptionDialog(
                        (Component) PluginServices.getMainFrame(),
                        explanation_panel,
                        PluginServices.getText(this, "stop_edition"),
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options,
                        options[2]);

        try {
            if (resp == JOptionPane.NO_OPTION) {
                // CANCEL EDITING
                table.getModel().getStore().cancelEditing();
            } else {

                if (resp == JOptionPane.YES_OPTION) {
                    // Save table
                    table.getModel().getStore().finishEditing();
                } else {
                    // This happens when user clicks on [x]
                    // to abruptly close previous JOptionPane dialog
                    // We do nothing (equivalent to 'Continue editing')
                }
            }
        } catch (DataException e) {
            logger.error("While finishing or canceling table editing: "
                    + e.getMessage(), e);
        }
    }

    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        } else if (v instanceof FeatureTableDocumentPanel
                && ((FeatureTableDocumentPanel) v).getModel().getStore()
                .isEditing()) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        } else {
            return false;
        }
    }

    /**
     * <p>
     * This class provides the status of extensions. If this extension has some
     * unsaved editing table (and save them), and methods to check if the
     * extension has some associated background tasks.
     *
     */
    private class StopEditingStatus implements IExtensionStatus {

        /**
         * This method is used to check if this extension has some unsaved
         * editing tables.
         *
         * @return true if the extension has some unsaved editing tables, false
         * otherwise.
         */
        public boolean hasUnsavedData() {
            Project project = ProjectManager.getInstance().getCurrentProject();
            List<Document> tables = project.getDocuments(TableManager.TYPENAME);
            for (int i = 0; i < tables.size(); i++) {
                FeatureStore store = ((TableDocument) tables.get(i)).getStore();
                if (store == null) {
                    continue;
                }
                if (store.isEditing()) {
                    return true;
                }
            }
            return false;
        }

        /**
         * This method is used to check if the extension has some associated
         * background process which is currently running.
         *
         * @return true if the extension has some associated background process,
         * false otherwise.
         */
        public boolean hasRunningProcesses() {
            return false;
        }

        /**
         * <p>
         * Gets an array of the traceable background tasks associated with this
         * extension. These tasks may be tracked, canceled, etc.
         * </p>
         *
         * @return An array of the associated background tasks, or null in case
         * there is no associated background tasks.
         */
        public IMonitorableTask[] getRunningProcesses() {
            return null;
        }

        /**
         * <p>
         * Gets an array of the UnsavedData objects, which contain information
         * about the unsaved editing tables and allows to save it.
         * </p>
         *
         * @return An array of the associated unsaved editing layers, or null in
         * case the extension has not unsaved editing tables.
         */
        public IUnsavedData[] getUnsavedData() {
            Project project = ProjectManager.getInstance().getCurrentProject();
            List<Document> tables = project.getDocuments(TableManager.TYPENAME);
            List<UnsavedTable> unsavedTables = new ArrayList<UnsavedTable>();
            for (int i = 0; i < tables.size(); i++) {
                TableDocument table = (TableDocument) tables.get(i);
                FeatureStore store = table.getStore();
                if (store == null) {
                    continue;
                }
                if (store.isEditing()) {
                    UnsavedTable ul
                            = new UnsavedTable(TableEditStopExtension.this);
                    ul.setTable(table);
                    unsavedTables.add(ul);
                }
            }
            return unsavedTables
                    .toArray(new IUnsavedData[unsavedTables.size()]);
        }
    }

    private class UnsavedTable extends UnsavedData {

        private TableDocument table;

        public UnsavedTable(IExtension extension) {
            super(extension);
        }

        public String getDescription() {
            return PluginServices.getText(this, "editing_table_unsaved");
        }

        public String getResourceName() {
            return table.getName();
        }

        public boolean saveData() {
            return executeSaveTable(table);
        }

        public void setTable(TableDocument table) {
            this.table = table;
        }

        @Override
        public String getIcon() {
            return "document-table-icon-small";
        }
    }

    // TODO Este codigo esta duplicado, tambien esta en la clase Table en el
    // metodo "public void stopEditing()"
    private boolean executeSaveTable(TableDocument table2) {
        FeatureStore fs = table2.getStore();
        if (fs.isEditing()) {
            try {
                fs.finishEditing();
            } catch (WriteException e) {
                NotificationManager.addError(PluginServices.getText(this, "error_saving_table"), e);
                return false;
            } catch (ReadException e) {
                NotificationManager.addError(PluginServices.getText(this, "error_saving_table"), e);
                return false;
            } catch (DataException e) {
                NotificationManager.addError(PluginServices.getText(this, "error_saving_table"), e);
                return false;
            }
        }
        return true;
    }

    @Override
    public IExtensionStatus getStatus() {
        return new StopEditingStatus();
    }

    private JPanel getExplanationPanel(String name) {

        BorderLayout bl = new BorderLayout(10, 10);
        JPanel resp = new JPanel(bl);

        String msg = Messages.getText("realmente_desea_guardar");
        JLabel topLabel = new JLabel(msg);

        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints cc = new GridBagConstraints();

        cc.gridx = 0;
        cc.gridy = 0;
        cc.anchor = GridBagConstraints.WEST;

        cc.insets = new Insets(3, 6, 3, 6);

        Font boldf = mainPanel.getFont().deriveFont(Font.BOLD);

        JLabel lbl = new JLabel(Messages.getText("_Guardar"));
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(Messages.getText("_Save_changes_performed")), cc);

        cc.gridx = 0;
        cc.gridy = 1;
        lbl = new JLabel(Messages.getText("_Descartar"));
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(Messages.getText("_Discard_and_lose_changes")), cc);

        cc.gridx = 0;
        cc.gridy = 2;
        lbl = new JLabel(Messages.getText("_Continuar"));
        lbl.setFont(boldf);
        mainPanel.add(lbl, cc);
        cc.gridx = 1;
        mainPanel.add(new JLabel(Messages.getText("_Do_not_save_yet_Stay_in_editing_mode")), cc);

        resp.add(mainPanel, BorderLayout.CENTER);
        resp.add(topLabel, BorderLayout.NORTH);
        return resp;
    }
}
