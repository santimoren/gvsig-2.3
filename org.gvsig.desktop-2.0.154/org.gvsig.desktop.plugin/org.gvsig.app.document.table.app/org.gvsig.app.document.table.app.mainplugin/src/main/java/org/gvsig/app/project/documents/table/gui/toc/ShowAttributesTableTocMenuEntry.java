/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.table.gui.toc;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.IExtension;
import org.gvsig.app.extension.ShowTable;
import org.gvsig.app.project.documents.view.toc.AbstractTocContextMenuAction;
import org.gvsig.app.project.documents.view.toc.ITocItem;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.i18n.Messages;



/**
 * Shows att table
 *
 * @author jldominguez
 */
public class ShowAttributesTableTocMenuEntry
extends AbstractTocContextMenuAction {
	
	public static final String EXTENSION_POINT_NAME = "ShowAttributesTable";
	private IExtension ext = null;
	
	public String getGroup() {
		return "group2"; 
	}

	public int getGroupOrder() {
		return 20;
	}

	public int getOrder() {
		return -1;
	}

	public String getText() {
		return Messages.getText("_Attributes_table");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return getExtension().isEnabled();
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		return getExtension().isVisible();
	}

	public void execute(ITocItem item, FLayer[] selectedItems) {
		getExtension().execute("layer-show-attributes-table");
	}
	
	private IExtension getExtension() {
		
		if (ext == null) {
			ext = PluginServices.getExtension(ShowTable.class);		
		}
		return ext;
	}
}
