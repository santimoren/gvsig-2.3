/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionListener;
import org.gvsig.fmap.mapcontext.layers.LayerPositionEvent;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.util.ArrayUtils;

/**
 * Extensi�n que abre las tablas asociadas a las vistas.
 *
 */
public class ShowTable extends Extension implements LayerCollectionListener {

    @Override
    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument doc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if (doc == null) {
            return false;
        }
        MapContext mapContext = doc.getMapContext();
        return mapContext.hasActiveVectorLayers();
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument doc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
        if (doc == null) {
            return false;
        }
        MapContext mapContext = doc.getMapContext();
        return mapContext.hasVectorLayers();
    }

    @Override
    public void execute(String command) {
        this.execute(command, null);
    }

    @Override
    public void execute(String command, Object[] args) {
        if ("layer-show-attributes-table".equalsIgnoreCase(command)) {
            ApplicationManager application = ApplicationLocator.getManager();
            FLayer[] layers = (FLayer[]) ArrayUtils.get(args, 0);
            if( layers == null ) {
                ViewDocument doc = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
                if (doc == null) {
                    return;
                }
                MapContext mapContext = doc.getMapContext();
                layers = mapContext.getLayers().getActives();
            }
            Project project = application.getCurrentProject();
            TableManager tableManager = getTableManager();

            for (FLayer layer : layers) {
                if (layer instanceof FLyrVect) {
                    FLyrVect layerVect = (FLyrVect) layer;
                    TableDocument tableDoc = tableManager.getTableDocument(layerVect);
                    if (tableDoc == null) {
                        tableDoc = (TableDocument) tableManager.createDocument();
                        tableDoc.setName(PluginServices.getText(this,
                                "Tabla_de_Atributos")
                                + ": "
                                + layerVect.getName());
                        tableDoc.setStore(layerVect.getFeatureStore());
                        tableDoc.setAssociatedLayer(layerVect);
                        layerVect.getParentLayer().addLayerCollectionListener(this);
                        project.addDocument(tableDoc);
                    }
                    IWindow tablePanel = tableManager.getMainWindow(tableDoc);
                    tableDoc.setModified(true);
                    application.getUIManager().addWindow(tablePanel);
                }
            }
        }
    }

    private TableManager getTableManager() {
        ApplicationManager application = ApplicationLocator.getManager();
        DocumentManager tableManager = application.getProjectManager().getDocumentManager(
                TableManager.TYPENAME
        );
        return (TableManager) tableManager;
    }

    @Override
    public void initialize() {
        IconThemeHelper.registerIcon("action", "layer-show-attributes-table", this);
    }

    @Override
    public void layerAdded(LayerCollectionEvent e) {
        // Nothing to do
    }

    @Override
    public void layerMoved(LayerPositionEvent e) {
        // Nothing to do
    }

    @Override
    public void layerRemoved(LayerCollectionEvent e) {
        FLayer layer = e.getAffectedLayer();
        // Just in case we where listening to a group of layers being removed
        // remove us from there
        if (layer instanceof FLayers) {
            ((FLayers) layer).removeLayerCollectionListener(this);
        }
        // Remove the related table document, if any
        if (layer instanceof FLyrVect) {
            getTableManager().removeTableDocument((FLyrVect) layer);
            // If the parent layers has not other child layers, for sure there
            // are not related tables opened, don't need to listen
            // LayerCollectionEvents anymore.
            // TODO: remove us also when there are not any child layers with
            // related table documents
            FLayers layers = layer.getParentLayer();
            if (layers != null && layers.getLayersCount() == 0) {
                layers.removeLayerCollectionListener(this);
            }
        }
    }

    @Override
    public void layerAdding(LayerCollectionEvent e) throws CancelationException {
        // Nothing to do
    }

    @Override
    public void layerMoving(LayerPositionEvent e) throws CancelationException {
        // Nothing to do
    }

    @Override
    public void layerRemoving(LayerCollectionEvent e)
            throws CancelationException {
        // Nothing to do
    }

    @Override
    public void visibilityChanged(LayerCollectionEvent e)
            throws CancelationException {
        // Nothing to do
    }
}
