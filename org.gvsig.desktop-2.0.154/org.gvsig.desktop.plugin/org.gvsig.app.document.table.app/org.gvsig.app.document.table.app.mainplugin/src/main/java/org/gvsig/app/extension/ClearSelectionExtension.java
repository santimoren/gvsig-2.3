/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSelection;

/**
 * Extensi�n encargada de limpiar la selecci�n.
 * 
 * @author Vicente Caballero Navarro
 */
public class ClearSelectionExtension extends Extension {

    public void execute(String s) {
        if (s.compareTo("selection-clear-table") == 0) {
            IWindow activeWindow =
                PluginServices.getMDIManager().getActiveWindow();

            if (isFeatureTableDocumentPanel(activeWindow)) {
                try {
                    FeatureSelection selection = getSelection(activeWindow);
                    selection.deselectAll();
                } catch (DataException e) {
                    NotificationManager.addError(e);
                }
            }
        }
    }

    public boolean isVisible() {
        IWindow activeWindow = PluginServices.getMDIManager().getActiveWindow();
        return isFeatureTableDocumentPanel(activeWindow);
    }

    private boolean isFeatureTableDocumentPanel(IWindow activeWindow) {
        return activeWindow instanceof FeatureTableDocumentPanel;
    }

    public boolean isEnabled() {
        IWindow activeWindow = PluginServices.getMDIManager().getActiveWindow();

        if (isFeatureTableDocumentPanel(activeWindow)) {
            try {
                return !getSelection(activeWindow).isEmpty();
            } catch (DataException e) {
                throw new RuntimeException(
                    "Error getting the selection to check if it is empty", e);
            }
        }

        return false;
    }

    private FeatureSelection getSelection(IWindow activeWindow)
        throws DataException {
        return (FeatureSelection) ((FeatureTableDocumentPanel) activeWindow)
            .getModel().getStore().getSelection();
    }

    public void initialize() {
        IconThemeHelper.registerIcon("action", "selection-clear", this);
    }

}
