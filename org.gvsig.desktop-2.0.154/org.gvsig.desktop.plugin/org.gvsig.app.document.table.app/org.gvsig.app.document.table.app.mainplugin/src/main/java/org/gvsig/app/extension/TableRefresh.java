/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.table.gui.GeneralTablePropertiesPageFactory;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.AbstractViewPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionListener;
import org.gvsig.fmap.mapcontext.layers.LayerPositionEvent;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.propertypage.PropertiesPageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extensi�n que refresca los datos de la tabla.
 *
 * @author dmartinez
 */
public class TableRefresh extends Extension  {
    private static final Logger LOG = LoggerFactory
        .getLogger(TableRefresh.class);

    public void initialize() {

    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        Document doc = application.getActiveDocument(TableManager.TYPENAME);
        return doc != null;
    }

    /**
     * @see com.iver.mdiApp.plugins.IExtension#updateUI(java.lang.String)
     */
    public void execute(String s) {
        if ("table-refresh".equalsIgnoreCase(s)) {
            ApplicationManager application = ApplicationLocator.getManager();
            Document doc = application.getActiveDocument(TableManager.TYPENAME);
            if (doc instanceof TableDocument){
                TableDocument docTable=(TableDocument)doc;
                try {
                    docTable.getStore().refresh();
                } catch (DataException e) {
                    LOG.warn("Error refreshing table", e);
                }
            }
        }
	}
}
