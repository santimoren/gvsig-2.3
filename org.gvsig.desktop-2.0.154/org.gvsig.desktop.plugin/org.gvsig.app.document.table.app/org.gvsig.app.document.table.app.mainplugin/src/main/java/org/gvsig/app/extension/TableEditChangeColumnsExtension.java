/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.dal.exception.DataException;

/**
 * DOCUMENT ME!
 * 
 * @author Vicente Caballero Navarro
 */
public class TableEditChangeColumnsExtension extends AbstractTableEditExtension {

	public void initialize() {
		super.initialize();
		IconThemeHelper.registerIcon("action", "table-rename-column", this);
		IconThemeHelper.registerIcon("action", "table-remove-column", this);
	}
    /**
     * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
        try {
            featureTableOperations.setTablePanel(table);
            if ("table-remove-column".equals(actionCommand)) {
                featureTableOperations.deleteAttributes(table.getTablePanel()
                    .getTable());
            } else
                    if ("table-rename-column".equals(actionCommand)) {
                        featureTableOperations.renameAttributes(table
                            .getTablePanel().getTable());
                    }
        } catch (DataException e) {
            NotificationManager.showMessageError(
                PluginServices.getText(this, "update_featuretype_error"), null);
        }

    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        try {
            if (table.getTablePanel().getTable().getSelectedColumnCount() > 0) {
                return true;
            }
        } catch (DataException e) {
            return false;
        }
        return false;
    }
}
