/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.gui.command.CommandStackDialog;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;


/**
 * DOCUMENT ME!
 *
 * @author Vicente Caballero Navarro
 */
public class TableCommandStackExtension extends Extension {
	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
        IconThemeHelper.registerIcon("action", "edit-undo-redo-actions", this);
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String s) {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		.getActiveWindow();

		FeatureTableDocumentPanel table = (FeatureTableDocumentPanel) f;
		TableDocument model = table.getModel();
		if (s.equals("edit-undo-redo-actions-table")) {
			FeatureStore featureStore = model.getStore();
			featureStore.addObserver(table);
			CommandStackDialog csd = new CommandStackDialog();

			csd.setModel(featureStore);

			PluginServices.getMDIManager().addWindow(csd);
		}
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
		return true;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
				.getActiveWindow();
		if (f instanceof FeatureTableDocumentPanel){
			FeatureTableDocumentPanel table = (FeatureTableDocumentPanel) f;
			TableDocument model = table.getModel();
			if (model.getStore().isEditing())
				return true;
		}
		return false;
	}

}
