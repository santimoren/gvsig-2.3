/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.util.Iterator;

import javax.swing.JOptionPane;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.filter.ExpressionListener;
import org.gvsig.app.gui.filter.FilterDialog;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.i18n.Messages;
import org.gvsig.utils.exceptionHandling.ExceptionListener;

/**
 * Extensi�n que abre un di�logo para poder hacer un filtro de una capa o tabla.
 * 
 * @author Vicente Caballero Navarro
 */
public class SelectByAttributesExtension extends Extension implements ExpressionListener {

    protected FeatureStore featureStore = null;
    protected FeatureTableDocumentPanel table;
    private String filterTitle;

    public void initialize() {
        registerIcons();
    }

    private void registerIcons() {
    	IconThemeHelper.registerIcon("action", "selection-by-attributes", this);
    }

    public void execute(String actionCommand) {
        if ("selection-by-attributes-table".equals(actionCommand)) {
            IWindow v = PluginServices.getMDIManager().getActiveWindow();

            if (v instanceof FeatureTableDocumentPanel) {
                table = (FeatureTableDocumentPanel) v;

                featureStore = table.getModel().getStore();
                filterTitle = table.getModel().getName();
                table.getModel().setModified(true);
            }

            doExecute();
        }
    }

    protected void doExecute() {
        FilterDialog dlg = new FilterDialog(filterTitle);
        dlg.addExpressionListener(this);
        dlg.addExceptionListener(new ExceptionListener() {

            public void exceptionThrown(Throwable t) {
                NotificationManager.addError(t.getMessage(), t);
            }
        });
        dlg.setModel(featureStore);
        PluginServices.getMDIManager().addWindow(dlg);
    }

    public boolean isEnabled() {
        return isVisible();
    }

    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        return (v instanceof FeatureTableDocumentPanel);
    }

    // if no filter expression -> no element selected
    public void newSet(String expression) throws DataException {
        if (!this.filterExpressionFromWhereIsEmpty(expression)) {
            FeatureSet set = null;
            try {
                set = doSet(expression);

                if (set == null) {
                    return;
                }
                featureStore.setSelection(set);

            } catch (Exception e) {
                
                JOptionPane.showMessageDialog(
                    ApplicationLocator.getManager().getRootComponent(),
                    Messages.getText("_Invalid_expression") + ":\n"
                        + getLastMessage(e),
                    Messages.getText("_Invalid_expression"),
                    JOptionPane.ERROR_MESSAGE);
                
            } finally {
                if (set != null) {
                    set.dispose();
                }
            }
        } else {
            // if no expression -> no element selected
            featureStore.getFeatureSelection().deselectAll();
        }
    }

    private FeatureSet doSet(String expression) throws DataException {
        FeatureQuery query = featureStore.createFeatureQuery();
        DataManager manager = DALLocator.getDataManager();
        query.setFilter(manager.createExpresion(expression));
        return featureStore.getFeatureSet(query);
    }

    public void addToSet(String expression) throws DataException {
        // if no filter expression -> don't add more elements to set
        if (!this.filterExpressionFromWhereIsEmpty(expression)) {
            FeatureSet set = null;
            try {
                set = doSet(expression);

                if (set == null) {
                    return;
                }
                featureStore.getFeatureSelection().select(set);
            } finally {
                if (set != null) {
                    set.dispose();
                }
            }
        }
    }

    public void fromSet(String expression) throws DataException {
        if ( !this.filterExpressionFromWhereIsEmpty(expression) ) {
            FeatureSet set = null;
            try {
                set = doSet(expression);
                if ( set == null ) {
                    return;
                }
                FeatureSelection oldSelection = featureStore.getFeatureSelection();
                FeatureSelection newSelection = featureStore.createFeatureSelection();
                Iterator iterator = set.iterator();
                while ( iterator.hasNext() ) {
                    Feature feature = (Feature) iterator.next();
                    if ( oldSelection.isSelected(feature) ) {
                        newSelection.select(feature);
                    }
                }
                featureStore.setSelection(newSelection);
            } finally {
                if ( set != null ) {
                    set.dispose();
                }
            }
        } else {
            featureStore.getFeatureSelection().deselectAll();
        }
    }
    
    /**
     * Returns true if the WHERE subconsultation of the filterExpression is
     * empty ("")
     * 
     * @author Pablo Piqueras Bartolom� (p_queras@hotmail.com)
     * @param expression
     *            An string
     * @return A boolean value
     */
    private boolean filterExpressionFromWhereIsEmpty(String expression) {
        
        if (expression == null) {
            return true;
        }
        
        String subExpression = expression.trim();
        
        if (subExpression.length() == 0) {
            return true;
        }
        
        int pos;

        // Remove last ';' if exists
        if (subExpression.charAt(subExpression.length() - 1) == ';') {
            subExpression =
                subExpression.substring(0, subExpression.length() - 1).trim();
        }

        // If there is no 'where' clause
        if ((pos = subExpression.indexOf("where")) == -1) {
            return false;
        }

        // If there is no subexpression in the WHERE clause -> true
        // ( + 5 is the length of the 'where')
        subExpression =subExpression.substring(pos + 5, subExpression.length()).trim(); 
                                                                             
        if (subExpression.length() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @param ex
     * @return
     */
    public static String getLastMessage(Throwable ex) {
        
        Throwable p = ex;
        while (p.getCause() != null && p.getCause() != p) {
            p = p.getCause();
        }
        return p.getMessage();
    }    
}
