/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {TableDocument implementation based on the gvSIG DAL API}
 */
package org.gvsig.app.project.documents.table.gui;


import java.util.logging.Level;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindowTransform;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.gui.IDocumentWindow;
import org.gvsig.app.project.documents.gui.WindowLayout;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTypesTablePanel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.notification.ColumnHeaderSelectionChangeNotification;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.swing.api.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Feature table visualization panel.
 * 
 * @author 2005- Vicente Caballero
 * @author 2009- gvSIG team
 */
public class FeatureTableDocumentPanel extends FeatureTypesTablePanel implements
    IDocumentWindow, IWindowTransform, Observer, Component {

    private static final Logger LOG = LoggerFactory
        .getLogger(FeatureTableDocumentPanel.class);

    private static final long serialVersionUID = -1003263265311764630L;

    private static final int DEFAULT_HEIGHT = 450;

    private static final int DEFAULT_WIDTH = 700;

    private boolean isPalette = false;

    private WindowInfo windowInfo = null;

    private TableDocument model = null;

    private boolean selectionIsEmpty = true;

    public FeatureTableDocumentPanel(Document document) throws DataException {
        super(((TableDocument) document).getFeatureStoreModel());
        this.model = (TableDocument) document;
        initialize();
    }

    public JComponent asJComponent() {
        return this;
    }
    
    // IWindow interface

    public WindowInfo getWindowInfo() {
        if (windowInfo == null) {
            windowInfo =
                new WindowInfo(WindowInfo.ICONIFIABLE | WindowInfo.MAXIMIZABLE
                    | WindowInfo.RESIZABLE);

            if (this.model == null) {
                windowInfo.setTitle("Tabla");
            } else {
                windowInfo.setTitle(this.model.getName());
            }

            windowInfo.setWidth(DEFAULT_WIDTH);
            windowInfo.setHeight(DEFAULT_HEIGHT);
        }
        return windowInfo;
    }

    // SingletonWindow interface

    public Object getWindowModel() {
        return this.model;
    }

    public void toPalette() {
        isPalette = true;
        windowInfo.toPalette(true);
        windowInfo.setClosed(false);
        PluginServices.getMDIManager().changeWindowInfo(this, getWindowInfo());
    }

    public void restore() {
        isPalette = false;
        windowInfo.toPalette(false);
        windowInfo.setClosed(false);
        PluginServices.getMDIManager().changeWindowInfo(this, getWindowInfo());
    }

    public boolean isPalette() {
        return isPalette;
    }

    public TableDocument getModel() {
        return model;
    }

    public void update(Observable observable, Object notification) {
        // If selection changes from nothing selected to anything selected
        // or the reverse, enable controls
        if (DataStoreNotification.SELECTION_CHANGE.equals(notification)) {
            try {
                if (selectionIsEmpty != getFeatureSelection().isEmpty()) {
                    selectionIsEmpty = !selectionIsEmpty;
        			enableControls(notification);
                }
            } catch (DataException e) {
                LOG.error("Error getting the current selection", e);
            }
		} else if (notification instanceof ColumnHeaderSelectionChangeNotification) {
			enableControls(notification);
		}
    }

	private void enableControls(Object notification) {
		try {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					PluginServices.getMainFrame().enableControls();
				}
			});
		} catch (Exception e) {
			LOG.warn("Error calling enableControls() after notification: "
					+ notification, e);
		}
	}

    private void initialize() throws DataException {
        getTablePanel().getTable().addObserver(this);
        getFeatureSelection().addObserver(this);
    }

    private FeatureSelection getFeatureSelection() throws DataException {
        return getTableModel().getFeatureStore().getFeatureSelection();
    }

    public Object getWindowProfile() {
        return WindowInfo.EDITOR_PROFILE;
    }

    public WindowLayout getWindowLayout() {
        return null;
    }

    public void setWindowLayout(WindowLayout layout) {

    }

    public void setDocument(Document document) {
        // FIXME: ?????????
    }

    public Document getDocument() {
        return model;
    }

    public void windowActivated() {
        // Do nothing
    }

    public void windowClosed() {
        // Do nothing
    }
    
    public FeatureAttributeDescriptor[] getSelectedColumnsDescriptors() {
        try {
            return this.getTablePanel().getTable().getSelectedColumnsAttributeDescriptor();
        } catch (DataException ex) {
            return null;
        }
    }
}
