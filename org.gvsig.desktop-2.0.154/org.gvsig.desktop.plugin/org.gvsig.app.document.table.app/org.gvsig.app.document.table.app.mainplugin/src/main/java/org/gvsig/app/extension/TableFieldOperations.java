/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTable;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.ConfigurableFeatureTableModel;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.exception.BaseException;

/**
 * Operations regarding order and duplicated values.
 * 
 * @author Vicente Caballero Navarro
 */
public class TableFieldOperations extends Extension {

    private static Logger logger =
        LoggerFactory.getLogger(TableFieldOperations.class);
    private FeatureTableDocumentPanel table = null;

    /**
     * @see org.gvsig.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
        IconThemeHelper.registerIcon("action", "table-order-desc", this);
        IconThemeHelper.registerIcon("action", "table-order-asc", this);
        IconThemeHelper.registerIcon("action", "selection-duplicates", this);    
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
        doExecute(actionCommand, table);
    }

    /**
     * "execute" method acction
     * 
     * @param actionCommand
     *            The acction command that executes this method
     * @param table
     *            Table to operate
     */
    protected void doExecute(String actionCommand,
        FeatureTableDocumentPanel table) {
        // FIXME
        ConfigurableFeatureTableModel cftm =
            table.getTablePanel().getTableModel();
        try {
            if ("table-order-asc".equals(actionCommand)) {
                cftm.orderByColumn(table.getTablePanel().getTable()
                    .getSelectedColumnsAttributeDescriptor()[0].getName(), true);
                
                // Mark as modified:
                table.getModel().setModified(true);

            } else {
                if ("table-order-desc".equals(actionCommand)) {
                    cftm.orderByColumn(table.getTablePanel().getTable()
                        .getSelectedColumnsAttributeDescriptor()[0].getName(),
                        false);
                    
                    // Mark as modified:
                    table.getModel().setModified(true);

                } else {
                    if ("selection-duplicates".equals(actionCommand)) {
                        
                        // Find and select duplicates in selected column
                        long[] valCases = findSelectDuplicates(table);
                        /*
                         * Show info dialog
                         */
                        String msg = "";
                        if (valCases[0] == 0) {
                            // No repetitions
                            msg = Messages.getText("_No_repetitions_found");
                        } else {
                            String[] args = new String[2];
                            args[0] = Long.toString(valCases[0]);
                            args[1] = Long.toString(valCases[1]);
                            msg = Messages.getText(
                                "_Found_N_diff_repeated_vals_in_total_N_cases", args);
                        }
                        JOptionPane.showMessageDialog(
                            ApplicationLocator.getManager().getRootComponent(),
                            msg,
                            Messages.getText("_Find_and_select_duplicates"),
                            JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        } catch (BaseException e) {
            logger.error("While applying field operation (" + actionCommand + ")", e);
        }
    }


    /**
     * Detects repeated values
     * 
     * @param tpanel
     * @return Returns two values: numbers of values repeated and number of rows
     * with values which are repeated
     * 
     * @throws DataException
     */
    private long[] findSelectDuplicates(FeatureTableDocumentPanel tpanel)
        throws DataException {
        
        long[] values_cases = {0, 0};
        
        ConfigurableFeatureTableModel cftm = tpanel.getTablePanel().getTableModel();
        FeatureTable ftable = tpanel.getTablePanel().getTable();
        
        // There is only one selected column
        String fieldName = ftable.getSelectedColumnsAttributeDescriptor()[0].getName();
        FeatureStore fstore = cftm.getFeatureStore();
        
        Set<Feature> selFeats = new HashSet<Feature>();
        
        FeatureQuery fquery = fstore.createFeatureQuery();
        /*
         * Create order based on selected column
         * (ascending but descending would be ok too)
         */
        FeatureQueryOrder fqo = new FeatureQueryOrder();
        fqo.add(fieldName, true);
        fquery.setOrder(fqo);
        FeatureSet fset = fstore.getFeatureSet(fquery);
        DisposableIterator iter = fset.fastIterator();
        Feature currFeat = null;
        Feature prevFeat = null;
        
        Object curVal = null;
        Object preVal = null;
        
        /*
         * Get first feature and go next if there is next
         */
        if (iter.hasNext()) {
            currFeat = (Feature) iter.next();
            currFeat = currFeat.getCopy();
            curVal = currFeat.get(fieldName);
        } else {
            iter.dispose();
            return values_cases;
        }
        
        boolean equals = false;
        /*
         * Iterate and get copy of features with repeated value
         */
        while (iter.hasNext()) {
            prevFeat = currFeat;
            currFeat = (Feature) iter.next();
            currFeat = currFeat.getCopy();
            
            preVal = curVal;
            curVal = currFeat.get(fieldName);
            
            if (sameValue(preVal, curVal)) {
                if (!equals) {
                    // We are entering a new set of repetitions
                    values_cases[0]++;
                }
                equals = true;
                selFeats.add(prevFeat);
            } else {
                if (equals) {
                    // We have gone out of equal group
                    selFeats.add(prevFeat);
                }
                equals = false;
            }
        }
        iter.dispose();
        FeatureSelection fsele = (FeatureSelection) fstore.getSelection();
        fsele.beginComplexNotification();
        /*
         * Set selection inside complex notification to prevent
         * lots of notifications
         */
        // ============================
        fsele.deselectAll();
        
        values_cases[1] = selFeats.size(); 
        if (values_cases[1] > 0) {
            Iterator<Feature> fiter = selFeats.iterator();
            while (fiter.hasNext()) {
                fsele.select(fiter.next());
            }
        }
        // ============================
        fsele.endComplexNotification();
        return values_cases;
    }
    

    private boolean sameValue(Object v1, Object v2) {
        
        if (v1 == null) {
            if (v2 == null) {
                // Both null, same value
                return true;
            } else {
                // Only one is null, not same value
                return false;
            }
        } else {
            if (v2 == null) {
                // Only one is null, not same value
                return false;
            } else {
                // Delegate in equals method
                return v1.equals(v2);
            }
        }
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        try {
            return (table.getTablePanel().getTable().getSelectedColumnCount() == 1);
        } catch (DataException e) {
            logger.error("While getting number of selected rows.", e);
        }
        return false;
    }

    /**
     * @see org.gvsig.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();
        if (v != null && v instanceof FeatureTableDocumentPanel) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        }
        return false;
    }

}
