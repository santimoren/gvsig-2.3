/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.project.documents.table;

import java.awt.Component;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.project.documents.table.gui.CSVSeparatorOptionsPanel;
import org.gvsig.app.project.documents.table.gui.Statistics.MyObjectStatistics;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to create dbf and csv files at disk with the statistics group generated
 * from a table.
 * 
 * dbf -> Data Base File
 * csv -> Comma Separated Value
 * 
 * @author �ngel Fraile Gri��n e-mail: angel.fraile@iver.es
 * 
 */

public class ExportStatisticsFile {

    private static final Logger logger = LoggerFactory
        .getLogger(ExportStatisticsFile.class);

    private String lastPath = null;
    private Hashtable<String, MyFileFilter> dbfExtensionsSupported; // Supported
                                                                    // extensions.
    private Hashtable<String, MyFileFilter> csvExtensionsSupported;

    public ExportStatisticsFile(List<MyObjectStatistics> valores) {

        JFileChooser jfc = new JFileChooser(lastPath);
        jfc.removeChoosableFileFilter(jfc.getAcceptAllFileFilter());

        // Adding required extensions (dbf, csv)
        dbfExtensionsSupported = new Hashtable<String, MyFileFilter>();
        csvExtensionsSupported = new Hashtable<String, MyFileFilter>();
        dbfExtensionsSupported.put("dbf", new MyFileFilter("dbf",
            PluginServices.getText(this, "Ficheros_dbf"), "dbf"));
        csvExtensionsSupported.put("csv", new MyFileFilter("csv",
            PluginServices.getText(this, "Ficheros_csv"), "csv"));

        Iterator<MyFileFilter> iter =
            csvExtensionsSupported.values().iterator();
        while (iter.hasNext()) {
            jfc.addChoosableFileFilter(iter.next());
        }

        iter = dbfExtensionsSupported.values().iterator();
        while (iter.hasNext()) {
            jfc.addChoosableFileFilter(iter.next());
        }

        // Opening a JFileCooser
        if (jfc.showSaveDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
            File endFile = jfc.getSelectedFile();
            if (endFile.exists()) {// File exists in the directory.
                int resp =
                    JOptionPane.showConfirmDialog(
                        (Component) PluginServices.getMainFrame(),
                        PluginServices.getText(this,
                            "fichero_ya_existe_seguro_desea_guardarlo")
                            + "\n"
                            + endFile.getAbsolutePath(), PluginServices
                            .getText(this, "guardar"),
                        JOptionPane.YES_NO_OPTION);// Informing the user
                if (resp != JOptionPane.YES_OPTION) {// cancel pressed.
                    return;
                }
            }// end if exits.
            MyFileFilter filter = (MyFileFilter) jfc.getFileFilter();// dbf, csv
            endFile = filter.normalizeExtension(endFile);// "name" + "." +
                                                         // "dbf", "name" + "."
                                                         // + "csv"

            if (filter.getExtensionOfAFile(endFile).toLowerCase()
                .compareTo("csv") == 0) { // csv file
                exportToCSVFile(valores, endFile); // export to csv format
            } else
                if (filter.getExtensionOfAFile(endFile).toLowerCase()
                    .compareTo("dbf") == 0) {// dbf file
                    try {
                        exportToDBFFile(valores, endFile);
                    } catch (Exception e) {
                        NotificationManager.addError(e);
                    } // export to dbf format
                }
        }// end if aprove option.
    }

    /**
     * Creating cvs format file with the statistics.
     * Option to select the two columns separator.
     * 
     * Example with semicolon: Name;data\n
     * Name2;data2\n
     * 
     * @param valores
     *            - Pairs: String name (key) + Double value (
     * @param endFile
     *            - File to write the information
     */

    private void exportToCSVFile(List<MyObjectStatistics> valores, File endFile) {

        try {
            CSVSeparatorOptionsPanel csvSeparatorOptions =
                new CSVSeparatorOptionsPanel();
            PluginServices.getMDIManager().addWindow(csvSeparatorOptions);

            String separator = csvSeparatorOptions.getSeparator();

            if (separator != null) {

                FileWriter fileCSV = new FileWriter(endFile);

                fileCSV.write(PluginServices.getText(this, "Nombre")
                    + separator + PluginServices.getText(this, "Valor") + "\n");

                Iterator<MyObjectStatistics> iterador = valores.listIterator();

                while (iterador.hasNext()) {// Writing value,value\n
                    MyObjectStatistics data = iterador.next();
                    fileCSV.write(data.getKey() + separator + (data.getValue())
                        + "\n");
                }
                fileCSV.close();
                JOptionPane.showMessageDialog(
                    null,
                    PluginServices.getText(this, "fichero_creado_en") + " "
                        + endFile.getAbsolutePath(),
                    PluginServices.getText(this, "fichero_creado_en_formato")
                        + " csv "
                        + PluginServices.getText(this, "mediante_el_separador")
                        + " \"" + separator + "\"",
                    JOptionPane.INFORMATION_MESSAGE);// Informing the user
            } else {
                return;
            }

        } catch (IOException e) {// Informing the user
            logger.error("Error exportando a formato csv");
            JOptionPane.showMessageDialog(
                null,
                PluginServices.getText(this,
                    "Error_exportando_las_estadisticas")
                    + " "
                    + endFile.getAbsolutePath(), PluginServices.getText(this,
                    "Error"), JOptionPane.ERROR_MESSAGE);
        }

    }

    public void exportToDBFFile(List<MyObjectStatistics> valores, File endFile)
        throws DataException, ValidateDataParametersException {
        DataManager datamanager = DALLocator.getDataManager();

        //
        // Averigua el proveedor en funcion del fichero
        // preguntandoselo al FilesystemServerExplorer.
        DataServerExplorerParameters no_params =
            (DataServerExplorerParameters) datamanager
                .createServerExplorerParameters(FilesystemServerExplorer.NAME);
        
        FilesystemServerExplorer explorer =
            (FilesystemServerExplorer) datamanager.openServerExplorer(
                FilesystemServerExplorer.NAME, no_params // empty params
                );
        String providerName = explorer.getProviderName(endFile);

        try {
            if (endFile.exists()) {
                endFile.delete();
            }
            endFile.createNewFile();
            
            DataStoreParameters dsp =
                explorer.createStoreParameters(endFile, providerName);
            NewFeatureStoreParameters parameters =
                (NewFeatureStoreParameters) datamanager
                    .createNewStoreParameters(FilesystemServerExplorer.NAME,
                        providerName);
            
            parameters.delegate(dsp);
            
            EditableFeatureType type =
                parameters.getDefaultFeatureType(); // .getEditable();
            type.add(PluginServices.getText(this, "Nombre"), DataTypes.STRING,
                50);
            type.add(PluginServices.getText(this, "Valor"), DataTypes.DOUBLE,
                100).setPrecision(25);

            parameters.setDefaultFeatureType(type);
            datamanager.newStore(FilesystemServerExplorer.NAME, providerName,
                parameters, true);

            FeatureStore target =
                (FeatureStore) datamanager.openStore(providerName, parameters);
            target.edit(FeatureStore.MODE_APPEND);
            Iterator<MyObjectStatistics> iterador = valores.listIterator();
            while (iterador.hasNext()) {
                MyObjectStatistics data = iterador.next();
                EditableFeature ef = target.createNewFeature().getEditable();
                ef.set(PluginServices.getText(this, "Nombre"), data.getKey());
                ef.set(PluginServices.getText(this, "Valor"), data.getValue());
                target.insert(ef);
            }
            target.finishEditing();
            target.dispose();
            JOptionPane.showMessageDialog(
                (Component) PluginServices.getMainFrame(),
                PluginServices.getText(this, "fichero_creado_en") + " "
                    + endFile.getAbsolutePath(),
                PluginServices.getText(this, "fichero_creado_en_formato")
                    + " dbf", JOptionPane.INFORMATION_MESSAGE);// Informing the
                                                               // user
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

/**
 * @author �ngel Fraile Gri��n e-mail: angel.fraile@iver.es
 * 
 *         Class to work with the file extensions.
 */
class MyFileFilter extends FileFilter {

    private String[] extensiones = new String[1];
    private String description;
    private boolean dirs = true;
    private String info = null;

    public MyFileFilter(String[] ext, String desc) {
        extensiones = ext;
        description = desc;
    }

    public MyFileFilter(String[] ext, String desc, String info) {
        extensiones = ext;
        description = desc;
        this.info = info;
    }

    public MyFileFilter(String ext, String desc) {
        extensiones[0] = ext;
        description = desc;
    }

    public MyFileFilter(String ext, String desc, String info) {
        extensiones[0] = ext;
        description = desc;
        this.info = info;
    }

    public MyFileFilter(String ext, String desc, boolean dirs) {
        extensiones[0] = ext;
        description = desc;
        this.dirs = dirs;
    }

    public MyFileFilter(String ext, String desc, boolean dirs, String info) {
        extensiones[0] = ext;
        description = desc;
        this.dirs = dirs;
        this.info = info;
    }

    public boolean accept(File f) {
        if (f.isDirectory()) {
            if (dirs) {
                return true;
            } else {
                return false;
            }
        }
        for (int i = 0; i < extensiones.length; i++) {
            if (extensiones[i].equals("")) {
                continue;
            }
            if (getExtensionOfAFile(f).equalsIgnoreCase(extensiones[i])) {
                return true;
            }
        }
        return false;
    }

    public String getDescription() {
        return description;
    }

    public String[] getExtensions() {
        return extensiones;
    }

    public boolean isDirectory() {
        return dirs;
    }

    public String getExtensionOfAFile(File file) {
        String name;
        int dotPos;
        name = file.getName();
        dotPos = name.lastIndexOf(".");
        if (dotPos < 1) {
            return "";
        }
        return name.substring(dotPos + 1);
    }

    public File normalizeExtension(File file) {
        String ext = getExtensionOfAFile(file);
        if (ext.equals("") || !(this.accept(file))) {
            return new File(file.getAbsolutePath() + "." + extensiones[0]);
        }
        return file;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
