package org.gvsig.app.extension;

import java.awt.Dimension;
import javax.swing.JOptionPane;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.featureform.swing.CreateJFeatureFormException;
import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.DataSwingManager;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTableAsForm extends Extension {

    private static final Logger logger = LoggerFactory.getLogger(ShowTableAsForm.class);

    @Override
    public void initialize() {
    }

    @Override
    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();
        if ("table-show-form".equalsIgnoreCase(actionCommand)) {
            TableDocument doc = (TableDocument) application.getActiveDocument(TableManager.TYPENAME);
            if (doc == null) {
                return;
            }
            try {
                JFeaturesForm form = this.createform(doc.getStore());
                form.showForm(WindowManager.MODE.WINDOW);
            } catch (Exception ex) {
                String msg = "Can't show form for table '" + doc.getName() + "'.";
                logger.warn(msg, ex);
                application.messageDialog(msg + "\n\n(See the error log for more information)", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private JFeaturesForm createform(FeatureStore featureStore) throws CreateJFeatureFormException, ServiceException, DataException {
        final DataSwingManager swingManager = DALSwingLocator.getSwingManager();
        final JFeaturesForm form = swingManager.createJFeaturesForm(featureStore);
        form.getFormset().setAllowDelete(true);
        form.getFormset().setAllowNew(true);
        form.setPreferredSize(new Dimension(400, 300));
        return form;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        Document doc = application.getActiveDocument(TableManager.TYPENAME);
        return doc != null;
    }
}
