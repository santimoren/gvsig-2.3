

package org.gvsig.app.extension;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.DocumentManager;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.gui.GeneralTablePropertiesPageFactory;
import org.gvsig.fmap.mapcontrol.MapControlLocator;
import org.gvsig.propertypage.PropertiesPageManager;


public class TableProperties extends Extension {

    public void initialize() {
        PropertiesPageManager propertiesPageManager = MapControlLocator.getPropertiesPageManager();
        propertiesPageManager.registerFactory(new GeneralTablePropertiesPageFactory());
    }

    public void execute(String actionCommand) {
        if( "table-show-properties".equalsIgnoreCase(actionCommand) ) {
            ApplicationManager application = ApplicationLocator.getManager();
            Document doc = application.getActiveDocument(TableManager.TYPENAME);
            if( doc == null ) {
                return;
            }
            ProjectManager projectmanager = ApplicationLocator.getProjectManager();
            DocumentManager tableManager = projectmanager.getDocumentManager(TableManager.TYPENAME);
            IWindow win = tableManager.getPropertiesWindow(doc);
            application.getUIManager().addWindow(win);
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();
        Document doc = application.getActiveDocument(TableManager.TYPENAME);
        return doc != null;
    }
    
}
