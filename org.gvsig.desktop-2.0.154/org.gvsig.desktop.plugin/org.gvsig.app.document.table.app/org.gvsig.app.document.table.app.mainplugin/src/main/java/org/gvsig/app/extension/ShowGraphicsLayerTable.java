/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Opens a table document of the current view's graphic layer.
 * 
 * @author gvSIG team
 */
public class ShowGraphicsLayerTable extends Extension implements
		PropertyChangeListener {

	private static final Logger LOG = LoggerFactory
			.getLogger(ShowGraphicsLayerTable.class);

	private ApplicationManager appManager = ApplicationLocator.getManager();

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
		return getCurrentViewDocument() != null;
	}

	private ViewDocument getCurrentViewDocument() {
		return (ViewDocument) appManager
				.getActiveDocument(ViewManager.TYPENAME);
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		try {
			ViewDocument document = getCurrentViewDocument();
			if (document == null) {
				return false;
			}
			GraphicLayer graphicsLayer = document.getMapContext()
					.getGraphicsLayer();
			TableManager tableManager = getTableManager();
			TableDocument tableDocument = tableManager
					.getTableDocument(graphicsLayer);
			return tableDocument != null
					|| document.getMapContext().getGraphicsLayer()
							.getFeatureStore().getFeatureCount() > 0;
		} catch (DataException e) {
			LOG.error("Error checking if the current document is a "
					+ "view and has data in the graphics layer", e);
			return false;
		}
	}

	/**
	 * @see com.iver.mdiApp.plugins.IExtension#updateUI(java.lang.String)
	 */
	public void execute(String action) {

		ViewDocument document = getCurrentViewDocument();
		GraphicLayer graphicsLayer = document.getMapContext()
				.getGraphicsLayer();

		ProjectManager projectManager = getProjectManager();
		TableManager tableManager = getTableManager();

		TableDocument tableDocument = tableManager
				.getTableDocument(graphicsLayer);
		FeatureStore fs = graphicsLayer.getFeatureStore();

		if (tableDocument == null) {
			tableDocument = (TableDocument) projectManager.createDocument(
					TableManager.TYPENAME,
					PluginServices.getText(this, "Tabla_de_Atributos") + ": "
							+ graphicsLayer.getName());
			tableDocument.setStore(fs);
			tableDocument.setAssociatedLayer(graphicsLayer);
			projectManager.getCurrentProject().add(tableDocument);
		}

		tableDocument.setModified(true);
		FeatureTableDocumentPanel featureTableDocumentPanel = (FeatureTableDocumentPanel) tableManager
				.getMainWindow(tableDocument);

		appManager.getUIManager().addWindow(featureTableDocumentPanel);

	}

	private ProjectManager getProjectManager() {
		return ProjectManager.getInstance();
	}

	private TableManager getTableManager() {
		TableManager tableManager = (TableManager) getProjectManager()
				.getDocumentManager(TableManager.TYPENAME);
		return tableManager;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
		registerIcons();
	}

	public void postInitialize() {
		// Listen just in case a view document is removed, to remove also
		// the related graphics layer table document, if any
		getProjectManager().getCurrentProject().addPropertyChangeListener(this);
	}

	private void registerIcons() {
		IconThemeHelper.registerIcon("action", "layer-show-attributes-table", this);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if ("delDocument".equals(evt.getPropertyName())) {
			if (evt.getOldValue() != null
					&& evt.getOldValue() instanceof ViewDocument) {
				ViewDocument viewDocument = (ViewDocument) evt.getOldValue();
				getTableManager().removeTableDocument(
						viewDocument.getMapContext().getGraphicsLayer());
			}
		}
	}
}
