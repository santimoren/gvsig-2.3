/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.tools.util.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TableEditStartExtension extends AbstractTableEditExtension {

    private static final Logger logger = LoggerFactory.getLogger(TableEditStartExtension.class);

    @Override
    public void initialize() {
        super.initialize();
        IconThemeHelper.registerIcon("action", "table-start-editing", this);
    }

    public void execute(String actionCommand) {
        this.execute(actionCommand,null);
    }

    @Override
    public void execute(String actionCommand, Object[] args) {
        if ("table-start-editing".equals(actionCommand)) {
            try {
                TableDocument doc = (TableDocument)ArrayUtils.get(args,0);
                if( doc == null ) {
                    doc = (TableDocument) table.getDocument();
                }
                EditingNotificationManager editingNotification = DALSwingLocator.getEditingNotificationManager();
                EditingNotification notification = editingNotification.notifyObservers(
                        this,
                        EditingNotification.BEFORE_ENTER_EDITING_STORE,
                        doc,
                        doc.getStore());
                if (notification.isCanceled()) {
                    return;
                }
                doc.getStore().edit(FeatureStore.MODE_FULLEDIT);
                ApplicationLocator.getManager().refreshMenusAndToolBars();
                editingNotification.notifyObservers(
                        this,
                        EditingNotification.AFTER_ENTER_EDITING_STORE,
                        doc,
                        doc.getStore());
            } catch (DataException e) {
                logger.warn("Problems starting table editing.", e);
            }
        }
    }

    public boolean isEnabled() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        }
        if (v instanceof FeatureTableDocumentPanel) {
            FeatureTableDocumentPanel t = (FeatureTableDocumentPanel) v;
            FeatureStore fs = t.getModel().getStore();
            return fs.allowWrite();
        }
        return false;
    }

    public boolean isVisible() {
        IWindow v = PluginServices.getMDIManager().getActiveWindow();

        if (v == null) {
            return false;
        }

        if (v instanceof FeatureTableDocumentPanel
                && !((FeatureTableDocumentPanel) v).getModel().getStore().isEditing()) {
            table = (FeatureTableDocumentPanel) v;
            return true;
        }

        return false;
    }
}
