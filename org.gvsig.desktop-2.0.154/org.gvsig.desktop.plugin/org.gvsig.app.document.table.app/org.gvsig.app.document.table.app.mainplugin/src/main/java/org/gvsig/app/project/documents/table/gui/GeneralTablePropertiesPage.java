package org.gvsig.app.project.documents.table.gui;

import java.awt.BorderLayout;
import java.util.Locale;
import java.util.Set;
import javax.swing.ComboBoxModel;
import javax.swing.JComponent;
import org.gvsig.andami.LocaleManager;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.FeatureTableConfigurationPanel;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

public class GeneralTablePropertiesPage extends GeneralTablePropertiesPageLayout implements PropertiesPage {

    private static final long serialVersionUID = -8639908797829738997L;
    private final TableDocument tableDocument;
    private FeatureTableConfigurationPanel columnsConfigurationPanel = null;
    private LocaleManager localeManager;

    public GeneralTablePropertiesPage(TableDocument tableDocument) {
        this.tableDocument = tableDocument;
        this.localeManager = PluginsLocator.getLocaleManager();
        this.initComponents();
    }

    private void initComponents() {
        txtName.setText(tableDocument.getName());
        txtDate.setText(tableDocument.getCreationDate());
        txtOwner.setText(tableDocument.getOwner());
        txtComments.setText(tableDocument.getComment());
        
        pnlColumns.setLayout(new BorderLayout());
        pnlColumns.add(this.getColumnsConfigurationPanel(),BorderLayout.CENTER);
        
        Set<Locale> localeSet = localeManager.getInstalledLocales();
        for (Locale item : localeSet) {
            this.cboLocaleOfData.addItem(new LocaleComboBoxItem(item));
        }
        Locale localeOfData = tableDocument.getFeatureStoreModel().getCurrentFeatureTableModel().getLocaleOfData();
        if ( localeOfData != null ) {
            this.cboLocaleOfData.setSelectedItem(new LocaleComboBoxItem(localeOfData));
        }
        
        translate();
    }

    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        lblName.setText(i18nManager.getTranslation("Nombre_XcolonX"));
        lblDate.setText(i18nManager.getTranslation("creation_date_XcolonX"));
        lblOwner.setText(i18nManager.getTranslation("owner_XcolonX"));
        lblLocaleOfData.setText(i18nManager.getTranslation("locale_XcolonX"));
        lblComments.setText(i18nManager.getTranslation("comentarios_XcolonX"));
        lblColumns.setText(i18nManager.getTranslation("_Column_information_XcolonX"));
        
    }

    public void accept() {

    }
    
    private Locale getLocaleOfData(){
    	LocaleComboBoxItem item = (LocaleComboBoxItem) cboLocaleOfData.getSelectedItem();
    	if (item==null){
    		return null;
    	}
    	return item.getLocale();
    }
    
    private FeatureTableConfigurationPanel getColumnsConfigurationPanel() {
        if (this.columnsConfigurationPanel == null) {
            this.columnsConfigurationPanel =
                new FeatureTableConfigurationPanel(tableDocument
                    .getFeatureStoreModel().getCurrentFeatureTableModel());
        }
        return this.columnsConfigurationPanel;
    }

    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("General");
    }
    
    public int getPriority() {
        return 1000;
    }

    public JComponent asJComponent() {
        return this;
    }

    public boolean whenAccept() {
        return whenApply();
    }

    public boolean whenApply() {
        tableDocument.setName(txtName.getText());
        tableDocument.setCreationDate(txtDate.getText());
        tableDocument.setOwner(txtOwner.getText());
        tableDocument.setComment(txtComments.getText());
        tableDocument.getFeatureStoreModel().getCurrentFeatureTableModel()
                .setLocaleOfData(getLocaleOfData());
        getColumnsConfigurationPanel().accept();
        return true;
    }

    public boolean whenCancel() {
        return true;
    }

    
    private class LocaleComboBoxItem {

        private Locale locale;

        public LocaleComboBoxItem(Locale locale) {
            this.locale = locale;
        }

        public String toString() {
            return localeManager.getLocaleDisplayName(this.locale);
        }

        public Locale getLocale() {
            return this.locale;
        }

        public boolean equals(Object o) {
            if ( o == null ) {
                return false;
            }
            if ( o == this ) {
                return true;
            }
            if ( !(o instanceof LocaleComboBoxItem) ) {
                return false;
            }
            LocaleComboBoxItem item = (LocaleComboBoxItem) o;
            if ( !locale.equals(item.getLocale()) ) {
                return false;
            }
            return true;
        }
    }

}
