
package org.gvsig.app.project.documents.table.gui;

import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.propertypage.PropertiesPage;
import org.gvsig.propertypage.PropertiesPageFactory;


public class GeneralTablePropertiesPageFactory implements PropertiesPageFactory {

    public boolean isVisible(Object obj) {
        // Always if enabled this page
        return true;
    }

    public PropertiesPage create(Object obj) {
        return new GeneralTablePropertiesPage((TableDocument)obj);
    }

    public String getGroupID() {
        return TableDocument.TABLE_PROPERTIES_PAGE_GROUP;
    }
    
}
