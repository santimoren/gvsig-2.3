/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gvsig.i18n.impl;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.i18n.I18nManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.task.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author usuario
 */
public class TranslationsConsolider {

    private static final Logger logger = LoggerFactory.getLogger(TranslationsConsolider.class);

    private I18nManager i18nManager = null;
    private PluginsManager pluginsManager = null;
    private Map<String, Map<String, String>> allTranslations = null;
    private Locale currentLocale = null;
    private Map<String, List<String>> keysInPlugins = null;

    public TranslationsConsolider() {
        this.pluginsManager = PluginsLocator.getManager();
        this.allTranslations = new HashMap<String, Map<String, String>>();
        this.currentLocale = this.getI18nManager().getCurrentLocale();
        this.keysInPlugins = new HashMap<String, List<String>>();
    }

    public void setCurrentLocale(Locale locale) {
        this.currentLocale = locale;
    }

    public Locale getCurrentLocale() {
        return this.currentLocale;
    }

    public I18nManager getI18nManager() {
        if ( this.i18nManager == null ) {
            this.i18nManager = I18nManagerImpl.getInstance();
        }
        return this.i18nManager;
    }

    public void addKey(String key, String pluginCode) {
        List<String> pluginCodes = this.keysInPlugins.get(key);
        if ( pluginCodes == null ) {
            pluginCodes = new ArrayList<String>();
            this.keysInPlugins.put(key, pluginCodes);
        }
        if ( !StringUtils.isBlank(pluginCode) ) {
            if ( !pluginCodes.contains(pluginCode) ) {
                pluginCodes.add(pluginCode);
            }
        }
    }

    public void addLocale(Locale locale) {
        if ( this.allTranslations.get(locale.toString()) == null ) {
            this.allTranslations.put(locale.toString().toLowerCase(), new HashMap<String, String>());
        }
    }

    public List<String> getPluginCodesOfKey(String key) {
        List<String> pluginCodes = this.keysInPlugins.get(key);
        return pluginCodes;
    }

    public Map<String, String> getTranslations(String locale) {
        Map<String, String> translations = this.allTranslations.get(locale.toString().toLowerCase());
        return translations;
    }

    public Map<String, String> getTranslations(Locale locale) {
        return this.getTranslations(locale.toString());
    }

    public void add(Locale locale, String key, String value, String pluginCode) {
        Map<String, String> translations = this.getTranslations(locale);
        if ( !translations.containsKey(key) ) {
            translations.put(key, value);
            this.addKey(key, pluginCode);
        }
    }

    public void add(Locale locale, Properties properties, String pluginCode) {
        this.addLocale(locale);
        Enumeration enumKeys = properties.keys();
        while ( enumKeys.hasMoreElements() ) {
            String key = (String) enumKeys.nextElement();
            String value = properties.getProperty(key);
            this.add(locale, key, value, pluginCode);
        }
    }

    public List<Locale> getLocales() {
        List<Locale> locales = new ArrayList<Locale>(this.allTranslations.size());
        Iterator<String> itLocales = this.allTranslations.keySet().iterator();
        while ( itLocales.hasNext() ) {
            Locale locale = new Locale(itLocales.next().toString());
            locales.add(locale);
        }
        return locales;
    }

    public List<String> getKeys() {
        List<String> keys = new ArrayList<String>(this.keysInPlugins.size());
        keys.addAll(this.keysInPlugins.keySet());
        Collections.sort(keys);
        return keys;
    }

    private String getResourceFileName(Locale locale) {
        StringBuilder fileName = new StringBuilder();
        fileName.append("text");
        if ( !StringUtils.isBlank(locale.getLanguage()) ) {
            fileName.append("_");
            fileName.append(locale.getLanguage());
        }
        if ( !StringUtils.isBlank(locale.getCountry()) ) {
            fileName.append("_");
            fileName.append(locale.getCountry());
        }
        if ( !StringUtils.isBlank(locale.getVariant()) ) {
            fileName.append("_");
            fileName.append(locale.getVariant());
        }
        fileName.append(".properties");
        return fileName.toString();
    }

    public String getTranslation(String locale, String key) {
        Map<String, String> translations = this.getTranslations(locale);
        if ( translations == null ) {
            logger.warn("Can't locate the translations for locale '" + locale + "'.");
            return null;
        }
        return translations.get(key);
    }

    public String getTranslation(Locale locale, String key) {
        return this.getTranslation(locale.toString(), key);
    }

    public String get(String key) {
        return this.getTranslation(currentLocale, key);
    }

    public void put(String key, String value) {
        this.add(currentLocale, key, value, null);
    }

    private int mergeWithTranslationsFromMessages(SimpleTaskStatus task, int n) {
        List<Locale> locales = this.getLocales();
        
        for(int i=0; i<locales.size(); i++) {
            Locale locale = locales.get(i);
            task.message("merge "+locale.toString());
            task.setCurValue(n++);
            Properties translations = Messages.getTranslations(locale);
            Enumeration<Object> keys = translations.keys();
            while( keys.hasMoreElements() ) {
                String key = (String) keys.nextElement();
                String value = translations.getProperty(key);
                this.add(locale, key, value, null);
            }
        }
        return n;
        
    }
    
    public void consolide() throws IOException {
        SimpleTaskStatus task = ToolsLocator.getTaskStatusManager().createDefaultSimpleTaskStatus("Consolide translations");
        task.setAutoremove(true);
        task.add();
        try {
            task.setTitle("Consolide translations");
            int n = this.loadAllTranslations(task);
            n = mergeWithTranslationsFromMessages(task,n);
            if( !task.isCancellationRequested() ) {
                this.storeAsProperties(task, n);
            }
        } finally {
            task.terminate();
        }
    }
    
    private int loadAllTranslations(SimpleTaskStatus task) {
        task.message("searching locales");
        Locale[] locales = this.getI18nManager().getInstalledLocales();

        List<PluginServices> plugins = this.pluginsManager.getPlugins();
        File appI18nFolder = pluginsManager.getApplicationI18nFolder();
        File[] subfolders = appI18nFolder.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isDirectory();
            }
        });
        int n = 1;
        task.setRangeOfValues(0, plugins.size() + subfolders.length + locales.length*2 + 1);

        task.message("loading from andami.");
        task.setCurValue(n++);
        loadTranslation(locales, appI18nFolder, null);

        for ( File subfolder : subfolders ) {
            if( task.isCancellationRequested() ) {
                return n;
            }
            task.setCurValue(n++);
            if ( subfolder.isDirectory() ) {
                task.message("loading " + subfolder.getName());
                if ( "andami".equals(subfolder.getName()) ) {
                    loadTranslation(locales, appI18nFolder, "org.gvsig.andami");
                } else {
                    loadTranslation(locales, appI18nFolder, null);
                }
            }
        }

        for ( PluginServices plugin : plugins ) {
            if( task.isCancellationRequested() ) {
                return n;
            }
            if ( plugin != null ) {
                task.message("loading " + plugin.getPluginName());
                task.setCurValue(n++);
                loadTranslation(locales, plugin.getPluginDirectory(), plugin.getPluginName());
            }
        }
        return n;
    }

    private void loadTranslation(Locale[] locales, File folder, String pluginCode) {
        for ( Locale locale : locales ) {
            File f1 = new File(folder, getResourceFileName(locale));
            File f2 = new File(folder, "i18n/" + getResourceFileName(locale));
            if ( !f1.exists() && !f2.exists() ) {
                if ( "es".equals(locale.getLanguage()) ) {
                    f1 = new File(folder, "text.properties");
                    f2 = new File(folder, "i18n/text.properties");
                }
            }
            if ( f1.exists() ) {
                loadTranslation(locale, f1, pluginCode);
            }
            if ( f2.exists() ) {
                loadTranslation(locale, f2, pluginCode);
            }
        }
    }

    private void loadTranslation(Locale locale, File f, String pluginCode) {
        FileInputStream fins = null;
        try {
            Properties properties = new Properties();
            fins = new FileInputStream(f);
            properties.load(fins);
            this.add(locale, properties, pluginCode);
        } catch (IOException ex) {
            logger.warn("Error processing property file '" + f.getAbsolutePath() + "'.", ex);
        } finally {
            IOUtils.closeQuietly(fins);
        }
    }

    private void storeAsProperties(SimpleTaskStatus task, int pos) throws IOException {
        task.message("Prepraring to store");
        File folder = new File(this.pluginsManager.getApplicationI18nFolder(), "translations.all");
        if( !folder.exists() ) {
            FileUtils.forceMkdir(folder);
        }
        List<String> keys = this.getKeys();
        List<Locale> locales = this.getLocales();
        for ( Locale locale : locales ) {
            task.setCurValue(pos++);
            task.message("Storing "+locale.toString());
            Map<String, String> translations = this.getTranslations(locale);
            Properties properties = new Properties();
            for ( String key : keys ) {
                String value = this.getTranslation(locale, key);
                if( value == null ) {
                    value = "";
                }
                properties.setProperty(key, value);
            }
            File f = new File(folder, this.getResourceFileName(locale));
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                properties.store(fos, null);
            } catch (Exception ex) {
                logger.warn("Can't write properties '" + f.getAbsolutePath() + "'.", ex);
            } finally {
                IOUtils.closeQuietly(fos);
            }

        }
        task.message("Storing plugin information");
        Properties properties = new Properties();
        for ( String key : keys ) {
            List<String> pluginCodes = this.keysInPlugins.get(key);
            StringBuilder ss = null;
            for ( String pluginCode : pluginCodes ) {
                if ( ss == null ) {
                    ss = new StringBuilder();
                } else {
                    ss.append(", ");
                }
                ss.append(pluginCode);
            }
            if ( ss == null ) {
                properties.setProperty(key, "");
            } else {
                properties.setProperty(key, ss.toString());
            }
        }
        FileOutputStream fos = null;
        File f = new File(folder, "keysbyplugin.properties");
        try {
            fos = new FileOutputStream(f);
            properties.store(fos, null);
        } catch (Exception ex) {
            logger.warn("Can't write properties '" + f.getAbsolutePath() + "'.", ex);
        } finally {
            IOUtils.closeQuietly(fos);
        }             
        task.message("");
    }

    /*
     def storeAsDBF(self):
     maxl = 100
     for k in self.keys():
     if len(k) > maxl:
     maxl = len(k)
     #print "max key len: ", maxl
        
     folder = File(self.__pluginsManager.getApplicationFolder(),"i18n")
     f = File(folder,"translations.dbf")
     try:
     f.delete()
     except:
     print "Can't delete file "+f.getAbsolutePath()
     schema = createSchema() 
     schema.append( "ID" , "Integer" , 7)
     schema.append( "key" , "String" , maxl)
     schema.append( "locale" , "String" , 30)
     schema.append( "translation" , "String" , 200)
     schema.append( "plugins" , "String" , 200)

     table = createDBF( schema, File(folder,"translations.dbf"))

     locales = self.getI18nManager().getInstalledLocales()
     idCounter = 1
     for key in self.keys():
     for locale in locales:
     pluginCodes = ""
     for pluginCode in self.getPluginCodesOfKey(key):
     pluginCodes += pluginCode + " "
     pluginCodes = str(pluginCodes).strip()
        
     table.append(ID=idCounter, 
     key=key, 
     locale=locale.toString(), 
     translation=self.getTranslation(locale, key),
     plugins=pluginCodes
     )
     idCounter +=1
     table.commit()
  
     */
}
