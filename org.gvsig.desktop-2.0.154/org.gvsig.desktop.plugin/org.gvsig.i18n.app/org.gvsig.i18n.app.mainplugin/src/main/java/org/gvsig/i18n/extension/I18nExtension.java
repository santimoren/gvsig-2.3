/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {New extension for installation and update of text translations}
 */
package org.gvsig.i18n.extension;

import org.gvsig.about.AboutManager;
import org.gvsig.about.AboutParticipant;
import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.i18n.extension.preferences.I18nPreferencePage;
import org.gvsig.tools.ToolsLocator;


/**
 * Texts localization management extension.
 * 
 * @author <a href="mailto:dcervera@disid.com">David Cervera</a>
 */
public class I18nExtension extends Extension {

    public void execute(String actionCommand) {
        // Nothing to do
    }

    public void initialize() {
    	IconThemeHelper.registerIcon("preferences", "i18n-preferences", this);
    	
        // Replace the default locale Preferences page for the new one
        ToolsLocator.getExtensionPointManager()
                .add("AplicationPreferences", "").append("LanguagePage", "",
                        new I18nPreferencePage());
    }

    public void postInitialize() {
        // Register the about panel
		ApplicationManager application = ApplicationLocator.getManager();
		
		AboutManager about = application.getAbout();

		about.addDeveloper(
				"DISID",
				this.getClass().getClassLoader().getResource("about/disid.html"),
				1
		);
		
		AboutParticipant participant = about.getDeveloper("DISID");
		participant.addContribution(
			"I18nExtension",
			"gvSIG translations management extension", 
			2009,1,1, 
			2009,12,31
		);   	
    }

    public boolean isEnabled() {
        return false;
    }

    public boolean isVisible() {
        return false;
    }
}