/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.i18n.extension.preferences;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.i18n.Messages;
import org.gvsig.i18n.extension.preferences.table.LocaleTableModel;
import org.gvsig.i18n.extension.preferences.table.RadioButtonCellEditor;
import org.gvsig.i18n.extension.preferences.table.RadioButtonCellRenderer;
import org.gvsig.i18n.impl.I18nManagerImpl;
import org.gvsig.tools.ToolsLocator;

/**
 * Prefence page to manage gvSIG locales.
 *
 * @author <a href="mailto:dcervera@disid.com">David Cervera</a>
 */
public class I18nPreferencePage extends AbstractPreferencePage {

    private static final long serialVersionUID = 7164183052397200888L;

    private final ImageIcon icon;

    private JTable localesTable;

    private LocaleTableModel tableModel;

    private JTextArea txtWarning = null;
    private JTextArea txtCollaboration = null;
    
    /**
     * Creates a new I18n preferences page.
     */
    public I18nPreferencePage() {
        setParentID("org.gvsig.coreplugin.preferences.general.GeneralPage");

        icon = PluginServices.getIconTheme().get("i18n-preferences");

       this.initComponents();
    }
    
    private void initComponents() {
        GridBagConstraints gridBagConstraints;

        Font hints_fnt = new JLabel().getFont();
        
        Component tablePanel = getLocalesPanel();
        txtWarning = getActiveLocaleLabel(this.getBackground(), hints_fnt.deriveFont(Font.BOLD));
        txtCollaboration = getCollaborationLabel(this.getBackground(), hints_fnt);
    
        Box.Filler fillerBorder1 = new Box.Filler(new Dimension(16, 16), new Dimension(16, 16), new Dimension(16, 16));
        Box.Filler fillerBorder2 = new Box.Filler(new Dimension(16, 16), new Dimension(16, 16), new Dimension(16, 16));
        Box.Filler fillerBottom = new Box.Filler(new Dimension(0, 0), new Dimension(0, 0), new Dimension(32767, 32767));

        GridBagLayout layout = new GridBagLayout();
        layout.columnWidths = new int[] {0, 4, 0, 4, 0};
        layout.rowHeights = new int[] {0, 4, 0, 4, 0, 4, 0, 4, 0};
        setLayout(layout);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        add(txtWarning, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        add(txtCollaboration, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.3;
        add(tablePanel, gridBagConstraints);
                
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        add(fillerBorder1, gridBagConstraints);
        
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        add(fillerBorder2, gridBagConstraints);
        
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 0.1;
        add(fillerBottom, gridBagConstraints);
        
        this.translate();
    }

    private void translate() {
        org.gvsig.tools.i18n.I18nManager i18nManager = ToolsLocator.getI18nManager();    
        txtCollaboration.setText(
                i18nManager.getTranslation(
                        "I18nPreferencePage.colaboraciones", 
                        new String[] { "translations@gvsig.org" }
                )
        );
        txtWarning.setText(i18nManager.getTranslation("I18nPreferencePage.ayuda"));
    }
    
    @Override
    public boolean isResizeable() {
		return true;
    }
    
    public String getID() {
        return getClass().getName();
    }

    public String getTitle() {
        return Messages.getText("idioma");
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public JPanel getPanel() {
        return this;
    }

    public void setChangesApplied() {
        if ( tableModel == null ) {
            return;
        }
        tableModel.setChangesApplied();
    }

    public boolean isValueChanged() {
        if ( tableModel == null ) {
            return false;
        }
        return tableModel.isValueChanged();
    }

    public void storeValues() throws StoreException {
        if ( tableModel == null ) {
            return;
        }
        tableModel.saveSelectedLocale();
    }

    public void initializeDefaults() {
        if ( tableModel == null ) {
            return;
        }
        tableModel.selectDefaultLocale();
    }

    public void initializeValues() {
        if ( tableModel == null ) {
            tableModel = new LocaleTableModel(I18nManagerImpl.getInstance());
            localesTable.setModel(tableModel);

            TableColumn activeColumn = localesTable.getColumnModel().getColumn(
                    LocaleTableModel.COLUMN_ACTIVE);
            activeColumn.setCellEditor(new RadioButtonCellEditor());
            activeColumn.setCellRenderer(new RadioButtonCellRenderer());

            localesTable
                    .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            localesTable.getSelectionModel().setSelectionInterval(0, 0);
        }
        tableModel.selectPreviousLocale();
        
        this.translate();
    }

    /**
     * Creates the Panel with the table of Locales.
     */
    private Component getLocalesPanel() {
        localesTable = new JTable();
        // Asignaremos el modelo a la tabla mas adelante para dar tiempo a que
        // se haya inicializado todo o casi todo en gvSIG.

        JScrollPane scrollPane = new JScrollPane(localesTable);

        // Container panel
        JPanel localesPanel = new JPanel();
        localesPanel.setLayout(new BoxLayout(localesPanel, BoxLayout.Y_AXIS));
        localesPanel.add(scrollPane);
        localesPanel.setAlignmentX(CENTER_ALIGNMENT);
        localesPanel.setPreferredSize(new Dimension(236, 200));
        localesPanel.setMaximumSize(new Dimension(500, 230));

        return localesPanel;
    }
    
    /**
     * Creates the JLabel to show information about the preference page.
     */
    private JTextArea getActiveLocaleLabel(Color bg, Font fnt) {
        JTextArea textArea = new JTextArea(Messages
                .getText("I18nPreferencePage.ayuda"));
        textArea.setEditable(false);
        textArea.setAutoscrolls(true);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setFont(fnt);
        textArea.setBackground(bg);
        return textArea;
    }

    /**
     * Creates the JLabel to show information about gvSIG translation
     * collaboration.
     */
    private JTextArea getCollaborationLabel(Color bg, Font fnt) {
        org.gvsig.tools.i18n.I18nManager i18nManager = ToolsLocator.getI18nManager();
        JTextArea textArea = new JTextArea(
                i18nManager.getTranslation(
                        "I18nPreferencePage.colaboraciones", 
                        new String[] { "translations@gvsig.org" }
                )
        );
        textArea.setEditable(false);
        textArea.setAutoscrolls(true);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setFont(fnt);
        textArea.setBackground(bg);
        return textArea;
    }

}
