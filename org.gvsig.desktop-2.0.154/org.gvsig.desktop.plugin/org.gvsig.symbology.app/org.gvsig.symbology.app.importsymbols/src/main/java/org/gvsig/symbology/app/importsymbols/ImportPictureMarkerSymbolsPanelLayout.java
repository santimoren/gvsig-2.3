/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.importsymbols;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;


public class ImportPictureMarkerSymbolsPanelLayout extends JPanel
{
   /**
	 * 
	 */
	private static final long serialVersionUID = -5315759500182797864L;
JLabel labelTitle = new JLabel();
   JLabel labelImagesToImport = new JLabel();
   JLabel labelFolderName = new JLabel();
   JTextField ctrltFolderName = new JTextField();
   JList ctrlExistingFolders = new JList();
   JLabel labelSize = new JLabel();
   JSpinner ctrlDefaultSize = new JSpinner();
   JButton ctrlCancel = new JButton();
   JButton ctrlImport = new JButton();
   JLabel labelOverwriteOptions = new JLabel();
   JComboBox ctrlOverwriteOptions = new JComboBox();
   JList ctrlImagesToImport = new JList();
   JButton ctrlBrowseImagesToImport = new JButton();

   /**
    * Default constructor
    */
   public ImportPictureMarkerSymbolsPanelLayout()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,FILL:PREF:GROW(1.0),CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      labelTitle.setName("labelTitle");
      labelTitle.setText("_Import_images_as_picture_marker_symbols");
      jpanel1.add(labelTitle,cc.xywh(2,2,6,1));

      labelImagesToImport.setName("labelImagesToImport");
      labelImagesToImport.setText("_Images_to_import");
      jpanel1.add(labelImagesToImport,cc.xywh(2,4,6,1));

      labelFolderName.setName("labelFolderName");
      labelFolderName.setText("_Folder_name_for_creatimg_the_symbols");
      jpanel1.add(labelFolderName,cc.xywh(2,7,6,1));

      ctrltFolderName.setName("ctrltFolderName");
      jpanel1.add(ctrltFolderName,cc.xywh(3,8,5,1));

      ctrlExistingFolders.setName("ctrlExistingFolders");
      JScrollPane jscrollpane1 = new JScrollPane();
      jscrollpane1.setViewportView(ctrlExistingFolders);
      jscrollpane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane1,new CellConstraints(3,9,5,1,CellConstraints.FILL,CellConstraints.TOP));

      labelSize.setName("labelSize");
      labelSize.setText("_Default_size_to_use_in_symbols");
      jpanel1.add(labelSize,cc.xywh(2,11,6,1));

      ctrlDefaultSize.setName("ctrlDefaultSize");
      jpanel1.add(ctrlDefaultSize,cc.xywh(3,12,5,1));

      ctrlCancel.setActionCommand("_Cancel");
      ctrlCancel.setName("ctrlCancel");
      ctrlCancel.setText("_Cancel");
      jpanel1.add(ctrlCancel,cc.xy(5,17));

      ctrlImport.setActionCommand("_import");
      ctrlImport.setName("ctrlImport");
      ctrlImport.setText("_import");
      jpanel1.add(ctrlImport,cc.xy(7,17));

      labelOverwriteOptions.setName("labelOverwriteOptions");
      labelOverwriteOptions.setText("_Overwrite_options");
      jpanel1.add(labelOverwriteOptions,cc.xywh(2,14,6,1));

      ctrlOverwriteOptions.setName("ctrlOverwriteOptions");
      ctrlOverwriteOptions.addItem("_Ask_to_user");
      ctrlOverwriteOptions.addItem("_Dont_overwrite");
      ctrlOverwriteOptions.addItem("_Overwrite_always");
      jpanel1.add(ctrlOverwriteOptions,cc.xywh(3,15,5,1));

      ctrlImagesToImport.setName("ctrlImagesToImport");
      JScrollPane jscrollpane2 = new JScrollPane();
      jscrollpane2.setViewportView(ctrlImagesToImport);
      jscrollpane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane2,new CellConstraints(3,5,5,1,CellConstraints.DEFAULT,CellConstraints.TOP));

      ctrlBrowseImagesToImport.setActionCommand("_Browse");
      ctrlBrowseImagesToImport.setName("ctrlBrowseImagesToImport");
      ctrlBrowseImagesToImport.setText("_Select_images");
      jpanel1.add(ctrlBrowseImagesToImport,new CellConstraints(3,6,5,1,CellConstraints.RIGHT,CellConstraints.DEFAULT));

      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5,6,7,8 },new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
