/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.importsymbols;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

import org.apache.commons.io.FilenameUtils;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.symbology.app.importsymbols.ImportPictureMarkerSymbolsProcess.YesNoAsk;
import org.gvsig.tools.util.FolderSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportPictureMarkerSymbolsPanel extends ImportPictureMarkerSymbolsPanelLayout {
	
	private static final long serialVersionUID = 1919826881234125305L;
	
	private static Logger logger = LoggerFactory.getLogger(ImportPictureMarkerSymbolsPanel.class);
	private ImportPictureMarkerSymbolsProcess process = null;
	private ApplicationManager application;
	private List<File> selectedFiles = null;

	public ImportPictureMarkerSymbolsPanel(ImportPictureMarkerSymbolsProcess process) {
		super();
		this.process = process;
		this.application = ApplicationLocator.getManager();
		initComponents();
	}

	private void closeWindow() {
		this.setVisible(false);
	}

	private class OverwriteItem {
		private String label;
		private YesNoAsk value;

		OverwriteItem(String label, YesNoAsk value) {
			this.label = label;
			this.value = value;
		}
		public String toString() {
			return this.label;
		}
		public YesNoAsk getValue() {
			return value;
		}
	}

	private class FileItem {
		private File value;

		FileItem(File value) {
			this.value = value;
		}
		public String toString() {
			return this.value.getName();
		}
		public File getValue() {
			return value;
		}
	}
	
	private void translate(Container container) {
		for (int n = 0; n < container.getComponentCount(); n++) {
			Component component = container.getComponent(n);
			try {
				if (component instanceof JLabel) {
					JLabel label = (JLabel) component;
					if( label.getText().startsWith("_") ) {
						label.setText(application.translate(label.getText()));
					}
				} else if (component instanceof AbstractButton) {
					AbstractButton button = (AbstractButton) component;
					if( button.getText().startsWith("_") ) {
						button.setText(application.translate(button.getText()));
					}
				} else if (component instanceof JTextComponent) {
					JTextComponent text = (JTextComponent) component;
					if( text.getText().startsWith("_") ) {
						text.setText(application.translate(text.getText()));
					}
				} else if (component instanceof Container) {
					translate((Container) component);
				}
			} catch (Throwable e) {
				// Ignore if
			} 
		}
	}
	
	private void initComponents() {
		translate(this);
		// ------------------------------------------------------------
		// Image file names
		this.ctrlBrowseImagesToImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doBrowseImagesToImport();
			}

		});
		
		// ------------------------------------------------------------
		// Default symbol size
		this.ctrlDefaultSize.setValue(this.process.getSymbolSize());
	
		// ------------------------------------------------------------
		// Target folder name
		if( this.process.getTargetFolderName()!=null ) {
			this.ctrltFolderName.setText(this.process.getTargetFolderName());
		}
		
		// ------------------------------------------------------------
		// List of existing target folders 
		FolderSet repo = this.process.getSymbolsRepository();
		final File[] folders = repo.listFiles(new FilenameFilter() {
			public boolean accept(File arg0, String arg1) {
				return arg0.isDirectory(); 
			}
		});
		final List<FileItem> folderItems = new ArrayList<FileItem>();
		for( int i=0; i<folders.length; i ++ ) {
			folderItems.add(new FileItem(folders[i]));
		}
		ctrlExistingFolders.setModel(new ListModel() {
			public void removeListDataListener(ListDataListener arg0) {
				// Do nothing
			}
			public int getSize() {
				return folderItems.size();
			}
			public Object getElementAt(int arg0) {
				return folderItems.get(arg0);
						
			}
			public void addListDataListener(ListDataListener arg0) {
				// Do nothing
			}
		});
		this.ctrlExistingFolders.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if( !arg0.getValueIsAdjusting() ) {
					try {
						File file = ((FileItem)ctrlExistingFolders.getSelectedValue()).getValue();
						ctrltFolderName.setText(file.getName());
					} catch(Exception ex) {
						logger.info("Error getting selected filename",ex);
					}
				}
			}
		});
		
		// ------------------------------------------------------------
		// Overwrite options
		final OverwriteItem[] overwriteItems = new OverwriteItem[] {
				new OverwriteItem(application.translate("_Ask_to_the_user"), YesNoAsk.ASK),
				new OverwriteItem(application.translate("_Dont_overwrite"), YesNoAsk.NO),
				new OverwriteItem(application.translate("_Overwrite_always"), YesNoAsk.YES)
		};
		this.ctrlOverwriteOptions.setModel(new ComboBoxModel() {
			OverwriteItem selected = null;
			public void removeListDataListener(ListDataListener arg0) {
				// Do nothing
			}
			public int getSize() {
				return overwriteItems.length;
			}
			public Object getElementAt(int arg0) {
				return overwriteItems[arg0];
			}
			public void addListDataListener(ListDataListener arg0) {
				// Do nothing
			}
			public void setSelectedItem(Object arg0) {
				this.selected = (OverwriteItem) arg0;
			}
			public Object getSelectedItem() {
				return this.selected;
			}
		});
		this.ctrlOverwriteOptions.setSelectedIndex(0);
		
		// ------------------------------------------------------------
		// Import button
		this.ctrlImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doImport();
			}
		});
		
		// ------------------------------------------------------------
		// Cancel button
		this.ctrlCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doCancel();
			}
		});


	}

	private void doImport() {
		if( this.ctrltFolderName.getText().trim().length()==0 ) {
			application.messageDialog(
					application.translate("_Should_select_a_folder_name"),
					this.process.getName(), 
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		if( this.selectedFiles==null || this.selectedFiles.size()==0 ) {
			application.messageDialog(
					application.translate("_Should_select_the_images_to_import"),
					this.process.getName(), 
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		this.process.setTargetFolderName(this.ctrltFolderName.getText());
		this.process.setSymbolSize( (( SpinnerNumberModel)this.ctrlDefaultSize.getModel()).getNumber().doubleValue());
		this.process.setOverwrite( ((OverwriteItem)this.ctrlOverwriteOptions.getSelectedItem()).getValue() );
		this.process.getSelectedImageFiles().clear();
		this.process.getSelectedImageFiles().addAll(selectedFiles);
		this.closeWindow();
	}

	private void doCancel() {
		this.process.cancelRequest();
		this.closeWindow();
	}

	private List<File> fileChooser(String title, boolean multiselect, FileFilter filter) {
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		fc.setMultiSelectionEnabled(multiselect);
		int r = fc.showDialog(application.getRootComponent(),title);
		if( r == JFileChooser.APPROVE_OPTION ) {
			return Arrays.asList(fc.getSelectedFiles()); 
		}
		return null;
	}
	
	private void doBrowseImagesToImport() {
		List<File> files = fileChooser(application.translate("_Select_the_images"), true, new FileFilter() {
			public String getDescription() {
				StringBuffer buffer = new StringBuffer();
				List<String> extensions = new ArrayList<String>();
				extensions.addAll(process.getAllowedFileExtensions());
				int l=extensions.size();
				if( l>=2 ) {
					for( int i=0; i<l-2; i++) {
						buffer.append(extensions.get(i));
						buffer.append(", ");
					}
					buffer.append( extensions.get(l-2));
					buffer.append(" & ");
					buffer.append( extensions.get(l-1));
				} else {
					buffer.append( extensions.get(0));
				}
				buffer.append(" ");
				buffer.append(application.translate("_Images"));
				return buffer.toString();
			}

			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = FilenameUtils.getExtension(f.getName()).toLowerCase(); 
				return extension != null && process.getAllowedFileExtensions().contains(extension);
			}
		});
		if( files != null ) {
			this.selectedFiles = files;
			final List<FileItem> folderItems = new ArrayList<FileItem>();
			for( int i=0; i<files.size(); i ++ ) {
				folderItems.add(new FileItem(files.get(i)));
			}
			ctrlImagesToImport.setModel(new ListModel() {
				public void removeListDataListener(ListDataListener arg0) {
					// Do nothing
				}
				public int getSize() {
					return folderItems.size();
				}
				public Object getElementAt(int arg0) {
					return folderItems.get(arg0);
							
				}
				public void addListDataListener(ListDataListener arg0) {
					// Do nothing
				}
			});
		}
	}
}
