/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.importsymbols;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportPictureMarkerSymbolsExtension extends Extension {

    @SuppressWarnings("unused")
    private static Logger logger = LoggerFactory.getLogger(ImportPictureMarkerSymbolsExtension.class);
    private ImportPictureMarkerSymbolsProcess process = null;

    public void initialize() {
      // Do nothing
    }
    public void execute(String actionCommand) {
      ApplicationManager application = ApplicationLocator.getManager();

      if ("tools-symbology-import-picture-marker-symbol".equalsIgnoreCase(actionCommand)) {
        if (process != null && process.isAlive()) {
          application.messageDialog(
        	  application.translate("_Import_process_already_running_Wait_to_terminate"),
              application.translate("_Import_picture_marker_symbols"),
              JOptionPane.WARNING_MESSAGE);
          return;
        }
        process = new ImportPictureMarkerSymbolsProcess();
        /*
		Component dlg = application.createComponent(ImportPictureMarkerSymbolsPanel.class, process);
		application.showDialog(dlg, application.translate("_Import_point_symbols"));
		*/
		if( !process.isCancellationRequested() ) {
			process.start();
		}
      }
    }
    public boolean isEnabled() {
      if (process != null && process.isAlive()) { 
        return false;
      }
      return true;
    }
    public boolean isVisible() {
      return true;
    }

}
