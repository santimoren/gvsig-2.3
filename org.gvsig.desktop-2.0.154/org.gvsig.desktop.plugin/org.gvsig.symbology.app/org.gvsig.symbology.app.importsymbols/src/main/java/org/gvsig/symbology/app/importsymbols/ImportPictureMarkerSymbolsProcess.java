/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.importsymbols;

import java.awt.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.marker.IPictureMarkerSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.SimpleTaskStatus;
import org.gvsig.tools.util.FolderSet;
import org.gvsig.tools.util.impl.DefaultFolderSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportPictureMarkerSymbolsProcess extends AbstractMonitorableTask {
	private static Logger logger = LoggerFactory
			.getLogger(ImportPictureMarkerSymbolsProcess.class);

	public enum YesNoAsk {
		YES, NO, ASK
	};
	
	private static final String[] SELECTION_SUFFIX = {"_sel", "_bg"};

	private ApplicationManager application = null;
	private SymbolManager symbolManager = null;
	private SymbologyManager symbologyManager = null;

	private Set<String> allowedFileExtensions = null;
	private List<File> notCreatedSymbols = new ArrayList<File>();

	private boolean quietMode = false;

	private YesNoAsk overwrite = YesNoAsk.ASK;
	private List<File> selectedImageFiles = new ArrayList<File>();

	// Name of the folder in the symbols repository
	private String targetFolderName;
	private double symbolSize = 18;

	protected ImportPictureMarkerSymbolsProcess() {
		super(false);
		this.application = ApplicationLocator.getManager();
		this.setTaskName(application.translate("_Import_picture_marker_symbols"));
	}

	public Set<String> getAllowedFileExtensions() {
		if (allowedFileExtensions == null) {
			allowedFileExtensions = new HashSet<String>();
			allowedFileExtensions.add("png");
			allowedFileExtensions.add("jpg");
			allowedFileExtensions.add("jpeg");
			allowedFileExtensions.add("gif");
			allowedFileExtensions.add("bmp");
			allowedFileExtensions.add("svg");
		}
		return allowedFileExtensions;
	}

	public YesNoAsk getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(YesNoAsk overwrite) {
		this.overwrite = overwrite;
	}

	public double getSymbolSize() {
		return symbolSize;
	}

	public void setSymbolSize(double symbolSize) {
		this.symbolSize = symbolSize;
	}

	public String getTargetFolderName() {
		return targetFolderName;
	}

	public void setTargetFolderName(String targetFolderName) {
		this.targetFolderName = targetFolderName;
	}

	public List<File> getSelectedImageFiles() {
		return selectedImageFiles;
	}

	public boolean isQuietMode() {
		return quietMode;
	}

	public void setQuietMode(boolean quietMode) {
		this.quietMode = quietMode;
	}

	public FolderSet getSymbolsRepository() {
		File symbolsFolder = new File(getSymbolManager().getSymbolPreferences()
				.getSymbolLibraryPath());

		FolderSet repo = new DefaultFolderSet();
		repo.add(symbolsFolder);
		return repo;
	}

	public List<File> getNotCreatedSymbols() {
		return notCreatedSymbols;
	}

	public void run() {
		SimpleTaskStatus status = null;
		try {
			status = (SimpleTaskStatus) this.getTaskStatus();
			this.preProcess();
			if (status.isCancellationRequested()) {
				status.cancel();
				return;
			}
			status.add();
			status.setRangeOfValues(1, this.getSelectedImageFiles().size());
			for (int i = 0; i < this.getSelectedImageFiles().size(); i++) {
				if (status.isCancellationRequested()) {
					status.cancel();
					return;
				}
				status.setCurValue(i+1);
				this.processItem(this.getSelectedImageFiles().get(i));
			}
			this.postPrecess();
			if (status.isCancellationRequested()) {
				status.cancel();
				return;
			}
			if (!this.isQuietMode()) {
				application.messageDialog(
				    "_Import_of_symbols_completed",
				    this.getName(),
				    JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			logger.info("Error in process", e);
			if (status != null) {
				status.abort();
			}
			if (!this.isQuietMode()) {
				application.message(
						"_Import_of_symbols_aborted" + "\n\n" + e.getMessage(),
						JOptionPane.WARNING_MESSAGE);
				application.messageDialog(
						"_Error_in_the_process_of_import_point_symbols_Some_symbols_cant_be_imported"
								+ "\n\n" + e.getMessage(), this.getName(),
						JOptionPane.WARNING_MESSAGE);
			}
		} finally {
			if (status != null) {
				// Mark process as terminate if it is running.
				if (status.isRunning()) {
					status.terminate();
				}
			}
			application.refreshMenusAndToolBars();
		}
	}

	private void preProcess() {
		if (this.isQuietMode() && this.getOverwrite() == YesNoAsk.ASK) {
			this.setOverwrite(YesNoAsk.YES);
		} else {
			Component dlg = application.createComponent(ImportPictureMarkerSymbolsPanel.class, this);
			application.showDialog(dlg, this.getName());
		}
	}

	private void processItem(File item) throws InterruptedException {
		File source_image;
		File source_image_sel;
		File target_image;
		File target_image_sel;
		File target_symbol;
		IPictureMarkerSymbol symbol;
		PersistenceManager persistenceManager = ToolsLocator
				.getPersistenceManager();

		if (isSelectedImage(item)) {
		    return;
		}
		
		source_image = this.getSourceImage(item);
		source_image_sel = this.getSourceImageSelected(item);
		target_image = this.getTargetImage(item);
		target_image_sel = this.getTargetImageSelected(item);
		target_symbol = this.getTargetSymbol(item);

		if (!source_image_sel.exists()) {
			source_image_sel = source_image;
		}

		if (target_symbol.exists()) {
			switch (this.getOverwrite()) {
			case ASK:
				int resp = application
						.confirmDialog(application.translate(
								"_Symbol_{0}_already_exists_Overwrite",
								target_symbol.getAbsolutePath()), this
								.getName(), JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (resp == JOptionPane.NO_OPTION) {
					return;
				}
				break;
			case NO:
				return;
			case YES:
				break;
			}
		}
		try {
			if( !target_image.getParentFile().exists() ) {
				target_image.getParentFile().mkdirs();
			}
			copyFile(source_image, target_image);
			copyFile(source_image_sel, target_image_sel);
			symbol = this.getSymbologyManager().createPictureMarkerSymbol(
					target_image.toURI().toURL(),
					target_image_sel.toURI().toURL());
			symbol.setSize(this.getSymbolSize());
			symbol.setDescription(FilenameUtils.getBaseName(target_image.getName()));

			PersistentState state = persistenceManager.getState(symbol);
			FileOutputStream out = new FileOutputStream(target_symbol);
			persistenceManager.saveState(state, out);
		} catch (Exception e) {
			logger.info("Can't create symbol from '" + item.getAbsolutePath()
					+ ".", e);
			this.notCreatedSymbols.add(item);
		}
	}

	/**
     * @param item
     * @return
     */
    private boolean isSelectedImage(File item) {
        
        String ext = FilenameUtils.getExtension(item.getName());
        String name = FilenameUtils.getBaseName(item.getName());

        File orig = null;
        String suf = null;
        for (int i=0; i<SELECTION_SUFFIX.length; i++) {
            suf = SELECTION_SUFFIX[i];
            if (name.endsWith(suf)) {
                orig = new File(item.getParentFile(), name.substring(0, name.length() - suf.length()) + "." + ext);
                if (orig.exists()) {
                    return true;
                }
            }
        }
        return false;
    }

    private void postPrecess() {
		if (!this.isQuietMode()) {
			if (this.notCreatedSymbols.size() > 0) {
				StringBuffer listOfFiles = new StringBuffer();
				listOfFiles.append("\n\n");
				for (File file : this.notCreatedSymbols) {
					listOfFiles.append(file.getAbsolutePath());
					listOfFiles.append("\n");
				}
				application.message(
						application.translate("_many_symbols_can_be_createds")
								+ listOfFiles.toString(),
						JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void copyFile(File source, File target) throws IOException {
		if (target.exists()) {
			switch (this.getOverwrite()) {
			case ASK:
				int resp = application
						.confirmDialog(application.translate(
								"_File_{0}_already_exists_Overwrite",
								target.getAbsolutePath()), this.getName(),
								JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (resp == JOptionPane.YES_OPTION) {
					FileUtils.copyFile(source, target);
				}
				break;
			case NO:
				break;
			case YES:
				FileUtils.copyFile(source, target);
				break;
			}
		} else {
			FileUtils.copyFile(source, target);
		}

	}

	private File getTargetSymbol(File item) {
		File target = FileUtils.getFile(getSymbolsRepository()
				.asFile(), this.getTargetFolderName(),
				FilenameUtils.getBaseName(item.getName())
						+ this.getSymbolManager().getSymbolPreferences()
								.getSymbolFileExtension());
		return target;
	}

	private File getSourceImage(File item) {
		return item;
	}

	private File getSourceImageSelected(File item) {
		String ext = FilenameUtils.getExtension(item.getName());
		String name = FilenameUtils.getBaseName(item.getName());
		
        File source = null;
        String suf = null;
        for (int i=0; i<SELECTION_SUFFIX.length; i++) {
            suf = SELECTION_SUFFIX[i];
            source = new File(item.getParentFile(), name + suf + "." + ext);
            if (source.exists()) {
                return source;
            }
        }

        source = new File(item.getParentFile(), name + SELECTION_SUFFIX[0] + "." + ext);
		return source;
	}

	private File getTargetImage(File item) {
		File target = FileUtils.getFile(getSymbolsRepository()
				.asFile(), this.getTargetFolderName(), item.getName());
		return target;
	}

	private File getTargetImageSelected(File item) {
		String ext = FilenameUtils.getExtension(item.getName());
		String name = FilenameUtils.getBaseName(item.getName());
		File target = FileUtils.getFile(getSymbolsRepository()
				.asFile(), this.getTargetFolderName(), name + "_sel." + ext);
		return target;
	}

	private SymbolManager getSymbolManager() {
		if (symbolManager == null) {
			symbolManager = MapContextLocator.getSymbolManager();
		}
		return symbolManager;
	}

	private SymbologyManager getSymbologyManager() {
		if (symbologyManager == null) {
			symbologyManager = SymbologyLocator.getSymbologyManager();
		}
		return symbologyManager;
	}

}
