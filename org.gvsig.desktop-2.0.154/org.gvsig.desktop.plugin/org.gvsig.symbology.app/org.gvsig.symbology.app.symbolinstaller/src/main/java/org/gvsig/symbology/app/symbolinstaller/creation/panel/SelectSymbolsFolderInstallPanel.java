/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.creation.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.i18n.Messages;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class SelectSymbolsFolderInstallPanel extends JPanel {

    private static final long serialVersionUID = 948949741432221418L;

    private JList folderJList;
    private File symbolsDirectory = null;
    private ArrayList<String> folders = null;

    public SelectSymbolsFolderInstallPanel() {
        super();
        initComponents();
    }

    private String getText(String string) {
        return Messages.getText(string);
    }

    private void initComponents() {

        JLabel symbolsFolderLabel =
            new JLabel(
                getText("_select_the_folder_to_create_the_package_with")
                    + ":");

        SymbolManager sym_man = MapContextLocator.getSymbolManager();
        symbolsDirectory =
            new File(sym_man.getSymbolPreferences().getSymbolLibraryPath());

        String[] itemsArray = symbolsDirectory.list();
        folders = new ArrayList<String>();

        for (int i = 0; i < itemsArray.length; i++) {
            File f = new File(symbolsDirectory, itemsArray[i]);
            if (f.isDirectory()) {
                folders.add(itemsArray[i]);
            }
        }

        folderJList = new JList(folders.toArray());
        folderJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // TODO: actionEvent should be added
        folderJList.setSelectedIndex(0);

        setLayout(new BorderLayout(2, 2));
        setBounds(2, 2, 2, 2);

        add(symbolsFolderLabel, BorderLayout.NORTH);

        JScrollPane scroller = new JScrollPane(folderJList);

        add(scroller);
    }

    public File getSelectedFolder() {
        String folderName = folders.get(folderJList.getSelectedIndex());
        return new File(symbolsDirectory + "/" + folderName.toString());
    }

    public void setSelectedFolder(File selectedFile) {
        folderJList.setSelectedValue(selectedFile, true);
    }

    public void actionPerformed(ActionEvent arg0) {
        // TODO Auto-generated method stub
    }

}
