/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.creation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JPanel;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.gui.beans.wizard.WizardPanel;
import org.gvsig.gui.beans.wizard.WizardPanelActionListener;
import org.gvsig.gui.beans.wizard.WizardPanelWithLogo;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.symbology.app.symbolinstaller.WizardListenerAdapter;
import org.gvsig.symbology.app.symbolinstaller.creation.wizard.OutputWizard;
import org.gvsig.symbology.app.symbolinstaller.creation.wizard.PackageInfoWizard;
import org.gvsig.symbology.app.symbolinstaller.creation.wizard.ProgressWizard;
import org.gvsig.symbology.app.symbolinstaller.creation.wizard.SelectSymbolsFolderInstallWizard;
import org.gvsig.symbology.app.symbolinstaller.execution.SymbolInstallerExecutionProviderFactory;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.locator.LocatorException;

public class DefaultMakeSymbolPackageWizard extends JPanel implements
    WizardPanel {

    
    private static final long serialVersionUID = 9205891710214122265L;
    private WizardPanelWithLogo wizardPanelWithLogo = null;

    // Wizards
    private PackageInfoWizard packageInfoWizard = null;
    private ProgressWizard progressWizard = null;
    private OutputWizard outputWizard = null;
    private SelectSymbolsFolderInstallWizard selectSymbolsFolderInstallWizard =
        null;
    private PackageInfo packageInfo = null;

    private File symbolFolderToInsertInPackage = null;

    private WizardListenerAdapter wizardListenerAdapter = null;

    public DefaultMakeSymbolPackageWizard() throws LocatorException {
        wizardPanelWithLogo = new WizardPanelWithLogo(); //IconThemeHelper.getImageIcon("wizard-make-symbol-package"));

        setCancelButtonEnabled(true);
        setFinishButtonEnabled(false);

        // WizardPanel wizardPanel = this;

        packageInfo =
        ToolsLocator.getPackageManager().createPackageInfo();
        packageInfo
            .setType(SymbolInstallerExecutionProviderFactory.PROVIDER_NAME);

        selectSymbolsFolderInstallWizard =
            new SelectSymbolsFolderInstallWizard(this, packageInfo);
        packageInfoWizard = new PackageInfoWizard(this, packageInfo);
        outputWizard = new OutputWizard(this, packageInfo);
        progressWizard =
            new ProgressWizard(this, packageInfo, (JOutputPanel) (outputWizard
                .getJPanel()));

        addWizards();

        // Adding the listeners
        wizardPanelWithLogo.setWizardListener(this);

        this.setLayout(new BorderLayout());
        this.add(wizardPanelWithLogo, BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(700, 550));
    }

    private void addWizards() {
        wizardPanelWithLogo.addOptionPanel(selectSymbolsFolderInstallWizard);
        wizardPanelWithLogo.addOptionPanel(packageInfoWizard);
        wizardPanelWithLogo.addOptionPanel(outputWizard);
        wizardPanelWithLogo.addOptionPanel(progressWizard);
    }

    public void setNextButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setNextButtonEnabled(isEnabled);
    }

    public void setCancelButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setCancelButtonEnabled(isEnabled);
    }

    public void setFinishButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setFinishButtonEnabled(isEnabled);
    }

    public void setBackButtonEnabled(boolean isEnabled) {
        wizardPanelWithLogo.setBackButtonEnabled(isEnabled);
    }

    public WizardPanelActionListener getWizardPanelActionListener() {
        if (wizardListenerAdapter == null) { // && (getWizardActionListener() !=
            // null))) {
            return new WizardListenerAdapter(this);
        }
        return wizardListenerAdapter;
    }

    public void setWizardPanelActionListener(
        WizardPanelActionListener wizardActionListener) {
        // TODO Auto-generated method stub
        // this.wizardListenerAdapter = wizardActionListener;
        throw new RuntimeException("Esto falta por ver que hace");
    }

    public void setSymbolFolderToInsertInPackage(File file) {
        symbolFolderToInsertInPackage = file;
        
        File pinfo = new File(file, "package.info");
        if (pinfo.exists()) {
            try {
                ToolsLocator.getPackageManager().readPacakgeInfo(
                    (org.gvsig.tools.packageutils.PackageInfo) packageInfo, pinfo);
                packageInfo
                    .setType(SymbolInstallerExecutionProviderFactory.PROVIDER_NAME);
            } catch (Exception e) {
                /*
                 * Do nothing, packageInfo remains empty
                 * form will be empty
                 * 
                 */
            }
        }
        
        
    }

    public File getSymbolFolderToInsertInPackage() {
        return symbolFolderToInsertInPackage;
    }

}
