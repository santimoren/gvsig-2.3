/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.execution;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.service.spi.AbstractProviderFactory;
import org.gvsig.tools.service.spi.Provider;
import org.gvsig.tools.service.spi.ProviderServices;

public class SymbolInstallerExecutionProviderFactory extends
    AbstractProviderFactory {

    public static final String PROVIDER_NAME = "symbols";
    public static final String PROVIDER_DESCRIPTION = "";

    private DynClass dynclass;

    @Override
    protected DynClass createParametersDynClass() {
        if (dynclass == null) {
            initialize();
        }
        return dynclass;
    }

    @Override
    protected Provider doCreate(DynObject parameters, ProviderServices services) {
        return new SymbolInstallerExecutionProvider(services);
    }

    public void initialize() {
        if (dynclass == null) {
            DynObjectManager dynObjectManager =
                ToolsLocator.getDynObjectManager();
            dynclass =
                dynObjectManager.createDynClass(PROVIDER_NAME,
                    PROVIDER_DESCRIPTION);
            dynObjectManager.add(dynclass);
        }
    }

}
