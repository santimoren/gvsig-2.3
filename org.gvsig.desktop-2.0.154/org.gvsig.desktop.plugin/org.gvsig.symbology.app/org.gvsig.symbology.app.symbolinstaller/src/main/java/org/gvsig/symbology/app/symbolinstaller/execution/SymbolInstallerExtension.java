/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.execution;

import java.io.File;
import java.util.Locale;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.ToolsWindowManager;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.symbology.app.symbolinstaller.creation.DefaultMakeSymbolPackageWizard;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class SymbolInstallerExtension extends Extension {

    public void initialize() {

        IconThemeHelper.registerIcon("symbol-installer", "wizard-make-symbol-package", this);
    }

    @Override
    public void postInitialize() {
        InstallerManager im = InstallerLocator.getInstallerManager();
        
        SymbolManager sym_man = MapContextLocator.getSymbolManager();
        File symbolsDirectory =
            new File(sym_man.getSymbolPreferences().getSymbolLibraryPath());

        im.setDefaultLocalAddonRepository(symbolsDirectory,SymbolInstallerExecutionProviderFactory.PROVIDER_NAME);
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return true;
    }

    public void execute(String actionCommand) {
        if (actionCommand.equals("Create_Package")) {
            DefaultMakeSymbolPackageWizard wizard =
                new DefaultMakeSymbolPackageWizard();

            WindowManager windowManager = new ToolsWindowManager();
            windowManager.showWindow(wizard, Messages.getText("_Create_package_symbols"),
                WindowManager.MODE.DIALOG);
        }
    }
}
