/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.symbology.app.symbolinstaller.creation.wizard;

import java.io.File;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.creation.JOutputPanel;
import org.gvsig.symbology.app.symbolinstaller.creation.DefaultMakeSymbolPackageWizard;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class OutputWizard implements OptionPanel {

    private JOutputPanel outputPanel;
    private PackageInfo packageInfo;

    // private DefaultMakeSymbolPackageWizard wizard;

    // private static final Logger logger =
    // LoggerFactory.getLogger(OutputWizard.class);

    public OutputWizard(DefaultMakeSymbolPackageWizard wizard,
        PackageInfo packageInfo) {
        super();
        // this.wizard = wizard;
        outputPanel =
            SwingInstallerLocator.getSwingInstallerManager()
                .createOutputPanel();
        this.packageInfo = packageInfo;
        this.outputPanel.setPackageInfo(packageInfo);
    }

    public JPanel getJPanel() {
        return this.outputPanel;
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

    public String getPanelTitle() {
        return getText("_output_options");
    }

    public void lastPanel() {

    }

    public void nextPanel() throws NotContinueWizardException {
        // try {
        File file = outputPanel.getPackageFile();
        File filePath = new File(file.getParent());
        if (!filePath.exists()) {
            JOptionPane.showMessageDialog(outputPanel,
                getText("_the_package_file_folder_does_not_exist") + ": "
                    + filePath);
            throw new NotContinueWizardException("", null, false);
        }
        if (outputPanel.isCreatePackageIndex()) {
            URL url = outputPanel.getDownloadURL();
            if (url == null) {
                throw new NotContinueWizardException("", null, false);
            }
            File indexFile = outputPanel.getPackageIndexFile();
            File indexFilePath = new File(indexFile.getParent());
            if (!indexFilePath.exists()) {
                JOptionPane.showMessageDialog(outputPanel,
                    getText("_the_package_index_file_folder_does_not_exist")
                        + ": " + indexFilePath);
                throw new NotContinueWizardException("", null, false);
            }
        }
        // } catch (FileNotFoundException e) {
        // logger.info(
        // getText("create_output_file_exception"), e);
        // JOptionPane.showMessageDialog(wizard,
        // getText("create_output_file_exception"));
        // }
    }

    public void updatePanel() {
        outputPanel.setPackageFile(getDefaultPackageBundleFile());
        outputPanel.setPackageIndexFile(getDefaultPackageIndexBundleFile());
    }

    private File getDefaultPackageBundleFile() {
        PluginsManager manager = PluginsLocator.getManager();
        File installsFolder = manager.getInstallFolder();
        String fileName =
            InstallerLocator.getInstallerManager().getPackageFileName(
                packageInfo);
        return new File(installsFolder, fileName);
    }

    private File getDefaultPackageIndexBundleFile() {
        PluginsManager manager = PluginsLocator.getManager();
        File installsFolder = manager.getInstallFolder();
        String fileName =
            InstallerLocator.getInstallerManager().getPackageIndexFileName(
                packageInfo);
        return new File(installsFolder, fileName);
    }

    
}
