/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.execution;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.execution.InstallPackageServiceException;
import org.gvsig.installer.lib.spi.InstallPackageProviderServices;
import org.gvsig.installer.lib.spi.InstallerProviderLocator;
import org.gvsig.installer.lib.spi.InstallerProviderManager;
import org.gvsig.installer.lib.spi.execution.InstallPackageProvider;
import org.gvsig.tools.service.spi.AbstractProvider;
import org.gvsig.tools.service.spi.ProviderServices;

public class SymbolInstallerExecutionProvider extends AbstractProvider
    implements InstallPackageProvider {

    public SymbolInstallerExecutionProvider(ProviderServices providerServices) {
        super(providerServices);
    }

    public class InstallerSymbolsDirectoryNotFoundException extends
        InstallPackageServiceException {

        private static final long serialVersionUID = 4416143986837955880L;
        private static final String message = "Symbols directory not found";
        private static final String KEY = "symbols_directory_not_found";

        public InstallerSymbolsDirectoryNotFoundException() {
            super(message, KEY, serialVersionUID);
        }
    }

    public void install(File applicationDirectory, InputStream inputStream,
        PackageInfo packageInfo) throws InstallPackageServiceException {

        
        SymbolManager sym_man = MapContextLocator.getSymbolManager();
        File symbolsDirectory =
            new File(sym_man.getSymbolPreferences().getSymbolLibraryPath());

        try {
            if (!symbolsDirectory.exists()) {
                throw new InstallerSymbolsDirectoryNotFoundException();
            }
            InstallerProviderManager installerProviderManager =
                InstallerProviderLocator.getProviderManager();
            InstallPackageProviderServices installerProviderServices =
                installerProviderManager.createInstallerProviderServices();

            installerProviderServices.decompress(inputStream, symbolsDirectory);

        } catch (Exception e) {
            throw new InstallPackageServiceException(e);
        }
    }

    public void installLater(File applicationDirectory,
        InputStream inputStream, PackageInfo packageInfo)
        throws InstallPackageServiceException, IOException {
        // TODO Auto-generated method stub

    }
}
