/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.creation.wizard;

import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.symbology.app.symbolinstaller.creation.DefaultMakeSymbolPackageWizard;
import org.gvsig.symbology.app.symbolinstaller.creation.panel.SelectSymbolsFolderInstallPanel;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class SelectSymbolsFolderInstallWizard implements OptionPanel {

    private SelectSymbolsFolderInstallPanel panel = null;
    // private PackageInfo packageInfo;
    DefaultMakeSymbolPackageWizard wizard;

    /**
     * @param defaultMakeSymbolPackageWizard
     * @param packageInfo
     */
    public SelectSymbolsFolderInstallWizard(
        DefaultMakeSymbolPackageWizard defaultMakeSymbolPackageWizard,
        PackageInfo packageInfo) {
        super();
        wizard = defaultMakeSymbolPackageWizard;
        panel = new SelectSymbolsFolderInstallPanel();
        // this.packageInfo = packageInfo;
    }

    public JPanel getJPanel() {
        return panel;
    }

    public void lastPanel() {

    }

    public void nextPanel() throws NotContinueWizardException {

        wizard.setSymbolFolderToInsertInPackage(panel.getSelectedFolder());
    }

    public void updatePanel() {

    }

    public String getPanelTitle() {
        return getText("_select_symbol_folder");
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

}
