/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.symbology.app.symbolinstaller.creation.wizard;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

import javax.swing.JPanel;

import org.gvsig.gui.beans.wizard.panel.NotContinueWizardException;
import org.gvsig.gui.beans.wizard.panel.OptionPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.creation.JPackageInfoPanel;
import org.gvsig.symbology.app.symbolinstaller.creation.DefaultMakeSymbolPackageWizard;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class PackageInfoWizard implements OptionPanel {

    private JPackageInfoPanel packageInfoPanel;

    private PackageInfo packageInfo;

    // private DefaultMakeSymbolPackageWizard wizard;

    public PackageInfoWizard(DefaultMakeSymbolPackageWizard wizard,
        PackageInfo packageInfo) {
        super();
        // this.wizard = wizard;
        this.packageInfoPanel =
            SwingInstallerLocator.getSwingInstallerManager()
                .createPackageInfoPanel();
        this.packageInfo = packageInfo;
    }

    private String getText(String msg) {
        return Messages.getText(msg);
    }

    public JPanel getJPanel() {
        return this.packageInfoPanel;
    }

    public String getPanelTitle() {
        return getText("_plugin_description");
    }

    public void lastPanel() {
        // TODO Auto-generated method stub

    }

    public void nextPanel() throws NotContinueWizardException {
        if (!this.packageInfoPanel.validatePanel()) {
            throw new NotContinueWizardException("", null, false);
        }
        this.packageInfoPanel.panelToPackageInfo(packageInfo);
    }

    public void updatePanel() {
        this.packageInfoPanel.packageInfoToPanel(packageInfo);
    }

}
