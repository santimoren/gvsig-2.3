/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.installer.app.extension.creation;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class MakePluginPackageExtension extends Extension {

	private static final Logger LOG = LoggerFactory
			.getLogger(MakePluginPackageExtension.class);

	public void execute(String actionCommand) {
		if ("tools-devel-create-package".equalsIgnoreCase(actionCommand)) {
			PluginsManager manager = PluginsLocator.getManager();

			try {
				PluginServices.getMDIManager().addCentredWindow(
						new MakePluginPackageWindow(manager
								.getApplicationFolder(), manager
								.getInstallFolder()));
			} catch (Exception e) {
				LOG.error("Error creating teh wizard to create an installer ",
						e);
			}
		}
	}

	public void initialize() {

	}

	@Override
	public void postInitialize() {
		super.postInitialize();
	    PluginsManager pm = PluginsLocator.getManager();
	    PackageInfo packageInfo = pm.getPackageInfo();
		Version version = packageInfo.getVersion();
		
		SwingInstallerLocator.getSwingInstallerManager().setApplicationVersion(version.format("%M.%m.%r"));
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		return true;
	}

}
