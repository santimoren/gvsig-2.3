/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */
package org.gvsig.installer.app.extension.execution;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.installer.lib.api.InstallerLocator;
import org.gvsig.installer.lib.api.InstallerManager;
import org.gvsig.installer.lib.api.PackageInfo;
import org.gvsig.installer.lib.api.Version;
import org.gvsig.installer.swing.api.SwingInstallerLocator;
import org.gvsig.installer.swing.api.SwingInstallerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class InstallPackageExtension extends Extension {

    private static final Logger logger = LoggerFactory
            .getLogger(InstallPackageExtension.class);

    public void execute(String actionCommand) {
        this.execute(actionCommand, null);
    }

    public void execute(String actionCommand, Object[] args) {

        if ("tools-addonsmanager".equalsIgnoreCase(actionCommand)) {
//            InstallerManager installManager = InstallerLocator.getInstallerManager();
//            if (installManager.needAdminRights()) {
//                showExternalAddonsManager();
//            } else {
                showInternalAddonsManager(args);
//            }
        }
    }

    private void showExternalAddonsManager() {
        InstallerManager installManager = InstallerLocator.getInstallerManager();

        PluginsManager pluginManager = PluginsLocator.getManager();
        
        String cmd = null;
        boolean useInternalAddonsManager = false;

        if( InstallerManager.OS.WINDOWS.equalsIgnoreCase(installManager.getOperatingSystem()) ) {
            cmd = "runas /noprofile /user:Administrator " + pluginManager.getApplicationFolder() + File.separator + "gvsig-package-installer.exe --install";
            try {
                Process p = Runtime.getRuntime().exec(cmd);
                if( p.exitValue() != 0 ) {
                    useInternalAddonsManager = true;
                }
            } catch (IOException ex) {
                logger.warn("Can't execute command '"+cmd+"'.",ex);
                useInternalAddonsManager = true;
            }
        } else {
            cmd = "pkexec " + pluginManager.getApplicationFolder() + File.separator + "/gvSIG.sh --install";
            try {
                Process p = Runtime.getRuntime().exec(cmd);
                if( p.exitValue() != 0 ) {
                    useInternalAddonsManager = true;
                }
            } catch (IOException ex) {
                logger.warn("Can't execute command '"+cmd+"'.",ex);
                useInternalAddonsManager = true;
            }
        }
        if( useInternalAddonsManager ) {
            logger.warn("Use internal addons manager.");
            showInternalAddonsManager(null);
        }
    }
    
    
    private void showInternalAddonsManager(Object[] args) {
        boolean skipBundleSelection = false;
        if (args != null && args.length > 0) {
            String subcmd = (String) args[0];
            if ("skipBundleSelection".equalsIgnoreCase(subcmd)) {
                skipBundleSelection = true;
            }
        }
        PluginsManager manager = PluginsLocator.getManager();
        try {
            PluginServices.getMDIManager().addCentredWindow(
                    new InstallPackageWindow(
                            manager.getApplicationFolder(),
                            manager.getInstallFolder(),
                            skipBundleSelection
                    )
            );
        } catch (Error e) {
            logger.error("Error creating the wizard to install a package ", e);
        } catch (Exception e) {
            logger.error("Error creating the wizard to install a package ", e);
        }

    }

    public void initialize() {
        PluginsManager pm = PluginsLocator.getManager();
        PackageInfo packageInfo = pm.getPackageInfo();
        Version version = packageInfo.getVersion();

        InstallerLocator.getInstallerManager().setVersion(version);
        try {
            IconThemeHelper.registerIcon("action", "tools-addonsmanager", this);

            SwingInstallerManager manager = SwingInstallerLocator
                    .getSwingInstallerManager();

            try {
                manager.setDefaultDownloadURL(
                        new File( pm.getApplicationFolder(),"gvsig-installer-urls.config")
                );
                manager.setDefaultDownloadURL(
                        new File( pm.getPlugin(this).getPluginDirectory(),"defaultDownloadsURLs")
                );
                if (packageInfo != null && (packageInfo.getState().startsWith(InstallerManager.STATE.BETA)
                        || packageInfo.getState().startsWith(InstallerManager.STATE.RC)
                        || packageInfo.getState().equalsIgnoreCase(InstallerManager.STATE.FINAL))) {

                    String installURL = manager.getInstallerManager().getDownloadBaseURL().toString()
                            + "dists/<%Version%>/builds/<%Build%>/packages.gvspki ## Official gvSIG repository (frozen in this version)";
                    manager.addDefaultDownloadURL(installURL);
                }
            } catch (Throwable th) {
                logger.info("Error. Can't select default gvspki", th);
            }

            InputStream is = this.getClass().getResourceAsStream(
                    "/defaultDownloadsURLs");
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String line = null;
            for (line = in.readLine(); line != null; line = in.readLine()) {
                try {
                    manager.addDefaultDownloadURL(line);
                } catch (MalformedURLException e) {
                    logger.error(
                            "Error creating the default packages download URL pointing to "
                            + line, e);
                }
            }
            manager.getInstallerManager().setVersion(version);
        } catch (Throwable e) {
            logger.error("Error reading the default packages download URL file "
                    + "/defaultDownloadsURLs", e);
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return true;
    }

}
