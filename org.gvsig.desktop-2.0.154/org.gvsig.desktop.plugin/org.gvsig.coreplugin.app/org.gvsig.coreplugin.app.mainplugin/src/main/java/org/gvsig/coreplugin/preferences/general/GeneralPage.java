/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.preferences.general;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.preferences.AbstractPreferencePage;

/**
 * First default page in the Preferences Dialog. It is supposed to be
 * used to hold simple user interaction settings such as:
 * <ol>
 * 	<li>
 * 		Window positions.
 *  </li>
 * 	<li>
 * 		Mouse behaviors (double click vs. simple click).
 *  </li>
 * 	<li>
 * 		Users' language preferences.
 *  </li>
 * 	<li>
 * 		Confirm overwriting files.
 *  </li>
 *  <li>
 *  	Toolbars config
 *  </li>
 * </ol>
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class GeneralPage extends AbstractPreferencePage {
	private ImageIcon icon;
	public static String id;
	
	private JCheckBox rememberWindowPosition;
	private JCheckBox rememberWindowSize;

	public GeneralPage() {
		super();
		initialize();
		id = this.getClass().getName();
	}

	private void initialize() {
		icon = PluginServices.getIconTheme().get("edit-setup-general");
		
		/*
		 * Removed until behavior is clear when
		 * we do not store pos/size
		 * 
		// remember windows position check box
		addComponent(rememberWindowPosition = new JCheckBox(PluginServices.getText(this, "options.general.remember_windows_pos")));
		// remember windows sizes check box
		addComponent(rememberWindowSize = new JCheckBox(PluginServices.getText(this, "options.general.remember_windows_size")));
		*/
	}


	public String getID() {
		return id;
	}

	public String getTitle() {
		return PluginServices.getText(this, "pref.general");
	}

	public JPanel getPanel() {
		return this;
	}

	public void initializeValues() {
	}

	public void storeValues() {
	}

	public void initializeDefaults() {
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public boolean isValueChanged() {
		return super.hasChanged();
	}

	public void setChangesApplied() {
		setChanged(false);
	}

	
    
}
