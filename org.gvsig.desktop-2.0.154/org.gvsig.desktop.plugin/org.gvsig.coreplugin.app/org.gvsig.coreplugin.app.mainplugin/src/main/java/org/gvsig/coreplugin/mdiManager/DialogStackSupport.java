/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.mdiManager;

import java.awt.Cursor;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JDialog;

import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *
 */
public class DialogStackSupport{
    /** log */
    private static Logger logger = LoggerFactory.getLogger(DialogStackSupport.class.getName());

    /** Pila de di�logos modales mostrados */
    private ArrayList dialogStack = new ArrayList(0);

    private ArrayList dialogCursors = new ArrayList(0);

    public DialogStackSupport(MDIFrame frame){
    }

    public void pushDialog(JDialog dlg){
    	dialogStack.add(dlg);
    }

    public JDialog popDialog(){
    	int dialogStackSize=dialogStack.size();
    	if (dialogStackSize<1)
    		return null;
    	return (JDialog) dialogStack.remove(dialogStackSize - 1);
    }

	/**
	 * @return
	 */
	public void setWaitCursor() {
        dialogCursors.clear();
		for (Iterator iter = dialogStack.iterator(); iter.hasNext();) {
			JDialog dlg = (JDialog) iter.next();
            dialogCursors.add(dlg.getCursor());
			dlg.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
 	        dlg.getGlassPane().setVisible(true);
 	   }
	}

	/**
	 *
	 */
	public void restoreCursor() {
		for (Iterator iter = dialogStack.iterator(); iter.hasNext();) {
			JDialog dlg = (JDialog) iter.next();

            // TODO: RECUPERAR EL CURSOR
            dlg.setCursor(null);
		    dlg.getGlassPane().setVisible(false);
		}
	}
}
