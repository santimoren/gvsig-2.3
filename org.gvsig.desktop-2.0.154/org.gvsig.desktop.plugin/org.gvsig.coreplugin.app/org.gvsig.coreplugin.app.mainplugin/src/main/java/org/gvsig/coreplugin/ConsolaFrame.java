/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin;

import java.awt.BorderLayout;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.MessageEvent;
import org.gvsig.andami.messages.NotificationListener;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;


/**
 * Frame que escucha los eventos del sistema de mensajes de la aplicaci�n y los
 * muestra.
 */
public class ConsolaFrame extends JPanel implements IWindow, SingletonWindow,NotificationListener {
	private StringBuffer info = new StringBuffer();
	private StringBuffer warn = new StringBuffer();
	private StringBuffer error = new StringBuffer();
	private StringBuffer all = new StringBuffer();
	private JEditorPane textoInfos;
	private JEditorPane textoWarnings;
	private JEditorPane textoErrores;
	private JEditorPane textoAll;

	private final static String marcaInfo ="font size='3' color='#000000'";
	private final static String marcaWarning ="font size='3' color='#800000'";
	private final static String marcaError ="font size='3' color='#FF0000'";
	private JTabbedPane tabs;
	private JPanel panelErrores;

    /**
     * Crea la consola con los mensajes producidos hasta el momento de la
     * creaci�n de este objeto.
     *
     * @param todo Array de todos los mensajes
     * @param infos Array de los mensajes de informaci�n
     * @param warns Array de los mensajes de warning
     * @param errors Array de los mensajes de error
     */
    public ConsolaFrame() {

		this.setSize(400, 325);

        //Las cajas de texto donde van los mensajes
        textoInfos = new JEditorPane();
        textoInfos.setEditable(false);
		textoInfos.setContentType("text/html");
        textoWarnings = new JEditorPane();
        textoWarnings.setEditable(false);
		textoWarnings.setContentType("text/html");
        textoErrores = new JEditorPane();
        textoErrores.setEditable(false);
		textoErrores.setContentType("text/html");
        textoAll = new JEditorPane();
        textoAll.setEditable(false);
		textoAll.setContentType("text/html");

		JScrollPane scroll = new JScrollPane(textoAll);
		scroll.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(
		JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		scroll.setPreferredSize(new Dimension(500, 500));
//		scroll.setMinimumSize(new Dimension(10, 10));
		JPanel panelAll = new JPanel(new BorderLayout());
		panelAll.add(scroll, BorderLayout.CENTER);

		scroll = new JScrollPane(textoInfos);
		scroll.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		scroll.setPreferredSize(new Dimension(250, 145));
//		scroll.setMinimumSize(new Dimension(10, 10));
		JPanel panelInfos = new JPanel(new BorderLayout());
		panelInfos.add(scroll, BorderLayout.CENTER);

		scroll = new JScrollPane(textoWarnings);
		scroll.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(
		JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		scroll.setPreferredSize(new Dimension(250, 145));
//		scroll.setMinimumSize(new Dimension(10, 10));
		JPanel panelWarnings = new JPanel(new BorderLayout());
		panelWarnings.add(scroll, BorderLayout.CENTER);

		scroll = new JScrollPane(textoErrores);
		scroll.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(
		JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		scroll.setPreferredSize(new Dimension(250, 145));
//		scroll.setMinimumSize(new Dimension(10, 10));
		panelErrores = new JPanel(new BorderLayout());
		panelErrores.add(scroll, BorderLayout.CENTER);

//        rellenar(infos, textoInfos, info, marcaInfo);
  //      rellenar(warns, textoWarnings, warn, marcaWarning);
    //    rellenar(errors, textoErrores, error, marcaError);
      //  rellenarTodo(todo, textoAll);

        //Las pesta�as
        tabs = new JTabbedPane();
        tabs.addTab(PluginServices.getText( this ,"todos"), panelAll);
        tabs.addTab(PluginServices.getText( this ,"info"), panelInfos);
        tabs.addTab(PluginServices.getText( this ,"warnings"), panelWarnings);
        tabs.addTab(PluginServices.getText( this ,"errores"), panelErrores);
		this.setLayout(new BorderLayout());
        this.add(tabs, BorderLayout.CENTER);

    }

	/**
	 * M�todo de utilidad para rellenar los cuadros de texto con un array de
	 * cadenas
	 *
	 * @param contenido array de cadenas que se mostrar�n en el cuadro de texto
	 *        separadas por un retorno de carro
	 * @param control Control en el que se pondr�n las cadenas
	 */
	private void rellenar(String[] contenido, JEditorPane control, StringBuffer buffer, String marca) {
		String content = "";
		for (int i = 0; i < contenido.length; i++) {
			content = content + "<"+marca+">"+contenido[i] + "</"+marca+"><br/";
		}

		buffer.append(content);

		control.setContentType("text/html");
		control.setText(content);
	}

	/*
	 * M�todo de utilidad para rellenar los cuadros de texto con un array de
	 * cadenas
	 *
	 * @param contenido array de cadenas que se mostrar�n en el cuadro de texto
	 *        separadas por un retorno de carro
	 * @param control Control en el que se pondr�n las cadenas
	 *
	private void rellenarTodo(Message[] contenido, JEditorPane control) {
		HashMap marcasTipoMsg = new HashMap();
		marcasTipoMsg.put(new Integer(Message.INFO), marcaInfo);
		marcasTipoMsg.put(new Integer(Message.WARNING), marcaWarning);
		marcasTipoMsg.put(new Integer(Message.ERROR), marcaError);

		String content = "";
		for (int i = 0; i < contenido.length; i++) {
			String marca = (String) marcasTipoMsg.get(new Integer(contenido[i].tipo));
			content = content + "<"+marca+">"+contenido[i].texto + "</"+marca+"><br/";
		}

		all.append(content);

		control.setContentType("text/html");
		control.setText(content);
	}*/


	/**
	 * Obtiene un string con la traza de una excepci�n a la consola
	 *
	 * @param t Elemento throwable
	 */
	private static String dumpStackTrace(Throwable t) {
		if ( t == null ) {
			return "";
		}
		StackTraceElement[] stes = t.getStackTrace();
		String todo = "<"+marcaWarning+">" + t.getClass().getName() + ": " + t.getLocalizedMessage() +
			"<"+marcaWarning+">" + "<br/";

		for (int i = 0; i < stes.length; i++) {
			todo += ("<"+marcaWarning+">&nbsp;&nbsp;&nbsp;&nbsp;" + stes[i].toString() + "<br/");
		}

		if (t.getCause() != null) {
			todo = todo + dumpStackTrace(t.getCause());
		}

		return todo;
	}

    private void recortarbuffers() {
        if( error.length()>60*1024 ) {
            error.delete(0, 30*1024);
        }
        if( warn.length()>60*1024 ) {
            warn.delete(0, 30*1024);
        }
        if( info.length()>60*1024 ) {
            info.delete(0, 30*1024);
        }
        if( all.length()>60*1024 ) {
            all.delete(0, 30*1024);
        }
    }
    /**
     * @see com.iver.mdiApp.NotificationListener#errorEvent(java.lang.String)
     */
    public void errorEvent(MessageEvent e) {
    	for (int i = 0; i < e.getMessages().length; i++){
    		String traza = dumpStackTrace(e.getExceptions()[i]);

			error.append("<"+marcaError+">" + e.getMessages()[i] + "</"+marcaError+"><br/" + traza);
			all.append("<"+marcaError+">" + e.getMessages()[i] + "</"+marcaError+"><br/" + traza);
    	}
        recortarbuffers();
        textoErrores.setText(error.toString());
        textoAll.setText(all.toString());
        tabs.setSelectedComponent(panelErrores);
    }

    /**
     * @see com.iver.mdiApp.NotificationListener#warningEvent(java.lang.String)
     */
    public void warningEvent(MessageEvent e) {
		for (int i = 0; i < e.getMessages().length; i++){
			String traza = dumpStackTrace(e.getExceptions()[i]);

			warn.append("<"+marcaWarning+">" + e.getMessages()[i] + "</"+marcaWarning+"><br/" + traza);
			all.append("<"+marcaWarning+">" + e.getMessages()[i] + "</"+marcaWarning+"><br/" + traza);
		}
        recortarbuffers();
        textoWarnings.setText(warn.toString());
        textoAll.setText(all.toString());
    }

    /**
     * @see com.iver.mdiApp.NotificationListener#infoEvent(java.lang.String)
     */
    public void infoEvent(MessageEvent e) {
		for (int i = 0; i < e.getMessages().length; i++){
			String traza = dumpStackTrace(e.getExceptions()[i]);

			info.append("<"+marcaInfo+">" + e.getMessages()[i] + "</"+marcaInfo+"><br/" + traza);
			all.append("<"+marcaInfo+">" + e.getMessages()[i] + "</"+marcaInfo+"><br/" + traza);
		}
                recortarbuffers();
		textoInfos.setText(info.toString() );
		textoAll.setText(all.toString());

    }

	/**
	 * @see com.iver.mdiApp.ui.MDIManager.IWindow#getModel()
	 */
	public Object getWindowModel() {
		return "consola";
	}

	/**
	 * @see org.gvsig.andami.ui.mdiManager.IWindow#getWindowInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo info = new WindowInfo(WindowInfo.MODELESSDIALOG|WindowInfo.RESIZABLE|WindowInfo.MAXIMIZABLE|WindowInfo.ICONIFIABLE);
		info.setTitle(PluginServices.getText(this, "titulo_consola"));
		return info;
	}

	public Object getWindowProfile() {
		return WindowInfo.PROPERTIES_PROFILE;
	}
}
