/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.config.generate.Extensions;
import org.gvsig.andami.plugins.config.generate.PluginConfig;
import org.gvsig.andami.preferences.DlgPreferences;
import org.gvsig.andami.preferences.IPreference;
import org.gvsig.andami.preferences.IPreferenceExtension;
import org.gvsig.coreplugin.preferences.general.AppearancePage;
import org.gvsig.coreplugin.preferences.general.BrowserControlPage;
import org.gvsig.coreplugin.preferences.general.ExtensionPage;
import org.gvsig.coreplugin.preferences.general.ExtensionsPage;
import org.gvsig.coreplugin.preferences.general.FolderingPage;
import org.gvsig.coreplugin.preferences.general.GeneralPage;
import org.gvsig.coreplugin.preferences.general.IconThemePage;
import org.gvsig.coreplugin.preferences.general.LanguagePage;
import org.gvsig.coreplugin.preferences.general.NotificationsPage;
import org.gvsig.coreplugin.preferences.general.ScreenSettingsPage;
import org.gvsig.coreplugin.preferences.general.SkinPreferences;
import org.gvsig.coreplugin.preferences.network.FirewallPage;
import org.gvsig.coreplugin.preferences.network.NetworkPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.i18n.I18nManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * <p>
 * Extension that provides support for visual application configuration
 * through a Preferences dialog where the user can specify its own settings for
 * general purpose aspects.
 * </p>
 * <p>
 * Adding new preference pages is made through ExtensionPoints by invoking
 * <b>
 * 	this.extensionPoints.add("AplicationPreferences","YourIPreferencesClassName", yourIPreferencesPage);
 * </b>
 * and then call <b>DlgPreferences.refreshExtensionPoints();</b>
 * </p>
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class PreferencesExtension extends Extension{

	private static Logger logger = LoggerFactory.getLogger(PreferencesExtension.class);
	private ExtensionPointManager extensionPoints =ToolsLocator.getExtensionPointManager();

	private boolean initilizedExtensions=false;

	public void initialize() {

		registerIcons();
		initializeCoreExtensions();
	}

	private void registerIcons(){
		IconThemeHelper.registerIcon("action", "edit-setup", this);
		IconThemeHelper.registerIcon("action", "application-exit", this);


		// AppearancePage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-appearance", this);
		// BrowserControlPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-browsercontrol", this);
		// DirExtensionPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-dirextension", this);
		// ExtensionPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-extension", this);
		// ExtensionsPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-extensions", this);
		// GeneralPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-general", this);
		// FolderingPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-foldering", this);
		// LanguagePage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-language", this);
		// ScreenSettingsPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-screensetting", this);
		// FirewallPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-firewall", this);
		// NetworkPage.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-network", this);
		// SkinPreferences.java
		IconThemeHelper.registerIcon("preferences", "edit-setup-skin", this);
		IconThemeHelper.registerIcon("preferences", "edit-setup-icontheme", this);
	}

	public void execute(String actionCommand) {
		if (!this.initilizedExtensions) {
			initializeExtensions();
			initializeExtensionsConfig();
			this.initilizedExtensions = true;
		}
		if ("edit-setup".equalsIgnoreCase(actionCommand)) {
			DlgPreferences dlgPreferences = PluginServices.getDlgPreferences();
			dlgPreferences.refreshExtensionPoints();
			PluginServices.getMDIManager().addWindow(dlgPreferences);
		} else if ( "application-exit".equalsIgnoreCase(actionCommand) ) {
            Launcher.closeApplication();
        }
	}
	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		return true;
	}

	private void initializeCoreExtensions() {
		ExtensionPoint ep = this.extensionPoints.add("AplicationPreferences", "");
		ep.append("GeneralPage", "", new GeneralPage());
		ep.append("NetworkPage", "", new NetworkPage());
		ep.append("FirewallPage", "", new FirewallPage());
		ep.append("LanguagePage", "", new LanguagePage());
		ep.append("ExtensionsPage", "", new ExtensionsPage());
		ep.append("AppearancePage", "", new AppearancePage());
		ep.append("FolderingPage", "", new FolderingPage());
		ep.append("ScreenSettingsPage", "", new ScreenSettingsPage());
		ep.append("SkinPreferences", "", new SkinPreferences());
		ep.append("IconThemePage", "", new IconThemePage());

		ep.append("NotificationsPage", "", new NotificationsPage());

//		this.extensionPoints.add("AplicationPreferences","GeneralPage", new GeneralPage());
//		this.extensionPoints.add("AplicationPreferences","NetworkPage", new NetworkPage());
//		this.extensionPoints.add("AplicationPreferences","FirewallPage", new FirewallPage());
//		this.extensionPoints.add("AplicationPreferences","LanguagePage", new LanguagePage());
//		this.extensionPoints.add("AplicationPreferences","ExtensionsPage", new ExtensionsPage());
//		this.extensionPoints.add("AplicationPreferences","AppearancePage", new AppearancePage());
//		this.extensionPoints.add("AplicationPreferences","FolderingPage", new FolderingPage());
//		this.extensionPoints.add("AplicationPreferences","ResolutionPage", new ScreenSettingsPage());
//		this.extensionPoints.add("AplicationPreferences","SkinPreferences", new SkinPreferences());
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("linux") != -1 || os.indexOf("unix")!= -1) {
			ep.append("BrowserControlPage", "", new BrowserControlPage());
//			this.extensionPoints.add("AplicationPreferences","BrowserControlPage", new BrowserControlPage());
		}

		//Falta los plugin
	}

	private void initializeExtensionsConfig() {
		HashMap pc = Launcher.getPluginConfig();
		ArrayList array = new ArrayList();
		Iterator iter = pc.values().iterator();

		while (iter.hasNext()) {
			array.add(((PluginConfig) iter.next()).getExtensions());
		}
		ExtensionPoint ep = this.extensionPoints.add("AplicationPreferences", "");

		Extensions[] exts = (Extensions[]) array.toArray(new Extensions[0]);
		for (int i = 0; i < exts.length; i++) {
			for (int j = 0; j < exts[i].getExtensionCount(); j++) {
				org.gvsig.andami.plugins.config.generate.Extension ext = exts[i]
						.getExtension(j);
				String sExt = ext.getClassName().toString();
				// String pn = null;
				// pn = sExt.substring(0, sExt.lastIndexOf("."));
				// dlgPrefs.addPreferencePage(new PluginsPage(pn));
				// dlgPrefs.addPreferencePage(new ExtensionPage(ext));
				ep.append(sExt, "", new ExtensionPage(ext));
			}
		}
	}
	/**
	 *
	 */
	private void initializeExtensions() {
		Iterator i = Launcher.getExtensionIterator();
		ExtensionPoint ep = this.extensionPoints.add("AplicationPreferences", "");
		I18nManager i18nManager = ToolsLocator.getI18nManager();

		while (i.hasNext()) {
			Object extension = i.next();

			if (extension instanceof IPreferenceExtension) {
				IPreferenceExtension pe = (IPreferenceExtension) extension;
				IPreference[] pp = null;
				try{
				    pp = pe.getPreferencesPages();
				} catch (Throwable th){
				    //Do nothing
				}

				if (pp == null) {
				    String msg = "Preferences page is NULL. Extension "
                        + extension.getClass().getName()
                        + " implements IPreferenceExtension but does not provide pref. pages. ";
				    logger.info(msg);
				    String[] params = {extension.getClass().getName()};
				    NotificationManager.addInfo(i18nManager.getTranslation("extension_does_not_provide_preferences_pages",params));
				} else {
					for (int j = 0; j < pp.length; j++) {
						try {
						    pp[j].initializeValues();
						    ep.append(pp[j].getID(), "", pp[j]);
                        } catch (Throwable th) {
                            String msg = "cant_initialize_values_for_{0}_page_in_{1}";
                            String[] params = {pp[j].getTitle(), extension.getClass().getName()};
                            NotificationManager.addWarning(i18nManager.getTranslation(msg,params),th);
                        }
					}
				}
			}
		}
	}

	public void postInitialize() {
		super.postInitialize();
		DlgPreferences dlgPreferences=PluginServices.getDlgPreferences();
		dlgPreferences.refreshExtensionPoints();
		dlgPreferences.storeValues();
	}
}
