/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.mdiManager;

import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.Vector;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.config.generate.Menu;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;


/**
 *
 */
public class WindowStackSupport {
	/* arrays for dynamically assigned shortcut keys */
	private boolean[] key_free;
	private String[] key;
	
	private Vector vistas = new Vector();

	private WindowInfoSupport vis;

	private Hashtable viewMenu = new Hashtable();

	/**
	 * @param vis
	 */
	public WindowStackSupport(WindowInfoSupport vis) {
this.vis = vis;
		
		/* restart window key shortcut numbering */
		key_free = new boolean[10];
		for ( int i = 0; i < 10; i ++ ) {
			key_free[i] = true;
		}
		key = new java.lang.String[10];
		/*
		 * We need to add CTRL modifier because
		 * our key stroke manager will
		 * not add default modifier (because we need to
		 * deal with F3, F7, etc, not CTRL+F3 etc) 
		 */
		key[0] = "ctrl+1";
		key[1] = "ctrl+2";
		key[2] = "ctrl+3";
		key[3] = "ctrl+4";
		key[4] = "ctrl+5";
		key[5] = "ctrl+6";
		key[6] = "ctrl+7";
		key[7] = "ctrl+8";
		key[8] = "ctrl+9";
		key[9] = "ctrl+0";
	}

	public void add(IWindow v, final ActionListener listener) {
		vistas.add(v);
		WindowInfo vi = vis.getWindowInfo(v);
		int id = vi.getId();
		Menu m = new Menu();
		m.setActionCommand(""+id);
		m.setTooltip(PluginServices.getText(this, "activa_la_ventana"));
		m.setText("Window/"+vi.getTitle());
		/* get the first free mnemonic (if any) and assign */
		for ( int i=0; i < 10; i++) {
			if ( key_free[i]) {
				m.setKey(key[i]);
				key_free[i] = false;
				break;
			}
		}
		viewMenu.put(v, m);
		PluginServices.getMainFrame().addMenu(m, listener, PluginServices.getPluginServices(this).getClassLoader() );
	}

	public void remove(IWindow v){
		Menu m = (Menu) viewMenu.get(v);
		if (m == null) return;
		/* free keyboard shortut taken by this window (if any) */
		if ( m.getKey() != null ) {
			for ( int i=0; i < 10; i++ ) {
				if ( m.getKey() == key[i]) {
					key_free[i] = true;
					break;
				}
			}
		}
		PluginServices.getMainFrame().removeMenu(m);
		viewMenu.remove(v);
		vistas.remove(v);
	}

	/**
	 * FJP: No se usa, y no s� para qu� estaba pensado.
	 */
	public void ctrltab(){
		IWindow v = (IWindow) vistas.remove(vistas.size() - 1);
		vistas.add(0, v);
	}

	public IWindow getActiveWindow(){
		if (vistas.size() == 0) return null;
        int index = vistas.size()-1;
        while (index >= 0)
        {
            IWindow aux = (IWindow) vistas.get(index);
            if (!aux.getWindowInfo().isPalette())
            {
//                System.err.println("getActiveView = " + aux.getWindowInfo().getTitle());
                return aux;
            }
            index--;
        }
        return null;
	}
    /**
     * Se utiliza cuando ya est� abierta la vista para indicar
     * que la pasamos a activa. De esta forma evitamos que el
     * getActiveView devuelva algo que no es.
     * En realidad lo que haces es mover la vista a la �ltima
     * posici�n.
     * @param v
     */
    public void setActive(IWindow v)
    {
        IWindow copia = null;
        boolean bCopiar = false;
        // Si es de tipo palette, no se pone como activa.
        // De esta forma, nunca nos la devolver�.... Bueno,
        // igual si cerramos la de encima. Voy a ponerle en
        // getActiveView que si es de tipo Palette, devuelva la
        // de abajo.
        if (v.getWindowInfo().isPalette()) return;

        for (int i=0; i < vistas.size(); i++)
        {
            IWindow aux = (IWindow) vistas.get(i);
            if (aux == v)
            {
                copia = aux;
                bCopiar = true;
            }
            if (bCopiar)
            {
                if (i < vistas.size()-1)
                {
                    IWindow siguiente = (IWindow) vistas.get(i+1);
                    vistas.set(i,siguiente);
                }
                else // La �ltima
                    vistas.set(i,copia);
            }
        } // for
    }
}
