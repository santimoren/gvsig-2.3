/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.preferences.general;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.preferences.AbstractPreferencePage;


public class ExtensionsPage extends AbstractPreferencePage {

	private JLabel jLabel = null;
	private ImageIcon icon;

	/**
	 * This is the default constructor
	 */
	public ExtensionsPage() {
		super();
		icon = PluginServices.getIconTheme().get("edit-setup-extensions");
		setParentID(GeneralPage.class.getName());
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		jLabel = new JLabel();
		jLabel.setText(PluginServices.getText(this,"configurar_todas_las_extensiones"));
		this.setLayout(new BorderLayout());
		this.setSize(300, 200);
		this.add(jLabel, java.awt.BorderLayout.CENTER);
	}

	public String getID() {
		return this.getClass().getName();
	}

	public String getTitle() {
		return PluginServices.getText(this,"extensiones");
	}

	public JPanel getPanel() {
		return this;
	}



	public void initializeValues() {
	}

	public void storeValues() {
	}

	public void initializeDefaults() {
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public boolean isValueChanged() {
		return false; // Because it does not manage values
	}

	public void setChangesApplied() {
	}
}
