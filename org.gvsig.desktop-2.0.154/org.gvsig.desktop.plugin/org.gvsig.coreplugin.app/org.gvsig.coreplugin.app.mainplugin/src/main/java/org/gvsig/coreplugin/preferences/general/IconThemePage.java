/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.preferences.general;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.event.ListDataListener;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IconThemePage extends AbstractPreferencePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4369071892027860769L;

	private static Logger logger = LoggerFactory.getLogger(IconThemePage.class);
	
	private String id;
	private boolean changed = false;
	private IconThemePageLayout panel = null;

	public IconThemePage() {
		super();
		initialize();
		id = this.getClass().getName();
		setParentID(GeneralPage.class.getName());
	}

	public String getID() {
		return id;
	}

	public String getTitle() {
		return Messages.getText("_Icon_theme");
	}

	public JPanel getPanel() {
		return this;
	}

	public void initializeValues() {
	}

	public void storeValues() throws StoreException {
		IconTheme iconTheme = (IconTheme) panel.combo_selection.getSelectedItem();
		if(iconTheme != null && iconTheme != iconTheme.getDefault() ) {
			Preferences prefs = Preferences.userRoot().node("gvsig.icontheme");
			prefs.put("default-theme", iconTheme.getID());
		}
	}

	public void initializeDefaults() {

	}

	private ImageIcon icon = null;
	public ImageIcon getIcon() {
		if (icon == null) {
			icon = IconThemeHelper.getImageIcon("edit-setup-icontheme");
		}
		return icon;
	}

	public boolean isValueChanged() {
		if( panel.combo_selection.getSelectedIndex()>=0 ) {
			return true;
		}
		return changed;
	}

	public void setChangesApplied() {
		changed = false;
	}

	private void initialize() {
		final IconThemeManager iconManager = ToolsSwingLocator.getIconThemeManager();
		this.setLayout(new BorderLayout());
		
		this.panel = new IconThemePageLayout();

		panel.combo_selection.setModel(new ComboBoxModel() {
			IconTheme selected = null;
			public void removeListDataListener(ListDataListener arg0) {
				// Do nothing
			}
			public int getSize() {
				return iconManager.getCount();
			}
			public Object getElementAt(int arg0) {
				return iconManager.get(arg0);
			}
			public void addListDataListener(ListDataListener arg0) {
				// Do nothing
			}
			public void setSelectedItem(Object arg0) {
				this.selected = (IconTheme) arg0;
			}
			public Object getSelectedItem() {
				return this.selected;
			}
		});
		
		panel.button_export.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				createDefaultIconTheme();
			}
		});
		
		// Traducir las etiquetas del panel
		panel.label_title.setText(translate(panel.label_title.getText()));
		panel.label_selection.setText(translate(panel.label_selection.getText()));
		panel.button_export.setText(translate(panel.button_export.getText()));
		
		this.add(panel, BorderLayout.CENTER);
	}
	
	private String translate(String s) {
		return Messages.getText(s);
	}
	
	private void createDefaultIconTheme() {
		PluginsManager pluginsManager = PluginsLocator.getManager();
		IconThemeManager iconManager = ToolsSwingLocator.getIconThemeManager();
		
		File f = new File(pluginsManager.getApplicationHomeFolder(),"icon-theme");
		if( !f.exists() ) {
			f.mkdir();
		}
		IconTheme theme = iconManager.getDefault();
		File f2 = new File(f,theme.getID()) ;
		if( !f2.exists() ) {
			logger.info("Creating default icon theme in "+f.getAbsolutePath());
			theme.export(f);
		}
	}
} 

