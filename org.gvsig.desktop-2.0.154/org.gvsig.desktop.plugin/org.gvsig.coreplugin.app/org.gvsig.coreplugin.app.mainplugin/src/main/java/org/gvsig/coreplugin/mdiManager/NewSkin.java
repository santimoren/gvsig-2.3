/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.mdiManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.KeyEventDispatcher;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.DefaultDesktopManager;
import javax.swing.DesktopManager;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.ToolsWindowManager;
import org.gvsig.andami.ui.mdiFrame.GlassPane;
import org.gvsig.andami.ui.mdiFrame.MDIFrame;
import org.gvsig.andami.ui.mdiFrame.NewStatusBar;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.MDIManager;
import org.gvsig.andami.ui.mdiManager.MDIUtilities;
import org.gvsig.andami.ui.mdiManager.SingletonDialogAlreadyShownException;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.andami.ui.theme.Theme;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;
import org.gvsig.tools.task.RunnableWithParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class NewSkin extends Extension implements MDIManager {

    private static final int DefaultXMargin = 20; // Added for the method 'centreJInternalFrame'
    private static final int DefaultYMargin = 20; // Added for the method 'centreJInternalFrame'
    private static final int MinimumXMargin = 130; // Added for the method 'centreJInternalFrame'
    private static final int MinimumYMargin = 60; // Added for the method 'centreJInternalFrame'

    /**
     * Variable privada <code>desktopManager</code> para usarlo cuando sale una
     * ventana que no queremos que nos restaure las que tenemos maximizadas.
     * Justo antes de usar el setMaximize(false), le pegamos el cambiazo.
     */
    private static DesktopManager desktopManager = new DefaultDesktopManager();

    /**
     * log
     */
    private static Logger logger = LoggerFactory.getLogger(NewSkin.class.getName());

    /**
     * Panel de la MDIFrame
     */
    private JDesktopPane panel = new MDIDesktopPane();

    /**
     * MDIFrame
     */
    private MDIFrame mainFrame;

    private GlassPane glassPane = new GlassPane();

    private DialogStackSupport dss;

    /**
     * Associates JInternalFrames with the IWindow they contain
     */
    private FrameWindowSupport fws;

    private WindowInfoSupport wis;

    private WindowStackSupport wss;

    private SingletonWindowSupport sws;

    private Cursor lastCursor = null;
    private ImageIcon image;
    private String typeDesktop;

    private int alignCounter = 1;

    //Anyade barras de scroll
    private void addScrolledDesktopPanel(JFrame parent, JDesktopPane desktopPane) {
        JPanel toppanel;

        toppanel = new JPanel();
        toppanel.setLayout(new BorderLayout());
        toppanel.setPreferredSize(new Dimension(200, 200));
        toppanel.setBackground(Color.RED);

        JScrollPane scrollPanel = new JScrollPane(desktopPane);

        toppanel.add(scrollPanel, BorderLayout.CENTER);

        parent.getContentPane().add(toppanel, BorderLayout.CENTER);
    }

    public JDesktopPane getDesktopPane() {
        return this.panel;
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#init(com.iver.andami.ui.mdiFrame.MDIFrame)
     */
    public void init(MDIFrame f) {

        // Inicializa el Frame y la consola
        mainFrame = f;
        mainFrame.setGlassPane(glassPane);
        panel.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

        //Anyade barras de scroll
        //mainFrame.getContentPane().add(panel, BorderLayout.CENTER);
        addScrolledDesktopPanel(mainFrame, panel);

        panel.setDesktopManager(desktopManager);

        fws = createFrameWindowSupport(mainFrame);
        dss = new DialogStackSupport(mainFrame);
        sws = new SingletonWindowSupport(wis, fws);
        wis = new WindowInfoSupport(mainFrame, fws, sws);
        fws.setVis(wis);
        wss = new WindowStackSupport(wis);

        // TODO (jaume) esto no deber�a de estar aqu�...
        // molar�a m�s en un di�logo de preferencias
        // es s�lo una prueba
        KeyStroke controlTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.CTRL_MASK);

        PluginServices.registerKeyStroke(controlTab, new KeyEventDispatcher() {

            public boolean dispatchKeyEvent(KeyEvent e) {
                IWindow[] views = getAllWindows();
                if (views.length <= 0 || e.getID() == KeyEvent.KEY_PRESSED) {
                    return false;
                }

                int current = 0;
                for (int i = 0; i < views.length; i++) {
                    if (views[i].equals(getActiveWindow())) {
                        current = i;
                        break;
                    }
                }
                addWindow(views[(current + 1) % views.length]);
                return true;
            }

        });
    }

    /**
     * Create the {@link FrameWindowSupport} instance
     *
     * @param mainFrame
     *
     */
    protected FrameWindowSupport createFrameWindowSupport(MDIFrame mainFrame) {
        return new FrameWindowSupport(mainFrame);
    }

    /* (non-javadoc)
     * @see com.iver.andami.ui.mdiManager.MDIManager#addWindow(com.iver.andami.ui.mdiManager.IWindow)
     */
    public IWindow addWindow(final IWindow p) throws SingletonDialogAlreadyShownException {

        if (!SwingUtilities.isEventDispatchThread()) {
            RunnableWithParameters action = new RunnableWithParameters() {
                public void run() {
                    this.returnValue = addWindow(p);
                }
            };
            try {
                SwingUtilities.invokeAndWait(action);
            } catch (Exception e) {
                logger.info("Can't add window from othrer thread that EventDispatch", e);
                IllegalThreadStateException e2 = new IllegalThreadStateException();
                e2.initCause(e);
                throw e2;
            }
            return (IWindow) action.getReturnValue();
        }

        // se obtiene la informaci�n de la vista
        WindowInfo wi = wis.getWindowInfo(p);

        // Se comprueban las incompatibilidades que pudieran haber en la vista
        MDIUtilities.checkWindowInfo(wi);
        if ((p instanceof SingletonWindow) && (wi.isModal())) {
            throw new RuntimeException("A modal view cannot be a SingletonView");
        }

        /*
         * Se obtiene la referencia a la vista anterior por si es una singleton
         * y est� siendo mostrada. Se obtiene su informaci�n si ya fue mostrada
         */
        boolean singletonPreviouslyAdded = false;

        if (p instanceof SingletonWindow) {
            SingletonWindow sw = (SingletonWindow) p;
            if (sws.registerWindow(sw.getClass(), sw.getWindowModel(), wi)) {
                singletonPreviouslyAdded = true;
            }
        }

        if (singletonPreviouslyAdded) {
            // Si la vista no est� actualmente abierta
            if (!sws.contains((SingletonWindow) p)) {
                JInternalFrame frame = fws.getJInternalFrame(p);
                sws.openSingletonWindow((SingletonWindow) p, frame);
                addJInternalFrame(frame, wi);
                wss.add(p, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        IWindow v = wis.getWindowById(Integer.parseInt(e
                                .getActionCommand()));
                        JInternalFrame f = fws.getJInternalFrame(v);
                        activateJInternalFrame(f);
                    }
                });
                return p;
            } else {
                // La vista est� actualmente abierta
                JInternalFrame frame = (JInternalFrame) sws
                        .getFrame((SingletonWindow) p);
                activateJInternalFrame(frame);
                wss.setActive(p);
                return fws.getWindow((JInternalFrame) frame);
            }
        } else {
            if (wi.isModal()) {
                addJDialog(p);
            } else {
                // Se sit�a la vista en la pila de vistas
                wss.add(p, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        IWindow v = wis.getWindowById(Integer.parseInt(e
                                .getActionCommand()));
                        JInternalFrame f = fws.getJInternalFrame(v);
                        activateJInternalFrame(f);
                    }
                });
                addJInternalFrame(p);
            }

            return p;
        }
    }

    public SingletonWindow getSingletonWindow(Class windowClass, Object model) {
        JInternalFrame frame = (JInternalFrame) sws.getFrame(windowClass, model);
        if (frame == null) {
            return null;
        }
        return (SingletonWindow) fws.getWindow((JInternalFrame) frame);
    }

    /* (non-javadoc)
     * @see com.iver.andami.ui.mdiManager.MDIManager#addWindow(com.iver.andami.ui.mdiManager.IWindow)
     */
    public IWindow addCentredWindow(IWindow p) throws SingletonDialogAlreadyShownException {
        IWindow window = addWindow(p);
        if (!p.getWindowInfo().isModal()) {
            centreFrame(window);
        }
        return window;
    }

    public IWindow addWindow(IWindow p, int align) throws SingletonDialogAlreadyShownException {
        boolean singletonPreviouslyAdded = false;
        WindowInfo wi = wis.getWindowInfo(p);

        if (p instanceof SingletonWindow) {
            SingletonWindow sw = (SingletonWindow) p;
            if (sws.registerWindow(sw.getClass(), sw.getWindowModel(), wi)) {
                singletonPreviouslyAdded = true;
            }
        }

        if (!singletonPreviouslyAdded) {
            Point new_loc = this.getLocationForAlignment(p, align);
            p.getWindowInfo().setX(new_loc.x);
            p.getWindowInfo().setY(new_loc.y);
        }

        IWindow window = addWindow(p);
        return window;
    }

    @Override
    public void move(final IWindow panel, final int x, final int y) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    move(panel, x, y);
                }
            });
            return;
        }
        Component window = fws.getFrame(panel);
        if (window == null) {
            return;
        }
        Point p = new Point();
        p.setLocation(x, y);
        window.setLocation(p);
    }
    
    /**
     * Centres the Frame in the contentPane of the MainFrame. If the frame can't
     * be showed completely, it tries to show its top-left corner.
     *
     * @author Pablo Piqueras Bartolom�
     *
     * @param panel The IWindow to centre
     */
    public synchronized void centreFrame(final IWindow panel) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    centreFrame(panel);
                }
            });
            return;
        }
        Component window = fws.getFrame(panel);
        if (window == null) {
            return;
        }

        // The top-left square of frame reference
        Point newReferencePoint = new Point();

        // A reference to the panel where the JInternalFrame will be displayed
        Container contentPane = ((JFrame) PluginServices.getMainFrame()).getContentPane();

        // Get the NewStatusBar component
        NewStatusBar newStatusBar = ((NewStatusBar) contentPane.getComponent(1));
        JDesktopPane jDesktopPane = this.getDesktopPane();

        int visibleWidth = contentPane.getWidth() - contentPane.getX(); // The last substraction is for if there is any menu,... at left
        int visibleHeight = contentPane.getHeight() - newStatusBar.getHeight() - contentPane.getY() - Math.abs(jDesktopPane.getY() - contentPane.getY()); // The last substraction is for if there is any menu,... at top
        int freeWidth = visibleWidth - window.getWidth();
        int freeHeight = visibleHeight - window.getHeight();

        // Calculate the new point reference (Assure that the top-left corner is showed)
        if (freeWidth < 0) {
            if (visibleWidth > MinimumXMargin) {
                newReferencePoint.x = DefaultXMargin;
            } else {
                newReferencePoint.x = 0;
            }
        } else {
            newReferencePoint.x = freeWidth / 2;
        }

        if (freeHeight < 0) {
            if (visibleHeight > MinimumYMargin) {
                newReferencePoint.y = DefaultYMargin;
            } else {
                newReferencePoint.y = 0;
            }
        } else {
            newReferencePoint.y = freeHeight / 2;
        }

        // Set the new location for this JInternalFrame
        window.setLocation(newReferencePoint);
    }

    public synchronized void alignFrame(final IWindow panel, final int mode) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    alignFrame(panel, mode);
                }
            });
            return;
        }
        Component window = fws.getFrame(panel);
        if (window == null) {
            return;
        }

        // The top-left square of frame reference
        Point newReferencePoint = new Point();

        // A reference to the panel where the JInternalFrame will be displayed
        Container contentPane = ((JFrame) PluginServices.getMainFrame()).getContentPane();

        // Get the NewStatusBar component
        NewStatusBar newStatusBar = ((NewStatusBar) contentPane.getComponent(1));
        JDesktopPane jDesktopPane = this.getDesktopPane();

        int visibleWidth = contentPane.getWidth() - contentPane.getX(); // The last substraction is for if there is any menu,... at left
        int visibleHeight = contentPane.getHeight() - newStatusBar.getHeight() - contentPane.getY() - Math.abs(jDesktopPane.getY() - contentPane.getY()); // The last substraction is for if there is any menu,... at top

//        ---------------------------------------------------------------
//        |FIRST_LINE_START(23)   PAGE_START(19)     FIRST_LINE_END(24) |
//        |                                                             |
//        |                                                             |
//        |LINE_START(21)           CENTER(10)              LINE_END(22)|
//        |                                                             |
//        |                                                             |
//        |LAST_LINE_START(25)     PAGE_END(20)       LAST_LINE_END(26) |
//        ---------------------------------------------------------------
        switch (mode) {
            case ALIGN_FIRST_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_PAGE_START:
                newReferencePoint.x = visibleWidth / 2;
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_FIRST_LINE_END:
                newReferencePoint.x = visibleWidth - window.getWidth() - DefaultXMargin;
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_FIRST_LINE_END_CASCADE:
                newReferencePoint.x = visibleWidth - window.getWidth() - (int) (DefaultXMargin + (DefaultXMargin * 1.5 * this.alignCounter));
                newReferencePoint.y = DefaultYMargin + (int) (DefaultYMargin * 1.5 * this.alignCounter);
                if (++this.alignCounter > 5) {
                    this.alignCounter = 0;
                }
                break;

            case ALIGN_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = visibleHeight / 2;
                break;

            case ALIGN_LINE_END:
                newReferencePoint.x = visibleWidth - window.getWidth() - DefaultXMargin;
                newReferencePoint.y = visibleHeight / 2;
                break;

            case ALIGN_LAST_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = visibleHeight - window.getHeight() - DefaultYMargin;
                break;

            case ALIGN_PAGE_END:
                newReferencePoint.x = visibleWidth / 2;
                newReferencePoint.y = visibleHeight - window.getHeight() - DefaultYMargin;
                break;

            case ALIGN_LAST_LINE_END:
                newReferencePoint.x = visibleWidth - window.getWidth() - DefaultXMargin;
                newReferencePoint.y = visibleHeight - window.getHeight() - DefaultYMargin;
                break;

            default:
            case ALIGN_CENTER:
                newReferencePoint.x = visibleWidth / 2;
                newReferencePoint.y = visibleHeight / 2;
                break;
        }

        if (newReferencePoint.x < 0) {
            newReferencePoint.x = DefaultXMargin;
        }
        if (newReferencePoint.y < 0) {
            newReferencePoint.y = DefaultYMargin;
        }
        window.setLocation(newReferencePoint);
    }

    /**
     * DOCUMENT ME!
     *
     * @param wnd DOCUMENT ME!
     * @param wi DOCUMENT ME!
     */
    private void addJInternalFrame(JInternalFrame wnd, WindowInfo wi) {
        wnd.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        wnd.addInternalFrameListener(new FrameListener());

        if (wi.isModeless() || wi.isPalette()) {
            panel.add(wnd, JDesktopPane.PALETTE_LAYER);
            if (wi.isPalette()) {
                wnd.setFocusable(false);
            }
        } else {
            panel.add(wnd);
        }
        updateFrameProperties(wnd, wi);
        activateJInternalFrame(wnd);
        try {
            wnd.setMaximum(wi.isMaximized());
        } catch (Exception ex) {
            String title = "unknow";
            try {
                title = wi.getTitle();
            } catch (Throwable ex2) {
                // Ignore error.
            }
            logger.warn("Can't maximize window (" + title + ").", ex);
        }
    }

    private void updateFrameProperties(JInternalFrame frame, WindowInfo wi) {
        int height, width;
        if (wi.isMaximized()) {
            if (wi.getNormalWidth() != -1) {
                width = wi.getNormalWidth();
            } else {
                width = frame.getNormalBounds().width;
            }
            if (wi.getNormalHeight() != -1) {
                height = wi.getNormalHeight();
            } else {
                height = frame.getNormalBounds().height;
            }

            frame.setSize(width, height);
            frame.setLocation(wi.getNormalX(), wi.getNormalY());
        } else {
            if (wi.getWidth() != -1) {
                width = wi.getWidth();
            } else {
                width = frame.getWidth();
            }
            if (wi.getHeight() != -1) {
                height = wi.getHeight();
            } else {
                height = frame.getHeight();
            }
            frame.setSize(width, height);
            frame.setLocation(wi.getX(), wi.getY());
        }
        frame.setTitle(wi.getTitle());
        frame.setVisible(wi.isVisible());
        frame.setResizable(wi.isResizable());
        frame.setIconifiable(wi.isIconifiable());
        frame.setMaximizable(wi.isMaximizable());
        try {
            frame.setMaximum(wi.isMaximized());
        } catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
            //e.printStackTrace();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param p
     */
    private void addJInternalFrame(IWindow p) {
        WindowInfo wi = wis.getWindowInfo(p);

        JInternalFrame wnd = fws.getJInternalFrame(p);

        if (p instanceof SingletonWindow) {
            SingletonWindow sv = (SingletonWindow) p;
            sws.openSingletonWindow(sv, wnd);
        }

        addJInternalFrame(wnd, wi);
    }

    /**
     * DOCUMENT ME!
     *
     * @param wnd
     */
    private void activateJInternalFrame(JInternalFrame wnd) {
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        try {
            wnd.moveToFront();
            logger.debug("Activando " + wnd.getTitle());
            wnd.setSelected(true);
            wnd.setIcon(false);
        } catch (PropertyVetoException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public void moveToFrom(IWindow win) {
        Component frame = fws.getFrame(win);
        if( frame instanceof JInternalFrame ) {
            activateJInternalFrame((JInternalFrame)frame);
        }
    }

    /**
     * Situa un di�logo modal en el centro de la pantalla
     *
     * @param d Di�logo que se quiere situar
     */
    private void centerDialog(JDialog d) {
        int offSetX = d.getWidth() / 2;
        int offSetY = d.getHeight() / 2;
        Point o = mainFrame.getLocation();
        int x = o.x + (mainFrame.getWidth() / 2) - offSetX;
        int y = o.y + (mainFrame.getHeight() / 2) - offSetY;
        if( x<0 ) {
            x = 0;
        }
        if( y<0 ) {
            y=0;
        }
        d.setLocation(x,y);
    }

    /**
     * DOCUMENT ME!
     *
     * @param p
     */
    private void addJDialog(final IWindow p) {
        JDialog dlg = fws.getJDialog(p);
        dlg.addWindowListener(new DialogWindowListener());
        centerDialog(dlg);
        dss.pushDialog(dlg);

        dlg.setVisible(wis.getWindowInfo(p).isVisible());
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#getActiveWindow()
     */
    public IWindow getActiveWindow() {
        JInternalFrame jif = panel.getSelectedFrame();

        if (jif != null) {
            IWindow theWindow = fws.getWindow(jif);
            if (theWindow == null) {
                return null;
            }
            if (theWindow.getWindowInfo().isPalette()) {
                return wss.getActiveWindow();
            } else {
                return fws.getWindow(jif);
            }
        }
        // return vss.getActiveView();

        return null;
    }

    public IWindow getFocusWindow() {
        JInternalFrame jif = panel.getSelectedFrame();

        if (jif != null) {
            IWindow theView = fws.getWindow(jif);
            if (theView == null) {
                return null;
            }
            return fws.getWindow(jif);
        }
        return null;
    }
    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#closeWindow(com.iver.andami.ui.mdiManager.IWindow)
     */

    public void closeWindow(final IWindow p) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    closeWindow(p);
                }
            });
            return;
        }
        // Si es un di�logo modal
        if (p.getWindowInfo().isModal()) {
            closeJDialog();
        } else { // Si no es modal se cierra el JInternalFrame
            closeJInternalFrame(fws.getJInternalFrame(p));
        }
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#closeAllWindows()
     */
    public void closeAllWindows() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    closeAllWindows();
                }
            });
            return;
        }
        ArrayList eliminar = new ArrayList();
        Iterator i = fws.getWindowIterator();

        while (i.hasNext()) {
            eliminar.add((IWindow) i.next());
        }

        for (Iterator iter = eliminar.iterator(); iter.hasNext();) {
            IWindow vista = (IWindow) iter.next();
            closeWindow(vista);
        }
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#getWindowInfo(com.iver.andami.ui.mdiManager.IWindow)
     */
    public WindowInfo getWindowInfo(IWindow w) {
        WindowInfo wi = wis.getWindowInfo(w);

        /*
         * This is done now in vis.getWindowInfo(w)
         *
         * JInternalFrame f = fws.getJInternalFrame(w);
         wi.setX(f.getX());
         wi.setY(f.getY());
         wi.setHeight(f.getHeight());
         wi.setWidth(f.getWidth());
         // isClosed() doesn't work as (I) expected, why? Using isShowing instead
         wi.setClosed(!f.isShowing());
         wi.setNormalBounds(f.getNormalBounds());
         wi.setMaximized(f.isMaximum());*/
        return wi;
    }

    /**
     * DOCUMENT ME!
     *
     * @param dialog
     * @throws RuntimeException DOCUMENT ME!
     */
    private void closeJDialog() {
        JDialog dlg = dss.popDialog();
        if (dlg == null) {
            return;
        }
        dlg.setVisible(false);

        IWindow s = fws.getWindow(dlg);

        callWindowClosed(s);

        fws.closeWindow(s);

        // Si es singleton se desasocia el modelo con la vista
        if (s instanceof SingletonWindow) {
            sws.closeWindow((SingletonWindow) s);
        }
    }

    /**
     * If <code>window</code> implements IWindowListener, sent it the
     * windowActivated event.
     *
     * @param window The IWindow which has to be notified.
     */
    private void callWindowClosed(IWindow window) {
        if (window instanceof IWindowListener) {
            ((IWindowListener) window).windowClosed();
        }
    }

    /**
     * If <code>window</code> implements IWindowListener, sent it the
     * windowActivated event.
     *
     * @param window The IWindow which has to be notified.
     */
    private void callWindowActivated(IWindow window) {
//        logger.debug("View '" + window.getWindowInfo().getTitle()
//               + "' activated (callViewActivated)");
        if (window instanceof IWindowListener) {
            ((IWindowListener) window).windowActivated();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param frame
     */
    private void closeJInternalFrame(final JInternalFrame frame) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    closeJInternalFrame(frame);
                }
            });
            return;
        }
        try {
            IWindow s = (IWindow) fws.getWindow(frame);

            frame.setClosed(true);
            callWindowClosed(s);
        } catch (PropertyVetoException e) {
            logger
                    .error(
                            "Not compatible with property veto's. Use ViewInfo instead.",
                            e);
        }
    }

    /*
     * @see com.iver.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    }

    /*
     * @see com.iver.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
        if ("window-cascade".equalsIgnoreCase(actionCommand)) {
            logger.info("action window-cascade not implemented.");

        } else if ("window-tile".equalsIgnoreCase(actionCommand)) {
            logger.info("action window-tile not implemented.");
        }
    }

    /*
     * @see com.iver.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * @see com.iver.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        // TODO Auto-generated method stub
        return true;
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#setWaitCursor()
     */
    public void setWaitCursor() {
        if (mainFrame != null) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        setWaitCursor();
                    }
                });
                return;
            }
            glassPane.setVisible(true);
            lastCursor = mainFrame.getCursor();
            dss.setWaitCursor();
            glassPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#restoreCursor()
     */
    public void restoreCursor() {
        if (mainFrame != null) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        restoreCursor();
                    }
                });
                return;
            }
            glassPane.setVisible(false);
            dss.restoreCursor();
            glassPane.setCursor(lastCursor);
        }
    }

    /**
     * Listener para los eventos de cerrado de los di�logos. Tiene su raz�n
     * de ser en que los di�logos han de devolverse al pool cuando se cierran
     *
     * @author Fernando Gonz�lez Cort�s
     */
    public class DialogWindowListener extends WindowAdapter {

        /**
         * Captura el evento de cerrado de los di�logos con el fin de realizar
         * tareas de mantenimiento
         *
         * @param e evento
         */
        public void windowActivated(WindowEvent e) {
            IWindow window = fws.getWindow((Component) e.getSource());
            callWindowActivated(window);

        }

        /**
         * Captura el evento de cerrado de los di�logos con el fin de realizar
         * tareas de mantenimiento
         *
         * @param e evento
         */
        public void windowClosing(WindowEvent e) {
            closeJDialog();
        }
    }

    /**
     * DOCUMENT ME!
     */
    public class FrameListener implements InternalFrameListener {
        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameActivated(javax.swing.event.InternalFrameEvent)
         */

        public void internalFrameActivated(InternalFrameEvent e) {
            // logger.debug("internalFrameActivated " +
            // e.getInternalFrame().getTitle());

            // activatedInternalFramesStack.push(e.getInternalFrame());
            IWindow panel = fws.getWindow((JInternalFrame) e.getSource());

            WindowInfo wi = wis.getWindowInfo(panel);
            if (wi.isPalette()) {
                return;
            }

            wss.setActive(panel);

            JInternalFrame frame = fws.getJInternalFrame(panel);

            if (wi.isMaximizable()) {
                frame.setMaximizable(true);
            }
            if (!frame.isMaximizable() && frame.isMaximum()) {
                try {
                    frame.setMaximum(false);
                } catch (PropertyVetoException e1) {
                }
            }
            mainFrame.enableControls();
            if (wi.getSelectedTools() == null) {
                // this is the first time this window is activated
                wi.setSelectedTools(new HashMap(mainFrame.getInitialSelectedTools()));
            }
            mainFrame.setSelectedTools(wi.getSelectedTools());
            callWindowActivated(panel);

        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameClosed(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameClosed(InternalFrameEvent e) {
        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameClosing(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameClosing(InternalFrameEvent e) {
            // Se elimina la memoria del JInternalFrame si no es ALWAYS_LIVE
            // logger.debug("internalFrameClosing " +
            // e.getInternalFrame().getTitle());

            JInternalFrame c = (JInternalFrame) e.getSource();
            try {
                ToolsWindowManager.Window wwin = (ToolsWindowManager.Window) c.getContentPane().getComponent(0);
                wwin.fireClosingWindow();
            } catch (Throwable ex) {

            }

            WindowInfo wi = wis.getWindowInfo((IWindow) fws.getWindow(c));

            IWindow win = fws.getWindow(c);
            callWindowClosed(win);
//            boolean alwaysLive;
            if (win instanceof SingletonWindow) {
                sws.closeWindow((SingletonWindow) win);
            }

            fws.closeWindow(win);

            panel.remove(c);

            wss.remove(win);

            if (!wi.isPalette()) {
                mainFrame.enableControls();
            }
            panel.repaint();

            // Para activar el JInternalFrame desde la que hemos
            // abierto la ventana que estamos cerrando
            IWindow lastWindow = wss.getActiveWindow();
            // La activamos
            if (lastWindow != null) {
                logger.debug(PluginServices.getText(this, "Devuelvo_el_foco_a_") + lastWindow.getWindowInfo().getTitle());
                JInternalFrame frame = fws.getJInternalFrame(lastWindow);
                try {
                    frame.setSelected(true);
                } catch (PropertyVetoException e1) {
                    // TODO Auto-generated catch block
                    // e1.printStackTrace();
                }
                // addView(lastView);
            }

        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameDeactivated(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameDeactivated(InternalFrameEvent e) {
            // logger.debug("internalDeActivated " +
            // e.getInternalFrame().getTitle());
            JInternalFrame c = (JInternalFrame) e.getSource();
            IWindow win = fws.getWindow(c);
            if (win != null) {
                WindowInfo wi = wis.getWindowInfo(win);
                if (wi.isPalette()) {
                    return;
                }

            }

        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameDeiconified(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameDeiconified(InternalFrameEvent e) {
            mainFrame.enableControls();
        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameIconified(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameIconified(InternalFrameEvent e) {
            mainFrame.enableControls();
        }

        /*
         * @see javax.swing.event.InternalFrameListener#internalFrameOpened(javax.swing.event.InternalFrameEvent)
         */
        public void internalFrameOpened(InternalFrameEvent e) {
            // logger.debug("internalFrameOpened. Source= " +
            // e.getSource().toString());
        }
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#closeSingletonWindow(java.lang.Class,
     *      java.lang.Object)
     */
    public boolean closeSingletonWindow(Class viewClass, Object model) {
        JInternalFrame frame = (JInternalFrame) sws.getFrame(viewClass, model);
        if (frame == null) {
            return false;
        }
        closeJInternalFrame(frame);
        return true;
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#closeSingletonWindow(java.lang.Object)
     */
    public boolean closeSingletonWindow(Object model) {
        JInternalFrame[] frames = (JInternalFrame[]) sws.getFrames(model);
        if (frames.length == 0) {
            return false;
        }
        for (int i = 0; i < frames.length; i++) {
            closeJInternalFrame(frames[i]);
        }
        return true;
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#getAllWindows()
     */
    public IWindow[] getAllWindows() {
        if (fws == null) {
            return null;
        }
        ArrayList windows = new ArrayList();
        Iterator i = fws.getWindowIterator();

        while (i.hasNext()) {
            windows.add((IWindow) i.next());
        }
        return (IWindow[]) windows.toArray(new IWindow[0]);
    }

    /*
     * @see com.iver.andami.ui.mdiManager.MDIManager#getOrderedWindows()
     */
    public IWindow[] getOrderedWindows() {
        TreeMap windows = new TreeMap();
        Iterator winIterator = fws.getWindowIterator();

        Component frame;
        IWindow win;
        /**
         * The order of the window in the JDesktopPane. Smaller numbers are
         * closer to the foreground.
         */
        int zPosition;
        while (winIterator.hasNext()) {
            win = (IWindow) winIterator.next();
            frame = fws.getFrame(win);
            zPosition = panel.getPosition(frame);

            if (zPosition == -1) {
                /*
                 * This is a minimized window.
                 * It will keep the -1 (first position) if it does not have the focus.
                 * (I think this never happens. Even if visually the minimized window
                 * appears selected, it does not have the focus), so minimized
                 * windows will lose the first position.
                 */
                if (!frame.isFocusOwner()) {
                    zPosition = 1000;
                }
            }
            int layer = panel.getLayer(frame);

            if (!(frame instanceof JDialog)) { //JDialogs are not in inside the LayeredPane
                // flatten all the layers
                if (layer == JLayeredPane.DEFAULT_LAYER.intValue()) {
                    zPosition += 50000;
                } else if (layer == JLayeredPane.PALETTE_LAYER.intValue()) {
                    zPosition += 40000;
                } else if (layer == JLayeredPane.MODAL_LAYER.intValue()) {
                    zPosition += 30000;
                } else if (layer == JLayeredPane.POPUP_LAYER.intValue()) {
                    zPosition += 20000;
                } else if (layer == JLayeredPane.DRAG_LAYER.intValue()) {
                    zPosition += 10000;
                }
            }
            windows.put(new Integer(zPosition), win);
        }
        winIterator = windows.values().iterator();
        ArrayList winList = new ArrayList();
        while (winIterator.hasNext()) {
            winList.add(winIterator.next());
        }

        return (IWindow[]) winList.toArray(new IWindow[0]);
    }

    public void setMaximum(final IWindow v, final boolean bMaximum) throws PropertyVetoException {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                        setMaximum(v, bMaximum);
                    } catch (PropertyVetoException e) {
                        logger.info("Error not in event dispatch thread", e);
                    }
                }
            });
            return;
        }
        JInternalFrame f = fws.getJInternalFrame(v);
        f.setMaximum(bMaximum);
    }

    public void changeWindowInfo(IWindow w, WindowInfo wi) {
        JInternalFrame f = fws.getJInternalFrame(w);
        f.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        JDesktopPane pnl = f.getDesktopPane();
        pnl.remove(f);
        int width;
        int height;
        if (wi.getWidth() != -1) {
            width = wi.getWidth();
        } else {
            width = f.getWidth();
        }
        if (wi.getHeight() != -1) {
            height = wi.getHeight();
        } else {
            height = f.getHeight();
        }
        f.setSize(new Dimension(width, height));
        f.setLocation(wi.getX(), wi.getY());
        if (wi.isPalette()) {
            pnl.add(f, JDesktopPane.PALETTE_LAYER);
            f.setFocusable(false);
        } else {
            pnl.add(f, JDesktopPane.DEFAULT_LAYER);
            f.setFocusable(true);
            if (wi.isClosed()) {
                closeWindow(w);
            }
        }

        if (wi.isMaximized()) {
            try {
                f.setMaximum(true);
            } catch (PropertyVetoException e) {
    			// TODO Auto-generated catch block
                //e.printStackTrace();
            }
            f.setNormalBounds(wi.getNormalBounds());
        }
        activateJInternalFrame(f);
    }

    public void refresh(final IWindow win) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    refresh(win);
                }
            });
            return;
        }
        Component frame = fws.getFrame(win);
        if (frame != null) {
            frame.setVisible(true);
        }
    }

    public void setBackgroundImage(ImageIcon image, String typeDesktop) {
        this.image = image;
        this.typeDesktop = typeDesktop;

    }

    public void showWindow(final JPanel panel, final String title, final MODE mode) {
        // This prepare a default JPanel that implements IWindow
        // for the passed panel and call to the addWindow
        // to show it.
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    showWindow(panel, title, mode);
                }
            });
            return;
        }
        ToolsSwingLocator.getWindowManager().showWindow(panel, title, mode);
    }

    public IWindow getWindow(JPanel panel){
        Iterator<IWindow> i = fws.getWindowIterator();

        while (i.hasNext()) {
            IWindow window = (IWindow) i.next();
            if(window instanceof JPanel){
                JPanel p = (JPanel)window;
                if(p.getComponentCount()==1){
                    if(p.getComponent(0) == panel){
                        return window;
                    }
                }
            }
        }
        return null;
    }

    private Point getLocationForAlignment(IWindow panel, int mode) {

        // The top-left square of frame reference
        Point newReferencePoint = new Point();

        // A reference to the panel where the JInternalFrame will be displayed
        Container contentPane = ((JFrame) PluginServices.getMainFrame()).getContentPane();

        // Get the NewStatusBar component
        NewStatusBar newStatusBar = ((NewStatusBar) contentPane.getComponent(1));

        JDesktopPane dpane = this.getDesktopPane();
        // The last substraction is for case when there is any menu,... at left
        int visibleWidth = contentPane.getWidth() - contentPane.getX();
        // The last substraction is for case when there is any menu,... at top
        int visibleHeight = contentPane.getHeight() - newStatusBar.getHeight() - contentPane.getY() - Math.abs(dpane.getY() - contentPane.getY());

//        ---------------------------------------------------------------
//        |FIRST_LINE_START(23)   PAGE_START(19)     FIRST_LINE_END(24) |
//        |                                                             |
//        |                                                             |
//        |LINE_START(21)           CENTER(10)              LINE_END(22)|
//        |                                                             |
//        |                                                             |
//        |LAST_LINE_START(25)     PAGE_END(20)       LAST_LINE_END(26) |
//        ---------------------------------------------------------------
        int win_h = panel.getWindowInfo().getHeight();
        int win_w = panel.getWindowInfo().getWidth();

        switch (mode) {
            case ALIGN_FIRST_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_PAGE_START:
                newReferencePoint.x = (visibleWidth / 2) - (win_w / 2);
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_FIRST_LINE_END:
                newReferencePoint.x = visibleWidth - win_w - DefaultXMargin;
                newReferencePoint.y = DefaultYMargin;
                break;

            case ALIGN_FIRST_LINE_END_CASCADE:
                newReferencePoint.x = visibleWidth - win_w - (int) (DefaultXMargin + (DefaultXMargin * 1.5 * this.alignCounter));
                newReferencePoint.y = DefaultYMargin + (int) (DefaultYMargin * 1.5 * this.alignCounter);
                if (++this.alignCounter > 5) {
                    this.alignCounter = 0;
                }
                break;

            case ALIGN_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = (visibleHeight / 2) - (win_h / 2);
                break;

            case ALIGN_LINE_END:
                newReferencePoint.x = visibleWidth - win_w - DefaultXMargin;
                newReferencePoint.y = (visibleHeight / 2) - (win_h / 2);
                break;

            case ALIGN_LAST_LINE_START:
                newReferencePoint.x = DefaultXMargin;
                newReferencePoint.y = visibleHeight - win_h - DefaultYMargin;
                break;

            case ALIGN_PAGE_END:
                newReferencePoint.x = (visibleWidth / 2) - (win_w / 2);
                newReferencePoint.y = visibleHeight - win_h - DefaultYMargin;
                break;

            case ALIGN_LAST_LINE_END:
                newReferencePoint.x = visibleWidth - win_w - DefaultXMargin;
                newReferencePoint.y = visibleHeight - win_h - DefaultYMargin;
                break;

            default:
            case ALIGN_CENTER:
                newReferencePoint.x = (visibleWidth / 2) - (win_w / 2);
                newReferencePoint.y = (visibleHeight / 2) - (win_h / 2);
                break;
        }

        if (newReferencePoint.x < 0) {
            newReferencePoint.x = DefaultXMargin;
        }
        if (newReferencePoint.y < 0) {
            newReferencePoint.y = DefaultYMargin;
        }

        return newReferencePoint;
    }

    public void setLocale(Locale locale) {
        IWindow[] win = this.getAllWindows();
        if (win == null) {
            return;
        }
        for (int i = 0; i < win.length; i++) {
            if (win[i] instanceof Component) {
                try {
                    ((Component) win[i]).setLocale(locale);
                } catch (Exception ex) {
                    // Ignore errors and try others windows.
                }
            }
        }
    }

}
