/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/**
 * 
 */
package org.gvsig.coreplugin.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.plugins.config.generate.Menu;
import org.gvsig.andami.ui.mdiFrame.SelectableToolBar;
import org.gvsig.utils.XMLEntity;


/**
 * @author cesar
 *
 */
public class ToolbarMenus extends Extension implements ActionListener {
	private final String ACTIONCOMMANDBASE = "CHANGE_VISIBILITY-";
	private final String MENUBASE = "Ver/Toolbars/";
	private final String ENABLEDIMAGE = "images/enabled.png";

	/* (non-Javadoc)
	 * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
	 */
	public void execute(String actionCommand) {
		// TODO Auto-generated method stub

	}


	/* (non-Javadoc)
	 * @see com.iver.andami.plugins.Extension#isEnabled()
	 */
	public boolean isEnabled() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.iver.andami.plugins.Extension#isVisible()
	 */
	public boolean isVisible() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.Extension#actionPerformed()
	 */
	public void actionPerformed(ActionEvent e) {
		String toolbarName = e.getActionCommand().substring(ACTIONCOMMANDBASE.length());
		javax.swing.JMenuItem menu = PluginServices.getMainFrame().getMenuEntry((MENUBASE+toolbarName).split("/"));
		
		if (!toolbarName.equals("")) {

			boolean oldVisibility = PluginServices.getMainFrame().getToolbarVisibility(toolbarName);
			if (oldVisibility==false) {
				URL icon = PluginServices.getPluginServices(this).getClassLoader().getResource(ENABLEDIMAGE);				
				menu.setIcon(new ImageIcon(icon));
				persistStatus(toolbarName, !oldVisibility);
			}
			else {
				menu.setIcon(null);
				persistStatus(toolbarName, !oldVisibility);
			}
			PluginServices.getMainFrame().setToolbarVisibility(toolbarName, !oldVisibility);
		}
	}

	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.plugins.Extension#initialize()
	 */
	public void initialize() {
		getPersistedStatus();
		SelectableToolBar[] toolBars = PluginServices.getMainFrame().getToolbars();
		for (int i=toolBars.length-1; i>0; i--) {
			Menu menu = new Menu();
			menu.setActionCommand(ACTIONCOMMANDBASE+toolBars[i].getName());
			//menu.setTooltip(PluginServices.getText(this, "muestra_oculta_la_toolbar"));
			menu.setText(MENUBASE+toolBars[i].getName());
			if (toolBars[i].getAndamiVisibility())
				menu.setIcon(ENABLEDIMAGE);
			PluginServices.getMainFrame().addMenu(menu, this, PluginServices.getPluginServices(this).getClassLoader());
		}
		
	}
	
	/**
	 * Save the status of the provided toolbar.
	 * 
	 * @param toolbarName The toolbar name whose status wants to be saved.
	 * @param visible Whether or not the toolbar is visible.
	 */
	private void persistStatus(String toolbarName, boolean visible) {
		PluginServices ps = PluginServices.getPluginServices(this); 
		XMLEntity xml = ps.getPersistentXML();
		XMLEntity child = null;
		for (int i=xml.getChildrenCount()-1; i>=0; i--) {
			if (xml.getChild(i).getName().equals("Toolbars"))
				child = xml.getChild(i).getChild(0);
				
		}
		if (child==null) {
			XMLEntity toolbars = new XMLEntity();
			toolbars.setName("Toolbars");
			child = new XMLEntity();
			toolbars.addChild(child);
			xml.addChild(toolbars);
		}
		
		if (visible) {
			child.putProperty(toolbarName, "visible");
		}
		else {
			child.putProperty(toolbarName, "hidden");
		}
		ps.setPersistentXML(xml);
		
	}

	/**
	 * Reads the stored toolbars' status from plugin-persinstence.xml,
	 * and sets the toolbars accordingly. 
	 *
	 */
	private void getPersistedStatus() {
		PluginServices ps = PluginServices.getPluginServices(this); 
		XMLEntity xml = ps.getPersistentXML();
		XMLEntity child = null;
		for (int i=xml.getChildrenCount()-1; i>=0; i--) {
			if (xml.getChild(i).getName().equals("Toolbars"))
				child = xml.getChild(i).getChild(0);
				
		}
		if (child!=null) {
			SelectableToolBar[] toolBars = PluginServices.getMainFrame().getToolbars();
			for (int i=toolBars.length-1; i>=0; i--) {
				if (child.contains(toolBars[i].getName()))
					toolBars[i].setAndamiVisibility(child.getStringProperty(toolBars[i].getName()).equals("visible"));
			}
		}
	}
}
