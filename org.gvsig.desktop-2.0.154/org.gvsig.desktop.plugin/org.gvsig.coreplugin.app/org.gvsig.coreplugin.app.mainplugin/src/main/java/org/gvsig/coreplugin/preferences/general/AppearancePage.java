/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.coreplugin.preferences.general;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import org.apache.commons.lang3.BooleanUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.Launcher;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.preferences.AbstractPreferencePage;

/**
 * Appearance page. Where the user can choose Look&Feels and maybe some more stuff.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class AppearancePage extends AbstractPreferencePage{
    private JComboBox lookAndFeelCombo;
    private String id;
    private ImageIcon icon;
    private String lookAndFeel;
    private boolean changed = false;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private PluginServices ps = PluginServices.getPluginServices(this);
	private ActionListener myAction;

	public AppearancePage() {
        super();
        id = this.getClass().getName();
        icon = PluginServices.getIconTheme().get("edit-setup-appearance");
        setParentID(GeneralPage.id);
        // install the plastic look and feel before getting the laf combobox
    	UIManager.installLookAndFeel("Plastic XP", "com.jgoodies.looks.plastic.PlasticXPLookAndFeel");
    	// install the extra LAF's before getting the LAF combobox
    	String osName = System.getProperty("os.name");
		if (osName.toLowerCase().startsWith("mac os x")) {
			UIManager.installLookAndFeel("Quaqua", "ch.randelshofer.quaqua.QuaquaLookAndFeel");
		}
    	addComponent(PluginServices.getText(this, "options.general.select_theme"), getLookAndFeelComboBox());
        myAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lookAndFeel = ((LF) lookAndFeelCombo.getSelectedItem()).getClassName();
                changed = true;

                /* Changing it on the fly can cause rendering issues
                try {
                    UIManager.setLookAndFeel(lookAndFeel);
                    SwingUtilities
                        .updateComponentTreeUI((Component) PluginServices
                            .getMainFrame());
                } catch (Exception ex) {
                    logger.warn(Messages.getString("Launcher.look_and_feel"),
                        ex);
                }
                */
            }
        };

    }

    public String getID() {
        return id;
    }

    public String getTitle() {
        return PluginServices.getText(this, "pref.appearance");
    }

    public JPanel getPanel() {
        return this;
    }

    public void initializeValues() {
    	getLookAndFeelComboBox().removeActionListener(myAction);
    	lookAndFeel = Launcher.getAndamiConfig().getLookAndFeel();
    	if (lookAndFeel == null) {
    		lookAndFeel = Launcher.getDefaultLookAndFeel();
    	}

    	for (int i=0; i<getLookAndFeelComboBox().getModel().getSize(); i++) {
    		LF element = (LF) getLookAndFeelComboBox().getModel().getElementAt(i);
    		if (element.getClassName().equals(lookAndFeel)) {
    			getLookAndFeelComboBox().setSelectedIndex(i);
    			break;
    		}
    	}
    	getLookAndFeelComboBox().addActionListener(myAction);
    }


	public void storeValues() {
		Launcher.getAndamiConfig().setLookAndFeel(lookAndFeel);
    }

    public void initializeDefaults() {
//    	getLookAndFeelComboBox().removeActionListener(myAction);

    	final String defaultLookAndFeel = Launcher.getDefaultLookAndFeel();
        for (int i = 0; i < getLookAndFeelComboBox().getModel().getSize(); i++) {
        	LF lf = (LF) getLookAndFeelComboBox().getModel().getElementAt(i);
        	if (defaultLookAndFeel.equals(lf.getClassName())) {
        		getLookAndFeelComboBox().setSelectedIndex(i);
        		break;
        	}
        }
//        getLookAndFeelComboBox().addActionListener(myAction);
    }

    public ImageIcon getIcon() {
        return icon;
    }

    private JComboBox getLookAndFeelComboBox() {
        if (lookAndFeelCombo==null) {
            boolean enablelaf = BooleanUtils.toBoolean(PluginServices.getArgumentByName("enablelaf"));
            if( enablelaf ) {
                LookAndFeelInfo[] lfs = UIManager.getInstalledLookAndFeels();
                ArrayList a = new ArrayList();
                for (int i = 0; i < lfs.length; i++) {
                    LF lf = new LF(lfs[i]);

                    // test if the look and feel is supported in this platform before adding it to the list
                    Class lafClassDef;
                                    try {
                                            lafClassDef = Class.forName(lfs[i].getClassName());
                                            LookAndFeel laf;
                                            laf = (LookAndFeel) lafClassDef.newInstance();

                                if (laf.isSupportedLookAndFeel()) {
                                                    a.add(lf);
                                            }

                                    } catch (ClassNotFoundException e2) {
                                            logger.error(ps.getText("error_loading_look_and_feel_"+lfs[i].getName()), e2);
                                    } catch (InstantiationException e1) {
                                            logger.error(ps.getText("error_loading_look_and_feel_"+lfs[i].getName()), e1);
                                    } catch (IllegalAccessException e1) {
                                            logger.error(ps.getText("error_loading_look_and_feel_"+lfs[i].getName()), e1);
                                    }
                }
                lookAndFeelCombo = new JComboBox(a.toArray(new LF[a.size()]));
            } else {
                lookAndFeelCombo = new JComboBox();
            }
        }
        return lookAndFeelCombo;
    }

    private class LF {
        LookAndFeelInfo lfi;

        public LF(LookAndFeelInfo lfi) {
            this.lfi = lfi;
        }

        public String getClassName() {
            return lfi.getClassName();
        }

        public String toString() {
            return lfi.getName();
        }
    }

	public boolean isValueChanged() {
		return changed;
	}

	public void setChangesApplied() {
		changed = false;
	}
}
