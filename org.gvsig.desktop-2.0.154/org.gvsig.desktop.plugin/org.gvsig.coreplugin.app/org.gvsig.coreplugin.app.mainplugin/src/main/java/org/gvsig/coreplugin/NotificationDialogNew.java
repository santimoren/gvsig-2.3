/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.MessageEvent;
import org.gvsig.andami.messages.NotificationListener;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.SingletonWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;

public class NotificationDialogNew extends JPanel implements IWindow,
		SingletonWindow, NotificationListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4363640321917006480L;

	JPanelConsole console;
	JPanelButtons buttons;
	JPanelDescription description;

	public NotificationDialogNew() {
		super();
		this.setLayout(new GridBagLayout());

		description = new JPanelDescription();
		console = new JPanelConsole();
		buttons = new JPanelButtons(console);

		addComppnent(this, description, 0, 0, 3, 1, 1.0, 1.0,
				GridBagConstraints.BOTH, GridBagConstraints.CENTER);
		addComppnent(this, buttons, 0, 1, 3, 1, 0, 0, GridBagConstraints.NONE,
				GridBagConstraints.NORTHEAST);
		addComppnent(this, console, 0, 2, 3, 1, 1.0, 2.0,
				GridBagConstraints.BOTH, GridBagConstraints.CENTER);
		this.setVisible(true);
		this.hideConsole();
	}

	void addComppnent(JPanel panel, JComponent component, int gridx, int gridy,
			int width, int height, double weightx, double weighty, int fill,
			int anchor) {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = gridx;
		constraints.gridy = gridy;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		constraints.weightx = weightx; // 1.0 se estira, 0 no.
		constraints.weighty = weighty; // 1.0 se estira, 0 no.
		constraints.fill = fill;
		constraints.anchor = anchor;
		constraints.ipadx = 2;
		constraints.ipady = 2;

		panel.add(component, constraints);
		component.setVisible(true);
	}

	public void setDescription(String description) {
		this.description.setDescription(description);
	}

	public void closeWindow() {
		PluginServices.getMDIManager().closeWindow(this);
	}

	public void hideConsole() {
		this.buttons.hideConsole();
	}

	public void showConsole() {
		this.buttons.showConsole();
	}

	class JPanelDescription extends JPanel {
		/**
			 * 
			 */
		private static final long serialVersionUID = 1529755877776747074L;
		JTextArea description = null;
		JLabel image;

		JPanelDescription() {
			LayoutManager layout;
			Border border;

			layout = new GridBagLayout();
			this.setLayout(layout);

			// border = BorderFactory.createTitledBorder("Description");
			border = BorderFactory.createEmptyBorder();
			this.setBorder(border);

			this.image = new JLabel();
			this.image.setIcon(IconThemeHelper.getImageIcon("show-console") );
			addComppnent(this, this.image, 0, 0, 1, 1, 0, 0,
					GridBagConstraints.NONE, GridBagConstraints.CENTER);

			this.description = new JTextArea();
			this.description.setEditable(false);
			this.description.setOpaque(false);
			this.description.setText("");
			JScrollPane scrollPanel = new JScrollPane(this.description);
			scrollPanel.setBorder(BorderFactory.createEmptyBorder());
			scrollPanel.setPreferredSize(new Dimension(600, 200));
			addComppnent(this, scrollPanel, 1, 0, 1, 1, 1, 1,
					GridBagConstraints.BOTH, GridBagConstraints.CENTER);
		}

		public String getDescription() {
			return this.description.getText();
		}

		public void setDescription(String description) {
			this.description.setText(description);
		}
	}

	class JPanelButtons extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1529755877776747074L;
		JButton close = null;
		JButton showDetails = null;
		JButton hideDetails = null;
		JPanel console = null;

		JPanelButtons(JPanel console) {
			this.console = console;
			this.setLayout(new FlowLayout());
			this.close = getCloseButton();
			this.showDetails = getShowDetailsButton();
			this.hideDetails = getHideDetailsButton();
			this.add(this.close);
			this.add(this.showDetails);
			this.add(this.hideDetails);
			hideConsole();
		}

		private JButton getCloseButton() {
			JButton button = new JButton(
					PluginServices.getText(this, "aceptar"));
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					closeWindow();
				}
			});
			return button;
		}

		private JButton getShowDetailsButton() {
			JButton button = new JButton(PluginServices.getText(this,
					"detalles") + " >>>");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showConsole();
				}
			});
			return button;
		}

		private JButton getHideDetailsButton() {
			JButton button = new JButton(PluginServices.getText(this,
					"detalles") + " <<<");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					hideConsole();
				}
			});
			return button;
		}

		public void hideConsole() {
			this.showDetails.setVisible(true);
			this.hideDetails.setVisible(false);
			this.console.setVisible(false);
		}

		public void showConsole() {
			this.showDetails.setVisible(false);
			this.hideDetails.setVisible(true);
			this.console.setVisible(true);
		}
	}

	class JPanelConsole extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6651900105647107644L;

		JPanelConsole() {
			BorderLayout layout = new BorderLayout();
			this.setLayout(new BorderLayout());
			TitledBorder border = BorderFactory.createTitledBorder("Console");
			this.setBorder(border);
			this.setLayout(layout);
			this.add(Consola.consolaFrame, BorderLayout.CENTER);
		}
	}

	public Object getWindowModel() {
		return "notification-dialog";
	}

	public WindowInfo getWindowInfo() {
		WindowInfo info = new WindowInfo(WindowInfo.MODELESSDIALOG
				| WindowInfo.ICONIFIABLE | WindowInfo.RESIZABLE | WindowInfo.MAXIMIZABLE);
		info.setTitle(PluginServices.getText(this, "Warning"));
		info.setHeight(this.getPreferredSize().height);
		info.setWidth(this.getPreferredSize().width);
		return info;
	}

	public Object getWindowProfile() {
		return WindowInfo.PROPERTIES_PROFILE;
	}

	public void errorEvent(MessageEvent e) {

	       if (e.getMessages() != null) {
	            for (int i = 0; i < e.getMessages().length; i++) {
	                this.setDescription(e.getMessages()[i]);
	            }
	        }

	    if (!canShowWindow(e.getExceptions())) {
            try {
                PluginServices.getMainFrame().getStatusBar().message(
                    this.description.getDescription(),
                    JOptionPane.ERROR_MESSAGE);
            } catch(Throwable ex) {
                // Ignora cualquier error que se produzca intentando mostrar el mensaje de error y nos
                // conformaremos con que este en el log.
            }
	        return;
	    }
	    

		PluginServices.getMDIManager().restoreCursor();
		if (SwingUtilities.isEventDispatchThread()) {
			PluginServices.getMDIManager().addCentredWindow(this);
		} else {
			final IWindow win = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					PluginServices.getMDIManager().addCentredWindow(win);
				}
			});
		}
	}

	public void warningEvent(MessageEvent e) {
	    
		if (e.getMessages() != null) {
			for (int i = 0; i < e.getMessages().length; i++) {
				this.setDescription(e.getMessages()[i]);
			}
		}

	      if (!canShowWindow(e.getExceptions())) {
	            try {
	                PluginServices.getMainFrame().getStatusBar().message(
	                    this.description.getDescription(),
	                    JOptionPane.ERROR_MESSAGE);
	            } catch(Throwable ex) {
	                // Ignora cualquier error que se produzca intentando mostrar el mensaje de error y nos
	                // conformaremos con que este en el log.
	            }
	            return;
	        }

		PluginServices.getMDIManager().restoreCursor();
		if (SwingUtilities.isEventDispatchThread()) {
			PluginServices.getMDIManager().addCentredWindow(this);
		} else {
			final IWindow win = this;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					PluginServices.getMDIManager().addCentredWindow(win);
				}
			});
		}
	}

	public void infoEvent(MessageEvent e) {
		// Do nothing, ignore info events
	}
	

    private boolean canShowWindow(Throwable[] ths) {
        
        if (ths == null) {
            return true;
        }
        try {
            for (int n=0; n<ths.length; n++) {
                StackTraceElement[] stack_ee = ths[n].getStackTrace();
                for (int i=0; i<stack_ee.length; i++) {
                    StackTraceElement se = stack_ee[i];
                    if (se.getClassName().startsWith("org.gvsig.fmap.mapcontrol.MapControl")) {
                        return false;
                    }
                }
            }
        } catch (Throwable t) {
            
        }
        return true;
        
    }

}
