/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.preferences.general;

//import com.jeta.open.i18n.I18NUtils;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.gvsig.i18n.Messages;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class IconThemePageLayout extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 732724276675072878L;

	JLabel label_title = new JLabel();
	JLabel label_selection = new JLabel();
	JTextArea label_export = new JTextArea();
	JComboBox combo_selection = new JComboBox();
	JButton button_export = new JButton();

	/**
	 * Default constructor
	 */
	public IconThemePageLayout() {
		initializePanel();
	}

	/**
	 * Adds fill components to empty cells in the first row and first column of
	 * the grid. This ensures that the grid spacing will be the same as shown in
	 * the designer.
	 * 
	 * @param cols
	 *            an array of column indices in the first row where fill
	 *            components should be added.
	 * @param rows
	 *            an array of row indices in the first column where fill
	 *            components should be added.
	 */
	void addFillComponents(Container panel, int[] cols, int[] rows) {
		Dimension filler = new Dimension(10, 10);

		boolean filled_cell_11 = false;
		CellConstraints cc = new CellConstraints();
		if (cols.length > 0 && rows.length > 0) {
			if (cols[0] == 1 && rows[0] == 1) {
				/** add a rigid area */
				panel.add(Box.createRigidArea(filler), cc.xy(1, 1));
				filled_cell_11 = true;
			}
		}

		for (int index = 0; index < cols.length; index++) {
			if (cols[index] == 1 && filled_cell_11) {
				continue;
			}
			panel.add(Box.createRigidArea(filler), cc.xy(cols[index], 1));
		}

		for (int index = 0; index < rows.length; index++) {
			if (rows[index] == 1 && filled_cell_11) {
				continue;
			}
			panel.add(Box.createRigidArea(filler), cc.xy(1, rows[index]));
		}

	}

	/**
	 * Helper method to load an image file from the CLASSPATH
	 * 
	 * @param imageName
	 *            the package and name of the file to load relative to the
	 *            CLASSPATH
	 * @return an ImageIcon instance with the specified image file
	 * @throws IllegalArgumentException
	 *             if the image resource cannot be loaded.
	 */
	public ImageIcon loadImage(String imageName) {
		try {
			ClassLoader classloader = getClass().getClassLoader();
			java.net.URL url = classloader.getResource(imageName);
			if (url != null) {
				ImageIcon icon = new ImageIcon(url);
				return icon;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		throw new IllegalArgumentException("Unable to load image: " + imageName);
	}

	/**
	 * Method for recalculating the component orientation for right-to-left
	 * Locales.
	 * 
	 * @param orientation
	 *            the component orientation to be applied
	 */
	public void applyComponentOrientation(ComponentOrientation orientation) {
		// Not yet implemented...
		// I18NUtils.applyComponentOrientation(this, orientation);
		super.applyComponentOrientation(orientation);
	}

	public JPanel createPanel() {
		
		GridBagLayout gbl = new GridBagLayout();
		JPanel resp = new JPanel(gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(2, 30, 0, 30);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		// ==============================
		gbc.gridy = 0;
		label_title.setName("label_title");
		label_title.setText(Messages.getText("_Icon_themes_availables"));
		resp.add(label_title, gbc);
		// ==============================
		gbc.gridy = 1;
		label_selection.setName("label_selection");
		label_selection.setText(Messages.getText(
				"_Select_an_icon_theme_as_default_for_use_in_the_application"));
		resp.add(label_selection, gbc);
		// ==============================
		gbc.gridy = 2;
		combo_selection.setName("combo_selection");
		resp.add(combo_selection, gbc);
		// ==============================
		gbc.gridy = 3;
		resp.add(new JLabel(" "), gbc);
		// ==============================
		gbc.gridy = 4;
		label_export.setName("label_export");
		label_export.setWrapStyleWord(true);
		label_export.setLineWrap(true);
		label_export.setFont(new JLabel().getFont());
		label_export.setBackground(resp.getBackground());
		label_export.setText(Messages.getText(
				"_You_can_export_the_default_icon_theme_to_use_as_base_for_create_a_custom_icon_theme"));
		resp.add(label_export, gbc);
		// ==============================
		gbc.gridy = 5;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.NORTH;
		button_export.setActionCommand("_Export_default_icon_theme");
		button_export.setName("button_export");
		button_export.setText(Messages.getText(
				"_Export_default_icon_theme"));
		resp.add(button_export, gbc);
		// ==============================
		return resp;
	}

	/**
	 * Initializer
	 */
	protected void initializePanel() {
		setLayout(new BorderLayout());
		add(createPanel(), BorderLayout.CENTER);
	}

}
