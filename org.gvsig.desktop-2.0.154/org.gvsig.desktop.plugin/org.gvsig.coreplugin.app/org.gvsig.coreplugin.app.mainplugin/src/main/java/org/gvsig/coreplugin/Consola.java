/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.tools.dynobject.DynObject;

/**
 * Extensión que registra un frame en la aplicación que recibe los eventos de
 * la consola de la aplicación y los muestra en el frame
 */
public class Consola extends Extension {

    //static PluginServices ps;
    public static ConsolaFrame consolaFrame;
    public static NotificationDialogNew notificationDialogNew;
    public static NotificationDialog notificationDialog;

    /**
     * @see com.iver.mdiApp.IExtension#initialize()
     */
    public void initialize() {
        IconThemeHelper.registerIcon("action", "show-console", this);

        PluginsManager pluginsManager = PluginsLocator.getManager();
        PluginServices plugin = pluginsManager.getPlugin(this.getClass());

        consolaFrame = new ConsolaFrame();
        NotificationManager.addNotificationListener(consolaFrame);

        DynObject pluginProperties = plugin.getPluginProperties();
        Boolean showNotificationsInConsole = (Boolean) pluginProperties.getDynValue("showNotificationsInConsole");
        if ( showNotificationsInConsole == null || showNotificationsInConsole.booleanValue() ) {
            notificationDialogNew = new NotificationDialogNew();
            NotificationManager.addNotificationListener(notificationDialogNew);
        }
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
        if ( "show-console".equalsIgnoreCase(actionCommand) ) {
            consolaFrame.setSize(400, 325);
            consolaFrame.setVisible(true);
            PluginServices.getMDIManager().addWindow(consolaFrame);
        }
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isVisible()
     */
    public boolean isVisible() {
        return true;
    }
}
