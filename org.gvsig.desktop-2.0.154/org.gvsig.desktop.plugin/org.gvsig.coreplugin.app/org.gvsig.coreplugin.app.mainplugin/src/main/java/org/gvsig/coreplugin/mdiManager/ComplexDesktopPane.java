/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.coreplugin.mdiManager;

import javax.swing.JPanel;


import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JDesktopPane;

import org.gvsig.utils.console.JConsole;
public class ComplexDesktopPane extends JPanel {

	private JSplitPane jSplitPane = null;
	private JDesktopPane jDesktopPane = null;
	private JConsole jConsole = null;
	/**
	 * This is the default constructor
	 */
	public ComplexDesktopPane() {
		super();
		initialize();
	}
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private  void initialize() {
		this.setLayout(new BorderLayout());
		this.setSize(300,200);
		this.add(getJSplitPane(), java.awt.BorderLayout.CENTER);
	}
	/**
	 * This method initializes jSplitPane	
	 * 	
	 * @return javax.swing.JSplitPane	
	 */    
	private JSplitPane getJSplitPane() {
		if (jSplitPane == null) {
			jSplitPane = new JSplitPane();
			jSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
			jSplitPane.setResizeWeight(0.8);
			jSplitPane.setTopComponent(getDesktopPane());
			jSplitPane.setBottomComponent(getJConsole());
		}
		return jSplitPane;
	}
	/**
	 * This method initializes jDesktopPane	
	 * 	
	 * @return javax.swing.JDesktopPane	
	 */    
	public JDesktopPane getDesktopPane() {
		if (jDesktopPane == null) {
			jDesktopPane = new JDesktopPane();
		}
		return jDesktopPane;
	}
	/**
	 * This method initializes jConsole	
	 * 	
	 * @return com.iver.utiles.console.JConsole	
	 */    
	private JConsole getJConsole() {
		if (jConsole == null) {
			jConsole = new JConsole();
		}
		return jConsole;
	}
   }
