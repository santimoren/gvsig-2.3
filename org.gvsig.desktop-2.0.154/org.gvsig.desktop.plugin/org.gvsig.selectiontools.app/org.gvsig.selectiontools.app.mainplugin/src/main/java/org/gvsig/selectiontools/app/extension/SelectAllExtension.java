/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;

/**
 * Extension to add support for selecting all the features of a vector layer.
 * 
 */
public class SelectAllExtension extends Extension {

    public void initialize() {
    	IconThemeHelper.registerIcon("action", "selection-select-all", this);
    }

    public void execute(String actionCommand) {
        if (actionCommand.equals("SELALL")) {
            IWindow view = PluginServices.getMDIManager().getActiveWindow();
            if (view instanceof IView) {
                MapControl mc = ((IView) view).getMapControl();
                FLayer[] activeLayers =
                    mc.getMapContext().getLayers().getActives();

                FLayer layer;

                for (int i = 0; i < activeLayers.length; i++) {
                    layer = activeLayers[i];

                    if ((layer.isAvailable()) && (layer instanceof FLyrVect)) {
                        FLyrVect lyrVect = (FLyrVect) layer;

                        try {
                            FeatureStore fs = lyrVect.getFeatureStore();
                            fs.getFeatureSelection().selectAll();
                        } catch (DataException e) {
                            NotificationManager.showMessageError("Data exception",
                                e);
                        }
                    }
                }
            }
        }
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        return document.getMapContext().hasActiveVectorLayers();
    }
}
