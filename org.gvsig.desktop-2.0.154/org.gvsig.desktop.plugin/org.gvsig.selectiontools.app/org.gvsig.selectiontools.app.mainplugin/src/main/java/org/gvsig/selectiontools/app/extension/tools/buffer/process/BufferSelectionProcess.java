/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension.tools.buffer.process;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.gui.beans.buttonspanel.ButtonsPanel;
import org.gvsig.gui.beans.incrementabletask.IncrementableProcess;
import org.gvsig.gui.beans.incrementabletask.IncrementableTask;
import org.gvsig.selectiontools.app.extension.tools.buffer.gui.BufferConfigurationPanel;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * 
 * 
 * @author Pablo Piqueras Bartolom� (pablo.piqueras@iver.es)
 */
public class BufferSelectionProcess extends IncrementableProcess {

    // private boolean layerWasBeingEdited = false;

    private MapControl mapControl = null;
    private byte pol_side = -1;
    private byte line_side = -1;
    private byte point_side = -1;
    private byte multi_point_side = -1;
    private short selectedDistanceUnit = -1;
    private FLyrVect[] layers = null;
    private final double f_width;
    private boolean multiLayerSelection = false;

    /**
     * For polygonal buffers, only compute interior buffers
     */
    public static final byte BUFFER_INSIDE_POLY = 0;

    /**
     * For polygonal buffers, only compute exterior buffers (is the default
     * operation, it applies to any geometry type)
     */
    public static final byte BUFFER_OUTSIDE_POLY = 1;

    /**
     * For polygonal buffers, compute interior and exterior buffers
     */
    public static final byte BUFFER_INSIDE_OUTSIDE_POLY = 2;

    /**
     * Buffer with square cap
     */
    public static final byte CAP_SQUARE = 0;
    /**
     * Buffer with round cap
     */
    public static final byte CAP_ROUND = 1;

    /**
     * flag that represents buffer computing with one only buffer distance.
     */
    public static final byte CONSTANT_DISTANCE_STRATEGY = 0;

    /**
     * buffer computing with variable buffer distance in function of feature
     * attribute value
     */
    public static final byte ATTRIBUTE_DISTANCE_STRATEGY = 1;

    /**
     * Creates a new
     * <p>
     * BufferSelectionProcess
     * </p>
     * .
     * 
     * @param title
     *            of the progress dialog
     * @param label
     *            the label that explains the process
     * @param mapControl
     *            reference to the current active view's <code>MapControl</code>
     *            .
     * @param pol_side
     *            side of the buffer in a polyline layer:
     *            {@link BufferConfigurationPanel#OUTSIDE
     *            BufferConfigurationPanel#OUTSIDE},
     *            {@link BufferConfigurationPanel#INSIDE
     *            BufferConfigurationPanel#INSIDE}, or
     *            {@link BufferConfigurationPanel#OUTSIDE_AND_INSIDE
     *            BufferConfigurationPanel#OUTSIDE_AND_INSIDE}
     * @param line_side
     *            side of the buffer in a line layer:
     *            {@link BufferConfigurationPanel#OUTSIDE_AND_INSIDE
     *            BufferConfigurationPanel#OUTSIDE_AND_INSIDE}
     * @param point_side
     *            side of the buffer in a point layer:
     *            {@link BufferConfigurationPanel#OUTSIDE
     *            BufferConfigurationPanel#OUTSIDE}
     * @param multi_point_side
     *            side of the buffer in a multi point layer:
     *            {@link BufferConfigurationPanel#OUTSIDE
     *            BufferConfigurationPanel#OUTSIDE}
     * @param width
     *            buffer's width
     * @param selectedDistanceUnit
     *            distance unit selected
     * @param activeLayers
     *            current active view's active layers
     * @param showBufferLayers
     *            determines if will show the layers with the buffers as new
     *            temporal layers
     * @param multiLayerSelection
     *            determines if the selection in each active layer affects the
     *            other
     */
    public BufferSelectionProcess(String title,
        String label,
        MapControl mapControl,
        byte pol_side,
        byte line_side,
        byte point_side,
        byte multi_point_side,
        double width,
        short selectedDistanceUnit,
        FLyrVect[] activeLayers,
        boolean multiLayerSelection) {
        super(title);

        this.label = label;
        this.mapControl = mapControl;
        this.pol_side = pol_side;
        this.line_side = line_side;
        this.point_side = point_side;
        this.multi_point_side = multi_point_side;
        this.f_width = width;
        this.selectedDistanceUnit = selectedDistanceUnit;
        this.layers = activeLayers;
        this.multiLayerSelection = multiLayerSelection;
        this.isPausable = true;
    }

    /**
     * Sets the object that will display the evolution of this loading process
     * as a progress dialog.
     * 
     * @param iTask
     *            the object that will display the evolution of this loading
     *            process
     */
    public void setIncrementableTask(IncrementableTask iTask) {
        this.iTask = iTask;
        iTask.setAskCancel(true);
        iTask.getButtonsPanel().addAccept();
        iTask.getButtonsPanel().setEnabled(ButtonsPanel.BUTTON_ACCEPT, false);

        JButton jButton =
            iTask.getButtonsPanel().getButton(ButtonsPanel.BUTTON_ACCEPT);
        jButton.addMouseListener(new MouseAdapter() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent
             * )
             */
            public void mouseClicked(MouseEvent e) {
                processFinalize();
                // Force repaint
                mapControl.getMapContext().invalidate();
            }
        });
    }

    /**
     * Importation process.
     * 
     * @throws InterruptedException
     *             if fails the process
     */
    public void process() throws InterruptedException {
        percentage = 5;
        double inc = 0;
        FeatureSelection selections[] = new FeatureSelection[layers.length];
        ArrayList<Geometry> buffer[] = new ArrayList[layers.length];
        try {

            /* 2- Gets the distance relation */
            percentage = 6;

            /* 3- Stores the new selections */
            FLyrVect layer = null;

            // int size;

            percentage = 9;

            /* 4- Gets the buffer and intersects to select new geometries */
            double width = 1; // default value (not used)
            byte side;

            percentage = 11;

            boolean isProjected = false;

            IWindow f = PluginServices.getMDIManager().getActiveWindow();
            if (f instanceof IView) {
                IView vista = (IView) f;

                // Sets projection
                IProjection proj =
                    vista.getMapControl().getViewPort().getProjection();

                // Sets distance units
                isProjected = proj.isProjected();
            }
            // Sets map units
            int mapUnits = -1;
            if (isProjected) {
                mapUnits =
                    ((IView) PluginServices.getMDIManager().getActiveWindow()).getMapControl()
                        .getViewPort()
                        .getMapUnits();
            } else {
                mapUnits = 1;
            }

            percentage = 14;

            FeatureStore store = null;
            inc = (100 - percentage) / layers.length;

            /* 4.1- For each vector layer with geometries selected */
            for (int i = 0; i < layers.length; i++) {
                try {
                    if (cancelProcess.isCanceled()) {
                        throw new InterruptedException();
                    }

                    layer = layers[i];
                    store = layer.getFeatureStore();
                    FeatureSelection selection = (FeatureSelection) store
                            .getFeatureSelection()
                            .clone();
                    selections[i] = selection;

                    log.addLine(PluginServices.getText(null,
                        "Starting_selection_of_layer")
                        + " \""
                        + layer.getName() + "\"");

                    switch (layer.getShapeType()) {
                    case Geometry.TYPES.POINT:
                        side = point_side;
                        break;
                    case Geometry.TYPES.CURVE:
                    case Geometry.TYPES.MULTICURVE:
                        side = line_side;
                        break;
                    case Geometry.TYPES.SURFACE:
                    case Geometry.TYPES.MULTISURFACE:
                        side = pol_side;
                        break;
                    case Geometry.TYPES.MULTIPOINT:
                        side = multi_point_side;
                        break;
                    default: // UNDEFINED
                        // UNSUPPORTED
                        log.addLine(PluginServices.getText(null,
                            "Layer_with_unsupported_geometries_type"));
                        percentage += inc;
                        continue;
                    }

                    /* 4.2- Calculates the width */
                    width =
                        f_width
                            * MapContext.getDistanceTrans2Meter()[selectedDistanceUnit]
                            / MapContext.getDistanceTrans2Meter()[mapUnits];

                    /* 4.4- Sets the buffer width */
                    /*
                    log.addLine(PluginServices.getText(null,
                        "Buffer_information") + ":");
                        */

                    /* 4.5- Shows width information */
                    /*
                    if (mapControl.getProjection().isProjected()) {
                        log.addLine("    "
                            + PluginServices.getText(null, "Buffer_width")
                            + ": " + width + " m.");
                    } else {
                        log.addLine("    "
                            + PluginServices.getText(null, "Buffer_width")
                            + ": " + width + " m");
                        log.addLine("    "
                            + PluginServices.getText(null, "Buffer_width")
                            + ": " + width
                            / MapContext.getDistanceTrans2Meter()[8] + " �");
                    }

                    log.addLine("    "
                        + PluginServices.getText(null, "Buffer_cap") + ": "
                        + PluginServices.getText(null, "Round"));

                    switch (side) {
                    case BUFFER_OUTSIDE_POLY:
                        log.addLine("    "
                            + PluginServices.getText(null, "Side") + ": "
                            + PluginServices.getText(null, "Outside"));
                        break;
                    case BUFFER_INSIDE_POLY:
                        log.addLine("    "
                            + PluginServices.getText(null, "Side") + ": "
                            + PluginServices.getText(null, "Inside"));
                        break;
                    case BUFFER_INSIDE_OUTSIDE_POLY:
                        log.addLine("    "
                            + PluginServices.getText(null, "Side")
                            + ": "
                            + PluginServices.getText(null, "Outside_and_inside"));
                        break;
                    }
                    */

                    /*
                     * 4.3- Creates the influence area
                     */
                    // if (cancelProcess.isCanceled()) {
                    // throw new InterruptedException();
                    // }

                    DisposableIterator selIt = selection.fastIterator();
                    ArrayList<Geometry> bufferLayer = new ArrayList<Geometry>();
                    while (selIt.hasNext()) {
                        Feature feat = (Feature) selIt.next();
                        Geometry geomBuffer = null;
                        switch (side) {
                        case BUFFER_OUTSIDE_POLY:
                            geomBuffer = buffer(
                                feat.getDefaultGeometry(),
                                layer.getProjection(),
                                layer.getCoordTrans(),
                                width);
                            // feat.getDefaultGeometry().buffer(width);
                            break;
                        case BUFFER_INSIDE_POLY:
                            geomBuffer = buffer(
                                feat.getDefaultGeometry(),
                                layer.getProjection(),
                                layer.getCoordTrans(),
                                -width);
                            // feat.getDefaultGeometry().buffer(-width);
                            break;
                        case BUFFER_INSIDE_OUTSIDE_POLY:
                            // Aqu� no deber�a llegar nunca, por si acaso lo
                            // dejamos igual que el BUFFER_OUTSIDE_POLY
                            geomBuffer = buffer(
                                feat.getDefaultGeometry(),
                                layer.getProjection(),
                                layer.getCoordTrans(),
                                width);
                            // feat.getDefaultGeometry().buffer(width);
                            break;
                        }
                        bufferLayer.add(geomBuffer);
                    }
                    buffer[i] = bufferLayer;
                    selIt.dispose();

                } catch (Exception e3) {
                    /* Notifies the exception in the log */
                    if (!cancelProcess.isCanceled()) {
                        NotificationManager.showMessageError(PluginServices.getText(null,
                            "Error_fallo_geoproceso"),
                            e3);
                        log.addLine(PluginServices.getText(null,
                            "Error_fallo_geoproceso"));
                    }

                    throw new InterruptedException();
                }
            }

            for (int i = 0; i < layers.length; i++) {
                layer = layers[i];
                
                layer.getFeatureStore().disableNotifications();

                FeatureSelection layerSelection =
                    layer.getFeatureStore().getFeatureSelection();
                
                Geometry aux_geometry;
                if (multiLayerSelection) {
                    for (int j = 0; j < buffer.length; j++) {
                        ArrayList<Geometry> bufferLayer = buffer[j];
                        if (bufferLayer != null) {
                            Iterator<Geometry> geomIt = bufferLayer.iterator();
                            while (geomIt.hasNext()) {
                                aux_geometry = geomIt.next();
                                FeatureSet aux_featSet =
                                    layer.queryByGeometry(aux_geometry,
                                        layer.getFeatureStore()
                                            .getDefaultFeatureType());
                                layerSelection.select(aux_featSet);
                            }
                        }
                    }

                } else {
                    ArrayList<Geometry> bufferLayer = buffer[i];
                    Iterator<Geometry> geomIt = bufferLayer.iterator();
                    while (geomIt.hasNext()) {
                        aux_geometry = geomIt.next();
                        FeatureSet aux_featSet =
                            layer.queryByGeometry(aux_geometry,
                                layer.getFeatureStore().getDefaultFeatureType());
                        layerSelection.select(aux_featSet);
                    }

                }
                
                layer.getFeatureStore().enableNotifications();
            }

            /*
            log.addLine(PluginServices.getText(null,
                "Selection_process_finished_succesfully"));
            log.addLine(""); // Empty line
            */
            
        } catch (Exception de) {
            /* 5- Notifies the exception in the log */
            if (!cancelProcess.isCanceled()) {
                log.addLine(PluginServices.getText(null, "Selection_restored"));

                throw new InterruptedException();
            }

            percentage += inc;
        }

        percentage = 100;
    }
    
    /**

     * @param geom geom of store
     * @param storeProj
     * @param layerCT
     * @param dist in meters
     * @return buffered geom in CRS of view
     */
    private Geometry buffer(
        Geometry geom,
        IProjection storeProj,
        ICoordTrans layerCT,
        double dist) throws Exception {
        
        if (layerCT == null) {
            // Not reprojected on the fly
            if (storeProj.isProjected()) {
                return geom.buffer(dist);
            } else {
                dist = dist * 180.0 / (Math.PI * 6378137);
                return geom.buffer(dist);
            }
            
        } else {
            
         // Reprojected on the fly
            Geometry aux = geom.cloneGeometry();
            aux.reProject(layerCT);
            return buffer(aux, layerCT.getPDest(), null, dist);
        }
    }

}
