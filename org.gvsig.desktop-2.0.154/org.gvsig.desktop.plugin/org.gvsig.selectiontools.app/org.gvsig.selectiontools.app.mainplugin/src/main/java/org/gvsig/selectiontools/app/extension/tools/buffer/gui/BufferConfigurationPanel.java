/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension.tools.buffer.gui;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.gui.beans.incrementabletask.IncrementableTask;
import org.gvsig.gui.beans.progresspanel.ProgressPanel;
import org.gvsig.i18n.Messages;
import org.gvsig.selectiontools.app.extension.tools.buffer.process.BufferSelectionProcess;
import org.gvsig.utils.swing.JComboBox;

/**
 *
 *
 * @author Pablo Piqueras Bartolom� (pablo.piqueras@iver.es)
 */
public class BufferConfigurationPanel extends JPanel implements IWindow {

    private WindowInfo viewInfo = null;
    private JTextField distanceText = null;
    private JComboBox distanceUnitsCombo = null;
    private JComboBox polygonSidesCombo = null;
    private JComboBox lineSidesCombo = null;
    private JComboBox pointSidesCombo = null;
    private JComboBox multiPointSidesCombo = null;
    private JLabel distanceUnitsLabel = null;
    private JLabel polygonSideLabel = null;
    private JLabel lineSideLabel = null;
    private JLabel pointSideLabel = null;
    private JLabel multiPointSideLabel = null;
    private JPanel polygonSidePanel = null;
    private JPanel lineSidePanel = null;
    private JPanel pointSidePanel = null;
    private JPanel multiPointSidePanel = null;
    private JLabel widthLabel = null;
    private JPanel widthPanel = null;
    private JPanel sidePanel = null;
    private JPanel optionsPanel = null;
    private AdaptedAcceptCancelPanel acceptCancelPanel = null;
    private List<FLyrVect> layers;
    private MapControl mapControl;
    private IView view;
    private JCheckBox multiLayerSelectionCBox;
    private SideInfo outside, inside, out_in_side;

    /**
     * Creates a new form where user can define the option of the buffer.
     */
    public BufferConfigurationPanel(List<FLyrVect> layers, IView view) {
        super();

        this.layers = layers;
        this.view = view;
        this.mapControl = view.getMapControl();

        initialize();
    }

    public BufferConfigurationPanel(FLyrVect[] array, IView view) {
        this(Arrays.asList(array),view);
    }

    /**
     * Initializes this component.
     */
    private void initialize() {
        outside =
            new SideInfo(BufferSelectionProcess.BUFFER_OUTSIDE_POLY,
                PluginServices.getText(null, "Outside"));
        inside =
            new SideInfo(BufferSelectionProcess.BUFFER_INSIDE_POLY,
                PluginServices.getText(null, "Inside"));
        out_in_side =
            new SideInfo(BufferSelectionProcess.BUFFER_INSIDE_OUTSIDE_POLY,
                PluginServices.getText(null, "Both"));

        setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setSize(300, 275);
        
        GridBagLayout gbl = new GridBagLayout();
        JPanel centerPanel = new JPanel(gbl);   
        GridBagConstraints c = new GridBagConstraints();
        
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        
        c.gridx = 0;
        c.gridy = 0;
        centerPanel.add(getWidthPanel(),c);
        
        c.gridy = 1;
        centerPanel.add(getSidePanel(),c);
        
        c.gridy = 2;
        centerPanel.add(getOptionsPanel(),c);
        
        this.add(centerPanel, BorderLayout.CENTER);
        
        this.add(getAdaptedAcceptCancelPanel(), BorderLayout.SOUTH);
    }

    private JPanel getOptionsPanel() {
        if (optionsPanel == null) {
            optionsPanel = new JPanel(new GridBagLayout());
            optionsPanel.setBorder(BorderFactory.createTitledBorder(PluginServices.getText(optionsPanel,
                "Options")));
            GridBagConstraints cons = new GridBagConstraints();
            
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(0, 5, 0, 5);
            
            cons.gridx = 0;
            cons.gridy = 0;
            cons.weightx = 1;
            optionsPanel.add(getMultiLayerSelectionCBox(),cons);
        }

        return optionsPanel;
    }

    private JCheckBox getMultiLayerSelectionCBox() {
        if (multiLayerSelectionCBox == null) {
            multiLayerSelectionCBox = new JCheckBox();
            multiLayerSelectionCBox.setText(PluginServices.getText(multiLayerSelectionCBox,
                "MultiLayer_selection"));
            multiLayerSelectionCBox.setSelected(true);
            multiLayerSelectionCBox.setToolTipText(PluginServices.getText(null,
                "multiLayerSelection_checkbox_TOOLTIP_HTML_explanation"));
        }

        return multiLayerSelectionCBox;
    }

    private JPanel getWidthPanel() {
        if (widthPanel == null) {
            widthPanel = new JPanel();
            widthPanel.setBorder(BorderFactory.createTitledBorder(PluginServices.getText(widthPanel,
                "width")));
            widthPanel.setLayout(new GridBagLayout());
            GridBagConstraints cons = new GridBagConstraints();
                        
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(0, 5, 0, 5);
            
            cons.gridx = 0;
            cons.gridy = 0;
            widthPanel.add(getWidthLabel(),cons);
            
            cons.gridx = 1;
            cons.gridwidth = 2;
            widthPanel.add(getWidthText(),cons);

            cons.gridx = 3;
            cons.gridwidth = 1;
            widthPanel.add(getDistanceUnitsLabel(),cons);
            
            cons.gridx = 4;
            cons.gridwidth = 2;
            widthPanel.add(getDistanceUnitsCombo(),cons);
        }

        return widthPanel;
    }

    private JLabel getWidthLabel() {
        if (widthLabel == null) {
            widthLabel = new JLabel();
            widthLabel.setText(PluginServices.getText(widthLabel, "Width"));
            widthLabel.setToolTipText(PluginServices.getText(null,
                "bufferWidth_TOOLTIP_HTML_explanation"));
        }

        return widthLabel;
    }

    private JTextField getWidthText() {
        if (distanceText == null) {
            /*
            DecimalFormat decimalFormat = new DecimalFormat();

            decimalFormat.setDecimalSeparatorAlwaysShown(true);
            decimalFormat.setMaximumIntegerDigits(12);
            decimalFormat.setMinimumIntegerDigits(1);
            decimalFormat.setMinimumFractionDigits(2);
            decimalFormat.setMaximumFractionDigits(4);

            decimalFormat.getDecimalFormatSymbols().setDecimalSeparator('.');

            NumberFormatter numberFormatter = new NumberFormatter();
            numberFormatter.setAllowsInvalid(false);
            numberFormatter.setOverwriteMode(false);
            numberFormatter.setCommitsOnValidEdit(true);
            numberFormatter.setMinimum(new Double(0));
            numberFormatter.setFormat(decimalFormat);
            */

            // numberFormatter.setFormat(new
            // DecimalFormat("([+]|[-])?[0-9]+([.][0-9]+)?"));

            distanceText = new JTextField();
            distanceText.setPreferredSize(new Dimension(65, 22));
            distanceText.setText("100");
        }

        return distanceText;
    }

    private JLabel getDistanceUnitsLabel() {
        if (distanceUnitsLabel == null) {
            distanceUnitsLabel =
                new JLabel(PluginServices.getText(distanceUnitsLabel, "Units").concat(":"));
            distanceUnitsLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);
            distanceUnitsLabel.setToolTipText(PluginServices.getText(null,
                "distanceUnitsLabel_TOOLTIP_HTML_explanation"));
        }

        return distanceUnitsLabel;
    }

    private JComboBox getDistanceUnitsCombo() {
        if (distanceUnitsCombo == null) {
            distanceUnitsCombo = new JComboBox();
            distanceUnitsCombo.setPreferredSize(new Dimension(65, 22));
            distanceUnitsCombo.addItem("km");
            distanceUnitsCombo.addItem("m");
            distanceUnitsCombo.addItem("cm");
            distanceUnitsCombo.addItem("mm");
            distanceUnitsCombo.addItem("mi");
            distanceUnitsCombo.addItem("Ya");
            distanceUnitsCombo.addItem("ft");
            distanceUnitsCombo.addItem("in");
            // distanceUnitsCombo.addItem("�");
            distanceUnitsCombo.setSelectedIndex(1); // By default in meters
            distanceUnitsCombo.setToolTipText(PluginServices.getText(null,
                "distanceUnitsLabel_TOOLTIP_HTML_explanation"));
        }

        return distanceUnitsCombo;
    }

    private JPanel getSidePanel() {
        if (sidePanel == null) {
            sidePanel = new JPanel();
            sidePanel.setLayout(new GridBagLayout());
            sidePanel.setBorder(BorderFactory.createTitledBorder(PluginServices.getText(sidePanel,
                "Side")));
            sidePanel.setToolTipText(PluginServices.getText(null,
                "sideLabel_TOOLTIP_HTML_explanation"));

            GridBagConstraints c = new GridBagConstraints();
            
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(0, 2, 0, 2);
            
            c.gridx = 0;
            c.gridy = 0;
            c.weightx = 1;
            sidePanel.add(getPolygonSidePanel(),c);

            c.gridy = 1;
            sidePanel.add(getLineSidePanel(),c);

            c.gridy = 2;
            sidePanel.add(getPointSidePanel(),c);

            c.gridy = 3;
            sidePanel.add(getMultiPointSidePanel(),c);
        }

        return sidePanel;
    }

    private JPanel getPolygonSidePanel() {
        if (polygonSidePanel == null) {
            polygonSidePanel = new JPanel(new GridBagLayout());
            GridBagConstraints cons = new GridBagConstraints();
            
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(5, 5, 5, 5);
            
            cons.gridx = 0;
            cons.gridy = 0;
            cons.weightx = 0;
            polygonSidePanel.add(getPolygonSideLabel(),cons);
            
            cons.gridx = 1;
            cons.weightx = 1;
            polygonSidePanel.add(getPolygonSidesCombo(),cons);
        }

        return polygonSidePanel;
    }

    private JLabel getPolygonSideLabel() {
        if (polygonSideLabel == null) {
            polygonSideLabel =
                new JLabel(PluginServices.getText(polygonSideLabel, "Polygon").concat(":"));
            polygonSideLabel.setToolTipText(PluginServices.getText(null,
                "polygonSideLabel_TOOLTIP_HTML_explanation"));
        }

        return polygonSideLabel;
    }

    private JComboBox getPolygonSidesCombo() {
        if (polygonSidesCombo == null) {
            polygonSidesCombo = new JComboBox();
            polygonSidesCombo.addItem(outside);
            polygonSidesCombo.addItem(inside);
            // polygonSidesCombo.addItem(out_in_side); // Disabled because fails
            // quite often
            polygonSidesCombo.setToolTipText(PluginServices.getText(null,
                "polygonSideLabel_TOOLTIP_HTML_explanation"));
        }

        return polygonSidesCombo;
    }

    private JPanel getLineSidePanel() {
        if (lineSidePanel == null) {
            lineSidePanel = new JPanel(new GridBagLayout());
            GridBagConstraints cons = new GridBagConstraints();
            
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(5, 5, 5, 5);
            
            cons.gridx = 0;
            cons.gridy = 1;
            cons.weightx = 0;
            polygonSidePanel.add(getLineSideLabel(),cons);
            
            cons.gridx = 1;
            cons.weightx = 1;
            polygonSidePanel.add(getLineSidesCombo(),cons);
        }

        return lineSidePanel;
    }

    private JLabel getLineSideLabel() {
        if (lineSideLabel == null) {
            lineSideLabel =
                new JLabel(PluginServices.getText(lineSideLabel, "Line").concat(":"));
            lineSideLabel.setToolTipText(PluginServices.getText(null,
                "lineSideLabel_TOOLTIP_HTML_explanation"));
        }

        return lineSideLabel;
    }

    private JComboBox getLineSidesCombo() {
        if (lineSidesCombo == null) {
            lineSidesCombo = new JComboBox();
            lineSidesCombo.addItem(outside);
            lineSidesCombo.setToolTipText(PluginServices.getText(null,
                "lineSideLabel_TOOLTIP_HTML_explanation"));
        }

        return lineSidesCombo;
    }

    private JPanel getPointSidePanel() {
        if (pointSidePanel == null) {
            pointSidePanel = new JPanel(new GridBagLayout());
            GridBagConstraints cons = new GridBagConstraints();
            
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(5, 5, 5, 5);
            
            cons.gridx = 0;
            cons.gridy = 2;
            cons.weightx = 0;
            polygonSidePanel.add(getPointSideLabel(),cons);
            
            cons.gridx = 1;
            cons.weightx = 1;
            polygonSidePanel.add(getPointSidesCombo(),cons);
        }

        return pointSidePanel;
    }

    private JLabel getPointSideLabel() {
        if (pointSideLabel == null) {
            pointSideLabel =
                new JLabel(PluginServices.getText(pointSideLabel, "Point").concat(":"));
            pointSideLabel.setToolTipText(PluginServices.getText(null,
                "pointSideLabel_TOOLTIP_HTML_explanation"));
        }

        return pointSideLabel;
    }

    private JComboBox getPointSidesCombo() {
        if (pointSidesCombo == null) {
            pointSidesCombo = new JComboBox();
            pointSidesCombo.addItem(outside);
            pointSidesCombo.setToolTipText(PluginServices.getText(null,
                "pointSideLabel_TOOLTIP_HTML_explanation"));
        }

        return pointSidesCombo;
    }

    private JPanel getMultiPointSidePanel() {
        if (multiPointSidePanel == null) {
            multiPointSidePanel = new JPanel(new FlowLayout());
            GridBagConstraints cons = new GridBagConstraints();
            
            cons.fill = GridBagConstraints.BOTH;
            cons.insets = new Insets(5, 5, 5, 5);
            
            cons.gridx = 0;
            cons.gridy = 3;
            cons.weightx = 0;
            polygonSidePanel.add(getMultiPointSideLabel(),cons);
            
            cons.gridx = 1;
            cons.weightx = 1;
            polygonSidePanel.add(getMultiPointSidesCombo(),cons);
        }

        return multiPointSidePanel;
    }

    private JLabel getMultiPointSideLabel() {
        if (multiPointSideLabel == null) {
            multiPointSideLabel =
                new JLabel(PluginServices.getText(multiPointSideLabel,
                    "MultiPoint") + ":");
            multiPointSideLabel.setToolTipText(PluginServices.getText(null,
                "multiPointSideLabel_TOOLTIP_HTML_explanation"));
        }

        return multiPointSideLabel;
    }

    private JComboBox getMultiPointSidesCombo() {
        if (multiPointSidesCombo == null) {
            multiPointSidesCombo = new JComboBox();
            multiPointSidesCombo.addItem(outside);
            multiPointSidesCombo.setToolTipText(PluginServices.getText(null,
                "multiPointSideLabel_TOOLTIP_HTML_explanation"));
        }

        return multiPointSidesCombo;
    }

    /**
     * <p>
     * This method initializes acceptCancelPanel.
     * </p>
     *
     * @return an adapted {@link AcceptCancelPanel AcceptCancelPanel}
     */
    private AdaptedAcceptCancelPanel getAdaptedAcceptCancelPanel() {
        if (acceptCancelPanel == null) {
            acceptCancelPanel = new AdaptedAcceptCancelPanel();
        }

        return acceptCancelPanel;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.andami.ui.mdiManager.IWindow#getWindowInfo()
     */
    public WindowInfo getWindowInfo() {
        if (viewInfo == null) {
            viewInfo = new WindowInfo(WindowInfo.MODALDIALOG);
            viewInfo.setTitle(
                Messages.getText("Select_by_buffer"));
            viewInfo.setWidth(this.getWidth());
            viewInfo.setHeight(this.getHeight());
        }

        return viewInfo;
    }

    /**
     * <p>
     * Adapts {@link AcceptCancelPanel AcceptCancelPanel} to be used as a
     * component of the <code>BufferConfigurationPanel</code> panel.
     * </p>
     *
     * @author Pablo Piqueras Bartolom� (pablo.piqueras@iver.es)
     */
    private class AdaptedAcceptCancelPanel extends AcceptCancelPanel {

        public AdaptedAcceptCancelPanel() {
            super();

            addOkButtonActionListener(getOKAction());
            addCancelButtonActionListener(getCancelAction());
        }

        /**
         * <p>
         * Create the action that will be executed when user pressed the
         * <i>ok</i> button.
         * </p>
         *
         * @return action that will be executed when user pressed the
         *         <i>cancel</i>
         *         button
         */
        private ActionListener getOKAction() {
            // OK button action
            return new ActionListener() {

                /*
                 * @see
                 * java.awt.event.ActionListener#actionPerformed(java.awt.event
                 * .ActionEvent)
                 */
                public void actionPerformed(ActionEvent e) {

                    /* 1- Validates the buffer width */
                    double width;

                    try {
                        width = Double.parseDouble(getWidthText().getText());
                        /*
                                .replaceAll("(\\.)?", "")
                                .replace(",", ".")); // Formats the decimal
                                                     // number to be parsed
                                                      *
                                                      */
                    } catch (Exception ex) {

                        JOptionPane.showMessageDialog(
                            ApplicationLocator.getManager().getRootComponent(),
                            Messages.getText("_Invalid_width_value")
                            + " : \"" + getWidthText().getText() + "\"",
                            Messages.getText("Select_by_buffer"),
                            JOptionPane.WARNING_MESSAGE);
                        return;
                    }

                    /* 2- Closes this window */
                    closeThis();


                    /* 3- Creates the process */
                    // checks layers to proccess if multilayer is not selected
                    List<FLyrVect> tmpLayersToProccess = new ArrayList<FLyrVect>();
                    tmpLayersToProccess.addAll(layers);
                    if (!multiLayerSelectionCBox.isSelected()) {
                        Iterator<FLyrVect> iter =
                            tmpLayersToProccess.iterator();
                        FLyrVect curLayer;
                        while (iter.hasNext()) {
                            curLayer = (FLyrVect) iter.next();
                            try {
                                if (curLayer.getFeatureStore()
                                    .getFeatureSelection()
                                    .isEmpty()) {
                                    iter.remove();
                                }
                            } catch (DataException e1) {
                                NotificationManager.showMessageError(PluginServices.getText(null,
                                    "Failed_selecting_layer"),
                                    e1);
                                return;

                            }
                        }
                    }
                    FLyrVect[] layersToProcess =
                        (FLyrVect[]) tmpLayersToProccess.toArray(new FLyrVect[tmpLayersToProccess.size()]);

                    BufferSelectionProcess iprocess =
                        new BufferSelectionProcess(PluginServices.getText(this,
                            "Select_by_buffer"),
                            PluginServices.getText(this,
                                "Ongoing_process_please_wait"),
                            mapControl,
                            ((SideInfo) getPolygonSidesCombo().getSelectedItem()).getSide(),
                            ((SideInfo) getLineSidesCombo().getSelectedItem()).getSide(),
                            ((SideInfo) getPointSidesCombo().getSelectedItem()).getSide(),
                            ((SideInfo) getMultiPointSidesCombo().getSelectedItem()).getSide(),
                            width,
                            (short) getDistanceUnitsCombo().getSelectedIndex(),
                            layersToProcess,
                            getMultiLayerSelectionCBox().isSelected());// getAddBufferLayersCBox().isSelected(),
                                                                       // getAddInfluenceAreaLayersCBox().isSelected(),
                                                                       // getMultiLayerSelectionCBox().isSelected());
                    IncrementableTask iTask =
                        new IncrementableTask(iprocess,
                            new ProgressPanel(false));
                    iTask.addIncrementableListener(iprocess);
                    iprocess.setIncrementableTask(iTask);

                    iTask.getProgressPanel()
                        .addComponentListener(new ComponentAdapter() {
                            public void componentHidden(ComponentEvent e) {
                                mapControl.getMapContext().invalidate();
                            }
                        });

                    /* 4- Starts the process */
                    iprocess.start();
                    iTask.start();
                }
            };
        }

        /**
         * <p>
         * Create the action that will be executed when user pressed the
         * <i>cancel</i> button.
         * </p>
         *
         * @return action that will be executed when user pressed the
         *         <i>cancel</i>
         *         button
         */
        private ActionListener getCancelAction() {
            // Cancel button action
            return new ActionListener() {

                /*
                 * @see
                 * java.awt.event.ActionListener#actionPerformed(java.awt.event
                 * .ActionEvent)
                 */
                public void actionPerformed(ActionEvent e) {
                    closeThis();
                }
            };
        }
    }

    /**
     * <p>
     * Closes this window.
     * </p>
     */
    private void closeThis() {
        PluginServices.getMDIManager().closeWindow(this);
    }

    /**
     *
     *
     *
     * @author Pablo Piqueras Bartolom� (pablo.piqueras@iver.es)
     */
    private class SideInfo {

        private byte side;
        private String name;

        public SideInfo(byte side, String name) {
            this.side = side;
            this.name = name;
        }

        public String toString() {
            return name;
        }

        public String getName() {
            return name;
        }

        public byte getSide() {
            return side;
        }
    }

    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }
}
