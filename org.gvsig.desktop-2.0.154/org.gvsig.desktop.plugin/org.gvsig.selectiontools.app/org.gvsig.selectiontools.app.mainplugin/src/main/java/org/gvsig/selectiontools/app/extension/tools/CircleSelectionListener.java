/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension.tools;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.CircleListener;

/**
 * <p>
 * Listener that selects all features of the active, available and vector layers
 * which intersect with the defined circle area in the associated
 * {@link MapControl MapControl} object.
 * </p>
 *
 * @author Pablo Piqueras Bartolomé (pablo.piqueras@iver.es)
 */
public class CircleSelectionListener implements CircleListener {



    /**
     * The cursor used to work with this tool listener.
     *
     * @see #getCursor()
     */
    private Cursor cur = null ;

    /**
     * Reference to the <code>MapControl</code> object that uses.
     */
    private MapControl mapCtrl;

    /**
     * <p>
     * Creates a new listener for selecting circular areas.
     * </p>
     *
     * @param mc
     *            the <code>MapControl</code> object where the measures are made
     */
    public CircleSelectionListener(MapControl mc) {
        this.mapCtrl = mc;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.iver.cit.gvsig.fmap.tools.Listeners.CircleListener#circle(com.iver
     * .cit.gvsig.fmap.tools.Events.MeasureEvent)
     */
    public void circle(MeasureEvent event) throws BehaviorException {
        if (event.getEvent().getID() == MouseEvent.MOUSE_RELEASED) {
            FLayer[] activeLayers =
                mapCtrl.getMapContext().getLayers().getActives();

            FLayer layer;
            FLyrVect lyrVect;

            Geometry geom = null;
            GeometryManager manager = GeometryLocator.getGeometryManager();
            Point center;
            try {
                center = (Point)manager.create(TYPES.POINT, SUBTYPES.GEOM2D);
                center.setX(event.getXs()[0].doubleValue());
                center.setY(event.getYs()[0].doubleValue());

                Point point2;
                point2 = (Point)manager.create(TYPES.POINT, SUBTYPES.GEOM2D);
                point2.setX(event.getXs()[1].doubleValue());
                point2.setY(event.getYs()[1].doubleValue());

                double radious = center.distance(point2);

                Circle circle = null;
                circle = (Circle) manager.create(TYPES.CIRCLE, SUBTYPES.GEOM2D);
                circle.setPoints(center, radious);
                geom = circle;
            } catch (CreateGeometryException e) {
                NotificationManager.showMessageError(PluginServices.getText(null,
                "Failed_creating_geometry"),
                e);
            } catch (GeometryOperationNotSupportedException e) {
                NotificationManager.showMessageError(PluginServices.getText(null,
                "Operation_not_supported"),
                e);
            } catch (GeometryOperationException e) {
                NotificationManager.showMessageError(PluginServices.getText(null,
                "Failed_performing_the_operation"),
                e);
            }
            if (geom == null)
                return;

            double flatness;

            // Creates the geometry
            // If the scale is < 500 -> approximates the circle with a polyline
            // with more points, as many as
            // smaller would be the scale
            if (mapCtrl.getMapContext().getScaleView() < 500) {
                GeneralPathX gP = new GeneralPathX();
                flatness = manager.getFlatness(); // ?????
                flatness =
                    mapCtrl.getMapContext().getScaleView() * flatness
                    / (500 * 2); // The number 2 forces to create the double
                // of points
                gP.append(geom.getPathIterator(null, flatness), true);
                try {
                    geom = manager.createSurface(gP, SUBTYPES.GEOM2D);
                } catch (CreateGeometryException e) {
                    NotificationManager.showMessageError(PluginServices.getText(null,
                    "Failed_creating_geometry"),
                    e);
                }
            } else {
                // Bigger scale -> Smaller flatness
                GeneralPathX gP = new GeneralPathX();
                flatness = manager.getFlatness(); // ?????
                flatness =
                    flatness / (mapCtrl.getMapContext().getScaleView() * 2);
                // *2 to reduce the number of lines of the polygon

                gP.append(geom.getPathIterator(null, flatness), true);
                try {
                    geom = manager.createSurface(gP, SUBTYPES.GEOM2D);
                } catch (CreateGeometryException e) {
                    NotificationManager.showMessageError(PluginServices.getText(null,
                    "Failed_creating_geometry"),
                    e);
                }
            }

            for (int i = 0; i < activeLayers.length; i++) {
                layer = activeLayers[i];

                if ((layer.isAvailable()) && (layer instanceof FLyrVect)) {
                    lyrVect = (FLyrVect) layer;
                    FeatureSet newSelection = null;

                    try {
                        newSelection =
                            lyrVect.queryByGeometry(geom,
                                lyrVect.getFeatureStore()
                                .getDefaultFeatureType());

                        if (event.getEvent().isControlDown()) {
                            ((FeatureSelection) lyrVect.getDataStore()
                                .getSelection()).select(newSelection);
                        } else {
                            lyrVect.getFeatureStore()
                            .setSelection(newSelection);
                        }

                    } catch (DataException e) {
                        NotificationManager.showMessageError(PluginServices.getText(null,
                            "Failed_selecting_geometries"),
                            e);
                    } finally {
                        newSelection.dispose();
                    }
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getCursor()
     */
	public Cursor getCursor() {
		if (cur == null) {
			cur = Toolkit.getDefaultToolkit().createCustomCursor(
					this.getImageCursor(), new java.awt.Point(16, 16), "");
		}
		return cur;
	}

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
     */
    public boolean cancelDrawing() {
        return false;
    }

    public Image getImageCursor() {
        return IconThemeHelper.getImage("cursor-select-by-circle");
    }
}
