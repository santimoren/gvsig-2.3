/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension.tools;

/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.ImageIcon;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.BehaviorException;
import org.gvsig.fmap.mapcontrol.tools.Events.MeasureEvent;
import org.gvsig.fmap.mapcontrol.tools.Listeners.PolylineListener;

/**
 * <p>
 * Listener that selects all features of the active and vector layers which
 * intersect with the defined polyline in the associated {@link MapControl
 * MapControl} object.
 * </p>
 * 
 * <p>
 * The selection will be produced after user finishes the creation of the
 * polyline.
 * </p>
 * 
 * @author Pablo Piqueras Bartolomé (pablo.piqueras@iver.es)
 */
public class PolyLineSelectionListener implements PolylineListener {

    /**
     * The image to display when the cursor is active.
     */
//    private final Image img = PluginServices.getIconTheme().get("cursor-select-by-polygon").getImage();

    /**
     * The cursor used to work with this tool listener.
     * 
     * @see #getCursor()
     */
    private Cursor cur = null;

    /**
     * Reference to the <code>MapControl</code> object that uses.
     */
    private MapControl mapCtrl;

    /**
     * <p>
     * Creates a new <code>PolygonSelectionListener</code> object.
     * </p>
     * 
     * @param mc
     *            the <code>MapControl</code> where is drawn the polyline
     */
    public PolyLineSelectionListener(MapControl mc) {
        this.mapCtrl = mc;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getCursor()
     */
    public Cursor getCursor() {
    	if( cur == null ) {
    		cur = Toolkit.getDefaultToolkit().createCustomCursor(this.getImageCursor(),
    		        new Point(16, 16),
    		        "");
    	}
        return cur;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
     */
    public boolean cancelDrawing() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#points(com.iver
     * .cit.gvsig.fmap.tools.Events.MeasureEvent)
     */
    public void points(MeasureEvent event) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#pointFixed(com
     * .iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
     */
    public void pointFixed(MeasureEvent event) throws BehaviorException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.iver.cit.gvsig.fmap.tools.Listeners.PolylineListener#polylineFinished
     * (com.iver.cit.gvsig.fmap.tools.Events.MeasureEvent)
     */
    public void polylineFinished(MeasureEvent event) throws BehaviorException {
        try {
            GeneralPathX gp = event.getGP();
            GeometryManager manager = GeometryLocator.getGeometryManager();
            Geometry geom = null;
            Surface surface = null;
            try {
                surface =
                    (Surface) manager.create(TYPES.SURFACE, SUBTYPES.GEOM2D);
                surface.setGeneralPath(gp);
                geom = surface;
            } catch (CreateGeometryException e1) {
                NotificationManager.showMessageError(PluginServices.getText(null,
                    "Failed_creating_geometry"),
                    e1);
            }
            if (geom == null)
                return;

            FLayer[] actives = mapCtrl.getMapContext().getLayers().getActives();

            for (int i = 0; i < actives.length; i++) {
                if (actives[i] instanceof FLyrVect) {
                    FLyrVect lyrVect = (FLyrVect) actives[i];
                    FeatureSet newSelection = null;

                    newSelection =
                        lyrVect.queryByGeometry(geom, lyrVect.getFeatureStore()
                            .getDefaultFeatureType());

                    if (event.getEvent().isControlDown()) {
                        ((FeatureSelection) lyrVect.getDataStore()
                            .getSelection()).select(newSelection);
                    } else {
                        lyrVect.getFeatureStore().setSelection(newSelection);
                    }
                }
            }
        } catch (com.vividsolutions.jts.geom.TopologyException topEx) {
            NotificationManager.showMessageError(PluginServices.getText(null,
                "Failed_selecting_geometries_by_polyline_topology_exception_explanation"),
                topEx);
        } catch (Exception ex) {
            NotificationManager.showMessageError(PluginServices.getText(null,
                "Failed_selecting_geometries"), ex);
        }
    }

    public Image getImageCursor() {
        return IconThemeHelper.getImage("cursor-select-by-polyline");
    }
}
