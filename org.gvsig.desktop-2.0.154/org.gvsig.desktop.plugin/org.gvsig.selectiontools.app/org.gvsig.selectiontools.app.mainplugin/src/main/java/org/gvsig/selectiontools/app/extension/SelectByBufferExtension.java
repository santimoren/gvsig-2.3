/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.selectiontools.app.extension.tools.buffer.gui.BufferConfigurationPanel;

/**
 * Extension to add support for selecting the geometries of the active vector
 * layers that intersect with a buffer around their previously selected
 * geometries.
 */
public class SelectByBufferExtension extends Extension {

    private static Logger logger
            = LoggerFactory.getLogger(SelectByBufferExtension.class);
    public static final String BUFFER_SELECTION_TOOL_NAME = "bufferSelection";

    public void initialize() {
        IconThemeHelper.registerIcon("action", "selection-select-by-buffer", this);
        IconThemeHelper.registerIcon("cursor", "cursor-select-by-buffer", this);
    }

    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        ViewDocument document = view.getViewDocument();

        if (actionCommand.equals("SELBUFFER")) {
            MapContext mapContext = document.getMapContext();

            FLayer layers[] = mapContext.getLayers().getActives();
            FLayer layer;
            List<FLyrVect> usefulLayers = new ArrayList<FLyrVect>();
            int emptySelectionLayers = 0;

            for (int i = 0; i < layers.length; i++) {
                layer = layers[i];
                if ((layer instanceof FLyrVect) && layer.isAvailable() && layer.isActive()) {
                    FLyrVect layervect = (FLyrVect) layer;
                    usefulLayers.add(layervect);
                    try {
                        if (layervect.getFeatureStore().getFeatureSelection().isEmpty()) {
                            emptySelectionLayers++;
                        }
                    } catch (DataException e) {
                        logger.warn("Error While getting selection for layer '" + layer.getName() + "'.", e);
                        application.messageDialog(
                                application.translate("Failed_selecting_layer") + ": " + layer.getName(),
                                application.translate("Warning"),
                                JOptionPane.WARNING_MESSAGE);
                    }
                }
            }

            if (usefulLayers.isEmpty() || emptySelectionLayers == usefulLayers.size()) {
                application.messageDialog(
                        application.translate("_There_are_no_geometries_selected"),
                        application.translate("Warning"),
                        JOptionPane.WARNING_MESSAGE);
                return;
            }

            // Creates and displays the configuration panel
            application.getUIManager().addWindow(
                    new BufferConfigurationPanel(usefulLayers, view));
        }

    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();

        FLayer layers[] = mapa.getLayers().getActives();
        FLayer layer;
        for (int i = 0; i < layers.length; i++) {
            layer = layers[i];
            if ((layer instanceof FLyrVect) && layer.isAvailable() && layer.isActive()) {
                FLyrVect layervect = (FLyrVect) layer;
                try {
                    if (!layervect.getFeatureStore().getFeatureSelection().isEmpty()) {
                        return true;
                    }
                } catch (DataException e) {
                    logger.warn("Error While getting selection for layer '" + layer.getName() + "'.", e);
                }
            }
        }
        return false;
    }
}
