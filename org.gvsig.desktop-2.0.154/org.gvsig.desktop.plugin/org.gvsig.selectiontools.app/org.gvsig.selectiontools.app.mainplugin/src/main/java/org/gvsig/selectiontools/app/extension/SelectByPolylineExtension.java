/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.selectiontools.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.app.project.documents.view.toolListeners.StatusBarListener;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Behavior.Behavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.MouseMovementBehavior;
import org.gvsig.fmap.mapcontrol.tools.Behavior.PolylineBehavior;
import org.gvsig.selectiontools.app.extension.tools.PolyLineSelectListener;

/**
 * Extension to add support for selecting the geometries of the active vector
 * layers that intersect with a polyline defined by the user.
 */
public class SelectByPolylineExtension extends Extension {

    public static final String POLYLINE_SELECTION_TOOL_NAME
            = "polylineSelection";

    public void initialize() {
        IconThemeHelper.registerIcon("action", "selection-select-by-polyline", this);
        IconThemeHelper.registerIcon("cursor", "cursor-select-by-polyline", this);
    }

    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return;
        }
        if (actionCommand.equals("SELPOLYLINE")) {
            MapControl mc = view.getMapControl();
            /*
             If current's view MapControl doesn't have the
             "polylineSelection" tool, adds it
             */
            if (!mc.getNamesMapTools().containsKey(POLYLINE_SELECTION_TOOL_NAME)) {
                PolyLineSelectListener polylineSelListener = new PolyLineSelectListener(mc);
                mc.addBehavior(
                        POLYLINE_SELECTION_TOOL_NAME,
                        new Behavior[]{
                            new PolylineBehavior(polylineSelListener),
                            new MouseMovementBehavior(new StatusBarListener(mc))
                        });
            }
            mc.setTool(POLYLINE_SELECTION_TOOL_NAME);
        }
    }

    public boolean isVisible() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        MapContext mapa = document.getMapContext();
        return mapa.getLayers().getLayersCount() > 0;
    }

    public boolean isEnabled() {
        ApplicationManager application = ApplicationLocator.getManager();

        IView view = (IView) application.getActiveComponent(ViewDocument.class);
        if (view == null) {
            return false;
        }
        ViewDocument document = view.getViewDocument();
        return document.getMapContext().hasActiveVectorLayers();
    }
}
