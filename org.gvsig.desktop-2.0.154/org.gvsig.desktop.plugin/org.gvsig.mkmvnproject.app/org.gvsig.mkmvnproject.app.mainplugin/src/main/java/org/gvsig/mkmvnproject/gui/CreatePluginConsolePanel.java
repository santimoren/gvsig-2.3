/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.mkmvnproject.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A panel which shows the execution log messages of the create plugin process.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class CreatePluginConsolePanel extends JPanel {

	private static final long serialVersionUID = -7213048219847956909L;

	private static final Logger logger = LoggerFactory
			.getLogger(CreatePluginConsolePanel.class);

	private JTextArea console;

	private PrintStream printStream;
	private PipedOutputStream pipedos;
	private PipedInputStream pipedis;
	private Thread consoleThread;

	/**
	 * Constructor.
	 * 
	 * @throws IOException
	 *             if there is an error creating the console streams
	 */
	public CreatePluginConsolePanel() throws IOException {
		super();
		initialize();
	}

	/**
	 * Constructor.
	 * 
	 * @param isDoubleBuffered
	 *            if the panel must be double buffered.
	 * @throws IOException
	 *             if there is an error creating the console streams
	 */
	public CreatePluginConsolePanel(boolean isDoubleBuffered)
			throws IOException {
		super(isDoubleBuffered);
		initialize();
	}

	/**
	 * Initializes the panel GUI.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		Dimension dimension = new Dimension(600, 200);
		setSize(dimension);
		setLayout(new BorderLayout());

		console = new JTextArea();
		console.setEditable(false);
		// console.setColumns(20);
		// console.setRows(5);
		console.setBackground(Color.WHITE);
		console.setLineWrap(true);
		console.setWrapStyleWord(true);

		JScrollPane consoleScrollPane = new JScrollPane(console);
		add(consoleScrollPane, BorderLayout.CENTER);

		pipedis = new PipedInputStream();

		pipedos = new PipedOutputStream(pipedis);

		printStream = new PrintStream(pipedos);
		
        logger.info("===================================");
        logger.info("= Opening plugin creation console =");
        logger.info("===================================");

		consoleThread = new ConsoleThread(pipedis, console);
		consoleThread.start();
	}

	/**
	 * Returns a {@link PrintStream} which allows to write messages to the
	 * console.
	 * 
	 * @return a {@link PrintStream} which allows to write messages to the
	 *         console
	 */
	public PrintStream getPrintStream() {
		return printStream;
	}

	/**
	 * Returns a {@link PrintStream} which allows to write error messages to the
	 * console.
	 * 
	 * @return a {@link PrintStream} which allows to write error messages to the
	 *         console
	 */
	public PrintStream getErrorPrintStream() {
		return getPrintStream();
	}

	/**
	 * Closes the console. Once this method is called, any more calls to the
	 * console {@link PrintStream} will throw an exception.
	 */
	public void closeConsole() {
	    
        logger.info("===================================");
        logger.info("= Closing plugin creation console =");
        logger.info("===================================");
	    
		printStream.flush();
		printStream.close();
		try {
			pipedos.close();
			pipedis.close();
			// Wait for the thread to finish
			consoleThread.join(500);
		} catch (IOException e) {
		    logger.warn("Error closing the internal piped streams", e);
		} catch (InterruptedException e) {
			// Nothing special to do
		    logger.debug("Console thread interrupted while waiting to finish", e);
		}
	}

	private static final class ConsoleThread extends Thread {

		private final PipedInputStream pipedis;
		private final JTextArea console;

		/**
		 * Constructor.
		 */
		public ConsoleThread(PipedInputStream pipedis, JTextArea console) {
			super("Create plugin console update thread");
			setDaemon(true);
			this.pipedis = pipedis;
			this.console = console;
		}

		@Override
		public void run() {
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(pipedis));
				String line;
				while ((line = reader.readLine()) != null) {
				    // =========== Writing lines to log too 
		            logger.info(line);
                    // =====================================
					SwingUtilities.invokeLater(new ConsoleUpdateRunnable(
							console, line));
				}
				logger.debug("Console input stream end, finish reading");
				reader.close();
			} catch (IOException e) {
			    logger.info("Write end dead.");
			}
		}

	}

	/**
	 * Runnable used to update the console text field.
	 * 
	 * @author gvSIG Team
	 * @version $Id$
	 */
	private static final class ConsoleUpdateRunnable implements Runnable {
		private final String newLine;
		private final JTextArea console;

		/**
		 * Constructor.
		 */
		public ConsoleUpdateRunnable(JTextArea console, String newLine) {
			this.console = console;
			this.newLine = newLine;
		}

		public void run() {
			console.append(newLine);
			console.append("\n");
		}
	}
}
