/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.mkmvnproject;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JOptionPane;

import org.apache.tools.ant.BuildListener;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.i18n.Messages;
import org.gvsig.mkmvnproject.gui.CreatePluginConsoleWindow;
import org.gvsig.mkmvnproject.gui.settings.PlatformPropertiesWindow;

/**
 * Extension to launch the project creation from templates.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public class MakeMavenProjectExtension extends Extension {

    private static final String ANT_BUILD_FILE = "mkmvnproject.xml";
    private static final String ANT_CHECK_JDK = "checkjdk.xml";

	private static final String ANT_TARGET = "mkproject";
	
	public static final String GVSIG_PLATFORM_PROPERTIES_FILE_NAME =
	    ".gvsig.platform.properties";

	private static final Logger LOG = LoggerFactory
			.getLogger(MakeMavenProjectExtension.class);

	private boolean enabled = true;

	private final Object lock = new Object();

	public void execute(String actionCommand) {
	    
	    /*
	     * Problem is that for some reason  dummy file cannot be
	     * compiled, but this plugin works (?)
	     * 
	    if (!checkJDK()) {
	        return;
	    }
	    */

//	    if (!checkPlatformProperties()) {
//	        return;
//	    }
	    
        // ==========================================================
		// TODO: add support to parallel executions in Andami??

        // Create messages console
	    CreatePluginConsoleWindow console = null;

        if (console == null) {
            try {
                console = new CreatePluginConsoleWindow();
            } catch (IOException e) {
                NotificationManager.addWarning("Error creating the console", e);
            }
        }
        if (console != null) {
            PluginServices.getMDIManager().addWindow(console, GridBagConstraints.LAST_LINE_START);
        }

		try {
			// Disable extension so it can't be executed again.
			synchronized (lock) {
				enabled = false;
			}
			new CreatePluginThread(console).start();
		} finally {
			synchronized (lock) {
				// create plugin process finish. Enable extension.
				enabled = true;
			}
		}
	}

	/**
	 * Checks existence of file:
	 * 
	 * USER_HOME/.gvsig.platform.properties
	 * 
	 * If not present, show text area with suggested content.
	 * 
     * @return whether the mentioned file is present or the
     * user has created it here and accepts to continue.
     *  
     */
    private boolean checkPlatformProperties() {
        
        String user_home = System.getProperty("user.home");
        File the_file = new File(
            user_home +
            File.separator +
            GVSIG_PLATFORM_PROPERTIES_FILE_NAME);

        Component parent_window = getActiveWindowComponent();

        try {
            if (the_file.exists()) {
                // ok file exists
                LOG.info("Found platform properties file: "
                    + the_file.getAbsolutePath());
                return true;
            }
            
            boolean is_windows = isThisWindows();
            PlatformPropertiesWindow ppw = new PlatformPropertiesWindow(
                the_file, is_windows);
            PluginServices.getMDIManager().addCentredWindow(ppw);
            // it's a modal dialog
            return ppw.validSettings();
            
        } catch (Exception ex) {
            
            String _msg =
                Messages.getText("_Error_while_checking_platform_properties")
                + "\n\n[ " + ex.getMessage() + " ]";
            String _tit = Messages.getText(
                "_Checking_platform_properties");
            JOptionPane.showMessageDialog(
                parent_window,
                _msg, _tit,
                JOptionPane.ERROR_MESSAGE);
        }
        return false;
        
    }


    /**
     * @return whether we are running on a windows system
     */
    private boolean isThisWindows() {
        String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("win") >= 0);
    }

    /**
     * Check and possibly show JDK and JAVA_HOME settings and
     * prompt user with suggestion.
     * 
     * @return whether JDK is detected or user accepts to
     * continue anyway
     */
    private boolean checkJDK() {
        
        boolean this_is_jdk = false;
        try {
            this_is_jdk = jdkAvailable();
        } catch (Exception ex) {
            LOG.info("While searching for JDK : " + ex.getMessage(), ex);
            this_is_jdk = false;
        }
        
        if (this_is_jdk) {
            return true;
        }
        
        String env_j_h = System.getenv("JAVA_HOME");
        
        String jdk_str = null;
        if (this_is_jdk) {
            jdk_str = Messages.getText("_gvSIG_is_running_on_JDK");
        } else {
            jdk_str = Messages.getText("_gvSIG_is_not_running_on_JDK");
        }
        
        String jh_str = null;
        if (env_j_h == null) {
            jh_str = Messages.getText("_JAVA_HOME_is_not_set");
        } else {
            jh_str = Messages.getText("_JAVA_HOME_is_set_to")
                + " '" + env_j_h + "'";
        }
        
        String recomm = "";
        if (true) { // !this_is_jdk && env_j_h == null) {
            recomm = "\n\n"
                + Messages.getText("_Requirements_for_this_plugin") + ":\n\n"
                + "- " + Messages.getText("_On_Linux_make_JAVA_HOME_point_to_JDK_1_5_higher");
            recomm = recomm + ".    \n- "
            + Messages.getText("_On_Windows_run_JDK_use_devel_exe") + ".";
        }
        
        String tot_msg = Messages.getText("_Detected_settings") + ":"
            + "\n\n"
            + "- " + jdk_str + "\n"
            + "- " + jh_str + "     " + recomm + "\n\n"
                + Messages.getText("_Continue_question");
            
        int opt = JOptionPane.showConfirmDialog(
            null, // getActiveWindowComponent(),
            tot_msg,
            Messages.getText("_Make_maven_project_plugin") + " - " + 
            Messages.getText("_Searching_JDK"),
            JOptionPane.YES_NO_OPTION,
            (recomm.length() == 0) ?
                JOptionPane.INFORMATION_MESSAGE : JOptionPane.WARNING_MESSAGE
            );
        
        return (opt == JOptionPane.YES_OPTION);
    }

    /**
     * @return
     */
    private Component getActiveWindowComponent() {
        
        IWindow iw = PluginServices.getMDIManager().getActiveWindow();
        if (iw instanceof Component) {
            return (Component) iw;
        } else {
            return null;
        }
    }


    private boolean jdkAvailable() throws Exception {
        
        ClassLoader loader = this.getClass().getClassLoader();
        URL class_file_url = loader.getResource("compilationcheck/DummyClass.class");
        File class_file = null;
        
        // deleting compiled test file if existed
        if (class_file_url != null) {
            class_file = new File(class_file_url.getFile());
            if (class_file.exists()) {
                class_file.delete();
            }
        }
        
        URL build = loader.getResource("scripts/" + ANT_CHECK_JDK);

        String execut = null;
        /*
        // first attempt: javac from JAVA_HOME
        execut =
            System.getenv().get("JAVA_HOME")
            + File.separator + "bin" + File.separator + "javac";
        
        if (execut != null) {
            this.runScript(build, "default", null, "executable", execut);
            class_file_url = loader.getResource("compilationcheck/DummyClass.class");
            if (class_file_url != null) {
                // OK, test file is not compiled
                return true;
            }
        }
        */
        
        // second attempt: java.home (jre)
        execut = System.getProperty("java.home");
        int ind = execut.lastIndexOf(File.separator);
        // one up, then bin/javac
        execut = execut.substring(0, ind)
            + File.separator
            + "bin" + File.separator + "javac";
        
        this.runScript(build, "default", null, "executable", execut);
        class_file_url = loader.getResource("compilationcheck/DummyClass.class");
        // if test file not compiled, then false
        return (class_file_url != null);
    }

    /**
	 * Thread to launch the ant base plugin creation project.
	 * 
	 * @author gvSIG Team
	 * @version $Id$
	 */
	private final class CreatePluginThread extends Thread {

		private final CreatePluginConsoleWindow console;

		/**
		 * Constructor.
		 * 
		 * @param console
		 */
		public CreatePluginThread(CreatePluginConsoleWindow console) {
			this.console = console;
			setName("Create plugin execution thread");
			setDaemon(true);
		}

		@Override
		public void run() {
			DefaultLogger log = new DefaultLogger();
			log.setMessageOutputLevel(Project.MSG_INFO);

			if (console != null) {
				log.setErrorPrintStream(console.getErrorPrintStream());
				log.setOutputPrintStream(console.getPrintStream());
			} else {
				LOG.warn("Console window not available, will use the default console for Ant");
				log.setErrorPrintStream(System.err);
				log.setOutputPrintStream(System.out);
			}

			
			
            ClassLoader loader = this.getClass().getClassLoader();
            URL build = loader.getResource("scripts/" + ANT_BUILD_FILE);
            runScript(build, ANT_TARGET, log);
		}
	}


	private void runScript(
	        URL build,
	        String target,
	        BuildListener blistener) {
	    
        runScript(build, target, blistener, null, null);
	    
	}

	private void runScript(
	    URL build,
	    String target,
	    BuildListener blistener,
	    String prop, String val) {
	    
	    LOG.info("Starting runScript:\n"
	        + "    build = " + build.toString() + "\n"
	        + "    property name = " + prop + "\n"
            + "    property value = " + val);
	        

        File file = new File(build.getFile());

        final Project ant = new Project();
        if (blistener != null) {
            ant.addBuildListener(blistener);
        }
        ant.setUserProperty("ant.file", file.getAbsolutePath());
        
        if (prop != null && val != null) {
            ant.setUserProperty(prop, val);
        }
        
        ant.init();
        ProjectHelper.getProjectHelper().parse(ant, file);

        LOG.info("Starting ant task with the file {} and the target {}",
                file, target);
        ant.executeTarget(target);
	}

	public void initialize() {
		// Nothing to do
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isVisible() {
		return true;
	}
	

}