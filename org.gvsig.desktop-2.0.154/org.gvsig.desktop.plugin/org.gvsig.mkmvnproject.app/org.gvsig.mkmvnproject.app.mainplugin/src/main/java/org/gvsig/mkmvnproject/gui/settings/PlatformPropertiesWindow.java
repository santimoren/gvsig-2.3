/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.mkmvnproject.gui.settings;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.i18n.Messages;


/**
 * This dialog lets the user write the file .gvsig.platform.properties
 * 
 * @author jldominguez
 *
 */
public class PlatformPropertiesWindow extends JPanel
implements IWindow, ActionListener, DocumentListener {
    
    private static final Logger logger =
        LoggerFactory.getLogger(PlatformPropertiesWindow.class);
    
    private WindowInfo winfo = null;
    private File targetFile = null;
    private boolean isWindows = false;
    
    private JPanel northPanel = null;
    
    private JTextArea contentArea = null;
    
    private JPanel southPanel = null;
    private JButton saveButton = null;
    private JButton suggButton = null;
    private JButton okButton = null;
    private JButton cancelButton = null;
    
    private boolean validSettings = false; 
    
    public PlatformPropertiesWindow(
        File non_existing_file, boolean is_win) {
        
        targetFile = non_existing_file;
        isWindows = is_win;
        init();
    }

    private void init() {
        
        this.setLayout(new BorderLayout());
        
        northPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();  
        c.insets = new Insets(1, 10, 1, 10);
        c.weightx = 0.5; // 
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        
        c.gridy = 0;
        northPanel.add(new JLabel(" "), c);
        c.gridy = 1;
        northPanel.add(new JLabel(Messages.getText(
            "_Mandatory_platform_properties_file_not_found")), c);
        c.gridy = 2;
        northPanel.add(new JLabel(Messages.getText(
            "_Use_this_text_editor_to_write_it")), c);
        c.gridy = 3;
        northPanel.add(new JLabel(" "), c);
        // =============================================
        // =============================================
        southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1, 5, 8, 4));
        // ------------
        southPanel.add(getSaveButton());
        southPanel.add(getSuggButton());
        // ------------
        southPanel.add(new JLabel(" "));
        // ------------
        southPanel.add(getOkButton());
        southPanel.add(getCancelButton());
        // =============================================
        
        this.add(northPanel, BorderLayout.NORTH);
        
        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(getContentArea());
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.add(scroll, BorderLayout.CENTER);
        
        JPanel south2 = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.insets = new Insets(15,15,15,15);
        south2.add(southPanel, cs);
        
        this.add(south2, BorderLayout.SOUTH);
    }

    private JTextArea getContentArea() {
        
        if (contentArea == null) {
            contentArea = new JTextArea();
            loadSuggested(contentArea);
            contentArea.getDocument().addDocumentListener(this);
        }
        return contentArea;
    }

    private JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton(Messages.getText("_Cancel"));
            cancelButton.addActionListener(this);
        }
        return cancelButton;
    }

    private JButton getSuggButton() {
        if (suggButton == null) {
            suggButton = new JButton(Messages.getText("_Suggested"));
            suggButton.addActionListener(this);
        }
        return suggButton;
    }

    private JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton(Messages.getText("_Accept"));
            okButton.addActionListener(this);
            okButton.setEnabled(false);
        }
        return okButton;
    }

    private JButton getSaveButton() {
        if (saveButton == null) {
            saveButton = new JButton(Messages.getText("_Save"));
            Font fnt = saveButton.getFont();
            fnt = fnt.deriveFont(Font.BOLD);
            saveButton.setFont(fnt);
            saveButton.addActionListener(this);
        }
        return saveButton;
    }

    // ===================================================
    // ===================================================

    public WindowInfo getWindowInfo() {
        if (winfo == null) {
            winfo = new WindowInfo(WindowInfo.MODALDIALOG);
            winfo.setWidth(550);
            winfo.setHeight(350);
            winfo.setTitle(
                Messages.getText("_Checking_platform_properties"));
        }
        return winfo;
    }

    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }
    
    public static final String SUGGESTED_WINDOWN_CONTENT =
        "native_platform=win\n" +
        "native_distribution=nt\n" +
        "native_compiler=vs8\n" +
        "native_arch=i386\n" +
        "native_libraryType=dynamic";
    
    public static final String SUGGESTED_NON_WINDOWS_CONTENT =
            "native_platform=linux\n" +
            "native_distribution=all\n" +
            "native_compiler=gcc4\n" +
            "native_arch=i386\n" +
            "native_libraryType=dynamic";


    public void actionPerformed(ActionEvent e) {
        
        Object src = e.getSource();
        if (src == getOkButton()) {
            if (getSaveButton().isEnabled()) {
                if (!userAcceptsNotSaving()) {
                    return;
                }
            }
            validSettings = true;
            PluginServices.getMDIManager().closeWindow(this);
        }
        
        if (src == getCancelButton()) {
            validSettings = false;
            PluginServices.getMDIManager().closeWindow(this);
        }
        
        if (src == getSaveButton()) {
            if (saveContent()) {
                this.getSaveButton().setEnabled(false);
            }
            this.refreshOkButton();
        }
        
        if (src == getSuggButton()) {
            loadSuggested(getContentArea());
            this.getSaveButton().setEnabled(true);
        }
        
    }


    /**
     * @return
     */
    private boolean userAcceptsNotSaving() {
        
        String _msg =
            Messages.getText("_There_are_unsaved_changes_Continue_question");
        String _tit =
            Messages.getText("_Not_saved");
        int usr_opt = JOptionPane.showConfirmDialog(
            this, _msg, _tit, JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE);
        return (usr_opt == JOptionPane.YES_OPTION);
    }

    private boolean saveContent() {
        
        try {
            if (targetFile.exists()) {
                targetFile.delete();
            }
            targetFile.createNewFile();
            
            FileOutputStream fos = new FileOutputStream(targetFile);
            String contnt = getContentArea().getText();
            fos.write(contnt.getBytes());
            fos.flush();
            fos.close();
            return true;
        } catch (IOException ioe) {
            logger.info("While writing file: " + ioe.getMessage(), ioe);
            String _msg = Messages.getText("_Unable_to_store_file")
                + ": " + targetFile.getAbsolutePath();
            String _tit = Messages.getText("_Error");
            JOptionPane.showMessageDialog(
                this, _msg, _tit, JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
    }

    private void loadSuggested(JTextArea ta) {

        if (isWindows) {
            ta.setText(SUGGESTED_WINDOWN_CONTENT);
        } else {
            ta.setText(SUGGESTED_NON_WINDOWS_CONTENT);
        }
    }

    public void insertUpdate(DocumentEvent e) {
        this.getSaveButton().setEnabled(true);
    }

    public void removeUpdate(DocumentEvent e) {
        this.getSaveButton().setEnabled(true);
    }

    public void changedUpdate(DocumentEvent e) {
        // should not be called because this text area is simple
    }
    
    public boolean validSettings() {
        return validSettings;
    }
    
    private void refreshOkButton() {
        this.getOkButton().setEnabled(targetFile.exists());
    }

}
