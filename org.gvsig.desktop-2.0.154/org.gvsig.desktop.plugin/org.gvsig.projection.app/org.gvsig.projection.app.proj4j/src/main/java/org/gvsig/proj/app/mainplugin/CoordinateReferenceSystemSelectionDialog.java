/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2012 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.proj.app.mainplugin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;

import javax.swing.JPanel;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;
import org.gvsig.proj.CoordinateReferenceSystem;
import org.gvsig.proj.cts.DefaultIProjection;
import org.gvsig.proj.swing.CoordinateReferenceSystemSelectorComponent;
import org.gvsig.proj.swing.CoordinateReferenceSystemSwingLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.Dialog;
import org.gvsig.tools.swing.api.windowmanager.WindowManager_v2;

/**
 * ISelectCrsPanel implementation based on the org.gvsig.proj library.
 * 
 * @author gvSIG Team
 */
public class CoordinateReferenceSystemSelectionDialog extends JPanel implements
    IWindow, ISelectCrsPanel {

    private static final long serialVersionUID = 810773451033764544L;
    private boolean okPressed = false;
    private IProjection lastProjection = null;
    private CoordinateReferenceSystemSelectorComponent component;

    /**
     * Constructor.
     */
    public CoordinateReferenceSystemSelectionDialog() {
        super();
        this.setLayout(new BorderLayout());

        I18nManager i18nManager = ToolsLocator.getI18nManager();
        WindowManager_v2 windowManager = (WindowManager_v2) ToolsSwingLocator.getWindowManager();
        final Dialog dialogPanel = windowManager.createDialog(
                getContentPanel(),
                i18nManager.getTranslation("selecciona_sistema_de_referencia"),
                i18nManager.getTranslation("reference_system"), 
                WindowManager_v2.BUTTONS_OK_CANCEL
        );
                
        add(dialogPanel.asJComponent(), BorderLayout.CENTER);
        dialogPanel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                switch( dialogPanel.getAction()) {
                    case WindowManager_v2.BUTTON_OK:
                        okPressed = true;
                        break;
                    case WindowManager_v2.BUTTON_CANCEL:
                        setProjection(lastProjection);
                        okPressed = false;
                        break;
                }
                setVisible(false);
            }
        });
    }

    @Override
    public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo =
            new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
        m_viewinfo.setTitle(PluginServices.getText(this,
            "selecciona_sistema_de_referencia"));
        Dimension dim = getPreferredSize();
        m_viewinfo.setWidth(dim.width);
        m_viewinfo.setHeight(dim.height);
        return m_viewinfo;
    }

    @Override
    public boolean isOkPressed() {
        return okPressed;
    }

    /**
     * @return
     */
    @Override
    public IProjection getProjection() {
        CoordinateReferenceSystem crs =
            component.getSelectedCoordinateReferenceSystem();
        if (crs == null) {
            return lastProjection;
        }
        return new DefaultIProjection(crs);
    }

    protected JComponent getContentPanel() {
        if (component == null) {

            component =
                CoordinateReferenceSystemSwingLocator.getSwingManager()
                    .createCoordinateReferenceSystemSelectionComponent();
        }

        return component.asJComponent();
    }

    /**
     * @param proj
     */
    @Override
    public void setProjection(IProjection proj) {
        CoordinateReferenceSystem crs = null;
        lastProjection = proj;
        if( proj != null ) {
            crs = ((DefaultIProjection) proj).getCoordinateReferenceSystem();
        }
        component.setCoordinateReferenceSystem(crs);
    }

    @Override
    public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
    }
}
