/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2012 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.proj.app.mainplugin;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.gui.panels.CRSSelectPanelFactory;

/**
 * Extension to register the org.gvsid.proj facade implementation
 * of the org.gvsig.projection library.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class CoordinateReferenceSystemExtension extends Extension {

//    private static final String DEFAULT_PROJECTION_KEY_NAME =
//        "DefaultProjection";
//    private static final String FACTORY_DEFAULT_PROJECTION = "EPSG:23030";
//
    public void initialize() {
    	CRSSelectPanelFactory.registerUIFactory(CoordinateReferenceSystemSelectUIFactory.class);

        // Default Projection
//        PluginServices ps = PluginServices.getPluginServices("org.gvsig.app");
//        XMLEntity xml = ps.getPersistentXML();
//        String projCode = null;
//        if (xml.contains(DEFAULT_PROJECTION_KEY_NAME)) {
//            projCode = xml.getStringProperty(DEFAULT_PROJECTION_KEY_NAME);
//        } else {
//            projCode = FACTORY_DEFAULT_PROJECTION;
//        }
//        
//        ((ProjectPreferences) ApplicationLocator.getManager().getPreferences(
//            "project")).setDefaultProjection(CRSFactory.getCRS(projCode));
    }

    public void execute(String actionCommand) {
        // Nothing to do
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return false;
    }

}
