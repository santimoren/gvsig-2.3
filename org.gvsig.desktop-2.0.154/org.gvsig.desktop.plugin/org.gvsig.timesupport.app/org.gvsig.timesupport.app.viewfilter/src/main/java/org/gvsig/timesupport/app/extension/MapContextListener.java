/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.extension;

import org.gvsig.fmap.mapcontext.layers.CancelationException;
import org.gvsig.fmap.mapcontext.layers.LayerCollectionEvent;
import org.gvsig.fmap.mapcontext.layers.LayerPositionEvent;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanel;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class MapContextListener implements org.gvsig.fmap.mapcontext.layers.LayerCollectionListener{
    private final MapContextToInterval mapContextToInterval;
    private final TimeSelectorPanel timeSelectorPanel;

    public MapContextListener(MapContextToInterval mapContextToInterval, TimeSelectorPanel timeSelectorPanel) {
        super();
        this.mapContextToInterval = mapContextToInterval;
        this.timeSelectorPanel = timeSelectorPanel;
    }

    public void layerAdded(LayerCollectionEvent e) {
        mapContextUpdated();
    }

    public void layerMoved(LayerPositionEvent e) {
        // TODO Auto-generated method stub

    }

    public void layerRemoved(LayerCollectionEvent e) {
        mapContextUpdated();        
    }

    public void layerAdding(LayerCollectionEvent e) throws CancelationException {
        // TODO Auto-generated method stub

    }

    public void layerMoving(LayerPositionEvent e) throws CancelationException {
        // TODO Auto-generated method stub

    }

    public void layerRemoving(LayerCollectionEvent e)
    throws CancelationException {
        // TODO Auto-generated method stub

    }

    public void visibilityChanged(LayerCollectionEvent e)
    throws CancelationException {
        // TODO Auto-generated method stub

    }

    private void mapContextUpdated(){
        mapContextToInterval.update();   
        if (mapContextToInterval.getInterval() == null){
            timeSelectorPanel.setVisible(false);
        }else{
            timeSelectorPanel.setVisible(true);
            timeSelectorPanel.initialize(mapContextToInterval.getInterval());
            timeSelectorPanel.setInstants(mapContextToInterval.getTimes());  
        }
    }
}
