/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.extension;

import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanel;
import org.gvsig.timesupport.swing.api.panel.TimeSelectorPanelNotification;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class TimeSelectorObserver implements Observer{
    private ViewDocument viewDocument = null;
    private Time time = null;

    public TimeSelectorObserver(ViewDocument viewDocument) {
        super();
        this.viewDocument = viewDocument;
    }

    public void update(Observable observable, Object notification) {
        TimeSelectorPanel timeSliderPanel = (TimeSelectorPanel)observable;
        TimeSelectorPanelNotification timeSelectorPanelNotification = (TimeSelectorPanelNotification)notification;

        if (timeSelectorPanelNotification.getType().equals(TimeSelectorPanelNotification.UPDATE_TIME)){
            Time newTime = timeSelectorPanelNotification.getTime();
            if (time == null){
                time = newTime;
                filter();
            }else{
                if (!time.equals(newTime)){
                    time = newTime;
                    filter();
                }
            }  
        } else if (timeSelectorPanelNotification.getType().equals(TimeSelectorPanelNotification.CLOSE_WINDOW)){
            time = null;
            filter();
        }
    }

    private void filter(){
        this.viewDocument.getMapContext().getViewPort().setTime(time);
    }   
}
