/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.extension;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteIntervalTypeNotRegisteredException;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class MapContextToInterval {
    private static final Logger LOG = LoggerFactory.getLogger(MapContextToInterval.class);
    
    private static final TimeSupportManager TIME_SUPPORT_MANAGER = TimeSupportLocator.getManager();
    
    private MapContext mapContext;
    private Interval interval = null;
    private List<Time> times;

    public MapContextToInterval(MapContext mapContext) {
        super();
        this.mapContext = mapContext;        
        times = new ArrayList();
        update();
    }
    
    public Interval getInterval(){
        return interval;        
    }
    
    public List getTimes(){
        return times;
    }

    public MapContext getMapContext() {
        return mapContext;
    }
    
    
    public void update(){
        LayersIterator it = new LayersIterator(mapContext.getLayers());
        Instant minInstant = null;
        Instant maxInstant = null;
        times.clear();
        interval = null;

        //TODO Separate absolute and relative time
        while (it.hasNext()){
            FLayer layer = it.nextLayer();
            if (layer instanceof SingleLayer){
                DataStore dataStore = ((SingleLayer)layer).getDataStore();
                if (dataStore.getInterval() != null){                    
                    Instant startInstant = dataStore.getInterval().getStart();
                    Instant endInstant = dataStore.getInterval().getEnd();
                    times.addAll(dataStore.getTimes());
                    if (minInstant == null){
                        minInstant = startInstant;
                        maxInstant =  endInstant;
                    }else{
                        if (minInstant.isAfter(startInstant)){
                            minInstant = startInstant;
                        }
                        if (maxInstant.isBefore(endInstant))
                            maxInstant = endInstant;
                    }                   
                }
            }
        }
        
        if (minInstant != null){
            if (minInstant.isAbsolute()){ 
                try {
                    interval = TIME_SUPPORT_MANAGER.createAbsoluteInterval((AbsoluteInstant)minInstant, (AbsoluteInstant)maxInstant);
                } catch (AbsoluteIntervalTypeNotRegisteredException e) {
                   LOG.error("Error creating the time interval", e);
                }   
            }else{
                interval = TIME_SUPPORT_MANAGER.createRelativeInterval(((RelativeInstant)minInstant).toMillis(), ((RelativeInstant)maxInstant).toMillis());  
            }               
        }        
    }
    

}
