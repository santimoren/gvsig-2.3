/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.animation.extension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.AbstractViewPanel;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.TimeSupportManager;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.TimeSupportSwingLocator;
import org.gvsig.timesupport.swing.api.TimeSupportSwingManager;
import org.gvsig.timesupport.swing.api.panel.AnimationBarPanel;
import org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel;

/**
 * Listener for the preferences dialog
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 */
public class AnimationPreferencesListener implements ActionListener {
	private AnimationPreferencesPanel    prefs               = null;
	private AnimationDialog              prefsDialog         = null;
	private TimeSupportSwingManager      swingManager        = null;
	private TimeSupportManager           timeSupportManager  = null;
	private TimeAnimation                animation           = null;
	private ViewDocument            view                = null;
	
	/**
	 * Constructor
	 * @param dialog {@link AnimationDialog}
	 * @param animation {@link TimeAnimation}
	 * @param view {@link AbstractViewPanel}
	 */
	public AnimationPreferencesListener(AnimationDialog dialog, TimeAnimation animation, ViewDocument view) {
		this.prefsDialog = dialog;
		this.animation = animation;
		this.view = view;
		this.prefs = (AnimationPreferencesPanel)dialog.getMainComponent();
		swingManager = TimeSupportSwingLocator.getSwingManager();
		timeSupportManager = TimeSupportLocator.getManager();
	}
	
	 /*
     * (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == prefsDialog.getButtonAccept()) {
			if(this.prefsDialog != null) {
				TimeAnimationDataModel dataModel = prefs.getOutput();
				RelativeInstant endWindow = addWindowTimeToInitInterval(dataModel);
				dataModel.setTimeWindow(dataModel.getInitTime(), endWindow);
				AnimationBarPanel barPanel = swingManager.createAnimationBarPanel(dataModel);
				
				AnimationDialog barDialog = new AnimationDialog(
	        			(JComponent)barPanel, 
	        			600, 100, 
	        			PluginServices.getText(this, "animation_bar"),
	        			false,
	        			false);
				AnimationBarListener listener = new AnimationBarListener(animation, barDialog, view);
				barDialog.setWindowListener(listener);
				barPanel.addButtonsListener(listener);
	        	PluginServices.getMDIManager().addWindow(barDialog);
				this.prefsDialog.close();
			}
		}
		if(e.getSource() == prefsDialog.getButtonCancel()) {
			if(prefsDialog != null) {
				prefsDialog.close();
			}
		}
	}
	
	
	/**
	 * Gets a date that is the sum of the RelativeInstant plus windowTime. That is calculated
	 * using the scale.
	 * @param dataModel {@link TimeAnimationDataModel}
	 * @return {@link RelativeInstant}
	 */
	private RelativeInstant addWindowTimeToInitInterval(TimeAnimationDataModel dataModel) {
		RelativeInstant instant = dataModel.getInitTime();
		int[] value = new int[7];
		int[] increment = dataModel.getWindowTimeByPartOfData();
		value[0] = instant.getYear() + increment[0];
		value[1] = instant.getMonthOfYear() + increment[1];
		value[2] = instant.getDayOfMonth() + increment[2];
		value[3] = instant.getHourOfDay() + increment[3];
		value[4] = instant.getMinuteOfHour() + increment[4];
		value[5] = instant.getSecondOfMinute() + increment[5];
		value[6] = instant.getMillisOfSecond() + increment[6];
		if(value[6] > 999) {
			value[5] += (int)(value[6] / 1000);
			value[6] = value[6] % 1000;
		}
		if(value[5] > 59) {
			value[4] += (int)(value[5] / 59);
			value[5] = value[5] % 60;
		}
		if(value[4] > 59) {
			value[3] += (int)(value[4] / 59);
			value[4] = value[4] % 60;
		}
		if(value[3] > 23) {
			value[2] += (int)(value[3] / 23);
			value[3] = value[3] % 24;
		}
		
		//Meses de 31 dias
		int t = 31;
		
		//Meses de 30 dias
		if(value[1] == 4 || value[1] == 6 || value[1] == 9 || value[1] == 11) { 
			t = 30;
		}
		//Meses de 29 dias
		if(value[1] == 2 && (value[0] % 4) == 0) {
			t = 29;
		}
		//Meses de 28 dias
		if(value[1] == 2 && (value[0] % 4) != 0) {
			t = 28;
		}
		
		if(value[2] > t) {
			value[1] += (int)(value[2] / t);
			value[2] = (value[2] - t);
		}

		if(value[1] > 12) {
			value[0] += (int)(value[1] / 12);
			value[1] = value[1] - 12;
		}

		return timeSupportManager.createRelativeInstant(
				value[0], value[1], value[2], value[3], value[4], value[5], value[6]);
	}
}
