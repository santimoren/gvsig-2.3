/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/
 
package org.gvsig.timesupport.app.animation.extension;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.i18n.Messages;

/**
* Dialog for the panel AnimationPreferences
* 
* @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
*/
public class AnimationDialog extends JPanel implements IWindow, IWindowListener {
	private static final long         serialVersionUID    = 1L;
	private JComponent                comp                = null;
	private int                       width               = 0;
	private int                       height              = 0;
	private String                    label               = "";
	private boolean                   notClosable         = false;
	private IWindowListener           listener            = null;
	private JPanel                    panelButtons        = null;
	private JButton                   buttonAccept        = null;
	private JButton                   buttonCancel        = null;
	private boolean                   resizable           = false;
	
	/**
	 * Constructor
	 * @param comp
	 * @param w 
	 *        width for the dialog
	 * @param h 
	 *        height for the dialog
	 * @param label 
	 *        Window label 
	 * @param frame 
	 *        true to add the buttons accept and cancel and false to hide them
	 * @param resizable
	 *        true to get a resizable window and false to get a window with a fixed size
	 */
    public AnimationDialog(JComponent comp, 
    		int w, 
    		int h, 
    		String label, 
    		boolean frame, 
    		boolean resizable) {
		super();
		this.resizable = resizable;
		this.comp = comp;
		this.width = w;
		this.height = h;
		this.label = label;
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1;
		gbc.weighty = 1;
        this.add(comp, gbc);
        if(frame) {
        	gbc.insets = new Insets(2, 2, 4, 2);
    		gbc.fill = GridBagConstraints.HORIZONTAL;
    		gbc.weighty = 0;
    		gbc.gridy = 1;
        	this.add(getJPanelButtons(), gbc);
        }
	}
    
    /**
     * Sets the {@link IWindowListener} to manage the events
     * @param listener {@link IWindowListener}
     */
    public void setWindowListener(IWindowListener listener) {
    	this.listener = listener;
    }
    
    /**
     * Creates the window without the close button
     */
    public void setNotClosable() {
    	notClosable = true;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.andami.ui.mdiManager.IWindow#getWindowInfo()
     */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo = null;
		int code = WindowInfo.MODELESSDIALOG;
		code = notClosable ? code | WindowInfo.NOTCLOSABLE : code;
		code = resizable ? code | WindowInfo.RESIZABLE : code;
		m_viewinfo = new WindowInfo(code);
		m_viewinfo.setTitle(label);
		m_viewinfo.setHeight(height);
		m_viewinfo.setWidth(width);
		return m_viewinfo;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.andami.ui.mdiManager.IWindow#getWindowProfile()
	 */
	public Object getWindowProfile() {
		return WindowInfo.PROPERTIES_PROFILE;
	}
	
	/**
	 * Actions to execute when the window is closed
	 */
	public void close() {
		try {
			PluginServices.getMDIManager().closeWindow(this);
		} catch (ArrayIndexOutOfBoundsException e) {
			//Si la ventana no se puede eliminar no hacemos nada
		}
	}
	
	/**
	 * Returns the component inside this window
	 * @return {@link JComponent}
	 */
	public JComponent getMainComponent() {
		return comp;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.andami.ui.mdiManager.IWindowListener#windowActivated()
	 */
	public void windowActivated() {
		if(listener != null)
			listener.windowActivated();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.andami.ui.mdiManager.IWindowListener#windowClosed()
	 */
	public void windowClosed() {
		if(listener != null)
			listener.windowClosed();
	}
	
	/**
	 * Gets the panel of buttons
	 * @return
	 */
	public JPanel getJPanelButtons() {
		if(panelButtons == null) {
			panelButtons = new JPanel();
			FlowLayout fl = new FlowLayout();
			fl.setAlignment(FlowLayout.RIGHT);
			fl.setHgap(2);
			fl.setVgap(0);
			panelButtons.setLayout(fl);
			panelButtons.add(getButtonAccept(), fl);
			panelButtons.add(getButtonCancel(), fl);
		}
		return panelButtons;
	}
	
	/**
	 * Gets the accept button
	 * @return {@link JButton}
	 */
	public JButton getButtonAccept() {
		if(buttonAccept == null)
			buttonAccept = new JButton(Messages.getText("accept"));
		return buttonAccept;
	}

	/**
	 * Gets the cancel button
	 * @return {@link JButton}
	 */
	public JButton getButtonCancel() {
		if(buttonCancel == null)
			buttonCancel = new JButton(Messages.getText("cancel"));
		return buttonCancel;
	}
	
	/**
	 * Adds a listener for the buttons
	 * @param listener {@link ActionListener}
	 */
	public void addAcceptCancelListener(ActionListener listener) {
		getButtonAccept().addActionListener(listener);
		getButtonCancel().addActionListener(listener);
	}
}  
