/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.animation.extension;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.AbstractViewPanel;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayersIterator;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.TimeSupportSwingLocator;
import org.gvsig.timesupport.swing.api.TimeSupportSwingManager;
import org.gvsig.timesupport.swing.api.panel.AnimationPreferencesPanel;

/**
 * Andami extension to animate the time variable on a gvSIG view
 * 
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 */
public class AnimationSupportExtension extends Extension {
    private TimeSupportSwingManager    swingManager        = null;
    private AnimationDialog            prefsDialog         = null; 

    public void initialize() {
        // Do nothing
    }

    @Override
    public void postInitialize() {
        super.postInitialize();
        swingManager = TimeSupportSwingLocator.getSwingManager();
    }

    /**
     * Throws the preferences dialog for animation
     * @param actionCommand
     */
    public void execute(String actionCommand) {
       if(actionCommand.equalsIgnoreCase("AnimationSupport") ) {
           ViewDocument viewDocument = getView();
           if(viewDocument == null) {
        	   return;
           }
           RelativeInstant[] instant = getRelativeInstants(viewDocument);
           int scale = getScale(instant);
           if(instant != null) {
        	   TimeAnimation animation = TimeSupportLocator.getManager().createTimeAnimation();
        	   TimeAnimationDataModel dataModel = animation.getDataModel();
        	   dataModel.setTimeInterval(instant[0], instant[1]);
        	   dataModel.setTimeStepScale(scale);
        	   dataModel.setWindowTimeScale(scale);
        	   AnimationPreferencesPanel prefs = swingManager.createAnimationPreferencesPanel(dataModel);
        	  
        	   prefsDialog = new AnimationDialog(prefs.asJComponent(), 
        			   600, 310, 
        			   PluginServices.getText(this, "animation_preferences"),
        			   true, 
        			   true);
        	   AnimationPreferencesListener listener = new AnimationPreferencesListener(prefsDialog, animation, viewDocument);
        	   prefsDialog.addAcceptCancelListener(listener);
        	   PluginServices.getMDIManager().addWindow(prefsDialog);
           }
       }
    }

    /**
     * Gets a scale to the step. This calculus is not accurate. 
     * @param instant {@link RelativeInstant} array
     * @return
     */
    private int getScale(RelativeInstant[] instant) {
    	int years = instant[1].getYear() - instant[0].getYear();
    	int scale = TimeAnimation.YEAR;
    	if(years == 1)
    		scale = TimeAnimation.MONTH;
    	if(years < 1) {
    		int months = instant[1].getMonthOfYear() - instant[0].getMonthOfYear();
    		scale = TimeAnimation.MONTH;
    		if(months == 1)
    			scale = TimeAnimation.DAY;
    		if(months < 1) {
    			int day = instant[1].getDayOfMonth() - instant[0].getDayOfMonth();
    			scale = TimeAnimation.DAY;
    			if(day == 1)
    				scale = TimeAnimation.HOUR;
    			if(day < 1) {
    				int hour = instant[1].getHourOfDay() - instant[0].getHourOfDay();
    				scale = TimeAnimation.HOUR;
    				if(hour == 1)
    					scale = TimeAnimation.SECOND;
    				if(hour < 1) {
    					int second = instant[1].getSecondOfDay() - instant[0].getSecondOfDay();
    					scale = TimeAnimation.SECOND;
    					if(second == 1)
    						scale = TimeAnimation.MILLISECOND;
    					if(second < 1)
    						scale = TimeAnimation.MILLISECOND;
    				}
    			}
    		}
    	}
    	return scale;
    }

    /**
     * This extension is enabled when exists a layer with time support 
     * loaded in the active view
     */
    public boolean isEnabled() {
        ViewDocument viewDocument = getView();
        if(viewDocument == null)
        	return false;
        LayersIterator it = new LayersIterator(viewDocument.getMapContext().getLayers());
 
        while (it.hasNext()) {
            FLayer layer = it.nextLayer();
            if (layer instanceof SingleLayer){
                DataStore dataStore = ((SingleLayer)layer).getDataStore();
                if (dataStore.getInterval() != null){           
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Gets the active view
     * @return {@link AbstractViewPanel}
     */
    private ViewDocument getView() {
        ApplicationManager application = ApplicationLocator.getManager();
        ViewDocument viewDocument = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);        
        return viewDocument;
    }
    
    /**
     * Gets the interval from a layer with time content
     * @param {@link AbstractViewPanel}
     * @return {@link RelativeInstant} array
     */
    private RelativeInstant[] getRelativeInstants(ViewDocument viewDocument) {
    	 LayersIterator it = new LayersIterator(viewDocument.getMapContext().getLayers());
    	 RelativeInstant start = null;
    	 RelativeInstant end = null;
         while (it.hasNext()) {
             FLayer layer = it.nextLayer();
             if (layer instanceof SingleLayer) {
                 DataStore dataStore = ((SingleLayer)layer).getDataStore();
                 if (dataStore.getInterval() != null){           
                	 Instant s = dataStore.getInterval().getStart();
                	 Instant e = dataStore.getInterval().getEnd();
                	 if(s instanceof RelativeInstant) { 
                		 if(start == null || s.isBefore(start))
                			 start = (RelativeInstant)s;
                	 }
                	 if(e instanceof RelativeInstant) {
                		 if(end == null || e.isAfter(end))
                			 end = (RelativeInstant)e;
                	 }
                 }
             }
         }
         if(start != null && end != null)
        	 return new RelativeInstant[]{start, end};
         return null;
    }
    
    public boolean isVisible() {
        return true;
    }

}
