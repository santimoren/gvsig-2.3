/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.timesupport.app.animation.extension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.gvsig.andami.ui.mdiManager.IWindowListener;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.AbstractViewPanel;
import org.gvsig.timesupport.RelativeInstant;
import org.gvsig.timesupport.Time;
import org.gvsig.timesupport.TimeSupportLocator;
import org.gvsig.timesupport.animation.AnimateClient;
import org.gvsig.timesupport.animation.TimeAnimation;
import org.gvsig.timesupport.animation.TimeAnimationDataModel;
import org.gvsig.timesupport.swing.api.panel.AnimationBarPanel;

/**
 * Listener for the preferences dialog
 * 
 * @author Nacho Brodin (nachobrodin@gmail.com)
 */
public class AnimationBarListener implements ActionListener, AnimateClient, IWindowListener {
	private TimeAnimation                animation           = null;
	private AnimationBarPanel            barPanel            = null;
	private ViewDocument            view                = null;
	private AnimationDialog              dialog              = null;
	
	public AnimationBarListener(TimeAnimation animation, AnimationDialog dialog, ViewDocument view) {
		this.animation = animation;
		animation.setAnimateClient(this);
		this.view = view;
		this.barPanel = (AnimationBarPanel)dialog.getMainComponent();
		this.dialog = dialog;
	}
	
	/**
	 * Stops the thread an set this to null. These are the actions when the
	 * window of animation is closed.
	 */
	public void destroyThread() {
		if(animation != null) {
			animation.stopAnimation();
			animation = null;
		}
	}
	
	 /*
     * (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
	public void actionPerformed(ActionEvent e) {
		if(barPanel.getIDSource(e.getSource()) == TimeAnimationDataModel.PLAY) {
			if(animation != null)
				animation.startAnimation();
		}
		if(barPanel.getIDSource(e.getSource()) == TimeAnimationDataModel.STOP) {
			if(animation != null && animation.isAlive()) {
				animation.stopAnimation();
				//animation = null;
			}
		}
		if(barPanel.getIDSource(e.getSource()) == TimeAnimationDataModel.PAUSE) {
			if(animation != null)
				animation.pauseAnimation();
		}
		
		if(barPanel.getIDSource(e.getSource()) == TimeAnimationDataModel.CLOSE) {
			if(animation != null) {
				destroyThread();
			}
			this.dialog.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.animation.AnimateClient#update(org.gvsig.timesupport.RelativeInstant, org.gvsig.timesupport.RelativeInstant)
	 */
	public void update(RelativeInstant startDateTime, RelativeInstant endDateTime) {
		//Updates the GUI
		animation.getDataModel().setTimeWindow(startDateTime, endDateTime);
		animation.getDataModel().setSliderPosition(startDateTime);
		
		//Updates the View
		Time time = TimeSupportLocator.getManager().createRelativeInterval(startDateTime, endDateTime);
		view.getMapContext().getViewPort().setTime(time);
		view.getMapContext().invalidate();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.animation.AnimateClient#endIteration(org.gvsig.timesupport.RelativeInstant)
	 */
	public void endIteration(RelativeInstant endDateTime) {
		animation.getDataModel().setSliderPosition(endDateTime);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.timesupport.animation.AnimateClient#end()
	 */
	public void end() {
		barPanel.resetButtonsStatus();
	}

	public void windowActivated() {
		
	}

	public void windowClosed() {
		if(animation != null) {
			destroyThread();
		}
	}
}
