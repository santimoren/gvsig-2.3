/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.app.extension;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerLocator;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.app.extension.preferences.NewLayerPreferencesPage;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.ServiceException;

/**
 * Andami extension to register the NewLayer preferences panel.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerPreferencesExtension extends Extension {

    private static final Logger LOG = LoggerFactory
        .getLogger(NewLayerPreferencesExtension.class);

    /**
     * The name of the preferences property with the names of the hidden
     * NewLayerSwingProviderFactory.
     */
    public static final String PREFERENCE_DISABLED_PROVIDERS =
        "disabledProviders";
    public static final String PREFERENCE_ENABLED_PROVIDERS =
        "enabledProviders";

    public void initialize() {

        NewLayerManager providerManager = NewLayerLocator.getManager();
        // Load from preferences the list of NewLayer Providers to ignore.
        DynObject preferences = this.getPlugin().getPluginProperties();
        @SuppressWarnings("unchecked")
        Set<String> disabledProviders =
            (Set<String>) preferences
                .getDynValue(PREFERENCE_DISABLED_PROVIDERS);
        if (disabledProviders != null) {
            for (Iterator<String> iterator = disabledProviders.iterator(); iterator
                .hasNext();) {
                NewLayerProviderFactory factory;
                String providerName = null;
                try {
                    providerName = iterator.next();
                    factory =
                        providerManager
                            .getNewLayerProviderFactory(providerName);
                    providerManager.enableProvider(factory, Boolean.FALSE);
                } catch (ServiceException e) {
                    LOG.warn("Disabled NewLayer provider " + providerName
                        + " is not available", e);
                }
            }
        }
        @SuppressWarnings("unchecked")
        Set<String> enabledProviders =
            (Set<String>) preferences.getDynValue(PREFERENCE_ENABLED_PROVIDERS);
        if (enabledProviders != null) {
            for (Iterator<String> iterator = enabledProviders.iterator(); iterator
                .hasNext();) {
                NewLayerProviderFactory factory;
                String providerName = null;
                try {
                    providerName = iterator.next();
                    factory =
                        providerManager
                            .getNewLayerProviderFactory(providerName);
                    providerManager.enableProvider(factory, Boolean.TRUE);
                } catch (ServiceException e) {
                    LOG.warn("Enabled NewLayer provider " + providerName
                        + " is not available", e);
                }
            }
        }

        // Register preferences page
        ExtensionPointManager extensionPoints =
            ToolsLocator.getExtensionPointManager();
        ExtensionPoint ep = extensionPoints.add("AplicationPreferences", "");

        IconThemeHelper.registerIcon("preferences", "newlayer-preferences", this);
        
        ep.append("NewLayerPreferencesPage", "", new NewLayerPreferencesPage());
    }

    public void execute(String actionCommand) {
        // Nothing to do
    }

    public boolean isEnabled() {
        return true;
    }

    public boolean isVisible() {
        return false;
    }

}
