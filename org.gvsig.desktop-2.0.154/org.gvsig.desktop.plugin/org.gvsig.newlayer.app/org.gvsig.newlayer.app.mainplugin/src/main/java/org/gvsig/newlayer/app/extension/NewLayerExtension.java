/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerLocator;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.newlayer.NewLayerService;
import org.gvsig.newlayer.NewLayerWizard;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class NewLayerExtension extends Extension {
	
	private ViewDocument view = null;

	public void initialize() {
	}

	@Override
	public void postInitialize() {
		super.postInitialize();
	}

	public void execute(String actionCommand) {
		
		ApplicationManager application = ApplicationLocator.getManager();
		ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);

		if (view != null) {
			NewLayerManager manager = NewLayerLocator.getManager();
			NewLayerService service = manager.createNewLayerService(view.getMapContext());
			NewLayerWizard wizard = manager.createNewLayerWizard(service);
			
			WindowManager windowMgr = ToolsSwingLocator.getWindowManager();
			windowMgr.showWindow(
			    wizard,
			    Messages.getText("_New_layer_wizard"),
			    WindowManager.MODE.DIALOG);
		}
	}

	public boolean isEnabled() {
		
		ApplicationManager application = ApplicationLocator.getManager();
		view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
		return (view != null);
		
	}

	public boolean isVisible() {
		
		ApplicationManager application = ApplicationLocator.getManager();
		view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
		return (view != null);
		
	}
	
	public ViewDocument getView() {
		return view;
	}
	
}
