/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.newlayer.app.extension.preferences;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.i18n.Messages;
import org.gvsig.newlayer.NewLayerLocator;
import org.gvsig.newlayer.NewLayerManager;
import org.gvsig.newlayer.NewLayerProviderFactory;
import org.gvsig.newlayer.app.extension.NewLayerPreferencesExtension;
import org.gvsig.newlayer.preferences.NewLayerPreferencesComponent;
import org.gvsig.tools.dynobject.DynObject;

/**
 * NewLayer related preferences page.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class NewLayerPreferencesPage extends AbstractPreferencePage {

    private static final long serialVersionUID = -5751805016601819817L;

//    private ImageIcon icon;

    private NewLayerManager manager;

    private NewLayerPreferencesComponent preferences;

    /**
     * Constructor.
     */
    public NewLayerPreferencesPage() {
//        icon = PluginServices.getIconTheme().get("newlayer-properties");

        manager = NewLayerLocator.getManager();
        addComponent(new JLabel(
            Messages.getText("newlayer_preferences_panel_description")));
        preferences = manager.createNewLayerProvidersPreferences();
        addComponent(preferences.asJComponent());

        // setParentID("org.gvsig.app.gui.preferencespage.ViewPage");
    }

    public String getID() {
        return getClass().getName();
    }

    public String getTitle() {
        return Messages.getText("newlayer_preferences");
    }

    public JPanel getPanel() {
        return this;
    }

    public void initializeValues() {
        // Nothing to do
    }

    public void initializeDefaults() {
        preferences.initializeDefaults();
    }

    public ImageIcon getIcon() {
        return IconThemeHelper.getImageIcon("newlayer-preferences");
    }

    public boolean isValueChanged() {
        return preferences.isValueChanged();
    }

    @Override
    public void storeValues() throws StoreException {
        // Update manager values
        List<NewLayerProviderFactory> providers = manager.getProviders();
        Set<NewLayerProviderFactory> disabledProviders =
            preferences.getDisabledProviders();
        Set<String> enabledNames = new HashSet<String>();
        Set<String> disabledNames =
            new HashSet<String>(disabledProviders.size());
        for (int i = 0; i < providers.size(); i++) {
            NewLayerProviderFactory provider = providers.get(i);
            boolean disabled = disabledProviders.contains(provider);
            provider.setEnabled(!disabled);
            if (disabled) {
                disabledNames.add(provider.getName());
            } else {
                enabledNames.add(provider.getName());
            }
        }
        // Persist them
        DynObject preferences =
            PluginsLocator.getManager()
                .getPlugin("org.gvsig.newlayer.app.extension")
                .getPluginProperties();
        preferences.setDynValue(
            NewLayerPreferencesExtension.PREFERENCE_ENABLED_PROVIDERS,
            enabledNames);
        preferences.setDynValue(
            NewLayerPreferencesExtension.PREFERENCE_DISABLED_PROVIDERS,
            disabledNames);
    }

    @Override
    public void setChangesApplied() {
        preferences.setChangesApplied();
    }

}
