/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.help;

import java.io.File;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.help.Help;
import org.gvsig.andami.plugins.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HelpExtension  extends Extension {

	private static Logger logger = LoggerFactory.getLogger(HelpExtension.class);

	public void initialize() {
	}

	public void execute(String actionCommand) {

		// If the option pressed is help control the help panel is created.
		if(actionCommand.equalsIgnoreCase("help-contents")){

			Help help = Help.getHelp();	//My constructor.
			help.show("docs.introduccion-a-gvsig.copy_of_queesgvsig.html");//Launch help panel.

			return;
		}
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		return true;
	}

	public static String getExtensionPath() {
		String pluginName = "org.gvsig.help";
		PluginServices ps = PluginServices.getPluginServices(pluginName);
		return ps.getPluginDirectory().getAbsolutePath();
	}

	@Override
	public void postInitialize() {
		super.postInitialize();
		Help help = Help.getHelp();	//My constructor.
		help.addResource(HelpExtension.getExtensionPath()+File.separator+ "gvSIG"+File.separator+"manual-de-usuario.zip");//Documentation path.
		help.addHelp("manual-de-usuario");

	}

}
