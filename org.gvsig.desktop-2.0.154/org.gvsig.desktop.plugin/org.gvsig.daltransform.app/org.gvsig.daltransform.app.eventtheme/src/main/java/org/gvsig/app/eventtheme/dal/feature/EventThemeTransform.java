/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.eventtheme.dal.feature;

import java.util.Arrays;

import javax.swing.JOptionPane;

import org.cresques.cts.IProjection;

import org.gvsig.app.ApplicationLocator;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.AbstractFeatureStoreTransform;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.SetReadOnlyAttributeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.TaskStatusManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a transformation for a events theme. The original 
 * {@link DataStore} have to have a couple of attributes, one with the X 
 * coordinate and the second one with the Y coordinate. The result of 
 * the transformation is a {@link DataStore} that has a new geometric 
 * attribute and its value is a point with the coordinates specified in 
 * the original {@link DataStore}. 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class EventThemeTransform extends AbstractFeatureStoreTransform {

	private static Logger logger = LoggerFactory.getLogger(
			EventThemeTransform.class);
	
    public static final String PERSISTENCE_DEFINITION_NAME = "EventThemeTransform";
    /**
     * Max number of features used for initial estimation
     * of extent. Many other features will be ignored
     * simply with "iter.next()"
     */
    public static int MAX_INI_FEATURES = 200;

    private String xFieldName = null;
    private String yFieldName = null;
    private String geometryFieldName = null;
    private IProjection projection = null;
    private FeatureType originalFeatureType;
    private GeometryManager geometryManager = GeometryLocator.getGeometryManager();    
    private Envelope envelope;

    public EventThemeTransform() {
        super();
        geometryManager = GeometryLocator.getGeometryManager();
    }

    /**
     * This method initializes the transformation, sets the name of the parameters and 
     * sets the value of the {@link FeatureType} returned by the transformation. 
     * @param store
     * The original store. 
     * @param geometryFieldName
     * The field that contains the geometric attribute.
     * @param xFieldName
     * The field that contains the X coordinate.
     * @param yFieldName
     * The field that contains the Y coordinate.
     * @throws DataException
     */
    public void initialize(
    		FeatureStore store,
    		String geometryFieldName,
    		String xFieldName,
    		String yFieldName,
    		IProjection projection) throws DataException{
    	
        setFeatureStore(store);
        this.xFieldName = xFieldName;
        this.yFieldName = yFieldName;
        this.projection = projection;
        if ((geometryFieldName == null) || (geometryFieldName.equals(""))){
            this.geometryFieldName = "the_geom";
        }else{
            this.geometryFieldName = geometryFieldName;
        }
        this.originalFeatureType = this.getFeatureStore()
        .getDefaultFeatureType();

        EditableFeatureType type = originalFeatureType.getEditable();
        if (type.get(this.geometryFieldName) == null){
            EditableFeatureAttributeDescriptor attributeDescriptor = type.add(this.geometryFieldName,  DataTypes.GEOMETRY);
            try {
                attributeDescriptor.setGeometryType(geometryManager.getGeometryType(TYPES.POINT, SUBTYPES.GEOM2D));
            } catch (GeometryTypeNotSupportedException e) {
                throw new InitializeException(e);
            } catch (GeometryTypeNotValidException e) {
                throw new InitializeException(e);
            }           
            attributeDescriptor.setSRS(projection);
        }

        try {
            /*
             * creates and updates envelope with all features
             */
            initEnvelope(store, MAX_INI_FEATURES);
        } catch (CreateEnvelopeException e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(e);
        }

        type.setDefaultGeometryAttributeName(this.geometryFieldName);
        FeatureType[] types = new FeatureType[] { type.getNotEditableCopy() };
        setFeatureTypes(Arrays.asList(types), types[0]);
    }
    
    

    /**
     * creates and updates envelope with all features
     * 
     */
    private void initEnvelope(FeatureStore fsto, int max_feats) throws CreateEnvelopeException {
        
        envelope = geometryManager.createEnvelope(SUBTYPES.GEOM2D);
        FeatureSet fset = null;
        DisposableIterator diter = null;
        Feature feat = null;
        try {
            fset = fsto.getFeatureSet();
            diter = fset.fastIterator();
            
            // int count = 0;
            int countused = 0;
            int nextwait = 1;
            int index = 0;
            
            while (diter.hasNext() && (countused < max_feats)) {
            	
                feat = (Feature) diter.next();
                /*
                 * This loop will use about 70 features from the first
                 * 3000 features, more and more separated each time.
                 * Something like:
                 * 
                 * 1st, 2nd ,3rd, 5th, 8th, 15, 25, 40, 100, 300...
                 * 
                 * Other features are ignored with diter.next().
                 * That causes an acceptable behavior even if the store
                 * is very large.
                 * 
                 * Afterwards, the extent is updated while drawing,
                 * so it's not very important.
                 */
                index++;
                if (index == nextwait) {
                	index = 0;
                	/*
                	 * This causes that the first 5 features
                	 * will always be used.
                	 */
                	if (countused > 5) {
                    	nextwait++;
                	}
                	this.updateEnvelope(feat);
                	countused++;
                }
                // count++;
            }
            
            diter.dispose();
            
        } catch (Exception dex) {
            throw new CreateEnvelopeException(SUBTYPES.GEOM2D, dex);
        }
    }
    
    public void setEnvelope(Envelope env) {
    	this.envelope = env;
    }

    /*
     * Currently not used
     */
    private void launchFullExtentThread(DisposableIterator diter) {
    	
    	ComputeExtentTask task = new ComputeExtentTask(diter, this);
    	task.start();
	}

	/* (non-Javadoc)
     * @see org.gvsig.fmap.dal.feature.FeatureStoreTransform#applyTransform(org.gvsig.fmap.dal.feature.Feature, org.gvsig.fmap.dal.feature.EditableFeature)
     */
    public void applyTransform(Feature source, EditableFeature target)
    throws DataException {
        
        this.copySourceToTarget(source, target);

        try {
            
            Geometry point = null;
            Object xval = source.get(xFieldName);
            Object yval = source.get(yFieldName);
            if (xval == null || yval == null) {
                logger.info("Found row with null coordinates in event theme (created null geometry)");
                target.set(geometryFieldName, null);
                target.setDefaultGeometry(null);  
            } else {
                point = geometryManager.createPoint(
                    new Double(xval.toString()),
                    new Double(yval.toString()),
                    SUBTYPES.GEOM2D);
                target.set(geometryFieldName, point);
                target.setDefaultGeometry(point);   
                envelope.add(point.getEnvelope());
            }
        } catch ( SetReadOnlyAttributeException e1) {            
            // Do nothing
            
        } catch (Exception e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(
                TYPES.POINT, SUBTYPES.GEOM2D, e);
        }       
        
    }
    
    /**
     * Used internally to initialize envelope
     * 
     */
    private void updateEnvelope(Feature feat) throws CreateGeometryException {

        Point point = geometryManager.createPoint(
            new Double(feat.get(xFieldName).toString()),
            new Double(feat.get(yFieldName).toString()),
            SUBTYPES.GEOM2D);
        envelope.add(point.getEnvelope());
    }

    /**
     * @param source
     * @param target
     */
    private void copySourceToTarget(Feature source, EditableFeature target) {
        FeatureAttributeDescriptor attr, attrTrg;
        FeatureType ftSrc = source.getType();
        FeatureType ftTrg = target.getType();


        for (int i = 0; i < source.getType().size(); i++) {
            attr = ftSrc.getAttributeDescriptor(i);
            if (ftTrg.getIndex(attr.getName()) > -1) {
                try {
                    target.set(attr.getName(), source.get(i));
                } catch (IllegalArgumentException e) {
                    attrTrg = ftTrg.getAttributeDescriptor(attr.getName());
                    target.set(attrTrg.getIndex(), attrTrg.getDefaultValue());
                }

            }
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.feature.FeatureStoreTransform#getSourceFeatureTypeFrom(org.gvsig.fmap.dal.feature.FeatureType)
     */
    public FeatureType getSourceFeatureTypeFrom(FeatureType targetFeatureType) {
        return this.originalFeatureType;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.feature.FeatureStoreTransform#isTransformsOriginalValues()
     */
    public boolean isTransformsOriginalValues() {
        return true;
    }

    public static void registerPersistent() {
        PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();

        if( persistenceManager.getDefinition(AbstractFeatureStoreTransform.class) == null ) {
            AbstractFeatureStoreTransform.registerPersistent();
        }

        DynStruct definition = persistenceManager.getDefinition(PERSISTENCE_DEFINITION_NAME);

        if (definition == null){           
            definition = persistenceManager.addDefinition(
                EventThemeTransform.class,
                PERSISTENCE_DEFINITION_NAME,
                "EventThemeTransform Persistence definition",
                null, 
                null
            );
            definition.extend(PersistenceManager.PERSISTENCE_NAMESPACE,
                ABSTRACT_FEATURESTORE_DYNCLASS_NAME);

            definition.addDynFieldString("geometryFieldName").setMandatory(true);
            definition.addDynFieldString("xFieldName").setMandatory(true);
            definition.addDynFieldString("yFieldName").setMandatory(true);
            definition.addDynFieldObject("projection").setType(DataTypes.CRS);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.tools.persistence.Persistent#saveToState(org.gvsig.tools.persistence.PersistentState)
     */
    public void saveToState(PersistentState state) throws PersistenceException {
        super.saveToState(state);
        state.set("geometryFieldName", this.geometryFieldName);
        state.set("xFieldName", this.xFieldName);
        state.set("yFieldName", this.yFieldName);
        state.set("projection", this.projection);		
    }

    /* (non-Javadoc)
     * @see org.gvsig.tools.persistence.Persistent#loadFromState(org.gvsig.tools.persistence.PersistentState)
     */
    public void loadFromState(PersistentState state)
    throws PersistenceException {
        super.loadFromState(state);		
        String geometryFieldName = state.getString("geometryFieldName");
        String xFieldName = state.getString("xFieldName");
        String yFieldName = state.getString("yFieldName");
        IProjection projection =  (IProjection)state.get("projection");	
        try {
            initialize(getFeatureStore(), geometryFieldName, xFieldName, yFieldName, projection);
        } catch (DataException e) {
            throw new PersistenceException("Impossible to create the transform", e);
        }
    }

    public Object getDynValue(String name) throws DynFieldNotFoundException {
        if (DataStore.METADATA_CRS.equals(name)){
            return projection;
        }else if(DataStore.METADATA_ENVELOPE.equals(name)){
            return envelope;
        }
        return null;
    }

    public boolean hasDynValue(String name) {
        return ((DataStore.METADATA_CRS.equals(name)) || 
            (DataStore.METADATA_ENVELOPE.equals(name)));
    }    
    
    
    /**
     * 
     * A thread to compute the true extent (in case it has a lot of features)
     * Currently not used.
     * 
     * @author jldominguez
     * 
     * @deprecated This is not used because it causes issues with
     * ConsurrentModificationException because the store is notified of
     * a change (the transformation). Anyway, this is not very important I think.
     */
    private class ComputeExtentTask extends AbstractMonitorableTask {
    	
    	private DisposableIterator disp_iter = null;
    	private EventThemeTransform tra_toupdate = null;
    	
    	public ComputeExtentTask(DisposableIterator diter, EventThemeTransform ettra) {
    		/*
    		 * Auto-added by task manager
    		 */
    		super(Messages.getText("_Extent_of_event_theme"), true);
    		disp_iter = diter;
    		tra_toupdate = ettra;
    	}
    	
    	public void run() {

    		Envelope env = null;
    		Feature feat = null;
    		Point point = null;
    		int count = 99;
    		
    		try {
                while (disp_iter.hasNext()) {
                    feat = (Feature) disp_iter.next();
                    point = geometryManager.createPoint(
                            Double.parseDouble(feat.get(xFieldName).toString()),
                            Double.parseDouble(feat.get(yFieldName).toString()),
                            SUBTYPES.GEOM2D);
                    if (env == null) {
                    	env = (Envelope) point.getEnvelope().clone();
                    } else {
                    	env.add(point.getEnvelope());
                    }
                    count++;
                    Thread.sleep(10);
                    if (count % 100 == 0) {
                    	System.out.println("COUNT = " + count);
                    }
                }
    		} catch (Exception exc) {
    			
    			ApplicationLocator.getManager().message(
    					Messages.getText("_Error_while_getting_extent"),
    					JOptionPane.ERROR_MESSAGE);
    			logger.info("Error while getting extent in thread.", exc);
    			
    		}
    		
    		disp_iter.dispose();
    		// =================
    		if (env != null) {
    			Envelope curr_env = (Envelope) tra_toupdate.getDynValue(
    					DataStore.METADATA_ENVELOPE); 
    			curr_env.add(env);
    		}
    		// =========== End
    		TaskStatusManager man = this.getTaskStatus().getManager();
    		man.remove(this.getTaskStatus());
    	}
    	
    }
}

