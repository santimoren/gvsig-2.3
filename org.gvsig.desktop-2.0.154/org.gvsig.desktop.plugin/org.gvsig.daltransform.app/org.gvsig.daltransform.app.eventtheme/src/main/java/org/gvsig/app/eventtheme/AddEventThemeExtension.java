/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA  02110-1301, USA.
*
*/
package org.gvsig.app.eventtheme;

import javax.swing.JOptionPane;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.eventtheme.daltransform.EventThemeTransformGui;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.swing.DataTransformWizard;
import org.gvsig.i18n.Messages;

/**
 * This extension opens the transformation wizard
 * with the "add event theme" option selected 
 *  
 * @author jldominguez
 *
 */
public class AddEventThemeExtension extends Extension {

	public void initialize() {
		
		IconThemeHelper.registerIcon("action", "view-event-theme-add", this);
		
	}

	public void execute(String comm) {
		
		if (comm.compareToIgnoreCase("view-event-theme-add") == 0) {
			
		    try {
		        DataTransformWizard dataTransformWizard =
		        		DataTransformLocator.getDataTransformManager().createWizard();
	            dataTransformWizard.setDataTransformGui(new EventThemeTransformGui());
		        PluginServices.getMDIManager().addWindow(dataTransformWizard.getWindow());
	        } catch (Exception e) {
	            
	        	JOptionPane.showInternalMessageDialog(
	        			ApplicationLocator.getManager().getRootComponent(),
	        			Messages.getText("_Error_while_opening_wizard")
	        			+ ":      \n\n" + e.getMessage(),
	        			Messages.getText("Anadir_capa_de_eventos"),
	                    JOptionPane.ERROR_MESSAGE);
	        }
			
		}
		
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		
		IWindow iw = ApplicationLocator.getManager().getActiveWindow();
		return (iw instanceof IView);
	}

}
