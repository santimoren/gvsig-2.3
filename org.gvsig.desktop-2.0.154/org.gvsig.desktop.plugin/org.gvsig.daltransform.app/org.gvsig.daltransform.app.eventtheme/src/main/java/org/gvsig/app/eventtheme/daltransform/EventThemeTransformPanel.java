/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.eventtheme.daltransform;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cresques.cts.IProjection;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.gui.panels.CRSSelectPanel;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;
import org.gvsig.app.project.ProjectPreferences;
import org.gvsig.daltransform.swing.DataTransformWizard;
import org.gvsig.daltransform.swing.impl.AbstractDataTransformWizardPanel;
import org.gvsig.daltransform.swing.impl.DefaultDataTransformWizard;
import org.gvsig.daltransform.swing.impl.components.FeatureTypeCombo;
import org.gvsig.daltransform.swing.impl.components.NumericFeatureTypeAttributesCombo;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.MapContext;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class EventThemeTransformPanel extends AbstractDataTransformWizardPanel implements ActionListener {
	private JLabel nameLabel;
	private JTextField nameText;
	private NumericFeatureTypeAttributesCombo xCombo;
	private JLabel xLabel;
	private NumericFeatureTypeAttributesCombo yCombo;
	private JLabel yLabel;
	private FeatureTypeCombo featureTypeCombo;
	private JLabel featureTypeLabel;
	private JPanel northPanel = null;
	private JTextField projectionText;
	private JLabel projectionLabel;
	private JButton projectionButton = null;

	public EventThemeTransformPanel() {
		super();
		initComponents();
		initLabels();
		resetProjection();
		featureTypeCombo.addActionListener(this);
		projectionButton.addActionListener(this);
	}

	private void resetProjection() {
	    
	    DefaultDataTransformWizard dtw = getDataTransformWizard();
	    if (dtw != null) {
	        MapContext mco = dtw.getMapContext();
	        if (mco != null && mco.getProjection() != null) {
	            projectionText.setText(mco.getProjection().getAbrev());
	            return;
	        }
	    }
	    
        ProjectPreferences prefs = new ProjectPreferences();
        projectionText.setText(prefs.getDefaultProjection().getAbrev());
	}

	
	public void setDataTransformWizard(DataTransformWizard dtw) {
	    super.setDataTransformWizard(dtw);
	    resetProjection();
	}

	private void initLabels() {
		featureTypeLabel.setText(PluginServices.getText(this, "events_feature_type_field") + ":");
		xLabel.setText(PluginServices.getText(this, "events_x_field") + ":");
		yLabel.setText(PluginServices.getText(this, "events_y_field") + ":");
		nameLabel.setText(PluginServices.getText(this, "events_geom_field") + ":");
		projectionLabel.setText(PluginServices.getText(this, "projection") + ":");
		projectionButton.setText("...");
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		featureTypeCombo = new FeatureTypeCombo();
		featureTypeLabel = new javax.swing.JLabel();
		xLabel = new javax.swing.JLabel();
		xCombo = new NumericFeatureTypeAttributesCombo();
		yLabel = new javax.swing.JLabel();
		yCombo = new NumericFeatureTypeAttributesCombo();
		nameLabel = new javax.swing.JLabel();
		nameText = new javax.swing.JTextField();
		northPanel= new JPanel();
		projectionText = new JTextField();
		projectionLabel = new JLabel();
		projectionButton = new JButton();
		projectionButton.setSize(25, 25);
		projectionText.setEnabled(false);

		setLayout(new BorderLayout());
		northPanel.setLayout(new java.awt.GridBagLayout());

		//		gridBagConstraints = new java.awt.GridBagConstraints();
		//		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		//		gridBagConstraints.insets = new Insets(5,2,2,2);
		//		northPanel.add(featureTypeLabel, gridBagConstraints);
		//
		//		featureTypeCombo.setModel(new javax.swing.DefaultComboBoxModel());
		//		gridBagConstraints = new java.awt.GridBagConstraints();
		//		gridBagConstraints.gridx = 0;
		//		gridBagConstraints.gridy = 1;
		//		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		//		gridBagConstraints.weightx = 1.0;
		//		gridBagConstraints.insets = new Insets(2,2,5,2);
		//		northPanel.add(featureTypeCombo, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(5,2,2,2);
		northPanel.add(xLabel, gridBagConstraints);

		xCombo.setModel(new javax.swing.DefaultComboBoxModel());
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(2,2,5,2);
		gridBagConstraints.weightx = 1.0;
		northPanel.add(xCombo, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(5,2,2,2);
		gridBagConstraints.gridwidth = 2;
		northPanel.add(yLabel, gridBagConstraints);

		yCombo.setModel(new javax.swing.DefaultComboBoxModel());
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new Insets(2,2,5,2);
		gridBagConstraints.gridwidth = 2;
		northPanel.add(yCombo, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(5,2,2,2);
		gridBagConstraints.gridwidth = 2;
		northPanel.add(nameLabel, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new Insets(2,2,5,2);
		gridBagConstraints.gridwidth = 2;
		northPanel.add(nameText, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new Insets(5,2,2,2);
		northPanel.add(projectionLabel, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(2,2,5,2);
		northPanel.add(projectionText, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.insets = new Insets(2,2,5,2);
		northPanel.add(projectionButton, gridBagConstraints);

		add(northPanel, BorderLayout.NORTH);
	}	

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		if (event == null){
			return;
		}
		if (event.getSource() == featureTypeCombo){
			Object obj = featureTypeCombo.getSelectedItem();
			if (obj != null){
				xCombo.addFeatureAttributes(featureTypeCombo.getSelectedFeatureType());
				yCombo.addFeatureAttributes(featureTypeCombo.getSelectedFeatureType());			
			}
		}else if (event.getSource() == projectionButton){
			ISelectCrsPanel panel = CRSSelectPanel.getUIFactory().getSelectCrsPanel(
					CRSFactory.getCRS(projectionText.getText()), false);
			PluginServices.getMDIManager().addWindow(panel);
			if (panel.isOkPressed()) {
				IProjection proj = panel.getProjection();
				projectionText.setText(proj.getAbrev());
			}
		}
	}

	public String getGeometryName(){
		return nameText.getText();
	}

	public String getXName(){
		return xCombo.getSelectedFeatureAttributeDescriptor().getName();
	}

	public String getYName(){
		return yCombo.getSelectedFeatureAttributeDescriptor().getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.app.daltransform.impl.AbstractDataTransformWizardPanel#updatePanel()
	 */
	public void updatePanel() {
		featureTypeCombo.addFeatureStore(getFeatureStore());
		if (nameText != null) {
		    nameText.setText(this.suggestedGeomFieldName(getFeatureStore()));
		}
		// actionPerformed(null);			
	}

	public IProjection getProjection() {
		return CRSFactory.getCRS(projectionText.getText());
	}
	
	private String suggestedGeomFieldName(FeatureStore fsto) {
	    
	    if (fsto == null) {
	        return "";
	    }
	    
	    FeatureType ft = null;
	    try {
            ft = fsto.getDefaultFeatureType();
        } catch (DataException e) {
            logger.info("Error: unable to suggest geom field name.", e);
            return "";
        }
	    
	    if (ft.getAttributeDescriptor("geometry") == null) {
	        return "geometry";
	    }
	    if (ft.getAttributeDescriptor("geom") == null) {
	        return "geom";
	    }
	    String aux = "";
	    int i = -1;
	    do {
	        i++;
	        aux = "geom" + i;
	    } while (ft.getAttributeDescriptor(aux) != null);
	    
	    return aux;
	}
}