/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.eventlayer.dal.feature;

import java.io.File;

import org.gvsig.app.eventtheme.dal.feature.EventThemeTransform;
import org.gvsig.daltransform.BaseFeatureStoreTransform;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class EventThemeTransformTest extends BaseFeatureStoreTransform {
	private final File dbf = new File("testdata/events.dbf");
	private final String xFieldName = "x";
	private final String yFieldName = "y";
	private final String geometryFieldName = "geom";
	private final String projection = "EPSG:23030";
	protected boolean testDBFInitialized = false;
	

	public DataStoreParameters getDefaultDataStoreParameters() throws DataException {
		DataStoreParameters dbfParameters = null;
		dbfParameters = dataManager.createStoreParameters("DBFStoreParameters");

		dbfParameters.setDynValue("dbfFile", dbf);

		return dbfParameters;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseFeatureStoreTransform#getFeatureStoreTransform()
	 */
	public FeatureStoreTransform getFeatureStoreTransform() throws DataException, ValidateDataParametersException {
		FeatureStore store = (FeatureStore) dataManager.openStore("DBF", 
		    this.getDefaultDataStoreParameters());
		EventThemeTransform transform = new EventThemeTransform();
		transform.initialize(store, geometryFieldName, xFieldName, yFieldName,
				CRSFactory.getCRS(projection));
		return transform;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseTestFeatureStore#hasExplorer()
	 */
	public boolean hasExplorer() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseTestFeatureStore#usesResources()
	 */
	public boolean usesResources() {
		// TODO Auto-generated method stub
		return false;
	}

}