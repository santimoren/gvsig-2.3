/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.app.join.dal.feature;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.gvsig.daltransform.BaseFeatureStoreTransform;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class JoinTransformTest extends BaseFeatureStoreTransform {
	private final File dbf = new File("src-test/org/gvsig/app/join/dal/feature/data/join.dbf");
	
	@Override
	public DataStoreParameters getDefaultDataStoreParameters()
			throws DataException {		
	    DataStoreParameters dbfParameters = null;
        dbfParameters = dataManager.createStoreParameters("DBFStoreParameters");

        dbfParameters.setDynValue("dbfFile", dbf);

		return dbfParameters;
	}

	@Override
	public FeatureStoreTransform getFeatureStoreTransform()
			throws DataException, ValidateDataParametersException {
		URL url = JoinTransformTest.class.getResource("data/events.dbf");
		
		FeatureStore store = (FeatureStore) dataManager.createStore(this
				.getDefaultDataStoreParameters());
		
		FeatureStore store2 = (FeatureStore) dataManager.createStore(this
				.getDefaultDataStoreParameters2());		
		
		JoinTransform transform = new JoinTransform();
				
		ArrayList names = new ArrayList();
		Iterator iter = store2.getDefaultFeatureType().iterator();// <FeatureAttributeDescriptor>
		while (iter.hasNext()) {
			names.add(((FeatureAttributeDescriptor) iter.next()).getName());
		}
		
		transform.initialize(store, store2, 
				"NOMBRE", "NOMBRE", 
				null, null, 
				(String[]) names.toArray(new String[0]));
		return transform;
	}
	
	private DataStoreParameters getDefaultDataStoreParameters2() throws InitializeException, ProviderNotRegisteredException{
		DataStoreParameters dbfParameters = dataManager.createStoreParameters("DBFStoreParameters");
		dbfParameters.setDynValue("dbfFile",dbf);
		return dbfParameters;
	}

}

