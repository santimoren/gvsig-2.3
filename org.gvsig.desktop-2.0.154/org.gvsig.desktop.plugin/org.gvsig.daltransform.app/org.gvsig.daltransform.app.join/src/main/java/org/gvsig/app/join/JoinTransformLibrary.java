/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.app.join;

import java.util.Locale;

import org.gvsig.app.join.dal.feature.JoinTransform;
import org.gvsig.app.join.daltransform.JoinTransformGui;
import org.gvsig.daltransform.DataTransformLibrary;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.DataTransformManager;
import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class JoinTransformLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsImplementationOf(JoinTransformLibrary.class);
        require(DALLibrary.class);
        require(DataTransformLibrary.class);

    }

	@Override
	protected void doInitialize() throws LibraryException {
		
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		// Validate there is any implementation registered.
		DataTransformManager dataTransformManager = DataTransformLocator.getDataTransformManager();
		if (dataTransformManager == null) {
			throw new ReferenceNotRegisteredException(
					DataTransformLocator.DATA_TRANSFORM_MANAGER_NAME, DataTransformLocator.getInstance());
		}	
		dataTransformManager.registerDataTransform("join", JoinTransformGui.class);		
		
		JoinTransform.registerPersistent();
	}

}

