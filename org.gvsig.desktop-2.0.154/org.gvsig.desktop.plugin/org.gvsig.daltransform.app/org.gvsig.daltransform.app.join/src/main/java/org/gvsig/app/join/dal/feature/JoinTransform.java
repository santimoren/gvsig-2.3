/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.join.dal.feature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.AbstractFeatureStoreTransform;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.SetReadOnlyAttributeException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.evaluator.EvaluatorFieldsInfo;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JoinTransform extends AbstractFeatureStoreTransform {

	private static Logger logger = LoggerFactory.getLogger(JoinTransform.class);
	
    public static final String PERSISTENCE_DEFINITION_NAME = "JoinTransform";
    
    /**
     * Store from which the join transform will get the additional attributes
     */
    private FeatureStore store2;

    /**
     * name of the key attr in store1 that will be used to match features in
     * store2
     */
    private String keyAttr1;

    /**
     * name of the key attr in store2 that will be used to match features in
     * store1
     */
    private String keyAttr2;

    /**
     * names of the attributes to join from store2 to store1
     */
    private String[] attrs;

    /**
     * Attribute names may change after transformation a prefix is applied.
     * This map keeps correspondence between store1 original names
     * and their transformed counterparts.
     */
    private Map<String,String> store1NamesMap;

    /**
     * Attribute names may change after transformation if they are repeated in
     * both stores or if a prefix is applied. This map keeps correspondence
     * between store2 original names and their transformed counterparts.
     */
    private Map<String,String> store2NamesMap;

    private JoinTransformEvaluator evaluator = null;

    private FeatureType originalFeatureType;

    private String[] attrsForQuery;

    private String prefix1;

    private String prefix2;

    /**
     * A default constructor
     */
    public JoinTransform() {
        store1NamesMap = new HashMap<String,String>();
        store2NamesMap = new HashMap<String,String>();
    }

    /**
     * Initializes all the necessary data for this transform
     *
     * @param store1
     *            store whose default feature type is the target of this
     *            transform
     *
     * @param store2
     *            store whose default feature type will provide the new
     *            attributes to join
     *
     * @param keyAttr1
     *            key attribute in store1 that matches keyAttr2 in store2
     *            (foreign key), used for joining both stores.
     *
     * @param keyAttr2
     *            key attribute in store2 that matches keyAttr1 in store2
     *            (foreign key), used for joining both stores.
     *
     * @param attrs
     *            names of the attributes in store2 that will be joined to
     *            store1.
     */
    public void initialize(FeatureStore store1, FeatureStore store2,
        String keyAttr1, String keyAttr2, String prefix1, String prefix2,
        String[] attrs)
    throws DataException {

        if (store1 == store2) {
            throw new IllegalArgumentException("store1 == store2");
        }

        // Initialize needed data
        this.setFeatureStore(store1);
        this.store2 = store2;
        this.keyAttr1 = keyAttr1;
        this.keyAttr2 = keyAttr2;
        this.prefix1 = prefix1; // TODO
        this.prefix2 = prefix2; // TODO
        this.attrs = attrs;

        // calculate this transform resulting feature type
        // by adding all specified attrs from store2 to store1's default
        // feature type
        // FIXME for more than one FTypes ??
        this.originalFeatureType = this.getFeatureStore()
        .getDefaultFeatureType();

        // keep index of geometry and att desc ==============
        int orig_geom_field_index =
            this.originalFeatureType.getDefaultGeometryAttributeIndex();
        FeatureAttributeDescriptor orig_geom_field_att = 
            this.originalFeatureType.getDefaultGeometryAttribute();
        
        // Create the feature type and copy the store 1 type        
        EditableFeatureType editableFeatureType = this.getFeatureStore().getDefaultFeatureType().getEditable();
        FeatureAttributeDescriptor[] featureAttributeDescriptors = editableFeatureType.getAttributeDescriptors();
        for (int i=0 ; i<featureAttributeDescriptors.length ; i++){ 
            editableFeatureType.remove(featureAttributeDescriptors[i].getName());           
        }    
        addFeatureType(editableFeatureType, featureAttributeDescriptors, prefix1, store1NamesMap);

        // =========== set the new geom field name and restore geometry values
        if (orig_geom_field_index >= 0) {
            EditableFeatureAttributeDescriptor ed_att = null;
            ed_att = (EditableFeatureAttributeDescriptor)
                editableFeatureType.getAttributeDescriptor(orig_geom_field_index);
            ed_att.setSRS(orig_geom_field_att.getSRS());
            ed_att.setObjectClass(orig_geom_field_att.getObjectClass());
            ed_att.setGeometryType(orig_geom_field_att.getGeomType());
            ed_att.setDefaultValue(orig_geom_field_att.getDefaultValue());

            String new_geom_field_name = ed_att.getName();
            editableFeatureType.setDefaultGeometryAttributeName(new_geom_field_name);
        }
        // =====================================================================

        // Add the store 2 fields    
        FeatureType featureType2 = store2.getDefaultFeatureType();

        // Add the fields       
        for (int i = 0; i < attrs.length; i++) {
            addFeatureType(editableFeatureType, featureType2.getAttributeDescriptor(attrs[i]), prefix2, store2NamesMap);        
        }

        if (this.store2NamesMap.containsKey(keyAttr2)) {
            this.attrsForQuery = this.attrs;
        } else {
            List<String> list = new ArrayList<String>(this.attrs.length + 1);
            list.addAll(Arrays.asList(this.attrs));
            list.add(keyAttr2);
            this.attrsForQuery = (String[]) list.toArray(new String[] {});
        }

        // assign calculated feature type as this transform's feature type
        FeatureType[] types = new FeatureType[] { editableFeatureType.getNotEditableCopy() };
        setFeatureTypes(Arrays.asList(types), types[0]);
    }

    private void addFeatureType(EditableFeatureType targetFeatureType, FeatureAttributeDescriptor[] featureAttributeDescriptors,
        String prefix, Map<String, String> storeMap) throws DataException{

        for (int i=0 ; i<featureAttributeDescriptors.length ; i++){         
            addFeatureType(targetFeatureType, featureAttributeDescriptors[i], prefix, storeMap);
        }
    }

    private void addFeatureType(EditableFeatureType targetFeatureType, FeatureAttributeDescriptor featureAttributeDescriptor,
        String prefix, Map<String, String> storeMap) throws DataException{

        String attName = featureAttributeDescriptor.getName();
        if ((prefix != null) && (!prefix.equals(""))){
            attName = prefix + "_" + attName;
        }

        // If an attribute already exists, calculate an alternate name and add it to our type
        int j = 0;
        while (targetFeatureType.getIndex(attName) >= 0) {
            attName = targetFeatureType.getAttributeDescriptor(attName).getName() + "_" + ++j;
        }

        EditableFeatureAttributeDescriptor editableFeatureAttributeDescriptor = 
            targetFeatureType.add(attName, featureAttributeDescriptor.getType(),
                featureAttributeDescriptor.getSize());
        editableFeatureAttributeDescriptor.setPrecision(featureAttributeDescriptor.getPrecision());

        // keep correspondence between original name and transformed name
        storeMap.put(featureAttributeDescriptor.getName(), attName);
    } 

    /**
     *
     *
     * @param source
     *
     * @param target
     *
     * @throws DataException
     */
    public void applyTransform(Feature source, EditableFeature target)
    throws DataException {

        // copy the data from store1 into the resulting feature
        this.copySourceToTarget(source, target);

        // ask store2 for the specified attributes, filtering by the key
        // attribute value
        // from the source feature
        JoinTransformEvaluator eval = this.getEvaluator();
        eval.updateValue(source.get(this.keyAttr1));

        FeatureQuery query = store2.createFeatureQuery();
        query.setAttributeNames(attrsForQuery);
        query.setFilter(eval);
        FeatureSet set = null;
        DisposableIterator itFeat = null;

        try {

            set = store2.getFeatureSet(query);
            // In this join implementation, we will take only the first matching
            // feature found in store2

            Feature feat;


            itFeat = set.fastIterator();
            if (itFeat.hasNext()) {
                feat = (Feature) itFeat.next();

                // copy all attributes from joined feature to target
                this.copyJoinToTarget(feat, target);
            }
        } finally {
            if (itFeat != null) {
                itFeat.dispose();
            }
            if (set != null) {
                set.dispose();
            }
        }
    }

    /**
     * @param feat
     * @param target
     */
    private void copyJoinToTarget(Feature join, EditableFeature target) {
        Iterator<Entry<String, String>> iter = store2NamesMap.entrySet()
        .iterator();
        Entry<String, String> entry;
        FeatureType trgType = target.getType();
        FeatureAttributeDescriptor attr;
        while (iter.hasNext()) {
            entry = iter.next();
            attr = trgType.getAttributeDescriptor((String) entry.getValue());
            if (attr != null) {
                target.set(attr.getIndex(), join.get((String) entry.getKey()));
            }
        }
    }

    /**
     * @param source
     * @param target
     */
    private void copySourceToTarget(Feature source, EditableFeature target) {
        FeatureAttributeDescriptor attr, attrTrg;
        FeatureType ftSrc = source.getType();
        FeatureType ftTrg = target.getType();


        for (int i = 0; i < source.getType().size(); i++) {
            attr = ftSrc.getAttributeDescriptor(i);
            attrTrg = ftTrg.getAttributeDescriptor(store1NamesMap.get(attr.getName()));
            if (attrTrg != null) {
                try {
                    target.set(attrTrg.getIndex(), source.get(i));
                } catch ( SetReadOnlyAttributeException e1) {
                    // Ignore, do nothing
                    
                } catch (IllegalArgumentException e) {
                    attrTrg = ftTrg.getAttributeDescriptor(attr.getName());
                    target.set(attrTrg.getIndex(), attrTrg.getDefaultValue());
                }

            }
        }

    }

    private JoinTransformEvaluator getEvaluator() {
        if (this.evaluator == null){
        	FeatureType ft2 = null;
        	try {
				ft2 = this.store2.getDefaultFeatureType();
			} catch (DataException e) {
				logger.error("While getting feat type.", e);
			}
        	FeatureAttributeDescriptor att2 = ft2.getAttributeDescriptor(keyAttr2);
        	boolean is_num = att2.getDataType().isNumeric();
            this.evaluator = new JoinTransformEvaluator(keyAttr2, is_num);
        }
        return evaluator;

    }

    private class JoinTransformEvaluator implements Evaluator {

        private String attribute;
        private boolean isNumeric = false;
        private Object value;
        private String sql;
        private EvaluatorFieldsInfo info = null;

        //		private int attributeIndex;

        public JoinTransformEvaluator(String attribute, boolean is_numeric) {
            this.attribute = attribute;
            this.isNumeric = is_numeric;
            this.value = null;
            this.info = new EvaluatorFieldsInfo();

            //			this.attributeIndex = attrIndex;
        }

        public void updateValue(Object value) {
            this.value = value;
            String qt = this.isNumeric ? "" : "'";
            this.sql = this.attribute + " = " + qt + this.value + qt;
            this.info = new EvaluatorFieldsInfo();
            this.info.addMatchFieldValue(this.attribute, value);
        }

        public Object evaluate(EvaluatorData arg0) throws EvaluatorException {
            Object curValue = arg0.getDataValue(attribute);
            if (curValue == null) {
                return value == null;
            }
            return curValue.equals(value);
        }

        public String getSQL() {
            return this.sql;
        }

        public String getDescription() {
            return "Evaluates join transform match";
        }

        public String getName() {
            return "JoinTransformEvaluator";
        }

        public EvaluatorFieldsInfo getFieldsInfo() {
            return this.info;
        }

    }	

    @SuppressWarnings("unchecked")
    public FeatureType getSourceFeatureTypeFrom(FeatureType arg0) {
        return originalFeatureType;
    }

    public boolean isTransformsOriginalValues() {
        return false;
    }

    public static void registerPersistent() {
        PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();

        if( persistenceManager.getDefinition(AbstractFeatureStoreTransform.class) == null ) {
            AbstractFeatureStoreTransform.registerPersistent();
        }

        DynStruct definition = persistenceManager.getDefinition(PERSISTENCE_DEFINITION_NAME);

        if (definition == null){  
            definition = persistenceManager.addDefinition(
                JoinTransform.class,
                PERSISTENCE_DEFINITION_NAME,
                "JoinTransform Persistence definition",
                null, 
                null
            );
            definition.extend(PersistenceManager.PERSISTENCE_NAMESPACE,
                ABSTRACT_FEATURESTORE_DYNCLASS_NAME);

            definition.addDynFieldObject("store2").setClassOfValue(FeatureStore.class).setMandatory(true);
            definition.addDynFieldString("keyAttr1").setMandatory(true);
            definition.addDynFieldString("keyAttr2").setMandatory(true);
            definition.addDynFieldString("prefix1").setMandatory(false);
            definition.addDynFieldString("prefix2").setMandatory(false);
            definition.addDynFieldList("attrs").setClassOfItems(String.class).setMandatory(true);
        }
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        super.saveToState(state);
        state.set("store2", this.store2);
        state.set("keyAttr1", this.keyAttr1);
        state.set("keyAttr2", this.keyAttr2);
        state.set("prefix1", this.prefix1);
        state.set("prefix2", this.prefix2);		
        state.set("attrs", this.attrs);
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        super.loadFromState(state);
        FeatureStore store2 = (FeatureStore) state.get("store2");
        String keyAttr1 = state.getString("keyAttr1");
        String keyAttr2 = state.getString("keyAttr2");
        String prefix1 =  state.getString("prefix1");
        String prefix2 =  state.getString("prefix2");
        String[] attrs = (String[]) state.getArray("attrs", String.class);
        try {
            initialize(getFeatureStore(), store2, keyAttr1, keyAttr2, prefix1, prefix2, attrs);
        } catch (DataException e) {
            throw new PersistenceException("Impossible to create the transform", e);
        }
    }

}
