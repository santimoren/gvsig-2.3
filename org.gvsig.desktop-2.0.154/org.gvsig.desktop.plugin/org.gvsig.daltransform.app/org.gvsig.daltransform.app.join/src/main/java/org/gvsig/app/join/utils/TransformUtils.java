/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.join.utils;

import java.util.ArrayList;

import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.dataTypes.DataTypes;


/**
 * {@link DataTypes}-related utility methods
 * 
 * @author jldominguez
 *
 */
public class TransformUtils {

    public static boolean isNumericType(int t) {
        return (t == DataTypes.DOUBLE
            || t == DataTypes.FLOAT
            || t == DataTypes.INT
            || t == DataTypes.LONG
            || t == DataTypes.BYTE);
    }
    
    public static boolean comparableTypes(int a, int b) {
        
        if (a == b) {
            return true;
        } else {
            return isNumericType(a) && isNumericType(b);
        }
        
    }
    /**
     * @param ft
     * @param the_type
     * @return
     */
    public static ArrayList<Integer> getComparableDataTypes(
        FeatureType ft,
        int the_type) {

        ArrayList<Integer> resp = new ArrayList<Integer>();
        
        FeatureAttributeDescriptor[] atts = ft.getAttributeDescriptors();
        int len = atts.length;
        for (int i=0; i<len; i++) {
            if (comparableTypes(the_type, atts[i].getType())) {
                resp.add(atts[i].getType());
            }
        }
        return resp;
    }

}
