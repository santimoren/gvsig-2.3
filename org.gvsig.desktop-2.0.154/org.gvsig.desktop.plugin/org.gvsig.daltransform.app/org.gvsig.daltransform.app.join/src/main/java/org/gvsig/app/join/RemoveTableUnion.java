/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.join;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.join.dal.feature.JoinTransform;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;


/**
 * @author Fernando Gonz�lez Cort�s
 */
public class RemoveTableUnion extends Extension{

	/**
	 * @see org.gvsig.andami.plugins.IExtension#initialize()
	 */
	public void initialize() {
		IconThemeHelper.registerIcon("action", "table-remove-join", this);
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#execute(java.lang.String)
	 */
	public void execute(String actionCommand) {
		if( "table-remove-join".equalsIgnoreCase(actionCommand)) {
			FeatureTableDocumentPanel t = (FeatureTableDocumentPanel) PluginServices.getMDIManager().getActiveWindow();
			TableDocument pt = t.getModel();
			FeatureStore fs = pt.getStore();
			this.removeJoinTransfor(fs);
	
			//		TODO
			//		if (fs instanceof JoinFeatureStore) {
			//			DataManager dm = DALLocator.getDataManager();
			//			DataStoreParameters originalParams = ((JoinFeatureStoreParameters) fs
			//					.getParameters()).getStorePrimary();
			//			FeatureStore original = null;
			//			try {
			//				original = (FeatureStore) dm.createStore(originalParams);
			//			} catch (InitializeException e) {
			//				NotificationManager.addError(e.getMessage(), e);
			//				return;
			//			}
			//
			//			pt.setStore(original);
			//			try {
			//				fs.dispose();
			//			} catch (CloseException e) {
			//				NotificationManager.addError(e);
			//			}
			//			t.setModel(pt);
			//
			//		}
	
			//		t.clearSelectedFields();
			t.getModel().setModified(true);
		}
	}

	public void removeJoinTransfor(FeatureStore store) {
		FeatureStoreTransforms transforms = store.getTransforms();
		int size = transforms.size();
		if (size < 1) {
			return;
		}
		FeatureStoreTransform join = transforms.getTransform(size - 1);
		if (join instanceof JoinTransform) {
			transforms.remove(join);
		} else {
			return;
		}



	}

	public boolean hasJoinTransform(FeatureStore store) {

		FeatureStoreTransforms transforms = store.getTransforms();
		int size = transforms.size();
		if (size < 1) {
			return false;
		}
		return (transforms.getTransform(size - 1) instanceof JoinTransform);

	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isEnabled()
	 */
	public boolean isEnabled() {
		IWindow v = PluginServices.getMDIManager().getActiveWindow();

		if (v == null) {
			return false;
		}

		if (v.getClass() == FeatureTableDocumentPanel.class) {
			FeatureTableDocumentPanel t = (FeatureTableDocumentPanel) v;
			// FIXME !!!! Asi se hacia antes
			//			if (t.getModel().getOriginal() != null){
			//				return true;
			//			}

			TableDocument pt = t.getModel();
			FeatureStore fs = pt.getStore();

			return this.hasJoinTransform(fs);
//			TODO
//			if (fs instanceof JoinFeatureStore) {
//				return true;
//			}

		}
		return false;
	}

	/**
	 * @see org.gvsig.andami.plugins.IExtension#isVisible()
	 */
	public boolean isVisible() {
		IWindow v = PluginServices.getMDIManager().getActiveWindow();

		if (v == null) {
			return false;
		}

		if (v instanceof FeatureTableDocumentPanel) {
			return true;
		} else {
			return false;
		}

	}

}
