/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.join.daltransform;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.join.dal.feature.JoinTransform;
import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.daltransform.swing.DataTransformWizardPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.FeatureTypesTablePanel;
import org.gvsig.fmap.mapcontrol.dal.feature.swing.table.ConfigurableFeatureTableModel;
import org.gvsig.i18n.Messages;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class JoinTransformGui implements DataTransformGui {
    
    private static Logger logger = LoggerFactory.getLogger(JoinTransformGui.class);
    
	private SelectSecondDataStoreWizardPanel secondDataStoreWizard = null;
	private SelectParametersWizardPanel parametersWizard = null;
	private List<DataTransformWizardPanel> panels = null;	
	
	public JoinTransformGui() {
		super();			
	}
	
	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformGui#createFeatureStoreTransform(org.gvsig.fmap.dal.feature.FeatureStore)
	 */
	public FeatureStoreTransform createFeatureStoreTransform(
			FeatureStore featureStore) throws DataException {
		JoinTransform transform = new JoinTransform();
		transform.initialize(featureStore,
				secondDataStoreWizard.getSelectedFeatureStore(),
				parametersWizard.getKeyAttr1(),
				parametersWizard.getkeyAtrr2(),
				parametersWizard.getPrefix1(),
				parametersWizard.getPrefix2(),
				parametersWizard.getAttributes()
		);
		return transform;
	}



    /* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformGui#getDescription()
	 */
	public String getDescription() {
		return PluginServices.getText(this, "join_description");
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformGui#createPanels(org.gvsig.app.daltransform.gui.FeatureTransformWizardModel)
	 */
	public List<DataTransformWizardPanel> createPanels() {
		if (panels == null){
			parametersWizard = new SelectParametersWizardPanel();
			secondDataStoreWizard = new SelectSecondDataStoreWizardPanel(parametersWizard);
			panels = new ArrayList<DataTransformWizardPanel>();
			panels.add(secondDataStoreWizard);
			panels.add(parametersWizard);				
		}	
		return panels;		
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformGui#getName()
	 */
	public String getName() {
		return PluginServices.getText(this, "join_name");
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.DataTransformGui#getMinDimension()
	 */
	public Dimension getMinDimension() {
		// TODO Auto-generated method stub
		return null;
	}

    public boolean accept(FeatureStore featureStore) {        
        return true;
    }

   
    
    public String toString() {
        return getName();
    }
}

