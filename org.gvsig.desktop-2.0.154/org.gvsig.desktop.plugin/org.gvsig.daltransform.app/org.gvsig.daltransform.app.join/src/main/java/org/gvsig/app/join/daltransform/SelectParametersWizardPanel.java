/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.join.daltransform;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.join.utils.TransformUtils;
import org.gvsig.daltransform.swing.DataTransformWizardPanel;
import org.gvsig.daltransform.swing.impl.AbstractDataTransformWizardPanel;
import org.gvsig.daltransform.swing.impl.components.FeatureTypeAttributesCombo;
import org.gvsig.daltransform.swing.impl.components.FeatureTypeAttributesList;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ParameterMissingException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.i18n.Messages;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class SelectParametersWizardPanel extends AbstractDataTransformWizardPanel
implements DataTransformWizardPanel, ActionListener{
    
    private static Logger logger =
        LoggerFactory.getLogger(SelectParametersWizardPanel.class);
    
	private javax.swing.JLabel attributesLabel;
	private FeatureTypeAttributesList attributesList;
	private javax.swing.JScrollPane attributesScroll;
	private FeatureTypeAttributesCombo key1Combo;
	private javax.swing.JLabel key1Label;
	private FeatureTypeAttributesCombo key2_Combo;
	private javax.swing.JLabel key2Label;
	private javax.swing.JLabel prefix1Label;
	private javax.swing.JTextField prefix1Text;
	private javax.swing.JLabel prefix2Label;
	private javax.swing.JTextField prefix2Text; 
	
	private FeatureStore secondFeatStore = null;

	/**
	 * @param featureTransformWizardModel
	 */
	public SelectParametersWizardPanel() {
		super();	
	}
	
	   
	public void updatePanel() {
        initComponents();
        initLabels();
        try {
            updateFeatureStores();
        } catch (DataException e) {
            logger.error("While updating feature stores", e);
        }
	}
	
	private void initLabels(){
		key1Label.setText(PluginServices.getText(this,"join_first_key"));
		key2Label.setText(PluginServices.getText(this,"join_second_key"));
		prefix1Label.setText(PluginServices.getText(this,"join_first_prefix"));
		prefix2Label.setText(PluginServices.getText(this,"join_second_prefix"));
		attributesLabel.setText(PluginServices.getText(this,"join_select_attributes"));
	}

	private void initComponents() {
		removeAll();
		prefix2Text = new javax.swing.JTextField();
		key2_Combo = new FeatureTypeAttributesCombo();
		prefix1Text = new javax.swing.JTextField();
		key1Label = new javax.swing.JLabel();
		
		key1Combo = new FeatureTypeAttributesCombo();
		// listen to changes in first combo
		// we'll update second combo leaving
		// fields of same type
		key1Combo.addActionListener(this);
		
		key2Label = new javax.swing.JLabel();
		prefix1Label = new javax.swing.JLabel();
		prefix2Label = new javax.swing.JLabel();
		attributesLabel = new javax.swing.JLabel();
		attributesScroll = new javax.swing.JScrollPane();
		attributesList = new FeatureTypeAttributesList();

		setLayout(new java.awt.GridBagLayout());
		
		java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 3, 2);
		add(prefix2Text, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 3, 2);
		add(key2_Combo, gridBagConstraints);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 3, 2);
		add(prefix1Text, gridBagConstraints);

		key1Label.setText("jLabel1");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(key1Label, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 3, 2);
		add(key1Combo, gridBagConstraints);

		key2Label.setText("jLabel2");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 2, 2, 2);
		add(key2Label, gridBagConstraints);

		prefix1Label.setText("jLabel1");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 2, 2, 2);
		add(prefix1Label, gridBagConstraints);

		prefix2Label.setText("jLabel2");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 2, 2, 2);
		add(prefix2Label, gridBagConstraints);

		attributesLabel.setText("attributesLabel");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 2, 2, 2);
		add(attributesLabel, gridBagConstraints);

		attributesScroll.setViewportView(attributesList);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 9;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		add(attributesScroll, gridBagConstraints);
	}
	

	/**
	 * @return
	 */
	public String getKeyAttr1() {
		return key1Combo.getSelectedFeatureAttributeDescriptor().getName();
	}

	/**
	 * @return
	 */
	public String getkeyAtrr2() {
		return key2_Combo.getSelectedFeatureAttributeDescriptor().getName();
	}

	/**
	 * @return
	 */
	public String getPrefix1() {
		return prefix1Text.getText();
	}

	/**
	 * @return
	 */
	public String getPrefix2() {
		return prefix2Text.getText();
	}

	/**
	 * @return
	 */
	public String[] getAttributes() {
		return attributesList.getAttributesName();
	}
	
	
	public void setSecondFeatureStore(FeatureStore selectedFeatureStore) {
	    secondFeatStore = selectedFeatureStore;
	    
	}
	/**
	 * @param selectedFeatureStore
	 * @throws DataException 
	 */
	public void updateFeatureStores() throws DataException {
	    
	    if (secondFeatStore == null) {
	        throw new ParameterMissingException("Second feature store");
	    }
	    
		key1Combo.addFeatureAttributes(getFeatureStore().getDefaultFeatureType());
		attributesList.addFeatureAttributes(secondFeatStore.getDefaultFeatureType());
		
		if (key1Combo.getItemCount() == 0) {
		    
            String _msg =
                Messages.getText("_First_table_has_no_fields");
            String _tit = Messages.getText("_Join");
            
            JOptionPane.showMessageDialog(
                this, _msg, _tit,
                JOptionPane.ERROR_MESSAGE);
            
		    getDataTransformWizard().setApplicable(false);
        } else {
            key1Combo.setSelectedIndex(0);
		}
	}


	/**
	 * Listen to changes in first combo box (key1)
	 */
    public void actionPerformed(ActionEvent e) {
        
        Object src = e.getSource();
        if (src == key1Combo
            && key1Combo != null
            && key2_Combo != null
            && secondFeatStore != null) {
            
            FeatureAttributeDescriptor att =
                key1Combo.getSelectedFeatureAttributeDescriptor();
            if (att != null) {
                
                int the_type = att.getType();
                key2_Combo.removeAllItems();
                FeatureType ft = null;
                
                try {
                    ft = secondFeatStore.getDefaultFeatureType();
                } catch (DataException de) {
                    logger.info("While getting feat type: " + de.getMessage());
                    getDataTransformWizard().setApplicable(false);
                    ApplicationLocator.getManager().message(
                        Messages.getText("_Error_getting_attributes"),
                        JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                ArrayList<Integer> comparable_types =
                    TransformUtils.getComparableDataTypes(ft, the_type);
                
                if (key2_Combo.addFeatureAttributes(ft, comparable_types) == 0) {

                    String _msg =
                        Messages.getText("_No_compatible_field_in_second_table");
                    String _tit = Messages.getText("_Join");
                    
                    JOptionPane.showMessageDialog(
                        this, _msg, _tit,
                        JOptionPane.ERROR_MESSAGE);
                    getDataTransformWizard().setApplicable(false);
                    
                } else {
                    getDataTransformWizard().setApplicable(true);
                    // JOptionPane.showMessageDialog(this, "(2) OK !!");
                }
            }
        }
        
    }
}

