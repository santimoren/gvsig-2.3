/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.app.join;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.extension.TableOperations;
import org.gvsig.app.join.daltransform.JoinTransformGui;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.swing.DataTransformWizard;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class JoinToolExtension extends TableOperations {

	public void initialize() {
		IconThemeHelper.registerIcon("action", "table-create-join", this);
	}
	
	/**
	 * @see com.iver.mdiApp.plugins.IExtension#updateUI(java.lang.String)
	 */
	public void execute(String actionCommand) {
		if( "table-create-join".equalsIgnoreCase(actionCommand)) {
		    try {
		        DataTransformWizard dataTransformWizard = DataTransformLocator.getDataTransformManager().createWizard();
	            dataTransformWizard.setDataTransformGui(new JoinTransformGui());
		        PluginServices.getMDIManager().addWindow(dataTransformWizard.getWindow());
	        } catch (Exception e) {
	            
	            IWindow iw = PluginServices.getMDIManager().getActiveWindow();
	            JOptionPane.showInternalMessageDialog(
	                iw instanceof Component ? (Component) iw : null,
	                    PluginServices.getText(this, "transform_create_wizard_exception"),
	                    PluginServices.getText(this, "feature_transform"),
	                    JOptionPane.ERROR_MESSAGE);
	        }
		}
	}		
	
}
