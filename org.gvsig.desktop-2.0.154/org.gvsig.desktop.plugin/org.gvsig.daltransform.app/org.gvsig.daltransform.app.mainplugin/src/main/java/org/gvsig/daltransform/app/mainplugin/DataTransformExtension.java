/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform.app.mainplugin;

import java.awt.Component;
import java.awt.GridBagConstraints;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.ToolsWindowManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.DataTransformManager;
import org.gvsig.daltransform.swing.JDialogDataTransformList;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DataTransformExtension extends Extension {
    
    private static final Logger logger =
        LoggerFactory.getLogger(DataTransformExtension.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.andami.plugins.IExtension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
        ApplicationManager application = ApplicationLocator.getManager();
        DataTransformManager manager =
            DataTransformLocator.getDataTransformManager();

        if ("CREATE".equalsIgnoreCase(actionCommand)) {
            try {
                PluginServices.getMDIManager().addWindow(
                    manager.createWizard().getWindow());

            } catch (Exception e) {
                
                logger.info("Error while creating data transform wizard", e);
                ApplicationLocator.getManager().messageDialog(
                    Messages.getText("transform_create_wizard_exception") +
                    ": " + e.getMessage(),
                    Messages.getText("feature_transform"),
                    JOptionPane.ERROR_MESSAGE);
                
            }
        } else
            if ("VIEW".equalsIgnoreCase(actionCommand)) {

                // there is no layer loaded
                if (application.getActiveDocument() == null) {
                    JOptionPane.showMessageDialog(null,
                        Messages.getText("not_featurestore_loaded"),
                        Messages.getText("view_transforms"),
                        JOptionPane.OK_OPTION + JOptionPane.WARNING_MESSAGE);
                    return;
                }

                JDialogDataTransformList dialog =
                    manager.createJDialogDataTransformList();

                ToolsWindowManager wm = new ToolsWindowManager();
                wm.showWindow(dialog, Messages.getText("view_transforms"),
                    WindowManager.MODE.WINDOW, GridBagConstraints.CENTER);
            }

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.andami.plugins.IExtension#initialize()
     */
    public void initialize() {
    	IconThemeHelper.registerIcon("feature-transform-tools", "feature-transform-wizard", this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.andami.plugins.Extension#postInitialize()
     */
    @Override
    public void postInitialize() {
        super.postInitialize();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.andami.plugins.IExtension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.iver.andami.plugins.IExtension#isVisible()
     */
    public boolean isVisible() {
        return true;
    }
}
