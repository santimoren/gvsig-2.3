/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.daltransform.swing.impl;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.Project;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.TableManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.swing.JDataTransformList;
import org.gvsig.daltransform.swing.JDialogDataTransformList;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.usability.UsabilitySwingManager;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultJDialogDataTransformList extends JDialogDataTransformList {

    private static final long serialVersionUID = 8666781163497235054L;
    private JDataTransformList transforms = null;

    private JButton botonEliminar;
    private JButton botonCerrar;
    private JComboBox comboBox;

    public DefaultJDialogDataTransformList() {
        super();
        initComponents();
        createGUI();
    }

    private class MyComboBoxItem {

        private String label;
        private FeatureStore store;

        MyComboBoxItem(String label, FeatureStore store) {
            this.label = label;
            this.store = store;
        }

        @Override
        public String toString() {
            return label;
        }

        public FeatureStore getStore() {
            return this.store;
        }
    }

    private class MyComboBoxModel implements ComboBoxModel {

        private List<MyComboBoxItem> items = new ArrayList<MyComboBoxItem>();
        private MyComboBoxItem selected = null;

        public MyComboBoxModel(List<MyComboBoxItem> items) {
            this.items.addAll(items);
        }

        public Object getSelectedItem() {
            return this.selected;
        }

        public MyComboBoxItem getSelected() {
            return this.selected;
        }

        public void setSelectedItem(Object arg0) {
            this.selected = (MyComboBoxItem) arg0;
        }

        public Object getElementAt(int arg0) {
            return this.items.get(arg0);
        }

        public MyComboBoxItem get(int arg0) {
            return this.items.get(arg0);
        }

        public int getSize() {
            return this.items.size();
        }

        public void removeListDataListener(ListDataListener arg0) {
            // TODO Auto-generated method stub

        }

        public void addListDataListener(ListDataListener arg0) {
            // TODO Auto-generated method stub

        }

    }

    private void initComponents() {
        MyComboBoxModel comboModel = new MyComboBoxModel(getAllStores());

        comboBox = new JComboBox(comboModel);

        comboBox.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JComboBox jcmbType = (JComboBox) e.getSource();
                MyComboBoxModel model = (MyComboBoxModel) jcmbType.getModel();
                updatePanel(model.getSelected().getStore());
            }
        });

        if (comboModel.getSize() > 0) {
            transforms =
                DataTransformLocator.getDataTransformManager()
                    .createJDataTransformList(comboModel.get(0).getStore());
            comboModel.setSelectedItem(comboModel.get(0));
        }
        if (transforms != null) {
            transforms.addListSelectionListener(new ListSelectionHandler());
        }
    }

    private List<MyComboBoxItem> getAllStores() {
        List<MyComboBoxItem> stores = new ArrayList<MyComboBoxItem>();
        ApplicationManager application = ApplicationLocator.getManager();
        Project project = application.getCurrentProject();

        List<Document> views = project.getDocuments(ViewManager.TYPENAME);
        for (Document doc : views) {
            ViewDocument viewDocument = (ViewDocument) doc;

            FLayer[] layers =
                viewDocument.getMapContext().getLayers().getVisibles();
            if (layers.length > 1) {
                for (int i = 1; i < layers.length; i++) {
                    if (layers[i] instanceof FLyrVect) {
                        FLyrVect layer = (FLyrVect) layers[i];
                        stores.add(new MyComboBoxItem(layer.getName(), layer
                            .getFeatureStore()));
                    }
                }
            }
        }
        List<Document> tables = project.getDocuments(TableManager.TYPENAME);
        for (Document doc : tables) {
            TableDocument tableDocument = (TableDocument) doc;
            stores.add(new MyComboBoxItem(doc.getName(), tableDocument
                .getStore()));
        }

        return stores;
    }

    private void createGUI() {

        java.awt.GridBagConstraints gridBagConstraints;
        this.setLayout(new java.awt.GridBagLayout());

        // Add an empty margin
        this.setBorder(new EmptyBorder(15, 15, 15, 15));

        UsabilitySwingManager usabilitymanager =
            ToolsSwingLocator.getUsabilitySwingManager();

        JLabel layerText = new JLabel(Messages.getText("select_layer"));
        JLabel transformText = new JLabel(Messages.getText("select_transform"));
        JPanel botones = new JPanel();

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        this.add(layerText, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        this.add(comboBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 2, 5, 2);
        this.add(transformText, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.CENTER;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        if (transforms != null) {
            JScrollPane scroller = new JScrollPane(transforms);
            this.add(scroller, gridBagConstraints);
        } else {
            JPanel emptyPanel = new JPanel();
            this.add(emptyPanel, gridBagConstraints);
        }

        botones.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
        botonCerrar = usabilitymanager.createJButton(new ActionClose(this));
        botonCerrar.setText(Messages.getText("close"));
        botonEliminar = usabilitymanager.createJButton(new ActionRemove(this));
        botonEliminar.setText(Messages.getText("remove"));
        botones.add(botonEliminar);
        botones.add(botonCerrar);
        botonEliminar.setEnabled(false);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 2, 2, 2);
        this.add(botones, gridBagConstraints);

        botonEliminar.setEnabled(false);

        updatePanel(null);
    }

    class ListSelectionHandler implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {

            if ((transforms != null) && (transforms.getSelected() != null)) {
                if (transforms.isLastTransform(transforms.getSelected())) {
                    botonEliminar.setEnabled(true);
                    return;
                }
            }
            botonEliminar.setEnabled(false);
        }

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(250, 300);
    }

    private class ActionRemove implements Action {

        private DefaultJDialogDataTransformList parent;

        public ActionRemove(DefaultJDialogDataTransformList parent) {
            this.parent = parent;
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        public Object getValue(String key) {
            return null;
        }

        public boolean isEnabled() {
            return true;
        }

        public void putValue(String key, Object value) {
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        public void setEnabled(boolean b) {
        }

        public void actionPerformed(ActionEvent e) {
            if (this.parent.transforms.getSelected() != null) {
                this.parent.transforms.remove(this.parent.transforms
                    .getSelected());
            }
            updatePanel(null);
        }

    }

    private class ActionClose implements Action {

        private DefaultJDialogDataTransformList parent;

        public ActionClose(DefaultJDialogDataTransformList parent) {
            this.parent = parent;
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        public Object getValue(String key) {
            return null;
        }

        public boolean isEnabled() {
            return true;
        }

        public void putValue(String key, Object value) {
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        public void setEnabled(boolean b) {
        }

        public void actionPerformed(ActionEvent e) {
            this.parent.setVisible(false);
        }

    }

    private void updatePanel(FeatureStore store) {

        if (transforms != null) {

            if (store != null) {
                FeatureStoreTransforms layerTransforms = store.getTransforms();

                transforms.changeStore(store);
                transforms.removeAllTransforms();
                transforms.addTransforms(layerTransforms);
            }

            // disable the remove button if the transforms list is empty
            if (transforms.getSelected() != null) {
                botonEliminar.setEnabled(transforms.getTransformsCount() > 0);
            }

        } else {
            botonEliminar.setEnabled(false);
        }

    }

    @Override
    public void setDeleteButton(boolean active) {
        botonEliminar.setEnabled(active);
    }

}
