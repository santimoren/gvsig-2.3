/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform.swing.impl.components;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class NumericFeatureTypeAttributesCombo extends FeatureTypeAttributesCombo{

	/**
     * 
     */
    private static final long serialVersionUID = -1694488470656611290L;

    /* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.combos.FeatureTypeAttributesCombo#addFeatureAttributeDescriptor(org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor)
	 */
	public void addFeatureAttributeDescriptor(
			FeatureAttributeDescriptor featureAttributeDescriptor) {
		int type = featureAttributeDescriptor.getType();
		if ((type == DataTypes.DOUBLE) ||
				(type == DataTypes.FLOAT) ||
				(type == DataTypes.INT) ||
				(type == DataTypes.LONG)){
			super.addFeatureAttributeDescriptor(featureAttributeDescriptor);
		}
	}	
}

