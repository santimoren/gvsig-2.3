/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.daltransform.swing.impl;

import javax.swing.JOptionPane;

import jwizardcomponent.FinishAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.i18n.Messages;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DataTransformSelectionAction extends FinishAction{
	private static final Logger logger = LoggerFactory.getLogger(DataTransformSelectionAction.class);
	private DefaultDataTransformWizard dataTransformWizard = null;
	
	public DataTransformSelectionAction(DefaultDataTransformWizard dataTransformWizard) {
		super(dataTransformWizard.getWizardComponents());		
		this.dataTransformWizard = dataTransformWizard;
	}

	/* (non-Javadoc)
	 * @see jwizardcomponent.Action#performAction()
	 */
	public void performAction() {	
	    
	    /*
	     * For layer attributes table, this depends on the user
	     * decision. For tables without layer, for example, this is
	     * always false
	     */
	    boolean result_added_separately = dataTransformWizard.isLayerLoaded();
		//Gets the selected transformation
		DataTransformGui featureTransformGui = dataTransformWizard.getDataTransformGui();
			
		//Gets the selected FeatureStore
		FeatureStore fsto = dataTransformWizard.getFeatureStore();
		// FeatureStore cloned_store = null;
		
		if (result_added_separately) {
	        try {
	            /*
	             * Clone if added separately
	             */
	            fsto = (FeatureStore) fsto.clone();
	        } catch (CloneNotSupportedException e1) {
	            // FeatureStore always implements the clone method
	        }
		}

		try {			
			//Gets the transform
			FeatureStoreTransform fst = null;
			
			// Apply the transformation
			fst = featureTransformGui.createFeatureStoreTransform(fsto);
			fsto.getTransforms().add(fst);

			//Create and load a new layer...
			if (result_added_separately){
			    MapContextManager manager = MapContextLocator.getMapContextManager();
			    FLayer layer = manager.createLayer(
			        featureTransformGui.toString(),
			        fsto);
			    
			    MapContext mco = dataTransformWizard.getMapContext();
			    if (mco != null) {
			        mco.getLayers().addLayer(layer);
			    } else {
			        ApplicationLocator.getManager().messageDialog(
                        Messages.getText("_Layer_was_not_added_to_any_view"),
                        Messages.getText("transform_apply"),
			            JOptionPane.ERROR_MESSAGE);
			    }
				
				layer.dispose();
			}
		} catch (DataException e) {
			logger.error("Error creating the transformation", e);
		} catch (LoadLayerException e) {
			logger.error("Error loading the layer", e);
		}
		//Closing the window
		PluginServices.getMDIManager().closeWindow(dataTransformWizard);
	}

}

