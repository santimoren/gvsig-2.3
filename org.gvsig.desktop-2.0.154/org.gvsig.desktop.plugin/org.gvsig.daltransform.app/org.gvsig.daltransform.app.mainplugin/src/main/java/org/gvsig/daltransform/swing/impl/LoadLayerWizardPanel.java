/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform.swing.impl;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.project.ProjectManager;
import org.gvsig.app.project.documents.Document;
import org.gvsig.app.project.documents.view.BaseViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.MapContext;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class LoadLayerWizardPanel extends AbstractDataTransformWizardPanel implements ItemListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7893735055404404591L;
	
	private static final Logger logger = LoggerFactory.getLogger(LoadLayerWizardPanel.class);
	private JCheckBox loadLayerCb = null;
	private JScrollPane messageScroll = null;
	private JTextArea messageTextArea = null;
	private JPanel northPanel = null;
	private JLabel selectViewLabel;
	private JComboBox selectViewCombo;
    private JPanel centerPanel;
	private FeatureStoreTransform transform = null;

	/**
	 * @param wizardComponents
	 */
	public LoadLayerWizardPanel() {
		super();	

		initComponents();
		initLabels();	
		addViews();
		loadLayerCb.addItemListener(this);
		loadLayerCb.setSelected(true);
		selectViewLabel.setEnabled(true);
		selectViewCombo.setEnabled(true);
	}

	private void initLabels(){
		loadLayerCb.setText(PluginServices.getText(this, "transform_load_layer_query"));
		selectViewLabel.setText(PluginServices.getText(this, "transform_view_to_load"));
	}

	private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        northPanel = new javax.swing.JPanel();
        messageScroll = new javax.swing.JScrollPane();
        messageTextArea = new javax.swing.JTextArea();
        loadLayerCb = new javax.swing.JCheckBox();
        selectViewLabel = new javax.swing.JLabel();
        centerPanel = new javax.swing.JPanel();
        selectViewCombo = new JComboBox();

        setLayout(new java.awt.BorderLayout());

        northPanel.setLayout(new java.awt.GridBagLayout());

        messageScroll.setBorder(null);

        messageTextArea.setColumns(20);
        messageTextArea.setEditable(false);
        messageTextArea.setLineWrap(true);

        messageTextArea.setWrapStyleWord(true);
        messageTextArea.setBackground(northPanel.getBackground());
        Font fnt = messageTextArea.getFont();
        fnt = fnt.deriveFont(Font.PLAIN);
        messageTextArea.setFont(fnt);

        
        messageTextArea.setRows(5);
        messageTextArea.setBorder(null);
        messageScroll.setViewportView(messageTextArea);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 5, 2);
        northPanel.add(messageScroll, gridBagConstraints);

        loadLayerCb.setText("jCheckBox1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        northPanel.add(loadLayerCb, gridBagConstraints);
        
        add(northPanel, java.awt.BorderLayout.NORTH);
        
        centerPanel.setLayout(new java.awt.GridBagLayout());

        selectViewLabel.setText("jLabel1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 5, 2);
        centerPanel.add(selectViewLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        centerPanel.add(selectViewCombo, gridBagConstraints);

        add(centerPanel, java.awt.BorderLayout.CENTER);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.app.daltransform.impl.AbstractDataTransformWizardPanel#updatePanel()
	 */
	public void updatePanel() {
		//Gets the selected transformation
		DataTransformGui featureTransformGui = getDataTransformWizard().getDataTransformGui();
		
		//Gets the selected FeatureStore
		FeatureStore featureStore = getFeatureStore();
		
		//Apply the transformation
		try {
			transform = featureTransformGui.createFeatureStoreTransform(featureStore);

			if (getDataTransformWizard().isFeatureStoreLayer()){
				updateGuiForLayer(transform);
			}else{
				updateGuiForTable(transform);
			}
		} catch (DataException e) {
			logger.error("Error creating the transformation", e);
		}		
	}

	/**
	 * Update the form when the transform has been applied in
	 * to a table
	 * @param transform
	 * @throws DataException
	 */
	private void updateGuiForTable(FeatureStoreTransform transform) throws DataException{
		FeatureType featureType = transform.getDefaultFeatureType();
		FeatureAttributeDescriptor[] featureAttributeDescriptors = featureType.getAttributeDescriptors();
		boolean hasGeometry = false;
		for (int i=0 ; i<featureAttributeDescriptors.length ; i++){
			if (featureAttributeDescriptors[i].getType() == DataTypes.GEOMETRY){
				hasGeometry = true;
			}
		}
		if (hasGeometry){
			if (selectViewCombo.getItemCount() == 0){
				messageTextArea.setText(PluginServices.getText(this, "transform_layout_not_view_to_load"));
				setLoadLayerVisible(false);
			}else{
				messageTextArea.setText(PluginServices.getText(this, "transform_layout_geometry"));
				setLoadLayerVisible(true);
			}
		}else{
			messageTextArea.setText(PluginServices.getText(this, "transform_layout_no_geometry"));
			setLoadLayerVisible(false);
		}		
	}

	/**
	 * Set if it is possible or not lo load a layer
	 * from the transformation
	 * @param isVisible
	 */
	private void setLoadLayerVisible(boolean isVisible){
		loadLayerCb.setVisible(isVisible);
		selectViewLabel.setVisible(isVisible);
		selectViewCombo.setVisible(isVisible);
	}

	/**
	 * Add the project views
	 */
	private void addViews(){
	    selectViewCombo.removeAllItems();
		
		List<Document> views = ProjectManager.getInstance().getCurrentProject().getDocuments(ViewManager.TYPENAME);
		for (Document view : views) {
		    selectViewCombo.addItem(view);
		}
//		ProjectExtension ext = (ProjectExtension) PluginServices.getExtension(ProjectExtension.class);
//		ArrayList<ProjectDocument> projects = ext.getProject().getDocumentsByType(ProjectViewFactory.registerName);
//		for (int i=0 ; i<projects.size() ; i++){
//			ProjectViewBase view = (ProjectViewBase)projects.get(i);
//			((DefaultListModel)selectViewList.getModel()).addElement(view);
//		}
//		if (selectViewList.getModel().getSize() > 0){
//			hasViews = true;
//		}
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof IView){
		    selectViewCombo.setSelectedItem(window);
		}
	}

    public boolean isNextButtonEnabled() {
        return true;
    }

	
	/**
	 * Update the form when the transform has been applied in
	 * to a layer
	 * @param transform
	 * @throws DataException
	 */
	private void updateGuiForLayer(FeatureStoreTransform transform){
		messageTextArea.setText(PluginServices.getText(this, "transform_layer"));
		setLoadLayerVisible(false);
	}

	/**
	 * @return if the layer has to be loaded 
	 */
	public boolean isLayerLoaded(){
		if (!loadLayerCb.isVisible()){
			return false;
		}
		return loadLayerCb.isSelected();
	}	

	/**
	 * @return the transform
	 */
	public FeatureStoreTransform getTransform() {
		return transform;
	}
	
	/**
	 * @return The mapcontext
	 */
	public MapContext getMapContext(){
		Object obj = selectViewCombo.getSelectedItem();
		if (obj != null){
			return ((BaseViewDocument)obj).getMapContext();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent arg0) {
		boolean isSelected = loadLayerCb.isSelected();
		selectViewLabel.setEnabled(isSelected);
		selectViewCombo.setEnabled(isSelected);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformWizard#getPanelTitle()
	 */
	public String getPanelTitle() {
		return PluginServices.getText(this, "transform_apply");
	}
	
}

