/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.daltransform.swing;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionListener;

import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public abstract class JDataTransformList extends JPanel {

    private static final long serialVersionUID = -6577975265992377228L;

    public abstract FeatureStoreTransform getSelected();

    public abstract void remove(FeatureStoreTransform transform);

    public <ListSelectionListenerHandler> void addListSelectionListener(
        ListSelectionListenerHandler listSelectionListenerHandler) {
    }

    public abstract void addTransforms(FeatureStoreTransforms transforms);

    public abstract void removeAllTransforms();

    public abstract void changeStore(FeatureStore featureStore);

    public abstract int getTransformsCount();

    public abstract boolean isLastTransform(FeatureStoreTransform selected);

    public abstract void addListSelectionListener(
        ListSelectionListener actionListener);

    public abstract int getSelectedIndex();

}
