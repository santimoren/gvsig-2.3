/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform.swing.impl;

import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.andami.PluginServices;
import org.gvsig.daltransform.DataTransformLocator;
import org.gvsig.daltransform.DataTransformManager;
import org.gvsig.daltransform.swing.DataTransformGui;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class SelectTransformWizardPanel extends AbstractDataTransformWizardPanel implements ListSelectionListener{
	/**
     * 
     */
    private static final long serialVersionUID = -4191590513663444616L;
    private JList transformList;
	private JScrollPane transformScrollPane;
	private JScrollPane descriptionScrollPane;
	private JTextArea descriptionText;
	private int maxWidth = 315;
	private int maxHeight = 600;

	/**
	 * @param wizardComponents
	 */
	public SelectTransformWizardPanel() {
		super();	
		initComponents();		
		addTransforms();
		transformList.addListSelectionListener(this);		
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		transformScrollPane = new javax.swing.JScrollPane();
		transformList = new javax.swing.JList();
		descriptionScrollPane = new javax.swing.JScrollPane();
		descriptionText = new javax.swing.JTextArea();

		setLayout(new java.awt.GridBagLayout());

		transformScrollPane.setViewportView(transformList);

		transformList.setModel(new DefaultListModel());

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 5, 2);
		add(transformScrollPane, gridBagConstraints);

		descriptionText.setColumns(20);
		descriptionText.setEditable(false);
		descriptionText.setRows(5);
		descriptionText.setLineWrap(true);
		descriptionText.setWrapStyleWord(true);
		descriptionText.setBackground(this.getBackground());
		
		
		Font fnt = descriptionText.getFont();
		fnt = fnt.deriveFont(Font.PLAIN);
		descriptionText.setFont(fnt);
		
		descriptionText.setBorder(null);
		descriptionScrollPane.setBorder(null);
		descriptionScrollPane.setViewportView(descriptionText);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
		add(descriptionScrollPane, gridBagConstraints);
	}

	/**
	 * Adding the objects
	 */
	protected void addTransforms(){
		DataTransformManager featureTransformManager = 
			DataTransformLocator.getDataTransformManager();			
		List<DataTransformGui> dataTransformGui =
			featureTransformManager.getDataTransforms();
		for (int i=0 ; i<dataTransformGui.size() ; i++){
			((DefaultListModel)transformList.getModel()).addElement(new FeatureTransformGuiWrapper(dataTransformGui.get(i)));
			Dimension dimension = dataTransformGui.get(i).getMinDimension();
			if (dimension != null){
				if (dimension.getHeight() > maxHeight){
					maxHeight = (int)dimension.getHeight();
				}
				if (dimension.getWidth() > maxWidth){
					maxWidth = (int)dimension.getWidth();
				}
			}
		}	
		updatePanel();
	}

	/*
	 * 	(non-Javadoc)
	 * @see org.gvsig.app.daltransform.impl.AbstractDataTransformWizardPanel#updatePanel()
	 */
	@Override
	public void updatePanel() {
		if (transformList.getSelectedIndex() == -1){
			if (transformList.getModel().getSize() > 0){
				transformList.setSelectedIndex(0);
				valueChanged(null);
			}
		}else{
			if (transformList.getModel().getSize() == 0){
				getDataTransformWizard().setApplicable(false);
			}
		}
	}	

	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent e) {
		Object obj = transformList.getSelectedValue();
		if (obj != null){
			descriptionText.setText(((FeatureTransformGuiWrapper)obj).getFeatureTransformGui().getDescription());
		}
	}

	public DataTransformGui getFeatureTransformGui(){
		Object obj = transformList.getSelectedValue();
		if (obj != null){
			return ((FeatureTransformGuiWrapper)obj).getFeatureTransformGui();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.gui.FeatureTransformWizard#getPanelTitle()
	 */
	public String getPanelTitle() {
		return PluginServices.getText(this, "transform_selection");
	}

	/**
	 * @return the maxWidth
	 */
	public int getMaxWidth() {
		return maxWidth;
	}

	/**
	 * @return the maxHeight
	 */
	public int getMaxHeight() {
		return maxHeight;
	}

	private class FeatureTransformGuiWrapper{
		private DataTransformGui featureTransformGui = null;

		/**
		 * @param featureTransformGui
		 */
		public FeatureTransformGuiWrapper(
				DataTransformGui featureTransformGui) {
			super();
			this.featureTransformGui = featureTransformGui;
		}

		/**
		 * @return the featureTransformGui
		 */
		public DataTransformGui getFeatureTransformGui() {
			return featureTransformGui;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {			
			return featureTransformGui.getName();
		}		
	}
}

