/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform;

import java.util.List;

import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.daltransform.swing.DataTransformWizard;
import org.gvsig.daltransform.swing.JDataTransformList;
import org.gvsig.daltransform.swing.JDialogDataTransformList;
import org.gvsig.fmap.dal.feature.FeatureStore;

/**
 * This singleton provides a centralized access to register
 * {@link DataTransformGui} that is a class to establish
 * a relationship between a tranformation and a user interface.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface DataTransformManager {

    /**
     * Register a data transformation
     * 
     * @param name
     *            the name used to register the transformation
     * @param featureTransformGui
     *            the class that contains the relationship between a
     *            transformation
     *            and its user interface. This class has to be an instance of
     *            {@link DataTransformGui}
     */
    public void registerDataTransform(String name, Class<?> dataTransformGui);

    /**
     * Register the GUI that is used to apply transformations.
     * 
     * @param dataTransformWizard
     *            the class that implements the GUI. This class has to be an
     *            instance of {@link DataTransformWizard}
     */
    public void registerDataTransformWizard(Class<?> dataTransformWizard);

    /**
     * Returns a list of the registered data transformations.
     * 
     * @return
     *         A list of the registered data transformations.
     */
    public List<DataTransformGui> getDataTransforms();

    /**
     * Creates a wizard that can be used to apply the transformations.
     * 
     * @return
     *         The wizard.
     */
    public DataTransformWizard createWizard() throws CreateWizardException;

    /**
     * It creates a wizard and selects the transformation specified
     * by the parameter.
     * 
     * @param transformName
     *            The name of the transformation to apply.
     * @return
     *         The wizard.
     * @throws CreateWizardException
     */
    public DataTransformWizard createWizard(String transformName)
        throws CreateWizardException, NotRegisteredTransformException;

    public JDataTransformList createJDataTransformList(FeatureStore featureStore);

    // public JDialogDataTransformList createJDialogDataTransformList(
    // FeatureStore featureStore);

    public JDialogDataTransformList createJDialogDataTransformList();

}
