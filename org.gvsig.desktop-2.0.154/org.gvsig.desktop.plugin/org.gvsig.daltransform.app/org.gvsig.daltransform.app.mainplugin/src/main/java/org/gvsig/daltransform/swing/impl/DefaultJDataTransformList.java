/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.daltransform.swing.impl;

import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.daltransform.swing.JDataTransformList;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;

/**
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DefaultJDataTransformList extends JDataTransformList {

    private static final long serialVersionUID = 3179282558802329381L;

    private FeatureStore store;
    private JList l;
    DefaultListModel model;

    public DefaultJDataTransformList(FeatureStore store) {
        this.store = store;
        createComponents();
    }

    private void createComponents() {
        FeatureStoreTransforms transforms = this.store.getTransforms();
        model = new DefaultListModel();
        for (int i = 0; i < transforms.size(); i++) {
            model.addElement(new ItemData(transforms.getTransform(i)));
        }
        l = new JList(model);
        l.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.add(l);
        this.setBackground(Color.WHITE);

    }

    @Override
    public void addListSelectionListener(ListSelectionListener actionListener) {
        this.l.addListSelectionListener(new ListSelectionListenerWrapper(this,
            actionListener));
    }

    private class ListSelectionListenerWrapper implements ListSelectionListener {

        private DefaultJDataTransformList list;
        private ListSelectionListener listListener;

        public ListSelectionListenerWrapper(DefaultJDataTransformList list,
            ListSelectionListener listListener) {
            this.list = list;
            this.listListener = listListener;
        }

        public void valueChanged(ListSelectionEvent e) {
            listListener.valueChanged(new ListSelectionEventWrapper(e, list));
        }
    }

    private class ListSelectionEventWrapper extends ListSelectionEvent {

        private static final long serialVersionUID = -8159225017709144435L;

        public ListSelectionEventWrapper(ListSelectionEvent e,
            DefaultJDataTransformList transformList) {
            super(transformList, e.getFirstIndex(), e.getLastIndex(), e
                .getValueIsAdjusting());
        }

    }

    private class ItemData {

        public FeatureStoreTransform transform;

        ItemData(FeatureStoreTransform transform) {
            this.transform = transform;
        }

        @Override
        public String toString() {
            return this.transform.getName();
        }

        public FeatureStoreTransform getTransform() {
            return transform;
        }

        @Override
        public boolean equals(Object obj) {
            return this.transform.equals(((ItemData) obj).transform);
        }
    }

    @Override
    public FeatureStoreTransform getSelected() {
        if ((l != null) && (l.getSelectedValue() != null)) {
            return ((ItemData) l.getSelectedValue()).getTransform();
        }
        return null;
    }

    @Override
    public void remove(FeatureStoreTransform transform) {
        this.store.getTransforms().remove(transform);
        model.removeElement(new ItemData(transform));
    }

    @Override
    public void addTransforms(FeatureStoreTransforms transforms) {
        for (int i = 0; i < transforms.size(); i++) {
            model.addElement(new ItemData(transforms.getTransform(i)));
        }
    }

    @Override
    public void removeAllTransforms() {
        model.removeAllElements();
    }

    @Override
    public void changeStore(FeatureStore featureStore) {
        this.store = featureStore;
    }

    @Override
    public int getTransformsCount() {
        return model.getSize();
    }

    @Override
    public boolean isLastTransform(FeatureStoreTransform selectedTransform) {

        ItemData selectedTransformItem = new ItemData(selectedTransform);
        ItemData lastTransformItem = (ItemData) model.get(model.size() - 1);

        if (selectedTransformItem.equals(lastTransformItem)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getSelectedIndex() {
        return this.l.getSelectedIndex();
    }

}
