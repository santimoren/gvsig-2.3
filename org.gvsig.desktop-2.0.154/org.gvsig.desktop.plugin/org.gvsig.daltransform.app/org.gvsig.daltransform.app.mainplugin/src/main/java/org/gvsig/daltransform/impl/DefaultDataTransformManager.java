/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
* MA  02110-1301, USA.
* 
*/

/*
* AUTHORS (In addition to CIT):
* 2009 {Iver T.I.}   {Task}
*/
 
package org.gvsig.daltransform.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.daltransform.CreateWizardException;
import org.gvsig.daltransform.DataTransformManager;
import org.gvsig.daltransform.NotRegisteredTransformException;
import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.daltransform.swing.DataTransformWizard;
import org.gvsig.daltransform.swing.JDataTransformList;
import org.gvsig.daltransform.swing.JDialogDataTransformList;
import org.gvsig.daltransform.swing.impl.DefaultJDataTransformList;
import org.gvsig.daltransform.swing.impl.DefaultJDialogDataTransformList;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultDataTransformManager implements DataTransformManager{
	private static final String TRANSFORM_GUI_EXTENSION_POINT = "TransformGuiExtensionPoint";
	private static final String TRANSFORM_WIZARD_EXTENSION_POINT = "TransformWizardExtensionPoint";
	private static final String TRANSFORM_WIZARD_NAME = "Wizard";
	private static final Logger logger = LoggerFactory.getLogger(DefaultDataTransformManager.class);
	private ExtensionPointManager extensionPoints = ToolsLocator.getExtensionPointManager();
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.feature.transform.FeatureTransformManager#registerFeatureTransform(java.lang.String, java.lang.Class)
	 */
	public void registerDataTransform(String name,
			Class<?> featureTransformPage) {
		if (!DataTransformGui.class.isAssignableFrom(featureTransformPage)) {
			throw new IllegalArgumentException(featureTransformPage.getName()
					+ " must implement the DataTransformGui interface");
		}
		
		ExtensionPoint extensionPoint = extensionPoints.add(TRANSFORM_GUI_EXTENSION_POINT, "");
		extensionPoint.append(name, name, featureTransformPage);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.feature.transform.FeatureTransformManager#getFeatureTransforms()
	 */
	@SuppressWarnings("unchecked")
    public List<DataTransformGui> getDataTransforms() {
		List<DataTransformGui> transformArray = new ArrayList<DataTransformGui>();
		
		ExtensionPoint ep = extensionPoints.add(TRANSFORM_GUI_EXTENSION_POINT);
		Iterator<ExtensionPoint.Extension> iterator = ep.iterator();
		while (iterator.hasNext()) {
			try {				
				transformArray.add((DataTransformGui)((ExtensionPoint.Extension) iterator
						.next()).create());				
			} catch (Exception e) {
				logger.error("Error creating a FeatureTranformationGui", e);
			} 
		}
		return transformArray;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.app.daltransform.DataTransformManager#createWizard()
	 */
	public DataTransformWizard createWizard() throws CreateWizardException {
		ExtensionPoint ep = extensionPoints.add(TRANSFORM_WIZARD_EXTENSION_POINT);
		try {
			return (DataTransformWizard)ep.create(TRANSFORM_WIZARD_NAME);			
		} catch (Exception e) {
			throw new CreateWizardException(e);
		}	
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.DataTransformManager#registerDataTransformWizard(java.lang.String, java.lang.Class)
	 */
	public void registerDataTransformWizard(Class<?> dataTransformWizard) {
		if (!DataTransformWizard.class.isAssignableFrom(dataTransformWizard)) {
			throw new IllegalArgumentException(dataTransformWizard.getName()
					+ " must implement the DataTransformWizard interface");
		}
		
		ExtensionPoint extensionPoint = extensionPoints.add(TRANSFORM_WIZARD_EXTENSION_POINT, "");
		extensionPoint.append(TRANSFORM_WIZARD_NAME, "", dataTransformWizard);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.app.daltransform.DataTransformManager#createWizard(java.lang.String)
	 */
	public DataTransformWizard createWizard(String transformName)
			throws CreateWizardException {
		ExtensionPoint ep = extensionPoints.add(TRANSFORM_GUI_EXTENSION_POINT);
		try {
			Object obj = ep.create(transformName);
			if (obj == null){
				throw new NotRegisteredTransformException(transformName);
			}
			DataTransformGui dataTransformGui = (DataTransformGui)obj;
			DataTransformWizard wizard = createWizard();
			wizard.setDataTransformGui(dataTransformGui);
			return wizard;
		} catch (Exception e) {
			throw new CreateWizardException(e);
		} 		
	}
	
    public JDataTransformList createJDataTransformList(FeatureStore featureStore) {
        return new DefaultJDataTransformList(featureStore);
    }

    public JDialogDataTransformList createJDialogDataTransformList() {
        return new DefaultJDialogDataTransformList();
    }

//    public JDialogDataTransformList createJDialogDataTransformList(
//        FeatureStore featureStore) {
//        return new DefaultJDialogDataTransformList(featureStore);
//    }
}

