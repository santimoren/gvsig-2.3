/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {Iver T.I.}   {Task}
 */

package org.gvsig.daltransform.swing.impl;

import java.util.List;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.wizard.WizardAndami;
import org.gvsig.daltransform.swing.DataTransformGui;
import org.gvsig.daltransform.swing.DataTransformWizard;
import org.gvsig.daltransform.swing.DataTransformWizardPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultDataTransformWizard extends WizardAndami implements
    DataTransformWizard {

    /**
     * 
     */
    private static final long serialVersionUID = 8346790424453722486L;
    private List<DataTransformWizardPanel> transformWizardPanels = null;
    // The three default panels
    private LoadLayerWizardPanel loadLayerWizardPanel = null;
    private SelectDataStoreWizardPanel selectDataStoreWizardPanel = null;
    private SelectTransformWizardPanel selectTransformWizardPanel = null;
    // If the wizard has to use a concrete transformation
    private DataTransformGui dataTransformGui = null;
    private int PANELS_TO_REFRESH = 2;

    /**
     * @param wizard
     */
    public DefaultDataTransformWizard() {
        super(IconThemeHelper.getImageIcon("feature-transform-wizard"));
        loadLayerWizardPanel = new LoadLayerWizardPanel();
        selectDataStoreWizardPanel = new SelectDataStoreWizardPanel();
        selectTransformWizardPanel = new SelectTransformWizardPanel();
        initialize();
        setSize(selectTransformWizardPanel.getMaxHeight(),
            selectTransformWizardPanel.getMaxWidth());
    }

    private void initialize() {
        
        getWizardComponents().getFinishButton().setEnabled(false);
        getWizardComponents().getBackButton().setEnabled(false);
        
        getWindowInfo().setTitle(
            PluginServices.getText(this, "transform_wizard"));
        addDataTransformWizardPanel(selectTransformWizardPanel);
        addDataTransformWizardPanel(selectDataStoreWizardPanel);
        getWizardComponents().setFinishAction(
            new DataTransformSelectionAction(this));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.gvsig.app.daltransform.gui.DataTransformWizard#setApplicable(boolean)
     */
    public void setApplicable(boolean isEnabled) {
        getWizardComponents().getNextButton().setEnabled(isEnabled);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.app.daltransform.DataTransformWizard#getDataTransformGui()
     */
    public DataTransformGui getDataTransformGui() {
        if (dataTransformGui == null) {
            return selectTransformWizardPanel.getFeatureTransformGui();
        } else {
            return dataTransformGui;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.gvsig.app.daltransform.DataTransformWizard#setDataTransformGui(org
     * .gvsig.app.daltransform.DataTransformGui)
     */
    public void updateGui() {
        // Remove all the panels
        for (int i = getWizardComponents().getWizardPanelList().size() - 1; i >= PANELS_TO_REFRESH; i--) {
            getWizardComponents().removeWizardPanel(i);
        }
        // Add new panels
        transformWizardPanels = getDataTransformGui().createPanels();
        for (int i = 0; i < transformWizardPanels.size(); i++) {
            addDataTransformWizardPanel(transformWizardPanels.get(i));
        }
        addDataTransformWizardPanel(loadLayerWizardPanel);
    }

    public void addDataTransformWizardPanel(
        DataTransformWizardPanel dataTransformWizardPanel) {
        dataTransformWizardPanel.setDataTransformWizard(this);
        getWizardComponents().addWizardPanel(
            new DataTransformWizardContainer(getWizardComponents(),
                dataTransformWizardPanel));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.app.daltransform.DataTransformWizard#getFeatureStore()
     */
    public FeatureStore getFeatureStore() {
        return selectDataStoreWizardPanel.getFeatureStore();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.app.daltransform.DataTransformWizard#isFeatureStoreLayer()
     */
    public boolean isFeatureStoreLayer() {
        return selectDataStoreWizardPanel.isFeatureStoreLayer();
    }

    public boolean isLayerLoaded() {
        return loadLayerWizardPanel.isLayerLoaded();
    }

    public MapContext getMapContext() {
        return loadLayerWizardPanel.getMapContext();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.app.daltransform.gui.DataTransformWizard#getWindow()
     */
    public IWindow getWindow() {
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.gvsig.app.daltransform.gui.DataTransformWizard#setDataTransformGui
     * (org.gvsig.app.daltransform.gui.DataTransformGui)
     */
    public void setDataTransformGui(DataTransformGui dataTransformGui) {
        this.dataTransformGui = dataTransformGui;
        PANELS_TO_REFRESH = 1;
        getWizardComponents().removeWizardPanel(0);
        
        selectDataStoreWizardPanel.updatePanel();
    }
}
