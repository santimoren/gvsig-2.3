/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geometry.app.generalpath;




import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.PluginsLocator;
import org.gvsig.andami.PluginsManager;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.i18n.I18nManager;



public class FlatnessPage extends AbstractPreferencePage implements KeyListener  {

    /**
     *
     */
    private static final long serialVersionUID = -6521882508307037082L;

    private static final Logger logger = LoggerFactory.getLogger(FlatnessPage.class);

    public static final String ID = FlatnessPage.class.getName();

    private JPanel flatnessPanel;
    private JTextArea jTextArea = null;
    private JTextField txtFlatness;
	private boolean valChanged = false;

    private DynObject pluginProperties;
    private PluginServices plugin;

    private ImageIcon icon;



    public FlatnessPage() {
        initComponents();
    }

    private void initComponents() {
        icon = IconThemeHelper.getImageIcon("preferences-layer-modify-flatness");
        PluginsManager pluginManager = PluginsLocator.getManager();
        this.plugin = pluginManager.getPlugin(this);
        this.pluginProperties = this.plugin.getPluginProperties();

        this.flatnessPanel = getFlatnessPanel();

        this.setLayout(new BorderLayout());
        this.add(this.flatnessPanel, BorderLayout.NORTH);
        initializeValues();
    }

    public void storeValues() throws StoreException {

      double flatness;
      try{
          flatness=Double.parseDouble(txtFlatness.getText());
      }catch (Exception e) {
          throw new StoreException(PluginServices.getText(this,"minimum_size_of_line_incorrect"));
      }
      GeometryManager geometryManager = GeometryLocator.getGeometryManager();
      geometryManager.setFlatness(flatness);

      this.pluginProperties.setDynValue("flatness", flatness);
      this.plugin.savePluginProperties();
    }

    public void setChangesApplied() {
      setChanged(false);
    }

    public String getID() {
        return ID;
    }

    public String getTitle() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        return i18nManager.getTranslation("Flatness");
    }

    public JPanel getPanel() {
        return this;
    }

    public void initializeValues() {
        Double flatness = (Double) pluginProperties.getDynValue("flatness");
        txtFlatness.setText(String.valueOf(flatness));
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();
        geometryManager.setFlatness(flatness.doubleValue());
    }

    public void initializeDefaults() {
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();
        txtFlatness.setText(String.valueOf(geometryManager.getFlatness()));
    }

    public ImageIcon getIcon() {
        return this.icon;
    }

    public boolean isValueChanged() {
        return valChanged;
    }

    public boolean isResizeable() {
      return true;
    }


  public JPanel getFlatnessPanel(){
  if (flatnessPanel == null){
      flatnessPanel = new JPanel();
      flatnessPanel.setLayout(new GridBagLayout());
      flatnessPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

      GridBagConstraints constraints = new GridBagConstraints();
      constraints.gridx = 0;
      constraints.gridy = 0;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.weightx = 1.0;
      constraints.gridwidth = 2;
      constraints.insets = new Insets(0, 0, 5, 0);
      flatnessPanel.add(getJTextArea(), constraints);

      constraints = new GridBagConstraints();
      constraints.gridx = 0;
      constraints.gridy = 1;
      flatnessPanel.add(new JLabel(PluginServices.getText(this, "densityfication") + ":"), constraints);

      txtFlatness = new JTextField("", 15);
      txtFlatness.addKeyListener(this);
      constraints = new GridBagConstraints();
      constraints.gridx = 1;
      constraints.gridy = 1;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.weightx = 1.0;
      flatnessPanel.add(txtFlatness, constraints);
  }
  return flatnessPanel;
}

	/**
	 * This method initializes jTextArea
	 *
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getJTextArea() {
		if (jTextArea == null) {
			jTextArea = new JTextArea();
			jTextArea.setBounds(new java.awt.Rectangle(13,7,285,57));
			jTextArea.setForeground(java.awt.Color.black);
			jTextArea.setBackground(java.awt.SystemColor.control);
			jTextArea.setRows(3);
			jTextArea.setWrapStyleWord(true);
			jTextArea.setLineWrap(true);
			jTextArea.setEditable(false);
			jTextArea.setText(PluginServices.getText(this,"specifies_the_minimum_size_of_the_lines_that_will_form_the_curves"));
		}
		return jTextArea;
	}

    public void keyTyped(KeyEvent e) {
        valChanged = true;
    }

    public void keyPressed(KeyEvent e) {
        valChanged = true;
    }

    public void keyReleased(KeyEvent e) {
        // do nothing
    }
}
