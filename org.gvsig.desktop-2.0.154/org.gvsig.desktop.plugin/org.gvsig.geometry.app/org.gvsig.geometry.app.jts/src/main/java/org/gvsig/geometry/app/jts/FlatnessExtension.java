package org.gvsig.geometry.app.jts;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;


public class FlatnessExtension extends Extension {

    public void initialize() {
        IconThemeHelper.registerIcon("preferences", "preferences-layer-modify-flatness", this);

        Double flatness =
            (Double) getPlugin().getPluginProperties().getDynValue("flatness");
        GeometryManager geomManager = GeometryLocator.getGeometryManager();
        geomManager.setFlatness(flatness.doubleValue());
    }

    public void execute(String actionCommand) {
        // TODO Auto-generated method stub

    }

    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    public void postInitialize() {
        ExtensionPointManager extensionPoints =
            ToolsLocator.getExtensionPointManager();
        ExtensionPoint ep = extensionPoints.add("AplicationPreferences", "");
        ep.append("Flatness", "", new FlatnessPage());
    }

}
