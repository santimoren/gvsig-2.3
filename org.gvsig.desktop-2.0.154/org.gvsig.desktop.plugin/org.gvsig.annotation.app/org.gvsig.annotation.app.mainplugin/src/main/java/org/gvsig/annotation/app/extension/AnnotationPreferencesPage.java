/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.app.extension;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.preferences.AbstractPreferencePage;
import org.gvsig.andami.preferences.StoreException;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.JAnnotationPreferencesPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.i18n.I18nManager;
/**
 *  Default configuration page.
 *  <b><b>
 *  Here the user can establish what settings wants to use by default annotations.
 *
 *
 * @author Vicente Caballero Navarro
 */
public class AnnotationPreferencesPage extends AbstractPreferencePage {
	/**
	 * 
	 */
	private static final long serialVersionUID = -472617080926223575L;
	protected String id;
	//private ImageIcon icon;
	private JTextField txtDefaultText;
//	private JComboBox cmbDefaultTypeFont;
//	private JComboBox cmbDefaultStyleFont;
//	private JTextField txtDefaultRotate;
//	private JTextField txtDefaultHeight;
//	private ColorChooserPanel jccDefaultColor;
	private JAnnotationPreferencesPanel panel = null;
	
	private AnnotationManager annotationManager = null;
	

	/**
	 * Creates a new panel containing View preferences settings.
	 *
	 */
	public AnnotationPreferencesPage() {
		super();
		id = this.getClass().getName();
		this.annotationManager = AnnotationLocator.getManager();
	}

	public void initializeValues() {
		getPanel(); // init UI		

		PluginServices ps = PluginServices.getPluginServices(this);
		DynObject props = ps.getPluginProperties();
		
		// first defaults, then stored if not null
		initializeDefaults();
		
		String str = null;
		Double dou = null;
		Integer col = null;
		
		str = (String) props.getDynValue(AnnotationExtension.PERSIST_DEF_TEXT_VALUE_KEY);
		if (str != null) {
		    txtDefaultText.setText(str);
		}
		str = (String) props.getDynValue(AnnotationExtension.PERSIST_DEF_FONT_TYPE_KEY);
        if (str != null) {
            panel.setDefaultFontType(str);
        }
        str = (String) props.getDynValue(AnnotationExtension.PERSIST_DEF_FONT_STYLE_KEY);
        if (str != null) {
            panel.setDefaultFontStyle(str);
        }
        dou = (Double) props.getDynValue(AnnotationExtension.PERSIST_DEF_FONT_HEIGHT_KEY);
        if (dou != null) {
            panel.setDefaultFontHeight(dou);   
        }
        dou = (Double) props.getDynValue(AnnotationExtension.PERSIST_DEF_FONT_ROTATION_KEY);
        if (str != null) {
            panel.setDefaultFontRotation(dou);   
        }
        col = (Integer) props.getDynValue(AnnotationExtension.PERSIST_DEF_FONT_COLOR_KEY);
        if (str != null) {
            panel.setDefaultFontColor(col);
        }
	}

	public String getID() {
		return id;
	}

	public String getTitle() {
		return PluginServices.getText(this, "annotation_preferences");
	}

	public JPanel getPanel() {
			if (panel==null) {
			    I18nManager i18nManager = ToolsLocator.getI18nManager();
			    JLabel lbl = new JLabel(PluginServices.getText(this,"text"));
				addComponent(lbl,
						txtDefaultText = new JTextField(), GridBagConstraints.BOTH, new Insets(4,0,4,8));
				lbl.setToolTipText(i18nManager.getTranslation("text_to_insert_when_non_value_at_field"));
				panel = AnnotationSwingLocator.getSwingManager().createAnnotationPreferences();
				addComponent(panel);
			}
			return this;
	}

	public void storeValues() throws StoreException {
	    
	    // Store in manager
	    annotationManager.setDefaultTextValue(txtDefaultText.getText());
        annotationManager.setDefaultFontType(panel.getDefaultFontType());
        annotationManager.setDefaultFontStyle(panel.getDefaultFontStyle());
        annotationManager.setDefaultFontHeight(panel.getDefaultFontHeight());   
        annotationManager.setDefaultFontColor(panel.getDefaultFontColor());
        annotationManager.setDefaultFontRotation(panel.getDefaultFontRotation());
            
        // Persist
	    PluginServices ps = PluginServices.getPluginServices(this);
	    DynObject props = ps.getPluginProperties();
	    
	    props.setDynValue(AnnotationExtension.PERSIST_DEF_FONT_COLOR_KEY,
	        panel.getDefaultFontColor());
        props.setDynValue(AnnotationExtension.PERSIST_DEF_TEXT_VALUE_KEY,
            txtDefaultText.getText());
        props.setDynValue(AnnotationExtension.PERSIST_DEF_FONT_TYPE_KEY,
            panel.getDefaultFontType());
        props.setDynValue(AnnotationExtension.PERSIST_DEF_FONT_STYLE_KEY,
            panel.getDefaultFontStyle());
        props.setDynValue(AnnotationExtension.PERSIST_DEF_FONT_HEIGHT_KEY,
            panel.getDefaultFontHeight());
        props.setDynValue(AnnotationExtension.PERSIST_DEF_FONT_ROTATION_KEY,
            panel.getDefaultFontRotation());
        
        ps.savePluginProperties();
	}


	public void initializeDefaults() {
		txtDefaultText.setText(annotationManager.getDefaultTextValue());
		panel.setDefaultFontType(annotationManager.getDefaultFontType());
		panel.setDefaultFontStyle(annotationManager.getDefaultFontStyle());
		panel.setDefaultFontHeight(annotationManager.getDefaultFontHeight());	
		panel.setDefaultFontColor(annotationManager.getDefaultFontColor());
		panel.setDefaultFontRotation(annotationManager.getDefaultFontRotation());		
	}

	public ImageIcon getIcon() {
		return IconThemeHelper.getImageIcon("annotation-preferences");
	}

	public boolean isValueChanged() {
		return super.hasChanged();
	}

	public void setChangesApplied() {
		setChanged(false);
	}
}
