/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.annotation.app.extension;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.annotation.AnnotationCreationFinishAction;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.locator.LocatorException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public class AddLayerFinishAction implements AnnotationCreationFinishAction {
	private MapContext mapContext;
	
	public AddLayerFinishAction(MapContext mapContext) {
		this.mapContext = mapContext;
	}

	public void finished(FeatureStore featureStore) {
		int res = JOptionPane.showConfirmDialog((JComponent) PluginServices.getMDIManager()
				.getActiveWindow(),
				PluginServices.getText(this,
				"insertar_en_la_vista_la_capa_creada"),
				PluginServices.getText(this, "insertar_capa"),
				JOptionPane.YES_NO_OPTION);

		if (res == JOptionPane.YES_OPTION) {
			try {
				addShape(featureStore);
			} catch (LoadLayerException e) {
				NotificationManager.addError(e);
			} catch (LocatorException e) {
				NotificationManager.addError(e);
			}
		}
	}

	private void addShape(FeatureStore featureStore) throws LoadLayerException, LocatorException {
		FLayer layer = MapContextLocator.getMapContextManager().createLayer("Annotations", featureStore);
		mapContext.getLayers().addLayer(layer);		
	}

}

