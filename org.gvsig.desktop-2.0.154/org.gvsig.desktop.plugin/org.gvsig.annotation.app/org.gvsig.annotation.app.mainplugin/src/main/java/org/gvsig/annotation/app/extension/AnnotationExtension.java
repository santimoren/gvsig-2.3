/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.annotation.AnnotationCreationService;
import org.gvsig.annotation.AnnotationLocator;
import org.gvsig.annotation.AnnotationManager;
import org.gvsig.annotation.swing.AnnotationSwingLocator;
import org.gvsig.annotation.swing.AnnotationSwingManager;
import org.gvsig.annotation.swing.AnnotationWindowManager;
import org.gvsig.annotation.swing.JAnnotationCreationServicePanel;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.ApplicationManager;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.ViewManager;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.service.ServiceException;

/**
 * Andami extension to show Annotation in the application.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class AnnotationExtension extends Extension {

    public static final String PERSIST_DEF_FONT_COLOR_KEY = "def_font_color";
    public static final String PERSIST_DEF_TEXT_VALUE_KEY = "def_text_value";
    public static final String PERSIST_DEF_FONT_TYPE_KEY = "def_font_type";
    public static final String PERSIST_DEF_FONT_STYLE_KEY = "def_font_style";
    public static final String PERSIST_DEF_FONT_HEIGHT_KEY = "def_font_height";
    public static final String PERSIST_DEF_FONT_ROTATION_KEY = "def_font_rotation";

	private AnnotationManager manager;
	private AnnotationSwingManager swingManager;

	public void initialize() {
		ExtensionPointManager extensionPoints = ToolsLocator.getExtensionPointManager();
		ExtensionPoint ep = extensionPoints.add("AplicationPreferences", "");

		ep.append("AnnotationPreferencesPage", "", new AnnotationPreferencesPage());

		IconThemeHelper.registerIcon("preferences", "annotation-preferences", this);
	}

	@Override
	public void postInitialize() {
		super.postInitialize();
		manager = AnnotationLocator.getManager();
		// Asignamos el locator e iniciamos la instancia
		swingManager = AnnotationSwingLocator.getSwingManager();

		swingManager.registerWindowManager(new GvSIGAnnotationWindowManager());

	}

	private FLyrVect getCurrentLayer() {
		return this.getCurrentLayer(null);
	}

	private FLyrVect getCurrentLayer(MapContext mapContext) {
		FLyrVect layer = null;

		if( mapContext == null ) {
			ApplicationManager application = ApplicationLocator.getManager();

			ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
			if( view == null ) {
				return null;
			}
			mapContext = view.getMapContext();
		}
		FLayer[] actives = mapContext.getLayers().getActives();

		for (int i=0 ; i<actives.length ; i++){
			if (actives[i] instanceof FLyrVect){
				layer = (FLyrVect)actives[i];
				break;
			}
		}
		return layer;
	}

	public void execute(String actionCommand) {
		ApplicationManager application = ApplicationLocator.getManager();

		ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
		if( view == null ) {
			return;
		}
		MapContext mapContext = view.getMapContext();
		FLyrVect layer = getCurrentLayer(mapContext);
		if (layer == null){
			return;
		}
		showAnnotation(manager, layer.getFeatureStore(), mapContext);
	}

	public void showAnnotation(AnnotationManager manager, FeatureStore featureStore, MapContext mapContext) {
		try {
			AnnotationCreationService service =
				(AnnotationCreationService) manager.getAnnotationCreationService(featureStore);
			service.setAnnotationCreationFinishAction(new AddLayerFinishAction(mapContext));

			JAnnotationCreationServicePanel panel =
				swingManager.createAnnotation(service);
			swingManager.getWindowManager().showWindow(panel, "Annotation",
					AnnotationWindowManager.MODE_WINDOW);

		} catch (ServiceException e) {
			NotificationManager.addError(e);
		}
	}

	public boolean isEnabled() {
		FLayer layer = getCurrentLayer();
		if( layer == null ) {
			return false;
		}
		return true;
	}

	public boolean isVisible() {
		ApplicationManager application = ApplicationLocator.getManager();

		ViewDocument view = (ViewDocument) application.getActiveDocument(ViewManager.TYPENAME);
		if( view == null ) {
			return false;
		}
		return true;
	}
}
