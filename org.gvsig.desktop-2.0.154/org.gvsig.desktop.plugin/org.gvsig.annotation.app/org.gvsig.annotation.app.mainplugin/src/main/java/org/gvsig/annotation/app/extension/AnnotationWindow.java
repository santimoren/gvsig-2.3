/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.app.extension;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.annotation.swing.AnnotationWindowManager;

/**
 * {@link IWindow} to show a Annotation.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class AnnotationWindow extends JPanel implements IWindow {

    private static final long serialVersionUID = -4401123724140025094L;

    private WindowInfo info;

    private Object profile = WindowInfo.EDITOR_PROFILE;

    /**
     * Constructor.
     * 
     * @param panel
     *            the panel to show
     * @param title
     *            the window title
     * @param mode
     *            the window mode
     * @see {@link AnnotationWindowManager#MODE_DIALOG}
     * @see {@link AnnotationWindowManager#MODE_TOOL}
     * @see {@link AnnotationWindowManager#MODE_WINDOW}
     */
    public AnnotationWindow(JPanel panel, String title, int mode) {
        this.setLayout(new BorderLayout());
    	add(panel, BorderLayout.CENTER);
    
        Dimension dimension = new Dimension(700, 400);
        setSize(dimension);
        int code =
            WindowInfo.ICONIFIABLE | WindowInfo.MAXIMIZABLE
                | WindowInfo.RESIZABLE;
        switch (mode) {
        case AnnotationWindowManager.MODE_DIALOG:
            code |= WindowInfo.MODALDIALOG;
            profile = WindowInfo.DIALOG_PROFILE;
            break;
        case AnnotationWindowManager.MODE_TOOL:
            code |= WindowInfo.PALETTE;
            profile = WindowInfo.TOOL_PROFILE;
            break;
        case AnnotationWindowManager.MODE_WINDOW:
        default:
            code |= WindowInfo.MODELESSDIALOG;
            profile = WindowInfo.EDITOR_PROFILE;
        }
        info = new WindowInfo(code);
        info.setTitle(title);
        info.setMinimumSize(dimension);
    }

    public WindowInfo getWindowInfo() {
        return info;
    }

    public Object getWindowProfile() {
        return profile;
    }
}
