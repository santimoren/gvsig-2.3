/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.annotation.app.extension;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JOptionPane;

import org.gvsig.andami.PluginServices;
import org.gvsig.annotation.swing.AnnotationWindowManager;
import org.gvsig.annotation.swing.AnnotationWizardPanelActionListener;
import org.gvsig.annotation.swing.JAnnotationCreationServicePanel;
import org.gvsig.annotation.swing.JAnnotationPreferencesPanel;

/**
 * {@link AnnotationWindowManager} implementation to show Annotation
 * windows as gvSIG windows
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class GvSIGAnnotationWindowManager implements AnnotationWindowManager, ComponentListener {

    public void showWindow(JAnnotationCreationServicePanel panel, String title, int mode) {
        AnnotationWindow window =
            new AnnotationWindow(panel, title, mode);

        panel.setAnnotationServicePanelActionListener(new AnnotationWizardListener(window));        

        window.addComponentListener(this);

        PluginServices.getMDIManager().addCentredWindow(window);
    }

    public void componentHidden(ComponentEvent componentEvent) {
        AnnotationWindow window =
            (AnnotationWindow) componentEvent.getSource();
        if (window.isVisible()){
            PluginServices.getMDIManager().closeWindow(window);
        }
    }

    public void componentMoved(ComponentEvent e) {
        // Nothing to do
    }

    public void componentResized(ComponentEvent e) {
        // Nothing to do
    }

    public void componentShown(ComponentEvent e) {
        // Nothing to do
    }

    public class AnnotationWizardListener implements AnnotationWizardPanelActionListener{
        private AnnotationWindow annotationWindow = null;

        public AnnotationWizardListener(AnnotationWindow annotationWindow) {
            super();
            this.annotationWindow = annotationWindow;
        }

        public void cancel(
            JAnnotationCreationServicePanel annotationCreationServicePanel) {
            if (annotationWindow.isVisible()){
                PluginServices.getMDIManager().closeWindow(annotationWindow); 
            }
        }

        public void finish(
            JAnnotationCreationServicePanel annotationCreationServicePanel) {
            if (annotationWindow.isVisible()){
                PluginServices.getMDIManager().closeWindow(annotationWindow);    
            }
        }
    }


    public boolean showFileExistsPopup(String title, String text) {
        int resp = JOptionPane.showConfirmDialog(
            (Component)PluginServices.getMainFrame(), text,
            title, JOptionPane.YES_NO_OPTION);
        if (resp == JOptionPane.YES_OPTION) {
            return true;
        }
        return false;
    }

    public void showPreferencesWindow(JAnnotationPreferencesPanel panel,
        String title, int mode) {
        // TODO Auto-generated method stub

    }
}
