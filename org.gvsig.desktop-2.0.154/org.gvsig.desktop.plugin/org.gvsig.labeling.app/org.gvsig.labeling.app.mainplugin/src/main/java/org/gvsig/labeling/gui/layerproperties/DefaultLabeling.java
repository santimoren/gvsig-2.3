/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.labeling.gui.layerproperties;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.IllegalComponentStateException;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.gui.styling.LayerPreview;
import org.gvsig.labeling.label.GeneralLabelingStrategy;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.DefaultLabelingMethod;

public class DefaultLabeling extends AbstractLabelingMethodPanel implements
		ActionListener {

	private static final Logger logger = LoggerFactory
			.getLogger(DefaultLabeling.class);

	private static final long serialVersionUID = 7100208944546698724L;
	private ILabelClass defaultLabel;
	private IPlacementConstraints placementConstraints;
	private LayerPreview layerPrev;
	private JCheckBox enableLayerPrev;
	private LabelClassPreview labelPrev;
	private JButton btnProperties;

	private FeatureAttributeDescriptor[] fieldDescs;

	private boolean allowOverlap;
	private IZoomConstraints zoomConstraints;
	private LabelClassProperties lcProp;

    private JSplitPane scrl;
    private JPanel aux;
    private JPanel aux2;


	public Class<? extends ILabelingMethod> getLabelingMethodClass() {
		return DefaultLabelingMethod.class;
	}

	public String getName() {
		return Messages.getText("label_features_in_the_same_way") + ".";
	}

	@Override
	public void fillPanel(FeatureType fty) {
		try {
			if (getEnableLayerPreview().isSelected()) {
				layerPrev.setLayer(layer);
			} else {
				layerPrev.setLayer(null);
			}

			fieldDescs = fty.getAttributeDescriptors();

			ILabelingStrategy labeling = layer.getLabelingStrategy();
			if (!(labeling instanceof GeneralLabelingStrategy)) {
				labeling = new GeneralLabelingStrategy();
				labeling.setLayer(layer);
				layer.setLabelingStrategy(labeling);
			}

		} catch (Exception e) {
			NotificationManager.addWarning(e.getMessage(), e);
		}

		ILabelClass lc = null;
		if (method.getLabelClasses() != null
				&& method.getLabelClasses().length > 0) {
			lc = method.getLabelClasses()[0];
		} else {
			lc = SymbologyLocator.getSymbologyManager().createDefaultLabel();
		}
		setLabel(lc);
		getLcProp();
	}

	private JButton getBtnProperties() {
		if (btnProperties == null) {
			btnProperties = new JButton(Messages.getText("properties"));
			btnProperties.addActionListener(this);
		}
		return btnProperties;
	}

	private LabelClassPreview getLabelPrev() {
		if (labelPrev == null) {
			labelPrev = new LabelClassPreview();
		}
		return labelPrev;
	}

	private JCheckBox getEnableLayerPreview() {
		if (enableLayerPrev == null) {
			enableLayerPrev = new JCheckBox(
					Messages.getText("Enable_layer_preview"));
			enableLayerPrev.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					try {
						if (e.getStateChange() == ItemEvent.SELECTED) {
							layerPrev.setLayer(layer);
                                                        updatePreview();
						} else if (e.getStateChange() == ItemEvent.DESELECTED) {
							layerPrev.setLayer(null);
						}
					} catch (ReadException e1) {
						logger.error("While setting changing layer in preview",
								e1);
					}
				}
			});
		}
		return enableLayerPrev;
	}

	private LayerPreview getLayerPreview() {
		if (layerPrev == null) {
			layerPrev = new LayerPreview();
		}
		return layerPrev;
	}

	@Override
    protected void initializePanel() {
        setLayout(new BorderLayout());
        add(getScrl(), BorderLayout.CENTER);
        scrl.setDividerLocation(500);
        getEnableLayerPreview().setSelected(false);
        add(getEnableLayerPreview(), BorderLayout.SOUTH);
    }

    private JSplitPane getScrl() {
        if (scrl == null) {
            scrl = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        }
        scrl.add(getLayerPreview(), JSplitPane.LEFT);
        scrl.add(getAuxPanel(), JSplitPane.RIGHT);
        add(scrl, BorderLayout.CENTER);
        return scrl;
    }

	JPanel getAuxPanel(){
	    if(aux == null){
            aux = new JPanel(new BorderLayout());
            aux.add(new JBlank(10, 10), BorderLayout.NORTH);
            aux.add(new JBlank(10, 10), BorderLayout.WEST);
            aux.add(getLabelPrev(), BorderLayout.CENTER);
            aux.add(new JBlank(10, 10), BorderLayout.EAST);
            aux.add(getAux2Panel(), BorderLayout.SOUTH);
	    }
	    return aux;
	}

	JPanel getAux2Panel(){
        if(aux2 == null){
            aux2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
            aux2.add(getBtnProperties());
        }
        return aux2;
    }

	private LabelClassProperties getLcProp() {
		if (lcProp == null) {

			int n = this.fieldDescs.length;
			String[] fnames = new String[n];
			int[] ftypes = new int[n];
			for (int i = 0; i < n; i++) {
				fnames[i] = fieldDescs[i].getName();
				ftypes[i] = fieldDescs[i].getType();
			}
			lcProp = new LabelClassProperties(this.layer.getFeatureStore());
			lcProp.setLabelClass(method.getLabelClasses()[0]);
		}
		return lcProp;

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getBtnProperties())) {
			LabelClassProperties lcProp = getLcProp();
			ILabelClass lc = defaultLabel;
			lcProp.setLabelClass(lc);
			boolean eval = false;
			while (!eval) {

				ApplicationLocator.getManager().getUIManager()
						.addWindow(lcProp);
				if (!lcProp.isAccepted()) {
					break;
				}
				;
				lc = lcProp.getLabelClass();
				eval = checkValidSQL(lc);
				if (eval) {
					/*
					 * Show warning if number of expressions != number of
					 * rectangles
					 */
					checkRectangles(lc);
				}
			}
			setLabel(lc);
		}
	}

	/**
	 * Checks whether the number of expressions euqls the number of rectangles
	 * in the style
	 *
	 * @param lc
	 * @return
	 */
	private void checkRectangles(ILabelClass lc) {

		ILabelStyle lbl_sty = lc.getLabelStyle();
		if (lbl_sty != null) {
			String[] lbl_exps = lc.getLabelExpressions();
			Rectangle2D[] lbl_rects = lbl_sty.getTextBounds();
			int empty = countEmpty(lbl_exps);

			if (lbl_exps != null && lbl_rects != null
					&& (empty > 0 || (lbl_exps.length != lbl_rects.length))) {

				String[] pars = new String[3];
				pars[0] = Integer.toString(lbl_exps.length);
				pars[1] = Integer.toString(empty);
				pars[2] = Integer.toString(lbl_rects.length);

				JOptionPane
						.showMessageDialog(
								ApplicationLocator.getManager()
										.getRootComponent(),
								Messages.getText(
										"_There_are_N_expressions_N_are_empty_and_N_rectangles_"
												+ "Check_label_expressions_and_background_image_properties",
										pars), Messages
										.getText("label_expression_editor"),
								JOptionPane.WARNING_MESSAGE);

			}
		}

	}

	private int countEmpty(String[] strs) {
		if (strs == null || strs.length == 0) {
			return 0;
		} else {
			int resp = 0;
			for (int i = 0; i < strs.length; i++) {
				if (strs[i] == null || strs[i].trim().length() == 0) {
					resp++;
				}
			}
			return resp;
		}
	}

	private boolean checkValidSQL(ILabelClass lc) {

		String sqlQuery = lc.getSQLQuery();
		if (sqlQuery != null && sqlQuery.trim().length() > 0) {
			return LabelClassUtils.validExpression(sqlQuery,
					layer.getFeatureStore(), true);
		} else {
			return true;
		}

	}

	private void setLabel(ILabelClass labelClass) {

		defaultLabel = labelClass;
		labelPrev.setLabelClass(defaultLabel);
		method = newMethodForThePreview(defaultLabel);

		updatePreview();

	}

	protected ILabelingMethod newMethodForThePreview(ILabelClass defaultLabel) {

		ILabelingMethod resp = SymbologyLocator.getSymbologyManager()
				.createDefaultLabelingMethod();
		resp.addLabelClass(defaultLabel);
		return resp;
	}

        private void updatePreview() {

            ILabelingStrategy stra = layer.getLabelingStrategy();
            if (method == null) {
                stra.setLabelingMethod(newMethodForThePreview(defaultLabel));
            } else {
                stra.setLabelingMethod(method);
            }

            layer.setIsLabeled(true);
            try {
                Rectangle r = layerPrev.getBounds();
                r.setLocation(layerPrev.getLocationOnScreen());
                layerPrev.paintImmediately(r);
                layerPrev.doLayout();
            } catch (IllegalComponentStateException ex) {
                        // this happens when the component is not showing in the
                // screen. If that is the case, then we don't need to do
                // anything.
            }
        }

	public void propertyChange(PropertyChangeEvent evt) {

		String prop = evt.getPropertyName();
		ILabelingStrategy strat = layer.getLabelingStrategy();

		if (AbstractLabelingMethodPanel.PLACEMENT_CONSTRAINTS.equals(prop)) {
			placementConstraints = (IPlacementConstraints) evt.getNewValue();
			strat.setPlacementConstraints(placementConstraints);
		} else if ((strat instanceof GeneralLabelingStrategy)
				&& AbstractLabelingMethodPanel.ALLOW_OVERLAP.equals(prop)) {
			allowOverlap = (Boolean) evt.getNewValue();
			((GeneralLabelingStrategy) strat).setAllowOverlapping(allowOverlap);
		} else if (AbstractLabelingMethodPanel.ZOOM_CONSTRAINTS.equals(prop)) {
			zoomConstraints = (IZoomConstraints) evt.getNewValue();
			strat.setZoomConstraints(zoomConstraints);
		}

		updatePreview();
	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (layerPrev != null) {
			layerPrev.setEnabled(enabled);
		}
		;
		if (labelPrev != null) {
			labelPrev.setEnabled(enabled);
		}
		;
		if (btnProperties != null) {
			btnProperties.setEnabled(enabled);
		}
		;
	}

}
