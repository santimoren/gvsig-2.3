/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.gui.styling.editortools;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.filechooser.FileFilter;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.app.gui.styling.EditorTool;
import org.gvsig.app.gui.styling.StyleEditor;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IBackgroundFileStyle;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.gui.beans.swing.JFileChooser;
import org.gvsig.i18n.Messages;
import org.gvsig.symbology.SymbologyLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * LabelStyleOpenBackgroundFile.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Jan 15, 2008
 *
 */
public class LabelStyleOpenBackgroundFile extends EditorTool {

	private static Logger logger =
			LoggerFactory.getLogger(LabelStyleOpenBackgroundFile.class);
	public static final String LABEL_STYLE_BACKGROUND_FILECHOOSER = "LABEL_STYLE_BACKGROUND_FILECHOOSER";
	public static String DEFAULT_LABEL_BACKGROUND_FOLDER;

	private JButton btnOpenFile;
	private ILabelStyle style;

	public LabelStyleOpenBackgroundFile(JComponent targetEditor) {
		super(targetEditor);
	}


	@Override
	public AbstractButton getButton() {
		return getBtnOpenFile();
	}

	private JButton getBtnOpenFile() {
		if (btnOpenFile== null) {
			
			btnOpenFile = new JButton(
					IconThemeHelper.getImageIcon("edit-add-background-image"));
			btnOpenFile.setToolTipText(Messages.getText(
					"_Set_new_background_image"));
			btnOpenFile.setSize(EditorTool.SMALL_BTN_SIZE);
			btnOpenFile.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JFileChooser jfChooser = new JFileChooser(
							LABEL_STYLE_BACKGROUND_FILECHOOSER,
							DEFAULT_LABEL_BACKGROUND_FOLDER);
					FileFilter ff = new FileFilter() {

						public boolean accept(File pathname) {
							if (pathname.isDirectory()) return true;
							String fileName = pathname.getAbsolutePath().toLowerCase();
							return fileName.endsWith("jpg") ||
									fileName.endsWith("png") ||
									fileName.endsWith("gif") ||
									fileName.endsWith("svg");
						}

						public String getDescription() {
							return Messages.getText(
									"all_supported_background_image_formats")+ " (*.svg, *.jpg, *.png, *.gif)";
						}
					};

					jfChooser.setFileFilter(ff);
					if (jfChooser.showOpenDialog(owner) == JFileChooser.APPROVE_OPTION) {
						File f = jfChooser.getSelectedFile();

						
						try {
							IBackgroundFileStyle bgStyle =
									SymbologyLocator.getSymbologyManager()
									.createBackgroundFileStyle(f.toURI().toURL());
							style.setBackgroundFileStyle(bgStyle);
						} catch (IOException e1) {
							logger.error("While getting background style", e1);
						}
					}

					((StyleEditor) owner).restorePreviousTool();
					owner.repaint();
				}
			});
		}
		return btnOpenFile;
	}

	@Override
	public Cursor getCursor() {
		return Cursor.getDefaultCursor();
	}

	@Override
	public String getID() {
		return "4";
	}

	@Override
	public boolean isSuitableFor(Object obj) {
		return obj instanceof ILabelStyle;
	}

	@Override
	public void setModel(Object objectToBeEdited) {
		style = (ILabelStyle) objectToBeEdited;
	}

	public void mousePressed(MouseEvent e) { }
	public void mouseReleased(MouseEvent e) { }
	public void mouseDragged(MouseEvent e) { }

}
