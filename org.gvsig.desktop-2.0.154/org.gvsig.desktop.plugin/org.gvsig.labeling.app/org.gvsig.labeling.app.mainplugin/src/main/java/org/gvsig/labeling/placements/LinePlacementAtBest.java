/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.placements;

import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelLocationMetrics;
import org.gvsig.tools.task.Cancellable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * LinePlacementAtBest.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Dec 17, 2007
 *
 */
public class LinePlacementAtBest extends CompoundLabelPlacement {
	
	private static Logger logger = LoggerFactory.getLogger(LinePlacementAtBest.class);
	static private ILabelPlacement[] pl = new ILabelPlacement[] {
		new LinePlacementInTheMiddle(),
		new LinePlacementAtExtremities()
	};

	public LinePlacementAtBest() {
		super(pl);
		// a patch just to keep in mind the end of the lines
	}


	public ArrayList<LabelLocationMetrics> guess(ILabelClass lc,
			Geometry geom, IPlacementConstraints constraints,
			double cartographicSymbolSize, Cancellable cancel, ViewPort vp) {
		
		if (cancel.isCanceled()) return CannotPlaceLabel.NO_PLACES;

		ArrayList<LabelLocationMetrics> llc = super.guess(lc, geom, constraints, cartographicSymbolSize, cancel,vp);
		if (constraints instanceof AbstractPlacementConstraints) {
			AbstractPlacementConstraints clone;
			try {
				clone = (AbstractPlacementConstraints) ((AbstractPlacementConstraints) constraints).clone();
				clone.setLocationAlongTheLine(IPlacementConstraints.AT_THE_END_OF_THE_LINE);
				llc.addAll(pl[1].guess(lc, geom, clone, cartographicSymbolSize, cancel,vp));
			} catch (CloneNotSupportedException e) {
				// this should never happen but
				// anyway a warning does not hurt anyone
				logger.info("Couldn't clone "+constraints.getClass().getName(), e);
			}

		}
		return llc;
	}
	public boolean isSuitableFor(
			IPlacementConstraints placementConstraints,
			int shapeType) {
		
		GeometryType gt = null;
		
		try {
			gt = GeometryLocator.getGeometryManager().getGeometryType(
					shapeType, Geometry.SUBTYPES.GEOM2D);
		} catch (Exception e) {
			logger.error("While getting aux geo type.", e);
		}
		
		if (gt.isTypeOf(TYPES.CURVE)
				|| shapeType == TYPES.MULTICURVE) {
			return placementConstraints != null && placementConstraints.isAtBestOfLine();
		} else {
			return false;
		}
	}


}