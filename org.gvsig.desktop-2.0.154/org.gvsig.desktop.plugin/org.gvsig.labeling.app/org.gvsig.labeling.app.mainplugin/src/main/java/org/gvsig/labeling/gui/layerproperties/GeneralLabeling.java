/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.labeling.gui.layerproperties;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.legend.gui.ILabelingStrategyPanel;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.label.GeneralLabelingFactory;
import org.gvsig.labeling.label.GeneralLabelingStrategy;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.symbology.SymbologyLocator;

/**
 * @author gvSIG Team
 *
 */
public class GeneralLabeling extends JPanel implements
ILabelingStrategyPanel, ActionListener {

	private static final long serialVersionUID = 8864709758980903351L;
	   private static final Logger logger = LoggerFactory
           .getLogger(GeneralLabeling.class);
	private static Comparator<Class<? extends ILabelingMethod>> comparator =
			new Comparator<Class<? extends ILabelingMethod>>(){
		public int compare(Class<? extends ILabelingMethod> o1,
				Class<? extends ILabelingMethod> o2) {
			return o1.getName().compareTo(o2.getName());
		}};

	private static TreeMap<
			Class<? extends ILabelingMethod>,
			Class<? extends AbstractLabelingMethodPanel>
		> methods
		= new TreeMap<
			Class<? extends ILabelingMethod>,
			Class<? extends AbstractLabelingMethodPanel>
		>(comparator);

	private JButton btnVisualization;
	private JButton btnPlacement;
	private JComboBox<?> cmbMethod;
	private JPanel methodPanel;
	private IPlacementConstraints placementConstraints;
	private IZoomConstraints zoomConstraints;
	private boolean noEvent;
	private FLyrVect targetLayer;
	private JCheckBox chkAllowLabelOverlapping;
	private FLyrVect auxLayer;
	private GeneralLabelingStrategy gStr;
	/**
	 *
	 */
	public GeneralLabeling() {
		initialize();
	}

	private void initialize() {

		setLayout(new BorderLayout());
		JPanel center = new JPanel(new BorderLayout(10, 10));
		center.setBorder(BorderFactory.createTitledBorder(
				null, Messages.getText("classes")));
		JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,0));
		aux.add(new JLabel(Messages.getText("method")+":"));
		aux.add(getCmbMethod());
		aux.setPreferredSize(new Dimension(605, 40));
		center.add(aux, BorderLayout.NORTH);


		// el panell del m�tode de moltes FeatureDependantLabelingMethod
		methodPanel = getMethodPanel();
		center.add(methodPanel, BorderLayout.CENTER);
		add(center, BorderLayout.CENTER);

		JPanel south = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
		south.setBorder(BorderFactory.createTitledBorder(
				null, Messages.getText("options")));
		south.add(getBtnVisualization());
		south.add(getBtnPlacement());
		south.add(getChkAllowLabelOverlapping());
		south.setPreferredSize(new Dimension(612, 60));

		add(south, BorderLayout.SOUTH);
	}


	private JPanel getMethodPanel() {
		if (methodPanel == null){
			methodPanel = new JPanel(new BorderLayout(10, 0));
		}
		return methodPanel;
	}

	private JCheckBox getChkAllowLabelOverlapping() {
		if (chkAllowLabelOverlapping == null) {
			chkAllowLabelOverlapping = new JCheckBox(
					Messages.getText("allow_label_overlapping"));
			chkAllowLabelOverlapping.addActionListener(this);
		}
		return chkAllowLabelOverlapping;
	}

	private void refreshControls() {
		// fires an event from the methods combo box
		actionPerformed(new ActionEvent(getCmbMethod(), 0, null));
	}

	private JButton getBtnVisualization() {
		if (btnVisualization == null) {
			btnVisualization = new JButton(
					Messages.getText("visualization")+"...");
			btnVisualization.setName("BTNVISUALIZATION");
			btnVisualization.addActionListener(this);
		}
		return btnVisualization;
	}

	private JButton getBtnPlacement() {
		if (btnPlacement == null) {
			btnPlacement = new JButton(
					Messages.getText("placement")+"...");
			btnPlacement.setName("BTNPLACEMENT");
			btnPlacement.addActionListener(this);
		}
		return btnPlacement;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getCmbMethod() {
		if (cmbMethod == null) {
			Iterator<Class<? extends AbstractLabelingMethodPanel>> it = methods.values().iterator();
			ArrayList<AbstractLabelingMethodPanel> panels = new ArrayList<AbstractLabelingMethodPanel>();
			while (it.hasNext()) {
				try {
					panels.add(it.next().newInstance());
				} catch (Exception e) {
					throw new Error(e);
				}
			}
			cmbMethod = new JComboBox(panels.toArray());
			cmbMethod.setSize(new Dimension(300, 22));
			cmbMethod.setName("CMBMETHOD");
			cmbMethod.addActionListener(this);
		}
		return cmbMethod;
	}

	public ILabelingStrategy getLabelingStrategy() {
		GeneralLabelingStrategy resp = GeneralLabelingFactory.
						createStrategy((FLayer) targetLayer,
								getMethod(),
								getPlacementConstraints(),
								getZoomConstraints());

		resp.setAllowOverlapping(getChkAllowLabelOverlapping().isSelected());
		return resp;
	}


	public void setModel(FLayer layer, ILabelingStrategy str) {
		if (layer instanceof FLyrVect) {
			try {
				targetLayer = (FLyrVect) layer;//.cloneLayer();

				auxLayer = (FLyrVect) targetLayer.cloneLayer();
				auxLayer.setParentLayer(layer.getParentLayer());
				auxLayer.setLegend((IVectorLegend)targetLayer.getLegend());
				auxLayer.setProjection(targetLayer.getProjection());

				if (str instanceof GeneralLabelingStrategy) {
					GeneralLabelingStrategy gls = (GeneralLabelingStrategy) str;
					gStr = (GeneralLabelingStrategy) gls.clone();
					auxLayer.setLabelingStrategy(gStr);
					gStr.setLayer(auxLayer);
					setMethod(gStr.getLabelingMethod(), auxLayer);
					placementConstraints = gStr.getPlacementConstraints();
					zoomConstraints = gStr.getZoomConstraints();
					getChkAllowLabelOverlapping().setSelected(gStr.isAllowingOverlap());
				} else {
					ILabelingStrategy ils = ((FLyrVect) layer).getLabelingStrategy();
					if (ils != null) {
						ils = (ILabelingStrategy) LabelClassUtils.clone(ils);
						auxLayer.setLabelingStrategy(ils);
					}
				}
			} catch (Exception e) {
				NotificationManager.addError(
						Messages.getText("While setting model."), e);
			}
			refreshControls();
		}
	}


	/**
	 * @param iLabelingMethodClass
	 */
	public static void addLabelingMethod(Class<? extends AbstractLabelingMethodPanel> iLabelingMethodClass) {
		try {
			methods.put(
					iLabelingMethodClass.newInstance().getLabelingMethodClass(),
					iLabelingMethodClass);
		} catch (Exception e) {
			NotificationManager.addError(
					Messages.getText("cannot_install_labeling_method"), e);
		}
	}

	private void setMethod(ILabelingMethod labelingMethod, FLyrVect srcLayer) {
		getMethodPanel().removeAll();
		AbstractLabelingMethodPanel p;
		try {
			p = methods.get(labelingMethod.getClass()).newInstance();
			p.setModel(labelingMethod, srcLayer);
			cmbMethod.setSelectedItem(p);
		} catch (Exception e) {
			// should be impossible;
			NotificationManager.addWarning(e.getLocalizedMessage());
		}

	}

	private ILabelingMethod getMethod() {
		AbstractLabelingMethodPanel p =
				((AbstractLabelingMethodPanel)cmbMethod.getSelectedItem());
		if (p != null){
			return p.getMethod();
		}

		return SymbologyLocator.getSymbologyManager().createDefaultLabelingMethod();
	}

	private IZoomConstraints getZoomConstraints() {
		if (zoomConstraints == null) {
			zoomConstraints =
					SymbologyLocator.getSymbologyManager().createDefaultZoomConstraints();

		}
		return zoomConstraints;
	}

	private IPlacementConstraints getPlacementConstraints() {
		return placementConstraints;
	}

	public void actionPerformed(ActionEvent e) {

		if (noEvent) return;
		JComponent c = (JComponent)e.getSource();

		if (c.equals(btnPlacement)) {

			try {
				IPlacementConstraints oldValue = getPlacementConstraints();
				IPlacementProperties pp = PlacementProperties.createPlacementProperties(
						getPlacementConstraints(),
						((FLyrVect) auxLayer).getShapeType());

				ApplicationLocator.getManager().getUIManager().addWindow(pp);

				placementConstraints = pp.getPlacementConstraints();

				((AbstractLabelingMethodPanel) cmbMethod.getSelectedItem()).
					propertyChange(new PropertyChangeEvent(
							this,
							AbstractLabelingMethodPanel.PLACEMENT_CONSTRAINTS,
							oldValue,
							placementConstraints));

			} catch (ClassCastException ccEx) {
				NotificationManager.addError(
						"Placement constraints not prepared for:"
						+auxLayer.getClass().getName(),
						ccEx);
			} catch (ReadException dEx) {
				NotificationManager.addError(
						"While getting layer type", dEx);
			}

		} else if (c.equals(btnVisualization)) {
			IZoomConstraints oldValue = getZoomConstraints();
			LabelScaleRange lsr = new LabelScaleRange(oldValue.getMinScale(), oldValue.getMaxScale());
			ApplicationLocator.getManager().getUIManager().addWindow(lsr);
			zoomConstraints =
					SymbologyLocator.getSymbologyManager().createDefaultZoomConstraints();
			zoomConstraints.setMaxScale(lsr.getMaxScale());
			zoomConstraints.setMinScale(lsr.getMinScale());
			zoomConstraints.setMode(
					lsr.getMaxScale() ==-1 && lsr.getMinScale() == -1 ?
							IZoomConstraints.DEFINED_BY_THE_LAYER :
							IZoomConstraints.DEFINED_BY_THE_USER);

			((AbstractLabelingMethodPanel) cmbMethod.getSelectedItem()).
			propertyChange(new PropertyChangeEvent(
					this,
					AbstractLabelingMethodPanel.ZOOM_CONSTRAINTS,
					oldValue,
					getZoomConstraints()));

		} else if (c.equals(chkAllowLabelOverlapping)) {
			boolean newValue = chkAllowLabelOverlapping.isSelected();
			((AbstractLabelingMethodPanel) cmbMethod.getSelectedItem()).
			propertyChange(new PropertyChangeEvent(
					this,
					AbstractLabelingMethodPanel.ALLOW_OVERLAP,
					!newValue,
					newValue));

        } else if (c.equals(getCmbMethod())) {
            AbstractLabelingMethodPanel p = (AbstractLabelingMethodPanel) cmbMethod.getSelectedItem();
            Container cont = methodPanel.getParent();
            cont.remove(methodPanel);
            emptyContainer(methodPanel);

            try {
                if (gStr != null) {
                    if (gStr.getLabelingMethod() != null) {
                        p.setModel(gStr.getLabelingMethod(), auxLayer);
                    }
                } else {
                    p.setModel(getMethod(), auxLayer);
                }
                methodPanel.add(p, BorderLayout.CENTER);

                methodPanel.repaint();
                setVisible(false);
                setVisible(true);
            } catch (Exception e1) {
                NotificationManager.addError(new Date(System.currentTimeMillis()).toString(), e1);
            }
            cont.add(methodPanel, BorderLayout.CENTER);
        }

	}

    private void emptyContainer(Container c) {
        for (int i = 0; i < c.getComponentCount(); i++) {
            c.remove(i);
        }
    }

	public String getLabelingStrategyName() {
		return Messages.getText("user_defined_labels");
	}

	public Class<? extends ILabelingStrategy> getLabelingStrategyClass() {
		return GeneralLabelingStrategy.class;
	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		getBtnPlacement().setEnabled(enabled);
		getBtnVisualization().setEnabled(enabled);
		getChkAllowLabelOverlapping().setEnabled(enabled);
		getCmbMethod().setEnabled(enabled);
		JPanel mp = getMethodPanel();//.setEnabled(enabled);
		mp.setEnabled(enabled);
		for (int i=0; i<mp.getComponentCount(); i++){
			Component c = mp.getComponent(i);
			c.setEnabled(enabled);
		}

	}
}
