/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: FeatureDependentLabeled.java 10671 2007-03-09 08:33:43Z jaume $
* $Log$
* Revision 1.2  2007-03-09 08:33:43  jaume
* *** empty log message ***
*
* Revision 1.1.2.2  2007/02/01 11:42:47  jaume
* *** empty log message ***
*
* Revision 1.1.2.1  2007/01/30 18:10:45  jaume
* start commiting labeling stuff
*
*
*/
package org.gvsig.labeling.label;


import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.IntersectsEnvelopeEvaluator;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.labeling.lang.EvaluatorCreator;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.labeling.symbol.CharacterMarkerSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IMask;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.AndEvaluator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FeatureDependentLabeled implements ILabelingMethod {
	
	public static final String FEATURE_DEP_LABELED_PERSISTENCE_NAME =
			"FEATURE_DEP_LABELED_PERSISTENCE_NAME";

	private static Logger logger =
			LoggerFactory.getLogger(FeatureDependentLabeled.class);
	
	private boolean flagDefinesPriorities;
	private List<ILabelClass> classes = new ArrayList<ILabelClass>();

	public void addLabelClass(ILabelClass lbl) throws IllegalArgumentException{
		if (getLabelClassByName(lbl.getName()) !=null) {
			throw new IllegalArgumentException("A class with the same name already exists!");
		}
		classes.add(lbl);
	}

	public void deleteLabelClass(ILabelClass lbl) {
		classes.remove(lbl);
	}

	public String getClassName() {
		return getClass().getName();
	}

	/*
	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());
		xml.putProperty("definesPriorities", definesPriorities());

		LabelClass[] labels = getLabelClasses();
		if (labels!=null) {
			XMLEntity xmlLabels = new XMLEntity();
			xmlLabels.putProperty("id", "LabelClasses");
			for (int i = 0; i < labels.length; i++) {
				xmlLabels.addChild(labels[i].getXMLEntity());
			}
			xml.addChild(xmlLabels);
		}
		return xml;
	}
	*/

	public ILabelClass[] getLabelClasses() {
		return classes.toArray(new ILabelClass[0]);
	}

	/*
	public void setXMLEntity(XMLEntity xml) {
		if (xml.contains("definesPriorities"))
			setDefinesPriorities(xml.getBooleanProperty("definesPriorities"));

		XMLEntity aux = xml.firstChild("id", "defaultLabelClass");

//		if (aux!=null)
//			defaultLabel = LabelingFactory.createLabelClassFromXML(aux);

		aux = xml.firstChild("id", "LabelClasses");
		if (aux!=null) {
			for (int i = 0; i < aux.getChildrenCount(); i++) {
				addLabelClass(LabelingFactory.
						createLabelClassFromXML(aux.getChild(i)));
			}
		}
	}
	*/

	public boolean allowsMultipleClass() {
		return true;
	}

	public void renameLabelClass(ILabelClass lbl, String newName) {
		ILabelClass label = (ILabelClass) classes.get(classes.indexOf(lbl));
		label.setName(newName);
	}

	public FeatureSet getFeatureIteratorByLabelClass(
			FLyrVect layer,
			ILabelClass lc,
			ViewPort viewPort,
			String[] usedFields) throws DataException {
		
		Envelope vp_env = viewPort.getAdjustedEnvelope();
		IProjection ipro = layer.getProjection();
		FeatureType fty = layer.getFeatureStore().getDefaultFeatureType();
		String geo_field_name = fty.getDefaultGeometryAttributeName();
		
		IntersectsEnvelopeEvaluator iee = new IntersectsEnvelopeEvaluator(
				vp_env, ipro, fty, geo_field_name); 
		
		FeatureStore fsto = layer.getFeatureStore();
		FeatureQuery fq = fsto.createFeatureQuery();
		
		
		if (usedFields != null) {
            fq.setAttributeNames(usedFields);
        }
		
		if (!LabelClassUtils.isUseSqlQuery(lc)) {
			/*
			 * No SQL, use only view port filter
			 */
			fq.addFilter(iee);
			return fsto.getFeatureSet(fq);
		}
		
		/*
		 * There is SQL
		 */
		String sql_str = lc.getSQLQuery();
		if (! LabelClassUtils.validExpression(sql_str, fsto, true)) {
			logger.error("SQL for labeling exists but is not valid: " + sql_str);
		}
		
		Evaluator eval = EvaluatorCreator.getEvaluator(sql_str);
		/*
		 * This will make the view port evaluator be applied first
		 */
		AndEvaluator and_ev = new AndEvaluator(iee);
		and_ev.addEvaluator(eval);
		fq.addFilter(and_ev);
		return fsto.getFeatureSet(fq);
	}

	public boolean definesPriorities() {
		return flagDefinesPriorities;
	}

	public void setDefinesPriorities(boolean flag) {
		if (flag == false) {
			ILabelClass[] lcs = getLabelClasses();
			for (int i = 0; i < lcs.length; i++) {
				lcs[i].setPriority(0);
			}
		}
		flagDefinesPriorities = flag;
	}

	public void clearAllClasses() {
		classes.clear();
	}

	public ILabelClass getLabelClassByName(String labelName) {
		ILabelClass[] classes = getLabelClasses();
		for (int i = 0; i < classes.length; i++) {
			if (classes[i].getName().equals(labelName)) {
				return classes[i];
			}
		}
		return null;
	}

	public ILabelingMethod cloneMethod() {
		
		ILabelingMethod resp = (ILabelingMethod) LabelClassUtils.clone(this);
		return resp;
	}
	
	public static void registerPersistent() {
		
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(FEATURE_DEP_LABELED_PERSISTENCE_NAME)==null ) {
			DynStruct definition = manager.addDefinition(
					FeatureDependentLabeled.class,
					FEATURE_DEP_LABELED_PERSISTENCE_NAME,
					FEATURE_DEP_LABELED_PERSISTENCE_NAME+" Persistence definition",
					null, 
					null
			);
			
			definition.addDynFieldBoolean("definesPriorities").setMandatory(true);
			definition.addDynFieldList("classes").setClassOfItems(ILabelClass.class)
			.setMandatory(true);
		}		
	}	

	public void loadFromState(PersistentState state) throws PersistenceException {
		
		this.setDefinesPriorities(state.getBoolean("definesPriorities"));
		List aux = state.getList("classes");
		ILabelClass item = null;
		Iterator iter = aux.iterator();
		
		try {
			while (iter.hasNext()) {
				item = (ILabelClass) iter.next();
				this.addLabelClass(item);
			}
		} catch (IllegalArgumentException exc) {
			throw new PersistenceException(exc);
		}
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		
		state.set("definesPriorities", flagDefinesPriorities);
		state.set("classes", classes);
		
	}
}
