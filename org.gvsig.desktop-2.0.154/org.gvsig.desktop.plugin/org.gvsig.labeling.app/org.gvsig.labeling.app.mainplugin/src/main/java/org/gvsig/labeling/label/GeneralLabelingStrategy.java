/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
 *
 * $Id: GeneralLabelingStrategy.java 13749 2007-09-17 14:16:11Z jaume $
 * $Log$
 * Revision 1.2  2007-09-17 14:16:11  jaume
 * multilayer symbols sizing bug fixed
 *
 * Revision 1.1  2007/05/22 12:17:41  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2007/05/22 10:05:31  jaume
 * *** empty log message ***
 *
 * Revision 1.10  2007/05/17 09:32:06  jaume
 * *** empty log message ***
 *
 * Revision 1.9  2007/05/09 11:04:58  jaume
 * refactored legend hierarchy
 *
 * Revision 1.8  2007/04/13 11:59:30  jaume
 * *** empty log message ***
 *
 * Revision 1.7  2007/04/12 14:28:43  jaume
 * basic labeling support for lines
 *
 * Revision 1.6  2007/04/11 16:01:08  jaume
 * maybe a label placer refactor
 *
 * Revision 1.5  2007/04/10 16:34:01  jaume
 * towards a styled labeling
 *
 * Revision 1.4  2007/04/02 16:34:56  jaume
 * Styled labeling (start commiting)
 *
 * Revision 1.3  2007/03/28 16:48:01  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2007/03/26 14:40:38  jaume
 * added print method (BUT UNIMPLEMENTED)
 *
 * Revision 1.1  2007/03/20 16:16:20  jaume
 * refactored to use ISymbol instead of FSymbol
 *
 * Revision 1.2  2007/03/09 11:20:57  jaume
 * Advanced symbology (start committing)
 *
 * Revision 1.1  2007/03/09 08:33:43  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.5  2007/02/21 07:34:08  jaume
 * labeling starts working
 *
 * Revision 1.1.2.4  2007/02/15 16:23:44  jaume
 * *** empty log message ***
 *
 * Revision 1.1.2.3  2007/02/09 07:47:05  jaume
 * Isymbol moved
 *
 * Revision 1.1.2.2  2007/02/02 16:21:24  jaume
 * start commiting labeling stuff
 *
 * Revision 1.1.2.1  2007/02/01 17:46:49  jaume
 * *** empty log message ***
 *
 *
 */
package org.gvsig.labeling.label;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.labeling.placements.ILabelPlacement;
import org.gvsig.labeling.placements.LinePlacementConstraints;
import org.gvsig.labeling.placements.MultiShapePlacementConstraints;
import org.gvsig.labeling.placements.PlacementManager;
import org.gvsig.labeling.placements.PointPlacementConstraints;
import org.gvsig.labeling.placements.PolygonPlacementConstraints;
import org.gvsig.labeling.placements.RemoveDuplicatesComparator;
import org.gvsig.labeling.symbol.SmartTextSymbol;
import org.gvsig.labeling.symbol.SymbolUtils;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelLocationMetrics;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;

/**
 *
 * GeneralLabelingStrategy.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Jan 4, 2008
 *
 */
public class GeneralLabelingStrategy implements ILabelingStrategy, Cloneable,
		CartographicSupport {

	private static final Logger logger = LoggerFactory
			.getLogger(GeneralLabelingStrategy.class);

	public static final String GENERAL_LABEL_STRATEGY_PERSISTENCE_NAME = "GENERAL_LABEL_STRATEGY_PERSISTENCE_NAME";

	public static PointPlacementConstraints DefaultPointPlacementConstraints = new PointPlacementConstraints();
	public static LinePlacementConstraints DefaultLinePlacementConstraints = new LinePlacementConstraints();
	public static PolygonPlacementConstraints DefaultPolygonPlacementConstraints = new PolygonPlacementConstraints();

	private static String[] NO_TEXT = { Messages.getText("text_field") };

	private static MultiShapePlacementConstraints DefaultMultiShapePlacementConstratints = new MultiShapePlacementConstraints();

	private ILabelingMethod method;
	private IPlacementConstraints placementConstraints;
	private IZoomConstraints zoomConstraints;

	private boolean allowOverlapping;

	protected FLyrVect layer;

	// private long parseTime;
	private int unit;
	private int referenceSystem;
	// private double sizeAfter;
	private boolean printMode = false; /*
										 * indicate whether output is for a
										 * print product (PDF, PS, ...)
										 */

	private List<Geometry> drawnGeometryLabels;

	public GeneralLabelingStrategy() {
		method = SymbologyLocator.getSymbologyManager()
				.createDefaultLabelingMethod();
	}

	public void setLayer(FLayer layer) {
		FLyrVect l = (FLyrVect) layer;
		this.layer = l;
	}

	public ILabelingMethod getLabelingMethod() {
		return method;
	}

	public void setLabelingMethod(ILabelingMethod method) {
		this.method = method;
	}

	private class GeometryItem {
		public Geometry geom = null;
		public int weigh = 0;
		public double savedPerimeter;

		public GeometryItem(Geometry geom, int weigh) {
			this.geom = geom;
			this.weigh = weigh;
			this.savedPerimeter = 0;
		}
	}

    public void draw(BufferedImage mapImage, Graphics2D mapGraphics, double scale, ViewPort viewPort,
        Cancellable cancel, double dpi) throws ReadException {

        drawnGeometryLabels = new ArrayList<Geometry>(1000);

        int x = (int) viewPort.getOffset().getX();
        int y = (int) viewPort.getOffset().getY();

        // offsets for page generation (PDF, PS, direct printing)
        int print_offset_x = x;
        int print_offset_y = y;
        if (printMode) {
            // for printing, we never offset the labels themselves
            x = 0;
            y = 0;
            printMode = false;
        }

        TreeMap<String[], GeometryItem> labelsToPlace = null;

        String[] usedFields = getUsedFields();

        int notPlacedCount = 0;
        int placedCount = 0;

        /*
         * Get the label placement solvers according the user's settings
         */
        ILabelPlacement placement = PlacementManager.getPlacement(getPlacementConstraints(), layer.getShapeType());

        BufferedImage targetBI;
        Graphics2D targetGr;

        /*
         * get an ordered set of the LabelClasses up on the label priority
         */
        ILabelClass[] lcs = method.getLabelClasses();
        TreeSet<ILabelClass> ts = new TreeSet<ILabelClass>(new LabelClassComparatorByPriority());

        for (int i = 0; i < lcs.length; i++)
            ts.add(lcs[i]);

        if (ts.size() == 0)
            return;

        /*
         * now we have an ordered set, it is only need to give a pass for each
         * label class to render by priorities.
         *
         * If no priorities were defined, the following loop only executes once
         */
        for (ILabelClass lc : ts) {

            if (!lc.isVisible(scale)) {
                /*
                 * Avoid non-visible labels
                 */
                continue;
            }

            FeatureSet fset = null;
            DisposableIterator diter = null;
            try {

                try {
                    fset = method.getFeatureIteratorByLabelClass(layer, lc, viewPort, usedFields);
                } catch (DataException e) {
                    throw new ReadException(layer.getFeatureStore().getProviderName(), e);
                }

                // duplicates treatment stuff
                /* handle the duplicates mode */
                int duplicateMode = getDuplicateLabelsMode();
                if (duplicateMode == IPlacementConstraints.REMOVE_DUPLICATE_LABELS) {
                    // we need to register the labels already placed

                    labelsToPlace = new TreeMap<String[], GeometryItem>(new RemoveDuplicatesComparator());
                }

                boolean bLabelsReallocatable = !isAllowingOverlap();

                BufferedImage overlapDetectImage = null;
                Graphics2D overlapDetectGraphics = null;
                if (bLabelsReallocatable) {
                    int width = viewPort.getImageWidth() + print_offset_x;

                    if (width < 0) {
                        width = 1;
                    }
                    int height = viewPort.getImageHeight() + print_offset_y;
                    if (height < 0) {
                        height = 1;
                    }
                    if (mapImage != null)
                        overlapDetectImage =
                            new BufferedImage(mapImage.getWidth() + print_offset_x, mapImage.getHeight()
                                + print_offset_y, BufferedImage.TYPE_INT_ARGB);
                    else
                        overlapDetectImage =
                            new BufferedImage(viewPort.getImageWidth() + print_offset_x, viewPort.getImageHeight()
                                + print_offset_y, BufferedImage.TYPE_INT_ARGB);

                    overlapDetectGraphics = overlapDetectImage.createGraphics();
                    overlapDetectGraphics.setRenderingHints(mapGraphics.getRenderingHints());
                }
                if (bLabelsReallocatable) {
                    targetBI = overlapDetectImage;
                    targetGr = overlapDetectGraphics;
                    targetGr.translate(-x, -y);
                } else {
                    targetBI = mapImage;
                    targetGr = mapGraphics;
                }

                try {
                    diter = fset.fastIterator();
                } catch (DataException e) {
                    throw new ReadException(layer.getFeatureStore().getProviderName(), e);
                }
                Feature featu = null;
                Geometry geome = null;

                while (!cancel.isCanceled() && diter.hasNext()) {

                    featu = (Feature) diter.next();
                    geome = featu.getDefaultGeometry();
                    if (geome == null || geome.getType() == Geometry.TYPES.NULL) {
                        continue;
                    }

                    if (!setupLabel(featu, lc, cancel, usedFields, viewPort, dpi, duplicateMode)) {
                        continue;
                    }

                    String[] texts = lc.getTexts();

                    if (duplicateMode == IPlacementConstraints.REMOVE_DUPLICATE_LABELS) {
                        // check if this text (so label) is already present in
                        // the map

                        GeometryItem item = labelsToPlace.get(texts);
                        if (item == null) {
                            item = new GeometryItem(geome, 0);
                            labelsToPlace.put(texts, item);
                        }
                        if (item.geom != null) {

                            notPlacedCount++;
                            if (geome.getType() != Geometry.TYPES.POINT) {

                                Envelope auxBox = geome.getEnvelope();
                                double perimeterAux = 2 * (auxBox.getLength(0) + auxBox.getLength(1));
                                if (perimeterAux > item.savedPerimeter) {
                                    item.geom = geome; // FConverter.jts_to_igeometry(jtsGeom);
                                    item.savedPerimeter = perimeterAux;
                                }
                            } else {
                                int weigh = item.weigh;

                                try {
                                    Point pointFromLabel = item.geom.centroid();
                                    Point pointGeome = geome.centroid();
                                    item.geom =
                                        GeometryLocator.getGeometryManager().createPoint(
                                            (pointFromLabel.getX() * weigh + pointGeome.getX()) / (weigh + 1),
                                            (pointFromLabel.getY() * weigh + pointGeome.getY()) / (weigh + 1),
                                            Geometry.SUBTYPES.GEOM2D);
                                } catch (Exception ex) {
                                    throw new ReadException(layer.getFeatureStore().getProviderName(), ex);
                                }

                            }
                        } else {
                            item.geom = geome;
                        }
                        item.weigh++;
                    } else {
                        // Check if size is a pixel
                        if (isOnePoint(viewPort, geome)) {
                            continue;
                        }

                        List<Geometry> geome_parts = new ArrayList<Geometry>();
                        if (duplicateMode == IPlacementConstraints.ONE_LABEL_PER_FEATURE_PART) {
                            geome_parts = getGeometryParts(geome);
                        } else {
                            geome_parts.add(geome);
                        }

                        try {
                            int n = geome_parts.size();
                            for (int k = 0; k < n; k++) {
                                drawLabelInGeom(targetBI, targetGr, lc, placement, viewPort, geome_parts.get(k),
                                    cancel, dpi, bLabelsReallocatable);
                            }
                        } catch (GeometryException e) {
                            throw new ReadException(layer.getFeatureStore().getProviderName(), e);
                        }

                        placedCount++;
                    }
                }

                // ======= End iteration in feature set ====================

                if (duplicateMode == IPlacementConstraints.REMOVE_DUPLICATE_LABELS) {
                    Iterator<String[]> textsIt = labelsToPlace.keySet().iterator();
                    while (!cancel.isCanceled() && textsIt.hasNext()) {
                        notPlacedCount++;
                        String[] texts = textsIt.next();

                        GeometryItem item = labelsToPlace.get(texts);
                        if (item != null) {
                            lc.setTexts(texts);
                            // Check if size is a pixel
                            if (isOnePoint(viewPort, item.geom)) {
                                continue;
                            }

                            try {
                                drawLabelInGeom(targetBI, targetGr, lc, placement, viewPort, item.geom, cancel, dpi,
                                    bLabelsReallocatable);
                            } catch (GeometryException e) {
                                throw new ReadException(layer.getFeatureStore().getProviderName(), e);
                            }
                        }
                    }
                }

                if (bLabelsReallocatable) {
                    targetGr.translate(x, y);
                    if (mapImage != null) {
                        mapGraphics.drawImage(overlapDetectImage, null, null);
                    } else {
                        mapGraphics.drawImage(overlapDetectImage, null, null);
                    }
                }

            } finally {
                if (diter != null) {
                    diter.dispose();
                }
                if (fset != null) {
                    fset.dispose();
                }
            }
        } // big iteration

    }

	private List<Geometry> getGeometryParts(Geometry ge) {

		List<Geometry> resp = new ArrayList<Geometry>();
		if (ge != null) {
			if (ge instanceof MultiPrimitive) {
				MultiPrimitive mp = (MultiPrimitive) ge;
				int n = mp.getPrimitivesNumber();
				for (int i = 0; i < n; i++) {
					resp.add(mp.getPrimitiveAt(i));
				}
			} else {
				resp.add(ge);
			}
		}
		return resp;
	}

	private void drawLabelInGeom(BufferedImage targetBI, Graphics2D targetGr,
			ILabelClass lc, ILabelPlacement placement, ViewPort viewPort,
			Geometry geom, Cancellable cancel, double dpi,
			boolean bLabelsReallocatable) throws GeometryException {

		lc.toCartographicSize(viewPort, dpi, null);
		ArrayList<LabelLocationMetrics> llm = null;
		llm = placement.guess(lc, geom, getPlacementConstraints(), 0, cancel,
				viewPort);

		setReferenceSystem(lc.getReferenceSystem());
		setUnit(lc.getUnit());

		/*
		 * search if there is room left by the previous and with more priority
		 * labels, then check the current level
		 */
		lookupAndPlaceLabel(targetBI, targetGr, llm, placement, lc, geom,
				viewPort, cancel, bLabelsReallocatable);

	}

	private int getDuplicateLabelsMode() {
		if (getPlacementConstraints() == null) {
			return IPlacementConstraints.DefaultDuplicateLabelsMode;
		}
		return getPlacementConstraints().getDuplicateLabelsMode();
	}

	private boolean lookupAndPlaceLabel(BufferedImage bi, Graphics2D g,
			ArrayList<LabelLocationMetrics> llm, ILabelPlacement placement,
			ILabelClass lc, Geometry geom, ViewPort viewPort,
			Cancellable cancel, boolean bLabelsReallocatable)
			throws GeometryException {

	    GeometryManager gm = GeometryLocator.getGeometryManager();
		int i;
		for (i = 0; !cancel.isCanceled() && i < llm.size(); i++) {
			LabelLocationMetrics labelMetrics = llm.get(i);

			IPlacementConstraints pc = getPlacementConstraints();
			if (pc instanceof MultiShapePlacementConstraints) {
				MultiShapePlacementConstraints mpc = (MultiShapePlacementConstraints) pc;

				GeometryType geom_gt = null;

				geom_gt = gm.getGeometryType(geom.getType(), SUBTYPES.GEOM2D);

				if (geom_gt.getType() == TYPES.POINT
						|| geom_gt.getType() == TYPES.MULTIPOINT) {
					pc = mpc.getPointConstraints();
				} else {
					if (geom_gt.isTypeOf(TYPES.CURVE)
							|| geom_gt.getType() == TYPES.MULTICURVE) {
						pc = mpc.getLineConstraints();
					} else {
						if (geom_gt.isTypeOf(TYPES.SURFACE)
								|| geom_gt.getType() == TYPES.MULTISURFACE) {
							pc = mpc.getPolygonConstraints();
						}
					}
				}
			}

			/*
			 * Ver comentario en el metodo drawLabelInGeom
			 */
			if (bLabelsReallocatable) {

				Geometry aux_geom = null;
				aux_geom = lc.getShape(labelMetrics);

				if (!isOverlapping(bi, aux_geom)) {

					if (!pc.isFollowingLine()) {
						lc.draw(g, labelMetrics, geom);
					} else {

						ILabelClass smsLc = new SmartTextSymbolLabelClass();
						SmartTextSymbol sms = new SmartTextSymbol(
								lc.getTextSymbol(), pc);

						double sizeBefore = lc.getTextSymbol().getFont()
								.getSize();
						double sizeAfter = SymbolUtils.getCartographicLength(
								this, sizeBefore, viewPort, viewPort.getDPI());
						sms.setFontSize(sizeAfter);

						smsLc.setTextSymbol(sms);
						geom.transform(viewPort.getAffineTransform());
						smsLc.draw(g, null, geom);
						sms.setFontSize(sizeBefore);

					}
					return true;
				}
			} else {
				if (!pc.isFollowingLine()) {
					lc.draw(g, labelMetrics, null);
				} else {
					ILabelClass smsLc = new SmartTextSymbolLabelClass();
					SmartTextSymbol sms = new SmartTextSymbol(
							lc.getTextSymbol(), pc);

					double sizeBefore = lc.getTextSymbol().getFont().getSize();
					double sizeAfter = SymbolUtils.getCartographicLength(this,
							sizeBefore, viewPort, viewPort.getDPI());
					sms.setFontSize(sizeAfter);

					smsLc.setTextSymbol(sms);
					geom.transform(viewPort.getAffineTransform());
					smsLc.draw(g, null, geom);

					sms.setFontSize(sizeBefore);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Divide una cadena de caracteres por el caracter dos puntos siempre que no
	 * est� entre comillas.
	 *
	 * @param str
	 *            Cadena de caracteres
	 *
	 * @return String[]
	 *
	 */
	private String[] divideExpression(String str) {
		ArrayList<String> r = new ArrayList<String>();
		boolean inQuotationMarks = false;
		int lastIndex = 0;
		for (int i = 0; i < str.length(); i++) {
			String currentChar = str.substring(i, i + 1);
			if (currentChar.compareTo("\"") == 0 ) {
				inQuotationMarks = !inQuotationMarks;
				// Si es el cierre de las comillas
				if(!inQuotationMarks){
					r.add(str.substring(lastIndex, i + 1).replace("\"", "'"));
					lastIndex = i + 1;
				}
			}
			if (currentChar.compareTo(":") == 0
					&& !inQuotationMarks) {
				if (lastIndex < i) {
					r.add(str.substring(lastIndex, i));
				}
				lastIndex = i + 1;
			}
		}
		if (lastIndex < str.length() - 1) {
			r.add(str.substring(lastIndex));
		}
		String[] result = new String[r.size()];
		r.toArray(result);
		return result;
	}

	/**
	 * Compute the texts to show in the label and store them in LabelClass.
	 */
	@SuppressWarnings("unchecked")
	private boolean setupLabel(Feature featu, ILabelClass lc,
			Cancellable cancel, String[] usedFields, ViewPort viewPort,
			double dpi, int duplicateMode) {

		String expr = lc.getStringLabelExpression();

		long pt1 = System.currentTimeMillis();
		String[] texts = NO_TEXT;
		List<String> preTexts = new ArrayList<String>();
		try {
			if (expr != null) {

				if (expr.equals("")) {
					expr = texts[0];
				}

				String[] multiexpr = divideExpression(expr);
				for (int i = 0; i < multiexpr.length; i++) {

					expr = multiexpr[i];
					Object res = LabelClassUtils.evaluate(expr,
							featu.getEvaluatorData());
					if (res != null) {
						preTexts.add(res.toString());
					} else {
						preTexts.add("");
					}
				}
				texts = new String[preTexts.size()];
				preTexts.toArray(texts);
				// parseTime += System.currentTimeMillis()-pt1;
			}
			lc.setTexts(texts);

		} catch (Exception e) {
			logger.warn("While setting up label", e);
			return false;
		}
		return true;
	}

    private boolean isOverlapping(BufferedImage bi, Geometry lblgeom) {

        for (Iterator iterator = drawnGeometryLabels.iterator(); iterator.hasNext();) {
            Geometry drawnGeometry = (Geometry) iterator.next();
            try {
                if (drawnGeometry.intersects(lblgeom)) {
                    return true;
                }
            } catch (GeometryOperationNotSupportedException | GeometryOperationException e) {
                logger.warn("Can't check overlapping geometry");
            }
        }
        drawnGeometryLabels.add(lblgeom);
        return false;

    }

	private boolean isOnePoint(ViewPort viewPort, Geometry geom) {

		boolean onePoint = false;
		int shapeType = geom.getType();

		if (shapeType != TYPES.POINT && shapeType != TYPES.MULTIPOINT) {

			Envelope env = geom.getEnvelope();
			double dist1Pixel = viewPort.getDist1pixel();
			onePoint = (env.getLength(0) <= dist1Pixel && env.getLength(1) <= dist1Pixel);
		}
		return onePoint;
	}

	public boolean isAllowingOverlap() {
		return allowOverlapping;
	}

	public void setAllowOverlapping(boolean allowOverlapping) {
		this.allowOverlapping = allowOverlapping;
	}

	public IPlacementConstraints getPlacementConstraints() {
		if (placementConstraints != null)
			return placementConstraints;

		GeometryType gt = null;

		try {
			gt = layer.getGeometryType();
			// force 2d for comparison
			gt = GeometryLocator.getGeometryManager().getGeometryType(
					gt.getType(), SUBTYPES.GEOM2D);
		} catch (Exception e) {
			logger.error("While getting placements constraints.", e);
			return null;
		}

		if (gt.isTypeOf(TYPES.POINT) || gt.isTypeOf(TYPES.MULTIPOINT)) {
			return DefaultPointPlacementConstraints;
		} else {
			if (gt.isTypeOf(TYPES.CURVE) || gt.isTypeOf(TYPES.MULTICURVE)) {
				return DefaultLinePlacementConstraints;
			} else {
				if (gt.isTypeOf(TYPES.SURFACE)
						|| gt.isTypeOf(TYPES.MULTISURFACE)) {
					return DefaultPolygonPlacementConstraints;
				} else {
					if (gt.isTypeOf(TYPES.AGGREGATE)
							|| gt.isTypeOf(TYPES.GEOMETRY)) {
						DefaultMultiShapePlacementConstratints
								.setPointConstraints(DefaultPointPlacementConstraints);
						DefaultMultiShapePlacementConstratints
								.setLineConstraints(DefaultLinePlacementConstraints);
						DefaultMultiShapePlacementConstratints
								.setPolygonConstraints(DefaultPolygonPlacementConstraints);
						return DefaultMultiShapePlacementConstratints;
					}
				}
			}
		}
		return null;
	}

	public void setPlacementConstraints(IPlacementConstraints constraints) {
		this.placementConstraints = constraints;
	}

	public IZoomConstraints getZoomConstraints() {
		return zoomConstraints;
	}

	public void setZoomConstraints(IZoomConstraints constraints) {
		this.zoomConstraints = constraints;
	}

	public void print(Graphics2D g, double scale, ViewPort viewPort,
			Cancellable cancel, PrintAttributes properties)
			throws ReadException {

		double dpi = 100;
		int pq = properties.getPrintQuality();
		if (pq == PrintAttributes.PRINT_QUALITY_NORMAL) {
			dpi = 300;
		} else if (pq == PrintAttributes.PRINT_QUALITY_HIGH) {
			dpi = 600;
		} else if (pq == PrintAttributes.PRINT_QUALITY_DRAFT) {
			dpi = 72;
		}

		viewPort.setOffset(new Point2D.Double(0, 0));

		/* signal printing output */
		printMode = true;

		draw(null, g, scale, viewPort, cancel, dpi);
	}

	public String[] getUsedFields() {

		/*
		 * TODO Solve the problem with the [ and ]. Currently SQLJEP evaluator
		 * cannot tell which fields are being used. Options: allow [] and remove
		 * them or maybe while parsing the SQLJEP evaluator can inform with
		 * events like "I have found a field"
		 */

		FeatureAttributeDescriptor[] atts = null;
		try {
			atts = layer.getFeatureStore().getDefaultFeatureType()
					.getAttributeDescriptors();
		} catch (DataException e) {
			logger.error("While getting atributes.", e);
		}

		int n = atts.length;
		String[] resp = new String[n];
		for (int i = 0; i < n; i++) {
			resp[i] = atts[i].getName();
		}
		return resp;

	}

	public boolean shouldDrawLabels(double scale) {
		double minScaleView = -1;
		double maxScaleView = -1;

		if (zoomConstraints != null) {
			minScaleView = zoomConstraints.getMinScale();
			maxScaleView = zoomConstraints.getMaxScale();
		}

		if (minScaleView == -1 && maxScaleView == -1) {
			// parameters not set, so the layer decides.
			return layer.isWithinScale(scale);
		}

		if (minScaleView >= scale) {
			return (maxScaleView != -1) ? maxScaleView <= scale : true;
		}

		return false;
	}

	public void setUnit(int unitIndex) {
		unit = unitIndex;

	}

	public int getUnit() {
		return unit;
	}

	public int getReferenceSystem() {
		return referenceSystem;
	}

	public void setReferenceSystem(int referenceSystem) {
		this.referenceSystem = referenceSystem;
	}

	public static void registerPersistent() {

		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if (manager.getDefinition(GENERAL_LABEL_STRATEGY_PERSISTENCE_NAME) == null) {
			DynStruct definition = manager.addDefinition(
					GeneralLabelingStrategy.class,
					GENERAL_LABEL_STRATEGY_PERSISTENCE_NAME,
					GENERAL_LABEL_STRATEGY_PERSISTENCE_NAME
							+ " Persistence definition", null, null);
			definition.addDynFieldObject("labelingMethod")
					.setClassOfValue(ILabelingMethod.class).setMandatory(true);
			definition.addDynFieldObject("placementConstraints")
					.setClassOfValue(IPlacementConstraints.class)
					.setMandatory(false);
			definition.addDynFieldObject("zoomConstraints")
					.setClassOfValue(IZoomConstraints.class)
					.setMandatory(false);

			definition.addDynFieldBoolean("allowOverlapping")
					.setMandatory(true);
			definition.addDynFieldInt("unit").setMandatory(true);
			definition.addDynFieldInt("referenceSystem").setMandatory(true);
		}
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {

		method = (ILabelingMethod) state.get("labelingMethod");

		if (state.hasValue("placementConstraints")) {
			placementConstraints = (IPlacementConstraints) state
					.get("placementConstraints");
		}

		if (state.hasValue("zoomConstraints")) {
			zoomConstraints = (IZoomConstraints) state.get("zoomConstraints");
		}

		this.allowOverlapping = state.getBoolean("allowOverlapping");
		this.unit = state.getInt("unit");
		this.referenceSystem = state.getInt("referenceSystem");
	}

	public void saveToState(PersistentState state) throws PersistenceException {

		state.set("labelingMethod", method);

		if (placementConstraints != null) {
			state.set("placementConstraints", placementConstraints);
		}

		if (zoomConstraints != null) {
			state.set("zoomConstraints", zoomConstraints);
		}

		state.set("allowOverlapping", allowOverlapping);
		state.set("unit", unit);
		state.set("referenceSystem", referenceSystem);

	}

	public double toCartographicSize(ViewPort vp, double dpi, Geometry geom) {
		/*
		 * This method is not used but we must implement CartographicSupport
		 */
		return 0;
	}

	public void setCartographicSize(double cartographicSize, Geometry geom) {
		/*
		 * This method is not used but we must implement CartographicSupport
		 */
	}

	public double getCartographicSize(ViewPort vp, double dpi, Geometry geom) {
		/*
		 * This method is not used but we must implement CartographicSupport
		 */
		return 0;
	}

	public Object clone() throws CloneNotSupportedException {
		return LabelClassUtils.clone(this);
	}

}
