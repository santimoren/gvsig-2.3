/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.labeling.gui.layerproperties;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.Collection;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import org.gvsig.app.gui.labeling.LabelClassEditor;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClassFactory;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.gui.beans.swing.celleditors.BooleanTableCellEditor;
import org.gvsig.gui.beans.swing.cellrenderers.BooleanTableCellRenderer;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.label.FeatureDependentLabeled;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureDependent extends AbstractLabelingMethodPanel implements ActionListener{
        private static final Logger logger = LoggerFactory.getLogger(FeatureDependent.class);

	private static final long serialVersionUID = 5493451803343695650L;
	private static int NAME_FIELD_INDEX = 0;
	private static int PREVIEW_FIELD_INDEX = 1;
	private static int FILTER_FIELD_INDEX = 2;
	private static int LABEL_EXPRESSION_FIELD_INDEX = 3;
	private static int VISIBLE_FIELD_INDEX = 4;
	JTable tblClasses = null;
	private JButton btnMoveUpClass;
	private JButton btnAddClass;
	private JCheckBox chkLabel;
	private JCheckBox chkDefinePriorities;
	private JScrollPane scrlPan;
	private boolean openEditor = false;
	private JButton btnMoveDownClass;
	private JButton btnDelClass;

    private JPanel panel;


	/*
	private String[] fieldNames;
	private int[] fieldTypes;
	*/
	private JPanel buttonPanel;
    private JComboBox comboLabelClassFactoryToUse;

	@Override
	public String getName() {

		return Messages.getText(
				"define_classes_of_features_and_label_each_differently")+".";
	}

	@Override
	public Class<? extends ILabelingMethod> getLabelingMethodClass() {
		return FeatureDependentLabeled.class;
	}


	private JCheckBox getChkDefinePriorities() {
		if (chkDefinePriorities == null) {
			chkDefinePriorities = new JCheckBox(Messages.getText("label_priority"));
			chkDefinePriorities.addActionListener(this);
			chkDefinePriorities.setName("CHK_DEFINE_PRIORITIES");
		}
		return chkDefinePriorities;
	}


	private JButton getBtnDelClass() {
		if (btnDelClass == null) {
			btnDelClass = new JButton(Messages.getText("delete"));
			btnDelClass.setName("BTNDELCLASS");
			btnDelClass.addActionListener(this);
		}
		return btnDelClass;
	}

	private JButton getBtnAddClass() {
		if (btnAddClass == null) {
			btnAddClass = new JButton(Messages.getText("add"));
			btnAddClass.setName("BTNADDCLASS");
			btnAddClass.addActionListener(this);
		}
		return btnAddClass;
	}

	private JButton getBtnMoveUpClass() {
		if (btnMoveUpClass == null) {
			btnMoveUpClass = new JButton(Messages.getText("move_up"));
			btnMoveUpClass.setName("BTNMOVEUPCLASS");
			btnMoveUpClass.addActionListener(this);
		}
		return btnMoveUpClass;
	}

	private JButton getBtnMoveDownClass() {
		if (btnMoveDownClass == null) {
			btnMoveDownClass = new JButton(Messages.getText("move_down"));
			btnMoveDownClass.setName("BTNMOVEDOWNCLASS");
			btnMoveDownClass.addActionListener(this);
		}
		return btnMoveDownClass;
	}

	private JScrollPane getCenterScrl() {

			scrlPan = new JScrollPane(getTblClasses());
			scrlPan.setPreferredSize(new Dimension(180, 300));

		return scrlPan;
	}


	private JTable getTblClasses() {

			tblClasses = new JTable(new LabelClassTableModel());
		tblClasses.setRowHeight(50);
		tblClasses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblClasses.addMouseListener(new MouseAdapter() {
			int prevRow =-1;
			@Override
			public void mouseClicked(MouseEvent e) {
					if (!tblClasses.isEnabled()){
						return;
					}
					int col = tblClasses.getColumnModel().getColumnIndexAtX(e.getX());
					int row = (int) ((e.getY()-tblClasses.getLocation().getY()) / tblClasses.getRowHeight());
					if(!(row > tblClasses.getRowCount()-1 || col > tblClasses.getColumnCount()-1))
					{
						openEditor = (row == prevRow && e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2
						) ;
						prevRow = row;
						if (openEditor)
							tblClasses.editCellAt(row, col);
					}
				}
		});

		tblClasses.getModel().addTableModelListener(new TableModelListener() {

			public void tableChanged(TableModelEvent e) {
				if (!tblClasses.isEnabled()){
					return;
				}

				if(e.getColumn() == VISIBLE_FIELD_INDEX){
					ILabelClass oldLc = (ILabelClass) tblClasses.getValueAt(e.getFirstRow(), PREVIEW_FIELD_INDEX);
					oldLc.setVisible(Boolean.valueOf(tblClasses.getValueAt(e.getFirstRow(), VISIBLE_FIELD_INDEX).toString()));
				}
			}

		});

		TableColumnModel cm = tblClasses.getColumnModel();

		tblClasses.getColumnModel().getColumn(PREVIEW_FIELD_INDEX).setCellRenderer(new TableCellRenderer() {

			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				LabelClassPreview lcPr = new LabelClassPreview();
				lcPr.setLabelClass((ILabelClass) value);
				return lcPr;
			}

		});
		tblClasses.getColumnModel().getColumn(VISIBLE_FIELD_INDEX).setCellRenderer(new BooleanTableCellRenderer(false));
		tblClasses.getColumnModel().getColumn(LABEL_EXPRESSION_FIELD_INDEX).setCellRenderer(new TableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				String expr = null;
				if (value != null)
					expr = (String) value;
				if (expr == null)
					expr = "";
				// LabelExpressionParser.tokenFor(LabelExpressionParser.EOEXPR);

//				expr = expr.replaceAll(LabelExpressionParser.tokenFor(LabelExpressionParser.EOFIELD), " | ");
				// expr = expr.substring(0, expr.length()-1);
				return new JLabel("<html><p>"+expr+"</p></html>", JLabel.CENTER);
			}
		});

		// the editors

		for (int i = 0; i < tblClasses.getColumnModel().getColumnCount(); i++) {
			if (i!= VISIBLE_FIELD_INDEX) {
				tblClasses.getColumnModel().getColumn(i).setCellEditor(
						new LabelClassCellEditor(this.layer.getFeatureStore()));
			} else {
				tblClasses.getColumnModel().getColumn(VISIBLE_FIELD_INDEX).
				setCellEditor(new BooleanTableCellEditor(tblClasses));
			}
		}
		((DefaultTableModel)tblClasses.getModel()).fireTableDataChanged();
		repaint();

		return tblClasses;
	}

        private class LabelClassCellEditor extends AbstractCellEditor implements TableCellEditor {

            private static final long serialVersionUID = 6399823783851437400L;
            private FeatureStore fsto = null;

            public LabelClassCellEditor(FeatureStore store) {
                fsto = store;
            }

            public Component getTableCellEditorComponent(
                    JTable table, Object value, boolean isSelected, int row, int column) {

                if (openEditor) {
                    SymbologySwingManager manager = SymbologySwingLocator.getSwingManager();
                    ILabelClass oldLc = (ILabelClass) tblClasses.getValueAt(row, PREVIEW_FIELD_INDEX);
                    oldLc.setVisible(Boolean.valueOf(tblClasses.getValueAt(row, VISIBLE_FIELD_INDEX).toString()));
                    LabelClassEditor lcProp = manager.createLabelClassEditor(oldLc,fsto);
                    oldLc.setTexts(new String[]{oldLc.getName()});

                    lcProp.showDialog();
                    if( !lcProp.isAccepted() ) {
                        return null;
                    }
                    ILabelClass newLc = lcProp.getLabelClass();

                    LabelClassTableModel m = (LabelClassTableModel) tblClasses.getModel();
                    Boolean changeDone = false;

                    if (!(oldLc.getName().equals(newLc.getName()))) {
                        if (!checSameLablClassName(m, newLc.getName())) {

                            m.setValueAt(newLc.getStringLabelExpression(), row, LABEL_EXPRESSION_FIELD_INDEX);
                            m.setValueAt(newLc.getName(), row, NAME_FIELD_INDEX);
                            m.setValueAt(newLc, row, PREVIEW_FIELD_INDEX);
                            m.setValueAt(newLc.getSQLQuery(), row, FILTER_FIELD_INDEX);
                            m.setValueAt(newLc.isVisible(), row, VISIBLE_FIELD_INDEX);
                            fireEditingStopped(); //Make the renderer reappear.
                            changeDone = true;
                        } else {
                            JOptionPane.showMessageDialog(tblClasses,
                                    Messages.getText(
                                            "cannot_exist_two_label_classes_with_the_same_name")
                                    + "\n",
                                    Messages.getText("error"),
                                    JOptionPane.ERROR_MESSAGE);
                            changeDone = true;
                        }
                    }
                    if (!changeDone) {
                        m.setValueAt(newLc.getStringLabelExpression(), row, LABEL_EXPRESSION_FIELD_INDEX);
                        m.setValueAt(newLc.getName(), row, NAME_FIELD_INDEX);
                        m.setValueAt(newLc, row, PREVIEW_FIELD_INDEX);
                        m.setValueAt(newLc.getSQLQuery(), row, FILTER_FIELD_INDEX);
                        m.setValueAt(newLc.isVisible(), row, VISIBLE_FIELD_INDEX);
                        fireEditingStopped(); //Make the renderer reappear.
                        changeDone = true;
                    }
                }

                method.clearAllClasses();
                ILabelClass[] classes = ((LabelClassTableModel) tblClasses.getModel()).toLabelClassArray();
                for (int i = 0; i < classes.length; i++) {
                    method.addLabelClass(classes[i]);
                }

                openEditor = false;
                return null;
            }

            public Object getCellEditorValue() {
                return null;
            }

        }


	private boolean checSameLablClassName(LabelClassTableModel mod, String name) {
		for (int i = 0; i < mod.getRowCount(); i++) {
			if(name.equals(mod.getLabelAtRow(i).getName()))
				return true;
		}
		return false;
	}


	private class LabelClassTableModel extends DefaultTableModel {

		private static final long serialVersionUID = -9152998982339430209L;
		Object[][] values;

		private String[] classesTableFieldNames = new String[] {
				Messages.getText("name"),
				Messages.getText("preview"),
				Messages.getText("filter"),
				Messages.getText("label_expression"),
				Messages.getText("visible"),
		};


		public LabelClassTableModel() {
			super();
			ILabelClass[] labelClasses = method.getLabelClasses();

			values = new Object[labelClasses.length][classesTableFieldNames.length];
			for (int i = 0; i < values.length; i++) {
				values[i][PREVIEW_FIELD_INDEX] = labelClasses[i];
				values[i][NAME_FIELD_INDEX] = labelClasses[i].getName();
				values[i][FILTER_FIELD_INDEX] = labelClasses[i].getSQLQuery();
				values[i][LABEL_EXPRESSION_FIELD_INDEX] = labelClasses[i].getStringLabelExpression();
				values[i][VISIBLE_FIELD_INDEX] = labelClasses[i].isVisible();
			}
			setDataVector(values, classesTableFieldNames);
		}

		public String getColumnName(int col) {
			return classesTableFieldNames[col];
		}

		public int getColumnCount() {
			return classesTableFieldNames.length;
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return true;
		}

		public ILabelClass getLabelAtRow(int row) {
			return (ILabelClass) getValueAt(row, PREVIEW_FIELD_INDEX);
		}

		public ILabelClass[] toLabelClassArray() {
			ILabelClass[] classes = new ILabelClass[getRowCount()];
			for (int i = 0; i < classes.length; i++) {
				classes[i] = getLabelAtRow(i);
				if (getChkDefinePriorities().isSelected()) {
					classes[i].setPriority(i);
				}
			}
			return classes;
		}

		public void setClassArray(ILabelClass[] classes) {
			for (int i = 0; i < classes.length; i++) {
					setValueAt(classes[i],i,PREVIEW_FIELD_INDEX);
					setValueAt(classes[i].getName(),i,NAME_FIELD_INDEX);
					setValueAt(classes[i].getSQLQuery(),i,FILTER_FIELD_INDEX);
					setValueAt(classes[i].getStringLabelExpression(),i,LABEL_EXPRESSION_FIELD_INDEX);
					setValueAt(classes[i].isVisible(),i,VISIBLE_FIELD_INDEX);
			}

		}

	}

	@Override
	protected void initializePanel() {
			setLayout(new BorderLayout());
			add(getPanel(),BorderLayout.CENTER);
		}

    private JPanel getPanel() {
        if (this.panel == null) {
            this.panel = new JPanel(new BorderLayout());

            GridLayout gl = new GridLayout(4, 1);
            buttonPanel = new JPanel(gl);
            buttonPanel.add(getBtnAddClass());
            buttonPanel.add(getBtnDelClass());
            // buttonPanel.addComponent(new JBlank(10, 10));
            buttonPanel.add(getBtnMoveUpClass());
            buttonPanel.add(getBtnMoveDownClass());

            JPanel auxp = new JPanel();
            auxp.add(buttonPanel);

            this.panel.add(auxp, BorderLayout.EAST);
            this.panel.add(getChkDefinePriorities(), BorderLayout.NORTH);

            JPanel centro = new JPanel(new BorderLayout());
            centro.add(getLabelClassSelector(), BorderLayout.NORTH);
            centro.add(getCenterScrl(), BorderLayout.CENTER);
            panel.add(centro, BorderLayout.CENTER);

        }
        return this.panel;

    }


        private JPanel getLabelClassSelector() {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            SymbologyManager symbologyMamanger = SymbologyLocator.getSymbologyManager();

            JPanel panel = new JPanel(new BorderLayout());
            panel.add(new JLabel(i18nManager.getTranslation("_Label_type_to_use_XcolonX")), BorderLayout.WEST);

            DefaultComboBoxModel model = new DefaultComboBoxModel();
            Collection<ILabelClassFactory> factories = symbologyMamanger.getLabelClassFactories();
            for( ILabelClassFactory factory : factories ) {
                model.addElement(factory);
            }
            this.comboLabelClassFactoryToUse = new JComboBox(model);
            panel.add(comboLabelClassFactoryToUse, BorderLayout.CENTER);
            if( model.getSize()>0 ) {
                comboLabelClassFactoryToUse.setSelectedIndex(0);
            }
            return panel;
        }

	public void fillPanel(FeatureType fty)
	throws ReadException {

		chkDefinePriorities.setSelected(this.method.definesPriorities());
		repaint();
	}

	private void swapClass(int classIndex, int targetPos,int numOfElements) {

		ILabelClass[] classes = ((LabelClassTableModel)tblClasses.getModel()).toLabelClassArray();
		ILabelClass aux = classes[targetPos];
		classes[targetPos] = classes[classIndex];
		classes[classIndex] = aux;
		((LabelClassTableModel)tblClasses.getModel()).setClassArray(classes);
	}

        public void actionPerformed(ActionEvent e) {
            if (method == null) {
                return;
            }

            JComponent c = (JComponent) e.getSource();
            LabelClassTableModel mod = ((LabelClassTableModel) tblClasses.getModel());

            if (c.equals(btnAddClass)) {
                ILabelClassFactory factory = (ILabelClassFactory) this.comboLabelClassFactoryToUse.getSelectedItem();
                if( factory == null ) {
                    logger.warn("Can't acces to the defaut selected labelclassfactory.");
                    return;
                }
                ILabelClass newClass = factory.create();

                String name = Messages.getText("labeling")
                        + String.valueOf(1);

                int count = 0;
                while (checSameLablClassName(mod, name)) {
                    count++;
                    name = Messages.getText("labeling")
                            + String.valueOf(count);
                }
                newClass.setName(name);
                mod.addRow(new Object[]{
                    newClass.getName(),
                    newClass,
                    newClass.getSQLQuery(),
                    newClass.getStringLabelExpression(),
                    newClass.isVisible()});

            } else if (c.equals(btnDelClass)) {
                if (mod.getRowCount() >= 1) {
                    int[] sRows = tblClasses.getSelectedRows();

                    for (int i = sRows.length - 1; i >= 0; i--) {
                        mod.removeRow(sRows[i]);
                    }
                }
            } else if (c.equals(chkDefinePriorities)) {

                method.setDefinesPriorities(chkDefinePriorities.isSelected());

            } else if (c.equals(chkLabel)) {
                int[] sRows = tblClasses.getSelectedRows();
                for (int i = sRows.length - 1; i >= 0; i--) {
                    ILabelClass lc = mod.getLabelAtRow(i);
                    lc.setVisible(chkLabel.isSelected());
                }

            } else if (c.equals(btnMoveUpClass)) {
                int[] indices = tblClasses.getSelectedRows();
                if (indices.length > 0) {
                    int classIndex = indices[0];
                    int targetPos = Math.max(0, classIndex - 1);
                    swapClass(classIndex, targetPos, indices.length);
                }

            } else if (c.equals(btnMoveDownClass)) {
                int[] indices = tblClasses.getSelectedRows();
                if (indices.length > 0) {
                    int classIndex = indices[indices.length - 1];
                    int targetPos = Math.min(tblClasses.getRowCount() - 1, classIndex + 1);
                    swapClass(classIndex, targetPos, indices.length);
                }

            } else if (c.equals(btnDelClass)) {
                int[] indices = tblClasses.getSelectedRows();
                if (indices.length > 0) {
                    int classIndex = indices[0];
                    int targetPos = Math.min(tblClasses.getRowCount(), classIndex);
                    swapClass(classIndex, targetPos, indices.length);
                }
            }

            mod.fireTableDataChanged();
            method.clearAllClasses();
            ILabelClass[] classes = ((LabelClassTableModel) tblClasses.getModel()).toLabelClassArray();
            for (int i = 0; i < classes.length; i++) {
                method.addLabelClass(classes[i]);
            }
            repaint();
        }

	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		getBtnAddClass().setEnabled(enabled);
		getBtnDelClass().setEnabled(enabled);
		getBtnMoveDownClass().setEnabled(enabled);
		getBtnMoveUpClass().setEnabled(enabled);
		getChkDefinePriorities().setEnabled(enabled);
		scrlPan.setEnabled(enabled);
		tblClasses.setEnabled(enabled);
	}






}
