/* gvSIG. Sistema de Información Geográfica de la Generalitat Valenciana
*
* Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
*/
package org.gvsig.labeling.gui.styling;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Stack;
import java.util.logging.Level;
import org.gvsig.fmap.dal.exception.DataException;

import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelable;
import org.gvsig.gui.beans.imagenavigator.IClientImageNavigator;
import org.gvsig.gui.beans.imagenavigator.ImageNavigator;
import org.gvsig.gui.beans.imagenavigator.ImageUnavailableException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.task.Cancellable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LayerPreview extends ImageNavigator implements IClientImageNavigator {
	private static final Logger logger = LoggerFactory.getLogger(LayerPreview.class);
                
	private static final long serialVersionUID = -7554322442540714114L;
	private FLayer layer = null;
	private ViewPort vp;
	private Stack<Cancellable> cancels = new Stack<Cancellable>();
	private static GeometryManager geoman = GeometryLocator.getGeometryManager();

	public LayerPreview() {
		super(null);
		setClientImageNavigator(this);
	}

	public void setLayer(FLayer layer) throws ReadException {
		
		this.layer = layer;
		if (layer != null) {
			vp = new ViewPort(layer.getProjection());
			Envelope env = layer.getFullEnvelope();
			setViewDimensions(
					env.getMinimum(0),
					env.getMaximum(1),
					env.getMaximum(0),
					env.getMinimum(1));
		}
		updateBuffer();
		setEnabled(true);
	}

	/*
	public static void main(String[] args) {
		JFrame jFrame = new JFrame("test");
		String fwAndamiDriverPath = "../_fwAndami/gvSIG/extensiones/com.iver.cit.gvsig/drivers";
		IProjection PROJ = CRSFactory.getCRS("EPSG:23030");
		try {
			LayerFactory.setDriversPath(new File(fwAndamiDriverPath).getAbsolutePath());
			LayerPreview prev = new LayerPreview();

			prev.setLayer(LayerFactory.
					createLayer(
							"line",
							"gvSIG shp driver",
							new File("/home/jaume/Documents/Cartografia/cv_300_polygons.shp"),
							PROJ));

			jFrame.setSize(new Dimension(598, 167));

			jFrame.setContentPane(prev);

			jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jFrame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

	public void drawImage(
			Graphics2D g,
			double left, double top, double right, double bottom,
			double zoom, int width, int height)
	throws ImageUnavailableException {
		
		Cancellable c = new PreviewCancellable();
		while (!cancels.isEmpty()) cancels.pop().setCanceled(true);

		cancels.push(c);
		if (this.layer == null || width <= 0 || height <= 0 || vp == null)
			return;
		
		Envelope env = null;
		try {
			env = geoman.createEnvelope(
					left,
					bottom,
					right,
					top,
					SUBTYPES.GEOM2D);
		} catch (CreateEnvelopeException e1) {
			
			throw new ImageUnavailableException(
					"Error while initializing layer preview", e1);
		}
		vp.setEnvelope(env);
		vp.setImageSize(new Dimension(width, height));
		BufferedImage bi = new BufferedImage(
				width, height, BufferedImage.TYPE_4BYTE_ABGR);

		double scale = this.layer.getMapContext().getScaleView();
                LayerWithSelection layer = new LayerWithSelection(this.layer);
		try {
			layer.getLayer().draw(bi, g, vp, c, scale);
			if (layer.getLayer() instanceof ILabelable && ((ILabelable) layer.getLayer()).isLabeled()) {
				((ILabelable) layer.getLayer()).drawLabels(bi, g, vp, c, scale,
						vp.getDPI());
			}
		} catch (ReadException e) {
			
			throw new ImageUnavailableException(
					"Error while drawing layer '" + this.layer.getName() + "'", e);
		} finally {
                    layer.restore();
                }
	}
	
	private class PreviewCancellable implements Cancellable {

		private boolean canc = false;
		
		public PreviewCancellable() {
			
		}
		
		public boolean isCanceled() {
			return canc;
		}

		public void setCanceled(boolean c) {
			canc = c;
		}
		
	}
	

        private static class LayerWithSelection {
            
            FeatureSelection randomSelection = null;
            FeatureSelection originalSelection = null;
            
            FLayer layer = null;
            FLyrVect layerVect = null;
            
            public LayerWithSelection(FLayer layer) {
                this.layer = layer;
                if( this.layer instanceof FLyrVect ) {
                    this.layerVect = (FLyrVect) this.layer;
                }
            }
        
            public FLayer getLayer() {
                this.save();
                return this.layer;
            }

            public void save() {
                if(this.originalSelection!=null ) {
                    return;
                }
                FeatureStore store = this.layerVect.getFeatureStore();
                try {
                    this.originalSelection = store.getFeatureSelection();
                    this.randomSelection = this.getRandomSelection();
                    store.setSelection(this.randomSelection);
                } catch (DataException ex) {
                    logger.warn("Can't set random selection.",ex);
                }
            }
            
            public void restore() {
                if( this.layerVect == null ) {
                    return;
                }
                if( this.originalSelection!=null ) {
                    try {
                        this.layerVect.getFeatureStore().setSelection(this.originalSelection);
                    } catch (DataException ex) {
                        logger.warn("Can't restore the original selection of layer.",ex);
                    }
                }
                if( this.randomSelection!=null ) {
                    this.randomSelection.dispose();
                }
            }
            
            private FeatureSelection getRandomSelection() {

                if( this.layer == null ) {
                    return null;
                }
                FLyrVect layer = null;
                if( !( this.layer instanceof FLyrVect) ) {
                    return null; 
                }
                layer = (FLyrVect) this.layer;
                DisposableIterator diter = null;
                FeatureSelection fsel = null;
                FeatureSet featureSet = null;
                FeatureStore store = layer.getFeatureStore();
                try {
                    try {
                        featureSet = store.getFeatureSet();
                        fsel = store.createFeatureSelection();
                        diter = featureSet.fastIterator();
                    } catch (DataException de) {
                        logger.error("While getting store selection", de);
                        return null;
                    }

                    long count = 0;
                    Feature feat = null;
                    while (diter.hasNext() && count < 10) {
                        feat = (Feature) diter.next();
                        if (count % 4 == 0) {
                            fsel.select(feat);
                        }
                        count++;
                    }
                } finally {
                    if (diter != null) {
                        diter.dispose();
                    }
                    if (featureSet != null) {
                        featureSet.dispose();
                    }                
                }
                return fsel;
            }
        }
}