	/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: OnSelectionLabeled.java 10815 2007-03-20 16:16:20Z jaume $
* $Log$
* Revision 1.3  2007-03-20 16:16:20  jaume
* refactored to use ISymbol instead of FSymbol
*
* Revision 1.2  2007/03/09 08:33:43  jaume
* *** empty log message ***
*
* Revision 1.1.2.2  2007/02/01 11:42:47  jaume
* *** empty log message ***
*
* Revision 1.1.2.1  2007/01/30 18:10:45  jaume
* start commiting labeling stuff
*
*
*/
package org.gvsig.labeling.label;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 *
 * OnSelectionLabeled.java
 *
 *
 * 
 *
 */
public class OnSelectionLabeled implements ILabelingMethod {

	private static final String FIELD_DEFAULT_LABEL = "defaultLabel";
	private static final String ONSELECTION_LABELING_METHOD_PERSISTENCE_NAME =
			"ONSELECTION_LABELING_METHOD_PERSISTENCE_NAME";
	private ILabelClass defaultLabel;

	/**
	 * Empty constructor for persistence.
	 */
	public OnSelectionLabeled() {
		// Nothing to do
	}

	public OnSelectionLabeled(ILabelClass defaultLabel) {
		this.defaultLabel = defaultLabel;
	}


	public void addLabelClass(ILabelClass lbl) {
		defaultLabel = lbl;
	}


	public void deleteLabelClass(ILabelClass lbl) {
		// does nothing
	}

	public boolean allowsMultipleClass() {
		return false;
	}

	public void renameLabelClass(ILabelClass lbl, String newName) {
		// does nothing
	}


	public FeatureSet getFeatureIteratorByLabelClass(FLyrVect layer, ILabelClass lc, ViewPort viewPort, String[] usedFields)
	throws DataException {
		
		FeatureStore fsto = layer.getFeatureStore();
		FeatureSet fset = (FeatureSelection) fsto.getSelection();
		return fset;
	}

	public boolean definesPriorities() {
		return false;
	}

	public void setDefinesPriorities(boolean flag) {
		/* nothing, since only one label class is suported */
	}
	
	public ILabelClass getLabelClassByName(String labelName) {
		return defaultLabel;
	}

	public void clearAllClasses() {
		defaultLabel = null;
	}

	public ILabelClass[] getLabelClasses() {
		return defaultLabel == null ? new ILabelClass[0] : new ILabelClass[] { defaultLabel };
	}

	public ILabelingMethod cloneMethod() {
		ILabelClass lbl = (ILabelClass) LabelClassUtils.clone(this.defaultLabel);
		OnSelectionLabeled resp = new OnSelectionLabeled(lbl);
		return resp;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#loadFromState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		defaultLabel = (ILabelClass) state.get(FIELD_DEFAULT_LABEL);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#saveToState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DEFAULT_LABEL, defaultLabel);
	}
	
	public static void registerPersistent() {

		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(ONSELECTION_LABELING_METHOD_PERSISTENCE_NAME)==null ) {
			DynStruct definition = manager.addDefinition(
					OnSelectionLabeled.class,
					ONSELECTION_LABELING_METHOD_PERSISTENCE_NAME,
					ONSELECTION_LABELING_METHOD_PERSISTENCE_NAME+
					" Persistence definition",
					null, 
					null
			);
			
			definition.addDynFieldObject(FIELD_DEFAULT_LABEL).setMandatory(true)
			.setClassOfValue(ILabelClass.class);
		}
		
	}
	
	// =================================================
	// =================================================
	
}
