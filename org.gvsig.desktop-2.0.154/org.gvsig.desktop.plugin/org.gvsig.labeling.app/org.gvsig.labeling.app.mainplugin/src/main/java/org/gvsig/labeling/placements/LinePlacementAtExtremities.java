/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.placements;

import java.awt.geom.Point2D;

import org.apache.batik.ext.awt.geom.PathLength;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelLocationMetrics;
import org.gvsig.tools.task.Cancellable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * LinePlacementAtExtremities.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Dec 17, 2007
 *
 */
public class LinePlacementAtExtremities extends AbstractLinePlacement {

	private static Logger logger =
			LoggerFactory.getLogger(LinePlacementAtExtremities.class);
			
	public LabelLocationMetrics initialLocation(
			ILabelClass lc,
			IPlacementConstraints pc, PathLength pathLen,
			Cancellable cancel) {
		
		if (cancel.isCanceled()) return null;

		float distance = 0;
		if (pc.isAtTheEndOfLine()) {
			distance = (float) (pathLen.lengthOfPath()) ;
		}

		Point2D p = pathLen.pointAtLength(distance);

		double theta = 0;
		if (pc.isParallel()) {
			// get the line theta and apply it
			theta = pathLen.angleAtLength(distance);

		} else if (pc.isPerpendicular()) {
			// get the line theta with 90 degrees
			theta = pathLen.angleAtLength(distance) + AbstractLinePlacement.HALF_PI;
		}

		/*
		 * Offset the point to a distance of the height of the
		 * label class's height to make the label appear to
		 * be on the line.
		 *
		 */
		double x = p.getX();
		double y = p.getY();
		double halfHeight = lc.getBounds().getHeight()*.5;

		double sinTheta = Math.sin(theta);
		double cosTheta = Math.cos(theta);
		double xOffset = halfHeight * sinTheta;
		double yOffset = halfHeight * cosTheta;

		if (pc.isAtTheEndOfLine()) {
			double width = lc.getBounds().getWidth();
			xOffset -= width * cosTheta;
			yOffset += width * sinTheta;
		}

		p.setLocation(x + xOffset, y - yOffset);

		return new LabelLocationMetrics(
				p,
				theta,
				true);
	}

	public boolean isSuitableFor(IPlacementConstraints placementConstraints,
			int shapeType) {
		
		GeometryType gt = null;
		
		try {
			gt = GeometryLocator.getGeometryManager().getGeometryType(
					shapeType, Geometry.SUBTYPES.GEOM2D);
		} catch (Exception e) {
			logger.error("While getting aux geo type.", e);
		}
		
		if (gt.isTypeOf(TYPES.CURVE)
				|| shapeType == TYPES.MULTICURVE) {
			
			return placementConstraints != null
					/* && !placementConstraints.isFollowingLine()*/
					&& (placementConstraints.isAtTheBeginingOfLine()
							|| placementConstraints.isAtTheEndOfLine());
		} else {
			return false;
		}
		
	}

}
