package org.gvsig.labeling.placements;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;


/**
 * @author  jaume dominguez faus - jaume.dominguez@iver.es
 */
public abstract class AbstractPlacementConstraints implements
Cloneable, IPlacementConstraints {
	
	public static final String PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME =
			"PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME";
	
	// CAUTION, THIS IS A CLONEABLE OBJECT, DON'T FORGET TO 
	// UPDATE clone() METHOD WHEN ADDING FIELDS
	private int duplicateLabelsMode = ONE_LABEL_PER_FEATURE_PART; // default duplicate treatment
	private int placementMode;
	private boolean belowTheLine;
	private boolean aboveTheLine;
	private boolean onTheLine;
	private boolean pageOriented;
	private int locationAlongLine;
	private boolean fitInsidePolygon;
	// CAUTION, THIS IS A CLONEABLE OBJECT, DON'T FORGET TO 
	// UPDATE clone() METHOD WHEN ADDING FIELDS
	
	
	
	public void setDuplicateLabelsMode(int mode) {
		if (mode != REMOVE_DUPLICATE_LABELS &&
			mode != ONE_LABEL_PER_FEATURE &&
			mode != ONE_LABEL_PER_FEATURE_PART)
			throw new IllegalArgumentException(
					"Only REMOVE_DUPLICATE_LABELS, " +
					"ONE_LABEL_PER_FEATURE " +
					"or ONE_LABEL_PER_FEATURE_PARTS allowed");
		this.duplicateLabelsMode = mode;
	}

	public void setPlacementMode(int mode) {
		if (this instanceof PointPlacementConstraints) {
			if (mode != OFFSET_HORIZONTALY_AROUND_THE_POINT &&
				mode != ON_TOP_OF_THE_POINT &&
				mode != AT_SPECIFIED_ANGLE &&
				mode != AT_ANGLE_SPECIFIED_BY_A_FIELD)
				throw new IllegalArgumentException(
						"Only OFFSET_HORIZONTALY_AROUND_THE_POINT, " +
						"ON_TOP_OF_THE_POINT, " +
						"AT_SPECIFIED_ANGLE " +
						"or AT_ANGLE_SPECIFIED_BY_A_FIELD allowed for points: "+ mode);
		}

		if (this instanceof PolygonPlacementConstraints) {
			if (mode != HORIZONTAL &&
				mode != PARALLEL )
					throw new IllegalArgumentException(
							"Only HORIZONTAL, " +
							"or PARALLEL allowed for polygons: "+ mode);
		}

		if (this instanceof LinePlacementConstraints) {
					if (mode != HORIZONTAL &&
				mode != PARALLEL &&
				mode != FOLLOWING_LINE &&
				mode != PERPENDICULAR)
				throw new IllegalArgumentException(
						"Only HORIZONTAL, PARALLEL," +
						"FOLLOWING_LINE, or PERPENDICULAR allowed for lines: "+ mode);
		}
		this.placementMode = mode;
	}

	public boolean isHorizontal() {
		return placementMode == HORIZONTAL;
	}

	public boolean isPerpendicular() {
		return placementMode  == PERPENDICULAR;
	}

	public boolean isFollowingLine() {
		return placementMode  == FOLLOWING_LINE;
	}

	public boolean isParallel() {
		return placementMode == PARALLEL;
	}

	public int getDuplicateLabelsMode() {
		return duplicateLabelsMode;
	}

	/**
	 * Tells if the place mode selected is to put the label over the <b>POINT</b>
	 * @return boolean
	 */
	public boolean isOnTopOfThePoint() {
		return placementMode == ON_TOP_OF_THE_POINT;
	}

	public boolean isBelowTheLine() {
		return belowTheLine;
	}

	public boolean isAboveTheLine() {
		return aboveTheLine;
	}

	public boolean isOnTheLine() {
		return onTheLine;
	}

	public boolean isPageOriented() {
		return pageOriented;
	}

	public void setOnTheLine(boolean b) {
		this.onTheLine = b;
	}

	public void setPageOriented(boolean b) {
		this.pageOriented = b;
	}

	public void setBelowTheLine(boolean b) {
		this.belowTheLine = b;
	}

	public void setAboveTheLine(boolean b) {
		this.aboveTheLine = b;
	}

	public boolean isAtTheEndOfLine() {
		return locationAlongLine == AT_THE_END_OF_THE_LINE;
	}

	public boolean isAtTheBeginingOfLine() {
		return locationAlongLine == AT_THE_BEGINING_OF_THE_LINE;
	}

	public boolean isInTheMiddleOfLine() {
		return locationAlongLine == AT_THE_MIDDLE_OF_THE_LINE;
	}
	
	public boolean isAtBestOfLine() {
		return locationAlongLine == AT_BEST_OF_LINE;
	}

	public boolean isAroundThePoint() {
		return placementMode == OFFSET_HORIZONTALY_AROUND_THE_POINT;
	}

	public boolean isFitInsidePolygon() {
		return fitInsidePolygon;
	}

	public void setFitInsidePolygon(boolean b) {
		fitInsidePolygon = b;
	}

	public void setLocationAlongTheLine(int location) {
		if (location != IPlacementConstraints.AT_THE_MIDDLE_OF_THE_LINE
			&& location != IPlacementConstraints.AT_THE_BEGINING_OF_THE_LINE
			&& location != IPlacementConstraints.AT_THE_END_OF_THE_LINE
			&& location != IPlacementConstraints.AT_BEST_OF_LINE) {
			throw new IllegalArgumentException("Only IPlacementConstraints.AT_THE_MIDDLE_OF_THE_LINE, " +
					"IPlacementConstraints.AT_THE_BEGINING_OF_THE_LINE, or " +
					"IPlacementConstraints.AT_THE_END_OF_THE_LINE, or " +
					"IPlacementConstraints.AT_BEST_OF_LINE values are allowed" );
		}
		this.locationAlongLine = location;
	}
	
	
	
	public void saveToState(PersistentState state) throws PersistenceException {

		state.set("duplicateLabelsMode", getDuplicateLabelsMode());
		state.set("placementMode", placementMode);
		state.set("locationAlongLine", locationAlongLine);
		
		state.set("belowTheLine", isBelowTheLine());
		state.set("aboveTheLine", isAboveTheLine());
		state.set("onTheLine", isOnTheLine());
		state.set("pageOriented", isPageOriented());
		state.set("fitInsidePolygon", isFitInsidePolygon());
	}

	public void loadFromState(PersistentState state) throws PersistenceException {
		
		setDuplicateLabelsMode(state.getInt("duplicateLabelsMode"));
		setPlacementMode(state.getInt("placementMode"));
		locationAlongLine = state.getInt("locationAlongLine");
		
		setBelowTheLine(state.getBoolean("belowTheLine"));
		setAboveTheLine(state.getBoolean("aboveTheLine"));
		setOnTheLine(state.getBoolean("onTheLine"));
		setPageOriented(state.getBoolean("pageOriented"));
		setFitInsidePolygon(state.getBoolean("fitInsidePolygon"));
	}

	public static void registerPersistent() {
		
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME) == null ) {
			DynStruct definition = manager.addDefinition(
					AbstractPlacementConstraints.class,
					PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME,
					PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME +" Persistence definition",
					null, 
					null);
			definition.addDynFieldBoolean("belowTheLine").setMandatory(true);
			definition.addDynFieldBoolean("aboveTheLine").setMandatory(true);
			definition.addDynFieldBoolean("onTheLine").setMandatory(true);
			definition.addDynFieldBoolean("pageOriented").setMandatory(true);
			definition.addDynFieldBoolean("fitInsidePolygon").setMandatory(true);
			
			definition.addDynFieldInt("duplicateLabelsMode").setMandatory(true);
			definition.addDynFieldInt("placementMode").setMandatory(true);
			definition.addDynFieldInt("locationAlongLine").setMandatory(true);
		}		
	}
	

	public Object clone() throws CloneNotSupportedException {
		try {
			AbstractPlacementConstraints clone = getClass().newInstance();
			clone.aboveTheLine        = this.aboveTheLine;
			clone.belowTheLine        = this.belowTheLine;
			clone.duplicateLabelsMode = this.duplicateLabelsMode;
			clone.fitInsidePolygon    = this.fitInsidePolygon;
			clone.locationAlongLine   = this.locationAlongLine;
			clone.onTheLine           = this.onTheLine;
			clone.pageOriented        = this.pageOriented;
			clone.placementMode       = this.placementMode;
			return clone;
		} catch (Exception e) {
			throw new CloneNotSupportedException(e.getMessage());
		}
	}
}
