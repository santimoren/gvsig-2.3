
package org.gvsig.labeling.gui.layerproperties;

import org.gvsig.app.gui.labeling.LabelClassEditor;
import org.gvsig.app.gui.labeling.LabelClassEditorFactory;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelClass;

public class LabelClassPropertiesFactory implements LabelClassEditorFactory {

    public String getName() {
       return "Simple class editor";
    }

    public String getID() {
        return "SimpleClassEditor";
    }

    public LabelClassEditor createEditor(ILabelClass labelClass, FeatureStore store) {
        LabelClassProperties editor = new LabelClassProperties(store);
        editor.setLabelClass(labelClass);
        return editor;
    }

    public boolean accept(Class<? extends ILabelClass> labelClass) {
        if( LabelClass.class.getName().equals(labelClass.getName()) ) {
            return true;
        }
        return false;
    }
    
}
