package org.gvsig.labeling.symbol;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.LineMetrics;
import java.awt.geom.AffineTransform;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;
import org.gvsig.labeling.placements.PointPlacementConstraints;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.IFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.text.ISimpleTextSymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.TextPath;

        
/**
 * Class used to create symbols composed using a text defined by
 * the user.This text can be edited (changing the color, the font of the characters, and
 * the rotation of the text)and has the property that can follow a path.If this path
 * does not exist, the text is treated as a simpletextsymbol (when is drawn).
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public class SmartTextSymbol implements ISimpleTextSymbol, CartographicSupport {
	
	private static final Logger logger = LoggerFactory.getLogger(
			SmartTextSymbol.class);
	// ===========================
	public static final String SMART_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME =
			"SMART_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME";
	
	private static final String FIELD_UNIT = "unit";
	private static final String FIELD_REFERENCE_SYSTEM = "referenceSystem";
	private static final String FIELD_IS_SHAPE_VISIBLE = "isShapeVisible";
	private static final String FIELD_DESCRIPTION = "description";
	private static final String FIELD_TEXT = "text";
	private static final String FIELD_FONT = "font";
	private static final String FIELD_TEXT_COLOR = "textColor";
	private static final String FIELD_ROTATION = "rotation";
	private static final String FIELD_AUTO_RESIZE = "autoResize";
	// ====================================
	private static final String FIELD_HAS_HALO = "hasHalo";
	private static final String FIELD_HALO_COLOR = "haloColor";
	private static final String FIELD_HALO_WIDTH = "haloWidth";
	// ====================================
	/*
	public static final int SYMBOL_STYLE_TEXTALIGNMENT_LEFT = 0;
	public static final int SYMBOL_STYLE_TEXTALIGNMENT_RIGHT = 1;
	public static final int SYMBOL_STYLE_TEXTALIGNMENT_CENTERED = 2;
	*/

	// ===========================
	private static GeometryManager geoman = GeometryLocator.getGeometryManager();
	
	private String text;
	private FontRenderContext frc = new FontRenderContext(
			new AffineTransform(), false, true);
	
	// Background: ITextBackground
//	Case
	private double characterSpacing;
	private double characterWidth;
//	Direction
	private IFillSymbol fillSymbol;
	private double flipAngle;
//	boolean kerning;
	private double leading;
//	Position: textPosition
	private Color ShadowColor;
	private double ShadowXOffset;
	private double ShadowYOffset;
//	TypeSetting: Boolean
	private double wordSpacing;
//	ISimpleTextSymbol : ITextSymbol
//	BreakCharacter: Long
//	Clip: Boolean
	private TextPath textPath;
	private double xOffset;
	private double yOffset;
	private double angle;
//	Color: IColor

//	HorizontalAlignment:
//	esriTextHorizontalAlignment
	private boolean rightToLeft;
	//	VerticalAlignment
	private double maskSize;
//	MaskStyle
	private  IFillSymbol maskSymbol;
	private double margin;
	private int alignment;
	private boolean kerning = false;
	private TextPath tp;
	private IPlacementConstraints constraints;
	
	private boolean shapeVisible = true;
	private int unit = -1;
	private int referenceSystem = CartographicSupport.WORLD;
	private double rotation = 0;
	private String desc = "";
	private Color textColor = Color.BLACK;
	private Font font;
	private boolean autoresize = false;
	
	// =======================
	
	private boolean hasHalo = false;
	private Color haloColor = Color.WHITE;
	private float haloWidth = 3;
	private BasicStroke haloStroke = new BasicStroke(
			6, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	
	// =======================
	

	public SmartTextSymbol(ITextSymbol sym, IPlacementConstraints constraints) {

//		if(sym instanceof SimpleTextSymbol){
//			SimpleTextSymbol mySym = (SimpleTextSymbol)sym;

			this.setAutoresizeEnabled(sym.isAutoresizeEnabled());
			this.setDescription(sym.getDescription());
			this.setFont(sym.getFont());
			this.setFontSize(sym.getFont().getSize());
			this.setIsShapeVisible(sym.isShapeVisible());
			this.setText(sym.getText());
			this.setTextColor(sym.getTextColor());
			
			this.setDrawWithHalo(sym.isDrawWithHalo());
			this.setHaloColor(sym.getHaloColor());
			this.setHaloWidth(sym.getHaloWidth());
			
			this.constraints = constraints;

			setCharacterSpacing(2); //???
			setWordSpacing(TextPath.DEFAULT_WORD_SPACING);
			boolean rtl = false; // right to left text
			if (constraints.isAtTheBeginingOfLine()) {
				if (rtl) {
				    
					setAlignment(ITextSymbol.SYMBOL_STYLE_ALIGNMENT_RIGHT);
				}
				else {
					setAlignment(ITextSymbol.SYMBOL_STYLE_ALIGNMENT_LEFT);
				}
			}
			else if (constraints.isAtTheEndOfLine()) {
				if (rtl) {
					setAlignment(ITextSymbol.SYMBOL_STYLE_ALIGNMENT_LEFT);
				}
				else {
					setAlignment(ITextSymbol.SYMBOL_STYLE_ALIGNMENT_RIGHT);
				}
			}
			else { //constraints.isInTheMiddleOfLine() or constraints.isAtBestOfLine()
				setAlignment(ITextSymbol.SYMBOL_STYLE_ALIGNMENT_CENTERED);
			}
			setKerning(false);
			setRightToLeft(rtl);
		// }
	}
	
	
	public void setIsShapeVisible(boolean v) {
		this.shapeVisible = v;
	}
	
	public boolean isShapeVisible() {
		return shapeVisible;
	}
	


	public SmartTextSymbol() {
		PointPlacementConstraints pc = new PointPlacementConstraints();
		this.constraints = pc;
		
		SymbolPreferences preferences =
				MapContextLocator.getSymbolManager().getSymbolPreferences();
		font = preferences.getDefaultSymbolFont();
		textColor = preferences.getDefaultSymbolColor();
	}

	/**
	 * Draws the text according. If this symbol has the text path set, then
	 * it is used as the text line, otherwise shp <b>must be an FPoint2D</b>
	 * indicating the starting point of the text and then the text will
	 * be rendered from there and following the rotation previously set.
	 */
	
	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature f, Cancellable cancel) {
	
		if (!isShapeVisible()) return;

		setMargin(0);
                if ( StringUtils.isEmpty(text) ) {
                    return;
                }

		char[] text_chars = text.toCharArray();
		tp = new TextPath(g, geom, text_chars, getFont(),
				(float) characterSpacing, (float) characterWidth, kerning,
				(float) leading, alignment, (float) wordSpacing, (float) margin, rightToLeft);
		Font font = getFont();
		g.setFont(font);
		FontRenderContext frc = g.getFontRenderContext();
		LineMetrics lineMetrics = font.getLineMetrics(getText(), frc);
		
//		GlyphVector glyph =  font.layoutGlyphVector(frc, charText, 0, charText.length, Font.LAYOUT_NO_START_CONTEXT);
		double cons = 0;

		/* Repartimos el leading (espacio de separación entre lineas)
		 * arriba y abajo para que exista la misma separación entre la
		 * caja de la letra y la linea tanto si se dibuja por abajo como
		 * si se dibuja por arriba. 
		 */
		if(this.constraints.isAboveTheLine()) {
			cons = lineMetrics.getDescent()+lineMetrics.getLeading()/2;
		}
		else if (this.constraints.isBelowTheLine()) {
			cons = -(lineMetrics.getAscent()+lineMetrics.getLeading()/2);
		}
		/* Dibujamos la letra de tal manera que el centro de la caja de letra
		 * coincida con la linea
		 */
		else if(this.constraints.isOnTheLine()) {
//			cons = lineMetrics.getDescent()+(lineMetrics.getLeading()/2)-(lineMetrics.getHeight()/2);
			cons = lineMetrics.getDescent()+lineMetrics.getLeading()-(lineMetrics.getHeight()/2);
		}
		
		double[] coords = null; // tp.nextPosForGlyph(0);

		for (int i = 0; i < tp.getGlyphCount(); i++) {
			coords = tp.nextPosForGlyph(i);
			if (coords[0] == TextPath.NO_POS || coords[1] == TextPath.NO_POS)
				continue;

			// move the label 'cons" units above/below the line
			double xOffset = cons * Math.sin(coords[2]);
			double yOffset = cons * Math.cos(coords[2]);

			g.translate(coords[0]+xOffset, coords[1]-yOffset);
			g.rotate(coords[2]);

			char[] aux = new char[1];
			aux[0] = text_chars[i];
			if (isDrawWithHalo()) {
				GlyphVector glyph = font.createGlyphVector(frc, aux);
				Shape outlineChar = glyph.getOutline();
				g.setStroke(haloStroke);
				g.setColor(getHaloColor());
				
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				// =============================
				g.draw(outlineChar);
				// =============================
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_OFF);
			}
			
			g.setColor(this.getTextColor());
			g.drawString(String.valueOf(text_chars[i]), 0, 0);
			g.rotate(-coords[2]);
			g.translate(-coords[0]-xOffset, -coords[1]+yOffset);
		}
	}

	public void getPixExtentPlus(
			Geometry shp, float[] distances, ViewPort viewPort, int dpi) {
		
		/*
		 * TODO Is this used?
		 */
		distances[0] = 0;
		distances[1] = 0;
		// throw new RuntimeException("Not yet implemented!");
	}

	public int getOnePointRgb() {
		return getTextColor().getRGB();
	}

	/*
	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());
		xml.putProperty("desc", getDescription());
		xml.putProperty("isShapeVisible", isShapeVisible());
		return xml;
	}
	*/

	public int getSymbolType() {
		return Geometry.TYPES.GEOMETRY;
	}

	public boolean isSuitableFor(Geometry geom) {
		return geom.getGeometryType().isTypeOf(TYPES.CURVE);
	}
	
	public void drawInsideRectangle(
			Graphics2D g,
			AffineTransform scaleInstance,
			Rectangle r,
			PrintAttributes properties) throws SymbolDrawingException {
		// let's take the bottom segment of the rectangle as the line
		
		Surface surf = null;
		try {
			surf = geoman.createSurface(SUBTYPES.GEOM2D);
		} catch (CreateGeometryException e) {
			logger.info("Error while creating surface.", e);
			throw new SymbolDrawingException(
					SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS);
		}
		surf.addVertex(r.getX(), r.getY());
		surf.addVertex(r.getX() + r.getWidth(), r.getY());
		
		if (properties == null) {
			draw(g, scaleInstance, surf, null, null);
		} else {
			print(g, scaleInstance, surf, properties);
		}
	}


	/*
	public void setXMLEntity(XMLEntity xml) {
		setFont(new Font("Arial", Font.PLAIN, 18));
		setText("this is my TEST text that follows a line");
		setDescription(xml.getStringProperty("desc"));
		setIsShapeVisible(xml.getBooleanProperty("isShapeVisible"));

	}
	*/

	public void setText(String txt) {
		this.text = txt;
	}

	public String getText() {
		return text;
	}

	public void setCharacterSpacing(double charSpacing) {
		this.characterSpacing = charSpacing;
	}

	public void setWordSpacing(double wordSpacing) {
		this.wordSpacing = wordSpacing;
	}

	public void setAlignment(int alignment) {
		this.alignment = alignment;
	}

	public void setKerning(boolean kerning) {
		this.kerning = kerning;
	}

	public void setMargin(double margin) {
		this.margin = margin;
	}

	public void setRightToLeft(boolean rightToLeft) {
		this.rightToLeft = rightToLeft;
	}
	
	// ==============================================
	// ==============================================
	// ==============================================

	
	public ISymbol getSymbolForSelection() {
		return this;
	}

	public boolean isOneDotOrPixel(Geometry geom,
			double[] positionOfDotOrPixel, ViewPort viewPort, int dpi) {
		
		int type = geom.getType();
		switch (type) {
		case Geometry.TYPES.NULL:
		case Geometry.TYPES.POINT:
		case Geometry.TYPES.MULTIPOINT:
			return false;
		default:
			Envelope geomBounds = geom.getEnvelope();
			double dist1Pixel = viewPort.getDist1pixel();

			float[] distances = new float[2];
			this.getPixExtentPlus(geom, distances, viewPort, dpi);

			boolean onePoint =
					((geomBounds.getLength(0) + distances[0] <= dist1Pixel)
					&& (geomBounds.getLength(1) + distances[1] <= dist1Pixel));

			if (onePoint) {
				positionOfDotOrPixel[0] = geomBounds.getMinimum(0);
				positionOfDotOrPixel[1] = geomBounds.getMinimum(1);
			}
			return onePoint;
		}
	}
	


	public String getDescription() {
		return desc;
	}


	public void setDescription(String d) {
		desc = d;
	}


	public Color getColor() {
		return this.getTextColor();
	}


	public void setColor(Color color) {
		this.setTextColor(color);
	}


	public void print(Graphics2D g, AffineTransform at, Geometry geom,
			PrintAttributes properties) {
		
		/*
		 * TODO Use properties
		 * (perhaps when using things of CartographicSupport)
		 */
		this.draw(g,  at, geom, null, null);
	}


	public void setFont(Font fnt) {
		this.font = fnt;
	}


	public Font getFont() {
		return font;
	}


	public Color getTextColor() {
		return this.textColor;
	}


	public void setTextColor(Color color) {
		this.textColor = color;
	}


	public void setFontSize(double d) {
		Font newFont = new Font(
				font.getName(),
				font.getStyle(),
				Math.round((float) d));
		this.font = newFont;		
	}


	public Geometry getTextWrappingShape(Point p) {
		
		Font font = getFont();
		GlyphVector gv = font.createGlyphVector(frc, text);

		Shape shape = gv.getOutline((float) p.getX(), (float) p.getY());
		Geometry myFShape;
		try {
			myFShape = geoman.createSurface(new GeneralPathX(shape
					.getBounds2D().getPathIterator(null)), SUBTYPES.GEOM2D);
			myFShape.transform(AffineTransform.getTranslateInstance(p.getX(), p.getY()));

			if (rotation != 0) {
				myFShape.transform(AffineTransform.getRotateInstance(rotation));
			}
			return myFShape;
		} catch (CreateGeometryException e) {
			logger.error("Error creating a surface", e);
		}
		return null;
	}


	public Rectangle getBounds() {
		
		Rectangle bounds = null;
		try {
			Geometry aux = getTextWrappingShape(geoman.createPoint(0,0, SUBTYPES.GEOM2D));
			Envelope env = aux.getEnvelope();
			bounds = new Rectangle(
					(int) env.getMinimum(0),
					(int) env.getMinimum(1),
					(int) env.getLength(0),
					(int) env.getLength(1));
		} catch (CreateGeometryException e) {
			logger.error("Error creating a point", e);
		}
		return bounds;
	}

	public double getRotation() {
		return this.rotation;
	}


	public void setRotation(double rot) {
		this.rotation = rot;
	}

	public void setAutoresizeEnabled(boolean ar) {
		autoresize = ar;
	}


	public boolean isAutoresizeEnabled() {
		return autoresize;
	}


	public Color getHaloColor() {
		return this.haloColor;
	}


	public void setHaloColor(Color co) {
		this.haloColor = co;
	}


	public float getHaloWidth() {
		return this.haloWidth;
	}


	public void setHaloWidth(float w) {
		this.haloWidth = w;
		this.haloStroke = new BasicStroke(
				2*haloWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	}


	public boolean isDrawWithHalo() {
		return this.hasHalo;
	}


	public void setDrawWithHalo(boolean h) {
		this.hasHalo = h;
	}

	// ==============================


	public void setUnit(int unitIndex) {
		unit = unitIndex;
	}


	public int getUnit() {
		return unit;
	}


	public int getReferenceSystem() {
		return this.referenceSystem;
	}


	public void setReferenceSystem(int rs) {
		this.referenceSystem = rs;
	}


	public void setCartographicSize(double cartographicSize, Geometry geom) {
		setFontSize(cartographicSize);
	}

	public double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		double oldSize = getFont().getSize();
		setCartographicSize(getCartographicSize(viewPort, dpi, geom), geom);
		return oldSize;
	}

	public double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom) {
		return SymbolUtils.	getCartographicLength(
				this, getFont().getSize(), viewPort, dpi);
	}

	// ========================
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	// ==========================
	
	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(SMART_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME)==null ) {
			DynStruct definition = manager.addDefinition(
					SmartTextSymbol.class,
					SMART_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME,
					SMART_TEXT_SYMBOL_PERSISTENCE_DEFINITION_NAME+" Persistence definition",
					null, 
					null
			);
			
			definition.addDynFieldString(FIELD_DESCRIPTION).setMandatory(true);
			definition.addDynFieldBoolean(FIELD_IS_SHAPE_VISIBLE).setMandatory(true);
			definition.addDynFieldInt(FIELD_REFERENCE_SYSTEM).setMandatory(true);
			definition.addDynFieldInt(FIELD_UNIT).setMandatory(true);
			definition.addDynFieldBoolean(FIELD_AUTO_RESIZE).setMandatory(true);
			definition.addDynFieldObject(FIELD_FONT).setClassOfValue(Font.class).setMandatory(true);
			definition.addDynFieldDouble(FIELD_ROTATION).setMandatory(true);
			definition.addDynFieldString(FIELD_TEXT).setMandatory(true);
			definition.addDynFieldObject(FIELD_TEXT_COLOR).setClassOfValue(Color.class).setMandatory(true);
			// halo
			definition.addDynFieldBoolean(FIELD_HAS_HALO).setMandatory(true);
			definition.addDynFieldObject(FIELD_HALO_COLOR).setClassOfValue(
					Color.class).setMandatory(true);
			definition.addDynFieldFloat(FIELD_HALO_WIDTH).setMandatory(true);
		}
		
	}
	
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		setDescription(state.getString(FIELD_DESCRIPTION));
		setIsShapeVisible(state.getBoolean(FIELD_IS_SHAPE_VISIBLE));
		setReferenceSystem(state.getInt(FIELD_REFERENCE_SYSTEM));
		setUnit(state.getInt(FIELD_UNIT));
		
		setAutoresizeEnabled(state.getBoolean(FIELD_AUTO_RESIZE));
		setFont((Font) state.get(FIELD_FONT));
		setRotation(state.getDouble(FIELD_ROTATION));
		setText(state.getString(FIELD_TEXT));
		setTextColor((Color) state.get(FIELD_TEXT_COLOR));
		// halo
		this.setDrawWithHalo(state.getBoolean(FIELD_HAS_HALO));
		this.setHaloColor((Color) state.get(FIELD_HALO_COLOR));
		this.setHaloWidth(state.getFloat(FIELD_HALO_WIDTH));
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set(FIELD_DESCRIPTION, getDescription());
		state.set(FIELD_IS_SHAPE_VISIBLE, isShapeVisible());
		state.set(FIELD_REFERENCE_SYSTEM, getReferenceSystem());
		state.set(FIELD_UNIT, getUnit());
		
		state.set(FIELD_AUTO_RESIZE, isAutoresizeEnabled());
		state.set(FIELD_FONT, getFont());
		state.set(FIELD_ROTATION, getRotation());
		state.set(FIELD_TEXT, getText());
		state.set(FIELD_TEXT_COLOR, getTextColor());
		// halo
		state.set(FIELD_HAS_HALO, this.isDrawWithHalo());
		state.set(FIELD_HALO_COLOR, this.getHaloColor());
		state.set(FIELD_HALO_WIDTH, this.getHaloWidth());
	}





}
