/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.gui.layerproperties;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.labeling.LabelClassEditor;
import org.gvsig.app.gui.styling.SingleStyleSelectorFilter;
import org.gvsig.app.gui.styling.StylePreviewer;
import org.gvsig.app.gui.styling.StyleSelector;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

import org.jfree.chart.block.EmptyBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * LabelClassProperties.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es May 27, 2008
 *
 */
public class LabelClassProperties extends JPanel implements LabelClassEditor, IWindow,
ActionListener {
	
	private static final Logger logger =
			LoggerFactory.getLogger(LabelClassProperties.class);
	
	private static final String TEST_SQL = "TEST_SQL";
	private static final String DELETE_FIELD = "DELETE_FIELD";
	private static final long serialVersionUID = 6528513396536811057L;
	private static final String ADD_FIELD = "ADD_FIELD";
	private static final String SELECT_STYLE = "SELECT_STYLE";
	private static final String CLEAR_STYLE = "CLEAR_STYLE";
	private static final String VERIFY = "VERIFY";
	private JTextField txtName;
	private ILabelClass lc, clonedClass;
	private JTable tblExpressions;
	private JButton btnRemoveField;
	private JButton btnAddField;
	private JButton btnVerify;
	private JComponent styPreviewer;
	private JButton btnSelStyle;
	private JRadioButton rdBtnFilteredFeatures;
	private JRadioButton rdBtnAllFeatures;
	private JTextField txtSQL;
	private JCheckBox chkLabelFeatures;
	private LabelTextStylePanel textStyle;
	private JPanel sqlPnl;
	private LabelClassPreview labelPreview;
	private FeatureStore featureStore = null;
	private JButton btnDontUseStyle;
	private boolean accepted = true;
        private AcceptCancelPanel acceptCancelPanel;

	/**
	 * <p>
	 * Creates a new instance of the dialog that configures all the properties
	 * of a LabelClass.
	 * </p>
	 * @param strategy
	 * @param asWindow
	 */
	public LabelClassProperties(FeatureStore fsto) {

		featureStore = fsto;
		initialize();
	}

	private void initialize() {
	    
	    I18nManager i18nManager = ToolsLocator.getI18nManager();
	    
		this.setPreferredSize(new Dimension(850, 580));
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(5, 5, 5, 5));


		JPanel northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout(FlowLayout.LEFT,5,5));
		
		JLabel lbl_name = new JLabel();
		lbl_name.setText(i18nManager.getTranslation("name").concat(":"));
		northPanel.add(lbl_name);

		txtName = new JTextField(40);
		northPanel.add(txtName);
		
		add(northPanel, BorderLayout.NORTH);

		
		JPanel centerPanel = new JPanel(new BorderLayout());
		
		textStyle = new LabelTextStylePanel();
		textStyle.addActionListener(this);
		centerPanel.add(textStyle, BorderLayout.NORTH);

		JPanel labelExpressionsPanel = new JPanel(new BorderLayout());
		labelExpressionsPanel.setBorder(new EmptyBorder(5, 7, 5, 5));
		labelExpressionsPanel.add(new JScrollPane(getTableFields()),
				BorderLayout.CENTER);

		JPanel a = new JPanel(new FlowLayout(FlowLayout.LEFT, 7, 0));
		a.add(new JLabel("<html><b>"+Messages.getText("text_fields")+"</b></html>"));
		centerPanel.add(labelExpressionsPanel, BorderLayout.CENTER);
		
		GridLayout gbl = new GridLayout(3, 1, 0, 5);
		JPanel auxp = new JPanel(gbl);
		auxp.add(getBtnVerify());
		auxp.add(getBtnAddField());
		auxp.add(getBtnRemoveField());
		auxp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		JPanel auxpp = new JPanel();
		auxpp.add(auxp);
		
		centerPanel.add(auxpp, BorderLayout.EAST);


		GridBagLayoutPanel aux = new GridBagLayoutPanel();
//		aux.setBorder(BorderFactory.createTitledBorder(PluginServices.getText(
//		this, "features")));
		rdBtnAllFeatures = new JRadioButton(Messages.getText("all_features"));
		rdBtnFilteredFeatures = new JRadioButton(Messages.getText("filtered_features").concat(" (SQL GDBMS)"));


		ButtonGroup g = new ButtonGroup();
		g.add(rdBtnAllFeatures);
		g.add(rdBtnFilteredFeatures);
		
		rdBtnAllFeatures.addActionListener(this);
		rdBtnFilteredFeatures.addActionListener(this);
		
		aux.addComponent(chkLabelFeatures = new JCheckBox(
				Messages.getText("label_features_in_this_class")));
		aux.addComponent("", rdBtnAllFeatures);
		aux.addComponent("", rdBtnFilteredFeatures);
		
		sqlPnl = new JPanel(new FlowLayout(FlowLayout.LEFT));
		sqlPnl.add(new JLabel("   SQL: SELECT * FROM "));
		sqlPnl.add(new JLabel("<" + Messages.getText("layer_name")
				+ ">"));
		sqlPnl.add(new JLabel(" WHERE  "));
		sqlPnl.add(txtSQL = new JTextField(30));
		sqlPnl.add(new JLabel(";"));
		aux.addComponent("", sqlPnl);

		JPanel auxPanel = new JPanel(new BorderLayout());
		
		JPanel aux2 = new JPanel();
		auxPanel.setBorder(BorderFactory.createTitledBorder(
				Messages.getText("background_style")));

		aux2.add(getStylePreviewer());

		gbl = new GridLayout(2, 1, 0, 5);
		JPanel aux3 = new JPanel(gbl);
		aux3.add(getBtnSelectStyle());
		aux3.add(getBtnDontUseStyle());
		aux3.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		auxpp = new JPanel();
		auxpp.add(aux3);

		auxPanel.add(aux2,BorderLayout.WEST);
		auxPanel.add(auxpp,BorderLayout.EAST);

		JPanel aux4 = new JPanel(new GridLayout(1,2));
		aux4.add(auxPanel);
		aux2 = new JPanel(new BorderLayout());
		aux2.setBorder(BorderFactory.createTitledBorder(Messages.getText("preview")));
		aux2.add(new JBlank(5, 5), BorderLayout.NORTH);
		aux2.add(labelPreview = new LabelClassPreview(), BorderLayout.CENTER);
        aux2.add(new JBlank(5, 5), BorderLayout.WEST);
        aux2.add(new JBlank(5, 5), BorderLayout.EAST);
        aux2.add(new JBlank(5, 5), BorderLayout.SOUTH);
		aux4.add(aux2);

		GridBagLayoutPanel aux5 = new GridBagLayoutPanel();
		aux5.addComponent("", aux);
		aux5.addComponent("", aux4);
		centerPanel.add(aux5, BorderLayout.SOUTH);
		add(centerPanel, BorderLayout.CENTER);
                this.acceptCancelPanel = new AcceptCancelPanel(this, this);
		add(this.acceptCancelPanel, BorderLayout.SOUTH);

		chkLabelFeatures.addActionListener(this);
		txtName.addActionListener(this);

	}
        
        public void setAcceptCancelVisible(boolean visible) {
            this.acceptCancelPanel.setVisible(visible);
        }

	private JComponent getStylePreviewer() {

		if (styPreviewer == null) {
			styPreviewer = new StylePreviewer();
			styPreviewer.setPreferredSize(new java.awt.Dimension(90,90));
			styPreviewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
			((StylePreviewer) styPreviewer).setShowOutline(true);
		}
		return styPreviewer;

	}

	private JButton getBtnSelectStyle() {
		if (btnSelStyle == null) {
			btnSelStyle = new JButton(Messages.getText("select"));
			btnSelStyle.setActionCommand(SELECT_STYLE);
			btnSelStyle.addActionListener(this);
		}

		return btnSelStyle;
	}


	private JButton getBtnRemoveField() {
		if (btnRemoveField == null) {
			btnRemoveField = new JButton(Messages.getText("remove"));
			btnRemoveField.setActionCommand(DELETE_FIELD);
			btnRemoveField.addActionListener(this);
		}

		return btnRemoveField;
	}

	private JButton getBtnAddField() {
		if (btnAddField == null) {
			btnAddField = new JButton(Messages.getText("add"));
			btnAddField.setActionCommand(ADD_FIELD);
			btnAddField.addActionListener(this);
		}

		return btnAddField;
	}

	private JButton getBtnVerify() {
		if(btnVerify == null) {
			btnVerify = new JButton(Messages.getText("verify"));
			btnVerify.setActionCommand(VERIFY);
			btnVerify.addActionListener(this);
		}
		return btnVerify;
	}

	private JButton getBtnDontUseStyle() {
		if (btnDontUseStyle == null) {
			btnDontUseStyle = new JButton(Messages.getText("no_style"));
			btnDontUseStyle.setActionCommand(CLEAR_STYLE);
			btnDontUseStyle.addActionListener(this);
		}

		return btnDontUseStyle;
	}

	private JTable getTableFields() {
		if (tblExpressions == null) {
			tblExpressions = new JTable();
			tblExpressions.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		}

		return tblExpressions;
	}


	private void setComponentEnabled(Component c, boolean b) {
		if (c instanceof JComponent) {
			JComponent c1 = (JComponent) c;
			for (int i = 0; i < c1.getComponentCount(); i++) {
				setComponentEnabled(c1.getComponent(i), b);
			}
		}
		c.setEnabled(b);
	}



	public void setLabelClass(ILabelClass labelClass) {
		if (labelClass == null)
			return;

		lc = labelClass;
                
                try {
                    clonedClass = (ILabelClass) lc.clone();
                } catch (CloneNotSupportedException ex) {
                    clonedClass = lc;
                }

		labelPreview.setLabelClass(clonedClass);
		boolean clo_use_sql = LabelClassUtils.isUseSqlQuery(clonedClass); 
		if (!clo_use_sql) {
			rdBtnAllFeatures.setSelected(true);
			txtSQL.setText("");
		} else {
			rdBtnFilteredFeatures.setSelected(true);
			txtSQL.setText(clonedClass.getSQLQuery());
		}

		chkLabelFeatures.setSelected(clonedClass.isVisible());
		rdBtnFilteredFeatures.setSelected(clo_use_sql);
		rdBtnAllFeatures.setSelected(!clo_use_sql);
		((StylePreviewer) styPreviewer).setStyle(clonedClass.getLabelStyle());
		txtName.setText(clonedClass.getName());
		
		// String expr = lc.getStringLabelExpression();

		/*
		String EOExpr = LabelExpressionParser.tokenFor(LabelExpressionParser.EOEXPR);
		if (expr == null)
			expr = EOExpr;
		expr = expr.trim();
		if (!expr.endsWith(EOExpr)) {
			throw new Error("Invalid expression. Missing EOExpr token ("+EOExpr+").\n"+expr);
		}
		expr = expr.substring(0, expr.length()-1);
		*/

		// expr = expr.trim();

		ITextSymbol lcTextSymbol = lc.getTextSymbol();
		ITextSymbol clonedSymbol = null;
		
		try {
			clonedSymbol = (ITextSymbol) lcTextSymbol.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("While clonin.", e);
		}

		textStyle.setModel(
				clonedSymbol, //lc.getTextSymbol(),
				lc.getUnit(),
				lc.getReferenceSystem());

		getTableFields().setRowHeight(22);
		getTableFields().setModel(new FieldTableExpressions(lc.getLabelExpressions()));
		TableColumnModel colMod = getTableFields().getColumnModel();
		colMod.getColumn(0).setPreferredWidth(100);
		colMod.getColumn(0).setWidth(100);
		colMod.getColumn(1).setResizable(true);
		colMod.getColumn(1).setPreferredWidth(513);
		
		FeatureAttributeDescriptor[] atts = null;
		
		try {
			atts = featureStore.getDefaultFeatureType().getAttributeDescriptors();
		} catch (DataException e) {
			logger.error("While getting attributes.", e);
		}
		
		getTableFields().setDefaultEditor(
				Object.class,
				new DefaultEditor(atts));
		repaint();

		actionPerformed(new ActionEvent(this, 0, null));
	}

        public JComponent asJComponent() {
            return this;
        }

        public void showDialog() {
            ApplicationLocator.getManager().getUIManager().addWindow(this);
        }

	private class DefaultEditor extends AbstractCellEditor implements
			TableCellEditor, ActionListener {

		JPanel editor;
		LabelExpressionEditorPanel dialog;
		private JTextField text;
		private JButton button;
		protected static final String EDIT = "edit";

		public DefaultEditor(FeatureAttributeDescriptor[] atts) {
			editor = new JPanel();
			editor.setLayout(new GridBagLayout());
			GridBagConstraints cons = new GridBagConstraints();
//			cons.anchor = GridBagConstraints.FIRST_LINE_START;
			cons.fill = cons.BOTH;
			cons.weightx=1.0;
			cons.weighty =1.0;
			text = new JTextField();
			editor.add(text, cons);

			GridBagConstraints cons1 = new GridBagConstraints();
//			cons.anchor = GridBagConstraints.FIRST_LINE_END;
			cons1.fill = cons1.VERTICAL;
			cons1.weighty =1.0;
//			cons.gridheight = GridBagConstraints.REMAINDER;
			button = new JButton("...");
			button.setActionCommand(DefaultEditor.EDIT);
			button.addActionListener(this);
			editor.add(button,cons1);

			editor.updateUI();

			// Set up the dialog that the button brings up.
			dialog = new LabelExpressionEditorPanel(atts);
		}

		public void actionPerformed(ActionEvent e) {
			if (EDIT.equals(e.getActionCommand())) {

				dialog.setValue(text.getText());
				ApplicationLocator.getManager().getUIManager().addWindow(dialog);
				//fireEditingStopped(); // Make the renderer reappear.
				text.setText(dialog.getValue());
			}
		}

		// Implement the one CellEditor method that AbstractCellEditor doesn't.
		public Object getCellEditorValue() {
			return text.getText();
		}

		// Implement the one method defined by TableCellEditor.
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			text.setText((String) value);
			return editor;
		}
	}




	public WindowInfo getWindowInfo() {
		WindowInfo wi = new WindowInfo(WindowInfo.MODALDIALOG
				| WindowInfo.RESIZABLE);
		wi.setTitle(Messages.getText("label_class_properties"));
		wi.setWidth(getPreferredSize().width + 8);
		wi.setHeight(getPreferredSize().height);
		return wi;
	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}


	/*
	 * Return a hashtable with example values.
	 * Only for verification purposes.
	private Hashtable<String, Value> getValuesTable(String[] fNames, int[] fTypes) {
		
		Hashtable<String, Value> parser_symbol_table = new Hashtable<String, Value>();

		Random rand = new Random();
		byte b = 0;
    	short s = (short)rand.nextInt(Short.MAX_VALUE);
    	int i = rand.nextInt();
		long l = rand.nextLong();
		boolean bool = rand.nextBoolean();
		float f = rand.nextFloat();
		double d = rand.nextDouble();

		for (int j = 0; j < fNames.length; j++) {
			Value value=null;

			int type = fTypes[j];
            switch (type) {
            case Types.BIGINT:
                value = ValueFactory.createValue(l);
                break;
            case Types.BIT:
            case Types.BOOLEAN:
                value = ValueFactory.createValue(bool);
                break;
            case Types.CHAR:
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
                value = ValueFactory.createValue("");
                break;
            case Types.DATE:
                Date auxDate = new Date();
            	if (auxDate != null){
                    value = ValueFactory.createValue(auxDate);
            	}
                break;
            case Types.DECIMAL:
            case Types.NUMERIC:
            case Types.FLOAT:
            case Types.DOUBLE:
                value = ValueFactory.createValue(d);
                break;
            case Types.INTEGER:
                value = ValueFactory.createValue(i);
                break;
            case Types.REAL:
                value = ValueFactory.createValue(f);
                break;
            case Types.SMALLINT:
                value = ValueFactory.createValue(s);
                break;
            case Types.TINYINT:
                value = ValueFactory.createValue(b);
                break;
            case Types.BINARY:
            case Types.VARBINARY:
            case Types.LONGVARBINARY:
                byte[] auxByteArray = {b}; //new byte[1];
                value = ValueFactory.createValue(auxByteArray);
                break;
            case Types.TIMESTAMP:
           		value = ValueFactory.createValue(new Timestamp(l));
                break;
            case Types.TIME:
            	value = ValueFactory.createValue(new Time(l));
                break;
            default:
            	value = ValueFactory.createValue("");
            	break;
        }
			parser_symbol_table.put(fNames[j], value);
		}
		return parser_symbol_table;
	}
		 */


	/*
	private boolean verifyExpresion(String expresion, Hashtable<String, Value> symbols){
		LabelExpressionParser parser;
		try {
			parser = new LabelExpressionParser(new StringReader(expresion),symbols);
			parser.LabelExpression();
			Expression expr = ((Expression)parser.getStack().pop());
			expr.evaluate();
		} catch (ExpressionException e2) {
			return false;
		} catch (Exception e3) {
			return false;
		}
		return true;
	}
	*/

	private boolean verifyExpressions(){
		
		String[] expressions = ((FieldTableExpressions) getTableFields().getModel()).getExpression();
		if(expressions.length == 1 && expressions[0].equals("")){
			return true;
		}
		boolean result = true;
		String message = "";
		for (int i = 0; i < expressions.length; i++) {
			String expresion = expressions[i];
			
			if( !LabelClassUtils.validExpression(expresion, featureStore, false) ){
				result = false;
				if(message.compareTo("")!=0){
					message = message +	" , " + (i+1);
				} else {
					message = " " + Messages.getText("at_fields")+" "+(i+1);
				}
			}
		}
		message = message +".";
		if(!result){
			JOptionPane.showMessageDialog(
					ApplicationLocator.getManager().getRootComponent(),
					Messages.getText("malformed_or_empty_expression") + message,
					Messages.getText("verify"),
					JOptionPane.ERROR_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(
					ApplicationLocator.getManager().getRootComponent(),
					Messages.getText("_Expressions_are_correct"),
					Messages.getText("verify"),
					JOptionPane.INFORMATION_MESSAGE);
		}
		return result;
	}

	public boolean isAccepted(){
		return accepted;
	}
        
        public void doCancel() {
            accepted = false;
            setLabelClass(lc);        
        }

        public void doAccept() {
            if (rdBtnFilteredFeatures.isSelected()) {
                String sqlstr = txtSQL.getText();
                if (!validVisibleSQL(sqlstr, featureStore)) {
                    JOptionPane.showMessageDialog(
                            ApplicationLocator.getManager().getRootComponent(),
                            Messages.getText("error_validating_filter_query")
                            + ":\n\n'" + sqlstr + "'",
                            Messages.getText("filtered_features"),
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            clonedClass.setUnit(textStyle.getUnit());
            clonedClass.setReferenceSystem(textStyle.getReferenceSystem());
            clonedClass.setTextSymbol(textStyle.getTextSymbol());
            lc = clonedClass;
            accepted = true;
        }

	public void actionPerformed(ActionEvent e) {
		setComponentEnabled(sqlPnl, rdBtnFilteredFeatures.isSelected());
		
		applySettings();
		
		if ("OK".equals(e.getActionCommand())) {
                        doAccept();
			try {
				ApplicationLocator.getManager().getUIManager().closeWindow(this);
			} catch (Exception ex) {
				// this only happens when running this as stand-alone app
				// from main method;
				logger.warn("While closing window.", ex);
			}
		} else if ("CANCEL".equals(e.getActionCommand())) {
                        doCancel();
			try {
				ApplicationLocator.getManager().getUIManager().closeWindow(this);
			} catch (Exception ex) {
				// this only happens when running this as stand-alone app
				// from main method;
				logger.warn("While closing window.", ex);
			}
		} else if (CLEAR_STYLE.equals(e.getActionCommand())) {
			clonedClass.setLabelStyle(null);
//			styPreviewer.setStyle(null);
			((StylePreviewer) styPreviewer).setStyle(null);
		} else if (TEST_SQL.equals(e.getActionCommand())) {
			logger.info("TEST_SQL = " + TEST_SQL);
		} else if (DELETE_FIELD.equals(e.getActionCommand())) {
			int[] rowInd = getTableFields().getSelectedRows();
			for (int i = rowInd.length-1; i >= 0 ; i--) {
				delField(rowInd[i]);
			}
			clonedClass.setLabelExpressions(((FieldTableExpressions) getTableFields().getModel()).getExpression());
			setLabelClass(clonedClass);
		} else if (VERIFY.equals(e.getActionCommand())) {
			verifyExpressions();
		} else if (ADD_FIELD.equals(e.getActionCommand())) {
			addField();
		} else if (SELECT_STYLE.equals(e.getActionCommand())) {

			IStyle myStyle = ((StylePreviewer)styPreviewer).getStyle();
			
			FeatureType fty = null;
			
			try {
				fty = featureStore.getDefaultFeatureType();
			} catch (DataException e1) {
				logger.error("While getting feat type.", e1);
			}
			
			int geotype = fty.getDefaultGeometryAttribute().getGeomType().getType();
			
			SingleStyleSelectorFilter filter =
					new SingleStyleSelectorFilter(ILabelStyle.class);
			StyleSelector stySel = null;
			
			try {
				stySel = new StyleSelector(myStyle, geotype, filter);
			} catch (IllegalArgumentException iae) {
				JOptionPane.showMessageDialog(this,
						Messages.getText("_Option_not_available_for_this_geometry_type"),
						getWindowInfo().getTitle(),
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			ApplicationLocator.getManager().getUIManager().addWindow(stySel);
			ILabelStyle sty = (ILabelStyle) stySel.getSelectedObject();

			if (sty != null) {
				// gather the style and apply to the class
				clonedClass.setLabelStyle(sty);
				clonedClass.setUnit(stySel.getUnit());
				clonedClass.setReferenceSystem(stySel.getReferenceSystem());
				setLabelClass(clonedClass);
			}

//			styPreviewer.setStyle(sty);
			((StylePreviewer) styPreviewer).setStyle(sty);

		}else if (e.getSource().equals(rdBtnAllFeatures) || e.getSource().equals(rdBtnFilteredFeatures)){
			clonedClass.setUseSqlQuery(rdBtnFilteredFeatures.isSelected());
		}

		repaint();
	}

	private boolean validVisibleSQL(String sqlstr, FeatureStore fsto) {
	    
	    return LabelClassUtils.validExpression(sqlstr, fsto, true);
    }

    private void applySettings() {
	    
		clonedClass.setVisible(chkLabelFeatures.isSelected());
		clonedClass.setName(txtName.getText());
		
		if (rdBtnFilteredFeatures.isSelected()) {
		    clonedClass.setSQLQuery(txtSQL.getText());
		} else {
		    clonedClass.setSQLQuery("");
		}
		
		JTable tableFields=getTableFields();
		TableCellEditor cellEditor=tableFields.getCellEditor();
		if(cellEditor != null){
			int row = tableFields.getEditingRow();
			int column = tableFields.getEditingColumn();
			cellEditor.stopCellEditing();
			Object value = cellEditor.getCellEditorValue();

			if (value != null) {
				((FieldTableExpressions) tableFields.
						getModel()).setValueAt(value, row, column);
			}
		}
		clonedClass.setLabelExpressions(
				((FieldTableExpressions) tableFields.
						getModel()).getExpression());
		clonedClass.setTextSymbol(textStyle.getTextSymbol());
	}

	private void addField() {
		addField("");
	}

	private void addField(String fieldExpr) {
		FieldTableExpressions m = ((FieldTableExpressions) getTableFields().getModel());
		m.addRow(new Object[] { m.getRowCount()+1, fieldExpr });
	}

	private void delField(int fieldIndex) {
		try {
			((FieldTableExpressions) getTableFields().getModel()).removeRow(fieldIndex);
		} catch (ArrayIndexOutOfBoundsException ex) {}
	}


	public ILabelClass getLabelClass() {
		return lc;
	}



	private static final Object[] TABLE_HEADERS = new String[] {
		
		Messages.getText("field_number"),
		Messages.getText("label_expression")
	};
	
	/*
	private static String fieldSeparator =
	LabelExpressionParser.tokenImage[LabelExpressionParser.EOFIELD].replaceAll("\"", "");
	*/
	
	private class FieldTableExpressions extends DefaultTableModel {
		private static final long serialVersionUID = 2002427714889477770L;
		public FieldTableExpressions(String[] expressions) {
			super();

			if(expressions != null && expressions.length > 0){
				Integer[] aux = new Integer[expressions.length];
				for (int i = 0; i < aux.length; i++) {
					aux[i] = i+1;
				}

				Object[][] values = new Object[aux.length][2];
				for (int i = 0; i < values.length; i++) {
					values[i][0] = aux[i];
					values[i][1] = expressions[i];
				}
				setDataVector(values, TABLE_HEADERS);
			}
			else{
				Object[][] values = new Object[1][2];
				values[0][0] = 1;
				values[0][1] = "";
				setDataVector(values,TABLE_HEADERS);
			}

		}


		@Override
		public boolean isCellEditable(int row, int column) {
			return column == 1;
		}

		public String[] getExpression() {

			String[] expressions = new String[getRowCount()];

			for (int i = 0; i < getRowCount(); i++) {
				expressions[i] = ""+(String) getValueAt(i,1);
			}
			return expressions;
		}

	}

//	public static void main(String[] args) {
//		JFrame f = new JFrame("Test LabelClassProperties panel");
//
//		String xmlString =
//			"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" +
//			"<xml-tag xmlns=\"http://www.gvsig.gva.es\">\n" +
//			"    <property key=\"className\" value=\"com.iver.cit.gvsig.fmap.rendering.styling.labeling.LabelClass\"/>\n" +
//			"    <property key=\"isVisible\" value=\"false\"/>\n" +
//			"    <property key=\"name\" value=\"Default\"/>\n" +
//			"    <property key=\"labelExpression\" value=\"[TIPO] : lao39805502232 : Substring([MOTO], 2,2);\"/>\n" +
//			"    <property key=\"unit\" value=\"-1\"/>\n" +
//			"    <property key=\"referenceSystem\" value=\"0\"/>\n" +
//			"    <property key=\"sqlQuery\" value=\"TIPO = 'C'\"/>\n" +
//			"    <property key=\"priority\" value=\"0\"/>\n" +
//			"    <xml-tag>\n" +
//			"        <property key=\"className\" value=\"org.gvsig.symbology.fmap.styles.SimpleLabelStyle\"/>\n" +
//			"        <property key=\"desc\" value=\"Placa Carrer VLC.style\"/>\n" +
//			"        <property key=\"markerPointX\" value=\"0.0\"/>\n" +
//			"        <property key=\"markerPointY\" value=\"0.0\"/>\n" +
//			"        <property key=\"minXArray\" value=\"0.35 ,0.25\"/>\n" +
//			"        <property key=\"minYArray\" value=\"0.15 ,0.5\"/>\n" +
//			"        <property key=\"widthArray\" value=\"0.5 ,0.6\"/>\n" +
//			"        <property key=\"heightArray\" value=\"0.27 ,0.37\"/>\n" +
//			"        <property key=\"id\" value=\"labelStyle\"/>\n" +
//			"        <xml-tag>\n" +
//			"            <property key=\"className\" value=\"org.gvsig.symbology.fmap.styles.RemoteFileStyle\"/>\n" +
//			"            <property key=\"source\" value=\"http://www.boomlapop.com/boomlapop.jpg\"/>\n" +
//			"            <property key=\"desc\"/>\n" +
//			"            <property key=\"id\" value=\"LabelStyle\"/>\n" +
//			"        </xml-tag>\n" +
//			"    </xml-tag>\n" +
//			"    <xml-tag>\n" +
//			"        <property key=\"className\" value=\"com.iver.cit.gvsig.fmap.core.symbols.SimpleTextSymbol\"/>\n" +
//			"        <property key=\"desc\"/>\n" +
//			"        <property key=\"isShapeVisible\" value=\"true\"/>\n" +
//			"        <property key=\"font\" value=\"Arial\"/>\n" +
//			"        <property key=\"fontStyle\" value=\"1\"/>\n" +
//			"        <property key=\"size\" value=\"12\"/>\n" +
//			"        <property key=\"text\" value=\"GeneralLabeling.sample_text\"/>\n" +
//			"        <property key=\"textColor\" value=\"255,255,0,255\"/>\n" +
//			"        <property key=\"unit\" value=\"-1\"/>\n" +
//			"        <property key=\"referenceSystem\" value=\"0\"/>\n" +
//			"        <property key=\"id\" value=\"TextSymbol\"/>\n" +
//			"    </xml-tag>\n" +
//			"</xml-tag>\n" +
//			"";
//
//		LabelClass lc = null;
//		try {
//			XMLEntity xml = new XMLEntity((XmlTag) XmlTag.unmarshal(
//					XMLEncodingUtils.getReader(new ByteArrayInputStream(xmlString.getBytes()))));
//			lc = new LabelClass();
//			lc.setXMLEntity(xml);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		String[] names = new String[] { "Field1", "Field2", "Field3"	};
//		int[] types = new int[] { Types.VARCHAR, Types.INTEGER, Types.DOUBLE };
//		final LabelClassProperties lcp = new LabelClassProperties(
//				names, types ) {
//			private static final long serialVersionUID = -1843509415649666459L;
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				super.actionPerformed(e);
//				if ("OK".equals(e.getActionCommand())) {
//					Sys tem.out.println(getLabelClass().getXMLEntity());
//				}
//			}
//		};
//		lcp.setLabelClass(lc);
//		f.setContentPane(lcp);
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		f.pack();
//		f.setVisible(true);
//
//	}

}
