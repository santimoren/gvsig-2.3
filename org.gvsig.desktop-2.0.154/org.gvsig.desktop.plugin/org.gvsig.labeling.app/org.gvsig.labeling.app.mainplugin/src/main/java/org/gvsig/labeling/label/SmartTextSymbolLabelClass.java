/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.label;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelLocationMetrics;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.labeling.symbol.SmartTextSymbol;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentContext;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.persistence.spi.PersistentContextServices;


/**
 * <p>
 * SmartTextSymbolLabelClass.java<br>
 * </p>
 *
 * <p>
 *
 *   This is a <b>wrapper</b> to be able to use a SmartTextSymbol as a
 * LabelClass. A SmartTextSymbol uses a line instead of a point.
 * Being in fact a Line and not a Marker, it does not make any sense
 * to have label styles since the label styles are well-defined rectangle
 * areas where texts are placed in fields backgrounded by an image. In this
 * contexts, the area is defined dinamically for each line, and there is no
 * sense to have texts fields. They will be rendered as a single string
 * along a line.<br>
 * </p>
 * <p> The label itself is the SmartTextSymbol,
 * the geometry (the line), the label expression,
 * and the text applied to the symbol.<br>
 * </p>
 * <p>
 *   Most of the operations performed by this LabelClass are in fact
 * delegated to the symbol.<br>
 * </p>
 *
 *
 * @author jldominguez
 *
 */
public class SmartTextSymbolLabelClass implements ILabelClass {
    
    private static Logger logger = LoggerFactory.getLogger(
        SmartTextSymbolLabelClass.class);
    
    public static final String SMART_LABEL_CLASS_PERSISTENCE_DEFINITION_NAME =
        "Smart LabelClass";

    private ILabelClass defLabelClass = null;
	private SmartTextSymbol smartTextSymbol;
	
	public SmartTextSymbolLabelClass() {
	    defLabelClass = SymbologyLocator.getSymbologyManager().createDefaultLabel();
	}

	public void draw(Graphics2D graphics, ILabelLocationMetrics llm, Geometry geo) {
		getTextSymbol().draw(graphics, null, geo, null, null);
	}

	public void drawInsideRectangle(Graphics2D graphics, Rectangle bounds)
			throws SymbolDrawingException {
	    
		getTextSymbol().drawInsideRectangle(graphics, null, bounds, null);
	}


	public String getClassName() {
		return getClass().getName();
	}

	public ILabelStyle getLabelStyle() {
		// label style not allowed in this context
		return null;
	}


	public ITextSymbol getTextSymbol() {
		if (smartTextSymbol == null) {
			smartTextSymbol = new SmartTextSymbol();
		}
		return smartTextSymbol;
	}


	public void setLabelStyle(ILabelStyle labelStyle) {
		// operation not supported in this context
	}


	public void setTextSymbol(ITextSymbol textSymbol) {
	    if (textSymbol instanceof SmartTextSymbol) {
	        this.smartTextSymbol = (SmartTextSymbol) textSymbol;
	    } else {
	        logger.error("While setting text symbol in label class",
	            new Exception("Text symbol must be a SmartTextSymbol here"));
	    }
		
	    defLabelClass.setTextSymbol(textSymbol);
	}
	
	public void loadFromState(PersistentState state)
        throws PersistenceException {
	    defLabelClass.loadFromState(state);
	}
	
	public void saveToState(PersistentState state)
	    throws PersistenceException {
	    defLabelClass.saveToState(state);
	}
	
    public static void registerPersistent() {

        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if (manager.getDefinition(SMART_LABEL_CLASS_PERSISTENCE_DEFINITION_NAME)==null) {
            
            DynStruct definition = manager.addDefinition(
                SmartTextSymbolLabelClass.class,
                    SMART_LABEL_CLASS_PERSISTENCE_DEFINITION_NAME,
                    SMART_LABEL_CLASS_PERSISTENCE_DEFINITION_NAME
                    + " Persistence definition",
                    null, 
                    null
            );

            definition.extend(manager.getDefinition("LabelClass"));
        }
        
    }

    // ========================================================
    // Delegating other methods on default behavior
    // ========================================================

    public boolean isVisible() {
        return defLabelClass.isVisible();
    }

    public boolean isVisible(double scale) {
        return this.isVisible();
    }
    
    public void setVisible(boolean isVisible) {
        defLabelClass.setVisible(isVisible);
    }

    public String getName() {
        return defLabelClass.getName();
    }

    public void setName(String name) {
        defLabelClass.setName(name);
    }

    public void setTexts(String[] texts) {
        defLabelClass.setTexts(texts);
    }

    public String[] getTexts() {
        return defLabelClass.getTexts();
    }

    public int getPriority() {
        return defLabelClass.getPriority();
    }

    public void setPriority(int priority) {
        defLabelClass.setPriority(priority);
    }

    public Geometry getShape(ILabelLocationMetrics llm)
        throws CreateGeometryException {

        return defLabelClass.getShape(llm);
    }

    public double getCartographicSize(ViewPort viewPort, double dpi,
        Geometry geom) {

        return defLabelClass.getCartographicSize(viewPort, dpi, geom);
    }

    public int getReferenceSystem() {
        return defLabelClass.getReferenceSystem();
    }

    public int getUnit() {
        return defLabelClass.getUnit();
    }

    public void setCartographicSize(double cartographicSize, Geometry geom) {
        defLabelClass.setCartographicSize(cartographicSize, geom);
    }

    public void setReferenceSystem(int referenceSystem) {
        defLabelClass.setReferenceSystem(referenceSystem);
    }

    public void setUnit(int unitIndex) {
        defLabelClass.setUnit(unitIndex);
    }

    public double toCartographicSize(ViewPort viewPort, double dpi,
        Geometry geom) {

        return defLabelClass.toCartographicSize(viewPort, dpi, geom);
    }

    public Rectangle getBounds() {
        return defLabelClass.getBounds();
    }

    public String getSQLQuery() {
        return defLabelClass.getSQLQuery();
    }

    public void setSQLQuery(String sqlQuery) {
        defLabelClass.setSQLQuery(sqlQuery);
    }

    public String[] getLabelExpressions() {
        return defLabelClass.getLabelExpressions();
    }

    public void setLabelExpressions(String[] lbl_exps) {
        defLabelClass.setLabelExpressions(lbl_exps);
    }

    public void setUseSqlQuery(boolean use_sql) {
        defLabelClass.setUseSqlQuery(use_sql);
    }

    public boolean isUseSqlQuery() {
        return defLabelClass.isUseSqlQuery();
    }

    public String getStringLabelExpression() {
        return defLabelClass.getStringLabelExpression();
    }
    
   public Object clone() throws CloneNotSupportedException {

	PersistenceManager persman = ToolsLocator.getPersistenceManager();

        PersistentContext context = null;
        Object resp = null;
        PersistentState state = null;
        try {
            state = persman.getState(this, true);
        } catch (PersistenceException e) {
            logger.warn("Can't clone, return me !!!!!!!!!!!!!!", e);
            return this;
        }
        context = state.getContext();
        if (context instanceof PersistentContextServices) {
            /*
             * Ensure that previous instances are not used,
             * so objects are recreated
             */
            ((PersistentContextServices) context).clear();
        }
        try {
            resp = persman.create(state);
        } catch (Exception e) {
            logger.warn("Can't clone, return me !!!!!!!!!!!!!!", e);
            return this;
        }
        return resp;

    }

}
