/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.placements;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 *
 * MultiShapePlacementConstraints.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Apr 1, 2008
 *
 */
public class MultiShapePlacementConstraints extends AbstractPlacementConstraints {

	public static final String MULTI_SHAPE_PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME =
			"MULTI_SHAPE_PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME";
	
	private PolygonPlacementConstraints polygonConstraints;
	private LinePlacementConstraints lineConstraints;
	private PointPlacementConstraints pointConstraints;


	public MultiShapePlacementConstraints() { }

	public MultiShapePlacementConstraints(
			PointPlacementConstraints pointConstraints,
			LinePlacementConstraints lineConstraints,
			PolygonPlacementConstraints polygonConstraints) {
		this.pointConstraints = pointConstraints;
		this.lineConstraints = lineConstraints;
		this.polygonConstraints = polygonConstraints;

	}

	public void setPolygonConstraints(PolygonPlacementConstraints p) {
		this.polygonConstraints = p;
	}

	public void setLineConstraints(LinePlacementConstraints l) {
		this.lineConstraints = l;
	}

	public void setPointConstraints(PointPlacementConstraints p) {
		this.pointConstraints = p;
	}

	public PolygonPlacementConstraints getPolygonConstraints() {
		return polygonConstraints;
	}

	public LinePlacementConstraints getLineConstraints() {
		return lineConstraints;
	}

	public PointPlacementConstraints getPointConstraints() {
		return pointConstraints;
	}
	
	// ======================================
	
	public void saveToState(PersistentState state) throws PersistenceException {
		
		super.saveToState(state);
		state.set("pointConstraints", this.pointConstraints);
		state.set("lineConstraints", this.lineConstraints);
		state.set("polygonConstraints", this.polygonConstraints);
	}

	public void loadFromState(PersistentState state) throws PersistenceException {
		
		super.loadFromState(state);
		this.pointConstraints = (PointPlacementConstraints)
				state.get("pointConstraints");
		this.lineConstraints = (LinePlacementConstraints)
				state.get("lineConstraints");
		this.polygonConstraints = (PolygonPlacementConstraints)
				state.get("polygonConstraints");
		
	}

	public static void registerPersistent() {
		
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(MULTI_SHAPE_PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME) == null ) {
			DynStruct definition = manager.addDefinition(
					MultiShapePlacementConstraints.class,
					MULTI_SHAPE_PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME,
					MULTI_SHAPE_PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME +" Persistence definition",
					null, 
					null);
			
			definition.extend(manager.getDefinition(
					AbstractPlacementConstraints.PLACEMENT_CONSTRAINTS_PERSISTENCE_NAME));
			
			definition.addDynFieldObject("pointConstraints").setClassOfValue(
					PointPlacementConstraints.class).setMandatory(true);
			definition.addDynFieldObject("lineConstraints").setClassOfValue(
					LinePlacementConstraints.class).setMandatory(true);
			definition.addDynFieldObject("polygonConstraints").setClassOfValue(
					PolygonPlacementConstraints.class).setMandatory(true);
		}		
	}
	
	public Object clone() throws CloneNotSupportedException {
		
		
		PointPlacementConstraints point_co = null;
		LinePlacementConstraints line_co = null;
		PolygonPlacementConstraints poly_co = null;
		
		try {
			point_co = (PointPlacementConstraints) pointConstraints.clone();
			line_co = (LinePlacementConstraints) lineConstraints.clone();
			poly_co = (PolygonPlacementConstraints) polygonConstraints.clone();
		} catch (CloneNotSupportedException ce) {
			throw ce;
		}
		
		MultiShapePlacementConstraints resp =
				new MultiShapePlacementConstraints(point_co, line_co, poly_co);
		return resp;
	}

}
