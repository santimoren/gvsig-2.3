package org.gvsig.labeling.placements;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.AbstractStyle;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 *  Specifies the point position for a label
 *
 */
public class PointLabelPositioner extends AbstractStyle {

	private static Logger logger = LoggerFactory.getLogger(PointLabelPositioner.class);
	
	public static final String POINT_LABEL_POSITIONER_PERSISTENCE_NAME =
			"POINT_LABEL_POSITIONER_PERSISTENCE_NAME";
	
	private static Geometry sampleGeometry = null;
	private int[] preferenceVector = new int[8];
	private static final Color[] colorVector = new Color[] {
		new Color(140, 140, 140), // gray
		new Color(140, 245, 130), // green
		new Color(130, 170, 245), // light blue
		new Color(100, 100, 255),   // dark blue
	};

	public static final byte FORBIDDEN 		   = 0;
	public static final byte PREFERENCE_HIGH   = 1;
	public static final byte PREFERENCE_NORMAL = 2;
	public static final byte PREFERENCE_LOW    = 3;
	/**
	 * Constructor method
	 *
	 */
	public PointLabelPositioner() {}

	/**
	 * Constructor method
	 *
	 * @param preferenceVector
	 * @param description
	 */
	public PointLabelPositioner(int[] preferenceVector, String description) {
		this.preferenceVector = preferenceVector;
		setDescription(description);
	}

	public void drawInsideRectangle(Graphics2D g, Rectangle r) {
		int size = Math.min(r.width, r.height) / 3;
		int j = -1;
		final int fontSize = (int) (size * 0.8);
		final Font font = new Font("Arial", Font.PLAIN, fontSize);
		RenderingHints old = g.getRenderingHints();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (int i = 0; i < 9; i++) {
			if (i == 4) continue;
			j++;
			int value = Math.abs(preferenceVector[j] % colorVector.length);
			int col = i % 3;
			int row = i / 3;

			g.setColor(colorVector[value]);
			g.fillRect(size * col, size*row, size, size);
			g.setColor(Color.BLACK);
			g.drawRect(size * col, size*row, size, size);
			g.setFont(font);
			g.drawString(String.valueOf(value),
					(float) ((size/2) - (fontSize/4)) + size * col,
					(float) (size * 0.8) + size*row);
		}
		g.setRenderingHints(old);
	}

	public boolean isSuitableFor(ISymbol sym) {
		return sym.isSuitableFor(getSampleGeometry());
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		
		super.loadFromState(state);
		preferenceVector = state.getIntArray("preferenceVector");
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		
		super.saveToState(state);
		state.set("preferenceVector", preferenceVector);
	}
	
	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if( manager.getDefinition(POINT_LABEL_POSITIONER_PERSISTENCE_NAME)==null ) {
			DynStruct definition = manager.addDefinition(
					PointLabelPositioner.class,
					POINT_LABEL_POSITIONER_PERSISTENCE_NAME,
					POINT_LABEL_POSITIONER_PERSISTENCE_NAME+" Persistence definition",
					null, 
					null
			);
			definition.extend(manager.getDefinition(
					AbstractStyle.STYLE_PERSISTENCE_DEFINITION_NAME));
			definition.addDynFieldArray("preferenceVector").setClassOfItems(
					int.class).setMandatory(true);
		}
		
	}
	
	/*
	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());
		xml.putProperty("desc", getDescription());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < preferenceVector.length; i++) {
			sb.append(preferenceVector[i]+" ,");
		}
		String s = sb.substring(0, sb.length()-2);
		xml.putProperty("preferenceVector", s);
		return xml;
	}

	public void setXMLEntity(XMLEntity xml) {
		setDescription(xml.getStringProperty("desc"));
		preferenceVector = xml.getByteArrayProperty("preferenceVector");
	}
	*/

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}	
	
	public void drawOutline(Graphics2D g, Rectangle r) {
		drawInsideRectangle(g, r);
	}

	public int[] getPreferenceVector() {
		return preferenceVector;
	}
	
	private static Geometry getSampleGeometry() {
		
		if (sampleGeometry == null) {
			try {
				sampleGeometry = GeometryLocator.getGeometryManager().createPoint(
						0, 0, Geometry.SUBTYPES.GEOM2D);
			} catch (Exception e) {
				logger.error("While getting sample geometry.", e);
			}
		}
		return sampleGeometry;
	}
}
