/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.placements;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelLocationMetrics;
import org.gvsig.tools.task.Cancellable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * PolygonPlacementOnCentroid.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Apr 1, 2008
 *
 */
//public class PolygonPlacementOnCentroid extends MarkerCenteredAtPoint implements ILabelPlacement {
public class PolygonPlacementOnCentroid extends MarkerPlacementOnPoint
implements ILabelPlacement {
	
	private static final Logger logger = LoggerFactory.getLogger(
			PolygonPlacementOnCentroid.class);

	public ArrayList<LabelLocationMetrics> guess(
			ILabelClass lc, Geometry geom,
			IPlacementConstraints placementConstraints,
			double cartographicSymbolSize, Cancellable cancel, ViewPort vp) {

		if (cancel.isCanceled()) return CannotPlaceLabel.NO_PLACES;

		Point cen = null;
		try {
			cen = geom.centroid();
		} catch (Exception e) {
			logger.error("While getting centroid.", e);
		}
		return super.guess(
				lc,
				cen,
				placementConstraints,
				cartographicSymbolSize, cancel,vp);
	}

	public ArrayList<LabelLocationMetrics> guess(
			ILabelClass lc, Geometry geom,
			IPlacementConstraints placementConstraints,
			double cartographicSymbolSize, Cancellable cancel) {

		if (cancel.isCanceled()) return CannotPlaceLabel.NO_PLACES;

		ArrayList<LabelLocationMetrics> guessed =
				new ArrayList<LabelLocationMetrics>();
		
		Point cen = null;
		try {
			cen = geom.centroid();
		} catch (Exception e) {
			logger.error("While getting centroid.", e);
		}
		Point2D p = new Point2D.Double(cen.getX(), cen.getY());
		Rectangle2D bounds = lc.getBounds();
		p.setLocation(
				p.getX() - (bounds.getWidth()*0.5),
				p.getY() - (bounds.getHeight()*0.5));// - 2);
		guessed.add(new LabelLocationMetrics(p, 0, true));
		return guessed;
	}

	public boolean isSuitableFor(
			IPlacementConstraints placementConstraints,
			int shapeType) {
		
		GeometryType gt = null;
		try {
			gt = GeometryLocator.getGeometryManager().getGeometryType(
					shapeType, SUBTYPES.GEOM2D);
		} catch (Exception e) {
			logger.error("While getting geo type.", e);
		}
		
		if (gt.isTypeOf(TYPES.SURFACE) || shapeType == TYPES.MULTISURFACE) {
			return (placementConstraints.isHorizontal()
					&&
					!placementConstraints.isFitInsidePolygon());
		}
		return false;
	}

}
