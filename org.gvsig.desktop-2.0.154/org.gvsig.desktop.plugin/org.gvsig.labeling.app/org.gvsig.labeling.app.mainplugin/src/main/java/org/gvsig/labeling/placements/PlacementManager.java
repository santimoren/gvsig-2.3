/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.placements;

import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.tools.locator.LocatorException;


/**
 *
 * PlacementManager.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Jan 3, 2008
 *
 */
public class PlacementManager {

	private static Logger logger = LoggerFactory.getLogger(PlacementManager.class);

	private static ArrayList<Class<?>> installedLabelPlacements = new ArrayList<Class<?>>();
	private static ArrayList<ILabelPlacement> availableLabelPlacements;
	private static final CannotPlaceLabel cantPlaceLabel = new CannotPlaceLabel();
	private PlacementManager() {}

	public static ILabelPlacement getPlacement(
			IPlacementConstraints placementConstraints, int shapeType) {

		ArrayList<ILabelPlacement> suitablePlacements = new ArrayList<ILabelPlacement>();
		if (placementConstraints.getClass().equals(MultiShapePlacementConstraints.class)) {
			MultiShapePlacementConstraints msp = (MultiShapePlacementConstraints) placementConstraints;

			return new MultiShapePlacement(
					getPlacement(msp.getPointConstraints(),   Geometry.TYPES.POINT),
					getPlacement(msp.getLineConstraints(),    Geometry.TYPES.CURVE),
					getPlacement(msp.getPolygonConstraints(), Geometry.TYPES.SURFACE)
					);
		} else {
			for (Iterator<ILabelPlacement> iterator = getAvailablePlacements().iterator(); iterator.hasNext();) {
				ILabelPlacement placement = iterator.next();
				if (placement.isSuitableFor(placementConstraints, shapeType)) {
					suitablePlacements.add(placement);
				}
			}

			if (suitablePlacements.size() == 0)
				return cantPlaceLabel;
			else if (suitablePlacements.size() == 1)
				return suitablePlacements.get(0);
			else
				return new CompoundLabelPlacement(
						(ILabelPlacement[]) suitablePlacements.
						toArray(new ILabelPlacement[suitablePlacements.size()]));

		}

	}

	public static void addLabelPlacement(Class<?> labelPlacementClass) {
//		if (!labelPlacementClass.isInstance(ILabelPlacement.class))
//		throw new IllegalArgumentException(
//			labelPlacementClass.getName()+" is not an valid label placement algorithm.");
		installedLabelPlacements.add(labelPlacementClass);

		// invalidate current list of available placements
		if (availableLabelPlacements != null)
			availableLabelPlacements.clear();
		availableLabelPlacements = null;
	}

	private static ArrayList<ILabelPlacement> getAvailablePlacements() {
		if (availableLabelPlacements == null) {
			availableLabelPlacements = new ArrayList<ILabelPlacement>(installedLabelPlacements.size());
			for (Iterator<Class<?>> iterator = installedLabelPlacements.iterator(); iterator.hasNext();) {
				Class<?> clazz = null;
				try {
					clazz = iterator.next();
					availableLabelPlacements.add((ILabelPlacement) clazz.newInstance());
				} catch (Exception e) {
					logger.error("Couldn't install label placement: "+clazz.getName(), e);
				}
			}
		}

		return availableLabelPlacements;
	}


	/**
	 * Creates a new instance of placement constraints from a vector layer. The
	 * placement constraints are created according the layer shape type.
	 * @param layerDest
	 * @return
	 * @throws LocatorException
	 * @throws GeometryTypeNotValidException
	 * @throws GeometryTypeNotSupportedException
	 */
	public static IPlacementConstraints createPlacementConstraints(int geotype)
			throws Exception {

		GeometryType geom_gt = GeometryLocator.getGeometryManager().getGeometryType(
				geotype, Geometry.SUBTYPES.GEOM2D);

		if (geom_gt.getType() == TYPES.POINT ||
				geom_gt.getType() == TYPES.MULTIPOINT) {
			return new PointPlacementConstraints();
		} else {
			if (geom_gt.isTypeOf(TYPES.CURVE) ||
					geom_gt.getType() == TYPES.MULTICURVE) {
				return new LinePlacementConstraints();
			} else {
				if (geom_gt.isTypeOf(TYPES.SURFACE) ||
						geom_gt.getType() == TYPES.MULTISURFACE) {
					return new PolygonPlacementConstraints();
				} else {
					if (geom_gt.getType() == TYPES.GEOMETRY ||
					geom_gt.getType() == TYPES.AGGREGATE) {

						return new MultiShapePlacementConstraints(
								(PointPlacementConstraints)
								createPlacementConstraints(TYPES.POINT),
								(LinePlacementConstraints)
								createPlacementConstraints(TYPES.CURVE),
								(PolygonPlacementConstraints)
								createPlacementConstraints(TYPES.SURFACE));
					}
				}
			}
		}
		logger.error("Unknown geo type in createPlacementConstraints: " + geotype);
		return null;
	}



}
