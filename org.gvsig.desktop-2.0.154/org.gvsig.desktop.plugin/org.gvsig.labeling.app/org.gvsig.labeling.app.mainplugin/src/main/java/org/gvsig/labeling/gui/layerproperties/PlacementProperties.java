/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: PlacementProperties.java 15674 2007-10-30 16:39:02Z jdominguez $
* $Log$
* Revision 1.7  2007-04-19 14:22:29  jaume
* *** empty log message ***
*
* Revision 1.6  2007/04/13 12:43:08  jaume
* *** empty log message ***
*
* Revision 1.5  2007/04/13 12:10:56  jaume
* *** empty log message ***
*
* Revision 1.4  2007/04/12 16:01:32  jaume
* *** empty log message ***
*
* Revision 1.3  2007/03/28 15:38:56  jaume
* GUI for lines, points and polygons
*
* Revision 1.2  2007/03/09 11:25:00  jaume
* Advanced symbology (start committing)
*
* Revision 1.1.2.2  2007/02/09 11:00:03  jaume
* *** empty log message ***
*
* Revision 1.1.2.1  2007/02/01 12:12:41  jaume
* theme manager window and all its components are now dynamic
*
*
*/
package org.gvsig.labeling.gui.layerproperties;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import org.cresques.cts.IProjection;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.gui.styling.SingleStyleSelectorFilter;
import org.gvsig.app.gui.styling.StylePreviewer;
import org.gvsig.app.gui.styling.StyleSelector;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelClass;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.IStyle;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.i18n.Messages;
import org.gvsig.labeling.label.GeneralLabelingStrategy;
import org.gvsig.labeling.lang.LabelClassUtils;
import org.gvsig.labeling.placements.MultiShapePlacementConstraints;
import org.gvsig.labeling.placements.PlacementManager;
import org.gvsig.labeling.placements.PointLabelPositioner;
import org.gvsig.labeling.placements.PointPlacementConstraints;
import org.gvsig.symbology.SymbologyLocator;
import org.gvsig.symbology.SymbologyManager;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.fill.ISimpleFillSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.line.ISimpleLineSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.IArrowDecoratorStyle;
import org.gvsig.symbology.fmap.mapcontext.rendering.symbol.style.ISimpleLineStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class PlacementProperties extends JPanel implements
IPlacementProperties, ActionListener {
	
	private static final Logger logger =
			LoggerFactory.getLogger(PlacementProperties.class);
	
	private static final long serialVersionUID = 1022470370547576765L;
	
	private PointLabelPositioner defaultPointStyle = new PointLabelPositioner(
			new int[] { 2, 2, 1, 3, 2, 3, 3, 2	},
			Messages.getText("prefer_top_right_all_allowed")
			);

	public IPlacementConstraints constraints;

	private PointLabelPositioner pointStyle = null;
	private int shapeType;
	private JPanel pnlContent = null;
	private JPanel pnlCenter = null;
	private JPanel orientationPanel;
	private DuplicateLayersMode	duplicateLabelsMode;
	private JRadioButton rdBtnHorizontal;
	private JRadioButton rdBtnParallel;
	private JRadioButton rdBtnFollowingLine;
	private JRadioButton rdBtnPerpendicular;
	private JPanel positionPanel;
	private MiniMapContext preview;
	private ArrayList<String> linePositionItem = new ArrayList<String>();

	private JPanel polygonSettingsPanel;
	private JRadioButton rdBtnAlwaysHorizontal;
	private JRadioButton rdBtnAlwaysStraight;
	private GridBagLayoutPanel pointSettingsPanel;
	private JRadioButton rdBtnOffsetLabelHorizontally;
	private JRadioButton rdBtnOffsetLabelOnTopPoint;
	private StylePreviewer stylePreview;
	private JButton btnChangeLocation;
	private JPanel locationPanel;
	private JComboBox cmbLocationAlongLines;
	private JRadioButton chkBellow;
	private JRadioButton chkOnTheLine;
	private JRadioButton chkAbove;
	private JComboBox cmbOrientationSystem;
	private JLabelHTML lblPointPosDesc = new JLabelHTML(""); //pointStyle.getDescription());
	IPlacementConstraints oldConstraints;
	private JCheckBox chkFitInsidePolygon;


	/**
	 * Constructs a new panel for PlacementProperties.
	 *
	 * @param constraints, if not null this parameters is used to fill the panel. If it is null
	 * this PlacementProperties constructor creates a new default one that is ready to be used
	 * for the shapetype passed as second parameter
	 *
	 * @param shapeType, defines the target shapetype of the IPlacementConstraints obtained;
	 * @throws ReadDriverException
	 */
	public PlacementProperties(IPlacementConstraints constraints, int shapeType) {
		initialize(constraints, shapeType, getPnlDuplicateLabels());
	}

	PlacementProperties(IPlacementConstraints costraints,
			int shapeType, DuplicateLayersMode duplicatesMode) {

		initialize( costraints, shapeType, duplicatesMode);
		refreshComponents();
	}

	private void refreshComponents() {
		getChkOnTheLine().setSelected(constraints.isOnTheLine());
		getChkAbove().setSelected(constraints.isAboveTheLine());
		getChkBelow().setSelected(constraints.isBelowTheLine());

		getRdBtnHorizontal().setSelected(constraints.isHorizontal());
		getRdBtnParallel().setSelected(constraints.isParallel());
		getRdBtnFollowingLine().setSelected(constraints.isFollowingLine());
		getRdBtnPerpendicular().setSelected(constraints.isPerpendicular());

		getCmbOrientationSystem().setSelectedIndex(constraints.isPageOriented() ? 1 : 0);

		// points mode
		if (constraints.isOnTopOfThePoint()) {
			getRdOffsetLabelOnTopPoint().setSelected(true);
		} else if (constraints.isAroundThePoint()) {
			getRdOffsetLabelHorizontally().setSelected(true);
		}

		// lines mode
		if (constraints.isAtTheBeginingOfLine()) {
			getCmbLocationAlongLines().setSelectedIndex(1);
		} else if (constraints.isInTheMiddleOfLine()) {
			getCmbLocationAlongLines().setSelectedIndex(0);
		} else if (constraints.isAtTheEndOfLine()) {
			getCmbLocationAlongLines().setSelectedIndex(2);
		} else if (constraints.isAtBestOfLine()) {
			getCmbLocationAlongLines().setSelectedIndex(3);
		}

		// polygon mode
		getChkFitInsidePolygon().setSelected(constraints.isFitInsidePolygon());
		if (constraints.isHorizontal()) {
			getRdBtnAlwaysHorizontal().setSelected(true);
		} else if (constraints.isParallel()) {
			getRdBtnAlwaysStraight().setSelected(true);
		}

//		if(constraints.isFollowingLine()){
//			setComponentEnabled(getPositionPanel(), false);
//		}


		// duplicates mode
		int dupMode = constraints.getDuplicateLabelsMode();
		duplicateLabelsMode.setMode(dupMode);
		applyToPreview();
	}

	private void initialize(
			IPlacementConstraints constraints,
			int shapeType, DuplicateLayersMode duplicatesMode) {
	    
        this.duplicateLabelsMode = duplicatesMode;

		this.shapeType = shapeType;
		this.oldConstraints = constraints;
//		this.constraints = constraints != null ?
//				PlacementManager.createPlacementConstraints(constraints.getXMLEntity())	:
//				PlacementManager.createPlacementConstraints(shapeType);
		if( constraints != null ) {
			
			this.constraints = (IPlacementConstraints) LabelClassUtils.clone(constraints);
			
			if (constraints instanceof PointPlacementConstraints) {
				PointLabelPositioner positioneer = ((PointPlacementConstraints)constraints).getPositioner();
				if (positioneer != null) this.setPointStyle(positioneer);
				this.lblPointPosDesc.setText(this.getPointStyle().getDescription());
			}
		} else {
			try {
				this.constraints = PlacementManager.createPlacementConstraints(shapeType);
			} catch (Exception e) {
				logger.error("While initializing placement constraints.", e);
				return;
			}
		}
		
		/*
		 * We need to update this here because GUI events would
		 * remove the previous value
		 */
		this.getPnlDuplicateLabels().setMode(this.constraints.getDuplicateLabelsMode());

		linePositionItem.add(Messages.getText("in_the_middle"));
		linePositionItem.add(Messages.getText("at_begin"));
		linePositionItem.add(Messages.getText("at_end"));
		linePositionItem.add(Messages.getText("at_best"));

        this.setLayout(new BorderLayout());
        this.setSize(new Dimension(410,380));
        this.add(getPnlContent(), BorderLayout.CENTER);
 	}

	public WindowInfo getWindowInfo() {
		WindowInfo viewInfo = new WindowInfo(
				WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
		viewInfo.setWidth(440);
		viewInfo.setHeight(520);
		viewInfo.setTitle(Messages.getText("placement_properties"));
		return viewInfo;
	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}

	private JPanel getPnlContent() {
		if (pnlContent == null) {
			pnlContent = new JPanel();
			pnlContent.setLayout(new BorderLayout());
			pnlContent.add(getPnlCenter(), java.awt.BorderLayout.CENTER);
			pnlContent.add(getPnlDuplicateLabels(), java.awt.BorderLayout.SOUTH);
		}
		return pnlContent;
	}

	private DuplicateLayersMode getPnlDuplicateLabels() {
		if (duplicateLabelsMode == null) {
			duplicateLabelsMode = new DuplicateLayersMode(this);
		}
		return duplicateLabelsMode;
	}

	private JPanel getPnlCenter() {
		
		if (pnlCenter == null) {
			pnlCenter = new JPanel();
			
			GeometryType gt = null;
			try {
				gt = GeometryLocator.getGeometryManager().getGeometryType(
						shapeType, SUBTYPES.GEOM2D);
			} catch (Exception e) {
				logger.error("While getting panel.", e);
				return null;
			}
			
			if (gt.isTypeOf(TYPES.POINT) || gt.isTypeOf(TYPES.MULTIPOINT)) {
				pnlCenter.setBorder(BorderFactory.
						createTitledBorder(null,
						Messages.getText("point_settings")));
				pnlCenter.add(getPointSettingsPanel());
			} else {
				if (gt.isTypeOf(TYPES.CURVE) || gt.isTypeOf(TYPES.MULTICURVE)) {
					pnlCenter.setLayout(new BorderLayout());
					pnlCenter.setBorder(BorderFactory.
							createTitledBorder(null,
									Messages.getText("line_settings")));
					JPanel aux = new JPanel(
							new GridLayout(1, 2));
					aux.add(getOrientationPanel());
					aux.add(getPositionPanel());
					pnlCenter.add(aux, BorderLayout.CENTER);
					pnlCenter.add(getLocationPanel(), BorderLayout.SOUTH);
				} else {
					if (gt.isTypeOf(TYPES.SURFACE) || gt.isTypeOf(TYPES.MULTISURFACE)) {
						pnlCenter.setLayout(new BorderLayout());
						pnlCenter.setBorder(BorderFactory.
								createTitledBorder(null,
										Messages.getText("polygon_settings")));
						pnlCenter.add(getPolygonSettingsPanel(), BorderLayout.CENTER);
					}
				}
			}
		}
		return pnlCenter;
	}

	private JPanel getLocationPanel() {
		if (locationPanel == null) {
			locationPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
			locationPanel.setBorder(BorderFactory.
					createTitledBorder(null,
							Messages.getText("location")));
			locationPanel.add(new JLabel(Messages.getText(
					"location_along_the_lines")+":"));
			locationPanel.add(getCmbLocationAlongLines());
		}

		return locationPanel;
	}

	private JComboBox getCmbLocationAlongLines() {
		if (cmbLocationAlongLines == null) {
			cmbLocationAlongLines = new JComboBox((String[]) linePositionItem.toArray(new String[linePositionItem.size()]));
			cmbLocationAlongLines.addActionListener(this);
		}

		return cmbLocationAlongLines;
	}

	private PointLabelPositioner getPointStyle(){
		if (pointStyle == null){
			pointStyle = defaultPointStyle;
			lblPointPosDesc.setText(pointStyle.getDescription());
		}
		return pointStyle;
	}

	private void setPointStyle(PointLabelPositioner pointStyle){
		this.pointStyle = pointStyle;
	}

	private GridBagLayoutPanel getPointSettingsPanel() {
		if (pointSettingsPanel == null) {
			pointSettingsPanel = new GridBagLayoutPanel();
			pointSettingsPanel.addComponent(getRdOffsetLabelHorizontally());
			JPanel aux = new JPanel();
			aux.add(getStylePreviewer());

			JPanel aux2 = new JPanel();
			aux2.setLayout(new BoxLayout(aux2, BoxLayout.Y_AXIS));
			aux2.add(lblPointPosDesc);
			aux2.add(new JBlank(20, 5));
			aux2.add(getBtnChangeLocation());

			aux.add(aux2);
			pointSettingsPanel.addComponent("", aux);
			pointSettingsPanel.addComponent("",
					new JLabel(Messages.getText("label-point-priority-help")));
			pointSettingsPanel.addComponent(getRdOffsetLabelOnTopPoint());
			ButtonGroup group = new ButtonGroup();
			group.add(getRdOffsetLabelHorizontally());
			group.add(getRdOffsetLabelOnTopPoint());
		}

		return pointSettingsPanel;
	}



	private JButton getBtnChangeLocation() {
		if (btnChangeLocation == null) {
			btnChangeLocation = new JButton(Messages.getText("change_location"));
			btnChangeLocation.addActionListener(this);
		}

		return btnChangeLocation;
	}

	private StylePreviewer getStylePreviewer() {
		if (stylePreview == null) {
			stylePreview = new StylePreviewer();
			stylePreview.setStyle(getPointStyle());
			stylePreview.setPreferredSize(new Dimension(80, 80));
		}
		return stylePreview;
	}

	private JRadioButton getRdOffsetLabelOnTopPoint() {
		if (rdBtnOffsetLabelOnTopPoint == null) {
			rdBtnOffsetLabelOnTopPoint = new JRadioButton(
					Messages.getText("offset_labels_on_top_of_the_points")) ;

		}

		return rdBtnOffsetLabelOnTopPoint;
	}

	private JRadioButton getRdOffsetLabelHorizontally() {
		if (rdBtnOffsetLabelHorizontally == null) {
			rdBtnOffsetLabelHorizontally = new JRadioButton(
					Messages.getText("offset_labels_horizontally"));

		}

		return rdBtnOffsetLabelHorizontally;
	}

	private JPanel getPolygonSettingsPanel() {
		if (polygonSettingsPanel == null) {
			polygonSettingsPanel = new JPanel(new BorderLayout(10, 10));
			JPanel aux = new JPanel();
			aux.setLayout(new BoxLayout(aux, BoxLayout.Y_AXIS));
			aux.add(new JBlank(10,10));
			aux.add(getRdBtnAlwaysHorizontal());
			aux.add(new JBlank(10,10));
			aux.add(getRdBtnAlwaysStraight());
			aux.add(new JBlank(10,50));

			polygonSettingsPanel.add(getPreview(), BorderLayout.CENTER);
			polygonSettingsPanel.add(aux, BorderLayout.EAST);
			ButtonGroup group = new ButtonGroup();
			group.add(getRdBtnAlwaysHorizontal());
			group.add(getRdBtnAlwaysStraight());
			polygonSettingsPanel.add(getChkFitInsidePolygon(), BorderLayout.SOUTH);
		}

		return polygonSettingsPanel;
	}

	private JCheckBox getChkFitInsidePolygon() {
		if (chkFitInsidePolygon == null) {
			chkFitInsidePolygon = new JCheckBox(
					Messages.getText("fit_inside_polygon"));
			chkFitInsidePolygon.addActionListener(this);

		}

		return chkFitInsidePolygon;
	}

	private JRadioButton getRdBtnAlwaysStraight() {
		if (rdBtnAlwaysStraight == null) {
			rdBtnAlwaysStraight = new JRadioButton(
					Messages.getText("always_straight"));
			rdBtnAlwaysStraight.addActionListener(this);
		}

		return rdBtnAlwaysStraight;
	}

	private JRadioButton getRdBtnAlwaysHorizontal() {
		if (rdBtnAlwaysHorizontal == null) {
			rdBtnAlwaysHorizontal = new JRadioButton(
					Messages.getText("always_horizontal"));
			rdBtnAlwaysHorizontal.addActionListener(this);
		}

		return rdBtnAlwaysHorizontal;
	}



	private JPanel getPositionPanel() {
		if (positionPanel == null) {
			positionPanel = new JPanel(new BorderLayout());
			positionPanel.setBorder(BorderFactory.
					createTitledBorder(null,
							Messages.getText("position")));
			GridBagLayoutPanel aux2 = new GridBagLayoutPanel();
			JPanel aux = new JPanel(new GridLayout(4, 1));

			aux.add(getChkAbove());
			getChkAbove().setSelected(true);
			aux.add(getChkOnTheLine());
			aux.add(getChkBelow());

			ButtonGroup group = new ButtonGroup();
			group.add(getChkAbove());
			group.add(getChkOnTheLine());
			group.add(getChkBelow());

			aux2.addComponent(aux);
			aux2.addComponent(
					Messages.getText("orientation_system"), getCmbOrientationSystem());
			positionPanel.add(getPreview(), BorderLayout.CENTER);
			positionPanel.add(aux2, BorderLayout.SOUTH);
		}
		return positionPanel;
	}

	private Component getPreview() {
		 if (preview == null) {
			 preview = new MiniMapContext(shapeType);
		 }
		 return preview;
	}

	private JComboBox getCmbOrientationSystem() {
		if (cmbOrientationSystem == null) {
			cmbOrientationSystem = new JComboBox(new String[] {
					Messages.getText("line"),
					Messages.getText("page")
			});
			cmbOrientationSystem.setSelectedIndex(1);
			cmbOrientationSystem.addActionListener(this);
		}

		return cmbOrientationSystem;
	}

	private JRadioButton getChkBelow() {
		if (chkBellow == null) {
			chkBellow = new JRadioButton(Messages.getText("below"));
			chkBellow.addActionListener(this);
		}
		return chkBellow;
	}

	private JRadioButton getChkOnTheLine() {
		if (chkOnTheLine == null) {
			chkOnTheLine = new JRadioButton(
					Messages.getText("on_the_line"));
			chkOnTheLine.addActionListener(this);
		}
		return chkOnTheLine;
	}

	private JRadioButton getChkAbove() {
		if (chkAbove == null) {
			chkAbove = new JRadioButton(Messages.getText("above"));
			chkAbove.addActionListener(this);
		}
		return chkAbove;
	}

	private JPanel getOrientationPanel() {
		if (orientationPanel == null) {
		    
		    orientationPanel = new JPanel(new BorderLayout());
            orientationPanel.setBorder(BorderFactory.
                createTitledBorder(null,
                        Messages.getText("orientation")));
		    
            GridBagLayoutPanel auxp = new GridBagLayoutPanel();
            auxp.addComponent(getRdBtnHorizontal());
            auxp.addComponent(getRdBtnParallel());
            auxp.addComponent(getRdBtnFollowingLine());
            auxp.addComponent(getRdBtnPerpendicular());
            
            orientationPanel.add(auxp, BorderLayout.NORTH);
            
			ButtonGroup group = new ButtonGroup();
			group.add(getRdBtnHorizontal());
			group.add(getRdBtnParallel());
			group.add(getRdBtnFollowingLine());
			group.add(getRdBtnPerpendicular());
		}
		return orientationPanel;
	}

	private JRadioButton getRdBtnParallel() {
		if (rdBtnParallel == null) {
			rdBtnParallel = new JRadioButton(
					Messages.getText("parallel"));

			rdBtnParallel.addActionListener(this);
		}
		return rdBtnParallel;
	}

	private JRadioButton getRdBtnFollowingLine() {
		if (rdBtnFollowingLine == null) {
			rdBtnFollowingLine = new JRadioButton(
					Messages.getText("following_line"));
			rdBtnFollowingLine.addActionListener(this);
		}
		return rdBtnFollowingLine;
	}

	private JRadioButton getRdBtnPerpendicular() {
		if (rdBtnPerpendicular == null) {
			rdBtnPerpendicular = new JRadioButton(
					Messages.getText("perpedicular"));
			rdBtnPerpendicular.addActionListener(this);
		}
		return rdBtnPerpendicular;
	}

	private JRadioButton getRdBtnHorizontal() {
		if (rdBtnHorizontal == null) {
			rdBtnHorizontal = new JRadioButton(
					Messages.getText("horizontal"));
			rdBtnHorizontal.addActionListener(this);
		}
		return rdBtnHorizontal;
	}




	public void applyConstraints() throws GeometryException {
		int mode=0;
		GeometryType gt = null;
		gt = GeometryLocator.getGeometryManager().getGeometryType(
				shapeType, SUBTYPES.GEOM2D);
		
		if (gt.isTypeOf(TYPES.POINT) || gt.isTypeOf(TYPES.MULTIPOINT)) {
			if (getRdOffsetLabelOnTopPoint().isSelected()) {
				mode = IPlacementConstraints.ON_TOP_OF_THE_POINT;
			} else if (getRdOffsetLabelHorizontally().isSelected()) {
				mode = IPlacementConstraints.OFFSET_HORIZONTALY_AROUND_THE_POINT;
			}
			((PointPlacementConstraints) constraints).
				setPositioner((PointLabelPositioner) stylePreview.getStyle());
		} else {
			if (gt.isTypeOf(TYPES.CURVE) || gt.isTypeOf(TYPES.MULTICURVE)) {
				if (getRdBtnFollowingLine().isSelected()) {
					mode = IPlacementConstraints.FOLLOWING_LINE;
				} else if (getRdBtnParallel().isSelected()) {
					mode = IPlacementConstraints.PARALLEL;
				} else if (getRdBtnPerpendicular().isSelected()) {
					mode = IPlacementConstraints.PERPENDICULAR;
				} else {
					mode = IPlacementConstraints.HORIZONTAL;
				}

				constraints.setAboveTheLine(getChkAbove().isSelected());
				constraints.setBelowTheLine(getChkBelow().isSelected());
				constraints.setOnTheLine(getChkOnTheLine().isSelected());

				constraints.setPageOriented(
						getCmbOrientationSystem().getSelectedIndex() == 1);
				int i = getCmbLocationAlongLines().getSelectedIndex();
				if (i == 0) {
					i = IPlacementConstraints.AT_THE_MIDDLE_OF_THE_LINE;
				} else if (i == 1) {
					i = IPlacementConstraints.AT_THE_BEGINING_OF_THE_LINE;
				} else if (i == 2) {
					i = IPlacementConstraints.AT_THE_END_OF_THE_LINE;
				} else if (i == 3) {
					i = IPlacementConstraints.AT_BEST_OF_LINE;
				}
				constraints.setLocationAlongTheLine(i);
			} else {
				if (gt.isTypeOf(TYPES.SURFACE) || gt.isTypeOf(TYPES.MULTISURFACE)) {
					mode = IPlacementConstraints.HORIZONTAL;
					if (getRdBtnAlwaysHorizontal().isSelected()) {
						mode = IPlacementConstraints.HORIZONTAL;
					} else if (getRdBtnAlwaysStraight().isSelected()) {
						mode = IPlacementConstraints.PARALLEL;
					}

					constraints.setFitInsidePolygon(getChkFitInsidePolygon().isSelected());
				}
			}
		}		
		
		constraints.setPlacementMode(mode);
		constraints.setDuplicateLabelsMode(duplicateLabelsMode.getMode());
	}

	private void setComponentEnabled(Component c, boolean b) {
		if (c instanceof JComponent) {
			JComponent c1 = (JComponent) c;
			for (int i = 0; i < c1.getComponentCount(); i++) {
				setComponentEnabled(c1.getComponent(i), b);
			}
		}
		c.setEnabled(b);
	}


	public IPlacementConstraints getPlacementConstraints() {
		return constraints;
	}

	public void actionPerformed(ActionEvent e) {
		JComponent c = (JComponent) e.getSource();
		boolean okPressed = "OK".equals(e.getActionCommand());
		boolean cancelPressed = "CANCEL".equals(e.getActionCommand());
		if (okPressed || cancelPressed) {
			if (okPressed) {
				try {
					applyConstraints();
				} catch (GeometryException e1) {
					logger.error("While applying constraints.", e1);
				}
			}

			if ("CANCEL".equals(e.getActionCommand()))
				constraints = oldConstraints;
			
			ApplicationLocator.getManager().getUIManager().closeWindow(this);

			return;
		} else if (c.equals(rdBtnAlwaysHorizontal)
				|| c.equals(rdBtnAlwaysStraight) ) {
			
			
		} else if (c.equals(rdBtnHorizontal)) {
			// lock position panel and location panel
			setComponentEnabled(getPositionPanel(), false);
			setComponentEnabled(getLocationPanel(), false);
		} else if (c.equals(rdBtnParallel) || c.equals(rdBtnPerpendicular)) {
			// unlock position panel and location panel but keep orientation system locked
			setComponentEnabled(getLocationPanel(), true);
			setComponentEnabled(getPositionPanel(), true);
			getCmbOrientationSystem().setEnabled(true);
		} else if (c.equals(rdBtnFollowingLine)) {
			setComponentEnabled(getLocationPanel(), true);
			setComponentEnabled(getPositionPanel(), true);
			getCmbOrientationSystem().setSelectedItem(Messages.getText("line"));
			getCmbOrientationSystem().setEnabled(false);
		} else if (c.equals(btnChangeLocation)) {
			StyleSelector stySel = new StyleSelector(
				getPointStyle(),
				TYPES.POINT,
				new SingleStyleSelectorFilter(PointLabelPositioner.class));
			
			ApplicationLocator.getManager().getUIManager().addWindow(stySel);
			
			IStyle sty = (IStyle) stySel.getSelectedObject();
			if (sty != null) {
				stylePreview.setStyle(sty);
				lblPointPosDesc.setText(sty.getDescription());
			}
		} else if (c == this.getChkFitInsidePolygon()) {
			
		} else if (c == getPnlDuplicateLabels()) {
			
		}
		
		applyToPreview();
		
		CompRefreshThread st = new CompRefreshThread(getPnlContent(), 500);
        st.start();

	}

	private void applyToPreview() {
		if (preview != null) {
			try {
				applyConstraints();
			} catch (GeometryException e) {
				logger.error("While applying constraints.", e);
			}
			preview.setConstraints(constraints);
		}
	}

	private class JLabelHTML extends JLabel {
		private static final long serialVersionUID = -5031405572546951108L;

		public JLabelHTML(String text) {
			super(text);
			setPreferredSize(new Dimension(250, 60));
		}

		@Override
		public void setText(String text) {
			// silly fix to avoid too large text lines
			super.setText("<html>"+text+"</html>");
		}
	}

	public static IPlacementProperties createPlacementProperties(
			IPlacementConstraints placementConstraints, int shapeType) {
		return createPlacementProperties(placementConstraints, shapeType, null);
	}

	protected static IPlacementProperties createPlacementProperties(
			IPlacementConstraints placementConstraints, int geotype,
			DuplicateLayersMode duplicatesMode) {
		
		IPlacementProperties pp = null;
		if (geotype == TYPES.GEOMETRY || geotype == TYPES.AGGREGATE) {
			try {
				pp = new MultiShapePlacementProperties(
						(MultiShapePlacementConstraints) placementConstraints);
			} catch (Exception e) {
				logger.error("While creating MultiShapePlacementProperties", e);
			}
		} else {
			pp = new PlacementProperties(placementConstraints, geotype, null);
		}
		((JPanel) pp).add(new AcceptCancelPanel(pp, pp), BorderLayout.SOUTH);
		return pp;
	}
	
    class CompRefreshThread extends Thread {
        
        private int waitt = 0;
        private Component compo = null;
        
        public CompRefreshThread(Component co, int t) {
            compo = co;
            waitt = t;
        }
        public void run() {
            
            try {
                /*
                 * We might need to wait a bit because the GUI thread needs
                 * some time to update (this is because we have used a modal dialog
                 * and we refresh after that dialog is closed)
                 */
                Thread.sleep(waitt);
            } catch (Exception exc) {
                
            }
            
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {
                        compo.repaint(); // .invalidate();
                    } catch (Exception exc) {
                        // logger.info("Error while refreshing components.", exc);
                    }
                }
            });
        }
    }
    
	
}  //  @jve:decl-index=0:visual-constraint="10,10"

class MiniMapContext extends JComponent {
	
	private static final Logger minilogger =
			LoggerFactory.getLogger(MiniMapContext.class);
	
	private static final long serialVersionUID = 229128782038834443L;
	private MapContext theMapContext;
	private FLyrVect line_layer;
	private FLyrVect bgpoly_layer;
	private int hMargin = 5, vMargin = 5;
	private FLyrVect poly_layer;
	
	private int geotype;
	private IPlacementConstraints placement;


	public MiniMapContext(int type) {
		setType(type);
	}

	public void setType(int type) {
		
		GeometryType gtype = null;
		try {
			gtype = GeometryLocator.getGeometryManager().getGeometryType(
					type, SUBTYPES.GEOM2D);
		} catch (Exception e) {
			minilogger.error("While getting geo type", e);
		}
		
		if (gtype.isTypeOf(TYPES.CURVE)
				|| gtype.getType() == TYPES.MULTICURVE) {
			this.geotype = TYPES.CURVE;
		} else {
			if (gtype.isTypeOf(TYPES.SURFACE)
					|| gtype.getType() == TYPES.MULTISURFACE) {
				this.geotype = TYPES.SURFACE;
			} else {
				// Should not get here
				this.geotype = TYPES.GEOMETRY;
			}
		}
		
	}

	public void setConstraints(IPlacementConstraints constraints) {
		placement = constraints;
		try {
			getMapContext(null);
		} catch (Exception e) {
			minilogger.error("While getting mini map context", e);
		}
		repaint();
	}

	private MapContext getMapContext(Dimension img_size) throws Exception {
		
		Envelope ini_env = null;
		ini_env = GeometryLocator.getGeometryManager().createEnvelope(
				289600, 3973500, 289600 + 2000, 3973700 + 2000,
				SUBTYPES.GEOM2D);

		
		if (theMapContext == null) {
			
			SymbologyManager symman = SymbologyLocator.getSymbologyManager();
			MapContextManager mcoman = MapContextLocator.getMapContextManager();
			
			line_layer = this.createLayerFromShpDocFile("line.shp", "EPSG:23030");
			ISimpleLineSymbol line_sym = symman.createSimpleLineSymbol();
			line_sym.setColor(Color.red);
			
			ISimpleLineStyle line_sty = symman.createSimpleLineStyle();
			IArrowDecoratorStyle arrow_style = symman.createArrowDecoratorStyle();
			
			arrow_style.getMarker().setSize(15);
			arrow_style.setArrowMarkerCount(1);
			arrow_style.getMarker().setColor(Color.red);
			line_sty.setArrowDecorator(arrow_style);

			line_sym.setLineStyle(line_sty);
			line_sym.setLineWidth(2);
			
			ISingleSymbolLegend line_leg = (ISingleSymbolLegend) 
			mcoman.createLegend(ISingleSymbolLegend.LEGEND_NAME);
			
			line_leg.setDefaultSymbol(line_sym);
			line_layer.setLegend(line_leg);
			// =====================================================
			
			bgpoly_layer = this.createLayerFromShpDocFile("bg-polygon.shp", "EPSG:23030");
			poly_layer = this.createLayerFromShpDocFile("polygon.shp", "EPSG:23030");

			ISimpleFillSymbol sym2 = symman.createSimpleFillSymbol();
			sym2.setFillColor(new Color(50, 245, 125));
			ISimpleLineSymbol outline = symman.createSimpleLineSymbol();
			outline.setLineColor(Color.DARK_GRAY);
			outline.setLineWidth(0.5);
			sym2.setOutline(outline);

			ISingleSymbolLegend poly_leg = (ISingleSymbolLegend) 
			mcoman.createLegend(ISingleSymbolLegend.LEGEND_NAME);
			poly_leg.setDefaultSymbol(sym2);
			poly_layer.setLegend(poly_leg);
			bgpoly_layer.setLegend(poly_leg);

			GeneralLabelingStrategy labeling1 = new GeneralLabelingStrategy();
			GeneralLabelingStrategy labeling2 = new GeneralLabelingStrategy();
			ILabelingMethod def_method = symman.createDefaultLabelingMethod();

			ILabelClass lc = null;
			if (def_method.getLabelClasses() != null && def_method.getLabelClasses().length > 0) {
				lc = def_method.getLabelClasses()[0];
			} else {
				lc = symman.createDefaultLabel();
				def_method.addLabelClass(lc);
			}

			String[] sampleExpression = { Messages.getText("text") };
			lc.setLabelExpressions(sampleExpression);

			lc.getTextSymbol().setFontSize(16);

			labeling1.setLabelingMethod(def_method);
			labeling2.setLabelingMethod(def_method);
			labeling1.setLayer(line_layer);
			labeling2.setLayer(poly_layer);

			line_layer.setLabelingStrategy(labeling1);
			line_layer.setIsLabeled(true);
			poly_layer.setLabelingStrategy(labeling2);
			poly_layer.setIsLabeled(true);
			
			ViewPort theViewPort = new ViewPort(CRSFactory.getCRS("EPSG:23030"));
			theMapContext = new MapContext(theViewPort);
			
			if (img_size != null) {
				theViewPort.setImageSize(img_size);
			} else {
				theViewPort.setImageSize(new Dimension(200, 200));
			}
			theViewPort.setEnvelope(ini_env);
			
			theMapContext.getLayers().addLayer(bgpoly_layer);
			theMapContext.getLayers().addLayer(poly_layer);
			theMapContext.getLayers().addLayer(line_layer);
		} else {
			
			if (img_size != null) {
				theMapContext.getViewPort().setImageSize(img_size);
			} else {
				theMapContext.getViewPort().setImageSize(new Dimension(200, 200));
			}
			theMapContext.getViewPort().setEnvelope(ini_env);
		}

		line_layer.getLabelingStrategy().setPlacementConstraints(placement);
		poly_layer.getLabelingStrategy().setPlacementConstraints(placement);
		Dimension sz = getBounds().getSize();
		sz.setSize(sz.width-2*hMargin, sz.height-2*vMargin);
		theMapContext.getViewPort().setImageSize(sz);
		theMapContext.getViewPort().setBackColor(new Color(255, 0, 0));
		theMapContext.invalidate();
		return theMapContext;
	}

	@Override
	protected void paintComponent(Graphics g) {
		try {
			Rectangle bounds = getBounds();
			Dimension sz = bounds.getSize();
			sz.setSize(sz.width-2*vMargin, sz.height-2*hMargin);
			Dimension imageSize = sz;
			
			MapContext mco = getMapContext(imageSize);
			if (this.geotype == TYPES.CURVE) {
				line_layer.setVisible(true);
				poly_layer.setVisible(false);
			} else {
				if (this.geotype == TYPES.SURFACE) {
					line_layer.setVisible(false);
					poly_layer.setVisible(true);
				}
			}
			bgpoly_layer.setVisible(line_layer.isVisible());

			BufferedImage bi = new BufferedImage(imageSize.width, imageSize.height, BufferedImage.TYPE_4BYTE_ABGR);
			mco.invalidate();
			mco.draw(bi, bi.createGraphics(), mco.getScaleView());
			g.setColor(new Color(150,180,255));
			g.fillRect(vMargin, hMargin, bounds.width-2*hMargin, bounds.height-2*vMargin);
			g.drawImage(bi, vMargin, vMargin, null);
			bi = null;
		} catch (Exception e) {
			
			minilogger.error("While painting.", e);
			String noneSelected = "["+Messages.getText("preview_not_available")+"]";
			int vGap = 5, hGap = 5;
			Rectangle r = getBounds();
			FontMetrics fm = g.getFontMetrics();
			int lineWidth = fm.stringWidth(noneSelected);
			float scale = (float) r.getWidth() / lineWidth;
			Font f = g.getFont();
			float fontSize = f.getSize()*scale;
			g.setFont(	f.deriveFont( fontSize ) );
			((Graphics2D) g).drawString(noneSelected, (r.x*scale) - (hGap/2), r.height/2+vGap*scale);
		}

	}
	
	private FLyrVect createLayerFromShpDocFile(
			String shp_file_name, String epsg_code) throws Exception {
		
		File shpfile = getDocFolderFile(shp_file_name);
		IProjection proj = CRSFactory.getCRS(epsg_code);
		DataStoreParameters ssp = DALLocator.getDataManager().createStoreParameters("Shape");
		ssp.setDynValue("shpfile", shpfile);
		ssp.setDynValue("CRS", proj);
		FLyrVect resp = null;
		resp = (FLyrVect) MapContextLocator.getMapContextManager().createLayer(
				shp_file_name, ssp);
		return resp;
	}
	
	private File getDocFolderFile(String name) {
		
		URL resource = this.getClass().getClassLoader().getResource(
				"docs" + File.separator + name);
		File resp = new File(resource.getFile());
		return resp;
	}
    
}
