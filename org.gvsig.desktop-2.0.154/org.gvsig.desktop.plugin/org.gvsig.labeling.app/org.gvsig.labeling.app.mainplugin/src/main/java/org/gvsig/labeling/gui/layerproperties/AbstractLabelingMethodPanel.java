/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.labeling.gui.layerproperties;

import java.beans.PropertyChangeListener;

import javax.swing.JPanel;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;


/**
 * @author gvSIG Team
 *
 */
public abstract class AbstractLabelingMethodPanel extends JPanel implements PropertyChangeListener {

	/**
     *
     */
    private static final long serialVersionUID = 3042343486793818865L;
    /**
     *
     */
    public static final String PLACEMENT_CONSTRAINTS = "PLACEMENT_CONSTRAINTS";
	/**
	 *
	 */
	public static final String ALLOW_OVERLAP = "ALLOW_OVERLAP";
	/**
	 *
	 */
	public static final String ZOOM_CONSTRAINTS= "ZOOM_CONSTRAINTS";

	protected FLyrVect layer;
	protected ILabelingMethod method;

	public abstract String getName();

	/**
	 * @return the labeling method class.
	 */
	public abstract Class<? extends ILabelingMethod> getLabelingMethodClass();

	/**
	 * @param method
	 * @param srcLayer
	 * @throws ReadException
	 */
	public void setModel(ILabelingMethod method, FLyrVect srcLayer)
			throws ReadException {

		if (srcLayer == null) {
			throw new ReadException("FLyrVect is null",
					new Exception("FLyrVect is null"));
		}
		this.layer = srcLayer;

		try {
			Class<? extends ILabelingMethod> labelingMethodClass = getLabelingMethodClass();
            if (method!= null && method.getClass().equals(labelingMethodClass)) {
				this.method = method;
			} else {
				this.method = labelingMethodClass.newInstance();
			}

		} catch (Exception e) {

			throw new ReadException(
					srcLayer.getFeatureStore().getName(),
					new Exception("Unable to load labeling method. Is it in your classpath?", e));
		}
		initializePanel();

		try {
			fillPanel(srcLayer.getFeatureStore().getDefaultFeatureType());
		} catch (DataException de) {
			throw new ReadException(srcLayer.getFeatureStore().getName(), de);
		}

	}

	protected abstract void initializePanel() ;
	/**
	 * @param ftype
	 * @throws ReadException
	 */
	public abstract void fillPanel(
			FeatureType ftype) throws ReadException;

	public String toString() {
		return getName();
	}

	/**
	 * @return the labeling method
	 */
	public ILabelingMethod getMethod() {
		return method;
	}

	public boolean equals(Object obj) {
		if (obj == null) return false;
		return getClass().equals(obj.getClass());
	}
}
