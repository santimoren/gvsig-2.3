package org.gvsig.labeling;

import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.gui.styling.StyleEditor;
import org.gvsig.app.project.documents.view.legend.gui.LabelingManager;
import org.gvsig.fmap.IconThemeHelper;
import org.gvsig.labeling.gui.layerproperties.DefaultLabeling;
import org.gvsig.labeling.gui.layerproperties.FeatureDependent;
import org.gvsig.labeling.gui.layerproperties.GeneralLabeling;
import org.gvsig.labeling.gui.layerproperties.LabelClassPropertiesFactory;
import org.gvsig.labeling.gui.layerproperties.OnSelection;
import org.gvsig.labeling.gui.styling.editortools.LabelStyleNewTextFieldTool;
import org.gvsig.labeling.gui.styling.editortools.LabelStyleOpenBackgroundFile;
import org.gvsig.labeling.gui.styling.editortools.LabelStylePanTool;
import org.gvsig.labeling.gui.styling.editortools.LabelStyleRemoveLastTextField;
import org.gvsig.labeling.gui.styling.editortools.PointLabelForbiddenPrecedenceTool;
import org.gvsig.labeling.gui.styling.editortools.PointLabelHighPrecedenceTool;
import org.gvsig.labeling.gui.styling.editortools.PointLabelLowPrecedenceTool;
import org.gvsig.labeling.gui.styling.editortools.PointLabelNormalPrecedenceTool;
import org.gvsig.labeling.label.FeatureDependentLabeled;
import org.gvsig.labeling.label.GeneralLabelingStrategy;
import org.gvsig.labeling.label.OnSelectionLabeled;
import org.gvsig.labeling.label.SmartTextSymbolLabelClass;
import org.gvsig.labeling.placements.AbstractPlacementConstraints;
import org.gvsig.labeling.placements.LinePlacementAtBest;
import org.gvsig.labeling.placements.LinePlacementAtExtremities;
import org.gvsig.labeling.placements.LinePlacementConstraints;
import org.gvsig.labeling.placements.LinePlacementInTheMiddle;
import org.gvsig.labeling.placements.MarkerPlacementAroundPoint;
import org.gvsig.labeling.placements.MarkerPlacementOnPoint;
import org.gvsig.labeling.placements.MultiShapePlacementConstraints;
import org.gvsig.labeling.placements.PlacementManager;
import org.gvsig.labeling.placements.PointLabelPositioner;
import org.gvsig.labeling.placements.PointPlacementConstraints;
import org.gvsig.labeling.placements.PolygonPlacementConstraints;
import org.gvsig.labeling.placements.PolygonPlacementInside;
import org.gvsig.labeling.placements.PolygonPlacementOnCentroid;
import org.gvsig.labeling.placements.PolygonPlacementParallel;
import org.gvsig.labeling.symbol.CharacterMarkerSymbol;
import org.gvsig.labeling.symbol.SmartTextSymbol;
import org.gvsig.symbology.fmap.mapcontext.rendering.legend.styling.LabelingFactory;
import org.gvsig.symbology.swing.SymbologySwingLocator;
import org.gvsig.symbology.swing.SymbologySwingManager;


public class ExtendedLabelingExtension extends Extension {

	public void execute(String actionCommand) {

	}

	public void initialize() {
		
		registerPersistence();
		registerIcons();

		// Editor tools
		StyleEditor.addEditorTool(LabelStylePanTool.class);
		StyleEditor.addEditorTool(LabelStyleNewTextFieldTool.class);
		StyleEditor.addEditorTool(LabelStyleRemoveLastTextField.class);
		StyleEditor.addEditorTool(LabelStyleOpenBackgroundFile.class);
		StyleEditor.addEditorTool(PointLabelHighPrecedenceTool.class);
		StyleEditor.addEditorTool(PointLabelNormalPrecedenceTool.class);
		StyleEditor.addEditorTool(PointLabelLowPrecedenceTool.class);
		StyleEditor.addEditorTool(PointLabelForbiddenPrecedenceTool.class);

		LabelingManager.addLabelingStrategy(GeneralLabeling.class);

		// labeling methods in the labeling page
		//						(inverse order to the wanted to be shown)
		GeneralLabeling.addLabelingMethod(OnSelection.class);
		GeneralLabeling.addLabelingMethod(FeatureDependent.class);
		GeneralLabeling.addLabelingMethod(DefaultLabeling.class);

		PlacementManager.addLabelPlacement(LinePlacementAtExtremities.class);
		PlacementManager.addLabelPlacement(LinePlacementAtBest.class);
		PlacementManager.addLabelPlacement(LinePlacementInTheMiddle.class);
		PlacementManager.addLabelPlacement(MarkerPlacementOnPoint.class);
		// PlacementManager.addLabelPlacement(MarkerCenteredAtPoint.class);
		PlacementManager.addLabelPlacement(MarkerPlacementAroundPoint.class);
		PlacementManager.addLabelPlacement(PolygonPlacementOnCentroid.class);
		PlacementManager.addLabelPlacement(PolygonPlacementInside.class);
		PlacementManager.addLabelPlacement(PolygonPlacementParallel.class);

            // LabelingFactory.setDefaultLabelingStrategy(GeneralLabelingStrategy.class);
            SymbologySwingManager manager = SymbologySwingLocator.getSwingManager();
            manager.registerLabelClassEditor(new LabelClassPropertiesFactory());
	}

	private void registerIcons() {
		
		IconThemeHelper.registerIcon("action", "edit-add-text", this);
		IconThemeHelper.registerIcon("action", "edit-remove-text", this);
		IconThemeHelper.registerIcon("action", "set-forbidden-precedence", this);
		IconThemeHelper.registerIcon("action", "set-high-precedence", this);
		IconThemeHelper.registerIcon("action", "set-low-precedence", this);
		IconThemeHelper.registerIcon("action", "set-normal-precedence", this);
		
		IconThemeHelper.registerIcon("action", "edit-shift-component", this);
		IconThemeHelper.registerIcon("action", "edit-add-background-image", this);
	}

	private void registerPersistence() {
		
		AbstractPlacementConstraints.registerPersistent();
		PointPlacementConstraints.registerPersistent();
		LinePlacementConstraints.registerPersistent();
		PolygonPlacementConstraints.registerPersistent();
		MultiShapePlacementConstraints.registerPersistent();
		
		CharacterMarkerSymbol.registerPersistent();
		SmartTextSymbol.registerPersistent();
		SmartTextSymbolLabelClass.registerPersistent();

		PointLabelPositioner.registerPersistent();
		
		GeneralLabelingStrategy.registerPersistent();
		
		OnSelectionLabeled.registerPersistent();
		FeatureDependentLabeled.registerPersistent();
	}

	public boolean isEnabled() {
		return false;
	}

	public boolean isVisible() {
		return false;
	}



}


