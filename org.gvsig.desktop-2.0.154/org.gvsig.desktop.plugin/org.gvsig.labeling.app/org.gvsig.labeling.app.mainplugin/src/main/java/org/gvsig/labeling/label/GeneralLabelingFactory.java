/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.labeling.label;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingMethod;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.IZoomConstraints;

/**
 *
 * ExtendedLabelingFactory.java
 *
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es Mar 6, 2008
 *
 */
public class GeneralLabelingFactory {

	public static GeneralLabelingStrategy createStrategy(
			FLayer layer,
			ILabelingMethod method,
			IPlacementConstraints placement_c,
			IZoomConstraints zoom_c) {

		GeneralLabelingStrategy resp = new GeneralLabelingStrategy();
		resp.setLayer(layer);
		if (method != null) {
			resp.setLabelingMethod(method);
		}
		if (placement_c != null) {
			resp.setPlacementConstraints(placement_c);
		}
		if (zoom_c != null) {
			resp.setZoomConstraints(zoom_c);
		}
		return resp;
	}
}
