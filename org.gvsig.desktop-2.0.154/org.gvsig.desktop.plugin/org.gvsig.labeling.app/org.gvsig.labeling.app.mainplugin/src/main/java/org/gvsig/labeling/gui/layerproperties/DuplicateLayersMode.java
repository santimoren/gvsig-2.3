/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.labeling.gui.layerproperties;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.gvsig.fmap.mapcontext.rendering.legend.styling.IPlacementConstraints;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.i18n.Messages;

public class DuplicateLayersMode extends GridBagLayoutPanel
implements ActionListener {
	
	private static final long serialVersionUID = 1091963463412809652L;
	private JRadioButton rdBtnRemoveDuplicates;
	private JRadioButton rdBtnOnePerFeature;
	private JRadioButton rdBtnOnePerFeaturePart;
	
	private ActionListener actListeneter = null;
	
	public DuplicateLayersMode(ActionListener act_lis) {
		super();
		actListeneter = act_lis;
		initialize();
	}
	private void initialize() {
		setBorder(BorderFactory.
				createTitledBorder(null,
						Messages.getText("duplicate_labels")));
		addComponent(getRdBtnRemoveDuplicates());
		addComponent(getRdBtnOnePerFeature());
		addComponent(getRdBtnOnePerFeaturePart());

		ButtonGroup group = new ButtonGroup();
		group.add(getRdBtnOnePerFeature());
		group.add(getRdBtnOnePerFeaturePart());
		group.add(getRdBtnRemoveDuplicates());
	}
	

	private JRadioButton getRdBtnOnePerFeaturePart() {
		if (rdBtnOnePerFeaturePart == null) {
			rdBtnOnePerFeaturePart = new JRadioButton(
					Messages.getText("place_one_label_per_feature_part"));
			rdBtnOnePerFeaturePart.addActionListener(this);
			
		}
		return rdBtnOnePerFeaturePart;
	}

	private JRadioButton getRdBtnOnePerFeature() {
		if (rdBtnOnePerFeature == null) {
			rdBtnOnePerFeature = new JRadioButton(
					Messages.getText("place_one_label_per_feature"));
			rdBtnOnePerFeature.setSelected(true);
			rdBtnOnePerFeature.addActionListener(this);
		}
		return rdBtnOnePerFeature;
	}

	private JRadioButton getRdBtnRemoveDuplicates() {
		if (rdBtnRemoveDuplicates == null) {
			rdBtnRemoveDuplicates = new JRadioButton(
					Messages.getText("remove_duplicate_labels"));
			rdBtnRemoveDuplicates.addActionListener(this);

		}
		return rdBtnRemoveDuplicates;
	}
	
	public void setMode(int dupMode) {
		
		rdBtnRemoveDuplicates.setSelected(dupMode == IPlacementConstraints.REMOVE_DUPLICATE_LABELS);
		rdBtnOnePerFeature.setSelected(dupMode == IPlacementConstraints.ONE_LABEL_PER_FEATURE);
		rdBtnOnePerFeaturePart.setSelected(dupMode == IPlacementConstraints.ONE_LABEL_PER_FEATURE_PART);
	}
	
	public int getMode() {
		if (rdBtnRemoveDuplicates.isSelected()) {
			return IPlacementConstraints.REMOVE_DUPLICATE_LABELS;
		}
		if (rdBtnOnePerFeature.isSelected()) {
			return IPlacementConstraints.ONE_LABEL_PER_FEATURE;
		}
		if (rdBtnOnePerFeaturePart.isSelected()) {
			return IPlacementConstraints.ONE_LABEL_PER_FEATURE_PART;
		}
		
		throw new Error("Unsupported layer duplicates mode");
	}
	public void actionPerformed(ActionEvent e) {
		
		if (actListeneter != null) {
			ActionEvent aev = new ActionEvent(this, 0, "");
			actListeneter.actionPerformed(aev);
		}
		
	}
}
