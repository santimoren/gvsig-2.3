/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.datalocator;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.view.gui.IView;
import org.gvsig.datalocator.gui.DataSelectionPanel;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;


/**
 * The DataLocatorExtension class allows to make a quick zoom based on an
 * alphanumeric attribute.
 *
 * @author jldominguez
 */
public class DataLocatorExtension extends Extension {

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#inicializar()
     */
    public void initialize() {
    	registerIcons();

    }

    private void registerIcons(){
    	IconThemeHelper.registerIcon("action", "view-navigation-locator-by-attribute", this);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
    	if( "view-navigation-locator-by-attribute".equalsIgnoreCase(actionCommand))  {
    	    
    	    IWindow wi = ApplicationLocator.getManager().getActiveWindow(); 
    	    if (wi instanceof IView) {
    	        MapContext mapContext = ((IView) wi).getMapControl().getMapContext();
                DataSelectionPanel selpan = new DataSelectionPanel(mapContext);
                ApplicationLocator.getManager().getUIManager().addWindow(selpan);
    	    }
    	}
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isEnabled()
     */
    public boolean isEnabled() {
        
		IWindow wi = PluginServices.getMDIManager().getActiveWindow();
		
		if (!(wi instanceof IView)) {
		    return false;
		}
		
		MapContext mapContext = ((IView) wi).getMapControl().getMapContext();
		List<FLyrVect> vects = getVectorLayers(mapContext.getLayers());
		/*
		 * View has at least one vector layer
		 */
		return vects.size() > 0;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isVisible()
     */
    public boolean isVisible() {
        IWindow wi = PluginServices.getMDIManager().getActiveWindow();
        return (wi instanceof IView);
    }
    
    
    /**
     * Recursively search for vector layers
     * 
     * @param layers
     * @return
     */
    public static List<FLyrVect> getVectorLayers(FLayers layers) {
        
        List<FLyrVect> resp = new ArrayList<FLyrVect>();
        // ================================================
        int len = layers.getLayersCount();
        for (int i=0; i<len; i++) {
            if (layers.getLayer(i) instanceof FLyrVect) {
                resp.add((FLyrVect) layers.getLayer(i));
            } else {
                // ===========================
                if (layers.getLayer(i) instanceof FLayers) {
                    List<FLyrVect> added = getVectorLayers((FLayers) layers.getLayer(i));
                    resp.addAll(added);
                }
                // ===========================
            }
        }
        // ================================================
        return resp;
        
    }

}
