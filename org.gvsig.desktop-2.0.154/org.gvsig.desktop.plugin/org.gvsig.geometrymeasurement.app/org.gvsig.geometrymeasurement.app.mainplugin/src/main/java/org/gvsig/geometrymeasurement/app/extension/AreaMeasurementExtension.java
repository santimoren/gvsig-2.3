/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geometrymeasurement.app.extension;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.geometrymeasurement.app.extension.utils.Operations;

/**
 * Andami extension to show AreaMeasurementExtension in the application.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class AreaMeasurementExtension extends
    AbstractGeometryMeasurementExtension {

    public void initialize() {
    super.initialize();
        IconThemeHelper.registerIcon("action", "table-add-area", this);
    }

    @Override
    protected void execute(String actionCommand, FeatureStore featureStore)
        throws Exception {
        new Operations().addDoubleFieldFromOperation(featureStore, "AREA",
            "area");
    }

    @Override
    protected boolean isVisibleForTable(TableDocument tableDocument) {
        return isStoreOfAnyType(tableDocument, new int[] { Geometry.TYPES.SURFACE,
            Geometry.TYPES.MULTISURFACE });
    }
}
