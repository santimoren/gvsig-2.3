/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geometrymeasurement.app.extension;

import java.awt.Component;
import java.util.Locale;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.messages.NotificationManager;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.app.ApplicationLocator;
import org.gvsig.app.project.documents.table.TableDocument;
import org.gvsig.app.project.documents.table.gui.FeatureTableDocumentPanel;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.i18n.Messages;

/**
 * Abstract class for Geometry measurement operations extensions.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public abstract class AbstractGeometryMeasurementExtension extends Extension {

    private static final Logger LOG = LoggerFactory
        .getLogger(AbstractGeometryMeasurementExtension.class);
    
    //Just to know if the resources has been initialized
    private static boolean isInitialized = false;
       
    public void initialize() {
        if (!isInitialized){
            isInitialized = true;
        }        
    }

    public boolean isEnabled() {
        TableDocument tableDocument;
        String name = "unknow";
        try {
            tableDocument = getActiveTableDocument();
            if (tableDocument != null) {
                name = tableDocument.getName();
            	return (tableDocument != null && tableDocument.getStore().allowWrite());
            }
        } catch (Throwable e) {
            LOG.info("Error accesing to the table document ("+name+")", e);
        }
        return false;
    }

    public final boolean isVisible() {
        TableDocument tableDocument;
        String name = "unknow";
        try {
            tableDocument = getActiveTableDocument();
            if (tableDocument != null) {
                name = tableDocument.getName();
                return isVisibleForTable(tableDocument);
            }
        } catch (Throwable e) {
            LOG.info("Error accesing to the table document ("+name+")", e);
        }       
        return false;
    }

    /**
     * Returns if the current extension is visible for the provided 
     * table.
     * 
     * The provided table will be always non null.
     * 
     * @param layer
     *            the active table
     * @return if the extension is visible
     */
    protected abstract boolean isVisibleForTable(TableDocument tableDocument);

    /**
     * Returns the active table document. If the active document is not a table,
     * or if the table has not a geometry, return null.
     * 
     * @return the active table document
     * @throws DataException 
     */
    protected TableDocument getActiveTableDocument() throws DataException {
        IWindow window = PluginServices.getMDIManager().getActiveWindow();

        if (window instanceof FeatureTableDocumentPanel) {
            FeatureTableDocumentPanel table = (FeatureTableDocumentPanel) window;
            TableDocument tableDocument = (TableDocument)table.getDocument();
            if (tableDocument.getStore().getDefaultFeatureType().getDefaultGeometryAttribute() != null){
                return tableDocument;
            }
           
        }
        return null;
    }

    protected boolean isStoreOfAnyType(TableDocument tableDocument, int[] geomTypes) {
        try {
            FeatureType featureType = tableDocument.getStore().getDefaultFeatureType();
            GeometryType geometryType = featureType.getDefaultGeometryAttribute().getGeomType();
            for (int i = 0; i < geomTypes.length; i++) {
                if (geometryType.isTypeOf(geomTypes[i])) {
                    return true;
                }
            }
        } catch (ReadException e) {
            LOG.error("Error reading the geometry type", e);
        } catch (DataException e) {
            LOG.error("Error reading the geometry type", e);
        }
        return false;
    }

    public void execute(String actionCommand) {
        try {
            TableDocument tableDocument = getActiveTableDocument();
            if (tableDocument == null) {
                return;
            }
            Component parentWindow = 
                (Component)ApplicationLocator.getManager().getUIManager().getActiveWindow();
            int addField = JOptionPane.showConfirmDialog(parentWindow, Messages.getText("new_field_query"),
                Messages.getText("info"), JOptionPane.YES_NO_OPTION);
            
            if (addField == JOptionPane.YES_OPTION){
                FeatureStore featureStore = tableDocument.getStore();
                execute(actionCommand, featureStore);
            }
        } catch (Exception e) {
            LOG.error("Not possible to add the new field", e);
            NotificationManager.addError(e);
        }
    }

    /**
     * Executes a command for the given {@link FeatureStore}.
     * 
     * @param actionCommand
     *            to execute
     * @param featureStore
     *            to use
     * @throws Exception
     *             if there is an error while executing
     */
    protected abstract void execute(String actionCommand,
        FeatureStore featureStore) throws Exception;

}
