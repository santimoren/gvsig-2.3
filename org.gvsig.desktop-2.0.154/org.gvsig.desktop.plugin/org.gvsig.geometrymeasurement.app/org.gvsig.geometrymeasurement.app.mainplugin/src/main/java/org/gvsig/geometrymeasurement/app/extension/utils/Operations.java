/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.geometrymeasurement.app.extension.utils;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.app.project.documents.table.TableOperations;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Andami extension to show PerimeterMeasurementExtension in the application.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Operations {

    private static final Logger LOG = LoggerFactory.getLogger(Operations.class);

    private static final int NUM_DECIMALS = 5;

    public void addDoubleFieldFromOperation(FeatureStore featureStore,
        String fieldName, String operationName) throws DataException,
        GeometryOperationNotSupportedException, GeometryOperationException {
        boolean isEditing = false;
        if (featureStore.isEditing()) {
            isEditing = true;
        } else {
            featureStore.edit();
        }

        featureStore.beginEditingGroup(operationName);

        // Adding the new attribute type
        FeatureType featureType = featureStore.getDefaultFeatureType();
        EditableFeatureType editableFeatureType = featureType.getEditable();

        String realFieldName = getFieldName(featureStore, fieldName, 0);
        EditableFeatureAttributeDescriptor editableFeatureAttributeDescriptor =
            editableFeatureType.add(realFieldName, DataTypes.DOUBLE);
        editableFeatureAttributeDescriptor.setPrecision(NUM_DECIMALS);
        editableFeatureAttributeDescriptor
            .setSize(TableOperations.MAX_FIELD_LENGTH);

        featureStore.update(editableFeatureType);

        DisposableIterator disposableIterator = null;
        FeatureSet featureSet = null;
        try {
            // Adding the attribute value
            featureSet = featureStore.getFeatureSet();
            disposableIterator = featureSet.fastIterator();
            while (disposableIterator.hasNext()) {
                Feature feature = (Feature) disposableIterator.next();
                EditableFeature editableFeature = feature.getEditable();
                editableFeature.setDouble(realFieldName, (Double) feature
                    .getDefaultGeometry().invokeOperation(operationName, null));
                featureSet.update(editableFeature);
            }
        } finally {
            if (disposableIterator != null) {
                disposableIterator.dispose();
            }
            if (featureSet != null) {
                featureSet.dispose();
            }
            try {
                featureStore.endEditingGroup();
            } catch (NeedEditingModeException e) {
                LOG.error("Exception endEditingGroup", e);
            }
        }

        if (!isEditing) {
            featureStore.finishEditing();
        }
    }

    public String getFieldName(FeatureStore featureStore, String fieldName,
        int index) throws DataException {
        String fullName = fieldName;
        if (index != 0) {
            fullName = fullName + index;
        }
        @SuppressWarnings("unchecked")
        Iterator<FeatureAttributeDescriptor> it =
            featureStore.getDefaultFeatureType().iterator();
        while (it.hasNext()) {
            FeatureAttributeDescriptor featureAttributeDescriptor = it.next();
            if (featureAttributeDescriptor.getName().equals(fullName)) {
                return getFieldName(featureStore, fieldName, index + 1);
            }
        }
        return fullName;
    }

    /**
     * @param featureStore
     * @param string
     * @param string2
     * @throws DataException
     */
    public void addXYPoints(FeatureStore featureStore, String xFieldName,
        String yFieldName) throws DataException {
        boolean isEditing = false;
        if (featureStore.isEditing()) {
            isEditing = true;
        } else {
            featureStore.edit();
        }

        // Adding the new attribute type
        FeatureType featureType = featureStore.getDefaultFeatureType();
        EditableFeatureType editableFeatureType = featureType.getEditable();

        String realXFieldName = getFieldName(featureStore, xFieldName, 0);
        EditableFeatureAttributeDescriptor editableFeatureAttributeDescriptor =
            editableFeatureType.add(realXFieldName, DataTypes.DOUBLE);
        editableFeatureAttributeDescriptor.setPrecision(NUM_DECIMALS);
        editableFeatureAttributeDescriptor
            .setSize(TableOperations.MAX_FIELD_LENGTH);

        String realYFieldName = getFieldName(featureStore, yFieldName, 0);
        editableFeatureAttributeDescriptor =
            editableFeatureType.add(realYFieldName, DataTypes.DOUBLE);
        editableFeatureAttributeDescriptor.setPrecision(NUM_DECIMALS);
        editableFeatureAttributeDescriptor
            .setSize(TableOperations.MAX_FIELD_LENGTH);

        featureStore.update(editableFeatureType);

        // Adding the attribute value
        FeatureSet featureSet = featureStore.getFeatureSet();
        DisposableIterator disposableIterator = featureSet.fastIterator();
        while (disposableIterator.hasNext()) {
            Feature feature = (Feature) disposableIterator.next();
            EditableFeature editableFeature = feature.getEditable();
            editableFeature.setDouble(realXFieldName,
                ((Point) feature.getDefaultGeometry()).getX());
            editableFeature.setDouble(realYFieldName,
                ((Point) feature.getDefaultGeometry()).getY());
            featureSet.update(editableFeature);
        }

        featureSet.dispose();

        if (!isEditing) {
            featureStore.finishEditing();
        }

    }

}
