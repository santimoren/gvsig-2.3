/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.centerviewpoint;

import java.awt.Color;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.PluginServices;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.app.project.documents.view.ViewDocument;
import org.gvsig.app.project.documents.view.gui.DefaultViewPanel;
import org.gvsig.centerviewpoint.gui.InputCoordinatesPanel;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.layers.FLayers;


/**
 * The CenterViewToPointExtension class allows to center the View over a
 * concrete point given by its coordinates.
 *
 * @author jmorell
 */
public class CenterViewToPointExtension extends Extension {
	private DefaultViewPanel vista;
	public static Color COLOR=Color.red;
	
	public void initialize() {
		IconThemeHelper.registerIcon("action", "view-navigation-center-view-to-point", this);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
    	if( "view-navigation-center-view-to-point".equalsIgnoreCase(actionCommand)) {
			vista = (DefaultViewPanel)PluginServices.getMDIManager().getActiveWindow();
	        MapContext mapContext = vista.getModel().getMapContext();
	        InputCoordinatesPanel dataSelectionPanel = new InputCoordinatesPanel(mapContext);
	        //dataSelectionPanel.setColor(color);
			PluginServices.getMDIManager().addWindow(dataSelectionPanel);
    	}
    }

    public DefaultViewPanel getView(){
    	return vista;
    }
    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isEnabled()
     */
    public boolean isEnabled() {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f.getClass() == DefaultViewPanel.class) {
		    DefaultViewPanel vista = (DefaultViewPanel) f;
		    ViewDocument model = vista.getModel();
		    MapContext mapa = model.getMapContext();
		    FLayers layers = mapa.getLayers();
		    for (int i=0;i < layers.getLayersCount();i++) {
               if (layers.getLayer(i).isAvailable()) return true;
		    }
		}
		return false;

    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isVisible()
     */
    public boolean isVisible() {
		org.gvsig.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f.getClass() == DefaultViewPanel.class) {
		    DefaultViewPanel vista = (DefaultViewPanel) f;
		    ViewDocument model = vista.getModel();
		    MapContext mapa = model.getMapContext();
            if (mapa.getLayers().getLayersCount() > 0) {
                return true;
            }
            return false;
        }
		return false;
	}

}
