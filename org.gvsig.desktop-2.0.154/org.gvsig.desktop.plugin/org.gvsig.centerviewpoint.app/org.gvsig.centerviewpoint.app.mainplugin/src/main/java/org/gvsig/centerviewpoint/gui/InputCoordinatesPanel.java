/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.centerviewpoint.gui;

import org.gvsig.andami.PluginServices;
import org.gvsig.andami.ui.mdiManager.IWindow;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.gui.panels.ColorChooserPanel;
import org.gvsig.app.project.documents.view.toolListeners.InfoListener;
import org.gvsig.centerviewpoint.CenterViewToPointExtension;
import org.gvsig.fmap.geom.*;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.*;
import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontrol.MapControl;
import org.gvsig.fmap.mapcontrol.tools.Events.PointEvent;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.util.prefs.Preferences;


/**
 * The InputCoordinatesPanel class creates a JPanel where the
 * user can input the coordinates of the point of reference
 * for center the View.
 *
 * @author jmorell
 */
public class InputCoordinatesPanel extends JPanel implements IWindow {
    private static final long serialVersionUID = 1L;
    private JLabel labelX = null;
    private JTextField textX = null;
    private JLabel labelY = null;
    private JLabel labelColor = null;
    private JTextField textY = null;
    private MapControl mapControl;
    private WindowInfo viewInfo = null;
    private String firstCoordinate;
    private String secondCoordinate;
    private Point2D center;
    private GraphicLayer lyr;
    private ColorChooserPanel colorPanel;
	private AcceptCancelPanel okCancelPanel = null;

	MapContextManager mapContextManager = MapContextLocator
			.getMapContextManager();

    private static final Logger LOG = LoggerFactory
        .getLogger(InputCoordinatesPanel.class);

	/**
     * This is the default constructor
     */
    public InputCoordinatesPanel(MapContext mapContext) {
        super();
        this.mapControl = new MapControl();
        mapControl.setMapContext(mapContext);
        lyr=mapControl.getMapContext().getGraphicsLayer();
        initializeCoordinates();
        initialize();
    }

    /**
     * Sets the proper text for the first and second coordinate labels,
     * depending on the kind of selected projection.
     *
     */
    private void initializeCoordinates() {
        IProjection proj = mapControl.getProjection();
        if (proj.isProjected()) {
            firstCoordinate = "X";
            secondCoordinate = "Y";
        } else {
            firstCoordinate = "Lon";
            secondCoordinate = "Lat";
        }
    }

    /**
     * Move the view's extent so that the specified point gets
     * centered.
     *
     * @throws Exception
     */
    private void zoomToCoordinates() throws Exception {
       try{
    	Envelope oldExtent = mapControl.getViewPort().getAdjustedEnvelope();
        double oldCenterX = oldExtent.getCenter(0);
        double oldCenterY = oldExtent.getCenter(1);
        double centerX = (new Double((String)textX.getText())).doubleValue();
        double centerY = (new Double((String)textY.getText())).doubleValue();
        center=new Point2D.Double(centerX,centerY);
        double movX = centerX-oldCenterX;
        double movY = centerY-oldCenterY;

        double minx = oldExtent.getMinimum(0) + movX;
        double miny = oldExtent.getMinimum(1) + movY;
        double maxX = oldExtent.getMaximum(0) + movX;
        double maxY = oldExtent.getMaximum(1) + movY;
        Envelope extent = GeometryLocator.getGeometryManager().createEnvelope(
            minx, miny,
            maxX, maxY,
            SUBTYPES.GEOM2D);
        mapControl.getViewPort().setEnvelope(extent);
       }catch (NumberFormatException e) {
    	   throw new Exception();
       }

    }

    /**
     * This method initializes this
     *
     * @return void
     */
    private void initialize() {

        I18nManager i18nManager = ToolsLocator.getI18nManager();

        this.setLayout(new BorderLayout(0,5));
        this.setBorder(new EmptyBorder(new Insets(5,5,5,5)));
        this.setSize(270, 150);

        labelX = new JLabel();
        labelX.setText(firstCoordinate + ":");

        labelY = new JLabel();
        labelY.setText(secondCoordinate + ":");

        GridBagLayout gbl = new GridBagLayout();
        JPanel centerPanel = new JPanel(gbl);
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(3,5,5,5);

        // JLabels
        c.weightx = 0;
        c.gridx = 0;

        c.gridy = 0;
        centerPanel.add(labelX, c);

        c.gridy = 1;
        centerPanel.add(labelY, c);

        c.gridwidth = 3;
        c.weightx = 1;
        c.gridx = 1;

        c.gridy = 0;
        centerPanel.add(getTextX(), c);

        c.gridy = 1;
        centerPanel.add(getTextY(), c);

        // Color chooser
        JPanel centerPanel2 = new JPanel(new BorderLayout());
        centerPanel2.add(getColorPanel(), BorderLayout.CENTER);

        // First TitledBorder
        TitledBorder tb1 = BorderFactory.createTitledBorder(new TitledBorder(i18nManager.getTranslation("coordinates")));
        centerPanel.setBorder(tb1);
        this.add(centerPanel, BorderLayout.NORTH);

        // Second TitledBorder
        TitledBorder tb2 = BorderFactory.createTitledBorder(new TitledBorder(i18nManager.getTranslation("point_color")));
        centerPanel2.setBorder(tb2);
        this.add(centerPanel2, BorderLayout.CENTER);

        // Buttons Panel
        this.add(getOkCancelPanel(), BorderLayout.SOUTH);
    }

    /**
     * This method initializes textX
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTextX() {
    	if (textX == null) {
    		textX = new JTextField();
    		textX.addActionListener(new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				textX.transferFocus();
    			}
    		});
    	}
    	return textX;
    }

    /**
     * This method initializes textY
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTextY() {
    	if (textY == null) {
    		textY = new JTextField();
    		textY.addActionListener(new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				textY.transferFocus();
    			}
    		});
    	}
    	return textY;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.ui.mdiManager.View#getViewInfo()
     */
    public WindowInfo getWindowInfo() {
        // TODO Auto-generated method stub
        if (viewInfo == null) {
            viewInfo=new WindowInfo(WindowInfo.MODALDIALOG);
            viewInfo.setTitle(PluginServices.getText(this,"Centrar_la_Vista_sobre_un_punto"));
            viewInfo.setWidth(this.getWidth());
            viewInfo.setHeight(this.getHeight());
        }
        return viewInfo;
    }
    /**
     * Opens the infoByPoint dialog for the selected point.
     *
     */
    private void openInfo(){
    	InfoListener infoListener=new InfoListener(mapControl);
    	MouseEvent e=new MouseEvent((Component)(((CenterViewToPointExtension)PluginServices.getExtension(CenterViewToPointExtension.class)).getView()),MouseEvent.BUTTON1,MouseEvent.ACTION_EVENT_MASK,MouseEvent.MOUSE_CLICKED,500,400,1,true);
    	Point2D centerPixels=mapControl.getViewPort().fromMapPoint(center.getX(),center.getY());
    	PointEvent pe=new PointEvent(centerPixels,e,mapControl);
    	try {
			infoListener.point(pe);
		} catch (org.gvsig.fmap.mapcontrol.tools.BehaviorException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (mapControl.getMapContext().getLayers().getActives().length==0){
			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"no_hay_ninguna_capa_seleccionada")+" \n"+
					PluginServices.getText(this,"debe_seleccionar_las_capas_de_las_que_quiera_obtener_informacion"));
		}
    }

    /**
     * Draws the selected point on the view.
     *
     * @param color
     */
    private void drawPoint(Color color){
    	CenterViewToPointExtension.COLOR=color;
    	lyr.clearAllGraphics();
		ISymbol theSymbol =
				mapContextManager.getSymbolManager().createSymbol(
				Geometry.TYPES.POINT, color);
        int idSymbol = lyr.addSymbol(theSymbol);
        GeometryManager geomManager = GeometryLocator.getGeometryManager();
//        org.gvsig.fmap.geom.Geometry geom = geomManager.create(center.getX(),center.getY());
        Point geom = null;
		try {
			geom = (Point)geomManager.create(TYPES.POINT, SUBTYPES.GEOM2D);
			geom.setX(center.getX());
			geom.setY(center.getY());
		} catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            LOG.error("Error creating a point geometry", e);
		}
//        FGraphic theGraphic = new FGraphic(geom, idSymbol);
//        lyr.addGraphic(theGraphic);
        lyr.addGraphic(geom, idSymbol);
        mapControl.drawGraphics();

    }


	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getColorPanel() {
		if (colorPanel==null){
		 	colorPanel=new ColorChooserPanel();
		 	colorPanel.setAlpha(250);
		 	colorPanel.setColor(CenterViewToPointExtension.COLOR);
		}
		 	return colorPanel;
	}

	/**
	 * This method initializes okCancelPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private AcceptCancelPanel getOkCancelPanel() {
		if (okCancelPanel == null) {
			ActionListener okAction, cancelAction;
			okAction = new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				try{
    				zoomToCoordinates();
    				}catch (Exception e1) {
    					JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"formato_de_numero_incorrecto"));
    					return;
    				}
    				// y sale.
                    if (PluginServices.getMainFrame() == null)
                        ((JDialog) (getParent().getParent().getParent().getParent())).dispose();
                    else
                        PluginServices.getMDIManager().closeWindow(InputCoordinatesPanel.this);
                    Preferences prefs = Preferences.userRoot().node( "gvsig.centerViewToPoint" );
                    if( prefs.get("showInfo", "True").equalsIgnoreCase("True")){
                    	openInfo();
                    }
                    drawPoint(((ColorChooserPanel)getColorPanel()).getColor());
    			}
    		};
    		cancelAction = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					closeThis();
				}
    		};

			okCancelPanel = new AcceptCancelPanel(okAction, cancelAction);
		}
		return okCancelPanel;
	}

	/**
	 * Close the window.
	 *
	 */
	private void closeThis() {
		PluginServices.getMDIManager().closeWindow(this);

	}

	public Object getWindowProfile() {
		return WindowInfo.DIALOG_PROFILE;
	}


}  //  @jve:decl-index=0:visual-constraint="103,18"
