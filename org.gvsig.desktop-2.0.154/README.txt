
Como compilar
================

Para compilar la aplicacion gvSIG es necesario disponer de:

- Un jdk 1.6 o superior (realmente solo se ha probado con el 1.6 y el 1.7).

- maven instalado en el sistema, recomenado maven 3 (aunque podria compilar 
  con maven 2), y que use el jdk 1.6 o superior. 

- Acceso a internet para acceder a los repositorios de maven de la aplicacion.

- Si usan maven2 debera configurar la variable MAVEN_OPTS con el valor adecuado 
  (ver "notas de compilacion"), con mvn3 en linux no suele ser necesario.

- Descargar la carpeta org.gvsig.desktop y ejecutar "mvn clean install".
  
  Esto, por defecto, desplegara en la carpeta target/product la instalacion 
  de gvSIG.

  Si se va a compilar todo desde cero, y no se tiene acceso al repositorio de
  gvSIG, puede ser necesario compilar antes el projecto org.gvsig.desktop.buildtools
  
Notas de compilacion
========================

MAVEN_OPTS
------------

Usando maven 2, para que compile correctamente gvsig precisaremos especificar 
en la variable MAVEN_OPTS una serie de valores para reserbar memoria para el 
compilador.

Valores adecuados para la compilacion del proyecto "org.gvsig.desktop" son::

  -Xms256m -Xmx512m -XX:PermSize=64m -XX:MaxPermSize=128m

Para asignar estos valores en linux puede hacerlo ejecutando la siguiente
instruccion en la linea de comandos desde la que vaya a compilar::

  export MAVEN_OPTS="-Xms256m -Xmx512m -XX:PermSize=64m -XX:MaxPermSize=128m"


En Windows se establece en esta opci�n o similar
(depende de versi�n de Windows):

  Panel de control
    > Sistema
      > Configurac�n avanzada del sistema
        > Variables de entorno
          > Nueva (en el valor no hace falta comillas)
        

Si esta compilando desde eclipse, precisara tener instalado el plugin maven2e

Usando maven2, cuando no especifica correctamente estos valores pueden 
aparecerle errores como:

- Error "Compilation failure..."::

    [ERROR] BUILD FAILURE
    [INFO] ---------------------------------------------------------------
    [INFO] Compilation failure
    Failure executing javac, but could not parse the error:


    The system is out of resources.
    Consult the following stack trace for details.
    java.lang.OutOfMemoryError: PermGen space


- Error "Error while executing forked tests..."::

    [ERROR] BUILD ERROR
    [INFO] ---------------------------------------------------------------
    [INFO] Error while executing forked tests.; nested exception is org.apache.maven.surefire.booter.shade.org.codehaus.plexus.util.cli.CommandLineException: Error setting up environmental variables

    java.io.IOException: error=12, Cannot allocate memory

gvsig-devel.properties
------------------------------

Por defecto al ejecutar "mvn install" sobre org.gvsig.desktop la aplicacion 
se desplegara en la carpeta target/product de ese mismo directorio. Una vez
ejecutado ese primer comando maven sobre la carpeta org.gvsig.desktop debera 
editar el fichero ".gvsig-devel.properties" de la carpeta del usuario si 
desea cambiarlo.

Si tiene varios espacios de trabajo y desea que cada uno despliegue sobre 
una carpeta distinta puede copiar el fichero de su carpeta de usuario a la 
carpeta raiz de org.gvsig.desktop con el nombre "gvsig-devel.properties" y 
cambiar en el sobre donde quiere hacer el despliegue, asi puede tener varias
"instancias" del desarrollo de gvSIG desplegandose sobre sitios distintos.

Si ejecuta "mvn install" desde algun subproyecto de org.gvsig.desktop tenga 
en cuenta que solo se utilizara el fichero ".gvsig-devel.properties" que hay 
en su carpeta de usuario y no el del que se encuentre en org.gvsig.desktop.


Como generar una distribucion de gvSIG
==========================================

- Descargaremos el trunk o nos aseguraremos de que estemos actualizados.

- Nos cercioraremos que en nuestro fichero "gvsig-devel.properties", se apunta
  al target de lo que nos acabamos de descargar, o simplemente borraremos
  el fichero para que se recree con el valor correcto.
  
- Comprobaremos que el numero de build de 

  org.gvsig.desktop/org.gvsig.desktop.plugin/org.gvsig.app/org.gvsig.app.mainplugin/buildNumber.properties
  
  Es el numero de build con el que debe salir la nueva distribucion.
  
  Si no lo es, ejecutaremos:
  
    mvn -Dincrease-build-number process-sources
  
  desde la carpeta org.gvsig.desktop  para incrementarlo y commitaremos los cambios.

- "cd org.gvsig.desktop"

- "mvn clean"

- "mvn install"

  Si la compilacion no ha dado problemas probaremos a ejecutar el gvSIG que hay en la carpeta
  org.gvsig.desktop/target/product para cercionarnos de que por lo menos arranca.

- "mvn release:prepare 2>&1 | tee /tmp/mvn-release-prepare.log"

  Esto incrementa en local los numeros de version de todos los proyecos maven y prepara el tag
  con los fuentes en el repositorio.

- "cd org.gvsig.desktop.installer"

- "mvn clean"

- "mvn install"

  Con esto se generara el instalable de gvSIG, asi como el paquete (gvspks) con los plugins basicos
  de la distribucion, y un zip con estos para incluirlos en el pool del repositorio de plugins
  de gvSIG. 
  Como ya hemos hecho el "release:prepare" y aun no el "release:perform", los numeros de version de los 
  proyectos saldran con las versiones finales (sin SNAPSHOT).

- Subiremos los binarios que haya en org.gvsig.desktop.installer/target a donde se
  encuentren los builds de la aplicacion, por ejemplo:
  
  http://downloads.gvsig.org/download/gvsig-desktop/dists/2.1.0/builds
    
  Los ficheros a subir, por ejemplo para la 2.1.0 build 2202 serian:
  
  - gvSIG-desktop-2.1.0-2202-devel-win-x86-online.exe (binarios para windows)
  - gvSIG-desktop-2.1.0-2202-devel-lin-x86-online.bin (binarios para linux)
  - gvSIG-desktop-2.1.0-2202-devel-all-x86-online.zip (zip multiplataforma)
  - gvSIG-desktop-2.1.0-2202-devel-all-all.gvspks (paquetes a usar en la instalacion)
  - gvSIG-desktop-2.1.0-2202-devel-all-all-pool.zip (paquetes para incluir en el pool)

- Si despues de ejecutar el "release:prepare" y antes de llegar aqui se produce algun error,
  ejecutaremos un "mvn release:rollback" para deshacer los cambios.
  
  A pesar de ello, se habra quedado creado el tag en el SVN, que podemos ir y eliminar a mano.
  
- "mvn release:perform 2>&1 | tee /tmp/mvn-release-perform.log"

  Si todo habia ido bien, al ejecutar esto, se subiran los artefactos maven de la distribucion al
  repositorio de maven, y se crearan los sites de todos los proyectos para esta version.
  
  Los artefactos con la nueva version en SNAPSHOT no se habran compilado, ni instalado o
  desplegado. Si consideramos oportuno disponer en el repo de maven de los
  nuevos artefactos en SNAPSHOT deberemos hacerlo a mano tras terminar todo el proceso.

Con esto se deberia haber completado la generacion de la distribucion. Ahora faltaria 
actualizar el repositorio de plugins de gvSIG.


Notas pendientes
=================

Puede ser interesante mirar el plugin 

http://mojo.codehaus.org/versions-maven-plugin/


A tener en cuenta
--------------------

- Andami se lleva ahora muchas mas dependencias que antes, entre otras el 
  api de la libreria de proyecciones (se lo lleva org.gvsig.ui) pero como 
  no se lleva una implementacion peta en la inicializacion. De momento he 
  puesto un exclude de org.gvsig.projection en el assembly.

- Los snappers que hay en appgvsig dependen de jts.
  Habria que ver de cambiar su implementacion para que usen el API
  de la libreria de geometrias y eliminar la dependencia de compilacion
  de appgvsig hacia jts del pom.

  - TangentPointSnapper
  - PixelSnapper
  - MediumPointSnapper
  - PerpendicularPointSnapper
  - NearestPointSnapper

- La clase Line2DOffset en org.gvsig.symbology.lib.impl hace uso intensivo 
  de JTS. Ver si es viable crear una operacion sobre la geometria que haga 
  lo que hace esa clase.

- En org.gvsig.symbology.swing se hace uso de funcionalidades de andami.

- Hay que repasar la asignacion de iconos a las capas. Ver la extension 
  de extDWG, pero puede afectar a otras.

- Ver que pasa con las clases de DAL FeatureQueryOrder y DefaultFeatureComparator.
  De momento he hecho una chapuza y he incrustado DefaultFeatureComparator 
  dentro de FeatureQueryOrder en el API.

- En la libreria de geometrias, la operacion Equals y el metodo equals de 
  Geometry hacen cosas distintas. En mapcontext, EqualsGeometryEvaluator, 
  he sustituido la llamada a la operacion por el metodo.

- El proyecto org.gvsig.new.layer.lib.impl tiene una dependencia con
  org.gvsig.projection.cresques.ui

- En PluginServices, en el metodo getPluginHomeFolder, habria que hacer que 
  si es nuevo copie los contenidos de los AlternativeNames a el. De esta 
  forma seguira funcionanado con la configuracion que tuviese el usuario 
  aunque se renombre un plugin.


