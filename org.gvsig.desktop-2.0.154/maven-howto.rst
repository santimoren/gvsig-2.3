
==================================
Usefull maven "howtos" and FAQs
==================================

.. contents::

How to reduce the process of "install" to run as fast as possible.
-------------------------------------------------------------------

Can reduce install execution skiping test execution and compilation,
javadoc generation, test signature checking, license checking, and
attach sources in jar.

  mvn  -Danimal.sniffer.skip=true -Dsource.skip=true -Dmaven.javadoc.skip=true -Dmaven.test.skip=true -DskipTests -Dgvsig.skip.downloadPluginTemplates=true install

How to increment the build number of gvSIG plugins
----------------------------------------------------

To increase the build number of gvSIG plugins, yo can do:

  mvn -Dincrease-build-number process-sources

How to deploy a package of a gvSIG plugin
--------------------------------------------

Yo can deploy the package of a gvSIG plugin with:

  mvn -Ddeploy-package -Duser=USER -Dpassword=PASSWORD install

Notes:
- Require that the gvsig.package.info.poolURL property that this set to the correct value.
- The process uses WEBDAV to upload the packages, gvspkg and gvspki, and require
  access to write in the location specified by gvsig.package.info.poolURL
- If "user" or "password" is not present, the process ask its each time it need.
- If folder specified in  gvsig.package.info.poolURL does not exist, the process try to create it.
- The process create a file "addon-request.txt" in the target with the information to
  add to the ticket to request the update of the package in the main repository of
  packages of gvSIG.

How show the list of files that have problems with the header.
----------------------------------------------------------------

You can use the mave plugin "license" to check the headers of the files in
the project. Use::

    mvn -Dlicense.quiet=false license:check

How to skip license check from command line
----------------------------------------------

If in the project is enabled by default the checking of the headers
of files, you can skip this setting the property "license.skip" to
true in the command line::

    mvn -Dlicense.skip=true install

How to skip attach sources in jar from command line
------------------------------------------------------

If in the project is enabled by default the generation of jar whith
the sources of the project, you can disable this setting the property
"source.skip" to true in the command line::

    mvn -Dsource.skip=true  install

How to skip test compile from command line
--------------------------------------------

You can skip the compilation of test setting the propety "maven.test.skip"
to true in the command line::

    mvn -Dmaven.test.skip=true  install


How to skip test execution from command line
----------------------------------------------

You can skip the tests execution setting the propety "skipTests" to true
in the command line::

    mvn -DskipTests install

How to skip javadoc generation from command line
--------------------------------------------------

You can skip the javadoc generation setting the property
"maven.javadoc.skip" to true in the command line::

    mvn -Dmaven.javadoc.skip=true  install

How to skip test signature cheks from command line
---------------------------------------------------

You can skip the signature check setting the property
"animal.sniffer.skip" to true in the command line::

    mvn -Danimal.sniffer.skip=true install

How to install a project without install submodules
----------------------------------------------------------

To install a project with submodules and only install the
parent project without submodules use the option "--non-recursive" ::

    mvn --non-recursive install

How to check and fix the header of source files.
--------------------------------------------------

You can check the headers of the files in you project
using the goal "license". To check the header use::

  mvn license:check

To fix the header use::

  mvn license:format

How to skip test compilation
--------------------------------

To configure a project to don't run the compilation
of test you can add to this pom the next configuration of
the plugin "maven-compiler-plugin"::

  <build>
    <plugins>
      ...
      <plugin>
        <!-- Skip compilation tests -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <executions>
          <execution>
            <id>default-testCompile</id>
            <phase>process-test-sources</phase>
            <goals>
              <goal>testCompile</goal>
            </goals>
            <configuration>
              <skip>true</skip>
            </configuration>
          </execution>
        </executions>
      </plugin>
      ...
    </plugins>
  </build>

Skip test execution
----------------------

To configure a project to don't run the execution
of test you can add to this pom the next configuration of
the plugin "maven-surefire-plugin"::


  <build>
    <plugins>
      ...
      <plugin>
        <!-- Skip test execution -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
      ...
    </plugins>
  </build>

Continue on test failure
-----------------------------

You can configure a project to continue on test execution
failure. To do this add to the pom of the project the next
configuration of plugin "maven-surefire-plugin" ::

  <build>
    <plugins>
      ...
      <plugin>
        <!-- Continue on test failure -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <testFailureIgnore>true</testFailureIgnore>
        </configuration>
      </plugin>
      ...
    </plugins>
  </build>


Set java compatibility
--------------------------

To set the compatibility with a java version  add to the
pom of the project the next configuration of plugin
"maven-compiler-plugin" ::

  <build>
    <plugins>
      ...
      <plugin>
          <!-- Set java compatibility -->
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <configuration>
              <source>1.5</source>
              <target>1.5</target>
              <encoding>ISO-8859-1</encoding>
          </configuration>
      </plugin>
      ...
    </plugins>
  </build>

Packaging tests in jar
------------------------

Test classes do not packaging in jar by default.
To packing add to pom::

  <build>
    <plugins>
      ...
      <plugin>
        <!-- Packaging tests in jar -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <executions>
          <!-- Generates a jar file only with the test classes -->
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/**</include>
              </includes>
            </configuration>
          </execution>
        </executions>
      </plugin>
      ...
    </plugins>
  </build>

How to set a dependency with tests jar
-----------------------------------------

You can set a dependency with a test jar adding to
the declaration of the dependency the scope of
test and the type of "test-jar"::

  <dependency>
      <groupId>...</groupId>
      <artifactId>...</artifactId>
      <type>test-jar</type>
      <scope>test</scope>
  </dependency>

How use ant in maven
-------------------------

You can use ant embed in the pom of you project.
To do this use::

  <plugin>
    <artifactId>maven-antrun-plugin</artifactId>
    <version>1.7</version>
    <executions>
      <execution>
        <phase>generate-sources</phase>
        <configuration>
          <target>
            <echo>Hello world!</echo>
          </target>
        </configuration>
        <goals>
          <goal>run</goal>
        </goals>
      </execution>
    </executions>
  </plugin>

Fail when execute "mvn deploy" with "No connector available"
-------------------------------------------------------------

When execute a "mvn deploy" fail with the error::

  [INFO] ------------------------------------------------------------------------
  [ERROR] Failed to execute goal org.apache.maven.plugins:maven-deploy-plugin:2.7:deploy
    (default-deploy) on project org.gvsig.desktop: Failed to deploy artifacts/metadata:
    No connector available to access repository gvsig-repository (dav:https://devel.gvsig.org/m2repo/j2se)
    of type default using the available factories WagonRepositoryConnectorFactory -> [Help 1]
  [ERROR]

This happens to be configured the webdav wagon as an extension in the section "build"::

  ...
  <build>
    <extensions>
        <extension>
            <groupId>org.apache.maven.wagon</groupId>
            <artifactId>wagon-webdav-jackrabbit</artifactId>
            <version>1.0-beta-7</version>
        </extension>
    </extensions>
  ...

Fail when execute "mvn release: prepare" with "svn command failed... Could not authenticate"
------------------------------------------------------------------------------------------------

When running "mvn release: prepare" updates poms, compiles, and then
fails with the following error ::

  [INFO] ------------------------------------------------------------------------
  [ERROR] Failed to execute goal org.apache.maven.plugins:maven-release-plugin:2.1:prepare
    (default-cli) on project org.gvsig.desktop: Unable to commit files
  [ERROR] Provider message:
  [ERROR] The svn command failed.
  [ERROR] Command output:
  [ERROR] svn: Commit failed (details follow):
  [ERROR] svn: MKACTIVITY of '/svn/gvsig-desktop/!svn/act/931a27bc-57e8-45d9-adcd-5a2cf54a7045':
    authorization failed: Could not authenticate to server: rejected Basic challenge (https://devel.gvsig.org)
  [ERROR] -> [Help 1]
  [ERROR]
  [ERROR]

Apparently maven in linux system use the svn of system and if you're not
authenticated when trying to access to the repository, svn fails.

This is solved by executing a commit from the command line on
some file of the project (only if you have not enabled the option
"store-passwords = no" in $ HOME / .subversion / config). For example, you
can add or remove at the end of "pom.xml" a blank line and then run
from the command line ::

  svn ci -m "" pom.xml

Alternatively can excute::

  svn list https://devel.gvsig.org/svn/gvsig-desktop/

And manually accept the certificate permanently.

Another option that works on Windows in declaring the user and password in the command:

mvn release:prepare -Dusername=[username] -Dpassword=[password]

Fail when execute "mvn release:perform" with unable to find valid certification
------------------------------------------------------------------------------------------------

When running "mvn release:perform" fail with unable to find valid certification::


  [INFO] [ERROR] Failed to execute goal org.apache.maven.plugins:maven-deploy-plugin:2.7:deploy (default-deploy) on project org.gvsig.tools: Failed to deploy artifacts: Could not transfer artifact org.gvsig:org.gvsig.tools:pom:3.0.15 from/to gvsig-repository (dav:https://devel.gvsig.org/m2repo/j2se): sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target -> [Help 1]
  [INFO] [ERROR]
  [INFO] [ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
  [INFO] [ERROR] Re-run Maven using the -X switch to enable full debug logging.
  [INFO] [ERROR]
  [INFO] [ERROR] For more information about the errors and possible solutions, please read the following articles:
  [INFO] [ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException

  Consulte la seccion "Acceso de escritura al repositorio maven de gvSIG" de

  http://www.gvsig.org/plone/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/trabajar-con-el-nucleo-de-gvsig/gvsig-compilation/initial-configuration/initial-configuration/?searchterm=maven%20java%20certification

How to deploy a jar to gvSIG maven repository
---------------------------------------------------

Si queremos subir el fichero:

- file formsrt.jar
- artifactId formsrt
- groupId com.jeta
- version 2.1.0_M3-1

Al repositorio de gvSIG tendriamos que:

- primero crear la carpeta https://devel.gvsig.org/m2repo/j2se/com/jeta/2.1.0_M3-1 en caso de que no exista.
  Podemos hacerlo usando un cliente de webdav, por ejemplo el cadaver.

- Podemos necesitar instalar los certificados correspondientes al https del servidor de gvSIG.
  Para ello podemos seguir las instrucciones indicadas en:

  http://docs.gvsig.org/plone/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/trabajar-con-el-nucleo-de-gvsig/gvsig-compilation/initial-configuration/initial-configuration#acceso-de-escritura-al-repositorio-maven-de-gvsig

- Nos aseguraremos que en el settings.xml estan incluidos en la seccion servers los datos para acceder al servidor de gvSIG::

    <?xml version="1.0" encoding="UTF-8"?>
    <settings xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
			    http://maven.apache.org/xsd/settings-1.0.0.xsd">
	<servers>
	    <server>
		<id>gvsig-repository</id>
		<username>USUARIO</username>
		<password>CLAVE</password>
		<filePermissions>664</filePermissions>
		<directoryPermissions>774</directoryPermissions>
	    </server>
	</servers>
    </settings>

- Luego subimos el fichero ejectando el siguiente comando maven::

    mvn deploy:deploy-file \
      -Durl=https://devel.gvsig.org/m2repo/j2se \
      -DrepositoryId=gvsig-repository \
      -DgroupId=com.jeta \
      -DartifactId=formsrt \
      -Dversion=2.1.0_M3-1 \
      -Dpackaging=jar \
      -Dfile=formsrt.jar \
      -Ddescription="Abeille runtime 2.1.0_M3 with the jgoodies clases removeds."

- Si queremos subir un "tar.gz", sustituiremos el packaging por "tar.gz", por ejemplo:

    mvn deploy:deploy-file \
      -Durl=https://devel.gvsig.org/m2repo/j2se \
      -DrepositoryId=gvsig-repository \
      -DgroupId=org.gdal \
      -DartifactId=gdal \
      -Dversion=1.11.2.1 \
      -Dpackaging=tar.gz \
      -Dclassifier="osgeolive-9.0-ubuntu-14.04.3-amd64" \
      -Dfile=gdal-1.11.2.1-osgeolive-9.0-ubuntu-14.04.3-amd64.tar.gz  \
      -Ddescription="GDAL extraido del OSGEO live 9.0 (Ubuntu 14.04.3 x86_64)"
