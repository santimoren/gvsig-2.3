/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author fdiaz
 *
 */
public class DefaultMultiSurface extends AbstractMultiSurface {

    /**
     *
     */
    private static final long serialVersionUID = 9211543270911740110L;


    /**
     * @param type
     * @param subtype
     */
    public DefaultMultiSurface(GeometryType geometryType) {
        super(geometryType.getSubType());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        DefaultMultiSurface clone = new DefaultMultiSurface(getGeometryType());
        for(int i=0; i<primitives.size(); i++){
            clone.addPrimitive((Primitive)primitives.get(i).cloneGeometry());
        }
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiSurface#addSurface(org.gvsig.fmap.geom.primitive.Surface)
     */
    public void addSurface(Surface surface) {
        GeometryType geometryType = surface.getGeometryType();
        if(geometryType.getSubType() == getGeometryType().getSubType()){
            primitives.add(surface);
            return;
        }
        String msg = StringUtils.replaceEach(
            "Only a surface subtype  %(subtypeSurface)s can be added to a MultiSurface subtype %(subtype)s",
            new String[]{"%(subtypeSurface)s","%(subtype)s"},
            new String[]{String.valueOf(geometryType.getSubType()), String.valueOf(getGeometryType().getSubType())}
        );
        logger.warn(msg);
        throw new UnsupportedOperationException(msg);

    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        switch (getGeometryType().getSubType()) {
        case Geometry.SUBTYPES.GEOM2D:
            return 2;
        case Geometry.SUBTYPES.GEOM2DM:
            return 3;
        case Geometry.SUBTYPES.GEOM3D:
            return 3;
        case Geometry.SUBTYPES.GEOM3DM:
            return 4;
        default:
            return 0;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = GeometryLocator.getGeometryManager().createMultiPoint(getGeometryType().getSubType());
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            MultiPoint primitivePoints = primitive.toPoints();
            for (int i=0; i<primitivePoints.getPrimitivesNumber(); i++) {
                multiPoint.addPoint(primitivePoints.getPointAt(i));
            }
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = GeometryLocator.getGeometryManager().createMultiLine(getGeometryType().getSubType());
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            MultiLine primitiveLines = primitive.toLines();
            for (int i=0; i<primitiveLines.getPrimitivesNumber(); i++) {
                multiLine.addPrimitive((Primitive)primitiveLines.getPrimitiveAt(i).cloneGeometry());
            }
        }
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = GeometryLocator.getGeometryManager().createMultiPolygon(getGeometryType().getSubType());
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            MultiPolygon primitivePolygons = primitive.toPolygons();
            for (int i=0; i<primitivePolygons.getPrimitivesNumber(); i++) {
                multiPolygon.addPrimitive((Primitive)primitivePolygons.getPrimitiveAt(i).cloneGeometry());
            }
        }
        return multiPolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            GeometryJTS primitive = (GeometryJTS) iterator.next();
            primitive.flip();
        }
        Collections.reverse(primitives);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.aggregate.AbstractMultiPrimitive#fixPrimitive(org.gvsig.fmap.geom.primitive.Primitive)
     */
    @Override
    protected Geometry fixPrimitive(Primitive primitive) {
        int primitiveSubType = primitive.getGeometryType().getSubType();
        int subType = getGeometryType().getSubType();
        if(primitiveSubType == subType){
            return primitive;
        }

        String message = StringUtils.replace("This MultiSurface only accept subtype %(subtype)s primitives", "%(subtype)s", String.valueOf(subType));
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        List<com.vividsolutions.jts.geom.Polygon> polygons =
            new ArrayList<com.vividsolutions.jts.geom.Polygon>(primitives.size());
        com.vividsolutions.jts.geom.Polygon polygon = null;
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            if (primitive instanceof Polygon) {
                polygon = (com.vividsolutions.jts.geom.Polygon) ((GeometryJTS) primitive).getJTS();
                polygons.add(polygon);
            } else {
                MultiPolygon multipolygon;
                try {
                    multipolygon = primitive.toPolygons();
                    for (int j = 0; j < multipolygon.getPrimitivesNumber(); j++) {
                        polygon =
                            (com.vividsolutions.jts.geom.Polygon) ((GeometryJTS) multipolygon.getPrimitiveAt(j)).getJTS();
                        polygons.add(polygon);
                    }
                } catch (GeometryException e) {
                    GeometryType geomType = primitive.getGeometryType();
                    logger.warn(StringUtils.replaceEach(
                        "Can't convert primitive type=%(type)s, %(subtype)s to MultiPolygon",
                        new String[] {"%(type)s", "%(subtype)s" },
                        new String[] { String.valueOf(geomType.getType()), String.valueOf(geomType.getSubType()) }));
                }
            }
        }
        return JTSUtils
            .createJTSMultiPolygon(polygons.toArray(new com.vividsolutions.jts.geom.Polygon[polygons.size()]));
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        MultiSurface result = new DefaultMultiSurface(getGeometryType());
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            Geometry offset = primitive.offset(distance);
            if(offset instanceof MultiPrimitive){
                MultiPrimitive multiOffset = (MultiPrimitive)offset;
                for(int i=0; i<multiOffset.getPrimitivesNumber(); i++){
                    result.addPrimitive(multiOffset.getPrimitiveAt(i));
                }
            } else {
                result.addPrimitive((Primitive)primitive.offset(distance));
            }
        }
        return result;
    }

}
