/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.circle;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * @author fdiaz
 *
 */
public class Circle2DZ extends BaseCircle2DZ implements Circle {

    /**
     *
     */
    private static final long serialVersionUID = 4678092839213094025L;

    /**
     * @param subtype
     */
    public Circle2DZ() {
        super(Geometry.TYPES.CIRCLE, Geometry.SUBTYPES.GEOM3D);
    }

    /**
     * @param center
     * @param radius
     */
    public Circle2DZ(Point center, double radius) {
        super(Geometry.TYPES.CIRCLE, Geometry.SUBTYPES.GEOM3D, center, radius);
    }
    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        ArrayListCoordinateSequence coordinates = getJTSCoordinates();
        return JTSUtils.createJTSPolygon(coordinates);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        Circle2DZ clone = new Circle2DZ();
        clone.setPoints(((PointJTS)((PointJTS)center).cloneGeometry()), radius);
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#getRectangleCorner()
     */
    public Point getRectangleCorner() {
        return new Point3D(center.getX()-radius, center.getY()-radius, ((Point3D)center).getZ());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#getHeight()
     */
    public double getRectangleHeight() {
        return radius*2;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#getWidth()
     */
    public double getRectangleWidth() {
        return radius*2;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return new Circle2DZ(((PointJTS)((PointJTS)center).cloneGeometry()), radius+distance);
    }
}
