/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.Collections;
import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractMultiPoint extends AbstractMultiPrimitive implements MultiPoint {

    /**
     *
     */
    private static final long serialVersionUID = 7056816718895043774L;

    /**
     * @param type
     * @param subtype
     */
    public AbstractMultiPoint(int subtype) {
        super(Geometry.TYPES.MULTIPOINT, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        ArrayListCoordinateSequence coordinateSequence = new ArrayListCoordinateSequence();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            PointJTS point = (PointJTS) iterator.next();
            coordinateSequence.add(point.getJTSCoordinate());
        }
        return JTSUtils.createJTSMultiPoint(coordinateSequence);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPoint#addPoint(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addPoint(Point point) {
        point = fixPoint(point);
        primitives.add(point);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPoint#getPointAt(int)
     */
    public Point getPointAt(int index) {
        return (Point)primitives.get(index);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return new DefaultGeneralPathX(getPathIterator(null),false,0);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * )
     */
    public PathIterator getPathIterator(AffineTransform at) {
        PointIterator pi = new PointIterator(at);
        return pi;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * , double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    protected class PointIterator extends GeneralPathXIterator {

        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;
        private int index = 0;

        /**
         * Creates a new PointIterator object.
         *
         * @param p
         *            The polygon
         * @param at
         *            The affine transform applied to coordinates during
         *            iteration
         */
        public PointIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            done = (primitives.size() == ++index);
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            Point point = (Point) primitives.get(index);
            coords[0] = point.getX();
            coords[1] = point.getY();
            at.transform(coords, 0, coords, 0, 1);
            return PathIterator.SEG_MOVETO;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            Point point = (Point) primitives.get(index);
            coords[0] = (float)point.getX();
            coords[1] = (float)point.getY();
            at.transform(coords, 0, coords, 0, 1);
            return PathIterator.SEG_MOVETO;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        Collections.reverse(primitives);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().buffer(distance));
    }

    /**
     * @param point
     * @return
     */
    protected abstract Point fixPoint(Point point);
}
