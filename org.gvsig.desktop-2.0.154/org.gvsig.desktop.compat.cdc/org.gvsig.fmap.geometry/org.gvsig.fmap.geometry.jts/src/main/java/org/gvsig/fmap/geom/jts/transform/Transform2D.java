package org.gvsig.fmap.geom.jts.transform;

import static java.lang.Math.abs;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.transform.Transform;

public class Transform2D implements Transform {

    /**
     * The constant used for testing results.
     */
    public final static double ACCURACY = 1e-12;

    // coefficients for x coordinate.
    protected double m00, m01, m02;

    // coefficients for y coordinate.
    protected double m10, m11, m12;

    /**
     * Creates a new AffineTransform2D, initialized with Identity.
     */
    public Transform2D() {
        // init to identity matrix
        m00 = m11 = 1;
        m01 = m10 = 0;
        m02 = m12 = 0;
    }

    /**
     * Creates a new transform from a java AWT transform.
     * @param transform
     */
    public Transform2D(java.awt.geom.AffineTransform transform) {
        double[] coefs = new double[6];
        transform.getMatrix(coefs);
        m00 = coefs[0];
        m10 = coefs[1];
        m01 = coefs[2];
        m11 = coefs[3];
        m02 = coefs[4];
        m12 = coefs[5];
    }

    /**
     * Creates a new Affine Transform by directly specifying the coefficients,
     * in the order m00, m01, m02, m10, m11, m12 (different order of
     * java.awt.geom.AffineTransform).
     * @param coefs
     */
    public Transform2D(double[] coefs) {
        if (coefs.length == 4) {
            m00 = coefs[0];
            m01 = coefs[1];
            m10 = coefs[2];
            m11 = coefs[3];
        } else {
            m00 = coefs[0];
            m01 = coefs[1];
            m02 = coefs[2];
            m10 = coefs[3];
            m11 = coefs[4];
            m12 = coefs[5];
        }
    }

    public Transform2D(double xx, double yx, double tx, double xy,
            double yy, double ty) {
        m00 = xx;
        m01 = yx;
        m02 = tx;
        m10 = xy;
        m11 = yy;
        m12 = ty;
    }
    
    /**
     * Returns coefficients of the transform in a linear array of 6 double.
     * @return 
     */
    public double[] coefficients() {
        double[] tab = {m00, m01, m02, m10, m11, m12};
        return tab;
    }

    /**
     * Returns the 3x3 square matrix representing the transform.
     *
     * @return the 3x3 affine transform representing the matrix
     */
    public double[][] affineMatrix() {
        double[][] tab = new double[][]{
            new double[]{m00, m01, m02},
            new double[]{m10, m11, m12},
            new double[]{0, 0, 1}};
        return tab;
    }

    /**
     * Returns this transform as an instance of java AWT AffineTransform.
     * @return 
     */
    public java.awt.geom.AffineTransform asAwtTransform() {
        return new java.awt.geom.AffineTransform(
                this.m00, this.m10, this.m01, this.m11, this.m02, this.m12);
    }

    /**
     * Returns the affine transform created by applying first the affine
     * transform given by <code>other</code>, then this affine transform. This
     * is the equivalent method of the 'concatenate' method in
     * java.awt.geom.AffineTransform.
     *
     * @param other the transform to apply first
     * @return the composition this * that
     */
    @Override
    public Transform concatenate(Transform other) {
        if (!(other instanceof Transform2D)) {
            throw new IllegalArgumentException("Can't concatenate an AffineTransform2D with a non AffineTransform2D (other " + other.toString() + ")");
        }
        Transform2D that = (Transform2D) other;
        double n00 = this.m00 * that.m00 + this.m01 * that.m10;
        double n01 = this.m00 * that.m01 + this.m01 * that.m11;
        double n02 = this.m00 * that.m02 + this.m01 * that.m12 + this.m02;
        double n10 = this.m10 * that.m00 + this.m11 * that.m10;
        double n11 = this.m10 * that.m01 + this.m11 * that.m11;
        double n12 = this.m10 * that.m02 + this.m11 * that.m12 + this.m12;
        return new Transform2D(n00, n01, n02, n10, n11, n12);
    }

    /**
     * Returns the affine transform created by applying first this affine
     * transform, then the affine transform given by <code>that</code>. This the
     * equivalent method of the 'preConcatenate' method in
     * java.awt.geom.AffineTransform. <code><pre>
     * shape = shape.transform(T1.chain(T2).chain(T3));
     * </pre></code> is equivalent to the sequence: <code><pre>
     * shape = shape.transform(T1);
     * shape = shape.transform(T2);
     * shape = shape.transform(T3);
     * </pre></code>
     *
     * @param other the transform to apply in a second step
     * @return the composition that * this
     */
    public Transform chain(Transform other) {
        if (!(other instanceof Transform2D)) {
            throw new IllegalArgumentException("Can't concatenate an AffineTransform2D with a non AffineTransform2D (other " + other.toString() + ")");
        }
        Transform2D that = (Transform2D) other;
        return new Transform2D(
                that.m00 * this.m00 + that.m01 * this.m10,
                that.m00 * this.m01 + that.m01 * this.m11,
                that.m00 * this.m02 + that.m01 * this.m12 + that.m02,
                that.m10 * this.m00 + that.m11 * this.m10,
                that.m10 * this.m01 + that.m11 * this.m11,
                that.m10 * this.m02 + that.m11 * this.m12 + that.m12);
    }

    /**
     * Return the affine transform created by applying first this affine
     * transform, then the affine transform given by <code>that</code>. This the
     * equivalent method of the 'preConcatenate' method in
     * java.awt.geom.AffineTransform.
     *
     * @param other the transform to apply in a second step
     * @return the composition that * this
     */
    @Override
    public Transform preConcatenate(Transform other) {
        return this.chain(other);
    }

    /**
     * Tests if this affine transform is a similarity.
     *
     * @return
     */
    public boolean isSimilarity() {
        // computation shortcuts
        double a = this.m00;
        double b = this.m01;
        double c = this.m10;
        double d = this.m11;

        // determinant
        double k2 = abs(a * d - b * c);

        // test each condition
        if (abs(a * a + b * b - k2) > ACCURACY) {
            return false;
        }
        if (abs(c * c + d * d - k2) > ACCURACY) {
            return false;
        }
        if (abs(a * a + c * c - k2) > ACCURACY) {
            return false;
        }
        if (abs(b * b + d * d - k2) > ACCURACY) {
            return false;
        }

        // if each test passed, return true
        return true;
    }

    /**
     * Tests if this affine transform is a motion, i.e. is composed only of
     * rotations and translations.
     * @return 
     */
    public boolean isMotion() {
        // Transform must be 1) an isometry and 2) be direct
        return isIsometry() && isDirect();
    }

    /**
     * Tests if this affine transform is an isometry, i.e. is equivalent to a
     * compound of translations, rotations and reflections. Isometry keeps area
     * of shapes unchanged, but can change orientation (direct or indirect).
     *
     * @return true in case of isometry.
     */
    public boolean isIsometry() {
        // extract matrix coefficients
        double a = this.m00;
        double b = this.m01;
        double c = this.m10;
        double d = this.m11;

        // transform vectors should be normalized
        if (abs(a * a + b * b - 1) > ACCURACY) {
            return false;
        }
        if (abs(c * c + d * d - 1) > ACCURACY) {
            return false;
        }

        // determinant must be -1 or +1
        if (abs(a * b + c * d) > ACCURACY) {
            return false;
        }

        // if all tests passed, return true;
        return true;
    }

    /**
     * Tests if this affine transform is direct, i.e. the sign of the
     * determinant of the associated matrix is positive. Direct transforms
     * preserve the orientation of transformed shapes.
     * @return 
     */
    public boolean isDirect() {
        return this.m00 * this.m11 - this.m01 * this.m10 > 0;
    }

    /**
     * Tests is this affine transform is equal to the identity transform.
     *
     * @return true if this transform is the identity transform
     */
    @Override
    public boolean isIdentity() {
        if (abs(this.m00 - 1) > ACCURACY) {
            return false;
        }
        if (abs(this.m01) > ACCURACY) {
            return false;
        }
        if (abs(this.m02) > ACCURACY) {
            return false;
        }
        if (abs(this.m10) > ACCURACY) {
            return false;
        }
        if (abs(this.m11 - 1) > ACCURACY) {
            return false;
        }
        if (abs(this.m12) > ACCURACY) {
            return false;
        }
        return true;
    }

    /**
     * Returns the inverse transform. If the transform is not invertible, throws
     * a new NonInvertibleTransformException.
     *
     * @return
     */
    @Override
    public Transform inverse() {
        double det = m00 * m11 - m10 * m01;

        if (Math.abs(det) < ACCURACY) {
            throw new NonInvertibleTransformException(this);
        }

        return new Transform2D(
                m11 / det, -m01 / det, (m01 * m12 - m02 * m11) / det,
                -m10 / det, m00 / det, (m02 * m10 - m00 * m12) / det);
    }

    private Point createPoint2D(double x, double y) {
        try {
            return GeometryLocator.getGeometryManager().createPoint(x, y, Geometry.SUBTYPES.GEOM2D);
        } catch (CreateGeometryException ex) {
            throw new RuntimeException("Can't create Point2D", ex);
        }
    }

    /**
     * Computes the coordinates of the transformed point.
     *
     * @param p
     * @return
     */
    @Override
    public Point transform(Point p) {
        Point dst = createPoint2D(
                p.getX() * m00 + p.getY() * m01 + m02,
                p.getX() * m10 + p.getY() * m11 + m12);
        return dst;
    }

    @Override
    public Point[] transform(Point[] src, Point[] dst) {
        if (dst == null) {
            dst = new Point[src.length];
        }

        double x, y;
        for (int i = 0; i < src.length; i++) {
            x = src[i].getX();
            y = src[i].getY();
            dst[i] = createPoint2D(
                    x * m00 + y * m01 + m02,
                    x * m10 + y * m11 + m12);
        }
        return dst;
    }

    /**
     * Displays the coefficients of the transform, row by row.
     *
     * @return
     */
    @Override
    public String toString() {
        return new String("AffineTransform2D(" + m00 + "," + m01 + "," + m02
                + "," + m10 + "," + m11 + "," + m12 + ",");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Transform2D)) {
            return false;
        }

        Transform2D that = (Transform2D) obj;

        if (!EqualUtils.areEqual(this.m00, that.m00)) {
            return false;
        }
        if (!EqualUtils.areEqual(this.m01, that.m01)) {
            return false;
        }
        if (!EqualUtils.areEqual(this.m02, that.m02)) {
            return false;
        }
        if (!EqualUtils.areEqual(this.m00, that.m00)) {
            return false;
        }
        if (!EqualUtils.areEqual(this.m01, that.m01)) {
            return false;
        }
        if (!EqualUtils.areEqual(this.m02, that.m02)) {
            return false;
        }

        return true;
    }

    @Override
    public Transform2D clone() {
        return new Transform2D(m00, m01, m02, m10, m11, m12);
    }
}
