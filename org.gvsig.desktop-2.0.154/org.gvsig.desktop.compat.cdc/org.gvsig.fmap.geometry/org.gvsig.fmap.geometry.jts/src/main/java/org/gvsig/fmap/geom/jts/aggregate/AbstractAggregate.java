/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Iterator;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.jts.AbstractGeometry;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractAggregate extends AbstractGeometry implements Aggregate {

    /**
     *
     */
    private static final long serialVersionUID = -2922828622616885472L;

    ArrayList<Primitive> primitives;

    /**
     * @param type
     * @param subtype
     */
    public AbstractAggregate(int type, int subtype) {
        super(type, subtype);
        primitives = new ArrayList<Primitive>();
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.Aggregate#getPrimitivesNumber()
     */
    public int getPrimitivesNumber() {
        return primitives.size();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.Aggregate#getPrimitiveAt(int)
     */
    public Primitive getPrimitiveAt(int i) {
        return primitives.get(i);
    }

    private class AggregateIterator implements Iterator<Geometry> {

        private int current = 0;

        AggregateIterator() {
        }

        @Override
        public boolean hasNext() {
            if( current<getPrimitivesNumber() ) {
                return true;
            }
            return false;
        }

        @Override
        public Geometry next() {
            return getPrimitiveAt(current++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

    @Override
    public Iterator<Geometry> iterator() {
        return new AggregateIterator();
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        primitives.ensureCapacity(capacity);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            primitive.reProject(ct);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            primitive.transform(at);
        }
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            GeometryJTS primitive = (GeometryJTS) iterator.next();
            if (primitive.is3D()) {
                return true;
            };
        }
        return false;
    }


    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return new DefaultGeneralPathX(getPathIterator(null),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.AbstractGeometry#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(super.equals(obj) && obj instanceof AbstractAggregate){
            AbstractAggregate other = (AbstractAggregate)obj;
            if(other.getPrimitivesNumber() == this.getPrimitivesNumber()){
                for (int j = 0; j < this.getPrimitivesNumber(); j++) {
                    if(!this.getPrimitiveAt(j).equals(other.getPrimitiveAt(j))){
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

}
