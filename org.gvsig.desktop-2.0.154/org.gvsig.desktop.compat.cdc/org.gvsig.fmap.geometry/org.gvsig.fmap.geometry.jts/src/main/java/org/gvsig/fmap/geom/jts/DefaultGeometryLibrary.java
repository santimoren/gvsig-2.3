/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.jts;

import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLibrary;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.aggregate.DefaultMultiCurve;
import org.gvsig.fmap.geom.jts.aggregate.DefaultMultiSurface;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3DM;
import org.gvsig.fmap.geom.jts.coerce.CoerceToByteArray;
import org.gvsig.fmap.geom.jts.coerce.CoerceToEnvelope;
import org.gvsig.fmap.geom.jts.coerce.CoerceToGeometry;
import org.gvsig.fmap.geom.jts.coerce.CoerceToString;
import org.gvsig.fmap.geom.jts.operation.fromjts.FromJTS;
import org.gvsig.fmap.geom.jts.operation.fromwkb.FromPostGISEWKB;
import org.gvsig.fmap.geom.jts.operation.fromwkb.FromWKB;
import org.gvsig.fmap.geom.jts.operation.fromwkt.FromWKT;
import org.gvsig.fmap.geom.jts.operation.tojts.ToJTS;
import org.gvsig.fmap.geom.jts.operation.towkb.ToPostGISEWKB;
import org.gvsig.fmap.geom.jts.operation.towkb.ToOGCWKB;
import org.gvsig.fmap.geom.jts.operation.towkt.ToWKT;
import org.gvsig.fmap.geom.jts.persistence.GeometryPersistenceFactory;
import org.gvsig.fmap.geom.jts.persistence.GeometryTypePersistenceFactory;
import org.gvsig.fmap.geom.jts.primitive.DefaultEnvelope;
import org.gvsig.fmap.geom.jts.primitive.DefaultNullGeometry;
import org.gvsig.fmap.geom.jts.primitive.Envelope2D;
import org.gvsig.fmap.geom.jts.primitive.Envelope3D;
import org.gvsig.fmap.geom.jts.primitive.Geometry2D;
import org.gvsig.fmap.geom.jts.primitive.Geometry2DM;
import org.gvsig.fmap.geom.jts.primitive.Geometry3D;
import org.gvsig.fmap.geom.jts.primitive.Geometry3DM;
import org.gvsig.fmap.geom.jts.primitive.curve.arc.Arc2D;
import org.gvsig.fmap.geom.jts.primitive.curve.arc.Arc2DZ;
import org.gvsig.fmap.geom.jts.primitive.curve.circumference.Circumference2D;
import org.gvsig.fmap.geom.jts.primitive.curve.circumference.Circumference2DZ;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3DM;
import org.gvsig.fmap.geom.jts.primitive.curve.periellipse.PeriEllipse2D;
import org.gvsig.fmap.geom.jts.primitive.curve.periellipse.PeriEllipse2DZ;
import org.gvsig.fmap.geom.jts.primitive.curve.spline.Spline2D;
import org.gvsig.fmap.geom.jts.primitive.curve.spline.Spline2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.spline.Spline3D;
import org.gvsig.fmap.geom.jts.primitive.curve.spline.Spline3DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point2DGeometryType;
import org.gvsig.fmap.geom.jts.primitive.point.Point2DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DGeometryType;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DM;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring2D;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring2DM;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring3D;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.circle.Circle2D;
import org.gvsig.fmap.geom.jts.primitive.surface.circle.Circle2DZ;
import org.gvsig.fmap.geom.jts.primitive.surface.ellipse.Ellipse2D;
import org.gvsig.fmap.geom.jts.primitive.surface.ellipse.Ellipse2DZ;
import org.gvsig.fmap.geom.jts.primitive.surface.ellipticarc.EllipticArc2D;
import org.gvsig.fmap.geom.jts.primitive.surface.ellipticarc.EllipticArc2DZ;
import org.gvsig.fmap.geom.jts.primitive.surface.filledspline.FilledSpline2D;
import org.gvsig.fmap.geom.jts.primitive.surface.filledspline.FilledSpline2DM;
import org.gvsig.fmap.geom.jts.primitive.surface.filledspline.FilledSpline3D;
import org.gvsig.fmap.geom.jts.primitive.surface.filledspline.FilledSpline3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.persistence.PersistenceManager;

/**
 * Registers the default implementation for {@link GeometryManager}
 *
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultGeometryLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsImplementationOf(GeometryLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        // Register the default GeometryManager
        GeometryLocator.registerGeometryManager(DefaultGeometryManager.class);
    }

    protected void doPostInitialize() throws LibraryException {

        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        DataTypesManager dataTypesManager = ToolsLocator.getDataTypesManager();

        dataTypesManager.setCoercion(DataTypes.GEOMETRY, new CoerceToGeometry());
        dataTypesManager.setCoercion(DataTypes.ENVELOPE, new CoerceToEnvelope());
        dataTypesManager.setCoercion(DataTypes.STRING,
            new CoerceToString(dataTypesManager.getCoercion(DataTypes.STRING)));
        dataTypesManager.setCoercion(DataTypes.BYTEARRAY,
            new CoerceToByteArray(dataTypesManager.getCoercion(DataTypes.BYTEARRAY)));

        GeometryOperation.OPERATION_INTERSECTS_CODE =
            geometryManager.getGeometryOperationCode(GeometryOperation.OPERATION_INTERSECTS_NAME);

        GeometryOperation.OPERATION_CONTAINS_CODE =
            geometryManager.getGeometryOperationCode(GeometryOperation.OPERATION_CONTAINS_NAME);

        // Register the geometries in 2D
        geometryManager.registerGeometryType(DefaultNullGeometry.class, "Null", TYPES.NULL, SUBTYPES.GEOM2D);

        geometryManager.registerGeometryType(Geometry2D.class, "Geometry2D", TYPES.GEOMETRY, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Geometry3D.class, "Geometry3D", TYPES.GEOMETRY, SUBTYPES.GEOM3D);
        geometryManager.registerGeometryType(Geometry2DM.class, "Geometry2DM", TYPES.GEOMETRY, SUBTYPES.GEOM2DM);
        geometryManager.registerGeometryType(Geometry3DM.class, "Geometry3DM", TYPES.GEOMETRY, SUBTYPES.GEOM3DM);

        // Register points in 2D
        geometryManager.registerGeometryType(new Point2DGeometryType());
//        geometryManager.registerGeometryType(Point2D.class, "Point2D", TYPES.POINT, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Point2DM.class, "Point2DM", TYPES.POINT, SUBTYPES.GEOM2DM);

        // Register curves in 2D
        geometryManager.registerGeometryType(Line2D.class, "Line2D", TYPES.LINE, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(Line2D.class, "Curve2D", TYPES.CURVE, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(Arc2D.class, "Arc2D", TYPES.ARC, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(Spline2D.class, "Spline2D", TYPES.SPLINE, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(Circumference2D.class, "Circumference2D", TYPES.CIRCUMFERENCE, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(PeriEllipse2D.class, "PeriEllipse2D", TYPES.PERIELLIPSE, SUBTYPES.GEOM2D, TYPES.CURVE);

        // Register surfaces in 2D
        geometryManager.registerGeometryType(Polygon2D.class, "Polygon2D", TYPES.POLYGON, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Polygon2D.class, "Surface2D", TYPES.SURFACE, SUBTYPES.GEOM2D, TYPES.SURFACE);

        geometryManager.registerGeometryType(Circle2D.class, "Circle2D", TYPES.CIRCLE, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ellipse2D.class, "Ellipse2D", TYPES.ELLIPSE, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ring2D.class, "Ring2D", TYPES.RING, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(FilledSpline2D.class, "FilledSpline2D", TYPES.FILLEDSPLINE, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(EllipticArc2D.class, "EllipticArc2D", TYPES.ELLIPTICARC, SUBTYPES.GEOM2D, TYPES.SURFACE);

        // Register curves in 2DM
        geometryManager.registerGeometryType(Line2DM.class, "Line2DM", TYPES.LINE, SUBTYPES.GEOM2DM, TYPES.CURVE);
        geometryManager.registerGeometryType(Line2DM.class, "Curve2DM", TYPES.CURVE, SUBTYPES.GEOM2DM, TYPES.CURVE);
        geometryManager.registerGeometryType(Spline2DM.class, "Spline2DM", TYPES.SPLINE, SUBTYPES.GEOM2DM, TYPES.CURVE);

        // Register surfaces in 2DM
        geometryManager.registerGeometryType(Polygon2DM.class, "Polygon2DM", TYPES.POLYGON, SUBTYPES.GEOM2DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(Polygon2DM.class, "Surface2DM", TYPES.SURFACE, SUBTYPES.GEOM2DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ring2DM.class, "Ring2DM", TYPES.RING, SUBTYPES.GEOM2DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(FilledSpline2DM.class, "FilledSpline2DM", TYPES.FILLEDSPLINE, SUBTYPES.GEOM2DM, TYPES.SURFACE);

        // Register multigeometries in 2D
//        geometryManager.registerGeometryType(BaseMultiPrimitive2D.class, "MultiPrimitive2D", TYPES.AGGREGATE,
//            SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(MultiPoint2D.class, "MultiPoint2D", TYPES.MULTIPOINT, SUBTYPES.GEOM2D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(DefaultMultiCurve.class, "MultiCurve", TYPES.MULTICURVE, SUBTYPES.GEOM2D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiLine2D.class, "MultiLine2D", TYPES.MULTILINE, SUBTYPES.GEOM2D, new int[]{TYPES.AGGREGATE, TYPES.MULTICURVE});
        geometryManager.registerGeometryType(DefaultMultiSurface.class, "MultiSurface", TYPES.MULTISURFACE, SUBTYPES.GEOM2D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiPolygon2D.class, "MultiPolygon2D", TYPES.MULTIPOLYGON, SUBTYPES.GEOM2D, new int[]{TYPES.AGGREGATE, TYPES.MULTISURFACE});

        // Register multigeometries in 2DM
        geometryManager.registerGeometryType(MultiPoint2DM.class, "MultiPoint2DM", TYPES.MULTIPOINT, SUBTYPES.GEOM2DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(DefaultMultiCurve.class, "MultiCurve", TYPES.MULTICURVE, SUBTYPES.GEOM2DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiLine2DM.class, "MultiLine2DM", TYPES.MULTILINE, SUBTYPES.GEOM2DM, new int[]{TYPES.AGGREGATE, TYPES.MULTICURVE});
        geometryManager.registerGeometryType(DefaultMultiSurface.class, "MultiSurface", TYPES.MULTISURFACE, SUBTYPES.GEOM2DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiPolygon2DM.class, "MultiPolygon2DM", TYPES.MULTIPOLYGON, SUBTYPES.GEOM2DM, new int[]{TYPES.AGGREGATE, TYPES.MULTISURFACE});

        // Register the geometries in 3D
        geometryManager.registerGeometryType(DefaultNullGeometry.class, TYPES.NULL, SUBTYPES.GEOM3D);

        // Register points in 3D
        geometryManager.registerGeometryType(new Point3DGeometryType());
//        geometryManager.registerGeometryType(Point3D.class, "Point3D", TYPES.POINT, SUBTYPES.GEOM3D);
        geometryManager.registerGeometryType(Point3DM.class, "Point3DM", TYPES.POINT, SUBTYPES.GEOM3DM);

        // Register curves in 3D
        geometryManager.registerGeometryType(Line3D.class, "Line3D", TYPES.LINE, SUBTYPES.GEOM3D, TYPES.CURVE);
        geometryManager.registerGeometryType(Line3D.class, "Curve3D", TYPES.CURVE, SUBTYPES.GEOM3D, TYPES.CURVE);
        geometryManager.registerGeometryType(Arc2DZ.class, "Arc3D", TYPES.ARC, SUBTYPES.GEOM3D, TYPES.CURVE);
        geometryManager.registerGeometryType(Spline3D.class, "Spline3D", TYPES.SPLINE, SUBTYPES.GEOM3D, TYPES.CURVE);
        geometryManager.registerGeometryType(Circumference2DZ.class, "Circumference2DZ", TYPES.CIRCUMFERENCE, SUBTYPES.GEOM3D, TYPES.CURVE);
        geometryManager.registerGeometryType(PeriEllipse2DZ.class, "PeriEllipse2DZ", TYPES.PERIELLIPSE, SUBTYPES.GEOM3D, TYPES.CURVE);

        // Register surfaces in 3D
        geometryManager.registerGeometryType(Polygon3D.class, "Polygon3D", TYPES.POLYGON, SUBTYPES.GEOM3D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Polygon3D.class, "Surface3D", TYPES.SURFACE, SUBTYPES.GEOM3D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ring3D.class, "Ring3D", TYPES.RING, SUBTYPES.GEOM3D, TYPES.SURFACE);
        geometryManager.registerGeometryType(FilledSpline3D.class, "FilledSpline3D", TYPES.FILLEDSPLINE, SUBTYPES.GEOM3D, TYPES.SURFACE);

        geometryManager.registerGeometryType(Circle2DZ.class, "Circle2DZ", TYPES.CIRCLE, SUBTYPES.GEOM3D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ellipse2DZ.class, "Ellipse2DZ", TYPES.ELLIPSE, SUBTYPES.GEOM3D, TYPES.SURFACE);

        geometryManager.registerGeometryType(EllipticArc2DZ.class, "EllipticArc3D", TYPES.ELLIPTICARC, SUBTYPES.GEOM3D, TYPES.SURFACE);

        // Register curves in 3DM
        geometryManager.registerGeometryType(Line3DM.class, "Line3DM", TYPES.LINE, SUBTYPES.GEOM3DM, TYPES.CURVE);
        geometryManager.registerGeometryType(Line3DM.class, "Curve3DM", TYPES.CURVE, SUBTYPES.GEOM3DM, TYPES.CURVE);
        geometryManager.registerGeometryType(Spline3DM.class, "Spline3DM", TYPES.SPLINE, SUBTYPES.GEOM3DM, TYPES.CURVE);

        // Register surfaces in 3DM
        geometryManager.registerGeometryType(Polygon3DM.class, "Polygon3DM", TYPES.POLYGON, SUBTYPES.GEOM3DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(Polygon3DM.class, "Surface3DM", TYPES.SURFACE, SUBTYPES.GEOM3DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(Ring3DM.class, "Ring3DM", TYPES.RING, SUBTYPES.GEOM3DM, TYPES.SURFACE);
        geometryManager.registerGeometryType(FilledSpline3DM.class, "FilledSpline3DM", TYPES.FILLEDSPLINE, SUBTYPES.GEOM3DM, TYPES.SURFACE);


        // Register multigeometries in 3D
        geometryManager.registerGeometryType(MultiPoint3D.class, "MultiPoint3D", TYPES.MULTIPOINT, SUBTYPES.GEOM3D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(DefaultMultiCurve.class, "MultiCurve", TYPES.MULTICURVE, SUBTYPES.GEOM3D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiLine3D.class, "MultiLine3D", TYPES.MULTILINE, SUBTYPES.GEOM3D, new int[]{TYPES.AGGREGATE, TYPES.MULTICURVE});
        geometryManager.registerGeometryType(DefaultMultiSurface.class, "MultiSurface", TYPES.MULTISURFACE, SUBTYPES.GEOM3D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiPolygon3D.class, "MultiPolygon3D", TYPES.MULTIPOLYGON, SUBTYPES.GEOM3D, new int[]{TYPES.AGGREGATE, TYPES.MULTISURFACE});

        // Register multigeometries in 3DM
        geometryManager.registerGeometryType(MultiPoint3DM.class, "MultiPoint3DM", TYPES.MULTIPOINT, SUBTYPES.GEOM3DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(DefaultMultiCurve.class, "MultiCurve", TYPES.MULTICURVE, SUBTYPES.GEOM3DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiLine3DM.class, "MultiLine3DM", TYPES.MULTILINE, SUBTYPES.GEOM3DM, new int[]{TYPES.AGGREGATE, TYPES.MULTICURVE});
        geometryManager.registerGeometryType(DefaultMultiSurface.class, "MultiSurface", TYPES.MULTISURFACE, SUBTYPES.GEOM3DM, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiPolygon3DM.class, "MultiPolygon3DM", TYPES.MULTIPOLYGON, SUBTYPES.GEOM3DM, new int[]{TYPES.AGGREGATE, TYPES.MULTISURFACE});

        // Register solids
//        geometryManager.registerGeometryType(Solid2DZ.class, "Solid3D", TYPES.SOLID, SUBTYPES.GEOM3D);
//        geometryManager.registerGeometryType(MultiSolid2DZ.class, "MultiSolid3D", TYPES.MULTISOLID, SUBTYPES.GEOM3D,
//            TYPES.AGGREGATE);

        // Persistence
        DefaultEnvelope.registerPersistent();
        Envelope2D.registerPersistent();
        Envelope3D.registerPersistent();

        PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();

        persistenceManager.registerFactory(new GeometryPersistenceFactory());
        persistenceManager.registerFactory(new GeometryTypePersistenceFactory());

        geometryManager.registerGeometryOperation(ToJTS.NAME, new ToJTS());
        geometryManager.registerGeometryOperation(FromJTS.NAME, new FromJTS());
        geometryManager.registerGeometryOperation(ToPostGISEWKB.NAME, new ToPostGISEWKB());
        geometryManager.registerGeometryOperation(FromWKB.NAME, new FromWKB());
        geometryManager.registerGeometryOperation(FromPostGISEWKB.NAME, new FromPostGISEWKB());
        geometryManager.registerGeometryOperation(ToOGCWKB.NAME, new ToOGCWKB());
        geometryManager.registerGeometryOperation(ToWKT.NAME, new ToWKT());
        geometryManager.registerGeometryOperation(FromWKT.NAME, new FromWKT());
    }
}
