/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.ring;

import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geomgraph.Position;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.buffer.OffsetCurveBuilder;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.BaseLine2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;

/**
 * @author fdiaz
 *
 */
public class Ring2D extends BaseLine2D implements Ring {

    /**
     *
     */
    private static final long serialVersionUID = 6640426694359410404L;

    /**
     * @param subtype
     */
    public Ring2D() {
        super(Geometry.TYPES.RING);
    }

    /**
     *
     */
    public Ring2D(Coordinate[] coordinates) {
        super(Geometry.TYPES.RING, coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Ring2D(cloneCoordinates().toCoordinateArray());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2D();
        multiLine.addPrimitive(new Line2D(coordinates.toCoordinateArray()));
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2D();
        multiPolygon.addPrimitive(new Polygon2D(coordinates.toCoordinateArray()));
        return multiPolygon;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        closePrimitive();
        return JTSUtils.createJTSLinearRing(coordinates);
    }


    /* (non-Javadoc)
    * @see org.gvsig.fmap.geom.Geometry#offset(double)
    */
      public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
          com.vividsolutions.jts.geom.LinearRing jtsRing = (LinearRing) getJTS();
          GeometryFactory factory = jtsRing.getFactory();
          BufferParameters bufParams = JTSUtils.getBufferParameters();

          OffsetCurveBuilder ocb = new OffsetCurveBuilder(factory.getPrecisionModel(), bufParams);

          Coordinate[] coordinates = jtsRing.getCoordinates();
          Coordinate[] coords = ocb.getRingCurve(coordinates, Position.LEFT, distance); // .getOffsetCurve(coordinates,

          return new Ring2D(coords);
      }


      /* (non-Javadoc)
       * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#reProject(org.cresques.cts.ICoordTrans)
       */
      @Override
      public void reProject(ICoordTrans ct) {
          super.reProject(ct);
          if (coordinates.size()>=2 && !isClosed()) {
              closePrimitive();
          }
      }
}
