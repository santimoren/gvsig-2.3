/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.periellipse;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.surface.ellipse.BaseEllipse2D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Ellipse;
import org.gvsig.fmap.geom.primitive.PeriEllipse;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public class PeriEllipse2D extends BaseEllipse2D implements PeriEllipse{

    /**
     *
     */
    private static final long serialVersionUID = 3134998820077975586L;

    /**
     * @param subtype
     */
    public PeriEllipse2D() {
        super(Geometry.TYPES.PERIELLIPSE);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        PeriEllipse2D clone = new PeriEllipse2D();
        clone.setPoints((Point)init.cloneGeometry(), (Point)end.cloneGeometry(), ydist);
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        ArrayListCoordinateSequence coordinates = getJTSCoordinates();
        return JTSUtils.createJTSLineString(coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Curve#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point initialPoint, Point endPoint) {
        String message = "Calling deprecated method setPoints of a perimeter of ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        PeriEllipse clonedEllipse = (PeriEllipse) this.cloneGeometry();

        GeometryManager geomManager = GeometryLocator.getGeometryManager();
        Point center = new Point2D((getAxis1Start().getX()+getAxis1End().getX())/2,
            (getAxis1Start().getY()+getAxis1End().getY())/2);
        double axis1Lenght = getAxis1Start().distance(getAxis1End());

        Point clonedAxis1Start = (Point) getAxis1Start().cloneGeometry();
        Point clonedAxis1End = (Point) getAxis1End().cloneGeometry();
        double clonedYDist = this.ydist+distance;

        clonedAxis1Start.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1Start().getX(), axis1Lenght/2+distance));
        clonedAxis1Start.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1Start().getY(), axis1Lenght/2+distance));

        clonedAxis1End.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1End().getX(), axis1Lenght/2+distance));
        clonedAxis1End.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1End().getY(), axis1Lenght/2+distance));

        clonedEllipse.setPoints(clonedAxis1Start, clonedAxis1End, clonedYDist);
        return clonedEllipse;
    }

}
