/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.spline;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.ReadOnlyCoordinates;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;


/**
 * @author fdiaz
 *
 */
public abstract class BaseSpline3D extends AbstractSpline {

    /**
     *
     */
    private static final long serialVersionUID = 6699898811455152758L;

//    /**
//     * @param subtype
//     */
//    public BaseSpline3D() {
//        super(Geometry.SUBTYPES.GEOM3D);
//        this.coordinates = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
//    }

    /**
     * @param polygon
     */
    public BaseSpline3D(int type) {
        super(type, Geometry.SUBTYPES.GEOM3D);
        this.coordinates = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
    }

//    /**
//     * @param subtype
//     * @param coordinates
//     * @param aVertex
//     */
//    public BaseSpline3D(Coordinate[] coordinates) {
//        this();
//        this.coordinates = new ArrayListCoordinateSequence(new ReadOnlyCoordinates(coordinates));
//        if (coordinates.length < 1) {
//            anyVertex = new Point3D(0, 0, 0);
//        } else {
//            anyVertex = new Point3D(coordinates[0].x, coordinates[0].y, coordinates[0].z);
//        }
//    }

    /**
     * @param type
     * @param coordinates
     */
    public BaseSpline3D(int type, Coordinate[] coordinates) {
        super(type, Geometry.SUBTYPES.GEOM3D);
        this.coordinates = new ArrayListCoordinateSequence(new ReadOnlyCoordinates(coordinates));
        if (coordinates.length < 1) {
            anyVertex = new Point3D(0, 0, 0);
        } else {
            anyVertex = new Point3D(coordinates[0].x, coordinates[0].y, coordinates[0].z);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        this.addVertex(new Point3D(x, y, 0));
        }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        this.addVertex(new Point3D(x, y, z));
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.spline.AbstractSpline#fixPoint(org.gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point3D) {
            return point;
        } else if (point instanceof Point3DM) {
            return new Point3D(point.getX(), point.getY(), ((Point3DM) point).getM());
        } else {
            return new Point3D(point.getX(), point.getY(), 0);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.spline.AbstractSpline#getSplineCoordinates()
     */
    @Override
    protected ArrayListCoordinateSequence getSplineCoordinates() {
        ArrayListCoordinateSequence splineCoordinates = new ArrayListCoordinateSequence();

        if (splineCoordinates == null || splineCoordinates.size() == 0) {
            int num = coordinates.size();
            double[] px = new double[num];
            double[] py = new double[num];
            double[] pz = new double[num];
            for (int i = 0; i < num; i++) {
                Coordinate coord = coordinates.get(i);
                px[i] = coord.x;
                py[i] = coord.y;
                pz[i] = coord.z;
            }
            Spline splineX = new Spline(px);
            Spline splineY = new Spline(py);
            Spline splineZ = new Spline(pz);
            splineCoordinates.add(coordinates.get(0));
            for (int i = 0; i < coordinates.size() - 1; i++) {
                for (int t = 1; t <= SUBSEGMENTS; t++) {
                    if ((t == SUBSEGMENTS) && (i == (coordinates.size() - 2))) {
                        // We don't calculate the last point to avoid a possible
                        // error precision with floating point numbers.
                        splineCoordinates.add(new Coordinate(px[px.length - 1], py[px.length - 1], pz[px.length - 1]));
                    } else {
                        double x1 = splineX.fn(i, ((double) t) / SUBSEGMENTS);
                        double y1 = splineY.fn(i, ((double) t) / SUBSEGMENTS);
                        double z1 = splineZ.fn(i, ((double) t) / SUBSEGMENTS);
                        splineCoordinates.add(new Coordinate(x1, y1, z1));
                    }
                }
            }
        }
        return splineCoordinates;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint3D();
        Coordinate[] coordinates = getJTS().getCoordinates();
        multiPoint.ensureCapacity(coordinates.length);
        for (int i = 0; i < coordinates.length; i++) {
            multiPoint.addPoint(new Point3D(coordinates[i]));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3D();
        Line line = new Line3D(getJTS().getCoordinates());
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon3D();
        Polygon polygon = new Polygon3D(getJTS().getCoordinates());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        Point3D vertex = new Point3D(this.coordinates.get(index));
        anyVertex = vertex;
        return vertex;
    }
}
