/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.operation.towkb;

import com.vividsolutions.jts.io.WKBWriter;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.operation.tojts.ToJTS;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

public class ToPostGISEWKB extends GeometryOperation {
	public static final String NAME = GeometryManager.OPERATIONS.TOEWKB;
	public static final int    CODE       =  GeometryLocator.getGeometryManager().getGeometryOperationCode(NAME);
	private static WKBWriter   writer     = null;
	private static int         dimension  = 2;

	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		int subType = geom.getGeometryType().getSubType();
		boolean is3D = subType == 1 || subType == 3;

		if(writer == null) {
			if(is3D) {
				dimension = 3;
				writer = new WKBWriter(3);
			} else {
				dimension = 2;
				writer = new WKBWriter();
			}
		} else {
			if(is3D && dimension == 2) {
				dimension = 3;
				writer = new WKBWriter(3);
			}
			if(!is3D && dimension == 3) {
				dimension = 2;
				writer = new WKBWriter();
			}
		}

		if (ctx == null) {
			return writer.write(((GeometryJTS)geom).getJTS());
		}

        try {
            return writer.write((com.vividsolutions.jts.geom.Geometry) geom.invokeOperation(ToJTS.CODE, ctx));
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryOperationException(e);
        }
	}

	public int getOperationIndex() {
		return CODE;
	}

}
