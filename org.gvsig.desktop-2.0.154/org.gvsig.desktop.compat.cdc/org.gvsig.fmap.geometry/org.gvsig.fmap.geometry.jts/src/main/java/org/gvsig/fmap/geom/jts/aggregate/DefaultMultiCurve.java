/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.vividsolutions.jts.geom.LineString;

import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author fdiaz
 *
 */
public class DefaultMultiCurve extends AbstractMultiCurve {

    /**
     * @param geometryType
     */
    public DefaultMultiCurve(GeometryType geometryType) {
        super(geometryType.getSubType());
    }

    /**
     * @param subtype
     */
    public DefaultMultiCurve(int subtype) {
        super(subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        DefaultMultiCurve clone = new DefaultMultiCurve(getGeometryType());
        for(int i=0; i<primitives.size(); i++){
            clone.addPrimitive((Primitive)primitives.get(i).cloneGeometry());
        }
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        switch (getGeometryType().getSubType()) {
        case Geometry.SUBTYPES.GEOM2D:
            return 2;
        case Geometry.SUBTYPES.GEOM2DM:
            return 3;
        case Geometry.SUBTYPES.GEOM3D:
            return 3;
        case Geometry.SUBTYPES.GEOM3DM:
            return 4;
        default:
            return 0;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multipoint = null;
        for (int i = 0; i < this.getPrimitivesNumber(); i++) {
            if(multipoint==null){
                multipoint = this.getPrimitiveAt(i).toPoints();
            } else {
                MultiPoint points = (MultiPoint)this.getPrimitiveAt(i).toPoints();
                for (int j = 0; j < points.getPrimitivesNumber(); j++) {
                    multipoint.addPrimitive(points.getPrimitiveAt(j));
                }
            }
        }
        return multipoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiline = null;
        for (int i = 0; i < this.getPrimitivesNumber(); i++) {
            if(multiline==null){
                multiline = this.getPrimitiveAt(i).toLines();
            } else {
                MultiLine lines = (MultiLine)this.getPrimitiveAt(i).toLines();
                for (int j = 0; j < lines.getPrimitivesNumber(); j++) {
                    multiline.addPrimitive(lines.getPrimitiveAt(j));
                }
            }
        }
        return multiline;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multipolygon = null;
        for (int i = 0; i < this.getPrimitivesNumber(); i++) {
            if(multipolygon==null){
                multipolygon = this.getPrimitiveAt(i).toPolygons();
            } else {
                MultiPolygon polygons = (MultiPolygon)this.getPrimitiveAt(i).toPolygons();
                for (int j = 0; j < polygons.getPrimitivesNumber(); j++) {
                    multipolygon.addPrimitive(polygons.getPrimitiveAt(j));
                }
            }
        }
        return multipolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        for (int i = 0; i < this.getPrimitivesNumber(); i++) {
            this.getPrimitiveAt(i).flip();
        }
        Collections.reverse(primitives);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.aggregate.AbstractMultiPrimitive#fixPrimitive(org.gvsig.fmap.geom.primitive.Primitive)
     */
    @Override
    protected Geometry fixPrimitive(Primitive primitive) {
        int primitiveSubType = primitive.getGeometryType().getSubType();
        int subType = getGeometryType().getSubType();
        if(primitiveSubType == subType){
            return primitive;
        }

        String message = StringUtils.replace("This MultiCurve only accept subtype %(subtype)s primitives", "%(subtype)s", String.valueOf(subType));
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        List<LineString> lineStrings =
            new ArrayList<LineString>(primitives.size());

        LineString line = null;
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            if (primitive instanceof Line) {
                line = (LineString) ((GeometryJTS) primitive).getJTS();
                lineStrings.add(line);
            } else {
                MultiLine multiline = null;
                try {
                    multiline = primitive.toLines();
                    for (int j = 0; j < multiline.getPrimitivesNumber(); j++) {
                        line =
                            (LineString) ((GeometryJTS) multiline.getPrimitiveAt(j)).getJTS();
                        lineStrings.add(line);
                    }
                } catch (GeometryException e) {
                    GeometryType geomType = primitive.getGeometryType();
                    logger.warn(StringUtils.replaceEach(
                        "Can't convert primitive type=%(type)s, %(subtype)s to MultiLine",
                        new String[] {"%(type)s", "%(subtype)s" },
                        new String[] { String.valueOf(geomType.getType()), String.valueOf(geomType.getSubType()) }));
                }
            }
        }
        return JTSUtils.createJTSMultiLineString(lineStrings.toArray(new LineString[lineStrings.size()]));
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        MultiCurve result = new DefaultMultiCurve(getGeometryType());
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            Geometry offset = primitive.offset(distance);
            if(offset instanceof MultiPrimitive){
                MultiPrimitive multiOffset = (MultiPrimitive)offset;
                for(int i=0; i<multiOffset.getPrimitivesNumber(); i++){
                    result.addPrimitive(multiOffset.getPrimitiveAt(i));
                }
            } else {
                result.addPrimitive((Primitive)primitive.offset(distance));
            }
        }
        return result;
    }
}
