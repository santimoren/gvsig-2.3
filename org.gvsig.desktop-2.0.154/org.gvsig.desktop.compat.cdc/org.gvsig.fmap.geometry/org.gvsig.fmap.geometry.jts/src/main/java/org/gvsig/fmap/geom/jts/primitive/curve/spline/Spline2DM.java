/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.spline;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point2DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2DM;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.OpenJUMPUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Spline;


/**
 * @author fdiaz
 *
 */
public class Spline2DM extends BaseSpline2DM implements Spline {

    /**
     *
     */
    private static final long serialVersionUID = 3275115053567971975L;

    /**
     * @param subtype
     */
    public Spline2DM() {
        super(Geometry.TYPES.SPLINE);
    }

    /**
    *
    */
    public Spline2DM(Coordinate[] coordinates) {
        super(Geometry.TYPES.SPLINE, coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        this.addVertex(new Point2DM(x, y, 0));
        }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Can't add x,y,z coordinate to Polygon2DM.";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Spline2DM(cloneCoordinates().toCoordinateArray());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.spline.AbstractSpline#fixPoint(org.gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point2DM) {
            return point;
        } else {
            return new Point2DM(point.getX(), point.getY(), 0);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.spline.AbstractSpline#getSplineCoordinates()
     */
    @Override
    protected ArrayListCoordinateSequence getSplineCoordinates() {
        ArrayListCoordinateSequence splineCoordinates = new ArrayListCoordinateSequence();

        if (splineCoordinates == null || splineCoordinates.size() == 0) {
            int num = coordinates.size();
            double[] px = new double[num];
            double[] py = new double[num];
            double[] pm = new double[num];
            for (int i = 0; i < num; i++) {
                Coordinate coord = coordinates.get(i);
                px[i] = coord.x;
                py[i] = coord.y;
                pm[i] = coord.getOrdinate(CoordinateSequence.M);
            }
            Spline splineX = new Spline(px);
            Spline splineY = new Spline(py);
            Spline splineM = new Spline(pm);
            splineCoordinates.add(coordinates.get(0));
            for (int i = 0; i < coordinates.size() - 1; i++) {
                for (int t = 1; t <= SUBSEGMENTS; t++) {
                    if ((t == SUBSEGMENTS) && (i == (coordinates.size() - 2))) {
                        // We don't calculate the last point to avoid a possible
                        // error precision with floating point numbers.
                        splineCoordinates.add(JTSUtils.createMCoordinate(px[px.length - 1], py[px.length - 1], pm[px.length - 1]));
                    } else {
                        double x1 = splineX.fn(i, ((double) t) / SUBSEGMENTS);
                        double y1 = splineY.fn(i, ((double) t) / SUBSEGMENTS);
                        double m1 = splineM.fn(i, ((double) t) / SUBSEGMENTS);
                        splineCoordinates.add(JTSUtils.createMCoordinate(x1, y1, m1));
                    }
                }
            }
        }
        return splineCoordinates;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint2DM();
        Coordinate[] coordinates = getJTS().getCoordinates();
        multiPoint.ensureCapacity(coordinates.length);
        for (int i = 0; i < coordinates.length; i++) {
            multiPoint.addPoint(new Point2DM(coordinates[i]));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2DM();
        Line line = new Line2DM(getJTS().getCoordinates());
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2DM();
        Polygon polygon = new Polygon2DM(getJTS().getCoordinates());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        Point2DM vertex = new Point2DM(this.coordinates.get(index));
        anyVertex = vertex;
        return vertex;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        Coordinate[] coords = getJTS().getCoordinates();
        ArrayListCoordinateSequence listCoordSequence = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
        for (int i = 0; i < coords.length; i++) {
            listCoordSequence.add(coords[i]);
        }
        if (isClosed()) {
            return JTSUtils.offsetClosedLine(listCoordSequence, distance);
        } else {
            return OpenJUMPUtils.offsetCleanOpenLine(listCoordSequence, distance);
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = super.equals(obj);
        if(res && obj instanceof Spline2DM){
            Spline2DM other = (Spline2DM)obj;
            if(this.getNumVertices() != other.getNumVertices()){
                return false;
            }
            for(int i=0; i < this.getNumVertices(); i++){
                if(other.coordinates.get(i).getOrdinate(2)!=this.coordinates.get(i).getOrdinate(2)){
                    return false;
                };
            }
            return true;
        } else {
            return false;
        }
    }
}
