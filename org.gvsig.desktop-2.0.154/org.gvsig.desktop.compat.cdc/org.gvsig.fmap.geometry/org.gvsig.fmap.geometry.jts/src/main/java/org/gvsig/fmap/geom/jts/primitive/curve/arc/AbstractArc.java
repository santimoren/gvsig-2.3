/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.arc;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.cresques.cts.CoordTransRuntimeException;
import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.curve.AbstractCurve;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.util.UtilFunctions;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Arc;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractArc extends AbstractCurve implements Arc {

    /**
     *
     */
    private static final long serialVersionUID = 454301669807892457L;

    /**
     * @param type
     * @param subtype
     */
    protected AbstractArc(int subtype) {
        super(Geometry.TYPES.ARC, subtype);
    }

    protected Point init;

    /**
     * This is the middle point (belongs to the arc), not the center
     * of the circle/ellipse
     */
    protected Point middle;
    protected Point end;


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Curve#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point initialPoint, Point endPoint) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int, int)
     */
    public double getCoordinateAt(int index, int dimension) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int, int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        String message = "Calling deprecated method setPoints of a arc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        try {
        init.reProject(ct);
        middle.reProject(ct);
        end.reProject(ct);
        } catch (CoordTransRuntimeException e){
            //Si ha fallado la reproyecci�n de alguno de los puntos, ponemos todas las coordenadas a 0
            init.setX(0);
            init.setY(0);
            middle.setX(0);
            middle.setY(0);
            end.setX(0);
            end.setY(0);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        init.transform(at);
        middle.transform(at);
        end.transform(at);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return init.getDimension();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return ((PointJTS)init).is3D();
    }

    /**
     * @param initialPoint
     * @return
     */
    protected abstract Point fixPoint(Point point);

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point startPoint, Point midPoint, Point endPoint) {
        init = fixPoint(startPoint);
        middle = fixPoint(midPoint);
        end = fixPoint(endPoint);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getInitPoint()
     */
    public Point getInitPoint() {
        return init;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getEndPoint()
     */
    public Point getEndPoint() {
        return end;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getMiddlePoint()
     */
    public Point getMiddlePoint() {
        return middle;
    }

    /**
     * Leaves the angle between PI and -PI
     * @param angle (radians)
     * @return
     */
    protected double normalizeAngle(double angle) {
        if (angle > -Math.PI && angle <= Math.PI) {
            return angle;
        }

        if (angle == Double.NEGATIVE_INFINITY || angle == Double.POSITIVE_INFINITY) {
            return 0;
        }

        double abs_ang = Math.abs(angle);
        double remove = Math.floor(abs_ang / (2 * Math.PI));
        remove = remove * 2 * Math.PI;
        double resp = 0;

        if (angle > 0) {
            resp = angle - remove;
            if (resp > Math.PI) {
                // final adjustment
                resp = resp - 2 * Math.PI;
            }
        } else {
            resp = angle + remove;
            if (resp <= -Math.PI) {
                // final adjustment
                resp = resp + 2 * Math.PI;
            }
        }

        return resp;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPointsStartExt(org.gvsig.fmap.geom.primitive.Point, double, double, double)
     */
    public void setPointsStartExt(Point center, double radius, double startAngle, double angleExt) {
        setPoints(center, radius, startAngle, angleExt);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPointsStartEnd(org.gvsig.fmap.geom.primitive.Point, double, double, double)
     */
    public void setPointsStartEnd(Point center, double radius, double startAngle, double endAngle) {

        if (startAngle == endAngle) {
            setPointsStartExt(center, radius, startAngle, 0);
        } else {

            /*
             * Normalize then force clockwise:
             */
            double norm_start = normalizeAngle(startAngle);
            double norm_end = normalizeAngle(endAngle);
            double ang_ext = 0;

            // clockwise
            // ang_ext must be positive
            if (norm_start >= norm_end) {
                ang_ext = norm_start - norm_end;
            } else {
                ang_ext = 2 * Math.PI - (norm_end - norm_start);
            }
            setPointsStartExt(center, radius, startAngle, ang_ext);

            // finally call other method with ang_ext
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {

       GeneralPathX gp = new DefaultGeneralPathX(getPathIterator(null, getManager().getFlatness()), is3D(), 0.0);
        return gp;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        return getPathIterator(at, getManager().getFlatness());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {

        java.awt.geom.Point2D.Double p1 = new java.awt.geom.Point2D.Double(init.getX(), init.getY());
        java.awt.geom.Point2D.Double p2 = new java.awt.geom.Point2D.Double(middle.getX(), middle.getY());
        java.awt.geom.Point2D.Double p3 = new java.awt.geom.Point2D.Double(end.getX(), end.getY());

        java.awt.geom.Arc2D arco = UtilFunctions.createArc(p1, p2, p3);
        if (arco == null) {
            logger.info("Did not set arc points (probably aligned points): " + p1.getX() + " " + p1.getY() + " :: "
                + p2.getX() + " " + p2.getY() + " :: " + p3.getX() + " " + p3.getY());
            throw new IllegalArgumentException("Did not set arc points (probably aligned points).");
        }

        return arco.getPathIterator(at, flatness);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        Point aux = init;
        init = end;
        end = aux;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getStartAngle()
     */
    public double getStartAngle() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getAngle(getCenterPoint(), getInitPoint());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getEndAngle()
     */
    public double getEndAngle() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getAngle(getCenterPoint(), getEndPoint());
    }

    private double getAngle(Point start, Point end) throws GeometryOperationNotSupportedException, GeometryOperationException {
        double angle = Math.acos((end.getX() - start.getX()) / start.distance(end));

        if (start.getY() > end.getY()) {
            angle = -angle;
        }

        if (angle < 0) {
            angle += (2 * Math.PI);
        }

        return angle;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        // TODO Auto-generated method stub
        Point center = getCenterPoint();
        double radius = center.distance(init);
        double scale = (radius+distance)/radius;
        AffineTransform at = getScaleAffineTransform(center, scale);
        Geometry cloned = this.cloneGeometry();
        cloned.transform(at);
        return cloned;
    }

    protected AffineTransform getScaleAffineTransform(Point center, Double scale)
        throws GeometryOperationNotSupportedException,
        GeometryOperationException {

        AffineTransform translate =
            AffineTransform
                .getTranslateInstance(-center.getX(), -center.getY());

        AffineTransform scaleTransform = AffineTransform.getScaleInstance(scale,scale);

        AffineTransform inverseTranslate =
            AffineTransform.getTranslateInstance(center.getX(), center.getY());
        AffineTransform at = new AffineTransform(translate);

        at.preConcatenate(scaleTransform);
        at.preConcatenate(inverseTranslate);
        return at;
    }
}
