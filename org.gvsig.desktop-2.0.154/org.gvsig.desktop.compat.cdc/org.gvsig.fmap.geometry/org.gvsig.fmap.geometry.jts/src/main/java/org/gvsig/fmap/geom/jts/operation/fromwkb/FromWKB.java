/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.jts.operation.fromwkb;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;


/**
 * 
 *
 * @author Vicente Caballero Navarro
 */
public class FromWKB extends GeometryOperation {
    public static final String NAME = GeometryManager.OPERATIONS.FROMWKB;
    
	public static final int CODE = GeometryLocator.getGeometryManager().
    	getGeometryOperationCode(NAME);
	private static PostGISEWKBParser wkbParser = new PostGISEWKBParser();
   
	/* 
	 * Como no tenemos un parser de OGC-WKB que es distinto del PostGIS-EWKB
	 * de momento, usamos el de PostGIS-EWKB
	 * 
	 * La especificación del OGC-WKB se puede encontrar en la wikipedia:
	 * https://en.wikipedia.org/wiki/Well-known_text#Well-known_binary
	 * 
	 * Podemos asumir que si el 5º byte del WKB es distinto de 0 se trata de PostGIS-EWKB
	*/
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
        throws GeometryOperationException {
        byte[] data = (byte[])ctx.getAttribute("data");

        try {
			return geom = wkbParser.parse(data);
		} catch (CreateGeometryException e) {
			return new GeometryOperationException(e);
		}
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#getOperationIndex()
     */
    public int getOperationIndex() {
        return CODE;
    }
}
