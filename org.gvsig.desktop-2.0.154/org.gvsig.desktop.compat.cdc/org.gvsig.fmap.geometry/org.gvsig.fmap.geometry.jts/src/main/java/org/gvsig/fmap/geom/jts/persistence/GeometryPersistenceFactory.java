/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.persistence;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;


/**
 * Factory to manage the persistence of {@link Geometry} objects.
 *
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class GeometryPersistenceFactory extends AbstractSinglePersistenceFactory {
    private static final String FIELD_STRING = "string";
    private static final String FIELD_SRS = "srs";

    private static final String DYNCLASS_NAME = "Geometry";
    private static final String DYNCLASS_DESCRIPTION = "gvSIG Geometry";

    private static final String OPERATION_FIELD_NAME = "";

    private static final GeometryManager geometryManager = GeometryLocator.getGeometryManager();

    /**
     * @param theClass
     * @param name
     * @param description
     * @param domainName
     * @param domainUrl
     */
    public GeometryPersistenceFactory() {
        super(Geometry.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null, null);

        DynStruct definition = this.getDefinition();

        definition.addDynFieldString(FIELD_STRING).setMandatory(true);
        definition.addDynFieldString(FIELD_SRS).setMandatory(false);
    }

    public Object createFromState(PersistentState state)
    throws PersistenceException {
        String string = state.getString(FIELD_STRING);
        String srs = state.getString(FIELD_SRS);

        try {
            return geometryManager.createFrom(string, srs);
        } catch (CreateGeometryException e) {
           throw new PersistenceException("Error creating the geometry", e);
        } catch (GeometryException e) {
            throw new PersistenceException("Error creating the geometry", e);
        }
    }

    public void saveToState(PersistentState state, Object obj)
    throws PersistenceException {
        try {
            state.set(FIELD_STRING, ((Geometry)obj).convertToWKT());
        } catch (GeometryOperationNotSupportedException e) {
            throw new PersistenceException("Error converting the geometry", e);
        } catch (GeometryOperationException e) {
            throw new PersistenceException("Error converting the geometry", e);
        }
    }
}
