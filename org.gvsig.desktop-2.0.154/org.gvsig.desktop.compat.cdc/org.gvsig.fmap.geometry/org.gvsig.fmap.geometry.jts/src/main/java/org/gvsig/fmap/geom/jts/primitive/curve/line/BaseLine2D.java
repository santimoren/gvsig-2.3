/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.line;

import java.util.ArrayList;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.ReadOnlyCoordinates;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * @author fdiaz
 *
 */
public abstract class BaseLine2D extends AbstractLine {

    /**
     *
     */
    private static final long serialVersionUID = -4703049397260763325L;

    /**
     * @param polygon
     */
    public BaseLine2D(int type) {
        super(type, Geometry.SUBTYPES.GEOM2D);
        this.coordinates = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
    }

    /**
     * @param polygon
     * @param coordinates
     */
    public BaseLine2D(int type, Coordinate[] coordinates) {
        super(type, Geometry.SUBTYPES.GEOM2D);
        initializeCoordinates(coordinates);
    }

    /**
     * @param coordinates
     */
    private void initializeCoordinates(Coordinate[] coordinates) {
        this.coordinates = new ArrayListCoordinateSequence(new ReadOnlyCoordinates(coordinates));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double,
     * double)
     */
    public void addVertex(double x, double y) {
        this.addVertex(new Point2D(x, y));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double,
     * double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Can't add x,y,z coordinate to Line2D.";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#fixPoint(org
     * .gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point2D) {
            return point;
        } else {
            return new Point2D(point.getX(), point.getY());
        }
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint2D();
        multiPoint.ensureCapacity(coordinates.size());
        for (Iterator<Coordinate> iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();
            multiPoint.addPoint(new Point2D(coordinate));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2D();
        multiLine.addPrimitive(this);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2D();
        Polygon2D polygon = new Polygon2D(coordinates.toCoordinateArray());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        Point2D vertex = new Point2D(this.coordinates.get(index));
        return vertex;
    }
}
