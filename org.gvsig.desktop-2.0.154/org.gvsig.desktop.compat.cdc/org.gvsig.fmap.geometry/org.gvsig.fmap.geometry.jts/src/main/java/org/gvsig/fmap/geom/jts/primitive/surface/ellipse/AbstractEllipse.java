/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.ellipse;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.cresques.cts.CoordTransRuntimeException;
import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.AbstractSurface;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.UtilFunctions;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Ellipse;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.tools.exception.BaseException;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractEllipse extends AbstractSurface {

    /**
     *
     */
    private static final long serialVersionUID = -4218931408628745830L;

    protected Point init;
    protected Point end;
    protected double ydist;

    /**
     * @param type
     * @param subtype
     */
    protected AbstractEllipse(int type, int subtype) {
        super(type, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getNumInteriorRings()
     */
    public int getNumInteriorRings() {
        String message = "Calling deprecated method getInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getInteriorRing(int)
     */
    public Ring getInteriorRing(int index) {
        String message = "Calling deprecated method getInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Ring)
     */
    public void addInteriorRing(Ring ring) {
        String message = "Calling unsupported method addInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Line)
     */
    public void addInteriorRing(Line ring) {

        String message = "Calling unsupported method addInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Polygon)
     */
    public void addInteriorRing(Polygon polygon) {
        String message = "Calling unsupported method addInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#removeInteriorRing(int)
     */
    public void removeInteriorRing(int index) {
        String message = "Calling unsupported method removeInteriorRing of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int, int)
     */
    public double getCoordinateAt(int index, int dimension) {
        String message = "Calling deprecated method getCoordinateAt of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int, int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        String message = "Calling deprecated method setCoordinateAt of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        String message = "Calling deprecated method addVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        String message = "Calling deprecated method addVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Calling deprecated method addVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        String message = "Calling deprecated method removeVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        String message = "Calling deprecated method getVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        String message = "Calling deprecated method getNumVertices of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        String message = "Calling deprecated method insertVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        String message = "Calling deprecated method setVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {
        String message = "Calling deprecated method setGeneralPath of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        String message = "Calling deprecated method addMoveToVertex of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        String message = "Calling deprecated method closePrimitive of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        String message = "Calling deprecated method ensureCapacity of a ellipse";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        // FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n,
        // rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(JTSUtils.getPointAtYAxisInEllipse(init, end, ydist));
        try {
            init.reProject(ct);
            end.reProject(ct);
            aux.reProject(ct);
        } catch (CoordTransRuntimeException e) {
            //Si no se ha podido reproyectar alguno de los puntos, les asignamos 0 a todas las coordenadas
            init.setX(0);
            init.setY(0);
            end.setX(0);
            end.setY(0);
            ydist = 0;
        }
        try {
            Point2D transformedMiddlePoint = new Point2D(JTSUtils.getMidPoint(init, end));
            ydist = transformedMiddlePoint.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the radius of the transformed circle.", e);
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(JTSUtils.getPointAtYAxisInEllipse(init, end, ydist));
        init.transform(at);
        end.transform(at);
        aux.transform(at);
        try {
            Point2D transformedMiddlePoint = new Point2D(JTSUtils.getMidPoint(init, end));
            ydist = transformedMiddlePoint.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the minor semi-axis of the transformed ellipse.", e);
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return init.getDimension();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        return getPathIterator(at, getManager().getFlatness());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {

        java.awt.geom.Point2D.Double p1 = new java.awt.geom.Point2D.Double(init.getX(), init.getY());
        java.awt.geom.Point2D.Double p2 = new java.awt.geom.Point2D.Double(end.getX(), end.getY());

        java.awt.Shape ellipse = UtilFunctions.createEllipse(p1, p2, ydist);

        return ellipse.getPathIterator(at, flatness);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {

        GeneralPathX gp = new DefaultGeneralPathX(getPathIterator(null, getManager().getFlatness()), is3D(), 0.0);
         return gp;
     }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return ((PointJTS)init).is3D();
    }


    /**
     * @param initialPoint
     * @return
     */
    protected abstract Point fixPoint(Point point);


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point, double)
     */
    public void setPoints(Point axis1Start, Point axis1End, double axis2Length) {
        init = fixPoint(axis1Start);
        end = fixPoint(axis1End);
        ydist = axis2Length;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis1Start()
     */
    public Point getAxis1Start() {
        return init;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis1End()
     */
    public Point getAxis1End() {
        return end;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis2Dist()
     */
    public double getAxis2Dist() {
        return ydist;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        //FIXME: throw UnssupportedOperationException or do nothing?
//        String message = "Can't flip a ellipse";
//        notifyDeprecated(message);
//        throw new UnsupportedOperationException(message);
    }

    protected double getAxis1Angle() throws GeometryOperationNotSupportedException, GeometryOperationException {
        double angle = Math.acos((end.getX() - init.getX()) / init.distance(end));

        if (init.getY() > end.getY()) {
            angle = -angle;
        }

        if (angle < 0) {
            angle += (2 * Math.PI);
        }
        return angle;
    }

}
