/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.util;

import java.util.ArrayList;
import java.util.Collection;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Envelope;

import org.gvsig.fmap.geom.jts.MCoordinate;

/**
 * @author fdiaz
 *
 */
public class ArrayListCoordinateSequence extends ArrayList<com.vividsolutions.jts.geom.Coordinate> implements com.vividsolutions.jts.geom.CoordinateSequence {


    /**
     *
     */
    private static final long serialVersionUID = -6406449425908120065L;

    /**
     *
     */
    public ArrayListCoordinateSequence(Collection<com.vividsolutions.jts.geom.Coordinate> coordinates) {
        super(coordinates);
    }

    /**
    *
    */
   public ArrayListCoordinateSequence() {
       super();
   }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getDimension()
     */
    public int getDimension() {

        // FIXME: �Esto es correcto?????

        com.vividsolutions.jts.geom.Coordinate c = get(0);
        if(c instanceof MCoordinate){
            if(Double.isNaN(c.z)){
                return 3;
            } else {
                return 4;
            }
        } else {
            if(Double.isNaN(c.z)){
                return 2;
            } else {
                return 3;
            }
        }
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinate(int)
     */
    public com.vividsolutions.jts.geom.Coordinate getCoordinate(int i) {
        return (com.vividsolutions.jts.geom.Coordinate) get(i);
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinateCopy(int)
     */
    public com.vividsolutions.jts.geom.Coordinate getCoordinateCopy(int i) {
        return (com.vividsolutions.jts.geom.Coordinate) ((com.vividsolutions.jts.geom.Coordinate)get(i)).clone();
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinate(int, com.vividsolutions.jts.geom.Coordinate)
     */
    public void getCoordinate(int index, com.vividsolutions.jts.geom.Coordinate coord) {
        com.vividsolutions.jts.geom.Coordinate c = ((com.vividsolutions.jts.geom.Coordinate) get(index));
        coord.x = c.x;
        coord.y = c.y;
        coord.z = c.z;
        if(coord instanceof MCoordinate)  {
            if (c instanceof MCoordinate){
                ((MCoordinate)coord).m = ((MCoordinate)c).m;
            } else {
                ((MCoordinate)coord).m = Double.NaN;
            }
        }
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getX(int)
     */
    public double getX(int index) {
        return ((com.vividsolutions.jts.geom.Coordinate) get(index)).x;
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getY(int)
     */
    public double getY(int index) {
        return ((com.vividsolutions.jts.geom.Coordinate) get(index)).y;
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#getOrdinate(int, int)
     */
    public double getOrdinate(int index, int ordinateIndex) {
       com.vividsolutions.jts.geom.Coordinate c = ((com.vividsolutions.jts.geom.Coordinate) get(index));
       switch (ordinateIndex) {
          case CoordinateSequence.X:  return c.x;
          case CoordinateSequence.Y:  return c.y;
          case CoordinateSequence.Z:  return c.z;
          case CoordinateSequence.M:
              if(c instanceof MCoordinate)  {
                  return ((MCoordinate)c).m;
              }
        }
        return Double.NaN;
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#setOrdinate(int, int, double)
     */
    public void setOrdinate(int index, int ordinateIndex, double value) {
        ((com.vividsolutions.jts.geom.Coordinate) get(index)).setOrdinate(ordinateIndex, value);
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#toCoordinateArray()
     */
    public com.vividsolutions.jts.geom.Coordinate[] toCoordinateArray() {
        return ((com.vividsolutions.jts.geom.Coordinate[])(this.toArray(new Coordinate[this.size()])));
    }

    /* (non-Javadoc)
     * @see com.vividsolutions.jts.geom.CoordinateSequence#expandEnvelope(com.vividsolutions.jts.geom.Envelope)
     */
    public Envelope expandEnvelope(Envelope env) {
        for (int i = 0; i < this.size(); i++) {
            env.expandToInclude((com.vividsolutions.jts.geom.Coordinate) get(i));
        }
        return env;
    }

    @Override
    public Object clone() {
        ArrayListCoordinateSequence cloned = new ArrayListCoordinateSequence();
        for(com.vividsolutions.jts.geom.Coordinate item: this) {
            cloned.add((com.vividsolutions.jts.geom.Coordinate)item.clone());
        }
        return cloned;
    }
}
