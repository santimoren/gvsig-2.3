/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.polygon;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;

/**
 * @author  fdiaz
 */
public class PolygonIterator extends GeneralPathXIterator{

    /** Transform applied on the coordinates during iteration */
    private AffineTransform at;

    /** True when the point has been read once */
    private boolean done;
    private int index = 0;
    private Polygon polygon;

    /**
     * Creates a new PointIterator object.
     *
     * @param p
     *            The polygon
     * @param at
     *            The affine transform applied to coordinates during
     *            iteration
     */
    public PolygonIterator(Polygon polygon, AffineTransform at) {
        super(new GeneralPathX());
        if (at == null) {
            at = new AffineTransform();
        }
        this.polygon = polygon;
        this.at = at;
        done = false;
    }

    /**
     * Return the winding rule for determining the interior of the path.
     *
     * @return <code>WIND_EVEN_ODD</code> by default.
     */
    public int getWindingRule() {
        return PathIterator.WIND_EVEN_ODD;
    }

    /**
     * @see java.awt.geom.PathIterator#next()
     */
    public void next() {

        int numVertices = this.polygon.getNumVertices();
        for (int i = 0; i < this.polygon.getNumInteriorRings(); i++) {
            numVertices += this.polygon.getInteriorRing(i).getNumVertices();
        }
        done = (numVertices == ++index);
    }

    /**
     * @see java.awt.geom.PathIterator#isDone()
     */
    public boolean isDone() {
        return done;
    }

    /**
     * @see java.awt.geom.PathIterator#currentSegment(double[])
     */
    public int currentSegment(double[] coords) {
        if (index<this.polygon.getNumVertices()){
            coords[0] = this.polygon.getCoordinateAt(index, 0);
            coords[1] = this.polygon.getCoordinateAt(index, 1);
            at.transform(coords, 0, coords, 0, 1);
            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        } else {
            int previousRingsSize = 0;
            for(int r=0; r<this.polygon.getNumInteriorRings(); r++){
                Ring ring = this.polygon.getInteriorRing(r);
                if (index < this.polygon.getNumVertices()+previousRingsSize+ring.getNumVertices()){
                    int ringIndex = index-(this.polygon.getNumVertices()+previousRingsSize);
                    coords[0] = ring.getCoordinateAt(ringIndex, 0);
                    coords[1] = ring.getCoordinateAt(ringIndex, 1);
                    at.transform(coords, 0, coords, 0, 1);
                    if (ringIndex == 0) {
                        return PathIterator.SEG_MOVETO;
                    } else {
                        return PathIterator.SEG_LINETO;
                    }
                } else {
                    previousRingsSize += ring.getNumVertices();
                }
            }
            return PathIterator.SEG_CLOSE; //FIXME: ??????
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.geom.PathIterator#currentSegment(float[])
     */
    public int currentSegment(float[] coords) {
        if (index<this.polygon.getNumVertices()){
            coords[0] = (float)this.polygon.getCoordinateAt(index, 0);
            coords[1] = (float)this.polygon.getCoordinateAt(index, 1);
            at.transform(coords, 0, coords, 0, 1);
            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        } else {
            int previousRingsSize = 0;
            for(int r=0; r<this.polygon.getNumInteriorRings(); r++){
                Ring ring = this.polygon.getInteriorRing(r);
                if (index < this.polygon.getNumVertices()+previousRingsSize+ring.getNumVertices()){
                    int ringIndex = index-(this.polygon.getNumVertices()+previousRingsSize);
                    coords[0] = (float)ring.getCoordinateAt(index, 0);
                    coords[1] = (float)ring.getCoordinateAt(index, 1);
                    at.transform(coords, 0, coords, 0, 1);
                    if (ringIndex == 0) {
                        return PathIterator.SEG_MOVETO;
                    } else {
                        return PathIterator.SEG_LINETO;
                    }
                } else {
                    previousRingsSize += ring.getNumVertices();
                }
            }
            return PathIterator.SEG_CLOSE; //FIXME: ??????
        }
    }
}