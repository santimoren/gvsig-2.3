/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.spatialindex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.SpatialIndexFactory;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dataTypes.DataTypesManager.Coercion;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.Manager;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractSpatialIndex implements SpatialIndex {
    private static Logger logger = LoggerFactory.getLogger(AbstractSpatialIndex.class);
    
    private SpatialIndexFactory factory = null;
    private GeometryManager manager = null;
    private DynObject parameters = null;
    private Coercion coercion = null;

    public AbstractSpatialIndex(GeometryManager geometryManager, SpatialIndexFactory factory, DynObject parameters) {
        this.factory = factory;
        this.manager = geometryManager;
        this.parameters = parameters;
    }
    
    public Manager getManager() {
        return this.manager;
    }
            
    public SpatialIndexFactory getFactory() {
        return this.factory;
    }

    public DynObject getParameters() {
        return this.parameters;
    }
    
    public Object getParameter(String name) {
        return this.parameters.getDynValue(name);
    }
    
    public void query(Geometry geom, Visitor visitor) {
        this.query(geom.getEnvelope(), visitor);
    }

    public Iterator query(Geometry geom) {
        return this.query(geom.getEnvelope());
    }

    public Iterator query(org.gvsig.fmap.geom.primitive.Envelope envelope) {
        return this.query(envelope, 0);
    }

    public Iterator query(Geometry geom, long limit) {
        return this.query(geom.getEnvelope(), limit);
    }

    public Iterator queryNearest(Envelope envelope) {
        return this.queryNearest(envelope, 0);
    }

    public Iterator queryNearest(Geometry geom) {
        return this.queryNearest(geom.getEnvelope(), 0);
    }

    public Iterator queryNearest(Geometry geom, long limit) {
        return this.queryNearest(geom.getEnvelope(), limit);
    }

    public void insert(Geometry geom, Object data) {
        this.insert(geom.getEnvelope(), data);
    }

    public void insert(Geometry geom) {
        this.insert(geom.getEnvelope(), geom);
    }

    public boolean remove(Geometry geom) {
        return this.remove(geom.getEnvelope(), geom);
    }

    public boolean remove(Geometry geom, Object data) {
        return this.remove(geom.getEnvelope(), data);
    }

    public List queryAsList(Envelope envelope) {
        return asList(query(envelope));
    }

    public List queryAsList(Geometry geom) {
        return asList(query(geom));
    }

    public List queryAllAsList() {
        return asList(queryAll());
    }

    protected List asList(Iterator it) {
        List l = new ArrayList();
        while (it.hasNext()) {
            l.add(it.next());
        }
        return l;
    }
    
    protected Object coerceData(Object data) {
        if( data == null ) {
            return data;
        }
        
        if( this.coercion == null ) {
            int dataType = this.getFactory().getDataTypeSupported();
            DataTypesManager typeManager = ToolsLocator.getDataTypesManager();
            this.coercion = typeManager.getCoercion(dataType);
        }
        try {
            return this.coercion.coerce(data);
        } catch (CoercionException ex) {
            logger.warn("Can't coerce data to the index date type.",ex);
            throw new IllegalArgumentException("Can't coerce data to the index date type.",ex);
        }
    }

    public void flush() {
        // Do nothing, overwrite in each implementation if need
    }

    
}
