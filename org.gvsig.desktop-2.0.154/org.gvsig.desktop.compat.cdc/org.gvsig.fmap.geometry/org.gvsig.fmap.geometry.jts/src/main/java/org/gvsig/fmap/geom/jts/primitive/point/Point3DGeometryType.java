/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.point;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.jts.DefaultGeometryType;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.PointGeometryType;

/**
 * Geometry type implementation for the creation of {@link Point3D} objects.
 *
 * @author gvSIG Team
 */
public class Point3DGeometryType extends DefaultGeometryType implements PointGeometryType {


    private static final String NAME = "Point3D";
    private static final int TYPE = Geometry.TYPES.POINT;
    private static final int SUBTYPE = Geometry.SUBTYPES.GEOM3D;

    /**
     * @param geomClass
     * @param name
     * @param type
     * @param subType
     */
    public Point3DGeometryType() {
        super(Point3D.class, NAME, TYPE, SUBTYPE);
    }
    public String getName() {
        return NAME;
    }

    public int getSubType() {
        return Geometry.SUBTYPES.GEOM3D;
    }

    public boolean isSubTypeOf(int geometrySubType) {
        return SUBTYPE == geometrySubType
            || super.isSubTypeOf(geometrySubType);
    }

    public Geometry create() throws CreateGeometryException {
        return new Point3D();
    }

    public Point createPoint(double x, double y) {
        return new Point3D(x, y, 0.0d);
    }

    public Point createPoint(double[] coordinates) {
        return new Point3D(coordinates[0], coordinates[1], coordinates[2]);
    }

}
