/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.line;

import java.util.ArrayList;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.MCoordinate;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.ReadOnlyCoordinates;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public abstract class BaseLine3DM extends AbstractLine {

    /**
     *
     */
    private static final long serialVersionUID = 1765763899814153661L;

    /**
     * @param polygon
     * @param coordinates
     */
    public BaseLine3DM(int type, Coordinate[] coordinates) {
        super(type, Geometry.SUBTYPES.GEOM3DM);
        initializeCoordinates(coordinates);
    }

    /**
     * @param coordinates
     */
    private void initializeCoordinates(Coordinate[] coordinates) {
        this.coordinates = new ArrayListCoordinateSequence(new ReadOnlyCoordinates(coordinates));
    }

    /**
     * @param polygon
     */
    public BaseLine3DM(int type) {
        super(type, Geometry.SUBTYPES.GEOM3DM);
        this.coordinates = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        this.addVertex(x, y, 0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        Point3DM point = new Point3DM(x, y, z, 0);
        this.addVertex(point);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#fixPoint(org.gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point3DM) {
            return point;
        } else if (point instanceof Point3D) {
            return new Point3DM(point.getX(), point.getY(), ((Point3D)point).getZ(), 0);
        } else {
            return new Point3DM(point.getX(), point.getY(), 0, 0);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint3DM();
        multiPoint.ensureCapacity(coordinates.size());
        for (Iterator<Coordinate> iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();
            multiPoint.addPoint(new Point3DM(coordinate));
        }
        return multiPoint;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3DM();
        multiLine.addPrimitive(this);
        return multiLine;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon3DM();
        Polygon3DM polygon = new Polygon3DM(coordinates.toCoordinateArray());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        Point3DM vertex = new Point3DM(this.coordinates.get(index));
        return vertex;
    }
}
