/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractMultiPolygon extends AbstractMultiSurface implements MultiPolygon {

    /**
     *
     */
    private static final long serialVersionUID = 6663703214829442424L;

    /**
     * @param type
     */
    public AbstractMultiPolygon(int subtype) {
        super(Geometry.TYPES.MULTIPOLYGON, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        com.vividsolutions.jts.geom.Polygon[] polygons = new com.vividsolutions.jts.geom.Polygon[primitives.size()];
        for (int i=0; i<primitives.size(); i++){
            polygons[i] = (com.vividsolutions.jts.geom.Polygon) ((GeometryJTS)primitives.get(i)).getJTS();
        }
        return JTSUtils.createJTSMultiPolygon(polygons);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        MultiPolygonIterator pi = new MultiPolygonIterator(at);
        return pi;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(getPathIterator(null), false, 0);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            ((GeometryJTS)iterator.next()).flip();
        }
        Collections.reverse(primitives);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiSurface#addSurface(org.gvsig.fmap.geom.primitive.Surface)
     */
    public void addSurface(Surface surface) {
        GeometryType geometryType = surface.getGeometryType();
        if(geometryType.getType() == Geometry.TYPES.POLYGON && geometryType.getSubType() == getGeometryType().getSubType()){
            primitives.add(surface);
            return;
        }
        String msg = StringUtils.replaceEach(
            "Only a polygon subtype  %(subtypeSurface)s can be added to a MultiPolygon subtype %(subtype)s",
            new String[]{"%(subtypeSurface)s","%(subtype)s"},
            new String[]{String.valueOf(geometryType.getSubType()), String.valueOf(getGeometryType().getSubType())}
        );
        logger.warn(msg);
        throw new UnsupportedOperationException(msg);

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    protected class MultiPolygonIterator extends GeneralPathXIterator {

        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;
        private int index = 0;
        private List<PathIterator>iterators = new ArrayList<PathIterator>(primitives.size());

        /**
         * Creates a new PointIterator object.
         *
         * @param p
         *            The polygon
         * @param at
         *            The affine transform applied to coordinates during
         *            iteration
         */
        public MultiPolygonIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
                Primitive primitive = (Primitive) iterator.next();
                iterators.add(primitive.getPathIterator(at));
            }
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            PathIterator pathIteratorPrimitive = iterators.get(index);
            pathIteratorPrimitive.next();
            if(pathIteratorPrimitive.isDone()){
                index++;
                done = (index==primitives.size());
            }
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            return iterators.get(index).currentSegment(coords);
        }

        /*
         * (non-Javadoc)
         *
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            return iterators.get(index).currentSegment(coords);
        }
    }

}
