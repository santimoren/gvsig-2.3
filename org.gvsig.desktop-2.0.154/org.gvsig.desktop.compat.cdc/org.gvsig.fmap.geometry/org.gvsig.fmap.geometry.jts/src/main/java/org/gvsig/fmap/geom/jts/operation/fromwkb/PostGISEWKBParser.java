/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.operation.fromwkb;

/*
 * Based in
 * PostGIS extension for PostgreSQL JDBC driver - Binary Parser
 *
 * (C) 2005 Markus Schaber, schabios@logi-track.com
 */
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.type.GeometryType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.io.WKBConstants;

import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Polygon;

/**
 * Parse binary representation of geometries. Currently, only text rep (hexed)
 * implementation is tested.
 *
 * It should be easy to add char[] and CharSequence ByteGetter instances,
 * although the latter one is not compatible with older jdks.
 *
 * I did not implement real unsigned 32-bit integers or emulate them with long,
 * as both java Arrays and Strings currently can have only 2^31-1 elements
 * (bytes), so we cannot even get or build Geometries with more than approx.
 * 2^28 coordinates (8 bytes each).
 *
 * @author markus.schaber@logi-track.com
 * 
 * https://github.com/postgis/postgis-java/blob/master/jdbc/src/main/java/org/postgis/binary/BinaryWriter.java
 *
 */
public class PostGISEWKBParser {

    private boolean gHaveM, gHaveZ, gHaveS; // M, Z y SRID

    private static final Logger LOG = LoggerFactory.getLogger(PostGISEWKBParser.class);
    private final GeometryManager geomManager = GeometryLocator.getGeometryManager();
    private final GeometryType[] pointGeometryTypes;

    public PostGISEWKBParser() {
        pointGeometryTypes
                = new GeometryType[]{
                    loadPointGeometryType(Geometry.SUBTYPES.GEOM2D, "2D"),
                    loadPointGeometryType(Geometry.SUBTYPES.GEOM3D, "3D"),
                    loadPointGeometryType(Geometry.SUBTYPES.GEOM2DM, "2DM"),
                    loadPointGeometryType(Geometry.SUBTYPES.GEOM3DM, "3DM")};
    }

    @SuppressWarnings("UseSpecificCatch")
    private GeometryType loadPointGeometryType(int subtype, String subTypeName) {
        try {
            return geomManager.getGeometryType(Geometry.TYPES.POINT, subtype);
        } catch (Exception e) {
            LOG.info("Unable to get a reference to the geometry "
                    + "type Point{}, to be cached", subTypeName);
            return null;
        }
    }

    /**
     * Parse a binary encoded geometry.
     *
     * Is synchronized to protect offset counter. (Unfortunately, Java does not
     * have neither call by reference nor multiple return values.)
     *
     * @param value
     * @return
     * @throws CreateGeometryException
     */
    public synchronized Geometry parse(byte[] value) throws CreateGeometryException {
        // BinaryByteGetter bytes = new ByteGetter.BinaryByteGetter(value);
        ByteBuffer buf = ByteBuffer.wrap(value);
        return parseGeometry(buf);
    }

    /**
     * Parse a geometry starting at offset.
     *
     * @param data
     * @return
     * @throws CreateGeometryException
     */
    protected Geometry parseGeometry(ByteBuffer data) throws CreateGeometryException {
        int realtype = parseTypeAndSRID(data);

        Geometry result1 = null;
        switch (realtype) {
            case WKBConstants.wkbPoint:
                result1 = parsePoint(data, gHaveZ, gHaveM);
                break;
            case WKBConstants.wkbLineString:
                result1 = parseLineString(data, gHaveZ, gHaveM);
                break;
            case WKBConstants.wkbPolygon:
                result1 = parsePolygon(data, gHaveZ, gHaveM);
                break;
            case WKBConstants.wkbMultiPoint:
                result1 = parseMultiPoint(data, gHaveZ, gHaveM);
                break;
            case WKBConstants.wkbMultiLineString:
                result1 = parseMultiLineString(data, gHaveZ, gHaveM);
                return result1;
            case WKBConstants.wkbMultiPolygon:
                result1 = parseMultiPolygon(data, gHaveZ, gHaveM);
                break;
            case WKBConstants.wkbGeometryCollection:
                result1 = parseCollection(data);
                break;
            default:
            //throw new IllegalArgumentException("Unknown Geometry Type!");
        }

        return result1;
    }

    protected int parseTypeAndSRID(ByteBuffer data) {
        byte endian = data.get(); // skip and test endian flag
        if (endian == 1) {
            data.order(ByteOrder.LITTLE_ENDIAN);
        }
        int typeword = data.getInt();
        
        int realtype = typeword & 0x1FFFFFFF; // cut off high flag bits

        gHaveZ = (typeword & 0x80000000) != 0;
        gHaveM = (typeword & 0x40000000) != 0;
        gHaveS = (typeword & 0x20000000) != 0;

        // not used
        int srid = -1;

        if (gHaveS) {
            srid = data.getInt();
        }

        return realtype;

    }

    private Point parsePoint(ByteBuffer data, boolean haveZ, boolean haveM)
            throws CreateGeometryException {
        double x = data.getDouble();
        double y = data.getDouble();
        Point point;

        int subtype = getSubType(haveZ, haveM);

        // If we have a cached GeometryType use it, otherwise call the manager
        if (pointGeometryTypes[subtype] == null) {
            point = (Point) geomManager.create(Geometry.TYPES.POINT, subtype);
        } else {
            point = (Point) pointGeometryTypes[subtype].create();
        }
        point.setX(x);
        point.setY(y);

        // Other dimensions
        if (haveZ) {
            point.setCoordinateAt(Geometry.DIMENSIONS.Z, data.getDouble());
        }
        if (haveM) {
           point.setCoordinateAt(point.getDimension()-1, data.getDouble());
        }
        return point;
    }

    /**
     * @param haveZ
     * @param haveM
     * @return
     */
    private int getSubType(boolean haveZ, boolean haveM) {
        int subtype;

        if( haveZ ) {
            if( haveM ) {
                subtype = Geometry.SUBTYPES.GEOM3DM;
            } else {
                subtype = Geometry.SUBTYPES.GEOM3D;
            }
        } else {
            if( haveM ) {
                subtype = Geometry.SUBTYPES.GEOM2DM;
            } else {
                subtype = Geometry.SUBTYPES.GEOM2D;
            }
        }
       return subtype;
    }

    private MultiPrimitive parseMultiLineString(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
        int subType = getSubType(haveZ, haveM);
        MultiPrimitive multiline = geomManager.createMultiLine(subType);

        int count = data.getInt();

        for (int i = 0; i < count; i++) {
            parseTypeAndSRID(data);
            Point[] points = parsePointArray(data, haveZ, haveM);
            Line line = geomManager.createLine(getSubType(haveZ, haveM));
            line.ensureCapacity(points.length);
            for (Point point : points) {
                line.addVertex(point);
            }
            multiline.addPrimitive(line);
        }
        return multiline;
    }

    private MultiPrimitive parseMultiPolygon(ByteBuffer data, boolean haveZ, boolean haveM)
            throws CreateGeometryException {
        int count = data.getInt();

        int subType = getSubType(haveZ, haveM);
        MultiPrimitive multipolygon = geomManager.createMultiPolygon(subType);

        for (int i = 0; i < count; i++) {
        	parseTypeAndSRID(data);
            Polygon polygon = geomManager.createPolygon(subType);
            int countRings = data.getInt();
            for (int nring = 0; nring < countRings; nring++) {
                OrientablePrimitive ring;
                if( nring==0 ) {
                    ring = polygon;
                } else {
                    ring = (Ring) geomManager.create(Geometry.TYPES.RING,subType);
                }
                Point[] points = parsePointsAsPointsArray(data, haveZ, haveM);

                    ring.ensureCapacity(points.length);
                int lastPoint = points.length - 1;
                for (int k = 0; k <= lastPoint; k++) {
                    ring.addVertex(points[k]);
                }
                if( nring!=0 ) {
                    polygon.addInteriorRing((Ring)ring);
                }
            }
            multipolygon.addPrimitive(polygon);
        }
        return multipolygon;
    }

    private Point[] parsePointsAsPointsArray(ByteBuffer data, boolean haveZ,
        boolean haveM) throws CreateGeometryException {
    int count = data.getInt();
    Point points[] = new Point[count];
    int subtype = getSubType(haveZ, haveM);

    for (int i = 0; i < count; i++) {
        points[i] = geomManager.createPoint(data.getDouble(), data.getDouble(), subtype);
        switch (subtype) {
            case Geometry.SUBTYPES.GEOM3D:
                points[i].setCoordinateAt(Geometry.DIMENSIONS.Z, data.getDouble()); // z
                break;
            case Geometry.SUBTYPES.GEOM2DM:
                points[i].setCoordinateAt(points[i].getDimension()-1, data.getDouble()); // m
                break;
            case Geometry.SUBTYPES.GEOM3DM:
                points[i].setCoordinateAt(Geometry.DIMENSIONS.Z, data.getDouble()); // z
                points[i].setCoordinateAt(points[i].getDimension()-1, data.getDouble()); // m
                break;
            default:
                break;
        }
    }
    return points;
}

    private MultiPrimitive parseCollection(ByteBuffer data) throws CreateGeometryException {
        int count = data.getInt();
        Geometry[] geoms = new Geometry[count];
        parseGeometryArray(data, geoms);
        MultiPrimitive multiPrimitive = (MultiPrimitive) geomManager.create(TYPES.AGGREGATE, SUBTYPES.GEOM2D);
        for (Geometry geom : geoms) {
            multiPrimitive.addPrimitive((Primitive) geom);
        }
        return multiPrimitive;
    }

    /**
     * Parse an Array of "full" Geometries
     *
     * @throws CreateGeometryException
     */
    private void parseGeometryArray(ByteBuffer data, Geometry[] container) throws CreateGeometryException {
        for (int i = 0; i < container.length; i++) {
            container[i] = parseGeometry(data);
        }
    }

    private Line parseLineString(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
        Point[] points = parsePointArray(data, haveZ, haveM);
        Line line = geomManager.createLine(getSubType(haveZ, haveM));
        line.ensureCapacity(points.length);
        for (Point point : points) {
            line.addVertex(point);
        }
        return line;
    }

    /**
     * Parse an Array of "slim" Points (without endianness and type, part of
     * LinearRing and Linestring, but not MultiPoint!
     *
     * @param haveZ
     * @param haveM
     * @throws CreateGeometryException
     */
    private Point[] parsePointArray(ByteBuffer data, boolean haveZ, boolean haveM)
            throws CreateGeometryException {
        int count = data.getInt();
        Point[] result = new Point[count];
        for (int i = 0; i < count; i++) {
            result[i] = parsePoint(data, haveZ, haveM);
        }
        return result;
    }

    private Polygon parsePolygon(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
        int count = data.getInt();
        int subType = getSubType(haveZ, haveM);

        Polygon polygon = geomManager.createPolygon(subType);
        fillLinearRing(data, polygon, haveZ, haveM);

        for (int i = 1; i < count; i++) {
            Ring ring = (Ring) geomManager.create(Geometry.TYPES.RING, subType);
            fillLinearRing(data, ring, haveZ, haveM);
            polygon.addInteriorRing(ring);
        }
        return polygon;
    }

    private void fillLinearRing(ByteBuffer data, OrientablePrimitive ring, boolean haveZ, boolean haveM)
        throws CreateGeometryException {
        Point[] points = parsePointArray(data, haveZ, haveM);
        int lastPoint = points.length - 1;
        ring.ensureCapacity(points.length);
        for (int i = 0; i <= lastPoint; i++) {
            ring.addVertex(points[i]);
        }
    }

    private MultiPoint parseMultiPoint(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
    	int subType = getSubType(haveZ, haveM);
        MultiPoint multipoint = geomManager.createMultiPoint(subType);
        int points = data.getInt();
        multipoint.ensureCapacity(points);
        for (int i = 0; i < points; i++) {
            parseTypeAndSRID(data);
            multipoint.addPoint(parsePoint(data, haveZ, haveM));
        }
        return multipoint;
    }

}
