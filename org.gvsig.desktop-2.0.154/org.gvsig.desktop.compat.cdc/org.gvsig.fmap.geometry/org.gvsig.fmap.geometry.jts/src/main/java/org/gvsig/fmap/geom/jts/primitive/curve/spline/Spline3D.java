/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.spline;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.OpenJUMPUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Spline;


/**
 * @author fdiaz
 *
 */
public class Spline3D extends BaseSpline3D implements Spline {

    /**
     *
     */
    private static final long serialVersionUID = 8377943996574176559L;

    /**
     * @param subtype
     */
    public Spline3D() {
        super(Geometry.TYPES.SPLINE);
    }

    /**
    *
    */
    public Spline3D(Coordinate[] coordinates) {
        super(Geometry.TYPES.SPLINE, coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Spline3D(cloneCoordinates().toCoordinateArray());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        Coordinate[] coords = getJTS().getCoordinates();
        ArrayListCoordinateSequence listCoordSequence = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
        for (int i = 0; i < coords.length; i++) {
            listCoordSequence.add(coords[i]);
        }
        if (isClosed()) {
            return JTSUtils.offsetClosedLine(listCoordSequence, distance);
        } else {
            return OpenJUMPUtils.offsetCleanOpenLine(listCoordSequence, distance);
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = super.equals(obj);
        if(res && obj instanceof Spline3D){
            Spline3D other = (Spline3D)obj;
            if(this.getNumVertices() != other.getNumVertices()){
                return false;
            }
            for(int i=0; i < this.getNumVertices(); i++){
                Coordinate coordinate = this.coordinates.get(i);
                Coordinate otherCoordinate = other.coordinates.get(i);
                if (otherCoordinate.getOrdinate(2) != coordinate.getOrdinate(2)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

}
