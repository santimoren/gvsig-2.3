/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.util;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.LineSegment;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Triangle;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.buffer.OffsetCurveBuilder;
import com.vividsolutions.jts.util.GeometricShapeFactory;

import org.hibernate.spatial.jts.mgeom.MCoordinateSequence;
import org.hibernate.spatial.jts.mgeom.MGeometryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.MCoordinate;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point2DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DM;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring2D;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring2DM;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring3D;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author fdiaz
 *
 */
public class JTSUtils {

    public static final Logger logger = LoggerFactory.getLogger(JTSUtils.class);

    private static class MyMGeometryFactory extends org.hibernate.spatial.jts.mgeom.MGeometryFactory {

        /**
         *
         */
        private static final long serialVersionUID = -8174926092714691479L;

        public com.vividsolutions.jts.geom.Point createPoint(CoordinateSequence coordinates) {
            if(!(coordinates instanceof MCoordinateSequence)){
                if (coordinates != null) {
                    coordinates = new MCoordinateSequence(coordinates);
                }
            }
            return (com.vividsolutions.jts.geom.Point) new com.vividsolutions.jts.geom.Point(coordinates, this);
        }

        public LinearRing createLinearRing(CoordinateSequence coordinates) {
            if(!(coordinates instanceof MCoordinateSequence)){
                if (coordinates != null) {
                    coordinates = new MCoordinateSequence(coordinates);
                }
            }
            return super.createLinearRing(coordinates);
        }

        @Override
        public LineString createLineString(CoordinateSequence coordinates) {
            if(!(coordinates instanceof MCoordinateSequence)){
                if (coordinates != null) {
                    coordinates = new MCoordinateSequence(coordinates);
                }
            }
            return super.createLineString(coordinates);
        }

        public MultiPoint createMultiPoint(CoordinateSequence coordinates) {
            if(!(coordinates instanceof MCoordinateSequence)){
                if (coordinates != null) {
                    coordinates = new MCoordinateSequence(coordinates);
                }
            }
            return super.createMultiPoint(coordinates);
        }

    }


    private static final com.vividsolutions.jts.geom.GeometryFactory factory =
        new com.vividsolutions.jts.geom.GeometryFactory();

    private static final com.vividsolutions.jts.geom.GeometryFactory mfactory = new MyMGeometryFactory();

    public static Point createPoint(GeometryType type, Coordinate coordinate) throws CreateGeometryException {

        switch (type.getSubType()) {
        case Geometry.SUBTYPES.GEOM2D:
            return new Point2D(coordinate);
        case Geometry.SUBTYPES.GEOM2DM:
            return new Point2DM(coordinate);
        case Geometry.SUBTYPES.GEOM3D:
            return new Point3D(coordinate);
        case Geometry.SUBTYPES.GEOM3DM:
            return new Point3DM(coordinate);
        default:
            Point p = null;
            p = (Point) type.create();
            for (int i = 0; i < p.getDimension(); i++) {
                p.setCoordinateAt(i, coordinate.getOrdinate(i));
            }
            break;
        }

        return null;
    }

    public static com.vividsolutions.jts.geom.GeometryFactory getFactory(CoordinateSequence coordinates){
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.factory;
        if(coordinates.size()>0 && coordinates.getCoordinate(0) instanceof org.hibernate.spatial.jts.mgeom.MCoordinate){
            factory = mfactory;
        }
        return factory;
    }

    public static com.vividsolutions.jts.geom.GeometryFactory getFactory(Coordinate coordinate) {
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.factory;
        if(coordinate instanceof org.hibernate.spatial.jts.mgeom.MCoordinate){
            factory = mfactory;
        }
        return factory;
    }

    public static com.vividsolutions.jts.geom.GeometryFactory getFactory(Coordinate[] coordinates) {
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.factory;
        if(coordinates.length>0 && coordinates[0] instanceof org.hibernate.spatial.jts.mgeom.MCoordinate){
                factory = mfactory;
        }
        return factory;
    }

    public static LineString createJTSLineString(CoordinateSequence coordinates) {
        return getFactory(coordinates).createLineString(coordinates);
    }

    public static LinearRing createJTSLinearRing(CoordinateSequence coordinates) {
        return getFactory(coordinates).createLinearRing(coordinates);
    }

    public static MultiPoint createJTSMultiPoint(CoordinateSequence coordinates) {
        return getFactory(coordinates).createMultiPoint(coordinates);
    }

    public static Geometry createGeometry(com.vividsolutions.jts.geom.Geometry jtsGeom) {
        if (jtsGeom instanceof com.vividsolutions.jts.geom.Point) {
            Coordinate coordinate = jtsGeom.getCoordinate();
            if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                if (Double.isNaN(coordinate.z)) {
                    return new Point2DM(coordinate);
                } else {
                    return new Point3DM(coordinate);
                }
            } else {
                if (Double.isNaN(coordinate.z)) {
                    return new Point2D(coordinate);
                } else {
                    return new Point3D(coordinate);
                }
            }
        }

        if (jtsGeom instanceof com.vividsolutions.jts.geom.LineString) {
            Coordinate[] coordinates = jtsGeom.getCoordinates();
            Coordinate coordinate = jtsGeom.getCoordinate();
            com.vividsolutions.jts.geom.LineString lineString = (com.vividsolutions.jts.geom.LineString) jtsGeom;
            if (!lineString.isEmpty()) {
                if (jtsGeom.getFactory() instanceof MGeometryFactory) {
                    if (coordinate!=null && Double.isNaN(coordinate.z)) {
                        return new Line2DM(coordinates);
                    } else {
                        return new Line3DM(coordinates);
                    }
                } else {
                    if (coordinate!=null && Double.isNaN(coordinate.z)) {
                        return new Line2D(coordinates);
                    } else {
                        return new Line3D(coordinates);
                    }
                }
            }
        }

        if (jtsGeom instanceof com.vividsolutions.jts.geom.Polygon && !jtsGeom.isEmpty()) {
            Polygon polygon;
            com.vividsolutions.jts.geom.Polygon polygonJTS = (com.vividsolutions.jts.geom.Polygon)jtsGeom;
            Coordinate[] coordinates = polygonJTS.getExteriorRing().getCoordinates();
            Coordinate coordinate = jtsGeom.getCoordinate();
            if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                if (Double.isNaN(coordinate.z)) {
                    polygon = new Polygon2DM(coordinates);
                } else {
                    polygon = new Polygon3DM(coordinates);
                }
            } else {
                if (Double.isNaN(coordinate.z)) {
                    polygon = new Polygon2D(coordinates);
                } else {
                    polygon = new Polygon3D(coordinates);
                }
            }
            for(int i = 0; i<polygonJTS.getNumInteriorRing(); i++){
                LineString ringJTS = polygonJTS.getInteriorRingN(i);
                coordinates = ringJTS.getCoordinates();
                coordinate = ringJTS.getCoordinate();
                Ring ring;
                if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                    if (Double.isNaN(coordinate.z)) {
                        ring = new Ring2DM(coordinates);
                    } else {
                        ring = new Ring3DM(coordinates);
                    }
                } else {
                    if (Double.isNaN(coordinate.z)) {
                        ring = new Ring2D(coordinates);
                    } else {
                        ring = new Ring3D(coordinates);
                    }
                }
                polygon.addInteriorRing(ring);
            }
            return polygon;
        }

        if (jtsGeom instanceof com.vividsolutions.jts.geom.GeometryCollection) {
            GeometryCollection collection = (com.vividsolutions.jts.geom.GeometryCollection)jtsGeom;
            Coordinate coordinate = collection.getCoordinate();
            MultiPrimitive multiprimitive = null;
            if (jtsGeom instanceof MultiLineString) {
                if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiLine2DM();
                    } else {
                        multiprimitive = new MultiLine3DM();
                    }
                } else {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiLine2D();
                    } else {
                        multiprimitive = new MultiLine3D();
                    }
                }
            }
            if (jtsGeom instanceof MultiPolygon) {
                if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiPolygon2DM();
                    } else {
                        multiprimitive = new MultiPolygon3DM();
                    }
                } else {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiPolygon2D();
                    } else {
                        multiprimitive = new MultiPolygon3D();
                    }
                }
            }

            if (jtsGeom instanceof MultiPoint) {
                if (jtsGeom.getFactory() instanceof MyMGeometryFactory) {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiPoint2DM();
                    } else {
                        multiprimitive = new MultiPoint3DM();
                    }
                } else {
                    if (Double.isNaN(coordinate.z)) {
                        multiprimitive = new MultiPoint2D();
                    } else {
                        multiprimitive = new MultiPoint3D();
                    }
                }
            }

            if (multiprimitive != null) {
                for (int i = 0; i < collection.getNumGeometries(); i++) {
                    com.vividsolutions.jts.geom.Geometry geometry = collection.getGeometryN(i);
                    multiprimitive.addPrimitive((Primitive) createGeometry(geometry));
                }
            }
            return multiprimitive;
        }

        return null;
    }

    /**
     * This function is called when the we need force types, that is the
     * destination
     * type does not match with the input geometry type
     *
     * @param g
     * @param sourceType
     * @param destinationType
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry convertTypes(com.vividsolutions.jts.geom.Geometry g,
        int sourceType, int destinationType) {

        com.vividsolutions.jts.geom.GeometryFactory factory = g.getFactory();

        if ((sourceType == Geometry.TYPES.CURVE || sourceType == Geometry.TYPES.SPLINE
            || sourceType == Geometry.TYPES.ARC || sourceType == Geometry.TYPES.ELLIPTICARC
            || sourceType == Geometry.TYPES.CIRCUMFERENCE || sourceType == Geometry.TYPES.PERIELLIPSE)
            && destinationType == Geometry.TYPES.MULTISURFACE) {
            if (g instanceof MultiLineString) {
                com.vividsolutions.jts.geom.Polygon[] poly = new com.vividsolutions.jts.geom.Polygon[((MultiLineString) g).getNumGeometries()];
                for (int i = 0; i < ((MultiLineString) g).getNumGeometries(); i++) {
                    com.vividsolutions.jts.geom.Geometry lineString = ((MultiLineString) g).getGeometryN(i);
                    poly[i] = convertLineStringToPolygon((LineString) lineString);
                }
                return factory.createMultiPolygon(poly);
            } else {
                return convertLineStringToPolygon((LineString) g);
            }
        }

        if ((sourceType == Geometry.TYPES.CIRCLE || sourceType == Geometry.TYPES.ELLIPSE
            || sourceType == Geometry.TYPES.ELLIPTICARC || sourceType == Geometry.TYPES.FILLEDSPLINE)
            && destinationType == Geometry.TYPES.MULTICURVE) {
            if (g instanceof com.vividsolutions.jts.geom.Polygon) {
                com.vividsolutions.jts.geom.Polygon poly = (com.vividsolutions.jts.geom.Polygon) g;
                LineString lineString = factory.createLinearRing(poly.getCoordinates());
                return factory.createMultiLineString(new LineString[] { lineString });
            }
        }
        return g;
    }

    private static com.vividsolutions.jts.geom.Polygon convertLineStringToPolygon(LineString line) {
        Coordinate[] coordinates = line.getCoordinates();
        com.vividsolutions.jts.geom.GeometryFactory factory = line.getFactory();
        LinearRing shell = factory.createLinearRing(coordinates);
        com.vividsolutions.jts.geom.Polygon pol = factory.createPolygon(shell, null);
        return pol;
    }

    public static MCoordinate createMCoordinate(double x, double y, double m) {
        return MCoordinate.create2dWithMeasure(x, y, m);
    }

    public static MCoordinate createMCoordinate(double x, double y, double z, double m) {
        return MCoordinate.create3dWithMeasure(x, y, z, m);
    }

    /**
     * @param init
     * @param middle
     * @param end
     * @return
     */
    public static Coordinate getCircumcentre(Point init, Point middle, Point end) {
        Triangle triangle =
            new Triangle(((PointJTS) init).getJTSCoordinate(),
                ((PointJTS) middle).getJTSCoordinate(),
                ((PointJTS) end).getJTSCoordinate());
        return triangle.circumcentre();
    }

    public static LineString createJTSLineStringFromArcPoints(Point centre, double radius, double startAngle, double endAngle){
        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        shapeFactory.setCentre(((PointJTS)centre).getJTSCoordinate());
        shapeFactory.setSize(radius*2);
        return shapeFactory.createArc(startAngle, endAngle);
    }

    public static MultiLineString createJTSMultiLineString(LineString[] lineStrings) {
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.factory;
        if(lineStrings.length>0){
            factory = lineStrings[0].getFactory();
        }
        return factory.createMultiLineString(lineStrings);
    }

    /**
     * @param coordinates
     * @param interiorRings
     * @return
     * @throws GeometryException
     */
    public static com.vividsolutions.jts.geom.Polygon createJTSPolygon(ArrayListCoordinateSequence coordinates,
        List<Ring> interiorRings) {
        com.vividsolutions.jts.geom.GeometryFactory factory = getFactory(coordinates);

        LinearRing shell = factory.createLinearRing(coordinates);
        LinearRing[] holes = new LinearRing[interiorRings.size()];
        for(int i=0; i<interiorRings.size(); i++){
            Ring ring = interiorRings.get(i);
            holes[i] = (LinearRing) ((GeometryJTS) ring).getJTS();
        }
        return factory.createPolygon(shell, holes);
    }


    /**
     * @param coordinates
     * @return
     * @throws GeometryException
     */
    public static com.vividsolutions.jts.geom.Polygon createJTSPolygon(ArrayListCoordinateSequence coordinates) {
        com.vividsolutions.jts.geom.GeometryFactory factory = getFactory(coordinates);

        LinearRing shell = factory.createLinearRing(coordinates);
        return factory.createPolygon(shell);
    }

    public static com.vividsolutions.jts.geom.MultiPolygon createJTSMultiPolygon(com.vividsolutions.jts.geom.Polygon[] polygons){
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.factory;
        if(polygons.length>0){
            factory = polygons[0].getFactory();
        }
        return factory.createMultiPolygon(polygons);
    }

    /**
     * Returns the intersection point between an ellipse and her minor axis.
     *
     * @param init
     * @param middle
     * @param end
     * @return
     */
    public static Coordinate getPointAtYAxisInEllipse(Point init, Point end, Double ydist) {

        LineSegment segment = new LineSegment(((PointJTS) init).getJTSCoordinate(),
            ((PointJTS) end).getJTSCoordinate());
        Coordinate middle = segment.midPoint();

        LineSegment midSegment = new LineSegment(middle, ((PointJTS) end).getJTSCoordinate());
        double modulus = midSegment.getLength();

        //Mayor semi-axis unitary vector
        Coordinate uni = new Coordinate((end.getX()-middle.x)/modulus, (end.getY()-middle.y)/modulus);

        //Minor axis unitary vector
        Coordinate perpUni = new Coordinate(-uni.y,uni.x);

        Coordinate point = new Coordinate(middle.x+(perpUni.x*ydist), middle.y+(perpUni.y*ydist));

        return point;
    }

    /**
     * Returns the middle point between two points.
     *
     * @param init
     * @param middle
     * @param end
     * @return
     */
    public static Coordinate getMidPoint(Point init, Point end) {

        LineSegment segment = new LineSegment(((PointJTS) init).getJTSCoordinate(),
            ((PointJTS) end).getJTSCoordinate());
        Coordinate middle = segment.midPoint();
        return middle;
    }

    /**
     * @param coordinate
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry createJTSPoint(Coordinate coordinate) {
        return getFactory(coordinate).createPoint(coordinate);
    }

    /**
     * @param coord
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry createJTSPolygon(Coordinate[] coordinates) {
        return getFactory(coordinates).createPolygon(coordinates);
    }

    public static BufferParameters getBufferParameters(){
        BufferParameters bufParams = new BufferParameters();
        bufParams.setSingleSided(true);
        bufParams.setEndCapStyle(BufferParameters.CAP_FLAT);
        bufParams.setJoinStyle(BufferParameters.CAP_ROUND);
        return bufParams;
    }


    /**
     * Creates a offset from a closed line at a distance
     *
     * @param distance
     * @return
     */
    public static Geometry offsetClosedLine(ArrayListCoordinateSequence coordinates, double distance) {
        com.vividsolutions.jts.geom.Geometry jtsGeom = JTSUtils.createJTSPolygon(coordinates);
        com.vividsolutions.jts.geom.Geometry geomJTS = jtsGeom.buffer(distance);
        if (geomJTS instanceof com.vividsolutions.jts.geom.Polygon) {
            com.vividsolutions.jts.geom.Polygon polygonJTS = (com.vividsolutions.jts.geom.Polygon) geomJTS;
            LineString shell = polygonJTS.getExteriorRing();
            int numHoles = polygonJTS.getNumInteriorRing();
            if (numHoles > 0) {
                LineString[] lineStrings = new LineString[numHoles + 1];
                lineStrings[0] = shell;
                for (int i = 0; i < numHoles; i++) {
                    LineString hole = polygonJTS.getInteriorRingN(i);
                    lineStrings[i + 1] = hole;
                }
                return JTSUtils.createGeometry(JTSUtils.createJTSMultiLineString(lineStrings));
            }
            return JTSUtils.createGeometry(shell);
        } else if (geomJTS instanceof com.vividsolutions.jts.geom.MultiPolygon) {
            MultiPolygon multiPolygonJTS = (com.vividsolutions.jts.geom.MultiPolygon) geomJTS;
            List<LineString> lineStringList = new ArrayList<LineString>();
            for (int i = 0; i < multiPolygonJTS.getNumGeometries(); i++) {
                com.vividsolutions.jts.geom.Polygon polygonJTS =
                    (com.vividsolutions.jts.geom.Polygon) multiPolygonJTS.getGeometryN(i);
                lineStringList.add(polygonJTS.getExteriorRing());
                int numHoles = polygonJTS.getNumInteriorRing();
                if (numHoles > 0) {
                    for (int h = 0; h < numHoles; h++) {
                        LineString hole = polygonJTS.getInteriorRingN(i);
                        lineStringList.add(hole);
                    }
                }
            }
            return JTSUtils.createGeometry(JTSUtils.createJTSMultiLineString(lineStringList.toArray(new LineString[lineStringList.size()])));
        }
        //No deber�a pasar por aqu�
        logger.warn("offsetClosedLine does not return or Polygon JTS or MultiPolygon JTS");
        return null;
    }

    /**
     * Creates a raw offset of a open line at a distance.
     *
     * @param coordinates
     * @param distance
     * @return
     */
    public static Geometry offsetRawOpenLine(ArrayListCoordinateSequence coordinates, double distance) {
        com.vividsolutions.jts.geom.GeometryFactory factory = getFactory(coordinates);

        BufferParameters bufParams = JTSUtils.getBufferParameters();
        LineString jtsGeom = JTSUtils.createJTSLineString(coordinates);

         OffsetCurveBuilder ocb = new
         OffsetCurveBuilder(factory.getPrecisionModel(), bufParams);
         Coordinate[] coords = ocb.getOffsetCurve(jtsGeom.getCoordinates(), distance);
         CoordinateSequence coordSequence = factory.getCoordinateSequenceFactory().create(coords);
         com.vividsolutions.jts.geom.Geometry offsetGeom = JTSUtils.createJTSLineString(coordSequence);

         return JTSUtils.createGeometry(offsetGeom);
    }

    /*
     *
     */
    public static double straightLineThroughTwoPointsEquation(double x1, double x2, double y1, double y2, double x) {
        if (x2 - x1 == 0.0) {
            return Double.POSITIVE_INFINITY;
        }
        return ((y2 - y1) * (x - x1) / (x2 - x1)) + y1;
    }

}
