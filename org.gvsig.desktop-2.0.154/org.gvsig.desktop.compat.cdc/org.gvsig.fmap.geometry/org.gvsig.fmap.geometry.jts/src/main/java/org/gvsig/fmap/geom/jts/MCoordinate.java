/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts;


/**
 * @author fdiaz
 *
 */
public class MCoordinate extends org.hibernate.spatial.jts.mgeom.MCoordinate {

    /**
     *
     */
    private static final long serialVersionUID = -734914554153589737L;

    /**
     * @param x
     * @param y
     * @param nan
     * @param m
     */
    public MCoordinate(double x, double y, double z, double m) {
        super(x, y, z, m);
    }


    /**
     * Default constructor
     */
    public MCoordinate() {
        super();
    }

    public static MCoordinate create2dWithMeasure(double x, double y, double m) {
        return new MCoordinate(x, y, Double.NaN, m);
    }

    public static MCoordinate create2d(double x, double y) {
        return new MCoordinate(x, y, Double.NaN, Double.NaN);
    }

    public static MCoordinate create3dWithMeasure(double x, double y, double z,
                                                  double m) {
        return new MCoordinate(x, y, z, m);
    }

    public static MCoordinate create3d(double x, double y, double z) {
        return new MCoordinate(x, y, z, Double.NaN);
    }

}
