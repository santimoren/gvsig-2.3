/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright © 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.io.WKTWriter;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import com.vividsolutions.jts.operation.overlay.snap.GeometrySnapper;
import com.vividsolutions.jts.operation.valid.IsValidOp;
import com.vividsolutions.jts.operation.valid.TopologyValidationError;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.jts.operation.towkb.OGCWKBEncoder;
import org.gvsig.fmap.geom.jts.operation.towkb.PostGISEWKBEncoder;
import org.gvsig.fmap.geom.jts.operation.towkt.EWKTWriter;
import org.gvsig.fmap.geom.jts.primitive.Envelope2D;
import org.gvsig.fmap.geom.jts.primitive.Envelope3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.OrientableCurve;
import org.gvsig.fmap.geom.primitive.OrientableSurface;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author fdiaz
 *
 */
public abstract class AbstractGeometry implements GeometryJTS {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractGeometry.class);

    /**
     *
     */
    private static final long serialVersionUID = 4999326772576222293L;

    private final GeometryType geometryType;


    /**
     *
     */
    public AbstractGeometry(int type, int subtype) {
        try {
            this.geometryType = GeometryLocator.getGeometryManager().getGeometryType(type, subtype);
        } catch (Exception e) {
            // TODO: Ver de crear una excepcion para esto o si hay alguna en el API.
            throw new RuntimeException(e);
        }
    }

    public GeometryType getGeometryType(){
        return geometryType;
    }

    protected GeometryManager getManager() {
        return GeometryLocator.getGeometryManager();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#contains(org.gvsig.fmap.geom.Geometry)
     */
    public boolean contains(Geometry geometry) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        if (!(geometry instanceof GeometryJTS)) {
            return false;
        }
        return getJTS().contains(((GeometryJTS) geometry).getJTS());
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
        return getJTS().compareTo(((GeometryJTS) o).getJTS());
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(double, double)
     */
    public boolean contains(double x, double y) {
        notifyDeprecated("Calling deprecated method of geometry contains from shape interface");
        Shape shp = getShape();
        return shp.contains(x, y);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#intersects(double, double, double, double)
     */
    public boolean intersects(double x, double y, double w, double h) {

        notifyDeprecated("Calling deprecated method of geometry intersects from shape interface");
        Shape shp = this.getShape();
        return shp.intersects(x, y, w, h);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(double, double, double, double)
     */
    public boolean contains(double x, double y, double w, double h) {
        notifyDeprecated("Calling deprecated method of geometry contains from shape interface");
        Shape shp = this.getShape();
        return shp.contains(x, y, w, h);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(java.awt.geom.Point2D)
     */
    public boolean contains(java.awt.geom.Point2D p) {
        notifyDeprecated("Calling deprecated method of geometry contains from shape interface");
        Shape shp = this.getShape();
        return shp.contains(p);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(java.awt.geom.Rectangle2D)
     */
    public boolean contains(Rectangle2D r) {
        notifyDeprecated("Calling deprecated method of geometry contains from shape interface");
        Shape shp = this.getShape();
        return shp.contains(r);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#distance(org.gvsig.fmap.geom.Geometry)
     */
    public double distance(org.gvsig.fmap.geom.Geometry other) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        return getJTS().distance(((GeometryJTS) other).getJTS());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#isWithinDistance(org.gvsig.fmap.geom.Geometry
     * , double)
     */
    public boolean isWithinDistance(org.gvsig.fmap.geom.Geometry other, double distance)
        throws GeometryOperationNotSupportedException, GeometryOperationException {
        return com.vividsolutions.jts.operation.distance.DistanceOp.isWithinDistance(this.getJTS(),
            ((GeometryJTS) other).getJTS(), distance);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#overlaps(org.gvsig.fmap.geom.Geometry)
     */
    public boolean overlaps(org.gvsig.fmap.geom.Geometry geometry) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().overlaps(((GeometryJTS) geometry).getJTS());
    }

    public boolean coveredBy(Geometry geometry) throws GeometryOperationNotSupportedException,
        GeometryOperationException {

        return getJTS().coveredBy(((GeometryJTS) geometry).getJTS());
    }

    public boolean covers(Geometry geometry) throws GeometryOperationNotSupportedException, GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().covers(((GeometryJTS) geometry).getJTS());

    }

    public boolean crosses(Geometry geometry) throws GeometryOperationNotSupportedException, GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().crosses(((GeometryJTS) geometry).getJTS());
    }

    public boolean intersects(Geometry geometry) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        return getJTS().intersects(((GeometryJTS) geometry).getJTS());
    }

    public boolean touches(Geometry geometry) throws GeometryOperationNotSupportedException, GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().touches(((GeometryJTS) geometry).getJTS());
    }

    public boolean disjoint(Geometry geometry) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().disjoint(((GeometryJTS) geometry).getJTS());
    }

    public boolean within(Geometry geometry) throws GeometryOperationNotSupportedException, GeometryOperationException {
        // TODO: this method can be implemented throw invokeOperation
        return getJTS().within(((GeometryJTS) geometry).getJTS());
    }

    public double area() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getJTS().getArea();
    }

    public double perimeter() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getJTS().getLength();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#intersects(java.awt.geom.Rectangle2D)
     */
    public boolean intersects(Rectangle2D r) {
        double x = r.getMinX();
        double y = r.getMinY();
        double w = r.getWidth();
        double h = r.getHeight();

        return fastIntersects(x, y, w, h);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#fastIntersects(double, double, double,
     * double)
     */
    public boolean fastIntersects(double x, double y, double w, double h) {
        com.vividsolutions.jts.geom.Geometry rect = null;
        com.vividsolutions.jts.geom.Coordinate[] coord = new com.vividsolutions.jts.geom.Coordinate[5];
        coord[0] = new com.vividsolutions.jts.geom.Coordinate(x, y);
        coord[1] = new com.vividsolutions.jts.geom.Coordinate(x + w, y);
        coord[2] = new com.vividsolutions.jts.geom.Coordinate(x + w, y + w);
        coord[3] = new com.vividsolutions.jts.geom.Coordinate(x, y + w);
        coord[4] = new com.vividsolutions.jts.geom.Coordinate(x, y);
        rect = JTSUtils.createJTSPolygon(coord);

        return getJTS().intersects(rect);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getEnvelope()
     */
    public Envelope getEnvelope() {
        if (is3D()) {
            Coordinate[] coordinates = getJTS().getCoordinates();

            double minx = Double.POSITIVE_INFINITY;
            double miny = Double.POSITIVE_INFINITY;
            double minz = Double.POSITIVE_INFINITY;

            double maxx = Double.NEGATIVE_INFINITY;
            double maxy = Double.NEGATIVE_INFINITY;
            double maxz = Double.NEGATIVE_INFINITY;

            double x;
            double y;
            double z;

            for (int i = 0; i < coordinates.length; i++) {
                x = coordinates[i].x;
                y = coordinates[i].y;
                z = coordinates[i].z;
                minx = Math.min(x, minx);
                miny = Math.min(y, miny);
                minz = Math.min(z, minz);
                maxx = Math.max(x, maxx);
                maxy = Math.max(y, maxy);
                maxz = Math.max(z, maxz);
            }

            if (minx <= maxx && miny <= maxy && minz <= maxz) {
                Point min = new Point3D(minx, miny, minz);
                Point max = new Point3D(maxx, maxy, maxz);
                return new Envelope3D(min, max);
            }
            return new Envelope3D();
        } else {
            com.vividsolutions.jts.geom.Envelope envelope = getJTS().getEnvelopeInternal();
            return new Envelope2D(envelope);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#isSimple()
     */
    public boolean isSimple() {
        return this.getJTS().isSimple();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#isCCW()
     */
    public boolean isCCW() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return CGAlgorithms.isCCW(this.getJTS().getCoordinates());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#invokeOperation(int,
     * org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    public Object invokeOperation(int index, GeometryOperationContext ctx)
        throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getManager().invokeOperation(index, this, ctx);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#invokeOperation(java.lang.String,
     * org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    public Object invokeOperation(String opName, GeometryOperationContext ctx)
        throws GeometryOperationNotSupportedException, GeometryOperationException {
        return getManager().invokeOperation(opName, this, ctx);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getType()
     */
    public int getType() {
        return this.getGeometryType().getType();
    }

    public byte[] convertToWKB() throws GeometryOperationNotSupportedException, GeometryOperationException {
    	try {
			return new OGCWKBEncoder().encode(this);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}

    }

    public byte[] convertToWKB(int srs) throws GeometryOperationNotSupportedException, GeometryOperationException {
    	/*
    	 * No se sabe si la especificación de OGC soporta SRS. OGCWKBEncoder no.
    	 */

    	try {
			return new OGCWKBEncoder().encode(this);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}
    }

    public byte[] convertToWKBForcingType(int srs, int type) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
    	/*
    	 * No se sabe si la especificación de OGC soporta SRS. OGCWKBEncoder no.
    	 */
    	Geometry geom = this;
        if(this.getType() != type){
            com.vividsolutions.jts.geom.Geometry jts = getJTS();
            jts = JTSUtils.convertTypes(jts, this.getType(), type);

            geom = JTSUtils.createGeometry(jts);
        }
    	try {
			return new OGCWKBEncoder().encode(geom);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}

    }



    public byte[] convertToEWKB() throws GeometryOperationNotSupportedException, GeometryOperationException {
    	try {
			return new PostGISEWKBEncoder().encode(this);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}

    }

    public byte[] convertToEWKB(int srs) throws GeometryOperationNotSupportedException, GeometryOperationException {
    	/*
    	 * No se sabe si la especificación de OGC soporta SRS. OGCWKBEncoder no.
    	 */

    	try {
			return new PostGISEWKBEncoder().encode(this);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}
    }

    public byte[] convertToEWKBForcingType(int srs, int type) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
    	/*
    	 * No se sabe si la especificación de OGC soporta SRS. OGCWKBEncoder no.
    	 */
    	Geometry geom = this;
        if(this.getType() != type){
            com.vividsolutions.jts.geom.Geometry jts = getJTS();
            jts = JTSUtils.convertTypes(jts, this.getType(), type);

            geom = JTSUtils.createGeometry(jts);
        }
    	try {
			return new PostGISEWKBEncoder().encode(geom);
		} catch (Exception e) {
			throw new GeometryOperationException(e);
		}

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#convertToWKT()
     */
    public String convertToWKT() throws GeometryOperationNotSupportedException, GeometryOperationException {
        int subType = getGeometryType().getSubType();

        EWKTWriter writer = null;

        switch (subType) {
        case Geometry.SUBTYPES.GEOM3D:
            writer = new EWKTWriter(3, false);
            break;
        case Geometry.SUBTYPES.GEOM2DM:
            writer = new EWKTWriter(3, true);
            break;
        case Geometry.SUBTYPES.GEOM3DM:
            writer = new EWKTWriter(4, true);
            break;

        default:
            writer = new EWKTWriter(2, false);
            break;
        }
        com.vividsolutions.jts.geom.Geometry jts = getJTS();
        return writer.write(jts);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#buffer(double)
     */
    public org.gvsig.fmap.geom.Geometry buffer(double distance) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().buffer(distance));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#snapTo(org.gvsig.fmap.geom.Geometry,
     * double)
     */
    public org.gvsig.fmap.geom.Geometry snapTo(org.gvsig.fmap.geom.Geometry other, double snapTolerance)
        throws GeometryOperationNotSupportedException, GeometryOperationException {
        Geometry result = null;
        GeometrySnapper snapper = new GeometrySnapper(getJTS());
        com.vividsolutions.jts.geom.Geometry jts_result = snapper.snapTo(((GeometryJTS) other).getJTS(), snapTolerance);
        result = JTSUtils.createGeometry(jts_result);
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getInteriorPoint()
     */
    public Point getInteriorPoint() throws GeometryOperationNotSupportedException, GeometryOperationException {

        com.vividsolutions.jts.geom.Geometry geometry = getJTS();
        com.vividsolutions.jts.geom.Point point = geometry.getInteriorPoint();
        Geometry result = JTSUtils.createGeometry(point);
        return (Point) result;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#isValid()
     */
    public boolean isValid() {
        return getJTS().isValid();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getValidationStatus()
     */
    public ValidationStatus getValidationStatus() {
        DefaultValidationStatus status = new DefaultValidationStatus(ValidationStatus.VALID, null);
        com.vividsolutions.jts.geom.Geometry jtsgeom = null;
        try {
            jtsgeom = this.getJTS();
            IsValidOp validOp = new IsValidOp(jtsgeom);
            if (validOp != null) {
                status.setValidationError(validOp.getValidationError());
            }
        } catch (Exception ex) {
            status.setStatusCode(ValidationStatus.CURRUPTED);
            status.setMesage("The geometry is corrupted.");
            if (this instanceof OrientableSurface) {
                int vertices = ((OrientableSurface) this).getNumVertices();
                if (vertices < 3) {
                    status.setStatusCode(ValidationStatus.TOO_FEW_POINTS);
                    status.setMesage(TopologyValidationError.errMsg[TopologyValidationError.TOO_FEW_POINTS]);
                }
            } else if (this instanceof OrientableCurve) {
                int vertices = ((OrientableCurve) this).getNumVertices();
                if (vertices < 2) {
                    status.setStatusCode(ValidationStatus.TOO_FEW_POINTS);
                    status.setMesage(TopologyValidationError.errMsg[TopologyValidationError.TOO_FEW_POINTS]);
                }
            }
        }
        return status;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#makeValid()
     */
    public org.gvsig.fmap.geom.Geometry makeValid() {
        try {
            ValidationStatus vs = this.getValidationStatus();
            if (vs.isValid()) {
                return this;
            }
            Geometry g = null;
            switch (vs.getStatusCode()) {
            case Geometry.ValidationStatus.RING_SELF_INTERSECTION:
            case Geometry.ValidationStatus.SELF_INTERSECTION:
                g = this.buffer(0);
                if (g.isValid()) {
                    return g;
                }
                break;

            case Geometry.ValidationStatus.TOO_FEW_POINTS:
                if (this instanceof OrientableCurve) {
                    int vertices = ((OrientableCurve) this).getNumVertices();
                    if (vertices < 2) {
                        return null; // new
                                     // DefaultNullGeometry(this.getGeometryType());
                    }
                }
                if (this instanceof OrientableSurface) {
                    int vertices = ((OrientableSurface) this).getNumVertices();
                    if (vertices < 3) {
                        return null; // new
                                     // DefaultNullGeometry(this.getGeometryType());
                    }
                }
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getBounds2D()
     */
    public Rectangle2D getBounds2D() {
        com.vividsolutions.jts.geom.Envelope envInternal = getJTS().getEnvelopeInternal();
        return new Rectangle2D.Double(envInternal.getMinX(), envInternal.getMinY(), envInternal.getWidth(),
            envInternal.getHeight());
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#getBounds()
     */
    public Rectangle getBounds() {
        return this.getShape().getBounds();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getInternalShape()
     */
    public Shape getInternalShape() {
        return getShape();
    }

    public void rotate(double radAngle, double basex, double basey) {

        AffineTransform at = new AffineTransform();
        at.rotate(radAngle, basex, basey);
        this.transform(at);
    }

    public void move(double dx, double dy) {

        AffineTransform at = new AffineTransform();
        at.translate(dx, dy);
        this.transform(at);
    }

    public void scale(Point basePoint, double sx, double sy) {

        AffineTransform at = new AffineTransform();
        at.setToTranslation(basePoint.getX(), basePoint.getY());
        at.scale(sx, sy);
        at.translate(-basePoint.getX(), -basePoint.getY());
        this.transform(at);
    }

    public Geometry[] closestPoints(Geometry other) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        Point[] points = null;

        Coordinate[] jts_points = DistanceOp.nearestPoints(getJTS(), ((GeometryJTS) other).getJTS());
        points = new Point[jts_points.length];
        for (int i = 0; i < jts_points.length; i++) {
            try {
                points[i] = JTSUtils.createPoint(this.getGeometryType(), jts_points[i]);
            } catch (CreateGeometryException e) {
                throw new GeometryOperationException(e);
            }
        }

        return (Geometry[]) points;
    }

    public Geometry convexHull() throws GeometryOperationNotSupportedException, GeometryOperationException {

        return JTSUtils.createGeometry(getJTS().convexHull());
    }

    public Geometry difference(Geometry other) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().difference(((GeometryJTS) other).getJTS()));
    }

    public Geometry intersection(Geometry other) throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().intersection(((GeometryJTS) other).getJTS()));
    }

    public Geometry union(Geometry other) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().union(((GeometryJTS) other).getJTS()));
    }

    public org.gvsig.fmap.geom.primitive.Point centroid() throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        try {
            return JTSUtils.createPoint(this.getGeometryType(), getJTS().getCentroid().getCoordinate());
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        }
    }

    protected void notifyDeprecated(String message) {
        logger.info(message);

    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#ensureOrientation(boolean)
     */
    public boolean ensureOrientation(boolean ccw) throws GeometryOperationNotSupportedException, GeometryOperationException {
        if(ccw!=isCCW()){
            flip();
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#out(org.gvsig.fmap.geom.Geometry)
     */
    public boolean out(Geometry geometry) throws GeometryOperationNotSupportedException, GeometryOperationException {
        GeometryJTS otherJtsGeom = (GeometryJTS)geometry;
        return (!contains(otherJtsGeom) && !intersects(otherJtsGeom));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GeometryJTS) {
            return this.getJTS().equals(((GeometryJTS) obj).getJTS());
        }
        return false;
    }

    public String toString(){
        return this.getGeometryType().getFullName();
    }


}
