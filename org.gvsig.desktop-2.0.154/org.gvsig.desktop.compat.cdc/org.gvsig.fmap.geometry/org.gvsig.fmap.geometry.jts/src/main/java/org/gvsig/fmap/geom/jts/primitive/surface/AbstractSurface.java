/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface;

import org.gvsig.fmap.geom.jts.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.primitive.SurfaceAppearance;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractSurface extends AbstractPrimitive implements Surface {

    /**
     *
     */
    private static final long serialVersionUID = 452141349577139850L;

    /**
    *
    */
   protected AbstractSurface(int type, int subtype) {
       super(type, subtype);
   }


   /* (non-Javadoc)
    * @see org.gvsig.fmap.geom.primitive.Surface#setSurfaceAppearance(org.gvsig.fmap.geom.primitive.SurfaceAppearance)
    */
   public void setSurfaceAppearance(SurfaceAppearance app) {
       // TODO Auto-generated method stub

   }

   /* (non-Javadoc)
    * @see org.gvsig.fmap.geom.primitive.Surface#getSurfaceAppearance()
    */
   public SurfaceAppearance getSurfaceAppearance() {
       // TODO Auto-generated method stub
       return null;
   }
}
