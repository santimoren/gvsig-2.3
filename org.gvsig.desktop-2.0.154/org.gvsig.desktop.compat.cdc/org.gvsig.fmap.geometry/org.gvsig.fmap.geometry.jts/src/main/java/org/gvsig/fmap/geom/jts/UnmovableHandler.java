/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts;

import com.vividsolutions.jts.geom.Point;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.handler.AbstractHandler;


/**
 * This class is for snapping purposes only, don't allows modify the geometries.
 *
 * @author fdiaz
 *
 */
public class UnmovableHandler extends AbstractHandler {

    protected static final Logger logger = LoggerFactory.getLogger(UnmovableHandler.class);

    public UnmovableHandler(java.awt.geom.Point2D p) {
        super();
        point = p;
    }

    public UnmovableHandler(Point p) {
        super();
        point = new java.awt.geom.Point2D.Double(p.getX(), p.getY());
        ;
    }

    public UnmovableHandler(double x, double y) {
        super();
        point = new java.awt.geom.Point2D.Double(x, y);
        ;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.handler.Handler#move(double, double)
     */
    public void move(double x, double y) {
        String message = "Can't move the coordinates of a UnmovableHandler.";
        logger.info(message);
        throw new UnsupportedOperationException(message);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
     */
    public void set(double x, double y) {
        String message = "Can't change the coordinates of a UnmovableHandler.";
        logger.info(message);
        throw new UnsupportedOperationException(message);
    }

}
