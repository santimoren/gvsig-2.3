/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.polygon;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geomgraph.Position;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.buffer.OffsetCurveBuilder;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2DM;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.BaseLine2DM;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point2DM;
import org.gvsig.fmap.geom.jts.primitive.ring.Ring2DM;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.primitive.SurfaceAppearance;


/**
 * @author fdiaz
 *
 */
public class Polygon2DM extends BaseLine2DM implements Polygon {

    /**
     *
     */
    private static final long serialVersionUID = 3755255543825597334L;

    List<Ring> interiorRings = new ArrayList<Ring>();

    /**
     * @param subtype
     */
    public Polygon2DM() {
        super(Geometry.TYPES.POLYGON);
    }

    /**
    *
    */
    public Polygon2DM(Coordinate[] coordinates) {
        super(Geometry.TYPES.POLYGON, coordinates);
        closePrimitive();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#setSurfaceAppearance(org.gvsig.fmap.geom.primitive.SurfaceAppearance)
     */
    public void setSurfaceAppearance(SurfaceAppearance app) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        Polygon2DM cloned = new Polygon2DM(cloneCoordinates().toCoordinateArray());
        for (int i = 0; i < getNumInteriorRings(); i++){
            cloned.addInteriorRing((Ring)getInteriorRing(i).cloneGeometry());
        }
        return cloned;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getSurfaceAppearance()
     */
    public SurfaceAppearance getSurfaceAppearance() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getNumInteriorRings()
     */
    public int getNumInteriorRings() {
        return interiorRings.size();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getInteriorRing(int)
     */
    public Ring getInteriorRing(int index) {
        return interiorRings.get(index);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Ring)
     */
    public void addInteriorRing(Ring ring) {
        interiorRings.add(ring);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Line)
     */
    public void addInteriorRing(Line line) {
        Ring2DM ring = new Ring2DM();
        ring.ensureCapacity(line.getNumVertices());
        for(int i=0; i<line.getNumVertices(); i++){
            Point2DM vertex = (Point2DM)line.getVertex(i);
            ring.addVertex(new Point2DM(vertex.getX(), vertex.getY(),vertex.getM()));
        }
        ring.closePrimitive();
        interiorRings.add(ring);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.
     * geom.primitive.Polygon)
     */
    public void addInteriorRing(Polygon polygon) {
        Ring2DM ring = new Ring2DM();
        ring.ensureCapacity(polygon.getNumVertices());
        for(int i=0; i<polygon.getNumVertices(); i++){
            Point2DM vertex = (Point2DM)polygon.getVertex(i);
            ring.addVertex(new Point2DM(vertex.getX(), vertex.getY(),vertex.getM()));
        }
        ring.closePrimitive();
        interiorRings.add(ring);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#removeInteriorRing(int)
     */
    public void removeInteriorRing(int index) {
        interiorRings.remove(index);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2DM();
        multiLine.addPrimitive(new Line2DM(coordinates.toCoordinateArray()));
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2DM();
        multiPolygon.addPrimitive(this);
        return multiPolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSPolygon(coordinates, interiorRings);
    }



    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * )
     */
    public PathIterator getPathIterator(AffineTransform at) {
        PolygonIterator pi = new PolygonIterator(this, at);
        return pi;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#ensureOrientation(boolean)
     */
    public boolean ensureOrientation(boolean ccw) throws GeometryOperationNotSupportedException, GeometryOperationException {
        boolean result = super.ensureOrientation(ccw);
        for (Iterator<Ring> iterator = interiorRings.iterator(); iterator.hasNext();) {
            GeometryJTS ring = (GeometryJTS) iterator.next();
            ring.ensureOrientation(!ccw);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#flip()
     */
    @Override
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        super.flip();
        for (Iterator<Ring> iterator = interiorRings.iterator(); iterator.hasNext();) {
            GeometryJTS ring = (GeometryJTS) iterator.next();
            ring.flip();
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#transform(java.awt.geom.AffineTransform)
     */
    @Override
    public void transform(AffineTransform at) {
        super.transform(at);
        for (Iterator<Ring> iterator = interiorRings.iterator(); iterator.hasNext();) {
            GeometryJTS ring = (GeometryJTS) iterator.next();
            ring.transform(at);
        }
    }


    /* (non-Javadoc)
    * @see org.gvsig.fmap.geom.Geometry#offset(double)
    */
      public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
          return JTSUtils.createGeometry(getJTS().buffer(distance));
      }

      /* (non-Javadoc)
       * @see org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#reProject(org.cresques.cts.ICoordTrans)
       */
      @Override
      public void reProject(ICoordTrans ct) {
          super.reProject(ct);
          for (Iterator<Ring> iterator = interiorRings.iterator(); iterator.hasNext();) {
              GeometryJTS ring = (GeometryJTS) iterator.next();
              ring.reProject(ct);
          }
          if (coordinates.size()>=2 && !isClosed()) {
              closePrimitive();
          }
      }

      @Override
      public boolean equals(Object obj) {
          boolean res = super.equals(obj);
          if(res && obj instanceof Polygon2DM){
              Polygon2DM other = (Polygon2DM)obj;
              if(this.getNumVertices() != other.getNumVertices()){
                  return false;
              }
              for(int i=0; i < this.getNumVertices(); i++){
                  if(other.coordinates.get(i).getOrdinate(2)!=this.coordinates.get(i).getOrdinate(2)){
                      return false;
                  };
              }
              if(this.getNumInteriorRings() != other.getNumInteriorRings()){
                  return false;
              }
              for(int i=0; i < this.getNumInteriorRings(); i++){
                  if(!other.getInteriorRing(i).equals(this.getInteriorRing(i))){
                      return false;
                  }
              }
              return true;
          } else {
              return false;
          }
      }
}
