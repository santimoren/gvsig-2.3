/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractMultiCurve extends AbstractMultiPrimitive implements MultiCurve {

    /**
     *
     */
    private static final long serialVersionUID = 5498201227873886542L;


    /**
     * @param multiline
     * @param subtype
     */
    public AbstractMultiCurve(int type, int subtype) {
        super(type, subtype);
    }

    /**
     * @param type
     */
    public AbstractMultiCurve(int subtype) {
        super(Geometry.TYPES.MULTICURVE, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiCurve#getCurveAt(int)
     */
    public Curve getCurveAt(int index) {
        return (Curve)primitives.get(index);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiCurve#addCurve(org.gvsig.fmap.geom.primitive.Curve)
     */
    public void addCurve(Curve curve) {
        primitives.add((Primitive) fixPrimitive(curve));
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        MultiCurveIterator pi = new MultiCurveIterator(at);
        return pi;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(getPathIterator(null), false, 0);
    }

    protected class MultiCurveIterator extends GeneralPathXIterator {

        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;
        private int index = 0;
        private List<PathIterator>iterators = new ArrayList<PathIterator>(primitives.size());

        /**
         * Creates a new PointIterator object.
         *
         * @param p
         *            The polygon
         * @param at
         *            The affine transform applied to coordinates during
         *            iteration
         */
        public MultiCurveIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
                Primitive primitive = (Primitive) iterator.next();
                iterators.add(primitive.getPathIterator(at));
            }
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            PathIterator pathIteratorPrimitive = iterators.get(index);
            pathIteratorPrimitive.next();
            if(pathIteratorPrimitive.isDone()){
                index++;
                done = (index==primitives.size());
            }
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            return iterators.get(index).currentSegment(coords);
        }

        /*
         * (non-Javadoc)
         *
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            return iterators.get(index).currentSegment(coords);
        }
    }

}
