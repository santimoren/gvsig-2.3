/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.arc;

import java.awt.geom.PathIterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;



/**
 * @author fdiaz
 *
 */
public class Arc2D extends AbstractArc {

    /**
     *
     */
    private static final long serialVersionUID = 3414562338763885954L;

    /**
     * @param subtype
     */
    public Arc2D() {
        super(Geometry.SUBTYPES.GEOM2D);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(org.gvsig.fmap.geom.primitive.Point, double, double, double)
     */
    public void setPoints(Point center, double radius, double startAngle, double angleExt) {

        double diameter = radius*2;
        double x;
        double y;
        double start;
        double extent;
        double angleOffset;
        final double pi2 = Math.PI*2;

        angleOffset = 0;
        if( angleExt<0 ) {
            angleOffset = pi2 + angleExt;
            angleExt = Math.abs(angleExt);
        }
        x = center.getX() - radius;
        y = center.getY() - radius;

        if( angleExt > 0 && (angleExt % pi2) == 0  ) {
            start = 0;
            extent = 360;
        } else {
            angleExt = angleExt % pi2; //Asumimos que aqui angleExt es siempre positivo
            startAngle = Math.abs(startAngle) % pi2;
            start = Math.toDegrees( pi2 - startAngle + angleOffset) ;
            extent = - Math.toDegrees(pi2 - angleExt);
        }


        double angleStart = Math.toRadians(-start);
        double initX = x + (Math.cos(angleStart) * 0.5 + 0.5) * diameter;
        double initY = y + (Math.sin(angleStart) * 0.5 + 0.5) * diameter;
        init = new Point2D(initX, initY);

        double angleEnd = Math.toRadians(-start - extent);
        double endX = x + (Math.cos(angleEnd) * 0.5 + 0.5) * diameter;
        double endY = y + (Math.sin(angleEnd) * 0.5 + 0.5) * diameter;
        end =  new Point2D(endX, endY);

        double angleMiddle = Math.toRadians(-start - extent)/2;
        double middleX = x + (Math.cos(angleMiddle) * 0.5 + 0.5) * diameter;
        double middleY = y + (Math.sin(angleMiddle) * 0.5 + 0.5) * diameter;
        middle =  new Point2D(middleX, middleY);
}

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#getCenterPoint()
     */
    public Point getCenterPoint() {
        ((PointJTS)init).getJTS();
        Point2D center = new Point2D(JTSUtils.getCircumcentre(init, middle, end));
        return center;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        Arc2D arc2D = new Arc2D();
        Point clonedInit = (Point)init.cloneGeometry();
        Point clonedMiddle = (Point)middle.cloneGeometry();
        Point clonedEnd = (Point)end.cloneGeometry();
        arc2D.setPoints(clonedInit, clonedMiddle, clonedEnd);
        return arc2D;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#fixPoint(org
     * .gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point2D) {
            return point;
        } else {
            return new Point2D(point.getX(), point.getY());
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        PathIterator pi = getPathIterator(null);
        ArrayListCoordinateSequence coordinates = new ArrayListCoordinateSequence();

        double coords[] = new double[6];
        while (!pi.isDone()) {
            switch (pi.currentSegment(coords)) {
            case PathIterator.SEG_MOVETO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                break;
            case PathIterator.SEG_LINETO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                break;
            case PathIterator.SEG_QUADTO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                coordinates.add(new Coordinate(coords[2], coords[3]));
                break;
            case PathIterator.SEG_CUBICTO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                coordinates.add(new Coordinate(coords[2], coords[3]));
                coordinates.add(new Coordinate(coords[4], coords[5]));
                break;
            case PathIterator.SEG_CLOSE:
                coordinates.add(coordinates.get(0));
                break;
            }
            pi.next();
        }
        return JTSUtils.createJTSLineString(coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint2D();
        Coordinate[] coordinates = getJTS().getCoordinates();
        multiPoint.ensureCapacity(coordinates.length);
        for (int i = 0; i < coordinates.length; i++) {
            multiPoint.addPoint(new Point2D(coordinates[i]));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2D();
        Line line = new Line2D(getJTS().getCoordinates());
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2D();
        Polygon polygon = new Polygon2D(getJTS().getCoordinates());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }

}
