/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2DM;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public class MultiLine2DM extends AbstractMultiLine {


    /**
     *
     */
    private static final long serialVersionUID = 7970383739561430594L;

    /**
     * @param type
     * @param subtype
     */
    public MultiLine2DM() {
        super(Geometry.SUBTYPES.GEOM2DM);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint2DM();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Line2DM line = (Line2DM) iterator.next();
            MultiPoint points = line.toPoints();
            multiPoint.ensureCapacity(multiPoint.getPrimitivesNumber()+points.getPrimitivesNumber());
            for(int i=0; i<points.getPrimitivesNumber(); i++){
                multiPoint.addPoint(points.getPointAt(i));
            }
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        return this;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2DM();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Line2DM line = (Line2DM) iterator.next();
            MultiPolygon polygons = line.toPolygons();
            multiPolygon.ensureCapacity(multiPolygon.getPrimitivesNumber()+polygons.getPrimitivesNumber());
            for(int i=0; i<polygons.getPrimitivesNumber(); i++){
                multiPolygon.addSurface((Polygon)polygons.getPrimitiveAt(i));
            }
        }
        return multiPolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        MultiLine2DM clone = new MultiLine2DM();
        return clonePrimitives(clone);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return 3;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.aggregate.AbstractMultiLine#fixLine(org.gvsig.fmap.geom.primitive.Line)
     */
    @Override
    protected Geometry fixPrimitive(Primitive primitive) {
        if(primitive instanceof Line2DM){
            return primitive;
        }

        if(primitive.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM2DM){
            try {
                return primitive.toLines();
            } catch (GeometryException e) {
                String message = "Can't convert primitive to lines";
                logger.warn(message);
                throw new RuntimeException(message);
            }
        }

        String message = "Only 2D primitives can be fixed to MultiLine2DM";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        MultiLine2DM result = new MultiLine2DM();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            Geometry offset = primitive.offset(distance);
            if(offset instanceof MultiLine){
                MultiLine multiOffset = (MultiLine)offset;
                for(int i=0; i<multiOffset.getPrimitivesNumber(); i++){
                    result.addPrimitive(multiOffset.getPrimitiveAt(i));
                }
            } else {
                result.addPrimitive((Primitive)primitive.offset(distance));
            }
        }
        return result;
    }
}
