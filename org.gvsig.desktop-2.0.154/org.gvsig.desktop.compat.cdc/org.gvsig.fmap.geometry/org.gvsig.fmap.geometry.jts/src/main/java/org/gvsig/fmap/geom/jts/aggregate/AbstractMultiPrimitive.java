/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractMultiPrimitive extends AbstractAggregate implements MultiPrimitive {

    /**
     *
     */
    private static final long serialVersionUID = -5993428771359742467L;

    /**
     * @param type
     * @param subtype
     */
    public AbstractMultiPrimitive(int type, int subtype) {
        super(type, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiPrimitive#addPrimitive(org.gvsig.fmap.geom.primitive.Primitive)
     */
    public void addPrimitive(Primitive primitive) {
        Geometry geom = fixPrimitive(primitive);
        if (geom != null) {
            if (geom instanceof MultiPrimitive) {
                for (int i = 0; i < ((MultiPrimitive) geom).getPrimitivesNumber(); i++) {
                    addPrimitive(((MultiPrimitive) geom).getPrimitiveAt(i));
                }
            } else {
                primitives.add((Primitive) geom);
            }
        }
    }

    /**
     * @param primitive
     * @return
     */
    protected abstract Geometry fixPrimitive(Primitive primitive);


    protected Geometry clonePrimitives(AbstractMultiPrimitive clone) {
        clone.ensureCapacity(primitives.size());
        for (Iterator iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            clone.addPrimitive((Primitive)primitive.cloneGeometry());
        }
        return clone;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getHandlers(int)
     */
    public Handler[] getHandlers(int type) {
        List<Handler> handlers = new ArrayList<Handler>();
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            Handler[] primitiveHandlers = primitive.getHandlers(type);
            for (int i = 0; i < primitiveHandlers.length; i++) {
                handlers.add(primitiveHandlers[i]);
            }
        }
        return handlers.toArray(new Handler[handlers.size()]);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.Aggregate#union()
     */
    public Geometry union() throws GeometryOperationException, GeometryOperationNotSupportedException {
        Geometry result = null;
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            if(result==null){
                result = primitive;
            } else {
                result.union(primitive);
            }
        }
        return result;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.Aggregate#intersection()
     */
    public Geometry intersection() throws GeometryOperationException, GeometryOperationNotSupportedException {
        Geometry result = null;
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Primitive primitive = (Primitive) iterator.next();
            if(result==null){
                result = primitive;
            } else {
                result.intersection(primitive);
            }
        }
        return result;
    }


}
