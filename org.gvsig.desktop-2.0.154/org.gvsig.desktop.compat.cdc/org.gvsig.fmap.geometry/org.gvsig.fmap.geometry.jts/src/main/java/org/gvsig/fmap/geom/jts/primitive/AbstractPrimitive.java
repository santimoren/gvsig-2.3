/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive;

import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.jts.AbstractGeometry;
import org.gvsig.fmap.geom.jts.UnmovableHandler;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractPrimitive extends AbstractGeometry implements Primitive{

    /**
     *
     */
    private static final long serialVersionUID = -8143412151139464535L;


    /**
     *
     */
    protected AbstractPrimitive(int type, int subtype) {
        super(type, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getHandlers(int)
     */
    public Handler[] getHandlers(int type) {
        List<Handler> handlers = new ArrayList<Handler>();
        Handler handler;
            Coordinate[] coordinates = getJTS().getCoordinates();
            for (int i = 0; i < coordinates.length; i++) {
                handler = new UnmovableHandler(coordinates[i].x, coordinates[i].y);
                handlers.add(handler);
            }

            PathIterator pi = getPathIterator(null);

            double coords[] = new double[6];
            while (!pi.isDone()) {
                switch (pi.currentSegment(coords)) {
                case PathIterator.SEG_MOVETO:
                    handler = new UnmovableHandler(coords[0], coords[1]);
                    handlers.add(handler);
                    break;
                case PathIterator.SEG_LINETO:
                    handler = new UnmovableHandler(coords[0], coords[1]);
                    handlers.add(handler);
                    break;
                case PathIterator.SEG_QUADTO:
                    handler = new UnmovableHandler(coords[0], coords[1]);
                    handlers.add(handler);
                    handler = new UnmovableHandler(coords[2], coords[3]);
                    handlers.add(handler);
                    break;
                case PathIterator.SEG_CUBICTO:
                    handler = new UnmovableHandler(coords[0], coords[1]);
                    handlers.add(handler);
                    handler = new UnmovableHandler(coords[2], coords[3]);
                    handlers.add(handler);
                    handler = new UnmovableHandler(coords[4], coords[5]);
                    handlers.add(handler);
                    break;
                case PathIterator.SEG_CLOSE:
                    break;
                }
                pi.next();
            }
        return handlers.toArray(new Handler[handlers.size()]);
    }
}
