/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.filledspline;

import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.GeometryJTS;
import org.gvsig.fmap.geom.jts.primitive.curve.spline.BaseSpline2D;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.FilledSpline;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.primitive.SurfaceAppearance;


/**
 * @author fdiaz
 *
 */
public class FilledSpline2D extends BaseSpline2D implements FilledSpline {

    /**
     *
     */
    private static final long serialVersionUID = 8857987884304738266L;

    /**
     *
     */
    public FilledSpline2D() {
        super(Geometry.TYPES.FILLEDSPLINE);
    }

    /**
     * @param coordinates
     */
    public FilledSpline2D(Coordinate[] coordinates) {
        super(Geometry.TYPES.FILLEDSPLINE, coordinates);
        closePrimitive();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#setSurfaceAppearance(org.gvsig.fmap.geom.primitive.SurfaceAppearance)
     */
    public void setSurfaceAppearance(SurfaceAppearance app) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getSurfaceAppearance()
     */
    public SurfaceAppearance getSurfaceAppearance() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getNumInteriorRings()
     */
    public int getNumInteriorRings() {
        String message = "Calling deprecated method getNumInteriorRings of a filled spline";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getInteriorRing(int)
     */
    public Ring getInteriorRing(int index) {
        String message = "Calling deprecated method getInteriorRing of a filled spline";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Ring)
     */
    public void addInteriorRing(Ring ring) {
        String message = "Calling deprecated method addInteriorRing of a filled spline";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Line)
     */
    public void addInteriorRing(Line line) {
        String message = "Calling deprecated method addInteriorRing of a filled spline";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Polygon)
     */
    public void addInteriorRing(Polygon polygon) {
        String message = "Calling deprecated method addInteriorRing of a filled spline";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#removeInteriorRing(int)
     */
    public void removeInteriorRing(int index) {
        String message = "Calling deprecated method removeInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new FilledSpline2D(cloneCoordinates().toCoordinateArray());
        }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSPolygon(getSplineCoordinates());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return JTSUtils.createGeometry(getJTS().buffer(distance));
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.curve.spline.AbstractSpline#reProject(org.cresques.cts.ICoordTrans)
     */
    @Override
    public void reProject(ICoordTrans ct) {
        super.reProject(ct);
        if (coordinates.size()>=2 && !isClosed()) {
            closePrimitive();
        }
    }
}
