
package org.gvsig.fmap.geom.jts.transform;

import org.gvsig.fmap.geom.transform.Transform;


/**
 * Exception thrown when trying to compute an inverse transform of a transform
 * that does not allows this feature.
 * @author dlegland
 */
public class NonInvertibleTransformException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected Transform transform;
    
    public NonInvertibleTransformException() {
    	this.transform = null;
    }
    
    public NonInvertibleTransformException(Transform transform) {
    	this.transform = transform;
    }
    
    public Transform getTransform() {
    	return transform;
    }
}
