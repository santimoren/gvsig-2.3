/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.spatialindex;

import com.infomatiq.jsi.IntProcedure;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.SpatialIndexFactory;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.visitor.Visitor;

import com.infomatiq.jsi.Rectangle;
import com.infomatiq.jsi.rtree.RTree;
import java.util.Collection;
import java.util.Properties;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.exception.BaseException;

public class SpatialIndexJSI extends AbstractSpatialIndex implements SpatialIndex {

    private RTree index = null;

    class Values implements IntProcedure {

        List elements = new ArrayList();

        public boolean execute(int arg0) {
            elements.add(new Integer(arg0));
            return true;
        }

        public List getElements() {
            return elements;
        }
    }

    public SpatialIndexJSI(GeometryManager geometryManager, SpatialIndexFactory factory, DynObject parameters) {
        super(geometryManager, factory, parameters);
        this.index = null;
        this.open();
    }

    public void open() {
        this.index = new RTree();
        Properties props = new Properties();
        if( this.getParameter("MaxNodeEntries")!=null ) {
            props.setProperty("MaxNodeEntries", ((Integer)this.getParameter("MaxNodeEntries")).toString());
        }
        if( this.getParameter("MinNodeEntries")!=null ) {
            props.setProperty("MinNodeEntries", ((Integer)this.getParameter("MinNodeEntries")).toString());
        }
        this.index.init(props);
    }

    public void close() {
        this.index = null;
    }

    private Rectangle asJSI(Envelope envelope) {
        Point min = envelope.getLowerCorner();
        Point max = envelope.getUpperCorner();

        Rectangle rectangle =
            new Rectangle((float) min.getX(), (float) min.getY(),
                (float) max.getX(), (float) max.getY());
        return rectangle;
    }

    public long size() {
        return this.index.size();
    }

    public void query(Envelope envelope, final Visitor visitor) {
        this.index.intersects(asJSI(envelope), new IntProcedure() {
            public boolean execute(int i) {
                try {
                    visitor.visit(new Integer(i));
                } catch (BaseException ex) {
                    return false;
                }
                return true;
            }
        });
    }

    public Iterator query(Envelope envelope, final long limit) {
        final List results = new ArrayList();

        this.index.intersects(asJSI(envelope), new IntProcedure() {
            public boolean execute(int i) {
                results.add(new Integer(i));
                return results.size()<limit;
            }
        });
        return results.iterator();
    }

    public Iterator queryNearest(Envelope envelope, long limit) {
        Collection results = this.index.nearest(asJSI(envelope), (int)limit);
        return results.iterator();
    }

    public Iterator queryAll() {
        return this.index.iterator();
    }

    public void insert(Envelope envelope, Object data) {
        Integer number = (Integer) this.coerceData(data);
        if( number == null ) {
            throw new IllegalArgumentException("null data are not allowed.");
        }
        this.index.add(asJSI(envelope), number.intValue());
    }

    public boolean remove(Envelope envelope, Object data) {
        Integer number = (Integer) this.coerceData(data);
        if( number == null ) {
            throw new IllegalArgumentException("null data are not allowed.");
        }
        return this.index.delete(asJSI(envelope), number.intValue());
    }

    public void removeAll() {
        this.close();
        this.open();
    }


}
