/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.aggregate;

import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3DM;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3DM;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3DM;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;


/**
 * @author fdiaz
 *
 */
public class MultiPoint3DM extends AbstractMultiPoint {

    /**
     *
     */
    private static final long serialVersionUID = -2230359991187613190L;

    /**
     * @param subtype
     */
    public MultiPoint3DM() {
        super(Geometry.SUBTYPES.GEOM3DM);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        MultiPoint3DM clone = new MultiPoint3DM();
        return clonePrimitives(clone);

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return 4;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(new PointIterator(null), false, 0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        return this;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3DM();
        Line line = new Line3DM();
        line.ensureCapacity(primitives.size());
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Point3DM point = (Point3DM) iterator.next();
            line.addVertex(point);
        }
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.aggregate.MultiLine#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon3DM();
        Polygon polygon = new Polygon3DM();
        polygon.ensureCapacity(primitives.size());
        for (Iterator<Primitive> iterator = primitives.iterator(); iterator.hasNext();) {
            Point3DM point = (Point3DM) iterator.next();
            polygon.addVertex(point);
        }
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.aggregate.AbstractMultiPrimitive#fixPrimitive(org.gvsig.fmap.geom.primitive.Primitive)
     */
    @Override
    protected Geometry fixPrimitive(Primitive primitive) {

        if(primitive instanceof Point3DM){
            return primitive;
        }

        if(primitive instanceof Point){
            Point point = (Point)primitive;
            if(point instanceof Point3D){
                return new Point3DM(point.getX(), point.getY(), ((Point3DM)point).getZ(), 0);
            }
            return new Point3DM(point.getX(), point.getY(), 0, 0);
        }

        if(primitive.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM){
            try {
                return primitive.toPoints();
            } catch (GeometryException e) {
                String message = "Can't convert primitive to points";
                logger.warn(message);
                throw new RuntimeException(message);
            }
        }

        String message = "Only 3DM primitives can be fixed to MultiPoint3DM";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);

    }

    protected Point fixPoint(Point point) {
        if (point instanceof Point3DM) {
            return point;
        } else if (point instanceof Point3D) {
            return new Point3DM(point.getX(), point.getY(), ((Point3D)point).getZ(), 0);
        } else {
            return new Point3DM(point.getX(), point.getY(), 0, 0);
        }
    }
}
