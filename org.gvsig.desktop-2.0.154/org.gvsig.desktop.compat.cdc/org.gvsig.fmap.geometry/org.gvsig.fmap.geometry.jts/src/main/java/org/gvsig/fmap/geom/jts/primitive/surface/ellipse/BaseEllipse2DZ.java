/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.ellipse;

import java.awt.geom.PathIterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon3D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line3D;
import org.gvsig.fmap.geom.jts.primitive.point.Point3D;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon3D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;


/**
 * @author fdiaz
 *
 */
public abstract class BaseEllipse2DZ extends AbstractEllipse {

    /**
     *
     */
    private static final long serialVersionUID = 1642655515234226372L;
    private double zValue = Double.NaN;

    /**
     * @param subtype
     */
    public BaseEllipse2DZ(int type) {
        super(type, Geometry.SUBTYPES.GEOM3D);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.jts.primitive.curve.line.AbstractLine#fixPoint(org
     * .gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point3D) {
            Point3D point3D = (Point3D) point;
            if (Double.isNaN(zValue) && !(Double.isNaN(point3D.getZ()))) {
                zValue = point3D.getZ();
                if (init!=null) {
                    ((Point3D) init).setZ(zValue);
                }
                if (end!=null) {
                    ((Point3D) end).setZ(zValue);
                }
                return point3D;
            }
        }
        return new Point3D(point.getX(), point.getY(), Double.isNaN(zValue)?0:zValue);
    }

    /**
     * @return
     */
    protected ArrayListCoordinateSequence getJTSCoordinates() {
        PathIterator pi = getPathIterator(null);
        ArrayListCoordinateSequence coordinates = new ArrayListCoordinateSequence();

        double coords[] = new double[6];
        while (!pi.isDone()) {
            switch (pi.currentSegment(coords)) {
            case PathIterator.SEG_MOVETO:
                coordinates.add(new Coordinate(coords[0], coords[1], zValue));
                break;
            case PathIterator.SEG_LINETO:
                coordinates.add(new Coordinate(coords[0], coords[1], zValue));
                break;
            case PathIterator.SEG_QUADTO:
                coordinates.add(new Coordinate(coords[0], coords[1], zValue));
                coordinates.add(new Coordinate(coords[2], coords[3], zValue));
                break;
            case PathIterator.SEG_CUBICTO:
                coordinates.add(new Coordinate(coords[0], coords[1], zValue));
                coordinates.add(new Coordinate(coords[2], coords[3], zValue));
                coordinates.add(new Coordinate(coords[4], coords[5], zValue));
                break;
            case PathIterator.SEG_CLOSE:
                coordinates.add((Coordinate)coordinates.get(0).clone());
                break;
            }
            pi.next();
        }
        return coordinates;
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint3D();
        Coordinate[] coordinates = getJTS().getCoordinates();
        multiPoint.ensureCapacity(coordinates.length);
        for (int i = 0; i < coordinates.length; i++) {
            multiPoint.addPoint(new Point3D(coordinates[i]));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine3D();
        Line line = new Line3D(getJTS().getCoordinates());
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon3D();
        Polygon polygon = new Polygon3D(getJTS().getCoordinates());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }



}
