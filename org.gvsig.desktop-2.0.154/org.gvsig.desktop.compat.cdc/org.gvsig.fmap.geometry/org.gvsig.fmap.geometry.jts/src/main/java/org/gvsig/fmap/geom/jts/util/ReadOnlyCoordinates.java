/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.util;

import java.util.Collection;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;


/**
 * @author fdiaz
 *
 */
public class ReadOnlyCoordinates implements Collection<Coordinate> {

    Coordinate coordinates[];

    /**
     *
     */
    public ReadOnlyCoordinates(Coordinate coordinates[]) {
        this.coordinates = coordinates;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#size()
     */
    public int size() {
        return this.coordinates.length;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#isEmpty()
     */
    public boolean isEmpty() {
        return this.coordinates.length==0;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        for (int i = 0; i < coordinates.length; i++) {
            if(coordinates[i].equals(o)){
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#iterator()
     */
    public Iterator<Coordinate> iterator() {
        return new Iterator<Coordinate>() {

            int i=0;

            public boolean hasNext() {
                return i<coordinates.length;
            }

            public Coordinate next() {
                return coordinates[i++];
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    /* (non-Javadoc)
     * @see java.util.Collection#toArray()
     */
    public Object[] toArray() {
        return this.coordinates;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#toArray(java.lang.Object[])
     */
    public <T> T[] toArray(T[] a) {
        return (T[]) this.coordinates;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#add(java.lang.Object)
     */
    public boolean add(Coordinate e) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see java.util.Collection#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see java.util.Collection#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        for (Iterator iterator = c.iterator(); iterator.hasNext();) {
            Object object = (Object) iterator.next();
            if(contains(object)){
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.util.Collection#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends Coordinate> c) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see java.util.Collection#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see java.util.Collection#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see java.util.Collection#clear()
     */
    public void clear() {
        throw new UnsupportedOperationException();
    }

}
