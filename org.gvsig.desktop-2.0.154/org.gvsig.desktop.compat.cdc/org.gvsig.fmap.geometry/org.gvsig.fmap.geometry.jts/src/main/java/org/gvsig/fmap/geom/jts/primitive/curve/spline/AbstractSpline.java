/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.spline;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.Collections;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.apache.commons.lang3.StringUtils;
import org.cresques.cts.ICoordTrans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.exception.ReprojectionRuntimeException;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.jts.primitive.curve.AbstractCurve;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.ReadOnlyCoordinates;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.IGeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractSpline extends AbstractCurve {

    /**
     *
     */
    private static final long serialVersionUID = -1562503359430991082L;

    private static final Logger logger = LoggerFactory.getLogger(AbstractSpline.class);

    protected ArrayListCoordinateSequence coordinates;
    protected PointJTS anyVertex;
    protected static final double SUBSEGMENTS = 30.0;

    /**
     * @param type
     * @param subtype
     */
    public AbstractSpline(int type, int subtype) {
        super(type, subtype);
    }

    /**
    *
    */
   public AbstractSpline(int type, int subtype, Coordinate[] coordinates, PointJTS aVertex) {
       this(type, subtype);
       this.coordinates = new ArrayListCoordinateSequence(new ReadOnlyCoordinates(coordinates));
       anyVertex = aVertex;
   }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSLineString(getSplineCoordinates());
    }

    protected abstract ArrayListCoordinateSequence getSplineCoordinates();


    static class Spline {
        private double y[];
        private double y2[];

        /**
         * The constructor calculates the second derivatives of the interpolating function
         * at the tabulated points xi, with xi = (i, y[i]).
         * Based on numerical recipes in C, http://www.library.cornell.edu/nr/bookcpdf/c3-3.pdf .
         * @param y Array of y coordinates for cubic-spline interpolation.
         */
        public Spline(double y[]) {
            this.y = y;
            int n = y.length;
            y2 = new double[n];
            double u[] = new double[n];
            for (int i = 1; i < n - 1; i++) {
                y2[i] = -1.0 / (4.0 + y2[i - 1]);
                u[i] = (6.0 * (y[i + 1] - 2.0 * y[i] + y[i - 1]) - u[i - 1]) / (4.0 + y2[i - 1]);
            }
            for (int i = n - 2; i >= 0; i--) {
                y2[i] = y2[i] * y2[i + 1] + u[i];
            }
        }

        /**
         * Returns a cubic-spline interpolated value y for the point between
         * point (n, y[n]) and (n+1, y[n+1), with t ranging from 0 for (n, y[n])
         * to 1 for (n+1, y[n+1]).
         * @param n The start point.
         * @param t The distance to the next point (0..1).
         * @return A cubic-spline interpolated value.
         */
        public double fn(int n, double t) {
            return t * y[n + 1] - ((t - 1.0) * t * ((t - 2.0) * y2[n] - (t + 1.0) * y2[n + 1])) / 6.0 + y[n] - t * y[n];
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig
     * .fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        point = fixPoint(point);
        coordinates.add(((PointJTS) point).getJTSCoordinate());
        anyVertex = (PointJTS) point;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.Curve#setPoints(org.gvsig.fmap.geom.primitive
     * .Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point initialPoint, Point endPoint) {
        initialPoint = fixPoint(initialPoint);
        endPoint = fixPoint(endPoint);
        coordinates.clear();
        addVertex(initialPoint);
        addVertex(endPoint);
        anyVertex = (PointJTS) endPoint;
    }

    /**
     * @param initialPoint
     * @return
     */
    protected abstract Point fixPoint(Point point);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int,
     * int)
     */
    public double getCoordinateAt(int index, int dimension) {
        return coordinates.getOrdinate(index, dimension);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int,
     * int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        coordinates.setOrdinate(index, dimension, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        coordinates.remove(index);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        return coordinates.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int,
     * org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        p = fixPoint(p);
        coordinates.add(index, ((PointJTS) p).getJTSCoordinate());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int,
     * org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        p = fixPoint(p);
        coordinates.set(index, ((PointJTS) p).getJTSCoordinate());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.
     * gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {

        PathIterator it = generalPathX.getPathIterator(null);
        double[] segment = new double[6];
        int i = 0;
        while(!it.isDone()){
            int type = it.currentSegment(segment);
            if(i==0){
                switch (type) {
                case IGeneralPathX.SEG_MOVETO:
                    Point p = new Point2D(segment[0], segment[1]);
                    p = fixPoint(p);
                    coordinates.add(((PointJTS)p).getJTSCoordinate());
                    break;
                default:
                    String message = StringUtils.replace("Type of segment %(segment)s isn't SEG_MOVETO.","%(segment)s",String.valueOf(i));
                    logger.warn(message);
                    throw new RuntimeException(message);
                }
            } else {
                //Dudo de que los casos SEG_QUADTO y SEG_CUBICTO est�n bien pero se hac�a lo mismo en la librer�a de geometr�as vieja.
                Point p;
                switch (type) {
                case IGeneralPathX.SEG_LINETO:
                    p = new Point2D(segment[0], segment[1]);
                    p = fixPoint(p);
                    coordinates.add(((PointJTS)p).getJTSCoordinate());
                    break;
                case IGeneralPathX.SEG_QUADTO:
                    for (int j = 0; j <= 1; j++) {
                        p = new Point2D(segment[i], segment[i+1]);
                        p = fixPoint(p);
                        coordinates.add(((PointJTS) p).getJTSCoordinate());
                    }
                    break;
                case IGeneralPathX.SEG_CUBICTO:
                    for (int j = 0; j <= 2; j++) {
                        p = new Point2D(segment[i], segment[i+1]);
                        p = fixPoint(p);
                        coordinates.add(((PointJTS) p).getJTSCoordinate());
                    }
                    break;
                case IGeneralPathX.SEG_CLOSE:
                    coordinates.add(coordinates.get(0));
                    break;
                default:
                    String message = StringUtils.replace("The general path has a gap in segment %(segment)s.","%(segment)s",String.valueOf(i));
                    logger.warn(message);
                    throw new RuntimeException(message);
                }
            }
            it.next();
            i++;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org
     * .gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        if (!coordinates.isEmpty() && !isClosed()) {
            coordinates.add((Coordinate)coordinates.get(0).clone());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        coordinates.ensureCapacity(capacity);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        if (ct == null) {
            return;
        }
        ArrayListCoordinateSequence tmpCoordinates = new ArrayListCoordinateSequence();
        tmpCoordinates.ensureCapacity(coordinates.size());
        for (Iterator<Coordinate> iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();

            java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(coordinate.x, coordinate.y);
            try {
                p = ct.convert(p, p);
                coordinate.x = p.getX();
                coordinate.y = p.getY();
                tmpCoordinates.add(coordinate);
            } catch (Exception exc) {
                /*
                 * This can happen when the reprojection lib is unable
                 * to reproject (for example the source point
                 * is out of the valid range and some computing
                 * problem happens)
                 */
            }
        }
        coordinates=tmpCoordinates;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        if (at == null) {
            return;
        }

        for (Iterator<Coordinate> iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();
            java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(coordinate.x, coordinate.y);

            at.transform(p, p);
            coordinate.x = p.getX();
            coordinate.y = p.getY();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return anyVertex.getDimension();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(new SplineIterator(affineTransform),false,0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * )
     */
    public PathIterator getPathIterator(AffineTransform at) {
        SplineIterator pi = new SplineIterator(at);
        return pi;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * , double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(new SplineIterator(null),false,0);
    }

    public class SplineIterator extends GeneralPathXIterator {

        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;
        private int index = 0;

        /**
         * Creates a new PointIterator object.
         *
         * @param p
         *            The polygon
         * @param at
         *            The affine transform applied to coordinates during
         *            iteration
         */
        public SplineIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            done = (getJTS().getCoordinates().length == ++index);
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            Coordinate[] jtsCoordinates = getJTS().getCoordinates();
            coords[0] = jtsCoordinates[index].x;
            coords[1] = jtsCoordinates[index].y;
            at.transform(coords, 0, coords, 0, 1);

            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        }

        /*
         * (non-Javadoc)
         *
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            Coordinate[] jtsCoordinates = getJTS().getCoordinates();
            coords[0] = (float)jtsCoordinates[index].x;
            coords[1] = (float)jtsCoordinates[index].y;
            at.transform(coords, 0, coords, 0, 1);

            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return anyVertex.is3D();
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        Collections.reverse(coordinates);
    }

    protected ArrayListCoordinateSequence cloneCoordinates() {
        ArrayListCoordinateSequence cloned = new ArrayListCoordinateSequence();
        cloned.ensureCapacity(coordinates.size());
        for (Iterator iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();
            cloned.add((Coordinate)coordinate.clone());
        }
        return cloned;
    }


    protected boolean isClosed(){
        return coordinates.get(0).equals(coordinates.get(coordinates.size()-1));
    }

}
