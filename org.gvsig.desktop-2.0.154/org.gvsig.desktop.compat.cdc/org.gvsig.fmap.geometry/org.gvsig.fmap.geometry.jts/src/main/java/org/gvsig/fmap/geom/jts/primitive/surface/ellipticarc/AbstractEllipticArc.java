/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.ellipticarc;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.PathIterator;

import org.cresques.cts.CoordTransRuntimeException;
import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.AbstractSurface;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.UtilFunctions;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.EllipticArc;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.tools.exception.BaseException;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractEllipticArc extends AbstractSurface {

    /**
     *
     */
    private static final long serialVersionUID = 6163269454714321454L;
    protected Point axis1Start;
    protected Point axis1End;
    protected double semiAxis2Length;
    protected double angSt;
    protected double angExt;


    /**
     * @param type
     * @param subtype
     */
    protected AbstractEllipticArc(int type, int subtype) {
        super(type, subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getNumInteriorRings()
     */
    public int getNumInteriorRings() {
        String message = "Calling deprecated method getInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getInteriorRing(int)
     */
    public Ring getInteriorRing(int index) {
        String message = "Calling deprecated method getInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Ring)
     */
    public void addInteriorRing(Ring ring) {
        String message = "Calling unsupported method addInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Line)
     */
    public void addInteriorRing(Line ring) {

        String message = "Calling unsupported method addInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Polygon)
     */
    public void addInteriorRing(Polygon polygon) {
        String message = "Calling unsupported method addInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#removeInteriorRing(int)
     */
    public void removeInteriorRing(int index) {
        String message = "Calling unsupported method removeInteriorRing of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int, int)
     */
    public double getCoordinateAt(int index, int dimension) {
        String message = "Calling deprecated method getCoordinateAt of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int, int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        String message = "Calling deprecated method setCoordinateAt of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        String message = "Calling deprecated method addVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        String message = "Calling deprecated method addVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Calling deprecated method addVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        String message = "Calling deprecated method removeVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        String message = "Calling deprecated method getVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        String message = "Calling deprecated method getNumVertices of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        String message = "Calling deprecated method insertVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        String message = "Calling deprecated method setVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {
        String message = "Calling deprecated method setGeneralPath of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        String message = "Calling deprecated method addMoveToVertex of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        String message = "Calling deprecated method closePrimitive of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        String message = "Calling deprecated method ensureCapacity of a ellipticArc";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(JTSUtils.getPointAtYAxisInEllipse(axis1Start, axis1End, semiAxis2Length));
        try {
        axis1Start.reProject(ct);
        axis1End.reProject(ct);
        aux.reProject(ct);
        } catch (CoordTransRuntimeException e) {
            //Si ha fallado la reproyecci�n de alguno de los puntos, ponemos todas las coordenadas a 0
            axis1Start.setX(0);
            axis1Start.setY(0);
            axis1End.setX(0);
            axis1End.setY(0);
            semiAxis2Length = 0;
        }
        try {
            Point2D transformedMiddlePoint = new Point2D(JTSUtils.getMidPoint(axis1Start, axis1End));
            semiAxis2Length = transformedMiddlePoint.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the radius of the transformed circle.", e);
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(JTSUtils.getPointAtYAxisInEllipse(axis1Start, axis1End, semiAxis2Length));
        axis1Start.transform(at);
        axis1End.transform(at);
        aux.transform(at);
        try {
            Point2D transformedMiddlePoint = new Point2D(JTSUtils.getMidPoint(axis1Start, axis1End));
            semiAxis2Length = transformedMiddlePoint.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the minor semi-axis of the transformed ellipse.", e);
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return axis1Start.getDimension();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        return getPathIterator(at, getManager().getFlatness());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {

        GeometryManager geomManager = GeometryLocator.getGeometryManager();

        try {
            double axis1Lenght = axis1Start.distance(axis1End);
            java.awt.geom.Point2D center =
                new java.awt.geom.Point2D.Double((axis1Start.getX() + axis1End.getX()) / 2,
                    (axis1Start.getY() + axis1End.getY()) / 2);
            double x = center.getX() - axis1Lenght / 2;
            double y = center.getY() - semiAxis2Length;

            double angle =
                UtilFunctions.getAngle(center, new java.awt.geom.Point2D.Double(axis1Start.getX(), axis1Start.getY()));

            Arc2D.Double arc =
                new Arc2D.Double(x, y, axis1Lenght, 2 * semiAxis2Length, Math.toDegrees(angSt), Math.toDegrees(angExt),
                    Arc2D.CHORD);
            AffineTransform mT = AffineTransform.getRotateInstance(angle, center.getX(), center.getY());
            return arc.getPathIterator(null, geomManager.getFlatness());
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the radius of the transformed circle.", e);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {

        GeneralPathX gp = new DefaultGeneralPathX(getPathIterator(null, getManager().getFlatness()), is3D(), 0.0);
         return gp;
     }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return ((PointJTS)axis1Start).is3D();
    }


    /**
     * @param initialPoint
     * @return
     */
    protected abstract Point fixPoint(Point point);


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point, double)
     */
    public void setPoints(Point axis1Start, Point axis1End, double axis2Length, double angSt, double angExt) {
        this.axis1Start = fixPoint(axis1Start);
        this.axis1End = fixPoint(axis1End);
        this.semiAxis2Length = axis2Length;
        this.angSt = angSt;
        this.angExt = angExt;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis1Start()
     */
    public Point getAxis1Start() {
        return axis1Start;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis1End()
     */
    public Point getAxis1End() {
        return axis1End;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Ellipse#getAxis2Dist()
     */
    public double getAxis2Dist() {
        return semiAxis2Length;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.EllipticArc#getAngSt()
     */
    public double getAngSt(){
        return this.angSt;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.EllipticArc#getAngExt()
     */
    public double getAngExt(){
        return this.angExt;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        //FIXME: throw UnssupportedOperationException or do nothing?
//        String message = "Can't flip a ellipse";
//        notifyDeprecated(message);
//        throw new UnsupportedOperationException(message);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        EllipticArc cloned = (EllipticArc) this.cloneGeometry();

        Point center = new Point2D((getAxis1Start().getX()+getAxis1End().getX())/2,
            (getAxis1Start().getY()+getAxis1End().getY())/2);
        double axis1Lenght = getAxis1Start().distance(getAxis1End());

        Point clonedAxis1Start = (Point) getAxis1Start().cloneGeometry();
        Point clonedAxis1End = (Point) getAxis1End().cloneGeometry();
        double clonedYDist = this.semiAxis2Length+distance;

        clonedAxis1Start.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1Start().getX(), axis1Lenght/2+distance));
        clonedAxis1Start.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1Start().getY(), axis1Lenght/2+distance));

        clonedAxis1End.setX(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getX(), getAxis1End().getX(), axis1Lenght/2+distance));
        clonedAxis1End.setY(JTSUtils.straightLineThroughTwoPointsEquation(0, axis1Lenght/2, center.getY(), getAxis1End().getY(), axis1Lenght/2+distance));

        cloned.setPoints(clonedAxis1Start, clonedAxis1End, clonedYDist, this.angSt, this.angExt);
        return cloned;
    }
}
