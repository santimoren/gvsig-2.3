/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.point;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.DirectPosition;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.exception.ReprojectionRuntimeException;
import org.gvsig.fmap.geom.handler.AbstractHandler;
import org.gvsig.fmap.geom.handler.FinalHandler;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.jts.primitive.Envelope2D;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;

/**
 * @author fdiaz
 *
 */
public abstract class AbstractPoint extends AbstractPrimitive implements PointJTS {

    /**
     *
     */
    private static final long serialVersionUID = -8378193151810866724L;
    protected com.vividsolutions.jts.geom.Coordinate coordinate;

    /**
    *
    */
    protected AbstractPoint(int subtype) {
        super(Geometry.TYPES.POINT, subtype);
    }

    /**
     *
     */
    public AbstractPoint(int subtype, com.vividsolutions.jts.geom.Coordinate coordinate) {
        this(subtype);
        this.coordinate = coordinate;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#getDirectPosition()
     */
    public DirectPosition getDirectPosition() {
        return new PointDirectPosition();
    }

    class PointDirectPosition implements DirectPosition {

        /*
         * (non-Javadoc)
         *
         * @see org.gvsig.fmap.geom.DirectPosition#getDimension()
         */
        public int getDimension() {
            return AbstractPoint.this.getDimension();
        }

        /*
         * (non-Javadoc)
         *
         * @see org.gvsig.fmap.geom.DirectPosition#getOrdinate(int)
         */
        public double getOrdinate(int dimension) {
            return AbstractPoint.this.getCoordinateAt(dimension);
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#setCoordinateAt(int, double)
     */
    public void setCoordinateAt(int dimension, double value) {
        this.coordinate.setOrdinate(dimension, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#setCoordinates(double[])
     */
    public void setCoordinates(double[] values) {
        for (int i = 0; i < values.length; i++) {
            this.coordinate.setOrdinate(i, values[i]);
        }
    }

  /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#setX(double)
     */
    public void setX(double x) {
        this.coordinate.x = x;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#setY(double)
     */
    public void setY(double y) {
        this.coordinate.y = y;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#getCoordinateAt(int)
     */
    public double getCoordinateAt(int dimension) {
        return this.coordinate.getOrdinate(dimension);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#getCoordinates()
     */
    public double[] getCoordinates() {
        double[] coords = new double[this.getDimension()];
        for (int i = 0; i < this.getDimension(); i++) {
            coords[i] = this.coordinate.getOrdinate(i);
        }
        return coords;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#getX()
     */
    public double getX() {
        return this.coordinate.x;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.Point#getY()
     */
    public double getY() {
        return this.coordinate.y;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSPoint(this.coordinate);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        if (ct == null) {
            return;
        }
        java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(this.getX(), this.getY());
        try {
            p = ct.convert(p, p);
            this.setX(p.getX());
            this.setY(p.getY());
        } catch (Exception exc) {
            /*
             * This can happen when the reprojection lib is unable
             * to reproject (for example the source point
             * is out of the valid range and some computing
             * problem happens)
             */
            this.setX(0);
            this.setY(0);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        if (at == null) {
            return;
        }

        java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(this.getX(), this.getY());
        at.transform(p, p);
        setX(p.getX());
        setY(p.getY());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.primitive.point.PointJTS#getJTSCoordinates()
     */
    public com.vividsolutions.jts.geom.Coordinate getJTSCoordinate() {
        return this.coordinate;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform), false, 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return new DefaultGeneralPathX(getPathIterator(null), false, 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * , double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getHandlers(int)
     */
    public Handler[] getHandlers(int type) {
        return new Handler[] { new PointHandler() };
    }

    class PointHandler extends AbstractHandler implements FinalHandler {

        public PointHandler() {
            point = new java.awt.geom.Point2D.Double(AbstractPoint.this.getX(), AbstractPoint.this.getY());
            index = 0;
        }

        public void move(double movex, double movey) {
            AbstractPoint.this.setX(AbstractPoint.this.getX() + movex);
            AbstractPoint.this.setY(AbstractPoint.this.getY() + movey);
        }

        public void set(double setx, double sety) {
            AbstractPoint.this.setX(setx);
            AbstractPoint.this.setY(sety);
        }
    }

    public Envelope getEnvelope() {
        return new Envelope2D(this.getX(), this.getY(), this.getX(), this.getY());
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        String message = "Can't get lines from a point";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        String message = "Can't get polygons from a point";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        //Do nothing
    }

    public abstract String toString();

}
