/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.util;

import java.util.ArrayList;
import java.util.Collection;

import com.vividsolutions.jts.algorithm.distance.DistanceToPoint;
import com.vividsolutions.jts.algorithm.distance.PointPairDistance;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.linemerge.LineMerger;

import org.gvsig.fmap.geom.Geometry;

/**
 * Based on portions of code from OpenJump
 *
 */
public class OpenJUMPUtils {

    /**
     * Creates a offset of a open line at a distance by removing segments that do not match the limit of the buffer.
     *
     * @param distance
     * @param factory
     * @return
     */
    public static Geometry offsetCleanOpenLine(ArrayListCoordinateSequence coordinates, double distance) {
        com.vividsolutions.jts.geom.GeometryFactory factory = JTSUtils.getFactory(coordinates);

        LineString jtsGeom = JTSUtils.createJTSLineString(coordinates);

        BufferParameters bufParams = JTSUtils.getBufferParameters();

        com.vividsolutions.jts.geom.Geometry sidedBuffer =
            new BufferOp(jtsGeom, bufParams).getResultGeometry(distance).getBoundary();
        Collection<LineString> offsetSegments = new ArrayList<LineString>();
        // Segments located entirely under this distance are excluded
        int quadrantSegments = bufParams.getQuadrantSegments();
        double lowerBound = Math.abs(distance) * Math.sin(Math.PI / (4 * quadrantSegments));
        // Segments located entirely over this distance are included
        // note that the theoretical approximation made with quadrantSegments
        // is offset*cos(PI/(4*quadrantSegments) but
        // offset*cos(PI/(2*quadrantSegments)
        // is used to make sure to include segments located on the boundary
        double upperBound = Math.abs(distance) * Math.cos(Math.PI / (2 * quadrantSegments));
        for (int i = 0; i < sidedBuffer.getNumGeometries(); i++) {
            Coordinate[] cc = sidedBuffer.getGeometryN(i).getCoordinates();
            PointPairDistance ppd = new PointPairDistance();
            DistanceToPoint.computeDistance(jtsGeom, cc[0], ppd);
            double dj = ppd.getDistance();
            for (int j = 1; j < cc.length; j++) {
                double di = dj;
                ppd = new PointPairDistance();
                DistanceToPoint.computeDistance(jtsGeom, cc[j], ppd);
                dj = ppd.getDistance();
                // segment along or touching the source geometry : eclude it
                if (Math.max(di, dj) < lowerBound || di == 0 || dj == 0) {
                    continue;
                }
                // segment along the buffer boundary : include it
                else if (Math.min(di, dj) > upperBound) {
                    LineString segment = jtsGeom.getFactory().createLineString(new Coordinate[] { cc[j - 1], cc[j] });
                    offsetSegments.add(segment);
                }
                // segment entirely located inside the buffer : exclude it
                else if (Math.min(di, dj) > lowerBound && Math.max(di, dj) < upperBound) {
                    continue;
                }
                // segment with a end at the offset distance and the other
                // located within the buffer : divide it
                else {
                    // One of the coordinates is closed to but not on the source
                    // curve and the other is more or less closed to offset
                    // distance
                    divide(offsetSegments, jtsGeom, cc[j - 1], cc[j], di, dj, lowerBound, upperBound);
                }
            }
        }
        Collection<LineString> offsetCurves = new ArrayList<LineString>();
        offsetCurves.addAll(merge(offsetSegments));

        return JTSUtils.createGeometry(factory.buildGeometry(offsetCurves));
    }

    // Recursive function to split segments located on the single-side buffer
    // boundary, but having a part of them inside the full buffer.


    /**
     * Recursive function to split segments located on the single-side buffer
     * boundary, but having a part of them inside the full buffer.
     *
     * @param offsetSegments
     * @param sourceCurve
     * @param c1
     * @param c2
     * @param d1
     * @param d2
     * @param lb
     * @param ub
     */
    private static void divide(Collection<LineString> offsetSegments, com.vividsolutions.jts.geom.Geometry sourceCurve,
            Coordinate c1, Coordinate c2, double d1, double d2, double lb, double ub) {
        // I stop recursion for segment < 2*lb to exclude small segments
        // perpendicular but very close to the boundary
        if (c1.distance(c2) < 2*lb) return;

        Coordinate c = new Coordinate((c1.x+c2.x)/2.0, (c1.y+c2.y)/2.0);
        PointPairDistance ppd = new PointPairDistance();
        DistanceToPoint.computeDistance(sourceCurve, c, ppd);
        double d = ppd.getDistance();
        if (Math.max(d1, d) < lb) {}
        else if (Math.min(d1, d) > lb && Math.max(d1, d) < ub) {}
        else if (Math.min(d1, d) > ub) {
            LineString segment = sourceCurve.getFactory().createLineString(
                        new Coordinate[]{c1, c});
            offsetSegments.add(segment);
        }
        else {
            divide(offsetSegments, sourceCurve, c1, c, d1, d, lb, ub);
        }
        if (Math.max(d, d2) < lb) {}
        else if (Math.min(d, d2) > lb && Math.max(d, d2) < ub) {}
        else if (Math.min(d, d2) > ub) {
            LineString segment = sourceCurve.getFactory().createLineString(
                        new Coordinate[]{c, c2});
            offsetSegments.add(segment);
        }
        else {
            divide(offsetSegments, sourceCurve, c, c2, d, d2, lb, ub);
        }
    }

    @SuppressWarnings("unchecked")
    private static Collection<LineString> merge(Collection<LineString> linestrings) {
        LineMerger merger = new LineMerger();
        merger.add(linestrings);
        return merger.getMergedLineStrings();
    }
}
