/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.line;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.OpenJUMPUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Line;


/**
 * @author fdiaz
 *
 */
public class Line3DM extends BaseLine3DM implements Line {

    /**
     *
     */
    private static final long serialVersionUID = 3467349699229302515L;

    /**
     *
     */
    public Line3DM() {
        super(Geometry.TYPES.LINE);
    }

    /**
     * @param coordinates
     */
    public Line3DM(Coordinate[] coordinates) {
        super(Geometry.TYPES.LINE, coordinates);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Line3DM(cloneCoordinates().toCoordinateArray());
     }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {

        if (isClosed()) {
            return JTSUtils.offsetClosedLine(this.coordinates, distance);
        } else {
//            return JTSUtils.offsetRawOpenLine(this.coordinates, distance);
            return OpenJUMPUtils.offsetCleanOpenLine(this.coordinates, distance);
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = super.equals(obj);
        if(res && obj instanceof Line3DM){
            Line3DM other = (Line3DM)obj;
            if(this.getNumVertices() != other.getNumVertices()){
                return false;
            }
            for(int i=0; i < this.getNumVertices(); i++){
                Coordinate coordinate = this.coordinates.get(i);
                Coordinate otherCoordinate = other.coordinates.get(i);
                if (otherCoordinate.getOrdinate(2) != coordinate.getOrdinate(2)) {
                    return false;
                }
                if (otherCoordinate.getOrdinate(3) != coordinate.getOrdinate(3)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

}
