/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.spline;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.jts.util.OpenJUMPUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Spline;

/**
 * @author fdiaz
 *
 */
public class Spline2D extends BaseSpline2D implements Spline {

    /**
     *
     */
    private static final long serialVersionUID = -4618430296292660668L;

    /**
     * @param subtype
     */
    public Spline2D() {
        super(Geometry.TYPES.SPLINE);
    }

    /**
    *
    */
    public Spline2D(Coordinate[] coordinates) {
        super(Geometry.TYPES.SPLINE, coordinates);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Spline2D(cloneCoordinates().toCoordinateArray());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        Coordinate[] coords = getJTS().getCoordinates();
        ArrayListCoordinateSequence listCoordSequence = new ArrayListCoordinateSequence(new ArrayList<Coordinate>());
        for (int i = 0; i < coords.length; i++) {
            listCoordSequence.add(coords[i]);
        }
        if (isClosed()) {
            return JTSUtils.offsetClosedLine(listCoordSequence, distance);
        } else {
            return OpenJUMPUtils.offsetCleanOpenLine(listCoordSequence, distance);
        }
    }
}
