/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.point;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.jts.DefaultGeometryType;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.PointGeometryType;
import org.gvsig.fmap.geom.type.AbstractGeometryType;

/**
 * Geometry type implementation for the creation of {@link Point2D} objects.
 *
 * @author gvSIG Team
 */
public class Point2DGeometryType extends DefaultGeometryType implements PointGeometryType {

    private static final String NAME = "Point2D";
    private static final int TYPE = Geometry.TYPES.POINT;
    private static final int SUBTYPE = Geometry.SUBTYPES.GEOM2D;

    /**
     * @param geomClass
     * @param name
     * @param type
     * @param subType
     */
    public Point2DGeometryType() {
        super(Point2D.class, NAME, TYPE, SUBTYPE);
    }

    public String getName() {
        return NAME;
    }

    public int getType() {
        return TYPE;
    }

    public int getSubType() {
        return SUBTYPE;
    }

    public boolean isTypeOf(int geometryType) {
        return TYPE == geometryType
            || Geometry.TYPES.GEOMETRY == geometryType;
    }

    public boolean isSubTypeOf(int geometrySubType) {
        return SUBTYPE == geometrySubType;
    }

    public Geometry create() throws CreateGeometryException {
        return new Point2D();
    }

    public Point createPoint(double x, double y) {
        return new Point2D(x, y);
    }

    public Point createPoint(double[] coordinates) {
        return new Point2D(coordinates[0], coordinates[1]);
    }

}
