/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.circle;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.cresques.cts.CoordTransRuntimeException;
import org.cresques.cts.ICoordTrans;

import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.AbstractSurface;
import org.gvsig.fmap.geom.jts.util.UtilFunctions;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.locator.LocatorException;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractCircle extends AbstractSurface {

    /**
     *
     */
    private static final long serialVersionUID = -5509291843865895995L;

    protected Point center;
    protected double radius;

    /**
     * @param type
     * @param subtype
     */
    public AbstractCircle(int type, int subtype) {
        super(type, subtype);
    }

    /**
     * @param type
     * @param subtype
     */
    protected AbstractCircle(int type, int subtype, Point center, double radius) {
        this(type, subtype);
        this.setCenter(center);
        this.setRadius(radius);
    }


    /**
     * @param initialPoint
     * @return
     */
    protected abstract Point fixPoint(Point point);

    /**
     * @param center the center to set
     */
    public void setCenter(Point center) {
        this.center = fixPoint(center);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#getCenter()
     */
    public Point getCenter() {
        return this.center;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#getRadious()
     */
    public double getRadious() {
        return this.radius;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return this.center.getDimension();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#isSimple()
     */
    public boolean isSimple() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point center, Point radius) {
        setCenter(center);
        this.radius = ((PointJTS)radius).getJTSCoordinate().distance(((PointJTS)this.center).getJTSCoordinate());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#setPoints(org.gvsig.fmap.geom.primitive.Point, double)
     */
    public void setPoints(Point center, double radius) {
        setCenter(center);
        this.radius = radius;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return ((PointJTS)center).is3D();
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int, int)
     */
    public double getCoordinateAt(int index, int dimension) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int, int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double)
     */
    public void addVertex(double x, double y) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(double, double, double)
     */
    public void addVertex(double x, double y, double z) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getVertex(int)
     */
    public Point getVertex(int index) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        String message = "Calling deprecated method getNumVertices of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org.gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        String message = "Calling deprecated method setPoints of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        String message = "Calling deprecated method ensureCapacity of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return getShape(null);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        return this.getPathIterator(at, getManager().getFlatness());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform, double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {

        java.awt.geom.Point2D.Double center = new java.awt.geom.Point2D.Double(this.center.getX(), this.center.getY());
        java.awt.geom.Arc2D arco = UtilFunctions.createCircle(center,  radius);

        return arco.getPathIterator(at, flatness);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        GeneralPathX gp = new DefaultGeneralPathX(getPathIterator(null, getManager().getFlatness()), is3D(), 0.0);
         return gp;
     }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getNumInteriorRings()
     */
    public int getNumInteriorRings() {
        String message = "Calling deprecated method getNumInteriorRings of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#getInteriorRing(int)
     */
    public Ring getInteriorRing(int index) {
        String message = "Calling deprecated method getInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Ring)
     */
    public void addInteriorRing(Ring ring) {
        String message = "Calling deprecated method addInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Line)
     */
    public void addInteriorRing(Line ring) {
        String message = "Calling deprecated method addInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#addInteriorRing(org.gvsig.fmap.geom.primitive.Polygon)
     */
    public void addInteriorRing(Polygon polygon) {
        String message = "Calling unsupported method addInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }
    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Surface#removeInteriorRing(int)
     */
    public void removeInteriorRing(int index) {
        String message = "Calling deprecated method removeInteriorRing of a circle";
        notifyDeprecated(message);
        throw new UnsupportedOperationException(message);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(center.getX(), center.getY()-radius);
        try {
            center.reProject(ct);
            aux.reProject(ct);
        } catch (CoordTransRuntimeException e){
            center.setX(0);
            center.setY(0);
            radius = 0;
            return;
        }
        try {
            radius = center.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the radius of the transformed circle.", e);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        //FIXME: Esto solo ser�a correcto para transformaciones de traslaci�n, rotaci�n y escala
        // Ser�a incorrecto para las de deformaci�n en cizallamiento

        Point2D aux = new Point2D(center.getX(), center.getY()-radius);
        center.transform(at);
        aux.transform(at);
        try {
            radius = center.distance(aux);
        } catch (BaseException e) {
            throw new UnsupportedOperationException("Error calculating the radius of the transformed circle.", e);
        }

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        //FIXME: throw UnssupportedOperationException or do nothing?
//        String message = "Can't flip a circle";
//        notifyDeprecated(message);
//        throw new UnsupportedOperationException(message);
    }

}
