/*
 * BinaryWriter.java
 * 
 * PostGIS extension for PostgreSQL JDBC driver - Binary Writer
 * 
 * (C) 2005 Markus Schaber, markus.schaber@logix-tt.com
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 2.1 of the License.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA or visit the web at
 * http://www.gnu.org.
 * 
 * $Id: BinaryWriter.java 2497 2006-10-02 23:26:34Z mschaber $
 */
package org.gvsig.fmap.geom.jts.operation.towkb;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.jts.operation.towkb.OGCWKBEncoder.wkbByteOrder;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;

public class PostGISEWKBEncoder {
	
	public final static int wkbPoint = 1;
	public final static int wkbLineString = 2;
	public final static int wkbPolygon = 3;
	public final static int wkbMultiPoint = 4;
	public final static int wkbMultiLineString = 5;
	public final static int wkbMultiPolygon = 6;
	public final static int wkbGeometryCollection = 7;

	
	public byte[] encode(Geometry geometry) throws IOException {
		ByteArrayOutputStream stream;

		if (geometry.getType() == TYPES.POINT) {
			stream = new ByteArrayOutputStream(50);
		} else {
			stream = new ByteArrayOutputStream(512);
		}

		writeGeometry(geometry, new DataOutputStream(stream));
		return stream.toByteArray();
	}

	
	static ByteBuffer bytebuffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
	
	private void writeInt(DataOutputStream dest, int value) throws IOException{
		byte[] bytes = bytebuffer.putInt(0, value).array();
		dest.writeByte(bytes[0]);
		dest.writeByte(bytes[1]);
		dest.writeByte(bytes[2]);
		dest.writeByte(bytes[3]);
	}
	
	private void writeDouble(DataOutputStream dest, double value) throws IOException{
		byte[] bytes = bytebuffer.putDouble(0, value).array();
		dest.writeByte(bytes[0]);
		dest.writeByte(bytes[1]);
		dest.writeByte(bytes[2]);
		dest.writeByte(bytes[3]);
		dest.writeByte(bytes[4]);
		dest.writeByte(bytes[5]);
		dest.writeByte(bytes[6]);
		dest.writeByte(bytes[7]);
	}
	
   
    protected void writeGeometry(Geometry geom, DataOutputStream dest) throws IOException {
        // write endian flag
        dest.writeByte(wkbByteOrder.wkbNDR);

        switch (geom.getType()) {
        case Geometry.TYPES.POINT :
        	encodeWKBGeomHead(wkbPoint, geom.getGeometryType(), dest);
            writePoint((Point) geom, dest);
            break;
        case Geometry.TYPES.LINE :
        	encodeWKBGeomHead(wkbLineString, geom.getGeometryType(), dest);
            writeLine((Line) geom, dest);
            break;
        case Geometry.TYPES.POLYGON :
        	encodeWKBGeomHead(wkbPolygon, geom.getGeometryType(), dest);
            writePolygon((Polygon) geom, dest);
            break;
        case Geometry.TYPES.MULTIPOINT :
        	encodeWKBGeomHead(wkbMultiPoint, geom.getGeometryType(), dest);
            writeMultiPoint((MultiPoint) geom, dest);
            break;
        case Geometry.TYPES.MULTICURVE :
        case Geometry.TYPES.MULTILINE :
        	encodeWKBGeomHead(wkbMultiLineString, geom.getGeometryType(), dest);
            writeMultiLineString((MultiCurve) geom, dest);
            break;
        case Geometry.TYPES.MULTISURFACE :
        case Geometry.TYPES.MULTIPOLYGON :
        	encodeWKBGeomHead(wkbMultiPolygon, geom.getGeometryType(), dest);
            writeMultiPolygon((MultiSurface) geom, dest);
            break;
//        case Geometry.TYPES.GEOMETRY :
//            writeCollection((GeometryCollection) geom, dest);
//            break;
        default :
            throw new IllegalArgumentException("Unknown Geometry Type: " + geom.getType());
        }
    }
    
    private void encodeWKBGeomHead(int wkbBaseType, GeometryType geometryType, DataOutputStream dest) throws IOException{
	    int typeword = wkbBaseType;
	    
	    GeometryType gtype = geometryType;
	    if (gtype.hasZ()) {
	        typeword |= 0x80000000;
	    }
	    if (gtype.hasM()) {
	        typeword |= 0x40000000;
	    }
	    writeInt(dest, typeword);
    }

    private void writePoint(Point geom,  DataOutputStream dest) throws IOException {
    	GeometryType gtype = geom.getGeometryType();
       
    	writeDouble(dest,geom.getX());
        writeDouble(dest,geom.getY());

        if (gtype.hasZ()) {
        	writeDouble(dest,geom.getCoordinateAt(2));
        }

        if (gtype.hasM()) {
        	writeDouble(dest,geom.getCoordinateAt(geom.getDimension()-1));
        }
    }


    private void writePointArray(Point[] geom, DataOutputStream dest) throws IOException {
        // number of points
        writeInt(dest,geom.length);
        for (int i = 0; i < geom.length; i++) {
            writePoint(geom[i], dest);
        }
    }

    private void writeMultiPoint(MultiPoint geom, DataOutputStream dest) throws IOException {
        writeInt(dest,geom.getPrimitivesNumber());
        Iterator it = geom.iterator();
        while(it.hasNext()) {
        	Point point = (Point) it.next();
        	writeGeometry(point, dest);
        }
    }

    private void writeLine(Curve geom, DataOutputStream dest) throws IOException {
    	writeInt(dest,geom.getNumVertices());
        for (int i = 0; i < geom.getNumVertices(); i++) {
            writePoint(geom.getVertex(i), dest);
        }
    }

//    private void writeLinearRing(LinearRing geom, DataOutputStream dest) throws IOException {
//    	writeInt(dest,geom.numPoints());
//        for (int i = 0; i < geom.numPoints(); i++) {
//            writePoint(geom.getPoint(i), dest);
//        }
//    }

    private void writePolygon(Polygon geom, DataOutputStream dest) throws IOException {
        writeInt(dest,geom.getNumInteriorRings()+1);
        writeInt(dest,geom.getNumVertices());
        for (int i = 0; i < geom.getNumVertices(); i++) {
            writePoint(geom.getVertex(i), dest);
        }
    }

    private void writeMultiLineString(MultiCurve geom, DataOutputStream dest) throws IOException {
        writeInt(dest,geom.getPrimitivesNumber());
        Iterator it = geom.iterator();
        while(it.hasNext()) {
        	Curve line = (Curve) it.next();
        	writeGeometry(line, dest);
        }
    }

    private void writeMultiPolygon(MultiSurface geom, DataOutputStream dest) throws IOException {
        writeInt(dest,geom.getPrimitivesNumber());
        Iterator it = geom.iterator();
        while(it.hasNext()) {
        	Surface pol = (Surface) it.next();
        	writeGeometry(pol, dest);
        }
    }

//    private void writeCollection(GeometryCollection geom, DataOutputStream dest) throws IOException {
//    	writeInt(dest,geom.getNumGeometries());
//        for (int i = 0; i < geom.getNumGeometries(); i++) {
//            writeGeometry(geom.get, dest);
//        }
//    }

    protected int estimateBytes(Geometry geom) {
        int result = 0;

        // write endian flag
        result += 1;

        // write typeword
        result += 4;

//        if (geom.srid != -1) {
//            result += 4;
//        }

        switch (geom.getType()) {
        case Geometry.TYPES.POINT :
            result += estimatePoint((Point) geom);
            break;
        case Geometry.TYPES.CURVE :
        case Geometry.TYPES.LINE :
            result += estimateLine((Curve) geom);
            break;
        case Geometry.TYPES.POLYGON :
            result += estimatePolygon((Polygon) geom);
            break;
        case Geometry.TYPES.MULTIPOINT :
            result += estimateMultiPoint((MultiPoint) geom);
            break;
        case Geometry.TYPES.MULTILINE :
            result += estimateMultiLineString((MultiLine) geom);
            break;
        case Geometry.TYPES.MULTIPOLYGON :
            result += estimateMultiPolygon((MultiPolygon) geom);
            break;
//        case Geometry.GEOMETRYCOLLECTION :
//            result += estimateCollection((GeometryCollection) geom);
//            break;
        default :
            throw new IllegalArgumentException("Unknown Geometry Type: " + geom.getType());
        }
        return result;
    }

    private int estimatePoint(Point geom) {
        // x, y both have 8 bytes
    	GeometryType gtype = geom.getGeometryType();
        int result = 16;
        if (gtype.hasZ()) {
            result += 8;
        }

        if (gtype.hasM()) {
            result += 8;
        }
        return result;
    }
    
    private int estimateGeometryArray(Geometry[] container) {
        int result = 0;
        for (int i = 0; i < container.length; i++) {
            result += estimateBytes(container[i]);
        }
        return result;
    }

    private int estimatePointArray(Point[] geom) {
        // number of points
        int result = 4;

        // And the amount of the points itsself, in consistent geometries
        // all points have equal size.
        if (geom.length > 0) {
            result += geom.length * estimatePoint(geom[0]);
        }
        return result;
    }

    private int estimateMultiPoint(MultiPoint geom) {
        // int size
        int result = 4;
        if (geom.getPrimitivesNumber() > 0) {
            // We can shortcut here, as all subgeoms have the same fixed size
            result += geom.getPrimitivesNumber() * estimateBytes(geom.getPointAt(0));
        }
        return result;
    }

    private int estimateLine(Curve geom) {
    	int result = 4;

        // And the amount of the points itsself, in consistent geometries
        // all points have equal size.
        if (geom.getNumVertices() > 0) {
            result += geom.getNumVertices() * estimatePoint(geom.getVertex(0));
        }
        return result;
    }

//    private int estimateLinearRing(LinearRing geom) {
//        return estimatePointArray(geom.getPoints());
//    }

    private int estimatePolygon(Polygon geom) {
        // int length
        int result = 4;

        // And the amount of the points itsself, in consistent geometries
        // all points have equal size.
        if (geom.getNumVertices() > 0) {
            result += geom.getNumVertices() * estimatePoint(geom.getVertex(0));
        }
        return result;
    }

    private int estimateMultiLineString(MultiLine geom) {
        // 4-byte count + subgeometries
    	int result = 4;
    	Iterator it = geom.iterator();
    	while(it.hasNext()){
    		Curve line = (Curve) it.next();
    		result = result + estimateLine(line);
    	}
        return result;
    }

    private int estimateMultiPolygon(MultiPolygon geom) {
        // 4-byte count + subgeometries
    	int result = 4;
    	Iterator it = geom.iterator();
    	while(it.hasNext()){
    		Polygon pol = (Polygon) it.next();
    		result = result + estimatePolygon(pol);
    	}
        return result;
    }

//    private int estimateCollection(GeometryCollection geom) {
//        // 4-byte count + subgeometries
//        return 4 + estimateGeometryArray(geom.getGeometries());
//    }
}

 