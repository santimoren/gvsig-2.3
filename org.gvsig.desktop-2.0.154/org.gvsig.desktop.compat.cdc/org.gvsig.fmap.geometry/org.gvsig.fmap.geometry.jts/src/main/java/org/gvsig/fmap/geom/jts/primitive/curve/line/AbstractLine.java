/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.curve.line;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.util.Collections;
import java.util.Iterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.apache.commons.lang3.StringUtils;
import org.cresques.cts.ICoordTrans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.ReprojectionRuntimeException;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.jts.primitive.curve.AbstractCurve;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.IGeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * @author fdiaz
 *
 */
public abstract class AbstractLine extends AbstractCurve {

    /**
     *
     */
    private static final long serialVersionUID = 5034197096871344597L;
    private static final Logger logger = LoggerFactory.getLogger(AbstractLine.class);

    protected ArrayListCoordinateSequence coordinates;

    public class VertexIterator implements Iterator<Point> {

        private PointJTS vertex;
        private int current;

        public VertexIterator() {
            this.current = 0;
            if( getNumVertices()>0 ) {
                this.vertex = (PointJTS) getVertex(0).cloneGeometry();
            } else {
                this.vertex = null;
            }
        }

        @Override
        public boolean hasNext() {
            return this.current < getNumVertices() ;
        }

        @Override
        public Point next() {
            this.vertex.setJTSCoordinate(coordinates.get(current) );
            this.current++;
            return this.vertex;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
    /**
    *
    */
    protected AbstractLine(int subtype) {
        super(Geometry.TYPES.LINE, subtype);
    }

    /**
     * @param type
     * @param subtype
     */
    public AbstractLine(int type, int subtype) {
        super(type, subtype);
    }

    abstract public Point getVertex(int index);

    public Iterator<Point> iterator() {
        return new VertexIterator();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        return JTSUtils.createJTSLineString(coordinates);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#addVertex(org.gvsig
     * .fmap.geom.primitive.Point)
     */
    public void addVertex(Point point) {
        point = fixPoint(point);
        coordinates.add(((PointJTS) point).getJTSCoordinate());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.Curve#setPoints(org.gvsig.fmap.geom.primitive
     * .Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point initialPoint, Point endPoint) {
        initialPoint = fixPoint(initialPoint);
        endPoint = fixPoint(endPoint);
        coordinates.clear();
        addVertex(initialPoint);
        addVertex(endPoint);
    }

    /**
     * @param point
     * @return
     */
    protected abstract Point fixPoint(Point point);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#getCoordinateAt(int,
     * int)
     */
    public double getCoordinateAt(int index, int dimension) {
        return coordinates.getOrdinate(index, dimension);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#setCoordinateAt(int,
     * int, double)
     */
    public void setCoordinateAt(int index, int dimension, double value) {
        coordinates.setOrdinate(index, dimension, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#removeVertex(int)
     */
    public void removeVertex(int index) {
        coordinates.remove(index);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#getNumVertices()
     */
    public int getNumVertices() {
        return coordinates.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#insertVertex(int,
     * org.gvsig.fmap.geom.primitive.Point)
     */
    public void insertVertex(int index, Point p) {
        p = fixPoint(p);
        coordinates.add(index, ((PointJTS) p).getJTSCoordinate());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#setVertex(int,
     * org.gvsig.fmap.geom.primitive.Point)
     */
    public void setVertex(int index, Point p) {
        p = fixPoint(p);
        coordinates.set(index, ((PointJTS) p).getJTSCoordinate());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#setGeneralPath(org.
     * gvsig.fmap.geom.primitive.GeneralPathX)
     */
    public void setGeneralPath(GeneralPathX generalPathX) {
        PathIterator it = generalPathX.getPathIterator(null);
        double[] segment = new double[6];
        int i = 0;
        while(!it.isDone()){
            int type = it.currentSegment(segment);
            if(i==0){
                switch (type) {
                case IGeneralPathX.SEG_MOVETO:
                    Point p = new Point2D(segment[0], segment[1]);
                    p = fixPoint(p);
                    coordinates.add(((PointJTS)p).getJTSCoordinate());
                    break;
                default:
                    String message = StringUtils.replace("Type of segment %(segment)s isn't SEG_MOVETO.","%(segment)s",String.valueOf(i));
                    logger.warn(message);
                    throw new RuntimeException(message);
                }
            } else {
                //Dudo de que los casos SEG_QUADTO y SEG_CUBICTO est�n bien pero se hac�a lo mismo en la librer�a de geometr�as vieja.
                Point p;
                switch (type) {
                case IGeneralPathX.SEG_LINETO:
                    p = new Point2D(segment[0], segment[1]);
                    p = fixPoint(p);
                    coordinates.add(((PointJTS)p).getJTSCoordinate());
                    break;
                case IGeneralPathX.SEG_QUADTO:
                    for (int j = 0; j <= 1; j++) {
                        p = new Point2D(segment[i], segment[i+1]);
                        p = fixPoint(p);
                        coordinates.add(((PointJTS) p).getJTSCoordinate());
                    }
                    break;
                case IGeneralPathX.SEG_CUBICTO:
                    for (int j = 0; j <= 2; j++) {
                        p = new Point2D(segment[i], segment[i+1]);
                        p = fixPoint(p);
                        coordinates.add(((PointJTS) p).getJTSCoordinate());
                    }
                    break;
                case IGeneralPathX.SEG_CLOSE:
                    if(!coordinates.get(0).equals(coordinates.get(coordinates.size()-1))){
                        coordinates.add(coordinates.get(0));
                    }
                    break;
                default:
                    String message = StringUtils.replace("The general path has a gap in segment %(segment)s.","%(segment)s",String.valueOf(i));
                    logger.warn(message);
                    throw new RuntimeException(message);
                }
            }
            it.next();
            i++;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#addMoveToVertex(org
     * .gvsig.fmap.geom.primitive.Point)
     */
    public void addMoveToVertex(Point point) {
        notifyDeprecated("Calling deprecated metohd addMoveToVertex in Line");
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.primitive.OrientablePrimitive#closePrimitive()
     */
    public void closePrimitive() {
        if (!coordinates.isEmpty() && !isClosed()) {
            coordinates.add((Coordinate)coordinates.get(0).clone());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.primitive.OrientablePrimitive#ensureCapacity(int)
     */
    public void ensureCapacity(int capacity) {
        this.coordinates.ensureCapacity(capacity);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        if (ct == null) {
            return;
        }
        ArrayListCoordinateSequence tmpCoordinates = new ArrayListCoordinateSequence();
        tmpCoordinates.ensureCapacity(coordinates.size());
        for (Iterator<Coordinate> iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();

            java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(coordinate.x, coordinate.y);
            try {
                p = ct.convert(p, p);
                coordinate.x = p.getX();
                coordinate.y = p.getY();
                tmpCoordinates.add(coordinate);
            } catch (Exception exc) {
                /*
                 * This can happen when the reprojection lib is unable
                 * to reproject (for example the source point
                 * is out of the valid range and some computing
                 * problem happens)
                 */
            }
        }
        coordinates=tmpCoordinates;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        if (at == null) {
            return;
        }

        for (int i = 0; i < coordinates.size(); i++) {
            Coordinate coordinate = coordinates.get(i);
            java.awt.geom.Point2D p = new java.awt.geom.Point2D.Double(coordinate.x, coordinate.y);

            at.transform(p, p);
            coordinate.x = p.getX();
            coordinate.y = p.getY();

        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return this.getGeometryType().getDimension();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape(java.awt.geom.AffineTransform)
     */
    public Shape getShape(AffineTransform affineTransform) {
        if( this.coordinates.isEmpty() ) {
            // Esto no deberia de pasar, se trataria de una geometria
            // corrupta.
            return new GeneralPath();
        }
        return new DefaultGeneralPathX(getPathIterator(affineTransform),false,0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getShape()
     */
    public Shape getShape() {
        return new DefaultGeneralPathX(getPathIterator(null),false,0);
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * )
     */
    public PathIterator getPathIterator(AffineTransform at) {
        LineIterator pi = new LineIterator(at);
        return pi;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * , double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return getPathIterator(at);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(getPathIterator(null), false, 0);
    }

    protected class LineIterator extends GeneralPathXIterator {

        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;
        private int index = 0;

        /**
         * Creates a new PointIterator object.
         *
         * @param p
         *            The polygon
         * @param at
         *            The affine transform applied to coordinates during
         *            iteration
         */
        public LineIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            done = (coordinates.size() == ++index);
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            coords[0] = coordinates.getX(index);
            coords[1] = coordinates.getY(index);
            at.transform(coords, 0, coords, 0, 1);

            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        }

        /*
         * (non-Javadoc)
         *
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            coords[0] = (float) coordinates.getX(index);
            coords[1] = (float) coordinates.getY(index);
            at.transform(coords, 0, coords, 0, 1);

            if (index == 0) {
                return PathIterator.SEG_MOVETO;
            } else {
                return PathIterator.SEG_LINETO;
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        int subtype = this.getGeometryType().getType();
        return subtype == Geometry.SUBTYPES.GEOM3D || subtype == Geometry.SUBTYPES.GEOM3DM;
    }

    protected boolean isClosed(){
        return coordinates.get(0).equals(coordinates.get(coordinates.size()-1));
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#flip()
     */
    public void flip() throws GeometryOperationNotSupportedException, GeometryOperationException {
        Collections.reverse(coordinates);
    }

    protected ArrayListCoordinateSequence cloneCoordinates() {
        ArrayListCoordinateSequence cloned = new ArrayListCoordinateSequence();
        cloned.ensureCapacity(coordinates.size());
        for (Iterator iterator = coordinates.iterator(); iterator.hasNext();) {
            Coordinate coordinate = (Coordinate) iterator.next();
            cloned.add((Coordinate)coordinate.clone());
        }
        return cloned;
    }

}
