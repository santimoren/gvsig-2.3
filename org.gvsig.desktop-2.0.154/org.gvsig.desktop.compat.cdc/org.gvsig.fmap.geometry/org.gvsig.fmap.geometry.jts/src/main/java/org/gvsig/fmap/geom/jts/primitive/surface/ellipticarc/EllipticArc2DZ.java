/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.ellipticarc;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.primitive.EllipticArc;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author fdiaz
 *
 */
public class EllipticArc2DZ extends BaseEllipticArc2D implements EllipticArc {

    /**
     * @param subtype
     */
    public EllipticArc2DZ() {
        super(Geometry.TYPES.ELLIPTICARC);
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        EllipticArc2DZ clone = new EllipticArc2DZ();
        clone.setPoints((Point)axis1Start.cloneGeometry(), (Point)axis1End.cloneGeometry(), semiAxis2Length, angSt, angExt);
        return clone;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#getJTS()
     */
    public com.vividsolutions.jts.geom.Geometry getJTS() {
        ArrayListCoordinateSequence coordinates = getJTSCoordinates();
        return JTSUtils.createJTSPolygon(coordinates);
    }
}
