/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.point;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import com.vividsolutions.jts.geom.Coordinate;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint3D;
import org.gvsig.fmap.geom.jts.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.jts.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.jts.primitive.curve.circumference.Circumference2D;
import org.gvsig.fmap.geom.jts.primitive.curve.circumference.Circumference2DZ;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author fdiaz
 *
 */
public class Point3D extends AbstractPoint {

    /**
     *
     */
    private static final long serialVersionUID = 7206091904842881396L;

    /**
    *
    */
   public Point3D(Coordinate coordinates) {
       super(Geometry.SUBTYPES.GEOM3D, coordinates);
   }

   /**
    *
    */
   public Point3D() {
       this(new Coordinate(0, 0, 0));
   }

   /**
   *
   */
   public Point3D(double x, double y, double z) {
       this(new Coordinate(x, y, z));
   }


    public double getZ() {
        return this.coordinate.z;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getDimension()
     */
    public int getDimension() {
        return 3;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#getGeometryType()
     */
    public GeometryType getGeometryType() {
        try {
            return GeometryLocator.getGeometryManager().getGeometryType(Geometry.TYPES.POINT, Geometry.SUBTYPES.GEOM3D);
        } catch (Exception e) {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
     */
    public Geometry cloneGeometry() {
        return new Point3D((Coordinate)this.coordinate.clone());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.GeometryJTS#is3D()
     */
    public boolean is3D() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.point.PointJTS#setJTSCoordinate(com.vividsolutions.jts.geom.Coordinate)
     */
    public void setJTSCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * @param zValue
     */
    public void setZ(double z) {
        this.coordinate.z = z;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.geom.Geometry#getGeneralPath()
     */
    public GeneralPathX getGeneralPath() {
        return new DefaultGeneralPathX(new PointIterator(null), true, getZ());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.geom.Geometry#getPathIterator(java.awt.geom.AffineTransform
     * )
     */
    public PathIterator getPathIterator(AffineTransform at) {
        PointIterator pi = new PointIterator(at);
        return pi;
    }

    public class PointIterator extends GeneralPathXIterator {
        /** Transform applied on the coordinates during iteration */
        private AffineTransform at;

        /** True when the point has been read once */
        private boolean done;

        /**
         * Creates a new PointIterator object.
         *
         * @param p The polygon
         * @param at The affine transform applied to coordinates during iteration
         */
        public PointIterator(AffineTransform at) {
            super(new GeneralPathX());
            if (at == null) {
                at = new AffineTransform();
            }

            this.at = at;
            done = false;
        }

        /**
         * Return the winding rule for determining the interior of the path.
         *
         * @return <code>WIND_EVEN_ODD</code> by default.
         */
        public int getWindingRule() {
            return PathIterator.WIND_EVEN_ODD;
        }

        /**
         * @see java.awt.geom.PathIterator#next()
         */
        public void next() {
            done = true;
        }

        /**
         * @see java.awt.geom.PathIterator#isDone()
         */
        public boolean isDone() {
            return done;
        }

        /**
         * @see java.awt.geom.PathIterator#currentSegment(double[])
         */
        public int currentSegment(double[] coords) {
            coords[0] = getX();
            coords[1] = getY();
            coords[2] = getZ();
            at.transform(coords, 0, coords, 0, 1);

            return PathIterator.SEG_MOVETO;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.PathIterator#currentSegment(float[])
         */
        public int currentSegment(float[] coords) {
            coords[0] = (float) getX();
            coords[1] = (float) getY();
            coords[2] = (float) getZ();

            at.transform(coords, 0, coords, 0, 1);

            return PathIterator.SEG_MOVETO;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint3D();
        multiPoint.addPoint(this);
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.Geometry#offset(double)
     */
    public Geometry offset(double distance) throws GeometryOperationNotSupportedException, GeometryOperationException {
        return new Circumference2DZ((Point)this.cloneGeometry(), distance);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("POINT Z (");
        for( int i=0; i<this.getDimension()-1; i++) {
            builder.append(this.getCoordinateAt(i));
            builder.append(" ");
        }
        builder.append(this.getCoordinateAt(this.getDimension()-1));
        builder.append(")");
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = super.equals(obj);
        if(res && obj instanceof Point3D){
            Point3D other = (Point3D)obj;
            return (this.getZ()==other.getZ());
        } else {
            return false;
        }
    }


}
