/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.jts.primitive.surface.circle;

import java.awt.geom.PathIterator;

import com.vividsolutions.jts.geom.Coordinate;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D.Double;

import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.jts.aggregate.MultiLine2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.jts.aggregate.MultiPolygon2D;
import org.gvsig.fmap.geom.jts.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.jts.primitive.point.Point2D;
import org.gvsig.fmap.geom.jts.primitive.point.PointJTS;
import org.gvsig.fmap.geom.jts.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.jts.util.ArrayListCoordinateSequence;
import org.gvsig.fmap.geom.jts.util.JTSUtils;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;


/**
 * @author fdiaz
 *
 */
public abstract class BaseCircle2D extends AbstractCircle {

    /**
     *
     */
    private static final long serialVersionUID = -6981603729327501715L;

    /**
     * @param type
     * @param subtype
     * @param center
     * @param radius
     */
    public BaseCircle2D(int type, int subtype, Point center, double radius) {
        super(type, subtype, center, radius);
    }

    /**
     * @param circle
     * @param geom2d
     */
    public BaseCircle2D(int type, int subtype) {
        super(type,subtype);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Circle#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
     */
    public void setPoints(Point p1, Point p2, Point p3) {
      this.center = new Point2D(JTSUtils.getCircumcentre(p1, p2, p3));
      this.radius = ((PointJTS)this.center).getJTS().distance(((PointJTS)p1).getJTS());
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.jts.primitive.surface.circle.AbstractCircle#fixPoint(org.gvsig.fmap.geom.primitive.Point)
     */
    @Override
    protected Point fixPoint(Point point) {
        if (point instanceof Point2D) {
            return point;
        } else {
            return new Point2D(point.getX(), point.getY());
        }
    }

    /**
     * @return
     */
    protected ArrayListCoordinateSequence getJTSCoordinates() {
        PathIterator pi = getPathIterator(null);
        ArrayListCoordinateSequence coordinates = new ArrayListCoordinateSequence();

        double coords[] = new double[6];
        while (!pi.isDone()) {
            switch (pi.currentSegment(coords)) {
            case PathIterator.SEG_MOVETO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                break;
            case PathIterator.SEG_LINETO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                break;
            case PathIterator.SEG_QUADTO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                coordinates.add(new Coordinate(coords[2], coords[3]));
                break;
            case PathIterator.SEG_CUBICTO:
                coordinates.add(new Coordinate(coords[0], coords[1]));
                coordinates.add(new Coordinate(coords[2], coords[3]));
                coordinates.add(new Coordinate(coords[4], coords[5]));
                break;
            case PathIterator.SEG_CLOSE:
                coordinates.add(coordinates.get(0));
                break;
            }
            pi.next();
        }
        if(!coordinates.get(0).equals(coordinates.get(coordinates.size()-1))){
            coordinates.add((Coordinate)coordinates.get(0).clone());
        }
        return coordinates;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPoints()
     */
    public MultiPoint toPoints() throws GeometryException {
        MultiPoint multiPoint = new MultiPoint2D();
        Coordinate[] coordinates = getJTS().getCoordinates();
        multiPoint.ensureCapacity(coordinates.length);
        for (int i = 0; i < coordinates.length; i++) {
            multiPoint.addPoint(new Point2D(coordinates[i]));
        }
        return multiPoint;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toLines()
     */
    public MultiLine toLines() throws GeometryException {
        MultiLine multiLine = new MultiLine2D();
        Line line = new Line2D(getJTS().getCoordinates());
        multiLine.addPrimitive(line);
        return multiLine;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Line#toPolygons()
     */
    public MultiPolygon toPolygons() throws GeometryException {
        MultiPolygon multiPolygon = new MultiPolygon2D();
        Polygon polygon = new Polygon2D(getJTS().getCoordinates());
        multiPolygon.addPrimitive(polygon);
        return multiPolygon;
    }
}
