package org.gvsig.fmap.geom.jts.operation.fromwkb;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;

public class TestWKBParser extends AbstractLibraryAutoInitTestCase {

	public void testPoint2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01010000403CDBA337DCC351C06D37C1374D3748400000000000002840";
		// SELECT ST_GeomFromEWKT('POINTM(-71.060316 48.432044 12)');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.POINT);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		assertEquals(geom.getDimension(), 3);
		Point point = (Point) geom;
		assertEquals(point.getX(), -71.060316);
		assertEquals(point.getY(), 48.432044);
		assertEquals(point.getCoordinateAt(2), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testPoint3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01010000C03CDBA337DCC351C06D37C1374D37484000000000000024400000000000002840";
		// SELECT ST_GeomFromEWKT('POINTZM(-71.060316 48.432044 12)');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.POINT);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		assertEquals(geom.getDimension(), 4);
		Point point = (Point) geom;
		assertEquals(point.getX(), -71.060316);
		assertEquals(point.getY(), 48.432044);
		assertEquals(point.getCoordinateAt(2), 10.0);
		assertEquals(point.getCoordinateAt(3), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	
	public void testLine2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "0102000040030000003CDBA337DCC351C06D37C1374D37484000000000000028403CDBA337DC8351C06D37C1374D37484000000000000026403CDBA337DCC351C06D37C1374DB747400000000000002440";
		// SELECT ST_GeomFromEWKT('LINESTRINGM(-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10 )');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.LINE);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getDimension(), 3);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		Curve line = (Curve) geom;
		
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testLine3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01020000C0030000003CDBA337DCC351C06D37C1374D374840000000000000224000000000000028403CDBA337DC8351C06D37C1374D374840000000000000204000000000000026403CDBA337DCC351C06D37C1374DB747400000000000001C400000000000002440";
		// SELECT ST_GeomFromEWKT('LINESTRINGM(-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10 )');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.LINE);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getDimension(), 4);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		Curve line = (Curve) geom;
		
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 9.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 8.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 7.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testPolygon2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "010300004001000000040000003CDBA337DCC351C06D37C1374D37484000000000000028403CDBA337DC8351C06D37C1374D37484000000000000026403CDBA337DCC351C06D37C1374DB7474000000000000024403CDBA337DCC351C06D37C1374D3748400000000000002840";
		// SELECT ST_GeomFromEWKT('POLYGONM((-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10, -71.060316 48.432044 12))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.POLYGON);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getDimension(), 3);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		Polygon line = (Polygon) geom;
		
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		Point vert4 = line.getVertex(3);
		assertEquals(vert4.getX(), -71.060316);
		assertEquals(vert4.getY(), 48.432044);
		assertEquals(vert4.getCoordinateAt(2), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testPolygon3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01030000C001000000040000003CDBA337DCC351C06D37C1374D374840000000000000224000000000000028403CDBA337DC8351C06D37C1374D3748400000000000002040"
				+ "00000000000026403CDBA337DCC351C06D37C1374DB747400000000000001C4000000000000024403CDBA337DCC351C06D37C1374D37484000000000000022400000000000002840";
		// SELECT ST_GeomFromEWKT('POLYGONM((-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10, -71.060316 48.432044 12))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.POLYGON);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getDimension(), 4);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		Polygon line = (Polygon) geom;
		
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 9.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 8.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 7.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		Point vert4 = line.getVertex(3);
		assertEquals(vert4.getX(), -71.060316);
		assertEquals(vert4.getY(), 48.432044);
		assertEquals(vert4.getCoordinateAt(2), 9.0);
		assertEquals(vert4.getCoordinateAt(3), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testMultiPoint2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01040000400200000001010000403CDBA337DCC351C06D37C1374D374840000000000000284001010000403CDBA337DC8351C06D3"
				+ "7C1374D3748400000000000002640";
		// SELECT ST_GeomFromEWKT('MULTIPOINTM((-71.060316 48.432044 12), (-70.060316 48.432044 11))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTIPOINT);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		assertEquals(geom.getDimension(), 3);
		MultiPoint mpoint = (MultiPoint) geom;
		Point point = mpoint.getPointAt(0);
		assertEquals(point.getX(), -71.060316);
		assertEquals(point.getY(), 48.432044);
		assertEquals(point.getCoordinateAt(2), 12.0);
		
		Point point1 = mpoint.getPointAt(1);
		assertEquals(point1.getX(), -70.060316);
		assertEquals(point1.getY(), 48.432044);
		assertEquals(point1.getCoordinateAt(2), 11.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testMultiPoint3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex ="01040000C00200000001010000C03CDBA337DCC351C06D37C1374D37484000000000000024400000000000002840010"
				+ "10000C03CDBA337DC8351C06D37C1374D37484000000000000022400000000000002640";
		// SELECT ST_GeomFromEWKT('MULTIPOINTZM((-71.060316 48.432044 10 12), (-70.060316 48.432044 9 11))');;
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTIPOINT);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		assertEquals(geom.getDimension(), 4);
		MultiPoint mpoint = (MultiPoint) geom;
		Point point = mpoint.getPointAt(0);
		assertEquals(point.getX(), -71.060316);
		assertEquals(point.getY(), 48.432044);
		assertEquals(point.getCoordinateAt(2), 10.0);
		assertEquals(point.getCoordinateAt(3), 12.0);
		
		Point point1 = mpoint.getPointAt(1);
		assertEquals(point1.getX(), -70.060316);
		assertEquals(point1.getY(), 48.432044);
		assertEquals(point1.getCoordinateAt(2), 9.0);
		assertEquals(point1.getCoordinateAt(3), 11.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	
	public void testMultiLine2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "0105000040020000000102000040030000003CDBA337DCC351C06D37C1374D37484000000000000028403CDBA337DC8351C06D37C1374D3748400000000000"
				+ "0026403CDBA337DCC351C06D37C1374DB7474000000000000024400102000040030000003CDBA337DC0352C06D37C1374DB7474000000000000028403CDBA337DCC351C06D37C1374DB7474000000000000026403CDBA337DC0352C06D37C1374D3747400000000000002440";
		// SELECT ST_GeomFromEWKT('MULTILINESTRINGM((-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10),(-72.060316 47.432044 12, -71.060316 47.432044 11, -72.060316 46.432044 10))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTILINE);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getDimension(), 3);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		MultiLine mline = (MultiLine) geom;
		
		Line line = (Line) mline.getCurveAt(0);
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		Line line1 = (Line) mline.getCurveAt(1);
		vert1 = line1.getVertex(0);
		assertEquals(vert1.getX(), -72.060316);
		assertEquals(vert1.getY(), 47.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		vert2 = line1.getVertex(1);
		assertEquals(vert2.getX(), -71.060316);
		assertEquals(vert2.getY(), 47.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		vert3 = line1.getVertex(2);
		assertEquals(vert3.getX(), -72.060316);
		assertEquals(vert3.getY(), 46.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	public void testMultiLine3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex ="01050000C00200000001020000C0030000003CDBA337DCC351C06D37C1374D374840000000000000F03F00000000000028403CDBA337DC8351C06D37C1374D3748400000"
				+ "00000000004000000000000026403CDBA337DCC351C06D37C1374DB747400000000000000840000000000000244001020000C0030000003CDBA337DC0352C06D37C1374DB74740"
				+ "000000000000104000000000000028403CDBA337DCC351C06D37C1374DB74740000000000000144000000000000026403CDBA337DC0352C06D37C1374D37474000000000000018400000000000002440";
		// SELECT ST_GeomFromEWKT('MULTILINESTRINGZM((-71.060316 48.432044 1 12, -70.060316 48.432044 2 11, -71.060316 47.432044 3 10),(-72.060316 47.432044 4 12, -71.060316 47.432044 5 11, -72.060316 46.432044 6 10))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTILINE);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getDimension(), 4);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		MultiLine mline = (MultiLine) geom;
		
		Line line = (Line) mline.getCurveAt(0);
		Point vert1 = line.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 1.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		Point vert2 = line.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 2.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		Point vert3 = line.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 3.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		Line line1 = (Line) mline.getCurveAt(1);
		vert1 = line1.getVertex(0);
		assertEquals(vert1.getX(), -72.060316);
		assertEquals(vert1.getY(), 47.432044);
		assertEquals(vert1.getCoordinateAt(2), 4.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		vert2 = line1.getVertex(1);
		assertEquals(vert2.getX(), -71.060316);
		assertEquals(vert2.getY(), 47.432044);
		assertEquals(vert2.getCoordinateAt(2), 5.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		vert3 = line1.getVertex(2);
		assertEquals(vert3.getX(), -72.060316);
		assertEquals(vert3.getY(), 46.432044);
		assertEquals(vert3.getCoordinateAt(2), 6.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	
	public void testMultiPolygon2DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "010600004002000000010300004001000000040000003CDBA337DCC351C06D37C1374D37484000000000000028403CDBA337DC8351C06D37C1374D37"
				+ "484000000000000026403CDBA337DCC351C06D37C1374DB7474000000000000024403CDBA337DCC351C06D37C1374D3748400000000000002840010300004001"
				+ "000000040000003CDBA337DC8351C06D37C1374DB7474000000000000028403CDBA337DC8351C06D37C1374D37484000000000000026403CDBA337DCC351C06D"
				+ "37C1374DB7474000000000000024403CDBA337DC8351C06D37C1374DB747400000000000002840";
		// SELECT ST_GeomFromEWKT('MULTIPOLYGONM(((-71.060316 48.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10, -71.060316 48.432044 12)),((-70.060316 47.432044 12, -70.060316 48.432044 11, -71.060316 47.432044 10, -70.060316 47.432044 12)))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTIPOLYGON);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM2DM);
		assertEquals(geom.getDimension(), 3);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), false);
		MultiPolygon mpol = (MultiPolygon) geom;
		
		Polygon pol = (Polygon) mpol.getSurfaceAt(0);
		Point vert1 = pol.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		Point vert2 = pol.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		Point vert3 = pol.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		Point vert4 = pol.getVertex(3);
		assertEquals(vert4.getX(), -71.060316);
		assertEquals(vert4.getY(), 48.432044);
		assertEquals(vert4.getCoordinateAt(2), 12.0);
		
		
		pol = (Polygon) mpol.getSurfaceAt(1);
		vert1 = pol.getVertex(0);
		assertEquals(vert1.getX(), -70.060316);
		assertEquals(vert1.getY(), 47.432044);
		assertEquals(vert1.getCoordinateAt(2), 12.0);
		
		vert2 = pol.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 11.0);
		
		vert3 = pol.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 10.0);
		
		vert4 = pol.getVertex(3);
		assertEquals(vert4.getX(), -70.060316);
		assertEquals(vert4.getY(), 47.432044);
		assertEquals(vert4.getCoordinateAt(2), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	
	public void testMultiPolygon3DM() throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException{
		String wkbHex = "01060000C00200000001030000C001000000040000003CDBA337DCC351C06D37C1374D374840000000000000F03F00000000000028403CDBA3"
				+ "37DC8351C06D37C1374D374840000000000000004000000000000026403CDBA337DCC351C06D37C1374DB74740000000000000084000000000000024"
				+ "403CDBA337DCC351C06D37C1374D374840000000000000F03F000000000000284001030000C001000000040000003CDBA337DC8351C06D37C1374DB7"
				+ "4740000000000000104000000000000028403CDBA337DC8351C06D37C1374D374840000000000000144000000000000026403CDBA337DCC351C06D37"
				+ "C1374DB74740000000000000184000000000000024403CDBA337DC8351C06D37C1374DB7474000000000000010400000000000002840";
		// SELECT ST_GeomFromEWKT('MULTIPOLYGONZM(((-71.060316 48.432044 1 12, -70.060316 48.432044 2 11, -71.060316 47.432044 3 10, -71.060316 48.432044 1 12)),((-70.060316 47.432044 4 12, -70.060316 48.432044 5 11, -71.060316 47.432044 6 10, -70.060316 47.432044 4 12)))');
		
		byte[] wkb = getWKB(wkbHex);
		assertEquals(wkbHex, getWKBHex(wkb));
		PostGISEWKBParser parser = new PostGISEWKBParser();
		Geometry geom = parser.parse(wkb);
		assertEquals(geom.getType(), Geometry.TYPES.MULTIPOLYGON);
		assertEquals(geom.getGeometryType().getSubType(), Geometry.SUBTYPES.GEOM3DM);
		assertEquals(geom.getDimension(), 4);
		assertEquals(geom.getGeometryType().hasM(), true);
		assertEquals(geom.getGeometryType().hasZ(), true);
		MultiPolygon mpol = (MultiPolygon) geom;
		
		Polygon pol = (Polygon) mpol.getSurfaceAt(0);
		Point vert1 = pol.getVertex(0);
		assertEquals(vert1.getX(), -71.060316);
		assertEquals(vert1.getY(), 48.432044);
		assertEquals(vert1.getCoordinateAt(2), 1.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		Point vert2 = pol.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 2.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		Point vert3 = pol.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 3.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		Point vert4 = pol.getVertex(3);
		assertEquals(vert4.getX(), -71.060316);
		assertEquals(vert4.getCoordinateAt(2), 1.0);
		assertEquals(vert4.getCoordinateAt(3), 12.0);
		
		
		pol = (Polygon) mpol.getSurfaceAt(1);
		vert1 = pol.getVertex(0);
		assertEquals(vert1.getX(), -70.060316);
		assertEquals(vert1.getY(), 47.432044);
		assertEquals(vert1.getCoordinateAt(2), 4.0);
		assertEquals(vert1.getCoordinateAt(3), 12.0);
		
		vert2 = pol.getVertex(1);
		assertEquals(vert2.getX(), -70.060316);
		assertEquals(vert2.getY(), 48.432044);
		assertEquals(vert2.getCoordinateAt(2), 5.0);
		assertEquals(vert2.getCoordinateAt(3), 11.0);
		
		vert3 = pol.getVertex(2);
		assertEquals(vert3.getX(), -71.060316);
		assertEquals(vert3.getY(), 47.432044);
		assertEquals(vert3.getCoordinateAt(2), 6.0);
		assertEquals(vert3.getCoordinateAt(3), 10.0);
		
		vert4 = pol.getVertex(3);
		assertEquals(vert4.getX(), -70.060316);
		assertEquals(vert4.getY(), 47.432044);
		assertEquals(vert4.getCoordinateAt(2), 4.0);
		assertEquals(vert4.getCoordinateAt(3), 12.0);
		
		byte[] bytes = geom.convertToEWKB();
		assertEquals(wkbHex, getWKBHex(bytes));
	}
	
	
	
	private String getWKBHex(byte[] bytes) {
		StringBuilder sbuilder = new StringBuilder();
		for(int i=0; i<bytes.length; i++){
			String s = Integer.toHexString(bytes[i]);
			if(s.length() < 2){
				s="0"+s;
			}else if(bytes[i]<0){
				s=s.substring(6);
			}
			sbuilder.append(s);
		}
		return sbuilder.toString().toUpperCase();
	}


	private byte[] getWKB(String wkbHex){
		byte[] aux = new byte[wkbHex.length()/2];
		for(int i=0; i<aux.length; i++){
			int n = Integer.parseInt(wkbHex.substring(i*2, i*2+2), 16);
			aux[i] = (byte) n;
		}
		return aux;
	}


	@Override
	protected void doSetUp() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
