/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.impl;

import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLibrary;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.area.Area;
import org.gvsig.fmap.geom.operation.distance.PointDistance;
import org.gvsig.fmap.geom.operation.ensureOrientation.EnsureOrientation;
import org.gvsig.fmap.geom.operation.flip.Flip;
import org.gvsig.fmap.geom.operation.fromwkb.FromWKB;
import org.gvsig.fmap.geom.operation.fromwkt.FromWKT;
import org.gvsig.fmap.geom.operation.isCCW.IsCCW;
import org.gvsig.fmap.geom.operation.offset.Offset;
import org.gvsig.fmap.geom.operation.perimeter.Perimeter;
import org.gvsig.fmap.geom.operation.perpendicular.Perpendicular;
import org.gvsig.fmap.geom.operation.perpendicular.PerpendicularPoint;
import org.gvsig.fmap.geom.operation.perpendicular.UnitVector;
import org.gvsig.fmap.geom.operation.relationship.Contains;
import org.gvsig.fmap.geom.operation.relationship.Crosses;
import org.gvsig.fmap.geom.operation.relationship.Disjoint;
import org.gvsig.fmap.geom.operation.relationship.Equals;
import org.gvsig.fmap.geom.operation.relationship.Intersects;
import org.gvsig.fmap.geom.operation.relationship.Out;
import org.gvsig.fmap.geom.operation.relationship.Overlaps;
import org.gvsig.fmap.geom.operation.relationship.Touches;
import org.gvsig.fmap.geom.operation.relationship.Within;
import org.gvsig.fmap.geom.operation.utils.PointGetAngle;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultGeometryOperationLibrary extends AbstractLibrary  {

    public void doRegistration() {
        registerAsServiceOf(GeometryLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
    }

    protected void doPostInitialize() throws LibraryException {

        // Validate there is any implementation registered.
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        geometryManager.registerGeometryOperation(Contains.NAME, new Contains());
        geometryManager.registerGeometryOperation(Crosses.NAME, new Crosses());
        geometryManager.registerGeometryOperation(Disjoint.NAME, new Disjoint());
        geometryManager.registerGeometryOperation(Equals.NAME, new Equals());
        geometryManager.registerGeometryOperation(Intersects.NAME, new Intersects());
        geometryManager.registerGeometryOperation(Overlaps.NAME, new Overlaps());
        geometryManager.registerGeometryOperation(Touches.NAME, new Touches());
        geometryManager.registerGeometryOperation(Within.NAME, new Within());
        geometryManager.registerGeometryOperation(IsCCW.NAME, new IsCCW());
//        geometryManager.registerGeometryOperation(FromWKT.NAME, new FromWKT());
//        geometryManager.registerGeometryOperation(FromWKB.NAME, new FromWKB());
        geometryManager.registerGeometryOperation(Flip.NAME, new Flip());
        geometryManager.registerGeometryOperation(EnsureOrientation.NAME,  new EnsureOrientation());
        geometryManager.registerGeometryOperation(PointDistance.NAME,  new PointDistance(), TYPES.POINT);
        geometryManager.registerGeometryOperation(PointGetAngle.NAME, new PointGetAngle());
        geometryManager.registerGeometryOperation(Out.NAME, new Out());
        geometryManager.registerGeometryOperation(Area.NAME, new Area());
        geometryManager.registerGeometryOperation(Perimeter.NAME, new Perimeter());
        geometryManager.registerGeometryOperation(Perpendicular.NAME,  new Perpendicular(), TYPES.POINT);
        geometryManager.registerGeometryOperation(PerpendicularPoint.NAME,  new PerpendicularPoint(), TYPES.POINT);
        geometryManager.registerGeometryOperation(UnitVector.NAME,  new UnitVector(), TYPES.POINT);
        geometryManager.registerGeometryOperation(Offset.NAME, new Offset());


        //To JTS
//        geometryManager.registerGeometryOperation(ToJTS.NAME, new Point2DToJTS(), TYPES.POINT);
//        geometryManager.registerGeometryOperationBySuperType(ToJTS.NAME, new Curve2DToJTS(), TYPES.CURVE);
//        geometryManager.registerGeometryOperationBySuperType(ToJTS.NAME, new Surface2DToJTS(), TYPES.SURFACE);
//        geometryManager.registerGeometryOperation(ToJTS.NAME, new MultiPointToJTS(), TYPES.MULTIPOINT);
//        geometryManager.registerGeometryOperation(ToJTS.NAME, new MultiCurveToJTS(), TYPES.MULTICURVE);
//        geometryManager.registerGeometryOperation(ToJTS.NAME, new MultiSurfaceToJTS(), TYPES.MULTISURFACE);
//        geometryManager.registerGeometryOperation(ToJTS.NAME, new BaseMultiPrimitiveToJTS(), TYPES.AGGREGATE);
//        geometryManager.registerGeometryOperation(Surface2DToJTSLineString.NAME, new Surface2DToJTSLineString(), TYPES.SURFACE);
//        geometryManager.registerGeometryOperation(Surface2DToJTSLineString.NAME, new Surface2DToJTSLineString(), TYPES.MULTISURFACE);
    }
}
