/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.perpendicular;

import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class PerpendicularOperationContext extends GeometryOperationContext {
    public static final String SECOND_POINT = "secondPoint";
    public static final String PERPENDICULAR_POINT = "perpendicularPoint";

    public PerpendicularOperationContext(Point secondPoint, Point perpendicularPoint) {
        this.setSecondPoint(secondPoint);
        this.setPerpendicularPoint(perpendicularPoint);      
    }    
    
    public void setSecondPoint(Point secondPoint) {
        this.setAttribute(SECOND_POINT, secondPoint);
    }

    public Point getSecondPoint() {
        return (Point) this.getAttribute(SECOND_POINT);
    }
    
    public void setPerpendicularPoint(Point perpendicularPoint) {
        this.setAttribute(PERPENDICULAR_POINT, perpendicularPoint);
    }

    public Point getPerpendicularPoint() {
        return (Point) this.getAttribute(PERPENDICULAR_POINT);
    }
}
