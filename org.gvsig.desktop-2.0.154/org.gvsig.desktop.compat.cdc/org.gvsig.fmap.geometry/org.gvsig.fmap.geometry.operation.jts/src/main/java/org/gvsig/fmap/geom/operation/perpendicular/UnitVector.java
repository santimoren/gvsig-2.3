/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.perpendicular;

import java.awt.geom.Point2D;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * Returns an unit vector from two points.
 * 
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class UnitVector extends GeometryOperation{
    public static final String NAME = "unitvector";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);

    public Object invoke(Geometry geom, GeometryOperationContext ctx)
        throws GeometryOperationException {
        Point point1 = (Point)geom;
        Point point2 = (Point)ctx.getAttribute(PerpendicularOperationContext.SECOND_POINT);

        Point2D paux = new Point2D.Double(point2.getX() - point1.getX(),
            point2.getY() - point1.getY());

        double v = Math.sqrt(Math.pow(paux.getX(), 2d) +
            Math.pow(paux.getY(), 2d));

        paux = new Point2D.Double(paux.getX() / v, paux.getY() / v);

        try{
            return  geomManager.createPoint(paux.getX() / v, paux.getY() / v, SUBTYPES.GEOM2D);
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        }
    }

    public int getOperationIndex() {      
        return CODE;
    }    
}
