/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.fromjts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.towkt.ToWKT;

/**
 * Deprecated: Esta clase est� obsoleta, se mantiene por compatibilidad con las constantes
 *
 * <p>
 * Converts a geometry from JTS to DAL format. This is a special geometry
 * operation
 * because don't use the Geometry parameter and returns a JTS geometry. This
 * kind
 * of operation can be called from the {@link GeometryManager} using
 * invokeOperation.
 * </p>
 * <p>
 * This operation needs a JTS geometry loaded in the context. The only attribute
 * of the {@link GeometryOperation} have to be a geometry of this type and the
 * ID
 * of this attribute will be JTSGeometry.
 * </p>
 *
 * @author Nacho Brodin (nachobrodin@gmail.com)
 *
 */
public class FromJTS extends GeometryOperation {

    public static final String NAME = "fromJTS";
    public static final String PARAM = "JTSGeometry";
    public static final int CODE = GeometryLocator.getGeometryManager().getGeometryOperationCode(NAME);
    protected static final Logger logger = LoggerFactory.getLogger(FromJTS.class);


    public int getOperationIndex() {
        logger.warn("Called getOperationIndex method of deprecated operation FromJTS");
        return CODE;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
            throws GeometryOperationException {
        logger.warn("Invoked deprecated operation FromJTS");
        throw new GeometryOperationException(null);
    }

}
