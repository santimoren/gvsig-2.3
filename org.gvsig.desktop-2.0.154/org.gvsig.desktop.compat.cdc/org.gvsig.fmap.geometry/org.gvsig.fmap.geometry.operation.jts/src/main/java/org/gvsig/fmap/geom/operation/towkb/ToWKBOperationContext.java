/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.towkb;

import org.gvsig.fmap.geom.operation.GeometryOperationContext;

/**
 * @author jmvivo
 *
 */
public class ToWKBOperationContext extends GeometryOperationContext {
	public static final String SRS = "srs";

	/**
	 * Set the srID to use in the WKB geometry
	 * 
	 * @param srID
	 */
	public void setSrID(int srID) {
		this.setAttribute(SRS, new Integer(srID));
	}

	/**
	 * Get the srId to use in the WKB geometry
	 * 
	 * @return
	 */
	public int getSrID() {
		Integer srs = (Integer) this.getAttribute(SRS);
		if( srs == null ) {
			return -1;
		}
		return srs.intValue();
	}


}
