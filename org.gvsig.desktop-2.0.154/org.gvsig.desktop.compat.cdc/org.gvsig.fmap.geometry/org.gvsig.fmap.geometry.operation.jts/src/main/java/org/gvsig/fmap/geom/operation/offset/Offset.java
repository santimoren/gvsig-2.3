/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.offset;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;


/**
 * This class builds an offset from a geometry and a distance
 *
 * @author fdiaz
 * @version $Id$
 *
 */
public class Offset extends GeometryOperation {
    public static final String NAME = "offset";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
        try {
            return geom.offset(((Double)ctx.getAttribute("geom")).doubleValue());
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryOperationException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#getOperationIndex()
     */
    public int getOperationIndex() {
        return CODE;
        }

}
