/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.isCCW;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * This class checks if the first part from the General Path of a complex geometry is CCW.
 * @return Boolean <code>true<code> if is CCW
 * @author Carlos S�nchez Peri��n <a href = "mailto:csanchez@prodevelop.es"> e-mail </a>
 */
public class IsCCW extends GeometryOperation{
    public static final String NAME = "isCCW";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);

	public int getOperationIndex() {
		return CODE;
	}

	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		try {
            return new Boolean(geom.isCCW());
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryOperationException(e);
        }
	}

}
