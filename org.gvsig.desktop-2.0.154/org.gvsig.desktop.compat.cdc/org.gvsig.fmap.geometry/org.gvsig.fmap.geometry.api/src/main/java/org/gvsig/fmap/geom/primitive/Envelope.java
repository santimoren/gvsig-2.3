/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.primitive;

import org.cresques.cts.ICoordTrans;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.Persistent;

/**
 * <p>
 * This interface is equivalent to the GM_Envelope specified in
 * <a
 * href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO
 * 19107</a>. A minimum bounding box or rectangle. Regardless of dimension, an
 * Envelope can be represented without ambiguity as two direct positions
 * (coordinate points). To encode an Envelope, it is sufficient to encode these
 * two points. This is consistent with all of the data types in this
 * specification, their state is represented by their publicly accessible
 * attributes.
 * </p>
 *
 * @see <a
 * href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO
 * 19107</a>
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface Envelope extends Persistent, Cloneable {

    /**
     * Returns the center ordinate along the specified dimension.
     *
     * @param dimension. The dimension
     * @return The value of the ordinate.
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    double getCenter(int dimension);

    /**
     * The length of coordinate sequence (the number of entries) in this
     * envelope.
     *
     * @return The dimension of the envelope.
     */
    int getDimension();

    /**
     * Returns the envelope length along the specified dimension.
     *
     * @param dimension The dimension.
     * @return The envelope length along a dimension.
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    double getLength(int dimension);

    /**
     * A coordinate position consisting of all the minimal ordinates for each
     * dimension for all points within the Envelope.
     *
     * @return The lower corner.
     */
    Point getLowerCorner();

    /**
     * Sets the coordinate position consisting of all the minimal ordinates for
     * each dimension for all points within the Envelope.
     *
     * @param point The lower corner.
     */
    void setLowerCorner(Point point);

    /**
     * Returns the maximal ordinate along the specified dimension.
     *
     * @param dimension The dimension.
     * @return The maximum value
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    double getMaximum(int dimension);

    /**
     * Returns the minimal ordinate along the specified dimension.
     *
     * @param dimension The dimension.
     * @return The minimum value.
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    double getMinimum(int dimension);

    /**
     * A coordinate position consisting of all the maximal ordinates for each
     * dimension for all points within the Envelope.
     *
     * @return The upper corner
     */
    Point getUpperCorner();

    /**
     * Sets the coordinate position consisting of all the maximal ordinates for
     * each dimension for all points within the Envelope.
     *
     * @param point The upper corner.
     */
    void setUpperCorner(Point upperCorner);

    /**
     * Adds a envelope to the current envelope.
     *
     * If the envelope to add is null or empty do not modify the current
     * enevelop and don't throw a exception.
     *
     * @param envelope The envelope to add.
     */
    void add(Envelope envelope);

    /**
     * Utility method to add the envelop of geometry to the current envelope.
     *
     * Is equivalent to:
     *  <code>add(geometry.getEnvelope())</code>
     *
     * @param envelope The envelope to add.
     */
    void add(Geometry geometry);

    /**
     * It returns the equivalent of an envelope like a geometry.
     *
     * @return A geometry that contains the same area that the envelope.
     *
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    Geometry getGeometry();

    /**
     * Returns <code>true</code> if the new envelope is contained in the current
     * envelope.
     *
     * @param envelope The envelope to compare.
     * @return If the current envelope contains the new envelope
     */
    boolean contains(Envelope envelope);

    /**
     * Returns <code>true</code> if the new envelope intersects with the current
     * envelope.
     *
     * @param envelope The envelope to compare.
     * @return If the current envelope intersects with the new envelope
     */
    boolean intersects(Envelope envelope);

    /**
     * Returns <code>true</code> if the geometry intersects with the current
     * envelope.
     *
     * @param geometry The geometry to compare.
     * @return If the current envelope intersects with the geometry
     */
    boolean intersects(Geometry geometry);

    /**
     * Converts the envelope to other coordinate reference system
     *
     * @param trans The CRS conversor
     * @return A new envelope in other CRS
     * @throws EnvelopeNotInitializedException if the envelope is empty.
     */
    Envelope convert(ICoordTrans trans);

    /**
     * Gets if the envelope is <code>null</code> or not. Is Empty means that the
     * lower and upper corner are <code>null</code>.
     *
     * @return <code>null</code> or not if is empty.
     */
    boolean isEmpty();

    void clear();

    /**
     * Centers the envelope to a given point
     * @param p Point to be centered
     */
    public void centerTo(Point p);

}
