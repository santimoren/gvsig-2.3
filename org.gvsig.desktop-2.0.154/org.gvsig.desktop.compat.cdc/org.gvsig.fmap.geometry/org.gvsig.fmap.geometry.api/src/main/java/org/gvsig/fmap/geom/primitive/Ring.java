
package org.gvsig.fmap.geom.primitive;


public interface Ring extends OrientablePrimitive, Iterable<Point> {
    
}
