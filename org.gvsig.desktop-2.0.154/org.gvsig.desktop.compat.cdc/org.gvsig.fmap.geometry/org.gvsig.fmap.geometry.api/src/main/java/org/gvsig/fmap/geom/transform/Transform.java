package org.gvsig.fmap.geom.transform;

import org.gvsig.fmap.geom.primitive.Point;

public interface Transform {
    
    public abstract Transform inverse();
    
    public abstract Point[] transform(Point[] src, Point[] dst);

    public abstract Point transform(Point src);
    
    public abstract boolean isIdentity();
    
    public abstract Transform concatenate(Transform other);
    
    public abstract Transform preConcatenate(Transform other);
    
}
