/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;

/**
 * Every geometry operation that is registered dynamically must extend this class.<br>
 *
 * The following example shows how to implement and register a custom operation:
 *  
 * <pre>
 * public class MyOperation extends GeometryOperation {
 * 
 *   // Check GeometryManager for alternative methods to register an operation  
 *   public static final int CODE = 
 *     GeometryManager.getInstance()
 *        .registerGeometryOperation("MyOperation", new MyOperation(), geomType);
 *   
 *   public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
 *        // Operation logic goes here
 *   }     
 *   
 *   public int getOperationIndex() {
 *      return CODE;
 *   }
 *   
 * }
 * </pre>
 *
 * @author jiyarza
 *
 */
public abstract class GeometryOperation {
	
	
	// Constants for well-known operations to avoid dependency between geometry model and
	// operations.
	public static final String OPERATION_INTERSECTS_NAME = "intersects";
	public static final String OPERATION_CONTAINS_NAME = "contains";
	
	public static int OPERATION_INTERSECTS_CODE = Integer.MIN_VALUE;
	public static int OPERATION_CONTAINS_CODE = Integer.MIN_VALUE;

	
	
	
	/**
	 * Invokes this operation given the geometry and context 
	 * @param geom Geometry to which apply this operation
	 * @param ctx Parameter container
	 * @return Place-holder object that may contain any specific return value. 
	 * @throws GeometryOperationException The implementation is responsible to throw this exception when needed.
	 */
	public abstract Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException;

	/**
	 * Returns the constant value that identifies this operation and that was obtained upon registering it. 
	 * @return operation unique index 
	 */
	public abstract int getOperationIndex();
}
