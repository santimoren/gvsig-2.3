/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is a default parameter container for geometry operation.<br>
 * 
 * Normally every GeometryOperation will extend this class and identify
 * its parameters publicly with getters/setters
 *
 * For those operations that need high performance, parameters should be declared as class 
 * members instead of using the setAttribute/getAttribute mechanism. This way you avoid a hash
 * and a cast operation.
 * 
 * @author jyarza
 *
 */
public class GeometryOperationContext {
	
	private Map ctx = new HashMap();
	
	/**
	 * Returns an attribute given its name.
	 * If it does not exist returns <code>null</code>
	 * @param name
	 * @return attribute
	 */
	public Object getAttribute(String name) {
		return ctx.get(name.toLowerCase());
	}
	
	/**
	 * Sets an attribute
	 * @param name
	 * @param value
	 */
	public GeometryOperationContext setAttribute(String name, Object value) {
		ctx.put(name.toLowerCase(), value);
		return this;
	}

}
