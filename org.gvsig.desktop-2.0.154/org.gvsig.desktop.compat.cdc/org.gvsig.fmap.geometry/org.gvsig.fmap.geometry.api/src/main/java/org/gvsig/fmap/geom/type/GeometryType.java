/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.type;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;

/**
 * This class represents the type of a geometry. All the geometries
 * has to have a type that can be retrieved using the 
 * {@link Geometry}{@link #getGeometryType()} method.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface GeometryType {
	
	/**
	 * @return the name of the geometry type.
	 */
	public String getName();
	
        /**
         * Return the full name of this geometry type.
         * It include the type name and the subtype name.
         * 
         * @return the full name of the geometry type
         */
        public String getFullName();
	/**
	 * @return the type of the geometry. It is a constant value
	 * that has to be one of the values in {@link Geometry.TYPES}
	 * The type is an abstract representation of the object (Point, Curve...) 
	 * but it is not a concrete representation (Point2D, Point3D...). 
	 */
	public int getType();	
	
	/**
	 * @return the subtype of the geometry. It is a constant value
	 * that has to be one of the values in {@link Geometry.SUBTYPES}.
	 * The subtype represents a set of geometries with a 
	 * dimensional relationship (2D, 3D, 2DM...)
	 */
	public int getSubType();
	
	/**
	 * Check if a geometry type inherits of other type. E.g:
	 * the super type of an arc could be a a curve, the super 
	 * type of a circle could be a surface...
	 * @param geometryType
	 * the value of the {@link Geometry.TYPES} to check if is 
	 * it super type
	 * @return
	 * <true> if the the parameter is a super type of this
	 * geometry type
	 */
	public boolean isTypeOf(int geometryType);
	   
	/**
     * Check if a geometry subType inherits of other subType. E.g:
     * the super Subtype of a geometry 3D could be a geometry 2D, 
     * because the 3D extends the behavior of a geometry 2D.
     * @param geometrySubType
     * the value of the {@link Geometry.SUBTYPES} to check if is 
     * it super subType
     * @return
     * <true> if the the parameter is a super subType of this
     * geometry type
     */
	public boolean isSubTypeOf(int geometrySubType);

    /**
     * Check if a geometry type inherits of other type. E.g:
     * the super type of an arc could be a a curve, the super
     * type of a circle could be a surface...
     * 
     * @param geometryType
     *            the geometry type to check if is it super type
     * @return
     *         if the the parameter is a super type of this
     *         geometry type
     */
    public boolean isTypeOf(GeometryType geometryType);

    /**
     * Check if a geometry subType inherits of other subType. E.g:
     * the super Subtype of a geometry 3D could be a geometry 2D,
     * because the 3D extends the behavior of a geometry 2D.
     * 
     * @param geometryType
     *            the geometry type to check if is it super subtype
     * @return
     *         if the the parameter is a super subType of this
     *         geometry type
     */
    public boolean isSubTypeOf(GeometryType geometryType);
	
	/**
	 * This method creates a {@link Geometry} with the type specified 
	 * by this class. The geometry is empty, and all the internal 
	 * attributes must be assigned to a value when the geometry has  
	 * been created.
	 * 
	 * @return
	 * A empty geometry 
	 * @throws InstantiationException
	 * This exception is maybe thrown when  the application is  trying 
	 * to instantiate the geometry
	 * @throws IllegalAccessException
	 * This exception is maybe thrown when  the application is  trying 
	 * to instantiate the geometry
	 */
	public Geometry create() throws CreateGeometryException;
		
	/**
	 * Registers an operation for this geometry type. 
	 * @param index
	 * @param geomOp
	 */
	public void setGeometryOperation(int index, GeometryOperation geomOp);
	
	/**
	 * Get the operation for this geometry at a concrete position
	 * @param index
	 * The position of the operation
	 * @return
	 * A geometry operation
	 */
	public GeometryOperation getGeometryOperation(int index);
		
	/**
	 * @return the geometry as a String
	 */
	public String toString();
        
        public int getDimension();
        
        
    public boolean hasZ();
    
    public boolean hasM();
	
}
