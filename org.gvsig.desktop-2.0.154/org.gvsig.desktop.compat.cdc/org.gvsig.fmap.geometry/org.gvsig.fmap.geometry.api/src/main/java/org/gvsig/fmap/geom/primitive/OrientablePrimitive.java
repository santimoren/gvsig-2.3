/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.primitive;


/**
 * <p>
 * This interface is equivalent to the GM_OrientablePrimitive specified in
 * <a href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO 19107</a>.
 * Orientable primitives are those that can be mirrored into new
 * geometric objects in terms of their internal local coordinate
 * systems (manifold charts).
 * </p>
 * <p>
 * For curves, the orientation reflects the direction in which the curve is traversed,
 * that is, the sense of its parameterization. When used as boundary curves,
 * the surface being bounded is to the "left" of the oriented curve.
 * </p>
 * <p>
 * For surfaces, the orientation reflects from which direction the local coordinate
 * system can be viewed as right handed, the "top" or the surface being the direction
 * of a completing z-axis that would form a right-handed system.
 * </p>
 * <p>
 * When used as a boundary surface, the bounded solid is "below" the surface.
 * The orientation of points and solids has no immediate geometric interpretation
 * in 3-dimensional space.
 * </p>
 * <p> OrientablePrimitive objects are essentially references to geometric primitives
 * that carry an "orientation" reversal flag (either "+" or "-") that determines whether
 * this primitive agrees or disagrees with the orientation of the referenced object.
 * </p>
 * @see <a href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO 19107</a>
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface OrientablePrimitive extends Primitive {

    /**
     * Gets the one of the values of a coordinate (direct position) in a
     * concrete dimension.
     *
     * @param index
     *            The index of the direct position to set.
     * @param dimension
     *            The dimension of the direct position.
     * @return The value of the coordinate
     */
    public double getCoordinateAt(int index, int dimension);


    /**
     * Sets the value of a coordinate (direct position) in a concrete dimension
     * @param index
     * The index of the direct position to set
     * @param dimension
     * The dimension of the direct position
     * @param value
     * The value to set
     */
    public void setCoordinateAt(int index, int dimension, double value);

    /**
     * Adds a vertex (or direct position) to the curve
     * @param point
     * The new point to add
     */
    public void addVertex(Point point);

    /**
     * Utility method for add a vertex
     * @param x
     * @param y
     * @see #addVertex(Point)
     */
    public void addVertex(double x, double y);

    /**
     * Utility method for add a vertex
     * @param x
     * @param y
     * @param z
     * @see #addVertex(Point)
     */
    public void addVertex(double x, double y, double z);

    /**
     * Remove a vertex (direct position) to the curve
     * @param index
     * The index of the vertex to remove
     */
    public void removeVertex(int index);

    /**
     * Gets a vertex (direct position)
     * @param index
     * The index of the vertex to get
     * @return
     * One point
     */
    public Point getVertex(int index);

    /**
     * Gets the number of vertices (direct positions) of the curve
     * @return
     * The number of vertices
     */
    public int getNumVertices();

    /**
     * Inserts a vertex (direct position) to the curve.
     * @param index
     * The index of the vertex where the new point has to be added.
     * @param p
     * The vertex to add.
     */
    public void insertVertex(int index, Point p);

    /**
     * Sets a vertex in a concrete position and replaces the
     * previous one that was in this position.
     * @param index
     * The index of the vertex where the new point has to be replaced.
     * @param p
     * The vertex to set.
     */
    public void setVertex(int index, Point p);

    /**
     * Sets all the coordinates of the curve
     * @param generalPathX The generalPath that contains all the coordinates
     * @deprecated use addVertex
     */
    public void setGeneralPath(GeneralPathX generalPathX);

    /**
     * Adds a vertex (or direct position) to the curve
     * @param point
     * The new point to add
     * @deprecated
     *      create a multigeometry or use methods to
     *      add an inner surface
     */
    public void addMoveToVertex(Point point);

    /**
     * Closes the geometry
     * @deprecated
     *      create a multigeometry, and it is not necessary to close a
     *      geometry.
     */
    public void closePrimitive();

    public void ensureCapacity(int capacity);

}
