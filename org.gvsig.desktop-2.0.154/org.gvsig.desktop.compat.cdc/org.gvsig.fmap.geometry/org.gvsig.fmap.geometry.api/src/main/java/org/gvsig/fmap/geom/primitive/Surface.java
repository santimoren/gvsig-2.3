/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.primitive;

/**
 * <p>
 * This interface is equivalent to the GM_Surface specified in <a href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012"
 * >ISO 19107</a>. Surface is a subclass of {@link Primitive} and is the basis
 * for 2-dimensional geometry. Unorientable surfaces such as the M�bius band
 * are not allowed.
 * <p/>
 * <p>
 * The orientation of a surface chooses an "up" direction through the choice of
 * the upward normal, which, if the surface is not a cycle, is the side of the
 * surface from which the exterior boundary appears counterclockwise. Reversal
 * of the surface orientation reverses the curve orientation of each boundary
 * component, and interchanges the conceptual "up" and "down" direction of the
 * surface.
 * </p>
 * <p>
 * If the surface is the boundary of a solid, the "up" direction is usually
 * outward. For closed surfaces, which have no boundary, the up direction is
 * that of the surface patches, which must be consistent with one another.
 * </p>
 *
 * @see <a
 *      href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO
 *      19107</a>
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 * @author <a href="mailto:jtorres@ai2.upv.es">Jordi Torres Fabra</a>
 */
public interface Surface extends OrientableSurface {

	/**
	 * Sets the appearance of the Surface
	 *
	 * @param app
	 *            The appearance of the surface
	 */
	public void setSurfaceAppearance(SurfaceAppearance app);

	/**
	 * Gets surface appearance
	 *
	 * @return the surface appearance
	 *
	 */
	public SurfaceAppearance getSurfaceAppearance();

        public int getNumInteriorRings();

        public Ring getInteriorRing(int index);

        public void addInteriorRing(Ring ring);

        /**
         * Creates a ring from the coordinates of the line and adds it to the surface like an inner ring.
         *
         * @param ring
         */
        public void addInteriorRing(Line line);

        /**
         * Creates a ring from the coordinates of the shell of the polygon and adds it to the surface like an inner ring.
         *
         * @param polygon
         */
        public void addInteriorRing(Polygon polygon);

        public void removeInteriorRing(int index);

}
