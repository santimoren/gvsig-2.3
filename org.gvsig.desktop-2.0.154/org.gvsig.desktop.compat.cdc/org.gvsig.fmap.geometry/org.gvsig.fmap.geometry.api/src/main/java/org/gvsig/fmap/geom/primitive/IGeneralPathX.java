/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gvsig.fmap.geom.primitive;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import org.cresques.cts.ICoordTrans;

/**
 *
 * @author usuario
 */
public interface IGeneralPathX {
    byte SEG_CLOSE = (byte) PathIterator.SEG_CLOSE;
    byte SEG_CUBICTO = (byte) PathIterator.SEG_CUBICTO;
    byte SEG_LINETO = (byte) PathIterator.SEG_LINETO;
    byte SEG_MOVETO = (byte) PathIterator.SEG_MOVETO;
    byte SEG_QUADTO = (byte) PathIterator.SEG_QUADTO;
    
    /**
     * An even-odd winding rule for determining the interior of
     * a path.
     */
    int WIND_EVEN_ODD = PathIterator.WIND_EVEN_ODD;
    /**
     * A non-zero winding rule for determining the interior of a
     * path.
     */
    int WIND_NON_ZERO = PathIterator.WIND_NON_ZERO;
    int[] curvesize = {1, 1, 2, 3, 0};

    void addSegment(Point[] segment);

    void append(PathIterator pi, boolean connect);
    
    public void append(GeneralPathX gp);

    Object clone();

    void closePath();

    boolean contains(double x, double y);

    boolean contains(Point2D p);

    boolean contains(double x, double y, double w, double h);

    boolean contains(Rectangle2D r);

    Shape createTransformedShape(AffineTransform at);

    void curveTo(double x1, double y1, double x2, double y2, double x3, double y3);

    void curveTo(Point point1, Point point2, Point point3);

    void flip();

    double[] get3DCoordinatesAt(int index);

    Rectangle getBounds();

    Rectangle2D getBounds2D();

    double[] getCoordinatesAt(int index);

    Point2D getCurrentPoint();

    int getNumCoords();

    int getNumTypes();

    PathIterator getPathIterator(AffineTransform at);

    PathIterator getPathIterator(AffineTransform at, double flatness);

    Point getPointAt(int index);

    double[] getPointCoords();

    byte[] getPointTypes();

    byte getTypeAt(int index);

    int getWindingRule();

    boolean intersects(double x, double y, double w, double h);

    boolean intersects(Rectangle2D r);

    boolean isCCW();

    boolean isClosed();

    boolean isSimple();

    void lineTo(double x, double y);

    void lineTo(Point point);

    void moveTo(double x, double y);

    void moveTo(Point point);

    void quadTo(double x1, double y1, double x2, double y2);

    void quadTo(Point point1, Point point2);

    void reProject(ICoordTrans ct);

    void reset();

    int setNumCoords(int numCoords);

    void setNumTypes(int numTypes);

    void setPointCoords(double[] pointCoords);

    void setPointTypes(byte[] pointTypes);

    void setWindingRule(int rule);

    void transform(AffineTransform at);
    
}
