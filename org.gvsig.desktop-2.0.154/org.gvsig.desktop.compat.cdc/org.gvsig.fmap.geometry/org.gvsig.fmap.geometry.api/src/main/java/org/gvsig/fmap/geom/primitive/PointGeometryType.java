/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.primitive;

import org.gvsig.fmap.geom.type.GeometryType;

/**
 * Geometry type for point geometries.
 *
 * @author gvSIG Team
 */
/**
 *
 * This class is deprecated.
 * Use GeometryType.
 *
 * @deprecated use GeometryType
 */
public interface PointGeometryType extends GeometryType {

    /**
     * Creates a new point object
     * @param x the x coordinate of the point
     * @param y the y coordinate of the point
     * @return the new point
     */
    Point createPoint(double x, double y);

    /**
     * Creates a new point object
     * @param coordinates the coordinates of the point
     * @return the new point
     */
    Point createPoint(double[] coordinates);
}
