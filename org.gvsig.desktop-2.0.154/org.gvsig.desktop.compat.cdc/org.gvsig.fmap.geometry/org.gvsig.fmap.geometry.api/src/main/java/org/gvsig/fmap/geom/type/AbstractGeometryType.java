/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.type;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperation;

public abstract class AbstractGeometryType implements GeometryType {

    /**
     * Registered operations for a concrete geometry type
     */
    private List geometryOperations = new ArrayList();
    
    public boolean isTypeOf(GeometryType geometryType) {
        return isTypeOf(geometryType.getType());
    }

    public boolean isSubTypeOf(GeometryType geometryType) {
        return isSubTypeOf(geometryType.getSubType());
    }

    public void setGeometryOperation(int index, GeometryOperation geomOp) {
        while (index > geometryOperations.size()) {
            geometryOperations.add(null);
        }

        if (index == geometryOperations.size()) {
            geometryOperations.add(geomOp);
        } else {
            geometryOperations.set(index, geomOp);
        }
    }

    public GeometryOperation getGeometryOperation(int index) {
        return (GeometryOperation) geometryOperations.get(index);
    }

    public boolean equals(Object obj) {
        if (obj instanceof GeometryType) {
            GeometryType other = (GeometryType) obj;
            return getType() == other.getType()
                && getSubType() == other.getSubType();
        }
        return false;
    }

    protected List getGeometryOperations() {
        return geometryOperations;
    }

    public String toString() {
        StringBuffer sb =
            new StringBuffer("[").append(getName()).append(",[")
                .append(getGeometryOperations().toString()).append("]");

        return sb.toString();
    }
    
	public boolean hasZ() {
		int subtype = this.getSubType();
		return subtype == Geometry.SUBTYPES.GEOM3D || subtype == Geometry.SUBTYPES.GEOM3DM;
	}

	public boolean hasM() {
		int subtype = this.getSubType();
		return subtype == Geometry.SUBTYPES.GEOM2DM || subtype == Geometry.SUBTYPES.GEOM3DM;
	}

}
