/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.visitor.Visitor;

public 	interface SpatialIndex extends Service {
    
        public SpatialIndexFactory getFactory();
        
        public void open();
        public void close();
    
	public void query(Envelope envelope, Visitor visitor);
	public void query(Geometry geom, Visitor visitor);
        
	public Iterator query(Envelope envelope, long limit);
	public Iterator query(Envelope envelope);
	public Iterator query(Geometry geom, long limit);
	public Iterator query(Geometry geom);

       	public Iterator queryNearest(Envelope envelope, long limit);
        public Iterator queryNearest(Envelope envelope);
	public Iterator queryNearest(Geometry geom, long limit);
	public Iterator queryNearest(Geometry geom);

        public Iterator queryAll();
	
        public List queryAsList(Envelope envelope);
	public List queryAsList(Geometry geom);
	public List queryAllAsList();
	
	public void insert(Envelope envelope, Object data);
	public void insert(Geometry geom, Object data);
        public void insert(Geometry geom);

	public boolean remove(Envelope envelope, Object data);
	public boolean remove(Geometry geom, Object data);
        public boolean remove(Geometry geom);

        public void removeAll();

        public long size();
        
        public void flush();
}
