/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.primitive;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import org.cresques.cts.ICoordTrans;
import org.gvsig.fmap.geom.GeometryLocator;

/**
 * 
 * This class is deprecated.
 * Use the API of Geometry to manipulate the geometry,
 * 
 * @deprecated use the geometry methods
 */
public class GeneralPathX implements Shape, Cloneable, Serializable, IGeneralPathX {


    protected static final int INIT_SIZE = 20;
    
    private IGeneralPathX implementation = null;
    
    protected GeneralPathX(boolean nouse) {
        super();
    }
    
    /**
     * @deprecated to create a GeneralPathX use GeometryManager.createGeneralPath
     */
    public GeneralPathX() {
    	this.implementation = GeometryLocator.getGeometryManager().createGeneralPath(WIND_EVEN_ODD, null);
    }

    /**
     * @deprecated to create a GeneralPathX use GeometryManager.createGeneralPath
     */
    public GeneralPathX(int rule) {
    	this.implementation = GeometryLocator.getGeometryManager().createGeneralPath(rule, null);
    }

    /**
     * @deprecated to create a GeneralPathX use GeometryManager.createGeneralPath
     */
    public GeneralPathX(int rule, int initialCapacity) {
    	this.implementation = GeometryLocator.getGeometryManager().createGeneralPath(rule, null);
    }

    /**
     * @deprecated to create a GeneralPathX use GeometryManager.createGeneralPath
     */
    public GeneralPathX(PathIterator pathIterator) {
    	this.implementation = GeometryLocator.getGeometryManager().createGeneralPath(WIND_EVEN_ODD, pathIterator);
    }

    public synchronized void moveTo(double x, double y) {
    	this.implementation.moveTo(x, y);
    }

    public synchronized void moveTo(Point point) {
    	this.implementation.moveTo(point);
    }

    public synchronized void lineTo(double x, double y) {
    	this.implementation.lineTo(x, y);
    }

    public synchronized void lineTo(Point point) {
    	this.implementation.lineTo(point);
    }

    public synchronized void addSegment(Point[] segment) {
    	this.implementation.addSegment(segment);
    }

    public synchronized void quadTo(double x1, double y1, double x2, double y2) {
    	this.implementation.quadTo(x1, y1, x2, y2);
    }

    public synchronized void quadTo(Point point1, Point point2) {
    	this.implementation.quadTo(point1, point2);
    }

    public synchronized void curveTo(double x1, double y1, double x2,
        double y2, double x3, double y3) {
    	this.implementation.curveTo(x1, y1, x2, y2, x3, y3);
    }

    public synchronized void curveTo(Point point1, Point point2, Point point3) {
    	this.implementation.curveTo(point1, point2, point3);
    }

    public synchronized void closePath() {
    	this.implementation.closePath();
    }

    public boolean isClosed() {
    	return this.implementation.isClosed();
    }

    public void append(PathIterator pi, boolean connect) {
    	this.implementation.append(pi, connect);
    }

    public void append(GeneralPathX gp) {
        this.implementation.append(gp);
    }
    
    public void setWindingRule(int rule) {
    	this.implementation.setWindingRule(rule);
    }

    public synchronized void reset() {
    	this.implementation.reset();
    }

    public void transform(AffineTransform at) {
    	this.implementation.transform(at);
    }

    public void reProject(ICoordTrans ct) {
    	this.implementation.reProject(ct);
    }

    public void setNumTypes(int numTypes) {
    	this.implementation.setNumTypes(numTypes);
    }

    public void setPointTypes(byte[] pointTypes) {
    	this.implementation.setPointTypes(pointTypes);
    }

    public void setPointCoords(double[] pointCoords) {
    	this.implementation.setPointCoords(pointCoords);
    }

    public void flip() {
    	this.implementation.flip();
    }

    public synchronized Point2D getCurrentPoint() {
    	return this.implementation.getCurrentPoint();
    }

    public synchronized int getWindingRule() {
    	return this.implementation.getWindingRule();
    }

    public synchronized Shape createTransformedShape(AffineTransform at) {
    	return this.implementation.createTransformedShape(at);
    }

    public java.awt.Rectangle getBounds() {
    	return this.implementation.getBounds();
    }

    public synchronized Rectangle2D getBounds2D() {
    	return this.implementation.getBounds2D();
    }

    public boolean contains(double x, double y) {
    	return this.implementation.contains(x, y);
    }

    public boolean contains(Point2D p) {
    	return this.implementation.contains(p);
    }

    public boolean contains(double x, double y, double w, double h) {
    	return this.implementation.contains(x, y, w, h);
    }

    public boolean contains(Rectangle2D r) {
    	return this.implementation.contains(r);
    }

    public boolean intersects(double x, double y, double w, double h) {
    	return this.implementation.intersects(x, y, w, h);
    }

    public boolean intersects(Rectangle2D r) {
    	return this.implementation.intersects(r);
    }

    public PathIterator getPathIterator(AffineTransform at) {
    	return this.implementation.getPathIterator(at);
    }

    public PathIterator getPathIterator(AffineTransform at, double flatness) {
    	return this.implementation.getPathIterator(at, flatness);
    }

    public Object clone() {
    	return this.implementation.clone();
    }

    public int getNumTypes() {
    	return this.implementation.getNumTypes();
    }

    public int setNumCoords(int numCoords) {
    	return this.implementation.setNumCoords(numCoords);
    }

    public int getNumCoords() {
    	return this.implementation.getNumCoords();
    }

    public byte getTypeAt(int index) {
    	return this.implementation.getTypeAt(index);
    }

    public byte[] getPointTypes() {
    	return this.implementation.getPointTypes();
    }

    public double[] getPointCoords() {
    	return this.implementation.getPointCoords();
    }

    public Point getPointAt(int index) {
    	return this.implementation.getPointAt(index);
    }

    public double[] getCoordinatesAt(int index) {
    	return this.implementation.getCoordinatesAt(index);
    }
    
    public double[] get3DCoordinatesAt(int index) {
    	return this.implementation.get3DCoordinatesAt(index);
    }

    public boolean isCCW() {
    	return this.implementation.isCCW();
    }

    public boolean isSimple() {
    	return this.implementation.isSimple();
    }
}
