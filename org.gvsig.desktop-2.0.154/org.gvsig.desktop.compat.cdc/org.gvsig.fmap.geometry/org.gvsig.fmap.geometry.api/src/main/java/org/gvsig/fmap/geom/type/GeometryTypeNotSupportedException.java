/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.fmap.geom.type;

import java.util.HashMap;
import java.util.Map;

import org.gvsig.fmap.geom.GeometryException;

/**
 * This exception is raised when the someone tries to access a geometry 
 * type that is not supported.
 * 
 * @author jiyarza
 */
public class GeometryTypeNotSupportedException extends GeometryException {

	/**
	 * Generated serial version UID 
	 */
	private static final long serialVersionUID = -196778635358286969L;

	/**
	 * Key to retrieve this exception's message
	 */
	private static final String MESSAGE_KEY = "geometry_type_not_supported_exception";
	/**
	 * This exception's message. Substitution fields follow the format %(fieldName) and 
	 * must be stored in the Map returned by the values() method. 
	 */
	private static final String FORMAT_STRING = 
		"The geometry class %(geomClassName) is not registered.";
	
	/**
	 * Class name of the geometry type. Should never be null in this exception
	 */
	private String geomClassName = null;
	
	
	/**
	 * Constructor with some context data for cases in which the root cause of 
	 * <code>this</code> is internal (usually an unsatisfied logic rule).
	 * @param geomClass geometry class
	 */
	public GeometryTypeNotSupportedException(Class geomClass){
		this(geomClass, null);
	}
	
	/**
	 * Constructor to use when <code>this</code> is caused by another Exception 
	 * but there is not further context data available.
	 * @param e cause exception
	 */
	public GeometryTypeNotSupportedException(Exception e) {
		this(null, e);
	}
	
	public GeometryTypeNotSupportedException(int type, int subType){
	    super(
	        "Geometry type %(type), %(subType) not supported.",
            "_geometry_type_XtypeX_XsubTypeX_not_supported",
	        serialVersionUID
	     );
		setValue("type", new Integer(type));
        setValue("subType", new Integer(subType));
	}
	
	public GeometryTypeNotSupportedException(String geomClassName){
        super(FORMAT_STRING, MESSAGE_KEY, serialVersionUID);
        
        if (geomClassName != null) {
            setValue("geomClassName", geomClassName);
        }
	}
	
	/**
	 * Main constructor that provides both context data and a cause Exception
	 * @param geomClass geometry class 
	 * @param e cause exception
	 */
	public GeometryTypeNotSupportedException(Class geomClass, Exception e) {
		super(FORMAT_STRING, e, MESSAGE_KEY, serialVersionUID);
		
		if (geomClass != null) {
		    setValue("geomClassName", geomClass.getName());
		}
	}
}
