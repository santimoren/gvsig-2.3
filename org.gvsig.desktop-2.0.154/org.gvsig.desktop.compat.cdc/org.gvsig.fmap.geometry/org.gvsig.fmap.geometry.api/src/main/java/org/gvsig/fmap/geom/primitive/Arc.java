/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.primitive;

import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * <p>
 * This interface is equivalent to the GM_Arc specified in <a href=
 * "http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012"
 * >ISO 19107</a>. A Arc is defined by 3 points, and consists of the arc of the
 * circle determined by the 3 points, starting at the first, passing through the
 * second and terminating at the third. If the 3 points are co-linear, then the
 * arc shall be a 3-point line string, and will not be able to return values for
 * center, radius, start angle and end angle.
 * </p>
 *
 * @see <a
 *      href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012">ISO
 *      19107</a>
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface Arc extends Curve {

    /**
     * Sets the three points to define an arc.
     * These are three ordered points that belong to the arc
     * (none of them is the center of the ellipse/circle). Therefore
     * they must not be aligned.
     *
     * @param startPoint
     *            The start point of an arc.
     * @param midPoint
     *            The middle point of an arc.
     * @param endPoint
     *            The end point of an arc.
     * @exception IllegalArgumentException
     *                if the three points are aligned or
     *                there is a repeated point.
     *
     */
    void setPoints(Point startPoint, Point midPoint, Point endPoint);

    /**
     * Sets the values to define an arc.
     *
     * @param center
     *            The center of the arc.
     * @param radius
     *            The radius.
     * @param startAngle
     *            The start angle of the arc (in radians)
     * @param angleExt
     *            The angular extent of the arc (in radians).
     *
     *            The sign convention is:
     *
     *            startAngle = 0 is "3 o'clock";
     *            startAngle = (PI / 3) is "1 o'clock";
     *            angleExt > 0 means "advancing clockwise";
     *            angleExt < 0 means "advancing counterclockwise".
     */
    void setPoints(Point center, double radius, double startAngle, double angleExt);

    /**
     * Sets the values to define an arc.
     *
     * @param center
     *            The center of the arc.
     * @param radius
     *            The radius.
     * @param startAngle
     *            The start angle of the arc (in radians)
     * @param angleExt
     *            The angular extent of the arc (in radians).
     *
     *            The sign convention is:
     *
     *            startAngle = 0 is "3 o'clock";
     *            startAngle = (PI / 3) is "1 o'clock";
     *            angleExt > 0 means "advancing clockwise";
     *            angleExt < 0 means "advancing counterclockwise".
     */
    void setPointsStartExt(Point center, double radius, double startAngle, double angleExt);

    /**
     * Sets the values to define an arc. The arc will go from
     * startAngle to endAngle clockwise. Angles will be
     * normalized to ]-PI, PI] (-PI excluded) before creating
     * the arc.
     *
     * @param center
     *            The center of the arc.
     * @param radius
     *            The radius.
     * @param startAngle
     *            The start angle of the arc (in radians)
     * @param endAngle
     *            The end angle of the arc (in radians).
     *
     */
    void setPointsStartEnd(Point center, double radius, double startAngle, double endAngle);

    /**
     * Return the first point that has been used to create the arc.
     *
     * @return
     *         The first point of the arc.
     */
    Point getInitPoint();

    /**
     * Return the end point that has been used to create the arc.
     *
     * @return
     *         The end point of the arc.
     */
    Point getEndPoint();

    /**
     * Return the center of the arc, that is, the center of the ellipse/circle
     * in which the arc is based.
     *
     * @return The center of the arc.
     */
    Point getCenterPoint();

    /**
     * Return the middle point of the arc.
     *
     * @return The middle point of the arc.
     */
    Point getMiddlePoint();

    /**
     * Returns the counterclockwise angle formed by the horizontal line passing
     * through the center, the center itself and the starting point.
     *
     * @return
     * @throws GeometryOperationNotSupportedException
     * @throws GeometryOperationException
     */
    double getStartAngle() throws GeometryOperationNotSupportedException, GeometryOperationException;

    /**
     * Returns the counterclockwise angle formed by the horizontal line passing
     * through the center, the center itself and the ending point.
     *
     * @return
     * @throws GeometryOperationNotSupportedException
     * @throws GeometryOperationException
     */
    double getEndAngle() throws GeometryOperationNotSupportedException, GeometryOperationException;

}
