/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.exception;

import java.awt.geom.Point2D;

import org.cresques.cts.IProjection;


/**
 * 
 * This exception is thrown when the reprojection library is unable to
 * reproject and we need to notify that there was a problem
 * (so far, the reproject method in Geometry API does not
 * throw a standard exception)
 * 
 * @author jldominguez
 *
 */
public class ReprojectionRuntimeException extends RuntimeException {
    
    public ReprojectionRuntimeException(
        IProjection from_p, IProjection to_p,
        Point2D from_point, Throwable cause) {
        
        super("Error while reprojecting point " + from_point.toString()
            + " (" + from_p.getAbrev() + " to " + to_p.getAbrev() + ")",
            cause);
    }

}
