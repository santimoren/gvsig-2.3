/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.curve.line;


import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * Polilinea 3D.
 *
 * @author Vicente Caballero Navarro
 */
public class Line2DZ extends Line2D implements Line {
    private static final long serialVersionUID = 3431077088722464314L;
   
    /**
     * The constructor with the GeometryType like and argument 
     * is used by the {@link GeometryType}{@link #create()}
     * to create the geometry
     * @param type
     * The geometry type
     */
    public Line2DZ(GeometryType geometryType) {
        super(geometryType);
    }

    /**
     * Constructor used in the {@link Geometry#cloneGeometry()} method
     * @param id
     * @param projection
     * @param gpx
     * @param pZ
     */
    Line2DZ(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx) {
        super(geometryType, id, projection, gpx);      
    }	

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.impl.Curve2D#getShapeType()
     */
    public int getShapeType() {
        return TYPES.CURVE;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.impl.Curve2D#cloneFShape()
     */
    public FShape cloneFShape() {
        return new Line2DZ(getGeometryType(), id, projection, (GeneralPathX) gp.clone());
    }
    
    public double getCoordinateAt(int index, int dimension) {
        if (index > gp.getNumCoords()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        double[] coords = gp.getPointAt(index).getCoordinates();
        if(dimension < coords.length)
        	return gp.getPointAt(index).getCoordinateAt(dimension);
        return 0D;
    }
    
    public int getDimension() {
        return 3;
    }
}
