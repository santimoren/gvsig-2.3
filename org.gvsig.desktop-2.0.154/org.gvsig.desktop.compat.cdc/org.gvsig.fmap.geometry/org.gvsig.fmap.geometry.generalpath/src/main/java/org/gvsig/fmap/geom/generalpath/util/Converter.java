/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.util;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.algorithm.RobustCGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateArrays;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Clase con varios metodos estaticos utilizados para pasar de java2d a jts
 * y viceversa.
 *
 * @author fjp
 * @deprecated to be removed or moved from API to implementation in gvSIG 2.1.0
 */
public class Converter {

    private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
    private static final Logger logger = LoggerFactory.getLogger(Converter.class);
    public final static com.vividsolutions.jts.geom.GeometryFactory geomFactory = new com.vividsolutions.jts.geom.GeometryFactory();
    public static CGAlgorithms cga = new RobustCGAlgorithms();

    private static GeometryManager manager = GeometryLocator.getGeometryManager();

    //returns true if testPoint is a point in the pointList list.
    static boolean pointInList(Coordinate testPoint, Coordinate[] pointList) {
        int t;
        int numpoints;
        Coordinate p;

        numpoints = Array.getLength(pointList);

        for ( t = 0; t < numpoints; t++ ) {
            p = pointList[t];

            if ( (testPoint.x == p.x) && (testPoint.y == p.y)
                    && ((testPoint.z == p.z) || (!(testPoint.z == testPoint.z))) //nan test; x!=x iff x is nan
                    ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Receives a JTS Geometry and returns a DAL Geometry
     *
     * @param jtsGeometry jts Geometry
     * @return IGeometry of FMap
     * @author azabala
     * @throws CreateGeometryException
     */
    public static Geometry jtsToGeometry(com.vividsolutions.jts.geom.Geometry geo) throws CreateGeometryException {
        Geometry shpNew = null;

        try {
            if ( geo instanceof Point ) {
                shpNew = geomManager.createPoint(((Point) geo).getX(), ((Point) geo).getY(), SUBTYPES.GEOM2D);
            }

            if ( geo.isEmpty() ) {
                shpNew = null;
            }

            try {
                if ( geo instanceof com.vividsolutions.jts.geom.MultiPoint ) {
                    shpNew = geomManager.create(TYPES.MULTIPOINT, SUBTYPES.GEOM2D);
                    for ( int i = 0; i < geo.getNumGeometries(); i++ ) {
                        Point point = (Point) geo.getGeometryN(i);
                        ((MultiPoint) shpNew).addPoint((org.gvsig.fmap.geom.primitive.Point) jtsToGeometry(point));
                    }

                }

                if ( geo instanceof Polygon ) {
                    shpNew = geomManager.createSurface(toShape((Polygon) geo), SUBTYPES.GEOM2D);
                }

                if ( geo instanceof MultiPolygon ) {
                    /*
                     * We need a loop because a global path would not
                     * differentiate between move-to inside a polygon (holes)
                     * and move-to from one polygon to another
                     */
                    MultiSurface msu = geomManager.createMultiSurface(SUBTYPES.GEOM2D);
                    MultiPolygon mpo = (MultiPolygon) geo;
                    Surface itemsu = null;
                    int npo = mpo.getNumGeometries();
                    for ( int i = 0; i < npo; i++ ) {
                        itemsu = geomManager.createSurface(
                                toShape((Polygon) mpo.getGeometryN(i)), SUBTYPES.GEOM2D);
                        msu.addPrimitive(itemsu);
                    }
                    shpNew = msu;
                }

                if ( geo instanceof LineString ) {
                    shpNew = geomManager.createCurve(toShape((LineString) geo), SUBTYPES.GEOM2D);
                }

                if ( geo instanceof MultiLineString ) {
                    shpNew = geomManager.create(TYPES.MULTICURVE, SUBTYPES.GEOM2D);
                    for ( int i = 0; i < ((MultiLineString) geo).getNumGeometries(); i++ ) {
                        com.vividsolutions.jts.geom.Geometry g = ((MultiLineString) geo).getGeometryN(i);
                        Curve c = geomManager.createCurve(toShape((LineString) g), SUBTYPES.GEOM2D);
                        ((MultiCurve) shpNew).addCurve(c);
                    }
                }
            } catch (CreateGeometryException e) {
                logger.error("Error creating a geometry", e);
            }

            return shpNew;
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Convierte un MultiPoint2D a un MultiPoint de JTS
     *
     * @param geom
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry geometryToJts(MultiPoint geom) {
        Coordinate[] theGeoms = new Coordinate[geom.getPrimitivesNumber()];
        for ( int i = 0; i < theGeoms.length; i++ ) {
            java.awt.geom.Point2D p = geom.getPrimitiveAt(i)
                    .getHandlers(Geometry.SELECTHANDLER)[0].getPoint();
            Coordinate c = new Coordinate(p.getX(), p.getY());
            theGeoms[i] = c;
        }
        com.vividsolutions.jts.geom.MultiPoint geomCol
                = new com.vividsolutions.jts.geom.GeometryFactory().createMultiPoint(theGeoms);
        return geomCol;
    }

    /**
     * Convierte una MultiCurve2D en una MultiLineString de JTS
     *
     * @param geom
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry geometryToJts(MultiCurve geom) {
        LineString[] lines = new LineString[geom.getPrimitivesNumber()];
        for ( int i = 0; i < lines.length; i++ ) {
            lines[i] = (LineString) geometryToJts((geom.getPrimitiveAt(i)));
        }
        return new com.vividsolutions.jts.geom.GeometryFactory().createMultiLineString(lines);
    }

    public static com.vividsolutions.jts.geom.Geometry multiCurveToJts(MultiCurve geom, int srid) {
        LineString[] lines = new LineString[geom.getPrimitivesNumber()];
        for ( int i = 0; i < lines.length; i++ ) {
            lines[i] = (LineString) curveToJts((geom.getPrimitiveAt(i)), srid);
        }
        return new com.vividsolutions.jts.geom.GeometryFactory().createMultiLineString(lines);
    }

    /**
     * Convierte una MultiSurface2D en un MultiPolygon de JTS
     *
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry geometryToJts(MultiSurface geom) {
        Polygon[] polygons = new Polygon[geom.getPrimitivesNumber()];
        for ( int i = 0; i < polygons.length; i++ ) {
            polygons[i] = (Polygon) geometryToJts((geom.getPrimitiveAt(i)));
        }
        return new com.vividsolutions.jts.geom.GeometryFactory().createMultiPolygon(polygons);
    }

    /**
     * Convierte una BaseMultiPrimitive en una GeometryCollection de JTS
     *
     * @return
     */
    public com.vividsolutions.jts.geom.Geometry geometryToJts(MultiPrimitive geom) {
        com.vividsolutions.jts.geom.Geometry[] geometriesAux = new LineString[geom.getPrimitivesNumber()];
        for ( int i = 0; i < geometriesAux.length; i++ ) {
            geometriesAux[i] = geometryToJts((geom.getPrimitiveAt(i)));
        }
        return new com.vividsolutions.jts.geom.GeometryFactory().createGeometryCollection(geometriesAux);
    }

    public static com.vividsolutions.jts.geom.Geometry geometryToJtsWithSRID(Geometry geom, int srid) {
        return geometryToJts(geom, geom.getType(), srid);
    }

    public static com.vividsolutions.jts.geom.Geometry geometryToJtsWithSRIDForcingType(Geometry geom, int srid, int type) {
        return geometryToJts(geom, type, srid);
    }

    public static com.vividsolutions.jts.geom.Geometry geometryToJts(Geometry geom) {
        return geometryToJts(geom, geom.getType(), -1);
    }

    private static boolean isClosed(Coordinate firstCoordinate, Coordinate lastCoordinate) {
        double diff = Math.abs(lastCoordinate.x - firstCoordinate.x);
        if ( diff > 0.000001 ) {
            return false;
        }
        diff = Math.abs(lastCoordinate.y - firstCoordinate.y);
        if ( diff > 0.000001 ) {
            return false;
        }
        return true;
    }

    private static com.vividsolutions.jts.geom.Geometry curveToJts(Geometry shp, int srid) {
        ArrayList arrayLines;
        LineString lin = null;
        int theType;
        int numParts = 0;
        double[] dataLine = new double[3];
        double[] dataQuad = new double[3];
        double[] dataCubic = new double[3];
        ArrayList arrayCoords = null;
        Coordinate coord;
        int subType = shp.getGeometryType().getSubType();
        boolean is3D = subType == 1 || subType == 3;

        arrayLines = new ArrayList();

		//El pathIterator no tiene en cuenta coordenadas 3D, por lo que de usar este, las Z's se perder�n
        //Las splines vienen con todos los puntos calculados por lo que no es necesario un 
        //iterador que interpole. Por esto obtenemos el mismo resultado recorriendo los puntos.
        //Tiene su riesgo pero hasta que se soporte el 3D en el iterador funciona para todos los casos
        //theIterator = pol.getPathIterator(null, manager.getFlatness());
        GeneralPathX gp = shp.getGeneralPath();

        //while (!theIterator.isDone()) {
        int nPoint = 0;
        for ( int nType = 0; nType < gp.getNumTypes(); nType++ ) {
            theType = gp.getTypeAt(nType);
            switch (theType) {
            case PathIterator.SEG_MOVETO:
            case PathIterator.SEG_LINETO: //Se lee un punto
                dataLine = gp.get3DCoordinatesAt(nPoint);
                nPoint++;
                break;
            case PathIterator.SEG_QUADTO: //Se leen dos puntos
                dataLine = gp.get3DCoordinatesAt(nPoint);
                dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                nPoint += 2;
                break;
            case PathIterator.SEG_CUBICTO: //Se leen tres puntos
                dataLine = gp.get3DCoordinatesAt(nPoint);
                dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                dataCubic = gp.get3DCoordinatesAt(nPoint + 2);
                nPoint += 3;
                break;
            }

            switch (theType) {
            case PathIterator.SEG_MOVETO:
                if ( arrayCoords == null ) {
                    arrayCoords = new ArrayList();
                } else {
                    lin = geomFactory.createLineString(
                            CoordinateArrays.toCoordinateArray(arrayCoords));
                    lin.setSRID(srid);
                    arrayLines.add(lin);
                    arrayCoords = new ArrayList();
                }

                numParts++;
                if ( is3D ) {
                    coord = new Coordinate(dataLine[0], dataLine[1], dataLine[2]);
                } else {
                    coord = new Coordinate(dataLine[0], dataLine[1]);
                }

                arrayCoords.add(coord);
                break;

            case PathIterator.SEG_LINETO:
                loadArrayCoordinates(arrayCoords, is3D, dataLine);
                break;

            case PathIterator.SEG_QUADTO:
                loadArrayCoordinates(arrayCoords, is3D, dataLine);
                loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                break;

            case PathIterator.SEG_CUBICTO:
                loadArrayCoordinates(arrayCoords, is3D, dataLine);
                loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                loadArrayCoordinates(arrayCoords, is3D, dataCubic);
                break;

            case PathIterator.SEG_CLOSE:
                Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                loadArrayCoordinates(arrayCoords, is3D, new double[]{firstCoord.x, firstCoord.y, firstCoord.z});
                break;
            } //end switch

            //theIterator.next();
        } //end while loop

        if ( arrayCoords.size() < 2 ) {

        } else {
            lin = new com.vividsolutions.jts.geom.GeometryFactory().createLineString(
                    CoordinateArrays.toCoordinateArray(arrayCoords));
            lin.setSRID(srid);
        }

        return lin;
    }

    /**
     * Convierte un Geometry de DAL a una Geometry del JTS. Para ello,
     * utilizamos un
     * flattened PathIterator. El flattened indica que las curvas las pasa a
     * segmentos de linea recta AUTOMATICAMENTE!!!.
     *
     * @param shp FShape que se quiere convertir.
     *
     * @return Geometry de JTS.
     */
    private static com.vividsolutions.jts.geom.Geometry geometryToJts(
            Geometry shp, int destinationType, int srid) {
        com.vividsolutions.jts.geom.Geometry geoJTS = null;
        Coordinate coord;
        ArrayList arrayCoords = null;
        ArrayList arrayLines;
        LineString lin;
        int theType;
        int numParts = 0;
        int sourceType = shp.getGeometryType().getType();
        int subType = shp.getGeometryType().getSubType();
        boolean is3D = subType == 1 || subType == 3;

        //Use this array to store segment coordinate data
        double[] dataLine = new double[3];
        double[] dataQuad = new double[3];
        double[] dataCubic = new double[3];

        switch (sourceType) {
        case Geometry.TYPES.POINT:
            if ( is3D ) {
                org.gvsig.fmap.geom.generalpath.primitive.point.Point2DZ p = (org.gvsig.fmap.geom.generalpath.primitive.point.Point2DZ) shp;
                coord = new Coordinate(p.getX(), p.getY(), p.getCoordinateAt(Geometry.DIMENSIONS.Z));
            } else {
                org.gvsig.fmap.geom.generalpath.primitive.point.Point2D p = (org.gvsig.fmap.geom.generalpath.primitive.point.Point2D) shp;
                coord = new Coordinate(p.getX(), p.getY());
            }
            geoJTS = geomFactory.createPoint(coord);
            geoJTS.setSRID(srid);
            break;

        case Geometry.TYPES.CURVE:
        case Geometry.TYPES.ARC:
        case Geometry.TYPES.SPLINE:
        case Geometry.TYPES.ELLIPTICARC:

            arrayLines = new ArrayList();

			//El pathIterator no tiene en cuenta coordenadas 3D, por lo que de usar este, las Z's se perder�n
            //Las splines vienen con todos los puntos calculados por lo que no es necesario un 
            //iterador que interpole. Por esto obtenemos el mismo resultado recorriendo los puntos.
            //Tiene su riesgo pero hasta que se soporte el 3D en el iterador funciona para todos los casos
            //theIterator = pol.getPathIterator(null, manager.getFlatness());
            GeneralPathX gp = shp.getGeneralPath();

            //while (!theIterator.isDone()) {
            int nPoint = 0;
            for ( int nType = 0; nType < gp.getNumTypes(); nType++ ) {
                theType = gp.getTypeAt(nType);
                switch (theType) {
                case PathIterator.SEG_MOVETO:
                case PathIterator.SEG_LINETO: //Se lee un punto
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    nPoint++;
                    break;
                case PathIterator.SEG_QUADTO: //Se leen dos puntos
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                    nPoint += 2;
                    break;
                case PathIterator.SEG_CUBICTO: //Se leen tres puntos
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                    dataCubic = gp.get3DCoordinatesAt(nPoint + 2);
                    nPoint += 3;
                    break;
                }

		// Populate a segment of the new GeneralPathX object.
                // Process the current segment to populate a new
                // segment of the new GeneralPathX object.
                switch (theType) {
                case PathIterator.SEG_MOVETO:
                    if ( arrayCoords == null ) {
                        arrayCoords = new ArrayList();
                    } else {
                        lin = geomFactory.createLineString(CoordinateArrays.toCoordinateArray(arrayCoords));
                        lin.setSRID(srid);
                        arrayLines.add(lin);
                        arrayCoords = new ArrayList();
                    }

                    numParts++;
                    if ( is3D ) {
                        coord = new Coordinate(dataLine[0], dataLine[1], dataLine[2]);
                    } else {
                        coord = new Coordinate(dataLine[0], dataLine[1]);
                    }

                    arrayCoords.add(coord);
                    break;

                case PathIterator.SEG_LINETO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    break;

                case PathIterator.SEG_QUADTO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                    break;

                case PathIterator.SEG_CUBICTO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                    loadArrayCoordinates(arrayCoords, is3D, dataCubic);
                    break;

                case PathIterator.SEG_CLOSE:
                    Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                    loadArrayCoordinates(arrayCoords, is3D, new double[]{firstCoord.x, firstCoord.y, firstCoord.z});
                    break;
                } //end switch

                //theIterator.next();
            } //end while loop

            if ( arrayCoords.size() < 2 ) {
                break;
            }
            lin = new com.vividsolutions.jts.geom.GeometryFactory().createLineString(
                    CoordinateArrays.toCoordinateArray(arrayCoords));

            lin.setSRID(srid);
            geoJTS = lin;

            /*
             * We were creating always a multilinestring here, but
             * we have decided that we should return the correct type
             * (linestring) and other parts of the application will have
             * to solve the problem, it there is a problem with this.
             * 
             arrayLines.add(lin);
             geoJTS = geomFactory.createMultiLineString(
             com.vividsolutions.jts.geom.GeometryFactory.toLineStringArray(arrayLines));
             geoJTS.setSRID(srid);
             */
            break;

        case Geometry.TYPES.SURFACE:
        case Geometry.TYPES.CIRCLE:
        case Geometry.TYPES.ELLIPSE:
            arrayLines = new ArrayList();

            ArrayList shells = new ArrayList();
            ArrayList holes = new ArrayList();
            Coordinate[] points = null;

            //El pathIterator no tiene en cuenta coordenadas 3D, pero para la creaci�n de elipses y circulos
            //es necesario el iterador que interpole puntos. El resultado es que en la creaci�n de geometr�as de este
            //tipo no se puede asignar la Z porque se perder�a, pero gvSIG tampoco dispone de esta funci�n, as� que
            //no se nota. Una vez creadas las geometr�a de tipo Elipse y circulo cuando las editamos ya tendr�n todos
            //los puntos calculados y se toman como l�neas por lo que ya se podr�a asignar la Z.
            PathIterator theIterator = shp.getPathIterator(null, manager.getFlatness());
            while ( !theIterator.isDone() ) {
                theType = theIterator.currentSegment(dataLine);

                switch (theType) {
                case PathIterator.SEG_MOVETO:
                    if ( arrayCoords == null ) {
                        arrayCoords = new ArrayList();
                    } else {
                        points = CoordinateArrays.toCoordinateArray(arrayCoords);

                        try {
                            LinearRing ring = geomFactory.createLinearRing(points);

                            if ( CGAlgorithms.isCCW(points) ) {
                                holes.add(ring);
                            } else {
                                shells.add(ring);
                            }
                        } catch (Exception e) {
                            logger.warn("Caught Topology exception in GMLLinearRingHandler",e);
                            boolean same = true;
                            for ( int i = 0; i < points.length - 1 && same; i++ ) {
                                if ( points[i].x != points[i + 1].x
                                        || points[i].y != points[i + 1].y /*||
                                         points[i].z != points[i+1].z*/ ) {
                                    same = false;
                                }
                            }
                            if ( same ) {
                                return geomFactory.createPoint(points[0]);
                            }
                            if ( points.length > 1 && points.length <= 3 ) {
                                // return geomFactory.createLineString(points);
                                return geomFactory.createMultiLineString(new LineString[]{geomFactory.createLineString(points)});
                            }
                            return null;
                        }
                        arrayCoords = new ArrayList();
                    }

                    numParts++;
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    break;

                case PathIterator.SEG_LINETO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    break;

                case PathIterator.SEG_QUADTO:
                    break;

                case PathIterator.SEG_CUBICTO:
                    break;

                case PathIterator.SEG_CLOSE:
                    Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                    loadArrayCoordinates(arrayCoords, is3D, new double[]{firstCoord.x, firstCoord.y, firstCoord.z});
                    break;
                } //end switch

                theIterator.next();
            } //end while loop

            Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
            Coordinate lastCoord = (Coordinate) arrayCoords.get(arrayCoords.size() - 1);
            if ( !isClosed(firstCoord, lastCoord) ) {
                arrayCoords.add(firstCoord);
            }
            points = CoordinateArrays.toCoordinateArray(arrayCoords);

            try {
                LinearRing ring = geomFactory.createLinearRing(points);

                if ( CGAlgorithms.isCCW(points) ) {
                    holes.add(ring);
                } else {
                    shells.add(ring);
                }
                ring.setSRID(srid);
            } catch (Exception e) {
                logger.warn("Caught Topology exception in GMLLinearRingHandler",e);
                boolean same = true;
                for ( int i = 0; i < points.length - 1 && same; i++ ) {
                    if ( points[i].x != points[i + 1].x
                            || points[i].y != points[i + 1].y /*||
                             points[i].z != points[i+1].z*/ ) {
                        same = false;
                    }
                }
                if ( same ) {
                    geoJTS = geomFactory.createPoint(points[0]);
                    geoJTS.setSRID(srid);
                    return geoJTS;
                }
                /*
                 * caso cuando es una linea de 3 puntos, no creo un LinearRing, sino
                 * una linea
                 */
                if ( points.length > 1 && points.length <= 3 ) {
                    // return geomFactory.createLineString(points);
                    geoJTS = geomFactory.createMultiLineString(new LineString[]{geomFactory.createLineString(points)});
                    geoJTS.setSRID(srid);
                    return geoJTS;
                }
                return null;
            }

            /* linRing = new GeometryFactory().createLinearRing(
             CoordinateArrays.toCoordinateArray(arrayCoords)); */
			// System.out.println("NumParts = " + numParts);
            //now we have a list of all shells and all holes
            ArrayList holesForShells = new ArrayList(shells.size());

            for ( int i = 0; i < shells.size(); i++ ) {
                holesForShells.add(new ArrayList());
            }

            //find homes
            for ( int i = 0; i < holes.size(); i++ ) {
                LinearRing testRing = (LinearRing) holes.get(i);
                LinearRing minShell = null;
                Envelope minEnv = null;
                Envelope testEnv = testRing.getEnvelopeInternal();
                Coordinate testPt = testRing.getCoordinateN(0);
                LinearRing tryRing = null;

                for ( int j = 0; j < shells.size(); j++ ) {
                    tryRing = (LinearRing) shells.get(j);

                    Envelope tryEnv = tryRing.getEnvelopeInternal();

                    if ( minShell != null ) {
                        minEnv = minShell.getEnvelopeInternal();
                    }

                    boolean isContained = false;
                    Coordinate[] coordList = tryRing.getCoordinates();

                    if ( tryEnv.contains(testEnv)
                            && (CGAlgorithms.isPointInRing(testPt, coordList)
                            || (pointInList(testPt, coordList))) ) {
                        isContained = true;
                    }

                    // check if this new containing ring is smaller than the current minimum ring
                    if ( isContained ) {
                        if ( (minShell == null) || minEnv.contains(tryEnv) ) {
                            minShell = tryRing;
                        }
                    }
                }

                if ( minShell == null ) {
                    // System.out.println(
                    // polygon found with a hole thats not inside a shell);
                    // azabala: we do the assumption that this hole is really a shell (polygon)
                    // whose point werent digitized in the right order
                    Coordinate[] cs = testRing.getCoordinates();
                    Coordinate[] reversed = new Coordinate[cs.length];
                    int pointIndex = 0;
                    for ( int z = cs.length - 1; z >= 0; z-- ) {
                        reversed[pointIndex] = cs[z];
                        pointIndex++;
                    }
                    LinearRing newRing = geomFactory.createLinearRing(reversed);
                    shells.add(newRing);
                    holesForShells.add(new ArrayList());
                } else {
                    ((ArrayList) holesForShells.get(shells.indexOf(minShell))).add(testRing);
                }
            }

            Polygon[] polygons = new Polygon[shells.size()];

            for ( int i = 0; i < shells.size(); i++ ) {
                polygons[i] = geomFactory.createPolygon(
                        (LinearRing) shells.get(i),
                        (LinearRing[]) ((ArrayList) holesForShells.get(i)).toArray(new LinearRing[0]));
                polygons[i].setSRID(srid);
            }
            holesForShells = null;
            shells = null;
            holes = null;

            if ( polygons.length == 1 ) {
                geoJTS = polygons[0];
            } else {
                // its a multi part
                geoJTS = geomFactory.createMultiPolygon(polygons);
            }
            geoJTS.setSRID(srid);
            break;

        case Geometry.TYPES.MULTICURVE:
            geoJTS = multiCurveToJts((MultiCurve) shp, srid);
            geoJTS.setSRID(srid);
            break;

        case Geometry.TYPES.MULTIPOINT:
            geoJTS = geometryToJts((MultiPoint) shp);
            geoJTS.setSRID(srid);
            break;

        case Geometry.TYPES.MULTISURFACE:
            geoJTS = multiSurfaceToJts((MultiSurface) shp, srid);
            geoJTS.setSRID(srid);
            break;
        }

        if ( destinationType != sourceType ) {
            geoJTS = convertTypes(geoJTS, sourceType, destinationType);
        }

        geoJTS.setSRID(srid);
        return geoJTS;
    }

    /**
     * This function is called when the we need force types, that is the
     * destination
     * type does not match with the input geometry type
     *
     * @param g
     * @param sourceType
     * @param destinationType
     * @return
     */
    private static com.vividsolutions.jts.geom.Geometry convertTypes(
            com.vividsolutions.jts.geom.Geometry g,
            int sourceType,
            int destinationType) {
        if ( (sourceType == Geometry.TYPES.CURVE
                || sourceType == Geometry.TYPES.SPLINE
                || sourceType == Geometry.TYPES.ARC
                || sourceType == Geometry.TYPES.ELLIPTICARC)
                && destinationType == Geometry.TYPES.MULTISURFACE ) {
            if ( g instanceof MultiLineString ) {
                Polygon[] poly = new Polygon[((MultiLineString) g).getNumGeometries()];
                for ( int i = 0; i < ((MultiLineString) g).getNumGeometries(); i++ ) {
                    com.vividsolutions.jts.geom.Geometry lineString = ((MultiLineString) g).getGeometryN(i);
                    poly[i] = convertLineStringToPolygon((LineString) lineString);
                }
                return geomFactory.createMultiPolygon(poly);
            } else {
                return convertLineStringToPolygon((LineString) g);
            }
        }

        if ( (sourceType == Geometry.TYPES.CIRCLE
                || sourceType == Geometry.TYPES.ELLIPSE)
                && destinationType == Geometry.TYPES.MULTICURVE ) {
            if ( g instanceof Polygon ) {
                Polygon poly = (Polygon) g;
                LineString lineString = geomFactory.createLinearRing(poly.getCoordinates());
                return geomFactory.createMultiLineString(new LineString[]{lineString});
            }
        }
        return g;
    }

    private static com.vividsolutions.jts.geom.Polygon convertLineStringToPolygon(LineString line) {
        Coordinate[] coordinates = line.getCoordinates();
        LinearRing shell = geomFactory.createLinearRing(coordinates);
        Polygon pol = geomFactory.createPolygon(shell, null);
        return pol;
    }

    /**
     * DOCUMENT ME!
     *
     * @param p DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    private static GeneralPathX toShape(Polygon p) {
        GeneralPathX resul = new GeneralPathX();
        Coordinate coord;

        for ( int i = 0; i < p.getExteriorRing().getNumPoints(); i++ ) {
            coord = p.getExteriorRing().getCoordinateN(i);

            if ( i == 0 ) {
                resul.moveTo(coord.x, coord.y);
            } else {
                resul.lineTo(coord.x, coord.y);
            }
        }

        for ( int j = 0; j < p.getNumInteriorRing(); j++ ) {
            LineString hole = p.getInteriorRingN(j);

            for ( int k = 0; k < hole.getNumPoints(); k++ ) {
                coord = hole.getCoordinateN(k);

                if ( k == 0 ) {
                    resul.moveTo(coord.x, coord.y);
                } else {
                    resul.lineTo(coord.x, coord.y);
                }
            }
        }

        return resul;
    }

    private static GeneralPathX toShape(MultiLineString mls)
            throws NoninvertibleTransformException, CreateGeometryException {
        GeneralPathX path = new GeneralPathX();

        for ( int i = 0; i < mls.getNumGeometries(); i++ ) {
            LineString lineString = (LineString) mls.getGeometryN(i);
            path.append(toShape(lineString).getPathIterator(null), false);
        }

		//BasicFeatureRenderer expects LineStrings and MultiLineStrings to be
        //converted to GeneralPathXs. [Jon Aquino]
        return path;
    }

    /**
     * DOCUMENT ME!
     *
     * @param lineString DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws NoninvertibleTransformException DOCUMENT ME!
     * @throws CreateGeometryException
     */
    private static GeneralPathX toShape(LineString lineString)
            throws NoninvertibleTransformException, CreateGeometryException {
        GeneralPathX shape = new GeneralPathX();
        org.gvsig.fmap.geom.primitive.Point viewPoint = coordinate2FPoint2D(lineString.getCoordinateN(0));
        shape.moveTo(viewPoint.getX(), viewPoint.getY());

        for ( int i = 1; i < lineString.getNumPoints(); i++ ) {
            viewPoint = coordinate2FPoint2D(lineString.getCoordinateN(i));
            shape.lineTo(viewPoint.getX(), viewPoint.getY());
        }

		//BasicFeatureRenderer expects LineStrings and MultiLineStrings to be
        //converted to GeneralPathXs. [Jon Aquino]
        return shape;
    }

    /**
     *
     */
    private static GeneralPathX toShape(MultiPolygon mp)
            throws NoninvertibleTransformException {
        GeneralPathX path = new GeneralPathX();

        for ( int i = 0; i < mp.getNumGeometries(); i++ ) {
            Polygon polygon = (Polygon) mp.getGeometryN(i);
            path.append(toShape(polygon).getPathIterator(null), false);
        }

		//BasicFeatureRenderer expects LineStrings and MultiLineStrings to be
        //converted to GeneralPathXs. [Jon Aquino]
        return path;
    }

    /**
     * DOCUMENT ME!
     *
     * @param coord DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * @throws CreateGeometryException
     */
    public static org.gvsig.fmap.geom.primitive.Point coordinate2FPoint2D(Coordinate coord) throws CreateGeometryException {
        return geomManager.createPoint(coord.x, coord.y, SUBTYPES.GEOM2D); //,coord.z);
    }

    /**
     * Convierte una Geometry de JTS a GeneralPathX.
     *
     * @param geometry Geometry a convertir.
     *
     * @return GeneralPathX.
     *
     * @throws NoninvertibleTransformException
     * @throws CreateGeometryException
     * @throws IllegalArgumentException
     */
    public static GeneralPathX toShape(com.vividsolutions.jts.geom.Geometry geometry)
            throws NoninvertibleTransformException, CreateGeometryException {
        if ( geometry.isEmpty() ) {
            return new GeneralPathX();
        }

        if ( geometry instanceof Polygon ) {
            return toShape((Polygon) geometry);
        }

        if ( geometry instanceof MultiPolygon ) {
            return toShape((MultiPolygon) geometry);
        }

        if ( geometry instanceof LineString ) {
            return toShape((LineString) geometry);
        }

        if ( geometry instanceof MultiLineString ) {
            return toShape((MultiLineString) geometry);
        }

        if ( geometry instanceof GeometryCollection ) {
            return toShape(geometry);
        }

        throw new IllegalArgumentException("Unrecognized Geometry class: "
                + geometry.getClass());
    }

    public static GeneralPathX transformToInts(GeneralPathX gp, AffineTransform at) {
        GeneralPathX newGp = new GeneralPathX();
        PathIterator theIterator;
        int theType;
        int numParts = 0;
        double[] theData = new double[6];
        java.awt.geom.Point2D ptDst = new java.awt.geom.Point2D.Double();
        java.awt.geom.Point2D ptSrc = new java.awt.geom.Point2D.Double();
        boolean bFirst = true;
        int xInt, yInt, antX = -1, antY = -1;

        theIterator = gp.getPathIterator(null); //, flatness);

        while ( !theIterator.isDone() ) {
            theType = theIterator.currentSegment(theData);
            switch (theType) {
            case PathIterator.SEG_MOVETO:
                numParts++;
                ptSrc.setLocation(theData[0], theData[1]);
                at.transform(ptSrc, ptDst);
                antX = (int) ptDst.getX();
                antY = (int) ptDst.getY();
                newGp.moveTo(antX, antY);
                bFirst = true;
                break;

            case PathIterator.SEG_LINETO:
                ptSrc.setLocation(theData[0], theData[1]);
                at.transform(ptSrc, ptDst);
                xInt = (int) ptDst.getX();
                yInt = (int) ptDst.getY();
                if ( (bFirst) || ((xInt != antX) || (yInt != antY)) ) {
                    newGp.lineTo(xInt, yInt);
                    antX = xInt;
                    antY = yInt;
                    bFirst = false;
                }
                break;

            case PathIterator.SEG_QUADTO:
                System.out.println("Not supported here");

                break;

            case PathIterator.SEG_CUBICTO:
                System.out.println("Not supported here");

                break;

            case PathIterator.SEG_CLOSE:
                newGp.closePath();

                break;
            } //end switch

            theIterator.next();
        } //end while loop

        return newGp;
    }

    public static Geometry transformToInts(Geometry gp, AffineTransform at) throws CreateGeometryException {
        GeneralPathX newGp = new GeneralPathX();
        double[] theData = new double[6];
        double[] aux = new double[6];

        // newGp.reset();
        PathIterator theIterator;
        int theType;
        int numParts = 0;

        java.awt.geom.Point2D ptDst = new java.awt.geom.Point2D.Double();
        java.awt.geom.Point2D ptSrc = new java.awt.geom.Point2D.Double();
        boolean bFirst = true;
        int xInt, yInt, antX = -1, antY = -1;

        theIterator = gp.getPathIterator(null); //, flatness);
        int numSegmentsAdded = 0;
        while ( !theIterator.isDone() ) {
            theType = theIterator.currentSegment(theData);

            switch (theType) {
            case PathIterator.SEG_MOVETO:
                numParts++;
                ptSrc.setLocation(theData[0], theData[1]);
                at.transform(ptSrc, ptDst);
                antX = (int) ptDst.getX();
                antY = (int) ptDst.getY();
                newGp.moveTo(antX, antY);
                numSegmentsAdded++;
                bFirst = true;
                break;

            case PathIterator.SEG_LINETO:
                ptSrc.setLocation(theData[0], theData[1]);
                at.transform(ptSrc, ptDst);
                xInt = (int) ptDst.getX();
                yInt = (int) ptDst.getY();
                if ( (bFirst) || ((xInt != antX) || (yInt != antY)) ) {
                    newGp.lineTo(xInt, yInt);
                    antX = xInt;
                    antY = yInt;
                    bFirst = false;
                    numSegmentsAdded++;
                }
                break;

            case PathIterator.SEG_QUADTO:
                at.transform(theData, 0, aux, 0, 2);
                newGp.quadTo(aux[0], aux[1], aux[2], aux[3]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CUBICTO:
                at.transform(theData, 0, aux, 0, 3);
                newGp.curveTo(aux[0], aux[1], aux[2], aux[3], aux[4], aux[5]);
                numSegmentsAdded++;
                break;

            case PathIterator.SEG_CLOSE:
                if ( numSegmentsAdded < 3 ) {
                    newGp.lineTo(antX, antY);
                }
                newGp.closePath();

                break;
            } //end switch

            theIterator.next();
        } //end while loop

        Geometry shp = null;
        switch (gp.getType()) {
        case Geometry.TYPES.POINT:
            shp = geomManager.createPoint(ptDst.getX(), ptDst.getY(), SUBTYPES.GEOM2D);
            break;

        case Geometry.TYPES.CURVE:
        case Geometry.TYPES.ARC:
        case Geometry.TYPES.ELLIPTICARC:
            try {
                shp = geomManager.createCurve(newGp, SUBTYPES.GEOM2D);
            } catch (CreateGeometryException e1) {
                logger.error("Error creating a curve", e1);
            }
            break;

        case Geometry.TYPES.SURFACE:
        case Geometry.TYPES.CIRCLE:
        case Geometry.TYPES.ELLIPSE:

            try {
                shp = geomManager.createSurface(newGp, SUBTYPES.GEOM2D);
            } catch (CreateGeometryException e) {
                logger.error("Error creating a surface", e);
            }
            break;
        }
        return shp;
    }

    public static Rectangle2D convertEnvelopeToRectangle2D(Envelope jtsR) {
        Rectangle2D.Double r = new Rectangle2D.Double(jtsR.getMinX(),
                jtsR.getMinY(), jtsR.getWidth(), jtsR.getHeight());
        return r;
    }

    public static Envelope convertEnvelopeToJTS(org.gvsig.fmap.geom.primitive.Envelope r) {
        Envelope e = new Envelope(r.getMinimum(0), r.getMaximum(0), r.getMinimum(1),
                r.getMaximum(1));
        return e;
    }

    /**
     * Return a correct polygon (no hole)
     *
     * @param coordinates
     * @return
     */
    public static Geometry getExteriorPolygon(Coordinate[] coordinates) {
        // isCCW = true => it's a hole
        Coordinate[] vs = new Coordinate[coordinates.length];
        if ( CGAlgorithms.isCCW(coordinates) ) {
            for ( int i = vs.length - 1; i >= 0; i-- ) {
                vs[i] = coordinates[i];
            }
        } else {
            vs = coordinates;
        }
        LinearRing ring = geomFactory.createLinearRing(vs);

        try {
            Surface surface = (Surface) manager.create(TYPES.SURFACE, SUBTYPES.GEOM2D);
            surface.setGeneralPath(toShape(ring));
            return surface;
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        } catch (CreateGeometryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isCCW(Point[] points) {
        int length = points.length;
        Coordinate[] vs;

        if ( points[0].getX() != points[length - 1].getX() || points[0].getY() != points[length - 1].getY() ) {
            vs = new Coordinate[length + 1];
            vs[points.length] = new Coordinate(points[0].getX(), points[0].getY());
        } else {
            vs = new Coordinate[length];
        }
        for ( int i = 0; i < length; i++ ) {
            vs[i] = new Coordinate(points[i].getX(), points[i].getY());
        }

        return CGAlgorithms.isCCW(vs);
    }

    public static boolean isCCW(Surface pol) {
        com.vividsolutions.jts.geom.Geometry jtsGeom = Converter.geometryToJts(pol);
        if ( jtsGeom.getNumGeometries() == 1 ) {
            Coordinate[] coords = jtsGeom.getCoordinates();
            return CGAlgorithms.isCCW(coords);
        }
        return false;
    }

    /**
     * Return a hole (CCW ordered points)
     *
     * @param coordinates
     * @return
     */
    public static Geometry getHole(Coordinate[] coordinates) {
        Coordinate[] vs = new Coordinate[coordinates.length];
        if ( CGAlgorithms.isCCW(coordinates) ) {
            vs = coordinates;

        } else {
            for ( int i = vs.length - 1; i >= 0; i-- ) {
                vs[i] = coordinates[i];
            }
        }
        LinearRing ring = geomFactory.createLinearRing(vs);

        try {
            Surface surface = (Surface) manager.create(TYPES.SURFACE, SUBTYPES.GEOM2D);
            surface.setGeneralPath(toShape(ring));
            return surface;
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        } catch (CreateGeometryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Shape getExteriorPolygon(GeneralPathX gp) {
        Area area = new Area(gp);
        area.isSingular();
        return area;
    }

    /**
     * Use it ONLY for NOT multipart polygons.
     *
     * @param pol
     * @return
     */
    public static Geometry getNotHolePolygon(Surface pol) {
        // isCCW == true => hole
        Coordinate[] coords;
        ArrayList arrayCoords = null;
        int theType;
        int numParts = 0;

        //Use this array to store segment coordinate data
        double[] theData = new double[4];

        ArrayList shells = new ArrayList();
        ArrayList holes = new ArrayList();
        Coordinate[] points = null;

        int subType = pol.getGeometryType().getSubType();
        boolean is3D = subType == 1 || subType == 3;

		//El pathIterator no tiene en cuenta coordenadas 3D
        //theIterator = pol.getPathIterator(null, manager.getFlatness());
        GeneralPathX gp = pol.getGeneralPath();

        //while (!theIterator.isDone()) {
        for ( int nPoint = 0; nPoint < gp.getNumCoords(); nPoint++ ) {
            theData = gp.getCoordinatesAt(nPoint);
            theType = gp.getTypeAt(nPoint);

			//Populate a segment of the new
            // GeneralPathX object.
            //Process the current segment to populate a new
            // segment of the new GeneralPathX object.
            switch (theType) {
            case PathIterator.SEG_MOVETO:

                // System.out.println("SEG_MOVETO");
                if ( arrayCoords == null ) {
                    arrayCoords = new ArrayList();
                } else {
                    points = CoordinateArrays.toCoordinateArray(arrayCoords);

                    try {
                        LinearRing ring = geomFactory.createLinearRing(points);

                        if ( CGAlgorithms.isCCW(points) ) {
                            holes.add(ring);
                        } else {
                            shells.add(ring);
                        }
                    } catch (Exception e) {
                        System.err.println("Caught Topology exception in GMLLinearRingHandler");

                        return null;
                    }
                    arrayCoords = new ArrayList();
                }

                numParts++;
                if ( is3D ) {
                    arrayCoords.add(new Coordinate(theData[0], theData[1], theData[2]));
                } else {
                    arrayCoords.add(new Coordinate(theData[0], theData[1]));
                }

                break;

            case PathIterator.SEG_LINETO:
                if ( is3D ) {
                    arrayCoords.add(new Coordinate(theData[0], theData[1], theData[2]));
                } else {
                    arrayCoords.add(new Coordinate(theData[0], theData[1]));
                }
                break;
            case PathIterator.SEG_QUADTO:
                System.out.println("SEG_QUADTO Not supported here");
                break;
            case PathIterator.SEG_CUBICTO:
                System.out.println("SEG_CUBICTO Not supported here");
                break;
            case PathIterator.SEG_CLOSE:
                Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                if ( is3D ) {
                    arrayCoords.add(new Coordinate(firstCoord.x, firstCoord.y, firstCoord.z));
                } else {
                    arrayCoords.add(new Coordinate(firstCoord.x, firstCoord.y));
                }
                break;
            } //end switch

            //theIterator.next();
        } //end while loop

        arrayCoords.add(arrayCoords.get(0));
        coords = CoordinateArrays.toCoordinateArray(arrayCoords);

        if ( numParts == 1 ) {
            return getExteriorPolygon(coords);
        }
        return pol;
    }

    /**
     * Metodo creado para construir un MultiSurface formado por varias surface
     *
     * @author Leticia Riestra
     * @param geom
     * @return
     */
    public static com.vividsolutions.jts.geom.Geometry multiSurfaceToJts(MultiSurface geom, int srid) {
        Polygon[] polygons = new Polygon[geom.getPrimitivesNumber()];
        for ( int i = 0; i < polygons.length; i++ ) {
            Primitive primit = geom.getPrimitiveAt(i);
            MultiPolygon polygon = null;
            if ( primit.getType() == Geometry.TYPES.ELLIPSE || primit.getType() == Geometry.TYPES.CIRCLE ) {
                polygon = (MultiPolygon) ellipseToJts((geom.getPrimitiveAt(i)), srid);
            } else {
                polygon = (MultiPolygon) surfaceToJts((geom.getPrimitiveAt(i)), srid);
            }

            polygons[i] = (Polygon) polygon.getGeometryN(0);//(Polygon) surfaceToJts((geom.getPrimitiveAt(i)), srid);
        }
        return new com.vividsolutions.jts.geom.GeometryFactory().createMultiPolygon(polygons);
    }

    private static com.vividsolutions.jts.geom.Geometry surfaceToJts(Geometry shp, int srid) {
        try {
            com.vividsolutions.jts.geom.Geometry geoJTS = null;
            int theType;
            int numParts = 0;
            ArrayList arrayCoords = null;
            int subType = shp.getGeometryType().getSubType();
            boolean is3D = subType == 1 || subType == 3;
            double[] dataLine = new double[3];
            double[] dataQuad = new double[3];
            double[] dataCubic = new double[3];

            ArrayList shells = new ArrayList();
            ArrayList holes = new ArrayList();
            Coordinate[] points = null;

		//El pathIterator no tiene en cuenta coordenadas 3D. En este caso no 
            //necesitamos un iterador que interpole porque las SURFACE vienen con todos
            //los puntos calculados, as� que hacemos un recorrido en vez de usar el 
            //iterador para poder obtener la Z y no perderla.
            //theIterator = shp.getPathIterator(null, manager.getFlatness());
            GeneralPathX gp = shp.getGeneralPath();

            //while (!theIterator.isDone()) {
            int nPoint = 0;
            for ( int nType = 0; nType < gp.getNumTypes(); nType++ ) {
                theType = gp.getTypeAt(nType);
                switch (theType) {
                case PathIterator.SEG_MOVETO:
                case PathIterator.SEG_LINETO: //Se lee un punto
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    nPoint++;
                    break;
                case PathIterator.SEG_QUADTO: //Se leen dos puntos
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                    nPoint += 2;
                    break;
                case PathIterator.SEG_CUBICTO: //Se leen tres puntos
                    dataLine = gp.get3DCoordinatesAt(nPoint);
                    dataQuad = gp.get3DCoordinatesAt(nPoint + 1);
                    dataCubic = gp.get3DCoordinatesAt(nPoint + 2);
                    nPoint += 3;
                    break;
                }
                //theType = theIterator.currentSegment(theData);

                switch (theType) {
                case PathIterator.SEG_MOVETO:

                    // System.out.println("SEG_MOVETO");
                    if ( arrayCoords == null ) {
                        arrayCoords = new ArrayList();
                    } else {
                        points = CoordinateArrays.toCoordinateArray(arrayCoords);

                        try {
                            LinearRing ring = geomFactory.createLinearRing(points);

                            if ( CGAlgorithms.isCCW(points) ) {
                                holes.add(ring);
                            } else {
                                shells.add(ring);
                            }
                        } catch (Exception e) {
                            boolean same = true;
                            for ( int i = 0; i < points.length - 1 && same; i++ ) {
                                if ( points[i].x != points[i + 1].x
                                        || points[i].y != points[i + 1].y /*||
                                         points[i].z != points[i+1].z*/ ) {
                                    same = false;
                                }
                            }
                            if ( same ) {
                                return geomFactory.createPoint(points[0]);
                            }

                            if ( points.length > 1 && points.length <= 3 ) {
                                // return geomFactory.createLineString(points);
                                return geomFactory.createMultiLineString(new LineString[]{geomFactory.createLineString(points)});
                            }

                            System.err.println(
                                    "Caught Topology exception in GMLLinearRingHandler");

                            return null;
                        }

                        arrayCoords = new ArrayList();
                    }

                    numParts++;
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    break;

                case PathIterator.SEG_LINETO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    break;

                case PathIterator.SEG_QUADTO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                    break;

                case PathIterator.SEG_CUBICTO:
                    loadArrayCoordinates(arrayCoords, is3D, dataLine);
                    loadArrayCoordinates(arrayCoords, is3D, dataQuad);
                    loadArrayCoordinates(arrayCoords, is3D, dataCubic);
                    break;

                case PathIterator.SEG_CLOSE:
                    Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                    loadArrayCoordinates(arrayCoords, is3D, new double[]{firstCoord.x, firstCoord.y, firstCoord.z});
                    break;
                } //end switch

                //theIterator.next();
            } //end while loop

            Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
            Coordinate lastCoord = (Coordinate) arrayCoords.get(arrayCoords.size() - 1);

            if ( !isClosed(firstCoord, lastCoord) ) {
                arrayCoords.add(firstCoord);
            }
            points = CoordinateArrays.toCoordinateArray(arrayCoords);

            try {
                LinearRing ring = geomFactory.createLinearRing(points);

                if ( CGAlgorithms.isCCW(points) ) {
                    holes.add(ring);
                } else {
                    shells.add(ring);
                }
                ring.setSRID(srid);
            } catch (Exception e) {
                boolean same = true;
                for ( int i = 0; i < points.length - 1 && same; i++ ) {
                    if ( points[i].x != points[i + 1].x
                            || points[i].y != points[i + 1].y /*||
                             points[i].z != points[i+1].z*/ ) {
                        same = false;
                    }
                }
                if ( same ) {
                    geoJTS = geomFactory.createPoint(points[0]);
                    geoJTS.setSRID(srid);
                    return geoJTS;
                }
                if ( points.length > 1 && points.length <= 3 ) {
                    // return geomFactory.createLineString(points);
                    geoJTS = geomFactory
                            .createMultiLineString(new LineString[]{geomFactory
                                .createLineString(points)});
                    geoJTS.setSRID(srid);
                    return geoJTS;
                }
                System.err.println(
                        "Caught Topology exception in GMLLinearRingHandler");

                return null;
            }

            /* linRing = new GeometryFactory().createLinearRing(
             CoordinateArrays.toCoordinateArray(arrayCoords)); */
		// System.out.println("NumParts = " + numParts);
            //now we have a list of all shells and all holes
            ArrayList holesForShells = new ArrayList(shells.size());

            for ( int i = 0; i < shells.size(); i++ ) {
                holesForShells.add(new ArrayList());
            }

            //find homes
            for ( int i = 0; i < holes.size(); i++ ) {
                LinearRing testRing = (LinearRing) holes.get(i);
                LinearRing minShell = null;
                Envelope minEnv = null;
                Envelope testEnv = testRing.getEnvelopeInternal();
                Coordinate testPt = testRing.getCoordinateN(0);
                LinearRing tryRing = null;

                for ( int j = 0; j < shells.size(); j++ ) {
                    tryRing = (LinearRing) shells.get(j);

                    Envelope tryEnv = tryRing.getEnvelopeInternal();

                    if ( minShell != null ) {
                        minEnv = minShell.getEnvelopeInternal();
                    }

                    boolean isContained = false;
                    Coordinate[] coordList = tryRing.getCoordinates();

                    if ( tryEnv.contains(testEnv)
                            && (CGAlgorithms.isPointInRing(testPt, coordList)
                            || (pointInList(testPt, coordList))) ) {
                        isContained = true;
                    }

                    // check if this new containing ring is smaller than the current minimum ring
                    if ( isContained ) {
                        if ( (minShell == null) || minEnv.contains(tryEnv) ) {
                            minShell = tryRing;
                        }
                    }
                }

                if ( minShell == null ) {
				//					System.out.println(
                    //					polygon found with a hole thats not inside a shell);
                    //					azabala: we do the assumption that this hole is really a shell (polygon)
                    //					whose point werent digitized in the right order
                    Coordinate[] cs = testRing.getCoordinates();
                    Coordinate[] reversed = new Coordinate[cs.length];
                    int pointIndex = 0;
                    for ( int z = cs.length - 1; z >= 0; z-- ) {
                        reversed[pointIndex] = cs[z];
                        pointIndex++;
                    }
                    LinearRing newRing = geomFactory.createLinearRing(reversed);
                    shells.add(newRing);
                    holesForShells.add(new ArrayList());
                } else {
                    ((ArrayList) holesForShells.get(shells.indexOf(minShell))).add(testRing);
                }
            }

            Polygon[] polygons = new Polygon[shells.size()];

            for ( int i = 0; i < shells.size(); i++ ) {
                polygons[i] = geomFactory.createPolygon(
                        (LinearRing) shells.get(i),
                        (LinearRing[]) ((ArrayList) holesForShells.get(i)).toArray(new LinearRing[0]));
                polygons[i].setSRID(srid);
            }

            holesForShells = null;
            shells = null;
            holes = null;

            geoJTS = geomFactory.createMultiPolygon(polygons);
            geoJTS.setSRID(srid);

            return geoJTS;
        } catch (RuntimeException ex) {
            logger.debug("Can't convert surface to jts.", ex);
            throw ex;
        }

    }

    private static com.vividsolutions.jts.geom.Geometry ellipseToJts(Geometry shp, int srid) {
        com.vividsolutions.jts.geom.Geometry geoJTS = null;
        int theType;
        int numParts = 0;
        ArrayList arrayCoords = null;
        int subType = shp.getGeometryType().getSubType();
        boolean is3D = subType == 1 || subType == 3;
        double[] dataLine = new double[3];

        ArrayList shells = new ArrayList();
        ArrayList holes = new ArrayList();
        Coordinate[] points = null;

		//El pathIterator no tiene en cuenta coordenadas 3D, pero para la creaci�n de elipses y circulos
        //es necesario el iterador que interpole puntos. El resultado es que en la creaci�n de geometr�as de este
        //tipo no se puede asignar la Z porque se perder�a, pero gvSIG tampoco dispone de esta funci�n, as� que
        //no se nota. Una vez creadas las geometr�a de tipo Elipse y circulo cuando las editamos ya tendr�n todos
        //los puntos calculados y se toman como l�neas por lo que ya se podr�a asignar la Z.
        PathIterator theIterator = shp.getPathIterator(null, manager.getFlatness());
        while ( !theIterator.isDone() ) {
            theType = theIterator.currentSegment(dataLine);

            switch (theType) {
            case PathIterator.SEG_MOVETO:
                if ( arrayCoords == null ) {
                    arrayCoords = new ArrayList();
                } else {
                    points = CoordinateArrays.toCoordinateArray(arrayCoords);

                    try {
                        LinearRing ring = geomFactory.createLinearRing(points);

                        if ( CGAlgorithms.isCCW(points) ) {
                            holes.add(ring);
                        } else {
                            shells.add(ring);
                        }
                    } catch (Exception e) {
                        boolean same = true;
                        for ( int i = 0; i < points.length - 1 && same; i++ ) {
                            if ( points[i].x != points[i + 1].x
                                    || points[i].y != points[i + 1].y /*||
                                     points[i].z != points[i+1].z*/ ) {
                                same = false;
                            }
                        }
                        if ( same ) {
                            return geomFactory.createPoint(points[0]);
                        }

                        if ( points.length > 1 && points.length <= 3 ) {
                            // return geomFactory.createLineString(points);
                            return geomFactory.createMultiLineString(new LineString[]{geomFactory.createLineString(points)});
                        }

                        System.err.println(
                                "Caught Topology exception in GMLLinearRingHandler");

                        return null;
                    }

                    arrayCoords = new ArrayList();
                }

                numParts++;
                loadArrayCoordinates(arrayCoords, is3D, dataLine);
                break;

            case PathIterator.SEG_LINETO:
                loadArrayCoordinates(arrayCoords, is3D, dataLine);
                break;

            case PathIterator.SEG_QUADTO:
                break;

            case PathIterator.SEG_CUBICTO:
                break;

            case PathIterator.SEG_CLOSE:
                Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
                loadArrayCoordinates(arrayCoords, is3D, new double[]{firstCoord.x, firstCoord.y, firstCoord.z});
                break;
            } //end switch

            theIterator.next();
        } //end while loop

        Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
        Coordinate lastCoord = (Coordinate) arrayCoords.get(arrayCoords.size() - 1);

        if ( !isClosed(firstCoord, lastCoord) ) {
            arrayCoords.add(firstCoord);
        }
        points = CoordinateArrays.toCoordinateArray(arrayCoords);

        try {
            LinearRing ring = geomFactory.createLinearRing(points);

            if ( CGAlgorithms.isCCW(points) ) {
                holes.add(ring);
            } else {
                shells.add(ring);
            }
            ring.setSRID(srid);
        } catch (Exception e) {
            boolean same = true;
            for ( int i = 0; i < points.length - 1 && same; i++ ) {
                if ( points[i].x != points[i + 1].x
                        || points[i].y != points[i + 1].y /*||
                         points[i].z != points[i+1].z*/ ) {
                    same = false;
                }
            }
            if ( same ) {
                geoJTS = geomFactory.createPoint(points[0]);
                geoJTS.setSRID(srid);
                return geoJTS;
            }
            if ( points.length > 1 && points.length <= 3 ) {
                // return geomFactory.createLineString(points);
                geoJTS = geomFactory
                        .createMultiLineString(new LineString[]{geomFactory
                            .createLineString(points)});
                geoJTS.setSRID(srid);
                return geoJTS;
            }
            System.err.println(
                    "Caught Topology exception in GMLLinearRingHandler");

            return null;
        }

        /* linRing = new GeometryFactory().createLinearRing(
         CoordinateArrays.toCoordinateArray(arrayCoords)); */
		// System.out.println("NumParts = " + numParts);
        //now we have a list of all shells and all holes
        ArrayList holesForShells = new ArrayList(shells.size());

        for ( int i = 0; i < shells.size(); i++ ) {
            holesForShells.add(new ArrayList());
        }

        //find homes
        for ( int i = 0; i < holes.size(); i++ ) {
            LinearRing testRing = (LinearRing) holes.get(i);
            LinearRing minShell = null;
            Envelope minEnv = null;
            Envelope testEnv = testRing.getEnvelopeInternal();
            Coordinate testPt = testRing.getCoordinateN(0);
            LinearRing tryRing = null;

            for ( int j = 0; j < shells.size(); j++ ) {
                tryRing = (LinearRing) shells.get(j);

                Envelope tryEnv = tryRing.getEnvelopeInternal();

                if ( minShell != null ) {
                    minEnv = minShell.getEnvelopeInternal();
                }

                boolean isContained = false;
                Coordinate[] coordList = tryRing.getCoordinates();

                if ( tryEnv.contains(testEnv)
                        && (CGAlgorithms.isPointInRing(testPt, coordList)
                        || (pointInList(testPt, coordList))) ) {
                    isContained = true;
                }

                // check if this new containing ring is smaller than the current minimum ring
                if ( isContained ) {
                    if ( (minShell == null) || minEnv.contains(tryEnv) ) {
                        minShell = tryRing;
                    }
                }
            }

            if ( minShell == null ) {
				//					System.out.println(
                //					polygon found with a hole thats not inside a shell);
                //					azabala: we do the assumption that this hole is really a shell (polygon)
                //					whose point werent digitized in the right order
                Coordinate[] cs = testRing.getCoordinates();
                Coordinate[] reversed = new Coordinate[cs.length];
                int pointIndex = 0;
                for ( int z = cs.length - 1; z >= 0; z-- ) {
                    reversed[pointIndex] = cs[z];
                    pointIndex++;
                }
                LinearRing newRing = geomFactory.createLinearRing(reversed);
                shells.add(newRing);
                holesForShells.add(new ArrayList());
            } else {
                ((ArrayList) holesForShells.get(shells.indexOf(minShell))).add(testRing);
            }
        }

        Polygon[] polygons = new Polygon[shells.size()];

        for ( int i = 0; i < shells.size(); i++ ) {
            polygons[i] = geomFactory.createPolygon(
                    (LinearRing) shells.get(i),
                    (LinearRing[]) ((ArrayList) holesForShells.get(i)).toArray(new LinearRing[0]));
            polygons[i].setSRID(srid);
        }

        holesForShells = null;
        shells = null;
        holes = null;

        geoJTS = geomFactory.createMultiPolygon(polygons);
        geoJTS.setSRID(srid);

        return geoJTS;
    }

    /**
     * Loads one element in the <code>List</code> of coordinates with the
     * data contained in the array of doubles
     *
     * @param arrayCoords
     * @param is3D
     * @param data
     */
    private static void loadArrayCoordinates(List arrayCoords, boolean is3D, double[] data) {
        if ( is3D ) {
            arrayCoords.add(new Coordinate(data[0], data[1], data[2]));
        } else {
            arrayCoords.add(new Coordinate(data[0], data[1]));
        }
    }

}
