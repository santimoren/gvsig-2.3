/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.persistence;

import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * Factory to persist {@link GeometryType} objects.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class GeometryTypePersistenceFactory extends
    AbstractSinglePersistenceFactory {

    private static final GeometryManager geometryManager = GeometryLocator
        .getGeometryManager();

    private static final String DYNCLASS_NAME = "GeometryType";
    private static final String DYNCLASS_DESCRIPTION = "gvSIG Geometry type";

    private static final String FIELD_TYPE = "type";
    private static final String FIELD_SUBTYPE = "subtype";

    /**
     * Creates a new {@link GeometryTypePersistenceFactory}. It will persist
     * the geometry type and subtype values, and use it to load
     * {@link GeometryType}s from persistence by calling
     * {@link GeometryManager#getGeometryType(int, int)}.
     */
    public GeometryTypePersistenceFactory() {
        super(GeometryType.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null,
            null);

        DynStruct definition = this.getDefinition();
        definition.addDynFieldInt(FIELD_TYPE).setMandatory(true);
        definition.addDynFieldInt(FIELD_SUBTYPE).setMandatory(true);
    }

    public Object createFromState(PersistentState state)
        throws PersistenceException {
        int type = state.getInt(FIELD_TYPE);
        int subType = state.getInt(FIELD_SUBTYPE);

        try {
            return geometryManager.getGeometryType(type, subType);
        } catch (GeometryTypeNotSupportedException e) {
            throw new PersistenceException(
                "Error creating the geometry type with type = " + type
                    + ", subType = " + subType, e);
        } catch (GeometryException e) {
            throw new PersistenceException(
                "Error creating the geometry type with type = " + type
                    + ", subType = " + subType, e);
        }
    }

    public void saveToState(PersistentState state, Object obj)
        throws PersistenceException {
        GeometryType type = (GeometryType) obj;
        state.set(FIELD_TYPE, type.getType());
        state.set(FIELD_SUBTYPE, type.getSubType());
    }

}
