/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.spatialindex;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.SpatialIndexFactory;
import org.gvsig.fmap.geom.generalpath.util.Converter;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.visitor.Visitor;

public class SpatialIndexJTS extends AbstractSpatialIndex implements SpatialIndex {

    private com.vividsolutions.jts.index.quadtree.Quadtree index = null;

    public SpatialIndexJTS(GeometryManager geometryManager, SpatialIndexFactory factory, DynObject parameters) {
        super(geometryManager, factory, parameters);
        this.index = null;
        open();
    }
    
    public void open() {
        this.index = new com.vividsolutions.jts.index.quadtree.Quadtree();
    }

    public void close() {
        this.index = null;
    }

    private com.vividsolutions.jts.geom.Geometry asJTS(org.gvsig.fmap.geom.Geometry geom) {
        return Converter.geometryToJts(geom);
    }

    public long size() {
        return this.index.size();
    }

    public void query(org.gvsig.fmap.geom.primitive.Envelope envelope,
            Visitor visitor) {
        com.vividsolutions.jts.index.ItemVisitor visitor_jts = new JTSVisitorWrapper(visitor);
        com.vividsolutions.jts.geom.Envelope env_jts = asJTS(envelope.getGeometry()).getEnvelopeInternal();
        this.index.query(env_jts, visitor_jts);

    }

    public Iterator query(org.gvsig.fmap.geom.primitive.Envelope envelope,long limit) {
        if( limit!=0 ) {
            throw new UnsupportedOperationException("Not supported yet."); 
        }
        com.vividsolutions.jts.geom.Envelope env_jts = asJTS(envelope.getGeometry()).getEnvelopeInternal();
        List result = this.index.query(env_jts);
        return result.iterator();
    }

    public Iterator queryNearest(org.gvsig.fmap.geom.primitive.Envelope envelope, long limit) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    public Iterator queryAll() {
        List result = this.index.queryAll();
        return result.iterator();
    }

    public void insert(org.gvsig.fmap.geom.primitive.Envelope envelope, Object data) {
        com.vividsolutions.jts.geom.Envelope env_jts = asJTS(envelope.getGeometry()).getEnvelopeInternal();
        index.insert(env_jts, data);
    }

    public boolean remove(org.gvsig.fmap.geom.primitive.Envelope envelope, Object data) {
        com.vividsolutions.jts.geom.Envelope env_jts = asJTS(envelope.getGeometry()).getEnvelopeInternal();
        return index.remove(env_jts, data);
    }

    public void removeAll() {
        index = new com.vividsolutions.jts.index.quadtree.Quadtree();
    }

    private class JTSVisitorWrapper implements com.vividsolutions.jts.index.ItemVisitor {

        private Visitor visitor = null;

        public JTSVisitorWrapper(Visitor visitor) {
            this.visitor = visitor;
        }

        public void visitItem(Object arg0) {
            try {
                this.visitor.visit(arg0);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }

    }

}
