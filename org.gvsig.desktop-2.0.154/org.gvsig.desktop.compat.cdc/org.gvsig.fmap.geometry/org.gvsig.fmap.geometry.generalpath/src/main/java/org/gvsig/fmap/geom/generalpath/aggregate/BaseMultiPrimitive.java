/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.aggregate;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.generalpath.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public abstract class BaseMultiPrimitive extends AbstractPrimitive implements
        MultiPrimitive {

    private static final long serialVersionUID = 8023609161647736932L;

    private static final Logger logger = LoggerFactory.getLogger(GeometryManager.class);

    protected List geometries = null;

    /**
     * The constructor with the GeometryType like and argument is used by the
     * {@link GeometryType}{@link #create()} to create the geometry
     *
     * @param type The geometry type
     */
    public BaseMultiPrimitive(GeometryType geometryType) {
        super(geometryType);
        geometries = new ArrayList();
    }

    BaseMultiPrimitive(GeometryType geometryType, String id, IProjection projection,
            Geometry[] geometries) {
        super(geometryType, id, projection);
        this.geometries = new ArrayList();
        for (int i = 0; i < geometries.length; i++) {
            this.geometries.add(geometries[i]);
        }
    }

    BaseMultiPrimitive(GeometryType geometryType, String id, IProjection projection) {
        super(geometryType, id, projection);
        this.geometries = new ArrayList();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(double, double)
     */
    public boolean contains(double x, double y) {

        boolean bResul;
        for (int i = 0; i < getPrimitivesNumber(); i++) {

            try {
                bResul = containsPoint(
                        (Geometry) geometries.get(i), x, y);
            } catch (GeometryOperationException e) {
                logger.error("While doing contains: " + e.getMessage(), e);
                bResul = true;
            }

            if (bResul) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(double, double, double, double)
     */
    public boolean contains(double x, double y, double w, double h) {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(java.awt.geom.Point2D)
     */
    public boolean contains(Point2D p) {
        boolean bResul;
        for (int i = 0; i < getPrimitivesNumber(); i++) {
            try {
                bResul = containsPoint(
                        (Geometry) geometries.get(i),
                        p.getX(), p.getY());
            } catch (GeometryOperationException e) {
                logger.error("While doing contains: " + e.getMessage(), e);
                bResul = true;
            }
            if (bResul) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#contains(java.awt.geom.Rectangle2D)
     */
    public boolean contains(Rectangle2D r) {
        boolean bResul;
        for (int i = 0; i < getPrimitivesNumber(); i++) {

            try {
                bResul = containsRectangle(
                        (Geometry) geometries.get(i),
                        r.getMinX(), r.getMinY(), r.getWidth(), r.getHeight());
            } catch (GeometryOperationException e) {
                logger.error("While doing contains: " + e.getMessage(), e);
                bResul = true;
            }

            if (bResul) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.geometries.iso.GM_Object#coordinateDimension()
     */
    public int getDimension() {
        return 2;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#fastIntersects(double,
     *      double, double, double)
     */
    public boolean fastIntersects(double x, double y, double w, double h) {

        boolean resp = false;
        for (int i = 0; i < getPrimitivesNumber(); i++) {

            Geometry geom = (Geometry) geometries.get(i);

            try {
                resp = intersectsRectangle(geom, x, y, w, h);
            } catch (GeometryOperationException e) {
                logger.error("While doing fast intersects: " + e.getMessage(), e);
                resp = true;
            }

            if (resp) {
                return true;
            }

        }
        return false;
    }


    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#getBounds()
     */
    public Rectangle getBounds() {
        Rectangle r = null;
        if (getPrimitivesNumber() > 0) {
            r = ((Geometry) geometries.get(0)).getShape().getBounds();
        }
        for (int i = 1; i < getPrimitivesNumber(); i++) {
            Rectangle r2 = ((Geometry) geometries.get(i)).getShape().getBounds();
            r.add(r2);
        }
        return r;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#getBounds2D()
     */
    public Rectangle2D getBounds2D() {
        return null;
    }

    public Handler[] getHandlers(int type) {
        int numPrimitives = getPrimitivesNumber();
        List handlers = new ArrayList();
        for (int i = 0; i < numPrimitives; i++) {
            Handler[] currentHandlers = getPrimitiveAt(i).getHandlers(type);
            for (int j = 0; j < currentHandlers.length; j++) {
                handlers.add(currentHandlers[j]);
            }
        }
        return (Handler[]) handlers.toArray(new Handler[handlers.size()]);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#getInternalShape()
     */
    public Shape getInternalShape() {
        return this;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#getPathIterator(java.awt.geom.AffineTransform)
     */
    public PathIterator getPathIterator(AffineTransform at) {
        GeneralPathX gpx = new GeneralPathX();
        int primiNum = getPrimitivesNumber();
        if (primiNum > 0) {
            Point2D p = ((Geometry) geometries.get(0)).getHandlers(Geometry.SELECTHANDLER)[0]
                    .getPoint();
            gpx.moveTo(p.getX(), p.getY());

            for (int i = 1; i < primiNum; i++) {
                p = ((Geometry) geometries.get(i)).getHandlers(Geometry.SELECTHANDLER)[0]
                        .getPoint();
                gpx.lineTo(p.getX(), p.getY());
            }
            Point2D p2 = ((Geometry) geometries.get(primiNum - 1)).getHandlers(Geometry.SELECTHANDLER)[((Geometry) geometries.get(primiNum - 1)).getHandlers(Geometry.SELECTHANDLER).length - 1]
                    .getPoint();
            gpx.lineTo(p2.getX(), p2.getY());
        }
        return gpx.getPathIterator(at);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#getPathIterator(java.awt.geom.AffineTransform,
     *      double)
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        GeneralPathX gpx = new GeneralPathX();
        int primiNum = getPrimitivesNumber();
        if (primiNum > 0) {

            Geometry prim = null;
            for (int i = 0; i < primiNum; i++) {
                prim = (Geometry) geometries.get(i);
                gpx.append(
                        prim.getPathIterator(at, flatness),
                        /*
                         * This is a relevant change.
                         * After BN 2056 this class will not return
                         * connected path iterator. There will be MOVE_TO
                         * between iterator of each primitive
                         */
                        false);
            }
        }
        /*
         * affine transform and flatness already applied in loop
         */
        return gpx.getPathIterator(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.geometries.iso.aggregate.GM_Aggregate#getPrimitiveAt(int)
     */
    public Primitive getPrimitiveAt(int i) {
        if (i < getPrimitivesNumber()) {
            return (Primitive) ((Geometry) geometries.get(i));
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.geometries.iso.aggregate.GM_Aggregate#getPrimitivesNumber()
     */
    public int getPrimitivesNumber() {
        if (geometries == null) {
            return 0;
        }
        return geometries.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.Shape#intersects(double, double, double, double)
     */
    public boolean intersects(double x, double y, double w, double h) {
        boolean bResul;
        for (int i = 0; i < getPrimitivesNumber(); i++) {

            try {
                bResul = containsRectangle((Geometry) geometries.get(i), x, y, w, h);
            } catch (GeometryOperationException e) {
                logger.error("While doing contains: " + e.getMessage(), e);
                bResul = true;
            }
            if (bResul) {
                return true;
            }
        }
        return false;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#intersects(java.awt.geom.Rectangle2D)
     */
    public boolean intersects(Rectangle2D r) {
        for (int i = 0; i < getPrimitivesNumber(); i++) {

            Geometry prim = this.getPrimitiveAt(i);
            if (prim.intersects(r)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#isSimple()
     */
    public boolean isSimple() {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#reProject(org.cresques.cts.ICoordTrans)
     */
    public void reProject(ICoordTrans ct) {
        for (int i = 0; i < getPrimitivesNumber(); i++) {
            ((Geometry) geometries.get(i)).reProject(ct);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.core.IGeometry#transform(java.awt.geom.AffineTransform)
     */
    public void transform(AffineTransform at) {
        for (int i = 0; i < getPrimitivesNumber(); i++) {
            ((Geometry) geometries.get(i)).transform(at);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.geometries.iso.GM_Object#getBoundary()
     */
    public Envelope getEnvelope() {
        Envelope r = null;
        if (getPrimitivesNumber() > 0) {
            r = ((Geometry) geometries.get(0)).getEnvelope();
        }
        for (int i = 1; i < getPrimitivesNumber(); i++) {
            Envelope r2 = ((Geometry) geometries.get(i)).getEnvelope();
            r.add(r2);
        }
        return r;
    }

    /**
     * @return the geometries
     * @deprecated
     */
    public Geometry[] getGeometries() {
        Geometry[] _geometries = new Geometry[geometries.size()];
        for (int i = 0; i < geometries.size(); i++) {
            _geometries[i] = ((Geometry) geometries.get(i));
        }
        return _geometries;
    }

    public FShape cloneFShape() {
        // TODO Auto-generated method stub
        return null;
    }

    public Handler[] getSelectHandlers() {
        // TODO Auto-generated method stub
        return null;
    }

    public Handler[] getStretchingHandlers() {
        // TODO Auto-generated method stub
        return null;
    }

    public GeneralPathX getGeneralPath() {
        // TODO Auto-generated method stub
        return null;
    }


    public void addPrimitive(Primitive primitive) {
        geometries.add(primitive);
    }

    public Geometry union() 
            throws GeometryOperationException, GeometryOperationNotSupportedException {
        Geometry result = null;
        if (getPrimitivesNumber() > 0) {
            result = this.getPrimitiveAt(0);
        }
        for (int i = 1; i < getPrimitivesNumber(); i++) {
            Primitive geom = this.getPrimitiveAt(i);
            result = result.union(geom);
        }
        return result;
    }

    public Geometry intersection() 
            throws GeometryOperationException, GeometryOperationNotSupportedException {
        Geometry result = null;
        if (getPrimitivesNumber() > 0) {
            result = this.getPrimitiveAt(0);
        }
        for (int i = 1; i < getPrimitivesNumber(); i++) {
            Primitive geom = this.getPrimitiveAt(i);
            result = result.intersection(geom);
        }
        return result;
    }

    public void ensureCapacity(int capacity) {
        ((ArrayList)this.geometries).ensureCapacity(capacity);
    }
        
}
