/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.curve.line;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Iterator;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.generalpath.DefaultGeometryManager;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.generalpath.primitive.curve.DefaultCurve;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.generalpath.util.UtilFunctions;
import org.gvsig.fmap.geom.primitive.Polygon;

/**
 * DOCUMENT ME!
 *
 * @author Fernando Gonz�lez Cort�s
 */
public class Line2D extends DefaultCurve implements Line {
	private static final long serialVersionUID = 8161943328767877860L;

	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public Line2D(GeometryType geomType) {
		super(geomType);
	}

	/**
	 * Constructor used in the {@link Geometry#cloneGeometry()} method.
	 * @param geomType
	 * @param id
	 * @param projection
	 * @param gpx
	 */
	public Line2D(GeometryType geomType, String id, IProjection projection, GeneralPathX gpx) {
		super(geomType, id, projection, gpx);
		gp=gpx;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#getShapeType()
	 */
	public int getShapeType() {
		return TYPES.CURVE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.FShape#cloneFShape()
	 */
	public FShape cloneFShape() {
		return new Line2D(getGeometryType(), id, projection, (GeneralPathX) gp.clone());
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Curve#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
	 */
	public void setPoints(Point startPoint, Point endPoint) {
		Point2D _startPoint = new java.awt.geom.Point2D.Double(startPoint.getCoordinateAt(0), startPoint.getCoordinateAt(1));
		Point2D _endPoint = new java.awt.geom.Point2D.Double(endPoint.getCoordinateAt(0), endPoint.getCoordinateAt(1));
		setPoints(_startPoint, _endPoint);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Curve#setPoints(java.awt.geom.Point2D, java.awt.geom.Point2D)
	 */
        
	private void setPoints(Point2D startPoint, Point2D endPoint) {
		java.awt.geom.Line2D line = UtilFunctions.createLine(startPoint, endPoint);
		setGeneralPath(new GeneralPathX(line.getPathIterator(null)));
	}

    public MultiPoint toPoints() throws GeometryException {
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPoint multipoint = manager.createMultiPoint(this.getGeometryType().getSubType());
        multipoint.ensureCapacity(this.getNumVertices());
        for( int n=0; n<this.getNumVertices(); n++ ) {
            multipoint.addPrimitive(this.getVertex(n));
        }
        return multipoint;    
    }

    public MultiLine toLines() throws GeometryException {
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiLine multi = manager.createMultiLine(this.getGeometryType().getSubType());
        multi.addPrimitive(this);
        return multi;
    }

    public MultiPolygon toPolygons() throws GeometryException {
        com.vividsolutions.jts.operation.polygonize.Polygonizer jts_polygonizer = new com.vividsolutions.jts.operation.polygonize.Polygonizer();
        
        jts_polygonizer.add(this.toJTS());
        Collection jts_polygons = jts_polygonizer.getPolygons();
        if( jts_polygons == null && jts_polygons.isEmpty() ) {
            return null;
        }
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPolygon multipolygon = manager.createMultiPolygon(this.getGeometryType().getSubType());
        Iterator it = jts_polygons.iterator();
        while( it.hasNext() ) {
            com.vividsolutions.jts.geom.Geometry jts_polygon = (com.vividsolutions.jts.geom.Geometry) it.next();
            Polygon polygon = (Polygon) manager.createFrom(jts_polygon);
            multipolygon.addPrimitive(polygon);
        }
        return multipolygon;
    }
    
}
