/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.point;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * Geometry type implementation for the creation of {@link Point3D} objects.
 * 
 * @author gvSIG Team
 */
public class Point3DGeometryType extends Point2DGeometryType {

    public String getName() {
        return "Point3D";
    }

    public int getSubType() {
        return Geometry.SUBTYPES.GEOM3D;
    }

    public boolean isSubTypeOf(int geometrySubType) {
        return Geometry.SUBTYPES.GEOM3D == geometrySubType
            || super.isSubTypeOf(geometrySubType);
    }

    public Geometry create() throws CreateGeometryException {
        return new Point2DZ(this);
    }

    public Point createPoint(double x, double y) {
        return new Point2DZ(x, y, 0.0d, this);
    }

    public Point createPoint(double[] coordinates) {
        return new Point2DZ(coordinates[0], coordinates[1], coordinates[2],
            this);
    }

}
