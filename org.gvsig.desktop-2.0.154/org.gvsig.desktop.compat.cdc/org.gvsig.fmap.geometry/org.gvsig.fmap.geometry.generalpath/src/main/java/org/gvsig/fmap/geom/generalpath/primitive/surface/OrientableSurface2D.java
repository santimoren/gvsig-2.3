/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.surface;

import com.vividsolutions.jts.geom.LineString;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientableSurface;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.primitive.SurfaceAppearance;
import org.gvsig.fmap.geom.generalpath.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.generalpath.primitive.OrientablePrimitive2D;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.generalpath.util.Converter;

/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public abstract class OrientableSurface2D extends OrientablePrimitive2D implements OrientableSurface {

    private static final long serialVersionUID = 9212660743520612859L;

    /**
     * The constructor with the GeometryType like and argument
     * is used by the {@link GeometryType}{@link #create()}
     * to create the geometry
     *
     * @param type
     * The geometry type
     */
    public OrientableSurface2D(GeometryType geometryType) {
        super(geometryType);
    }

    public OrientableSurface2D(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx) {
        super(geometryType, id, projection, gpx);
    }

    public SurfaceAppearance getSurfaceAppearance() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setSurfaceAppearance(SurfaceAppearance app) {
		// TODO Auto-generated method stub

    }

    public int getNumInteriorRings() {
            com.vividsolutions.jts.geom.Polygon jts = (com.vividsolutions.jts.geom.Polygon) this.toJTS();
            int count = jts.getNumInteriorRing();
            return count;
    }

    public Ring getInteriorRing(int index) {
        try {
            com.vividsolutions.jts.geom.Polygon jts = (com.vividsolutions.jts.geom.Polygon) this.toJTS();
            LineString jts_ring = jts.getInteriorRingN(index);
            Geometry ring = Converter.jtsToGeometry(jts_ring);
            return (Ring) ring;
        } catch (Exception ex) {
            logger.warn("Can't get interior ring '"+index+"'.",ex);
            throw  new RuntimeException(ex.getMessage(),ex);
        }
    }

    public void addInteriorRing(Ring ring) {
        this.getGeneralPath().append(ring.getGeneralPath());
    }

    public void removeInteriorRing(int index) {
    }

}
