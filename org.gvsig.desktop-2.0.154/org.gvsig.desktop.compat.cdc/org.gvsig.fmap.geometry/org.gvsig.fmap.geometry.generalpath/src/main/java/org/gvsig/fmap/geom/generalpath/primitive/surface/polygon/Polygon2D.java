/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.surface.polygon;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.TopologyException;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;
import com.vividsolutions.jts.precision.GeometryPrecisionReducer;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.generalpath.DefaultGeometryManager;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.generalpath.primitive.surface.OrientableSurface2D;
import org.gvsig.fmap.geom.generalpath.util.Converter;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * Pol�gono 2D.
 *
 * @author Vicente Caballero Navarro
 */
public class Polygon2D extends OrientableSurface2D implements Polygon {
	private static final long serialVersionUID = -8448256617197415743L;

	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public Polygon2D(GeometryType geomType) {
		super(geomType);
	}

	/**
	 * Constructor used in the {@link Geometry#cloneGeometry()} method.
	 * @param geomType
	 * @param id
	 * @param projection
	 * @param gpx
	 */
	protected Polygon2D(GeometryType geomType, String id, IProjection projection, GeneralPathX gpx) {
		super(geomType, id, projection, gpx);
	}	

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#getShapeType()
	 */
	public int getShapeType() {
		return TYPES.SURFACE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.FShape#cloneFShape()
	 */
	public FShape cloneFShape() {
		return new Polygon2D(getGeometryType(), id, projection, (GeneralPathX) gp.clone());
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.OrientablePrimitive2D#intersects(java.awt.geom.Rectangle2D)
	 */
	public boolean intersects(Rectangle2D r) {
		return gp.intersects(r);
	}

	
    public Geometry union(Geometry other)
        throws GeometryOperationNotSupportedException,
        GeometryOperationException {

        com.vividsolutions.jts.geom.Geometry this_jts, other_jts, union_jts;
        Geometry resp = null;

        try {
            this_jts = getJTS();
            other_jts = Converter.geometryToJts(other);
            union_jts = this_jts.union(other_jts);
            if (union_jts instanceof com.vividsolutions.jts.geom.Polygon) {
                double tole = union_jts.getArea();
                tole = tole * 1e-12;
                union_jts = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) union_jts, tole);
            } else {
                if (union_jts instanceof com.vividsolutions.jts.geom.MultiPolygon) {
                    double tole = union_jts.getArea();
                    tole = tole * 1e-12;
                    union_jts =
                        removeInvalidHoles((com.vividsolutions.jts.geom.MultiPolygon) union_jts, tole);
                }
            }
            resp = Converter.jtsToGeometry(union_jts);
            return resp;
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        } catch (TopologyException e) {
            /*
             * If JTS throws this, we'll try to simplify them
             */
            try {
                this_jts = getJTS();
                other_jts = Converter.geometryToJts(other);

                PrecisionModel pm = new PrecisionModel(1000000);
                GeometryPrecisionReducer gpr = new GeometryPrecisionReducer(pm);
                this_jts = gpr.reduce(this_jts);
                other_jts = gpr.reduce(other_jts);
                union_jts = this_jts.union(other_jts);

                if (union_jts instanceof com.vividsolutions.jts.geom.Polygon) {
                    double tole = union_jts.getArea();
                    tole = tole * 1e-12;
                    union_jts = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) union_jts, tole);
                } else {
                    if (union_jts instanceof com.vividsolutions.jts.geom.MultiPolygon) {
                        double tole = union_jts.getArea();
                        tole = tole * 1e-12;
                        union_jts =
                            removeInvalidHoles((com.vividsolutions.jts.geom.MultiPolygon) union_jts, tole);
                    }
                }

                resp = Converter.jtsToGeometry(union_jts);
                return resp;
            } catch (Exception exc) {
                throw new GeometryOperationException(exc);
            }
        }
    }
    
    
    private com.vividsolutions.jts.geom.Geometry getJTS() {
        return Converter.geometryToJts(this);
    }
    
    private com.vividsolutions.jts.geom.MultiPolygon removeInvalidHoles(com.vividsolutions.jts.geom.MultiPolygon mpo, double tol) {

        GeometryFactory gf = new GeometryFactory(mpo.getPrecisionModel());
        
        int npo = mpo.getNumGeometries();
        com.vividsolutions.jts.geom.Polygon[] pos = new com.vividsolutions.jts.geom.Polygon[npo];
        for (int i=0; i<npo; i++) {
            pos[i] = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) mpo.getGeometryN(i), tol);
        }
        return gf.createMultiPolygon(pos);
    }
    
    private com.vividsolutions.jts.geom.Polygon removeInvalidHoles(com.vividsolutions.jts.geom.Polygon po, double tol) {
        
        GeometryFactory gf = new GeometryFactory(po.getPrecisionModel());
        
        int nholes = po.getNumInteriorRing();
        List validholes = new ArrayList();
        for (int i=0; i<nholes; i++) {
            LineString ls = po.getInteriorRingN(i);
            LinearRing lr = toLinearRing(ls, gf);
            if (getLinearRingArea(lr, gf) > tol) {
                validholes.add(lr);
            }
            
        }
        
        if (validholes.size() < nholes) {
            
            LinearRing[] holes = (LinearRing[]) validholes.toArray(new LinearRing[0]);
            return gf.createPolygon(
                toLinearRing(po.getExteriorRing(), gf), holes);
        } else {
            return po;
        }
        
    }

    private double getLinearRingArea(LinearRing lr, GeometryFactory gf) {
        com.vividsolutions.jts.geom.Polygon po = gf.createPolygon(lr);
        double resp = po.getArea(); 
        return resp;
    }

    private LinearRing toLinearRing(LineString ls, GeometryFactory gf) {
        return gf.createLinearRing(ls.getCoordinateSequence());
    }

    public MultiPoint toPoints() throws GeometryException {
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPoint multipoint = manager.createMultiPoint(this.getGeometryType().getSubType());
        multipoint.ensureCapacity(this.getNumVertices());
        for( int n=0; n<this.getNumVertices(); n++ ) {
            multipoint.addPrimitive(this.getVertex(n));
        }
        return multipoint;    
    }

    public MultiLine toLines() throws GeometryException {
        GeometryFactory fact = new GeometryFactory();
        Coordinate[] coordinates = this.toJTS().getCoordinates();
        LineString jts_line = fact.createLineString(coordinates);
        
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        Geometry line = manager.createFrom(jts_line);
        MultiCurve multi = manager.createMultiCurve(this.getGeometryType().getSubType());
        multi.addPrimitive((Primitive) line);
        return (MultiLine) multi;
    }

    public MultiPolygon toPolygons() throws GeometryException {
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPolygon multi = manager.createMultiPolygon(this.getGeometryType().getSubType());
        multi.addPrimitive(this);
        return multi;    
    }
	
}