/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.geom.generalpath;

import java.lang.reflect.Constructor;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.type.AbstractGeometryType;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultGeometryType extends AbstractGeometryType {
	
	/** 
	 * Geometry type name
	 */
	private String name;
		
	/** Class that implements this class type */
	private Class geometryClass;
	
	/**
	 * The type of the geometry. The type is an abstract representation
	 * of the object (Point, Curve...) but it is not a concrete 
	 * representation (Point2D, Point3D...). To do that comparation
	 * the id field is used.
	 */
	private int type;
	
	/**
	 * The subtype of the geometry. The subtype represents a set of 
	 * geometries with a dimensional relationship (2D, 3D, 2DM...)
	 */
	private int subType;	
		
	/**
	 * Super types of a geometry. e.g: the super type of an
	 * arc is a curve, the super type of a circle is a surface...
	 * The supertypes are defined in the ISO 19107 
	 * 
	 * @see <a href="http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=26012"
	 * >ISO 19107< /a>
	 */
	private int[] superTypes = null;
	
	/**
     * Super SubTypes of a geometry. e.g: the super SubType of a
     * geometry 3D is a geometry 2D, because the 3D extends the
     * behavior of a geometry 2D
     */
	private int[] superSubTypes = null;
	
    private Constructor constructor;
    private final Object[] parameters = { this };

	/**
     * This constructor is used by the {@link GeometryManager} when it
     * register a new GeometryType. It has not be used from other
     * parts.
     * @param geomClass
     * Geometry class (e.g: Point2D.class)
     * @param name
     * Symbolic Geometry name that is used to persist the geometry type. In some
     * cases, it is better to use this name because the id can change for different
     * application executions.              
     * @param id
     * Geometry id  
     * @param typeName
     * The geometry type name
     * @param type
     * The geometry abstract type    
     * @param superTypes
     * The superTypes of the geometry type
     * @param superSubTypes
     * The superSubtypes of the geometry type      
     */
    public DefaultGeometryType(Class geomClass, String name, int type, int subType, 
        int[] superTypes, int[] superSubTypes) {
        this.geometryClass = geomClass;
        if (name == null) {
            this.name = geomClass.getName();
        } else {
            this.name = name;
        }
        this.type = type;   
        this.subType = subType;
        this.superTypes = superTypes;
        this.superSubTypes = superSubTypes;

        Class[] parameterTypes = { GeometryType.class };
        try {
            constructor = geometryClass.getConstructor(parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(
                "Error constructor of the geometry class " + geomClass
                    + " with one parameter of type GeometryType not found", e);
        }
    }	
	
	/**
	 * This constructor is used by the {@link GeometryManager} when it
	 * register a new GeometryType. It has not be used from other
	 * parts.
	 * @param geomClass
	 * Geometry class (e.g: Point2D.class)
	 * @param name
	 * Symbolic Geometry name that is used to persist the geometry type. In some
	 * cases, it is better to use this name because the id can change for different
	 * application executions.  	 		
	 * @param id
	 * Geometry id	
	 * @param typeName
	 * The geometry type name
	 * @param type
	 * The geometry abstract type			
	 */
	public DefaultGeometryType(Class geomClass, String name, int type, int subType) {
		this(geomClass, name, type, subType, new int[0], new int[0]);
	}
	
	/**
	 * This method creates a {@link Geometry} with the type specified 
	 * by this GeometryType. The geometry has to have a constructor
	 * without arguments.
	 * 
	 * @return A new geometry
	 * @throws CreateGeometryException 
	 */
	public Geometry create() throws CreateGeometryException{
		try {
            return (Geometry) constructor.newInstance(parameters);
		} catch (Exception e) {
			throw new CreateGeometryException(type, subType, e);
		} 
	}
	
	public Class getGeometryClass() {
		return geometryClass;
	}
	
	public String getName() {
		return name;
	}

	public int getType() {
		return type;
	}

	public int getSubType() {
		return subType;
	}

    public boolean isTypeOf(int geometryType) {
        if (type == geometryType){
            return true;
        }
        for (int i=0 ; i<superTypes.length ; i++){
            if(superTypes[i] == geometryType){
                return true;
            }
        }
        return Geometry.TYPES.GEOMETRY == geometryType;
    }

    public boolean isSubTypeOf(int geometrySubType) {
        if (subType == geometrySubType){
            return true;
        }
        for (int i=0 ; i<superSubTypes.length ; i++){
            if(superSubTypes[i] == geometrySubType){
                return true;
            }
        }
        return false;
    }

}

