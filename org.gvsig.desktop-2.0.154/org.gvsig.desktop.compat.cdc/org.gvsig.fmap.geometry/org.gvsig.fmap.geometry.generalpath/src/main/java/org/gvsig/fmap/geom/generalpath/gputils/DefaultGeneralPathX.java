/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.gputils;

/*
 * Based on portions of code from java.awt.geom.GeneralPath of the
 * OpenJDK project (Copyright (c) 1996, 2006, Oracle and/or its affiliates)
 */
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.IllegalPathStateException;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.cresques.cts.ICoordTrans;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.jdk.GeomUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequences;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import java.io.Serializable;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.IGeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.generalpath.primitive.point.Point2DZ;

/**
 * The <code>GeneralPathX</code> class represents a geometric path
 * constructed from straight lines, and quadratic and cubic
 * (B&eacute;zier) curves. It can contain multiple subpaths.
 * <p>
 * The winding rule specifies how the interior of a path is determined. There
 * are two types of winding rules: EVEN_ODD and NON_ZERO.
 * <p>
 * An EVEN_ODD winding rule means that enclosed regions of the path alternate
 * between interior and exterior areas as traversed from the outside of the path
 * towards a point inside the region.
 * <p>
 * A NON_ZERO winding rule means that if a ray is drawn in any direction from a
 * given point to infinity and the places where the path intersects the ray are
 * examined, the point is inside of the path if and only if the number of times
 * that the path crosses the ray from left to right does not equal the number of
 * times that the path crosses the ray from right to left.
 * 
 * @version 1.58, 01/23/03
 * @author Jim Graham
 * @deprecated
 *             use the geometry methods
 */
public class DefaultGeneralPathX extends GeneralPathX implements  Shape, Cloneable, Serializable { 

    /**
     * Default serial version ID
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultGeneralPathX.class);

    protected static GeometryManager geomManager = GeometryLocator
        .getGeometryManager();

    private List pointTypes = new ArrayList();
    private List pointCoords = new ArrayList();

    private Byte[] SEG_TYPES = new Byte[] {
        new Byte((byte)0),
        new Byte((byte)1),
        new Byte((byte)2),
        new Byte((byte)3),
        new Byte((byte)4),
        new Byte((byte)5),
        new Byte((byte)6),
        new Byte((byte)7),
        new Byte((byte)8),
        new Byte((byte)9),
        new Byte((byte)10)
    };
    
    int windingRule;

    private boolean isSimple = true;

    static final int EXPAND_MAX = 500;

    private  DefaultGeneralPathX() {
        super(false);
        setWindingRule(WIND_EVEN_ODD);
    }
    /**
     * Constructs a new <code>GeneralPathX</code> object with the specified
     * winding rule to control operations that require the interior of the
     * path to be defined.
     * 
     * @param rule
     *            the winding rule
     * @see #WIND_EVEN_ODD
     * @see #WIND_NON_ZERO
     */
    public DefaultGeneralPathX(int rule) {
        super(false);
        setWindingRule(rule);
    }

    /**
     * Constructs a new <code>GeneralPathX</code> object from an arbitrary
     * {@link Shape} object.
     * All of the initial geometry and the winding rule for this path are
     * taken from the specified <code>Shape</code> object.
     * 
     * @param s
     *            the specified <code>Shape</code> object
     */
    public DefaultGeneralPathX(PathIterator piter) {
        this(WIND_EVEN_ODD);
        setWindingRule(piter.getWindingRule());
        append(piter, false);
    }

    private void needRoom(int newTypes, int newCoords, boolean needMove) {
        if (needMove && getNumTypes() == 0) {
            throw new IllegalPathStateException("missing initial moveto "
                + "in path definition");
        }
    }

    /**
     * Adds a point to the path by moving to the specified
     * coordinates.
     * 
     * @param x
     *            ,&nbsp;y the specified coordinates
     * @deprecated
     *             use moveTo(Point)
     */
    public synchronized void moveTo(double x, double y) {
        int numtypes = getNumTypes();
        if (numtypes > 0 && getTypeAt(numtypes - 1) == SEG_MOVETO) {
            Point point = getPointAt(getNumCoords() - 1);
            point.setX(x);
            point.setY(y);
        } else {
            needRoom(1, 2, false);
            pointTypes.add(SEG_TYPES[SEG_MOVETO]);
            addPoint(x, y);
        }
    }

    public synchronized void moveTo(Point point) {
        int numtypes = getNumTypes();
        if (numtypes > 0 && getTypeAt(numtypes - 1) == SEG_MOVETO) {
            pointCoords.remove(getNumCoords() - 1);
            addPoint(point);
        } else {
            needRoom(1, 2, false);
            pointTypes.add(SEG_TYPES[SEG_MOVETO]);
            addPoint(point);
        }
    }

    /**
     * Adds a point to the path by drawing a straight line from the
     * current coordinates to the new specified coordinates.
     * 
     * @param x
     *            ,&nbsp;y the specified coordinates
     * @deprecated
     *             use lineTo(Point)
     */
    public synchronized void lineTo(double x, double y) {
        needRoom(1, 2, true);
        pointTypes.add(SEG_TYPES[SEG_LINETO]);
        addPoint(x, y);
    }

    public synchronized void lineTo(Point point) {
        needRoom(1, 2, true);
        pointTypes.add(SEG_TYPES[SEG_LINETO]);
        addPoint(point);
    }

    public synchronized void addSegment(Point[] segment) {
        if (segment != null && segment.length > 0) {
            needRoom(segment.length, 2 * segment.length, true);
            for (int i = 0; i < segment.length; i++) {
                pointTypes.add(SEG_TYPES[SEG_LINETO]);
                addPoint(segment[i]);
            }
        }
    }

    private void addPoint(double x, double y) {
        try {
            pointCoords.add(geomManager.createPoint(x, y,
                Geometry.SUBTYPES.GEOM2D));
        } catch (CreateGeometryException e) {
            LOG.error("Error creating a point", e);
        }
    }

    private void addPoint(Point point) {
        pointCoords.add(point);
    }

    /**
     * Adds a curved segment, defined by two new points, to the path by
     * drawing a Quadratic curve that intersects both the current
     * coordinates and the coordinates (x2,&nbsp;y2), using the
     * specified point (x1,&nbsp;y1) as a quadratic parametric control
     * point.
     * 
     * @param x1
     *            ,&nbsp;y1 the coordinates of the first quadratic control
     *            point
     * @param x2
     *            ,&nbsp;y2 the coordinates of the final endpoint
     * @deprecated
     *             use quadTo(Point, Point)
     */
    public synchronized void quadTo(double x1, double y1, double x2, double y2) {
        needRoom(1, 4, true);
        pointTypes.add(SEG_TYPES[SEG_QUADTO]);
        addPoint(x1, y1);
        addPoint(x2, y2);
        isSimple = false;
    }

    public synchronized void quadTo(Point point1, Point point2) {
        needRoom(1, 4, true);
        pointTypes.add(SEG_TYPES[SEG_QUADTO]);
        addPoint(point1);
        addPoint(point2);
        isSimple = false;
    }

    /**
     * Adds a curved segment, defined by three new points, to the path by
     * drawing a B&eacute;zier curve that intersects both the current
     * coordinates and the coordinates (x3,&nbsp;y3), using the
     * specified points (x1,&nbsp;y1) and (x2,&nbsp;y2) as
     * B&eacute;zier control points.
     * 
     * @param x1
     *            ,&nbsp;y1 the coordinates of the first B&eacute;ezier
     *            control point
     * @param x2
     *            ,&nbsp;y2 the coordinates of the second B&eacute;zier
     *            control point
     * @param x3
     *            ,&nbsp;y3 the coordinates of the final endpoint
     * @deprecated
     *             use curveTo(Point, Point, Point)
     */
    public synchronized void curveTo(double x1, double y1, double x2,
        double y2, double x3, double y3) {
        needRoom(1, 6, true);
        pointTypes.add(SEG_TYPES[SEG_CUBICTO]);
        addPoint(x1, y1);
        addPoint(x2, y2);
        addPoint(x3, y3);
        isSimple = false;
    }

    public synchronized void curveTo(Point point1, Point point2, Point point3) {
        needRoom(1, 6, true);
        pointTypes.add(SEG_TYPES[SEG_CUBICTO]);
        addPoint(point1);
        addPoint(point2);
        addPoint(point3);
        isSimple = false;
    }

    /**
     * Closes the current subpath by drawing a straight line back to
     * the coordinates of the last <code>moveTo</code>. If the path is already
     * closed then this method has no effect.
     */
    public synchronized void closePath() {
        if (getNumTypes() == 0 || getTypeAt(getNumTypes() - 1) != SEG_CLOSE) {
            needRoom(1, 0, true);
            // Adding a geometry like the last geometry
            // addPoint(100, 100);
            pointTypes.add(SEG_TYPES[SEG_CLOSE]);
        }
    }

    /**
     * Check if the first part is closed.
     * 
     * @return
     */
    public boolean isClosed() {
        PathIterator theIterator =
            getPathIterator(null, geomManager.getFlatness());
        double[] theData = new double[6];
        double xFinal = 0;
        double yFinal = 0;
        double xIni = 0;
        double yIni = 0;
        boolean first = true;

        while (!theIterator.isDone()) {
            // while not done
            int theType = theIterator.currentSegment(theData);

            switch (theType) {
            case PathIterator.SEG_MOVETO:
                xIni = theData[0];
                yIni = theData[1];
                if (!first) {
                    break;
                }
                first = false;
                break;

            case PathIterator.SEG_LINETO:
                xFinal = theData[0];
                yFinal = theData[1];
                break;
            case PathIterator.SEG_CLOSE:
                return true;

            } // end switch

            theIterator.next();
        }
        if ((xFinal == xIni) && (yFinal == yIni))
            return true;
        return false;
    }

    /**
     * Appends the geometry of the specified {@link PathIterator} object
     * to the path, possibly connecting the new geometry to the existing
     * path segments with a line segment.
     * If the <code>connect</code> parameter is <code>true</code> and the
     * path is not empty then any initial <code>moveTo</code> in the
     * geometry of the appended <code>Shape</code> is turned into a
     * <code>lineTo</code> segment.
     * If the destination coordinates of such a connecting <code>lineTo</code>
     * segment match the ending coordinates of a currently open
     * subpath then the segment is omitted as superfluous.
     * The winding rule of the specified <code>Shape</code> is ignored
     * and the appended geometry is governed by the winding
     * rule specified for this path.
     * 
     * @param pi
     *            the <code>PathIterator</code> whose geometry is appended to
     *            this path
     * @param connect
     *            a boolean to control whether or not to turn an
     *            initial <code>moveTo</code> segment into a <code>lineTo</code>
     *            segment
     *            to connect the new geometry to the existing path
     */
    public void append(PathIterator pi, boolean connect) {
        double coords[] = new double[6];
        while (!pi.isDone()) {
            switch (pi.currentSegment(coords)) {
            case SEG_MOVETO:
                if (!connect || getNumTypes() < 1 || getNumCoords() < 2) {
                    moveTo(coords[0], coords[1]);
                    break;
                }
                if (getTypeAt(getNumTypes() - 1) != SEG_CLOSE
                    && getPointAt(getNumCoords() - 1).getX() == coords[0]
                    && getPointAt(getNumCoords() - 1).getY() == coords[1]) {
                    // Collapse out initial moveto/lineto
                    break;
                }
                // NO BREAK;
            case SEG_LINETO:
                lineTo(coords[0], coords[1]);
                break;
            case SEG_QUADTO:
                quadTo(coords[0], coords[1], coords[2], coords[3]);
                break;
            case SEG_CUBICTO:
                curveTo(coords[0], coords[1], coords[2], coords[3], coords[4],
                    coords[5]);
                break;
            case SEG_CLOSE:
                closePath();
                break;
            }
            pi.next();
            connect = false;
        }
    }
   
    public void append(GeneralPathX gp) {
        for( int i=0 ; i<gp.getNumCoords(); i++ ) {
            byte type = gp.getTypeAt(i);
            Point point = gp.getPointAt(i);
            pointTypes.add(SEG_TYPES[type]);
            addPoint(point);
        }
    }
  
    /**
     * Returns the fill style winding rule.
     * 
     * @return an integer representing the current winding rule.
     * @see #WIND_EVEN_ODD
     * @see #WIND_NON_ZERO
     * @see #setWindingRule
     */
    public synchronized int getWindingRule() {
        return windingRule;
    }

    /**
     * Sets the winding rule for this path to the specified value.
     * 
     * @param rule
     *            an integer representing the specified
     *            winding rule
     * @exception <code>IllegalArgumentException</code> if <code>rule</code> is
     *            not either <code>WIND_EVEN_ODD</code> or
     *            <code>WIND_NON_ZERO</code>
     * @see #WIND_EVEN_ODD
     * @see #WIND_NON_ZERO
     * @see #getWindingRule
     */
    public void setWindingRule(int rule) {
        if (rule != WIND_EVEN_ODD && rule != WIND_NON_ZERO) {
            throw new IllegalArgumentException("winding rule must be "
                + "WIND_EVEN_ODD or " + "WIND_NON_ZERO");
        }
        windingRule = rule;
    }

    /**
     * Returns the coordinates most recently added to the end of the path
     * as a {@link Point2D} object.
     * 
     * @return a <code>Point2D</code> object containing the ending
     *         coordinates of the path or <code>null</code> if there are no
     *         points
     *         in the path.
     */
    public synchronized Point2D getCurrentPoint() {
        if (getNumTypes() < 1 || getNumCoords() < 1) {
            return null;
        }
        int index = getNumCoords();
        if (getTypeAt(getNumTypes() - 1) == SEG_CLOSE) {
            loop: for (int i = getNumTypes() - 2; i > 0; i--) {
                switch (getTypeAt(i)) {
                case SEG_MOVETO:
                    break loop;
                case SEG_LINETO:
                    index -= 2;
                    break;
                case SEG_QUADTO:
                    index -= 4;
                    break;
                case SEG_CUBICTO:
                    index -= 6;
                    break;
                case SEG_CLOSE:
                    break;
                }
            }
        }
        return new Point2D.Double(getPointAt(index - 1).getX(), getPointAt(
            index - 1).getY());
    }

    /**
     * Resets the path to empty. The append position is set back to the
     * beginning of the path and all coordinates and point types are
     * forgotten.
     */
    public synchronized void reset() {
        pointCoords.clear();
        pointTypes.clear();
    }

    /**
     * Transforms the geometry of this path using the specified
     * {@link AffineTransform}.
     * The geometry is transformed in place, which permanently changes the
     * boundary defined by this object.
     * 
     * @param at
     *            the <code>AffineTransform</code> used to transform the area
     */
    public void transform(AffineTransform at) {
        for (int i = 0; i < getNumCoords(); i++) {
            getPointAt(i).transform(at);
        }
    }

    public void reProject(ICoordTrans ct) {
        for (int i = 0; i < getNumCoords(); i++) {
            getPointAt(i).reProject(ct);
        }
    }

    /**
     * Returns a new transformed <code>Shape</code>.
     * 
     * @param at
     *            the <code>AffineTransform</code> used to transform a
     *            new <code>Shape</code>.
     * @return a new <code>Shape</code>, transformed with the specified
     *         <code>AffineTransform</code>.
     */
    public synchronized Shape createTransformedShape(AffineTransform at) {
        DefaultGeneralPathX gp = (DefaultGeneralPathX) clone();
        if (at != null) {
            gp.transform(at);
        }
        return gp;
    }

    /**
     * Return the bounding box of the path.
     * 
     * @return a {@link java.awt.Rectangle} object that
     *         bounds the current path.
     */
    public java.awt.Rectangle getBounds() {
        return getBounds2D().getBounds();
    }

    /**
     * Returns the bounding box of the path.
     * 
     * @return a {@link Rectangle2D} object that
     *         bounds the current path.
     */
    public synchronized Rectangle2D getBounds2D() {
        double x1, y1, x2, y2;
        int i = getNumCoords();
        if (i > 0) {
            y1 = y2 = getPointAt(--i).getY();
            x1 = x2 = getPointAt(i).getX();
            while (i > 0) {
                double y = getPointAt(--i).getY();
                double x = getPointAt(i).getX();
                if (x < x1)
                    x1 = x;
                if (y < y1)
                    y1 = y;
                if (x > x2)
                    x2 = x;
                if (y > y2)
                    y2 = y;
            }
        } else {
            x1 = y1 = x2 = y2 = 0.0f;
        }
        return new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);
    }

    /**
     * Tests if the specified coordinates are inside the boundary of
     * this <code>Shape</code>.
     * 
     * @param x
     *            ,&nbsp;y the specified coordinates
     * @return <code>true</code> if the specified coordinates are inside this
     *         <code>Shape</code>; <code>false</code> otherwise
     */
    public boolean contains(double x, double y) {
        if (pointTypes.size() < 2) {
            return false;
        }
        int cross =
            GeomUtilities.pointCrossingsForPath(getPathIterator(null), x, y);
        if (windingRule == WIND_NON_ZERO) {
            return (cross != 0);
        } else {
            return ((cross & 1) != 0);
        }
    }

    /**
     * Tests if the specified <code>Point2D</code> is inside the boundary
     * of this <code>Shape</code>.
     * 
     * @param p
     *            the specified <code>Point2D</code>
     * @return <code>true</code> if this <code>Shape</code> contains the
     *         specified <code>Point2D</code>, <code>false</code> otherwise.
     */
    public boolean contains(Point2D p) {
        return contains(p.getX(), p.getY());
    }

    /**
     * Tests if the specified rectangular area is inside the boundary of
     * this <code>Shape</code>.
     * 
     * @param x
     *            ,&nbsp;y the specified coordinates
     * @param w
     *            the width of the specified rectangular area
     * @param h
     *            the height of the specified rectangular area
     * @return <code>true</code> if this <code>Shape</code> contains
     *         the specified rectangluar area; <code>false</code> otherwise.
     */
    public boolean contains(double x, double y, double w, double h) {
        return GeomUtilities
            .contains(getPathIterator(null), x, y, x + w, y + h);
    }

    /**
     * Tests if the specified <code>Rectangle2D</code> is inside the boundary of
     * this <code>Shape</code>.
     * 
     * @param r
     *            a specified <code>Rectangle2D</code>
     * @return <code>true</code> if this <code>Shape</code> bounds the
     *         specified <code>Rectangle2D</code>; <code>false</code> otherwise.
     */
    public boolean contains(Rectangle2D r) {
        return contains(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }

    /**
     * Tests if the interior of this <code>Shape</code> intersects the
     * interior of a specified set of rectangular coordinates.
     * 
     * @param x
     *            ,&nbsp;y the specified coordinates
     * @param w
     *            the width of the specified rectangular coordinates
     * @param h
     *            the height of the specified rectangular coordinates
     * @return <code>true</code> if this <code>Shape</code> and the
     *         interior of the specified set of rectangular coordinates
     *         intersect
     *         each other; <code>false</code> otherwise.
     */
    public boolean intersects(double x, double y, double w, double h) {
        return GeomUtilities.intersects(getPathIterator(null), x, y, w, h);
    }

    /**
     * Tests if the interior of this <code>Shape</code> intersects the
     * interior of a specified <code>Rectangle2D</code>.
     * 
     * @param r
     *            the specified <code>Rectangle2D</code>
     * @return <code>true</code> if this <code>Shape</code> and the interior
     *         of the specified <code>Rectangle2D</code> intersect each
     *         other; <code>false</code> otherwise.
     */
    public boolean intersects(Rectangle2D r) {
        return intersects(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }

    /**
     * Returns a <code>PathIterator</code> object that iterates along the
     * boundary of this <code>Shape</code> and provides access to the
     * geometry of the outline of this <code>Shape</code>.
     * The iterator for this class is not multi-threaded safe,
     * which means that this <code>GeneralPathX</code> class does not
     * guarantee that modifications to the geometry of this
     * <code>GeneralPathX</code> object do not affect any iterations of
     * that geometry that are already in process.
     * 
     * @param at
     *            an <code>AffineTransform</code>
     * @return a new <code>PathIterator</code> that iterates along the
     *         boundary of this <code>Shape</code> and provides access to the
     *         geometry of this <code>Shape</code>'s outline
     */
    public PathIterator getPathIterator(AffineTransform at) {
        if (isSimple) {
            return new GeneralPathXIteratorSimple(this, at);
        } else {
            return new GeneralPathXIterator(this, at);
        }
    }

    /**
     * Returns a <code>PathIterator</code> object that iterates along the
     * boundary of the flattened <code>Shape</code> and provides access to the
     * geometry of the outline of the <code>Shape</code>.
     * The iterator for this class is not multi-threaded safe,
     * which means that this <code>GeneralPathX</code> class does not
     * guarantee that modifications to the geometry of this
     * <code>GeneralPathX</code> object do not affect any iterations of
     * that geometry that are already in process.
     * 
     * @param at
     *            an <code>AffineTransform</code>
     * @param flatness
     *            the maximum distance that the line segments used to
     *            approximate the curved segments are allowed to deviate
     *            from any point on the original curve
     * @return a new <code>PathIterator</code> that iterates along the flattened
     *         <code>Shape</code> boundary.
     */
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return new FlatteningPathIterator(getPathIterator(at), flatness);
    }

    /**
     * Creates a new object of the same class as this object.
     * 
     * @return a clone of this instance.
     * @exception OutOfMemoryError
     *                if there is not enough memory.
     * @see java.lang.Cloneable
     * @since 1.2
     */
    public Object clone() {
        DefaultGeneralPathX copy = new DefaultGeneralPathX();
        copy.windingRule = windingRule;
        copy.isSimple = isSimple;
        for (int i = 0; i < getNumTypes(); i++) {
            copy.pointTypes.add(pointTypes.get(i));
        }
        for (int i = 0; i < getNumCoords(); i++) {
            copy.addPoint((Point) getPointAt(i).cloneGeometry());
        }
        return copy;

    }

    DefaultGeneralPathX(int windingRule, byte[] pointTypes, int numTypes,
        double[] pointCoords, int numCoords) {

        // used to construct from native
        super(false);

        this.windingRule = windingRule;
        this.setPointTypes(pointTypes);
        this.setNumTypes(numTypes);
        this.setPointCoords(pointCoords);
        this.setNumCoords(numCoords);
    }

    public void setNumTypes(int numTypes) {

    }

    public int getNumTypes() {
        return pointTypes.size();
    }

    public int setNumCoords(int numCoords) {
        return pointCoords.size();
    }

    public int getNumCoords() {
        return pointCoords.size();
    }

    public byte getTypeAt(int index) {
        return ((Byte) pointTypes.get(index)).byteValue();
    }

    /**
     * @deprecated
     *             use the geometry methods.
     */
    public void setPointTypes(byte[] pointTypes) {
        this.pointTypes.clear();
        for (int i = 0; i < pointTypes.length; i++) {
            this.pointTypes.add(SEG_TYPES[pointTypes[i]]);
        }
    }

    /**
     * @deprecated
     *             use the geometry methods.
     */
    public byte[] getPointTypes() {
        byte[] bytes = new byte[pointTypes.size()];
        for (int i = 0; i < pointTypes.size(); i++) {
            bytes[i] = ((Byte) pointTypes.get(i)).byteValue();
        }
        return bytes;
    }

    /**
     * @param pointCoords
     * @deprecated
     *             use the geometry methods.
     */
    public void setPointCoords(double[] pointCoords) {
        this.pointCoords.clear();
        for (int i = 0; i < pointCoords.length; i = i + 2) {
            try {
                addPoint(geomManager.createPoint(pointCoords[i],
                    pointCoords[i + 1], Geometry.SUBTYPES.GEOM2D));
            } catch (CreateGeometryException e) {
                LOG.error("Error creating a point", e);
            }
        }
    }

    /**
     * @deprecated
     *             use the geometry methods.
     */
    public double[] getPointCoords() {
        double[] doubles = new double[pointCoords.size() * 2];
        for (int i = 0; i < getNumCoords(); i++) {
            doubles[i * 2] = getPointAt(i).getX();
            doubles[(i * 2) + 1] = getPointAt(i).getY();
        }
        return doubles;
    }

    public Point getPointAt(int index) {
        return (Point) pointCoords.get(index);
    }

    public double[] getCoordinatesAt(int index) {
        return getPointAt(index).getCoordinates();
    }
    
    public double[] get3DCoordinatesAt(int index) {
    	Point p = getPointAt(index);
    	if(p instanceof Point2DZ) {
    		return getPointAt(index).getCoordinates();
    	}
    	double[] coords = new double[3];
    	coords[0] = p.getX();
    	coords[1] = p.getY();
    	coords[2] = 0D;
    	return coords;
    }

    /**
     * Convertimos el path a puntos y luego le damos la vuelta.
     */
    public void flip() {
        PathIterator theIterator =
            getPathIterator(null, geomManager.getFlatness());
        double[] theData = new double[6];
        CoordinateList coordList = new CoordinateList();
        Coordinate c1;
        DefaultGeneralPathX newGp = new DefaultGeneralPathX();
        ArrayList listOfParts = new ArrayList();
        while (!theIterator.isDone()) {
            // while not done
            int type = theIterator.currentSegment(theData);
            switch (type) {
            case SEG_MOVETO:
                coordList = new CoordinateList();
                listOfParts.add(coordList);
                c1 = new Coordinate(theData[0], theData[1]);
                coordList.add(c1, true);
                break;
            case SEG_LINETO:
                c1 = new Coordinate(theData[0], theData[1]);
                coordList.add(c1, true);
                break;

            case SEG_CLOSE:
                coordList.add(coordList.getCoordinate(0));
                break;

            }
            theIterator.next();
        }

        for (int i = listOfParts.size() - 1; i >= 0; i--) {
            coordList = (CoordinateList) listOfParts.get(i);
            Coordinate[] coords = coordList.toCoordinateArray();
            CoordinateArraySequence seq = new CoordinateArraySequence(coords);
            CoordinateSequences.reverse(seq);
            coords = seq.toCoordinateArray();
            newGp.moveTo(coords[0].x, coords[0].y);
            for (int j = 1; j < coords.length; j++) {
                newGp.lineTo(coords[j].x, coords[j].y);
            }
        }
        reset();
        append(newGp.getPathIterator(null), false);
    }

    /**
     * Check if the first part is CCW.
     * 
     * @return
     */
    public boolean isCCW() {
        PathIterator theIterator =
            getPathIterator(null, geomManager.getFlatness()); // polyLine.getPathIterator(null,
                                                              // flatness);
        double[] theData = new double[6];
        Coordinate first = null;
        CoordinateList coordList = new CoordinateList();
        Coordinate c1;
        boolean bFirst = true;
        while (!theIterator.isDone()) {
            // while not done
            int type = theIterator.currentSegment(theData);
            switch (type) {
            case SEG_MOVETO:
                c1 = new Coordinate(theData[0], theData[1]);
                if (bFirst == false) // Ya tenemos la primera parte.
                    break;
                if (bFirst) {
                    bFirst = false;
                    first = c1;
                }
                coordList.add(c1, true);
                break;
            case SEG_LINETO:
                c1 = new Coordinate(theData[0], theData[1]);
                coordList.add(c1, true);
                break;

            }
            theIterator.next();
        }
        coordList.add(first, true);
        return CGAlgorithms.isCCW(coordList.toCoordinateArray());
    }

    /**
     * @return the isSimple
     */
    public boolean isSimple() {
        return isSimple;
    }
    
    public void ensureCapacity(int capacity) {
        
    }
}
