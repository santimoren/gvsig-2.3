/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.surface.polygon;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * Polilinea 3D.
 *
 * @author Vicente Caballero Navarro
 */
public class Polygon2DZ extends Polygon2D implements Polygon {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public Polygon2DZ(GeometryType geometryType) {
		super(geometryType);
	}

	/**
	 * Constructor used in the {@link Geometry#cloneGeometry()} method
	 * @param id
	 * @param projection
	 * @param gpx
	 * @param pZ
	 */
	Polygon2DZ(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx) {
		super(geometryType, id, projection, gpx);		
	}

	/**
	 * Clona FPolygon3D.
	 *
	 * @return FShape clonado.
	 */
	public FShape cloneFShape() {
		return new Polygon2DZ(getGeometryType(), id, projection, (GeneralPathX) gp.clone());
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.Surface2D#getShapeType()
	 */
	public int getShapeType() {
		return TYPES.SURFACE;
	}	

    public int getDimension() {
        return 3;
    }
}
