/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.point;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * 3D point implementation.
 * 
 * @author Vicente Caballero Navarro
 * @author gvSIG team
 */
public class Point2DZ extends Point2D implements Point {

	private static final long serialVersionUID = 3113070237182638858L;

	public static final String PERSISTENCE_DEFINITION_NAME = "Point2DimensionsZ";
	private double z = 0.0d;

	/**
	 * The constructor with the GeometryType like and argument is used by the
	 * {@link GeometryType}{@link #create()} to create the geometry
	 * 
	 * @param type
	 *            The geometry type
	 */
	public Point2DZ(GeometryType geomType) {
		super(geomType);
	}

	/**
	 * Constructor used in the {@link Geometry#cloneGeometry()} method
	 * 
	 * @param id
	 * @param projection
	 * @param x
	 * @param y
	 * @param z
	 */
	Point2DZ(GeometryType geomType, String id, IProjection projection,
			double x, double y, double z) {
		super(geomType, id, projection, x, y);
		this.z = z;
	}

    public Point2DZ(double x, double y, double z, GeometryType geometryType) {
        super(geometryType);
        this.x = x;
        this.y = y;
        this.z = z;
    }

	public Point2DZ(double x, double y, double z) {
		super(TYPES.POINT, SUBTYPES.GEOM3D);
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getShapeType() {
		return TYPES.POINT;
	}

	public FShape cloneFShape() {
		return new Point2DZ(getGeometryType(), id, projection, x, y, z);
	}

	public int getDimension() {
		return 3;
	}

	public boolean equals(Object other) {
		if (!super.equals(other)) {
			return false;
		}

		Point2DZ pother = (Point2DZ) other;
		if (Math.abs(this.z - pother.z) > 0.0000001) {
			return false;
		}
		return true;
	}

	public void setCoordinates(double[] values) {
		super.setCoordinates(values);
		if (values.length > 2) {
			z = values[2];
		}
	}

	public void setCoordinateAt(int dimension, double value) {
		if (dimension == Geometry.DIMENSIONS.Z) {
			this.z = value;
		} else {
			super.setCoordinateAt(dimension, value);
		}
	}

	public double getCoordinateAt(int dimension) {
		return (dimension == Geometry.DIMENSIONS.Z) ? this.z : super
				.getCoordinateAt(dimension);
	}

	public double getOrdinate(int dimension) {
		return (dimension == Geometry.DIMENSIONS.Z) ? this.z : super
				.getCoordinateAt(dimension);
	}

	public double[] getCoordinates() {
		return new double[] { getX(), getY(), this.z };
	}

	protected String getFullTypeName() {
		return "Point2DZ";
	}

}
