/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.curve;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.OrientableCurve;
import org.gvsig.fmap.geom.generalpath.primitive.OrientablePrimitive2D;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public abstract class OrientableCurve2D extends OrientablePrimitive2D implements OrientableCurve{
	private static final long serialVersionUID = 5985127974671632981L;

	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public OrientableCurve2D(GeometryType geometryType) {
		super(geometryType);		
	}
	
	public OrientableCurve2D(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx) {
		super(geometryType, id, projection, gpx);		
	}
}
