/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.io.Serializable;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.NullGeometry;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.generalpath.util.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.io.WKBWriter;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import com.vividsolutions.jts.operation.overlay.snap.GeometrySnapper;
import com.vividsolutions.jts.operation.valid.IsValidOp;
import com.vividsolutions.jts.operation.valid.TopologyValidationError;
import org.gvsig.fmap.geom.generalpath.DefaultValidationStatus;
import org.gvsig.fmap.geom.primitive.OrientableCurve;
import org.gvsig.fmap.geom.primitive.OrientableSurface;
import org.gvsig.fmap.geom.generalpath.gputils.GeneralPathAdapter;


/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public abstract class AbstractPrimitive implements Primitive, FShape, Shape, Serializable {
	
	public static final Logger logger = LoggerFactory.getLogger(AbstractPrimitive.class);

	private static final long serialVersionUID = -4334977368955260872L;
	protected String id = null;
	protected IProjection projection = null;
	protected GeometryType geometryType = null;
	protected static final GeometryManager geomManager = GeometryLocator.getGeometryManager();

	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public AbstractPrimitive(GeometryType geometryType) {
		this(geometryType, null, null);		
	}
	
	protected AbstractPrimitive(int type, int subtype) {
		try {
			geometryType = geomManager.getGeometryType(type, subtype);
		} catch (Exception e) {
			//TODO Not registered geometry
			geometryType = null;
		}
	}	
	
	public AbstractPrimitive(GeometryType geometryType, String id, IProjection projection) {
		super();
		this.id = id;
		this.projection = projection;
		this.geometryType = geometryType;
	}

	public AbstractPrimitive(GeometryType geometryType, IProjection projection) {
		this(geometryType, null, projection);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#getGeometryType()
	 */
	public GeometryType getGeometryType() {
		return geometryType;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#getType()
	 */
	public int getType() {
		return geometryType.getType();
	}

	/**
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.core.Geometry#getInternalShape()
	 * @deprecated this Geometry is a Shape.
	 */
	public Shape getInternalShape() {
		return this;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.geometries.iso.AbstractGeometry#getId()
	 */
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.geometries.iso.AbstractGeometry#getSRS()
	 */
	public IProjection getSRS() {
		return projection;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.geometries.iso.AbstractGeometry#transform(org.cresques.cts.IProjection)
	 */
	public AbstractPrimitive transform(IProjection newProjection) {
		Geometry newGeom = cloneGeometry();
		ICoordTrans coordTrans = projection.getCT(newProjection);
		newGeom.reProject(coordTrans);
		return (AbstractPrimitive)newGeom;
	}



	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#fastIntersects(double, double, double, double)
	 */
	public boolean fastIntersects(double x, double y, double w, double h) {

		boolean resp = true;
		
		try {
			resp = intersectsRectangle(this, x, y, w, h);
		} catch (GeometryOperationException e) {
			logger.error("While doing fastintersects: " + e.getMessage(), e);
		}
		
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#cloneGeometry()
	 */
	public Geometry cloneGeometry() {
		return (Geometry)cloneFShape();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#getHandlers(int)
	 */
	public Handler[] getHandlers(int type) {
		if (type==STRETCHINGHANDLER){
			return getStretchingHandlers();
		}else if (type==SELECTHANDLER){
			return getSelectHandlers();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#isSimple()
	 */
	public boolean isSimple() {
            com.vividsolutions.jts.geom.Geometry jtsgeom = this.toJTS();
            return jtsgeom.isSimple();
	}

        public boolean isValid() {
            try {
                com.vividsolutions.jts.geom.Geometry jtsgeom = this.toJTS();
                return jtsgeom.isValid();
            } catch(Exception ex) {
                return false;
            }
        }

        public ValidationStatus getValidationStatus() {
            DefaultValidationStatus status = new DefaultValidationStatus(
                    ValidationStatus.VALID,
                    null
            );
            com.vividsolutions.jts.geom.Geometry jtsgeom = null;
            try {
                jtsgeom = this.toJTS();
                IsValidOp validOp = new IsValidOp(jtsgeom);
                if( validOp != null ) {
                    status.setValidationError(validOp.getValidationError());
                }
            } catch(Exception ex) {
                status.setStatusCode(ValidationStatus.CURRUPTED);
                status.setMesage("The geometry is corrupted.");
                if( this instanceof OrientableSurface ) {
                    int vertices = ((OrientableSurface)this).getNumVertices();
                    if( vertices < 3) {
                        status.setStatusCode(ValidationStatus.TOO_FEW_POINTS);
                        status.setMesage(TopologyValidationError.errMsg[TopologyValidationError.TOO_FEW_POINTS]);
                    }
                } else if( this instanceof OrientableCurve ) {
                    int vertices = ((OrientableCurve)this).getNumVertices();
                    if( vertices < 2) {
                        status.setStatusCode(ValidationStatus.TOO_FEW_POINTS);
                        status.setMesage(TopologyValidationError.errMsg[TopologyValidationError.TOO_FEW_POINTS]);
                    }
                } 
            }
            return status;
        }
        
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#invokeOperation(int, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invokeOperation(int index, GeometryOperationContext ctx) throws GeometryOperationNotSupportedException, GeometryOperationException {
		return geomManager.invokeOperation(index, this, ctx);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.Geometry#invokeOperation(java.lang.String, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invokeOperation(String oppName, GeometryOperationContext ctx) throws GeometryOperationNotSupportedException, GeometryOperationException {
		return geomManager.invokeOperation(oppName, this, ctx);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(T)
	 */
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String name=getGeometryType().getName();
		return name.substring(name.lastIndexOf(".")+1);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}

		AbstractPrimitive other = (AbstractPrimitive) obj;
		if (this.getGeometryType().getType() != other.getGeometryType()
				.getType()) {
			return false;

		}
		
		if (this.getGeometryType().getSubType() != other.getGeometryType()
				.getSubType()) {
			return false;

		}
		
		if (this instanceof NullGeometry || obj instanceof NullGeometry) {
		    /*
		     * If any of them is null geometry, they both have to be
		     * null geometry, or else, they are not equal.
		     * This prevents null pointer exception in the rest of the
		     * method
		     */
	        if (this instanceof NullGeometry && obj instanceof NullGeometry) {
	            return true;
	        } else {
	            return false;
	        }
		}
		
		if (this.projection != other.projection) {
			if (this.projection == null) {
				return false;
			}
			if (this.projection.getAbrev() != other.projection.getAbrev()) { //FIXME this must be false
				return false;
			}
		}
		if (!this.getBounds().equals(other.getBounds())) {
			return false;
		}


		GeneralPathX myPath = this.getGeneralPath();
		GeneralPathX otherPath = other.getGeneralPath();
		if (myPath == null) {
			if (otherPath != null) {
				return false;
			} else {
				// TODO checkThis
				return true;
			}

		}
		if (myPath.getNumTypes() != otherPath.getNumTypes()) {
			return false;
		}
		if (Math.abs(myPath.getNumCoords() - otherPath.getNumCoords()) > this
				.getDimension()) {
			return false;
		}
		PathIterator myIter = myPath.getPathIterator(null);
		PathIterator otherIter = otherPath.getPathIterator(null);
		int myType,otherType;
		// FIXME when 3D, 2DM and 3DM
		double[] myData = new double[6];
		double[] otherData = new double[6];
		double[] myFirst = new double[] { myPath.getPointAt(0).getX(),myPath.getPointAt(0).getY()};
		double[] otherFirst = new double[] { otherPath.getPointAt(0).getX(),otherPath.getPointAt(0).getY()};

		while (!myIter.isDone()) {
			if (otherIter.isDone()) {
				return false;
			}
			for (int i = 0; i < myData.length; i++) {
				myData[i] = 0.0;
				otherData[i] = 0.0;
			}

			myType = myIter.currentSegment(myData);
			otherType = otherIter.currentSegment(otherData);

			switch (myType) {
			case PathIterator.SEG_LINETO:
				if (otherType != myType) {
					if (otherType == PathIterator.SEG_CLOSE){
						if (!comparePathIteratorData(otherFirst, myData)) {
							return false;
						}
					} else {
						return false;
					}
				} else {
					if (!comparePathIteratorData(myData, otherData)) {
						return false;
					}
				}
				break;


			case PathIterator.SEG_CLOSE:
				if (otherType != myType) {
					if (otherType == PathIterator.SEG_LINETO) {
						if (!comparePathIteratorData(myFirst, otherData)) {
							return false;
						}
					} else {
						return false;
					}
				} else {
					if (!comparePathIteratorData(myData, otherData)) {
						return false;
					}
				}
				break;



			case PathIterator.SEG_MOVETO:
			case PathIterator.SEG_QUADTO:
			case PathIterator.SEG_CUBICTO:
				if (otherType != myType) {
					return false;
				}
				if (!comparePathIteratorData(myData, otherData)) {
					return false;
				}
				break;

			} // end switch


			myIter.next();
			otherIter.next();
		}
		if (!otherIter.isDone()) {
			return false;
		}
		return true;
	}

	private boolean comparePathIteratorData(double[] org, double[] other) {
		for (int i = 0; i < org.length; i++) {
			if (Math.abs(org[i] - other[i]) > 0.0000001) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.FShape#getShapeType()
	 */
	public int getShapeType() {
		return getType();
	}
	
	
	
	
	
	
	
	
	/**
	 * Utility method
	 * @param geometry
	 * @param x
	 * @param y
	 * @return
	 * @throws GeometryOperationException
	 */
	protected boolean containsPoint(Geometry geom, double x, double y) throws GeometryOperationException {
		
		// exclude disjoint 
		Envelope env = geom.getEnvelope();
		if (x > env.getMaximum(0)) return false; 
		if (y > env.getMaximum(1)) return false; 
		if (x < env.getMinimum(0)) return false; 
		if (y < env.getMinimum(1)) return false; 
		
		// boxes overlap, need long version
		
		Geometry geompoint = null;
		try {
			geompoint = GeometryLocator.getGeometryManager().createPoint(x, y, SUBTYPES.GEOM2D);
		} catch (Exception e1) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_CONTAINS_CODE, e1);
		}
		
		GeometryOperationContext drgoc = new GeometryOperationContext();
		 drgoc.setAttribute("geom",geompoint);
		
		Object resp = null;
		boolean respboolean = true;
		try {
			resp = geom.invokeOperation(GeometryOperation.OPERATION_CONTAINS_CODE, drgoc);
			respboolean = ((Boolean) resp).booleanValue();
		} catch (Exception e) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_CONTAINS_CODE, e);
		}

		return respboolean;
	}

	/**
	 * 
	 * @param geometry
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @return
	 */
	protected boolean containsRectangle(Geometry geom, double x, double y,
			double w, double h) throws GeometryOperationException {
		
		
		// exclude disjoint boxes
		Envelope env = geom.getEnvelope();
		if (x > env.getMaximum(0)) return false; 
		if ((x+w) < env.getMinimum(0)) return false; 
		if (y > env.getMaximum(1)) return false; 
		if ((y+h) < env.getMinimum(1)) return false; 
		
		if (w == 0 && h == 0) {
			return  containsPoint(geom, x, y); 
		}
		
		// boxes overlap, need long version
		Geometry rectgeom = null;
		GeneralPathX gpx = new GeneralPathX();
		gpx.moveTo(x, y);
		gpx.lineTo(x+w, y);
		gpx.lineTo(x+w, y+h);
		gpx.lineTo(x, y+h);
		gpx.lineTo(x, y);
		
		try {
			rectgeom = GeometryLocator.getGeometryManager().createSurface(gpx, SUBTYPES.GEOM2D);
		} catch (Exception e1) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_CONTAINS_CODE, e1);
		}
		
		GeometryOperationContext drgoc = new GeometryOperationContext();
		drgoc.setAttribute("geom",rectgeom);

		Object resp = null;
		boolean respboolean = true;
		try {
			resp = geom.invokeOperation(GeometryOperation.OPERATION_CONTAINS_CODE, drgoc);
			respboolean = ((Boolean) resp).booleanValue();
		} catch (Exception e) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_CONTAINS_CODE, e);
		}

		return respboolean;
	}
	

	/**
	 * 
	 * @param geom
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @return
	 */
	protected boolean intersectsRectangle(Geometry geom, double x, double y,
			double w, double h) throws GeometryOperationException {
		
		// exclude disjoint boxes
		Envelope env = geom.getEnvelope();
		if (x > env.getMaximum(0)) return false; 
		if ((x+w) < env.getMinimum(0)) return false; 
		if (y > env.getMaximum(1)) return false; 
		if ((y+h) < env.getMinimum(1)) return false; 
		
		// boxes overlap, need long version
		Geometry rectgeom = null;
		GeneralPathX gpx = new GeneralPathX();
		gpx.moveTo(x, y);
		gpx.lineTo(x+w, y);
		gpx.lineTo(x+w, y+h);
		gpx.lineTo(x, y+h);
		gpx.lineTo(x, y);
		
		try {
			rectgeom = GeometryLocator.getGeometryManager().createSurface(gpx, SUBTYPES.GEOM2D);
		} catch (Exception e1) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_INTERSECTS_CODE, e1);
		}

		GeometryOperationContext drgoc = new GeometryOperationContext();
		drgoc.setAttribute("geom",rectgeom);
		
		Object resp = null;
		boolean respboolean = true;
		try {
			resp = geom.invokeOperation(GeometryOperation.OPERATION_INTERSECTS_CODE, drgoc);
			respboolean = ((Boolean) resp).booleanValue();
		} catch (Exception e) {
			throw new GeometryOperationException(geom.getType(), GeometryOperation.OPERATION_INTERSECTS_CODE, e);
		}

		return respboolean;


	}

	public com.vividsolutions.jts.geom.Geometry toJTS() {
		return Converter.geometryToJts(this);
	}
	
	public byte[] convertToWKB() throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		return (byte[]) this.invokeOperation(OPERATIONS.CONVERTTOWKB, null);
	}

	public byte[] convertToWKB(int srs) 
		throws GeometryOperationNotSupportedException, GeometryOperationException {
		int subType = getGeometryType().getSubType();
		boolean is3D = subType == 1 || subType == 3;
		
		WKBWriter write = null;
		if(is3D)
			write = new WKBWriter(3);
		else
			write = new WKBWriter(2);
		return write.write(Converter.geometryToJtsWithSRID(this, srs));
	}
	
	public byte[] convertToWKBForcingType(int srs, int type) 
		throws GeometryOperationNotSupportedException, GeometryOperationException {
		int subType = getGeometryType().getSubType();
		boolean is3D = subType == 1 || subType == 3;

		WKBWriter write = null;
		if(is3D)
			write = new WKBWriter(3);
		else
			write = new WKBWriter(2);
		return write.write(Converter.geometryToJtsWithSRIDForcingType(this, srs, type));
	}

	public String convertToWKT() throws GeometryOperationNotSupportedException,
			GeometryOperationException {
        	return (String) this.invokeOperation(OPERATIONS.CONVERTTOWKT, null);
	}

	public Geometry buffer(double distance)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		try {
			return Converter.jtsToGeometry( toJTS().buffer(distance) );
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
	}
	
	public boolean contains(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().contains(Converter.geometryToJts(geometry));
	}
	
	public double distance(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().distance( Converter.geometryToJts(geometry));
	}
	
	public Geometry snapTo(Geometry other, double snapTolerance)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		Geometry result = null;
		GeometrySnapper snapper = new GeometrySnapper(toJTS());
		com.vividsolutions.jts.geom.Geometry jts_result = snapper.snapTo(Converter.geometryToJts(other), snapTolerance);
		try {
			result = Converter.jtsToGeometry(jts_result);
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
		return result;
	}
	
	public boolean isWithinDistance(Geometry other, double distance)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		return DistanceOp.isWithinDistance(this.toJTS(), Converter.geometryToJts(other), distance);
	}
	
	
	public Geometry[] closestPoints(Geometry other)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		Geometry[] points = null;

		Coordinate[] jts_points = DistanceOp.closestPoints(this.toJTS(),
				Converter.geometryToJts(other));
		points = new Point[jts_points.length];
		GeometryFactory geomFactory = new GeometryFactory();
		for (int n = 0; n < jts_points.length; n++) {
			try {
				points[n] = Converter.jtsToGeometry(geomFactory.createPoint(jts_points[n]));
			} catch (CreateGeometryException e) {
				throw new GeometryOperationException(e);
			}
		}
		return points;
	}
	
	public boolean overlaps(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().overlaps(Converter.geometryToJts(geometry));
	}
	
	public Geometry convexHull() throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		try {
			return Converter.jtsToGeometry( toJTS().convexHull() );
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
	}
	
	public boolean coveredBy(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().coveredBy( Converter.geometryToJts(geometry));
	}
	
        public boolean covers(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().covers( Converter.geometryToJts(geometry));
	}
	
	public boolean crosses(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().crosses(Converter.geometryToJts(geometry));
	}
	
	public Geometry difference(Geometry other)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		try {
			return Converter.jtsToGeometry( toJTS().difference( Converter.geometryToJts(other)) );
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
	}
	
	public Geometry intersection(Geometry other)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		try {
			Geometry geom = null;
			com.vividsolutions.jts.geom.Geometry jtsgeom = toJTS().intersection(Converter.geometryToJts(other));
			if( jtsgeom == null ) {
				return null;
			}
			if( jtsgeom.isEmpty() ) {
				return null;
			}
			geom = Converter.jtsToGeometry( jtsgeom );
			return geom;
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
	}
	
	public boolean intersects(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
	    try {
	        return toJTS().intersects(Converter.geometryToJts(geometry));
	    } catch (Exception exc) {
	        /*
	         * The JTS library sometimes throws an exception
	         * (Example: "TopologyException: side location conflict")
	         */
	        throw new GeometryOperationException(exc);
	    }
		
	}
	
	public boolean touches(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().touches(Converter.geometryToJts(geometry));
	}
	
	public Geometry union(Geometry other)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		try {
			return Converter.jtsToGeometry( toJTS().union(Converter.geometryToJts(other)) );
		} catch (CreateGeometryException e) {
			throw new GeometryOperationException(e);
		}
	}
	
	public boolean disjoint(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().disjoint(Converter.geometryToJts(geometry));
	}
	
	public boolean within(Geometry geometry)
			throws GeometryOperationNotSupportedException,
			GeometryOperationException {
		// TODO: this method can be implemented throw invokeOperation 
		return toJTS().within(Converter.geometryToJts(geometry));
	}
	
	public Point centroid() throws GeometryOperationNotSupportedException, GeometryOperationException {
		com.vividsolutions.jts.geom.Geometry geometry = toJTS();
		com.vividsolutions.jts.geom.Point point = geometry.getCentroid();
		GeometryOperationContext geometryOperationContext = new GeometryOperationContext();
		geometryOperationContext.setAttribute("JTSGeometry", point);
		return (Point)this.invokeOperation("fromJTS", geometryOperationContext);		
	}
	
	   
	public Point getInteriorPoint() throws GeometryOperationNotSupportedException, GeometryOperationException {
	    
	    try {
	        com.vividsolutions.jts.geom.Geometry geometry = toJTS();
	        com.vividsolutions.jts.geom.Point point = geometry.getInteriorPoint();
	        GeometryOperationContext geometryOperationContext = new GeometryOperationContext();
	        geometryOperationContext.setAttribute("JTSGeometry", point);
	        return (Point)this.invokeOperation("fromJTS", geometryOperationContext);
        } catch (GeometryOperationNotSupportedException ns_ex) {
            throw ns_ex;
        } catch (GeometryOperationException op_ex) {
            throw op_ex;
	    } catch (Exception ex) {
	        /*
	         * This is for unexpected null pointer exceptions or other
	         */
	        throw new GeometryOperationException(ex);
	    }
	}   

	
	public double area() throws GeometryOperationNotSupportedException, GeometryOperationException
    {
        //Using get getJTS method instead of use the "toJTS" operation just for performance
        return toJTS().getArea();
    }
    
    public double perimeter() throws GeometryOperationNotSupportedException, GeometryOperationException
    {
      //Using get getJTS method instead of use the "toJTS" operation just for performance
        return toJTS().getLength();
    }

    public Shape getShape() {
        return this;
    }  
    
    public Shape getShape(AffineTransform affineTransform) {
        if (affineTransform == null){
            return this;
        }
        GeneralPathX gp = getGeneralPath();
        if( gp==null ) {
            return null;
        }
        return new GeneralPathAdapter(affineTransform, gp);
    }     
    
    public void rotate(double radAngle, double basex, double basey) {
        
        AffineTransform at = new AffineTransform();
        at.rotate(radAngle,basex,basey);
        this.transform(at);
    }
    
    public void move(double dx, double dy) {
        
        AffineTransform at = new AffineTransform();
        at.translate(dx, dy);
        this.transform(at);
    }
    
    public void scale(Point basePoint, double sx, double sy) {
        
        AffineTransform at = new AffineTransform();
        at.setToTranslation(basePoint.getX(),basePoint.getY());
        at.scale(sx,sy);
        at.translate(-basePoint.getX(),-basePoint.getY());
        this.transform(at);
    }

    public boolean isCCW() throws GeometryOperationNotSupportedException, GeometryOperationException {
        return this.getGeneralPath().isCCW();
    }

    public Geometry makeValid() {
        try {
            ValidationStatus vs = this.getValidationStatus();
            if (vs.isValid()) {
                return this;
            }
            Geometry g = null;
            switch (vs.getStatusCode()) {
                case Geometry.ValidationStatus.RING_SELF_INTERSECTION:
                case Geometry.ValidationStatus.SELF_INTERSECTION:
                    g = this.buffer(0);
                    if (g.isValid()) {
                        return g;
                    }
                    break;
                    
                case Geometry.ValidationStatus.TOO_FEW_POINTS:
                    if( this instanceof OrientableCurve ) {
                        int vertices = ((OrientableCurve)this).getNumVertices();
                        if( vertices<2 ) {
                            return new DefaultNullGeometry(this.getGeometryType());
                        }
                    }
                    if( this instanceof OrientableSurface ) {
                        int vertices = ((OrientableSurface)this).getNumVertices();
                        if( vertices<3 ) {
                            g = new DefaultNullGeometry(this.getGeometryType());
                            return g;
                        }
                    }
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

}


/*
Testing equality of Geometry objects
http://docs.geotools.org/stable/userguide/library/jts/equals.html

com.vividsolutions.jts.geom
Class Geometry
http://www.vividsolutions.com/jts/javadoc/com/vividsolutions/jts/geom/Geometry.html#normalize%28%29
*/