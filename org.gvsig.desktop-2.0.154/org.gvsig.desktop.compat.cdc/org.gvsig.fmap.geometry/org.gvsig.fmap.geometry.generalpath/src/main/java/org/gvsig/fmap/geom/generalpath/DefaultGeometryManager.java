/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.generalpath;

import org.gvsig.fmap.geom.generalpath.spatialindex.SpatialIndexFactoryJTS;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.SpatialIndexFactory;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.generalpath.spatialindex.SpatialIndexFactoryJSI;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.generalpath.gputils.DefaultGeneralPathX;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.IGeneralPathX;
import org.gvsig.fmap.geom.primitive.NullGeometry;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.PointGeometryType;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.generalpath.primitive.DefaultNullGeometry;
import org.gvsig.fmap.geom.generalpath.primitive.Envelope2D;
import org.gvsig.fmap.geom.generalpath.primitive.Envelope3D;
import org.gvsig.fmap.geom.generalpath.util.Converter;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ServiceFactory;

/**
 * Default implementation for the {@link GeometryManager}. When the
 * application starts, this class is registered in the {@link GeometryLocator}
 * using the {@link DefaultGeometryLibrary}.
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */

public class DefaultGeometryManager implements GeometryManager {

    private static final Logger logger = LoggerFactory
    .getLogger(GeometryManager.class);
    private double flatness = 0.8;

    private Map spatialIndexFactories = new HashMap();
    
    /**
     * This list holds the unique name of all registered geometry operations.
     * The index in which they are stored is also the operation code used to
     * invoke each one of them
     */
    private List geometryOperations = new ArrayList();

    /**
     * Common operations are registered here. Type specific operations are
     * registered in the corresponding GeometryType instance
     */
    // private List commonOperations = new ArrayList();

    /**
     * This map holds the instances of all registered GeometryType. The key is
     * the name of the specific Geometry subclass.
     * In other words, the string "org.gvsig.fmap.geom.primitive.Point2D" is the
     * hash key to obtain an instance of GeometryType holding the
     * operations associated to the class org.gvsig.fmap.geom.primitive.Point2D.
     */
    private Map geometryTypeName = new HashMap();

    /**
     * Matrix of geometry types by type (row) and subtype (column). This matrix
     * will contain null values in the cells where a GeometryType hasn't been
     * registered.
     */
    private GeometryType[][] geometryTypes;

    // Initially create a matrix of 17 x 6, which are the current default
    // types and subtypes. If another type or subtype is registered, the
    // matrix will grow as needed
    private static final int DEFAULT_TYPES_SIZE = 17;
    private static final int DEFAULT_SUBTYPES_SIZE = 6;

    public DefaultGeometryManager() throws GeometryException {
        this(DEFAULT_TYPES_SIZE, DEFAULT_SUBTYPES_SIZE);
    }

    public DefaultGeometryManager(int initialTypesSize, int initialSubtypesSize) throws GeometryException {
        geometryTypes = new GeometryType[initialTypesSize][initialSubtypesSize];
        this.addServiceFactory(new SpatialIndexFactoryJTS());
        this.addServiceFactory(new SpatialIndexFactoryJSI());
    }

    public int registerGeometryOperation(String geomOpName,
        GeometryOperation geomOp, GeometryType geomType) {
        if (geomOp == null) {
            throw new IllegalArgumentException("geomOp cannot be null.");
        }
        if (geomType == null) {
            throw new IllegalArgumentException("geomType cannot be null.");
        }

        int index = getGeometryOperationCode(geomOpName);

        geomType.setGeometryOperation(index, geomOp);

        return index;
    }

    public int registerGeometryOperation(String geomOpName,
        GeometryOperation geomOp) {
        if (geomOpName == null) {
            throw new IllegalArgumentException("geomOpName cannot be null.");
        }
        if (geomOp == null) {
            throw new IllegalArgumentException("geomOp cannot be null.");
        }

        int index = getGeometryOperationCode(geomOpName);

        Iterator it = geometryTypeName.values().iterator();
        while (it.hasNext()) {
            GeometryType geometryType = (GeometryType) it.next();
            registerGeometryOperation(geomOpName, geomOp, geometryType);
        }

        return index;

    }

    public int registerGeometryOperation(String geomOpName,
        GeometryOperation geomOp, int type, int subType)
    throws GeometryTypeNotSupportedException, GeometryTypeNotValidException {
        GeometryType geometryType = getGeometryType(type, subType);
        return registerGeometryOperation(geomOpName, geomOp, geometryType);
    }

    public int registerGeometryOperation(String geomOpName,
        GeometryOperation geomOp, int type) {
        int code = -1;
        Iterator it = geometryTypeName.values().iterator();
        while (it.hasNext()) {
            GeometryType geometryType = (GeometryType) it.next();
            if ((type == geometryType.getType())) {
                code =
                    registerGeometryOperation(geomOpName, geomOp, geometryType);
            }
        }
        return code;
    }

    public int registerGeometryOperationBySubtype(String geomOpName,
        GeometryOperation geomOp, int subType) {
        int code = -1;
        Iterator it = geometryTypeName.values().iterator();
        while (it.hasNext()) {
            GeometryType geometryType = (GeometryType) it.next();
            if ((subType == geometryType.getSubType())) {
                code =
                    registerGeometryOperation(geomOpName, geomOp, geometryType);
            }
        }
        return code;
    }    

    public int registerGeometryOperationBySuperType(String geomOpName,
        GeometryOperation geomOp, int superType) {       
        int code = -1;
        Iterator it = geometryTypeName.values().iterator();
        while (it.hasNext()) {
            GeometryType geometryType = (GeometryType) it.next();
            if (geometryType.isTypeOf(superType)) {
                code =
                    registerGeometryOperation(geomOpName, geomOp, geometryType);
            }
        }
        return code;
    }

    public int registerGeometryOperationBySuperSubType(String geomOpName,
        GeometryOperation geomOp, int superSubType) {       
        int code = -1;
        Iterator it = geometryTypeName.values().iterator();
        while (it.hasNext()) {
            GeometryType geometryType = (GeometryType) it.next();
            if (geometryType.isSubTypeOf(superSubType)) {
                code =
                    registerGeometryOperation(geomOpName, geomOp, geometryType);
            }
        }
        return code;
    }

    public GeometryType registerGeometryType(Class geomClass, String name,
        int type, int subType) {
        return registerGeometryType(geomClass, name, type, subType,
            new int[0], new int[0]);
    }    

    public GeometryType registerGeometryType(Class geomClass, String name,
        int type, int subType, int superType, int superSubType) {
        return registerGeometryType(geomClass, name, type, subType,
            new int[]{superType}, new int[]{superSubType});
    }

    public GeometryType registerGeometryType(Class geomClass, String name,
        int type, int subType, int superType) {
        return registerGeometryType(geomClass, name, type, subType,
            new int[]{superType}, new int[0]);
    }

    public GeometryType registerGeometryType(Class geomClass, String name,
        int type, int subType, int[] superTypes) {
        return registerGeometryType(geomClass, name, type, subType,
            superTypes, new int[0]);
    }

    /**
     * Registers a Geometry implementation class with a predefined geometry type
     * and returns the
     * associated GeometryType instance. Available predefined types are defined
     * in {@link Geometry.TYPES} If the class is already registered then this
     * method throws an IllegalArgumentException.<br>
     * 
     * How to register a geometry class with a predefined type:
     * 
     * <pre>
     * 
     * public class Point2D implements Point {
     *   private static final GeometryType geomType = GeometryManager.getInstance()
     *    .registerBasicGeometryType(Point2D.class, "Point2D", Geometry.TYPES.POINT);
     * 
     * ...
     *   public int getType() {
     *      return geomType.getType();
     *   }
     * }
     * </pre>
     * 
     * @param geomClass
     *            Geometry subclass. It must not be null and must implement
     *            Geometry, otherwise an exception
     *            is raised.
     * @param name
     *            Symbolic name for the geometry type, it can be null. If it is
     *            null then the symbolic name
     *            will be the simple class name.
     * @param id
     *            Geometry identifier.
     * @param type
     *            Type of geometry. Must be a value defined in
     *            {@link Geometry.TYPES}
     * @param subType
     *            SubType of geometry. Must be a value defined in
     *            {@link Geometry.SUBTYPES}
     * @return Instance of GeometryType associated to the Geometry
     *         implementation class
     *         geomClass
     * @throws IllegalArgumentException
     *             If geomClass is null or does not implement Geometry
     */
    public GeometryType registerGeometryType(Class geomClass, String name,
        int type, int subType, int[] superTypes, int superSubTypes[]) {
        if (geomClass == null) {
            throw new IllegalArgumentException("geomClass cannot be null.");
        }

        if (!Geometry.class.isAssignableFrom(geomClass)) {
            throw new IllegalArgumentException(geomClass.getName()
                + " must implement the Geometry interface");
        }

        // Check if it is registered
        GeometryType geomType = null;
        if (type >= geometryTypes.length || subType >= geometryTypes[0].length
            || (geomType = geometryTypes[type][subType]) == null) {
            geomType =
                new DefaultGeometryType(geomClass, name, type, subType,
                    superTypes, superSubTypes);
            registerGeometryType(geomType);
        }

        logger.debug("Class {} registered with name {}", geomClass,
            geomType.getName());

        return geomType;
    }

    public void registerGeometryType(GeometryType geometryType) {
        if (geometryType.getType() >= geometryTypes.length
            || geometryType.getSubType() >= geometryTypes[0].length) {

            // Recreate the geometry type matrix if the types
            // or subtypes don't fit
            int newTypesSize =
                geometryType.getType() < geometryTypes.length
                ? geometryTypes.length : geometryType.getType() + 1;
            int newSubTypesSize =
                geometryType.getSubType() < geometryTypes[0].length
                ? geometryTypes[0].length : geometryType.getSubType() + 1;
                GeometryType[][] newMatrix =
                    new GeometryType[newTypesSize][newSubTypesSize];

                for (int i = 0; i < geometryTypes.length; i++) {
                    System.arraycopy(geometryTypes[i], 0, newMatrix[i], 0,
                        geometryTypes[i].length);
                }
                geometryTypes = newMatrix;
        }

        geometryTypes[geometryType.getType()][geometryType.getSubType()] =
            geometryType;
        geometryTypeName.put(geometryType.getName(), geometryType);
    }

    public GeometryType registerGeometryType(Class geomClass, int type,
        int subType) {
        return registerGeometryType(geomClass, null, type, subType);
    }

    public GeometryType getGeometryType(int type, int subType)
    throws GeometryTypeNotSupportedException, GeometryTypeNotValidException {
        GeometryType gType = null;
        if (type >= geometryTypes.length || subType >= geometryTypes[0].length) {
            throw new GeometryTypeNotValidException(type, subType);
        }

        gType = geometryTypes[type][subType];

        if (gType == null) {
            throw new GeometryTypeNotSupportedException(type, subType);
        }

        return gType;
    }

    public Geometry create(GeometryType geomType)
    throws CreateGeometryException {
        return geomType.create();
    }

    public Geometry create(String name) throws CreateGeometryException {
        if (!geometryTypeName.containsKey(name)) {
            throw new IllegalArgumentException(name
                + " has not been registered yet.");
        }
        return ((GeometryType) geometryTypeName.get(name)).create();
    }

    public Geometry create(int type, int subType)
    throws CreateGeometryException {
        try {
            return getGeometryType(type, subType).create();
        } catch (GeometryException e) {
            throw new CreateGeometryException(type, subType, e);
        }
    }

    public Curve createCurve(GeneralPathX generalPathX, int subType)
    throws CreateGeometryException {
        Curve curve = (Curve) create(TYPES.CURVE, subType);
        curve.setGeneralPath(generalPathX);
        return curve;
    }

    public NullGeometry createNullGeometry(int subType)
    throws CreateGeometryException {
        NullGeometry nullGeom = (NullGeometry) create(TYPES.NULL, subType);
        return nullGeom;
    }

    public Point createPoint(double x, double y, int subType)
    throws CreateGeometryException {
        Point point = (Point) create(TYPES.POINT, subType);
        point.setX(x);
        point.setY(y);
        return point;
    }

    public Surface createSurface(GeneralPathX generalPathX, int subType)
    throws CreateGeometryException {
        Surface surface = (Surface) create(TYPES.SURFACE, subType);
        surface.setGeneralPath(generalPathX);
        return surface;
    }

    public GeometryOperation getGeometryOperation(int opCode, int type,
        int subType) throws GeometryTypeNotSupportedException,
        GeometryOperationNotSupportedException, GeometryTypeNotValidException {
        GeometryType geometryType = getGeometryType(type, subType);
        return geometryType.getGeometryOperation(opCode);
    }

    public GeometryOperation getGeometryOperation(int opCode)
        throws GeometryOperationNotSupportedException {
        if (opCode < 0) {
            throw new GeometryOperationNotSupportedException(opCode);
        }
        GeometryType type =
            (GeometryType) geometryTypeName.get(DefaultNullGeometry.class
                .getName());
        if (type == null) {
            throw new GeometryOperationNotSupportedException(opCode);
        }
        return type.getGeometryOperation(opCode);
    }

    public Object invokeOperation(int opCode, Geometry geom,
        GeometryOperationContext ctx)
    throws GeometryOperationNotSupportedException,
    GeometryOperationException {
        GeometryOperation geomOp =
            geom.getGeometryType().getGeometryOperation(opCode);

        if (geomOp != null) {
            return geomOp.invoke(geom, ctx);
        }

        throw new GeometryOperationNotSupportedException(opCode,
            geom.getGeometryType());
    }

    public Object invokeOperation(String geomOpName, Geometry geom,
        GeometryOperationContext ctx)
    throws GeometryOperationNotSupportedException,
    GeometryOperationException {
        int index = geometryOperations.indexOf(geomOpName);
        if (index == -1) {
            throw new GeometryOperationNotSupportedException(-1);
        }
        return invokeOperation(index, geom, ctx);
    }

    public Object invokeOperation(String geomOpName,
        GeometryOperationContext ctx)
        throws GeometryOperationNotSupportedException,
        GeometryOperationException {
        int index = geometryOperations.indexOf(geomOpName);
        GeometryOperation geomOp = getGeometryOperation(index);
        return geomOp.invoke(null, ctx);
    }

    public Envelope createEnvelope(int subType) {
        // TODO: register the envelopes!!!
        switch (subType) {
        case SUBTYPES.GEOM3D:
            return new Envelope3D();
        default:
            return new Envelope2D();
        }
    }

    public Envelope createEnvelope(double minX, double minY, double maxX,
        double maxY, int subType) throws CreateEnvelopeException {
        org.gvsig.fmap.geom.primitive.Point min = null;
        org.gvsig.fmap.geom.primitive.Point max = null;
        try {
            PointGeometryType gType = (PointGeometryType) getGeometryType(TYPES.POINT, subType);
            min = gType.createPoint(minX, minY);
            max = gType.createPoint(maxX, maxY);
        } catch (GeometryTypeNotSupportedException e) {
            throw new CreateEnvelopeException(subType, e);
        } catch (GeometryTypeNotValidException e) {
            throw new CreateEnvelopeException(subType, e);
        }
        switch (subType) {
        case SUBTYPES.GEOM2D:
        case SUBTYPES.GEOM2DM:
            // Small optimization to directly create an Envelope2D
            // return new Envelope2D(minX, minY, maxX, maxY);
            return new Envelope2D(min, max);
        default:
            Envelope envelope = createEnvelope(subType);
            envelope.setLowerCorner(min);
            envelope.setUpperCorner(max);
            return envelope;
        }
    }

    public MultiCurve createMultiCurve(GeneralPathX generalPathX, int subType)
    throws CreateGeometryException {
        if (subType != SUBTYPES.GEOM2D) {
            // FIXME Exception
            throw new UnsupportedOperationException();
        }
        MultiCurve multiCurve = (MultiCurve) create(TYPES.MULTICURVE, subType);
        PathIterator piter = generalPathX.getPathIterator(null);
        GeneralPathX tmpPath = null;
        Curve tmpCurve = null;
        double[] coords = new double[6];
        double[] first = new double[6];
        int type;
        while (!piter.isDone()) {
            type = piter.currentSegment(coords);
            switch (type) {
            case PathIterator.SEG_MOVETO:
                if (tmpPath != null) {
                    tmpCurve = createCurve(tmpPath, subType);
                    multiCurve.addCurve(tmpCurve);
                }
                System.arraycopy(coords, 0, first, 0, 2);
                tmpPath = new GeneralPathX(piter.getWindingRule());
                tmpPath.moveTo(coords[0], coords[1]);
                break;

            case PathIterator.SEG_LINETO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.lineTo(coords[0], coords[1]);
                break;

            case PathIterator.SEG_QUADTO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.quadTo(coords[0], coords[1], coords[2], coords[3]);
                break;

            case PathIterator.SEG_CUBICTO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.curveTo(coords[0], coords[1], coords[2], coords[3],
                    coords[4], coords[5]);
                break;

            case PathIterator.SEG_CLOSE:
                tmpPath.lineTo(first[0], first[1]);
                break;

            } // end switch

            piter.next();

        }
        if (tmpPath != null) {
            tmpCurve = createCurve(tmpPath, subType);
            multiCurve.addCurve(tmpCurve);
        }

        return multiCurve;

    }

    public MultiSurface createMultiSurface(GeneralPathX generalPathX,
        int subType) throws CreateGeometryException {
        if (subType != SUBTYPES.GEOM2D) {
            // FIXME Exception
            throw new UnsupportedOperationException();
        }
        MultiSurface multiSurface =
            (MultiSurface) create(TYPES.MULTISURFACE, subType);
        PathIterator piter = generalPathX.getPathIterator(null);
        GeneralPathX tmpPath = null;
        Surface tmpSurface = null;
        double[] coords = new double[6];
        double[] first = new double[6];
        int type;
        while (!piter.isDone()) {
            type = piter.currentSegment(coords);
            switch (type) {
            case PathIterator.SEG_MOVETO:
                if (tmpPath != null) {
                    tmpSurface = createSurface(tmpPath, subType);
                    multiSurface.addSurface(tmpSurface);
                }
                System.arraycopy(coords, 0, first, 0, 2);
                tmpPath = new GeneralPathX(piter.getWindingRule());
                tmpPath.moveTo(coords[0], coords[1]);

            case PathIterator.SEG_LINETO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.lineTo(coords[0], coords[1]);
                break;

            case PathIterator.SEG_QUADTO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.quadTo(coords[0], coords[1], coords[2], coords[3]);
                break;

            case PathIterator.SEG_CUBICTO:
                if (tmpPath == null) {
                    System.arraycopy(coords, 0, first, 0, 2);
                    tmpPath = new GeneralPathX(piter.getWindingRule());
                }
                tmpPath.curveTo(coords[0], coords[1], coords[2], coords[3],
                    coords[4], coords[5]);
                break;

            case PathIterator.SEG_CLOSE:
                tmpPath.lineTo(first[0], first[1]);
                break;
            } // end switch

            piter.next();

        }
        if (tmpPath != null) {
            tmpSurface = createSurface(tmpPath, subType);
            multiSurface.addSurface(tmpSurface);
        }

        return multiSurface;

    }

    public int getGeometryOperationCode(String geomOpName) {
        if (geomOpName == null) {
            throw new IllegalArgumentException("geomOpName cannot be null.");
        }

        int index = geometryOperations.indexOf(geomOpName);
        if (index == -1) {
            geometryOperations.add(geomOpName);
            index = geometryOperations.indexOf(geomOpName);
        }
        return index;
    }

    public List getGeometryOperationNames() {
        List operations = new ArrayList();
        for (int i = 0; i < operations.size(); i++) {
            operations.add(geometryOperations.get(i));
        }
        return operations;
    }

    public double getFlatness() {
        return flatness;
    }

    public void setFlatness(double flatness) {
        this.flatness = flatness;
    }

    public Geometry createFrom(String wkt, String srs) throws GeometryException {
        GeometryOperationContext context = new GeometryOperationContext();
        context.setAttribute("text", wkt);
        context.setAttribute("srs", srs);
        
        try {
            return (Geometry) this.invokeOperation(OPERATIONS.FROMWKT, context);
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryException(e);
        } catch (GeometryOperationException e) {
            throw new GeometryException(e);
        }
    }

    public Geometry createFrom(com.vividsolutions.jts.geom.Geometry geom) throws GeometryException {
        return Converter.jtsToGeometry(geom);
    }

    public Geometry createFrom(String wkt) throws GeometryException {
        GeometryOperationContext context = new GeometryOperationContext();
        context.setAttribute("text", wkt);
        context.setAttribute("srs", null);

        try {
            return (Geometry) this.invokeOperation(OPERATIONS.FROMWKT, context);
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryException(e);
        } catch (GeometryOperationException e) {
            throw new GeometryException(e);
        }
    }

    public Geometry createFrom(byte[] wkb) throws GeometryException {
        GeometryOperationContext context = new GeometryOperationContext();
        context.setAttribute("data", wkb);
        try {
            return (Geometry) this.invokeOperation(OPERATIONS.FROMWKB, context);
        } catch (GeometryOperationNotSupportedException e) {
            throw new GeometryException(e);
        } catch (GeometryOperationException e) {
            throw new GeometryException(e);
        }
    }
 
    public IGeneralPathX createGeneralPath(int rule, PathIterator pathIterator) {
		if( pathIterator == null ) {
			return new DefaultGeneralPathX(rule);
		}
		return  new DefaultGeneralPathX(pathIterator);
	}

        public MultiPoint createMultiPoint(int subType) throws CreateGeometryException {
            return (MultiPoint) create(TYPES.MULTIPOINT, subType);
        }

	public Line createLine(int subType) throws CreateGeometryException {
		return (Line) create(TYPES.CURVE, subType);
	}
	
	public Curve createCurve(int subType) throws CreateGeometryException {
		return (Curve) create(TYPES.CURVE, subType);
	}
	
	public MultiCurve createMultiCurve(int subType)
			throws CreateGeometryException {
		return (MultiCurve) create(TYPES.MULTICURVE, subType);
	}
	
	public MultiLine createMultiLine(int subType)
			throws CreateGeometryException {
		return (MultiLine) create(TYPES.MULTICURVE, subType);
	}
	
	public MultiSurface createMultiSurface(int subType)
			throws CreateGeometryException {
		return (MultiSurface) create(TYPES.MULTISURFACE, subType);
	}
	
	public MultiPolygon createMultiPolygon(int subType)
			throws CreateGeometryException {
		return (MultiPolygon) create(TYPES.MULTISURFACE, subType);
	}
	
	public Polygon createPolygon(int subType) throws CreateGeometryException {
		return (Polygon) create(TYPES.SURFACE, subType);
	}

	public Surface createSurface(int subType) throws CreateGeometryException {
		return (Surface) create(TYPES.SURFACE, subType);
	}

    public SpatialIndex createDefaultMemorySpatialIndex() throws ServiceException {
        return this.createSpatialIndex(SpatialIndexFactoryJTS.NAME,null);
    }

    public SpatialIndex createSpatialIndex(String name, DynObject parameters) throws ServiceException {
        SpatialIndexFactory factory = this.getSpatialIndexFactory(name);
        if (factory == null) {
            throw new CantExistsService(name);
        }
        return (SpatialIndex) factory.create(parameters, this);
    }

    public SpatialIndexFactory getSpatialIndexFactory(String name) {
        return (SpatialIndexFactory) this.spatialIndexFactories.get(name);
    }

    public void addServiceFactory(ServiceFactory serviceFactory) {
        serviceFactory.initialize();
        this.spatialIndexFactories.put(serviceFactory.getName(), serviceFactory);
    }

    public Service createService(DynObject serviceParameters) throws ServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public DynObject createServiceParameters(String serviceName) throws ServiceException {
        SpatialIndexFactory factory = this.getSpatialIndexFactory(serviceName);
        if( factory == null ) {
            throw  new CantExistsService(serviceName);
        }
        return factory.createParameters();
    }

    public Service getService(DynObject parameters) throws ServiceException {
        return this.createSpatialIndex((String) parameters.getDynValue("serviceName"), parameters);
    }

    public class CantExistsService extends ServiceException {

        public CantExistsService(String serviceName) {
            super("Can't existe service %(service).", "_Cant_existe_service_XserviceX", 100001);
            setValue("service",serviceName);
        }
        
    }
}
