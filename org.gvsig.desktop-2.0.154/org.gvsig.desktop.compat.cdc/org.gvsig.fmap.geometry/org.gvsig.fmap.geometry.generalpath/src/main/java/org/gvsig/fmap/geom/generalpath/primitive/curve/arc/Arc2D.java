/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.curve.arc;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.handler.AbstractHandler;
import org.gvsig.fmap.geom.handler.CenterHandler;
import org.gvsig.fmap.geom.handler.FinalHandler;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Arc;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.generalpath.primitive.curve.DefaultCurve;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.generalpath.util.UtilFunctions;



/**
 * DOCUMENT ME!
 *
 * @author Vicente Caballero Navarro
 */
public class Arc2D extends DefaultCurve implements Arc {
	private static final long serialVersionUID = 6416027005106924030L;
	
	private static final Logger logger = LoggerFactory.getLogger(Arc2D.class);

	private Point2D init;
	
	/**
	 * This is the middle point (belongs to the arc), not the center
	 * of the circle/ellipse
	 */
	private Point2D middle;
	private Point2D end;
	
	private static final GeometryManager geomManager =
			GeometryLocator.getGeometryManager();
		
	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public Arc2D(GeometryType geometryType) {
		super(geometryType);		
	}
	
	/**
	 * Constructor used in the {@link Geometry#cloneGeometry()} method
	 * @param id
	 * @param projection
	 * @param gpx
	 * @param i
	 * @param c
	 * @param e
	 */
	protected Arc2D(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx, Point2D i,Point2D c, Point2D e) {
		super(geometryType, id, projection, gpx);
		init=i;
		middle=c;
		end=e;
	}	
	
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.core.FShape#cloneFShape()
	 */
	public FShape cloneFShape() {
		Arc2D arc = new Arc2D(getGeometryType(), id, projection, (GeneralPathX) gp.clone(),init,middle,end);
		return arc;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.OrientablePrimitive2D#transform(java.awt.geom.AffineTransform)
	 */
	public void transform(AffineTransform at) {
	    
	    if (at == null) {
	        return;
	    }
	    
		gp.transform(at);
		InitHandler inithandler=(InitHandler)getStretchingHandlers()[0];
		//CenterHandler centerhandler=(CenterHandler)getHandlers()[1];
		EndHandler endhandler=(EndHandler)getStretchingHandlers()[1];
		Point2D aux1=new Point2D.Double();
		Point2D aux2=new Point2D.Double();
		Point2D aux3=new Point2D.Double();
		at.transform(inithandler.getPoint(),aux1);
		inithandler.setPoint(aux1);
		//at.transform(centerhandler.getPoint(),aux2);
		//centerhandler.setPoint(aux2);
		at.transform(endhandler.getPoint(),aux3);
		endhandler.setPoint(aux3);
		CenterSelHandler centerhandler=(CenterSelHandler)getSelectHandlers()[1];
		at.transform(centerhandler.getPoint(),aux2);
		centerhandler.setPoint(aux2);

	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.Curve2D#getShapeType()
	 */
	public int getShapeType() {
		return TYPES.ARC;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.OrientablePrimitive2D#getStretchingHandlers()
	 */
	public Handler[] getStretchingHandlers() {
		ArrayList handlers = new ArrayList();

		handlers.add(new InitHandler(0, init.getX(), init.getY()));
		//handlers.add(new CenterHandler(1, center.getX(), center.getY()));
		handlers.add(new EndHandler(1, end.getX(), end.getY()));

		return (Handler[]) handlers.toArray(new Handler[0]);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.impl.OrientablePrimitive2D#getSelectHandlers()
	 */	
	public Handler[] getSelectHandlers() {
		ArrayList handlers = new ArrayList();

		handlers.add(new InitSelHandler(0, init.getX(), init.getY()));
		handlers.add(new CenterSelHandler(1, middle.getX(), middle.getY()));
		handlers.add(new EndSelHandler(2, end.getX(), end.getY()));

		return (Handler[]) handlers.toArray(new Handler[0]);
	}
	/**
	 * DOCUMENT ME!
	 *
	 * @author Vicente Caballero Navarro
	 */	
	class CenterSelHandler extends AbstractHandler implements CenterHandler{
		/**
		 * Crea un nuevo PointHandler.
		 *
		 * @param i DOCUMENT ME!
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 */
		public CenterSelHandler(int i, double x, double y) {
		    middle = new Point2D.Double(x, y);
			index = i;
		}

		/**
		 * DOCUMENT ME!
		 *
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 *
		 * @return DOCUMENT ME!
		 */
		public void move(double x, double y) {
		}
		public void setPoint(Point2D p){
		    middle=p;
		}
		public Point2D getPoint(){
			return middle;
		}

		/**
		 * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
		 */
		public void set(double x, double y) {
		    middle=new Point2D.Double(x,y);
			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init, middle, end);
			gp = new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @author Vicente Caballero Navarro
	 */
	class InitHandler extends AbstractHandler implements FinalHandler{
		/**
		 * Crea un nuevo PointHandler.
		 *
		 * @param i DOCUMENT ME!
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 */
		public InitHandler(int i, double x, double y) {
			init = new Point2D.Double(x, y);
			index = i;
		}

		/**
		 * DOCUMENT ME!
		 *
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 *
		 * @return DOCUMENT ME!
		 */
		public void move(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			init=new Point2D.Double(init.getX()+x,init.getY()+y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			middle=UtilFunctions.getPoint(mediop,perp[1],dist);

			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
		public void setPoint(Point2D p){
			init=p;
		}
		public Point2D getPoint(){
			return init;
		}

		/**
		 * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
		 */
		public void set(double x, double y) {
			}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @author Vicente Caballero Navarro
	 */
	class EndHandler extends AbstractHandler implements FinalHandler{
		/**
		 * Crea un nuevo PointHandler.
		 *
		 * @param i DOCUMENT ME!
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 */
		public EndHandler(int i, double x, double y) {
			end = new Point2D.Double(x, y);
			index = i;
		}

		/**
		 * DOCUMENT ME!
		 *
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 *
		 * @return DOCUMENT ME!
		 */
		public void move(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			end=new Point2D.Double(end.getX()+x,end.getY()+y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			middle=UtilFunctions.getPoint(mediop,perp[1],dist);

			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
		public void setPoint(Point2D p){
			end=p;
		}
		public Point2D getPoint(){
			return end;
		}

		/**
		 * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
		 */
		public void set(double x, double y) {

		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @author Vicente Caballero Navarro
	 */
	class InitSelHandler extends AbstractHandler implements FinalHandler{
		/**
		 * Crea un nuevo PointHandler.
		 *
		 * @param i DOCUMENT ME!
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 */
		public InitSelHandler(int i, double x, double y) {
			init = new Point2D.Double(x, y);
			index = i;
		}

		/**
		 * DOCUMENT ME!
		 *
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 *
		 * @return DOCUMENT ME!
		 */
		public void move(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			init=new Point2D.Double(init.getX()+x,init.getY()+y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			middle=UtilFunctions.getPoint(mediop,perp[1],dist);

			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
		public void setPoint(Point2D p){
			init=p;
		}
		public Point2D getPoint(){
			return init;
		}

		/**
		 * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
		 */
		public void set(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			init=new Point2D.Double(x,y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			// TODO comentado para quitar warning: Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			///center=TrigonometricalFunctions.getPoint(mediop,perp[1],dist);
			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @author Vicente Caballero Navarro
	 */
	class EndSelHandler extends AbstractHandler implements FinalHandler{
		/**
		 * Crea un nuevo PointHandler.
		 *
		 * @param i DOCUMENT ME!
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 */
		public EndSelHandler(int i, double x, double y) {
			end = new Point2D.Double(x, y);
			index = i;
		}

		/**
		 * DOCUMENT ME!
		 *
		 * @param x DOCUMENT ME!
		 * @param y DOCUMENT ME!
		 *
		 * @return DOCUMENT ME!
		 */
		public void move(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			end=new Point2D.Double(end.getX()+x,end.getY()+y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			middle=UtilFunctions.getPoint(mediop,perp[1],dist);

			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
		public void setPoint(Point2D p){
			end=p;
		}
		public Point2D getPoint(){
			return end;
		}

		/**
		 * @see org.gvsig.fmap.geom.handler.Handler#set(double, double)
		 */
		public void set(double x, double y) {
			Point2D mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			double dist=mediop.distance(middle);
			end=new Point2D.Double(x,y);

			mediop=new Point2D.Double((init.getX()+end.getX())/2,(init.getY()+end.getY())/2);
			// TODO comentado para quitar warning: Point2D[] perp=UtilFunctions.getPerpendicular(init,end,mediop);
			if (UtilFunctions.getAngle(end,init)<=Math.PI){
				dist=-dist;
			}
			///center=TrigonometricalFunctions.getPoint(mediop,perp[1],dist);
			java.awt.geom.Arc2D arco = UtilFunctions.createArc(init,middle, end);
			gp=new GeneralPathX(arco.getPathIterator(null,
					geomManager.getFlatness()));
		}
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.core.FPolyline2D#intersects(java.awt.geom.Rectangle2D)
	 */
	public boolean intersects(Rectangle2D r) {
		return gp.intersects(r);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
	 */
	public void setPoints(Point p1, Point p2, Point p3) {
		Point2D _p1 = new java.awt.geom.Point2D.Double(p1.getCoordinateAt(0), p1.getCoordinateAt(1));
		Point2D _p2 = new java.awt.geom.Point2D.Double(p2.getCoordinateAt(0), p2.getCoordinateAt(1));
		Point2D _p3 = new java.awt.geom.Point2D.Double(p3.getCoordinateAt(0), p3.getCoordinateAt(1));
		setPoints(_p1, _p2, _p3);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Curve2D#setGeneralPath(org.gvsig.fmap.geom.primitive.GeneralPathX)
	 */
	public void setGeneralPath(GeneralPathX generalPathX) {
		throw new UnsupportedOperationException("Use setPoints(Point p1, Point p2, Point p3)");
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(java.awt.geom.Point2D, java.awt.geom.Point2D, java.awt.geom.Point2D)
	 */
	private void setPoints(Point2D p1, Point2D p2, Point2D p3) {
		java.awt.geom.Arc2D arco = UtilFunctions.createArc(p1, p2, p3);
		if (arco == null) {
            logger.info("Did not set arc points (probably aligned points): "
                + p1.getX() + " " + p1.getY() + " :: "
                + p2.getX() + " " + p2.getY() + " :: "
                + p3.getX() + " " + p3.getY());
		    throw new IllegalArgumentException("Did not set arc points (probably aligned points).");
		}
		
		this.gp = new GeneralPathX(	arco.getPathIterator(null,
				geomManager.getFlatness()));
		this.init = p1;
        this.middle = p2;
		this.end = p3;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(org.gvsig.fmap.geom.primitive.Point, double, double, double)
	 */
	public void setPoints(Point center, double radius, double initAngle,
			double angleExt) {
		Point2D _center = new java.awt.geom.Point2D.Double(center.getCoordinateAt(0), center.getCoordinateAt(1));
		setPoints(_center, radius, initAngle, angleExt);
	}
	
    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPointsStartExt(org.gvsig.fmap.geom.primitive.Point, double, double, double)
     */
    public void setPointsStartExt(Point center, double radius,
        double startAngle, double angleExt) {
        setPoints(center, radius, startAngle, angleExt);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Arc#setPointsStartEnd(org.gvsig.fmap.geom.primitive.Point, double, double, double)
     */
    public void setPointsStartEnd(Point center, double radius,
        double startAngle, double endAngle) {
        
        if (startAngle == endAngle) {
            setPointsStartExt(center, radius, startAngle, 0);
        } else {
            
            /*
             * Normalize then force clockwise:
             */
            double norm_start = normalizeAngle(startAngle);
            double norm_end = normalizeAngle(endAngle);
            double ang_ext = 0;

            // clockwise
            // ang_ext must be positive
            if (norm_start >= norm_end) {
                ang_ext = norm_start - norm_end;
            } else {
                ang_ext = 2 * Math.PI - (norm_end - norm_start);
            }

            // finally call other method with ang_ext
            setPointsStartExt(center, radius, startAngle, ang_ext);
        }
    }

	/**
	 * Leaves the angle between PI and -PI
     * @param angle (radians)
     * @return
     */
    private double normalizeAngle(double angle) {
        
        if (angle > -Math.PI && angle <= Math.PI) {
            return angle;
        }
        
        if (angle == Double.NEGATIVE_INFINITY
            || angle == Double.POSITIVE_INFINITY) {
            return 0;
        }
        
        double abs_ang = Math.abs(angle);
        double remove = Math.floor(abs_ang / (2 * Math.PI));
        remove = remove * 2 * Math.PI;
        double resp = 0;
        
        if (angle > 0) {
            resp = angle - remove;
            if (resp > Math.PI) {
                // final adjustment
                resp = resp - 2 * Math.PI;
            }
        } else {
            resp = angle + remove;
            if (resp <= -Math.PI) {
                // final adjustment
                resp = resp + 2 * Math.PI;
            }
        }
        
        return resp;
    }

    /* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#setPoints(java.awt.geom.Point2D, double, double, double)
	 */
	private void setPoints(Point2D center, double radius, double initAngle,
			double angleExt) {
            
            double x;
            double y;
            double w;
            double h;
            double start;
            double extent;
            double angleOffset;
            final double pi2 = Math.PI*2;
            
            angleOffset = 0;
            if( angleExt<0 ) {
                angleOffset = pi2 + angleExt;
                angleExt = Math.abs(angleExt);
            }            
            x = center.getX() - radius;
            y = center.getY() - radius;
            w = radius * 2;
            h = w; // Son siempre arcos de circunferencia
            
            if( angleExt > 0 && (angleExt % pi2) == 0  ) {
                start = 0;
                extent = 360;
            } else {
                angleExt = angleExt % pi2; //Asumimos que aqui angleExt es siempre positivo
                initAngle = Math.abs(initAngle) % pi2;
                start = Math.toDegrees( pi2 - initAngle + angleOffset) ;
                extent = - Math.toDegrees(pi2 - angleExt);
            }
            java.awt.geom.Arc2D.Double arco = new java.awt.geom.Arc2D.Double(
                    x,y,w,h,start,extent,java.awt.geom.Arc2D.OPEN);
            
            this.gp = new GeneralPathX(	arco.getPathIterator(null,geomManager.getFlatness()));

            this.init = arco.getStartPoint();
            this.end = arco.getEndPoint();

            java.awt.geom.Arc2D.Double semiarco = new java.awt.geom.Arc2D.Double(
                    x,y,w,h,start,extent/2,java.awt.geom.Arc2D.OPEN);
            this.middle = semiarco.getEndPoint();
	}
	


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Curve2D#setPoints(org.gvsig.fmap.geom.primitive.Point, org.gvsig.fmap.geom.primitive.Point)
	 */
	public void setPoints(Point startPoint, Point endPoint) {
		throw new UnsupportedOperationException("Use setGeneralPathX");
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#getEndPoint()
	 */
	public Point getEndPoint() {
		return new org.gvsig.fmap.geom.generalpath.primitive.point.Point2D(end.getX(), end.getY());
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#getInitPoint()
	 */
	public Point getInitPoint() {
		return new org.gvsig.fmap.geom.generalpath.primitive.point.Point2D(init.getX(), init.getY());
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.primitive.Arc#getCenterPoint()
	 */
	public Point getCenterPoint() {
		Point2D p = UtilFunctions.getCenter(init, middle,end);
		try {
			return new org.gvsig.fmap.geom.generalpath.primitive.point.Point2D(p.getX(), p.getY());
		} catch (Exception e) {
			return  null;
		}
	}



		
}
