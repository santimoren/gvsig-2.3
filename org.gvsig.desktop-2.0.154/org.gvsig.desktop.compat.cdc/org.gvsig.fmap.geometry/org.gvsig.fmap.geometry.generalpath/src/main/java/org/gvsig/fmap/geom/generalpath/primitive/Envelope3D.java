/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive;

import org.gvsig.fmap.geom.generalpath.primitive.point.Point2DZ;
import org.cresques.cts.ICoordTrans;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.EnvelopeNotInitializedException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.PersistenceManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class Envelope3D extends DefaultEnvelope implements Cloneable {

    public static final String PERSISTENCE_DEFINITION_NAME = "Envelope3Dimensions";
    private static final int DIMENSION = 3;
    private boolean isZInitilized = false;

    public Envelope3D() {
        super();
    }

    public Envelope3D(Point min, Point max) {
        super(min, max);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Envelope#getDimension()
     */
    public int getDimension() {
        return DIMENSION;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Envelope#convert(org.cresques.cts.ICoordTrans)
     */
    public Envelope convert(ICoordTrans trans) {

        if (isEmpty) {
            throw new EnvelopeNotInitializedException();
        }

        if (trans == null) {
            // clone
            return new Envelope2D(this.getLowerCorner(), this.getUpperCorner());
        }

//        if (this.getDimension() > 2) {
//            return null;
//        }

	    // We'll reproject by taking samples like this:
        // 
        //  *---*---*---*---*
        //  |               |
        //  *   *   *   *   *
        //  |               |
        //  *   *   *   *   *
        //  |               |
        //  *   *   *   *   *
        //  |               |
        //  *---*---*---*---*
        // 
        // This is because:
        // 
        // - In some CRS (for example EPSG:4326) the north/south pole is a "line"
        //   while in other CRS the north/south pole is a point, so if you
        //   reproject the bounding box of the world, the result can be absurd.
        // - Sometimes the top/bottom/right/bottom of one envelope
        //   corresponds to a strange point in the the other envelope
        //   (not even a point in the perimeter)
        // - More generally, reprojecting usually implies a rotation (the result
        //   is a rotated envelope) so it's better to use a few
        //   samples along the perimeter.
        double xmin = getMinimum(0);
        double ymin = getMinimum(1);
        double step_w = 0.25 * (getMaximum(0) - xmin);
        double step_h = 0.25 * (getMaximum(1) - ymin);

        java.awt.geom.Point2D sample = null;
        java.awt.geom.Point2D sample_trans = null;
        // Init with worst values
        java.awt.geom.Point2D res_min = new java.awt.geom.Point2D.Double(
                Double.MAX_VALUE, Double.MAX_VALUE);
        java.awt.geom.Point2D res_max = new java.awt.geom.Point2D.Double(
                -Double.MAX_VALUE, -Double.MAX_VALUE);

        int added = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                sample = new java.awt.geom.Point2D.Double(
                        xmin + i * step_w,
                        ymin + j * step_h);
                sample_trans = new java.awt.geom.Point2D.Double(0, 0);
                try {
                    sample_trans = trans.convert(sample, sample_trans);
                } catch (Exception exc) {
                    // Unable to convert this one: ignore
                    continue;
                }
	                // Update max/min found
                // X
                if (sample_trans.getX() > res_max.getX()) {
                    res_max.setLocation(sample_trans.getX(), res_max.getY());
                }
                if (sample_trans.getX() < res_min.getX()) {
                    res_min.setLocation(sample_trans.getX(), res_min.getY());
                }
                // Y
                if (sample_trans.getY() > res_max.getY()) {
                    res_max.setLocation(res_max.getX(), sample_trans.getY());
                }
                if (sample_trans.getY() < res_min.getY()) {
                    res_min.setLocation(res_min.getX(), sample_trans.getY());
                }
                added++;
            }
        }

        if (added == 0) {
            //logger.error("Unable to reproject envelope with transf: " + trans.toString());
            return null;
        }

        return new Envelope3D(
                new Point2DZ(res_min.getX(), res_min.getX(), this.min.getCoordinateAt(2)),
                new Point2DZ(res_max.getX(), res_max.getX(), this.max.getCoordinateAt(2))
        );

    }

    public static void registerPersistent() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if (manager.getDefinition(PERSISTENCE_DEFINITION_NAME) == null) {
            DynStruct definition = manager.addDefinition(
                    Envelope3D.class,
                    PERSISTENCE_DEFINITION_NAME,
                    "Envelope3D persistence definition",
                    null,
                    null
            );

            definition.extend(manager.getDefinition(DefaultEnvelope.PERSISTENCE_DEFINITION_NAME));
        }
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private void createPoints() {
        this.min = new Point2DZ(0, 0, 0);
        this.max = new Point2DZ(0, 0, 0);
    }

    public void add(Envelope envelope) {
        int i;

        if (envelope == null || envelope.isEmpty()) {
            return;
        }

        int maxDimension = DIMENSION;

        if (envelope.getDimension() == 2) {
            maxDimension = 2;
        }

        if (this.isZInitilized) {
            for (i = 0; i < maxDimension; i++) {
                this.min.setCoordinateAt(i,
                        Math.min(this.min.getCoordinateAt(i), envelope.getMinimum(i)));
                this.max.setCoordinateAt(i,
                        Math.max(this.max.getCoordinateAt(i), envelope.getMaximum(i)));
            }
            return;
        }

        if (isEmpty) {
            createPoints();
            if (maxDimension == 3) {
                this.isZInitilized = true;
            }
            for (i = 0; i < maxDimension; i++) {
                this.min.setCoordinateAt(i, envelope.getMinimum(i));
                this.max.setCoordinateAt(i, envelope.getMaximum(i));
            }
            isEmpty = false;
        } else {
            if (maxDimension == DIMENSION) {
                this.min.setCoordinateAt(2, envelope.getMinimum(2));
                this.max.setCoordinateAt(2, envelope.getMaximum(2));
                this.isZInitilized = true;
            }
            for (i = 0; i < maxDimension; i++) {
                this.min.setCoordinateAt(i,
                        Math.min(this.min.getCoordinateAt(i), envelope.getMinimum(i)));
                this.max.setCoordinateAt(i,
                        Math.max(this.max.getCoordinateAt(i), envelope.getMaximum(i)));
            }
        }
    }
}
