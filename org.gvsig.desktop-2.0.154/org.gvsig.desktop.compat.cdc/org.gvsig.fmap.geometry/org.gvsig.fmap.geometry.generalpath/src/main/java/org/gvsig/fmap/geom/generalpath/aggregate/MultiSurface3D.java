/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.aggregate;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class MultiSurface3D extends MultiSurface2D {
    private static final long serialVersionUID = 3088528632507696618L;

    public MultiSurface3D(GeometryType geomType) {
        super(geomType);       
    }
    
    public MultiSurface3D(GeometryType geomType, String id,
        IProjection projection) {
        super(geomType, id, projection);       
    }

    public Geometry cloneGeometry() {
        MultiSurface3D auxSurface = new MultiSurface3D(geometryType, id, projection);
        for (int i = 0; i < getPrimitivesNumber(); i++) {
            auxSurface.addSurface((Surface)((Surface) geometries.get(i)).cloneGeometry());
        }
        return auxSurface;
    }
    
    public int getDimension() {
        return 3;
    }   
}
