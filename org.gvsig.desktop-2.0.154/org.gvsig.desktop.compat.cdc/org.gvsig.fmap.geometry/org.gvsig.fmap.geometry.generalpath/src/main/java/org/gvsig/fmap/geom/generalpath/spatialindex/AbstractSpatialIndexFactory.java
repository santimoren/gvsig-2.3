/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.fmap.geom.generalpath.spatialindex;

import org.gvsig.fmap.geom.SpatialIndexFactory;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;


public abstract class AbstractSpatialIndexFactory implements SpatialIndexFactory {

    protected DynStruct parametersDefinition = null;
    
    protected DynStruct getBaseParametersDefinition() {
        DynObjectManager manager = ToolsLocator.getDynObjectManager();
        DynStruct struct = manager.createDynClass("SpatialIndexParameters_"+this.getName(), "Spatial index creation parameters for '"+this.getName()+"'.");
        struct.addDynFieldString("name")
                .setMandatory(true)
                .setReadOnly(true)
                .setHidden(true);
        return struct;
    }
    
    public void initialize() {
        // Do nothing
    }

    public boolean isNearestQuerySupported() {
        return false;
    }

    public boolean canUseToleranzeInNearestQuery() {
        return false;
    }

    public boolean canLimitResults() {
        return false;
    }

    public int getDataTypeSupported() {
        return DataTypes.OBJECT;
    }

    public boolean isInMemory() {
        return true;
    }

    public boolean allowNullData() {
        return false;
    }

}
