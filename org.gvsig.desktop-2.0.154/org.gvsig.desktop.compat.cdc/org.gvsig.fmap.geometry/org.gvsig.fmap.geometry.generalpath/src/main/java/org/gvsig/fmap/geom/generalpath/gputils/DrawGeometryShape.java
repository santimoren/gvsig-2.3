/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.gputils;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.geom.Geometry;

/**
 * Shape implementation used to draw in AWT a {@link Geometry} object.
 * The {@link Geometry} coordinates will become converted to integer after
 * applying any needed transformations. Also they will be simplified by
 * ignoring repeated points.
 * 
 * @author gvSIG Team
 */
public class DrawGeometryShape implements Shape {

    private Geometry geometry;
    private AffineTransform affineTransform = null;

    /**
     * Constructor.
     * 
     * @param geometry
     *            to draw in AWT
     */
    public DrawGeometryShape(Geometry geometry) {
        this.geometry = geometry;
    }

    public DrawGeometryShape(Geometry geometry,
        AffineTransform affineTransform) {
        this.geometry = geometry;
        this.affineTransform = affineTransform;
    }

    public Rectangle getBounds() {
        return geometry.getBounds();
    }

    public boolean contains(double x, double y) {
        return geometry.contains(x, y);
    }

    public boolean contains(Point2D p) {
        return geometry.contains(p);
    }

    public boolean intersects(double x, double y, double w, double h) {
        return geometry.intersects(x, y, w, h);
    }

    public boolean intersects(Rectangle2D r) {
        return geometry.intersects(r);
    }

    public boolean contains(double x, double y, double w, double h) {
        return geometry.contains(x, y, w, h);
    }

    public Rectangle2D getBounds2D() {
        return geometry.getBounds2D();
    }

    public boolean contains(Rectangle2D r) {
        return geometry.contains(r);
    }

    public PathIterator getPathIterator(AffineTransform at) {
        AffineTransform applicableTransform = at;

        if (affineTransform != null) {
            if (at != null && at.getType() != AffineTransform.TYPE_IDENTITY) {
                applicableTransform = (AffineTransform) at.clone();
                applicableTransform.concatenate(affineTransform);
            } else {
                applicableTransform = affineTransform;
            }
        }

        return new DrawGeneralPathXIterator(geometry.getGeneralPath(),
            applicableTransform);
    }

    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return new FlatteningPathIterator(getPathIterator(at), flatness);
    }

}
