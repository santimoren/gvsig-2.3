/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.gputils;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.generalpath.gputils.GeneralPathXIterator;
import org.gvsig.fmap.geom.generalpath.gputils.GeneralPathXIteratorSimple;


/**
 * @author gvSIG Team
 * @version $Id$
 * @deprecated
 */
public class GeneralPathAdapter implements Shape {
    private AffineTransform affineTransform;
    private GeneralPathX generalPathX;

    public GeneralPathAdapter(AffineTransform affineTransform,
        GeneralPathX generalPathX) {
        super();
        this.affineTransform = affineTransform;
        this.generalPathX = generalPathX;
    }

    public Rectangle getBounds() {      
        return generalPathX.getBounds();
    }

    public Rectangle2D getBounds2D() {       
        return generalPathX.getBounds2D();
    }

    public boolean contains(double x, double y) {
        return generalPathX.contains(x, y);
    }

    public boolean contains(Point2D p) {       
        return generalPathX.contains(p);
    }

    public boolean intersects(double x, double y, double w, double h) {      
        return generalPathX.intersects(x, y, w, h);
    }

    public boolean intersects(Rectangle2D r) {       
        return generalPathX.intersects(r);
    }

    public boolean contains(double x, double y, double w, double h) {     
        return generalPathX.contains(x, y, w, h);
    }

    public boolean contains(Rectangle2D r) {       
        return generalPathX.contains(r);
    }

    public PathIterator getPathIterator(AffineTransform at) {     
        AffineTransform applicableTransform = null;

        if (affineTransform != null){
            if (at != null && at.getType() != AffineTransform.TYPE_IDENTITY){
                applicableTransform = (AffineTransform)at.clone();
                applicableTransform.concatenate(affineTransform);                
            }else{
                applicableTransform = affineTransform;
            }
        }else{
            applicableTransform = at;
        }        

        if (generalPathX.isSimple()){
            return new GeneralPathXIteratorSimple(generalPathX, applicableTransform);
        }else{
            return new GeneralPathXIterator(generalPathX, applicableTransform);
        }
    }

    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return new FlatteningPathIterator(getPathIterator(at), flatness);        
    }
}
