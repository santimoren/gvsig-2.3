/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.coerce;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataTypesManager.Coercion;

/**
 * Convert a Geometry to Byte Array encoded as WKB.
 *  
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class CoerceToByteArray implements Coercion {

	Coercion previous = null;
	
	public CoerceToByteArray() {
		// Do nothing
	}
	
	public CoerceToByteArray(Coercion previous) {
		this.previous = previous;
	}
	
	
	public Object coerce(Object value) throws CoercionException {
		try {
			if( value == null || value instanceof byte[] ) {
				return value;
			}
			if( value instanceof Geometry ) {
				return ((Geometry)value).convertToWKT();
			}
			if( previous != null ) {
				return previous.coerce(value);
			}
		} catch (Exception e) {
			throw new CoercionException(e);
		}
		throw new CoercionException();
	}

}
