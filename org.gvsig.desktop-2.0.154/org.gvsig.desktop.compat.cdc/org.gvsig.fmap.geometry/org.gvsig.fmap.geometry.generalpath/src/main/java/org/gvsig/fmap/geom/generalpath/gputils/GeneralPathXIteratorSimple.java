/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.gputils;

/*
 * Based on portions of code from the
 * OpenJDK project (Copyright (c) 1996, 2006, Oracle and/or its affiliates)
 */

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import org.gvsig.fmap.geom.primitive.IGeneralPathX;

/*
 * @(#)GeneralPathXIterator.java	1.21 03/01/23
 *
 * Copyright 2003 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * This class represents the iterator for General Paths X.
 * It can be used to retrieve all of the elements in a GeneralPathX.
 * The {@link GeneralPathX#getPathIterator}
 *  method is used to create a
 * GeneralPathXIterator for a particular GeneralPathX.
 * The iterator can be used to iterator the path only once.
 * Subsequent iterations require a new iterator.
 *
 * @see GeneralPathX
 *
 * @version 10 Feb 1997
 * @author	Jim Graham
 * @deprecated 
 *      this class will be removed to the API. Use the
 *      geometry methods to get the internal points.
 */
public class GeneralPathXIteratorSimple implements PathIterator {
    protected int typeIdx = 0;
    protected int pointIdx = 0;
    protected IGeneralPathX path;
    protected AffineTransform affine;

    private static final int curvesize[] = {1, 1, 2, 3, 0};

    /**
     * Constructs an iterator given a GeneralPathX.
     * @see GeneralPathX#getPathIterator
     */
    public GeneralPathXIteratorSimple(IGeneralPathX path) {
        this(path, null);
    }

    /**
     * Constructs an iterator given a GeneralPathX and an optional
     * AffineTransform.
     * @see GeneralPathX#getPathIterator
     */
    public GeneralPathXIteratorSimple(IGeneralPathX path, AffineTransform at) {
        this.path = path;
        this.affine = at;
    }

    /**
     * Return the winding rule for determining the interior of the
     * path.
     * @see PathIterator#WIND_EVEN_ODD
     * @see PathIterator#WIND_NON_ZERO
     */
    public int getWindingRule() {
        return path.getWindingRule();
    }

    /**
     * Tests if there are more points to read.
     * @return true if there are more points to read
     */
    public boolean isDone() {
        return (typeIdx >= path.getNumTypes());
    }

    /**
     * Moves the iterator to the next segment of the path forwards
     * along the primary direction of traversal as long as there are
     * more points in that direction.
     */
    public void next() {        
        int type = path.getTypeAt(typeIdx++);
        pointIdx += curvesize[type];
    }

    /**
     * Returns the coordinates and type of the current path segment in
     * the iteration.
     * The return value is the path segment type:
     * SEG_MOVETO, SEG_LINETO, SEG_QUADTO, SEG_CUBICTO, or SEG_CLOSE.
     * A float array of length 6 must be passed in and may be used to
     * store the coordinates of the point(s).
     * Each point is stored as a pair of float x,y coordinates.
     * SEG_MOVETO and SEG_LINETO types will return one point,
     * SEG_QUADTO will return two points,
     * SEG_CUBICTO will return 3 points
     * and SEG_CLOSE will not return any points.
     * @see PathIterator#SEG_MOVETO
     * @see PathIterator#SEG_LINETO
     * @see PathIterator#SEG_QUADTO
     * @see PathIterator#SEG_CUBICTO
     * @see PathIterator#SEG_CLOSE
     */
    public int currentSegment(float[] coords) {
        int type = path.getTypeAt(typeIdx);
        if (type != PathIterator.SEG_CLOSE){
            double[] coordinates = path.getCoordinatesAt(pointIdx);       
            if (affine != null) {
                affine.transform(coordinates, 0,
                    coords, 0,
                    1);
            } else {
                coords[0] = (float) coordinates[0];
                coords[1] = (float) coordinates[1];              
            }
        }
        return type;
    }

    /**
     * Returns the coordinates and type of the current path segment in
     * the iteration.
     * The return value is the path segment type:
     * SEG_MOVETO, SEG_LINETO, SEG_QUADTO, SEG_CUBICTO, or SEG_CLOSE.
     * A double array of length 6 must be passed in and may be used to
     * store the coordinates of the point(s).
     * Each point is stored as a pair of double x,y coordinates.
     * SEG_MOVETO and SEG_LINETO types will return one point,
     * SEG_QUADTO will return two points,
     * SEG_CUBICTO will return 3 points
     * and SEG_CLOSE will not return any points.
     * @see PathIterator#SEG_MOVETO
     * @see PathIterator#SEG_LINETO
     * @see PathIterator#SEG_QUADTO
     * @see PathIterator#SEG_CUBICTO
     * @see PathIterator#SEG_CLOSE
     */
    public int currentSegment(double[] coords) {
        int type = path.getTypeAt(typeIdx);
        if (type != PathIterator.SEG_CLOSE){
            double[] coordinates = path.getCoordinatesAt(pointIdx);
            if (affine != null) {            
                affine.transform(coordinates, 0, coords, 0, 1);
            } else {           
                coords[0] = coordinates[0];
                coords[1] = coordinates[1];      
            }  
        }
        return type;
    } 
    
    public int currentSegment3D(double[] coords) {
        int type = path.getTypeAt(typeIdx);
        if (type != PathIterator.SEG_CLOSE){
            double[] coordinates = path.get3DCoordinatesAt(pointIdx);
            if (affine != null) {            
                affine.transform(coordinates, 0, coords, 0, 1);
            } else {           
                coords[0] = coordinates[0];
                coords[1] = coordinates[1];      
                coords[2] = coordinates[2];      
            }  
        }
        return type;
    } 
}
