/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.point;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;

import org.gvsig.fmap.geom.DirectPosition;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.exception.ReprojectionRuntimeException;
import org.gvsig.fmap.geom.generalpath.DefaultGeometryManager;
import org.gvsig.fmap.geom.handler.AbstractHandler;
import org.gvsig.fmap.geom.handler.FinalHandler;
import org.gvsig.fmap.geom.handler.Handler;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.generalpath.gputils.FShape;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.generalpath.gputils.PointIterator;
import org.gvsig.fmap.geom.generalpath.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.generalpath.primitive.Envelope2D;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * 2D point implementation.
 * 
 * @author Vicente Caballero Navarro
 * @author gvSIG team
 */
public class Point2D extends AbstractPrimitive implements Point, DirectPosition {
	
	private static final long serialVersionUID = 1836257305083881299L;

    protected static final String COORDINATES_FIELD = "coordinates";
    protected double x;
    protected double y;

    /**
     * The constructor with the GeometryType like and argument
     * is used by the {@link GeometryType}{@link #create()} to create the
     * geometry
     * 
     * @param type
     *            The geometry type
     */
    public Point2D(GeometryType geometryType) {
    	super(geometryType, null, null);
        this.x = 0.0d;
        this.y = 0.0d;
    }

    /**
     * Constructor used in the {@link Geometry#cloneGeometry()} method
     */
    Point2D(GeometryType geometryType, String id, IProjection projection,
        double x, double y) {
        super(geometryType, id, projection);
        this.x = x;
        this.y = y;
    }

    public Point2D(double x, double y) {
        super(TYPES.POINT, SUBTYPES.GEOM2D);
        this.x = x;
        this.y = y;
    }

    public Point2D(GeometryType geometryType, double x, double y) {
        super(geometryType, null, null);
        this.x = x;
        this.y = y;
    }

    public Point2D(int type, int subtype) {
        super(type, subtype);
        this.x = 0.0d;
        this.y = 0.0d;
    }

    public Point2D(java.awt.geom.Point2D point) {
        super(TYPES.POINT, SUBTYPES.GEOM2D);
        this.x = point.getX();
        this.y = point.getY();
    }

    /**
     * Aplica la transformaciónn de la matriz de transformación que se pasa como
     * parámetro.
     * 
     * @param at
     *            Matriz de transformación.
     */
    public void transform(AffineTransform at) {
        
        if (at == null) {
            return;
        }
        
        double[] coordinates = getCoordinates();
        at.transform(coordinates, 0, coordinates, 0, 1);
        setCoordinates(coordinates);
    }

    public boolean contains(double x, double y) {
        if ((this.x == x) || (this.y == y)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean contains(double x, double y, double w, double h) {
        return false;
    }

    public boolean intersects(double x, double y, double w, double h) {
        Rectangle2D.Double rAux = new Rectangle2D.Double(x, y, w, h);

        return rAux.contains(this.x, this.y);
    }

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 0, 0);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean contains(java.awt.geom.Point2D p) {
        return false;
    }

    public Rectangle2D getBounds2D() {
        return new Rectangle2D.Double(x - 0.01, y - 0.01, 0.02, 0.02);
    }

    public boolean contains(Rectangle2D r) {
        return false;
    }

    public boolean intersects(Rectangle2D r) {
        return r.contains(x, y);
    }

    public Shape getShape(AffineTransform affineTransform) {
        return this;
    }

    public PathIterator getPathIterator(AffineTransform at) {
        return new PointIterator(getPoint2D(), at);
    }

    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return new PointIterator(getPoint2D(), at);
    }

    private java.awt.geom.Point2D getPoint2D() {
        return new java.awt.geom.Point2D.Double(x, y);
    }

    public int getShapeType() {
        return TYPES.POINT;
    }

    public FShape cloneFShape() {
        return new Point2D(getGeometryType(), id, projection, x, y);
    }

    public void reProject(ICoordTrans ct) {
        if( ct==null ) {
            return;
        }
        java.awt.geom.Point2D p = getPoint2D();
         try {
            p = ct.convert(p, p);
            this.x = p.getX();
            this.y = p.getY();
        } catch (Exception exc) {
            /*
             * This can happen when the reprojection lib is unable
             * to reproject (for example the source point
             * is out of the valid range and some computing
             * problem happens)
             */
            throw new ReprojectionRuntimeException(
                ct.getPOrig(), ct.getPDest(), getPoint2D(), exc);
        }
    }

    public Handler[] getStretchingHandlers() {
        return new Handler[] { new PointHandler(0, x, y, this) };
    }

    public Handler[] getSelectHandlers() {
        return new Handler[] { new PointHandler(0, x, y, this) };
    }

    /**
     * 
     * @author Vicente Caballero Navarro
     * @author gvSIG team
     */
    class PointHandler extends AbstractHandler implements FinalHandler {

        private final Point gvSIGPoint;

        public PointHandler(int i, double x, double y, Point gvSIGPoint) {
            this.gvSIGPoint = gvSIGPoint;
            point = new java.awt.geom.Point2D.Double(x, y);
            index = i;
        }

        public void move(double movex, double movey) {
            gvSIGPoint.setX(gvSIGPoint.getX() + movex);
            gvSIGPoint.setY(gvSIGPoint.getY() + movey);
        }

        public void set(double setx, double sety) {
            gvSIGPoint.setX(setx);
            gvSIGPoint.setY(sety);
        }
    }

    public int getDimension() {
        return 2;
    }

    public Envelope getEnvelope() {
        return new Envelope2D(x - 0.01, y - 0.01, x + 0.02, y + 0.02);
    }

    public GeneralPathX getGeneralPath() {
        return null;
    }

    public DirectPosition getDirectPosition() {
        return this;
    }

    public double getOrdinate(int dim) {
        if (dim == Geometry.DIMENSIONS.X) {
            return getX();
        } else
            if (dim == Geometry.DIMENSIONS.Y) {
                return getY();
            } else {
                return 0;
            }
    }

    public boolean equals(Object other) {
        if (!super.equals(other)) {
            return false;
        }
        Point2D pother = (Point2D) other;
        if (Math.abs(this.getX() - pother.getX()) > 0.0000001) {
            return false;
        }
        if (Math.abs(this.getY() - pother.getY()) > 0.0000001) {
            return false;
        }
        return true;

    }

    public double[] getCoordinates() {
        return new double[] { x, y };
    }

    public double getCoordinateAt(int dimension) {
        switch (dimension) {
        case Geometry.DIMENSIONS.X:
            return x;
        case Geometry.DIMENSIONS.Y:
            return y;
        default:
            throw new ArrayIndexOutOfBoundsException(dimension);
        }
    }

    public void setCoordinateAt(int dimension, double value) {
        switch (dimension) {
        case Geometry.DIMENSIONS.X:
            this.x = value;
            break;
        case Geometry.DIMENSIONS.Y:
            this.y = value;
            break;
        default:
            throw new ArrayIndexOutOfBoundsException(dimension);
        }
    }

    public void setCoordinates(double[] values) {
        this.x = values[Geometry.DIMENSIONS.X];
        this.y = values[Geometry.DIMENSIONS.Y];
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        double[] coordinates = getCoordinates();
        buffer.append(getFullTypeName()).append("(").append(coordinates[0]);
        for (int i = 1; i < coordinates.length; i++) {
            buffer.append(",").append(coordinates[i]);
        }
        buffer.append(")");
        return buffer.toString();
    }

    protected String getFullTypeName() {
        return "Point2D";
    }
    
    public int getType() {
        return TYPES.POINT;
    }


    public MultiPoint toPoints() throws GeometryException {
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPoint multi = manager.createMultiPoint(this.getGeometryType().getSubType());
        multi.addPrimitive(this);
        return multi;
    }
    
}
