/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse;

import java.awt.geom.Point2D;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author paco
 *
 */
public class Ellipse2DZ extends Ellipse2D implements Surface {
	private static final long serialVersionUID = -6678908069111004754L;
	
	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public Ellipse2DZ(GeometryType geometryType) {
		super(geometryType);		
	}
	
	/**
	 * @param id
	 * @param projection
	 * @param gpx
	 * @param i
	 * @param e
	 * @param d
	 */
	Ellipse2DZ(GeometryType geometryType, String id, IProjection projection, GeneralPathX gpx,
			Point2D i, Point2D e, double d, double z) {
		super(geometryType, id, projection, gpx, i, e, d);		
	}
	
    public double getCoordinateAt(int index, int dimension) {
        if (index > gp.getNumCoords()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        double[] coords = gp.getPointAt(index).getCoordinates();
        if(dimension < coords.length)
        	return gp.getPointAt(index).getCoordinateAt(dimension);
        return 0D;
    }
	
    public int getDimension() {
        return 3;
    }
}
