/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.generalpath;

import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLibrary;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.generalpath.aggregate.BaseMultiPrimitive2D;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiCurve2D;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiCurve2DZ;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiPoint2D;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiPoint2DZ;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiSolid2DZ;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiSurface2D;
import org.gvsig.fmap.geom.generalpath.aggregate.MultiSurface3D;
import org.gvsig.fmap.geom.generalpath.persistence.GeometryPersistenceFactory;
import org.gvsig.fmap.geom.generalpath.coerce.CoerceToByteArray;
import org.gvsig.fmap.geom.generalpath.coerce.CoerceToEnvelope;
import org.gvsig.fmap.geom.generalpath.coerce.CoerceToGeometry;
import org.gvsig.fmap.geom.generalpath.coerce.CoerceToString;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.generalpath.primitive.curve.arc.Arc2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.Circle2D;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.Circle2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.generalpath.primitive.curve.line.Line2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.DefaultEnvelope;
import org.gvsig.fmap.geom.generalpath.primitive.DefaultNullGeometry;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.Ellipse2D;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.Ellipse2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.EllipticArc2D;
import org.gvsig.fmap.geom.generalpath.primitive.surface.ellipse.EllipticArc2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.Envelope2D;
import org.gvsig.fmap.geom.generalpath.primitive.Envelope3D;
import org.gvsig.fmap.geom.generalpath.primitive.Geometry2D;
import org.gvsig.fmap.geom.generalpath.primitive.Geometry2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.point.Point2DGeometryType;
import org.gvsig.fmap.geom.generalpath.primitive.point.Point3DGeometryType;
import org.gvsig.fmap.geom.generalpath.primitive.solid.Solid2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.curve.spline.Spline2D;
import org.gvsig.fmap.geom.generalpath.primitive.curve.spline.Spline2DZ;
import org.gvsig.fmap.geom.generalpath.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.generalpath.primitive.surface.polygon.Polygon2DZ;
import org.gvsig.fmap.geom.generalpath.persistence.GeometryTypePersistenceFactory;
import org.gvsig.fmap.geom.generalpath.primitive.curve.arc.Arc2D;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.persistence.PersistenceManager;

/**
 * Registers the default implementation for {@link GeometryManager}
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultGeometryLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsImplementationOf(GeometryLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        // Register the default GeometryManager
        GeometryLocator.registerGeometryManager(DefaultGeometryManager.class);
    }

    protected void doPostInitialize() throws LibraryException {
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        DataTypesManager dataTypesManager = ToolsLocator.getDataTypesManager();

        dataTypesManager
        .setCoercion(DataTypes.GEOMETRY, new CoerceToGeometry());
        dataTypesManager
        .setCoercion(DataTypes.ENVELOPE, new CoerceToEnvelope());
        dataTypesManager.setCoercion(DataTypes.STRING, new CoerceToString(
            dataTypesManager.getCoercion(DataTypes.STRING)));
        dataTypesManager.setCoercion(
            DataTypes.BYTEARRAY,
            new CoerceToByteArray(dataTypesManager
                .getCoercion(DataTypes.BYTEARRAY)));

        GeometryOperation.OPERATION_INTERSECTS_CODE =
            GeometryLocator.getGeometryManager().getGeometryOperationCode(
                GeometryOperation.OPERATION_INTERSECTS_NAME);

        GeometryOperation.OPERATION_CONTAINS_CODE =
            GeometryLocator.getGeometryManager().getGeometryOperationCode(
                GeometryOperation.OPERATION_CONTAINS_NAME);

        // Register the geometries in 2D
        geometryManager.registerGeometryType(DefaultNullGeometry.class, "Null",
            TYPES.NULL, SUBTYPES.GEOM2D);
        
        geometryManager.registerGeometryType(Geometry2D.class, "Geometry2D",
                TYPES.GEOMETRY, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Geometry2DZ.class, "Geometry3D",
            TYPES.GEOMETRY, SUBTYPES.GEOM3D);

        //Register points in 2D
        geometryManager.registerGeometryType(new Point2DGeometryType());

        //Register curves in 2D
        geometryManager.registerGeometryType(Line2D.class, "Curve2D",
            TYPES.CURVE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Arc2D.class, "Arc2D", 
            TYPES.ARC, SUBTYPES.GEOM2D, TYPES.CURVE);
        geometryManager.registerGeometryType(Spline2D.class, "Spline2D",
            TYPES.SPLINE, SUBTYPES.GEOM2D, TYPES.CURVE);

        //Register surfaces in 2D
        geometryManager.registerGeometryType(Polygon2D.class, "Surface2D",
            TYPES.SURFACE, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(Circle2D.class, "Circle2D",
            TYPES.CIRCLE, SUBTYPES.GEOM2D, TYPES.SURFACE);      
        geometryManager.registerGeometryType(Ellipse2D.class, "Ellipse2D",
            TYPES.ELLIPSE, SUBTYPES.GEOM2D, TYPES.SURFACE);
        geometryManager.registerGeometryType(EllipticArc2D.class,"EllipticArc2D", 
            TYPES.ELLIPTICARC, SUBTYPES.GEOM2D, TYPES.SURFACE);

        //Register multigeometries in 2D
        geometryManager.registerGeometryType(BaseMultiPrimitive2D.class, "MultiPrimitive2D", 
            TYPES.AGGREGATE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(MultiPoint2D.class, "MultiPoint2D",
            TYPES.MULTIPOINT, SUBTYPES.GEOM2D, TYPES.AGGREGATE);
        geometryManager.registerGeometryType(MultiCurve2D.class, "MultiCurve2D",
            TYPES.MULTICURVE, SUBTYPES.GEOM2D, TYPES.AGGREGATE); 
        geometryManager.registerGeometryType(MultiSurface2D.class,"MultiSurface2D",
            TYPES.MULTISURFACE, SUBTYPES.GEOM2D, TYPES.AGGREGATE);

        // Register the geometries in 3D
        geometryManager.registerGeometryType(DefaultNullGeometry.class,
            TYPES.NULL, SUBTYPES.GEOM3D);

        //Register points in 3D
        geometryManager.registerGeometryType(new Point3DGeometryType());

        //Register curves in 3D
        geometryManager.registerGeometryType(Line2DZ.class, "Curve3D", 
            TYPES.CURVE, SUBTYPES.GEOM3D, new int[0], new int[]{SUBTYPES.GEOM2D});
        geometryManager.registerGeometryType(Arc2DZ.class, "Arc3D", 
            TYPES.ARC, SUBTYPES.GEOM3D, TYPES.CURVE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Spline2DZ.class, "Spline3D", 
            TYPES.SPLINE, SUBTYPES.GEOM3D, TYPES.CURVE, SUBTYPES.GEOM2D);

        //Register surfaces in 3D
        geometryManager.registerGeometryType(Polygon2DZ.class, "Surface3D", 
            TYPES.SURFACE, SUBTYPES.GEOM3D, new int[0], new int[]{SUBTYPES.GEOM2D});
        geometryManager.registerGeometryType(Circle2DZ.class, "Circle3D",
            TYPES.CIRCLE, SUBTYPES.GEOM3D, TYPES.SURFACE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(Ellipse2DZ.class, "Ellipse3D", 
            TYPES.ELLIPSE,SUBTYPES.GEOM3D, TYPES.SURFACE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(EllipticArc2DZ.class, "EllipticArc3D",
            TYPES.ELLIPTICARC, SUBTYPES.GEOM3D, TYPES.SURFACE, SUBTYPES.GEOM2D);

        //Register multigeometries in 3D
        geometryManager.registerGeometryType(MultiPoint2DZ.class, "Multipoint3D", 
            TYPES.MULTIPOINT, SUBTYPES.GEOM3D, TYPES.AGGREGATE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(MultiCurve2DZ.class,"MultiCurve3D",
            TYPES.MULTICURVE, SUBTYPES.GEOM3D, TYPES.AGGREGATE, SUBTYPES.GEOM2D);
        geometryManager.registerGeometryType(MultiSurface3D.class,"MultiSurface3D",
            TYPES.MULTISURFACE, SUBTYPES.GEOM3D, TYPES.AGGREGATE, SUBTYPES.GEOM2D);

        //Register solids
        geometryManager.registerGeometryType(Solid2DZ.class, "Solid3D",
            TYPES.SOLID, SUBTYPES.GEOM3D);
        geometryManager.registerGeometryType(MultiSolid2DZ.class,"MultiSolid3D",
            TYPES.MULTISOLID, SUBTYPES.GEOM3D, TYPES.AGGREGATE);

        // Persistence
        DefaultEnvelope.registerPersistent();
        Envelope2D.registerPersistent();
        Envelope3D.registerPersistent();

        PersistenceManager persistenceManager =
            ToolsLocator.getPersistenceManager();

        persistenceManager.registerFactory(new GeometryPersistenceFactory());
        persistenceManager
            .registerFactory(new GeometryTypePersistenceFactory());
    }
}
