/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.aggregate;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.aggregate.MultiSolid;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Solid;
import org.gvsig.fmap.geom.generalpath.primitive.solid.Solid2DZ;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public class MultiSolid2DZ extends BaseMultiPrimitive implements MultiSolid {
	private static final long serialVersionUID = 6690458251466529134L;
	
	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public MultiSolid2DZ(GeometryType geomType) {
		super(geomType);		
	}
	
	MultiSolid2DZ(GeometryType geomType, String id, IProjection projection) {
		super(geomType, id, projection);
	}

	MultiSolid2DZ(GeometryType geomType, String id, IProjection projection, Solid2DZ[] solids) {
		super(geomType,id, projection, solids);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.geometries.iso.GM_Object#coordinateDimension()
	 */
	public int getDimension() {
		return 3;
	}	

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiSolid#addSolid(org.gvsig.fmap.geom.primitive.Solid)
	 */
	public void addSolid(Solid solid) {
		geometries.add(solid);
	}

        public void addPrimitive(Primitive primitive) {
		addSolid((Solid)primitive);
	}
	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiSolid#getSolidAt(int)
	 */
	public Solid getSolidAt(int index) {
		return (Solid)getPrimitiveAt(index);
	}

}
