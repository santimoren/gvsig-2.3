/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.aggregate;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.TopologyException;
import com.vividsolutions.jts.precision.GeometryPrecisionReducer;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.generalpath.DefaultGeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.generalpath.primitive.surface.polygon.Polygon2D;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.generalpath.util.Converter;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Polygon;

/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public class MultiSurface2D extends BaseMultiPrimitive implements MultiSurface, MultiPolygon {
	private static final long serialVersionUID = 625054010696136925L;

	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public MultiSurface2D(GeometryType geomType) {
		super(geomType);		
	}
	
	MultiSurface2D(GeometryType geomType, String id, IProjection projection) {
		super(geomType, id, projection);
	}

	MultiSurface2D(GeometryType geomType, String id, IProjection projection,
			Polygon2D[] polygons) {
		super(geomType, id, projection, polygons);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.iver.cit.gvsig.fmap.core.FGeometryCollection#cloneGeometry()
	 */
	public Geometry cloneGeometry() {
		MultiSurface2D auxSurface = new MultiSurface2D(geometryType, id, projection);
		for (int i = 0; i < getPrimitivesNumber(); i++) {
			auxSurface.addSurface((Surface)((Surface) geometries.get(i)).cloneGeometry());
		}
		return auxSurface;
	}	

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiSurface#addSurface(org.gvsig.fmap.geom.primitive.Surface)
	 */
	public void addSurface(Surface surface) {
		geometries.add(surface);
	}

        public void addPrimitive(Primitive primitive) {
		addSurface((Surface)primitive);
	}
	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiSurface#getSurfaceAt(int)
	 */
	public Surface getSurfaceAt(int index) {
		return (Surface)getPrimitiveAt(index);
	}
	
	
    public Geometry union(Geometry other)
        throws GeometryOperationNotSupportedException,
        GeometryOperationException {

        com.vividsolutions.jts.geom.Geometry this_jts, other_jts, union_jts;
        Geometry resp = null;

        try {
            this_jts = getJTS();
            other_jts = Converter.geometryToJts(other);
            union_jts = this_jts.union(other_jts);
            if (union_jts instanceof com.vividsolutions.jts.geom.Polygon) {
                double tole = union_jts.getArea();
                tole = tole * 1e-12;
                union_jts = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) union_jts, tole);
            } else {
                if (union_jts instanceof com.vividsolutions.jts.geom.MultiPolygon) {
                    double tole = union_jts.getArea();
                    tole = tole * 1e-12;
                    union_jts =
                        removeInvalidHoles((com.vividsolutions.jts.geom.MultiPolygon) union_jts, tole);
                }
            }
            resp = Converter.jtsToGeometry(union_jts);
            return resp;
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        } catch (TopologyException e) {
            /*
             * If JTS throws this, we'll try to simplify them
             */
            try {
                this_jts = getJTS();
                other_jts = Converter.geometryToJts(other);

                PrecisionModel pm = new PrecisionModel(1000000);
                GeometryPrecisionReducer gpr = new GeometryPrecisionReducer(pm);
                this_jts = gpr.reduce(this_jts);
                other_jts = gpr.reduce(other_jts);
                union_jts = this_jts.union(other_jts);

                if (union_jts instanceof com.vividsolutions.jts.geom.Polygon) {
                    double tole = union_jts.getArea();
                    tole = tole * 1e-12;
                    union_jts = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) union_jts, tole);
                } else {
                    if (union_jts instanceof com.vividsolutions.jts.geom.MultiPolygon) {
                        double tole = union_jts.getArea();
                        tole = tole * 1e-12;
                        union_jts =
                            removeInvalidHoles((com.vividsolutions.jts.geom.MultiPolygon) union_jts, tole);
                    }
                }

                resp = Converter.jtsToGeometry(union_jts);
                return resp;
            } catch (Exception exc) {
                throw new GeometryOperationException(exc);
            }
        }
    }
    
    private com.vividsolutions.jts.geom.Geometry getJTS() {
        return Converter.geometryToJts(this);
    }
    
    private com.vividsolutions.jts.geom.MultiPolygon removeInvalidHoles(com.vividsolutions.jts.geom.MultiPolygon mpo, double tol) {

        GeometryFactory gf = new GeometryFactory(mpo.getPrecisionModel());
        
        int npo = mpo.getNumGeometries();
        com.vividsolutions.jts.geom.Polygon[] pos = new com.vividsolutions.jts.geom.Polygon[npo];
        for (int i=0; i<npo; i++) {
            pos[i] = removeInvalidHoles((com.vividsolutions.jts.geom.Polygon) mpo.getGeometryN(i), tol);
        }
        return gf.createMultiPolygon(pos);
    }
    
    private com.vividsolutions.jts.geom.Polygon removeInvalidHoles(com.vividsolutions.jts.geom.Polygon po, double tol) {
        
        GeometryFactory gf = new GeometryFactory(po.getPrecisionModel());
        
        int nholes = po.getNumInteriorRing();
        List validholes = new ArrayList();
        for (int i=0; i<nholes; i++) {
            LineString ls = po.getInteriorRingN(i);
            LinearRing lr = toLinearRing(ls, gf);
            if (getLinearRingArea(lr, gf) > tol) {
                validholes.add(lr);
            }
            
        }
        
        if (validholes.size() < nholes) {
            
            LinearRing[] holes = (LinearRing[]) validholes.toArray(new LinearRing[0]);
            return gf.createPolygon(
                toLinearRing(po.getExteriorRing(), gf), holes);
        } else {
            return po;
        }
        
    }

    private double getLinearRingArea(LinearRing lr, GeometryFactory gf) {
        com.vividsolutions.jts.geom.Polygon po = gf.createPolygon(lr);
        double resp = po.getArea(); 
        return resp;
    }

    private LinearRing toLinearRing(LineString ls, GeometryFactory gf) {
        return gf.createLinearRing(ls.getCoordinateSequence());
    }

    public MultiPolygon toPolygons() throws GeometryException {
        return this;
    }

    public MultiPoint toPoints() throws GeometryException {
        if( this.getPrimitivesNumber() < 1 ) {
            return null;
        }
        int numPoints = 0;
        for( int i=0; i<this.getPrimitivesNumber(); i++ ) {
            numPoints += ((Curve)(this.getPrimitiveAt(i))).getNumVertices();
        }
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPoint multipoint = manager.createMultiPoint(this.getGeometryType().getSubType());
        multipoint.ensureCapacity(numPoints);
        for( int i=0; i<this.getPrimitivesNumber(); i++ ) {
            Curve curve = (Curve)(this.getPrimitiveAt(i));
            for( int n=0; n<curve.getNumVertices(); n++ ) {
                multipoint.addPrimitive(curve.getVertex(n));
            }
        }        
        return multipoint;
    }

    public MultiLine toLines() throws GeometryException {
        if( this.getPrimitivesNumber() < 1 ) {
            return null;
        }
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiLine multi = manager.createMultiLine(this.getGeometryType().getSubType());
        multi.ensureCapacity(this.getPrimitivesNumber());
        for( int i=0; i<this.getPrimitivesNumber(); i++ ) {
            Polygon polygon = (Polygon)(this.getPrimitiveAt(i));
            MultiLine lines = polygon.toLines();
            for( int n=0; n<lines.getPrimitivesNumber(); n++ ) {
                multi.addPrimitive(lines.getPrimitiveAt(n));
            }
        }        
        return multi;
    }
}
