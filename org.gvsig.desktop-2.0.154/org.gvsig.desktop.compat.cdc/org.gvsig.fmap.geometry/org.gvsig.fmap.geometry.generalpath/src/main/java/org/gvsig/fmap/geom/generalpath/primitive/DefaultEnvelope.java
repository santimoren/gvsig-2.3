/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.geom.generalpath.primitive;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.EnvelopeNotInitializedException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;


/**
 * A minimum bounding box or rectangle. Regardless of dimension, an Envelope
 * can be represented without ambiguity as two direct positions (coordinate
 * points). To encode an Envelope, it is sufficient to encode these two
 * points. This is consistent with all of the data types in this
 * specification, their state is represented by their publicly accessible
 * attributes.

 * @author Vicente Caballero Navarro
 */
public abstract class DefaultEnvelope implements Envelope, org.gvsig.tools.lang.Cloneable{
    private static final Logger LOG =
        LoggerFactory.getLogger(DefaultEnvelope.class);

    public static final String PERSISTENCE_DEFINITION_NAME = "Envelope";

    protected static final String LOWERCORNER_FIELD = "lowerCorner";
    protected static final String UPPERCORNER_FIELD = "upperCorner";

    protected Point min;
    protected Point max;

    protected boolean isEmpty;

    protected static GeometryManager manager = GeometryLocator.getGeometryManager();

    public DefaultEnvelope(){
        super();
        isEmpty = true;
    }

    public DefaultEnvelope(Point min, Point max){
        super();
        this.min = min;
        this.max = max;
        isEmpty = false;
    }

    public String toString() {
        if( this.isEmpty ) {
            return "Envelop(empty)";
        }
        return MessageFormat.format(
            "Envelop(min={0},max={1})", 
            new Object[] {
                min.toString(),
                max.toString()
            }
        );
    }

    /**
     * Returns the center ordinate along the specified dimension.
     *
     * @param dimension DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public double getCenter(int dimension) {
        if (isEmpty){
            throw new EnvelopeNotInitializedException();
        }
        return (min.getCoordinateAt(dimension) + max.getCoordinateAt(dimension)) * 0.5;
    }

    /**
     * Returns the envelope length along the specified dimension.
     *
     * @param dimension
     *
     * @return
     */
    public double getLength(int dimension) {
        if (isEmpty){
            throw new EnvelopeNotInitializedException();
        }
        if (max.getCoordinateAt(dimension) > min.getCoordinateAt(dimension)) {
            return max.getCoordinateAt(dimension) - min.getCoordinateAt(dimension);
        }

        return min.getCoordinateAt(dimension) - max.getCoordinateAt(dimension);
    }

    /**
     * A coordinate position consisting of all the minimal ordinates for each
     * dimension for all points within the Envelope.
     *
     * @return
     */
    public Point getLowerCorner() {
        return min;
    }

    /**
     * Returns the maximal ordinate along the specified dimension.
     *
     * @param dimension
     *
     * @return
     */
    public double getMaximum(int dimension) {
        if (isEmpty){
            throw new EnvelopeNotInitializedException();
        }
        return max.getCoordinateAt(dimension);
    }

    /**
     * Returns the minimal ordinate along the specified dimension.
     *
     * @param dimension
     *
     * @return
     */
    public double getMinimum(int dimension) {
        if (isEmpty){
            throw new EnvelopeNotInitializedException();
        }
        return min.getCoordinateAt(dimension);
    }

    /**
     * A coordinate position consisting of all the maximal ordinates for each
     * dimension for all points within the Envelope.
     *
     * @return
     */
    public Point getUpperCorner() {
        return max;
    }



    public Geometry getGeometry() {
        if (isEmpty){
            throw new EnvelopeNotInitializedException();
        }
        try {
            Surface surface = (Surface)manager.create(TYPES.SURFACE, SUBTYPES.GEOM2D);
            surface.addMoveToVertex((Point)min.cloneGeometry());
            surface.addVertex(manager.createPoint(getMaximum(0),getMinimum(1), SUBTYPES.GEOM2D));
            surface.addVertex((Point)max.cloneGeometry());
            surface.addVertex(manager.createPoint(getMinimum(0),getMaximum(1), SUBTYPES.GEOM2D));
            surface.closePrimitive();
            return surface;
        } catch (CreateGeometryException e) {
            LOG.error("Error creting the surface", e);
        }	  
        return null;
    }

    public boolean contains(Envelope envelope) {
        if (isEmpty){
            return false;
        }
        if((envelope == null) || (envelope.isEmpty())) {
            return false;
        }
        for (int i = 0; i < getDimension(); i++) {
            if (getMinimum(i) > envelope.getMinimum(i)
                || getMaximum(i) < envelope.getMaximum(i)) {
                return false;
            }
        }
        return true;
    }

    public boolean intersects(Envelope envelope) {
        if (isEmpty){
            return false;
        }
        if((envelope == null) || (envelope.isEmpty())) {
            return false;
        }
        int dimension = getDimension();
        for (int i = 0; i < dimension; i++) {
            if (getMinimum(i)>envelope.getMaximum(i)){
                return false;
            } else if (getMaximum(i)<envelope.getMinimum(i)){
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object other) {
        if (!(other == null || other instanceof Envelope)) {
            return false;
        }
        Envelope otherEnv = (Envelope) other;
        if (isEmpty && otherEnv.isEmpty()){
            return true;
        }
        if (otherEnv.getDimension() != this.getDimension()) {
            return false;
        }
        for (int i = 0; i < this.getDimension(); i++) {
            if (otherEnv.getMinimum(i) != this.getMinimum(i)) {
                return false;
            }
            if (otherEnv.getMaximum(i) != this.getMaximum(i)) {
                return false;
            }
        }
        return true;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Envelope#setLowerCorner(org.gvsig.fmap.geom.primitive.Point)
     */
    public void setLowerCorner(Point lowerCorner) {
        this.min = lowerCorner;
        if (max != null){
            isEmpty = false;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.geom.primitive.Envelope#setUpperCorner(org.gvsig.fmap.geom.primitive.Point)
     */
    public void setUpperCorner(Point upperCorner) {
        this.max = upperCorner;
        if (min != null){
            isEmpty = false;
        }
    }

    public static void registerPersistent() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if( manager.getDefinition(PERSISTENCE_DEFINITION_NAME)==null ) {
            DynStruct definition = manager.addDefinition(
                DefaultEnvelope.class,
                PERSISTENCE_DEFINITION_NAME,
                "DefaultEnvelope persistence definition",
                null, 
                null
            ); 

            definition.addDynFieldObject(LOWERCORNER_FIELD).setClassOfValue(Point.class).setMandatory(true);
            definition.addDynFieldObject(UPPERCORNER_FIELD).setClassOfValue(Point.class).setMandatory(true);
        }
    }

    public void loadFromState(PersistentState state)
    throws PersistenceException {
        setLowerCorner((Point)state.get(LOWERCORNER_FIELD));		
        setUpperCorner((Point)state.get(UPPERCORNER_FIELD));		
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set(LOWERCORNER_FIELD, min);	
        state.set(UPPERCORNER_FIELD, max);		
    }

    public Object clone() throws CloneNotSupportedException {
        DefaultEnvelope other = (DefaultEnvelope) super.clone();
        if (!isEmpty){        
            other.max = (Point) max.cloneGeometry();
            other.min = (Point) min.cloneGeometry();
        }
        return other;
    }

    public boolean isEmpty() {       
        return isEmpty;
    }	
    
    public void add(Geometry geometry) {
        this.add(geometry.getEnvelope());
    }

    public void clear() {
        isEmpty = true;
    }
    
}
