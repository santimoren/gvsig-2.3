/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.generalpath.aggregate;

import java.util.Collection;
import java.util.Iterator;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.generalpath.DefaultGeometryManager;
import org.gvsig.fmap.geom.generalpath.primitive.AbstractPrimitive;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.generalpath.primitive.curve.line.Line2D;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.type.GeometryType;

/**
 * @author Jorge Piera Llodr� (jorge.piera@iver.es)
 */
public class MultiCurve2D extends BaseMultiPrimitive implements MultiCurve, MultiLine {
	private static final long serialVersionUID = -5279820875142516144L;
     
	/**
	 * The constructor with the GeometryType like and argument 
	 * is used by the {@link GeometryType}{@link #create()}
	 * to create the geometry
	 * @param type
	 * The geometry type
	 */
	public MultiCurve2D(GeometryType geomType) {
		super(geomType);		
	}
	
	MultiCurve2D(GeometryType geomType, String id, IProjection projection) {
		super(geomType, id, projection);
	}

	MultiCurve2D(GeometryType geomType, String id, IProjection projection, Line2D[] lines) {
		super(geomType, id, projection, lines);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.iver.cit.gvsig.fmap.core.FGeometryCollection#cloneGeometry()
	 */
	public Geometry cloneGeometry() {
		MultiCurve2D auxCurve = new MultiCurve2D(geometryType, id, projection);
		for (int i = 0; i < getPrimitivesNumber(); i++) {
			auxCurve.addCurve((Curve)((Curve) geometries.get(i)).cloneGeometry());
		}
		return auxCurve;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiCurve#addCurve(org.gvsig.fmap.geom.primitive.Curve)
	 */
	public void addCurve(Curve curve) {
		geometries.add(curve);
	}

        public void addPrimitive(Primitive primitive) {
		addCurve((Curve)primitive);
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.aggregate.MultiCurve#getCurveAt(int)
	 */
	public Curve getCurveAt(int index) {
		return (Curve)getPrimitiveAt(index);
	}

    public MultiPolygon toPolygons() throws GeometryException {
        if( this.getPrimitivesNumber() < 1 ) {
            return null;
        }
        com.vividsolutions.jts.geom.Geometry jts_union = null;
        jts_union = ((AbstractPrimitive)this.getPrimitiveAt(0)).toJTS();
        for( int i=1; i<this.getPrimitivesNumber(); i++) {
            jts_union = jts_union.union(((AbstractPrimitive)this.getPrimitiveAt(i)).toJTS());
        }
        com.vividsolutions.jts.operation.polygonize.Polygonizer jts_polygonizer = new com.vividsolutions.jts.operation.polygonize.Polygonizer();
        
        jts_polygonizer.add(jts_union);
        Collection jts_polygons = jts_polygonizer.getPolygons();
        if( jts_polygons == null && jts_polygons.isEmpty() ) {
            return null;
        }
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPolygon multi=  manager.createMultiPolygon(this.getGeometryType().getSubType());
        Iterator it = jts_polygons.iterator();
        while( it.hasNext() ) {
            com.vividsolutions.jts.geom.Geometry jts_polygon = (com.vividsolutions.jts.geom.Geometry) it.next();
            Polygon polygon = (Polygon) manager.createFrom(jts_polygon);
            multi.addPrimitive(polygon);
        }
        return multi;
    }

    public MultiPoint toPoints() throws GeometryException {
        if( this.getPrimitivesNumber() < 1 ) {
            return null;
        }
        int numPoints = 0;
        for( int i=0; i<this.getPrimitivesNumber(); i++ ) {
            numPoints += ((Curve)(this.getPrimitiveAt(i))).getNumVertices();
        }
        DefaultGeometryManager manager = (DefaultGeometryManager) GeometryLocator.getGeometryManager();
        MultiPoint multipoint = manager.createMultiPoint(this.getGeometryType().getSubType());
        multipoint.ensureCapacity(numPoints);
        for( int i=0; i<this.getPrimitivesNumber(); i++ ) {
            Curve curve = (Curve)(this.getPrimitiveAt(i));
            for( int n=0; n<curve.getNumVertices(); n++ ) {
                multipoint.addPrimitive(curve.getVertex(n));
            }
        }        
        return multipoint;
    }

    public MultiLine toLines() throws GeometryException {
        return this;
    }
    
    
}
