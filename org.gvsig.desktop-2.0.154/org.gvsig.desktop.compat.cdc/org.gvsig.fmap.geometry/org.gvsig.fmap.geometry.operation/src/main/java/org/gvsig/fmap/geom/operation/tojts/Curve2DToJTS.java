/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.geom.operation.tojts;

import java.awt.geom.PathIterator;
import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.primitive.Point;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateArrays;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class Curve2DToJTS extends ToJTS{
    
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.tojts.ToJTS#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		int srid = -1;
		if (ctx != null){
			srid = ((JTSGeometryOperationContext)ctx).getSrid();
		}
		ArrayList arrayLines = new ArrayList();
		PathIterator theIterator = geom.getPathIterator(null, geomManager.getFlatness());
		int theType;
		double[] theData = new double[6];
		ArrayList arrayCoords = null;
		LineString lin;
		int numParts = 0;
		Coordinate coord;
		
		while (!theIterator.isDone()) {
			//while not done
			theType = theIterator.currentSegment(theData);

			//Populate a segment of the new
			// GeneralPathX object.
			//Process the current segment to populate a new
			// segment of the new GeneralPathX object.
			switch (theType) {
			case PathIterator.SEG_MOVETO:

				// System.out.println("SEG_MOVETO");
				if (arrayCoords == null) {
					arrayCoords = new ArrayList();
				} else {
					lin = geomFactory.createLineString(CoordinateArrays.toCoordinateArray(
							arrayCoords));
					lin.setSRID(srid);
					arrayLines.add(lin);
					arrayCoords = new ArrayList();
				}

				numParts++;
				coord = new Coordinate(theData[0], theData[1]);

				arrayCoords.add(coord);

				break;

			case PathIterator.SEG_LINETO:

				// System.out.println("SEG_LINETO");
				arrayCoords.add(new Coordinate(theData[0],
						theData[1]));

				break;

			case PathIterator.SEG_QUADTO:
				System.out.println("Not supported here");

				break;

			case PathIterator.SEG_CUBICTO:
				System.out.println("Not supported here");

				break;

			case PathIterator.SEG_CLOSE:
				// A�adimos el primer punto para cerrar.
				Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
					// Solo anyadimos cuando no esta ya cerrado
				arrayCoords.add(new Coordinate(firstCoord.x,
						firstCoord.y));

				break;
			} //end switch

			theIterator.next();
		} //end while loop

		if (arrayCoords.size()<2) {
			throw new GeometryOperationException(geom.getType(), geom.getGeometryType().getSubType());
		}
		lin = geomFactory.createLineString(CoordinateArrays.toCoordinateArray(
				arrayCoords));

		lin.setSRID(srid);
		arrayLines.add(lin);
		
		LineString[] lineString = GeometryFactory.toLineStringArray(arrayLines);
		
		if (lineString.length == 1) {
		    return lineString[0];
		} else {
	        MultiLineString resp = geomFactory.createMultiLineString(lineString);
	        resp.setSRID(srid);
	        return resp;
		}
	}	
}
