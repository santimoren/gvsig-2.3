/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.tojts;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MultiCurveToJTS extends ToJTS{
    
    private static Logger logger = LoggerFactory.getLogger(MultiCurveToJTS.class);
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.tojts.ToJTS#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		int srid = -1;
		if (ctx != null){
			srid = ((JTSGeometryOperationContext)ctx).getSrid();
		}
		MultiCurve multiCurve = (MultiCurve)geom;
		Object item = null;
		List ls_list = new ArrayList();

		int prim_n = multiCurve.getPrimitivesNumber();
		
        for (int i = 0; i < prim_n; i++){
        	try {
        	    item = multiCurve.getPrimitiveAt(i).invokeOperation(CODE, ctx);
        	    
        	    if (item instanceof MultiLineString) {
        	        
                    /*
                     * A multilinestring can also be returned
                     */
        	        MultiLineString mls = (MultiLineString) item;
        	        for (int k = 0; k<mls.getNumGeometries(); k++) {
        	            com.vividsolutions.jts.geom.Geometry p = mls.getGeometryN(k);
        	            if (p instanceof LineString) {
        	                ls_list.add(p);
        	            } 
        	        }
        	        
        	    } else {
                    if (item instanceof LineString) {
                        /*
                         * Including primitives converted to linestring.
                         * Short primitives can return a point
                         */
                        ls_list.add(item);
                    } else {
                        String txt = (item == null) ? "NULL" : item.getClass().getName();
                        logger.info("Warning: excluding primitive in MultiCurveToJTS because it converted to a JTS " + txt);
                    }
        	    }
        	    
			} catch (GeometryOperationNotSupportedException e) {
				throw new GeometryOperationException(e);
			}
        }

        LineString[] lines = (LineString[]) ls_list.toArray(new LineString[0]);

        MultiLineString lineString = new com.vividsolutions.jts.geom.GeometryFactory().createMultiLineString(lines);
        lineString.setSRID(srid);
        return lineString;
	}	
}
