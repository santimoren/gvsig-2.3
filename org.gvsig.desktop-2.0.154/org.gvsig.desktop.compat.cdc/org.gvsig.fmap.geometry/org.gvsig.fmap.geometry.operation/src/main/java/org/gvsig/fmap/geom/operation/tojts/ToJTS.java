/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.tojts;

import java.lang.reflect.Array;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.generalpath.util.Converter;

import com.vividsolutions.jts.geom.Coordinate;

public abstract class ToJTS extends GeometryOperation {
    public static final String NAME = "toJTS";
	protected static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);	
	protected final static com.vividsolutions.jts.geom.GeometryFactory geomFactory = new com.vividsolutions.jts.geom.GeometryFactory();

	public int getOperationIndex() {
		return CODE;
	}

	protected boolean isClosed(Coordinate firstCoordinate, Coordinate lastCoordinate){
		double diff = Math.abs(lastCoordinate.x - firstCoordinate.x);
		if (diff > 0.000001){
			return false;
		}
		diff = Math.abs(lastCoordinate.y - firstCoordinate.y);
		if (diff > 0.000001) {
			return false;
		}
		return true;
	}
	
	protected boolean pointInList(Coordinate testPoint, Coordinate[] pointList) {
		int t;
		int numpoints;
		Coordinate p;

		numpoints = Array.getLength(pointList);

		for (t = 0; t < numpoints; t++) {
			p = pointList[t];

			if ((testPoint.x == p.x) && (testPoint.y == p.y) &&
					((testPoint.z == p.z) || (!(testPoint.z == testPoint.z))) //nan test; x!=x iff x is nan
			) {
				return true;
			}
		}

		return false;
	}
}
