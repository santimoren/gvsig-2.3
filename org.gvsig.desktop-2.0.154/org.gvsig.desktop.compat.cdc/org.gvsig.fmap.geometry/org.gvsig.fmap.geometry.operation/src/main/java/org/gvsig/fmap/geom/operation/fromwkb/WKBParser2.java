/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.fromwkb;

/*
 * Based in
 * PostGIS extension for PostgreSQL JDBC driver - Binary Parser
 *
 * (C) 2005 Markus Schaber, schabios@logi-track.com
 */

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.vividsolutions.jts.io.WKBConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;

/**
 * Parse binary representation of geometries. Currently, only text rep (hexed)
 * implementation is tested.
 *
 * It should be easy to add char[] and CharSequence ByteGetter instances,
 * although the latter one is not compatible with older jdks.
 *
 * I did not implement real unsigned 32-bit integers or emulate them with long,
 * as both java Arrays and Strings currently can have only 2^31-1 elements
 * (bytes), so we cannot even get or build Geometries with more than approx.
 * 2^28 coordinates (8 bytes each).
 *
 * @author markus.schaber@logi-track.com
 *
 */
public class WKBParser2 {

    private static final Logger LOG = LoggerFactory.getLogger(WKBParser2.class);

	private GeometryManager geomManager = GeometryLocator.getGeometryManager();
    private boolean gHaveM, gHaveZ, gHaveS; // M, Z y SRID
    private GeometryType[] pointGeometryTypes;

    /**
     * @throws GeometryTypeNotValidException
     * @throws GeometryTypeNotSupportedException
     * 
     */
    public WKBParser2() {
        pointGeometryTypes =
            new GeometryType[] {
                loadPointGeometryType(Geometry.SUBTYPES.GEOM2D, "2D"),
                loadPointGeometryType(Geometry.SUBTYPES.GEOM3D, "3D"),
                loadPointGeometryType(Geometry.SUBTYPES.GEOM2DM, "2DM"),
                loadPointGeometryType(Geometry.SUBTYPES.GEOM3DM, "3DM") };
    }

    private GeometryType loadPointGeometryType(int subtype, String subTypeName) {
        try {
            return geomManager.getGeometryType(Geometry.TYPES.POINT, subtype);
        } catch (Exception e) {
            LOG.warn("Unable to get a reference to the geometry "
                + "type Point{}, to be cached", subTypeName);
            return null;
        }
    }

    /**
     * Parse a binary encoded geometry.
     * 
     * Is synchronized to protect offset counter. (Unfortunately, Java does not
     * have neither call by reference nor multiple return values.)
     * 
     * @throws CreateGeometryException
     */
    public synchronized Geometry parse(byte[] value) throws CreateGeometryException {
        // BinaryByteGetter bytes = new ByteGetter.BinaryByteGetter(value);
        ByteBuffer buf = ByteBuffer.wrap(value);
        return parseGeometry(buf);
    }

    protected void parseTypeAndSRID(ByteBuffer data)
    {
        byte endian = data.get(); //skip and test endian flag
        /* if (endian != data.endian) {
            throw new IllegalArgumentException("Endian inconsistency!");
        } */
        int typeword = data.getInt();

        int realtype = typeword & 0x1FFFFFFF; //cut off high flag bits

        gHaveZ = (typeword & 0x80000000) != 0;
        gHaveM = (typeword & 0x40000000) != 0;
        gHaveS = (typeword & 0x20000000) != 0;

        int srid = -1;

        if (gHaveS) {
            srid = data.getInt();
        }

    }


    /** Parse a geometry starting at offset. 
     * @throws CreateGeometryException */
    protected Geometry parseGeometry(ByteBuffer data) throws CreateGeometryException {
        byte endian = data.get(); //skip and test endian flag
        if (endian == 1)
        {
        	data.order(ByteOrder.LITTLE_ENDIAN);
        }
        /* if (endian != data.endian) {
            throw new IllegalArgumentException("Endian inconsistency!");
        } */
        int typeword = data.getInt();

        int realtype = typeword & 0x1FFFFFFF; //cut off high flag bits

        boolean haveZ = (typeword & 0x80000000) != 0;
        boolean haveM = (typeword & 0x40000000) != 0;
        boolean haveS = (typeword & 0x20000000) != 0;

        int srid = -1;

        if (haveS) {
            srid = data.getInt();
        }
        Geometry result1;
        switch (realtype) {
        case WKBConstants.wkbPoint :
        	result1 = parsePoint(data, haveZ, haveM);
            break;
        case WKBConstants.wkbLineString :
            result1 = parseLineString(data, haveZ, haveM);
            break;
        case WKBConstants.wkbPolygon :
            result1 = parsePolygon(data, haveZ, haveM);
            break;
        case WKBConstants.wkbMultiPoint:
            result1 = parseMultiPoint(data);
            break;
        case WKBConstants.wkbMultiLineString:
            result1 = parseMultiLineString(data);
            break;
        case WKBConstants.wkbMultiPolygon:
            result1 = parseMultiPolygon(data);
            break;
        case WKBConstants.wkbGeometryCollection :
            result1 = parseCollection(data);
            break;
        default :
            throw new IllegalArgumentException("Unknown Geometry Type!");
        }

        /*Geometry result = result1;

        if (haveS) {
            result.setSrid(srid);
        } */
        return result1;
    }

    private Point parsePoint(ByteBuffer data, boolean haveZ, boolean haveM)
        throws CreateGeometryException {
        double x = data.getDouble();
        double y = data.getDouble();
        Point point;

        int subtype = getSubType(haveZ, haveM);

        // If we have a cached GeometryType use it, otherwise call the manager
        if( pointGeometryTypes[subtype] == null ) {
            point = (Point) geomManager.create(Geometry.TYPES.POINT, subtype);
        } else {
            point = (Point) pointGeometryTypes[subtype].create();
        }
        point.setX(x);
        point.setY(y);

        // Other dimensions
        if (haveZ) {
            point.setCoordinateAt(Geometry.DIMENSIONS.Z, data.getDouble());
            if (haveM) {
                point.setCoordinateAt(Geometry.DIMENSIONS.Z + 1,
                    data.getDouble());
            }
        } else {
            if (haveM) {
                point.setCoordinateAt(Geometry.DIMENSIONS.Y + 1,
                    data.getDouble());
            }
        }

        return point;
    }

    /**
     * @param haveZ
     * @param haveM
     * @return
     */
    private int getSubType(boolean haveZ, boolean haveM) {
        int subtype =
            haveZ ? (haveM ? Geometry.SUBTYPES.GEOM3DM
                : Geometry.SUBTYPES.GEOM3D) : (haveM
                ? Geometry.SUBTYPES.GEOM2DM : Geometry.SUBTYPES.GEOM2D);
        return subtype;
    }

    /** Parse an Array of "full" Geometries 
     * @throws CreateGeometryException */
    private void parseGeometryArray(ByteBuffer data, Geometry[] container) throws CreateGeometryException {
        for (int i = 0; i < container.length; i++) {
            container[i] = parseGeometry(data);
        }
    }

    /**
     * Parse an Array of "slim" Points (without endianness and type, part of
     * LinearRing and Linestring, but not MultiPoint!
     * 
     * @param haveZ
     * @param haveM
     * @throws CreateGeometryException
     */
    private Point[] parsePointArray(ByteBuffer data, boolean haveZ,
        boolean haveM) throws CreateGeometryException {
        int count = data.getInt();
        Point[] result = new Point[count];
        for (int i = 0; i < count; i++) {
            result[i] = parsePoint(data, haveZ, haveM);
        }
        return result;
    }

    private double[][] parsePointsAsDoubleArray(ByteBuffer data, boolean haveZ,
        boolean haveM) throws CreateGeometryException {
        int count = data.getInt();
        double points[][] = null;
        int subtype = getSubType(haveZ, haveM);

        switch (subtype) {
        case Geometry.SUBTYPES.GEOM2D:
            points = new double[count][2];
            break;
        case Geometry.SUBTYPES.GEOM3D:
        case Geometry.SUBTYPES.GEOM2DM:
            points = new double[count][3];
            break;
        case Geometry.SUBTYPES.GEOM3DM:
            points = new double[count][4];
            break;
        default:
            break;
        }

        for (int i = 0; i < count; i++) {
            points[i][0] = data.getDouble(); // x
            points[i][1] = data.getDouble(); // y
            switch (subtype) {
            case Geometry.SUBTYPES.GEOM3D:
            case Geometry.SUBTYPES.GEOM2DM:
                points[i][2] = data.getDouble(); // z or m
                break;
            case Geometry.SUBTYPES.GEOM3DM:
                points[i][2] = data.getDouble(); // z
                points[i][3] = data.getDouble(); // m
                break;
            default:
                break;
            }
        }
        return points;
    }

    private MultiPoint parseMultiPoint(ByteBuffer data) throws CreateGeometryException {
    	MultiPoint multipoint = (MultiPoint) geomManager.create(TYPES.MULTIPOINT, SUBTYPES.GEOM2D);
        int points = data.getInt();
        multipoint.ensureCapacity(points);
        for (int i=0; i < points; i++) {
        	parseTypeAndSRID(data);
        	multipoint.addPoint(parsePoint(data, gHaveZ, gHaveM));
        }
        return multipoint;
    }

    private Curve parseLineString(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
        Point[] points = parsePointArray(data, haveZ, haveM);
        Curve curve = (Curve) geomManager.create(TYPES.CURVE, getSubType(haveZ, haveM));    
        curve.addMoveToVertex(points[0]);
        for (int i = 1; i< points.length; i++)
        {
            curve.addVertex(points[i]);
        }
        return curve;
    }

    private void fillLinearRing(ByteBuffer data, OrientablePrimitive orientablePrimitive, boolean haveZ, boolean haveM) throws CreateGeometryException {
        Point[] points = parsePointArray(data, haveZ, haveM);
       
        orientablePrimitive.addMoveToVertex(points[0]);
        int lastPoint = points.length - 1;
        for (int i = 1; i< lastPoint; i++)
        {
            orientablePrimitive.addVertex(points[i]);
        }        
    }

    private Surface parsePolygon(ByteBuffer data, boolean haveZ, boolean haveM) throws CreateGeometryException {
	        int count = data.getInt();
	        int subType = getSubType(haveZ, haveM);        
	        
	        Surface surface = (Surface) geomManager.create(TYPES.SURFACE, subType);
	        	      
	        for (int i = 0; i < count; i++) {
	            fillLinearRing(data, surface, haveZ, haveM); 
	        }
	        	   
	        surface.closePrimitive();	        
	       
			return surface;
	 }
    
    
    private MultiCurve parseMultiLineString(ByteBuffer data) throws CreateGeometryException {
		MultiCurve multiCurve = (MultiCurve) geomManager.create(TYPES.MULTICURVE, getSubType(gHaveZ, gHaveM));
        Curve curve = (Curve) geomManager.create(TYPES.CURVE, getSubType(gHaveZ, gHaveM));
        fillOrientablePrimitive(data, curve);
		multiCurve.addCurve(curve);
        return multiCurve;
}	
    /**
     * @param data
     * @return
     * @throws CreateGeometryException
     */
    private void fillOrientablePrimitive(ByteBuffer data, OrientablePrimitive orientablePrimitive)
        throws CreateGeometryException {
		int count = data.getInt();
         
        for (int i=0; i < count; i++)
        {
            parseTypeAndSRID(data);
            Point[] points = parsePointArray(data, gHaveZ, gHaveM);
 
            orientablePrimitive.addMoveToVertex(points[0]);
            for (int j = 1; j < points.length; j++) {
                orientablePrimitive.addVertex(points[j]);
            }           
        }		
	}
    
    private MultiSurface parseMultiPolygon(ByteBuffer data)
            throws CreateGeometryException {
            int count = data.getInt();
            
            int subType = getSubType(gHaveZ, gHaveM);
            MultiSurface multiSurface = (MultiSurface)geomManager.create(TYPES.MULTISURFACE, subType);
            
            Point point;
            for (int i = 0; i < count; i++) {
                parseTypeAndSRID(data);
                int countRings = data.getInt();
                Surface surface = (Surface)geomManager.create(TYPES.SURFACE, subType);
                for (int j = 0; j < countRings; j++) {
                    double[][] points =
                        parsePointsAsDoubleArray(data, gHaveZ, gHaveM);
                    
                    //Add the initial point
                    surface.addVertex(geomManager.createPoint(points[0][0], points[0][1], subType));
                    
                    //Add the other points
                    int lastPoint = points.length - 1;
                    for (int k=1 ; k<lastPoint ; k++){                    
                        point = geomManager.createPoint(points[k][0], points[k][1], subType);
                        for (int l=2 ; l<points[k].length ; i++){
                            point.setCoordinateAt(l, points[k][l]);
                        }
                        surface.addVertex(point);
                    }   
                    surface.closePrimitive();
                }
                multiSurface.addSurface(surface);
            }        
            return multiSurface;
    }
    
    private MultiPrimitive parseCollection(ByteBuffer data) throws CreateGeometryException {
        int count = data.getInt();
        Geometry[] geoms = new Geometry[count];
        parseGeometryArray(data, geoms);
        MultiPrimitive multiPrimitive = (MultiPrimitive) geomManager.create(TYPES.AGGREGATE, SUBTYPES.GEOM2D);
        for (int i=0 ; i<geoms.length ; i++){
        	multiPrimitive.addPrimitive((Primitive) geoms[i]);
        }
        return multiPrimitive;
    }	
}
