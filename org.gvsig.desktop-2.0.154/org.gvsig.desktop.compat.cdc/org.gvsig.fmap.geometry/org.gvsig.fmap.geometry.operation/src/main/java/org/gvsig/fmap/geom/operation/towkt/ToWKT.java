/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.towkt;

import com.vividsolutions.jts.io.WKTWriter;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.operation.tojts.ToJTS;

public class ToWKT extends GeometryOperation {
    public static final String NAME = "toWKT";
	public static final int CODE = GeometryLocator.getGeometryManager().
    	getGeometryOperationCode(NAME);
	
	private static WKTWriter writer = new WKTWriter();

	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		try {
            return writer.write((com.vividsolutions.jts.geom.Geometry)geom.invokeOperation(ToJTS.CODE, null));
        } catch (GeometryOperationNotSupportedException e) {
           throw new GeometryOperationException(e);
        }
	}

	public int getOperationIndex() {
		return CODE;
	}

}
