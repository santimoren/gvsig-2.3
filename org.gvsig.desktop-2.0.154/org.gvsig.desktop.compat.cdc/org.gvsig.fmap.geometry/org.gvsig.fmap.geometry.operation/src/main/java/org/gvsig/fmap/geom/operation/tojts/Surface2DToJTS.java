/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.geom.operation.tojts;

import java.awt.geom.PathIterator;
import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateArrays;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class Surface2DToJTS extends ToJTS{
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.tojts.ToJTS#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		int srid = -1;
		if (ctx != null){
			srid = ((JTSGeometryOperationContext)ctx).getSrid();
		}
		ArrayList arrayLines = new ArrayList();
		PathIterator theIterator = geom.getPathIterator(null, geomManager.getFlatness());
		int theType;
		double[] theData = new double[6];
		ArrayList arrayCoords = null;
		LineString lin;
		int numParts = 0;
		Coordinate coord;

		ArrayList shells = new ArrayList();
		ArrayList holes = new ArrayList();
		Coordinate[] points = null;

		while (!theIterator.isDone()) {
			//while not done
			theType = theIterator.currentSegment(theData);

			//Populate a segment of the new
			// GeneralPathX object.
			//Process the current segment to populate a new
			// segment of the new GeneralPathX object.
			switch (theType) {
			case PathIterator.SEG_MOVETO:

				// System.out.println("SEG_MOVETO");
				if (arrayCoords == null) {
					arrayCoords = new ArrayList();
				} else {
					points = CoordinateArrays.toCoordinateArray(arrayCoords);

					try {
						LinearRing ring = geomFactory.createLinearRing(points);

						if (CGAlgorithms.isCCW(points)) {
							holes.add(ring);
						} else {
							shells.add(ring);
						}
					} catch (Exception e) {
						/* (jaume) caso cuando todos los puntos son iguales
						 * devuelvo el propio punto
						 */
						boolean same = true;
						for (int i = 0; i < points.length-1 && same; i++) {
							if (points[i].x != points[i+1].x ||
									points[i].y != points[i+1].y /*||
									points[i].z != points[i+1].z*/
							) {
								same = false;
							}
						}
						if (same) {
							return geomFactory.createPoint(points[0]);
						}
						/*
						 * caso cuando es una l�nea de 3 puntos, no creo un LinearRing, sino
						 * una linea
						 */
						if (points.length>1 && points.length<=3) {
							// return geomFactory.createLineString(points);
							return geomFactory.createMultiLineString(new LineString[] {geomFactory.createLineString(points)});
						}

						System.err.println(
						"Caught Topology exception in GMLLinearRingHandler");

						return null;
					}

					/* if (numParts == 1)
					 {
					 linRingExt = new GeometryFactory().createLinearRing(
					 CoordinateArrays.toCoordinateArray(arrayCoords));
					 }
					 else
					 {
					 linRing = new GeometryFactory().createLinearRing(
					 CoordinateArrays.toCoordinateArray(arrayCoords));
					 arrayLines.add(linRing);
					 } */
					arrayCoords = new ArrayList();
				}

				numParts++;
				arrayCoords.add(new Coordinate(theData[0],
						theData[1]));

				break;

			case PathIterator.SEG_LINETO:

				// System.out.println("SEG_LINETO");
				arrayCoords.add(new Coordinate(theData[0],
						theData[1]));

				break;

			case PathIterator.SEG_QUADTO:
				System.out.println("SEG_QUADTO Not supported here");

				break;

			case PathIterator.SEG_CUBICTO:
				System.out.println("SEG_CUBICTO Not supported here");

				break;

			case PathIterator.SEG_CLOSE:

				// A�adimos el primer punto para cerrar.
				Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
				arrayCoords.add(new Coordinate(firstCoord.x,
						firstCoord.y));

				break;
			} //end switch

			// System.out.println("theData[0] = " + theData[0] + " theData[1]=" + theData[1]);
			theIterator.next();
		} //end while loop


		Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
		Coordinate lastCoord = (Coordinate) arrayCoords.get(arrayCoords
				.size() - 1);
		if (!isClosed(firstCoord, lastCoord)) {
			arrayCoords.add(firstCoord);
		}
		points = CoordinateArrays.toCoordinateArray(arrayCoords);

		try {
			LinearRing ring = geomFactory.createLinearRing(points);

			if (CGAlgorithms.isCCW(points)) {
				holes.add(ring);
			} else {
				shells.add(ring);
			}
			ring.setSRID(srid);
		} catch (Exception e) {
			/* (jaume) caso cuando todos los puntos son iguales
			 * devuelvo el propio punto
			 */
			boolean same = true;
			for (int i = 0; i < points.length-1 && same; i++) {
				if (points[i].x != points[i+1].x ||
						points[i].y != points[i+1].y /*||
						points[i].z != points[i+1].z*/
				) {
					same = false;
				}
			}
			if (same) {
				com.vividsolutions.jts.geom.Geometry geoJTS = geomFactory.createPoint(points[0]);
				geoJTS.setSRID(srid);
				return geoJTS;
			}
			/*
			 * caso cuando es una l�nea de 3 puntos, no creo un LinearRing, sino
			 * una linea
			 */
			if (points.length>1 && points.length<=3) {
				// return geomFactory.createLineString(points);
				com.vividsolutions.jts.geom.Geometry geoJTS = geomFactory
						.createMultiLineString(new LineString[] { geomFactory
								.createLineString(points) });
				geoJTS.setSRID(srid);
				return geoJTS;
			}
			System.err.println(
			"Caught Topology exception in GMLLinearRingHandler");

			return null;
		}

		/* linRing = new GeometryFactory().createLinearRing(
		 CoordinateArrays.toCoordinateArray(arrayCoords)); */

		// System.out.println("NumParts = " + numParts);
		//now we have a list of all shells and all holes
		ArrayList holesForShells = new ArrayList(shells.size());

		for (int i = 0; i < shells.size(); i++) {
			holesForShells.add(new ArrayList());
		}

		//find homes
		for (int i = 0; i < holes.size(); i++) {
			LinearRing testRing = (LinearRing) holes.get(i);
			LinearRing minShell = null;
			Envelope minEnv = null;
			Envelope testEnv = testRing.getEnvelopeInternal();
			Coordinate testPt = testRing.getCoordinateN(0);
			LinearRing tryRing = null;

			for (int j = 0; j < shells.size(); j++) {
				tryRing = (LinearRing) shells.get(j);

				Envelope tryEnv = tryRing.getEnvelopeInternal();

				if (minShell != null) {
					minEnv = minShell.getEnvelopeInternal();
				}

				boolean isContained = false;
				Coordinate[] coordList = tryRing.getCoordinates();

				if (tryEnv.contains(testEnv) &&
						(CGAlgorithms.isPointInRing(testPt, coordList) ||
								(pointInList(testPt, coordList)))) {
					isContained = true;
				}

				// check if this new containing ring is smaller than the current minimum ring
				if (isContained) {
					if ((minShell == null) || minEnv.contains(tryEnv)) {
						minShell = tryRing;
					}
				}
			}

			if (minShell == null) {
//				System.out.println(
//				"polygon found with a hole thats not inside a shell");
//				azabala: we do the assumption that this hole is really a shell (polygon)
//				whose point werent digitized in the right order
				Coordinate[] cs = testRing.getCoordinates();
				Coordinate[] reversed = new Coordinate[cs.length];
				int pointIndex = 0;
				for(int z = cs.length-1; z >= 0; z--){
					reversed[pointIndex] = cs[z];
					pointIndex++;
				}
				LinearRing newRing = geomFactory.createLinearRing(reversed);
				shells.add(newRing);
				holesForShells.add(new ArrayList());
			} else {
				((ArrayList) holesForShells.get(shells.indexOf(minShell))).add(testRing);
			}
		}

		Polygon[] polygons = new Polygon[shells.size()];

		for (int i = 0; i < shells.size(); i++) {
			polygons[i] = geomFactory.createPolygon((LinearRing) shells.get(
					i),
					(LinearRing[]) ((ArrayList) holesForShells.get(i)).toArray(
							new LinearRing[0]));
			polygons[i].setSRID(srid);
		}
	
		holesForShells = null;
		shells = null;
		holes = null;

		//com.vividsolutions.jts.geom.Geometry geoJTS = geomFactory.createMultiPolygon(polygons);
		polygons[0].setSRID(srid);

		com.vividsolutions.jts.geom.Geometry geoJTS;
        if (polygons.length == 1) {
            geoJTS = polygons[0];
        } else {
            // its a multi part
            geoJTS = geomFactory.createMultiPolygon(polygons);
        }
        geoJTS.setSRID(srid);
		return geoJTS;
	}
}
