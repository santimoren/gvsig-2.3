/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.flip;

import java.awt.geom.PathIterator;
import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.CoordinateSequences;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

/**
 * This class converts the path into points, then flip it down. 
 * 
 * @author Carlos S�nchez Peri��n <a href = "mailto:csanchez@prodevelop.es"> e-mail </a>
 */
public class Flip extends GeometryOperation{
    public static final String NAME = "flip";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);
	
	private GeneralPathX generalPathX = null;
    
	public int getOperationIndex() {
		return CODE;
	}
	
    public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
    	generalPathX = geom.getGeneralPath();
		if(generalPathX == null){
			//if there isn't path the operation hasn't sense.
			return null;
    	}
		PathIterator theIterator = geom.getPathIterator(null, geomManager.getFlatness()); //polyLine.getPathIterator(null, flatness);
    	double[] theData = new double[6];
    	//Coordinate first = null;
    	CoordinateList coordList = new CoordinateList();
    	Coordinate c1;
    	GeneralPathX newGp = new GeneralPathX();
    	ArrayList listOfParts = new ArrayList();
    	while (!theIterator.isDone()) {
    		//while not done
    		int type = theIterator.currentSegment(theData);
    		switch (type)
    		{
    		case (byte) PathIterator.SEG_MOVETO:
    			coordList = new CoordinateList();
    			listOfParts.add(coordList);
    			c1= new Coordinate(theData[0], theData[1]);
    			coordList.add(c1, true);
    			break;
    	    case (byte) PathIterator.SEG_LINETO:
    			c1= new Coordinate(theData[0], theData[1]);
    			coordList.add(c1, true);
    			break;
    		case (byte) PathIterator.SEG_CLOSE:
    			coordList.add(coordList.getCoordinate(0));
    			break;
    		}
    		theIterator.next();
    	}
    		
    	for (int i=listOfParts.size()-1; i>=0; i--)
    	{
    		coordList = (CoordinateList) listOfParts.get(i);
    		Coordinate[] coords = coordList.toCoordinateArray();
    		CoordinateArraySequence seq = new CoordinateArraySequence(coords);
    		CoordinateSequences.reverse(seq);
    		coords = seq.toCoordinateArray();
    		newGp.moveTo(coords[0].x, coords[0].y);
    		for (int j=1; j < coords.length; j++)
    		{
    			newGp.lineTo(coords[j].x, coords[j].y);
    		}
    	}
    	generalPathX.reset();
    	generalPathX.append(newGp.getPathIterator(null), false);	
    	
		return null;		
	}
}