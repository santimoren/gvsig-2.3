/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.geom.operation.tojts;

import java.awt.geom.PathIterator;
import java.util.ArrayList;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateArrays;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Converts a Surface2D into a MultiLineString object
 * @author <a href="mailto:nachobrodin@gmail.com">Nacho Brodin</a>
 */
public class Surface2DToJTSLineString extends ToJTS {
	public static final String NAME = "toJTSLineString";
	public static final int CODE = geomManager.getGeometryOperationCode(NAME);
	
	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.tojts.ToJTS#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException {
		int srid = -1;
		if (ctx != null){
			srid = ((JTSGeometryOperationContext)ctx).getSrid();
		}
		PathIterator theIterator = geom.getPathIterator(null, geomManager.getFlatness());
		int theType;
		double[] theData = new double[6];
		ArrayList arrayCoords = null;
		Coordinate[] points = null;
		ArrayList listRings = new ArrayList();
		theIterator = geom.getPathIterator(null, geomManager.getFlatness());

		while (!theIterator.isDone()) {
			theType = theIterator.currentSegment(theData);

			switch (theType) {
			case PathIterator.SEG_MOVETO:
				if (arrayCoords == null) {
					arrayCoords = new ArrayList();
				} else
					arrayCoords.clear();
				arrayCoords.add(new Coordinate(theData[0], theData[1]));
				break;

			case PathIterator.SEG_LINETO:
				arrayCoords.add(new Coordinate(theData[0],theData[1]));
				break;
			case PathIterator.SEG_QUADTO:
				System.out.println("SEG_QUADTO Not supported here");
				break;
			case PathIterator.SEG_CUBICTO:
				System.out.println("SEG_CUBICTO Not supported here");
				break;
			case PathIterator.SEG_CLOSE:
				break;
			}
			theIterator.next();
		} 
		
		Coordinate firstCoord = (Coordinate) arrayCoords.get(0);
		Coordinate lastCoord = (Coordinate) arrayCoords.get(arrayCoords.size() - 1);
		if (!isClosed(firstCoord, lastCoord)) {
			arrayCoords.add(firstCoord);
		}
		points = CoordinateArrays.toCoordinateArray(arrayCoords);
		listRings.add(geomFactory.createLinearRing(points));
		
		LinearRing[] list = (LinearRing[])listRings.toArray(new LinearRing[listRings.size()]);
		MultiLineString lineStr = new com.vividsolutions.jts.geom.GeometryFactory().createMultiLineString(list);
		lineStr.setSRID(srid);
		return lineStr;
	}
}
