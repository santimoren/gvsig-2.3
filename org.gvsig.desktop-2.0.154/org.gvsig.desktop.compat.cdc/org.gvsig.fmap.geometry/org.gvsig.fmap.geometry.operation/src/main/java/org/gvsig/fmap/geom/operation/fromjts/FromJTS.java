/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.fromjts;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.generalpath.util.Converter;

/**
 * <p>
 * Converts a geometry from JTS to DAL format. This is a special geometry
 * operation
 * because don't use the Geometry parameter and returns a JTS geometry. This
 * kind
 * of operation can be called from the {@link GeometryManager} using
 * invokeOperation.
 * </p>
 * <p>
 * This operation needs a JTS geometry loaded in the context. The only attribute
 * of the {@link GeometryOperation} have to be a geometry of this type and the
 * ID
 * of this attribute will be JTSGeometry.
 * </p>
 *
 * @author Nacho Brodin (nachobrodin@gmail.com)
 *
 */
public class FromJTS extends GeometryOperation {

    public static final String NAME = "fromJTS";
    public static final String PARAM = "JTSGeometry";
    protected static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);
    protected final static com.vividsolutions.jts.geom.GeometryFactory geomFactory = new com.vividsolutions.jts.geom.GeometryFactory();

    public int getOperationIndex() {
        return CODE;
    }

    /*
     * (non-Javadoc)
     * @see org.gvsig.fmap.geom.operation.GeometryOperation#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
     */
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
            throws GeometryOperationException {
        Object obj = ctx.getAttribute("JTSGeometry");
        if ( obj instanceof com.vividsolutions.jts.geom.Geometry ) {
            try {
                return Converter.jtsToGeometry((com.vividsolutions.jts.geom.Geometry) obj);
            } catch (CreateGeometryException e) {
                throw new GeometryOperationException(e);
            }
        }
        return null;
    }

}
