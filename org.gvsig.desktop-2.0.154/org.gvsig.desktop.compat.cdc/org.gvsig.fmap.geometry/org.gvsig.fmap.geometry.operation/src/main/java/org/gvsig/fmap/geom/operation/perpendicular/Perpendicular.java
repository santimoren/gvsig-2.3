/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation.perpendicular;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperation;
import org.gvsig.fmap.geom.operation.GeometryOperationContext;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.primitive.Point;


/**
 * Gets a couple of points that defines a line that is perpendicular to other line
 * and intersects with a point.
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class Perpendicular extends GeometryOperation{
    public static final String NAME = "perpendicular";
    private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
    public static final int CODE = geomManager.getGeometryOperationCode(NAME);
   
    public Object invoke(Geometry geom, GeometryOperationContext ctx)
        throws GeometryOperationException {
        Point point1 = (Point)geom;
        Point point2 = (Point)ctx.getAttribute(PerpendicularOperationContext.SECOND_POINT);
        Point perpendicularPoint = (Point)ctx.getAttribute(PerpendicularOperationContext.PERPENDICULAR_POINT);        
        
        if ((point2.getY() - point1.getY()) == 0) {
            try {
                return new Point[] {
                    geomManager.createPoint(perpendicularPoint.getX(), 0, SUBTYPES.GEOM2D),
                    geomManager.createPoint(perpendicularPoint.getX(), 1, SUBTYPES.GEOM2D)
                };
            } catch (CreateGeometryException e) {
                throw new GeometryOperationException(e);
            }
        }

        //Pendiente de la recta perpendicular
        double m = (point1.getX() - point2.getX()) / (point2.getY() - point1.getY());

        //b de la funcion de la recta perpendicular
        double b = perpendicularPoint.getY() - (m * perpendicularPoint.getX());

        //Obtenemos un par de puntos
        try {
            return new Point[] {
                geomManager.createPoint(0, (m * 0) + b, SUBTYPES.GEOM2D),
                geomManager.createPoint(1000, (m * 1000) + b, SUBTYPES.GEOM2D)
            };
        } catch (CreateGeometryException e) {
            throw new GeometryOperationException(e);
        }
    }
   
    public int getOperationIndex() {      
        return CODE;
    }    
}
