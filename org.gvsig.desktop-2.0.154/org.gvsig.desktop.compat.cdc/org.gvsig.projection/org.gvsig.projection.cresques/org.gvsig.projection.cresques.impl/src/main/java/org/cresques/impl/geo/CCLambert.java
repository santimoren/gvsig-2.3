/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.cresques.impl.geo;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IDatum;
import org.cresques.cts.IProjection;
import org.cresques.geo.ViewPortData;

import org.cresques.px.Extent;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;


/**
 * Proyeccion de Conica Comforme Lambert
 */
public class CCLambert extends Projection {
    static String name = "Conica Comforme Lambert";
    static String abrev = "CCLam";

    public CCLambert(Ellipsoid eli) {
        super(eli);
        grid = new Graticule(this);
    }

    @Override
    public ICoordTrans getCT(IProjection dest) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
	public String getAbrev() {
        return abrev;
    }

    public static CCLambert getProjection(Ellipsoid eli) {
        return new CCLambert(eli);
    }

    /**
     *
     * @param eli
     * @param name
     * @return 
     */
    public static IProjection getProjectionByName(IDatum eli, String name) {
        if (name.indexOf("CCL") < 0) {
            return null;
        }

        return getProjection((Ellipsoid) eli);
    }

    /**
     *
     * @param x
     * @param y
     * @return 
     */
    @Override
    public Point2D createPoint(double x, double y) {
        return new Point2D.Double(x, y);
    }

    /**
     *
     * @param lPt
     * @return
     */
    @Override
    public Point2D toGeo(Point2D lPt) {
        GeoPoint gPt = new GeoPoint();

        return toGeo(lPt, gPt);
    }

    /**
     *
     * @param lPt
     * @param gPt
     * @return
     */
    public GeoPoint toGeo(Point2D lPt, GeoPoint gPt) {
        return gPt;
    }

    /**
     *
     * @param gPt
     * @param lPt
     * @return
     */
    @Override
    public Point2D fromGeo(Point2D gPt, Point2D lPt) {
        return lPt;
    }

    private void generateGrid(Graphics2D g, Extent extent, AffineTransform mat) {
        grid = new Graticule(this);
    }

    @Override
    public void drawGrid(Graphics2D g, ViewPortData vp) {
        generateGrid(g, vp.getExtent(), vp.getMat());
        grid.setColor(gridColor);
        grid.draw(g, vp);
    }

    /* (non-Javadoc)
     * @see org.cresques.cts.IProjection#getScale(double, double, double, double)
     */
    @Override
    public double getScale(double minX, double maxX, double width, double dpi) {
        // TODO Auto-generated method stub
        return -1D;
    }

	/* (non-Javadoc)
	 * @see org.cresques.cts.IProjection#getExtent(java.awt.geom.Rectangle2D, double, double, double, double, double, double)
	 */
    @Override
	public Rectangle2D getExtent(Rectangle2D extent, double scale, double wImage, double hImage, double mapUnits, double distanceUnits, double dpi) {
		return null;
	}


    /**
     *
     * @return
     */    
    @Override
	public String getFullCode() {
		return getAbrev();
	}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }
}
