/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.cresques.impl;

import org.cresques.ProjectionLibrary;
import org.cresques.impl.cts.ProjectionPool;

import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class CresquesCtsLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsImplementationOf(ProjectionLibrary.class,1);
    }

    protected void doInitialize() throws LibraryException {
        CRSFactory.registerCRSFactory( new ProjectionPool());
    }

    protected void doPostInitialize() throws LibraryException {
        // Nothing to do
    }
}
