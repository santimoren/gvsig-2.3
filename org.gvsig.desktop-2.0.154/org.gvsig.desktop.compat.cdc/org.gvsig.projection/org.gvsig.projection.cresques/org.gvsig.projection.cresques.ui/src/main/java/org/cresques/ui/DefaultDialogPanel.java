/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.cresques.ui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.cresques.Messages;


/**
 * @author Luis W. Sevilla (sevilla_lui@gva.es)
 */
public class DefaultDialogPanel extends JPanel { // implements
                                                 // ComponentListener{
    final private static long serialVersionUID = -3370601314380922368L;
    protected JPanel contentPane = null;
    private JPanel tabPane = null;
    protected JPanel buttonPane = null;
    private JButton acceptButton = null;
    private JButton cancelButton = null;
    protected int cWidth = 0, difWidth = 0;
    protected int cHeight = 0, difHeight = 0;
	protected JPanel pButton = null;

    /**
     * Constructor
     * @param init
     */
    public DefaultDialogPanel(boolean init){
    	if(init)
    		initialize();
    }

    /**
     * Constructor
     */
    public DefaultDialogPanel() {
        super();
        this.initialize();
    }

    /**
     * This method initializes this
     *
     * @return void
     */
    public void initialize() {
        GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
        gridBagConstraints11.gridx = 0;
        gridBagConstraints11.insets = new java.awt.Insets(4,0,0,0);
        gridBagConstraints11.gridy = 1;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5,0,2,0);
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        setLayout(new GridBagLayout());
        setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));

        this.add(getTabPanel(), gridBagConstraints);
        this.add(getPButton(), gridBagConstraints11);
    }

    /**
     * Obtiene el panel general
     * @return
     */
    protected JPanel getContentPanel() {
        if (contentPane == null) {
            contentPane = new JPanel();
            contentPane.setLayout(new GridBagLayout());
        }

        return contentPane;
    }

    public JPanel getTabPanel() {
        if (tabPane == null) {
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.insets = new java.awt.Insets(5,0,0,0);
            gridBagConstraints2.gridy = 0;
            gridBagConstraints2.gridx = 0;
            tabPane = new JPanel();
            tabPane.setLayout(new GridBagLayout());

            tabPane.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.SoftBevelBorder.RAISED));
            tabPane.add(getContentPanel(), gridBagConstraints2);
        }

        return tabPane;
    }

    /**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPButton() {
		if (pButton == null) {
			FlowLayout flowLayout1 = new FlowLayout();
			flowLayout1.setAlignment(java.awt.FlowLayout.RIGHT);
			flowLayout1.setHgap(0);
			flowLayout1.setVgap(0);
			pButton = new JPanel();
			pButton.setLayout(flowLayout1);
			pButton.add(getButtonPanel(), null);
		}
		return pButton;
	}


    /**
     * Obtiene el panel que contiene los botones de Aceptar, Cancelar y Aplicar
     * @return
     */
    protected JPanel getButtonPanel() {
        if (buttonPane == null) {
            GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
            gridBagConstraints3.gridx = 1;
            gridBagConstraints3.insets = new java.awt.Insets(0,3,0,3);
            gridBagConstraints3.gridy = 0;
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.insets = new java.awt.Insets(0,0,0,3);
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.gridx = 0;
            buttonPane = new JPanel();
            buttonPane.setLayout(new GridBagLayout());
            buttonPane.add(getAcceptButton(), gridBagConstraints1);
            buttonPane.add(getCancelButton(), gridBagConstraints3);
        }

        return buttonPane;
    }

    /**
     * This method initializes Accept button
     *
     * @return javax.swing.JButton
     */
    public JButton getAcceptButton() {
        if (acceptButton == null) {
            acceptButton = new JButton("Aceptar");
            acceptButton.setText(Messages.getText("Aceptar"));
        }

        return acceptButton;
    }

    /**
     * This method initializes Cancel Button
     *
     * @return javax.swing.JButton
     */
    public JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton("Cancelar");
            cancelButton.setText(Messages.getText("Cancelar"));
        }

        return cancelButton;
    }
}
