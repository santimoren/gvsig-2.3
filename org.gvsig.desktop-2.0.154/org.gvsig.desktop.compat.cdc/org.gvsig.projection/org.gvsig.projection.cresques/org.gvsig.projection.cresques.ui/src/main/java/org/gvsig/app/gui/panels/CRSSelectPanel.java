/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cresques.cts.IProjection;
import org.gvsig.app.gui.panels.crs.ICrsUIFactory;
import org.gvsig.app.gui.panels.crs.ISelectCRSButton;


public abstract class CRSSelectPanel extends JPanel implements ISelectCRSButton {
	private boolean transPanelActive = false;

	protected ActionListener actionListener = null;

	/**
	 * 
	 * @param panelClass
	 * @deprecated use CRSSelectPanelFactory.registerPanelClass
	 */
	public static void registerPanelClass(Class panelClass) {
		CRSSelectPanelFactory.registerPanelClass(panelClass);
	}

	/**
	 * 
	 * @param uiFactory
	 * @deprecated use CRSSelectPanelFactory.registerUIFactory
	 */
	public static void registerUIFactory(Class uiFactory) {
		CRSSelectPanelFactory.registerUIFactory(uiFactory);
	}

	/**
	 * 
	 * @param proj
	 * @return CRSSelectPanel
	 * @deprecated use CRSSelectPanelFactory.getPanel
	 */
	public static CRSSelectPanel getPanel(IProjection proj) {
		return CRSSelectPanelFactory.getPanel(proj);
	}
	
	/**
	 * 
	 * @return ICrsUIFactory
	 * @deprecated use CRSSelectPanelFactory.getUIFactory
	 */
	public static ICrsUIFactory getUIFactory() {
		return CRSSelectPanelFactory.getUIFactory();
	}

	
	public CRSSelectPanel(IProjection proj) {
		super();
	}
	
	abstract public JButton getJBtnChangeProj();
	
	abstract public JLabel getJLabel();
	
	abstract public IProjection getCurProj();
	
	abstract public boolean isOkPressed();
	/**
	 * @param actionListener The actionListener to set.
	 */
	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;
	}

	public boolean isTransPanelActive() {
		return transPanelActive;
	}

	public void setTransPanelActive(boolean transPanelActive) {
		this.transPanelActive = transPanelActive;
	}
		
}
