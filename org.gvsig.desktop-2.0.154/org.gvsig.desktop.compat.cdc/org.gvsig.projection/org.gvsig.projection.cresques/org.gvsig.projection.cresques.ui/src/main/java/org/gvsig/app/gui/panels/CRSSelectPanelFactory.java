/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.app.gui.panels;

import java.lang.reflect.InvocationTargetException;

import org.cresques.cts.IProjection;
import org.gvsig.app.gui.panels.crs.ICrsUIFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CRSSelectPanelFactory  {
	private static Logger logger = LoggerFactory.getLogger(CRSSelectPanelFactory.class);
	
	private static Class panelClass = null;
	private static Class uiFactory = null;

	public static void registerPanelClass(Class panelClass) {
		CRSSelectPanelFactory.panelClass = panelClass;
	}

	public static void registerUIFactory(Class uiFactory) {
		CRSSelectPanelFactory.uiFactory = uiFactory;
	}
	
	public static CRSSelectPanel getPanel(IProjection proj) {
		CRSSelectPanel panel = null;
		Class [] args = {IProjection.class};
		Object [] params = {proj};
		try {
			panel = (CRSSelectPanel) panelClass.getConstructor(args).newInstance(params);
		} catch (IllegalArgumentException e) {
			logger.error("Error creating CRS selection button", e);
		} catch (SecurityException e) {
			logger.error("Error creating CRS selection button", e);
		} catch (InstantiationException e) {
			logger.error("Error creating CRS selection button", e);
		} catch (IllegalAccessException e) {
			logger.error("Error creating CRS selection button", e);
		} catch (InvocationTargetException e) {
			logger.error("Error creating CRS selection button", e);
		} catch (NoSuchMethodException e) {
			logger.error("Error creating CRS selection button", e);
		}
		return panel;
	}
		
	public static ICrsUIFactory getUIFactory() {
		ICrsUIFactory factory = null;
		try {
			factory = (ICrsUIFactory) uiFactory.newInstance();
		} catch (InstantiationException e) {
			logger.error("Error creating CRS UI factory. Switching to default factory", e);
		} catch (IllegalAccessException e) {
			logger.error("Error creating CRS UI factory. Switching to default factory", e);
		}
		return factory;
	}
	
}
