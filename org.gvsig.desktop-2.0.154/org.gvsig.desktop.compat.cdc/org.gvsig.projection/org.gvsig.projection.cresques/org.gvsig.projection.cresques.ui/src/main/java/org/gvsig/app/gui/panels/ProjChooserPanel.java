/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * Created on 30-ene-2005
 */
package org.gvsig.app.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cresques.Messages;
import org.cresques.cts.IProjection;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;

/**
 * @author Luis W. Sevilla (sevilla_lui@gva.es)
 */
public class ProjChooserPanel extends CRSSelectPanel {

    private IProjection curProj = null;
    private JLabel jLblProj = null;
    private JLabel jLblProjName = null;
    private JButton jBtnChangeProj = null;
    private final JPanel jPanelProj = null;
    private boolean okPressed = false;

    /**
      *
      * @param proj
      */
    public ProjChooserPanel(IProjection proj) {
        super(proj);
        setCurProj(proj);
        initialize();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        GridLayout gl = new GridLayout(1,2);
        
        this.setLayout(gl); // new FlowLayout(FlowLayout.LEFT, 15, 0));
        this.add(getJLblProjName());
        
        JPanel secondHalf = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;   //request any extra vertical space
        c.gridx = 0;      
        c.gridwidth = 1;  
        c.gridy = 0;      
        secondHalf.add(getJLblProj(), c);
        
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0.0;  
        c.gridx = 1;      
        secondHalf.add(getJBtnChangeProj(), c);

        this.add(secondHalf);
        initBtnChangeProj();
    }

    private void initBtnChangeProj() {
        getJBtnChangeProj().addActionListener(
            new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    okPressed = false;

                    ISelectCrsPanel csSelect =
                        getUIFactory().getSelectCrsPanel(curProj, true);

                    ToolsSwingLocator.getWindowManager().showWindow((JComponent) csSelect, Messages.getText("selecciona_sistema_de_referencia"), MODE.DIALOG);

                    if (csSelect.isOkPressed()) {
                        curProj = csSelect.getProjection();
                        jLblProj.setText(curProj.getAbrev());
                        okPressed = true;
                        if (actionListener != null) {
                            actionListener.actionPerformed(e);
                        }
                    }
                }
            });
    }

    public JLabel getJLblProjName() {
        if (jLblProjName == null) {
            jLblProjName = new JLabel("Proyeccion actual");
            jLblProjName.setText(Messages.getText("__proyeccion_actual")); //$NON-NLS-1$
        }
        return jLblProjName;
    }

    @Override
    public JLabel getJLabel() {
        return getJLblProjName();
    }

    public JLabel getJLblProj() {
        if (jLblProj == null) {
            jLblProj = new JLabel();
            jLblProj.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            if (curProj != null)
                jLblProj.setText(curProj.getAbrev());
        }
        return jLblProj;
    }

    public void addBtnChangeProjActionListener(java.awt.event.ActionListener al) {
        jBtnChangeProj.addActionListener(al);
    }

    /**
     * This method initializes jButton
     * 
     * @return javax.swing.JButton
     */
    @Override
    public JButton getJBtnChangeProj() {
        if (jBtnChangeProj == null) {
            jBtnChangeProj = new JButton();
            jBtnChangeProj.setText("..."); //$NON-NLS-1$
            // jBtnChangeProj.setPreferredSize(new java.awt.Dimension(26, 26));
        }
        return jBtnChangeProj;
    }

    /**
     * @return Returns the curProj.
     */
    @Override
    public IProjection getCurProj() {
        return curProj;
    }

    /**
     * @param curProj
     *            The curProj to set.
     */
    @Override
    public void setCurProj(IProjection curProj) {
        this.curProj = curProj;
    }

    /**
     * @return Returns the okPressed.
     */
    @Override
    public boolean isOkPressed() {
        return okPressed;
    }
} // @jve:decl-index=0:visual-constraint="10,10"
