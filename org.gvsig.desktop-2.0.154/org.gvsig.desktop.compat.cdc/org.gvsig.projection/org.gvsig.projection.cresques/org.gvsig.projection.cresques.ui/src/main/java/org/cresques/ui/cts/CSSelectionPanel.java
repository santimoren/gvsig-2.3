/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.cresques.ui.cts;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.cresques.Messages;
import org.cresques.cts.IProjection;
import org.cresques.ui.LoadableComboBox;

//import es.gva.cit.geoexplorer.ui.LoadableComboBox;

/**
 * Panel de edici�n de Sistemas de referencia
 * 
 * @author "Luis W. Sevilla" <sevilla_lui@gva.es>
 */
public class CSSelectionPanel extends JPanel {

    final private static long serialVersionUID = -3370601314380922368L;
    private LoadableComboBox datumComboBox = null;
    private LoadableComboBox projComboBox = null;
    private LoadableComboBox huseComboBox = null;
    private JLabel jLabel = null;
    private JLabel jLabel1 = null;
    private JLabel jLabel2 = null;
    private String tit;
    private CSSelectionModel model;

    /**
     * Constructor de la clase.
     */
    public CSSelectionPanel(String tit) {
        super();

        if (tit == null) {
            tit = Messages.getText("reference_system");
            if (tit == null)
                tit = "Reference System";
        }

        this.tit = tit;
        setModel(new CSSelectionModel());
        initialize();
    }

    /**
     * Inicializa el panel.
     * 
     * @return javax.swing.JPanel
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(2, 5, 2, 5);
        c.gridy = -1;

        setBorder(BorderFactory.createCompoundBorder(null, BorderFactory
            .createTitledBorder(null, Messages.getText("reference_system"),
                TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, null, null)));

        jLabel = new JLabel(Messages.getText("datum") + ":");
        addRow(c, jLabel, getDatumComboBox());

        jLabel1 = new JLabel(Messages.getText("projection") + ":");
        addRow(c, jLabel1, getProjComboBox());

        jLabel2 = new JLabel(Messages.getText("zone") + ":");
        addRow(c, jLabel2, getHuseComboBox());

        setHuseComboBoxEnabled(false);
    }

    public void setModel(CSSelectionModel model) {
        this.model = model;

        getHuseComboBox().loadData(model.getZoneList());
        getDatumComboBox().loadData(model.getDatumList());
        getProjComboBox().loadData(model.getProjectionList());
    }

    private void setHuseComboBoxEnabled(boolean enabled) {
        if (jLabel2 != null) {
            jLabel2.setEnabled(enabled);
        }

        getHuseComboBox().setEnabled(enabled);
    }

    private void setDatumComboBoxEnabled(boolean enabled) {
        if (jLabel != null) {
            jLabel.setEnabled(enabled);
        }

        getDatumComboBox().setEnabled(enabled);
    }

    public void setProjection(IProjection proj) {
        model.setProjection(proj);

        setDatumComboBoxEnabled(true);
        getDatumComboBox().setSelectedIndex(model.getSelectedDatum());

        getProjComboBox().removeAllItems();
        getProjComboBox().loadData(model.getProjectionList());

        model.setProjection(proj);
        getProjComboBox().setSelectedIndex(model.getSelectedProj());
        model.setProjection(proj);

        if (model.getSelectedZone() >= 0) {
            setHuseComboBoxEnabled(true);
            getHuseComboBox().removeAllItems();
            getHuseComboBox().loadData(model.getZoneList());

            model.setProjection(proj);
            getHuseComboBox().setSelectedIndex(model.getSelectedZone());
        } else {
            setHuseComboBoxEnabled(false);
            getHuseComboBox().setSelectedIndex(0);
        }
    }

    /**
     * Inicializa datumComboBox
     * 
     * @return javax.swing.JComboBox
     */
    private LoadableComboBox getDatumComboBox() {
        if (datumComboBox == null) {
            datumComboBox = new LoadableComboBox();

            datumComboBox.addItemListener(new java.awt.event.ItemListener() {

                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    model.setSelectedDatum(e.getItem());
                    getProjComboBox().removeAllItems();
                    getProjComboBox().loadData(model.getProjectionList());
                }
            });
        }

        return datumComboBox;
    }

    /**
     * Inicializa projComboBox
     * 
     * @return javax.swing.JComboBox
     */
    private LoadableComboBox getProjComboBox() {
        if (projComboBox == null) {
            projComboBox = new LoadableComboBox();
            // projComboBox.setBounds(14, 80, 250, 23);
            projComboBox.addItemListener(new java.awt.event.ItemListener() {

                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    model.setSelectedProj(e.getItem());

                    if (model.getSelectedProjType() == CSSelectionModel.TRANSVERSAL) {
                        setHuseComboBoxEnabled(true);
                        getHuseComboBox().removeAllItems();
                        getHuseComboBox().loadData(model.getZoneList());

                    } else {
                        setHuseComboBoxEnabled(false);
                    }

                }
            });
        }

        return projComboBox;
    }

    /**
     * Inicializa usoComboBox
     * 
     * @return javax.swing.JComboBox
     */
    private LoadableComboBox getHuseComboBox() {
        if (huseComboBox == null) {
            huseComboBox = new LoadableComboBox();
            huseComboBox.addItemListener(new java.awt.event.ItemListener() {

                public void itemStateChanged(java.awt.event.ItemEvent e) {
                    model.setSelectedZone(e.getItem());
                }
            });
        }

        return huseComboBox;
    }

    /**
     * @return
     */
    public IProjection getProjection() {
        return model.getProjection();
    }

    private void addRow(GridBagConstraints c, JComponent label, JComponent text) {
        c.anchor = GridBagConstraints.WEST;
        c.weightx = 0.0d;
        c.gridx = 0;
        c.gridy++;
        add(label, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0d;
        c.gridx = 1;
        add(text, c);
    }
}
