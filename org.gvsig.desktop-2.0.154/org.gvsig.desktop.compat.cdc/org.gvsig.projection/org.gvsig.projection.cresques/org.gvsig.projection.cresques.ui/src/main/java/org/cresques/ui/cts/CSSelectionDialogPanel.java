/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.cresques.ui.cts;

import javax.swing.JPanel;

import org.cresques.Messages;
import org.cresques.impl.cts.ProjectionPool;
import org.cresques.ui.DefaultDialogPanel;

/**
 * Dialogo para abrir fichero.
 * 
 * @author "Luis W. Sevilla" <sevilla_lui@gva.es>
 */
public class CSSelectionDialogPanel extends DefaultDialogPanel {

    final private static long serialVersionUID = -3370601314380922368L;

    public CSSelectionPanel getProjPanel() {
        return (CSSelectionPanel) getContentPanel();
    }

    protected JPanel getContentPanel() {
        if (contentPane == null) {
            contentPane =
                new CSSelectionPanel(Messages.getText("reference_system"));

            ((CSSelectionPanel) contentPane).setProjection(new ProjectionPool()
                .get("EPSG:32619"));
        }

        return contentPane;
    }
}
