/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * Created on 26-ene-2005
 */
package org.gvsig.app.project.documents.view.info.gui;

import java.awt.Dimension;

import org.cresques.Messages;
import org.cresques.cts.IProjection;
import org.cresques.ui.cts.CSSelectionDialogPanel;
import org.gvsig.andami.ui.mdiManager.WindowInfo;
import org.gvsig.app.gui.panels.crs.ISelectCrsPanel;

public class CSSelectionDialog extends CSSelectionDialogPanel implements ISelectCrsPanel  {

    private boolean okPressed = false;
    private IProjection lastProj = null;

    /**
	 *
	 */
    public CSSelectionDialog() {
        super();
        this.init();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void init() {
        getAcceptButton().addActionListener(
            new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    closeWindow();
                    okPressed = true;
                }
            });
        getCancelButton().addActionListener(
            new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {
                    setProjection(lastProj);
                    closeWindow();
                    okPressed = false;
                }
            });
    }

    public void closeWindow() {
    	this.setVisible(false);
    }
    
    public boolean isOkPressed() {
        return okPressed;
    }

    /**
     * @return
     */
    public IProjection getProjection() {
        return getProjPanel().getProjection();
    }

    /**
     * @param proj
     */
    public void setProjection(IProjection proj) {
        lastProj = proj;
        getProjPanel().setProjection(proj);
    }

	public WindowInfo getWindowInfo() {
        WindowInfo m_viewinfo =
                new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
            m_viewinfo.setTitle(Messages.getText("selecciona_sistema_de_referencia"));
            Dimension dim = getPreferredSize();
            m_viewinfo.setWidth(dim.width);
            m_viewinfo.setHeight(dim.height);
            return m_viewinfo;
	}

	public Object getWindowProfile() {
        return WindowInfo.DIALOG_PROFILE;
	}

}
