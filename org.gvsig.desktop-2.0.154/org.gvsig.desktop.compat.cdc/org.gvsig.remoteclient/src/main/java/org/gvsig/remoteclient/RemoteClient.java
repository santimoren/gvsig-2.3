/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.remoteclient;

import org.gvsig.compat.net.ICancellable;


/**
 * <p></p>
 *
 */
public abstract class RemoteClient {

/**
 * <p>Represents ...</p>
 *
 */
    protected String hostName;

/**
 * <p>Represents ...</p>
 *
 */
    protected int port;

/**
 * <p>Represents ...</p>
 *
 */
    protected String serviceName;

/**
 * <p>Represents ...</p>
 *
 */
    private String type;

/**
 * <p>Represents ...</p>
 *
 */
    private String subtype;

/**
 * <p>Represents ...</p>
 *
 *
 * @return
 */
    public String getHost() {
        return hostName;
    }

/**
 * <p>Represents ...</p>
 *
 *
 * @param _hostName
 */
    public void setHost(String _hostName) {
        hostName = _hostName;
    }

/**
 * <p>Represents ...</p>
 *
 *
 * @return
 */
    public int getPort() {
        // your code here
        return port;
    }

/**
 * <p>Does ...</p>
 *
 *
 * @param _port
 */
    public void setPort(int _port) {
        port = _port;
    }

/**
 * <p>Does ...</p>
 *
 *
 * @return
 */
    public String getServiceName() {
        // your code here
        return serviceName;
    }

/**
 * <p>Does ...</p>
 *
 *
 * @param _serviceName
 */
    public void setServiceName(String _serviceName) {
        serviceName = _serviceName;
    }

/**
 * <p>Does ...</p>
 *
 */
    public abstract boolean connect(boolean override, ICancellable cancel);

/**
 * <p>Does ...</p>
 *
 */
    public abstract void close();

/**
 * <p>Represents ...</p>
 *
 *
 * @return
 */
    public String getType() {
        return type;
    }

/**
 * <p>Represents ...</p>
 *
 *
 * @param _type
 */
    public void setType(String _type) {
        type = _type;
    }

/**
 * <p>Represents ...</p>
 *
 *
 * @return
 */
    public String getSubtype() {
        return subtype;
    }

/**
 * <p>Represents ...</p>
 *
 *
 * @param _subtype
 */
    public void setSubtype(String _subtype) {
        subtype = _subtype;
    }
 }
