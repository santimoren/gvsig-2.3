/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.remoteclient.utils;

public class BoundaryBox {
    
    private double xmin;

    private double xmax;
    private double ymin;
    private double ymax;
    private String srs;
    public double getXmin() {        
        return xmin;
    } 
    public void setXmin(double _xmin) {        
        xmin = _xmin;
    } 
    public double getXmax() {        
        return xmax;
    } 
    public void setXmax(double _xmax) {        
        xmax = _xmax;
    } 
    public double getYmin() {        
        return ymin;
    } 
    public void setYmin(double _ymin) {        
        ymin = _ymin;
    } 
    public double getYmax() {        
        return ymax;
    } 
    public void setYmax(double _ymax) {        
        ymax = _ymax;
    } 
    public String getSrs() {        
        return srs;
    } 
    public void setSrs(String _srs) {        
        srs = _srs;
    }
    
    public String toString(){
        String s = srs + " (" + xmin + ", " + ymin + ", " + xmax + ", " + ymax + ")";
        
        return s;
    }
    
    public static boolean validLat(double v) {
        return v >= -90 && v <= 90;
    }
    
    public static boolean validLon(double v) {
        return v >= -180 && v <= 180;
    }
    
    public static BoundaryBox clipGeodeticBBox(BoundaryBox bbox) {
        
        BoundaryBox resp = new BoundaryBox();
        resp.setSrs(bbox.getSrs());
        resp.setXmin(bbox.getXmin());
        resp.setXmax(bbox.getXmax());
        resp.setYmin(bbox.getYmin());
        resp.setYmax(bbox.getYmax());
        
        // ==================================================
        // Fix longitude
        // ==================================================
        if (resp.getXmax() > resp.getXmin()
            && validLon(resp.getXmin()) && !validLon(resp.getXmax())) {
            // only xmax is wrong:
            resp.setXmax(180);
        } else {
            if (resp.getXmax() > resp.getXmin()
                && !validLon(resp.getXmin()) && validLon(resp.getXmax())) {
                // only xmin is wrong:
                resp.setXmin(-180);
            } else {
                if (resp.getXmax() > resp.getXmin()
                    && validLon(resp.getXmin()) && validLon(resp.getXmax())) {
                    // OK, do nothing
                } else {
                    resp.setXmax(180);
                    resp.setXmin(-180);
                }
            }
        }
        // ==================================================
        // Fix latitude
        // ==================================================
        if (resp.getYmax() > resp.getYmin()
            && validLat(resp.getYmin()) && !validLat(resp.getYmax())) {
            // only ymax is wrong:
            resp.setYmax(90);
        } else {
            if (resp.getYmax() > resp.getYmin()
                && !validLat(resp.getYmin()) && validLat(resp.getYmax())) {
                // only ymin is wrong:
                resp.setYmin(-90);
            } else {
                if (resp.getYmax() > resp.getYmin()
                    && validLat(resp.getYmin()) && validLat(resp.getYmax())) {
                    // OK, do nothing
                } else {
                    // box was a mess, let's fix it
                    resp.setYmax(90);
                    resp.setYmin(-90);
                }
            }
        }
        // ===============================================
        return resp;
        
    }
 }
