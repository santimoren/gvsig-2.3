/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.remoteclient;

import org.gvsig.remoteclient.ogc.OGCClientOperation;

/**
 * This class represents the client status at a certain moment
 * it describes the "graphic" situation or the requirements of the data
 * to be retrieved.
 * 
 */
public abstract class RemoteClientStatus {

	// width and heigh of the map
    private int width;
    private int height;
    
    //format of the image to be retrieved
    private String format;
    private String infoFormat;
    // spatial reference system of the image to be retrieved
    private String srs;
    // exception format, to be retrieved in case of error
    private String exceptionFormat;
    
    //To set if the client has to use GET or POST
	private int protocol = OGCClientOperation.PROTOCOL_UNDEFINED;

	public int getWidth() {        
        return width;
    }
    
    public void setWidth(int _width) {        
    	width = _width;
    } 

    public int getHeight() {                
        return height;
    } 
    public void setHeight(int _height) {        
        height = _height;
    } 

    public String getFormat() {        
        return format;
    }
    
    public void setFormat(String _format) {        
        format = _format;
    } 
    
    public String getInfoFormat() {        
        return infoFormat;
    } 
    
    public void setInfoFormat(String _format) {        
        infoFormat = _format;
    } 

    public String getSrs() {        
        return srs;
    } 

    public void setSrs(String _srs) {        
        srs = _srs;
    } 

    public void setExceptionFormat(String _format) {        
        exceptionFormat = _format;
    } 
    
    public String getExceptionFormat() {        
        return exceptionFormat;
    }
    
    /**
	 * @return the protocol
	 */
	public int getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(int protocol) {
		this.protocol = protocol;
	}
 
 }
