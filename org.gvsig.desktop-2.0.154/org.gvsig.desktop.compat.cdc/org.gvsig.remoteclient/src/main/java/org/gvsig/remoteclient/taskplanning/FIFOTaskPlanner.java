/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.remoteclient.taskplanning;

/**
 * A simple FIFO task planner. The tasks returned by this planner are executed
 * enterely. It does not issue another task until the current is finished. 
 * @author jaume
 *
 */
public class FIFOTaskPlanner implements ITaskPlanner {
	IQueue queue;
	
	/**
	 * Creates a new instance of FIFOTaskPlanner that will work against the
	 * queue passed as paramenter 
	 * @param queue, the IQueue to be planned
	 */
	public FIFOTaskPlanner(IQueue queue) {
		this.queue = queue;
	}

	
	public IRunnableTask nextTask() {
		synchronized (this) {
			return (IRunnableTask) queue.getTasks().remove(0);
		}
	}
	/**
	 * FIFO plans have no previous tasks so, null is always returned.
	 */
	public IRunnableTask previousTask() {
		return null;
	}

}
