/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.remoteclient.ogc.request;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import org.gvsig.compat.CompatLocator;
import org.gvsig.compat.lang.StringUtils;
import org.gvsig.compat.net.ICancellable;
import org.gvsig.remoteclient.RemoteClientStatus;
import org.gvsig.remoteclient.ogc.OGCClientOperation;
import org.gvsig.remoteclient.ogc.OGCProtocolHandler;
import org.gvsig.remoteclient.utils.Utilities;

public abstract class OGCRequest {
	protected RemoteClientStatus status = null;
	protected OGCProtocolHandler protocolHandler = null;
	protected boolean isDeleted = false;
	protected static final String XMLTAG_STARTCHARACTER = "<";
	protected static final String XMLTAG_FINISHCHARACTER = "</";
	protected static final String XMLTAG_ENDCHARACTER = ">";
	
	private static final StringUtils stringUtils = CompatLocator.getStringUtils();

	public OGCRequest(RemoteClientStatus status, OGCProtocolHandler protocolHandler) {
		super();
		this.status = status;
		this.protocolHandler = protocolHandler;
	}	

	/**
	 * Send a request to the server.
	 * @return
	 * The server reply
	 * @throws IOException 
	 * @throws UnknownHostException 
	 * @throws ConnectException 
	 */
	public File sendRequest(ICancellable cancel) throws ConnectException, UnknownHostException, IOException{
		//if the status is null is because is a GetCapabilities operation
		if (status != null){
			if (status.getProtocol() != OGCClientOperation.PROTOCOL_UNDEFINED){
				if (status.getProtocol() == OGCClientOperation.PROTOCOL_GET){
					String onlineResource = protocolHandler.getHost();
					String symbol = getSymbol(onlineResource);
					onlineResource = onlineResource + symbol;
					return sendHttpGetRequest(onlineResource, cancel);
				}else{
					String onlineResource = protocolHandler.getHost();
					return sendHttpPostRequest(onlineResource);
				}
			}
		}

		//if exists an online resource for the GET operation
		String onlineResource = protocolHandler.getServiceInformation().getOnlineResource(getOperationName(), OGCClientOperation.PROTOCOL_GET);
		if (onlineResource != null){
			String symbol = getSymbol(onlineResource);
			onlineResource = onlineResource + symbol;
			return sendHttpGetRequest(onlineResource, cancel);
		}
		//if exists an online resource for the POST operation
		onlineResource =  protocolHandler.getServiceInformation().getOnlineResource(getOperationName(), OGCClientOperation.PROTOCOL_POST);
		if (onlineResource != null){
			return sendHttpPostRequest(onlineResource);
		}
		//If the online resource doesn't exist, it tries with the server URL and GET
		onlineResource = protocolHandler.getHost();
		String symbol = getSymbol(onlineResource);
		onlineResource = onlineResource + symbol;
		return sendHttpGetRequest(onlineResource, cancel);
	}

	protected abstract String getHttpGetRequest(String onlineResource);

	protected abstract String getHttpPostRequest(String onlineResource);

	protected abstract String getTempFilePrefix();

	protected abstract String getOperationName();

	protected abstract String getSchemaLocation();	

	/**
	 * @return the URL used in the HTTP get operation
	 * @throws MalformedURLException
	 */
	public URL getURL() throws MalformedURLException{
		String onlineResource = protocolHandler.getServiceInformation().getOnlineResource(getOperationName(), OGCClientOperation.PROTOCOL_GET);
		if (onlineResource != null){
			String symbol = getSymbol(onlineResource);
			onlineResource = onlineResource + symbol;
			return new URL(getHttpGetRequest(onlineResource));
		}
		
		//If the online resource doesn't exist, it tries with the server URL and GET
		onlineResource = protocolHandler.getHost();
		String symbol = getSymbol(onlineResource);
		onlineResource = onlineResource + symbol;
		return new URL(getHttpGetRequest(onlineResource));
	}

	/**
	 * Send a Http request using the get protocol
	 * @param onlineResource
	 * @return
	 * @throws ConnectException
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private File sendHttpGetRequest(String onlineResource, ICancellable cancel) throws ConnectException, UnknownHostException, IOException{
		URL url = new URL(stringUtils.replaceAll(getHttpGetRequest(onlineResource), " ", "%20"));
		if (isDeleted()){
			Utilities.removeURL(url);
		}
		return Utilities.downloadFile(url, getTempFilePrefix(), cancel);		
	}

	/**
	 * Send a Http request using the post protocol
	 * @param onlineResource
	 * @return
	 * @throws ConnectException
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private File sendHttpPostRequest(String onlineResource) throws ConnectException, UnknownHostException, IOException{
		URL url = new URL(onlineResource);
		String data = getHttpPostRequest(onlineResource);
		if (isDeleted()){
			Utilities.removeURL(url+data);
		}
		return Utilities.downloadFile(url, data, getTempFilePrefix(), null);		
	}

	/**
	 * Just for not repeat code. Gets the correct separator according 
	 * to the server URL
	 * @param h
	 * @return
	 */
	protected static String getSymbol(String h) {
		String symbol;
		if (h.indexOf("?")==-1) 
			symbol = "?";
		else if (h.indexOf("?")!=h.length()-1)
			symbol = "&";
		else
			symbol = "";
		return symbol;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	protected String createXMLStartTag(String tagName){
	    return XMLTAG_STARTCHARACTER + tagName + XMLTAG_ENDCHARACTER;
	}
	
   protected String createXMLEndtTag(String tagName){
        return XMLTAG_FINISHCHARACTER + tagName + XMLTAG_ENDCHARACTER;
    }

}

