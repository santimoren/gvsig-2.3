/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.remoteclient.utils;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.gvsig.compat.CompatLocator;
import org.gvsig.compat.net.Downloader;
import org.gvsig.compat.net.ICancellable;




/**
 * Clase con m�todos de utilidad en el protocolo WMS
 *
 * @authors Laura D�az, jaume dominguez faus
 */
public class Utilities {
	private static final Downloader downloader = CompatLocator.getDownloader();
    private static String characters;
	static boolean canceled;
	static final long latency = 500;
	/**
	 * Used to cancel a group of files
	 * <b>key</b>: Group id, <b>value</b>: Boolean (true if
	 * the group has to be canceled. Otherwise it is
	 * false)
	 */
	static Hashtable canceledGroup = new Hashtable();
	/**
	 * <b>key</b>: URL, <b>value</b>: path to the downloaded file.
	 */
	private static Hashtable downloadedFiles;
	static Exception downloadException;
	private static final String tempDirectoryPath = System.getProperty("java.io.tmpdir")+"/tmp-andami";


	static {
		characters = "";
		for (int j = 32; j<=127; j++){
			characters += (char) j;
		}
		characters += "�������������������������������������������ǡ�����\n\r\f\t��";
	}


	/**
	 * Checks a File and tries to figure if the file is a text or a binary file.<br>
	 * Keep in mind that binary files are those that contains at least one
	 * non-printable character.
	 *
	 * @param file
	 * @return <b>true</b> when the file is <b>pretty problably</b> text,
	 * <b>false</b> if the file <b>is</b> binary.
	 */
	public static boolean isTextFile(File file){
		return isTextFile(file, 1024);
	}

	/**
	 * Checks a File and tries to figure if the file is a text or a binary file.<br>
	 * Keep in mind that binary files are those that contains at least one
	 * non-printable character.
	 *
	 * @param file
	 * @param byteAmount, number of bytes to check.
	 * @return <b>true</b> when the file is <b>pretty problably</b> text,
	 * <b>false</b> if the file <b>is</b> binary.
	 */
	public static boolean isTextFile(File file, int byteAmount){
		int umbral = byteAmount;
		try {
			FileReader fr = new FileReader(file);
			for (int i = 0; i < umbral; i++) {
				int c = fr.read();
				if (c==-1){
					// End of file. If we reach this
					// everything before is printable data.
					return true;
				}
				char ch = (char) c;
				if (characters.indexOf(ch)==-1){
					// We've found a non-printable character.
					// Then we'll assume that this file is binary.
					return false;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Checks a byte array and tells if it contains only text or contains
	 * any binary data.
	 *
	 * @param file
	 * @return <b>true</b> when the data is <b>only</b> text, <b>false</b> otherwise.
	 * @deprecated
	 */
	public static boolean isTextData(byte[] data){
		char[] charData = new char[data.length];
		for (int i = 0; i<data.length; i++){
			charData[i] = (char) data[i];
		}

		for (int i = 0; i < data.length; i++) {
			int c = charData[i];


			if (c==-1){
				// End of file. If we reach this
				// everything before is printable data.
				return true;
			}
			char ch = (char) c;
			if (characters.indexOf(ch)==-1){
				// We've found a non-printable character.
				// Then we'll assume that this file is binary.

				//System.out.println(ch+" at "+i);
				return false;
			}
		}
		return true;
	}




	/**
	 * Copia el contenido de un InputStream en un OutputStream
	 *
	 * @param in InputStream
	 * @param out OutputStream
	 */
	public static void serializar(InputStream in, OutputStream out) {
		byte[] buffer = new byte[102400];

		int n;

		try {
			while ((n = in.read(buffer)) != -1) {
				out.write(buffer, 0, n);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Elimina del xml la declaraci�n del DTD
	 *
	 * @param bytes bytes del fichero XML de respuesta a getCapabilities
	 * @param startTag Tag raiz del xml respuesta a getCapabilities
	 *
	 * @return bytes del fichero XML sin la declaraci�n del DTD
	 */
	public static byte[] eliminarDTD(byte[] bytes, String startTag) {
		String text = new String(bytes);
		int index1 = text.indexOf("?>") + 2;
		int index2;

		try {
			index2 = findBeginIndex(bytes, startTag);
		} catch (Exception e) {
			return bytes;
		}

		byte[] buffer = new byte[bytes.length - (index2 - index1)];
		System.arraycopy(bytes, 0, buffer, 0, index1);
		System.arraycopy(bytes, index2, buffer, index1, bytes.length - index2);

		return buffer;
	}

	/**
	 * Obtiene el �ndice del comienzo del xml
	 *
	 * @param bytes bytes del fichero XML en el que se busca
	 * @param tagRaiz Tag raiz del xml respuesta a getCapabilities
	 *
	 * @return �ndice donde empieza el tag raiz
	 *
	 * @throws Exception Si no se encuentra el tag
	 */
	private static int findBeginIndex(byte[] bytes, String tagRaiz)
	throws Exception {
		try {
			int nodo = 0;
			int ret = -1;

			int i = 0;

			while (true) {
				switch (nodo) {
				case 0:

					if (bytes[i] == '<') {
						ret = i;
						nodo = 1;
					}

					break;

				case 1:

					if (bytes[i] == ' ') {
					} else if (bytes[i] == tagRaiz.charAt(0)) {
						nodo = 2;
					} else {
						nodo = 0;
					}

					break;

				case 2:

					String aux = new String(bytes, i, 18);

					if (aux.equalsIgnoreCase(tagRaiz.substring(1))) {
						return ret;
					}

					nodo = 0;

					break;
				}

				i++;
			}
		} catch (Exception e) {
			throw new Exception("No se pudo parsear el xml", e);
		}
	}

	/**
	 * Converts the contents of a Vector to a comma separated list
	 *
	 * */
	public static String Vector2CS(Vector v)
	{
		String str = new String();
		if (v != null)
		{
			int i;
			for (i=0; i<v.size() ;i++)
			{
				str = str + v.elementAt(i);
				if (i<v.size()-1)
					str = str + ",";
			}
		}
		return str;
	}

	public static boolean isValidVersion(String version)
	{
		if(version.trim().length() == 5)
		{
			if ( (version.charAt(1)=='.') && (version.charAt(3)=='.'))
			{
				char x = version.charAt(0);
				char y = version.charAt(2);
				char z = version.charAt(4);

				if ((Character.isDigit(x)) && (Character.isDigit(y)) && (Character.isDigit(z)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * Crea un fichero temporal con un nombre concreto y unos datos pasados por
	 * par�metro.
	 * @param fileName Nombre de fichero
	 * @param data datos a guardar en el fichero
	 */
	public static void createTemp(String fileName, String data)throws IOException{
		File f = new File(fileName);
		DataOutputStream dos = new DataOutputStream( new BufferedOutputStream(new FileOutputStream(f)) );
		dos.writeBytes(data);
		dos.close();
		f.deleteOnExit();
	}

	/**
	 * Checks if a String is a number or not
	 *
	 * @param String, s
	 * @return boolean, true if s is a number
	 */
	public static boolean isNumber(String s)
	{
		try
		{
			//double d = Double.parseDouble(s);
			return true;
		}
		catch(NumberFormatException e)
		{
			return false;
		}

	}

	/**
	 * Parses the String containing different items [character] separated and
	 * creates a vector with them.
	 * @param str String contains item1[c]item2[c]item3...
	 * @param c is the string value for separating the items
	 * @return Vector containing all the items
	 */
	public static Vector createVector(String str, String c)
	{
		StringTokenizer tokens = new StringTokenizer(str, c);
		Vector v = new Vector();
		try
		{
			while (tokens.hasMoreTokens())
			{
				v.addElement(tokens.nextToken());
			}
			return v;
		}
		catch (Exception e)
		{
			return new Vector();
		}
	}

	/**
	 * @param dimensions
	 * @return
	 */
	public static String Vector2URLParamString(Vector v) {
		if (v==null) return "";
		String s = "";
		for (int i = 0; i < v.size(); i++) {
			s += v.get(i);
			if (i<v.size()-1)
				s += "&";
		}
		return s;
	}
	
	/**
	 * Downloads an URL into a temporary file that is removed the next time the
	 * tempFileManager class is called, which means the next time gvSIG is launched.
	 *
	 * @param url
	 * @param name
	 * @return
	 * @throws IOException
	 * @throws ServerErrorResponseException
	 * @throws ConnectException
	 * @throws UnknownHostException
	 */
	public static synchronized File downloadFile(URL url, String name, ICancellable cancel) throws IOException,ConnectException, UnknownHostException{
	    return downloader.downloadFile(url, name, cancel);
	}
	
	private static String calculateFileName(String name){
		int index = name.lastIndexOf(".");
		if (index > 0){
			return tempDirectoryPath + "/" + name.substring(0,index) + System.currentTimeMillis() + 
				name.substring(index, name.length());
		}
		return tempDirectoryPath+"/"+name+System.currentTimeMillis();
	}

	/**
	 * Downloads a URL using the HTTP Post protocol
	 * @param url
	 * The server URL
	 * @param data
	 * The data to send in the request
	 * @param name
	 * A common name for all the retrieved files
	 * @param cancel
	 * Used to cancel the downloads
	 * @return
	 * The retrieved file
	 * @throws IOException
	 * @throws ConnectException
	 * @throws UnknownHostException
	 */
	public static synchronized File downloadFile(URL url, String data, String name, ICancellable cancel) throws IOException,ConnectException, UnknownHostException{
	    return downloader.downloadFile(url, data, name, cancel);
	}
        
	public static synchronized File downloadFile(URL url, String data, String name, ICancellable cancel, int maxbytes) throws IOException,ConnectException, UnknownHostException{
	    return downloader.downloadFile(url, data, name, cancel,maxbytes);
	}

	/**
	 * Cleans every temporal file previously downloaded.
	 */
	public static void cleanUpTempFiles() {
		downloader.cleanUpTempFiles();
	}


	/**
	 * Remove an URL from the system cache. The file will remain in the file
	 * system for further eventual uses.
	 * @param request
	 */
	public static void removeURL(URL url) {
		downloader.removeURL(url);
	}

	/**
	 * Remove an URL from the system cache. The file will remain in the file
	 * system for further eventual uses.
	 * @param request
	 */
	public static void removeURL(Object url) {
	    downloader.removeURL(url);
	}
	
        
        public static String getAbsolutePathOrEmpty(File f) {
            if( f==null ) {
                return "";
            }
            return f.getAbsolutePath();
        }
        
        /**
	 * This class has to be deleted when all the classes uses the ICancellable
	 * method from libCompat
	 * @author gvSIG Team
	 * @version $Id: Utilities.java 33983 2010-10-27 12:37:48Z nbrodin $
	 * @deprecated You should use always org.gvsig.compat.net.ICancellable
	 */
	/*private static class CancellableAdapter implements org.gvsig.compat.net.ICancellable {
	    private ICancellable cancellable = null;

        public CancellableAdapter(
            org.gvsig.remoteclient.wms.ICancellable cancellable) {
            super();
            this.cancellable = cancellable;
        }

        public Object getID() {            
            return cancellable.getID();
        }

        public boolean isCanceled() {     
            return cancellable.isCanceled();
        } 	    
	}*/

}
