/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.remoteclient.taskplanning;

/**
 * Interface implemented by those tasks that can be background-executed,
 * cancelled or any other thing.
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface IRunnableTask {
	/**
	 * Executes this task's operations.
	 */
	public void execute();
	
	/**
	 * Cancels the current execution, if any, of this task. Should have no
	 * effect if the task is not executing anything.
	 */
	public void cancel();
	
	/**
	 * Tells if the task is on execution.
	 * @return true if the task is busy, false otherwise.
	 */
	public boolean isRunning();
	
	/**
	 * Returns the timeout set to this task in milliseconds
	 * @return the amount of milliseconds to wait until the task
	 * 		   will be considered as unsuccessful, or 0 or less to
	 * 		   say that task can wait forever.
	 */
	public long getTaskTimeout();
}
