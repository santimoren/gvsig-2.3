/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.remoteclient.taskplanning.retrieving;

/**
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * Luis W. Sevilla (sevilla_lui@gva.es)
 */
public class RetrieveEvent {
	public static final int NOT_STARTED = 0;
	public static final int CONNECTING = 1;
	public static final int TRANSFERRING = 2;
	public static final int REQUEST_FINISHED = 3;
	public static final int REQUEST_FAILED = 4;
	public static final int REQUEST_CANCELLED = 5;
	public static final int POSTPROCESSING = 6;
	
	/**
	 * redundant; use REQUEST_FAILED
	 * @deprecated ?
	 */
	public static final int ERROR = 11;
	
	private int eventType;
	
	public void setType(int type) {
		eventType = type;
	}

	public int getType() {
		return eventType;
	}
}
