/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.remoteclient.taskplanning;

/**
 * <p>
 * ITaskPlanner provides an interface to program your own task planning. It gives
 * you operations for pick a task from the queue according on the criteria that
 * you designed.<br>
 * </p>
 * <p>
 * The simplest implementation of ITaskPlanner would be a FIFO task planner (see
 * FIFOTaskPlanner.java) which takes jobs from a task queue in the same order
 * they were put. But any kind of planner is possible (SJF, LIFO, RoundRobin, etc.). 
 * </p>
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public interface ITaskPlanner {
	/**
	 * Takes the next task to be executed.
	 * @return IRunnableTask representing the next task to be executed
	 */
	public IRunnableTask nextTask();
	
	/**
	 * Takes the previous executed task. Notice that it may or may not have
	 * sense for specific implementations. For example, in a FIFO-like planner,
	 * the task is taken from the queue, executed until it is finished and 
	 * removed from the queue. So, there is no previous task.
	 * 
	 * @return IRunnableTask representing the previous executed task, or null
	 * if none.
	 * @deprecated (probably this is unuseful and i'll remove it)
	 */
	public IRunnableTask previousTask();
}
