
package org.gvsig.fmap.dal.feature;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DataQuery;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.undo.RedoException;
import org.gvsig.tools.undo.UndoException;
import org.gvsig.tools.visitor.Visitor;

/**
 * This class is intended to be used in test.
 * Use it directly or extend it and overwrite the methods you need.
 * This class is maintained as part of the DAL API.
 */


public class DummyFetureStore implements FeatureStore {

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean allowWrite() {
        return false;
    }

    @Override
    public FeatureType getDefaultFeatureType() throws DataException {
        return null;
    }

    @Override
    public FeatureType getFeatureType(String featureTypeId) throws DataException {
        return null;
    }

    @Override
    public List getFeatureTypes() throws DataException {
        return null;
    }

    @Override
    public DataStoreParameters getParameters() {
        return null;
    }

    @Override
    public boolean canWriteGeometry(int gvSIGgeometryType) throws DataException {
        return false;
    }

    @Override
    public Envelope getEnvelope() throws DataException {
        return null;
    }

    @Override
    public IProjection getSRSDefaultGeometry() throws DataException {
        return null;
    }

    @Override
    public void export(DataServerExplorer explorer, String provider, NewFeatureStoreParameters params) throws DataException {

    }

    @Override
    public FeatureSet getFeatureSet() throws DataException {
        return null;
    }

    @Override
    public FeatureSet getFeatureSet(FeatureQuery featureQuery) throws DataException {
        return null;
    }

    @Override
    public void getFeatureSet(FeatureQuery featureQuery, Observer observer) throws DataException {

    }

    @Override
    public void getFeatureSet(Observer observer) throws DataException {

    }

    @Override
    public List<Feature> getFeatures(FeatureQuery query, int pageSize) {
        return null;
    }

    @Override
    public Feature getFeatureByReference(FeatureReference reference) throws DataException {
        return null;
    }

    @Override
    public Feature getFeatureByReference(FeatureReference reference, FeatureType featureType) throws DataException {
        return null;
    }

    @Override
    public void edit() throws DataException {

    }

    @Override
    public void edit(int mode) throws DataException {

    }

    @Override
    public void cancelEditing() throws DataException {

    }

    @Override
    public void finishEditing() throws DataException {

    }

    @Override
    public void commitChanges() throws DataException {

    }

    @Override
    public boolean canCommitChanges() throws DataException {
        return false;
    }

    @Override
    public boolean isEditing() {
        return false;
    }

    @Override
    public boolean isAppending() {
        return false;
    }

    @Override
    public void update(EditableFeatureType featureType) throws DataException {

    }

    @Override
    public void update(EditableFeature feature) throws DataException {

    }

    @Override
    public void delete(Feature feature) throws DataException {

    }

    @Override
    public void insert(EditableFeature feature) throws DataException {

    }

    @Override
    public EditableFeature createNewFeature() throws DataException {
        return null;
    }

    @Override
    public EditableFeature createNewFeature(FeatureType type, Feature defaultValues) throws DataException {
        return null;
    }

    @Override
    public EditableFeature createNewFeature(FeatureType type, boolean defaultValues) throws DataException {
        return null;
    }

    @Override
    public EditableFeature createNewFeature(boolean defaultValues) throws DataException {
        return null;
    }

    @Override
    public EditableFeature createNewFeature(Feature defaultValues) throws DataException {
        return null;
    }

    @Override
    public void validateFeatures(int mode) throws DataException {

    }

    @Override
    public boolean isAppendModeSupported() {
        return false;
    }

    @Override
    public void beginEditingGroup(String description) throws NeedEditingModeException {

    }

    @Override
    public void endEditingGroup() throws NeedEditingModeException {

    }

    @Override
    public FeatureIndex createIndex(FeatureType featureType, String attributeName, String indexName) throws DataException {
        return null;
    }

    @Override
    public FeatureIndex createIndex(String indexTypeName, FeatureType featureType, String attributeName, String indexName) throws DataException {
        return null;
    }

    @Override
    public FeatureIndex createIndex(FeatureType featureType, String attributeName, String indexName, Observer observer) throws DataException {
        return null;
    }

    @Override
    public FeatureIndex createIndex(String indexTypeName, FeatureType featureType, String attributeName, String indexName, Observer observer) throws DataException {
        return null;
    }

    @Override
    public FeatureIndexes getIndexes() {
        return null;
    }

    @Override
    public void setSelection(FeatureSet selection) throws DataException {

    }

    @Override
    public FeatureSelection createFeatureSelection() throws DataException {
        return null;
    }

    @Override
    public FeatureSelection getFeatureSelection() throws DataException {
        return null;
    }

    @Override
    public boolean isLocksSupported() {
        return false;
    }

    @Override
    public FeatureLocks getLocks() throws DataException {
        return null;
    }

    @Override
    public FeatureStoreTransforms getTransforms() {
        return null;
    }

    @Override
    public FeatureQuery createFeatureQuery() {
        return null;
    }

    @Override
    public long getFeatureCount() throws DataException {
        return 0;
    }

    @Override
    public void createCache(String name, DynObject parameters) throws DataException {

    }

    @Override
    public FeatureCache getCache() {
        return null;
    }

    @Override
    public boolean isKnownEnvelope() {
        return false;
    }

    @Override
    public boolean hasRetrievedFeaturesLimit() {
        return false;
    }

    @Override
    public int getRetrievedFeaturesLimit() {
        return 0;
    }

    @Override
    public Feature getFeature(DynObject dynobject) {
        return null;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getFullName() {
        return null;
    }

    @Override
    public String getProviderName() {
        return null;
    }

    @Override
    public void refresh() throws DataException {

    }

    @Override
    public DataSet getDataSet() throws DataException {
        return null;
    }

    @Override
    public DataSet getDataSet(DataQuery dataQuery) throws DataException {
        return null;
    }

    @Override
    public void accept(Visitor visitor) throws BaseException {

    }

    @Override
    public void accept(Visitor visitor, DataQuery dataQuery) throws BaseException {

    }

    @Override
    public void getDataSet(Observer observer) throws DataException {

    }

    @Override
    public void getDataSet(DataQuery dataQuery, Observer observer) throws DataException {

    }

    @Override
    public DataSet getSelection() throws DataException {
        return null;
    }

    @Override
    public void setSelection(DataSet selection) throws DataException {

    }

    @Override
    public DataSet createSelection() throws DataException {
        return null;
    }

    @Override
    public Iterator getChildren() {
        return null;
    }

    @Override
    public DataServerExplorer getExplorer() throws DataException, ValidateDataParametersException {
        return null;
    }

    @Override
    public DataQuery createQuery() {
        return null;
    }

    @Override
    public Interval getInterval() {
        return null;
    }

    @Override
    public Collection getTimes() {
        return null;
    }

    @Override
    public Collection getTimes(Interval interval) {
        return null;
    }

    @Override
    public void disableNotifications() {

    }

    @Override
    public void enableNotifications() {

    }

    @Override
    public void beginComplexNotification() {

    }

    @Override
    public void endComplexNotification() {

    }

    @Override
    public void addObserver(Observer obsrvr) {

    }

    @Override
    public void deleteObserver(Observer obsrvr) {

    }

    @Override
    public void deleteObservers() {

    }

    @Override
    public void saveToState(PersistentState ps) throws PersistenceException {

    }

    @Override
    public void loadFromState(PersistentState ps) throws PersistenceException {

    }

    @Override
    public Object getMetadataID() throws MetadataException {
        return null;
    }

    @Override
    public String getMetadataName() throws MetadataException {
        return null;
    }

    @Override
    public Set getMetadataChildren() throws MetadataException {
        return null;
    }

    @Override
    public DynClass getDynClass() {
        return null;
    }

    @Override
    public void implement(DynClass dc) {

    }

    @Override
    public void delegate(DynObject d) {

    }

    @Override
    public Object getDynValue(String string) throws DynFieldNotFoundException {
        return null;
    }

    @Override
    public void setDynValue(String string, Object o) throws DynFieldNotFoundException {

    }

    @Override
    public boolean hasDynValue(String string) {
        return false;
    }

    @Override
    public Object invokeDynMethod(String string, Object[] os) throws DynMethodException {
        return null;
    }

    @Override
    public Object invokeDynMethod(int i, Object[] os) throws DynMethodException {
        return null;
    }

    @Override
    public void clear() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void undo() throws UndoException {

    }

    @Override
    public void undo(int i) throws UndoException {

    }

    @Override
    public void redo() throws RedoException {

    }

    @Override
    public void redo(int i) throws RedoException {

    }

    @Override
    public List getUndoInfos() {
        return null;
    }

    @Override
    public List getRedoInfos() {
        return null;
    }

    @Override
    public boolean canUndo() {
        return false;
    }

    @Override
    public boolean canRedo() {
        return false;
    }

}
