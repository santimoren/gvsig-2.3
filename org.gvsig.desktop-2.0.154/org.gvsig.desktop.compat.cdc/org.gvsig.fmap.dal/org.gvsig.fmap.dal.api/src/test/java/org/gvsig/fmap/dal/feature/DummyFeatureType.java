
package org.gvsig.fmap.dal.feature;

import java.util.Iterator;
import java.util.List;
import org.cresques.cts.IProjection;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynMethod;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;

/**
 * This class is intended to be used in test.
 * Use it directly or extend it and overwrite the methods you need.
 * This class is maintained as part of the DAL API.
 */


public class DummyFeatureType implements FeatureType {

    @Override
    public FeatureType getCopy() {
        return null;
    }

    @Override
    public FeatureRules getRules() {
        return null;
    }

    @Override
    public EditableFeatureType getEditable() {
        return null;
    }

    @Override
    public int getIndex(String name) {
        return 0;
    }

    @Override
    public Object get(String name) {
        return null;
    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public FeatureAttributeDescriptor getAttributeDescriptor(String name) {
        return null;
    }

    @Override
    public FeatureAttributeDescriptor getAttributeDescriptor(int index) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getDefaultGeometryAttributeName() {
        return null;
    }

    @Override
    public int getDefaultGeometryAttributeIndex() {
        return 0;
    }

    @Override
    public List getSRSs() {
        return null;
    }

    @Override
    public IProjection getDefaultSRS() {
        return null;
    }

    @Override
    public boolean hasEvaluators() {
        return false;
    }

    @Override
    public boolean hasOID() {
        return false;
    }

    @Override
    public boolean allowAutomaticValues() {
        return false;
    }

    @Override
    public FeatureAttributeDescriptor[] getAttributeDescriptors() {
        return null;
    }

    @Override
    public FeatureAttributeDescriptor[] getPrimaryKey() {
        return null;
    }

    @Override
    public FeatureAttributeDescriptor getDefaultGeometryAttribute() {
        return null;
    }

    @Override
    public FeatureAttributeDescriptor getDefaultTimeAttribute() {
        return null;
    }

    @Override
    public DynClass[] getSuperDynClasses() {
        return null;
    }

    @Override
    public DynMethod getDeclaredDynMethod(String string) throws DynMethodException {
        return null;
    }

    @Override
    public DynMethod[] getDeclaredDynMethods() throws DynMethodException {
        return null;
    }

    @Override
    public DynMethod getDynMethod(String string) throws DynMethodException {
        return null;
    }

    @Override
    public DynMethod[] getDynMethods() throws DynMethodException {
        return null;
    }

    @Override
    public DynMethod getDynMethod(int i) throws DynMethodException {
        return null;
    }

    @Override
    public void addDynMethod(DynMethod dm) {

    }

    @Override
    public void removeDynMethod(String string) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getNamespace() {
        return null;
    }

    @Override
    public String getFullName() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void setNamespace(String string) {

    }

    @Override
    public void setDescription(String string) {

    }

    @Override
    public DynField getDynField(String string) {
        return null;
    }

    @Override
    public DynField getDeclaredDynField(String string) {
        return null;
    }

    @Override
    public DynField[] getDynFields() {
        return null;
    }

    @Override
    public DynField[] getDeclaredDynFields() {
        return null;
    }

    @Override
    public DynField addDynField(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldBoolean(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldInt(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldLong(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldFloat(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldDouble(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldString(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldDate(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldList(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldArray(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldMap(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldSet(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldObject(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldFile(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldFolder(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldURL(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldURI(String string) {
        return null;
    }

    @Override
    public DynField addDynFieldSingle(String string, int i, Object o, boolean bln, boolean bln1) {
        return null;
    }

    @Override
    public DynField addDynFieldSingle(String string, int i, Object o) {
        return null;
    }

    @Override
    public DynField addDynFieldRange(String string, int i, Object o, Object o1, Object o2, boolean bln, boolean bln1) {
        return null;
    }

    @Override
    public DynField addDynFieldRange(String string, int i, Object o, Object o1, Object o2) {
        return null;
    }

    @Override
    public DynField addDynFieldChoice(String string, int i, Object o, DynObjectValueItem[] dovis, boolean bln, boolean bln1) {
        return null;
    }

    @Override
    public DynField addDynFieldChoice(String string, int i, Object o, DynObjectValueItem[] dovis) {
        return null;
    }

    @Override
    public void removeDynField(String string) {

    }

    @Override
    public void validate(DynObject d) throws DynObjectValidateException {

    }

    @Override
    public DynObject newInstance() {
        return null;
    }

    @Override
    public void extend(DynStruct ds) {

    }

    @Override
    public void extend(String string, String string1) {

    }

    @Override
    public void extend(String string) {

    }

    @Override
    public void remove(DynStruct ds) {

    }

    @Override
    public DynStruct[] getSuperDynStructs() {
        return null;
    }

    @Override
    public boolean isInstance(DynObject d) {
        return false;
    }

    @Override
    public boolean isExtendable(DynStruct ds) {
        return false;
    }

    @Override
    public Iterator<FeatureAttributeDescriptor> iterator() {
        return null;
    }

}
