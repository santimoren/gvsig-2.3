
package org.gvsig.fmap.dal.feature;

import java.text.DateFormat;
import java.util.List;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataType;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldIsNotAContainerException;
import org.gvsig.tools.dynobject.exception.DynFieldValidateException;
import org.gvsig.tools.evaluator.Evaluator;

/**
 * This class is intended to be used in test.
 * Use it directly or extend it and overwrite the methods you need.
 * This class is maintained as part of the DAL API.
 */

public class DummyFeatureAttributeDescriptor implements FeatureAttributeDescriptor {

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public FeatureAttributeDescriptor getCopy() {
        return null;
    }

    @Override
    public String getDataTypeName() {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getPrecision() {
        return 0;
    }

    @Override
    public Class getObjectClass() {
        return null;
    }

    @Override
    public int getMinimumOccurrences() {
        return 0;
    }

    @Override
    public int getMaximumOccurrences() {
        return 0;
    }

    @Override
    public boolean isPrimaryKey() {
        return false;
    }

    @Override
    public boolean allowNull() {
        return false;
    }

    @Override
    public Evaluator getEvaluator() {
        return null;
    }

    @Override
    public IProjection getSRS() {
        return null;
    }

    @Override
    public int getGeometryType() {
        return 0;
    }

    @Override
    public int getGeometrySubType() {
        return 0;
    }

    @Override
    public GeometryType getGeomType() {
        return null;
    }

    @Override
    public DateFormat getDateFormat() {
        return null;
    }

    @Override
    public int getIndex() {
        return 0;
    }

    @Override
    public Object getAdditionalInfo(String infoName) {
        return null;
    }

    @Override
    public boolean isAutomatic() {
        return false;
    }

    @Override
    public boolean isTime() {
        return false;
    }

    @Override
    public boolean isIndexed() {
        return false;
    }

    @Override
    public boolean allowIndexDuplicateds() {
        return false;
    }

    @Override
    public boolean isIndexAscending() {
        return false;
    }

    @Override
    public FeatureAttributeGetter getFeatureAttributeGetter() {
        return null;
    }

    @Override
    public void setFeatureAttributeGetter(FeatureAttributeGetter featureAttributeGetter) {

    }

    @Override
    public FeatureAttributeEmulator getFeatureAttributeEmulator() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public DataType getDataType() {
        return null;
    }

    @Override
    public String getSubtype() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return null;
    }

    @Override
    public String getGroup() {
        return null;
    }

    @Override
    public int getOder() {
        return 0;
    }

    @Override
    public boolean isMandatory() {
        return false;
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    @Override
    public boolean isHidden() {
        return false;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public boolean isContainer() {
        return false;
    }

    @Override
    public DynObjectValueItem[] getAvailableValues() {
        return null;
    }

    @Override
    public Object getMinValue() {
        return null;
    }

    @Override
    public Object getMaxValue() {
        return null;
    }

    @Override
    public Class getClassOfValue() {
        return null;
    }

    @Override
    public Class getClassOfItems() {
        return null;
    }

    @Override
    public DynField setDescription(String string) {
        return null;
    }

    @Override
    public DynField setType(int i) {
        return null;
    }

    @Override
    public DynField setType(DataType dt) {
        return null;
    }

    @Override
    public DynField setSubtype(String string) {
        return null;
    }

    @Override
    public DynField setDefaultFieldValue(Object o) {
        return null;
    }

    @Override
    public DynField setGroup(String string) {
        return null;
    }

    @Override
    public DynField setOrder(int i) {
        return null;
    }

    @Override
    public DynField setMandatory(boolean bln) {
        return null;
    }

    @Override
    public DynField setHidden(boolean bln) {
        return null;
    }

    @Override
    public DynField setPersistent(boolean bln) {
        return null;
    }

    @Override
    public DynField setAvailableValues(DynObjectValueItem[] dovis) {
        return null;
    }

    @Override
    public DynField setAvailableValues(List list) {
        return null;
    }

    @Override
    public DynField setMinValue(Object o) {
        return null;
    }

    @Override
    public DynField setMaxValue(Object o) {
        return null;
    }

    @Override
    public DynField setClassOfValue(Class type) throws DynFieldIsNotAContainerException {
        return null;
    }

    @Override
    public DynField setClassOfItems(Class type) throws DynFieldIsNotAContainerException {
        return null;
    }

    @Override
    public DynField setReadOnly(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField getElementsType() {
        return null;
    }

    @Override
    public DynField setElementsType(int i) throws DynFieldIsNotAContainerException {
        return null;
    }

    @Override
    public DynField setElementsType(DynStruct ds) throws DynFieldIsNotAContainerException {
        return null;
    }

    @Override
    public void validate(Object o) throws DynFieldValidateException {

    }

    @Override
    public Object coerce(Object o) throws CoercionException {
        return null;
    }

    @Override
    public int getTheTypeOfAvailableValues() {
        return 0;
    }

    @Override
    public DynField setDefaultDynValue(Object o) {
        return null;
    }

    @Override
    public DynField setTheTypeOfAvailableValues(int i) {
        return null;
    }

}
