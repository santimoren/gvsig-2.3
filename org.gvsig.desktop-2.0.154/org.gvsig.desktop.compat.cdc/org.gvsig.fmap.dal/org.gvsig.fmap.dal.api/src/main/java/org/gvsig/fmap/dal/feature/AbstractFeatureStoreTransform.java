/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * Abstract feature store transform intended for giving a partial default implementation 
 * of the {@link FeatureStoreTransform} interface to other transform implementations. It is recommended
 * to extend this class when implementing new {@link FeatureStoreTransform}s.
 * 
 * The {@link FeatureType} of this class is not persistent: it has to be generated
 * by the child implementations of this abstract class when they are created
 * using the persistence mechanism.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public abstract class AbstractFeatureStoreTransform implements
FeatureStoreTransform {
    public static final String METADATA_DEFINITION_NAME = "FeatureStoreTransform";
	public static final String ABSTRACT_FEATURESTORE_DYNCLASS_NAME = "AbstractFeatureStoreTransform";

	private FeatureStore store;

	private FeatureType defaultFeatureType = null;
	private List featureTypes = new ArrayList();

    protected String name;

    protected String descripcion;

    private DynObject originalMetadata = null;

    
    public AbstractFeatureStoreTransform() {
        this(null, "");
    }

    public AbstractFeatureStoreTransform(String name, String description) {
        if( name == null || "".equals(name) ) {
            this.name = this.getClass().getName();
        } else {
            this.name = name;
        }
        this.descripcion = description;

    }
    
	public String getDescription() {
	    return this.descripcion;
	}
	
	public String getName() {
	    return this.name;
	}
	
	public FeatureType getDefaultFeatureType() throws DataException {
		return defaultFeatureType;
	}

	public List getFeatureTypes() throws DataException {
		return featureTypes;
	}

	public void setFeatureStore(FeatureStore store) {
		this.store = store;
	}

	public FeatureStore getFeatureStore() {
		return store;
	}

	protected void setFeatureTypes(List types, FeatureType defaultType) {
		this.featureTypes.clear();
		this.featureTypes.addAll(types);
		this.defaultFeatureType = defaultType;
	}

	public static void registerPersistent() {
	    PersistenceManager persistenceManager = ToolsLocator.getPersistenceManager();
	    DynStruct definition = persistenceManager.getDefinition(ABSTRACT_FEATURESTORE_DYNCLASS_NAME);

	    if (definition == null){
	        definition = persistenceManager.addDefinition(
	            AbstractFeatureStoreTransform.class, 
	            ABSTRACT_FEATURESTORE_DYNCLASS_NAME, 
	            "AbstractFeatureStoreTransform Persistent definition", 
	            null, 
	            null
	        );

	        definition.addDynFieldObject("store")
	            .setClassOfValue(FeatureStore.class)
	            .setMandatory(true);
	    }
	}

	public void loadFromState(PersistentState state) throws PersistenceException {
		this.store = (FeatureStore) state.get("store");
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("store", store);	
	}

	
	public final void setSourceMetadata(DynObject metadata) {
		this.originalMetadata = metadata;
	}
	
	protected DynObject getSourceMetadata() {
		return this.originalMetadata;
	}
	
	/**
	 * Get the metadata value for the name <code>name</code>.
	 * 
	 * Overwrite this method to support that this transform overwrite the values
	 * of the statore's metadata.
	 * 
	 * @see {#Metadata.getDynValue}
	 */
	public Object getDynValue(String name) throws DynFieldNotFoundException {
		throw new DynFieldNotFoundException(name, "transform");
	}

	/**
	 * Return true is the value <code>name</name> has a value in the metadata
	 * 
	 * Overwrite this method to support that this transform overwrite the values
	 * of the statore's metadata.
	 * 
	 * @see {#Metadata.hasDynValue}
	 */
	public boolean hasDynValue(String name) {
		return false;
	}

	/**
	 * Set the value of a metadata.
	 * 
	 * Overwrite this method to support that this transform overwrite the values
	 * of the statore's metadata.
	 * 
	 * @see {#Metadata.getDynValue}
	 */
	public void setDynValue(String name, Object value)
			throws DynFieldNotFoundException {
		throw new DynFieldNotFoundException(name, "transform");
	}
	
	  
	public Object clone() throws CloneNotSupportedException {
	    return super.clone();
	    
	}

}
