/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.text.DateFormat;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.evaluator.Evaluator;

/**
 * A feature attribute descriptor contains information about
 * one of the attributes in a feature, such as its name, data type
 * or precision.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public interface FeatureAttributeDescriptor extends DynField {

	/**
	 * Returns a clone of this attribute descriptor
	 *
	 * @return FeatureAttributeDescriptor
	 * 						A new copy of this
	 */
	public FeatureAttributeDescriptor getCopy();

	/**
	 * Returns the name of this attribute's data type.
	 *
	 * @return
	 * 		a string containing the name of this attribute's data type.
	 */
	public String getDataTypeName();

	/**
	 * Returns a number that indicates the size of this attribute. See the
	 * documentation for the various constants of {@link DataTypes}
	 * for how to interpret this value. As an example, when the data type is
	 * {@link DataTypes#STRING}, this value indicates the maximum length of the string.
	 *
	 * @return
	 * 		an <code>int</code> indicating the size of the attribute.
	 */
	public int getSize();

	/**
	 * For attributes of type {@link DataTypes#DOUBLE} and {@link DataTypes#FLOAT}
	 * , this returns the maximum number of places after the decimal point. For
	 * other types, this must always return zero.
	 */
	public int getPrecision();

	/**
	 * For attributes of type {@link DataTypes#OBJECT},
	 * this returns the Java {@link Class} object that class or interface that
	 * all values of this attribute can be cast to.
	 */
	public Class getObjectClass();

	/**
	 * Returns the minimum number of occurrences of this attribute on a given
	 * feature.  The vast majority of data sources and data consumers will only
	 * function with this value being zero or one.  If the minimum number of
	 * occurrences is zero, this is equivalent, in SQL terms, to the attribute
	 * being nillable.
	 */
	public int getMinimumOccurrences();

	/**
	 * Returns the maximum number of occurrences of this attribute on a given
	 * feature.  The vast majority of data sources and data consumers will only
	 * function with this value being one.  A value of {@link Integer#MAX_VALUE}
	 * indicates that the maximum number of occurrences is unbounded.
	 */
	public int getMaximumOccurrences();

	/**
	 * Returns {@code true} if this attribute forms all or part of the unique identifying
	 * value for the feature it is contained by.  The primary key attributes uniquely
	 * identify this feature from other features of the same type.  This is different
	 * from the {@linkplain Feature#getReference()}, which must uniquely identify
	 * the {@link Feature} among all feature types.
	 */
	public boolean isPrimaryKey();

	/**
	 * Indicates whether this attribute accepts null values.
	 *
	 * @return
	 * 		true if this attribute can be null, false if not.
	 */
	public boolean allowNull();

	/**
	 * Returns an evaluator that will be used to calculate
	 * the value of this attribute
	 */
	public Evaluator getEvaluator();

	/**
	 * If this attribute is a {@link Geometry}, this method returns its
	 * Spatial Reference System.
	 *
	 * @return
	 * 		the SRS if this attribute is a {@link Geometry}, otherwise this method returns null.
	 */
	public IProjection getSRS();

	    /**
     * If this attribute is a {@link Geometry}, this method returns the specific
     * geometry type,
     * as defined in {@link Geometry.TYPES}.
     * 
     * @return
     *         One of {@link Geometry.TYPES}
     * @deprecated use {@link #getGeomType()} instead. To be removed in gvSIG
     *             2.1.
     */
	public int getGeometryType();

	    /**
     * If this attribute is a {@link Geometry}, this method returns the specific
     * geometry subtype,
     * as defined in {@link Geometry.SUBTYPES}.
     * 
     * @return
     *         One of {@link Geometry.SUBTYPES}
     * @deprecated use {@link #getGeomType()} instead. To be removed in gvSIG
     *             2.1.
     */
	public int getGeometrySubType();

    /**
     * Returns the {@link GeometryType} of the attribute if it is a geometry.
     * 
     * @return the geometry type
     */
    public GeometryType getGeomType();

	/**
	 * If this attribute is of type Date, then this method returns
	 * the date format set by the data store.
	 *
	 * @return
	 * 		a date format
	 */
	public DateFormat getDateFormat();

	/**
	 * Returns this attribute relative position within the {@link Feature}.
	 *
	 * @return
	 * 		an index
	 */
	public int getIndex();

	/**
	 * Returns additional information of the attribute
	 *
	 * @return info
	 *
	 */
	public Object getAdditionalInfo(String infoName);

	/**
	 * Returns if value is created automatically by the source
	 */
	public boolean isAutomatic();

	/**
	 * Gets if the attribute is a temporal attribute.
	 * @return
	 *         <code>true</code> if is a temporal attribute
	 */
	public boolean isTime();  

        /**
         * Return true if the attribute has and index in the table.
         * 
         * @return true if indexed.
         */
        public boolean isIndexed();
        public boolean allowIndexDuplicateds();
        public boolean isIndexAscending();
        
	/**
	 * Gets if the attribute has a {@link FeatureAttributeGetter}.
	 * @return
	 *             a FeatureAttributeGetter or null.
         * @deprecated use getFeatureAttributeGetterAndSetter
	 */
	public FeatureAttributeGetter getFeatureAttributeGetter();
	
	/**
	 * Sets the {@link FeatureAttributeGetter} that is used to update the 
	 * presentation of a field.
	 * @param featureAttributeGetter
	 *             the {@link FeatureAttributeGetter} to set.
         * @deprecated use setFeatureAttributeGetterAndSetter
	 */
	public void setFeatureAttributeGetter(FeatureAttributeGetter featureAttributeGetter);

     	/**
	 * Gets the attribute emulator associatted {@link FeatureAttributeEmulator} to this attribute.
	 * @return
	 *             a FeatureAttributeEmulator or null.
	 */
	public FeatureAttributeEmulator getFeatureAttributeEmulator();
	
}
