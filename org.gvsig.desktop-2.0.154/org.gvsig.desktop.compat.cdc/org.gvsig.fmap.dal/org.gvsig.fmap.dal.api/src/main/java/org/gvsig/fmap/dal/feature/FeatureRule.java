/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import org.gvsig.fmap.dal.exception.DataException;

/**
 * Represents a Feature validation rule. These rules are used to 
 * check Feature state integrity in editing mode.
 * 
 * Desde una rule que se ejecuta en el UPDATE podemos ejecutar un
 * update de esta feature en el store. Pero si la rule se ejecuta
 * en el FINISH_EDITING no es posible hacer update de ninguna feature
 * del store.
 *
 */
public interface FeatureRule {
	
	/**
	 * Returns the rule name
	 * 
	 * @return
	 * 		the rule name
	 */
	public String getName();
	
	/**
	 * Returns the rule description
	 * 
	 * @return
	 * 		the rule description
	 */
	public String getDescription();
	
	/**
	 * This is the method that applies this rule to the {@link Feature}, given also its associated {@link FeatureStore}.
	 * @param feature
	 * 			Feature to which apply the rule
	 * @param featureStore
	 * 			FeatureStore to which the Feature belongs
	 * @throws DataException
	 * 			if an error occurs during validation
	 */
	public void validate(Feature feature, FeatureStore featureStore)
			throws DataException;
	
	/**
	 * Indicates whether this rule should be checked at update.
	 * 
         * Only in this rules can be updated the feature in the store.
         * 
	 * @return
	 * 		true if this rule should be checked at update, otherwise false.
	 */
	public boolean checkAtUpdate();
	
	/**
	 * Returns true if this rule should be applied just when editing is being finished.
	 *
         * In this rules, can't be updated any feature in the store.
         * 
	 * @return	
	 * 		true if this rule should be applied when editing is being finished, otherwise false.
	 */
	public boolean checkAtFinishEditing();
}
