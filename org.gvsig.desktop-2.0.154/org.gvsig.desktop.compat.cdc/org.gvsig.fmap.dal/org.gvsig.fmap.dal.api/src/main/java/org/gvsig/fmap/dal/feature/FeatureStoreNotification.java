/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.tools.undo.command.Command;

public interface FeatureStoreNotification extends DataStoreNotification {



	public static final String BEFORE_UNDO = "before_Undo_DataStore";
	public static final String BEFORE_REDO = "before_Redo_DataStore";

	public static final String AFTER_UNDO = "after_Undo_DataStore";
	public static final String AFTER_REDO = "after_Redo_DataStore";

	public static final String BEFORE_REFRESH = "before_refresh";
	public static final String AFTER_REFRESH = "after_refresh";

	public static final String LOCKS_CHANGE = "after_LockedChange_DataStore";

	public static final String BEFORE_STARTEDITING = "before_StartEditing_DataStore";
	public static final String AFTER_STARTEDITING = "after_StartEditing_DataStore";

	public static final String BEFORE_CANCELEDITING = "before_CancelEditing_DataStore";
	public static final String AFTER_CANCELEDITING = "after_CancelEditing_DataStore";

	public static final String BEFORE_FINISHEDITING = "before_FinishEditing_DataStore";
	public static final String AFTER_FINISHEDITING = "after_FinishEditing_DataStore";


	public static final String BEFORE_UPDATE_TYPE = "before_Update_Type";
	public static final String AFTER_UPDATE_TYPE = "after_Update_Type";

	public static final String BEFORE_UPDATE = "before_Update_Feature";
	public static final String AFTER_UPDATE = "after_Update_Feature";

	public static final String BEFORE_DELETE = "before_Delete_Feature";
	public static final String AFTER_DELETE = "after_Delete_Feature";

	public static final String BEFORE_INSERT = "before_Insert_Feature";
	public static final String AFTER_INSERT = "after_Insert_Feature";

	public static final String LOAD_FINISHED = "Load_Finished";
	public static final String TRANSFORM_CHANGE = "Transform_Change";

    /**
     * Notifies about a Feature index filling operation being started. The
     * notification object will provide the index object.
     */
    public static final String INDEX_FILLING_STARTED = "Index_Filling_Started";
    /**
     * Notifies about a Feature index finished filling successfully. The
     * notification object will provide the index object.
     */
    public static final String INDEX_FILLING_SUCCESS = "Index_Filling_Success";
    /**
     * Notifies about a Feature index filling operation being cancelled. The
     * notification object will provide the index object.
     */
    public static final String INDEX_FILLING_CANCELLED =
        "Index_Filling_Cancelled";
    /**
     * Notifies about a Feature index finished operation error. The notification
     * will provide the source exception.
     */
    public static final String INDEX_FILLING_ERROR = "Index_Filling_Error";

	public Feature getFeature();

	public DataSet getCollectionResult();

	public boolean loadSucefully();

	public Exception getExceptionLoading();

	public Command getCommand();

	public EditableFeatureType getFeatureType();
}
