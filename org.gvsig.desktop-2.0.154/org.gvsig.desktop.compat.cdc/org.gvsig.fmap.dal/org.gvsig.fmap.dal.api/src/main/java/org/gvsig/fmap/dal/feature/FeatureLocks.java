/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.observer.WeakReferencingObservable;
import org.gvsig.tools.persistence.Persistent;

/**
 * Manages locks of Features.
 *
 * @author <a href="mailto:jjdelcerro@gvsig.org">Joaquin del Cerro</a>
 */

public interface FeatureLocks extends WeakReferencingObservable, Persistent {

	public void apply();

	public Iterator getGroups();

	public int getGroupsCount();

	public boolean isLocked(FeatureReference reference);

	public boolean isLocked(Feature feature);

	public long getLocksCount();

	public Iterator getLocks();

    void setDefaultTimeout(long milisecs);

    long getDefaultTimeout();



    boolean lock(FeatureReference reference);

    boolean lock(Feature feature);

    boolean lock(FeatureSet features) throws DataException;

    void lockAll() throws DataException;

    boolean unlock(FeatureReference reference);

    boolean unlock(Feature feature);

    boolean unlock(FeatureSet features) throws DataException;

    void unlockAll() throws DataException;

	public long getUnappliedLocksCount();

    Iterator getUnappliedLocks();

}
