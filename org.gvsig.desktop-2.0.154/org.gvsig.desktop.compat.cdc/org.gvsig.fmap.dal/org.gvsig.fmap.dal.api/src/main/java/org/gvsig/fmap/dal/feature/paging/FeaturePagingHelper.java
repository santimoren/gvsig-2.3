/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Add pagination to a FeatureCollection}
 */
package org.gvsig.fmap.dal.feature.paging;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.dynobject.DynObjectPagingHelper;
import org.gvsig.tools.exception.BaseException;

/**
 * Helper interface to access the values of a FeatureCollection by position.
 *
 * @author gvSIG team
 */
public interface FeaturePagingHelper extends DynObjectPagingHelper {

    /**
     * Returns the Feature located at the index position.
     *
     * @param index to locate the Feature in the Collection
     * @return the Feature located at the index position
     * @throws BaseException if there is an error getting the Feature
     */
    Feature getFeatureAt(long index) throws BaseException;

    /**
     * Returns all the values of the current loaded page.
     *
     * @return all the values of the current loaded page
     */
    Feature[] getCurrentPageFeatures();

    /**
     * Returns the FeatureStore used to fetch the data.
     *
     * @return the FeatureStore
     */
    FeatureStore getFeatureStore();

    /**
     * Returns the query used to load the Features.
     *
     * @return the query used to load the Features
     */
    FeatureQuery getFeatureQuery();

    /**
     * Returns the FeatureType used of the data.
     *
     * @return the FeatureType of the data
     */
    FeatureType getFeatureType();

    /**
     * Updates a {@link Feature} with the given {@link EditableFeature} in the
     * current {@link FeatureSet}. <br>
     *
     * @param feature an instance of {@link EditableFeature} with which to
     * update the associated {@link Feature}.
     *
     * @throws BaseException
     *
     * @see {@link FeatureSet#update(EditableFeature)}
     *      {@link FeaturePagingHelper#getFeatureSet()}
     */
    void update(EditableFeature feature) throws BaseException;

    /**
     * Deletes a {@link Feature} from current {@link FeatureSet}.<br>
     *
     * @param feature the {@link Feature} to delete.
     *
     * @throws BaseException
     *
     * @see {@link FeatureSet#delete(Feature)}
     *      {@link FeaturePagingHelper#getFeatureSet()}
     */
    void delete(Feature feature) throws BaseException;

    /**
     * Inserts a new feature in this {@link FeatureSet}. It needs to be an
     * instance of {@link EditableFeature} as it has not been stored yet.<br>
     *
     * Any {@link Iterator} from this store that was still in use can will not
     * reflect this change.
     *
     * @param feature the {@link EditableFeature} to insert.
     *
     * @throws BaseException
     *
     * @see {@link FeatureSet#insert(EditableFeature)}
     *      {@link FeaturePagingHelper#getFeatureSet()}
     */
    void insert(EditableFeature feature) throws BaseException;

    /**
     * Sets that the selected Features get returned first.
     */
    void setSelectionUp(boolean selectionUp);

    boolean isSelectionUp();

    /**
     * Return a List of Fearures with the contents of this PagingHelper
     *
     * @return List of Features
     */
    List asList();

    /**
     * Return a List of DynObjects with the contents of this PagingHelper
     *
     * @return List of DynObjects
     */
    List asListOfDynObjects();

    public FeatureSelection getSelection();

    public void setSelection(FeatureSelection selection);

}
