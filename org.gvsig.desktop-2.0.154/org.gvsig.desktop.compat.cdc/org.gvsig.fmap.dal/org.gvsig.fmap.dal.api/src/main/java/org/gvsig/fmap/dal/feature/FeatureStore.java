/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.Iterator;
import java.util.List;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.undo.UndoRedoStack;

/**
 * <p>
 * A FeatureStore is a type of store whose data consists on sets of
 * {@link Feature}(s). {@link Feature}(s) from the same FeatureStore can be of
 * different {@link FeatureType}(s) (as in GML format for instance).
 * </p>
 *
 * <p>
 * FeatureStore allows:
 * </p>
 * <ul>
 * <li>Obtaining the default {@link FeatureType}. A FeatureStore always has one
 * and only one default FeatureType.
 * <li>Obtaining the list of {@link FeatureType}(s) defined in the FeatureStore.
 * <li>Obtaining, filtering and sorting subsets of data ({@link FeatureSet})
 * through {@link FeatureQuery}, as well as background loading.
 * <li>Obtaining the total {@link Envelope} (AKA bounding box or extent) of the
 * store.
 * <li>Support for editing {@link FeatureType}(s).
 * <li>Obtaining information about contained {@link Geometry} types.
 * <li>Exporting to another store.
 * <li>Indexing.
 * <li>Selection.
 * <li>Locks management.
 * </ul>
 *
 */
public interface FeatureStore extends DataStore, UndoRedoStack, Cloneable {

    public static final String METADATA_DEFINITION_NAME = "FeatureStore";

    /** Indicates that this store is in query mode */
    final static int MODE_QUERY = 0;

    /** Indicates that this store is in full edit mode */
    final static int MODE_FULLEDIT = 1;

    /** Indicates that this store is in append mode */
    final static int MODE_APPEND = 2;

    /*
     * =============================================================
     *
     * information related services
     */

    /**
     * Indicates whether this store allows writing.
     *
     * @return
     *         true if this store can be written, false if not.
     */
    public boolean allowWrite();

    /**
     * Returns this store's default {@link FeatureType}.
     *
     * @return
     *         this store's default {@link FeatureType}.
     *
     * @throws DataException
     */
    public FeatureType getDefaultFeatureType() throws DataException;

    /**
     * Returns this store's featureType {@link FeatureType} matches with
     * featureTypeId.
     *
     * @param featureTypeId
     *
     * @return this store's default {@link FeatureType}.
     *
     * @throws DataException
     */
    public FeatureType getFeatureType(String featureTypeId)
        throws DataException;

    /**
     * Returns this store's {@link FeatureType}(s).
     *
     * @return a list with this store's {@link FeatureType}(s).
     *
     * @throws DataException
     */
    public List getFeatureTypes() throws DataException;

    /**
     * Returns this store's parameters.
     *
     * @return
     *         {@link DataStoreParameters} containing this store's parameters
     */
    public DataStoreParameters getParameters();

    /**
     *@throws DataException
     * @deprecated Mirar de cambiarlo a metadatos
     */
    public boolean canWriteGeometry(int gvSIGgeometryType) throws DataException;

    /**
     * Returns this store's total envelope (extent).
     *
     * @return this store's total envelope (extent) or <code>null</code> if
     *         store not have geometry information
     */
    public Envelope getEnvelope() throws DataException;

    /**
     *
     * @deprecated use getDefaultFeatureType().getDefaultSRS()
     * @return
     * @throws DataException
     */
    public IProjection getSRSDefaultGeometry() throws DataException;

    /**
     * Exports this store to another store.
     *
     * @param explorer
     *            {@link DataServerExplorer} target
     * @param params
     *            New parameters of this store that will be used on the target
     *            explorer
     *
     * @throws DataException
     *
     * @Deprecated this method is unstable
     */
    public void export(DataServerExplorer explorer, String provider,
        NewFeatureStoreParameters params) throws DataException;

    /*
     * =============================================================
     *
     * Query related services
     */

    /**
     * Returns all available features in the store.
     * <p>
     * <em>
     * <strong>NOTE:</strong> if you use this method to get a
     * {@link FeatureSet}, you  must get sure it is disposed
     * (@see {@link DisposableIterator#dispose()}) in any case, even if an
     * error occurs while getting the data. It is recommended to use the
     * <code>accept</code> methods instead, which handle everything for you.
     * Take into account the accept methods may use a fast iterator to
     * get the features.
     * </em>
     * </p>
     *
     * @see #accept(org.gvsig.tools.visitor.Visitor)
     *
     * @return a collection of features
     * @throws ReadException
     *             if there is any error while reading the features
     */
    FeatureSet getFeatureSet() throws DataException;

    /**
     * Returns a subset of features taking into account the properties and
     * restrictions of the FeatureQuery.
     * <p>
     * <em>
     * <strong>NOTE:</strong> if you use this method to get a
     * {@link FeatureSet}, you  must get sure it is disposed
     * (@see {@link DisposableIterator#dispose()}) in any case, even if an
     * error occurs while getting the data. It is recommended to use the
     * <code>accept</code> methods instead, which handle everything for you.
     * Take into account the accept methods may use a fast iterator to
     * get the features.
     * </em>
     * </p>
     *
     * @see #accept(org.gvsig.tools.visitor.Visitor,
     *      org.gvsig.fmap.dal.DataQuery)
     *
     * @param featureQuery
     *            defines the characteristics of the features to return
     * @return a collection of features
     * @throws ReadException
     *             if there is any error while reading the features
     */
    FeatureSet getFeatureSet(FeatureQuery featureQuery) throws DataException;

    /**
     * Loads a subset of features taking into account the properties and
     * restrictions of the FeatureQuery. Feature loading is performed by calling
     * the Observer, once each loaded Feature.
     *
     * @param featureQuery
     *            defines the characteristics of the features to return
     * @param observer
     *            to be notified of each loaded Feature
     * @throws DataException
     *             if there is any error while loading the features
     */
    void getFeatureSet(FeatureQuery featureQuery, Observer observer)
        throws DataException;

    /**
     * Loads all available feature in the store. The loading of Features is
     * performed by calling the Observer, once each loaded Feature.
     *
     * @param observer
     *            to be notified of each loaded Feature
     * @throws DataException
     *             if there is any error while loading the features
     */
    void getFeatureSet(Observer observer) throws DataException;

    /**
     * Return a paginated list of Features filtered by the query.
     * 
     * The returned List of Features is paginated, and the page size
     * used is "pageSize".
     * 
     * @param query to filter the returned feature list
     * @param pageSize the page size of the list
     * @return the list of features
     */
    public List<Feature> getFeatures(FeatureQuery query, int pageSize);
    
    /**
     * Returns the feature given its reference.
     *
     * @param reference
     *            a unique FeatureReference
     * @return
     *         The Feature
     *
     * @throws DataException
     *
     */
    public Feature getFeatureByReference(FeatureReference reference)
        throws DataException;

    /**
     * Returns the feature given its reference and feature type.
     *
     * @param reference
     *            a unique FeatureReference
     *
     * @param featureType
     *            FeatureType to which the requested Feature belongs
     *
     * @return
     *         The Feature
     *
     * @throws DataException
     *
     */
    public Feature getFeatureByReference(FeatureReference reference,
        FeatureType featureType) throws DataException;

    /*
     * =============================================================
     *
     * Editing related services
     */

    /**
     * Enters editing state.
     */
    public void edit() throws DataException;

    /**
     * Enters editing state specifying the editing mode.
     *
     * @param mode
     *
     * @throws DataException
     */
    public void edit(int mode) throws DataException;

    /**
     * Cancels all editing since the last edit().
     *
     * @throws DataException
     */
    public void cancelEditing() throws DataException;

    /**
     * Exits editing state.
     *
     * @throws DataException
     */
    public void finishEditing() throws DataException;

    /**
     * Save changes in the provider without leaving the edit mode.
     * Do not call observers to communicate a change of ediding mode.
     * The operation's history is eliminated to prevent inconsistencies
     * in the data.
     *
     * @throws DataException
     */
    public void commitChanges() throws DataException ;

    /**
     *
     * Returns true if you can call CommitChanges method.
     * If not in editing or changes have been made in the structure
     * return false.
     *
     * @return true if can call commitChanges
     * @throws DataException
     */
    public boolean canCommitChanges() throws DataException;


    /**
     * Indicates whether this store is in editing state.
     *
     * @return
     *         true if this store is in editing state, false if not.
     */
    public boolean isEditing();

    /**
     * Indicates whether this store is in appending state. In this state the new
     * features are automatically inserted at the end of the {@link FeatureSet}.
     *
     * @return true if this store is in appending state.
     */
    public boolean isAppending();

    /**
     * Updates a {@link FeatureType} in the store with the changes in the
     * {@link EditableFeatureType}.<br>
     *
     * Any {@link FeatureSet} from this store that are used will be invalidated.
     *
     * @param featureType
     *            an {@link EditableFeatureType} with the changes.
     *
     * @throws DataException
     */
    public void update(EditableFeatureType featureType) throws DataException;

    /**
     * Updates a {@link Feature} in the store with the changes in the
     * {@link EditableFeature}.<br>
     *
     * Any {@link FeatureSet} from this store that was still in use will be
     * invalidated. You can override this using
     * {@link FeatureSet#update(EditableFeature)}.
     *
     * @param feature
     *            the feature to be updated
     *
     * @throws DataException
     */
    public void update(EditableFeature feature) throws DataException;

    /**
     * Deletes a {@link Feature} from the store.<br>
     *
     * Any {@link FeatureSet} from this store that was still in use will be
     * invalidated. You can override this using {@link Iterator#remove()} from
     * {@link FeatureSet}.
     *
     * @param feature
     *            The feature to be deleted.
     *
     * @throws DataException
     */
    public void delete(Feature feature) throws DataException;

    /**
     * Inserts a {@link Feature} in the store.<br>
     *
     * Any {@link FeatureSet} from this store that was still in use will be
     * invalidated. You can override this using
     * {@link FeatureSet#insert(EditableFeature)}.
     *
     * @param feature
     *            The feature to be inserted
     *
     * @throws DataException
     */
    public void insert(EditableFeature feature) throws DataException;

    /**
     * Creates a new feature using the default feature type and returns it as an
     * {@link EditableFeature}
     *
     * @return a new feature in editable state
     *
     * @throws DataException
     */
    public EditableFeature createNewFeature() throws DataException;

    /**
     * Creates a new feature of the given {@link FeatureType} and uses the given
     * {@link Feature} as default values to initialize it.
     *
     * @param type
     *            the new feature's feature type
     *
     * @param defaultValues
     *            a feature whose values are used as default values for the new
     *            feature.
     *
     * @return the new feature.
     *
     * @throws DataException
     */
    public EditableFeature createNewFeature(FeatureType type,
        Feature defaultValues) throws DataException;

    /**
     * Creates a new feature of the given {@link FeatureType}. The flag
     * defaultValues is used to indicate whether the new feature should be
     * initialized with default values or not.
     *
     * @param type
     *            the new feature's feature type
     *
     * @param defaultValues
     *            if true the new feature is initialized with each attribute's
     *            default value.
     *
     * @return
     *         the new feature
     *
     * @throws DataException
     */
    public EditableFeature createNewFeature(FeatureType type,
        boolean defaultValues) throws DataException;

    /**
     * Creates a new feature of default {@link FeatureType}. The flag
     * defaultValues is used to indicate whether the new feature should be
     * initialized with default values or not.
     *
     * @param defaultValues
     *            if true the new feature is initialized with each attribute's
     *            default value.
     *
     * @return
     *         the new feature
     *
     * @throws DataException
     */
    public EditableFeature createNewFeature(boolean defaultValues)
        throws DataException;

    /**
     * Creates a new feature of default {@link FeatureType}. 
     * The new feature should be initialized with the values of the feature 
     * passed as parameter.
     * Values are inicialiced by name from the feature specified. Error in
     * value assignement are ignoreds.
     * 
     * @param defaultValues the values to initialize the new feature.
     * @return the new feature
     * @throws DataException 
     */
    public EditableFeature createNewFeature(Feature defaultValues)
        throws DataException;

    /**
     * Applies the validation rules associated to the given mode to the active
     * {@link FeatureSet}.
     *
     * @param mode
     *            can be one of {MODE_QUERY, MODE_FULLEDIT, MODE_APPEND}
     *
     * @throws DataException
     */
    public void validateFeatures(int mode) throws DataException;

    /**
     * Indicates whether this store supports append mode.
     *
     * @return
     *         true if this store supports append mode.
     */
    public boolean isAppendModeSupported();

    /**
     * Initiates an editing group. This is typically used to group series of
     * store editing operations.
     *
     * @param description
     *            Description of the editing group.
     *
     * @throws NeedEditingModeException
     */
    public void beginEditingGroup(String description)
        throws NeedEditingModeException;

    /**
     * Finishes an editing group.
     *
     * @throws NeedEditingModeException
     */
    public void endEditingGroup() throws NeedEditingModeException;

    /*
     * =============================================================
     *
     * Index related services
     */

    /**
     * Creates an index which will be applied to the features of the given type,
     * by using the data of the given attribute.
     *
     * @param featureType
     *            The FeatureType to which the indexed attribute belongs.
     *
     * @param attributeName
     *            The name of the attributed to be indexed
     *
     * @param indexName
     *            The index name
     *
     * @return the resulting {@link FeatureIndex}
     *
     *
     * @throws FeatureIndexException
     *             if there is an error creating the index
     */
    public FeatureIndex createIndex(FeatureType featureType,
        String attributeName, String indexName) throws DataException;

    /**
     * Creates an index which will be applied to the features of the given type,
     * by using the data of the given attribute.
     *
     * @param indexTypeName
     *            the type of the index to be created. That name is
     *            related to one of the registered index providers
     * @param featureType
     *            The FeatureType to which the indexed attribute belongs.
     *
     * @param attributeName
     *            The name of the attributed to be indexed
     *
     * @param indexName
     *            The index name
     *
     * @return the resulting {@link FeatureIndex}
     *
     *
     * @throws FeatureIndexException
     *             if there is an error creating the index
     */
    public FeatureIndex createIndex(String indexTypeName,
        FeatureType featureType, String attributeName, String indexName)
        throws DataException;

    /**
     * Creates an index which will be applied to the features of the given type,
     * by using the data of the given attribute. This method will return without
     * waiting for the index to be filled, as that will be performed in
     * background. An optional {@link Observer} parameter is provided to be
     * notified ( {@link FeatureStoreNotification#INDEX_FILLING_SUCCESS} )
     * when the index has finished filling with data and is available to be
     * used.
     *
     * @param featureType
     *            The FeatureType to which the indexed attribute belongs.
     *
     * @param attributeName
     *            The name of the attributed to be indexed
     *
     * @param indexName
     *            The index name
     *
     * @param observer
     *            to notify to when the created index has finished filling
     *            with data and is available to be used. The observer will
     *            receive then a
     *            {@link FeatureStoreNotification#INDEX_FILLING_SUCCESS}
     *            notification, with the index object if it has finished
     *            successfully, or a
     *            {@link FeatureStoreNotification#INDEX_FILLING_ERROR}
     *            notification with the exception object if there has been
     *            any error in the process. Optional.
     *
     * @return the resulting {@link FeatureIndex}
     *
     * @see FeatureStoreNotification#INDEX_FILLING_STARTED
     * @see FeatureStoreNotification#INDEX_FILLING_SUCCESS
     * @see FeatureStoreNotification#INDEX_FILLING_CANCELLED
     * @see FeatureStoreNotification#INDEX_FILLING_ERROR
     *
     * @throws FeatureIndexException
     *             if there is an error creating the index
     */
    public FeatureIndex createIndex(FeatureType featureType,
        String attributeName, String indexName, Observer observer)
        throws DataException;

    /**
     * Creates an index which will be applied to the features of the given type,
     * by using the data of the given attribute. This method will return without
     * waiting for the index to be filled, as that will be performed in
     * background. An optional {@link Observer} parameter is provided to be
     * notified ( {@link FeatureStoreNotification#INDEX_FILLING_SUCCESS} )
     * when the index has finished filling with data and is available to be
     * used.
     *
     * @param indexTypeName
     *            the type of the index to be created. That name is
     *            related to one of the registered index providers
     * @param featureType
     *            The FeatureType to which the indexed attribute belongs.
     *
     * @param attributeName
     *            The name of the attributed to be indexed
     *
     * @param indexName
     *            The index name
     *
     * @param observer
     *            to notify to when the created index has finished filling
     *            with data and is available to be used. The observer will
     *            receive then a
     *            {@link FeatureStoreNotification#INDEX_FILLING_SUCCESS}
     *            notification, with the index object if it has finished
     *            successfully, or a
     *            {@link FeatureStoreNotification#INDEX_FILLING_ERROR}
     *            notification with the exception object if there has been
     *            any error in the process. Optional.
     *
     * @return the resulting {@link FeatureIndex}
     *
     * @see FeatureStoreNotification#INDEX_FILLING_STARTED
     * @see FeatureStoreNotification#INDEX_FILLING_SUCCESS
     * @see FeatureStoreNotification#INDEX_FILLING_CANCELLED
     * @see FeatureStoreNotification#INDEX_FILLING_ERROR
     *
     * @throws FeatureIndexException
     *             if there is an error creating the index
     */
    public FeatureIndex createIndex(String indexTypeName,
        FeatureType featureType, String attributeName, String indexName,
        Observer observer) throws DataException;

    /**
     * Returns a FeatureIndexes structure containing all available indexes in
     * the store.
     *
     * @return
     */
    public FeatureIndexes getIndexes();

    /*
     * =============================================================
     *
     * Selection related services
     */

    /**
     * Sets the selection to the passed {@link FeatureSet}
     *
     * @param selection
     *            A {@link FeatureSet} with the requested selection
     */
    public void setSelection(FeatureSet selection) throws DataException;

    /**
     * Creates a {@link FeatureSelection}
     *
     * @return
     *         a {@link FeatureSelection}
     *
     * @throws DataException
     */
    public FeatureSelection createFeatureSelection() throws DataException;

    /**
     * Returns the current {@link FeatureSelection}.
     * Create a empty selection if not exits.
     *
     * @return
     *         current {@link FeatureSelection}.
     *
     * @throws DataException
     */
    public FeatureSelection getFeatureSelection() throws DataException;

    /*
     * =============================================================
     *
     * Lock related services
     */

    /**
     * Indicates whether this store supports locks.
     *
     * @return
     *         true if this store supports locks, false if not.
     */
    public boolean isLocksSupported();

    /**
     * Returns the set of locked features
     *
     * @return
     *         set of locked features
     *
     * @throws DataException
     */
    public FeatureLocks getLocks() throws DataException;

    /*
     * =============================================================
     * Transforms related services
     * =============================================================
     */

    /**
     * Returns this store transforms
     *
     * @return
     *         this store transforms
     */
    public FeatureStoreTransforms getTransforms();

    /**
     * Returns a new {@link FeatureQuery} associated to this store.
     *
     * @return
     *         a new {@link FeatureQuery} associated to this store.
     */
    public FeatureQuery createFeatureQuery();

    /**
     * Returns featue count of this store.
     *
     * @return
     * @throws DataException
     */
    public long getFeatureCount() throws DataException;

    /**
     * Creates a vectorial cache that is used to save and retrieve data.
     *
     * @param name
     *            the cache name.
     * @param parameters
     *            parameters to create the stores used to save data.
     * @throws DataException
     */
    public void createCache(String name, DynObject parameters)
        throws DataException;

    /**
     * @return the vectorial cache
     */
    public FeatureCache getCache();

    /**
     * Return if the provider knows the real envelope of a layer. If not,
     * the {@link FeatureStoreProvider#getEnvelope()} method doesn't return
     * the full envelope.
     *
     * @return
     *         <true> if it knows the real envelope.
     */
    public boolean isKnownEnvelope();

    /**
     * Return if the maximum number of features provided by the
     * provider are limited.
     *
     * @return
     *         <true> if there is a limit of features.
     */
    public boolean hasRetrievedFeaturesLimit();

    /**
     * If the {@link FeatureStoreProvider#hasRetrievedFeaturesLimit()} returns
     * true,
     * it returns the limit of features retrieved from the provider.
     *
     * @return
     *         The limit of the retrieved features.
     */
    public int getRetrievedFeaturesLimit();

    /**
     * Return the associated feature to the dynobject.
     * If the dynobject isn't associated to a feature of this store, return null.
     *
     * @param dynobject
     * @return
     */
    public Feature getFeature(DynObject dynobject);
    
    public Iterator iterator();
}
