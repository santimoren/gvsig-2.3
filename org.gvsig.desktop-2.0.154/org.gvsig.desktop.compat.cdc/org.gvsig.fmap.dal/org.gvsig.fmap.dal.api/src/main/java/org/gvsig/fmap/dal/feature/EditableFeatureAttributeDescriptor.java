/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.text.DateFormat;
import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.exception.AttributeFeatureTypeIntegrityException;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.evaluator.Evaluator;

/**
 * This interface represents a FeatureAttributeDescriptor in editable state.
 * To edit a FeatureAttributeDescriptor you have to obtain its instance of
 * EditableFeatureAttributeDescriptor and then perform editing operations on it.
 *
 * Once you have completed the editing you can save the changes to the original
 * FeatureAttributeDescriptor. This is the only way to edit a FeatureAttributeDescriptor.
 */
public interface EditableFeatureAttributeDescriptor extends
		FeatureAttributeDescriptor {

	/**
	 * Checks attribute integrity
	 */
	void checkIntegrity() throws AttributeFeatureTypeIntegrityException;

	/**
	 * Sets the name
	 * @param
	 * 		name to set
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setName(String name);

	/**
	 * Sets the data type
	 * @param
	 * 		type one of the constants defined in {@link DataTypes}
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setDataType(int type);

	/**
	 * Sets the size
	 * @param size
	 * 			a size of type int
	 * @return this
	 *
	 */
	public EditableFeatureAttributeDescriptor setSize(int size);

	/**
	 * Sets the precision
	 *
	 * @param
	 * 		precision of type int
	 *
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setPrecision(int precision);

	/**
	 * Sets the Class to which the related FeatureAttribute can be cast
	 *
	 * @param theClass
	 * 				Class to which the related FeatureAttribute can be cast
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setObjectClass(Class theClass);

	/**
	 * Sets the number of minimum occurrences
	 *
	 * @param minimumOccurrences
	 *
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setMinimumOccurrences(
			int minimumOccurrences);

	/**
	 * Sets the maximum number of occurrences
	 *
	 * @param maximumOccurrences
	 *
	 * @return
	 */
	public EditableFeatureAttributeDescriptor setMaximumOccurrences(
			int maximumOccurrences);

	/**
	 * Sets whether the related FeatureAttribute is part of the FeatureType's primary key
	 *
	 * @param isPrimaryKey
	 * 				true if is part of the primary key
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setIsPrimaryKey(
			boolean isPrimaryKey);

	/**
	 * Sets the expression evaluator that the FeatureAttribute will use
	 * @param evaluator
	 * 				an implementation of DAL's Evaluator interface
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setEvaluator(Evaluator evaluator);

     	/**
	 * Sets the {@link FeatureAttributeEmulator} that is used to update the 
	 * presentation of a field.
	 * @param featureAttributeEmulator
	 *             the {@link FeatureAttributeEmulator} to set.
	 */
	public EditableFeatureAttributeDescriptor setFeatureAttributeEmulator(FeatureAttributeEmulator featureAttributeEmulator);

	/**
	 * Sets whether the related FeatureAttribute is read only
	 *
	 * @param isReadOnly
	 *
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setIsReadOnly(boolean isReadOnly);

	/**
	 * Sets whether the related FeatureAttribute can have a null value
	 *
	 * @param allowNull
	 * 				a boolean value determining whether the FeatureAttribute can be null
	 *
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setAllowNull(boolean allowNull);

	/**
	 * Sets the SRS.
	 *
	 * @param SRS
	 *
	 * @return
	 */
	public EditableFeatureAttributeDescriptor setSRS(IProjection SRS);

	    /**
     * Sets the geometry type
     * 
     * @param geometryType
     * 
     * @return this
     * @deprecated use {@link #setGeometryType(GeometryType)} instead
     */
    public EditableFeatureAttributeDescriptor setGeometryType(int geometryType);

	    /**
     * Sets the geometry subtype
     * 
     * @param geometrySubType
     * 
     * @return this
     * @deprecated use {@link #setGeometryType(GeometryType)} instead
     */
	public EditableFeatureAttributeDescriptor setGeometrySubType(
			int geometrySubType);

    /**
     * Sets the geometry type
     * 
     * @param geometryType
     * 
     * @return this
     */
    public EditableFeatureAttributeDescriptor setGeometryType(
        GeometryType geometryType);

    public EditableFeatureAttributeDescriptor setGeometryType(int geometrySubType, int geometryType);

    /**
     * Sets the default value
     * 
     * @param defaultValue
     * 
     * @return this
     */
	public EditableFeatureAttributeDescriptor setDefaultValue(
			Object defaultValue);
 
	/**
	 * Sets additional information of the attribute
	 * @return TODO
	 *
	 *
	 */
	public EditableFeatureAttributeDescriptor setAdditionalInfo(String infoName, Object value);

	/**
	 * Sets whether the related FeatureAttribute is part of the FeatureType's
	 * primary key
	 *
	 * @param isPrimaryKey
	 *            true if is part of the primary key
	 * @return this
	 */
	public EditableFeatureAttributeDescriptor setIsAutomatic(
			boolean isAutomatic);

	/**
	 * Sets is the attribute is a temporal attribute.
     *
 	 * @param isTime
 	 *        <code>true</code> if the attribute is temporal
	 * @return
	 *         this
	 */
	public EditableFeatureAttributeDescriptor setIsTime(boolean isTime);
	
        /**
         * Sets if this attribute is indexed in the table.
         * @param isIndexed
         * @return  this
         */
        public EditableFeatureAttributeDescriptor setIsIndexed(boolean isIndexed);
        
        public EditableFeatureAttributeDescriptor setAllowIndexDuplicateds(boolean allowDuplicateds);
        
        public EditableFeatureAttributeDescriptor setIsIndexAscending(boolean ascending);
        
	/**
	 * Returns the attribute descriptor's name before
	 * it was changed or null if never changed 
	 * @return
	 */
        public String getOriginalName();
    
       /**
	 * If this attribute is of type Date, then this method set
	 * the date format set by the data store.
	 *
	 * @return
	 */
	public EditableFeatureAttributeDescriptor  setDateFormat(DateFormat dateFormat);
    
}
