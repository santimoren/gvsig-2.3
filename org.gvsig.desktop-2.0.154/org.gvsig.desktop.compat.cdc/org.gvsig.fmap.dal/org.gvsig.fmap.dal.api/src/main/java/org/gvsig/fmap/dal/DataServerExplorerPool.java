

package org.gvsig.fmap.dal;

import java.util.Iterator;
import org.gvsig.tools.persistence.Persistent;


public interface DataServerExplorerPool extends Persistent {
    
    
    public void add(String name, DataServerExplorerParameters explorer);

    public void add(String name, String description, DataServerExplorerParameters explorer);
    
    public boolean contains(String name);
    
    public boolean contains(DataServerExplorerPoolEntry entry);
    
    public boolean isEmpty();
    
    public void remove(DataServerExplorerPoolEntry explorer);
    
    public void remove(int explorerIndex);
    
    public void remove(String name);
    
    public int size();
    
    public DataServerExplorerPoolEntry get(int index);

    public DataServerExplorerPoolEntry get(String name);
    
    public Iterator iterator();
}
