/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.List;

import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.geom.Geometry;

public interface FeatureStoreProviderFactory extends DataStoreProviderFactory {


    /**
     * Returns a list of {@link DataTypes} supported
     * by this FeatureStoreProviderFactory
     *
     * @return  list of {@link DataTypes} supported
     * by this FeatureStoreProviderFactory or null
     * if it has no restrictions on data types
     */
    public List getSupportedDataTypes();

	/**
	 * {@link Geometry}
	 *
	 * Return a matrix (list of pairs) (int, int]
	 * [Geometry.TYPE,GEOMETRY.SUBTYPE]
	 * with types and subtypes supported by this provider.
	 *
	 * If the provider has not vector support
	 * or has not restrictions over geometry types return null.
	 *
	 * @return Matrix of Geometry.TYPES, SUBTYPES or null
	 */
	public List getSupportedGeometryTypesSubtypes();

	/**
	 *
	 * @return whether this factory allows mandatory attributes
	 * in the generated stores' feature types.
	 */
	public boolean allowsMandatoryAttributes();

	/**
	 *
     * @return whether this factory allows primary key attributes
     * in the generated stores' feature types.
	 */
	public boolean allowsPrimaryKeyAttributes();

	/**
	 *
	 * @return a new instance of a default feature type
	 * (changes to that feature type do not affect following calls)
	 */
	public FeatureType createDefaultFeatureType();

	/**
	 *
	 * @return whether stores created by this factory
	 * allow several geometry types.
	 * Actually redundant after adding
	 * getSupportedGeometryTypesSubtypes())
	 */
	// public int allowMultipleGeometryTypes();

	public int allowEditableFeatureType();

    public int useLocalIndexesCanImprovePerformance();

    /**
     * Returns max size for attributes names
     * returns -1 if it is undefined
     * @return
     */
    public int getMaxAttributeNameSize();

}
