


package org.gvsig.fmap.dal;

import org.gvsig.tools.persistence.Persistent;



public interface DataServerExplorerPoolEntry extends Persistent {
    
    public String getName();

    public DataServerExplorerParameters getExplorerParameters();
    
    public String getDescription();
    
    public void copyTo(DataServerExplorerPoolEntry target);
}
