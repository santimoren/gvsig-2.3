/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.rule;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.AbstractFeatureRule;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.primitive.Surface;


public class FeatureRulePolygon extends AbstractFeatureRule {
	private static GeometryManager geomManager = GeometryLocator.getGeometryManager();
	
	public FeatureRulePolygon() {
		super("RulePolygon", "Ensure orientation of geometry");
	}

	public void validate(Feature feature, FeatureStore store)
			throws DataException {

        try {
			Geometry geom = feature.getDefaultGeometry();
			GeneralPathX gp = new GeneralPathX();
			gp.append(geom.getPathIterator(null, geomManager.getFlatness()), true);

			if (gp.isClosed()) {
				if (gp.isCCW()) {
					gp.flip();
					GeometryManager geomManager = GeometryLocator.getGeometryManager();
					Surface surface = (Surface)geomManager.create(TYPES.SURFACE, SUBTYPES.GEOM2D);
					surface.setGeneralPath(gp);
					geom = surface;
	        		EditableFeature editable = feature.getEditable();
					editable.setDefaultGeometry(geom);
					store.update(editable);
				}
	        }
		} catch (Exception e) {
			throw new FeatureRulePolygonException(e, store.getName());
		}
	}

	public class FeatureRulePolygonException extends DataException {

		/**
		 *
		 */
		private static final long serialVersionUID = -3014970171661713021L;
		private final static String MESSAGE_FORMAT = "Can't apply rule  in store %(store)s.";
		private final static String MESSAGE_KEY = "_FeatureRulePolygonException";

		public FeatureRulePolygonException(Throwable cause, String store) {
			super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
			this.setValue("store", store);
		}
	}

}
