/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.exception;

import org.gvsig.fmap.dal.exception.DataException;


/**
 * @author jiyarza
 */
public class FeatureIndexException extends DataException {
	
	private static final long serialVersionUID = 932690269454242645L;	
	
	private static final String MESSAGE_KEY = "index_exception";
	private static final String FORMAT_STRING = 
		"Index Exception.";	
	
	/**
	 * Main constructor that provides a cause Exception
	 * @param e
	 */
	public FeatureIndexException(Exception e) {
        this(FORMAT_STRING, e, MESSAGE_KEY, serialVersionUID);
	}

    /**
     * @see DataException#DataException(String, String, long)
     */
    public FeatureIndexException(String messageFormat, String messageKey,
        long code) {
        super(messageFormat, messageKey, code);
    }

    /**
     * @see DataException#DataException(String, Throwable, String, long)
     */
    public FeatureIndexException(String messageFormat, Throwable cause,
        String messageKey, long code) {
        super(messageFormat, cause, messageKey, code);
    }

}


