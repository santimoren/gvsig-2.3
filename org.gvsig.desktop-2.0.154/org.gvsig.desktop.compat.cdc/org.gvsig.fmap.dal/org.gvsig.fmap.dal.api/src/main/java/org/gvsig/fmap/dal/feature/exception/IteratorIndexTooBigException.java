/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.exception;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSet;

/**
 * Exception thrown when the index value for an iterator of a {@link FeatureSet}
 * is too big.
 * 
 * @see FeatureSet#iterator(long)
 * @see FeatureSet#fastIterator(long)
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class IteratorIndexTooBigException extends DataException {
	
	private static final long serialVersionUID = -2774419869818426273L;
	
	private final static String MESSAGE_FORMAT =
			"The index value %(value) is bigger than the "
					+ "Iterator maximum value: %(maxValue)";
	private final static String MESSAGE_KEY = "_IteratorIndexTooBigException";

	/**
	 * Creates a new {@link IteratorIndexTooBigException}.
	 * 
	 * @param value
	 *            the wanted Iterator index value
	 * @param maxValue
	 *            the maximum Iterator index value
	 */
	public IteratorIndexTooBigException(long value, long maxValue) {
		super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
		setValue("value", new Long(value));
		setValue("maxValue", new Long(maxValue));
	}
}
