/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create Parameter object to define FeatureCollections queries}
 */
package org.gvsig.fmap.dal.feature;

import org.gvsig.fmap.dal.DataQuery;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.lang.Cloneable;

/**
 * Defines the properties of a collection of Features, as a result of a query
 * through a FeatureStore.
 * <p>
 * A FeatureQuery is always defined by a FeatureType, or by the list of
 * attribute names of the FeatureStore to return.
 * </p>
 * <p>
 * The filter allows to select Features whose properties have values with the
 * characteristics defined by the filter.
 * </p>
 * <p>
 * The order is used to set the order of the result FeatureCollection, based on
 * the values of the properties of the Features.
 * </p>
 * <p>
 * The scale parameter can be used by the FeatureStore as a hint about the
 * quality or resolution of the data needed to view or operate with the data
 * returned. As an example, the FeatureStore may use the scale to return only a
 * representative subset of the data, or maybe to return Features with less
 * detail, like a point or a line instead of a polygon.
 * </p>
 * <p>
 * If an implementation of FeatureStore is able to get other parameters to
 * customize the behavior of the getDataCollection methods, there is an option
 * to set more parameters through the setAttribute method.
 * </p>
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public interface FeatureQuery extends DataQuery, Cloneable {

	/**
	 * Returns the names of the attributes to load from each {@link Feature}.
	 *
	 * @return the attribute names to load
	 */
	String[] getAttributeNames();

	/**
	 * Sets the names of the attributes to load from each {@link Feature}.
	 *
	 * @param attributeNames
	 *            the attribute names to load
	 */
	void setAttributeNames(String[] attributeNames);

	/**
	 * Adds the name of an attribute that has to be used to load each
	 * {@link Feature}.
	 *
	 * @param attributeName
	 *             the attribute name to load
	 */
	void addAttributeName(String attributeName);

        /**
         * Return true if has set attribute names
         *
         * @return true if has attribute names, otherwise false
         */
        boolean hasAttributeNames();

        /**
         * Remove all the attribute names specifieds.
         */
        void clearAttributeNames();

        /**
	 * Returns the names of the attributes that are constants in each {@link Feature}.
	 * These attributes will not be charged.
         *
	 * @return the attribute names that are constant
	 */
	String[] getConstantsAttributeNames();

	/**
	 * Set of attribute names to be treated as constants for each {@link Feature}.
	 *
	 * @param attributeNames
	 *            the attribute names to be constants
	 */
	void setConstantsAttributeNames(String[] attributeNames);

	/**
	 * Add an attribute name that will be treated as constant for each
	 * {@link Feature}.
	 *
	 * @param attributeName
	 *             the attribute name to be treated as constant
	 */
	void addConstantAttributeName(String attributeName);

        /**
         * Return true if has set constants attribute names
         *
         * @return true if has constants attribute names, otherwise false
         */
        boolean hasConstantsAttributeNames();

        /**
         * Remove all the names specified as constants attributes.
         */
        void clearConstantsAttributeNames();

	/**
	 * Sets the {@link FeatureType} of the {@link Feature}s to load. It may be
	 * used as an alternative way to set a subset of the list of attribute names
	 * to load, by creating a sub-FeatureType.
	 *
	 * @param featureType
	 *            the feature type of the data to load
	 */
	void setFeatureType(FeatureType featureType);

	/**
	 * Returns the {@link FeatureType} id of the {@link Feature}s to load.
	 *
	 * @return the {@link FeatureType} id of the {@link Feature}s to load
	 */
	String getFeatureTypeId();

	/**
	 * Sets the {@link FeatureType} id of the {@link Feature}s to load. This way
	 * all {@link Feature} attributes will be loaded.
	 *
	 * @param featureTypeId
	 *            the {@link FeatureType} id of the {@link Feature}s to load
	 */
	void setFeatureTypeId(String featureTypeId);

	/**
	 * Returns the filter to apply to the {@link Feature}s to load.
	 *
	 * @return the filter
	 */
	Evaluator getFilter();

	/**
	 * Sets the filter to apply to the {@link Feature}s to load.
	 *
	 * @param filter
	 *            the filter to apply to the {@link Feature}s to load
	 */
	void setFilter(Evaluator filter);

        void setFilter(String filter);

	/**
	 * Adds a filter to apply to the {@link Feature}s to load. A query
	 * can have more that one filter and all of them are applied when
	 * the query is applied.
         * If filter is null do nothing.
	 *
	 * @param filter
	 *             a filter to apply to the {@link Feature}s to load
	 */
	void addFilter(Evaluator filter);

        void addFilter(String filter);

        void clearFilter();
        
	/**
	 * Returns if a filter has been defined for the query.
	 *
	 * @return if a filter has been defined for the query
	 */
	boolean hasFilter();

	/**
	 * Returns the order of the {@link Feature}s to load.
	 *
	 * @return the order of the {@link Feature}s to load
	 */
	FeatureQueryOrder getOrder();

	/**
	 * Sets the order of the {@link Feature}s to load.
	 *
	 * @param order
	 *            the order of the {@link Feature}s to load
	 */
	void setOrder(FeatureQueryOrder order);

	/**
	 * Returns if an order has been set for the elements returned by the query.
	 *
	 * @return if an order has been set for the elements returned by the query
	 */
	boolean hasOrder();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see #clone()
	 */
	FeatureQuery getCopy();

	/**
	 * Returns the maximum number of elements to return with this query.
	 * <p>
	 * <strong>NOTE:</strong> this value may be ignored by the underlying data
	 * source, or only used as a hint, so don't rely on it being used, as you
	 * may actually get more values than the limit.
	 * </p>
	 *
	 * @return the maximum number of elements to return with this query
	 */
	long getLimit();

	/**
	 * Sets the maximum number of elements to return with this query.
	 * <p>
	 * <strong>NOTE:</strong> this value may be ignored by the underlying data
	 * source, or only used as a hint, so don't rely on it being used, as you
	 * may actually get more values than the limit.
	 * </p>
	 *
	 * @param limit
	 *            the maximum number of elements to return with this query
	 */
	void setLimit(long limit);

	/**
	 * Returns the load page size, as the number of elements to be retrieved in
	 * block by the data source. This value is only used as a hint to the
	 * underlying data source, as a way to tell how many Features may be read in
	 * a block.
	 *
	 * @return the load page size
	 */
	long getPageSize();

	/**
	 * Sets the load page size, as the number of elements to be retrieved in
	 * block by the data source. This value is only used as a hint to the
	 * underlying data source, as a way to tell how many Features may be read in
	 * a block.
	 *
	 * @param pageSize
	 *            the load page size
	 */
	void setPageSize(long pageSize);
}