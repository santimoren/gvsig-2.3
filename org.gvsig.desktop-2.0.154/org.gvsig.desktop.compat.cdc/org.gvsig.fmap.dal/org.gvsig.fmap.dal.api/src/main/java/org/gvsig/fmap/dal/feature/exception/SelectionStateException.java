/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.fmap.dal.feature.exception;

import org.gvsig.fmap.dal.exception.DataException;

/**
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class SelectionStateException extends DataException {

    private final static String MESSAGE_FORMAT = "Error getting or setting the selection state";
    private final static String MESSAGE_KEY = "_SelectionStateException";

    private static final long serialVersionUID = -1183610070098605104L;

    /**
     * Creates a new SelectionStateException.
     */
    public SelectionStateException() {
        super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
    }

    /**
     * Creates a new SelectionStateException with a cause.
     */
    public SelectionStateException(Throwable cause) {
        super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
    }

}
