
package org.gvsig.fmap.dal.feature;

import java.io.File;
import org.gvsig.tools.dynobject.DynClass;


public interface FeatureTypeDefinitionsManager {
    
    /**
     * Search if exists a DynClass associated to the feature type and return it.
     * If do not exists a DynClass for this feature type return the feature type
     * as the default dynClass for it.
     * 
     * @param store
     * @param featureType
     * @return  the DynClass associated to this feature type
     */
    public DynClass get(FeatureStore store, FeatureType featureType);

    /**
     * Return true if exists a DynClass defined associated to the feature 
     * type passed as parameter.
     * 
     * @param store
     * @param featureType
     * @return 
     */
    public boolean contains(FeatureStore store, FeatureType featureType);
    
    /**
     * Associate the DynClass to the feature type.
     * This association persist betwen sessions.
     * 
     * @param store
     * @param featureType
     * @param dynClass 
     */
    public void add(FeatureStore store, FeatureType featureType, DynClass dynClass);
    
    /**
     * Unlink the DynClass associated to the feature type.
     * This force reload next time to call "get" for this
     * feature type.
     * 
     * @param store
     * @param featureType 
     */
    public void remove(FeatureStore store, FeatureType featureType);

    /**
     * Add the definitions of feature types in the file.
     * 
     * @param model 
     */
    public void addModel(File model);

}
