/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
* AUTHORS (In addition to CIT):
* 2008 {{Company}}   {{Task}}
*/


package org.gvsig.fmap.dal.exception;


public class ReadRuntimeException extends DataRuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7756645643644789613L;
	private final static String MESSAGE_FORMAT = "There was errors loading the feature '%(oid)' from '%(store)'.";
	private final static String MESSAGE_KEY = "_ReadRuntimeException";

	public ReadRuntimeException(String store, Throwable cause) {
		super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
		setValue("store", store);
		setValue("oid", "unknown");
	}

        public ReadRuntimeException(String store, Object oid,Throwable cause) {
		super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
		setValue("store", store);
		setValue("oid", String.format("%s", oid));
	}
        
        protected ReadRuntimeException(String fmt, Throwable cause, String msgkey, int code) {
            super(fmt, cause, msgkey, code);
        }        
        
        protected ReadRuntimeException(String fmt, Throwable cause, String msgkey, long code) {
            super(fmt, cause, msgkey, code);
        }
}

