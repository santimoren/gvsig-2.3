
package org.gvsig.fmap.dal.feature;


public interface FacadeOfAFeature {
    public Feature getFeature();
    
    public EditableFeature getEditableFeature();
}
