/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import java.util.ArrayList;
import java.util.List;

import org.cresques.ProjectionLibrary;

import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.fmap.geom.GeometryLibrary;
import org.gvsig.metadata.MetadataLibrary;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.timesupport.TimeSupportLibrary;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Initializes gvSIG's desktop DAL by registering the default implementation for
 * {@link DataManager} and {@link ResourceManager}.
 * 
 */
public class DALLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsAPI(DALLibrary.class);
        require(ToolsLibrary.class);
        require(MetadataLibrary.class);
        require(ProjectionLibrary.class);
        require(GeometryLibrary.class);
        require(TimeSupportLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        // Do nothing
    }

    protected void doPostInitialize() throws LibraryException {
        List exs = new ArrayList();

        try {
            registerDataStoreMetadataDefinition();
        } catch (MetadataException e) {
            exs.add(e);
        }
        try {
            registerSpatialDataStoreMetadataDefinition();
        } catch (MetadataException e) {
            exs.add(e);
        }
        try {
            registerFeatureStoreMetadataDefinition();
        } catch (MetadataException e) {
            exs.add(e);
        }

        // Validate there is any implementation registered.
        DataManager dataManager = DALLocator.getDataManager();
        if (dataManager == null) {
            throw new ReferenceNotRegisteredException(
                DALLocator.DATA_MANAGER_NAME, DALLocator.getInstance());
        }

        if (exs.size() > 0) {
            throw new LibraryException(this.getClass(), exs);
        }
    }

    private void registerDataStoreMetadataDefinition() throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(DataStore.METADATA_DEFINITION_NAME) == null) {
            manager
                .addDefinition(DataStore.METADATA_DEFINITION_NAME,
                    DALLibrary.class
                        .getResourceAsStream("MetadataDefinitions.xml"),
                    DALLibrary.class.getClassLoader());
        }
    }

    private void registerSpatialDataStoreMetadataDefinition()
        throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(DataStore.SPATIAL_METADATA_DEFINITION_NAME) == null) {
            manager
                .addDefinition(DataStore.SPATIAL_METADATA_DEFINITION_NAME,
                    DALLibrary.class
                        .getResourceAsStream("MetadataDefinitions.xml"),
                    DALLibrary.class.getClassLoader());
        }
    }

    private void registerFeatureStoreMetadataDefinition()
        throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(DataStore.FEATURE_METADATA_DEFINITION_NAME) == null) {
            manager
                .addDefinition(DataStore.FEATURE_METADATA_DEFINITION_NAME,
                    DALLibrary.class
                        .getResourceAsStream("MetadataDefinitions.xml"),
                    DALLibrary.class.getClassLoader());
        }
    }

}
