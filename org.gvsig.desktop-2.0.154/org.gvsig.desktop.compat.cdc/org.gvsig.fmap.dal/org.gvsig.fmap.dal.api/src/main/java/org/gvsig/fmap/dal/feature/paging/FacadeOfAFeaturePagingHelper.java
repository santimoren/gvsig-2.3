
package org.gvsig.fmap.dal.feature.paging;


public interface FacadeOfAFeaturePagingHelper {
    
    public FeaturePagingHelper getFeaturePagingHelper();
}
