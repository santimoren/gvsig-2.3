/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import java.util.Collection;
import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.metadata.Metadata;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.timesupport.Time;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.ComplexWeakReferencingObservable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.visitor.Visitable;
import org.gvsig.tools.visitor.Visitor;

/**
 * <p>
 * This is the basic interface for all data stores. Depending on the context, it
 * can represent a geographic layer, an alphanumeric database table or any data
 * file. DataStore offers generic services like:
 * <ul>
 * <li>Open and close data stores</li>
 * <li>Access to data sets, with the possibility of loading data in background.</li>
 * <li>Use of selection and locks, as well as data sets</li>
 * <li>Editing</li>
 * <li>Register event observers through the Observable interface</li>
 * <li>Access to embedded data stores (like GML)</li>
 * <li>Information about the Spatial Reference Systems used by the data store</li>
 * </ul>
 * </p>
 * <br>
 *
 */
public interface DataStore extends ComplexWeakReferencingObservable,
		Persistent, Metadata, Disposable, Visitable {

	public static final String METADATA_DEFINITION_NAME = "DataProvider";

	public static final String FEATURE_METADATA_DEFINITION_NAME = "FeatureProvider";

	public static final String SPATIAL_METADATA_DEFINITION_NAME = "SpatialProvider";

	/**
	 * Metadata property name for the provider name provided by the data provider.
	 * 
	 * This metadata is provided by all data providers.
	 */
	public static final String METADATA_PROVIDER = "ProviderName";

	/**
	 * Metadata property name for Container name provided by the data provider.
	 * By explample, in a dbf file, this is the name of dbf.
	 * 
	 * This metadata is provided by all data providers.
	 */
	public static final String METADATA_CONTAINERNAME = "ContainerName";

	/**
	 * Metadata property name for the feature type provided by the data provider.
	 * 
	 * This metadata is provided by all tabular data providers.
	 */
	public static final String METADATA_FEATURETYPE = "FeatureType";

	/**
	 * Metadata property name for CRS provided by the data provider.
	 * 
	 * This metadata is only provided by data provider with spatial 
	 * information.
	 */
	public static final String METADATA_CRS = "CRS";

	/**
	 * Metadata property name for Envelope provided by the data provider
	 * 
	 * This metadata is only provided by data provider with spatial 
	 * information.
	 */
	public static final String METADATA_ENVELOPE = "Envelope";

	/**
	 * Returns the name associated to the store.
	 * This name is provided for informational purposes only.
	 * Explamples:
	 * 
	 * In a dbf the filename without the path
	 * 
	 * In a DDBB table the name of the table
	 * 
	 * In a WFS layer the name of the layer.
	 *
	 * @return String containing this store's name.
	 */
	public String getName();
	
	/**
	 * Returns a more descriptive name for the store that getName.
	 * This name is provided for informational purposes only.
	 * Explamples:
	 * 
	 * In a file based store may return the full name of the filename, path and filename.
	 * 
	 * In a data base based store may return "server:dbname:tablename"
	 * 
	 * In a WFS layer based store may return "server:layername"
	 * 
	 * @return String Containing the full name of the store
	 */
	public String getFullName();

	/**
	 * Return the of parameters of this store
	 *
	 * @return parameters of this store
	 */
	public DataStoreParameters getParameters();

	/**
	 * Return the provider name that use this store.
	 * 
	 * @return provider name of this store
	 */
	public String getProviderName();
	
	/**
	 * Refreshes this store state.
	 *
	 * @throws DataException
	 */
	public void refresh() throws DataException;

	/**
	 * Returns all available data.
	 *
	 * @return a set of data
	 * @throws DataException
	 *             if there is any error while loading the data
	 */
	DataSet getDataSet() throws DataException;

	/**
	 * Returns a subset of data taking into account the properties and
	 * restrictions of the DataQuery.
	 *
	 * @param dataQuery
	 *            defines the properties of the requested data
	 * @return a set of data
	 * @throws DataException
	 *             if there is any error while loading the data
	 */
	DataSet getDataSet(DataQuery dataQuery) throws DataException;

    /**
     * Provides each value of this Store to the provided {@link Visitor}.
     * The values received through the {@link Visitor#visit(Object)} method
     * may be transient, reused or externally modifiable, so they can't
     * be used to be stored in any external form out of the visit method.
     * 
     * If you need to store any of the values out of the
     * {@link Visitor#visit(Object)} method execution, create a copy or clone
     * the received value in order to be stored.
     * 
     * @param visitor
     *            the visitor to apply to each value.
     * @exception BaseException
     *                if there is an error while performing the visit
     */
    public void accept(Visitor visitor) throws BaseException;

	/**
     * Provides each value of this Store to the provided {@link Visitor}.
     * The values received through the {@link Visitor#visit(Object)} method
     * may be transient, reused or externally modifiable, so they can't
     * be used to be stored in any external form out of the visit method.
     * 
     * If you need to store any of the values out of the
     * {@link Visitor#visit(Object)} method execution, create a copy or clone
     * the received value in order to be stored.
     * 
     * @param visitor
     *            the visitor to apply to each value.
     * @param dataQuery
     *            defines the properties of the data to visit
     * @exception BaseException
     *                if there is an error while performing the visit
     */
	public void accept(Visitor visitor, DataQuery dataQuery)
			throws BaseException;

	/**
	 * Loads all available data and notifies the observer for each loaded block of data.
	 *
	 * @param observer
	 *            to be notified for each block of data loaded
	 * @throws DataException
	 *             if there is any error while loading the data
	 */
	void getDataSet(Observer observer) throws DataException;

	/**
	 * Loads a subset of data taking into account the properties and
	 * restrictions of the DataQuery. Data loading is performed by calling the
	 * Observer, once each data block is loaded.
	 *
	 * @param dataQuery
	 *            defines the properties of the requested data
	 * @param observer
	 *            to be notified for each block of data loaded
	 * @throws DataException
	 *             if there is any error while loading the data
	 */
	void getDataSet(DataQuery dataQuery, Observer observer) throws DataException;

	/**
	 * Returns the selected set of data
	 *
	 * @return DataSet
	 */

	public DataSet getSelection() throws DataException;

	/**
	 * Sets the current data selection with the given data set.
	 *
	 * @param DataSet
	 *            selection
	 * @throws DataException
	 */
	public void setSelection(DataSet selection) throws DataException;

	/**
	 * Creates a new selection.
	 *
	 * @return DataSet that contains the selection
	 *
	 * @throws DataException
	 */
	public DataSet createSelection() throws DataException;

	/**
	 * Returns an iterator over this store children
	 *
	 * @return Iterator over this DataStore children
	 */
	public Iterator getChildren();

	/**
	 * Returns the DataServerExplorer to which this DataStore belongs, if there
	 * is any.
	 * 
	 * @return DataServerExplorer to which this DataStore belongs, or
	 *         <code>null</code> if this was not accessed through any
	 *         DataServerExplorer.
	 * 
	 * @throws DataException
	 * @throws ValidateDataParametersException
	 */
	public DataServerExplorer getExplorer() throws DataException,
			ValidateDataParametersException;


	/**
	 * Returns a new instance of a {@link DataQuery}.
	 *
	 * @return new {@link DataQuery} instance.
	 *
	 * @throws DataException
	 */
	public DataQuery createQuery();
	
	/**
	 * Gets the {@link Interval} of the store, that means the temporal
	 * interval where the store has valid data.
	 * @return
	 *         a time interval or null if there is not time support
	 */
	public Interval getInterval();
	
	/**
	 * Gets all the possible values of time for which the store has data.  
	 * @return
	 *         a collection of {@link Time} objects.
	 */
	public Collection getTimes();
	
	/**
	 * Gets all the possible values of time for which the store has data
	 * and intersects with an interval.
	 * @param interval
	 *         the interval of time
	 * @return
	 *         a collection of {@link Time} objects.
	 */
	public Collection getTimes(Interval interval);
}

