
package org.gvsig.fmap.dal;

import org.gvsig.fmap.dal.DataStoreProviderFactory;


public interface ExternalStoreProviderFactory extends DataStoreProviderFactory {
    
}
