/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.exception;


/**
 * FIXME
 *
 *
 */

public class ReadException extends DataException {

	/**
	 *
	 */
	private static final long serialVersionUID = -8512708060535066804L;
	private final static String MESSAGE_FORMAT = "There was errors loading from '%(store)'.";
	private final static String MESSAGE_KEY = "_ReadException";

	public ReadException(String store, Throwable cause) {
		super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
		setValue("store", store);
	}
        
        protected ReadException(String fmt, Throwable cause, String msgkey, int code) {
            super(fmt, cause, msgkey, code);
        }        
        
        protected ReadException(String fmt, Throwable cause, String msgkey, long code) {
            super(fmt, cause, msgkey, code);
        }

}
