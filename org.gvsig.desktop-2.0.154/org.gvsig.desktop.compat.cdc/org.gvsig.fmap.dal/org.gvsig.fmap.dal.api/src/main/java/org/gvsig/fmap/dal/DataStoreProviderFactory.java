/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.service.spi.ProviderFactory;

public interface DataStoreProviderFactory extends ProviderFactory {

    public static final int UNKNOWN = 0;
    public static final int YES = 1;
    public static final int NO = 2;

    /**
     * Returns the name of the provider
     *
     * @return name of the provider
     */
    public String getName();

    /**
     * Return a short descripion about the provider
     *
     * @return description about the provider
     */
    public String getDescription();

    /**
     * The provider has read support
     *
     * @return YES if has read support
     */
    public int allowRead();

    /**
     * The provider has write support
     *
     * @return YES if has write support
     */
    public int allowWrite();

    /**
     * The provider can create new stores.
     *
     * @return YES if has creation support
     */
    public int allowCreate();

    /**
     * The provider has tabular support. This support is the minimum requisite
     * for a FeatureStore.
     *
     * @return YES if has write support
     */
    public int hasTabularSupport();

    /**
     * The provider has vectorial support.
     *
     * @return YES if has vectorial support
     */
    public int hasVectorialSupport();

    /**
     * The provider has raster support.
     *
     * @return YES if has raster support
     */
    public int hasRasterSupport();

    /**
     * Builds a specific provider
     *
     * @param parameters
     * @param providerServices
     * @return the provider
     * @throws org.gvsig.fmap.dal.exception.InitializeException
     */
    public DataStoreProvider createProvider(DataParameters parameters, DataStoreProviderServices providerServices) throws InitializeException;

}
