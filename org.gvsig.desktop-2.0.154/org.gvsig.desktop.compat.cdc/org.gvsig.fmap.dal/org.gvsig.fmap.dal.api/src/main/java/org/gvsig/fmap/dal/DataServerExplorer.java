/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import java.io.File;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.Disposable;

/**
 * DataServerExplorer is an abstraction for any type of data server. It allows
 * connecting to the server and browsing its contents.
 *
 * More specifically, this interface provides a list of the available data
 * stores in a server.
 */
public interface DataServerExplorer extends Disposable {

	/**
	 * Returns the DataServerExplorer's name
	 *
	 * @return String containing this DataServerExplorer's name
	 */
	public String getProviderName();

	/**
	 * Indicates whether this DataServerExplorer can create a new DataStore in the
	 * server.
	 *
	 * @return true if this DataServerExplorer can be created or false otherwise.
	 */
	public boolean canAdd();

	/**
	 * Indicates whether this DataServerExplorer can create a new DataStore in
	 * the server, given the store name.
	 *
	 * @param storeName
	 *            store name.
	 *
	 * @return true if this DataServerExplorer can create a new store or false
	 *         otherwise.
	 *
	 * @throws DataException
	 */
	public boolean canAdd(String storeName)
			throws DataException;

	/**
	 * Provides a list of available stores in the server.
	 *
	 * @return list of DataStoreParameters
	 *
	 * @throws DataException
	 */
	public List list() throws DataException;

        public DataStoreParameters get(String name) throws DataException;
        
	public static final int MODE_ALL = 0;
	public static final int MODE_FEATURE = 1;
	public static final int MODE_GEOMETRY = 2;
	public static final int MODE_RASTER = 4;

	/**
	 * Provides a list of available stores in the server of a type.
	 * 
	 * @param mode
	 *            , filter store from a type: {@link #MODE_ALL},
	 *            {@link #MODE_FEATURE}, {@link #MODE_FEATURE_GEOMETRY},
	 *            {@link #MODE_RASTER}
	 * 
	 * @return list of DataStoreParameters
	 * 
	 * @throws DataException
	 */
	public List list(int mode) throws DataException;

	/**
	 * Creates a new DataStore in this server.
	 *
	 * @param parameters
	 *            , an instance of DataStoreParameters from
	 *            {@link DataServerExplorer#getAddParameters(String)} that
	 *            describes the new DataStore.
	 * @param overwrite
	 *            if the store already exists
	 *
	 * @return true if the DataStoreParameters were successfully added, false
	 *         otherwise.
	 *
	 * @throws DataException
	 */
	public boolean add(String provider, NewDataStoreParameters parameters, boolean overwrite)
			throws DataException;

	/**
	 * Removes a store from the server given its DataStoreParameters. If the
	 * store is a file then this method deletes the file, if it is a table in a
	 * database then this method drops the table, and so on.
	 *
	 * @param parameters
	 * @throws DataException
	 */
	void remove(DataStoreParameters parameters) throws DataException;

	/**
	 * Given the store's name, returns its parameters for creation.
	 *
	 * @param storeName
	 *
	 * @return parameters to create a store
	 *
	 * @throws DataException
	 */
	public NewDataStoreParameters getAddParameters(String storeName)
			throws DataException;

	/**
	 * Returns this DataServerExplorer parameters
	 *
	 * @return an instance of DataServerExplorerParameters containing this
	 *         DataServerExplorer parameters.
	 */
	public DataServerExplorerParameters getParameters();

	/**
	 * Return the list of provider names that this server allow.
	 *  
	 * @return List of provider names
	 */
	public List getDataStoreProviderNames();

        /**
         * Return the file resource associated to this name and store.
         * If the resource not exists or the explorer don't support this opperation
         * return null.
         * 
         * @param dataStore
         * @param resourceName
         * @return file resource or null
         * @throws DataException 
         */
        public File getResourcePath(DataStore dataStore, String resourceName) throws DataException;

}
