/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;


/**
 * Abstract feature rule intended for giving a partial default implementation 
 * of the {@link FeatureRule} interface to other rule implementations. It is recommended
 * to extend this class when implementing new {@link FeatureRule}s.
 * 
 * @author Vicente Caballero Navarro
 */
public abstract class AbstractFeatureRule implements FeatureRule {
	protected String name;
	protected String description;
	protected boolean checkAtUpdate;
	protected boolean checkAtFinishEdition;

	protected AbstractFeatureRule(String name, String description) {
		this.init(name,description,true,true);
	}

	protected AbstractFeatureRule(String name, String description,
			boolean checkAtUpdate, boolean checkAtFinishEdition) {
		this.init(name, description, checkAtUpdate, checkAtFinishEdition);
	}

	protected void init(String name, String description, boolean checkAtUpdate,
			boolean checkAtFinishEdition) {
		this.name=name;
		this.description=description;
		this.checkAtUpdate = checkAtUpdate;
		this.checkAtFinishEdition = checkAtFinishEdition;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean checkAtFinishEditing() {
		return this.checkAtFinishEdition;
	}

	public boolean checkAtUpdate() {
		return this.checkAtUpdate;
	}

}
