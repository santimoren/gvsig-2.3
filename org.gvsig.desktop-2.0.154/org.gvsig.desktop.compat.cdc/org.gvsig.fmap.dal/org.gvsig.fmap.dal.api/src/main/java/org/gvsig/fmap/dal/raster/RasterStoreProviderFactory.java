
package org.gvsig.fmap.dal.raster;

import org.gvsig.fmap.dal.DataStoreProviderFactory;


public interface RasterStoreProviderFactory extends DataStoreProviderFactory {
    
}
