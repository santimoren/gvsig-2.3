/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.exception;

import org.gvsig.fmap.dal.exception.DataException;

/**
 * 
 * Used to notify the case when the name of an attribute is not valid
 * for a certain data store. Example: A field name longer than
 * 11 characters in a DBF file. Perhaps: a field name with invalid characters
 * in a database.
 * 
 * @author jldominguez
 *
 */
public class AttributeNameException extends DataException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static String MESSAGE_FORMAT =
			"Attribute name '%(attrname)' not valid in '%(storeName)'.";
	private final static String MESSAGE_KEY = "_AttributeNameException";

	public AttributeNameException(
			String attrname,
			String storeName) {
		
		super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
		setValue("attrname", attrname);
		setValue("storeName", storeName);

	}
	
	public AttributeNameException(
			String messageFormat, String messageKey, long code) {
		
		super(messageFormat, messageKey, code);
	}


}
