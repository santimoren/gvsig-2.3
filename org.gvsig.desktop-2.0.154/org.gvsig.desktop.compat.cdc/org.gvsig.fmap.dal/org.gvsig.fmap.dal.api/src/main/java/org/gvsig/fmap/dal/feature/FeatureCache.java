/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.dal.feature;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.geom.primitive.Envelope;

/**
 * This class represents a vectorial cache that is used to save and retrieve
 * {@link FeatureSet}s. First time that a {@link FeatureStore} have to retrieve
 * data from a {@link FeatureStoreProvider} can save the {@link FeatureSet} in
 * the cache with its envelope. Next time that the store has to access to a envelope
 * contained in the previous envelope can retrieve the features directly 
 * from the cache and it is not necessary to retrieve data from the provider.
 * 
 * The cache supports different scales and creates
 *  
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public interface FeatureCache {

	/**
	 * Delete all the features for a concrete scale.
	 * @param scale
	 * The scale.
	 * @throws DataException
	 */
	public void delete(double scale) throws DataException;
	
	/**
	 * Delete all the features for all the scales in the cache system.
	 * @throws DataException
	 */
	public void deleteAll() throws DataException;
	
	/**
	 * Add a set of features contained in a envelope for a concrete scale.
	 * @param featureSet
	 * The feature set retrieved from the server.
	 * @throws DataException
	 */
	public void addFeatures(FeatureSet featureSet, FeatureStore featureStore) throws DataException;
	
	/**
	 * Return a feature set form a concrete envelope and a scale.
	 * @param envelope
	 * The envelope.
	 * @param scale
	 * The scale.
	 * @return
	 * A feature set contained in the evelope.
	 * @throws DataException
	 */
	public FeatureSet getFeatureSet(Envelope envelope, double scale) throws DataException;
}

