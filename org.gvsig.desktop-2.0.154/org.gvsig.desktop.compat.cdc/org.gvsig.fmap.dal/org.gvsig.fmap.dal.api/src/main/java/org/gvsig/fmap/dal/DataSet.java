/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.Visitable;
import org.gvsig.tools.visitor.Visitor;

/**
 * <p> Interface that represents a generic set of data. It may proceed either from a data 
 * query, a user selection or a collection of locked elements.</p>
 */
public interface DataSet extends Visitable, Disposable {
	
	/**
	 * Indicates whether this DataSet belongs to a specific store
	 * @param store 
	 * 			a DataStore   
	 * @return true if this belongs to the given DataStore, false if not.
	 */
	public boolean isFromStore(DataStore store);

    /**
     * Provides each value of this Store to the provided {@link Visitor}.
     * The values received through the {@link Visitor#visit(Object)} method
     * may be transient, reused or externally modifiable, so they can't
     * be used to be stored in any external form out of the visit method.
     * 
     * If you need to store any of the values out of the
     * {@link Visitor#visit(Object)} method execution, create a copy or clone
     * the received value in order to be stored.
     * 
     * @param visitor
     *            the visitor to apply to each value.
     * @exception BaseException
     *                if there is an error while performing the visit
     */
    public void accept(Visitor visitor) throws BaseException;

}
