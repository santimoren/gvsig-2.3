/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import java.util.List;

import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.Tags;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.exception.BaseException;

/**
 * There are two top level management roles within DAL: data access and resource management.
 *
 * This class is responsible of the data access management role. It provides ways
 * for registering and instantiating {@link DataServerExplorer}(s), {@link DataStore}(s),
 * {@link Evaluator}(s) and {@link FeatureIndex}(es).
 *
 * @see ResourceManager
 *
 */

public interface DataManager {
        public static final String CREATE_STORE_AUTHORIZATION = "dal-create-store";
        public static final String READ_STORE_AUTHORIZATION = "dal-read-store";
        public static final String WRITE_STORE_AUTHORIZATION = "dal-write-store";

        /**
	 * Returns the default DAL's temporary directory
	 *
	 * @return Temporary directory name
	 */
	public String getTemporaryDirectory();

	/*
	 * ====================================================================
	 *
	 * Store related services
	 */

	/**
	 * Creates, initializes and returns an instance of DataStoreParameters given
	 * the name with which their provider is registered.
	 * 
	 * @param name  provider name
         * @return the data store parameters
	 * 
	 * @throws ProviderNotRegisteredException
	 *             if the memory provider is not registered
	 * @throws InitializeException
	 *             if there is an error initializing the parameters for the
	 *             memory provider
	 **/
	public DataStoreParameters createStoreParameters(String name)
			throws InitializeException, ProviderNotRegisteredException;
        

	/**
	 * Creates, initializes and fill an instance of DataStoreParameters from
         * the tags of the DynStruct passed as parameter.
	 * 
	 * @param struct structure from which tags were created ths parameters.
         * @return the data store parameters
	 * 
	 * @throws ProviderNotRegisteredException
	 *             if the memory provider is not registered
	 * @throws InitializeException
	 *             if there is an error initializing the parameters for the
	 *             memory provider
	 **/
        public DataStoreParameters createStoreParameters(DynStruct struct)
			throws InitializeException, ProviderNotRegisteredException;

        public DataStoreParameters createStoreParameters(Tags tags) throws InitializeException, ProviderNotRegisteredException;
        
	/**
	 * Creates, initializes and returns an instance of NewDataStoreParameters
	 * given the name with which their provider is registered.
	 * 
	 * @param explorer
         * @param provider
         * @return 
	 * 
	 * @throws InitializeException
	 * @throws ProviderNotRegisteredException
	 */
	public NewDataStoreParameters createNewStoreParameters(String explorer, String provider)
			throws InitializeException, ProviderNotRegisteredException;

	/**
	 * 
	 * Creates, initializes and returns an instance of DataStore given the
	 * DataStoreParameters.
	 * 
         * @param provider
	 * @param parameters
	 *            parameters used to instantiate and initialize the DataStore
         * @return 
	 * 
	 * @throws ProviderNotRegisteredException
	 *             if the memory provider is not registered
	 * @throws InitializeException
	 *             if there is an error initializing the parameters for the
	 *             memory provider
	 * @throws ValidateDataParametersException
	 *             if the parameters to open the memory based store are not
	 *             valid
	 */
	public DataStore openStore(String provider, DynObject parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

	public DataStore openStore(String provider, DataStoreParameters parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

        public DataStore openStore(DynStruct struct) 
                throws InitializeException, ProviderNotRegisteredException, ValidateDataParametersException ;

        /**
	 * @deprecated see openStore
	 */
	public DataStore createStore(DynObject parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

        /**
	 * @deprecated see openStore
	 */
        public DataStore createStore(DataStoreParameters parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;


	/**
	 * Create a new physical store 
	 *  
	 * @param parameters
	 *
	 * @throws InitializeException
	 * @throws ProviderNotRegisteredException
	 * @throws ValidateDataParametersException
	 */
	public void newStore(String explorer, String provider, NewDataStoreParameters parameters, boolean overwrite)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

	/**
	 * Returns a list of Strings containing the names of all available DataStore
	 * providers.
	 *
	 * @return list of String containing available DataStore provider names
	 */
	public List getStoreProviders();

	/**
	 * Returns a list of Strings containing the names of all available DataStore
	 * providers for an explorer.
	 * 
	 * @param explorer
	 *            name
	 */
	public List getStoreProviders(String name);

	/*
	 * ====================================================================
	 *
	 * Explorer related services
	 */
	/**
	 * Returns an instance of {@link DataServerExplorerParameters} corresponding to
	 * the given name.
	 *
	 * @param name
	 *            name of a registered server explorer provider
	 *
	 * @throws InitializeException
	 * 			if parameter initialization causes an error.
	 *
	 * @throws ProviderNotRegisteredException
	 * 			if could not find a provider by the given name.
	 *
	 **/
	public DataServerExplorerParameters createServerExplorerParameters(
			String name)
			throws InitializeException, ProviderNotRegisteredException;

	/**
	 * Returns an instance of {@link DataServerExplorer} given its parameters.
	 *
	 * @param parameters
	 *            parameters used to instantiate and initialize the
	 *            {@link DataServerExplorer}.
	 *
	 * @return an instance of {@link DataServerExplorer}.
	 *
	 * @throws InitializeException
	 *
	 * @throws ProviderNotRegisteredException
	 * @throws ValidateDataParametersException
	 */
	public DataServerExplorer openServerExplorer(
			String name,
			DataServerExplorerParameters parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

	/**
	 * @deprecated see openServerExplorer
	 */
	public DataServerExplorer createServerExplorer(
			DataServerExplorerParameters parameters)
			throws InitializeException, ProviderNotRegisteredException,
			ValidateDataParametersException;

	/**
	 * Returns a list of String containing the names of the available
	 * DataServerExplorer providers.
	 *
	 * @return list of String containing the names of the available
	 *         DataServerExplorer providers.
	 */
	public List getExplorerProviders();

	/*
	 * ====================================================================
	 *
	 * Expression evaluation related services
	 */

	/**
	 * Registers the default expression evaluator. It is used by DAL to evaluate
	 * and resolve query filters and expressions.
	 *
	 * @param evaluator
	 *            Class that will be called to evaluate the expression. It must
	 *            implement {@link Evaluator}.
	 */
	public void registerDefaultEvaluator(Class evaluator);

	/**
	 * Creates an instance of Evaluator that represents the given expression.
	 *
	 * @param expression
	 *            String containing a CQL expression.
	 * @return instance of Evaluator representing the given expression.
	 * @throws InitializeException
	 */
	public Evaluator createExpresion(String expression)
			throws InitializeException;

	/*
	 * ====================================================================
	 *
	 * Index related services
	 */


	/**
	 * Returns a list of String containing the names of the available index providers.
	 *
	 * @return
	 * 		list of strings with the names of the available index providers
	 */
	public List getFeatureIndexProviders();

	/**
	 * Sets the default DataIndexProvider for the given data type.
	 *
	 * @param dataType
	 * 				one of the data types defined in {@link DataTypes}.
	 * @param name
	 * 			Provider's name
	 */
    public void setDefaultFeatureIndexProviderName(int dataType, String name);

	/**
	 * Returns the default DataIndexProvider name, given a data type. Data types
	 * are defined in {@link DataTypes}.
	 *
	 * @param dataType
	 *            one of the constants in {@link DataTypes}.
	 *
	 * @return
	 * 		the name of the default {@link FeatureIndexProvider} if there is
	 * 		anyone available for the given data type.
	 */
    public String getDefaultFeatureIndexProviderName(int dataType);

    /**
	 * Returns a list of String containing the names of the available cache providers.
	 *
	 * @return
	 * 		list of strings with the names of the available cache providers
	 */    
    public List getFeatureCacheProviders();

	/**
	 * Returns an instance of {@link DataServerExplorerParameters} corresponding
	 * to the given name used by the cache to create a store to save the
	 * retrieved data.
	 * 
	 * @param name
	 *            name of a registered feature cache provider
	 * 
	 * @throws InitializeException
	 *             if parameter initialization causes an error.
	 * 
	 * @throws ProviderNotRegisteredException
	 *             if could not find a cache provider by the given name.
	 * 
	 */
	public DynObject createCacheParameters(String name)
			throws InitializeException, ProviderNotRegisteredException;

	/**
	 * Utility method to create the {@link DataStoreParameters} to create a
	 * {@link FeatureStore} based on the {@link MemoryStoreProvider}.
	 * 
	 * @param autoOrderAttributeName
	 *            the name of the {@link Feature} attribute to be used to order
	 *            the store {@link Feature}s by default. Set to null if you
	 *            don't want any order by default
	 * @return the parameters for the memory based store
	 * @throws InitializeException
	 *             if there is an error initializing the parameters for the
	 *             memory provider
	 */
	public DataStoreParameters createMemoryStoreParameters(
			String autoOrderAttributeName) throws InitializeException;

	/**
	 * Utility method to create the a {@link FeatureStore} based on the
	 * {@link MemoryStoreProvider}.
	 * 
	 * @param autoOrderAttributeName
	 *            the name of the {@link Feature} attribute to be used to order
	 *            the store {@link Feature}s by default. Set to null if you
	 *            don't want any order by default
	 * @return the the memory based store
	 * @throws InitializeException
	 *             if there is an error initializing the parameters for the
	 *             memory provider
	 */
	public FeatureStore createMemoryStore(String autoOrderAttributeName)
			throws InitializeException;

    /**
     * Creates a {@link FeaturePagingHelper} to paginate data from a
     * {@link FeatureStore}.
     * 
     * @param featureStore
     *            to get the {@link Feature}s from
     * @param pageSize
     *            the page size
     * @return a {@link FeaturePagingHelper}
     * @throws BaseException
     *             if there is an error creating the helper
     */
	public FeaturePagingHelper createFeaturePagingHelper(
        FeatureStore featureStore, int pageSize) throws BaseException;

	/**
     * Creates a {@link FeaturePagingHelper} to paginate data from a
     * {@link FeatureStore}.
     * 
     * @param featureStore
     *            to get the {@link Feature}s from
     * @param featureQuery
     *            to filter and/or order the data
     * @param pageSize
     *            the page size
     * @return a {@link FeaturePagingHelper}
     * @throws BaseException
     *             if there is an error creating the helper
     */
	public FeaturePagingHelper createFeaturePagingHelper(
			FeatureStore featureStore, FeatureQuery featureQuery, int pageSize)
        throws BaseException;
	
	public void setOpenErrorHandler(OpenErrorHandler handler);
	
	public OpenErrorHandler  getOpenErrorHandler();
	
	public DataStoreProviderFactory getStoreProviderFactory(String name);

	public EditableFeatureType createFeatureType();
	
	public List getDataTypes();
	
	/**
	 * Registers a class that can be used to create a {@link FeatureAttributeGetter}
	 * and associate it to a {@link FeatureAttributeDescriptor}.
	 * 
   	 * @param name
   	 *             the name used to register the class.
	 * @param clazz
	 *             it has to be an instance of {@link FeatureAttributeDescriptor}         
	 */
	public void registerFeatureAttributeGetter(String name, Class clazz);
	
	/**
	 * Creates a {@link FeatureAttributeGetter} by name. If there is not any class
	 * registered with this name or if there is any error an exception is thrown.
	 * 
	 * @param name
	 *             the name that was used to register the class              
	 * @return
	 *             a {@link FeatureAttributeGetter}
	 * @throws InitializeException
	 *             if there is any error creating the object
	 */
	public FeatureAttributeGetter createFeatureAttributeGetter(String name) throws InitializeException;
        
        public DataServerExplorerPool getDataServerExplorerPool();
        public void setDataServerExplorerPool(DataServerExplorerPool pool);

}
