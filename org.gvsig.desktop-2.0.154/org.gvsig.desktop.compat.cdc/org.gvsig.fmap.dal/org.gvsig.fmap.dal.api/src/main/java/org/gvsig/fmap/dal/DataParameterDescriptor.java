/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

/**
 * This interface describes the properties of a parameter. It is useful 
 * for any component that needs to know how to edit or visualize it.
 *
 */
public interface DataParameterDescriptor {
	
	/** Accepts a single value */
	public static final int SINGLE = 1;
	/** Accepts a single value from a value set */
	public static final int CHOICE = 2;
	/** Accepts a range of values defined with a minimum and a maximum value */
	public static final int RANGE = 3;

	/**
	 * Returns the parameter's name
	 * 
	 * @return String containing the parameter's name
	 */
	public String getName();
	
	/**
	 * Returns the parameter's description
	 * 
	 * @return String containing the parameter's description.
	 */
	public String getDescription();

	/**
	 * Returns the parameter's data type.
	 * 
	 * @return parameter's data type.
	 * 
	 * @see DataTypes
	 */
	public int getDataType();

	/**
	 * Returns the parameter's default value.
	 * 
	 * @return an Object containing the default value. Use the data type to cast.
	 */
	public Object getDefaultValue();
	
	/**
	 * Returns one of the available values type.
	 * 
	 * @return an <code>int</code> with one of the available values type.
	 * 
	 * @see #SINGLE
	 * @see #CHOICE
	 * @see #RANGE
	 */
	public int getAvailableValuesType();

	/**
	 * Returns an array containing the available values accepted by the parameter.
	 * 
	 * @return array of Object containing the available values accepted by the parameter. Use the data type to cast.
	 */
	public Object[] getAvailableValues();

	/**
	 * Returns the minimum value when the parameter accepts a range of values.
	 * 
	 * @return range's minimum value
	 */
	public Object getMinValue();

	/**
	 * Returns the maximum value when the parameter accepts a range of values.
	 * 
	 * @return range's maximum value.
	 */
	public Object getMaxValue();
}
