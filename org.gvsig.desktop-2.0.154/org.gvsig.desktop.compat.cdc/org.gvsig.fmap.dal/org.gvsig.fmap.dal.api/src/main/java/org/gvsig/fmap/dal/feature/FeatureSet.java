/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.IndexedVisitable;
import org.gvsig.tools.visitor.Visitor;

/**
 * A FeatureSet represents a set of {@link Feature}(s). These sets of features
 * are typically obtained directly from a {@link FeatureStore}, or through a
 * {@link FeatureQuery}.
 * 
 * A FeatureSet may contain subsets of {@link Feature}(s) of different
 * {@link FeatureType}(s). It allows iterating and editing the features.
 * 
 * FIXME: Actualizar javadoc
 * 
 * Si el store en el que esta basado el featureset es modificado, se realiza un
 * update, insert, delete o se modifican los featuretype de este, el FeatureSet
 * quedara invalidado, y cualquier acceso a el provocara que se lance una
 * excepcion de tiempo de ejecucion ConcurrentModificationException.
 * 
 * Habria que indicar que invocar al metodo accept del interface visitor
 * provocara la excepcion ConcurrentModificationException si el store a sido
 * modificado desde otro punto.
 * 
 * Indicar que los metodos insert/delete/update ademas de actuar sobre el set,
 * actuan sobre el store en el que este esta basado, pero que no invalidan el
 * set sobre el que se ejecutan. No se si esto deberia hacerse mencion en esos
 * metodos o en la doc general del featureset.
 * 
 */
public interface FeatureSet extends DataSet, IndexedVisitable {

	/**
	 * Returns the default {@link FeatureType} of this FeatureSet.
	 * 
	 * @return default {@link FeatureType} in this FeatureSet.
	 */
	public FeatureType getDefaultFeatureType();

	/**
	 * Returns a list of the {@link FeatureType}(s) in this FeatureSet.
	 * 
	 * @return list of the {@link FeatureType}(s) in this FeatureSet.
	 */
	public List getFeatureTypes();

	/**
	 * Returns the number of {@link Feature}(s) contained in this FeatureSet.
	 * 
	 * The value returned by this method won't be accurate when
	 * the FeatureStore is being edited and this set's features
	 * are modified, added or deleted.
	 *  
	 * @return number of {@link Feature}(s) contained in this FeatureSet.
	 * 
	 * @throws DataException
	 */
    public long getSize() throws DataException;

    /**
     * Returns an iterator over the elements in this collection, in the order
     * (if any) defined when the collection was obtained.
     * 
     * The iterator starts at the specified position in this collection. The
     * specified index indicates the first element that would be returned by an
     * initial call to the <tt>next</tt> method. An initial call to the
     * <tt>previous</tt> method would return the element with the specified
     * index minus one.
     * 
     * <p>
     * <em>
     * <strong>NOTE:</strong> if you use this method to get the iterator, you
     * must get sure the iterator is disposed (@see
     * {@link DisposableIterator#dispose()}) in any case, even if an error occurs
     * while getting the data. It is recommended to use the <code>accept</code>
     * methods instead, which handle everything for you. 
     * Take into account the accept methods may use a fast iterator to 
     * get the features.
     * </em>
     * </p>
     * 
     * @see #accept(org.gvsig.tools.visitor.Visitor)
     * @see #accept(org.gvsig.tools.visitor.Visitor, long)
     * @see #fastIterator()
     * @see #fastIterator(long)
     * 
     * @param index
     *            index of first element to be returned from the iterator (by a
     *            call to the <tt>next</tt> method).
     * @return an iterator of the elements in this collection (in proper
     *         sequence), starting at the specified position in the collection.
     * @throws DataException
     *             if the index is out of range (index &lt; 0 || index &gt;
     *             size()).
     * @deprecated use {@link #fastIterator(long)} instead
     */
	DisposableIterator iterator(long index) throws DataException;

    /**
     * Returns an iterator over the elements in this collection, in the order
     * (if any) defined when the collection was obtained.
     * 
     * @see #accept(org.gvsig.tools.visitor.Visitor)
     * @see #accept(org.gvsig.tools.visitor.Visitor, long)
     * @see #fastIterator()
     * @see #fastIterator(long)
     * 
     * @return an iterator of the elements in this collection (in proper
     *         sequence).
     * 
     * @deprecated use fastiterator. In next versions the signature of this
     * method will be changed to "Iterator<Feature> iterator()".
     */
     DisposableIterator iterator();

    /**
     * Returns a fast iterator over this set.
     * <p>
     * Fast in this case means that each of the elements returned may be a
     * reused or pooled object instance, so don't use it to be stored in any
     * way.
     * </p>
     * <p>
     * If you need to store one of the {@link Feature} of the iterator, use the
     * {@link Feature#getCopy()} to create a clone of the object.
     * </p>
     * 
     * <p>
     * <em>
     * <strong>NOTE:</strong> if you use this method to get the iterator, you
     * must get sure the iterator is disposed (@see
     * {@link DisposableIterator#dispose()}) in any case, even if an error occurs
     * while getting the data. It is recommended to use the <code>accept</code>
     * methods instead, which handle everything for you. 
     * Take into account the accept methods may use a fast iterator to 
     * get the features.
     * </em>
     * </p>
     * 
     * @see #accept(org.gvsig.tools.visitor.Visitor)
     * @see #accept(org.gvsig.tools.visitor.Visitor, long)
     * 
     * @return an iterator over this set.
     * 
     * @throws DataException
     */
	public DisposableIterator fastIterator() throws DataException;

    /**
     * Returns a fast iterator over this set, starting from the given index.
     * <p>
     * Fast in this case means that each of the elements returned may be a
     * reused or pooled object instance, so don't use it to be stored in any
     * way.
     * </p>
     * <p>
     * If you need to store one of the {@link Feature} of the iterator, use the
     * {@link Feature#getCopy()} to create a clone of the object.
     * </p>
     * 
     * <p>
     * <em>
     * <strong>NOTE:</strong> if you use this method to get the iterator, you
     * must get sure the iterator is disposed (@see
     * {@link DisposableIterator#dispose()}) in any case, even if an error occurs
     * while getting the data. It is recommended to use the <code>accept</code>
     * methods instead, which handle everything for you. 
     * Take into account the accept methods may use a fast iterator to 
     * get the features.
     * </em>
     * </p>
     * 
     * @see #accept(org.gvsig.tools.visitor.Visitor)
     * @see #accept(org.gvsig.tools.visitor.Visitor, long)
     * 
     * @param index
     *            position in which the iterator is initially located.
     * 
     * @return an iterator initially located at the position indicated by the
     *         given index
     * 
     * @throws DataException
     */
	public DisposableIterator fastIterator(long index) throws DataException;

	/**
	 * Indicates whether this FeatureSet contains zero features.
	 * 
	 * The value returned by this method won't be accurate when
	 * the FeatureStore is being edited and this set's features
	 * are modified, added or deleted.
	 *  
	 * @return true if this FeatureSet is empty, false otherwise.
	 * 
	 * @throws DataException
	 */
	boolean isEmpty() throws DataException;

	/**
	 * Updates a {@link Feature} with the given {@link EditableFeature}.<br>
	 * 
	 * Any {@link DisposableIterator} from this store that was still in use can will not
	 * reflect this change.
	 * 
	 * @param feature
	 *            an instance of {@link EditableFeature} with which to update
	 *            the associated {@link Feature}.
	 * 
	 * @throws DataException
	 */
	public void update(EditableFeature feature) throws DataException;

	/**
	 * Deletes a {@link Feature} from this FeatureSet.<br>
	 * 
	 * Any {@link DisposableIterator} from this store that was still in use will be
	 * <i>unsafe</i>. Use {@link DisposableIterator#remove()} instead.
	 * 
	 * @param feature
	 *            the {@link Feature} to delete.
	 * 
	 * @throws DataException
	 */
	public void delete(Feature feature) throws DataException;

	/**
	 * Inserts a new feature in this set. It needs to be an instance of
	 * {@link EditableFeature} as it has not been stored yet.<br>
	 * 
	 * Any {@link DisposableIterator} from this store that was still in use can will not
	 * reflect this change.
	 * 
	 * @param feature
	 *            the {@link EditableFeature} to insert.
	 * 
	 * @throws DataException
	 */
	public void insert(EditableFeature feature) throws DataException;

    /**
     * Returns a {@link DynObjectSet} of the contents of this set.
     * Defaults to fast iteration.
     * 
     * @return a {@link DynObjectSet}
     */
    public DynObjectSet getDynObjectSet();

    /**
     * Returns a {@link DynObjectSet} of the contents of this set.
     * 
     * @param fast
     *            if the set will be able to be iterated in a fast way, by
     *            reusing the {@link DynObject} instance for each
     *            {@link Feature} instance.
     * @return a {@link DynObjectSet}
     */
    public DynObjectSet getDynObjectSet(boolean fast);

    /**
     * Provides each value of this Store to the provided {@link Visitor},
     * beginning from the provided index position.
     * The values received through the {@link Visitor#visit(Object)} method
     * may be transient, reused or externally modifiable, so they can't
     * be used to be stored in any external form out of the visit method.
     * 
     * If you need to store any of the values out of the
     * {@link Visitor#visit(Object)} method execution, create a copy or clone
     * the received value in order to be stored.
     * 
     * @param visitor
     *            the visitor to apply to each value.
     * @param firstValueIndex
     *            index of first element to be visited by the {@link Visitor}
     * @exception BaseException
     *                if there is an error while performing the visit
     */
    void accept(Visitor visitor, long firstValueIndex) throws BaseException;
}