/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataEvaluatorRuntimeException;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class FeatureQueryOrder implements Persistent, Cloneable {

	private List members = new ArrayList();

	public class FeatureQueryOrderMember implements Persistent, Cloneable {
		String attributeName = null;
		Evaluator evaluator = null;
		boolean ascending;

		FeatureQueryOrderMember(String attributeName, boolean ascending) {
			this.attributeName = attributeName;
			this.ascending = ascending;
		}
		FeatureQueryOrderMember(Evaluator evaluator, boolean ascending) {
			this.evaluator = evaluator;
            this.ascending = ascending;
		}
		public boolean hasEvaluator() {
			return this.evaluator != null;
		}
		public Evaluator getEvaluator() {
			return this.evaluator;
		}
		public boolean getAscending() {
			return this.ascending;
		}
		public String getAttributeName() {
			return this.attributeName;
		}
		public void loadFromState(PersistentState state)
				throws PersistenceException {
			this.attributeName = state.getString("attributeName");
			this.ascending = state.getBoolean("ascending");
			this.evaluator = (Evaluator) state.get("evaluator");
		}

		public void saveToState(PersistentState state)
				throws PersistenceException {
			state.set("attributeName", this.attributeName);
			state.set("ascending", this.ascending);
			if (this.evaluator != null) {
				state.set("evaluator", evaluator);
			}

		}

		public Object clone() throws CloneNotSupportedException {
			// Nothing more to clone
			return super.clone();
		}
	}

	public Object add(String attributeName, boolean ascending) {
        FeatureQueryOrderMember member = new FeatureQueryOrderMember(
                attributeName, ascending);
		if( members.add(member) ) {
			return member;
		}
		return null;
	}

	public Object add(Evaluator evaluator, boolean ascending) {
		FeatureQueryOrderMember member = new FeatureQueryOrderMember(
				evaluator,
				ascending);
		if (members.add(member)) {
			return member;
		}
		return null;
	}

	public Iterator iterator() {
		return members.iterator();
	}

	public boolean remove(FeatureQueryOrderMember member) {
		return members.remove(member);
	}

	public void remove(int index) {
		members.remove(index);
	}

	public void clear() {
		members.clear();
	}

	public int size() {
		return this.members.size();
	}

	public Comparator getFeatureComparator() {
		return new DefaultFeatureComparator(this);
	}

	public Object clone() throws CloneNotSupportedException {
		FeatureQueryOrder clone = (FeatureQueryOrder) super.clone();

		if (members != null) {
			clone.members = new ArrayList(members.size());
			for (int i = 0; i < members.size(); i++) {
				clone.members.add(((Cloneable) members.get(i)).clone());
			}
		}

		return clone;
	}

	public FeatureQueryOrder getCopy() {
		FeatureQueryOrder aCopy = new FeatureQueryOrder();
		Iterator iter = this.members.iterator();
		FeatureQueryOrderMember member;
		while (iter.hasNext()) {
			member = (FeatureQueryOrderMember) iter.next();
			if (member.hasEvaluator()) {
				aCopy.add(member.getEvaluator(), member.getAscending());
			} else {
				aCopy.add(member.getAttributeName(), member.getAscending());
			}
		}
		return aCopy;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.members = (List) state.get("menbers");
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("menbers", members);
	}
	
	private class DefaultFeatureComparator implements Comparator {

		private FeatureQueryOrder order;


		public DefaultFeatureComparator(FeatureQueryOrder order) {
			this.order = order;
			// TODO optimizar en un array???

		}


		private int myCompare(Object arg0, Object arg1) {
			if (arg0 == null){
				if (arg1 == null){
					return 0;
				} else{
					return 1;
				}
			} else if (arg1 == null){
				if (arg0 == null) {
					return 0;
				} else {
					return 1;
				}
			}
			if (arg0 instanceof Comparable) {
				return ((Comparable) arg0).compareTo(arg1);
			} else if (arg1 instanceof Comparable) {
				return ((Comparable) arg1).compareTo(arg0) * -1;
			}

			if (arg0.equals(arg1)){
				return 0;
			} else{
				return -1;
			}

		}

		public int compare(Object arg0, Object arg1) {
			Iterator iter = this.order.iterator();
			int returnValue = 0;
			Feature f0 = (Feature) arg0;
			Feature f1 = (Feature) arg1;
			Object item;
			String attrName;
			Evaluator evaluator;
			while (returnValue == 0 && iter.hasNext()) {
				item = iter.next();
				if (item instanceof String) {
					attrName = (String) item;
					returnValue = this
							.myCompare(f0.get(attrName), f1
							.get(attrName));
				} else {
					evaluator = (Evaluator) item;
					try {
						returnValue = this.myCompare(evaluator
								.evaluate((EvaluatorData) f0), evaluator
								.evaluate((EvaluatorData) f1));
					} catch (EvaluatorException e) {
						throw new DataEvaluatorRuntimeException(e);
					}
				}
			}

			return returnValue;
		}

	}

}