/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature;

import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.lang.Cloneable;

/**
 * This interface represents a container for store transforms.
 *
 * Provides access to transforms by index and by iterator, and also allows
 * adding and removing transforms.
 *
 */
public interface FeatureStoreTransforms extends Cloneable {

	/**
	 * Returns the FeatureStoreTransform given its index.
	 *
	 * @param index
	 * 			a position in this FeatureStoreTransforms
	 *
	 * @return
	 * 		the FeatureStoreTransform that is stored in the given index.
	 */
	public FeatureStoreTransform getTransform(int index);

	/**
	 * Adds a FeatureStoreTransform to this container.
	 *
	 * @param transform
	 * 			the FeatureStoreTransform to add
	 *
	 * @return
	 * 		the added FeatureStoreTransform
	 *
	 * @throws DataException
	 */
	public FeatureStoreTransform add(FeatureStoreTransform transform)
			throws DataException;

	/**
	 * Returns the number of FeatureStoreTransforms stored in this container
	 *
	 * @return
	 * 		number of FeatureStoreTransforms stored in this container
	 */
	public int size();

	/**
	 * Indicates whether this container is empty.
	 *
	 * @return
	 * 		true if this container is empty, false if not
	 */
	public boolean isEmpty();

	/**
	 * Returns an iterator over this container elements.
	 *
	 * @return
	 * 		an iterator over this container elements.
	 */
	public Iterator iterator();

	/**
	 * Empties this container.
	 *
	 */
	public void clear();

	/**
	 * Removes the {@link FeatureStoreTransform} given its index.
	 *
	 * @param index
	 * 			the position of the {@link FeatureStoreTransform} to remove.
	 * @return
	 * 		the removed object
	 */
	public Object remove(int index);

	/**
	 * Removes the given {@link FeatureStoreTransform}.
	 *
	 * @param transform
	 * 				{@link FeatureStoreTransform} to remove
	 * @return
	 * 		true if the transform was successfully removed, false if not.
	 */
	public boolean remove(FeatureStoreTransform transform);

	/**
	 * Retruns true if any {@link FeatureStoreTransform} make changes of any
	 * previous attributes values.<br>
	 *
	 * If all {@link FeatureStoreTransform} affects only to attributes defition,
	 * the {@link FeatureSet} performance can be better.
	 *
	 * @return
	 */
	public boolean isTransformsOriginalValues();

}
