/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

/**
 * Contains an action to execute with
 * {@link JDBCHelper#doConnectionAction(ConnectionAction)} inside of a DB
 * transaction.
 * 
 * @see {@link JDBCHelper#doConnectionAction(ConnectionAction)},
 *      {@link ConnectionAction}
 */
public interface TransactionalAction extends ConnectionAction {

	/**
	 * If before run <code>action</code> there's a transaction open and this
	 * methos returns <code>false</code> an exception will be throwed
	 *
	 * @return
	 */
	boolean continueTransactionAllowed();

}