

package org.gvsig.fmap.dal.store.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.store.jdbc.exception.JDBCPreparingSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface Appender {
    
    public void begin() throws DataException;

    public void end() throws DataException;

    public void abort() throws DataException ;

    public void append(FeatureProvider feature) throws DataException ;
}
