/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.jdbc;

import org.gvsig.fmap.dal.resource.db.DBResourceParameters;

public class JDBCResourceParameters extends DBResourceParameters
		implements JDBCConnectionParameters {

    protected static final String PARAMETERS_DEFINITION_NAME = "JDBCResourceParameters";

	protected JDBCResourceParameters(String parametersDefinitionName, String providerName) {
		super(parametersDefinitionName,providerName);
	}
	
	public JDBCResourceParameters() {
		this(PARAMETERS_DEFINITION_NAME, JDBCResource.NAME);
	}

    public JDBCResourceParameters(String parametersDefinitionName, String providerName, 
    		String url, String host, Integer port,
			String dbName, String user, String password, 
			String jdbcDriverClassName) {
		this(parametersDefinitionName,providerName);

		this.setUrl(url);
		this.setHost(host);
		this.setPort(port);
		this.setDBName(dbName);
		this.setUser(user);
		this.setPassword(password);
		this.setJDBCDriverClassName(jdbcDriverClassName);
	}

    public JDBCResourceParameters(String url, String host, Integer port,
			String dbName,
			String user, String password, String jdbcDriverClassName) {
		this(PARAMETERS_DEFINITION_NAME, JDBCResource.NAME, url, host, port, dbName, user, password, jdbcDriverClassName);
	}

	public void setJDBCDriverClassName(String className) {
		this.setDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME, className);
	}

	public String getJDBCDriverClassName() {
		return (String) this.getDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME);
	}

	public String getCatalog() {
		return (String) this.getDynValue(CATALOG_PARAMTER_NAME);
	}

	public void setCatalog(String catalog) {
		this.setDynValue(CATALOG_PARAMTER_NAME, catalog);
	}

	public String getSchema() {
		return (String) this.getDynValue(SCHEMA_PARAMTER_NAME);
	}

	public void setSchema(String schema) {
		this.setDynValue(SCHEMA_PARAMTER_NAME, schema);
	}

	public String getUrl() {
		return (String) this.getDynValue(URL_PARAMTER_NAME);
	}

	public void setUrl(String url) {
		this.setDynValue(URL_PARAMTER_NAME, url);
	}

}
