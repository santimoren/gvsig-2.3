/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorerParameters;

public class JDBCServerExplorerParameters extends
		DBServerExplorerParameters implements JDBCConnectionParameters {
	
	public static final String PARAMETERS_DEFINITION_NAME = "JDBCServerExplorerParameters";

	public static final String SHOWINFORMATIONDBTABLES_PARAMTER_NAME = "showInformationDBTables";

	public JDBCServerExplorerParameters() {
		super(PARAMETERS_DEFINITION_NAME, JDBCServerExplorer.NAME);
	}
	
	public JDBCServerExplorerParameters(String parametersDefinitionName, String name) {
		super(parametersDefinitionName, name);
	}

	public void setJDBCDriverClassName(String className) {
		this.setDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME, className);
	}

	public String getJDBCDriverClassName() {
		return (String) this.getDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME);
	}

	public String getCatalog() {
		return (String) this.getDynValue(CATALOG_PARAMTER_NAME);
	}

	public void setCatalog(String catalog) {
		this.setDynValue(CATALOG_PARAMTER_NAME, catalog);
	}

	public String getSchema() {
		return (String) this.getDynValue(SCHEMA_PARAMTER_NAME);
	}

	public void setSchema(String schema) {
		this.setDynValue(SCHEMA_PARAMTER_NAME, schema);
	}

	public void setShowInformationDBTables(boolean show){
		this.setShowInformationDBTables(new Boolean(show));
	}

	public void setShowInformationDBTables(Boolean show) {
		this.setDynValue(SHOWINFORMATIONDBTABLES_PARAMTER_NAME, show);
	}

	public Boolean getShowInformationDBTables() {
		return (Boolean) this.getDynValue(SHOWINFORMATIONDBTABLES_PARAMTER_NAME);
	}

	public String getUrl() {
		return (String) this.getDynValue(URL_PARAMTER_NAME);
	}

	public void setUrl(String url) {
		this.setDynValue(URL_PARAMTER_NAME, url);
	}

}
