/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.jdbc;

import java.util.NoSuchElementException;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureProviderIterator;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.exception.BaseException;

/**
 * @author jmvivo
 *
 */
public class JDBCIterator extends AbstractFeatureProviderIterator {

	protected int resultSetID;
	protected Boolean hasNext = null;
	protected FeatureType featureType;
	protected JDBCSetProvider set;

	protected JDBCIterator(JDBCStoreProvider store, JDBCSetProvider set,
			FeatureType featureType,
			int resulsetID) throws DataException {
		super(store);
		this.resultSetID = resulsetID;
		this.featureType = featureType;
		this.set = set;
		this.hasNext = null;
		if (this.set != null && resulsetID >= 0){
	        this.set.addResulsetReference(resulsetID);
		}
	}

	private JDBCStoreProvider getJDBCStoreProvider() {
		return (JDBCStoreProvider) getFeatureStoreProvider();
	}

    public final Object next() {
        try {
            getDataStoreProvider().open();
        } catch (OpenException e) {
            throw new RuntimeException(e);
        }
        return internalNext();
    }

    public final boolean hasNext() {
        return internalHasNext();
    }

	@Override
	protected boolean internalHasNext() {
		if (hasNext == null) {
			try {
				if (getJDBCStoreProvider().resulsetNext(resultSetID)) {
					hasNext = Boolean.TRUE;
				} else {
					hasNext = Boolean.FALSE;
					this.close();
				}
			} catch (DataException e) {
				// FIXME Exception
				throw new RuntimeException(e);
			}
		}
		return hasNext.booleanValue();
	}

	@Override
	protected Object internalNext() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		FeatureProvider data;
		try {
			data = getFeatureProvider();
		} catch (DataException e) {
			// FIXME Exception
			throw new RuntimeException(e);
		}
		hasNext = null;
		return data;
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

	protected FeatureProvider createFeatureProvider() throws DataException {
		return getFeatureStoreProvider().createFeatureProvider(featureType);
	}

	protected FeatureProvider getFeatureProvider() throws DataException {
		FeatureProvider data = createFeatureProvider();
		getJDBCStoreProvider().loadFeatureProvider(data, resultSetID);
		return data;
	}

	@Override
	protected void doDispose() throws BaseException {
		if (resultSetID >= 0){
			this.close();
		}
		this.featureType = null;
	}

	protected void close() {
		try {
			this.set.removeResulsetReference(resultSetID);
			getJDBCStoreProvider().closeResulset(resultSetID);
			resultSetID = -1;
		} catch (DataException e) {
			throw new RuntimeException(e);
		}
	}

}
