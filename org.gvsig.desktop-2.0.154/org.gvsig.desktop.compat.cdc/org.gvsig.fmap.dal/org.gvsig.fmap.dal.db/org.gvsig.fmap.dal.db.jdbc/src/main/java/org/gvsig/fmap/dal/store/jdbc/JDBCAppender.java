package org.gvsig.fmap.dal.store.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.exception.AlreadyEditingException;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.exception.StoreCancelEditingException;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.store.jdbc.exception.JDBCPreparingSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JDBCAppender implements Appender {

    final static private Logger logger = LoggerFactory
            .getLogger(JDBCAppender.class);

    public String sql;
    public List<FeatureAttributeDescriptor> attributes;
    public Connection connection = null;
    protected JDBCStoreProviderWriter provider;

    public JDBCAppender(JDBCStoreProviderWriter provider) {
        this.provider = provider;
    }

    public void begin() throws DataException {
        if (this.connection != null) {
            throw new AlreadyEditingException(this.getSourceId());
        }
        try {
            StringBuilder sqlb = new StringBuilder();
            List<FeatureAttributeDescriptor> attrs = new ArrayList<FeatureAttributeDescriptor>();

            prepareSQLAndAttributeListForInsert(sqlb, attrs);

            sql = sqlb.toString();
            attributes = attrs;
            this.connection = this.provider.getHelper().getConnection();
            this.provider.getHelper().getResource().beginUse();
            this.connection.setAutoCommit(false);
        } catch (SQLException ex) {
            try {
                this.connection = null;
                this.provider.getHelper().getResource().endUse();
            } catch(Exception ex1) {
                // Ignore errors
            }
            throw new PerformEditingException(this.getSourceId(), ex);               
        }
    }

    public void end() throws DataException {
        if (this.connection == null) {
            throw new NeedEditingModeException(this.getSourceId());
        }
        sql = null;
        attributes = null;
        resetCount();
        try {
            this.connection.commit();
        } catch (SQLException ex) {
            throw new PerformEditingException(this.getSourceId().toString(), ex);
        } finally {
            this.provider.getHelper().closeConnection(this.connection);
            this.connection = null;
            this.provider.getHelper().getResource().endUse();
        }

    }

    public void abort() throws DataException {
        if (this.connection == null) {
            throw new StoreCancelEditingException(null, this.getSourceId());
        }
        sql = null;
        attributes = null;
        resetCount();
        try {
            this.connection.rollback();
        } catch (SQLException ex) {
            throw new PerformEditingException(this.getSourceId(), ex);
        } finally {
            this.connection = null;
            this.provider.getHelper().getResource().endUse();
        }
    }

    public void append(FeatureProvider feature) throws DataException {
        if (this.connection == null) {
            throw new NeedEditingModeException(this.getSourceId());
        }

        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(sql);
            perfomInsert(connection, st, sql, feature, attributes);
            resetCount();

        } catch (SQLException e) {
            throw new JDBCPreparingSQLException(sql, e);
        } catch (Exception e) {
            throw new PerformEditingException(this.getSourceId(), e);
        } finally {
            try {
                st.close();
            } catch (Exception ex) {
                // Do nothing
            }
        }
    }

    protected void prepareSQLAndAttributeListForInsert(StringBuilder sqlb,
            List<FeatureAttributeDescriptor> attributes) throws DataException {
        this.provider.prepareSQLAndAttributeListForInsert(sqlb, attributes);
    }

    private void perfomInsert(Connection conn, PreparedStatement insertSt,
            String sql, FeatureProvider feature, List<FeatureAttributeDescriptor> attributes)
            throws DataException {
        this.provider.perfomInsert(conn, insertSt, sql, feature, attributes);
    }

    private void resetCount() {
        this.provider.resetCount();
    }

    private String getSourceId() {
        return this.provider.getSourceId().toString();
    }

}
