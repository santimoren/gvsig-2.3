/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.jdbc;

import java.text.MessageFormat;

import org.gvsig.fmap.dal.store.db.DBStoreParameters;

/**
 * Parameters class for JDBC generic provider
 *
 * @author jmvivo
 *
 */
public class JDBCStoreParameters extends DBStoreParameters
		implements JDBCConnectionParameters {

	public static final String PARAMETERS_DEFINITION_NAME = "JDBCStoreParameters";

	public JDBCStoreParameters() {
		super(PARAMETERS_DEFINITION_NAME,JDBCStoreProvider.NAME);
	}

	protected JDBCStoreParameters(String parametersDefinitionName) {
		super(parametersDefinitionName,JDBCStoreProvider.NAME);
	}

	protected JDBCStoreParameters(String parametersDefinitionName, String providerName) {
		super(parametersDefinitionName,providerName);
	}

	public boolean isValid() {
		return this.getHost() != null;
	}

	public String getHost() {
		return (String) this.getDynValue(HOST_PARAMTER_NAME);
	}

	public Integer getPort() {
		return (Integer) this.getDynValue(PORT_PARAMTER_NAME);
	}

	public String getDBName() {
		return (String) this.getDynValue(DBNAME_PARAMTER_NAME);
	}

	public String getUser() {
		return (String) this.getDynValue(USER_PARAMTER_NAME);
	}

	public String getPassword() {
		return (String) this.getDynValue(PASSWORD_PARAMTER_NAME);
	}

	public void setHost(String host) {
		this.setDynValue(HOST_PARAMTER_NAME, host);
	}

	public void setPort(int port) {
		this.setDynValue(PORT_PARAMTER_NAME, new Integer(port));
	}

	public void setPort(Integer port) {
		this.setDynValue(PORT_PARAMTER_NAME, port);
	}

	public void setDBName(String dbName) {
		this.setDynValue(DBNAME_PARAMTER_NAME, dbName);
	}

	public void setUser(String user) {
		this.setDynValue(USER_PARAMTER_NAME, user);
	}

	public void setPassword(String password) {
		this.setDynValue(PASSWORD_PARAMTER_NAME, password);
	}

	/**
	 * Set <code>JDBC Driver class name</code> parameter
	 *
	 * @param className
	 */
	public void setJDBCDriverClassName(String className) {
		this.setDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME, className);
	}

	public String getJDBCDriverClassName() {
		return (String) this.getDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME);
	}

	public String getCatalog() {
		return (String) this.getDynValue(CATALOG_PARAMTER_NAME);
	}


	/**
	 * Set <code>catalog</code> parameter
	 *
	 * @param className
	 */
	public void setCatalog(String catalog) {
		this.setDynValue(CATALOG_PARAMTER_NAME, catalog);
	}

	public String getSchema() {
		return (String) this.getDynValue(SCHEMA_PARAMTER_NAME);
	}

	/**
	 * Set <code>schema</code> parameter
	 *
	 * @param className
	 */
	public void setSchema(String schema) {
		this.setDynValue(SCHEMA_PARAMTER_NAME, schema);
	}

	public String getTable() {
		return (String) this.getDynValue(TABLE_PARAMTER_NAME);
	}

	public void setTable(String table) {
		this.setDynValue(TABLE_PARAMTER_NAME, table);
	}

	public String getFieldsString() {
		return (String) this.getDynValue(FIELDS_PARAMTER_NAME);
	}

	public String[] getFields() {
		String fields = (String) this.getDynValue(FIELDS_PARAMTER_NAME);
		if (fields == null) {
			return null;
		}
		// FIXME check for fields with spaces and special chars
		return fields.split(",");
	}

	public void setFields(String fields) {
		this.setDynValue(FIELDS_PARAMTER_NAME, fields);
	}

	public void setFields(String[] fields) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < fields.length - 1; i++) {
			str.append(fields[i]);
			str.append(",");
		}
		str.append(fields[fields.length - 1]);

		this.setDynValue(FIELDS_PARAMTER_NAME, str.toString());
	}

	public String getSQL() {
		return (String) this.getDynValue(SQL_PARAMTER_NAME);
	}

	public void setSQL(String sql) {
		this.setDynValue(SQL_PARAMTER_NAME, sql);
	}

	public String getBaseFilter() {
		return (String) this.getDynValue(BASEFILTER_PARAMTER_NAME);
	}

	public void setBaseFilter(String initialFilter) {
		this.setDynValue(BASEFILTER_PARAMTER_NAME, initialFilter);
	}

	public String getBaseOrder() {
		return (String) this.getDynValue(BASEORDER_PARAMTER_NAME);
	}

	public void setBaseOrder(String order) {
		this.setDynValue(BASEORDER_PARAMTER_NAME, order);
	}

	public String getPkFieldsString() {
		return (String) this.getDynValue(PKFIELDS_PARAMTER_NAME);
	}

	public String[] getPkFields() {
		String fields = (String) this.getDynValue(PKFIELDS_PARAMTER_NAME);
		if (fields == null) {
			return null;
		}
		// FIXME check for fields with spaces and special chars
		return fields.split(",");
	}

	public void setPkFields(String fields) {
		this.setDynValue(PKFIELDS_PARAMTER_NAME, fields);
	}

	public void setPkFields(String[] fields) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < fields.length - 1; i++) {
			str.append(fields[i]);
			str.append(",");
		}
		str.append(fields[fields.length - 1]);

		this.setDynValue(PKFIELDS_PARAMTER_NAME, str.toString());
	}

	/**
	 * Return table <code>name</code> or <code>schema.tableName</code> if
	 * <code>schema</code> parameter is set.
	 *
	 * @return
	 */
	public String tableID() {
		if (this.getSchema() == null || this.getSchema() == "") {
            return escapeName(this.getTable());
		}
        return escapeName(this.getSchema()) + "." + escapeName(this.getTable());
	}

    protected String escapeName(String name) {
        return "\"".concat(name).concat("\"");
    }

	/**
	 * Compound a string that can identify the source
	 *
	 * @return
	 */
	public String getSourceId() {
		if (getTable() != null) {
			return MessageFormat.format(
					"provider={0}:url=\"{1}\":table=\"{2}\":user={3}:driverclass={4}", 
					this.getDataStoreName(),
					this.getUrl(),
					this.getTable(),
					this.getUser(),
					this.getJDBCDriverClassName()
			);
		}
		return MessageFormat.format(
				"provider={0}:url=\"{1}\":sql=\"{2}\":user={3}:driverclass={4}", 
				this.getDataStoreName(),
				this.getUrl(),
				this.getSQL(),
				this.getUser(),
				this.getJDBCDriverClassName()
		);
	}

	public String getUrl() {
		return (String) this.getDynValue(URL_PARAMTER_NAME);
	}

	/**
	 * Set <code>JDBC connection url</code> parameter
	 *
	 * @param url
	 */
	public void setUrl(String url) {
		this.setDynValue(URL_PARAMTER_NAME, url);
	}

}
