/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.jdbc;

import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.resource.db.DBResourceLibrary;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.store.db.DBHelper;
import org.gvsig.fmap.dal.store.db.DBStoreLibrary;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * Initialize JDBC Provider Library
 * 
 * @author jmvivo
 *
 */
public class JDBCLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
        require(DBStoreLibrary.class);
        require(DBResourceLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		LibraryException ex=null;
		
		DBHelper.registerParametersDefinition(
				 JDBCStoreParameters.PARAMETERS_DEFINITION_NAME,
				 JDBCStoreParameters.class,
				 "JDBCParameters.xml"
		);
		DBHelper.registerParametersDefinition(
				JDBCNewStoreParameters.PARAMETERS_DEFINITION_NAME,
				JDBCNewStoreParameters.class,
				"JDBCParameters.xml"
		);
		DBHelper.registerParametersDefinition(
				JDBCServerExplorerParameters.PARAMETERS_DEFINITION_NAME, 
				JDBCServerExplorerParameters.class, 
				"JDBCParameters.xml"
		);
		DBHelper.registerParametersDefinition(
				JDBCResourceParameters.PARAMETERS_DEFINITION_NAME, 
				JDBCResourceParameters.class, 
				"JDBCParameters.xml"
		);
		
		try {
			DBHelper.registerMetadataDefinition(
					JDBCStoreProvider.METADATA_DEFINITION_NAME, 
					JDBCStoreProvider.class, 
					"JDBCMetadata.xml"
			);
		} catch (MetadataException e) {
			ex = new LibraryException(this.getClass(),e);
		}
		
		
    	ResourceManagerProviderServices resman = (ResourceManagerProviderServices) DALLocator
		.getResourceManager();

		if (!resman.getResourceProviders().contains(JDBCResource.NAME)) {
			resman.register(JDBCResource.NAME, JDBCResource.DESCRIPTION,
					JDBCResource.class, JDBCResourceParameters.class);
		}
		
		DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator
				.getDataManager();
		
		
		if (!dataman.getStoreProviders().contains(JDBCStoreProvider.NAME)) {
			dataman.registerStoreProvider(JDBCStoreProvider.NAME,
					JDBCStoreProviderWriter.class, JDBCStoreParameters.class);
		}
		
		if (!dataman.getExplorerProviders().contains(JDBCStoreProvider.NAME)) {
			dataman.registerExplorerProvider(JDBCServerExplorer.NAME,
					JDBCServerExplorer.class,
					JDBCServerExplorerParameters.class);
		}
		
		if( ex!=null ) {
			throw ex;
		}
	}
}
