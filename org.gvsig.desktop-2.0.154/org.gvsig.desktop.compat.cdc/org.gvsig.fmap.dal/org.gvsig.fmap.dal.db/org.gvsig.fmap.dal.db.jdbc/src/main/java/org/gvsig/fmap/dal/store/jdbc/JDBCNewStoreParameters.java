/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.store.db.DBNewStoreParameters;

import static org.gvsig.fmap.dal.store.jdbc.JDBCConnectionParameters.CATALOG_PARAMTER_NAME;
import static org.gvsig.fmap.dal.store.jdbc.JDBCConnectionParameters.JDBC_DRIVER_CLASS_PARAMTER_NAME;
import static org.gvsig.fmap.dal.store.jdbc.JDBCConnectionParameters.SCHEMA_PARAMTER_NAME;

public class JDBCNewStoreParameters extends DBNewStoreParameters implements
        JDBCConnectionParameters {

    public static final String PARAMETERS_DEFINITION_NAME = "JDBCNewStoreParameters";

    public JDBCNewStoreParameters() {
        super(PARAMETERS_DEFINITION_NAME, JDBCStoreProvider.NAME);
    }

    protected JDBCNewStoreParameters(String parametersDefinitionName, String providerName) {
        super(parametersDefinitionName, providerName);
    }

    /**
     * Set <code>JDBC Driver class name</code> parameter
     *
     * @param className
     */
    public void setJDBCDriverClassName(String className) {
        this.setDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME, className);
    }

    public String getJDBCDriverClassName() {
        return (String) this.getDynValue(JDBC_DRIVER_CLASS_PARAMTER_NAME);
    }

    public String getCatalog() {
        return (String) this.getDynValue(CATALOG_PARAMTER_NAME);
    }

    /**
     * Set <code>catalog</code> parameter
     *
     * @param className
     */
    public void setCatalog(String catalog) {
        this.setDynValue(CATALOG_PARAMTER_NAME, catalog);
    }

    public String getSchema() {
        return (String) this.getDynValue(SCHEMA_PARAMTER_NAME);
    }

    /**
     * Set <code>schema</code> parameter
     *
     * @param className
     */
    public void setSchema(String schema) {
        this.setDynValue(SCHEMA_PARAMTER_NAME, schema);
    }

    public String getUrl() {
        return (String) this.getDynValue(URL_PARAMTER_NAME);
    }

    /**
     * Set <code>JDBC connection url</code> parameter
     *
     * @param url
     */
    public void setUrl(String url) {
        this.setDynValue(URL_PARAMTER_NAME, url);
    }

    /**
     * Return table <code>name</code> or <code>schema.tableName</code> if
     * <code>schema</code> parameter is set.
     *
     * @return
     */
    public String tableID() {
        if (this.getSchema() == null || this.getSchema() == "") {
            return escapeName(this.getTable());
        }
        return escapeName(this.getSchema()) + "." + escapeName(this.getTable());
    }

    protected String escapeName(String name) {
        return "\"".concat(name).concat("\"");
    }

    public String getSelectRole() {
        String value = (String) this.getDynValue("SelectRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    public String getInsertRole() {
        String value = (String) this.getDynValue("InsertRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getUpdateRole() {
        String value = (String) this.getDynValue("UpdateRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getDeleteRole() {
        String value = (String) this.getDynValue("DeleteRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getTruncateRole() {
        String value = (String) this.getDynValue("TruncateRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getReferenceRole() {
        String value = (String) this.getDynValue("ReferenceRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getTriggerRole() {
        String value = (String) this.getDynValue("TriggerRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getAllRole() {
        String value = (String) this.getDynValue("AllRole");
        return StringUtils.defaultIfBlank(value, null);
    }
    
    
    public String getPostCreatingStatement() {
        String value = (String) this.getDynValue("PostCreatingStatement");
        return StringUtils.defaultIfBlank(value, null);
    }

    
    public void setSelectRole(String role) {
        this.setDynValue("SelectRole", role);
    }
    
    public void setInsertRole(String role) {
        this.setDynValue("InsertRole", role);
    }
    
    public void setUpdateRole(String role) {
        this.setDynValue("UpdateRole", role);
    }
    
    public void setDeleteRole(String role) {
        this.setDynValue("DeleteRole", role);
    }
    
    public void setTruncateRole(String role) {
        this.setDynValue("TruncateRole", role);
    }
    
    public void setReferenceRole(String role) {
        this.setDynValue("ReferenceRole", role);
    }
    
    public void setTriggerRole(String role) {
        this.setDynValue("TriggerRole", role);
    }
    
    public void setAllRole(String role) {
        this.setDynValue("AllRole", role);
    }
    
    public void setPostCreatingStatement(String statement) {
        this.setDynValue("PostCreatingStatement", statement);
    }
}
