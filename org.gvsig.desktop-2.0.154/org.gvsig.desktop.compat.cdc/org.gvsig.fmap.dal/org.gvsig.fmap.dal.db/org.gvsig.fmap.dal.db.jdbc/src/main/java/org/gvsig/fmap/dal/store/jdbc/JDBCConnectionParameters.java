/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

import org.gvsig.fmap.dal.serverexplorer.db.DBConnectionParameter;

/**
 * Interface JDBC Store Parameters
 *
 * @author jmvivo
 *
 */
public interface JDBCConnectionParameters extends DBConnectionParameter {

	/**
	 * Parameter name for <code>JDBC driver class name</code>
	 */
	public static final String JDBC_DRIVER_CLASS_PARAMTER_NAME = "jdbcdriverclass";

	/**
	 * Parameter name for <code>catalog</code> 
	 */
	public static final String CATALOG_PARAMTER_NAME = "catalog";

	/**
	 * Parameter name for <code>schema</code> 
	 */
	public static final String SCHEMA_PARAMTER_NAME = "schema";

	/**
	 * Parameter name for <code>JDBC connection URL</code>
	 */
	public static final String URL_PARAMTER_NAME = "url";

	/**
	 * Return <code>JDBC driver class name</code> parameter
	 *
	 * @return
	 */
	public String getJDBCDriverClassName();

	/**
	 * Return <code>catalog</code> parameter
	 *
	 * @return
	 */
	public String getCatalog();

	/**
	 * Return <code>schema</code> parameter
	 *
	 * @return
	 */
	public String getSchema();

	/**
	 * Return <code>JDBC connection URL</code> parameter
	 * 
	 * @return
	 */
	public String getUrl();

}
