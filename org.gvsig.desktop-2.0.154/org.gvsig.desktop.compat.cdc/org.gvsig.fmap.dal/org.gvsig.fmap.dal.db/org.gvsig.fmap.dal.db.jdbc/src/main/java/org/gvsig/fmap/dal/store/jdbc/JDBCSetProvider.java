/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder.FeatureQueryOrderMember;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureSetProvider;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorFieldValue;
import org.gvsig.tools.evaluator.EvaluatorFieldsInfo;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jmvivo
 *
 */
public class JDBCSetProvider extends AbstractFeatureSetProvider {

	final static private Logger logger = LoggerFactory
			.getLogger(JDBCSetProvider.class);

	protected String filter;
	protected String order;
	protected Long size = null;
	protected Boolean isEmpty = null;

	protected List resultSetIDReferenced;

	private JDBCHelper helper = null;

	private int defaultFetchSize = 1000;

	private long limit = -1l;

    private boolean directSQLMode;

	public JDBCSetProvider(JDBCStoreProvider store, FeatureQuery query,
			FeatureType featureType) throws DataException {
		super(store, query, featureType);
		this.helper = store.getHelper();
		this.resultSetIDReferenced = new ArrayList();
		this.directSQLMode =  store.isDirectSQLMode();
	
		if (query.hasFilter() && this.canFilter()) {
			setFilter(query.getFilter());
		} else {
			setFilter(null);
		}

		if (query.hasOrder() && canOrder()) {
			setOrder(query.getOrder());
		} else {
			setOrder(null);
		}

		if (query != null) {
			limit = query.getLimit();
		}
	}

	/**
	 * @return the defaultFetchSize
	 */
	public int getDefaultFetchSize() {
		return defaultFetchSize;
	}

	/**
	 * @param defaultFetchSize
	 *            the defaultFetchSize to set
	 */
	public void setDefaultFetchSize(int defaultFetchSize) {
		this.defaultFetchSize = defaultFetchSize;
	}

	protected String getSqlForEvaluator(Evaluator filter) {
		if (filter == null) {
			return null;
		}
		EvaluatorFieldsInfo info = filter.getFieldsInfo();
		String filterString = filter.getSQL();
		if (info == null) {
			return filterString;
		}
		String[] filterNames = info.getFieldNames();
		String[] finalNames = new String[filterNames.length];
		EvaluatorFieldValue[] fValues;

		List values = new ArrayList();

		FeatureAttributeDescriptor attr;
		for (int i = 0; i < filterNames.length; i++) {
			attr = getFeatureType().getAttributeDescriptor(filterNames[i]);
			if (attr == null) {
				finalNames[i] = filterNames[i];
				continue;
			}
			finalNames[i] = getEscapedFieldName(attr.getName());

		}

		for (int i = 0; i < filterNames.length; i++) {
			if (!filterNames[i].equals(finalNames[i])) {
				filterString = filterString.replaceAll(
						"\\b" + filterNames[i] + "\\b",
						finalNames[i]);
			}
		}

		return filterString;
	}


	protected String getEscapedFieldName(String fieldName) {
		if (helper == null) {
			helper = getJDBCStoreProvider().getHelper();
		}
		return helper.escapeFieldName(fieldName);
	}


	protected void setOrder(FeatureQueryOrder order) {
		if (order == null || order.size() == 0) {
			this.order = null;
			return;
		}

		StringBuilder buffer = new StringBuilder();
		Iterator iter = order.iterator();
		FeatureQueryOrderMember menber;
		while (true) {
			menber = (FeatureQueryOrderMember) iter.next();
			if (menber.hasEvaluator()) {
				buffer.append(getSqlForEvaluator(menber.getEvaluator()));
			} else {
				buffer.append(getEscapedFieldName(menber.getAttributeName()));
			}
			if (menber.getAscending()) {
				buffer.append(" ASC");
			} else {
				buffer.append(" DESC");
			}
			if (iter.hasNext()) {
				buffer.append(", ");
			} else {
				buffer.append(' ');
				break;
			}
		}

		this.order = buffer.toString();
	}

	protected void setFilter(Evaluator filter) {
		this.filter = getSqlForEvaluator(filter);
	}



	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#canFilter()
	 */
	public boolean canFilter() {
		Evaluator filter = getQuery().getFilter();
		if (directSQLMode){
		    return false;
		}
		if (filter != null) {
			if (filter.getSQL() == null || filter.getSQL().length() == 0) {
				return false;
			} else {
				// TODO Check Geom fields if
				EvaluatorFieldsInfo fInfo = filter.getFieldsInfo();
				if (fInfo == null || fInfo.getFieldNames() == null) {
					return true;
				}
				Iterator names = Arrays.asList(fInfo.getFieldNames())
						.iterator();
				String name;
				int type;
				while (names.hasNext()) {
					name = (String) names.next();
					type =
							this.getFeatureType()
									.getAttributeDescriptor(name)
							.getType();
					if (type == DataTypes.GEOMETRY
							&& !this.helper.supportsGeometry()) {
						return false;
					}



				}

				return true;
			}

		} else{
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#canIterateFromIndex()
	 */
	public boolean canIterateFromIndex() {
		return helper.supportOffset();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#canOrder()
	 */
	public boolean canOrder() {
	    if (directSQLMode){
	        return false;
	    }
		// TODO Check Geom fields if postgis not are available
		FeatureQuery query = getQuery();
		if (query.hasOrder()) {
			Iterator iter = query.getOrder().iterator();
			FeatureQueryOrderMember menber;
			String sql;
			while (iter.hasNext()){
				menber = (FeatureQueryOrderMember) iter.next();
				if (menber.hasEvaluator()){
					sql = menber.getEvaluator().getSQL();
					if (sql == null || sql.length() == 0) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private JDBCStoreProvider getJDBCStoreProvider() {
		return (JDBCStoreProvider) getStore();
	}

	@Override
	protected void doDispose() throws BaseException {
		if (resultSetIDReferenced != null) {
			Iterator iter = resultSetIDReferenced.iterator();
			Integer resID;
			while (iter.hasNext()) {
				resID = (Integer) iter.next();
				if (resID != null) {
					logger.warn(
						"ResultSet (ID {}) not closed on dispose, will close",
						resID);
					try {
						getJDBCStoreProvider().closeResulset(resID.intValue());
					} catch (DataException e) {
						logger.error("Close resulset Exception", e);
					}
				}
				iter.remove();
			}
		}
		resultSetIDReferenced = null;
		filter = null;
		order = null;
		size = null;
		isEmpty = null;
	}

	protected String getSQL(long fromIndex) throws DataException {
		return getJDBCStoreProvider().compoundSelect(getFeatureType(), filter,
				order, limit, fromIndex);

	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#getSize()
	 */
	public long getSize() throws DataException {
		if (size == null) {
			size = new Long(getJDBCStoreProvider().getCount(filter));
		}
		return size.longValue();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#isEmpty()
	 */
	public boolean isEmpty() throws DataException {
		JDBCStoreProvider store = getJDBCStoreProvider();
		if (isEmpty == null) {
			if (size == null) {
				String sql =
						store.compoundSelect(getFeatureType(), filter, null, 1,
								0);
				int rsID = store.createResultSet(sql, getFetchSize());
				isEmpty = new Boolean(!store.resulsetNext(rsID));
				store.closeResulset(rsID);
			} else {
				isEmpty = new Boolean(size.longValue() < 1);
			}
		}
		return isEmpty.booleanValue();
	}

	protected int getFetchSize() {
		long pageSize = -1;
		if (getQuery() != null) {
			pageSize = getQuery().getPageSize();
			pageSize =
					pageSize > Integer.MAX_VALUE ? Integer.MAX_VALUE : pageSize;
		}
		return (pageSize > 0 ? (int) pageSize : defaultFetchSize);
	}

	protected JDBCIterator createFastIterator(long index) throws DataException {
		if (isEmpty != null && isEmpty.booleanValue()) {
			return new EmptyJDBCIterator(getJDBCStoreProvider());
		}
		int rsID =
				getJDBCStoreProvider().createResultSet(getSQL(index),
						getFetchSize());
		return createDefaultFastIterator(rsID);
	}

	protected JDBCIterator createDefaultFastIterator(int resultSetID)
			throws DataException {
		return new JDBCFastIterator(getJDBCStoreProvider(), this,
				getFeatureType(), resultSetID);
	}

	protected JDBCIterator createIterator(long index) throws DataException {
        if (isEmpty != null && isEmpty.booleanValue()) {
            return new EmptyJDBCIterator(getJDBCStoreProvider());
        }
        int rsID =
                getJDBCStoreProvider().createResultSet(getSQL(index),
                        getFetchSize());
        return createDefaultIterator(rsID);
	}

	protected JDBCIterator createDefaultIterator(int resultSetID)
			throws DataException {
		return new JDBCIterator(getJDBCStoreProvider(), this, getFeatureType(),
				resultSetID);
	}

	public void addResulsetReference(int resulsetID) {
		this.resultSetIDReferenced.add(new Integer(resulsetID));
	}

	public void removeResulsetReference(int resulsetID) {
		this.resultSetIDReferenced.remove(new Integer(resulsetID));
	}

	private class EmptyJDBCIterator extends JDBCIterator {

		protected EmptyJDBCIterator(JDBCStoreProvider store) throws DataException {
			super(store, null, null, -1);
		}

		@Override
		protected boolean internalHasNext() {
			return false;
		}

		@Override
		protected Object internalNext() {
			throw new NoSuchElementException();
		}

		@Override
		protected void doDispose() throws BaseException {
			// nothing to do
		}

	}

}
