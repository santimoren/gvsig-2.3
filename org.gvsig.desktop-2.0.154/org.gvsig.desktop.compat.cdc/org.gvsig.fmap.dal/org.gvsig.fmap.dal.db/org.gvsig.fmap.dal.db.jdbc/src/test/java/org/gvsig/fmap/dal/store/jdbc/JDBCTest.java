/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.jdbc;

import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.BaseTestFeatureStore;

import com.mysql.jdbc.Driver;

/**
 * @author jmvivo
 *
 */
public class JDBCTest extends BaseTestFeatureStore {

	@Override
	protected void doSetUp() throws Exception {
		// Nothing to do	
	}

	public DataStoreParameters getDefaultDataStoreParameters()
			throws DataException {
		JDBCStoreParameters parameters = null;
		parameters = (JDBCStoreParameters) dataManager
				.createStoreParameters(JDBCStoreProvider.NAME);


		parameters.setHost("localhost");
		parameters.setPort(3306);
		parameters.setUser("test");
		parameters.setPassword("test");
		parameters.setDBName("gis");
		parameters.setTable("alfanum");
		parameters.setUrl("jdbc:mysql://" + parameters.getHost() + ":"
				+ parameters.getPort() + "/" + parameters.getDBName());
		parameters.setJDBCDriverClassName(Driver.class.getName());

		return parameters;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseTestFeatureStore#hasExplorer()
	 */
	@Override
	public boolean hasExplorer() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.BaseTestFeatureStore#usesResources()
	 */
	@Override
	public boolean usesResources() {
		// TODO Auto-generated method stub
		return true;
	}

}
