/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.db;

import java.io.InputStream;

import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.persistence.PersistenceManager;

public final class DBHelper {

	private DBHelper() {
		// Do nothing
	}
	
	@SuppressWarnings("unchecked")
	public static void registerParametersDefinition(String name, Class theClass, String filename) {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if (manager.getDefinition(name) == null) {
			InputStream resource = theClass.getResourceAsStream(filename);
			if( resource==null ) {
				throw new IllegalArgumentException("File '"+ filename + "' not found as a resource of '"+theClass.getName()+"'.");
			}
			manager.addDefinition(
					theClass,
					name, 
					resource,
					theClass.getClassLoader(), 
					null, 
					null
			);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void registerMetadataDefinition(String name, Class theClass, String filename) throws MetadataException {
		MetadataManager manager = MetadataLocator.getMetadataManager();
		if( manager.getDefinition(name)==null ) {
			InputStream resource = theClass.getResourceAsStream(filename);
			if( resource==null ) {
				throw new IllegalArgumentException("File '"+ filename + "' not found as a resource of '"+theClass.getName()+"'.");
			}
			manager.addDefinition(
					name, 
					theClass.getResourceAsStream(filename),
					theClass.getClassLoader()
			);
		}
	}

	public static DynObject newParameters(String name) {
    	return ToolsLocator.getDynObjectManager()
			.createDynObject(
				ToolsLocator.getPersistenceManager().getDefinition(name)
			);
	}
	
	public static DynObject newMetadataContainer(String name) {
    	return ToolsLocator.getDynObjectManager()
			.createDynObject(
				MetadataLocator.getMetadataManager().getDefinition(name)
			);
	}

}
