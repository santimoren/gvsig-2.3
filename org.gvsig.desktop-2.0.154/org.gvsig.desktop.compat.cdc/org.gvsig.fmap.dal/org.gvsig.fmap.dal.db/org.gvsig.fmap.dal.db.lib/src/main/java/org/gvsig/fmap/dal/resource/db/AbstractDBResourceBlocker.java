/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.resource.db;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.spi.AbstractResource;

/**
 * Abstract Data Base Resource implementation that prevents the concurrent
 * access.
 *
 * @author jmvivo
 * 
 */
public abstract class AbstractDBResourceBlocker extends AbstractResource
		implements
		DBResource {

	/**
	 * Default constructor
	 *
	 * @param parameters
	 * @throws InitializeException
	 */
	protected AbstractDBResourceBlocker(DBResourceParameters parameters)
			throws InitializeException {
		super(parameters);
	}


	/**
	 * Return a connection to the data base.<br>
	 * Connect to the Data Base if is needed
	 *
	 * @return connection to the data base
	 */
	public Object get() throws AccessResourceException {
		if (!isConnected()) {
			try {
				this.connect();
			} catch (DataException e) {
				throw new AccessResourceException(this, e);
			}
		}
		try {
			return getTheConnection();
		} catch (DataException e) {
			throw new AccessResourceException(this, e);
		}
	}

	/**
	 * Return a connection to the data base.<br>
	 * Connect to the Data Base if is needed
	 *
	 *
	 * @return connection to the data base
	 * @see #get()
	 */
	public Object getConnection() throws AccessResourceException {
		return get();
	}

	/**
	 * Establish connection to data base
	 *
	 * @throws DataException
	 */
	public final void connect() throws DataException {
		if (this.isConnected()) {
			return;
		}
		prepare();
		connectToDB();
	}

	/**
	 * Check if parameters is the same for this resource.<br>
	 * <br>
	 *
	 * <strong>Note:</strong> override this method to add checks for specify
	 * final implementation parameters
	 *
	 *
	 * @see AbstractResource#isThis(ResourceParameters)
	 */
	public boolean isThis(ResourceParameters parameters)
			throws ResourceException {
		if (!(parameters instanceof DBResourceParameters)) {
			return false;
		}
		DBResourceParameters params = (DBResourceParameters) parameters
				.getCopy();
		prepare(params);
		DBResourceParameters myParams = (DBResourceParameters) this
				.getParameters();

		if (!equals(myParams.getHost(), params.getHost())) {
			return false;
		}
		if (!equals(myParams.getPort(), params.getPort())) {
			return false;
		}
		if (!equals(myParams.getUser(), params.getUser())) {
			return false;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	protected boolean equals(Comparable v1, Comparable v2) {
		if (v1 == v2) {
			return true;
		}
		if ((v1 != null) && (v2 != null)) {
			return v1.compareTo(v2) == 0;
		}
		return false;
	}

	/**
	 * final implementation method to Establish connection to data base<br>
	 * Called from {@link #get()}<br>
	 * <br>
	 *
	 *
	 * @throws DataException
	 */
	protected abstract void connectToDB() throws DataException;

	/**
	 * final implementation method to get a connection to data base<br>
	 * Called from {@link #connect()}<br>
	 * <br>
	 * This method is called with the connection establish
	 *
	 * @throws DataException
	 */
	protected abstract Object getTheConnection() throws DataException;

}
