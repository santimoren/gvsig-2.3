/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.serverexplorer.db;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.feature.FeatureType;

/**
 * Base interface of {@link DataServerExplorer} for Data Base type server
 *
 * @author jmvivo
 *
 */
public interface DBServerExplorer extends DataServerExplorer {

	/**
	 * Return a instance of {@link NewDataStoreParameters} to fill
	 *
	 * @return
	 * @throws DataException
	 */
	public NewDataStoreParameters getAddParameters()
			throws DataException;

	/**
	 * Return the {@link FeatureType} of the store of <code>dsp</code> store
	 *
	 * @param dsp
	 * @return
	 * @throws DataException
	 */
	public FeatureType getFeatureType(DataStoreParameters dsp)
			throws DataException;
	/**
	 * Open a store defined by <code>dsp</code>
	 *
	 * @param dsp
	 * @return
	 * @throws DataException
	 */
	public DataStore open(DataStoreParameters dsp)
			throws DataException;

	/**
	 * Open connection to the server
	 *
	 * @throws OpenException
	 */
	public void open() throws OpenException;


	/**
	 * Close connection to the server
	 *
	 * @throws OpenException
	 */
	public void close() throws CloseException;
}
