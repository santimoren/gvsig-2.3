/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.db.spi;

import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorer;
import org.gvsig.fmap.dal.spi.AbstractDataServerExplorer;
import org.gvsig.fmap.dal.spi.DataServerExplorerProvider;
import org.gvsig.fmap.dal.spi.DataServerExplorerProviderServices;
import org.gvsig.fmap.dal.spi.DataStoreProvider;
import org.gvsig.tools.dispose.impl.AbstractDisposable;

/**
 * Abstract class for {@link DBServerExplorer} implementation
 *
 * @author jmvivo
 *
 */
public abstract class AbstractDBServerExplorer extends AbstractDataServerExplorer
		implements DBServerExplorer, DataServerExplorerProvider {

//	private DataServerExplorerProviderServices dataServerExplorerProviderServices;
//	private DataServerExplorerParameters dataServerParameters;

	protected AbstractDBServerExplorer(DataServerExplorerParameters parameters,
			DataServerExplorerProviderServices services) {
            super(parameters, services);
	}

	/**
	 * Return the name of {@link DataStoreProvider} returned by this
	 * ServerExplorer
	 *
	 * @return
	 */
	public abstract String getStoreName();

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorer#canAdd(java.lang.String)
	 */
	public boolean canAdd(String storeName) throws DataException {
		return getStoreName().equals(storeName);
	}


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.serverexplorer.db.DBServerExplorer#getAddParameters(java.lang.String)
	 */
	public NewDataStoreParameters getAddParameters(String storeName)
			throws DataException {
		if (!getStoreName().equals(storeName)) {
			// FIXME exception
			throw new IllegalArgumentException();
		}
		return getAddParameters();
	}

}
