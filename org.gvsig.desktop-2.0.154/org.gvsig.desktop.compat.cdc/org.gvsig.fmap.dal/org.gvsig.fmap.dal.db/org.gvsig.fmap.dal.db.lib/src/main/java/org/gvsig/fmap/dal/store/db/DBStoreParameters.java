/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.db;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.OpenDataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.serverexplorer.db.DBConnectionParameter;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.dynobject.DelegatedDynObject;

/**
 * Abstract Data base Store Parameters
 *
 * @author jmvivo
 *
 */
public abstract class DBStoreParameters extends AbstractDataParameters
		implements
		OpenDataStoreParameters, DBConnectionParameter {

	public static final String PARAMETERS_DEFINITION_NAME = "DBStoreParameters";

	/**
	 * Parameter name for <code>sql</code><br>
	 *
	 * @see #getSQL()
	 * @see #setSQL(String)
	 */
	public static final String SQL_PARAMTER_NAME = "SQL";

	/**
	 * Parameter name for <code>fields</code><br>
	 *
	 * @see #getFields()
	 * @see #getFieldsString()
	 * @see #setFields(String)
	 * @see #setFields(String[])
	 */
	public static final String FIELDS_PARAMTER_NAME = "Fields";

	/**
	 * Parameter name for <code>initial filter</code><br>
	 *
	 * @see #getBaseFilter()
	 * @see #setBaseFilter(String)
	 */
	public static final String BASEFILTER_PARAMTER_NAME = "BaseFilter";

	/**
	 * Parameter name for <code>initial order</code><br>
	 *
	 * @see #getBaseOrder()
	 * @see #setBaseOrder(String)
	 */
	public static final String BASEORDER_PARAMTER_NAME = "BaseOrder";

	/**
	 * Parameter name for <code>pk fields</code><br>
	 *
	 * @see #getPkFields()
	 * @see #getPkFieldsString()
	 * @see #setPkFields(String)
	 * @see #setPkFields(String[])
	 */
	public static final String PKFIELDS_PARAMTER_NAME = "PKFields";

	/**
	 * Parameter name for <code>default geometry</code><br>
	 *
	 * @see #getDefaultGeometryField()
	 * @see #setDefaultGeometryField(String)
	 */
	public static final String DEFAULTGEOMETRY_PARAMTER_NAME = "DefaultGeometryField";

	/**
	 * Parameter name for the name of <code>table</code><br>
	 *
	 * @see #getTable()
	 * @see #setTable(String)
	 */
	public static final String TABLE_PARAMTER_NAME = "Table";

	/**
	 * Parameter name for <code>working area</code><br>O
	 *
	 * @see #getWorkingArea()
	 * @see #setWorkingArea(Envelope)
	 */
	public static final String WORKINGAREA_PARAMTER_NAME = "Workingarea";


	/**
	 * Parameter name for <code>CRS</code><br>
	 * 
	 * @see #setSRSID(String)
	 * @see #getSRS()
	 * @see #setSRS(IProjection)
	 */
	public static final String CRS_PARAMTER_NAME = "CRS";

	/**
	 * This instance contains the value of the current parameters.
	 */
	private DelegatedDynObject parameters;

	public DBStoreParameters(String parametersDefinitionName, String providerName) {
		this.parameters  = (DelegatedDynObject) DBHelper.newParameters(parametersDefinitionName);
		this.setDynValue( DataStoreProviderServices.PROVIDER_PARAMTER_NAME, providerName);
	}
	
	protected DelegatedDynObject getDelegatedDynObject() {
		return parameters;
	}

	public String getDataStoreName() {
		return (String) this.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
	}

	public String getDescription() {
		return this.getDynClass().getDescription();
	}

	public boolean isValid() {
		return this.getHost() != null;
	}

	public String getHost() {
		return (String) this.getDynValue(HOST_PARAMTER_NAME);
	}

	public Integer getPort() {
		return (Integer) this.getDynValue(PORT_PARAMTER_NAME);
	}

	public String getDBName() {
		return (String) this.getDynValue(DBNAME_PARAMTER_NAME);
	}

	public String getUser() {
		return (String) this.getDynValue(USER_PARAMTER_NAME);
	}

	public String getPassword() {
		return (String) this.getDynValue(PASSWORD_PARAMTER_NAME);
	}

	public void setHost(String host) {
		this.setDynValue(HOST_PARAMTER_NAME, host);
	}

	public void setPort(int port) {
		this.setDynValue(PORT_PARAMTER_NAME, new Integer(port));
	}

	/**
	 * Set <code>port/code> parameter value
	 *
	 * @param port
	 */
	public void setPort(Integer port) {
		this.setDynValue(PORT_PARAMTER_NAME, port);
	}

	/**
	 * Set <code>data base name/code> parameter value
	 *
	 * @param data
	 *            base name
	 */
	public void setDBName(String dbName) {
		this.setDynValue(DBNAME_PARAMTER_NAME, dbName);
	}

	/**
	 * Set <code>user/code> parameter value
	 *
	 * @param user
	 */
	public void setUser(String user) {
		this.setDynValue(USER_PARAMTER_NAME, user);
	}

	/**
	 * Set <code>password/code> parameter value
	 *
	 * @param password
	 */
	public void setPassword(String password) {
		this.setDynValue(PASSWORD_PARAMTER_NAME, password);
	}

	/**
	 * Get <code>table</code> parameter value<br>
	 * <br>
	 *
	 * This parameters describes what table or view we want to connect.<br>
	 *
	 * Not used if <code>sql</code> parameter set.
	 *
	 *
	 * @param password
	 *
	 * @see #setTable(String)
	 * @see #getSQL()
	 * @see #setSQL(String)
	 */
	public String getTable() {
		return (String) this.getDynValue(TABLE_PARAMTER_NAME);
	}

	/**
	 * Set <code>table</code> parameter value<br>
	 * <br>
	 *
	 * This parameters describes what table or view we want to connect.<br>
	 *
	 * Not used if <code>sql</code> parameter set.
	 *
	 *
	 * @param password
	 *
	 * @see #getTable(String)
	 * @see #getSQL()
	 * @see #setSQL(String)
	 */
	public void setTable(String table) {
		this.setDynValue(TABLE_PARAMTER_NAME, table);
	}

	/**
	 * Get a comma separated list of the field names that we want to use.<br>
	 * <br>
	 *
	 * A <code>null</code> or empty string means that we want all fields
	 * available.
	 *
	 * @return
	 * @see #getFields()
	 * @see #setFields(String)
	 * @see #setFields(String[])
	 */
	public String getFieldsString() {
		return (String) this.getDynValue(FIELDS_PARAMTER_NAME);
	}

	/**
	 * Get an array of the field names that we want to use.<br>
	 * <br>
	 *
	 * A <code>null</code> means that we want all fields available.
	 *
	 * @return
	 * @see #getFieldsString()
	 * @see #setFields(String)
	 * @see #setFields(String[])
	 */
	public String[] getFields() {
		String fields = (String) this.getDynValue(FIELDS_PARAMTER_NAME);
		if (fields == null) {
			return null;
		}
		// FIXME check for fields with spaces and special chars
		return fields.split(",");
	}

	/**
	 * Set a comma separated list of the field names that we want to use.<br>
	 * <br>
	 *
	 * A <code>null</code> means that we want all fields available.
	 *
	 * @return
	 * @see #getFields()
	 * @see #getFieldsString()
	 * @see #setFields(String[])
	 */
	public void setFields(String fields) {
		this.setDynValue(FIELDS_PARAMTER_NAME, fields);
	}

	/**
	 * Set an array of the field names that we want to use.<br>
	 * <br>
	 *
	 * A <code>null</code> means that we want all fields available.
	 *
	 * @return
	 * @see #getFieldsString()
	 * @see #getFields()
	 * @see #setFields(String)
	 */
	public void setFields(String[] fields) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < fields.length - 1; i++) {
			str.append(fields[i]);
			str.append(",");
		}
		str.append(fields.length - 1);

		this.setDynValue(FIELDS_PARAMTER_NAME, fields);
	}

	/**
	 * Get the SQL to use as source of the store instead a table or a view.<br>
	 * <br>
	 *
	 * If this property is set the store changes his work flow:
	 * <ul>
	 * <li>store is in <i>read only</i> mode.</li>
	 * <li><code>table</code> property is ignored.</li>
	 * <li><code>pkFields</code> is not identified automatically</li>
	 * <li>filter and order will be resolved locally <i>(pour performance)</i></li>
	 * </ul>
	 *
	 * @return sql to use
	 *
	 * @see #getTable()
	 * @see #setTable()
	 * @see #getPkFields()
	 * @see #setPkFields(String)
	 */
	public String getSQL() {
		return (String) this.getDynValue(SQL_PARAMTER_NAME);
	}

	/**
	 * Set the SQL to use as source of the store instead a table or a view.<br>
	 * <br>
	 * <strong>Note:</strong>see {@link #getSQL()} for description
	 *
	 * @see #getSQL()
	 * @see #getTable()
	 * @see #setTable()
	 * @see #getPkFields()
	 * @see #setPkFields(String)
	 */
	public void setSQL(String sql) {
		this.setDynValue(SQL_PARAMTER_NAME, sql);
	}

	/**
	 * Get initial filter to use.<br>
	 * <br>
	 *
	 * This filter is passed to BD provider on each request as a base filter.
	 *
	 * @return filter
	 *
	 * @see #setBaseFilter(String)
	 */
	public String getBaseFilter() {
		return (String) this.getDynValue(BASEFILTER_PARAMTER_NAME);
	}

	/**
	 * Set initial filter to use.<br>
	 * <br>
	 *
	 * This filter is passed to BD provider on each request as a base filter.
	 *
	 * @return filter
	 *
	 * @see #getInitialFilter(String)
	 */
	public void setBaseFilter(String filter) {
		this.setDynValue(BASEFILTER_PARAMTER_NAME, filter);
	}

	/**
	 * Get initial order to use.<br>
	 * <br>
	 *
	 * This order is used if no custom order is request in query.
	 *
	 * @return order
	 *
	 * @see #setBaseOrder(String)
	 */
	public String getBaseOrder() {
		return (String) this.getDynValue(BASEORDER_PARAMTER_NAME);
	}

	/**
	 * Set initial order to use.<br>
	 * <br>
	 *
	 * This order is used if no custom order is request in query.
	 *
	 * @return filter
	 *
	 * @see #getBaseOrder()
	 */
	public void setBaseOrder(String order) {
		this.setDynValue(BASEORDER_PARAMTER_NAME, order);
	}

	/**
	 * Get a comma separated list of the field names that compound the primary
	 * key.<br>
	 * <br>
	 *
	 * A <code>null</code> or empty string means that library must detect this
	 * information.<br>
	 * <br>
	 * <strong>Note:</strong>If this parameters is undefined, the library can't
	 * do this detection, some services will don't be available for this store
	 * (<i>like selection, editing or {@link Feature#getReference()}</i>)
	 *
	 * @return
	 * @see #getPkFields()
	 * @see #setPkFields(String)
	 * @see #setPkFields(String[])
	 */
	public String getPkFieldsString() {
		return (String) this.getDynValue(PKFIELDS_PARAMTER_NAME);
	}

	/**
	 * Get an array of the field names that compound the primary key.<br>
	 * <br>
	 *
	 * A <code>null</code> or empty string means that library must detect this
	 * information.<br>
	 * <br>
	 * <strong>Note:</strong> see {@link #getPkFieldsString()}
	 *
	 * @return
	 * @see #getPkFieldsString()
	 * @see #setPkFields(String)
	 * @see #setPkFields(String[])
	 */
	public String[] getPkFields() {
		String fields = (String) this.getDynValue(PKFIELDS_PARAMTER_NAME);
		if (fields == null) {
			return null;
		}
		// FIXME check for fields with spaces and special chars
		return fields.split(",");
	}

	/**
	 * Set a comma separated list of the field names that compound the primary
	 * key.<br>
	 * <br>
	 *
	 * A <code>null</code> or empty string means that library must detect this
	 * information.<br>
	 * <br>
	 * <strong>Note:</strong> see {@link #getPkFieldsString()}
	 *
	 * @return
	 * @see #getPkFields()
	 * @see #getPkFieldsString()
	 * @see #setPkFields(String[])
	 */
	public void setPkFields(String fields) {
		this.setDynValue(PKFIELDS_PARAMTER_NAME, fields);
	}

	/**
	 * Set an array of the field names that compound the primary key.<br>
	 * <br>
	 *
	 * A <code>null</code> or empty string means that library must detect this
	 * information.<br>
	 * <br>
	 * <strong>Note:</strong> see {@link #getPkFieldsString()}
	 *
	 * @return
	 * @see #getPkFieldsString()
	 * @see #getPkFieldsString()
	 * @see #setPkFields(String)
	 */
	public void setPkFields(String[] fields) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < fields.length - 1; i++) {
			str.append(fields[i]);
			str.append(",");
		}
		str.append(fields.length - 1);

		this.setDynValue(PKFIELDS_PARAMTER_NAME, fields);
	}

	/**
	 * Return the geometry field to use like default geometry (<i>see
	 * {@link Feature#getDefaultGeometry()}</i>)<br>
	 * <br>
	 *
	 * This option is supported only in geometry providers.<br>
	 * If this parameters is not set and store has only one field geometry then
	 * library uses that field as default
	 *
	 * @return
	 *
	 * @see #setDefaultGeometryField(String)
	 */
	public String getDefaultGeometryField() {
		return (String) this.getDynValue(DEFAULTGEOMETRY_PARAMTER_NAME);
	}

	/**
	 * Set the geometry field to use like default geometry.<br>
	 * See {@link #getDefaultGeometryField()} for description.
	 *
	 * @param geomName
	 *
	 * @see #getDefaultGeometryField()
	 */
	public void setDefaultGeometryField(String geomName) {
		this.setDynValue(DEFAULTGEOMETRY_PARAMTER_NAME, geomName);
	}

	/**
	 * Get the filter by area of {@link #getDefaultGeometryField()} used in this
	 * store.<br>
	 * <br>
	 *
	 * Supported only for stores with geometric fields
	 *
	 * @return
	 */
	public Envelope getWorkingArea() {
		return (Envelope) this.getDynValue(WORKINGAREA_PARAMTER_NAME);
	}

	/**
	 * Set the filter by area of {@link #getDefaultGeometryField()} used in this
	 * store.<br>
	 * <br>
	 *
	 * Supported only for stores with geometric fields
	 *
	 */
	public void setWorkingArea(Envelope workingArea) {
		this.setDynValue(WORKINGAREA_PARAMTER_NAME, workingArea);
	}

	/**
	 * Set manually the SRS for the <code>default geometry</code> field of this
	 * layer.<br>
	 * <br>
	 * 
	 * if is set to <code>null</code> or empty string means that the library
	 * must detect it automatically.<br>
	 * <br>
	 * 
	 * Supported only for stores with geometric fields.
	 * 
	 * @return
	 * 
	 * @see #setSRSID(String)
	 * @see #getSRSID()
	 * @see #getSRS()
	 */
	public void setCRS(IProjection srs) {
		setDynValue(CRS_PARAMTER_NAME, srs);
	}

	public void setCRS(String srs) {
		setDynValue(CRS_PARAMTER_NAME, srs);
	}

	/**
	 * Get the SRS id string set manually for the <code>default geometry</code>
	 * field of this layer.<br>
	 * <br>
	 *
	 * if is set to <code>null</code> means that the library must detect it
	 * automatically.<br>
	 * <br>
	 *
	 * Supported only for stores with geometric fields.
	 *
	 * @return
	 *
	 * @see #setSRSID(String)
	 * @see #getSRS()
	 * @see #setSRS(IProjection)
	 */
	public IProjection getCRS() {
		return (IProjection) getDynValue(CRS_PARAMTER_NAME);
	}

}
