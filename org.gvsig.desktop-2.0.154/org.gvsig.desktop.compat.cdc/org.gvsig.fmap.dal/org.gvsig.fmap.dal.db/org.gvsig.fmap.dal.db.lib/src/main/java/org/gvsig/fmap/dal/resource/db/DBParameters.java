/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.resource.db;

/**
 * Interface for describe parameters of a Data Base
 *
 *
 * @author jmvivo
 *
 */
public interface DBParameters {

	/**
	 * Parameter name for <code>host</code>
	 */
	public static final String HOST_PARAMTER_NAME = "host";

	/**
	 * Parameter name for <code>port</code> 
	 */
	public static final String PORT_PARAMTER_NAME = "port";

	/**
	 * Parameter name for <code>data base name</code> 
	 */
	public static final String DBNAME_PARAMTER_NAME = "dbname";

	/**
	 * Parameter name for <code>user</code>
	 */
	public static final String USER_PARAMTER_NAME = "dbuser";

	/**
	 * Parameter name for <code>password</code>
	 */
	public static final String PASSWORD_PARAMTER_NAME = "password";

	/**
	 * Return the value of <code>host</code> parameter
	 *
	 * @return
	 */
	public String getHost();

	/**
	 * Return the value of <code>port</code> parameter
	 *
	 * @return
	 */
	public Integer getPort();

	/**
	 * Return the value of <code>data base name</code> parameter
	 *
	 * @return
	 */
	public String getDBName();

	/**
	 * Return the value of <code>password</code> parameter
	 *
	 * @return
	 */
	public String getPassword();

	/**
	 * Return the value of <code>user</code> parameter
	 *
	 * @return
	 */
	public String getUser();

}
