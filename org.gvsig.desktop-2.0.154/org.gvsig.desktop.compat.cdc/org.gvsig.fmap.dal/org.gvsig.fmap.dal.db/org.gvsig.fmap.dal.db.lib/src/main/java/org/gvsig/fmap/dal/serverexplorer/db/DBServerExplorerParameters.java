/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.db;

import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.dal.store.db.DBHelper;
import org.gvsig.tools.dynobject.DelegatedDynObject;

/**
 * Abstract class of {@link DataServerExplorerParameters} for Data Base type
 * server
 *
 * @author jmvivo
 *
 */
public abstract class DBServerExplorerParameters extends AbstractDataParameters
		implements DataServerExplorerParameters, DBConnectionParameter {

    public static final String PARAMETERS_DEFINITION_NAME = "DBServerExplorerParameters";

    private DelegatedDynObject parameters;

	public DBServerExplorerParameters(String parametersDefinitionName, String explorerName) {
		this.parameters = (DelegatedDynObject) DBHelper.newParameters(parametersDefinitionName);
		this.setDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME, explorerName);
	}

	public String getExplorerName() {
		return (String) this.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
	}
	
	protected DelegatedDynObject getDelegatedDynObject() {
		return parameters;
	}

	public String getHost() {
		return (String) this.getDynValue(HOST_PARAMTER_NAME);
	}

	public Integer getPort() {
		return (Integer) this.getDynValue(PORT_PARAMTER_NAME);
	}

	public String getDBName() {
		return (String) this.getDynValue(DBNAME_PARAMTER_NAME);
	}

	public String getUser() {
		return (String) this.getDynValue(USER_PARAMTER_NAME);
	}

	public String getPassword() {
		return (String) this.getDynValue(PASSWORD_PARAMTER_NAME);
	}

	/**
	 * Set <code>host</code> parameter value
	 *
	 * @param host
	 */
	public void setHost(String host) {
		this.setDynValue(HOST_PARAMTER_NAME, host);
	}

	/**
	 * Set <code>port/code> parameter value
	 *
	 * @param port
	 */
	public void setPort(int port) {
		this.setDynValue(PORT_PARAMTER_NAME, new Integer(port));
	}

	/**
	 * Set <code>port/code> parameter value
	 *
	 * @param port
	 */
	public void setPort(Integer port) {
		this.setDynValue(PORT_PARAMTER_NAME, port);
	}

	/**
	 * Set <code>data base name/code> parameter value
	 *
	 * @param data
	 *            base name
	 */
	public void setDBName(String dbName) {
		this.setDynValue(DBNAME_PARAMTER_NAME, dbName);
	}

	/**
	 * Set <code>user/code> parameter value
	 *
	 * @param user
	 */
	public void setUser(String user) {
		this.setDynValue(USER_PARAMTER_NAME, user);
	}

	/**
	 * Set <code>password/code> parameter value
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.setDynValue(PASSWORD_PARAMTER_NAME, password);
	}

}
