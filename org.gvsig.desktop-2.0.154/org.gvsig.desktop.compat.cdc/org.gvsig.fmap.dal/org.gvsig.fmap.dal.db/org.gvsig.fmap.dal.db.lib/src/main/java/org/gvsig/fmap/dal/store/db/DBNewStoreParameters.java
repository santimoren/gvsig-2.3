/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.db;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.db.DBConnectionParameter;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DelegatedDynObject;


/**
 * Abstract Data base Store Parameters
 *
 * @author jmvivo
 *
 */
public abstract class DBNewStoreParameters extends AbstractDataParameters
        implements NewFeatureStoreParameters, DBConnectionParameter {

    public static final String PARAMETERS_DEFINITION_NAME = "DBNewStoreParameters";

    
    /**
     * Parameter name for the name of <code>table</code><br>
     *
     * @see #getTable()
     * @see #setTable(String)
     */
    public static final String TABLE_PARAMTER_NAME = "Table";
    
    /**
     * This instance contains the value of the current parameters.
     */
    private DelegatedDynObject parameters;

    public DBNewStoreParameters(String parametersDefinitionName, String providerName) {
        this.parameters = (DelegatedDynObject) DBHelper.newParameters(parametersDefinitionName);
        this.setDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME, providerName);
    }

    protected DelegatedDynObject getDelegatedDynObject() {
        return parameters;
    }

    public String getDataStoreName() {
        return (String) this.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
    }

    public String getDescription() {
        return this.getDynClass().getDescription();
    }

    public boolean isValid() {
        return this.getHost() != null;
    }

    public String getHost() {
        return (String) this.getDynValue(HOST_PARAMTER_NAME);
    }

    public Integer getPort() {
        return (Integer) this.getDynValue(PORT_PARAMTER_NAME);
    }

    public String getDBName() {
        return (String) this.getDynValue(DBNAME_PARAMTER_NAME);
    }

    public String getUser() {
        return (String) this.getDynValue(USER_PARAMTER_NAME);
    }

    public String getPassword() {
        return (String) this.getDynValue(PASSWORD_PARAMTER_NAME);
    }

    public void setHost(String host) {
        this.setDynValue(HOST_PARAMTER_NAME, host);
    }

    public void setPort(int port) {
        this.setDynValue(PORT_PARAMTER_NAME, new Integer(port));
    }

    /**
     * Set <code>port/code> parameter value
     *
     * @param port
     */
    public void setPort(Integer port) {
        this.setDynValue(PORT_PARAMTER_NAME, port);
    }

    /**
     * Set <code>data base name/code> parameter value
     *
     * @param data
     *            base name
     */
    public void setDBName(String dbName) {
        this.setDynValue(DBNAME_PARAMTER_NAME, dbName);
    }

    /**
     * Set <code>user/code> parameter value
     *
     * @param user
     */
    public void setUser(String user) {
        this.setDynValue(USER_PARAMTER_NAME, user);
    }

    /**
     * Set <code>password/code> parameter value
     *
     * @param password
     */
    public void setPassword(String password) {
        this.setDynValue(PASSWORD_PARAMTER_NAME, password);
    }

    public EditableFeatureType getDefaultFeatureType() {
        return (EditableFeatureType) this.getDynValue(FEATURETYPE_PARAMTER_NAME);
    }

    public void setDefaultFeatureType(FeatureType featureType) {
        this.setDynValue(FEATURETYPE_PARAMTER_NAME, featureType);
    }

    /**
     * Get <code>table</code> parameter value<br>
     * <br>
     *
     * This parameters describes what table we want to create.<br>
     *
     * @see #setTable(String)
     */
    public String getTable() {
        return (String) this.getDynValue(TABLE_PARAMTER_NAME);
    }

    /**
     * Set <code>table</code> parameter value<br>
     * <br>
     *
     * This parameters describes what table we want to create.<br>
     *
     * @param table
     *
     * @see #getTable(String)
     */
    public void setTable(String table) {
        this.setDynValue(TABLE_PARAMTER_NAME, table);
    }
}
