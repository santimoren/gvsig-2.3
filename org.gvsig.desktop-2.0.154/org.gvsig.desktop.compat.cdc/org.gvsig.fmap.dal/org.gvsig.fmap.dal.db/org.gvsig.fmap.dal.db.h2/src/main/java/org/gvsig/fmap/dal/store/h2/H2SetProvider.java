/* gvSIG. Geographic Information System of the Valencian Government
*
* Copyright (C) 2007-2008 Infrastructures and Transports Department
* of the Valencian Government (CIT)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA  02110-1301, USA.
*
*/

/*
* AUTHORS (In addition to CIT):
* 2009 IVER T.I   {{Task}}
*/

/**
 *
 */
package org.gvsig.fmap.dal.store.h2;

import java.util.ArrayList;
import java.util.List;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.dal.store.db.DBStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCSetProvider;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreProvider;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorFieldValue;
import org.gvsig.tools.evaluator.EvaluatorFieldsInfo;

/**
 * @author jmvivo
 *
 */
public class H2SetProvider extends JDBCSetProvider {

    private static Logger logger = LoggerFactory.getLogger(H2SetProvider.class);
    
	public H2SetProvider(JDBCStoreProvider store, FeatureQuery query,
			FeatureType featureType) throws DataException {
		super(store, query, featureType);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureSetProvider#canFilter()
	 */
	public boolean canFilter() {
		// TODO more checks
		if (!super.canFilter()) {
			return false;
		}
		return true;

	}
	
    protected String getSqlForEvaluator(Evaluator filter) {
        
        String resp = null;
        if (filter != null && filter.getSQL() != null) {
            // =================================================
            EvaluatorFieldsInfo info = filter.getFieldsInfo();
            String filterString = filter.getSQL();
            
            String[] filterNames = null;
            String[] finalNames = null;
            
            if (info == null) {
                filterNames = getFieldNames(getFeatureType());
            } else {
                filterNames = info.getFieldNames();
            }
            
            finalNames = new String[filterNames.length];
            FeatureAttributeDescriptor attr;
            for (int i = 0; i < filterNames.length; i++) {
                attr = getFeatureType().getAttributeDescriptor(filterNames[i]);
                if (attr == null) {
                    finalNames[i] = filterNames[i];
                    continue;
                }
                finalNames[i] = getEscapedFieldName(attr.getName());
            }

            for (int i = 0; i < filterNames.length; i++) {
                if (!filterNames[i].equals(finalNames[i])) {
                    filterString = filterString.replaceAll(
                            "\\b" + filterNames[i] + "\\b",
                            finalNames[i]);
                }
            }
            resp = filterString;        
        }
        // ================================
        // In any case, append working area filter
        
        try {
            resp = appendWorkingAreaCondition(resp);
        } catch (Exception e) {
            logger.error("While appending working area condition.", e);
        }
        return resp;
    }	
    
    private String[] getFieldNames(FeatureType ft) {
        
        if (ft == null) {
            return new String[0];
        }
        FeatureAttributeDescriptor[] atts = ft.getAttributeDescriptors();
        String[] resp = new String[atts.length];
        for (int i=0; i<atts.length; i++) {
            resp[i] = atts[i].getName();
        }
        return resp;
    }
    
    private String getFunctionName(String newFunctionName) {
        
        H2StoreProvider pg_sto_prov = (H2StoreProvider) this.getStore();
        H2Helper hpr = pg_sto_prov.getPgHelper();
        if (hpr == null) {
            logger.info("Unable to get PG helper.", new Exception("Helper is null"));
            return newFunctionName;
        } else {
            return hpr.getFunctionName(newFunctionName);
        }
    }


    private String appendWorkingAreaCondition(String sql) throws Exception {
        
        
        DBStoreParameters dbParams = 
        (DBStoreParameters) getStore().getParameters();
        
        Envelope wa = dbParams.getWorkingArea(); 
        if (wa == null) {
            return sql;
        } else {
            
            FeatureStore fstore = this.getStore().getFeatureStore();
            String geoname =
                fstore.getDefaultFeatureType().getDefaultGeometryAttributeName();
            
            StringBuffer strbuf = new StringBuffer();
            
            if (sql == null)  {
                strbuf.append(
                    getFunctionName("ST_Intersects") + "("
                    + getFunctionName("ST_GeomFromText") + "('");
            } else {
                strbuf.append("(");
                strbuf.append(sql);
                strbuf.append(") AND "
                    + getFunctionName("ST_Intersects") + "("
                    + getFunctionName("ST_GeomFromText") + "('");
            }
            
            String workAreaWkt = null;
            workAreaWkt = wa.getGeometry().convertToWKT();
            strbuf.append(workAreaWkt);
            strbuf.append("', ");
            
            H2StoreProvider sprov = (H2StoreProvider) getStore();
            H2Helper helper = sprov.getPgHelper();
            
            IProjection proj = dbParams.getCRS();
            int sridInt = helper.getProviderSRID(proj); 
            if (sridInt == -1) {
                throw new CreateGeometryException(
                        new Exception("CRS is null or unknown."));
            } else {
                strbuf.append(Integer.toString(sridInt));
            }
            strbuf.append("), " + getFunctionName("ST_Envelope") + "(");
            strbuf.append(helper.escapeFieldName(geoname));
            strbuf.append("))");
            
            return strbuf.toString();
        }
    }
	

}
