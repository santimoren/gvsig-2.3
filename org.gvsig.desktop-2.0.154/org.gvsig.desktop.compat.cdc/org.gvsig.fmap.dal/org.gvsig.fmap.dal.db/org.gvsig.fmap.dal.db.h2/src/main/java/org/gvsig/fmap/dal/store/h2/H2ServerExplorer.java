/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */

/*
 * AUTHORS (In addition to CIT):
 * 2009 IVER T.I   {{Task}}
 */
/**
 *
 */
package org.gvsig.fmap.dal.store.h2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.RemoveException;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.spi.DataServerExplorerProviderServices;
import org.gvsig.fmap.dal.store.jdbc.JDBCHelper;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorer;
import org.gvsig.fmap.dal.store.jdbc.JDBCStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.TransactionalAction;
import org.gvsig.fmap.dal.store.jdbc.exception.JDBCExecuteSQLException;
import org.gvsig.fmap.dal.store.jdbc.exception.JDBCSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jmvivo
 *
 */
public class H2ServerExplorer extends JDBCServerExplorer {

    final static private Logger logger = LoggerFactory
            .getLogger(H2ServerExplorer.class);

    public static final String NAME = "H2SQLExplorer";

    public H2ServerExplorer(
            H2ServerExplorerParameters parameters,
            DataServerExplorerProviderServices services)
            throws InitializeException {
        super(parameters, services);
    }

    private H2ServerExplorerParameters getH2SQLParameters() {
        return (H2ServerExplorerParameters) getParameters();
    }

    protected JDBCHelper createHelper() throws InitializeException {
        return new H2Helper(this, getH2SQLParameters());
    }

    public String getStoreName() {
        return H2StoreProvider.NAME;
    }

    public String getProviderName() {
        return NAME;
    }

    protected JDBCStoreParameters createStoreParams()
            throws InitializeException, ProviderNotRegisteredException {
        H2StoreParameters orgParams = (H2StoreParameters) super
                .createStoreParams();

        orgParams.setUseSSL(getH2SQLParameters().getUseSSL());

        return orgParams;
    }

	// ****************************
    public boolean canAdd() {
        return true;
    }

    public DataStoreParameters getOpenParameters() throws DataException {
        H2ServerExplorerParameters parameters = getH2SQLParameters();
        H2StoreParameters params = new H2StoreParameters();
        params.setHost(parameters.getHost());
        params.setPort(parameters.getPort());
        params.setDBName(parameters.getDBName());
        params.setUser(parameters.getUser());
        params.setPassword(parameters.getPassword());
        params.setCatalog(parameters.getCatalog());
        params.setSchema(parameters.getSchema());
        params.setJDBCDriverClassName(parameters.getJDBCDriverClassName());
        params.setUrl(parameters.getUrl());
        return params;
    }

    protected void checkIsMine(DataStoreParameters dsp) {
        if (!(dsp instanceof H2ConnectionParameters)) {
            // FIXME Excpetion ???
            throw new IllegalArgumentException(
                    "not instance of H2SQLConnectionParameters");
        }
        super.checkIsMine(dsp);

        H2ConnectionParameters pgp = (H2ConnectionParameters) dsp;
        if (pgp.getUseSSL().booleanValue() != getH2SQLParameters()
                .getUseSSL()) {
            throw new IllegalArgumentException("worng explorer: Host");
        }
    }

    public void remove(DataStoreParameters dsp) throws RemoveException {
        final H2StoreParameters pgParams = (H2StoreParameters) dsp;

        TransactionalAction action = new TransactionalAction() {
            public boolean continueTransactionAllowed() {
                return false;
            }

            public Object action(Connection conn) throws DataException {

                Statement st;
                try {
                    st = conn.createStatement();
                } catch (SQLException e) {
                    throw new JDBCSQLException(e);
                }

                String sqlDrop = "Drop table "
                        + pgParams.tableID();

                StringBuilder strb = new StringBuilder();
                strb.append("Delete from GEOMETRY_COLUMNS where f_table_schema = ");
                if (pgParams.getSchema() == null || pgParams.getSchema().length() == 0) {
                    strb.append("current_schema() ");
                } else {
                    strb.append('\'');
                    strb.append(pgParams.getSchema());
                    strb.append("' ");
                }
                strb.append("and f_table_name = '");
                strb.append(pgParams.getTable());
                strb.append('\'');

                String sqlDeleteFromGeometry_column = strb.toString();
                try {
                    try {
                        JDBCHelper.execute(st, sqlDrop);
                    } catch (SQLException e) {
                        throw new JDBCExecuteSQLException(sqlDrop, e);
                    }

                    try {
                        JDBCHelper.execute(st, sqlDeleteFromGeometry_column);
                    } catch (SQLException e) {
                        throw new JDBCExecuteSQLException(
                                sqlDeleteFromGeometry_column, e);
                    }

                } finally {
                    try {
                        st.close();
                    } catch (SQLException e) {
                    };
                }
                return null;
            }
        };
        try {
            this.helper.doConnectionAction(action);
        } catch (Exception e) {
            throw new RemoveException(this.getProviderName(), e);
        }
    }

    public NewDataStoreParameters getAddParameters() throws DataException {
        H2ServerExplorerParameters parameters = getH2SQLParameters();
        H2NewStoreParameters params = new H2NewStoreParameters();
        params.setHost(parameters.getHost());
        params.setPort(parameters.getPort());
        params.setDBName(parameters.getDBName());
        params.setUser(parameters.getUser());
        params.setPassword(parameters.getPassword());
        params.setCatalog(parameters.getCatalog());
        params.setSchema(parameters.getSchema());
        params.setJDBCDriverClassName(parameters.getJDBCDriverClassName());
        params.setUrl(parameters.getUrl());
        params.setUseSSL(parameters.getUseSSL());

        params.setDefaultFeatureType(this.getServerExplorerProviderServices()
                .createNewFeatureType());

        return params;
    }

    public boolean hasGeometrySupport() {
        return true;
    }

    protected H2Helper getPgHelper() {
        return (H2Helper) getHelper();
    }

    @Override
    public List getDataStoreProviderNames() {
        List x = new ArrayList(1);
        x.add(H2StoreProvider.NAME);
        return x;
    }

    public void updateTableStatistics(String tableName) throws JDBCExecuteSQLException {
        String sql="";
        try {
            
            if( tableName.startsWith("\"") ) {
                sql = "VACUUM ANALYZE " + tableName ;
            } else {
                sql = "VACUUM ANALYZE \"" + tableName + "\"";
            }
            Connection conn = this.getHelper().getConnection();
            Statement st = conn.createStatement();
            JDBCHelper.execute(st, sql);
        } catch (Exception ex) {
            throw new JDBCExecuteSQLException(sql, ex);
        }

    }

}
