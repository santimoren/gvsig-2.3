/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */

/*
 * AUTHORS (In addition to CIT):
 * 2009 IVER T.I   {{Task}}
 */
package org.gvsig.fmap.dal.store.h2;

import java.io.File;
import org.gvsig.fmap.dal.store.jdbc.JDBCResourceParameters;

import static org.gvsig.fmap.dal.store.h2.H2ConnectionParameters.PARAMETER_NAME_BASEFOLDER;

public class H2ResourceParameters extends JDBCResourceParameters
        implements H2ConnectionParameters {

    public static final String PARAMETERS_DEFINITION_NAME = "H2SQLResourceParameters";

    public H2ResourceParameters() {
        super(PARAMETERS_DEFINITION_NAME, H2Resource.NAME);
    }

    public H2ResourceParameters(String url, String host, Integer port,
            String dbName, String user, String password,
            String jdbcDriverClassName, Boolean ssl) {
        super(PARAMETERS_DEFINITION_NAME, H2Resource.NAME, url, host, port, dbName, user, password, jdbcDriverClassName);
        if ( ssl != null ) {
            this.setUseSSL(ssl.booleanValue());
        }
    }

    public String getUrl() {
        return H2ParametersHelper.getConnectionString(this);
    }

    public Boolean getUseSSL() {
        return (Boolean) this.getDynValue(PARAMETER_NAME_USESSL);
    }

    public void setUseSSL(Boolean useSSL) {
        this.setDynValue(PARAMETER_NAME_USESSL, useSSL);
    }

    public void setUseSSL(boolean useSSL) {
        this.setDynValue(PARAMETER_NAME_USESSL, new Boolean(useSSL));
    }

    public File getBaseFolder() {
        return (File) this.getDynValue(PARAMETER_NAME_BASEFOLDER);
    }

    public void setBaseFolder(File baseFolder) {
        this.setDynValue(PARAMETER_NAME_BASEFOLDER, baseFolder);
    }
}
