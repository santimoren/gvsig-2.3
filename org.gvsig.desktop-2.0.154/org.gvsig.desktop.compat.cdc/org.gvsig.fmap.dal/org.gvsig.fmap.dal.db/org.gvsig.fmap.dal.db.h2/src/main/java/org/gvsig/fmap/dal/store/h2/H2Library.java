/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.fmap.dal.store.h2;

import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.store.db.DBHelper;
import org.gvsig.fmap.dal.store.jdbc.JDBCLibrary;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class H2Library extends AbstractLibrary {

    static String DEFAULT_H2_DRIVER_NAME = "org.h2.Driver";

    @Override
    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
        require(JDBCLibrary.class);
    }

    @Override
    protected void doInitialize() throws LibraryException {
    }

    @Override
    protected void doPostInitialize() throws LibraryException {
        LibraryException ex = null;

        //new JDBCLibrary().postInitialize();

        DBHelper.registerParametersDefinition(
                H2StoreParameters.PARAMETERS_DEFINITION_NAME,
                H2StoreParameters.class,
                "H2SQLParameters.xml"
        );
        DBHelper.registerParametersDefinition(
                H2NewStoreParameters.PARAMETERS_DEFINITION_NAME,
                H2NewStoreParameters.class,
                "H2SQLParameters.xml"
        );
        DBHelper.registerParametersDefinition(
                H2ServerExplorerParameters.PARAMETERS_DEFINITION_NAME,
                H2ServerExplorerParameters.class,
                "H2SQLParameters.xml"
        );
        DBHelper.registerParametersDefinition(
                H2ResourceParameters.PARAMETERS_DEFINITION_NAME,
                H2ResourceParameters.class,
                "H2SQLParameters.xml"
        );
        try {
            DBHelper.registerMetadataDefinition(
                    H2StoreProvider.METADATA_DEFINITION_NAME,
                    H2StoreProvider.class,
                    "H2SQLMetadata.xml"
            );
        } catch (MetadataException e) {
            ex = new LibraryException(this.getClass(), e);
        }

        ResourceManagerProviderServices resman = (ResourceManagerProviderServices) DALLocator
                .getResourceManager();

        if ( !resman.getResourceProviders().contains(H2Resource.NAME) ) {
            resman.register(H2Resource.NAME,
                    H2Resource.DESCRIPTION, H2Resource.class,
                    H2ResourceParameters.class);
        }

        DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator
                .getDataManager();

        if ( !dataman.getStoreProviders().contains(H2StoreProvider.NAME) ) {
            dataman.registerStoreProvider(H2StoreProvider.NAME,
                    H2StoreProvider.class,
                    H2StoreParameters.class);
        }

        if ( !dataman.getExplorerProviders().contains(
                H2StoreProvider.NAME) ) {
            dataman.registerExplorerProvider(H2ServerExplorer.NAME,
                    H2ServerExplorer.class,
                    H2ServerExplorerParameters.class);
        }
        if ( ex != null ) {
            throw ex;
        }
    }

}
