/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.fmap.dal.store.h2;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class H2ParametersHelper {

    private static Logger logger = LoggerFactory.getLogger(H2ParametersHelper.class);

    public static String getConnectionString(H2ConnectionParameters params) {
        String connectionString = null;

        File baseFolder = params.getBaseFolder();
        String baseFolderPath = null;
        if ( baseFolder == null ) {
            baseFolder = getDefaultBaseFolder();
        }
        if ( baseFolder == null ) {
            baseFolderPath = baseFolder.getAbsolutePath();
            baseFolderPath = baseFolderPath.replace("\\", "/");
        }
        
        String dbname = params.getDBName();
        if ( dbname == null ) {
            logger.warn("dbname is null.");
            throw new IllegalArgumentException("dbname is null");
        }

        String host = params.getHost();

        Integer port = params.getPort();
        if ( port == null ) {
            port = new Integer(9123);
        }

        if ( StringUtils.isBlank(host) ) {
            if( baseFolderPath == null ) {
                throw new IllegalArgumentException("baseFolder required");
            }
            connectionString = "jdbc:h2:" + baseFolderPath + "/" + dbname + ";MODE=PostgreSQL;AUTO_SERVER=TRUE";
        } else {
            if ( host.trim().equalsIgnoreCase("localhost") ) {
                if( baseFolderPath == null ) {
                    connectionString = "jdbc:h2:tcp://localhost:" + port.intValue() + "/" + dbname + ";MODE=PostgreSQL";
                } else {
                    connectionString = "jdbc:h2:" + baseFolderPath + "/" + dbname + ";MODE=PostgreSQL;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=" + port;
                }
            } else {
                if( baseFolderPath == null ) {
                    connectionString = "jdbc:h2:tcp://" + host + ":" + port.intValue() + "/" + dbname + ";MODE=PostgreSQL";
                } else {
                    connectionString = "jdbc:h2:tcp://" + host + ":" + port.intValue() + "/" + dbname + ":" + baseFolder + ";MODE=PostgreSQL";
                }
            }
        }

        return connectionString;
    }

    public static File getDefaultBaseFolder() {
        File baseFolder = null;
        String s = System.getProperty("user.home");
        File baseFolderInHome = new File(s + File.separator + "gvSIG" + File.separator + "dbs.h2");
        if ( !baseFolderInHome.exists() ) {
            try {
                FileUtils.forceMkdir(baseFolderInHome);
                baseFolder = baseFolderInHome;
            } catch (IOException ex) {

            }
        }
        if ( baseFolder == null ) {
            s = System.getProperty("java.io.tmpdir");
            File baseFolderInTemp = new File(s + File.separator + "gvSIG" + File.separator + "dbs.h2");
            if ( !baseFolderInTemp.exists() ) {
                try {
                    FileUtils.forceMkdir(baseFolderInTemp);
                    baseFolder = baseFolderInTemp;
                } catch (IOException ex) {

                }
            }
        }
        return baseFolder;
    }

}
