/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package org.gvsig.fmap.dal.store.h2;

import java.io.File;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.store.jdbc.JDBCNewStoreParameters;

public class H2NewStoreParameters extends JDBCNewStoreParameters
        implements NewFeatureStoreParameters, H2ConnectionParameters {

    public static final String PARAMETERS_DEFINITION_NAME = "H2SQLNewStoreParameters";

    public H2NewStoreParameters() {
        super(PARAMETERS_DEFINITION_NAME, H2StoreProvider.NAME);
    }

    public EditableFeatureType getDefaultFeatureType() {
        return (EditableFeatureType) this.getDynValue(FEATURETYPE_PARAMTER_NAME);
    }

    public void setDefaultFeatureType(FeatureType featureType) {
        this.setDynValue(FEATURETYPE_PARAMTER_NAME, featureType);
    }

    public Boolean getUseSSL() {
        return (Boolean) this.getDynValue(PARAMETER_NAME_USESSL);
    }

    public void setUseSSL(Boolean useSSL) {
        this.setDynValue(PARAMETER_NAME_USESSL, useSSL);
    }
    
    
    public File getBaseFolder() {
        return (File) this.getDynValue(PARAMETER_NAME_BASEFOLDER);
    }

    public void setBaseFolder(File baseFolder) {
        this.setDynValue(PARAMETER_NAME_BASEFOLDER, baseFolder);
    }


}
