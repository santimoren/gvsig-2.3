/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing.impl.dynformfield;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dynform.spi.DynFormSPILocator;
import org.gvsig.tools.dynform.spi.DynFormSPIManager;
import org.gvsig.tools.dynform.spi.JDynFormFieldFactory;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ServiceManager;

public class JDynFormFieldFeaturesTableLinkFactory implements JDynFormFieldFactory {

	private String name = null;
	private DynStruct parametersDefinition = null;
	
        @Override
	public String getName() {
		if( name == null ) {
			DynFormSPIManager manager = DynFormSPILocator.getDynFormSPIManager();
			this.name = manager.makeServiceName(DataTypes.LIST, "DAL.SimpleFeaturesTableLink");
		}
		return this.name;
	}


        @Override
	public Service create(DynObject parameters, ServiceManager serviceManager)
			throws ServiceException {
		return new JDynFormFieldFeaturesTableLink(parameters, serviceManager);
	}

        @Override
	public DynObject createParameters() {
		return ToolsLocator.getDynObjectManager().createDynObject(parametersDefinition);
	}

        @Override
	public void initialize() {
		if( this.parametersDefinition == null ) {
			String serviceName = this.getName();
    		DynObjectManager manager = ToolsLocator.getDynObjectManager();
    		this.parametersDefinition = manager.createDynClass(
    				serviceName, "Parameters definition for Feature links fields in dynamic forms");
    		this.parametersDefinition.addDynFieldObject(DynFormSPIManager.FIELD_FIELDDEFINITION)
				.setClassOfValue(DynField.class).setMandatory(true);
    		this.parametersDefinition.addDynFieldObject(DynFormSPIManager.FIELD_VALUE)
				.setClassOfValue(DynField.class).setMandatory(true);
		}
	}


}
