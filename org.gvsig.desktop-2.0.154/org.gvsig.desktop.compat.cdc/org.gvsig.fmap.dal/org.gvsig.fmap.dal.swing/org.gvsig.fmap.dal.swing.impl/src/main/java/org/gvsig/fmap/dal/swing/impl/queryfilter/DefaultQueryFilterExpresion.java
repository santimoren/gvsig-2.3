package org.gvsig.fmap.dal.swing.impl.queryfilter;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;
import org.freixas.jcalendar.JCalendar;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.queryfilter.QueryFilterExpresion;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultQueryFilterExpresion extends DefaultQueryFilterExpresionView implements QueryFilterExpresion {

    private static final Logger logger = LoggerFactory.getLogger(DefaultQueryFilterExpresion.class);

    public static final String EQ = "eq";
    public static final String NE = "ne";
    public static final String GT = "gt";
    public static final String LT = "lt";
    public static final String GE = "ge";
    public static final String LE = "le";
    public static final String AND = "and";
    public static final String OR = "or";
    public static final String NOT = "not";
    public static final String DATE = "date";
    public static final String PARENTHESIS = "parenthesis";
    public static final String LIKE = "like";

    private FeatureStore store = null;
    private JCalendar jcalendar = null;
    private FillValuesTask fillValuesTask = null;
    private RSyntaxTextArea txtExpresion;

    public DefaultQueryFilterExpresion(FeatureStore store) {
        this.initComponents();
        this.setFeatureStore(store);
    }

    private void initComponents() {
        this.lstValues.setModel(new DefaultListModel());
        ActionListener actionOperator = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doApplyOperator(ae.getActionCommand());
            }
        };
        this.btnAnd.addActionListener(actionOperator);
        this.btnGreat.addActionListener(actionOperator);
        this.btnGreatEqual.addActionListener(actionOperator);
        this.btnLess.addActionListener(actionOperator);
        this.btnLessEqual.addActionListener(actionOperator);
        this.btnLike.addActionListener(actionOperator);
        this.btnNot.addActionListener(actionOperator);
        this.btnEqual.addActionListener(actionOperator);
        this.btnNotEqual.addActionListener(actionOperator);
        this.btnOr.addActionListener(actionOperator);
        this.btnParenthesis.addActionListener(actionOperator);

        this.btnClearText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doClearExpresion();
            }
        });
        this.btnDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                JPopupMenu menu = new JPopupMenu();
                menu.add(getJCalendar());
                JComponent thisComp = (JComponent) event.getSource();
                menu.show(thisComp, 0, thisComp.getY() + 22);
            }
        });
        this.lstFields.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                int index;
                switch (evt.getClickCount()) {
                    case 1:
                        index = lstFields.locationToIndex(evt.getPoint());
                        if (index >= 0) {
                            doFillValues((String) (lstFields.getModel().getElementAt(index)));
                        }
                        break;
                    case 2:
                        index = lstFields.locationToIndex(evt.getPoint());
                        if (index >= 0) {
                            putSymbol(" " + lstFields.getModel().getElementAt(index) + " ");
                        }
                        break;
                }
            }
        });
        this.lstValues.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                int index;
                if (evt.getClickCount() == 1) {
                    index = lstValues.locationToIndex(evt.getPoint());
                    if (index >= 0) {
                        Object value = lstValues.getModel().getElementAt(index);
                        if (value != null) {
                            putValue(value.toString());
                        }
                    }
                }
            }
        });
        this.txtExpresion = this.createTextArea();
        this.pnlExpresionContainer.setLayout(new BorderLayout());
        this.pnlExpresionContainer.add(new RTextScrollPane(this.txtExpresion), BorderLayout.CENTER);
    }

    public void doFillValues(String fieldName) {
        if (this.fillValuesTask != null) {
            this.fillValuesTask.cancel();
        }
        this.fillValuesTask = new FillValuesTask(fieldName);
        ((DefaultListModel) (this.lstValues.getModel())).removeAllElements();
        this.lblLoading.setText("Loading...");
        this.fillValuesTask.start();
    }

    private RSyntaxTextArea createTextArea() {
        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        textArea.setCodeFoldingEnabled(true);
        textArea.setClearWhitespaceLinesEnabled(true);
        textArea.setAutoIndentEnabled(true);
        textArea.setCloseCurlyBraces(true);
        textArea.setWhitespaceVisible(true);
        textArea.setAnimateBracketMatching(true);
        textArea.setBracketMatchingEnabled(true);
        textArea.setAutoIndentEnabled(true);
        textArea.setTabsEmulated(true);
        textArea.setTabSize(2);
        textArea.setAntiAliasingEnabled(true);
        textArea.setAutoscrolls(true);
        textArea.setRows(4);
        return textArea;
    }

    @Override
    public JComponent asJComponent() {
        return this;
    }

    private class FillValuesTask extends Thread {

        private final String[] fieldNames;
        private boolean isCanceled;

        public FillValuesTask(String fieldName) {
            super();
            this.fieldNames = new String[]{fieldName};
            this.isCanceled = false;
        }

        public void cancel() {
            this.isCanceled = true;
        }

        @Override
        public void run() {
            try {
                FeatureQuery query = store.createFeatureQuery();
                query.setAttributeNames(fieldNames);
                FeatureSet fs = store.getFeatureSet(query);
                final TreeSet values = new TreeSet(new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        try {
                            return ((Comparable) o1).compareTo(o2);
                        } catch(Exception ex) {
                            return 1;
                        }
                    }
                });
                fs.accept(new Visitor() {
                    @Override
                    public void visit(Object o) throws VisitCanceledException, BaseException {
                        values.add(((Feature) o).get(0));
                        if (isCanceled) {
                            throw new VisitCanceledException();
                        }
                    }
                });
                final DefaultListModel model = new DefaultListModel();
                for (Object value : values) {
                    model.addElement(value);
                }
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        lstValues.setModel(model);
                        lblLoading.setText("");
                    }
                });
                fillValuesTask = null;
            } catch (Exception ex) {
                logger.warn("Can't fill list of values.", ex);
            }
        }
    }

    @Override
    public void setFeatureStore(FeatureStore store) {
        this.store = store;
        DefaultListModel<String> model = new DefaultListModel<String>();
        List<String> names = new ArrayList<String>();
        try {
            for (FeatureAttributeDescriptor descriptor : store.getDefaultFeatureType()) {
                names.add(descriptor.getName());
            }
            Collections.sort(names);
            for( String name : names ) {
                model.addElement(name);
            }
            this.lstFields.setModel(model);
        } catch (DataException e) {
            logger.warn("Can't fill fields in filter dialog.", e);
        }
    }

    @Override
    public void setExpresion(String expresion) {
        this.txtExpresion.setText(expresion);
    }

    @Override
    public String getExpresion() {
        return this.txtExpresion.getText();
    }

    private JCalendar getJCalendar() {
        if (this.jcalendar == null) {
            this.jcalendar = new JCalendar();
            this.jcalendar.addDateListener(new DateListener() {
                @Override
                public void dateChanged(DateEvent de) {
                    doSetDate(de.getSelectedDate().getTime());
                }
            });
        }
        return (JCalendar) this.jcalendar;
    }

    public void doSetDate(Date date) {
        this.putValue(date);
    }

    public void doClearExpresion() {
        this.txtExpresion.setText("");
    }

    public void doApplyOperator(String operator) {
        switch (operator) {
            case EQ:
                putSymbol(" = ");
                break;
            case NE:
                putSymbol(" <> ");
                break;
            case GT:
                putSymbol(" > ");
                break;
            case LT:
                putSymbol(" < ");
                break;
            case GE:
                putSymbol(" >= ");
                break;
            case LE:
                putSymbol(" <= ");
                break;
            case AND:
                putSymbol(" and ");
                break;
            case OR:
                putSymbol(" or ");
                break;
            case NOT:
                putSymbol(" not ");
                break;
            case PARENTHESIS:
                putSymbol(" (  ) ");
                break;
            case LIKE:
                putSymbol(" like ");
                break;

        }
    }

    private void putSymbol(String symbol) {
        int position = this.txtExpresion.getCaretPosition();
        this.txtExpresion.setText(insert(this.txtExpresion.getText(), position, symbol));
        if (symbol.equals(" () ")) {
            position = position + 3;
        } else {
            position = position + symbol.length();
        }
        this.txtExpresion.setCaretPosition(position);
    }

    private void putValue(Object value) {
        if (value instanceof Date) {
            putSymbol(" DATE('" + value + "') ");
        } else if (value instanceof Boolean) {
            putSymbol(value.toString());
        } else if (value instanceof String) {
            putSymbol(" '" + value + "' ");
        } else {
            putSymbol(" " + value.toString() + " ");
        }
    }

    private String insert(String base, int position, String toInsert) {
        return base.substring(0, position) + toInsert + base.substring(position);
    }

}
