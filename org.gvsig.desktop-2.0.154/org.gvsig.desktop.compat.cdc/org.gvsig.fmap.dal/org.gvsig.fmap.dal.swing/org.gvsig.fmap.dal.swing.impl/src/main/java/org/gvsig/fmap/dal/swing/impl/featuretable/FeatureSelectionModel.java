/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {}  {{Task}}
 */
package org.gvsig.fmap.dal.swing.impl.featuretable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 2010- C�sar Ordi�ana - gvSIG team
 */
public class FeatureSelectionModel implements ListSelectionModel, Observer {
    private static final Logger LOG = LoggerFactory.getLogger(FeatureSelectionModel.class);

	protected EventListenerList listenerList = new EventListenerList();

	private final FeatureTableModel featureTableModel;

	private int selectionMode = SINGLE_INTERVAL_SELECTION;

	private boolean isAdjusting = false;

	private int anchor = -1;

	private int lead = -1;

	private int currentFirst = -1;
	private int currentLast = -1;

	/**
	 * Creates a new {@link FeatureSelectionModel} with a
	 * {@link FeatureTableModel} used to get the {@link Feature}s by position in
	 * the table.
	 *
	 * @param featureTableModel
	 *            to get Features from
	 * @throws DataException
	 *             if there is an error getting the store selection
	 */
	public FeatureSelectionModel(FeatureTableModel featureTableModel)  {
		this.featureTableModel = featureTableModel;
                this.featureTableModel.getFeatureSelection().addObserver(this);
                this.featureTableModel.addChangeListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if( FeatureTableModel.CHANGE_SELECTION.equals(e.getActionCommand()) ) {
                            featureSelectionChanged();
                        }
                    }
                });
	}
        
        private void featureSelectionChanged() {
            this.featureTableModel.getFeatureSelection().addObserver(this);
        }
        
    @Override
	public int getAnchorSelectionIndex() {
		return anchor;
	}

    @Override
	public int getLeadSelectionIndex() {
		return lead;
	}

    @Override
	public int getMaxSelectionIndex() {

	    int resp = this.getSelectionIndex(true);
	    return resp;
	    /*
	     *
	     * The call to "featureTableModel.getFeatureAt(i)"
	     * causes a lot of reloadPage all over
	     *
		FeatureSelection selection = getFeatureSelection();
		try {
			if (!selection.isEmpty()) {
				for (int i = featureTableModel.getRowCount() - 1; i >= 0; i--) {
					if (selection.isSelected(featureTableModel.getFeatureAt(i))) {
						return i;
					}
				}
			}
		} catch (DataException e) {
			throw new SelectionChangeException(e);
		}
		return -1;
		*/
	}

    @Override
	public int getMinSelectionIndex() {

	    int resp = this.getSelectionIndex(false);
	    return resp;

	    /*
	     *
         * The call to "featureTableModel.getFeatureAt(i)"
         * causes a lot of reloadPage all over
	     *
		try {

		    int ii = 0;

		    FeatureSelection selection = this.getFeatureSelection();
            for (int i = 0; i < featureTableModel.getRowCount(); i++) {
                if (selection.isSelected(featureTableModel.getFeatureAt(i))) {
                    ii = i;
                }
            }
		} catch (Exception e) {
			throw new SelectionChangeException(e);
		}
		*/

	}

    @Override
	public void insertIndexInterval(int index, int length, boolean before) {
		// Nothing to do
	}

    @Override
	public void removeIndexInterval(int index0, int index1) {
		// Nothing to do
	}

    @Override
	public void setAnchorSelectionIndex(int index) {
		this.anchor = index;
	}

    @Override
	public void setLeadSelectionIndex(int index) {
		this.lead = index;
	}

    @Override
	public void addSelectionInterval(int index0, int index1) {
	    if (!featureTableModel.isSelectionLocked()){
	        doWithSelection(new FeatureSelectionOperation() {

                    @Override
	            public void doWithSelection(FeatureSelection selection, int first,
	                int last) throws DataException {
	                for (int i = first; i <= last; i++) {
	                    Feature feature = getFeature(i);
	                    if (!selection.isSelected(feature)) {
	                        selection.select(feature);
	                    }
	                }
	            }

	        }, index0, index1, true);
	    }
	}

    @Override
	public void setSelectionInterval(int index0, int index1) {
	    if (!featureTableModel.isSelectionLocked()){
	        doWithSelection(new FeatureSelectionOperation() {

                    @Override
	            public void doWithSelection(FeatureSelection selection, int first,
	                int last) throws DataException {
	                selection.deselectAll();
	                for (int i = first; i <= last; i++) {
	                    Feature feature = getFeature(i);
	                    selection.select(feature);
	                }
	            }

	        }, index0, index1, true);
	    }
	}

    @Override
	public void removeSelectionInterval(int index0, int index1) {
	    if (!featureTableModel.isSelectionLocked()){
	        doWithSelection(new FeatureSelectionOperation() {

                    @Override
	            public void doWithSelection(FeatureSelection selection, int first,
	                int last) throws DataException {
	                for (int i = first; i <= last; i++) {
	                    Feature feature = getFeature(i);
	                    if (selection.isSelected(feature)) {
	                        selection.deselect(feature);
	                    }
	                }
	            }

	        }, index0, index1, false);
	    }
	}

    @Override
	public void clearSelection() {
	    if (!featureTableModel.isSelectionLocked()){
	        try {
	            getFeatureSelection().deselectAll();
	        } catch (DataException e) {
	            throw new SelectionChangeException(e);
	        }
	    }
	}

    @Override
	public boolean isSelectedIndex(int index) {
		if (index == -1) {
			return false;
		}
		Feature feature = featureTableModel.getFeatureAt(index);
		return getFeatureSelection().isSelected(feature);
	}

    @Override
	public boolean isSelectionEmpty() {
		try {
			return getFeatureSelection().isEmpty();
		} catch (DataException ex) {
			throw new SelectionChangeException(ex);
		}
	}

    /**
     *
     * @return
     */
    @Override
	public boolean getValueIsAdjusting() {
		return isAdjusting;
	}

    @Override
	public void setValueIsAdjusting(boolean valueIsAdjusting) {
		if (this.isAdjusting != valueIsAdjusting) {
			this.isAdjusting = valueIsAdjusting;
			if (this.isAdjusting) {
				getFeatureSelection().beginComplexNotification();
			} else {
				getFeatureSelection().endComplexNotification();
			}
		}
	}

    @Override
	public int getSelectionMode() {
		return selectionMode;
	}

    @Override
	public void setSelectionMode(int selectionMode) {
		this.selectionMode = selectionMode;
	}

    @Override
	public void addListSelectionListener(ListSelectionListener listener) {
		listenerList.add(ListSelectionListener.class, listener);
	}

    @Override
	public void removeListSelectionListener(ListSelectionListener listener) {
		listenerList.remove(ListSelectionListener.class, listener);
	}

    @Override
	public void update(Observable observable, Object notification) {
            if( observable instanceof FeatureSelection ) {
                try{
                    fireValueChanged(-1, -1, false);
                }catch(ConcurrentDataModificationException e){
                    LOG.warn("The store has been updated and the selection can not be refreshed", e);
                }                        
            }
	}

	private FeatureSelection getFeatureSelection() {
                return this.featureTableModel.getFeatureSelection();
	}

	/**
	 * @param operation
	 * @param index0
	 * @param index1
	 */
	private void doWithSelection(FeatureSelectionOperation operation,
			int index0, int index1, boolean select) {
		// Set the anchor and lead
		anchor = index0;
		lead = index1;

		// As index0 <= index1 is no guaranteed, calculate the first and second
		// values
		int first = (index0 <= index1) ? index0 : index1;
		int last = (index0 <= index1) ? index1 : index0;

		//If the new selection is not updated don't continue
		if ((currentFirst == first) && (currentLast == last)){
		    return;
		}
		currentFirst = first;
		currentLast = last;

		FeatureSelection selection = getFeatureSelection();

		// Perform the selection operation into a complex notification
		selection.beginComplexNotification();
		try {
			// Is a full select or deselect
			if (first == 00 && last == featureTableModel.getRowCount() - 1) {
				if (select) {
					selection.selectAll();
				} else {
					selection.deselectAll();
				}
			} else {
				operation.doWithSelection(selection, first, last);
			}
		} catch (DataException e) {
			throw new SelectionChangeException(e);
		} finally {
			selection.endComplexNotification();
		}

		fireValueChanged(first, last, isAdjusting);
	}

	/**
	 * Returns a Feature by table row position.
	 */
	private Feature getFeature(int index) {
		return featureTableModel.getFeatureAt(index);
	}

	/**
	 * Returns the FeatureStore.
	 */
	private FeatureStore getFeatureStore() {
		return featureTableModel.getFeatureStore();
	}

	/**
	 * @param firstIndex
	 *            the first index in the interval
	 * @param lastIndex
	 *            the last index in the interval
	 * @param isAdjusting
	 *            true if this is the final change in a series of adjustments
	 * @see EventListenerList
	 */
	protected void fireValueChanged(int firstIndex, int lastIndex,
			boolean isAdjusting) {
		Object[] listeners = listenerList.getListenerList();
		ListSelectionEvent e = null;

		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ListSelectionListener.class) {
				if (e == null) {
					e =
							new ListSelectionEvent(this, firstIndex, lastIndex,
									isAdjusting);
				}
				((ListSelectionListener) listeners[i + 1]).valueChanged(e);
			}
		}
	}

	private interface FeatureSelectionOperation {
		void doWithSelection(FeatureSelection selection, int first, int last)
				throws DataException;
	}

	/**
	 *
	 * Return the index of the the first (last) selected feature
	 *
	 * @param last whether to return the index of the last selected feature
	 * @return
	 */
	private int getSelectionIndex(boolean last) {

        int ind = -1;
        int resp = -1;

        FeatureSet fs = null;
        DisposableIterator diter = null;

        try {
            FeatureSelection selection = getFeatureSelection();
            if (!selection.isEmpty()) {
            	FeatureQuery query = this.featureTableModel.getFeatureQuery();
            	if(query!= null){
            		fs = getFeatureStore().getFeatureSet(query);
            	}else{
            		fs = getFeatureStore().getFeatureSet();
            	}
                diter = fs.fastIterator();
                Feature feat = null;
                while (diter.hasNext()) {
                    ind++;
                    feat = (Feature) diter.next();
                    if (selection.isSelected(feat)) {
                        resp = ind;
                        if (!last) {
                            break;
                        }
                    }
                }

            }
        } catch (DataException e) {
            throw new SelectionChangeException(e);
        } finally {
            if (diter != null) {
                diter.dispose();
            }

            if (fs != null) {
                fs.dispose();
            }
        }
        return resp;
	}

}