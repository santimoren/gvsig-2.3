package org.gvsig.fmap.dal.swing.impl.featuretable.table.renders;

import java.awt.Component;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.swing.FeatureTableModel;

public class FormattedCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -162038647556726890L;
    private final FeatureTableModel tableModel;
    private final DecimalFormat decimalFormat;
    private final SimpleDateFormat dateFormat;

    public FormattedCellRenderer(FeatureTableModel tableModel) {
        this.tableModel = tableModel;
        this.decimalFormat = (DecimalFormat) NumberFormat.getInstance(this.tableModel.getLocaleOfData());
        this.dateFormat = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.MEDIUM, this.tableModel.getLocaleOfData());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table,value, isSelected, hasFocus,row, column);
        try {
            if ( value instanceof Number ) {
                String pattern = this.tableModel.getColumnFormattingPattern(column);
                if ( !StringUtils.isBlank(pattern) ) {
                    this.decimalFormat.applyPattern(pattern);
                    String formated = this.decimalFormat.format(value);
                    this.setHorizontalAlignment(SwingConstants.RIGHT);
                    this.setText(formated);
                    return this;
                }
            }
            if ( value instanceof Date ) {
                String pattern = this.tableModel.getColumnFormattingPattern(column);
                if ( !StringUtils.isBlank(pattern) ) {
                    this.dateFormat.applyPattern(pattern);
                    String formated = this.dateFormat.format(value);
                    this.setHorizontalAlignment(SwingConstants.RIGHT);
                    this.setText(formated);
                    return this;
                }
            }
        } catch (Exception ex) {
            // Do nothing, use values from default renderer
        }
        return this;
    }

}
