package org.gvsig.fmap.dal.swing.impl.featuretable.table.editors;

import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.DefaultFeatureTableModel;

import org.gvsig.tools.dataTypes.DataTypes;

public class FormattedCellEditor extends DefaultCellEditor {

    /**
     *
     */
    private static final long serialVersionUID = -1409667557139440155L;
    private final FeatureTableModel tableModel;
    private final SimpleDateFormat dateformat;
    private final NumberFormat numberFormat;
    private String pattern;
    private int type;

    public FormattedCellEditor(DefaultFeatureTableModel model) {
        super(new JFormattedTextField());
        this.tableModel = model;

        Locale localeOfData = this.tableModel.getLocaleOfData();
        
        this.dateformat = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(
                DateFormat.MEDIUM,
                DateFormat.MEDIUM,
                localeOfData
        );
        this.dateformat.setTimeZone(TimeZone.getDefault());
        
        this.numberFormat = NumberFormat.getInstance(localeOfData);
        this.numberFormat.setMaximumFractionDigits(Integer.MAX_VALUE);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {

        ((JComponent) getComponent()).setBorder(new LineBorder(Color.black));

        JFormattedTextField editor = (JFormattedTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
        this.pattern = this.tableModel.getColumnFormattingPattern(column);

        if( value == null ) {
            int attrindex = this.tableModel.getOriginalColumnIndex(column);
            FeatureAttributeDescriptor attrdesc = this.tableModel.getFeatureType().getAttributeDescriptor(attrindex);
            value = attrdesc.getDefaultValue();
        }
        if ( value instanceof Number ) {
            if ( value instanceof Float ) {
                this.type = DataTypes.FLOAT;
            } else if ( value instanceof Integer ) {
                this.type = DataTypes.INT;
            } else if ( value instanceof Long ) {
                this.type = DataTypes.LONG;
            } else {
                this.type = DataTypes.DOUBLE;
            }

            editor.setFormatterFactory(new DefaultFormatterFactory(
                    new NumberFormatter(numberFormat)));

            editor.setValue(value);
        }
        if ( value instanceof Date ) {
            this.type = DataTypes.DATE;
            dateformat.applyPattern(this.pattern);
            editor.setFormatterFactory(new DefaultFormatterFactory(
                    new DateFormatter(dateformat)));

            editor.setValue(value);
        }
        return editor;
    }

    @Override
    public Object getCellEditorValue() {
        Object superValue = super.getCellEditorValue();
        String str = (String) superValue;
        if ( str == null ) {
            return null;
        }

        if ( str.length() == 0 ) {
            return null;
        }

        try {
            ParsePosition pos = new ParsePosition(0);

            Number n = null;
            switch (this.type) {
            case DataTypes.DOUBLE:
                n = numberFormat.parse(str, pos);
                if ( pos.getIndex() != str.length() ) {
                    throw new ParseException("parsing incomplete", pos.getIndex());
                }
                return new Double(n.doubleValue());
                
            case DataTypes.FLOAT:
                n = numberFormat.parse(str, pos);
                if ( pos.getIndex() != str.length() ) {
                    throw new ParseException("parsing incomplete", pos.getIndex());
                }
                return new Float(n.floatValue());
                
            case DataTypes.INT:
                n = numberFormat.parse(str, pos);
                if ( pos.getIndex() != str.length() ) {
                    throw new ParseException("parsing incomplete", pos.getIndex());
                }
                return new Integer(n.intValue());
                
            case DataTypes.LONG:
                n = numberFormat.parse(str, pos);
                if ( pos.getIndex() != str.length() ) {
                    throw new ParseException("parsing incomplete", pos.getIndex());
                }
                return new Long(n.longValue());

            case DataTypes.DATE://                
                dateformat.applyPattern(this.pattern);
                Date d = dateformat.parse(str);
                return d;

            default:
                return superValue;
            }

        } catch (ParseException pex) {
            throw new RuntimeException(pex);
        }
    }

    public boolean stopCellEditing() {
        try {
            this.getCellEditorValue();
            return super.stopCellEditing();
        } catch (Exception ex) {
            ((JComponent) getComponent()).setBorder(new LineBorder(Color.red));
            return false;
        }

    }

}
