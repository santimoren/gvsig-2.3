
package org.gvsig.fmap.dal.swing.impl.featuretable.table;

import java.text.DateFormat;
import java.util.List;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataType;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldIsNotAContainerException;
import org.gvsig.tools.dynobject.exception.DynFieldValidateException;
import org.gvsig.tools.evaluator.Evaluator;


public class DynFieldFacadeOfAFeatureAttributeDescriptor implements FeatureAttributeDescriptor {

    private DynField field;
    public DynFieldFacadeOfAFeatureAttributeDescriptor(DynField field) {
        this.field = field;
    }
    
    @Override
    public FeatureAttributeDescriptor getCopy() {
        try {
            return (FeatureAttributeDescriptor) field.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String getDataTypeName() {
        return field.getDataType().getName();
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public int getPrecision() {
        return 0;
    }

    @Override
    public Class getObjectClass() {
        return this.getDataType().getDefaultClass();
    }

    @Override
    public int getMinimumOccurrences() {
        return 0;
    }

    @Override
    public int getMaximumOccurrences() {
        return 0;
    }

    @Override
    public boolean isPrimaryKey() {
        return false;
    }

    @Override
    public boolean allowNull() {
        return true;
    }

    @Override
    public Evaluator getEvaluator() {
        return null;
    }

    @Override
    public IProjection getSRS() {
        return null;
    }

    @Override
    public int getGeometryType() {
        return Geometry.TYPES.GEOMETRY;
    }

    @Override
    public int getGeometrySubType() {
        return Geometry.SUBTYPES.UNKNOWN;
    }

    @Override
    public GeometryType getGeomType() {
        return null;
    }

    @Override
    public DateFormat getDateFormat() {
        return null;
    }

    @Override
    public int getIndex() {
        return -1;
    }

    @Override
    public Object getAdditionalInfo(String infoName) {
        return null;
    }

    @Override
    public boolean isAutomatic() {
        return false;
    }

    @Override
    public boolean isTime() {
        return false;
    }

    @Override
    public boolean isIndexed() {
        return false;
    }

    @Override
    public boolean allowIndexDuplicateds() {
        return false;
    }

    @Override
    public boolean isIndexAscending() {
        return false;
    }

    @Override
    public FeatureAttributeGetter getFeatureAttributeGetter() {
        return null;
    }

    @Override
    public void setFeatureAttributeGetter(FeatureAttributeGetter featureAttributeGetter) {
    }

    @Override
    public FeatureAttributeEmulator getFeatureAttributeEmulator() {
        return null;
    }

    @Override
    public String getName() {
        return this.field.getName();
    }

    @Override
    public int getType() {
        return this.field.getDataType().getType();
    }

    @Override
    public DataType getDataType() {
        return this.field.getDataType();
    }

    @Override
    public String getSubtype() {
        return null;
    }

    @Override
    public String getDescription() {
        return this.field.getDescription();
    }

    @Override
    public Object getDefaultValue() {
        return this.field.getDefaultValue();
    }

    @Override
    public String getGroup() {
        return this.field.getGroup();
    }

    @Override
    public int getOder() {
        return this.field.getOder();
    }

    @Override
    public boolean isMandatory() {
        return this.field.isMandatory();
    }

    @Override
    public boolean isPersistent() {
        return this.field.isPersistent();
    }

    @Override
    public boolean isHidden() {
        return this.field.isHidden();
    }

    @Override
    public boolean isReadOnly() {
        return this.field.isReadOnly();
    }

    @Override
    public boolean isContainer() {
        return this.field.isContainer();
    }

    @Override
    public DynObjectValueItem[] getAvailableValues() {
        return this.field.getAvailableValues();
    }

    @Override
    public Object getMinValue() {
        return this.field.getMinValue();
    }

    @Override
    public Object getMaxValue() {
        return this.field.getMaxValue();
    }

    @Override
    public Class getClassOfValue() {
        return this.field.getClassOfValue();
    }

    @Override
    public Class getClassOfItems() {
        return this.field.getClassOfItems();
    }

    @Override
    public DynField setDescription(String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setType(int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setType(DataType type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setSubtype(String subtype) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setDefaultFieldValue(Object defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setGroup(String groupName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setOrder(int order) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setMandatory(boolean mandatory) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setHidden(boolean hidden) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setPersistent(boolean persistent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setAvailableValues(DynObjectValueItem[] values) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setAvailableValues(List values) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setMinValue(Object minValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setMaxValue(Object maxValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setClassOfValue(Class theClass) throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setClassOfItems(Class theClass) throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setReadOnly(boolean isReadOnly) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField getElementsType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setElementsType(int type) throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setElementsType(DynStruct type) throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validate(Object value) throws DynFieldValidateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object coerce(Object value) throws CoercionException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getTheTypeOfAvailableValues() {
        return this.field.getTheTypeOfAvailableValues();
    }

    @Override
    public DynField setDefaultDynValue(Object defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setTheTypeOfAvailableValues(int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DynFieldFacadeOfAFeatureAttributeDescriptor other = (DynFieldFacadeOfAFeatureAttributeDescriptor) super.clone(); 
        other.field = (DynField) other.field.clone();
        return other;
    }
}
