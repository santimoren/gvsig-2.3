/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.swing.impl.featuretable;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JComponent;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;

import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.JFeatureTable;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Panel to show a table of Feature data.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class FeatureTablePanel extends JPanel implements Observer,JFeatureTable {

    private static final Logger logger = LoggerFactory.getLogger(FeatureTablePanel.class);

    private static final long serialVersionUID = -9199073063283531216L;

    private FeatureTable table = null;
    private JScrollPane jScrollPane = null;
    private JPanel selectionPanel;
    private JLabel selectionLabel;

    public FeatureTablePanel(FeatureTableModel tableModel,
            boolean isDoubleBuffered)  {
        super(isDoubleBuffered);
        table = new FeatureTable(tableModel);
        this.initComonents();
        tableModel.getFeatureStore().addObserver(this);
        tableModel.getFeatureSelection().addObserver(this);
        tableModel.addChangeListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if( FeatureTableModel.CHANGE_SELECTION.equals(e.getActionCommand()) ) {
                    featureSelectionChanged();
                }
            }
        });        
    }
    
    private void featureSelectionChanged() {
        ((FeatureTableModel)table.getModel()).getFeatureSelection().addObserver(this);
    }

    private void initComonents() {
        this.setLayout(new BorderLayout());

        table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);

        jScrollPane = new JScrollPane();
        jScrollPane.setViewportView(table);
        createTheRowHeader();

        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent pce) {
                if( "RowHeight".equalsIgnoreCase(pce.getPropertyName())) {
                    // No he averigado como cambiar el ancho de las lineas
                    // ya creadas de la cabezera de lineas, asi que la
                    // reconstruyo entera cuando algo cambia.
                    createTheRowHeader();
                }
            }
        });

        getFeatureTableModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tme) {
                // No he averigado como cambiar el ancho de las lineas
                // ya creadas de la cabezera de lineas, asi que la
                // reconstruyo entera cuando algo cambia.
                createTheRowHeader();
            }
        });
        selectionLabel = new JLabel();
        selectionLabel.setText( this.getSelectionLabel() );
        selectionPanel = new JPanel();
        selectionPanel.add(selectionLabel, BorderLayout.EAST);
 
        this.add(jScrollPane, BorderLayout.CENTER);
        this.add(selectionPanel, BorderLayout.SOUTH);
    }
    
    public void addColumnSelectionListener(ActionListener listener) {
       this.table.addColumnSelectionListener(listener);
    }
    
    public void fireColumnSelection(ActionEvent e) {
        this.table.fireColumnSelection(e);
    }    

    @Override
    public void setModel(TableModel dataModel) {
        this.table.setModel(dataModel);
    }

    @Override
    public TableModel getModel() {
        return this.table.getModel();
    }    

    @Override
    public FeatureTableModel getFeatureTableModel() {
        return (FeatureTableModel) this.table.getModel();
    }    
    
    @Override
    public int getSelectedColumnCount() {
        return this.table.getSelectedColumnCount();
    }

    @Override
    public int[] getSelectedColumns() {
        return this.table.getSelectedColumns();
    }
    
    private String getSelectionLabel() {
        String label =
        	getFeatureTableModel().getSelectionCount() + 
            " / " +
            getFeatureTableModel().getRowCount() + 
            " " +
            Messages.getText("registros_seleccionados_total") + 
            ".";
        return label;
    }
    
    private void createTheRowHeader() {
        // No se si ha sido paranoia o que, pero parece que si la recreo sin mas
        // a veces parece como si no la cambiase, asi que he probado a encolarlo 
        // en la cola de eventos de swing y parece que siempre funciona.
        //
        // Cuando se estan mostrando las geometrias, que cada linea tiene un ancho
        // distinto, se le llama muchisimas veces;
        // habria que evaluar retenerlas por ejemplo durante un segundo y solo 
        // recrearlo entonces.
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if( table.getRowCount()<1 ) {
                    jScrollPane.setRowHeaderView(null);
                    return;
                }
                ListModel lm = new AbstractListModel() {
                    private static final long serialVersionUID = -310252331647118271L;
                    @Override
                    public int getSize() {
                        return table.getRowCount();
                    }

                    @Override
                    public Object getElementAt(int index) {
                        return String.valueOf(index + 1);
                    }
                };
                final JList rowHeader = new JList(lm);
                rowHeader.setBackground(table.getTableHeader().getBackground());
                rowHeader.setCellRenderer(new RowHeaderRenderer(table,rowHeader));
                jScrollPane.setRowHeaderView(rowHeader);
            }
        });
    }

    @Override
    public JTable getJTable() {
        return this.table;
    }

    @Override
    public JComponent asJComponent() {
        return this;
    }

    private static class RowHeaderRenderer extends JButton implements ListCellRenderer {
        private static final long serialVersionUID = -3811876981965840126L;
        private JTable table = null;
        private final Dimension dimension = new Dimension();
        private final JList rowHeader;
        
        RowHeaderRenderer(JTable table,JList rowHeader) {
            JTableHeader header = table.getTableHeader();
            setOpaque(true);
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(CENTER);
            setForeground(header.getForeground());
            setBackground(header.getBackground());
            setFont(header.getFont());
            this.table = table;
            this.rowHeader = rowHeader;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            setText((value == null) ? "" : value.toString());
            this.setPreferredSize(null); // Fuerza recalcular el tama�o del boton
            this.dimension.height = this.table.getRowHeight(index);
            this.dimension.width = this.getPreferredSize().width+10;
            this.setPreferredSize(this.dimension);
            return this;
        }

    }

    @Override
    public void update(Observable observable, Object notification) {
    	if( observable instanceof FeatureSelection ) {
    		// Ha cambiado la seleccion, simplemente repintamos.
    		repaint();
    		return;
    	}
    	// If selection changes from nothing selected to anything selected
    	// or the reverse, update selection info
    	if( observable instanceof FeatureStore ) {
    		String type = ((DataStoreNotification) notification).getType();
    		if ( DataStoreNotification.SELECTION_CHANGE.equals(type) ) {
    			selectionLabel.setText( this.getSelectionLabel() );
    		}
    	}
    }

    @Override
    public void setCurrent(int index) {
        logger.warn("setCurrent not implemented !!!!!!!!!!!!!!!!!!!!!!!!");
    }

    public void setVisibleStatusbar(boolean visible) {
        this.selectionPanel.setVisible(visible);
    }
    
    public boolean isVisibleStatusbar() {
        return this.selectionPanel.isVisible();
    }
}
