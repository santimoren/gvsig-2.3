package org.gvsig.fmap.dal.swing.impl.queryfilter;

import com.jeta.open.i18n.I18NUtils;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class DefaultQueryFilterExpresionView extends JPanel
{
   JList lstFields = new JList();
   JButton btnEqual = new JButton();
   JButton btnDate = new JButton();
   JButton btnLike = new JButton();
   JButton btnNotEqual = new JButton();
   JButton btnLess = new JButton();
   JButton btnGreat = new JButton();
   JButton btnLessEqual = new JButton();
   JButton btnGreatEqual = new JButton();
   JButton btnAnd = new JButton();
   JButton btnOr = new JButton();
   JButton btnNot = new JButton();
   JButton btnParenthesis = new JButton();
   JButton btnClearText = new JButton();
   JList lstValues = new JList();
   JLabel lblValues = new JLabel();
   JLabel lblLoading = new JLabel();
   JPanel pnlExpresionContainer = new JPanel();
   JLabel lblFields = new JLabel();

   /**
    * Default constructor
    */
   public DefaultQueryFilterExpresionView()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(0.6),FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(0.5),FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:GROW(0.2),CENTER:DEFAULT:NONE,CENTER:DEFAULT:GROW(1.0),CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lstFields.setName("lstFields");
      JScrollPane jscrollpane1 = new JScrollPane();
      jscrollpane1.setViewportView(lstFields);
      jscrollpane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane1,new CellConstraints(2,4,1,1,CellConstraints.FILL,CellConstraints.FILL));

      jpanel1.add(createPanel1(),new CellConstraints(4,4,1,1,CellConstraints.CENTER,CellConstraints.CENTER));
      lstValues.setName("lstValues");
      JScrollPane jscrollpane2 = new JScrollPane();
      jscrollpane2.setViewportView(lstValues);
      jscrollpane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane2,new CellConstraints(6,4,1,1,CellConstraints.FILL,CellConstraints.FILL));

      lblValues.setName("lblValues");
      lblValues.setText("Valores conocidos:");
      jpanel1.add(lblValues,cc.xy(6,2));

      lblLoading.setName("lblLoading");
      jpanel1.add(lblLoading,cc.xy(6,3));

      pnlExpresionContainer.setBackground(new Color(255,255,255));
      pnlExpresionContainer.setName("pnlExpresionContainer");
      jpanel1.add(pnlExpresionContainer,new CellConstraints(2,6,5,1,CellConstraints.FILL,CellConstraints.FILL));

      lblFields.setName("lblFields");
      lblFields.setText("Campos");
      jpanel1.add(lblFields,cc.xy(2,3));

      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5,6,7 },new int[]{ 1,2,3,4,5,6,7 });
      return jpanel1;
   }

   public JPanel createPanel1()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      btnEqual.setActionCommand("eq");
      btnEqual.setName("btnEqual");
      btnEqual.setText("=");
      jpanel1.add(btnEqual,cc.xy(1,1));

      btnDate.setActionCommand("date");
      btnDate.setName("btnDate");
      btnDate.setText("Date");
      jpanel1.add(btnDate,cc.xy(7,1));

      btnLike.setActionCommand("like");
      btnLike.setName("btnLike");
      btnLike.setText("like");
      jpanel1.add(btnLike,cc.xy(5,1));

      btnNotEqual.setActionCommand("neq");
      btnNotEqual.setName("btnNotEqual");
      btnNotEqual.setText("!=");
      jpanel1.add(btnNotEqual,cc.xy(3,1));

      btnLess.setActionCommand("lt");
      btnLess.setName("btnLess");
      btnLess.setText("<");
      jpanel1.add(btnLess,cc.xy(1,3));

      btnGreat.setActionCommand("gt");
      btnGreat.setName("btnGreat");
      btnGreat.setText(">");
      jpanel1.add(btnGreat,cc.xy(3,3));

      btnLessEqual.setActionCommand("le");
      btnLessEqual.setName("btnLessEqual");
      btnLessEqual.setText("<=");
      jpanel1.add(btnLessEqual,cc.xy(5,3));

      btnGreatEqual.setActionCommand("ge");
      btnGreatEqual.setName("btnGreatEqual");
      btnGreatEqual.setText(">=");
      jpanel1.add(btnGreatEqual,cc.xy(7,3));

      btnAnd.setActionCommand("and");
      btnAnd.setName("btnAnd");
      btnAnd.setText("and");
      jpanel1.add(btnAnd,cc.xy(1,5));

      btnOr.setActionCommand("or");
      btnOr.setName("btnOr");
      btnOr.setText("or");
      jpanel1.add(btnOr,cc.xy(3,5));

      btnNot.setActionCommand("not");
      btnNot.setName("btnNot");
      btnNot.setText("not");
      jpanel1.add(btnNot,cc.xy(5,5));

      btnParenthesis.setActionCommand("parenthesis");
      btnParenthesis.setName("btnParenthesis");
      btnParenthesis.setText("()");
      jpanel1.add(btnParenthesis,cc.xy(7,5));

      btnClearText.setActionCommand("Borrar texto");
      btnClearText.setName("btnClearText");
      btnClearText.setText("Borrar texto");
      jpanel1.add(btnClearText,cc.xywh(1,7,7,1));

      addFillComponents(jpanel1,new int[]{ 2,4,6 },new int[]{ 2,4,6 });
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
