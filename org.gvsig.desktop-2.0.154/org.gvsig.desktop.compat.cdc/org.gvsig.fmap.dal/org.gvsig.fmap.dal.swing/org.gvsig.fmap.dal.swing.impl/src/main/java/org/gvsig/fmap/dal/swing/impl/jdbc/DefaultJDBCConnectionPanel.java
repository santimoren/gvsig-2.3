/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gvsig.fmap.dal.swing.impl.jdbc;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataServerExplorerPool;
import org.gvsig.fmap.dal.DataServerExplorerPoolEntry;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.fmap.dal.swing.jdbc.JDBCConnectionPanel;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// org.gvsig.fmap.mapcontrol.dal.jdbc.JDBCConnectionPanel
public class DefaultJDBCConnectionPanel extends DefaultJDBCConnectionPanelView implements JDBCConnectionPanel{

    private static final Logger
 logger = LoggerFactory.getLogger(DefaultJDBCConnectionPanel.class);
    private JDBCServerExplorerParameters forcedParameters;

    @Override
    public JComponent asJComponent() {
        return this;
    }
    
    private static class ServerExplorerParametersComboItem {

        private JDBCServerExplorerParameters params;
        private String label;

        public ServerExplorerParametersComboItem(String label, JDBCServerExplorerParameters params) {
            this.params = params;
            this.label = label;
        }

        public ServerExplorerParametersComboItem(JDBCServerExplorerParameters params) {
            this(params.getExplorerName(), params);
        }

        @Override
        public String toString() {
            return this.label;
        }

        public JDBCServerExplorerParameters getParams() {
            return this.params;
        }

        public String getLabel() {
            return this.label;
        }
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public DefaultJDBCConnectionPanel() {
        initComponents();
    }

    protected void initComponents() {
        this.cboConnections.setEditable(true);
        this.cboConnections.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                onChangeConnection();
            }
        });
        this.cboConnectors.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                onChangeConnector();
            }
        });
        try {
            fillConnections();
            fillConnectors();
        } catch(Throwable th) {
            // Ignore it to allow use in GUI builders
        }
        this.translate();
        adjustPreferedHeight(this.lblFoother,300);
    }

    private void adjustPreferedHeight(JLabel label, int prefWidth) {
        JLabel resizer = new JLabel();
        resizer.setText(label.getText());
        View view = (View) resizer.getClientProperty(BasicHTML.propertyKey);
        view.setSize(prefWidth,15);
        float w = view.getPreferredSpan(View.X_AXIS);
        float h = view.getPreferredSpan(View.Y_AXIS);
        Dimension dim = new Dimension(
                (int) Math.ceil(w),
                (int) Math.ceil(h)
        );
        label.setPreferredSize(dim);
    }
    
    private void translate() {
        I18nManager i18nManager = ToolsLocator.getI18nManager();
        
        this.lblConnectionName.setText(i18nManager.getTranslation("_Connection_name"));
        this.lblConnector.setText(i18nManager.getTranslation("_Driver_type"));
        this.lblServer.setText(i18nManager.getTranslation("_Host"));
        this.lblPort.setText(i18nManager.getTranslation("_Port"));
        this.lblDataBase.setText(i18nManager.getTranslation("_Database"));
        this.lblUsername.setText(i18nManager.getTranslation("_User"));
        this.lblPassword.setText(i18nManager.getTranslation("_Password"));
        this.lblFoother.setText("<html>"+i18nManager.getTranslation("_JDBCConecctionPanel_foother")+"</html>");
    }
    
    @Override
    public void setServerExplorerParameters(JDBCServerExplorerParameters parameters) {
        this.forcedParameters = (JDBCServerExplorerParameters) parameters.getCopy();
        
        int indexConnector = this.getIndexOfConnector(parameters);
        if ( indexConnector >= 0 && this.cboConnectors.getSelectedIndex()!=indexConnector ) {
            this.cboConnectors.setSelectedIndex(indexConnector);
        }

        this.txtServer.setText(parameters.getHost());
        Integer port = parameters.getPort();
        if ( port == null ) {
            this.txtPort.setText("");
        } else {
            this.txtPort.setText(String.valueOf(port));
        }
        this.txtDataBase.setText(parameters.getDBName());
        this.txtUsername.setText(parameters.getUser());
        this.txtPassword.setText(parameters.getPassword());
    }

    @Override
    public JDBCServerExplorerParameters getServerExplorerParameters() {
        JDBCServerExplorerParameters params;
        JDBCServerExplorerParameters connector = this.getConnector();
        if( this.forcedParameters==null ) {
            params = (JDBCServerExplorerParameters) connector.getCopy();
        } else {
            params = (JDBCServerExplorerParameters) this.forcedParameters.getCopy();
        }
        String s = this.getServer();
        if( s!=null ) {
            params.setHost(s);
        }
        int n = this.getPort();
        if( n>0 ) {
            params.setPort(n);
        }
        s = this.getDataBaseName();
        if( s!=null ) {
            params.setDBName(s);
        }
        s = this.getUsername();
        if( s!=null ) {
            params.setUser(s);
        }
        s = this.getPassword();
        if( s!=null ) {
            params.setPassword(s);
        }

        if ( this.getConnectionName() != null ) {
            DataManager dataManager = DALLocator.getDataManager();
            DataServerExplorerPool pool = dataManager.getDataServerExplorerPool();
            pool.add(this.getConnectionName(), params);
        }
        return params;
    }

    protected void setConnectionName(String connectionName) {
        JTextField txtConnections = (JTextField) this.cboConnections.getEditor().getEditorComponent();
        txtConnections.setText(connectionName);
    }

    @Override
    public String getConnectionName() {
        JTextField txtConnections = (JTextField) this.cboConnections.getEditor().getEditorComponent();
        String value = txtConnections.getText();
        return (String) StringUtils.defaultIfBlank(value, null);
    }

    protected JDBCServerExplorerParameters getConnector() {
        ServerExplorerParametersComboItem item = (ServerExplorerParametersComboItem) this.cboConnectors.getSelectedItem();
        JDBCServerExplorerParameters value = item.getParams();
        return value;
    }

    protected String getConnectorName() {
        JDBCServerExplorerParameters value = this.getConnector();
        if ( value == null ) {
            return null;
        }
        return (String) StringUtils.defaultIfBlank(value.getExplorerName(), null);
    }

    protected String getServer() {
        return (String) StringUtils.defaultIfBlank(this.txtServer.getText(), null);
    }

    protected int getPort() {
        String svalue = (String) StringUtils.defaultIfBlank(this.txtPort.getText(), null);
        int ivalue;
        try {
            ivalue = Integer.parseInt(svalue);
        } catch (Exception ex) {
            ivalue = -1;
        }
        return ivalue;
    }

    protected String getDataBaseName() {
        return (String) StringUtils.defaultIfBlank(this.txtDataBase.getText(), null);
    }

    protected String getUsername() {
        return (String) StringUtils.defaultIfBlank(this.txtUsername.getText(), null);
    }

    protected String getPassword() {
        return (String) StringUtils.defaultIfBlank(this.txtPassword.getText(), null);
    }

    private void onChangeConnector() {
        ServerExplorerParametersComboItem item = (ServerExplorerParametersComboItem) this.cboConnectors.getSelectedItem();
        if( item==null ) {
            return;
        }
        JDBCServerExplorerParameters connector = item.getParams();
        
        if ( connector == null ) {
            return;
        }
        this.setServerExplorerParameters(connector);
    }

    private void onChangeConnection() {
        Object item = this.cboConnections.getSelectedItem();
        if ( item instanceof ServerExplorerParametersComboItem ) {
            JDBCServerExplorerParameters connection = ((ServerExplorerParametersComboItem) item).getParams();
            if ( connection == null ) {
                return;
            }
            this.setServerExplorerParameters(connection);
        }
    }
    

    private int getIndexOfConnector(JDBCServerExplorerParameters explorerParameters) {
        String code = null;
        try {
            code = explorerParameters.toString();
            ComboBoxModel model = this.cboConnectors.getModel();
            for ( int i = 0; i < model.getSize(); i++ ) {
                ServerExplorerParametersComboItem x = (ServerExplorerParametersComboItem) model.getElementAt(i);
                if ( x.getLabel().equalsIgnoreCase(explorerParameters.getExplorerName()) ) {
                    return i;
                }
            }
        } catch (Exception ex) {
            logger.warn("Can't get index of exporer parameter '" + code + "'.", ex);
        }
        return -1;
    }

    private void fillConnectors() {
        DataManager dataManager = DALLocator.getDataManager();
        List  explorers = dataManager.getExplorerProviders();

        DataServerExplorerParameters params;

        JDBCServerExplorerParameters last = null;
        Iterator it = explorers.iterator();
        while ( it.hasNext() ) {
            String explorerName = (String) it.next();
            try {
                params = dataManager.createServerExplorerParameters(explorerName);
            } catch (DataException e) {
                continue;
            }
            if ( params instanceof JDBCServerExplorerParameters ) {
                JDBCServerExplorerParameters dbParams = (JDBCServerExplorerParameters) params;
                if( dbParams.getClass().getName().equals(JDBCServerExplorerParameters.class.getName()) ) {
                    last = dbParams;
                } else {
                    this.cboConnectors.addItem(
                            new ServerExplorerParametersComboItem(dbParams)
                    );
                }
            }
        }
        if( last!=null ) {
            this.cboConnectors.addItem(
                    new ServerExplorerParametersComboItem(last)
            );
        }
    }

    private void fillConnections() {
        DataManager dataManager = DALLocator.getDataManager();
        DataServerExplorerPool pool = dataManager.getDataServerExplorerPool();

        DataServerExplorerParameters params;

        Iterator it = pool.iterator();
        while ( it.hasNext() ) {
            DataServerExplorerPoolEntry entry = (DataServerExplorerPoolEntry) it.next();
            if ( entry.getExplorerParameters() instanceof JDBCServerExplorerParameters ) {
                JDBCServerExplorerParameters dbParams = (JDBCServerExplorerParameters) entry.getExplorerParameters();
                this.cboConnections.addItem(
                        new ServerExplorerParametersComboItem(entry.getName(), dbParams)
                );
            }
        }
        this.cboConnections.setSelectedIndex(-1);
    }

    @Override
    public void delete() {
        String name = this.getConnectionName();
        DataManager dataManager = DALLocator.getDataManager();
        DataServerExplorerPool pool = dataManager.getDataServerExplorerPool();
        
        pool.remove(name);
    }
    
    @Override
    public void clear() {
        this.cboConnections.setSelectedIndex(-1);
        this.cboConnectors.setSelectedIndex(-1);
        this.txtServer.setText("");
        this.txtPort.setText("");
        this.txtDataBase.setText("");
        this.txtUsername.setText("");
        this.txtPassword.setText("");
    }
}
