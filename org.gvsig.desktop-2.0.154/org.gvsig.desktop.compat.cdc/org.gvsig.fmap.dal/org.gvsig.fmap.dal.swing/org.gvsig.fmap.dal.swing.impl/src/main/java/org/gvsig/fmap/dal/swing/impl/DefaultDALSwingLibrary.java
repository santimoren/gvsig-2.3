/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.swing.impl;

import org.gvsig.featureform.swing.impl.dynformfield.JDynFormFieldFeatureLinkFactory;
import org.gvsig.featureform.swing.impl.dynformfield.JDynFormFieldFeaturesTableLinkFactory;
import org.gvsig.fmap.dal.impl.DefaultEditingNotificationManager;
import org.gvsig.fmap.dal.swing.DALSwingLibrary;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.impl.dynobjectutils.CreateComboModelFromTable;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.spi.DynFormSPILocator;
import org.gvsig.tools.dynform.spi.DynFormSPIManager;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;


/**
 * @author fdiaz
 *
 */
public class DefaultDALSwingLibrary  extends AbstractLibrary{



    @Override
    public void doRegistration() {
        registerAsImplementationOf(DALSwingLibrary.class);
    }


    @Override
    protected void doInitialize() throws LibraryException {
        DALSwingLocator.registerSwingManager(DefaultDataSwingManager.class);
        DALSwingLocator.registerEditingNotificationManager(DefaultEditingNotificationManager.class);
    }


    @Override
    protected void doPostInitialize() throws LibraryException {
        DynFormSPIManager manager = DynFormSPILocator.getDynFormSPIManager();
        if( manager != null ) {
                manager.addServiceFactory(new JDynFormFieldFeatureLinkFactory());
                manager.addServiceFactory(new JDynFormFieldFeaturesTableLinkFactory());
        }
        DynObjectManager dynObjectmanager = ToolsLocator.getDynObjectManager();
        dynObjectmanager.registerDynMethod(new CreateComboModelFromTable("DAL.createComboModelFromTable", "XXXXXXXXXXXXXXXXXXXX."));
                
    }

}
