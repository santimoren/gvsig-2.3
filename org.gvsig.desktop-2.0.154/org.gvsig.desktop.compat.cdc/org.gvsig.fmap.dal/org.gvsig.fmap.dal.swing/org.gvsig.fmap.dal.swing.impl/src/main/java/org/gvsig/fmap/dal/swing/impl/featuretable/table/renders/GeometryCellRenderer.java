/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2008 {DiSiD Technologies}  {Create a JTable TableModel for a FeatureCollection}
 */
package org.gvsig.fmap.dal.swing.impl.featuretable.table.renders;

import javax.swing.table.DefaultTableCellRenderer;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * Simple renderer for cells of type {@link Geometry} .
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class GeometryCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -2470239399517077869L;

    protected void setValue(Object value) {
        if (value == null) {
            setText("");
        } else {
            Geometry geometry = (Geometry) value;
            try {
                String geomTxt = geometry.convertToWKT();
                setText(geomTxt);
            } catch (GeometryOperationNotSupportedException e) {
                throw new RuntimeException(
                    "Error getting as WKT the geometry: " + geometry, e);
            } catch (GeometryOperationException e) {
                throw new RuntimeException(
                    "Error getting as WKT the geometry: " + geometry, e);
            }
            setToolTipText(geometry.getGeometryType().getName());
        }
    }
}