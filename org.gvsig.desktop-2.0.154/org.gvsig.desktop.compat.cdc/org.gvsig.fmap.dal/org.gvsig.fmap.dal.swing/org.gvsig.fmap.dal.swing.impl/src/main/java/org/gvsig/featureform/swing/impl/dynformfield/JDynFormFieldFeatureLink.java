/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing.impl.dynformfield;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Formatter;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.DataSwingManager;

import org.gvsig.tools.dynform.JDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.AbstractJDynFormField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.spi.ServiceManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;

public class JDynFormFieldFeatureLink extends AbstractJDynFormField implements JDynFormField {

    private static final String DAL_code = "DAL.code";
    private static final String DAL_foreingTable = "DAL.foreingTable";
    private static final String DAL_foreingCode = "DAL.foreingCode";
    private static final String DAL_foreingDescriptionMask = "DAL.foreingDescriptionMask";
    private static final String DAL_foreingDescriptionFields = "DAL.foreingDescriptionFields";
    
    private DynObject assignedValue = null;
    private DynObject value = null;
//    private JTextField txtCode = null;
    private JTextField txtDescription = null;
    private JButton btnLink = null;
    private JButton btnUnlink = null;
    private JButton btnEdit = null;

    private String codeFieldName = null;
    private String foreingTableName;
    private String foreingCodeName;
    private String foreingDescriptionMask = null;
    private String foreingDescriptionFieldNames[] = null;

    /**
     *
     * @param parameters
     * @param serviceManager
     */
    public JDynFormFieldFeatureLink(DynObject parameters,
            ServiceManager serviceManager) {
        super(parameters, serviceManager);
        if (this.getParameterValue() != null) {
            this.assignedValue = (DynObject) this.getParameterValue();
        }
    }

    @Override
    public Object getAssignedValue() {
        return this.assignedValue;
    }

    @Override
    public void initComponent() {
        this.foreingDescriptionFieldNames = null;
        this.codeFieldName = getTagValueAsString(DAL_code, null);
        this.foreingTableName = getTagValueAsString(DAL_foreingTable, null);
        this.foreingCodeName = getTagValueAsString(DAL_foreingCode, null);

        this.foreingDescriptionMask = getTagValueAsString(DAL_foreingDescriptionMask, null);
        String fieldNames = getTagValueAsString(DAL_foreingDescriptionFields, null);
        if (!StringUtils.isEmpty(fieldNames)) {
            this.foreingDescriptionFieldNames = fieldNames.split(",");
        }
        
        
        if (StringUtils.isEmpty(this.foreingDescriptionMask)
                && this.foreingDescriptionFieldNames != null
                && this.foreingDescriptionFieldNames.length == 1) {
            this.foreingDescriptionMask = "%s";
        }
//        this.txtCode = new JTextField();
        this.txtDescription = new JTextField();
        this.btnLink = createButton("Select item to link", "link.png");
        this.btnUnlink = createButton("Remove link", "unlink.png");
        this.btnEdit = createButton("View linked item", "edit.png");

        this.btnLink.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doLink();
            }
        });
        this.btnUnlink.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doUnlink();
            }
        });
        this.btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                doEdit();
            }
        });

//        this.txtCode.setText("          ");
//        this.txtDescforeingCodeNameription.setText("                    ");
//        this.txtCode.setEditable(false);
        this.txtDescription.setEditable(false);

        if (StringUtils.isEmpty(codeFieldName) || StringUtils.isEmpty(foreingDescriptionMask)
                || StringUtils.isEmpty(foreingTableName) || StringUtils.isEmpty(foreingCodeName)
                || this.foreingDescriptionFieldNames == null) {
            this.btnEdit.setEnabled(false);
            this.btnLink.setEnabled(false);
            this.btnUnlink.setEnabled(false);
        }

        this.contents = new JPanel();
        this.contents.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
//        c.fill = GridBagConstraints.NONE;
//        c.ipadx = 4;
//        c.ipady = 1;
//        c.gridx = 0;
//        c.gridy = 0;
//        c.weightx = 0;
//        this.contents.add(this.txtCode, c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx = 4;
        c.ipady = 1;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        this.contents.add(this.txtDescription, c);
        c.fill = GridBagConstraints.NONE;
        c.ipadx = 4;
        c.ipady = 1;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0;
        this.contents.add(this.btnLink, c);
        c.fill = GridBagConstraints.NONE;
        c.ipadx = 4;
        c.ipady = 1;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 0;
        this.contents.add(this.btnUnlink, c);
        c.fill = GridBagConstraints.NONE;
        c.ipadx = 4;
        c.ipady = 1;
        c.gridx = 4;
        c.gridy = 0;
        c.weightx = 0;
        this.contents.add(this.btnEdit, c);

        this.setValue(this.assignedValue);
    }

    private JButton createButton(final String tip, final String image) {
        URL url = this.getClass().getClassLoader().getResource("org/gvsig/featureform/swing/impl/" + image);
        Icon icon = new ImageIcon(url);
        JButton button = new JButton(icon);
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        button.setToolTipText(tip);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        return button;
    }

    private void doLink() {

    }

    private void doUnlink() {
        this.clear();
    }

    private void doEdit() {
        try {
            DataManager dalmanager = DALLocator.getDataManager();
            DataSwingManager daluimanager = DALSwingLocator.getSwingManager();
            Feature feature = ((FacadeOfAFeature)(this.value)).getFeature();
            FeatureStore store = feature.getStore();
            FeatureQuery query = store.createFeatureQuery();
            query.addFilter(dalmanager.createExpresion(foreingCodeName + " = " + feature.getString(foreingCodeName)));
            JFeaturesForm form = daluimanager.createJFeaturesForm(store);
            form.setQuery(query);
            form.showForm(WindowManager.MODE.WINDOW);
        } catch (Exception ex) {
            logger.warn("Can't show linked form",ex);
        }
    }

    private String getDescription() {
        if (StringUtils.isEmpty(foreingDescriptionMask) || this.foreingDescriptionFieldNames == null) {
            return "";
        }
        Object[] values = new Object[this.foreingDescriptionFieldNames.length];
        for( int i=0; i<values.length; i++ ) {
            values[i] = this.value.getDynValue(this.foreingDescriptionFieldNames[i]);
        }
        Formatter f = new Formatter();
        try {
            String description = f.format(this.foreingDescriptionMask, values).toString();
            return description;
        } catch (Exception ex) {
            // TODO: log error
            return "";
        }
    }

    @Override
    public void setReadOnly(boolean readonly) {
        boolean enabled = !readonly;
//        this.txtCode.setEnabled(enabled);
        if( this.txtDescription == null ) {
            return;
        }
        this.txtDescription.setEnabled(enabled);
        this.btnEdit.setEnabled(enabled);
        this.btnLink.setEnabled(enabled);
        this.btnUnlink.setEnabled(enabled);
    }

    @Override
    public void setValue(Object value) {
        if (value == null) {
            this.clear();
            return;
        }
        this.value = (DynObject) value;
//        this.txtCode.setText("");
        this.txtDescription.setText("");
        if (StringUtils.isEmpty(foreingCodeName) || StringUtils.isEmpty(foreingDescriptionMask)
                || this.foreingDescriptionFieldNames == null) {
        }
//        Object x = this.value.getDynValue(foreingCodeName);
//        if (x != null) {
//            this.txtCode.setText(x.toString());
//        }
        String description = this.getDescription();
        if (description != null) {
            this.txtDescription.setText(description);
        }
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void fetch(DynObject container) {
        if (StringUtils.isEmpty(this.codeFieldName)) {
            return;
        }
        DynObject value = (DynObject) this.getValue();
        if (value == null) {
            container.setDynValue(this.codeFieldName, null);
        } else {
            if (StringUtils.isEmpty(this.foreingCodeName)) {
                return;
            }
            container.setDynValue(this.codeFieldName, value.getDynValue(this.foreingCodeName));
        }
    }

    @Override
    public boolean hasValidValue() {
        return true;
    }

    @Override
    public void clear() {
        this.value = null;
//        this.txtCode.setText("");
        this.txtDescription.setText("");
    }
}
