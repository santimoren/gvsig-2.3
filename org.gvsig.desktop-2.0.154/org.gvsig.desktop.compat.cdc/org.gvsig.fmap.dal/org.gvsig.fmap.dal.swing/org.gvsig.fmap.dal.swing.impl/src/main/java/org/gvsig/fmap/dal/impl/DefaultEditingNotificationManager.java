/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.impl;

import java.util.Arrays;
import java.util.List;
import javax.swing.SwingUtilities;
import org.gvsig.featureform.swing.JFeatureForm;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.EditingNotification;
import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.observer.ObservableHelper;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.BaseNotification;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.windowmanager.Dialog;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager_v2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Acts as a hub to centralize editing notification events in a single manager.
 *
 * All objects that modify a store notify this manager to reported the actions
 * to the objects interested in knowing when start or end editing, regardless
 * of where the editing work is done.
 *
 */
public class DefaultEditingNotificationManager implements EditingNotificationManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultEditingNotificationManager.class);
            
    private ObservableHelper helper = null;

    private static final int SOURCE = 1;
    private static final int DOCUMENT = 2;
    private static final int AUXDATA = 3;
    private static final int STORE = 4;
    private static final int FEATURE = 5;
    private static final int FEATURETYPE = 6;
    
    private static List cancelableNotifications = null;



    public class DefaultEditingNotification extends BaseNotification implements EditingNotification {

        private boolean validateTheFeature = true;
        
        DefaultEditingNotification(String type) {
            super(type, 7);
        }

        @Override
        public Object getSource() {
            return this.getValue(SOURCE);
        }

        @Override
        public Object getDocument() {
            return this.getValue(DOCUMENT);
        }

        public Object getAuxData() {
            return this.getValue(AUXDATA);
        }

        @Override
        public DataStore getStore() {
            return (DataStore) this.getValue(STORE);
        }

        @Override
        public FeatureStore getFeatureStore() {
            return (FeatureStore) this.getValue(STORE);
        }

        @Override
        public Feature getFeature() {
            return (Feature) this.getValue(FEATURE);
        }

        @Override
        public EditableFeatureType getFeatureType() {
            return (EditableFeatureType) this.getValue(FEATURETYPE);
        }

        @Override
        public boolean isCancelable() {
            if( cancelableNotifications==null ) {
                String nn[] = new String[] {
                    BEFORE_ENTER_EDITING_STORE,
                    BEFORE_EXIT_EDITING_STORE,
                    BEFORE_INSERT_FEATURE,
                    BEFORE_REMOVE_FEATURE,
                    BEFORE_UPDATE_FEATURE,
                    BEFORE_UPDATE_FEATURE_TYPE
                };
                cancelableNotifications = Arrays.asList(nn);
            }
            return cancelableNotifications.contains(this.getType() );
        }

        @Override
        public void setSkipFeatureValidation(boolean skipTheFeatureValidation) {
            this.validateTheFeature = !skipTheFeatureValidation;
        }

        @Override
        public boolean shouldValidateTheFeature() {
            return this.validateTheFeature;
        }
    }

    public DefaultEditingNotificationManager() {
        this.helper = new ObservableHelper();
    }

    @Override
    public void addObserver(Observer o) {
        this.helper.addObserver(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        this.helper.deleteObserver(o);
    }

    @Override
    public void deleteObservers() {
        this.helper.deleteObservers();
    }

    @Override
    public EditingNotification notifyObservers(EditingNotification notification) {
        try {
            this.helper.notifyObservers(this, notification);
        } catch(Exception ex) {
            logger.warn("Problems notifing to observers of DefaultEditingNotificationManager.",ex);
        }
        return notification;
    }

    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata, DataStore store, Feature feature, EditableFeatureType featureType) {
        EditingNotification notification = new DefaultEditingNotification(type);
        notification.setValue(SOURCE,source);
        notification.setValue(DOCUMENT,document);
        notification.setValue(AUXDATA,auxdata);
        notification.setValue(STORE,store);
        notification.setValue(FEATURE,feature);
        notification.setValue(FEATURETYPE,featureType);
        return this.notifyObservers(notification);
    }

    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata, DataStore store) {
        return this.notifyObservers(source, type, document, auxdata, store, null, null);
    }

    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata) {
        return this.notifyObservers(source, type, document, auxdata, null, null, null);
    }

    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store) {
        return this.notifyObservers(source, type, document, null, store, null, null);
    }

    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store, Feature feature) {
        return this.notifyObservers(source, type, document, null, store, feature, null);
    }

    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store, EditableFeatureType featureType) {
        return this.notifyObservers(source, type, document, null, store, null, featureType);
    }
    
    @Override
    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata, DataStore store, Feature feature) {
        return this.notifyObservers(source, type, document, auxdata, store, feature, null);
    }

    @Override
    public boolean validateFeature(Feature feature) {
        while( true ) {
            if( !needAskUser(feature) ) {
                // La feature se puede validar y no precisa de intervencion del
                // usuario. Retornamos true.
                return true;
            }
            // No se habia podido validar la feature, se piden al
            // usuario los datos que faltan.
            AskRequiredAttributtes ask = new AskRequiredAttributtes();
            ask.showDialog(feature);
            if( ask.userCancel() ) {
                // No se habia podido validar la feature, se le han pedido al
                // usuario los datos que faltan y este ha pulsado en cancel,
                // asi que la feature no se puede validar, y retornamos
                // false.
                return false;
            }
        }
    }
    
    private boolean needAskUser(Feature feature) {
        FeatureType featureType = feature.getType();
        FeatureAttributeDescriptor[] attributeDescriptors = featureType.getAttributeDescriptors();

        for (FeatureAttributeDescriptor attrDesc : attributeDescriptors) {
            if ( attrDesc.isAutomatic() ) {
                break;
            }
            if ( (attrDesc.isPrimaryKey() || !attrDesc.allowNull())
                    && feature.get(attrDesc.getName()) == null ) {
                return true;
            }
        }
        return false;
    }

    private class AskRequiredAttributtes {

        private boolean userCancelValue = true;

        public boolean userCancel() {            
            return this.userCancelValue;
        }
        
        public void showDialog(final Feature feature) {
            if ( !SwingUtilities.isEventDispatchThread() ) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            showDialog(feature);
                        }
                    }
                    );
                } catch (Exception e1) {
                    message("Can't show form to fill need data.",e1);
                    this.userCancelValue = true;
                    return;
                }
            }

            try {
                JFeatureForm form = DALSwingLocator.getSwingManager().createJFeatureForm(feature);
                WindowManager_v2 winManager = (WindowManager_v2) ToolsSwingLocator.getWindowManager();
                Dialog dialog = winManager.createDialog(
                        form.asJComponent(),
                        form.getDynForm().getDefinition().getLabel(),
                        ToolsLocator.getI18nManager().getTranslation("_Fill_the_required_fields"), 
                        WindowManager_v2.BUTTONS_OK_CANCEL
                );
                dialog.show(WindowManager.MODE.DIALOG);
                if( dialog.getAction() == WindowManager_v2.BUTTON_OK ) {
                    this.userCancelValue = true;
                } else {
                    form.fetch((EditableFeature) feature);
                    this.userCancelValue = false;
                }
                
            } catch (Exception ex) {
                message("Can't show form to fill need data.",ex);
                this.userCancelValue = true;
            }
        }
        
        private void message(String msg, Throwable ex) {
            if( ex==null ) {
                logger.warn(msg);
            } else {
                logger.warn(msg,ex);
            }
//            msg = msg + "\nSee to application log for more information.";
//            ApplicationManager application = ApplicationLocator.getManager();
//            application.message(msg,JOptionPane.WARNING_MESSAGE);
        }
    }

    
    
}
