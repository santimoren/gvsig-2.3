package org.gvsig.featureform.swing.impl.dynformset;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.DynFormDefinition;
import org.gvsig.tools.dynform.spi.DynFormSPIManager;
import org.gvsig.tools.dynform.spi.JDynFormSetFactory;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.service.Service;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ServiceManager;

public class FeatureTableJDynFormSetFactory implements JDynFormSetFactory {

    public static final String NAME = "DAL.SimpleFeatureTable";
    private DynStruct parametersDefinition = null;

    public String getName() {
        return NAME;
    }

    public Service create(DynObject parameters, ServiceManager serviceManager) throws ServiceException {
        return new FeatureTableJDynFormSet(
                serviceManager,
                (DynFormDefinition) parameters.getDynValue("definition")
        );
    }

    public DynObject createParameters() {
        return ToolsLocator.getDynObjectManager().createDynObject(parametersDefinition);
    }

    public void initialize() {
        if ( this.parametersDefinition == null ) {
            String serviceName = this.getName();
            DynObjectManager manager = ToolsLocator.getDynObjectManager();
            this.parametersDefinition = manager.createDynClass(
                    serviceName, "Parameters definition for Subform JDynFormSet");
            this.parametersDefinition.addDynFieldObject(DynFormSPIManager.FIELD_FORMSETDEFINITION)
                    .setClassOfValue(DynFormDefinition.class).setMandatory(true);
        }
    }

}
