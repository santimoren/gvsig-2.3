/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing.impl.dynformfield;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JPanel;
import org.gvsig.fmap.dal.feature.paging.FacadeOfAFeaturePagingHelper;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.JFeatureTable;

import org.gvsig.tools.dynform.JDynFormField;
import org.gvsig.tools.dynform.spi.dynformfield.AbstractJDynFormField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.spi.ServiceManager;

public class JDynFormFieldFeaturesTableLink extends AbstractJDynFormField implements JDynFormField {

    private FacadeOfAFeaturePagingHelper assignedValue = null;
    private FacadeOfAFeaturePagingHelper value = null;
    private FeatureTableModel featureTableMode;
    private JFeatureTable featureTable;

    /**
     *
     * @param parameters
     * @param serviceManager
     */
    public JDynFormFieldFeaturesTableLink(DynObject parameters,
            ServiceManager serviceManager) {
        super(parameters, serviceManager);
        if (this.getParameterValue() != null) {
            this.assignedValue = (FacadeOfAFeaturePagingHelper) this.getParameterValue();
        }
    }

    @Override
    public Object getAssignedValue() {
        return this.assignedValue;
    }

    @Override
    public void initComponent() {
        this.contents = new JPanel();
        JPanel panel = (JPanel) this.contents;
        panel.setLayout(new BorderLayout());
        if( assignedValue != null ) {
            FeaturePagingHelper featurePager = ((FacadeOfAFeaturePagingHelper)assignedValue).getFeaturePagingHelper();
            this.featureTableMode = DALSwingLocator.getSwingManager().createFeatureTableModel(featurePager);
            this.featureTable = DALSwingLocator.getSwingManager().createJFeatureTable(featureTableMode);
            int height = this.getTagValueAsInt("DAL.ui.height", -1);
            if( height > 0 ) {
                Dimension dim = this.featureTable.asJComponent().getPreferredSize();
                dim.height = height;
                this.featureTable.asJComponent().setPreferredSize(dim);
            }
            panel.add(this.featureTable.asJComponent(), BorderLayout.CENTER);
        }
        this.setValue(this.assignedValue);
    }

    @Override
    public void setValue(Object value) {
        this.clear();
        if( value == null ) {
            return;
        }
        this.value = (FacadeOfAFeaturePagingHelper) value;
        JPanel panel = (JPanel) this.contents;
        FeaturePagingHelper featurePager = ((FacadeOfAFeaturePagingHelper)value).getFeaturePagingHelper();
        this.featureTableMode = DALSwingLocator.getSwingManager().createFeatureTableModel(featurePager);
        this.featureTable = DALSwingLocator.getSwingManager().createJFeatureTable(featureTableMode);
        int height = this.getTagValueAsInt("DAL.ui.height", -1);
        if( height > 0 ) {
            Dimension dim = this.featureTable.asJComponent().getPreferredSize();
            dim.height = height;
            this.featureTable.asJComponent().setPreferredSize(dim);
        }
        panel.add(this.featureTable.asJComponent(), BorderLayout.CENTER);
}

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void fetch(DynObject container) {
    }

    @Override
    public boolean hasValidValue() {
        return true;
    }

    @Override
    public void clear() {
        this.value = null;
        this.featureTableMode = null;
        this.featureTable = null;
        JPanel panel = (JPanel) this.contents;
        panel.removeAll();
    }
}
