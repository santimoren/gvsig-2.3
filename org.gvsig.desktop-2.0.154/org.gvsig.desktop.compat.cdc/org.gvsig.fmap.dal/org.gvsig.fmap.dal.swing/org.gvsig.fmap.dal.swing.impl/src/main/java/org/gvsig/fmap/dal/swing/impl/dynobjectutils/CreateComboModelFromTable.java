package org.gvsig.fmap.dal.swing.impl.dynobjectutils;

import java.util.Formatter;

import javax.swing.DefaultComboBoxModel;

import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.AbstractDynMethod;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.Tags;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateComboModelFromTable extends AbstractDynMethod {
	
	public static final Logger logger = LoggerFactory.getLogger(CreateComboModelFromTable.class);

    private static final String DAL_foreingCode = "DAL.foreingCode";
    private static final String DAL_foreingDescriptionMask = "DAL.foreingDescriptionMask";
    private static final String DAL_foreingDescriptionFields = "DAL.foreingDescriptionFields";
    private static final String DAL_pageSize = "DAL.pageSize";

    public CreateComboModelFromTable(String methodName, String description) {
        super(methodName, description);
    }

    public Object invoke(DynObject self, Object[] args) throws DynMethodException {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        try {
            DynField_v2 field = (DynField_v2) args[0];
            String foreignCodeName = (String) field.getTags().get(DAL_foreingCode);
            int pageSize;
            try {
                pageSize = field.getTags().getInt(DAL_pageSize);
            } catch (CoercionException ex) {
                pageSize = 100;
            }
            if (pageSize < 100) {
                pageSize = 100;
            }
            Tags tags = field.getTags();
            String[] foreingDescriptionFieldNames = null;
            String foreingDescriptionMask = (String) tags.get(DAL_foreingDescriptionMask);
            String fieldNames = (String) tags.get(DAL_foreingDescriptionFields);
            if (!StringUtils.isEmpty(fieldNames)) {
                foreingDescriptionFieldNames = fieldNames.split(",");
            }
            if (StringUtils.isEmpty(foreingDescriptionMask)
                    && foreingDescriptionFieldNames != null
                    && foreingDescriptionFieldNames.length == 1) {
                foreingDescriptionMask = "%s";
            }

            DataManager manager = DALLocator.getDataManager();
            DataStoreParameters params = manager.createStoreParameters(tags);
            FeatureStore store = (FeatureStore) manager.openStore((String) tags.get("dal.openstore.provider"), params);
            FeatureSet set = store.getFeatureSet();
            DisposableIterator it = set.fastIterator();
            while (it.hasNext()) {
                Feature f = (Feature) it.next();
                DynObjectValueItem item = new DynObjectValueItem(
                        f.get(foreignCodeName),
                        this.getDescription(f, foreingDescriptionFieldNames, foreingDescriptionMask)
                );
                model.addElement(item);
            }
        } catch (Exception ex) {
        	logger.debug("Error while creating ComboModelFromTable:", ex);
        }
        return model;
    }

    private String getDescription(Feature feature, String[] foreingDescriptionFieldNames, String foreingDescriptionMask) {
        if (StringUtils.isEmpty(foreingDescriptionMask) || foreingDescriptionFieldNames == null) {
            return "";
        }
        Object[] values = new Object[foreingDescriptionFieldNames.length];
        for (int i = 0; i < values.length; i++) {
            values[i] = feature.get(foreingDescriptionFieldNames[i]);
        }
        Formatter f = new Formatter();
        try {
            String description = f.format(foreingDescriptionMask, values).toString();
            return description;
        } catch (Exception ex) {
            // TODO: log error
            return "";
        }
    }

}
