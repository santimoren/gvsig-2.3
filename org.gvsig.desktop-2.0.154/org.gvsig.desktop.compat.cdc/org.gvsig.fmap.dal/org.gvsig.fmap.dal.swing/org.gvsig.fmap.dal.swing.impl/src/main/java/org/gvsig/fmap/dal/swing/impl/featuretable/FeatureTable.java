/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.swing.impl.featuretable;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.DefaultFeatureTableModel;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.editors.FormattedCellEditor;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.editors.GeometryWKTCellEditor;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.renders.FeatureCellRenderer;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.renders.FormattedCellRenderer;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.renders.GeometryWKTCellRenderer;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.renders.JToggleButtonHeaderCellRenderer;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;

public class FeatureTable extends JTable implements Observer, Observable {

    private static final long serialVersionUID = -6139395189283163964L;

    private final FeatureTableModel featureTableModel;
    private JToggleButtonHeaderCellRenderer headerCellRenderer;
    private final Set<ActionListener> columnSelectionListeners;
    private static final int COLUMN_HEADER_MARGIN = 8;

    private static final int COLUMN_HEADER_MIN_WIDTH = 50;

    public FeatureTable(FeatureTableModel featureTableModel) {
        super(featureTableModel);
        this.featureTableModel = featureTableModel;
        this.columnSelectionListeners = new HashSet<>();
        init();
    }

    public void addColumnSelectionListener(ActionListener listener) {
       this.columnSelectionListeners.add(listener);
    }

    public void fireColumnSelection(ActionEvent e) {
        for( ActionListener listener : this.columnSelectionListeners ) {
            try {
                listener.actionPerformed(e);
            } catch(Exception ex) {

            }
        }
    }

    @Override
    public void update(Observable observable, Object notification) {
        if( observable instanceof FeatureSelection ) {
            // Ha cambiado la seleccion, simplemente repintamos.
            repaint();
            return;
        }
        if( observable instanceof FeatureStore ) {
            FeatureStoreNotification fsNotification = (FeatureStoreNotification) notification;
            String type = fsNotification.getType();
            /*
             * This is necessary to let Swing know
             * that editing (in terms of Swing, not gvsig editing)
             * must be cancelled because the deleted row
             * is perhaps the row that was being edited
             */
            if (FeatureStoreNotification.BEFORE_DELETE.equals(type)) {
                if (this.isEditing()) {
                    ChangeEvent che = new ChangeEvent(this);
                    this.editingCanceled(che);
                }
            }
        }
    }

    @Override
    public void addObserver(Observer observer) {
        headerCellRenderer.addObserver(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        headerCellRenderer.deleteObserver(observer);
    }

    @Override
    public void deleteObservers() {
        headerCellRenderer.deleteObservers();
    }

//    public void setSelectionUp(boolean selectionUp) {
//            ((FeatureTableModel) getModel()).setSelectionUp(selectionUp);
//            scrollRectToVisible(getCellRect(0, 0, true));
//    }
    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();
        // Add a cell renderer for Geometries and Features
        setDefaultRenderer(Geometry.class, new GeometryWKTCellRenderer());
        setDefaultEditor(Geometry.class, new GeometryWKTCellEditor());
        setDefaultRenderer(Feature.class, new FeatureCellRenderer());

        if (this.getModel() instanceof DefaultFeatureTableModel) {
            DefaultFeatureTableModel model = (DefaultFeatureTableModel) this.getModel();
            setDefaultRenderer(Double.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Float.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Integer.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Long.class, new FormattedCellRenderer(model));
            setDefaultRenderer(Date.class, new FormattedCellRenderer(model));
            setDefaultEditor(Double.class, new FormattedCellEditor(model));
            setDefaultEditor(Float.class, new FormattedCellEditor(model));
            setDefaultEditor(Integer.class, new FormattedCellEditor(model));
            setDefaultEditor(Long.class, new FormattedCellEditor(model));
            setDefaultEditor(Date.class, new FormattedCellEditor(model));
        }

        // Set the selected row colors
        setSelectionForeground(Color.blue);
        setSelectionBackground(Color.yellow);
    }

    /**
     * Initializes the table GUI.
     */
    private void init() {
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        featureTableModel.getFeatureStore().addObserver(this);
        this.featureTableModel.getFeatureSelection().addObserver(this);
        this.featureTableModel.addChangeListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if( FeatureTableModel.CHANGE_SELECTION.equals(e.getActionCommand()) ) {
                    featureSelectionChanged();
                }
            }
        });        
        // Change the selection model to link with the FeatureStore selection
        // through the FeatureTableModel
        setRowSelectionAllowed(true);
        setColumnSelectionAllowed(false);
        setSelectionModel(new FeatureSelectionModel(featureTableModel));

        headerCellRenderer = new JToggleButtonHeaderCellRenderer(this);
        getTableHeader().setDefaultRenderer(headerCellRenderer);

        TableColumnModel tcmodel = getColumnModel();
        for (int i = 0; i < tcmodel.getColumnCount(); i++) {
            TableColumn col = tcmodel.getColumn(i);
            // Get width of column header
            TableCellRenderer renderer = col.getHeaderRenderer();
            if (renderer == null) {
                renderer = getTableHeader().getDefaultRenderer();
            }
            Component comp
                    = renderer.getTableCellRendererComponent(this,
                            col.getHeaderValue(), false, false, 0, i);
            int width = comp.getPreferredSize().width;
            width
                    = width < COLUMN_HEADER_MIN_WIDTH ? COLUMN_HEADER_MIN_WIDTH
                            : width;
            col.setPreferredWidth(width + 2 * COLUMN_HEADER_MARGIN);
        }
        this.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    }

    private void featureSelectionChanged() {
        this.featureTableModel.getFeatureSelection().addObserver(this);
    }

    @Override
    public void setModel(TableModel dataModel) {
        super.setModel(dataModel);
    }

    @Override
    public int getSelectedColumnCount() {
        return headerCellRenderer.getSelectedColumns().length;
    }

    @Override
    public int[] getSelectedColumns() {
        return headerCellRenderer.getSelectedColumns();
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        // Clear the header selection
        if (e != null && e.getFirstRow() == TableModelEvent.HEADER_ROW
                && headerCellRenderer != null) {
            headerCellRenderer.deselectAll();
        }

        super.tableChanged(e);
    }

    @Override
    public Class<?> getColumnClass(int column) {
        Class resp = super.getColumnClass(column);
        if (Timestamp.class.isAssignableFrom(resp)) {
            return Object.class;
        } else {
            return resp;
        }
    }

    @Override
    public int getSelectedRowCount() {
        return this.featureTableModel.getSelectionCount();
    }
}
