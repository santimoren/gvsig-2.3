package org.gvsig.fmap.dal.swing.impl.jdbc;

import com.jeta.open.i18n.I18NUtils;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class DefaultJDBCConnectionPanelView extends JPanel
{
   JLabel lblConnectionName = new JLabel();
   JLabel lblConnector = new JLabel();
   JLabel lblServer = new JLabel();
   JLabel lblPort = new JLabel();
   JLabel lblDataBase = new JLabel();
   JLabel lblUsername = new JLabel();
   JLabel lblPassword = new JLabel();
   JComboBox cboConnections = new JComboBox();
   JComboBox cboConnectors = new JComboBox();
   JTextField txtServer = new JTextField();
   JTextField txtPort = new JTextField();
   JTextField txtDataBase = new JTextField();
   JTextField txtUsername = new JTextField();
   JTextField txtPassword = new JTextField();
   JLabel lblFoother = new JLabel();

   /**
    * Default constructor
    */
   public DefaultJDBCConnectionPanelView()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0),FILL:DEFAULT:NONE","CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblConnectionName.setName("lblConnectionName");
      lblConnectionName.setText("Nombre de la conexion");
      jpanel1.add(lblConnectionName,cc.xy(2,2));

      lblConnector.setName("lblConnector");
      lblConnector.setText("Conector");
      jpanel1.add(lblConnector,cc.xy(2,4));

      lblServer.setName("lblServer");
      lblServer.setText("Servidor");
      jpanel1.add(lblServer,cc.xy(2,6));

      lblPort.setName("lblPort");
      lblPort.setText("Puerto");
      jpanel1.add(lblPort,cc.xy(2,8));

      lblDataBase.setName("lblDataBase");
      lblDataBase.setText("Base de datos");
      jpanel1.add(lblDataBase,cc.xy(2,10));

      lblUsername.setName("lblUsername");
      lblUsername.setText("Usuario");
      jpanel1.add(lblUsername,cc.xy(2,12));

      lblPassword.setName("lblPassword");
      lblPassword.setText("Clave");
      jpanel1.add(lblPassword,cc.xy(2,14));

      cboConnections.setEditable(true);
      cboConnections.setName("cboConnections");
      cboConnections.setRequestFocusEnabled(false);
      jpanel1.add(cboConnections,cc.xy(4,2));

      cboConnectors.setName("cboConnectors");
      jpanel1.add(cboConnectors,cc.xy(4,4));

      txtServer.setName("txtServer");
      jpanel1.add(txtServer,cc.xy(4,6));

      txtPort.setName("txtPort");
      jpanel1.add(txtPort,cc.xy(4,8));

      txtDataBase.setName("txtDataBase");
      jpanel1.add(txtDataBase,cc.xy(4,10));

      txtUsername.setName("txtUsername");
      jpanel1.add(txtUsername,cc.xy(4,12));

      txtPassword.setName("txtPassword");
      jpanel1.add(txtPassword,cc.xy(4,14));

      jpanel1.add(createPanel1(),cc.xywh(2,16,3,1));
      addFillComponents(jpanel1,new int[]{ 1,2,3,4,5 },new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 });
      return jpanel1;
   }

   public JPanel createPanel1()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("RIGHT:12DLU:GROW(1.0)","CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      lblFoother.setName("lblFoother");
      lblFoother.setText("<html> Tenga en cuenta que el nombre de usuario y la  contraseña deben coincidir en mayusculas y minusculas con el registrado en la base de dato. <html>");
      jpanel1.add(lblFoother,cc.xy(1,1));

      addFillComponents(jpanel1,new int[0],new int[0]);
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
