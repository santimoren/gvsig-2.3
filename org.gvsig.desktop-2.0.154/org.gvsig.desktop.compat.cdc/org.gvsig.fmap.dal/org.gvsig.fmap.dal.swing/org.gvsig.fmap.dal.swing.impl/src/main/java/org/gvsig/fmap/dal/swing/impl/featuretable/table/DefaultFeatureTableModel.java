/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.swing.impl.featuretable.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.impl.featuretable.table.renders.GetFeatureAtException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.ComplexNotification;
import org.gvsig.tools.observer.ComplexObserver;
import org.gvsig.tools.observer.Observable;

public class DefaultFeatureTableModel extends AbstractTableModel implements org.gvsig.fmap.dal.swing.FeatureTableModel,  ComplexObserver  {

    private static final long serialVersionUID = -8223987814719746492L;

    private static final Logger logger = LoggerFactory.getLogger(DefaultFeatureTableModel.class);

    private List<String> columnNames;

    private List<String> visibleColumnNames;

    private List<String> visibleColumnNamesOriginal;

    private Map<String, String> name2Alias;

    private Map<String, String> name2AliasOriginal;

    private Map<String,String> patterns = null;

    private Locale localeOfData;

    private final FeaturePagingHelper featurePager;

    /** Used to know if a modification in the FeatureStore is created by us. */
    private EditableFeature editableFeature;

    private boolean selectionLocked=false;

    private final DelayAction delayAction = new DelayAction();

    private FeatureSelection selection = null;

    private Set<ActionListener> changeListeners = null;
    
    public DefaultFeatureTableModel(FeaturePagingHelper featurePager) {
        this.featurePager = featurePager;
        this.localeOfData = Locale.getDefault();
        this.initialize();
    }

    private void initialize() {
        this.getFeatureStore().addObserver(this);

        int columns = this.getOriginalColumnCount();

        // Initilize visible columns
        columnNames = new ArrayList<>(columns);
        visibleColumnNames = new ArrayList<>(columns);
        for (int i = 0; i < columns; i++) {
            FeatureAttributeDescriptor descriptor = this.getInternalColumnDescriptor(i);
            String columnName = descriptor.getName();
            columnNames.add(columnName);

            // By default, geometry columns will not be visible
            if (descriptor.getType() != DataTypes.GEOMETRY) {
                visibleColumnNames.add(columnName);
            }
        }
        visibleColumnNamesOriginal = new ArrayList<>(visibleColumnNames);

        // Initialize alias
        name2Alias = new HashMap<>(columns);
        name2AliasOriginal = new HashMap<>(columns);

        initializeFormatingPatterns();
        updatePagerWithHiddenColums();
    }

    private void initializeFormatingPatterns() {
        int columns = this.getOriginalColumnCount();

        this.patterns = new HashMap<>();
        for (int i = 0; i < columns; i++) {
            FeatureAttributeDescriptor descriptor = this.getInternalColumnDescriptor(i);
            String columnName = descriptor.getName();
            switch(descriptor.getDataType().getType()) {
            case DataTypes.BYTE:
            case DataTypes.INT:
            case DataTypes.LONG:
                String defaultIntegerPattern = "#,##0";
                this.patterns.put(columnName,defaultIntegerPattern);
                break;
            case DataTypes.DOUBLE:
                String defaultDoublePattern = "#,##0.0000000000";
                this.patterns.put(columnName,defaultDoublePattern);
                break;
            case DataTypes.FLOAT:
                String defaultFloatPattern = "#,##0.0000";
                this.patterns.put(columnName,defaultFloatPattern);
                break;
            case DataTypes.DATE:
                String defaultDatePattern = new SimpleDateFormat().toPattern();
                this.patterns.put(columnName,defaultDatePattern);
                break;
            default:
                this.patterns.put(columnName,null);
            }
        }

    }

    private void updatePagerWithHiddenColums() {
    	return;
    	
//        FeatureQuery query = this.getFeaturePager().getFeatureQuery();
//        if (this.getFeaturePager().getFeatureStore().isEditing()) {
//            if (query.hasConstantsAttributeNames()) {
//                query.clearConstantsAttributeNames();
//            }
//        } else {
//            query.setConstantsAttributeNames(this.getHiddenColumnNames());
//        }
//        try {
//            this.getFeaturePager().reload();
//        } catch (BaseException ex) {
//            logger.warn("Can't reload paging-helper.", ex);
//        }
    }

    @Override
    public FeaturePagingHelper getFeaturePager() {
        return this.featurePager;
    }

    @Override
    public FeatureQuery getFeatureQuery() {
        return this.getFeaturePager().getFeatureQuery();
    }

    @Override
    public FeatureType getFeatureType() {
        return this.getFeaturePager().getFeatureType();
    }

    @Override
    public FeatureStore getFeatureStore() {
        return this.getFeaturePager().getFeatureStore();
    }

    @Override
    public int getColumnCount() {
        return visibleColumnNames.size();
    }

    public int getOriginalColumnCount() {
        FeatureType featureType = getFeatureType();
        return featureType.size();
    }

    @Override
    public String getColumnName(int column) {
        String columName = getOriginalColumnName(column);
        return this.getColumnAlias(columName);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);

        // Return the class of the FeatureAttributeDescriptor for the value
        FeatureAttributeDescriptor attributeDesc = this.getInternalColumnDescriptor(originalIndex);
        if (attributeDesc == null) {
        	return super.getColumnClass(originalIndex);
        }
        Class<?> clazz = attributeDesc.getObjectClass();
        return (clazz == null ? super.getColumnClass(originalIndex) : clazz);
    }

    @Override
    public FeatureAttributeDescriptor getColumnDescriptor(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);
        return this.getInternalColumnDescriptor(originalIndex);
    }

    protected FeatureAttributeDescriptor getInternalColumnDescriptor(int columnIndex) {
        FeatureType featureType = getFeatureType();
        if( featureType == null ) {
            return null;
        }
        return featureType.getAttributeDescriptor(columnIndex);
    }

    @Override
    public String getOriginalColumnName(int column) {
        return getInternalColumnDescriptor(column).getName();
    }

    @Override
    public void setColumnVisible(String name, boolean visible) {
        if (!columnNames.contains(name)) {
            throw new InvalidParameterException(name); // FIXME
        }
        if( visible ) {
            if ( !visibleColumnNames.contains(name) ) {
                visibleColumnNames.add(name);
                setVisibleColumns(visibleColumnNames);
            }
        } else {
            if ( visibleColumnNames.contains(name) ) {
                visibleColumnNames.remove(name);
                setVisibleColumns(visibleColumnNames);
                fireTableStructureChanged();
            }
        }
    }

    public void setFeatureType(FeatureType featureType) {
        // Check if there is a new column name
        List<String> newColumns = new ArrayList<>();
        List<String> renamedColumnsNewName = new ArrayList<>();

        Iterator<FeatureAttributeDescriptor> attrIter = featureType.iterator();
        FeatureAttributeDescriptor fad ;
        EditableFeatureAttributeDescriptor efad ;

        String colName;
        while (attrIter.hasNext()) {
            fad = attrIter.next();
            colName = fad.getName();
            if (!columnNames.contains(colName)) {
                if (fad instanceof EditableFeatureAttributeDescriptor) {
                    efad = (EditableFeatureAttributeDescriptor) fad;
                    /*
                     * If editable att descriptor,
                     * check original name
                     */
                    if (efad.getOriginalName() != null) {
                        if (!columnNames.contains(efad.getOriginalName())) {
                            /*
                             * Check with original name but add current name
                             */
                            newColumns.add(colName);
                        } else {
                            /*
                             * List of new names of renamed columns
                             */
                            renamedColumnsNewName.add(colName);
                        }
                    } else {
                        newColumns.add(colName);
                    }
                } else {
                    newColumns.add(colName);
                }
            }
        }

        // Update column names
        columnNames.clear();
        @SuppressWarnings("unchecked")
        Iterator<FeatureAttributeDescriptor> visibleAttrIter =
            featureType.iterator();
        while (visibleAttrIter.hasNext()) {
            fad = visibleAttrIter.next();
            colName = fad.getName();
            columnNames.add(colName);
            //If the column is added has to be visible
            if (!visibleColumnNames.contains(colName)) {

                if (((newColumns.contains(colName)
                    || renamedColumnsNewName.contains(colName)))
                    &&
                    fad.getType() != DataTypes.GEOMETRY) {
                    // Add new columns and renamed
                    visibleColumnNames.add(colName);
                    visibleColumnNamesOriginal.add(colName);
                }
                /*
                if (renamedColumnsNewName.contains(colName)) {
                    // Add renamed
                    insertWhereOldName(visibleColumnNames, colName, fad);
                    insertWhereOldName(visibleColumnNamesOriginal, colName, fad);
                }
                */
            }
        }

        // remove from visible columns removed columns
        visibleColumnNames = intersectKeepOrder(columnNames, visibleColumnNames);
        // instead of: visibleColumnNames.retainAll(columnNames);

        visibleColumnNamesOriginal = intersectKeepOrder(columnNames, visibleColumnNamesOriginal);
        // instead of: visibleColumnNamesOriginal.retainAll(columnNames);

        // remove from alias map removed columns
        name2Alias.keySet().retainAll(columnNames);
        name2AliasOriginal.keySet().retainAll(columnNames);

        initializeFormatingPatterns();

        getFeatureQuery().setFeatureType(featureType);
        reloadFeatures();
        //Selection must be locked to avoid losing it when the table is refreshed
        selectionLocked=true;
        //The table is refreshed
        try {
            fireTableStructureChanged();
        } catch (Exception e) {
            logger.warn("Couldn't reload changed table");
        }finally{
            //The locked selection is unlocked.
            selectionLocked=false;
        }

    }

    private void reloadFeatures() {
        try {
            this.getFeaturePager().reload();
        } catch (BaseException ex) {
            throw new FeaturesDataReloadException(ex);
        }
    }

    /**
     * keeps order of first parameter
     *
     * @param lista
     * @param listb
     * @return
     */
    private List<String> intersectKeepOrder(List<String> lista, List<String> listb) {

        List<String> resp = new ArrayList<>();
        resp.addAll(lista);
        resp.retainAll(listb);
        return resp;
    }

    public void setVisibleColumns(List<String> names) {
        // Recreate the visible column names list
        // to maintain the original order
        visibleColumnNames = new ArrayList<>(names.size());
        for (int i = 0; i < columnNames.size(); i++) {
            String columnName = columnNames.get(i);
            if (names.contains(columnName)) {
                visibleColumnNames.add(columnName);
            }
        }
        updatePagerWithHiddenColums();
        fireTableStructureChanged();
    }

    protected String[] getHiddenColumnNames() {
        List<String> hiddenColumns = new ArrayList<String>();
        hiddenColumns.addAll(columnNames);

        for (int i = 0; i < visibleColumnNames.size(); i++) {
            String columnName = visibleColumnNames.get(i);
            hiddenColumns.remove(columnName);
        }
        if( hiddenColumns.size()<1 ) {
            return null;
        }
        return (String[]) hiddenColumns.toArray(new String[hiddenColumns.size()]);
    }

    /**
     * Changes all columns to be visible.
     */
    public void setAllVisible() {
        visibleColumnNames.clear();
        visibleColumnNames.addAll(columnNames);
        fireTableStructureChanged();
    }

    @Override
    public void setColumnOrder(String name, boolean ascending)
        throws BaseException {
        FeatureQueryOrder order = this.getFeatureQuery().getOrder();
        if (order == null) {
            order = new FeatureQueryOrder();
            this.getFeatureQuery().setOrder(order);
        }
        order.clear();
        order.add(name, ascending);
        this.getFeaturePager().reload();
        fireTableChanged(new TableModelEvent(this, 0, this.getRowCount() - 1));
    }

    @Override
    public int getRowCount() {
        // Return the total size of the collection
        // If the size is bigger than INTEGER.MAX_VALUE, return that instead
        try {
            long totalSize = this.getFeaturePager().getTotalSize();
            if (totalSize > Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            } else {
                return (int) totalSize;
            }
        } catch (ConcurrentDataModificationException e) {
            logger.debug("Error while getting the total size of the set", e);
            return 0;
        }
    }

    @Override
    public boolean isColumnVisible(String name) {
        return visibleColumnNames.contains(name);
    }

    @Override
    public String getColumnAlias(String name) {
        String alias = name2Alias.get(name);
        return alias == null ? name : alias;
    }

    @Override
    public void setColumnAlias(String name, String alias) {
        name2Alias.put(name, alias);
        fireTableStructureChanged();
    }

    @Override
    public int getOriginalColumnIndex(int columnIndex) {
        String columnName = visibleColumnNames.get(columnIndex);
        return columnNames.indexOf(columnName);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // Get the Feature at row "rowIndex", and return the value of the
        // attribute at "columnIndex"
        Feature feature = getFeatureAt(rowIndex);
        return feature == null ? null : getFeatureValue(feature, columnIndex);
    }

    @Override
    public Feature getFeatureAt(int rowIndex) {
        try {
            return this.getFeaturePager().getFeatureAt(rowIndex);
        } catch (BaseException ex) {
            throw new GetFeatureAtException(rowIndex, ex);
        }
    }

    protected Object getFeatureValue(Feature feature, int columnIndex) {
        int realColumnIndex = getOriginalColumnIndex(columnIndex);
        return feature.get(realColumnIndex);
    }

    protected EditableFeature setFeatureValue(Feature feature, int columnIndex,
        Object value) {
        int realColumnIndex = getOriginalColumnIndex(columnIndex);
        EditableFeature editableFeature = feature.getEditable();
        editableFeature.set(realColumnIndex, value);
        return editableFeature;
    }


    public void acceptChanges() {
    	visibleColumnNamesOriginal = new ArrayList<>(visibleColumnNames);
    	name2AliasOriginal = new HashMap<>(name2Alias);
    }

    public void cancelChanges() {
    	visibleColumnNames = new ArrayList<>(visibleColumnNamesOriginal);
    	name2Alias = new HashMap<>(name2AliasOriginal);
    	fireTableStructureChanged();
    }


    @Override
    public String getColumnFormattingPattern(int column) {
        String columnName = this.visibleColumnNames.get(column);
        return this.getColumnFormattingPattern(columnName);
    }

    @Override
    public String getColumnFormattingPattern(String columnName) {
        String pattern = this.patterns.get(columnName);
        return pattern;
    }

    @Override
    public void setColumnFormattingPattern(String columnName, String pattern) {
        this.patterns.put(columnName,pattern);
    }

    @Override
    public Locale getLocaleOfData() {
        return this.localeOfData;
    }

    @Override
    public void setLocaleOfData(Locale locale) {
        this.localeOfData = locale;
    }

    public boolean isSelectionLocked() {
        return selectionLocked;
    }

    @Override
    public boolean isSelectionUp() {
        return this.getFeaturePager().isSelectionUp();
    }

    @Override
    public void setSelectionUp(boolean selectionUp) {
        this.getFeaturePager().setSelectionUp(selectionUp);
        fireTableChanged(new TableModelEvent(this, 0, getRowCount() - 1));
    }

    private class DelayAction extends Timer implements ActionListener, Runnable {
        private static final int STATE_NONE = 0;
        private static final int STATE_NEED_RELOADALL = 1;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED = 2;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_UPDATED = 4;
        private static final int STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED = 8;
        private static final int STATE_NEED_RELOAD_FEATURE_TYPE = 16;
        private static final int STATE_NEED_SELECTION_UP = 32;
        private static final int STATE_NEED_RELOAD_ALL_FEATURES=64;

        private static final long serialVersionUID = -5692569125344166705L;

        private int state = STATE_NONE;
        private Feature feature;
        private FeatureType featureType;
        private boolean isSelecctionUp;

        public DelayAction() {
            super(1000,null);
            this.setRepeats(false);
            this.reset();
            this.addActionListener(this);
        }

        private void reset() {
            this.state = STATE_NONE;
            this.isSelecctionUp = false;
            this.feature = null;
            this.featureType = null;
        }

        public void actionPerformed(ActionEvent ae) {
            this.run();
        }

        public void run() {
            if( !SwingUtilities.isEventDispatchThread() ) {
                SwingUtilities.invokeLater(this);
                return;
            }
            this.stop();
            logger.info("DelayAction.run["+this.state+"] begin");
            switch(this.state) {
            case STATE_NEED_RELOADALL:
                reloadAll();
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                reloadIfFeatureCountChanged(feature);
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                reloadIfFeatureUpdated(feature);
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                reloadIfTypeChanged(featureType);
                break;
            case STATE_NEED_RELOAD_FEATURE_TYPE:
                reloadFeatureType();
                updatePagerWithHiddenColums();
                break;
            case STATE_NEED_RELOAD_ALL_FEATURES:
                reloadFeatures();
                fireTableChanged(new TableModelEvent(DefaultFeatureTableModel.this, 0, getRowCount()));
                break;
            case STATE_NEED_SELECTION_UP:
            case STATE_NONE:
            default:
                break;
            }
            if( isSelecctionUp ) {
                getFeaturePager().setSelectionUp(true);
            }
            this.reset();
            logger.info("DelayAction.run["+this.state+"] end");
        }

        public void nextState(int nextstate) {
            this.nextState(nextstate, null, null);
        }

        public void nextState(int nextstate, Feature feature) {
            this.nextState(nextstate, feature, null);
        }

        public void nextState(int nextstate, FeatureType featureType) {
            this.nextState(nextstate, null, featureType);
        }

        public void nextState(int nextstate, Feature feature, FeatureType featureType) {
            this.feature = feature;
            this.featureType = featureType;
            switch(nextstate) {
            case STATE_NEED_RELOADALL:
            case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
            case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                //case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                    this.state = STATE_NEED_RELOADALL;
                    break;
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                    this.state = STATE_NEED_RELOAD_FEATURE_TYPE;
                    break;
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                    this.state=STATE_NEED_RELOAD_ALL_FEATURES;
                    break;
                case STATE_NEED_SELECTION_UP:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    break;
                }
                break;
            case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
            case STATE_NEED_RELOAD_FEATURE_TYPE:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                    this.state = STATE_NEED_RELOAD_FEATURE_TYPE;
                    break;
                case STATE_NEED_SELECTION_UP:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    break;
                }
                break;
            case STATE_NEED_SELECTION_UP:
                switch(this.state) {
                case STATE_NEED_RELOADALL:
                case STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED:
                case STATE_NEED_RELOAD_IF_FEATURE_UPDATED:
                case STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED:
                case STATE_NEED_RELOAD_ALL_FEATURES:
                case STATE_NEED_RELOAD_FEATURE_TYPE:
                case STATE_NEED_SELECTION_UP:
                    this.isSelecctionUp = true;
                    break;
                case STATE_NONE:
                default:
                    this.state = nextstate;
                    this.isSelecctionUp = true;
                    break;
                }
                break;
            case STATE_NONE:
            default:
                this.state = STATE_NONE;
                break;
            }
            if( this.state != STATE_NONE ) {
                this.start();
            }
        }

    }

    /**
     * Reloads the table data if a feature has been changed, not through the
     * table.
     */
    private void reloadIfFeatureCountChanged(Feature feature) {
        // Is any data is changed in the FeatureStore, notify the model
        // listeners. Ignore the case where the updated feature is
        // changed through us.
        if (editableFeature == null || !editableFeature.equals(feature)) {
            reloadFeatures();
            //Selection must be locked to avoid losing it when the table is refreshed
            selectionLocked=true;
            //The table is refreshed
            try {
                fireTableDataChanged();
            } catch (Exception e) {
                logger.warn("Couldn't reload changed table");
            }finally{
                //The locked selection is unlocked.
                selectionLocked=false;
            }
        }
    }

    private void reloadIfFeatureUpdated(Feature feature) {
        // Is any data is changed in the FeatureStore, notify the model
        // listeners. Ignore the case where the updated feature is
        // changed through us.
        if (editableFeature == null || !editableFeature.equals(feature)) {
            reloadFeatures();
            fireTableChanged(new TableModelEvent(this, 0, getRowCount()));
        }
    }

    /**
     * Reloads data and structure if the {@link FeatureType} of the features
     * being shown has changed.
     */
    private void reloadIfTypeChanged(FeatureType updatedType) {
        // If the updated featured type is the one currently being
        // shown, reload the table.
        if (updatedType != null
            && updatedType.getId().equals(getFeatureType().getId())) {
            setFeatureType(updatedType);
        }
    }

    private void reloadAll() {
    	reloadFeatureType();
    }

    private void reloadFeatureType() {
        try {
            FeatureType featureType = this.getFeaturePager().getFeatureType();
            FeatureStore store = this.getFeaturePager().getFeatureStore();
            this.setFeatureType( store.getFeatureType(featureType.getId()) );
        } catch (DataException e) {
            throw new FeaturesDataReloadException(e);
        }
    }

    @Override
    public void update(final Observable observable, final Object notification) {
        if (notification instanceof ComplexNotification) {
            // A lot of things might have happened in the store, so don't
            // bother looking into each notification.
            this.delayAction.nextState(DelayAction.STATE_NEED_RELOADALL);
//            reloadAll();
        } else if (observable.equals(getFeatureStore())
                && notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification fsNotification
                    = (FeatureStoreNotification) notification;
            String type = fsNotification.getType();

            // If there are new, updated or deleted features
            // reload the table data
            if (FeatureStoreNotification.AFTER_DELETE.equals(type)
                    || FeatureStoreNotification.AFTER_INSERT.equals(type)) {
//                reloadIfFeatureCountChanged(fsNotification.getFeature());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_COUNT_CHANGED, fsNotification.getFeature());

            } else if (FeatureStoreNotification.AFTER_UPDATE.equals(type)) {
//                reloadIfFeatureUpdated(fsNotification.getFeature());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_UPDATED, fsNotification.getFeature());

            } else if (FeatureStoreNotification.AFTER_UPDATE_TYPE.equals(type)) {
//                reloadIfTypeChanged(fsNotification.getFeatureType());
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_IF_FEATURE_TYPE_CHANGED, fsNotification.getFeatureType());

            } else if (FeatureStoreNotification.TRANSFORM_CHANGE.equals(type)
                    || FeatureStoreNotification.AFTER_UNDO.equals(type)
                    || FeatureStoreNotification.AFTER_REDO.equals(type)
                    || FeatureStoreNotification.AFTER_REFRESH.equals(type))  {
//                reloadAll();
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOADALL);

            } else if (FeatureStoreNotification.AFTER_FINISHEDITING.equals(type)
                    || FeatureStoreNotification.AFTER_STARTEDITING.equals(type)
                    || FeatureStoreNotification.AFTER_CANCELEDITING.equals(type)) {
                /*
                No tengo nada claro por que es necesario llamar al reloadFeatureType
                pero si no se incluye hay problemas si durante la edicion se a�aden
                campos a la tabla. Sin esto, al cerrar la edicion, los campos a�adidos
                desaparecen de la tabla aunque estan en el fichero.
                Ver ticket #2434 https://devel.gvsig.org/redmine/issues/2434
                */
//                reloadFeatureType();
//                updatePaginHelperWithHiddenColums();
                this.delayAction.nextState(DelayAction.STATE_NEED_RELOAD_FEATURE_TYPE, fsNotification.getFeatureType());
            } else if (FeatureStoreNotification.SELECTION_CHANGE.equals(type)) {
                if( this.isSelectionUp() ) {
                    this.setSelectionUp(true);
                    this.delayAction.nextState(DelayAction.STATE_NEED_SELECTION_UP);
                }
            }
        }
    }
    
    @Override
    public FeatureSelection getFeatureSelection() {
        if (selection == null) {
            try {
                return getFeatureStore().getFeatureSelection();
            } catch (Exception e) {
                logger.warn("Error getting the selection", e);
            }
        }
        return selection;
    }
    
    @Override
    public void setFeatureSelection(FeatureSelection selection) {
        this.selection = selection;
        this.featurePager.setSelection(selection);
        this.fireChangeListeners(new ActionEvent(this, 0,CHANGE_SELECTION));
    }
    
    public void addChangeListener(ActionListener listener) {
        if( this.changeListeners==null) {
            this.changeListeners = new HashSet<>();
        }
        this.changeListeners.add(listener);
    }
    
    public void fireChangeListeners(ActionEvent event) {
        if( this.changeListeners == null ) {
            return;
        }
        for( ActionListener listener : this.changeListeners ) {
            try {
                listener.actionPerformed(event);
            } catch(Exception ex) {
                // Ignore
            }
        }
    }
    
    @Override
    public int getSelectionCount() {
        try {
            FeatureSelection selection = this.getFeatureSelection();
            return (int) selection.getSize();
        } catch (DataException ex) {
            throw new RuntimeException("Can't get selection of the FeatureTableModel",ex);
        }
    }


}
