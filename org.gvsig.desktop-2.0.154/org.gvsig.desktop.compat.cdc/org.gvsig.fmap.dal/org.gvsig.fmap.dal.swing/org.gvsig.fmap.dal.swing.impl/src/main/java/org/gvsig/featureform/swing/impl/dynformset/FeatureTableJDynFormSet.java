/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing.impl.dynformset;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Action;
import javax.swing.Box.Filler;
import javax.swing.JButton;

import javax.swing.JComponent;
import javax.swing.JPanel;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.FeatureTableModel;
import org.gvsig.fmap.dal.swing.JFeatureTable;
import org.gvsig.tools.dynform.DynFormDefinition;
import org.gvsig.tools.dynform.spi.AbstractJDynFormSet;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.service.spi.ServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureTableJDynFormSet extends AbstractJDynFormSet  {

    private static final Logger logger = LoggerFactory.getLogger(FeatureTableJDynFormSet.class);

    private int current = 0;
    
    private JButton btnModify;
    private JButton btnNew;
    private JButton btnRemove;
    private JButton btnSelect;
    private JButton btnShow;
    private Filler filler1;
    private Filler filler2;
    private JFeatureTable table = null;
    private FeatureTableModel tableModel = null;

    public FeatureTableJDynFormSet(ServiceManager manager, DynFormDefinition definition) throws ServiceException {
        super(manager, definition);
    }

    @Override
    public JComponent asJComponent() {
        if (this.contents == null) {
            try {
                this.initComponents();
            } catch (ServiceException e) {
                throw new RuntimeException(e.getLocalizedMessage(), e);
            }
        }
        this.fireFormMovedToEvent(current);
        return this.contents;
    }

    private void initComponents() throws ServiceException {
        JPanel panel = new JPanel();
        GridBagConstraints gridBagConstraints;

        filler1 = new Filler(new Dimension(5, 5), new Dimension(5, 5), new Dimension(5, 5));
        this.tableModel = DALSwingLocator.getSwingManager().createEmptyFeatureTableModel(this.definition.getElementsType());
        this.table = DALSwingLocator.getSwingManager().createJFeatureTable(tableModel);
        btnNew = new JButton();
        btnShow = new JButton();
        btnModify = new JButton();
        btnRemove = new JButton();
        btnSelect = new JButton();
        filler2 = new Filler(new Dimension(5, 5), new Dimension(5, 5), new Dimension(5, 5));
        

        GridBagLayout layout = new GridBagLayout();
        layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0};
        layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        panel.setLayout(layout);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridheight = 11;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panel.add(table.asJComponent(), gridBagConstraints);

        btnNew.setText("_nuevo");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btnNew, gridBagConstraints);

        btnShow.setText("_mostrar");
        btnShow.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnShowActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btnShow, gridBagConstraints);

        btnModify.setText("_modificar");
        btnModify.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnModifyActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btnModify, gridBagConstraints);

        btnRemove.setText("_eliminar");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btnRemove, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 14;
        panel.add(filler1, gridBagConstraints);

        btnSelect.setText("_seleccionar");
        btnSelect.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnSelectActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btnSelect, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        panel.add(filler2, gridBagConstraints);
    }

    private void btnShowActionPerformed(ActionEvent evt) {                                        

    }                                       

    private void btnNewActionPerformed(ActionEvent evt) {                                       

    }                                      

    private void btnModifyActionPerformed(ActionEvent evt) {                                          

    }                                         

    private void btnRemoveActionPerformed(ActionEvent evt) {                                          

    }                                         

    private void btnSelectActionPerformed(ActionEvent evt) {                                          

    }    
    
    @Override
    public void setValues(List values) throws ServiceException {
        super.setValues(values);
        if (this.contents != null) {
            doChangeValues();
        }
    }

    @Override
    public void setValues(DynObjectSet values) throws ServiceException {
        super.setValues(values);
        if (this.contents != null) {
            doChangeValues();
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
    }

    
    @Override
    public void setFormSize(int width, int height) {
        super.setFormSize(width, height);
    }

    @Override
    public int getCurrentIndex() {
        return this.current;
    }

    @Override
    public void setCurrentIndex(int index) {
        if (index < 0 || index > countValues()) {
            throw new IllegalArgumentException("Index (" + index + ") out of range [0.." + countValues() + "].");
        }
        this.table.setCurrent(this.current);
    }


    @Override
    public void addAction(Action action) {

    }

    private void doChangeValues() {

    }

    @Override
    public boolean hasValidValues() {
        return true;
    }

    @Override
    public boolean hasValidValues(List<String> fieldsName) {
        return true;
    }

    @Override
    public List getValues() {
        return this.values;
    }


}
