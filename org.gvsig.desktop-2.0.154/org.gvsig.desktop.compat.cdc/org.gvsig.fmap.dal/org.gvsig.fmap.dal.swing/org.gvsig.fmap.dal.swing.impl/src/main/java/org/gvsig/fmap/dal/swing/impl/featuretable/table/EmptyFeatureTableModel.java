/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.swing.impl.featuretable.table;

import java.awt.event.ActionListener;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.ComplexObserver;
import org.gvsig.tools.observer.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmptyFeatureTableModel extends AbstractTableModel implements org.gvsig.fmap.dal.swing.FeatureTableModel,  ComplexObserver  {

    private static final long serialVersionUID = -8223987814719746492L;
    
    private static final Logger logger = LoggerFactory.getLogger(EmptyFeatureTableModel.class);

    private List<String> columnNames;

    private List<String> visibleColumnNames;

    private List<String> visibleColumnNamesOriginal;

    private Map<String, String> name2Alias;

    private Map<String, String> name2AliasOriginal;

    private Map<String,String> patterns = null;

    private Locale localeOfData;    

    private DynStruct struct = null;
    

    public EmptyFeatureTableModel(DynStruct struct) {
        this.struct = struct;
        this.localeOfData = Locale.getDefault();
        this.initialize();
    }

    private void initialize() {
        
        int columns = this.getOriginalColumnCount();

        // Initilize visible columns
        columnNames = new ArrayList<>(columns);
        visibleColumnNames = new ArrayList<>(columns);
        for (int i = 0; i < columns; i++) {
            String columnName = super.getColumnName(i);
            columnNames.add(columnName);

            // By default, geometry columns will not be visible
            FeatureAttributeDescriptor descriptor = this.getInternalColumnDescriptor(i);
            if (descriptor.getType() != DataTypes.GEOMETRY) {
                visibleColumnNames.add(columnName);
            }
        }
        visibleColumnNamesOriginal = new ArrayList<>(visibleColumnNames);
        
        // Initialize alias
        name2Alias = new HashMap<>(columns);
        name2AliasOriginal = new HashMap<>(columns);

        // Initialize formating patters
        this.patterns = new HashMap<>();
        for (int i = 0; i < columns; i++) {
            FeatureAttributeDescriptor descriptor = this.getInternalColumnDescriptor(i);
            String columnName = descriptor.getName();
            switch(descriptor.getDataType().getType()) {
            case DataTypes.BYTE:
            case DataTypes.INT:
            case DataTypes.LONG:
                String defaultIntegerPattern = "#,##0";
                this.patterns.put(columnName,defaultIntegerPattern);
                break;
            case DataTypes.DOUBLE:
            case DataTypes.FLOAT:
                String defaultDecimalPattern = "#,##0.000";
                this.patterns.put(columnName,defaultDecimalPattern);
                break;
            case DataTypes.DATE:
                String defaultDatePattern = new SimpleDateFormat().toPattern();
                this.patterns.put(columnName,defaultDatePattern);
                break;
            default:
                this.patterns.put(columnName,null);
            }
        }

        updatePagerWithHiddenColums();
    }

    private void updatePagerWithHiddenColums() {
        FeatureQuery query = this.getFeaturePager().getFeatureQuery();
        if (this.getFeaturePager().getFeatureStore().isEditing()) {
            if (query.hasConstantsAttributeNames()) {
                query.clearConstantsAttributeNames();
            }
        } else {
            query.setConstantsAttributeNames(this.getHiddenColumnNames());
        }
        try {
            this.getFeaturePager().reload();
        } catch (BaseException ex) {
            logger.warn("Can't reload paging-helper.", ex);
        }
    }
    
    @Override
    public FeaturePagingHelper getFeaturePager() {
        return null;
    }
        
    @Override
    public FeatureQuery getFeatureQuery() {
        return null;
    }

    @Override
    public FeatureType getFeatureType() {
        return null;
    }
    
    @Override
    public FeatureStore getFeatureStore() {
        return null;
    }

    @Override
    public FeatureSelection getFeatureSelection() {
        return null;
    }

    @Override
    public void setFeatureSelection(FeatureSelection selection) {
        // Do nothing
    }

    @Override
    public int getColumnCount() {
        return visibleColumnNames.size();
    }

    public int getOriginalColumnCount() {
        return this.struct.getDynFields().length;
    }

    @Override
    public String getColumnName(int column) {
        String columName = getOriginalColumnName(column);
        return this.getColumnAlias(columName);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);
        
        // Return the class of the FeatureAttributeDescriptor for the value
        FeatureAttributeDescriptor attributeDesc = this.getInternalColumnDescriptor(originalIndex);
        if (attributeDesc == null) {
        	return super.getColumnClass(originalIndex);
        }
        Class<?> clazz = attributeDesc.getObjectClass();
        return (clazz == null ? super.getColumnClass(originalIndex) : clazz);
    }

    @Override
    public FeatureAttributeDescriptor getColumnDescriptor(int columnIndex) {
        int originalIndex = getOriginalColumnIndex(columnIndex);
        return this.getInternalColumnDescriptor(originalIndex);
    }
    
    protected FeatureAttributeDescriptor getInternalColumnDescriptor(int columnIndex) {
        return new DynFieldFacadeOfAFeatureAttributeDescriptor(this.struct.getDynFields()[columnIndex]);
    }

    @Override
    public String getOriginalColumnName(int column) {
        return this.struct.getDynFields()[column].getName();
    }

    @Override
    public void setColumnVisible(String name, boolean visible) {
        // If we don't have already the column as visible,
        // add to the list, without order, and recreate
        // the visible columns list in the original order
        if (!columnNames.contains(name)) {
            throw new InvalidParameterException(name); // FIXME
        }
        if (visible && !visibleColumnNames.contains(name)) {
            visibleColumnNames.add(name);
            setVisibleColumns(visibleColumnNames);
        } else {
            visibleColumnNames.remove(name);
            setVisibleColumns(visibleColumnNames);
            fireTableStructureChanged();
        }

    }

    public void setFeatureType(FeatureType featureType) {
    }

    private void setVisibleColumns(List<String> names) {
        // Recreate the visible column names list
        // to maintain the original order        
        visibleColumnNames = new ArrayList<>(names.size());
        for (String columnName : columnNames) {
            if (names.contains(columnName)) {
                visibleColumnNames.add(columnName);
            }
        }
        updatePagerWithHiddenColums();
        fireTableStructureChanged();
    }

    protected String[] getHiddenColumnNames() {
        List<String> hiddenColumns = new ArrayList<>();
        hiddenColumns.addAll(columnNames);
        
        for (String columnName : visibleColumnNames) {
            hiddenColumns.remove(columnName);
        }
        if( hiddenColumns.size()<1 ) {
            return null;
        }
        return (String[]) hiddenColumns.toArray(new String[hiddenColumns.size()]);
    }
        
    /**
     * Changes all columns to be visible.
     */
    @Override
    public void setAllVisible() {
        visibleColumnNames.clear();
        visibleColumnNames.addAll(columnNames);
        fireTableStructureChanged();
    }

    @Override
    public void setColumnOrder(String name, boolean ascending)
        throws BaseException {
    }

    @Override
    public int getRowCount() {
        return 0;
    }

    @Override
    public boolean isColumnVisible(String name) {
        return visibleColumnNames.contains(name);
    }

    @Override
    public String getColumnAlias(String name) {
        String alias = name2Alias.get(name);
        return alias == null ? name : alias;
    }

    @Override
    public void setColumnAlias(String name, String alias) {
        name2Alias.put(name, alias);
        fireTableStructureChanged();
    }

    @Override
    public int getOriginalColumnIndex(int columnIndex) {
        String columnName = visibleColumnNames.get(columnIndex);
        return columnNames.indexOf(columnName);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
    }
    
    @Override
    public Feature getFeatureAt(int rowIndex) {
        return null;
    }    
    
    protected Object getFeatureValue(Feature feature, int columnIndex) {
        return null;
    }

    protected EditableFeature setFeatureValue(Feature feature, int columnIndex,
        Object value) {
        return null;
    }
    

    public void acceptChanges() {
    	visibleColumnNamesOriginal = new ArrayList<>(visibleColumnNames);
    	name2AliasOriginal = new HashMap<>(name2Alias);
    }
    
    public void cancelChanges() {
    	visibleColumnNames = new ArrayList<>(visibleColumnNamesOriginal);
    	name2Alias = new HashMap<>(name2AliasOriginal);
    	fireTableStructureChanged();
    }

    
    @Override
    public String getColumnFormattingPattern(int column) {
        String columnName = this.visibleColumnNames.get(column);
        return this.getColumnFormattingPattern(columnName);
    }
    
    @Override
    public String getColumnFormattingPattern(String columnName) {
        String pattern = this.patterns.get(columnName);
        return pattern;
    }
    
    @Override
    public void setColumnFormattingPattern(String columnName, String pattern) {
        this.patterns.put(columnName,pattern);
    }
    
    @Override
    public Locale getLocaleOfData() {
        return this.localeOfData;
    }
    
    @Override
    public void setLocaleOfData(Locale locale) {
        this.localeOfData = locale;
    }
    
    @Override
    public boolean isSelectionLocked() {
        return false;
    }    

    @Override
    public boolean isSelectionUp() {
        return false;
    }    

    @Override
    public void setSelectionUp(boolean selectionUp) {
    }

    @Override
    public void update(final Observable observable, final Object notification) {
    }

    @Override
    public int getSelectionCount() {
        return 0;
    }

    @Override
    public void addChangeListener(ActionListener listener) {
        // Do nothing
    }
    
    
}
