/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.DALSwingLocator;
import org.gvsig.fmap.dal.swing.DataSwingManager;
import org.gvsig.fmap.dal.swing.queryfilter.QueryFilterExpresion;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.AbortActionException;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.DynFormManager;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynform.JDynFormSet;
import org.gvsig.tools.dynform.JDynFormSet.JDynFormSetListener;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct_v2;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.service.ServiceException;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.threadsafedialogs.ThreadSafeDialogsManager;
import org.gvsig.tools.swing.api.windowmanager.Dialog;
import org.gvsig.tools.swing.api.windowmanager.WindowManager;
import org.gvsig.tools.swing.api.windowmanager.WindowManager_v2;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;
import org.gvsig.tools.swing.icontheme.IconTheme;

/**
 * @author fdiaz
 *
 */
public class DefaultJFeaturesForm implements JFeaturesForm {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJFeaturesForm.class);

    private static final int PAGE_SIZE = 500;
    private final JPanel panel;
    private JDynFormSet formset;
    private FeatureStore store;
    private FeaturePagingHelper ph;
    private DynClass definition = null;
    private FeatureQuery currentQuery;
    private List<Action> otherActions;

    public DefaultJFeaturesForm() {
        this.panel = new JPanel(new BorderLayout());
        this.otherActions = new ArrayList<>();
    }

    @Override
    public void setPreferredSize(Dimension dimension) {
        panel.setPreferredSize(dimension);
    }

    private void updateForm() {
        if (this.formset == null) {
            this.panel.add(this.getFormset().asJComponent(), BorderLayout.CENTER);
        }
        try {
            this.ph = DALLocator.getDataManager().createFeaturePagingHelper(store, this.currentQuery, PAGE_SIZE);
            this.formset.setValues(ph.asListOfDynObjects());
        } catch (Exception ex) {
            throw new RuntimeException("Can't update form", ex);
        }
        this.formset.setReadOnly(false);
    }

    @Override
    public JComponent asJComponent() {
        if (this.ph == null) {
            try {
                updateForm();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return this.panel;
    }

    @Override
    public void bind(FeatureStore store) {
        if (store == null) {
            throw new IllegalArgumentException("bind need a store as parameter, not a null.");
        }
        try {
            this.bind(store, store.getDefaultFeatureType());
        } catch (Exception ex) {
            throw new RuntimeException("Can't bind store '" + store.getName() + "' to form", ex);
        }
    }

    public void bind(FeatureStore store, DynClass definition) throws ServiceException, DataException {
        if (this.store == store) {
            return;
        }
        if( definition == null ) {
            definition = store.getDefaultFeatureType();
        }
        if (formset != null) {
            this.panel.remove(formset.asJComponent());
            this.formset = null;
        }
        this.store = store;
        this.ph = null;
        this.definition = definition;
    }

    @Override
    public JDynFormSet getFormset() {
        if (this.formset == null) {
            DynFormManager formManager = DynFormLocator.getDynFormManager();
            try {
                this.formset = formManager.createJDynFormSet(this.definition);
            } catch (ServiceException ex) {
                throw new RuntimeException("Can't create form set.", ex);
            }

            //this.formset.setLayoutMode(JDynForm.USE_SEPARATORS);
            this.formset.setReadOnly(false);
            this.formset.setAllowClose(true);
            this.formset.setAllowDelete(false);
            this.formset.setAllowNew(false);
            this.formset.setAllowSearch(true);
            this.formset.setAllowUpdate(true);
            this.formset.setAutosave(true);

            this.formset.addAction(new FinishEditingAction());
            for( Action action : this.otherActions ) {
                this.formset.addAction(action);
            }

            this.formset.addListener(new FormSetListener());
        }
        return this.formset;
    }

    @Override
    public void addAction(Action action) {
        this.otherActions.add(action);
        if( this.formset!=null ) {
            this.formset.addAction(action);
        }
    }

    @Override
    public long getCurrentIndex() {
        if( this.formset==null ) {
            return -1;
        }
        return this.formset.getCurrentIndex();
    }

    @Override
    public Feature get(long index) {
        if( this.formset==null || this.ph==null ) {
            return null;
        }
        try {
            return this.ph.getFeatureAt(index);
        } catch (BaseException ex) {
            return null;
        }
    }

    private class FinishEditingAction extends AbstractAction implements Observer {

        public FinishEditingAction() {
            I18nManager i18nManager = ToolsLocator.getI18nManager();
            IconTheme iconTheme = ToolsSwingLocator.getIconThemeManager().getDefault();

            this.putValue(NAME,null);
            this.putValue(SHORT_DESCRIPTION,i18nManager.getTranslation("_Stop_editing"));
            this.putValue(SMALL_ICON, iconTheme.get("table-stop-editing"));
            this.putValue(ACTION_COMMAND_KEY, "finishEditing");

            this.setEnabled(store.isEditing());
            store.addObserver(this);
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if( store.isEditing() ) {
                try {
                    I18nManager i18nManager = ToolsLocator.getI18nManager();
                    ThreadSafeDialogsManager dialogManager = ToolsSwingLocator.getThreadSafeDialogsManager();
                    int x = dialogManager.confirmDialog(
                            "� Desea terminar edicion y guardar los cambios ?\n\nPulse cancelar para cancelar la edicion y los cambios.",
                            i18nManager.getTranslation("_Stop_editing"),
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE
                    );
                    switch(x) {
                        case JOptionPane.YES_OPTION:
                            store.finishEditing();
                            break;
                        case JOptionPane.NO_OPTION:
                            break;
                        case JOptionPane.CANCEL_OPTION:
                            store.cancelEditing();
                            break;
                    }
                } catch (DataException ex) {
                    LOGGER.warn("Can't finish editing in FeatureForm ("+store.getName()+").",ex);
                }
            }
        }

        @Override
        public void update(Observable observable, Object notification) {
            if( notification instanceof FeatureStoreNotification ) {
                FeatureStoreNotification n =  (FeatureStoreNotification) notification;
                switch( n.getType() )  {
                    case FeatureStoreNotification.AFTER_STARTEDITING:
                        this.setEnabled(true);
                        break;
                    case FeatureStoreNotification.AFTER_FINISHEDITING:
                        this.setEnabled(false);
                        break;
                    case FeatureStoreNotification.AFTER_CANCELEDITING:
                        this.setEnabled(false);
                        break;
                }
            }
        }

    }

    @Override
    public void setQuery(FeatureQuery query) {
        if (this.ph != null) {
            if (this.formset != null && this.formset.isAutosave() && this.formset.countValues() > 0) {
                if (!store.isEditing()) {
                    try {
                        store.edit();
                    } catch (DataException e1) {
                        throw new RuntimeException("Can't set query", e1);
                    }
                }
                saveChanges(this.formset);
            }
        }
        this.currentQuery = query;
        updateForm();
    }

    @Override
    public void showForm(MODE mode) {
        this.panel.add(this.getFormset().asJComponent(), BorderLayout.CENTER);
        WindowManager winmgr = ToolsSwingLocator.getWindowManager();
        String title;
        if( this.definition instanceof DynStruct_v2 ) {
            title = ((DynStruct_v2)this.definition).getLabel();
        } else {
            title = this.definition.getName();
        }
        winmgr.showWindow(this.asJComponent(), title, mode);
    }

    private void saveChanges(JDynFormSet dynformSet) {
        if( dynformSet.isInNewState() ) {
            Feature feat = null;
            try {
                feat = store.createNewFeature(false);
            } catch (DataException ex) {
                LOGGER.warn("Can't create new feature.",ex);
                I18nManager i18nManager = ToolsLocator.getI18nManager();
                dynformSet.message(i18nManager.getTranslation("error_saving_data_will_not_save"));
                throw new RuntimeException("Can't new save values");
            }
            DynObject dynObject = feat.getAsDynObject();
            dynformSet.getFormValues(dynObject);
            try {
                ph.insert(((FacadeOfAFeature)dynObject).getEditableFeature());
            } catch (BaseException e) {
                throw new RuntimeException("Can't save values", e);
            }
            try {
                this.formset.setValues(ph.asListOfDynObjects());
                this.formset.setCurrentIndex((int)(ph.getTotalSize())-1);
            } catch(Exception ex) {
                LOGGER.warn("Can't reload form data after insert.",ex);
            }
        } else {
            int index = dynformSet.getCurrentIndex();
            DynObject dynObject = dynformSet.get(index);

            if ( !(dynObject instanceof FacadeOfAFeature) ) {
                LOGGER.warn("Can't get the associated feature index " + index);
                I18nManager i18nManager = ToolsLocator.getI18nManager();
                dynformSet.message(i18nManager.getTranslation("error_saving_data_will_not_save"));
                throw new RuntimeException("Can't save values");
            }
            dynformSet.getFormValues(dynObject);
            try {
                ph.update(((FacadeOfAFeature)dynObject).getEditableFeature());
            } catch (BaseException e) {
                throw new RuntimeException("Can't save values", e);
            }
        }

    }

    @Override
    public void saveChanges() {
        if (this.formset != null && this.formset.countValues() > 0) {
            if (store != null && !store.isEditing()) {
                try {
                    store.edit();
                } catch (DataException e1) {
                    LOGGER.warn("Can't edit the store " + store.getName());
                    throw new RuntimeException("Can't save changes.", e1);
                }
            }
            this.saveChanges(this.formset);
        }
    }

    @Override
    public long getDataSetSize() {
        if (this.ph != null) {
            return ph.getTotalSize();
        }
        return 0;
    }

    @Override
    public FeatureStore getFeatureStore() {
        return this.store;
    }

    private class FormSetListener implements JDynFormSetListener {

        @Override
        public void formMessage(String message) {
        }

        @Override
        public void formClose() {
            panel.setVisible(false);
        }

        @Override
        public void formMovedTo(int currentPosition) throws AbortActionException {
            LOGGER.trace("formMovedTo " + currentPosition);
        }

        @Override
        public void formBeforeSave(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formBeforeSave");
            if (!store.isEditing()) {
                try {
                    store.edit();
                } catch (DataException e1) {
                    throw new StoreEditException(e1, store.getName());
                }
            }
        }

        public void formBeforeNew(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formBeforeNew");
        }

        public void formBeforeDelete(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formBeforeDelete");
        }

        @Override
        public void formAfterSave(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formAfterSave");
            saveChanges(dynformSet);
        }

        @Override
        public void formAfterNew(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formAfterNew");
        }

        @Override
        public void formAfterDelete(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formAfterDelete");
        }

        @Override
        public void formBeforeSearch(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formBeforeSearch");
            DataManager dataManager = DALLocator.getDataManager();
            DataSwingManager dataSwingmanager = DALSwingLocator.getSwingManager();
            WindowManager_v2 winmgr = (WindowManager_v2) ToolsSwingLocator.getWindowManager();

            QueryFilterExpresion querypanel = dataSwingmanager.createQueryFilterExpresion(store);
            Dialog dialog = winmgr.createDialog(
                    querypanel.asJComponent(),
                    "Filtro",
                    "Creacion de filtro sobre '"+store.getName()+"'",
                    WindowManager_v2.BUTTONS_OK_CANCEL
            );
            dialog.show(WindowManager.MODE.DIALOG);

            if( dialog.getAction() == WindowManager_v2.BUTTON_OK ) {
                String expresion = querypanel.getExpresion();
                try {
                    FeatureQuery query = store.createFeatureQuery();
                    if( !StringUtils.isEmpty(expresion) ) {
                        query.setFilter(dataManager.createExpresion(expresion));
                    }
                    FeatureSet set = store.getFeatureSet(query);
                    setQuery(query);
                } catch (Exception ex) {
                    LOGGER.warn("Can't apply filter '" + expresion + "'.", ex);
                    return;
                }
            }
        }

        @Override
        public void formAfterSearch(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formAfterSearch");
        }

        @Override
        public void formBeforeCancelNew(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formBeforeCancelNew");
        }

        @Override
        public void formAfterCancelNew(JDynFormSet dynformSet) throws AbortActionException {
            LOGGER.trace("formAfterCancelNew");
        }
    }

    private static class StoreEditException extends AbortActionException {

        /**
         *
         */
        private static final long serialVersionUID = -7682017811778577130L;

        public StoreEditException(Throwable cause, String storename) {
            super("Can't edit the store '%(storename)'", cause, "cant_edit_the store_XstorenameX", serialVersionUID);
            setValue("storename", storename);
        }
    }
}
