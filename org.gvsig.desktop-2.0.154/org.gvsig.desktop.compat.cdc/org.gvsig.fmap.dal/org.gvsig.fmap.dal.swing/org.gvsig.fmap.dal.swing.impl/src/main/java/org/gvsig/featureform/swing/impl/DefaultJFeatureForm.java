
package org.gvsig.featureform.swing.impl;

import javax.swing.JComponent;
import org.gvsig.featureform.swing.JFeatureForm;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.DynClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultJFeatureForm implements JFeatureForm {
    private static final Logger logger = LoggerFactory.getLogger(DefaultJFeatureForm.class);
    
    Feature feature;
    FeatureStore store;
    JDynForm form;
    
    @Override
    public JComponent asJComponent() {
       return this.form.asJComponent();
    }

    public DefaultJFeatureForm() {
    	this.feature = null;
        this.store = null;
    }
    
    @Override
    public void setStore(FeatureStore store) {
        if( this.store.equals(store) ) {
            return;
        }
        this.store = store;
        if( store == null ) {
            this.form = null;
            return;
        }
        try {
            FeatureType featureType = this.store.getDefaultFeatureType();
            FeatureTypeDefinitionsManager featureTypeDefinitionsManager = DALLocator.getFeatureTypeDefinitionsManager();
            DynClass dynClass = featureTypeDefinitionsManager.get(store,featureType);
            this.form = DynFormLocator.getDynFormManager().createJDynForm(dynClass);
        } catch (Exception ex) {
            logger.warn("Can't create form.",ex);
            throw new RuntimeException("Can't create form.",ex);
        }
    }
    
    @Override
    public void setFeature(Feature feature) {
        this.feature = feature;
        this.setStore(this.feature.getStore());
        this.form.setValues(feature.getAsDynObject());
    }
    
    @Override
    public JDynForm getDynForm() {
        return this.form;
    }
    
    @Override
    public void fetch(EditableFeature feature) {
        this.form.getValues(feature.getAsDynObject());
    }
    
}
