
package org.gvsig.fmap.dal.swing;

import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.table.TableModel;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.tools.exception.BaseException;


public interface FeatureTableModel extends TableModel {

    public static final String CHANGE_SELECTION = "ChangeSelection";
    
    public Locale getLocaleOfData();
    
    public void setLocaleOfData(Locale locale);

    public void setAllVisible();
    
    public int getOriginalColumnIndex(int index);
    
    public String getOriginalColumnName(int index);

    public FeatureAttributeDescriptor getColumnDescriptor(int columnIndex);

    public String getColumnFormattingPattern(int columnIndex);

    public String getColumnFormattingPattern(String columnName);

    public void setColumnFormattingPattern(String columnName, String pattern);

    public boolean isColumnVisible(String columnName);

    public void setColumnVisible(String columnName, boolean visible);

    public String getColumnAlias(String columnName);

    public void setColumnAlias(String columnName, String alias);

    public void setColumnOrder(String columnName, boolean ascending) throws BaseException;

    public Feature getFeatureAt(int rowIndex);

    public FeatureQuery getFeatureQuery();

    public FeatureStore getFeatureStore();

    public FeatureType getFeatureType();

    public FeaturePagingHelper getFeaturePager();

    public  void setSelectionUp(boolean selectionUp);
    
    public  boolean isSelectionUp();
    
    public boolean isSelectionLocked();
            
    public int getSelectionCount();
    
    public FeatureSelection getFeatureSelection();
    
    public void setFeatureSelection(FeatureSelection selection);    
    
    public void addChangeListener(ActionListener listener);
    
}
