/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.featureform.swing;

import java.awt.Dimension;
import javax.swing.Action;

import org.gvsig.fmap.dal.feature.Feature;

import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dynform.JDynFormSet;
import org.gvsig.tools.swing.api.Component;
import org.gvsig.tools.swing.api.windowmanager.WindowManager.MODE;

/**
 * @author fdiaz
 *
 */
public interface JFeaturesForm extends Component {

    public void setPreferredSize(Dimension dimension);

    public void bind(FeatureStore store);

    public void setQuery(FeatureQuery query);

    public long getDataSetSize();

    public void showForm(MODE mode);
    
    public void saveChanges();

    public JDynFormSet getFormset();
    
    public FeatureStore getFeatureStore();
    
    public void addAction(Action action);
    
    public long getCurrentIndex();
    
    public Feature get(long index);
}
