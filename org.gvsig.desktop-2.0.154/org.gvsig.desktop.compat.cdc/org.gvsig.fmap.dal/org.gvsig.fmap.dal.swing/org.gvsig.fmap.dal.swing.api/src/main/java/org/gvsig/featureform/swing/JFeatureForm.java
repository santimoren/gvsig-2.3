
package org.gvsig.featureform.swing;

import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.swing.api.Component;


public interface JFeatureForm extends Component {
    
    public void setStore(FeatureStore store);
    
    public void setFeature(Feature feature);
    
    public JDynForm getDynForm();
    
    public void fetch(EditableFeature feature);
}
