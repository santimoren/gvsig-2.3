/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.swing;

import javax.swing.ComboBoxModel;
import org.gvsig.featureform.swing.CreateJFeatureFormException;
import org.gvsig.featureform.swing.JFeatureForm;
import org.gvsig.featureform.swing.JFeaturesForm;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.swing.jdbc.JDBCConnectionPanel;
import org.gvsig.fmap.dal.swing.queryfilter.QueryFilterExpresion;
import org.gvsig.tools.dynobject.DynStruct;

/**
 * @author fdiaz
 *
 */
public interface DataSwingManager {

    public JFeaturesForm createJFeaturesForm(FeatureStore store) throws CreateJFeatureFormException;

    public JFeatureForm createJFeatureForm(FeatureStore store) throws CreateJFeatureFormException;

    public JFeatureForm createJFeatureForm(Feature feature) throws CreateJFeatureFormException;

    public JDBCConnectionPanel createJDBCConnectionPanel();

    public QueryFilterExpresion createQueryFilterExpresion(FeatureStore store);

    public FeatureTableModel createFeatureTableModel(FeatureStore featureStore, FeatureQuery featureQuery);

    public FeatureTableModel createFeatureTableModel(FeaturePagingHelper featurePager);

    public FeatureTableModel createEmptyFeatureTableModel(DynStruct struct);

    public JFeatureTable createJFeatureTable(FeatureTableModel model);

    public JFeatureTable createJFeatureTable(FeatureTableModel model, boolean isDoubleBuffered);

}
