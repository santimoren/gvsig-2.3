
package org.gvsig.fmap.dal.swing.queryfilter;

import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.swing.api.Component;


public interface QueryFilterExpresion extends Component {

    String getExpresion();

    void setExpresion(String expresion);

    void setFeatureStore(FeatureStore store);
    
}
