package org.gvsig.fmap.dal.swing.jdbc;

import org.gvsig.fmap.dal.store.jdbc.JDBCServerExplorerParameters;
import org.gvsig.tools.swing.api.Component;



public interface JDBCConnectionPanel extends Component {

    public void setServerExplorerParameters(JDBCServerExplorerParameters parameters);
    public JDBCServerExplorerParameters getServerExplorerParameters();
    public void delete();
    public void clear();
    public String getConnectionName();

}
