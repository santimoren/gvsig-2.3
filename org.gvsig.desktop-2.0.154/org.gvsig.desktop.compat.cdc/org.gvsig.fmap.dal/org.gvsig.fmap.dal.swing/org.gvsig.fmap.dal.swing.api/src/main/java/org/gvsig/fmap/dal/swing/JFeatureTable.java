
package org.gvsig.fmap.dal.swing;

import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import org.gvsig.tools.swing.api.Component;


public interface JFeatureTable extends Component {
    
    public JTable getJTable();
    
    public void addColumnSelectionListener(ActionListener listener);
    
    public FeatureTableModel getFeatureTableModel();
    
    public void setModel(TableModel dataModel);
    
    public TableModel getModel();
            
    public void setCurrent(int index);
    
    public void setVisible(boolean visible);
    
    public boolean isVisible();
    
    public int getSelectedColumnCount();

    public int[] getSelectedColumns();

    public void setVisibleStatusbar(boolean visible);
    
    public boolean isVisibleStatusbar();
}
