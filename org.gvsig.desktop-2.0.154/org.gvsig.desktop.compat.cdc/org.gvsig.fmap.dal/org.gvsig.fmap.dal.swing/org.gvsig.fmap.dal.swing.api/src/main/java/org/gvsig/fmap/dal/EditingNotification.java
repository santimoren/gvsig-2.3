/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.observer.Notification;

public interface EditingNotification extends Notification {

    public static final String BEFORE_ENTER_EDITING_STORE = "BEFORE_ENTER_EDITING_STORE";
    public static final String BEFORE_EXIT_EDITING_STORE = "BEFORE_EXIT_EDITING_STORE";
    public static final String BEFORE_INSERT_FEATURE = "BEFORE_INSERT_FEATURE";
    public static final String BEFORE_UPDATE_FEATURE = "BEFORE_UPDATE_FEATURE";
    public static final String BEFORE_REMOVE_FEATURE = "BEFORE_REMOVE_FEATURE";
    public static final String BEFORE_UPDATE_FEATURE_TYPE = "BEFORE_UPDATE_FEATURE_TYPE";

    public static final String AFTER_ENTER_EDITING_STORE = "AFTER_ENTER_EDITING_STORE";
    public static final String AFTER_EXIT_EDITING_STORE = "AFTER_EXIT_EDITING_STORE";
    public static final String AFTER_INSERT_FEATURE = "AFTER_INSERT_FEATURE";
    public static final String AFTER_UPDATE_FEATURE = "AFTER_UPDATE_FEATURE";
    public static final String AFTER_REMOVE_FEATURE = "AFTER_REMOVE_FEATURE";
    public static final String AFTER_UPDATE_FEATURE_TYPE = "AFTER_UPDATE_FEATURE_TYPE";

    public Object getSource();
    
    public Object getDocument();
    
    public Object getAuxData();
    
    public DataStore getStore();

    public FeatureStore getFeatureStore();
    
    public Feature getFeature();
    
    public EditableFeatureType getFeatureType();
    
    public boolean isCancelable();
    
    public void  setSkipFeatureValidation(boolean skip);
    
    public boolean shouldValidateTheFeature();
}
