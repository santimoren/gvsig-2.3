/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2014 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.swing;

import org.gvsig.fmap.dal.EditingNotificationManager;
import org.gvsig.tools.locator.BaseLocator;

/**
 * @author fdiaz
 *
 */
public class DALSwingLocator extends BaseLocator {

    /**
     * AttributeEditor swing manager name.
     */
    public static final String SWING_MANAGER_NAME = "dal.swing.manager";

    /**
     * AttributeEditor swing manager description.
     */
    public static final String SWING_MANAGER_DESCRIPTION = "DAL Swing Manager";

    private static final String EDITINGNOTIFICATION_MANAGER_NAME = "dal.editingnotification.manager";
    private static final String EDITINGNOTIFICATION_MANAGER_DESCRIPTION = "DAL editing notification manager";
    
    private static final String LOCATOR_NAME = "dal.swing.locator";

    /**
     * Unique instance.
     */
    private static final DALSwingLocator INSTANCE
            = new DALSwingLocator();

    /**
     * Return the singleton instance.
     *
     * @return the singleton instance
     */
    public static DALSwingLocator getInstance() {
        return INSTANCE;
    }

    /**
     * Return the Locator's name
     *
     * @return a String with the Locator's name
     */
    public final String getLocatorName() {
        return LOCATOR_NAME;
    }

    /**
     * Registers the Class implementing the PersistenceManager interface.
     *
     * @param clazz implementing the SwingManager interface
     */
    public static void registerSwingManager(Class clazz) {
        getInstance().register(SWING_MANAGER_NAME, SWING_MANAGER_DESCRIPTION,
                clazz);
    }

    /**
     * Gets the instance of the {@link DataSwingManager} registered.
     *
     * @return {@link DataSwingManager}
     */
    public static DataSwingManager getSwingManager() {
        return (DataSwingManager) getInstance()
                .get(SWING_MANAGER_NAME);
    }

    /**
     * Registers the Class implementing the EditingNotificationManager interface.
     *
     * @param clazz implementing the EditingNotificationManager interface
     */
    public static void registerEditingNotificationManager(Class clazz) {
        getInstance().register(EDITINGNOTIFICATION_MANAGER_NAME, EDITINGNOTIFICATION_MANAGER_DESCRIPTION,
                clazz);
    }

    /**
     * Gets the instance of the {@link EditingNotificationManager} registered.
     *
     * @return {@link EditingNotificationManager}
     */
    public static EditingNotificationManager getEditingNotificationManager() {
        return (EditingNotificationManager) getInstance()
                .get(EDITINGNOTIFICATION_MANAGER_NAME);
    }    
}
