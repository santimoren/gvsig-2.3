/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.tools.observer.Observable;

/**
 * 
 * Acts as a hub to centralize editing notification events in a single manager.
 * 
 * All objects that modify a store notify this manager to reported the actions 
 * to  the objects interested in knowing when start or end editing, regardless 
 * of where the editing work is done.
 * 
 */
public interface EditingNotificationManager extends Observable {
 
    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata, DataStore store, Feature feature);

    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store, Feature feature);

    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata, DataStore store);

    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store);

    public EditingNotification notifyObservers(Object source, String type, Object document, DataStore store, EditableFeatureType featuretType);

    public EditingNotification notifyObservers(Object source, String type, Object document, Object auxdata);

    public EditingNotification notifyObservers(EditingNotification notification);
    
    public boolean validateFeature(Feature feature);
}
