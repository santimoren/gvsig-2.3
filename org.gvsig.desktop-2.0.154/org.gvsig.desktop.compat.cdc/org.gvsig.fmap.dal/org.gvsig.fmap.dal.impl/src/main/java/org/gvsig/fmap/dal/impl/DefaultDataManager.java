/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.impl;

import org.gvsig.fmap.dal.ExternalStoreProviderFactory;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataServerExplorerPool;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreFactory;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.OpenErrorHandler;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.impl.DefaultEditableFeatureType;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureIndex;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.feature.paging.impl.FeaturePagingHelperImpl;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices;
import org.gvsig.fmap.dal.feature.spi.cache.FeatureCacheProvider;
import org.gvsig.fmap.dal.feature.spi.cache.FeatureCacheProviderFactory;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProvider;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProviderServices;
import org.gvsig.fmap.dal.raster.RasterStoreProviderFactory;
import org.gvsig.fmap.dal.raster.spi.CoverageStoreProvider;
import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataServerExplorerProvider;
import org.gvsig.fmap.dal.spi.DataStoreInitializer;
import org.gvsig.fmap.dal.spi.DataStoreProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.dal.store.memory.MemoryStoreParameters;
import org.gvsig.fmap.dal.store.memory.MemoryStoreProvider;
import org.gvsig.fmap.geom.DataTypes;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectEncoder;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.DynStruct_v2;
import org.gvsig.tools.dynobject.Tags;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.identitymanagement.UnauthorizedException;
import org.gvsig.tools.operations.OperationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultDataManager implements DataManager,
    DataManagerProviderServices {
    private static final Logger logger = LoggerFactory.getLogger(DefaultDataManager.class);
    
    final static private String DATA_MANAGER_STORE = "Data.manager.stores";
    final static private String DATA_MANAGER_STORE_DESCRIPTION =
        "DAL stores providers";

    final static private String DATA_MANAGER_STORE_PARAMS =
        "Data.manager.stores.params";
    final static private String DATA_MANAGER_STORE_PARAMS_DESCRIPTION =
        "DAL stores providers parameters";

    final static private String DATA_MANAGER_EXPLORER =
        "Data.manager.explorers";
    final static private String DATA_MANAGER_EXPLORER_DESCRIPTION =
        "DAL explorers providers";

    final static private String DATA_MANAGER_EXPLORER_PARAMS =
        "Data.manager.explorers.params";
    final static private String DATA_MANAGER_EXPLORER_PARAMS_DESCRIPTION =
        "DAL explorer providers parameters";

    final static private String DATA_MANAGER_INDEX = "Data.manager.indexes";
    final static private String DATA_MANAGER_INDEX_DESCRIPTION =
        "DAL index providers";

    final static private String DATA_MANAGER_CACHE = "Data.manager.caches";
    final static private String DATA_MANAGER_CACHE_DESCRIPTION =
        "DAL cache providers";

    final static private String DATA_MANAGER_EXPRESION_EVALUATOR =
        "Data.manager.expresion.evaluator";
    final static private String DATA_MANAGER_EXPRESION_EVALUATOR_DEFAULT =
        "default";
    final static private String DATA_MANAGER_EXPRESION_EVALUATOR_DESCRIPTION =
        "DAL expresion evaluators.";

    final static private String DATA_MANAGER_STORE_FACTORY =
        "Data.manager.stores.factory";
    final static private String DATA_MANAGER_STORE_FACTORY_DESCRIPTION =
        "DAL stores factories";

    final static private String DATA_MANAGER_STORE_PROVIDER_FACTORY =
        "Data.manager.providers.factory";
    final static private String DATA_MANAGER_STORE_PROVIDER_FACTORY_DESCRIPTION =
        "DAL store provider factories";
    
    final static private String DATA_MANAGER_FEATURE_ATTTRIBUTE_GETTER = 
        "Data.manager.feature.attribute.getter";
    final static private String DATA_MANAGER_FEATURE_ATTTRIBUTE_GETTER_DESCRIPTION = 
        "DAL feature attribute getters";

    /** This map contains the name of the default provider for each data type */
    private Map defaultDataIndexProviders;

    private OpenErrorHandler openErrorHandler;

    private List dataTypes;
    
    private DataServerExplorerPool dataServerExplorerPool = null;
    

    public DefaultDataManager() {
        /*
         * Create te extensions point in te registry.
         */
        ToolsLocator.getExtensionPointManager().add(DATA_MANAGER_STORE,
            DATA_MANAGER_STORE_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(DATA_MANAGER_STORE_PARAMS,
            DATA_MANAGER_STORE_PARAMS_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(DATA_MANAGER_EXPLORER,
            DATA_MANAGER_EXPLORER_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(
            DATA_MANAGER_EXPLORER_PARAMS,
            DATA_MANAGER_EXPLORER_PARAMS_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(DATA_MANAGER_INDEX,
            DATA_MANAGER_INDEX_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(
            DATA_MANAGER_EXPRESION_EVALUATOR,
            DATA_MANAGER_EXPRESION_EVALUATOR_DESCRIPTION);

        ToolsLocator.getExtensionPointManager().add(
            DATA_MANAGER_STORE_PROVIDER_FACTORY,
            DATA_MANAGER_STORE_PROVIDER_FACTORY_DESCRIPTION);

        initializeIndexes();
    }

    /**
     * 
     * @return ResourceManager
     */

    public ResourceManager getResourceManager() {
        return DALLocator.getResourceManager();
    }

    public OperationManager getOperationManager() {
        return ToolsLocator.getOperationManager();
    }

    public String getTemporaryDirectory() {
        // FIXME Define a better tempdir solution
        String tmp = System.getProperty("TMP");
        if (tmp == null) {
            tmp = System.getProperty("TEMP");
        }
        if (tmp == null) {
            File tmp_file = new File(System.getProperty("HOME"), "_daltmp_");
            int i = 1;
            while (!tmp_file.exists() || !tmp_file.isDirectory()) {
                tmp_file = new File(tmp_file.getAbsolutePath() + i);
                i++;
            }
            if (!tmp_file.exists()) {
                tmp_file.mkdir();
            }
            tmp = tmp_file.getAbsolutePath();
        }
        return tmp;
    }

    /*
     * ====================================================================
     * 
     * Store related services
     */
    @Override
    public void registerStoreProvider(String name, Class storeProviderClass,
        Class parametersClass) {
        if (name == null || storeProviderClass == null || parametersClass == null) {
            throw new IllegalArgumentException("Any parameters can be null");
        }

        if (!DataStoreParameters.class.isAssignableFrom(parametersClass)) {
            throw new IllegalArgumentException(parametersClass.getName()
                + " must implement org.gvsig.fmap.dal.DataStoreParameters");
        }

        if (CoverageStoreProvider.class.isAssignableFrom(storeProviderClass)) {
            // Envuelve al proveedor en una factoria por defecto.
            this.registerStoreProviderFactory(new DataStoreProviderToCoverageProviderFactoryWrapper(
                name, "", storeProviderClass, parametersClass));
            return;
        }

        if (FeatureStoreProvider.class.isAssignableFrom(storeProviderClass)) {
            // Envuelve al proveedor en una factoria por defecto.
            this.registerStoreProviderFactory(new DataStoreProviderToFeatureStoreProviderFactoryWrapper(
                name, "", storeProviderClass, parametersClass));
            return;
        }

        throw new IllegalArgumentException("Not supported implemtation: name="
            + name + " provider class=" + storeProviderClass.getName());

    }

    public void registerStoreProviderFactory(DataStoreProviderFactory factory) {
        ExtensionPoint factories =
            ToolsLocator.getExtensionPointManager().add(
                DATA_MANAGER_STORE_PROVIDER_FACTORY,
                DATA_MANAGER_STORE_PROVIDER_FACTORY_DESCRIPTION);
        factories.append(factory.getName(), factory.getDescription(), factory);

    }

    public void registerStoreFactory(String name, Class storeProviderClass) {
        if (name == null || storeProviderClass == null) {
            // FIXME Exception
            throw new IllegalArgumentException("Any parameters can be null");
        }

        if (!(DataStoreFactory.class.isAssignableFrom(storeProviderClass))) {
            // FIXME Exception
            throw new IllegalArgumentException(
                "Not supported implemtation: name=" + name + " provider class="
                    + storeProviderClass.getName());
        }

        ToolsLocator
            .getExtensionPointManager()
            .add(DATA_MANAGER_STORE_FACTORY,
                DATA_MANAGER_STORE_FACTORY_DESCRIPTION)
            .append(name, null, storeProviderClass);
    }

    public DataStoreParameters createStoreParameters(String name)
        throws InitializeException, ProviderNotRegisteredException {
        try {

            // Si hay una factoria para ese proveedor se los pedimos a la
            // factoria
            DataStoreProviderFactory providerFactory =
                (DataStoreProviderFactory) ToolsLocator
                    .getExtensionPointManager()
                    .get(DATA_MANAGER_STORE_PROVIDER_FACTORY).create(name);
            if (providerFactory != null) {
                return (DataStoreParameters) providerFactory.createParameters();
            }

            // Si no hay factoria lo hacemos como se hacia antes por mantener
            // compatibilidad.
            DataStoreParameters params =
                (DataStoreParameters) ToolsLocator.getExtensionPointManager()
                    .get(DATA_MANAGER_STORE_PARAMS).create(name);
            if (params == null) {
                throw new ProviderNotRegisteredException(name);
            }
            return params;
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        } catch (SecurityException e) {
            throw new InitializeException(e);
        } catch (IllegalArgumentException e) {
            throw new InitializeException(e);
        }
    }
    
    public DataStoreParameters toDataStoreParameters(String provider, DynObject doparams) throws InitializeException, ProviderNotRegisteredException {
        if (doparams instanceof DataStoreParameters) {
            return (DataStoreParameters) doparams;
        }
        String providerName;
        try {
            providerName = (String) doparams.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
        } catch(Exception ex) {
            providerName = provider;
        }
        DataStoreParameters parameters = DALLocator.getDataManager().createStoreParameters(providerName);
        ToolsLocator.getDynObjectManager().copy(doparams, parameters);
        return parameters;
    }
    
    private DataStore localOpenStore(String provider,
        DynObject doparameters) throws InitializeException,
        ProviderNotRegisteredException, ValidateDataParametersException {

        DataStoreParameters parameters = toDataStoreParameters(provider, doparameters);
        
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        if( ! identityManager.getCurrentIdentity().isAuthorized(READ_STORE_AUTHORIZATION, parameters, provider) ) {
            throw new UnauthorizedException(READ_STORE_AUTHORIZATION, parameters, provider);
        }
        
        // Usa el DynObjectEncoder y no el toString para poder ver los parametros aunque
        // el proveedor de datos sobreescriba el metodo toString.
        DynObjectEncoder encoder = ToolsLocator.getDynObjectManager().createSimpleDynObjectEncoder();
        logger.info("openStore('{}','{}')", provider, encoder.encode(parameters));
        
        String name = provider; 

        parameters.validate();

        DataStoreProviderFactory providerFactory = this.getStoreProviderFactory(name);
        if( providerFactory == null ) {
            throw new CantFindTheProviderFactoryException(name);
        }
        if( providerFactory instanceof FeatureStoreProviderFactory ) {
            DataStore store = this.createDefaultFeatureStore();
            this.intializeDataStore(store, parameters);
            return store;
            
        } else if( providerFactory instanceof RasterStoreProviderFactory ) {
            DataStore store = this.createDefaultRasterStore();
            this.intializeDataStore(store, parameters);
            return store;
            
        } else if( providerFactory instanceof ExternalStoreProviderFactory ) {
            DataStoreFactory factory = this.getStoreFactory(name);
            factory.setParameters(parameters);
            DataStore store = factory.createStore();
            this.intializeDataStore(store, parameters);
            return store;
            
        }
        throw new UnsupportedProviderFactoryException(name,providerFactory);
    }

    private static class UnsupportedProviderFactoryException extends InitializeException {
        private static final long serialVersionUID = 2635455949723618046L;

        public UnsupportedProviderFactoryException(String resourceName,DataStoreProviderFactory providerFactory) {
		super("Error intializing resource '%(resource)'. Unsupported provider factory '%(factory).", 
                    "_UnsupportedProviderFactoryException", 
                    serialVersionUID
                );
		setValue("resource", resourceName);
		setValue("factory", providerFactory.getClass().getName());
        }
        
    }
    
    private static class CantFindTheProviderFactoryException extends InitializeException {
        private static final long serialVersionUID = -2736434434177690011L;

        public CantFindTheProviderFactoryException(String resourceName) {
		super("Error intializing resource '%(resource)'. Can't find provider factory for this resource.", 
                    "_CantFindTheProviderFactoryException", 
                    serialVersionUID
                );
		setValue("resource", resourceName);
        }
        
    }
    
    private DataStore createDefaultFeatureStore() {
        return new DefaultFeatureStore();
    }
    
    private DataStore createDefaultRasterStore() {
        try {
            return (DataStore) this.defaultRasterStoreClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("Can't create the default RasterStore", ex);
        }
    }
    
    private Class defaultRasterStoreClass = null;
    public void registerDefaultRasterStore(Class rasterStoreClass) {
        this.defaultRasterStoreClass = rasterStoreClass;
    }
    
    private DataStoreFactory getStoreFactory(String name) throws InitializeException {
        ExtensionPoint.Extension point =
            ToolsLocator.getExtensionPointManager()
                .get(DATA_MANAGER_STORE_FACTORY).get(name);
        try {
            DataStoreFactory factory = (DataStoreFactory) point.create();
            return factory;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new InitializeException(e);
        }
    }
    
    public DataStore openStore(String provider, DynObject parameters)
        throws InitializeException, ProviderNotRegisteredException,
        ValidateDataParametersException {

        return localOpenStore(provider, parameters);
    }

    @Override
    public DataStore openStore(String provider, DataStoreParameters parameters) throws InitializeException, ProviderNotRegisteredException, ValidateDataParametersException {
        return localOpenStore(provider, parameters);
    }

    
    
    /**
     * @deprecated see openStore
     */
    public DataStore createStore(DynObject doparameters)
        throws InitializeException, ProviderNotRegisteredException,
        ValidateDataParametersException {
        DataStoreParameters parameters = toDataStoreParameters(null,doparameters);
        return openStore(parameters.getDataStoreName(), parameters);
    }
    
    /**
     * @deprecated see openStore
     */
    @Override
    public DataStore createStore(DataStoreParameters parameters) throws InitializeException, ProviderNotRegisteredException, ValidateDataParametersException {
        return this.openStore(parameters.getDataStoreName(), parameters);
    }

    
    public List getStoreProviders() {
        ExtensionPointManager epmanager =
            ToolsLocator.getExtensionPointManager();
        List names1 = epmanager.get(DATA_MANAGER_STORE).getNames();
        List names2 =
            epmanager.get(DATA_MANAGER_STORE_PROVIDER_FACTORY).getNames();
        List names3 = new ArrayList();
        names3.addAll(names1);
        names3.addAll(names2);
        return names3;
    }

    @Override
    public DataStoreProviderFactory getStoreProviderFactory(String name) {
        try {
            return (DataStoreProviderFactory) ToolsLocator
                .getExtensionPointManager()
                .get(DATA_MANAGER_STORE_PROVIDER_FACTORY).create(name);
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    public List getStoreProviders(String name) {
        return getStoreProviders(); // FIXME: need filter from the name of
        // the explorer
    }

    public NewDataStoreParameters createNewStoreParameters(String explorer,
        String provider) throws InitializeException,
        ProviderNotRegisteredException {

        DataServerExplorerParameters parameters;
        DataServerExplorer server;
        try {
            parameters = this.createServerExplorerParameters(explorer);
            server =
                this.openServerExplorerWithoutValidate(explorer, parameters);
            return server.getAddParameters(provider);
        } catch (ValidateDataParametersException e) {
            throw new InitializeException(e);
        } catch (DataException e) {
            throw new InitializeException(e);
        }
    }

    public void newStore(String explorer, String provider,
        NewDataStoreParameters parameters, boolean overwrite)
        throws InitializeException, ProviderNotRegisteredException,
        ValidateDataParametersException {
        
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        if( ! identityManager.getCurrentIdentity().isAuthorized(CREATE_STORE_AUTHORIZATION, parameters, provider) ) {
            throw new UnauthorizedException(CREATE_STORE_AUTHORIZATION, parameters, provider);
        }
        parameters.validate();

        DataServerExplorerParameters explorerParameters;
        DataServerExplorer server;
        explorerParameters = this.createServerExplorerParameters(explorer);
        server =
            this.openServerExplorerWithoutValidate(explorer, explorerParameters);
        try {
            server.add(provider, parameters, overwrite);
        } catch (DataException e) {
            throw new InitializeException(e);
        }
    }

    /*
     * ====================================================================
     * 
     * Explorer related services
     */
    public void registerExplorerProvider(String name, Class explorerClass,
        Class parametersClass) {

        if (name == null || explorerClass == null || parametersClass == null) {
            // FIXME Exception
            throw new IllegalArgumentException("Any parameters can be null");
        }

        if (!DataServerExplorerParameters.class
            .isAssignableFrom(parametersClass)) {
            // FIXME Exception
            throw new IllegalArgumentException(
                parametersClass.getName()
                    + " must implement org.gvsig.fmap.dal.DataServerExplorerParameters");
        }

        if (!DataServerExplorer.class.isAssignableFrom(explorerClass)) {
            // FIXME Exception
            throw new IllegalArgumentException(explorerClass.getName()
                + " must implement org.gvsig.fmap.dal.DataServerExplorer");
        }

        ToolsLocator.getExtensionPointManager()
            .add(DATA_MANAGER_EXPLORER, DATA_MANAGER_EXPLORER_DESCRIPTION)
            .append(name, null, explorerClass);

        ToolsLocator
            .getExtensionPointManager()
            .add(DATA_MANAGER_EXPLORER_PARAMS,
                DATA_MANAGER_EXPLORER_PARAMS_DESCRIPTION)
            .append(name, null, parametersClass);
    }

    public DataServerExplorerParameters createServerExplorerParameters(
        String name) throws InitializeException, ProviderNotRegisteredException {
        try {
            DataServerExplorerParameters params =
                (DataServerExplorerParameters) ToolsLocator
                    .getExtensionPointManager()
                    .get(DATA_MANAGER_EXPLORER_PARAMS).create(name);
            if (params == null) {
                throw new ProviderNotRegisteredException(name);
            }
            return params;
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        } catch (SecurityException e) {
            throw new InitializeException(e);
        } catch (IllegalArgumentException e) {
            throw new InitializeException(e);
        }
    }

    public DataServerExplorer openServerExplorer(String explorer,
        DataServerExplorerParameters parameters) throws InitializeException,
        ProviderNotRegisteredException, ValidateDataParametersException {

        if (parameters != null) {
            parameters.validate();
        }
        return this.openServerExplorerWithoutValidate(explorer, parameters);
    }

    private DataServerExplorer openServerExplorerWithoutValidate(
        String explorerName, DataServerExplorerParameters parameters)
        throws InitializeException, ProviderNotRegisteredException,
        ValidateDataParametersException {

        String name = explorerName; // parameters.getExplorerName();

        try {
            DataServerExplorerProvider server =
                (DataServerExplorerProvider) ToolsLocator
                    .getExtensionPointManager()
                    .get(DATA_MANAGER_EXPLORER)
                    .create(
                        name,
                        new Object[] { parameters,
                            new DefaultDataServerExplorerProviderServices() });
            if (server == null) {
                throw new ProviderNotRegisteredException(name);
            }
            return server;
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        } catch (SecurityException e) {
            throw new InitializeException(e);
        } catch (IllegalArgumentException e) {
            throw new InitializeException(e);
        } catch (NoSuchMethodException e) {
            throw new InitializeException(e);
        } catch (InvocationTargetException e) {
            throw new InitializeException(e);
        }
    }

    /**
     * @deprecated see openServerExplorer
     */
    public DataServerExplorer createServerExplorer(
        DataServerExplorerParameters parameters) throws InitializeException,
        ProviderNotRegisteredException, ValidateDataParametersException {
        return openServerExplorer(parameters.getExplorerName(), parameters);
    }

    public List getExplorerProviders() {
        return ToolsLocator.getExtensionPointManager()
            .get(DATA_MANAGER_EXPLORER).getNames();
    }

    /*
     * ====================================================================
     * 
     * Expresion evaluation related services
     */
    public Evaluator createExpresion(String expresion)
        throws InitializeException {
        try {
            return (Evaluator) ToolsLocator
                .getExtensionPointManager()
                .get(DATA_MANAGER_EXPRESION_EVALUATOR)
                .create(DATA_MANAGER_EXPRESION_EVALUATOR_DEFAULT,
                    new Object[] { expresion });
        } catch (SecurityException e) {
            throw new InitializeException(e);
        } catch (IllegalArgumentException e) {
            throw new InitializeException(e);
        } catch (NoSuchMethodException e) {
            throw new InitializeException(e);
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        } catch (InvocationTargetException e) {
            throw new InitializeException(e);
        }
    }

    public void registerDefaultEvaluator(Class evaluatorClass) {
        if (!Evaluator.class.isAssignableFrom(evaluatorClass)) {
            throw new ClassCastException();
        }
        ToolsLocator
            .getExtensionPointManager()
            .add(DATA_MANAGER_EXPRESION_EVALUATOR,
                DATA_MANAGER_EXPRESION_EVALUATOR_DESCRIPTION)
            .append(DATA_MANAGER_EXPRESION_EVALUATOR_DEFAULT,
                "Default expresion evaluator for use in DAL", evaluatorClass);
    }

    /*
     * ====================================================================
     * 
     * Index related services
     */

    public List getFeatureIndexProviders() {
        return ToolsLocator.getExtensionPointManager().get(DATA_MANAGER_INDEX)
            .getNames();
    }

    public void setDefaultFeatureIndexProviderName(int dataType, String name) {
        defaultDataIndexProviders.put(new Integer(dataType), name);
    }

    public String getDefaultFeatureIndexProviderName(int dataType) {
        return (String) defaultDataIndexProviders.get(new Integer(dataType));
    }

    public FeatureIndexProviderServices createFeatureIndexProvider(String name,
        FeatureStore store, FeatureType type, String indexName,
        FeatureAttributeDescriptor attr) throws InitializeException,
        ProviderNotRegisteredException {

        if (name == null) {
            name = getDefaultFeatureIndexProviderName(attr.getType());
        }

        if (name == null) {
            throw new InitializeException(
                "There not any index provider registered.", null);
        }

        try {
            FeatureIndexProvider provider =
                (FeatureIndexProvider) ToolsLocator.getExtensionPointManager()
                    .get(DATA_MANAGER_INDEX).create(name);
            if (provider == null) {
                throw new ProviderNotRegisteredException(name);
            }
            FeatureIndexProviderServices services =
                new DefaultFeatureIndex((FeatureStoreProviderServices) store,
                    type, provider, attr.getName(), indexName);
            services.initialize();
            return services;
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        } catch (SecurityException e) {
            throw new InitializeException(e);
        } catch (IllegalArgumentException e) {
            throw new InitializeException(e);
        }
    }

    public void registerFeatureIndexProvider(String name, String description,
        Class clazz, int dataType) {
        ToolsLocator.getExtensionPointManager()
            .add(DATA_MANAGER_INDEX, DATA_MANAGER_INDEX_DESCRIPTION)
            .append(name, null, clazz);

        if (getDefaultFeatureIndexProviderName(dataType) == null) {
            setDefaultFeatureIndexProviderName(dataType, name);
        }
    }

    private void initializeIndexes() {
        this.defaultDataIndexProviders = new HashMap();
    }

    public void intializeDataStore(DataStore store,
        DataStoreParameters parameters) throws InitializeException,
        ProviderNotRegisteredException {

        ((DataStoreInitializer) store).intializePhase1(this, parameters);
        DataStoreProvider provider =
            this.createProvider((DataStoreProviderServices) store, parameters);
        ((DataStoreInitializer) store).intializePhase2(provider);

    }

    public DataStoreProvider createProvider(
        DataStoreProviderServices providerServices,
            DataStoreParameters parameters) throws InitializeException,
        ProviderNotRegisteredException {
        String name = parameters.getDataStoreName();
        DataStoreProvider provider = null;
        boolean retry = true;
        while (retry) {
            try {
                DataStoreProviderFactory providerFactory =
                    (DataStoreProviderFactory) ToolsLocator
                        .getExtensionPointManager()
                        .get(DATA_MANAGER_STORE_PROVIDER_FACTORY).create(name);
                if (providerFactory != null) {
                    provider =
                        (DataStoreProvider) providerFactory.createProvider(
                            parameters, providerServices);
                } else {
                    provider =
                        (DataStoreProvider) ToolsLocator
                            .getExtensionPointManager()
                            .get(DATA_MANAGER_STORE)
                            .create(name,
                                new Object[] { parameters, providerServices });
                }
                retry = false;
            } catch (Exception e) {
                if (openErrorHandler != null) {
                    retry = openErrorHandler.canRetryOpen(e, parameters);
                } else {
                    retry = false;
                }
                if (!retry) {
                    throw new InitializeException(
                        parameters.getDataStoreName(), e);
                }
            }
        }
        if (provider == null) {
            throw new ProviderNotRegisteredException(name);
        }
        return provider;
    }

    public void registerFeatureCacheProvider(
        FeatureCacheProviderFactory providerFactory) {
        ToolsLocator.getExtensionPointManager()
            .add(DATA_MANAGER_CACHE, DATA_MANAGER_CACHE_DESCRIPTION)
            .append(providerFactory.getName(), "", providerFactory);
    }

    public FeatureCacheProvider createFeatureCacheProvider(String name,
        DynObject parameters) throws DataException {
        if (name == null) {
            throw new InitializeException(
                "It is necessary to provide a cache name", null);
        }
        if (parameters == null) {
            throw new InitializeException(
                "It is necessary to provide parameters to create the explorer",
                null);
        }
        FeatureCacheProviderFactory featureCacheProviderFactory;
        try {
            featureCacheProviderFactory =
                (FeatureCacheProviderFactory) ToolsLocator
                    .getExtensionPointManager().get(DATA_MANAGER_CACHE)
                    .create(name);
            if (featureCacheProviderFactory == null) {
                throw new ProviderNotRegisteredException(name);
            }
            return featureCacheProviderFactory.createCacheProvider(parameters);
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        }
    }

    public List getFeatureCacheProviders() {
        ExtensionPoint extensionPoint =
            ToolsLocator.getExtensionPointManager().get(DATA_MANAGER_CACHE);
        if (extensionPoint != null) {
            return ToolsLocator.getExtensionPointManager()
                .get(DATA_MANAGER_CACHE).getNames();
        } else {
            return new ArrayList();
        }
    }

    public DynObject createCacheParameters(String name)
        throws InitializeException, ProviderNotRegisteredException {
        if (name == null) {
            throw new InitializeException(
                "It is necessary to provide a cache name", null);
        }
        FeatureCacheProviderFactory featureCacheProviderFactory;
        try {
            featureCacheProviderFactory =
                (FeatureCacheProviderFactory) ToolsLocator
                    .getExtensionPointManager().get(DATA_MANAGER_CACHE)
                    .create(name);
            if (featureCacheProviderFactory == null) {
                throw new ProviderNotRegisteredException(name);
            }
            return featureCacheProviderFactory.createParameters();
        } catch (InstantiationException e) {
            throw new InitializeException(e);
        } catch (IllegalAccessException e) {
            throw new InitializeException(e);
        }
    }

    public DataStoreParameters createMemoryStoreParameters(
        String autoOrderAttributeName) throws InitializeException {

        DataStoreParameters parameters;
        try {
            parameters = createStoreParameters(MemoryStoreProvider.NAME);
            if (autoOrderAttributeName != null) {
                parameters.setDynValue(
                    MemoryStoreParameters.ORDER_PARAMETER_NAME,
                    autoOrderAttributeName);
            }
            return parameters;
        } catch (ProviderNotRegisteredException e) {
            throw new InitializeException("MemoryStoreProvider", e);
        }
    }

    public FeatureStore createMemoryStore(String autoOrderAttributeName)
        throws InitializeException {

        DataStoreParameters parameters =
            createMemoryStoreParameters(autoOrderAttributeName);
        try {
            return (FeatureStore) createStore(parameters);
        } catch (ValidateDataParametersException e) {
            throw new InitializeException("MemoryStoreProvider", e);
        } catch (ProviderNotRegisteredException e) {
            throw new InitializeException("MemoryStoreProvider", e);
        }
    }

    public FeaturePagingHelper createFeaturePagingHelper(
        FeatureStore featureStore, int pageSize) throws BaseException {
        return new FeaturePagingHelperImpl(featureStore, pageSize);
    }

    public FeaturePagingHelper createFeaturePagingHelper(
        FeatureStore featureStore, FeatureQuery featureQuery, int pageSize)
        throws BaseException {
        return new FeaturePagingHelperImpl(featureStore, featureQuery, pageSize);
    }

    public void setOpenErrorHandler(OpenErrorHandler handler) {
        openErrorHandler = handler;
    }
    
    public OpenErrorHandler getOpenErrorHandler() {
        return this.openErrorHandler;
    }
            
    public EditableFeatureType createFeatureType() {
        return new DefaultEditableFeatureType();
    }

    public List getDataTypes() {
        if (dataTypes == null) {
            DataTypesManager manager = ToolsLocator.getDataTypesManager();
            dataTypes = new ArrayList();
            dataTypes.add(manager.get(DataTypes.STRING));
            dataTypes.add(manager.get(DataTypes.BOOLEAN));
            dataTypes.add(manager.get(DataTypes.INT));
            dataTypes.add(manager.get(DataTypes.DOUBLE));
            dataTypes.add(manager.get(DataTypes.DATE));
            dataTypes.add(manager.get(DataTypes.GEOMETRY));
        }
        return dataTypes;
    }
    
    public void registerFeatureAttributeGetter(String name, Class clazz) {
        if (name == null || clazz == null) {           
            throw new IllegalArgumentException("Any parameters can be null");
        }
        if(!(FeatureAttributeGetter.class.isAssignableFrom(clazz))) {            
            throw new IllegalArgumentException(
                "Not supported implemtation: name=" + name
                + " class=" + clazz.getName());
        }
    }
    
    public FeatureAttributeGetter createFeatureAttributeGetter(String name) throws InitializeException {
        if (name == null) {           
            throw new IllegalArgumentException("The parameter can not be null");
        }

        try{
            return (FeatureAttributeGetter) ToolsLocator.getExtensionPointManager().get(DATA_MANAGER_FEATURE_ATTTRIBUTE_GETTER).create(name);
        } catch (InstantiationException e) {
            throw new InitializeException("FeatureAttributeGetter", e);
        } catch (IllegalAccessException e) {
            throw new InitializeException("FeatureAttributeGetter", e);
        }
    }

    public DataServerExplorerPool getDataServerExplorerPool() {
        if( this.dataServerExplorerPool==null )  {
            this.dataServerExplorerPool = new DefaultDataServerExplorerPool();
        }
        return this.dataServerExplorerPool;
    }
    
    public void setDataServerExplorerPool(DataServerExplorerPool pool) {
        this.dataServerExplorerPool = pool;
    }

    private static final String DAL_OpenStore = "dal.openstore.";
    
    @Override
    public DataStoreParameters createStoreParameters(DynStruct struct) throws InitializeException, ProviderNotRegisteredException {
        if( !(struct instanceof DynStruct_v2 ) ) {
            throw new IllegalArgumentException("A DynStruct_v2 is required.");
        }
        Tags tags = ((DynStruct_v2)struct).getTags();
        return this.createStoreParameters(tags);
    }

    public DataStoreParameters createStoreParameters(Tags tags) throws InitializeException, ProviderNotRegisteredException {
        String providerName = (String) tags.get(DAL_OpenStore+"provider");
        if( providerName == null ) {
            throw new IllegalArgumentException("Tag DAL.OpenStore.provider is missing in struct.");
        }
        int prefixlen = DAL_OpenStore.length();
        DataStoreParameters parameters = this.createStoreParameters(providerName);
        for( String key : tags ) {
            if( key.startsWith(DAL_OpenStore) ) {
                parameters.setDynValue(key.substring(prefixlen), tags.get(key));
            }
        }
        return parameters;
    }

    @Override
    public DataStore openStore(DynStruct struct) throws InitializeException, ProviderNotRegisteredException, ValidateDataParametersException {
        DataStoreParameters paramters = this.createStoreParameters(struct);
        DataStore store = this.openStore(paramters.getDataStoreName(), paramters);
        return store;
    }
  
}
