/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.paging.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.tools.paging.PagingCalculator.Sizeable;

/**
 * @author 2010- C�sar Ordi�ana - gvSIG team
 */
public class FeatureSetSizeableDelegate implements Sizeable {

	private static final Logger LOG =
			LoggerFactory.getLogger(FeatureSetSizeableDelegate.class);

	private final FeatureSet set;

	public FeatureSetSizeableDelegate(FeatureSet set) {
		this.set = set;
	}

	public long getSize() {
		try {
			return set.getSize();
		} catch (DataException e) {
			LOG.error("Error getting the size of the FeatureSet: " + set, e);
			return 0l;
		}
	}
}