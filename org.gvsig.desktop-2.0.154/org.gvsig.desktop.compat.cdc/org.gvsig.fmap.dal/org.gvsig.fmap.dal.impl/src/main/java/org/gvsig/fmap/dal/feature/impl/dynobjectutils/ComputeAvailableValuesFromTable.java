package org.gvsig.fmap.dal.feature.impl.dynobjectutils;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.AbstractDynMethod;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynMethodNotSupportedException;

public class ComputeAvailableValuesFromTable extends AbstractDynMethod {

    public ComputeAvailableValuesFromTable(String methodName, String description) {
        super(methodName,description);
    }

    @Override
    public Object invoke(DynObject self, Object[] args) throws DynMethodException {
        if (!(self instanceof FacadeOfAFeature)) {
            return null;
        }

        DynField_v2 field = (DynField_v2) args[0];
        String attrValueName = (String) field.getTags().get("attrValueName");
        String attrLabelName = (String) field.getTags().get("attrLabelName");
        String storeName = (String) field.getTags().get("storeName");

        Feature feature = ((FacadeOfAFeature) self).getFeature();
        return this.getAvailableValues(feature, storeName, attrLabelName, attrValueName);
    }

    private DynObjectValueItem[] getAvailableValues(Feature feature, String storeName, String attrLabelName, String attrValueName) throws ComputeAvailableValuesFromTableException {
        try {
            DataManager manager = DALLocator.getDataManager();

            FeatureStore mystore = feature.getStore();
            DataStoreParameters storeParmeters = mystore.getExplorer().get(storeName);
            FeatureStore store = (FeatureStore) manager.openStore(mystore.getProviderName(), storeParmeters);
            FeatureSet set = store.getFeatureSet();
            DynObjectValueItem[] values = new DynObjectValueItem[(int)(set.getSize())];
            int n = 0;
            DisposableIterator it = set.fastIterator();
            while( it.hasNext() ) {
                Feature f = (Feature) it.next();
                values[n++] =  new DynObjectValueItem(f.getString(attrValueName), f.getString(attrLabelName));
            }
            return values;
        } catch (Exception ex) {
            throw new ComputeAvailableValuesFromTableException(storeName, ex);
        }
    }

    private static class ComputeAvailableValuesFromTableException extends DynMethodException {

        private final static String MESSAGE_FORMAT = "Can't retrieve available values from %(storename)s.";
        private final static String MESSAGE_KEY = "_ComputeAvailableValuesFromTableException";
        private static final long serialVersionUID = -3248317756866564508L;

        public ComputeAvailableValuesFromTableException(String storename, Throwable cause) {
            super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
            setValue("storename", storename);
            this.initCause(cause);
        }
    }

}
