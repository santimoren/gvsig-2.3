/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.index.AbstractFeatureIndexProvider;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.SpatialIndex;
import org.gvsig.fmap.geom.primitive.Envelope;

public class MemorySpatialIndexProvider extends AbstractFeatureIndexProvider {

	public static final String NAME = "MemorySpatialIndexProvider";
	
	private SpatialIndex index = null;
	
	public MemorySpatialIndexProvider() {
		
	}
	
    public void initialize() {
    	try {
			this.index = GeometryLocator.getGeometryManager().createDefaultMemorySpatialIndex();
		} catch (Exception e) {
			throw new RuntimeException();
		}
    }

    public void delete(Object o, FeatureReferenceProviderServices fref) {
        Geometry geom = (Geometry) o;
        this.index.remove(geom, fref.getOID());
    }

    public void insert(Object o, FeatureReferenceProviderServices fref) {
        if (o == null ) {
            return;
        }
        Geometry geom = (Geometry) o;
        this.index.insert(geom, fref.getOID());

    }

    public List match(Object value) throws FeatureIndexException {
        Envelope env = null;
        if (value instanceof Envelope) {
            env = (Envelope) value;
        } else {
            if (value instanceof Geometry) {
                env = ((Geometry) value).getEnvelope();
            }
        }
        return new LongList(this.index.queryAsList(env));
        
    }

    public List match(Object min, Object max) {
        throw new UnsupportedOperationException(
            "Can't perform this kind of search.");
    }

    public List nearest(int count, Object value) throws FeatureIndexException {
        throw new UnsupportedOperationException(
            "Can't perform this kind of search.");
    }

    public boolean isMatchSupported() {
        return true;
    }

    public boolean isNearestSupported() {
        return false;
    }

    public boolean isNearestToleranceSupported() {
        return false;
    }

    public boolean isRangeSupported() {
        return false;
    }

    public List nearest(int count, Object value, Object tolerance)
        throws FeatureIndexException {
        throw new UnsupportedOperationException();
    }

    public List range(Object value1, Object value2)
        throws FeatureIndexException {
        throw new UnsupportedOperationException();
    }

    public void clear() throws DataException {
        this.index.removeAll();
    }
 }
