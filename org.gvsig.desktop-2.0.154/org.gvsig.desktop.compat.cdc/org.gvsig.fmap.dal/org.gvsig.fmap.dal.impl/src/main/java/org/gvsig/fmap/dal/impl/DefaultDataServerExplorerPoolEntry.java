/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gvsig.fmap.dal.impl;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataServerExplorerPoolEntry;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 *
 * @author usuario
 */
public class DefaultDataServerExplorerPoolEntry implements DataServerExplorerPoolEntry {

    private static final String PERSISTENCE_DEFINITION_NAME = "DataServerExplorerPoolEntry";

    protected String name;
    protected String description;
    protected DataServerExplorerParameters explorer;

    public DefaultDataServerExplorerPoolEntry() {
        // Required by Persistent 
    }
    
    public DefaultDataServerExplorerPoolEntry(String name, String description, DataServerExplorerParameters explorer) {
        this.name = name;
        this.description = description;
        this.explorer = explorer;

    }

    public String getName() {
        return this.name;
    }

    public void copyTo(DataServerExplorerPoolEntry target) {
        DefaultDataServerExplorerPoolEntry other = (DefaultDataServerExplorerPoolEntry)target;
        other.name = this.name;
        other.description = this.description;
        other.explorer = this.explorer;                 
    }
    
    public DataServerExplorerParameters getExplorerParameters() {
        return this.explorer;
    }

    public String getDescription() {
        return this.description;
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("name", this.name);
        state.set("description", this.description);
        state.set("explorer", this.explorer);
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        this.description = state.getString("description");
        this.name = state.getString("name");
        this.explorer = (DataServerExplorerParameters) state.get("explorer");
    }

    public static void registerPersistenceDefinition() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if ( manager.getDefinition(PERSISTENCE_DEFINITION_NAME) == null ) {
            DynStruct definition
                    = manager.addDefinition(DefaultDataServerExplorerPoolEntry.class,
                            PERSISTENCE_DEFINITION_NAME, PERSISTENCE_DEFINITION_NAME
                            + " Persistent definition", null, null);

            definition.addDynFieldString("name")
                    .setMandatory(true)
                    .setPersistent(true);
            
            definition.addDynFieldString("description")
                    .setMandatory(false)
                    .setPersistent(true);
            
            definition.addDynFieldObject("explorer")
                    .setClassOfValue(DataServerExplorerParameters.class)
                    .setMandatory(true)
                    .setPersistent(true);
        }
    }

}
