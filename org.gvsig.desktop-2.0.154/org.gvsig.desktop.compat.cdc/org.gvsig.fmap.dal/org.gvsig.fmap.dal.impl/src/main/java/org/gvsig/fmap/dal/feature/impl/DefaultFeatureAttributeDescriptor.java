/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.ArrayUtils;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataType;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynField_LabelAttribute;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynMethod;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.Tags;
import org.gvsig.tools.dynobject.exception.DynFieldIsNotAContainerException;
import org.gvsig.tools.dynobject.exception.DynFieldValidateException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.impl.DefaultTags;
import org.gvsig.tools.evaluator.AbstractEvaluator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class DefaultFeatureAttributeDescriptor implements
        FeatureAttributeDescriptor, Persistent, DynField_v2, DynField_LabelAttribute {

    protected boolean allowNull;
    protected DataType dataType;
    protected DateFormat dateFormat;
    protected Object defaultValue;
    protected int index;
    protected int maximumOccurrences;
    protected int minimumOccurrences;
    protected int size;
    protected String name;
    protected Class objectClass;
    protected int precision;
    protected Evaluator evaluator;
    protected boolean primaryKey;
    protected boolean readOnly;
    protected IProjection SRS;
    protected GeometryType geomType;
    protected int geometryType;
    protected int geometrySubType;
    protected Map additionalInfo;
    protected boolean isAutomatic;
    protected boolean isTime = false;
    protected FeatureAttributeGetter featureAttributeGetter = null;
    protected FeatureAttributeEmulator featureAttributeEmulator = null;
    protected boolean indexed = false;
    protected boolean isIndexAscending = true;
    protected boolean allowIndexDuplicateds = true;

    protected DynObjectValueItem[] availableValues;
    protected String description;
    protected Object minValue;
    protected Object maxValue;
    protected String label;
    protected int order;
    protected boolean hidden;
    protected String groupName;
    protected Tags tags = new DefaultTags();
    private DynMethod availableValuesMethod;
    private DynMethod calculateMethod;

    protected DefaultFeatureAttributeDescriptor() {
        this.allowNull = true;
        this.dataType = null;
        this.dateFormat = null;
        this.defaultValue = null;
        this.index = -1;
        this.maximumOccurrences = 0;
        this.minimumOccurrences = 0;
        this.size = 0;
        this.name = null;
        this.objectClass = null;
        this.precision = 0;
        this.evaluator = null;
        this.primaryKey = false;
        this.readOnly = false;
        this.SRS = null;
        this.geometryType = Geometry.TYPES.NULL;
        this.geometrySubType = Geometry.SUBTYPES.UNKNOWN;
        this.additionalInfo = null;
        this.isAutomatic = false;
    }

    protected DefaultFeatureAttributeDescriptor(
            DefaultFeatureAttributeDescriptor other) {
        copyFrom(other);
    }
    
    @Override
    public void copyFrom(DynField other1) {
        if( !(other1 instanceof DefaultFeatureAttributeDescriptor) ) {
            throw new IllegalArgumentException("Can't copy from a non DefaultFeatureAttributeDescriptor");
        }
        DefaultFeatureAttributeDescriptor other = (DefaultFeatureAttributeDescriptor) other1;
        this.allowNull = other.allowNull;
        this.dataType = other.dataType;
        this.dateFormat = other.dateFormat;
        this.defaultValue = other.defaultValue;
        this.index = other.index;
        this.maximumOccurrences = other.maximumOccurrences;
        this.minimumOccurrences = other.minimumOccurrences;
        this.size = other.size;
        this.name = other.name;
        this.objectClass = other.objectClass;
        this.precision = other.precision;
        this.evaluator = other.evaluator;
        this.primaryKey = other.primaryKey;
        this.readOnly = other.readOnly;
        this.SRS = other.SRS;
        this.geometryType = other.geometryType;
        this.geometrySubType = other.geometrySubType;
        this.geomType = other.geomType;
        if (other.additionalInfo != null) {
            Iterator iter = other.additionalInfo.entrySet().iterator();
            Map.Entry entry;
            this.additionalInfo = new HashMap();
            while (iter.hasNext()) {
                entry = (Entry) iter.next();
                this.additionalInfo.put(entry.getKey(), entry.getValue());
            }
        } else {
            this.additionalInfo = null;
        }
        this.isAutomatic = other.isAutomatic;
        this.isTime = other.isTime;
        this.featureAttributeEmulator = other.featureAttributeEmulator;
        this.indexed = other.indexed;
        this.isIndexAscending = other.isIndexAscending;
        this.allowIndexDuplicateds = other.allowIndexDuplicateds;
    }
    
    @Override
    public String getDataTypeName() {
        if (this.getDataType() == null) {
            return "(unknow)";
        }
        return this.getDataType().getName();
    }

    @Override
    public FeatureAttributeDescriptor getCopy() {
        return new DefaultFeatureAttributeDescriptor(this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new DefaultFeatureAttributeDescriptor(this);
    }
    
    @Override
    public boolean allowNull() {
        return allowNull;
    }

    @Override
    public DataType getDataType() {
        if (featureAttributeGetter != null) {
            return featureAttributeGetter.getDataType();
        }
        return this.dataType;
    }

    @Override
    public DateFormat getDateFormat() {
        return this.dateFormat;
    }

    @Override
    public Object getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public Evaluator getEvaluator() {
        return this.evaluator;
    }

    @Override
    public int getGeometryType() {
        return this.geometryType;
    }

    @Override
    public int getGeometrySubType() {
        return this.geometrySubType;
    }

    @Override
    public GeometryType getGeomType() {
        if (this.geomType == null) {
            try {
                this.geomType
                        = GeometryLocator.getGeometryManager().getGeometryType(
                                this.geometryType, this.geometrySubType);
            } catch (GeometryException e) {
                throw new RuntimeException(
                        "Error getting geometry type with type = "
                        + this.geometryType + ", subtype = "
                        + this.geometrySubType, e);
            }
        }
        return this.geomType;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    protected FeatureAttributeDescriptor setIndex(int index) {
        this.index = index;
        return this;
    }

    @Override
    public int getMaximumOccurrences() {
        return this.maximumOccurrences;
    }

    @Override
    public int getMinimumOccurrences() {
        return this.minimumOccurrences;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Class getObjectClass() {
        if (getDataType().getType() == DataTypes.OBJECT) {
            return objectClass;
        }
        return getDataType().getDefaultClass();
    }

    @Override
    public int getPrecision() {
        return this.precision;
    }

    @Override
    public IProjection getSRS() {
        return this.SRS;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public boolean isPrimaryKey() {
        return this.primaryKey;
    }

    @Override
    public boolean isReadOnly() {
        if (this.readOnly) {
            return true;
        }
        if (this.getEvaluator() != null) {
            return true;
        }
        if (this.featureAttributeEmulator != null) {
            return !this.featureAttributeEmulator.allowSetting();
        }
        return false;
    }

    @Override
    public Object getAdditionalInfo(String infoName) {
        if (this.additionalInfo == null) {
            return null;
        }
        return this.additionalInfo.get(infoName);
    }

    @Override
    public boolean isAutomatic() {
        return this.isAutomatic;
    }

    private boolean compareObject(Object a, Object b) {
        if (a != b) {
            if (a == null) {
                return false;
            }
            return a.equals(b);
        }
        return true;

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DefaultFeatureAttributeDescriptor)) {
            return false;
        }
        DefaultFeatureAttributeDescriptor other
                = (DefaultFeatureAttributeDescriptor) obj;

        if (this.allowNull != other.allowNull) {
            return false;
        }

        if (this.index != other.index) {
            return false;
        }

        if (!compareObject(this.name, other.name)) {
            return false;
        }

        if (this.getDataType() != other.getDataType()) {
            return false;
        }

        if (this.size != other.size) {
            return false;
        }

        if (!compareObject(this.defaultValue, other.defaultValue)) {
            return false;
        }

        if (!compareObject(this.defaultValue, other.defaultValue)) {
            return false;
        }

        if (this.primaryKey != other.primaryKey) {
            return false;
        }

        if (this.isAutomatic != other.isAutomatic) {
            return false;
        }

        if (this.readOnly != other.readOnly) {
            return false;
        }

        if (this.precision != other.precision) {
            return false;
        }

        if (this.maximumOccurrences != other.maximumOccurrences) {
            return false;
        }

        if (this.minimumOccurrences != other.minimumOccurrences) {
            return false;
        }

        if (this.geometryType != other.geometryType) {
            return false;
        }

        if (this.geometrySubType != other.geometrySubType) {
            return false;
        }

        if (!compareObject(this.evaluator, other.evaluator)) {
            return false;
        }

        if (!compareObject(this.SRS, other.SRS)) {
            return false;
        }

        if (!compareObject(this.dateFormat, other.dateFormat)) {
            return false;
        }

        if (!compareObject(this.objectClass, other.objectClass)) {
            return false;
        }

        return true;
    }

    @Override
    public void loadFromState(PersistentState state)
            throws PersistenceException {
        allowNull = state.getBoolean("allowNull");
        dataType
                = ToolsLocator.getDataTypesManager().get(state.getInt("dataType"));
        // FIXME how persist dateFormat ???
        // dateFormat;
        defaultValue = state.get("defaultValue");

        index = state.getInt("index");
        maximumOccurrences = state.getInt("maximumOccurrences");
        minimumOccurrences = state.getInt("minimumOccurrences");
        size = state.getInt("size");
        name = state.getString("name");
        try {
            objectClass = Class.forName(state.getString("objectClass"));
        } catch (ClassNotFoundException e) {
            throw new PersistenceException(e);
        }
        precision = state.getInt("precision");
        evaluator = (Evaluator) state.get("evaluator");
        primaryKey = state.getBoolean("primaryKey");
        readOnly = state.getBoolean("readOnly");
        String srsId = state.getString("srsId");
        if (srsId != null) {
            SRS = CRSFactory.getCRS(srsId);
        }
        geometryType = state.getInt("geometryType");
        geometrySubType = state.getInt("geometrySubType");
        additionalInfo = (Map) state.get("aditionalInfo");
        isAutomatic = state.getBoolean("isAutomatic");
    }

    @Override
    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("allowNull", allowNull);
        state.set("dataType", dataType);
        // FIXME how persist dateFormat ???
        // dateFormat;

        defaultValue = state.get("defaultValue");

        index = state.getInt("index");
        maximumOccurrences = state.getInt("maximumOccurrences");
        minimumOccurrences = state.getInt("minimumOccurrences");
        size = state.getInt("size");
        name = state.getString("name");
        try {
            objectClass = Class.forName(state.getString("objectClass"));
        } catch (ClassNotFoundException e) {
            throw new PersistenceException(e);
        }
        precision = state.getInt("precision");
        evaluator = (Evaluator) state.get("evaluator");
        primaryKey = state.getBoolean("primaryKey");
        readOnly = state.getBoolean("readOnly");
        String srsId = state.getString("srsId");
        if (srsId != null) {
            SRS = CRSFactory.getCRS(srsId);
        }
        geometryType = state.getInt("geometryType");
        geometrySubType = state.getInt("geometrySubType");
        additionalInfo = (Map) state.get("aditionalInfo");
        isAutomatic = state.getBoolean("isAutomatic");
    }

    /*
     * Start of DynField interface Implementation
     *
     */

    public Tags getTags() {
        return tags;
    }

    @Override
    public DynObjectValueItem[] getAvailableValues() {
        return this.availableValues;
    }

    @Override
    public String getDescription() {
        if( this.description == null ) {
            return getName();
        }
        return this.description;
    }

    @Override
    public Object getMaxValue() {
        return this.maxValue;
    }

    @Override
    public Object getMinValue() {
        return this.minValue;
    }

    @Override
    public int getTheTypeOfAvailableValues() {
        return 1;
    }

    @Override
    public int getType() {
        if (featureAttributeGetter != null) {
            return featureAttributeGetter.getDataType().getType();
        }
        return getDataType().getType();
    }

    @Override
    public boolean isMandatory() {
        return !allowNull() || isPrimaryKey();
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    @Override
    public DynField setAvailableValues(DynObjectValueItem[] values) {
        if ( ArrayUtils.isEmpty(values) ) {
            this.availableValues = null;
        } else {
            this.availableValues = values;
        }
        return this;
    }

    @Override
    public DynField setDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public DynField setMandatory(boolean mandatory) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setMaxValue(Object maxValue) {
        try {
            this.maxValue = this.coerce(maxValue);
        } catch (CoercionException e) {
            throw new IllegalArgumentException(e);
        }
        return this;
    }

    @Override
    public DynField setMinValue(Object minValue) {
        try {
            this.maxValue = this.coerce(minValue);
        } catch (CoercionException e) {
            throw new IllegalArgumentException(e);
        }
        return this;
    }

    @Override
    public DynField setPersistent(boolean persistent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setTheTypeOfAvailableValues(int type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setType(int type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setDefaultDynValue(Object defaultValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class getClassOfValue() {
        return null;
    }

    @Override
    public DynField getElementsType() {
        return null;
    }

    @Override
    public DynField setClassOfValue(Class theClass)
            throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setElementsType(DynStruct type)
            throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setElementsType(int type)
            throws DynFieldIsNotAContainerException {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setSubtype(String subtype) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void validate(Object value) throws DynFieldValidateException {

        if (value == null && !this.allowNull()) {
            throw new DynFieldValidateException(value, this, null);
        }

        try {
            this.dataType.coerce(value);
        } catch (CoercionException e) {
            throw new DynFieldValidateException(value, this, e);
        }

        /*
         * Other checks will be needed
         */
    }

    @Override
    public String getSubtype() {
        if (featureAttributeGetter != null) {
            return featureAttributeGetter.getDataType().getSubtype();
        }
        return this.dataType.getSubtype();
    }

    @Override
    public Object coerce(Object value) throws CoercionException {
        if ( value == null ) {
            return value; // O debe devolver this.defaultValue
        }
        try {
            return this.getDataType().coerce(value);
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public DynField setAvailableValues(List values) {
        if (  values == null || values.isEmpty() ) {
            this.availableValues = null;
        } else {
            this.availableValues = (DynObjectValueItem[]) values.toArray(
                new DynObjectValueItem[values.size()]
            );
        }
        return this;
    }

    @Override
    public String getGroup() {
        return this.groupName;
    }

    @Override
    public int getOder() {
        return this.order;
    }

    @Override
    public String getLabel() {
        if( this.label == null ) {
            return this.getName();
        }
        return this.label;
    }

    @Override
    public DynField setLabel(String label) {
        this.label = label;
        return this;
    }

    @Override
    public DynField setGroup(String groupName) {
        this.groupName = groupName;
        return this;
    }

    @Override
    public DynField setOrder(int order) {
        this.order = order;
        return this;
    }

    @Override
    public DynField setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

    @Override
    public boolean isHidden() {
        return this.hidden;
    }

    @Override
    public DynField setReadOnly(boolean arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isContainer() {
        return false;
    }

    @Override
    public Class getClassOfItems() {
        return null;
    }

    @Override
    public DynField setDefaultFieldValue(Object defaultValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setClassOfItems(Class theClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DynField setType(DataType type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isTime() {
        return isTime;
    }

    @Override
    public FeatureAttributeGetter getFeatureAttributeGetter() {
        return featureAttributeGetter;
    }

    @Override
    public void setFeatureAttributeGetter(
            FeatureAttributeGetter featureAttributeTransform) {
        this.featureAttributeGetter = featureAttributeTransform;
    }

    @Override
    public FeatureAttributeEmulator getFeatureAttributeEmulator() {
        return this.featureAttributeEmulator;
    }

    @Override
    public boolean isIndexed() {
        return this.indexed;
    }

    @Override
    public boolean allowIndexDuplicateds() {
        return this.allowIndexDuplicateds;
    }

    @Override
    public boolean isIndexAscending() {
        return this.isIndexAscending;
    }

    @Override
    public DynField setClassOfValue(DynStruct dynStrct) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setClassOfValue(String theClassNameOfValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getClassNameOfValue() {
        return null;
    }

    @Override
    public DynStruct getDynClassOfValue() {
        return null;
    }

    @Override
    public DynField setTypeOfItems(int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getTypeOfItems() {
        return DataTypes.INVALID;
    }

    @Override
    public DynField setClassOfItems(DynStruct dynStrct) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DynField setClassOfItems(String theClassNameOfValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getClassNameOfItems() {
        return null;
    }

    @Override
    public DynStruct getDynClassOfItems() {
        return null;
    }

    @Override
    public DynField setRelationType(int relationType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRelationType() {
        return RELATION_TYPE_NONE;
    }

    @Override
    public DynField setAvailableValues(DynMethod availableValuesMethod) {
        this.availableValuesMethod = availableValuesMethod;
        return this;
    }

    @Override
    public DynObjectValueItem[] getAvailableValues(DynObject self) {
        if( this.availableValuesMethod != null ) {
            DynObjectValueItem[] values;
            try {
                values = (DynObjectValueItem[]) this.availableValuesMethod.invoke(self,new Object[] {this});
            } catch (DynMethodException ex) {
                return this.availableValues;
            }
            if( values != null ) {
                return values;
            }
        }
        return this.availableValues;
    }

    @Override
    public DynMethod getAvailableValuesMethod() {
        return this.availableValuesMethod;
    }

    @Override
    public boolean isAvailableValuesCalculated() {
        return this.availableValuesMethod!=null;
    }

    @Override
    public DynMethod getCalculateMethod() {
        return this.calculateMethod;
    }

    @Override
    public DynField setCalculateMethod(DynMethod method) {
        this.calculateMethod = method;
        return this;
    }
    
    @Override
    public boolean isCalculated() {
        return this.calculateMethod != null;
    }
    
    @Override
    public Object getCalculatedValue(DynObject self) {
        try {
            return this.calculateMethod.invoke(self, new Object[] { this });
        } catch (DynMethodException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public DynField setValidateElements(boolean validate) {
        return this;
    }

    @Override
    public boolean getValidateElements() {
        return false;
    }

    private class ConstantValueEvaluator extends AbstractEvaluator {

        @Override
        public Object evaluate(EvaluatorData data) throws EvaluatorException {
            return defaultValue;
        }

        @Override
        public String getName() {
            return "Constant attribute " + name;
        }
    }

    public void setConstantValue(boolean isConstantValue) {
        if (isConstantValue) {
            /* Cuando un attributo tiene asociado un evaluador, este se interpreta
             * como que no debe cargarse de la fuente de datos subyacente, siendo
             * el evaluador el que se encarga de proporcionar su valor.
             * Nos limitamos a asignar un evaluador que retorna simpre el valor
             * por defecto para ese attributo.
             */
            this.evaluator = new ConstantValueEvaluator();
        } else {
            this.evaluator = null;
        }
    }

}
