/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.dynobjectutils;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.BaseWeakReferencingObservable;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * {@link DynObject} implementation to facade of a {@link FeatureSet} and allow
 * to be used as a {@link DynObjectSet}.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class DynObjectSetFeatureSetFacade extends BaseWeakReferencingObservable
    implements DynObjectSet, Observer {

    private final FeatureSet featureSet;

    private final FeatureStore store;

    private final DynObjectFeatureFacade featureFacade =
        new DynObjectFeatureFacade(null);

    private final boolean fast;

    /**
     * Creates a new facade over a given feature set, with fast dynobject
     * iteration.
     * 
     * @param featureSet
     *            to facade
     */
    public DynObjectSetFeatureSetFacade(FeatureSet featureSet,
        FeatureStore store) {
        this(featureSet, store, true);
    }

    /**
     * Creates a new facade over a given feature set, with fast dynobject
     * iteration.
     * 
     * @param featureSet
     *            to facade
     * @param fast
     *            if true try to reuse objects as much as possible to make the
     *            object iteration faster. If true, DynObjects got through the
     *            returned set must not be stored unless cloned.
     */
    public DynObjectSetFeatureSetFacade(FeatureSet featureSet,
        FeatureStore store, boolean fast) {
        this.featureSet = featureSet;
        this.store = store;
        store.addObserver(this);
        this.fast = fast;
    }

    public void dispose() {
        store.deleteObserver(this);
    }

    public void accept(final Visitor visitor, long firstValueIndex)
        throws BaseException {
        featureSet.accept(new Visitor() {

            public void visit(Object obj) throws VisitCanceledException,
                BaseException {
                DynObjectFeatureFacade feature;
                if (fast) {
                    feature = featureFacade;
                } else {
                    feature = new DynObjectFeatureFacade();
                }
                feature.setFeature(((Feature) obj).getCopy());
                visitor.visit(feature);
            }
        }, firstValueIndex);
    }

    public void accept(final Visitor visitor) throws BaseException {
        featureSet.accept(new Visitor() {

            public void visit(Object obj) throws VisitCanceledException,
                BaseException {
                featureFacade.setFeature(((Feature) obj).getCopy());
                visitor.visit(featureFacade);
            }
        });
    }

    public long getSize() throws BaseException {
        return featureSet.getSize();
    }

    public DisposableIterator iterator(long index) throws BaseException {
        if (fast) {
            return new DynObjectIteratorFeatureIteratorFacade(
                featureSet.fastIterator(index), featureFacade);
        } else {
            return new DynObjectIteratorFeatureIteratorFacade(
                featureSet.fastIterator(index));
        }
    }

    public DisposableIterator iterator() throws BaseException {
        if (fast) {
            return new DynObjectIteratorFeatureIteratorFacade(
                featureSet.fastIterator(), featureFacade);
        } else {
            return new DynObjectIteratorFeatureIteratorFacade(
                featureSet.fastIterator());
        }
    }

    public boolean isEmpty() throws BaseException {
        return featureSet.isEmpty();
    }

    public boolean isDeleteEnabled() {
        return store.isEditing();
    }

    public void delete(DynObject dynObject) throws BaseException {
        DynObjectFeatureFacade facade = (DynObjectFeatureFacade) dynObject;
        featureSet.delete(facade.getFeature());
    }

    public boolean isUpdateEnabled() {
        return store.isEditing();
    }

    public void update(DynObject dynObject) throws BaseException {
        DynObjectFeatureFacade facade = (DynObjectFeatureFacade) dynObject;
        featureSet.update(facade.getEditableFeature());
    }

    public void update(Observable observable, Object notification) {
        if (observable.equals(store)
            && notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification event =
                (FeatureStoreNotification) notification;
            if (event.getType() == FeatureStoreNotification.AFTER_STARTEDITING
                || event.getType() == FeatureStoreNotification.AFTER_FINISHEDITING
                || event.getType() == FeatureStoreNotification.AFTER_CANCELEDITING) {
                setChanged();
                notify(this, new Notification() {

                    public String getType() {
                        return Notification.EDITION_STATUS_CHANGE;
                    }
                });
            }
        }

    }
}
