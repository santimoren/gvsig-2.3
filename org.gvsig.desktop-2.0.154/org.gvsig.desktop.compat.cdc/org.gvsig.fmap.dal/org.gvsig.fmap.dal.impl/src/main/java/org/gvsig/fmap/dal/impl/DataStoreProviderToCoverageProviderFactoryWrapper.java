/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.impl;

import org.gvsig.fmap.dal.ExternalStoreProviderFactory;
import java.lang.reflect.Constructor;

import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.DataStoreProvider;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.spi.AbstractDataStoreProviderFactory;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DynObject;

public class DataStoreProviderToCoverageProviderFactoryWrapper extends
		AbstractDataStoreProviderFactory implements ExternalStoreProviderFactory {

	private Class providerClass = null;
	private Class parametersClass = null;

	public DataStoreProviderToCoverageProviderFactoryWrapper(String name, String description,
			Class provider, Class parametersClass) {
                super(name,description);
		this.providerClass = provider;
		this.parametersClass = parametersClass;
	}

        @Override
	public DataStoreProvider createProvider(DataParameters parameters,
			DataStoreProviderServices providerServices)
			throws InitializeException {
		try {
			Class[] argsTypes = new Class[] { DataParameters.class,
					DataStoreProviderServices.class };
			Object[] args = new Object[] { parameters, providerServices };

			Constructor contructor;
			contructor = findConstructor(providerClass, argsTypes);
			return (DataStoreProvider) contructor.newInstance(args);
		} catch (Throwable e) {
			throw new InitializeException(e);
		}
	}

        @Override
	public final DynObject createParameters() {
		try {
			return (DynObject) parametersClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected Constructor findConstructor(Class clazz, Class[] types)
			throws SecurityException, NoSuchMethodException {
		try {
			return clazz.getConstructor(types);
		} catch (NoSuchMethodException e) {
			// Nothing to do
		}

		// search for the required constructor
		Constructor[] constrs = clazz.getConstructors();
		boolean allMatch;
            for (Constructor constr : constrs) {
                Class[] paramTypes = constr.getParameterTypes();
                if (paramTypes.length == types.length) {
                    // a general candidate
                    allMatch = true;
                    for (int j = 0; j < paramTypes.length; j++) {
                        if (!isParameterCompatible(types[j], paramTypes[j])) {
                            allMatch = false;
                            break;
					}
				}
                    if (allMatch) {
                        return constr;
                    }
                }
            }
		StringBuilder strb = new StringBuilder();
		strb.append(clazz.getName());
		strb.append('(');
		if (types.length > 0) {
			for (int i = 0; i < types.length - 1; i++) {
				strb.append(types[i].getName());
				strb.append(',');
			}
			strb.append(types[types.length - 1].getName());
		}
		strb.append(')');
		throw new NoSuchMethodException(strb.toString());
	}

	private boolean isParameterCompatible(Class current, Class defined) {
		if (current == null) {
			return !defined.isPrimitive();
		} else {
			return current.isAssignableFrom(defined);
		}
	}

}
