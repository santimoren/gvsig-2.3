/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.dynobjectutils;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;

/**
 * {@link DynObject} implementation to facade a iterator of a {@link FeatureSet}
 * and allow to be used as a {@link DynObjectSet} iterator.
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DynObjectIteratorFeatureIteratorFacade implements
    DisposableIterator {

    private final DisposableIterator featureIterator;
    private DynObjectFeatureFacade featureFacade;

    /**
     * Creates a new DynObjects iterator facade over a feature iterator.
     * Each {@link Feature} will be returned through a new or reused
     * {@link DynObjectFeatureFacade} which allows the {@link Feature} to be
     * used like a DynObject.
     * 
     * @param featureIterator
     *            to facade
     * @param featureFacade
     *            if not null this object will be reused as the facade for the
     *            Feature objects of the feature iterator
     */
    public DynObjectIteratorFeatureIteratorFacade(
        DisposableIterator featureIterator, DynObjectFeatureFacade featureFacade) {
        this.featureIterator = featureIterator;
        this.featureFacade = featureFacade;
    }

    /**
     * Creates a new DynObjects iterator facade over a feature iterator.
     * Each {@link Feature} will be returned through a new
     * {@link DynObjectFeatureFacade} which allows the {@link Feature} to be
     * used like a DynObject.
     * 
     * @param featureIterator
     *            to facade
     */
    public DynObjectIteratorFeatureIteratorFacade(
        DisposableIterator featureIterator) {
        this.featureIterator = featureIterator;
    }

    public void dispose() {
        featureIterator.dispose();
    }

    public boolean hasNext() {
        return featureIterator.hasNext();
    }

    public Object next() {
        Feature feature = (Feature) featureIterator.next();
        if (featureFacade == null) {
        	featureFacade = new DynObjectFeatureFacade();
        }
        featureFacade.setFeature(feature.getCopy());
        return featureFacade;
    }

    public void remove() {
        featureIterator.remove();
    }

}
