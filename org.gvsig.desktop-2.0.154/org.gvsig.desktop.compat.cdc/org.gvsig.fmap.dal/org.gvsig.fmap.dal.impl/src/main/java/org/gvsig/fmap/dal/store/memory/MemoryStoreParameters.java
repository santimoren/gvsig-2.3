/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.dal.store.memory;

import org.gvsig.fmap.dal.OpenDataStoreParameters;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MemoryStoreParameters extends AbstractDataParameters implements
OpenDataStoreParameters{
	  
	private static final String PARAMETERS_DEFINITION_NAME = "MemoryStoreParameters";

	private static final String NAME_PARAMETER_NAME = "name";
	private static final String NAME_PARAMETER_DESCRIPTION = "Memory data name";
	public static final String ORDER_PARAMETER_NAME = "autoOrderAttributeName";
	private static final String ORDER_PARAMETER_DESCRIPTION = "Atribute name to order by, by default";

	private DelegatedDynObject parameters;

	public MemoryStoreParameters() {
		this(MemoryStoreProvider.NAME);
	}

	public MemoryStoreParameters(String name) {
		super();
		initialize(name);
	}

	protected void initialize(String name) {
		this.parameters = (DelegatedDynObject) ToolsLocator.getDynObjectManager()
			.createDynObject(
					ToolsLocator.getPersistenceManager().getDefinition(PARAMETERS_DEFINITION_NAME)
		);
		setDynValue(NAME_PARAMETER_NAME, name);
	}

	protected DelegatedDynObject getDelegatedDynObject() {
		return parameters;
	}
	
	   protected static void registerParametersDefinition() {
	    	PersistenceManager manager = ToolsLocator.getPersistenceManager();
	    	if( manager.getDefinition(PARAMETERS_DEFINITION_NAME) == null ) {
				DynStruct parametersDefinition = manager.addDefinition(
						MemoryStoreParameters.class, 
						PARAMETERS_DEFINITION_NAME,
						(String)null,
						null, 
						null
				);
				parametersDefinition.addDynFieldString(NAME_PARAMETER_NAME)
					.setDescription(NAME_PARAMETER_DESCRIPTION)
					.setMandatory(true);
				parametersDefinition.addDynFieldString(ORDER_PARAMETER_NAME)
					.setDescription(ORDER_PARAMETER_DESCRIPTION);
				
	    	}
	    }
	
	public String getDataStoreName() {
		return MemoryStoreProvider.NAME;
	}

	public String getDescription() {
		return MemoryStoreProvider.DESCRIPTION;		
	}

	public boolean isValid() {
		return true;
	}

	public String getName() {
		return (String) parameters.getDynValue(NAME_PARAMETER_NAME);
	}

	public String getOrderByAttributeName() {
		return (String) parameters.getDynValue(ORDER_PARAMETER_NAME);
	}
}
