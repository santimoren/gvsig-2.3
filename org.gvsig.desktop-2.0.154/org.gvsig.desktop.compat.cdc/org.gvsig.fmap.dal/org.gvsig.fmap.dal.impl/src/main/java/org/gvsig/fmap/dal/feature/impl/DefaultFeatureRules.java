/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureRule;
import org.gvsig.fmap.dal.feature.FeatureRules;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.exception.ValidateFeaturesException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultFeatureRules extends ArrayList implements FeatureRules {
        
        private final static Logger logger = LoggerFactory.getLogger(DefaultFeatureRules.class);
        
	/**
	 *
	 */
	private static final long serialVersionUID = -8084546505498274121L;

	public FeatureRule add(FeatureRule rule) {
		if (super.add(rule)) {
			return rule;
		}
		return null;
	}

	public FeatureRule getRule(int index) {
		return (FeatureRule) super.get(index);
	}

	public boolean remove(FeatureRule rule) {
		return super.remove(rule);
	}

	public FeatureRules getCopy() {
		DefaultFeatureRules copy = new DefaultFeatureRules();
		copy.addAll(this);
		return copy;
	}

        /**
         * @deprecated use validate(Feature feature, int mode)
         * @param feature
         * @throws DataException 
         */
	public void validate(Feature feature) throws DataException {
            logger.warn("Calling deprecated method validate without mode.");
            FeatureStore store = ((DefaultFeature)feature).getStore();
            Iterator featureRules=iterator();
            while (featureRules.hasNext()) {
                    FeatureRule rule = (FeatureRule) featureRules.next();
                    rule.validate(feature, store);
            }

	}

	public void validate(Feature feature, int mode) throws DataException {
            FeatureStore store = ((DefaultFeature)feature).getStore();
            Iterator featureRules=iterator();
            while (featureRules.hasNext()) {
                FeatureRule rule = (FeatureRule) featureRules.next();
                if( rule.checkAtFinishEditing() && mode == Feature.FINISH_EDITING ) {
                    rule.validate(feature, store);
                }
                if( rule.checkAtUpdate() && mode == Feature.UPDATE ) {
                    rule.validate(feature, store);
                }
            }

	}




}
