/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.text.DateFormat;
import java.util.HashMap;

import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.exception.AttributeFeatureTypeIntegrityException;
import org.gvsig.fmap.dal.feature.exception.AttributeFeatureTypeSizeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.evaluator.Evaluator;

public class DefaultEditableFeatureAttributeDescriptor extends
    DefaultFeatureAttributeDescriptor implements
    EditableFeatureAttributeDescriptor {
    
    private static Logger logger = LoggerFactory.getLogger(
        DefaultEditableFeatureAttributeDescriptor.class);

    private DefaultFeatureAttributeDescriptor source;
    private boolean hasStrongChanges;
    private String originalName = null;

    protected DefaultEditableFeatureAttributeDescriptor(
        DefaultFeatureAttributeDescriptor other) {
        super(other);
        if (other instanceof DefaultEditableFeatureAttributeDescriptor) {
            DefaultEditableFeatureAttributeDescriptor other_edi =
                (DefaultEditableFeatureAttributeDescriptor) other;
            originalName = other_edi.getOriginalName();
            this.source = other_edi.getSource();
        } else {
            this.source = other;
        }
        hasStrongChanges = false;
    }

    public DefaultEditableFeatureAttributeDescriptor() {
        super();
        this.source = null;
        hasStrongChanges = false;
    }

    public DefaultFeatureAttributeDescriptor getSource() {
        return this.source;
    }

    public void fixAll() {
    }

    public void checkIntegrity() throws AttributeFeatureTypeIntegrityException {
        AttributeFeatureTypeIntegrityException ex =
            new AttributeFeatureTypeIntegrityException(getName());
        if (this.size < 0) {
            ex.add(new AttributeFeatureTypeSizeException(this.size));
        }

        if( this.dataType.isObject() && this.objectClass == null ) {
            logger.warn("Incorrect data type object, objectClass is null.");
            ex.add(new AttributeFeatureTypeIntegrityException(this.name));
        }
        
        // TODO: Add other integrity checks...

        if (ex.size() > 0) {
            throw ex;
        }
    }

    public EditableFeatureAttributeDescriptor setAllowNull(boolean allowNull) {
        this.allowNull = allowNull;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setDataType(int type) {
        this.dataType = ToolsLocator.getDataTypesManager().get(type);
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setDefaultValue(
        Object defaultValue) {
        this.defaultValue = defaultValue;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setEvaluator(Evaluator evaluator) {
        if (this.evaluator != null && evaluator == null) {
            hasStrongChanges = true;
        }
        this.evaluator = evaluator;
        return this;
    }

    public EditableFeatureAttributeDescriptor setFeatureAttributeEmulator(FeatureAttributeEmulator featureAttributeEmulator) {
        this.featureAttributeEmulator = featureAttributeEmulator;
        return this;
    }
        
    public EditableFeatureAttributeDescriptor setGeometryType(int type) {
        this.geometryType = type;
        if( this.geometrySubType == Geometry.SUBTYPES.UNKNOWN ) {
            this.geometrySubType = Geometry.SUBTYPES.GEOM2D;
        }
        this.geomType = null;
        return this;
    }

    public EditableFeatureAttributeDescriptor setGeometrySubType(int subType) {
        this.geometrySubType = subType;
        this.geomType = null;
        return this;
    }

    public EditableFeatureAttributeDescriptor setGeometryType(
        GeometryType geometryType) {
        this.geomType = geometryType;
        this.geometryType = this.geomType.getType();
        this.geometrySubType = this.geomType.getSubType();
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    @Override
    public EditableFeatureAttributeDescriptor setGeometryType(int type, int subType) {
        this.geometryType = type;
        this.geometrySubType = subType;
        this.geomType = null;
        return this;
    }

    public EditableFeatureAttributeDescriptor setIsPrimaryKey(
        boolean isPrimaryKey) {
        this.primaryKey = isPrimaryKey;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setIsReadOnly(boolean isReadOnly) {
        this.readOnly = isReadOnly;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setMaximumOccurrences(
        int maximumOccurrences) {
        this.maximumOccurrences = maximumOccurrences;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setMinimumOccurrences(
        int minimumOccurrences) {
        this.minimumOccurrences = minimumOccurrences;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setName(String name) {
        if (originalName == null) {
            originalName = this.name;
            hasStrongChanges = true;
        }
        this.name = name;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }
    
    public String getOriginalName() {
        return originalName;
    }

    public EditableFeatureAttributeDescriptor setObjectClass(Class theClass) {
        this.objectClass = theClass;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setPrecision(int precision) {
        this.precision = precision;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setSRS(IProjection SRS) {
        this.SRS = SRS;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public EditableFeatureAttributeDescriptor setSize(int size) {
        this.size = size;
        if (this.evaluator != null) {
            hasStrongChanges = true;
        }
        return this;
    }

    public boolean hasStrongChanges() {
        return hasStrongChanges;
    }

    public EditableFeatureAttributeDescriptor setAdditionalInfo(
        String infoName, Object value) {
        if (this.additionalInfo == null) {
            this.additionalInfo = new HashMap();
        }
        this.additionalInfo.put(infoName, value);
        return this;
    }

    public EditableFeatureAttributeDescriptor setIsAutomatic(boolean isAutomatic) {
        this.isAutomatic = isAutomatic;
        return this;
    }
    
    public EditableFeatureAttributeDescriptor setIsTime(boolean isTime) {
            this.isTime = isTime;
            if( this.evaluator != null ) {
        hasStrongChanges=true;
    }
            return this;
    }

    public EditableFeatureAttributeDescriptor setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
        return this;
    }

    public EditableFeatureAttributeDescriptor setIsIndexed(boolean isIndexed) {
        this.indexed = isIndexed;
        return this;
    }
    
    public EditableFeatureAttributeDescriptor setAllowIndexDuplicateds(boolean allowDuplicateds) {
        this.allowIndexDuplicateds = allowDuplicateds;
        return this;
    }

    public EditableFeatureAttributeDescriptor setIsIndexAscending(boolean ascending) {
        this.isIndexAscending = ascending;
        return this;
    }
    
}
