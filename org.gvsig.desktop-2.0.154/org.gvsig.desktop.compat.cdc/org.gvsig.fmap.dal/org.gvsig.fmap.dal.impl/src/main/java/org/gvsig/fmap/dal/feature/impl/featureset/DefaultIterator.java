/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.featureset;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeature;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.exception.BaseException;


class DefaultIterator extends AbstractDisposable implements DisposableIterator {

	protected Iterator iterator;
	protected DefaultFeatureSet fset;
	protected Feature lastFeature = null;

	public DefaultIterator(DefaultFeatureSet featureSet) {
		this.fset = featureSet;
	}

	public DefaultIterator(DefaultFeatureSet featureSet, long index)
			throws DataException {
		this.fset = featureSet;
		if (index > 0) {
			if (featureSet.provider.canIterateFromIndex()) {
				try {
					this.iterator = featureSet.provider.iterator(index);
				} catch (UnsupportedOperationException e) {
					this.iterator = featureSet.provider.iterator();
					skypto(index);
				}
			} else {
				this.iterator = featureSet.provider.iterator();
				skypto(index);
			}
		} else {
			this.iterator = featureSet.provider.iterator();
		}
	}

	protected void skypto(long index) {
		// TODO: Comprobar si esta bien la condicion de n<=
		for (long n = 0; n <= index && this.getIterator().hasNext(); n++, this
				.getIterator()
				.next()) {
			;
		}
	}

        @Override
        public boolean hasNext() {
            if (this.fset == null) {
                return false;
            }
            fset.checkSourceStoreModified();
            if (this.getIterator().hasNext()) {
                return true;
            }
            try {
                this.doDispose();
            } catch (BaseException ex) {
                throw new RuntimeException("Can't dispose iterator.",ex);
            }
            return false;
        }

        @Override
	public Object next() {
                if( fset == null ) {
			throw new NoSuchElementException();
                }
		fset.checkSourceStoreModified();
		lastFeature = null;
		if (!this.hasNext()) {
			throw new NoSuchElementException();
		}
		try {
			lastFeature = this.createFeature((FeatureProvider) this.getIterator()
					.next());
			return lastFeature;
		} catch (DataException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove() {
		if (!fset.store.isEditing()) {
			throw new UnsupportedOperationException();
		}
		if (lastFeature == null) {
			throw new IllegalStateException();
		}
		try {
			this.fset.delete(this.lastFeature);
		} catch (DataException e) {
			// FIXME Cambiar excepcion a una Runtime de DAL
			throw new RuntimeException(e);
		}
		lastFeature = null;
	}

	protected DefaultFeature createFeature(FeatureProvider fData)
			throws DataException {
		fData.setNew(false);
		if (this.fset.transform.isEmpty()) {
			return new DefaultFeature(fset.store, fData);
		} else {
			return (DefaultFeature) this.fset.transform.applyTransform(
					new DefaultFeature(fset.store, fData), fset
							.getDefaultFeatureType());
		}
	}

	protected Iterator getIterator() {
		return this.iterator;
	}

	protected boolean isDeletedOrHasToSkip(FeatureProvider data) {
		return false;
	}

	protected void doNext() throws DataException {

	}

	protected void doDispose() throws BaseException {
		if (iterator instanceof DisposableIterator){
			((DisposableIterator)this.iterator).dispose();
		}
		this.iterator = null;
		this.fset = null;
		this.lastFeature = null;
	}

}
