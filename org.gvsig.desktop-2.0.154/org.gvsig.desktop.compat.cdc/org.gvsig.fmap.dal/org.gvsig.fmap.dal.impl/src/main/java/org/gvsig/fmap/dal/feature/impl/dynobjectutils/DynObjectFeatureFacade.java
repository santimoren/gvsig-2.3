/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.dynobjectutils;

import java.util.Map;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.lang.Cloneable;

/**
 * {@link DynObject} implementation to facade a Feature and allow to be used as
 * a {@link DynObject}.
 *
 * This implementation may be reused to be used with many {@link Feature}
 * objects, but not at the same time.
 *
 * @author gvSIG Team
 * @version $Id$
 */
public class DynObjectFeatureFacade implements DynObject, Cloneable, FacadeOfAFeature {

    private static FeatureTypeDefinitionsManager featureTypeDefinitionsManager = null;

    private Feature feature;
    private DynClass dynClass;
    private EditableFeature editable;

    private final Object lock = new Object();

    /**
     * Empty constructor.
     */
    public DynObjectFeatureFacade() {
        this(null);
    }
    /**
     * Creates a facade over a {@link Feature}.
     *
     * @param feature
     */
    public DynObjectFeatureFacade(Feature feature) {
        if( featureTypeDefinitionsManager==null ) {
            featureTypeDefinitionsManager = DALLocator.getFeatureTypeDefinitionsManager();
        }
        this.feature = feature;
        this.editable = null;
        this.dynClass = null;
    }

    @Override
    public DynClass getDynClass() {
        if( this.dynClass == null ) {
            Feature feature = this.getFeature();
            FeatureType featureType = feature.getType();
            FeatureStore store = feature.getStore();
            this.dynClass = featureTypeDefinitionsManager.get(store,featureType);
        }
        return this.dynClass;
    }

    @Override
    public Object getDynValue(String name) throws DynFieldNotFoundException {
        try {
            Object value = this.getFeature().get(name);
            return value;
        } catch(IllegalArgumentException ex) {
            DynClass dynClass = this.getDynClass();
            DynField field = dynClass.getDynField(name);
            if( field instanceof DynField_v2 && ((DynField_v2)field).isCalculated() ) {
                return  ((DynField_v2)field).getCalculatedValue(this);
            }
            throw ex;
        }
    }

    @Override
    public void setDynValue(String name, Object value)
            throws DynFieldNotFoundException {
        synchronized (lock) {
            if (editable == null) {
                editable = feature.getEditable();
            }
        }
        editable.set(name, value);
    }

    @Override
    public boolean hasDynValue(String name) {
        DynClass dynClass = this.getDynClass();
        DynField field = dynClass.getDynField(name);
        return field!=null;
    }

    @Override
    public Object invokeDynMethod(String name, Object[] args)
            throws DynMethodException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object invokeDynMethod(int code, Object[] args)
            throws DynMethodException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void implement(DynClass dynClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delegate(DynObject dynObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        // Nothing to do
    }

    @Override
    public Feature getFeature() {
        return editable == null ? feature : editable;
    }

    public void setFeature(Feature feature) {
        synchronized (lock) {
            this.feature = feature;
            this.editable = null;
        }
    }

    @Override
    public EditableFeature getEditableFeature() {
        return editable;
    }

    @Override
    public String toString() {
        if (editable != null) {
            return editable.toString();
        }
        return this.feature.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DynObjectFeatureFacade other = (DynObjectFeatureFacade) super.clone();
        if (feature != null) {
            other.feature = this.feature.getCopy();
        }
        if (editable != null) {
            other.editable = (EditableFeature) this.editable.getCopy();
        }
        return other;
    }
}
