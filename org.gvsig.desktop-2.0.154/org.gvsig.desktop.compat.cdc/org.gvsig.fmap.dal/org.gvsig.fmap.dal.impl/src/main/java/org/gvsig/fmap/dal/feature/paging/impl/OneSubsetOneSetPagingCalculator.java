/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.paging.impl;

import org.gvsig.tools.paging.DefaultPagingCalculator;

/**
 * Performs the calculations needed to be able to do pagination with two Sets of
 * data which must be viewed as a single set of data. The first set is supposed
 * to be a subset of the second one, so the size of the first set must be
 * subtracted from the second one.
 * 
 * @author gvSIG team
 */
public class OneSubsetOneSetPagingCalculator extends DefaultPagingCalculator {

    private final Sizeable firstSet;
    private final Sizeable secondSet;

    /**
     * @see AbstractPagingCalculator#AbstractPagingCalculator(int).
     * @param firstSet
     *            the first set of elements to calculate pagination for
     * @param secondSet
     *            the second set of elements to calculate pagination for
     */
    public OneSubsetOneSetPagingCalculator(Sizeable firstSet,
        Sizeable secondSet, int maxPageSize) {
        super(firstSet, maxPageSize);
        this.firstSet = firstSet;
        this.secondSet = secondSet;
    }

    /**
     * @see AbstractPagingCalculator#AbstractPagingCalculator(int, long).
     * @param firstSet
     *            the first set of elements to calculate pagination for
     * @param secondSet
     *            the second set of elements to calculate pagination for
     */
    public OneSubsetOneSetPagingCalculator(Sizeable firstSet,
        Sizeable secondSet, int maxPageSize, long currentPage) {
        super(firstSet, maxPageSize, currentPage);
        this.firstSet = firstSet;
        this.secondSet = secondSet;
    }

    public long getTotalSize() {
        return secondSet.getSize();
    }

    /**
     * Returns the size of the first set.
     * 
     * @return the first set size
     */
    public long getFirstSetSize() {
        return firstSet.getSize();
    }

    /**
     * Returns the size of the second set.
     * 
     * @return the second set size
     */
    public long getSecondSetSize() {
        return secondSet.getSize() - getFirstSetSize();
    }

    /**
     * Returns the index of the first element in the page which is available
     * into the first set, if the current page has elements on it.
     * 
     * @return the index of the first element into the first set
     */
    public long getFirstSetInitialIndex() {
        if (hasCurrentPageAnyValuesInFirstSet()) {
            return calculateFirstSetInitialIndex();
        } else {
            return -1;
        }
    }

    private long calculateFirstSetInitialIndex() {
        return getCurrentPage() * getMaxPageSize();
    }

    /**
     * Returns the index of the first element in the page which is available
     * into the second set, if the current page has elements on it.
     * 
     * @return the index of the first element into the second set
     */
    public long getSecondSetInitialIndex() {
        if (hasCurrentPageAnyValuesInSecondSet()) {
            if (hasCurrentPageAnyValuesInFirstSet()) {
                return 0;
            } else {
                return getInitialIndex() - getFirstSetSize();
            }
        } else {
            return -1;
        }
    }

    /**
     * Returns the number of elements of the current page into the first set.
     * 
     * @return the number of elements of the current page into the first set
     */
    public long getFirstSetHowMany() {
        if (hasCurrentPageAnyValuesInFirstSet()) {
            return (hasCurrentPageAllValuesInFirstSet()) ? getCurrentPageSize()
                : (getFirstSetSize() - calculateFirstSetInitialIndex());
        } else {
            return 0;
        }
    }

    /**
     * Returns the number of elements of the current page into the second set.
     * 
     * @return the number of elements of the current page into the second set
     */
    public long getSecondSetHowMany() {
        if (hasCurrentPageAnyValuesInSecondSet()) {
            return getLastIndex() - getFirstSetSize()
                - getSecondSetInitialIndex() + 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns if the current page has all its values into the first set.
     * 
     * @return if the current page has all its values into the first set
     */
    protected boolean hasCurrentPageAllValuesInFirstSet() {
        return calculateFirstSetInitialIndex() + getCurrentPageSize() <= getFirstSetSize();
    }

    /**
     * Returns if the current page has any of its values into the first set.
     * 
     * @return if the current page has any of its values into the first set
     */
    public boolean hasCurrentPageAnyValuesInFirstSet() {
        return calculateFirstSetInitialIndex() < getFirstSetSize();
    }

    /**
     * Returns if the current page has any of its values into the second set.
     * 
     * @return if the current page has any of its values into the second set
     */
    public boolean hasCurrentPageAnyValuesInSecondSet() {
        return getLastIndex() >= getFirstSetSize();
    }
}
