/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataQuery;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.CloneException;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.CreateException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureCache;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureIndexes;
import org.gvsig.fmap.dal.feature.FeatureLocks;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureReferenceSelection;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.feature.exception.AlreadyEditingException;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.feature.exception.CreateFeatureException;
import org.gvsig.fmap.dal.feature.exception.DataExportException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.exception.FinishEditingException;
import org.gvsig.fmap.dal.feature.exception.GetFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.IllegalFeatureException;
import org.gvsig.fmap.dal.feature.exception.IllegalFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.NeedEditingModeException;
import org.gvsig.fmap.dal.feature.exception.NoNewFeatureInsertException;
import org.gvsig.fmap.dal.feature.exception.NullFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.exception.PersistenceCantFindDefaultFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.PersistenceCantFindFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.PersistenceStoreAlreadyLoadedException;
import org.gvsig.fmap.dal.feature.exception.SelectionNotAllowedException;
import org.gvsig.fmap.dal.feature.exception.StoreCancelEditingException;
import org.gvsig.fmap.dal.feature.exception.StoreDeleteEditableFeatureException;
import org.gvsig.fmap.dal.feature.exception.StoreDeleteFeatureException;
import org.gvsig.fmap.dal.feature.exception.StoreEditException;
import org.gvsig.fmap.dal.feature.exception.StoreInsertFeatureException;
import org.gvsig.fmap.dal.feature.exception.StoreUpdateFeatureException;
import org.gvsig.fmap.dal.feature.exception.StoreUpdateFeatureTypeException;
import org.gvsig.fmap.dal.feature.exception.ValidateFeaturesException;
import org.gvsig.fmap.dal.feature.exception.WriteNotAllowedException;
import org.gvsig.fmap.dal.feature.impl.expansionadapter.MemoryExpansionAdapter;
import org.gvsig.fmap.dal.feature.impl.featureset.DefaultFeatureSet;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.DynObjectFeatureFacade;
import org.gvsig.fmap.dal.feature.impl.undo.DefaultFeatureCommandsStack;
import org.gvsig.fmap.dal.feature.impl.undo.FeatureCommandsStack;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices;
import org.gvsig.fmap.dal.feature.spi.cache.FeatureCacheProvider;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProviderServices;
import org.gvsig.fmap.dal.impl.DefaultDataManager;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.spi.DataStoreInitializer;
import org.gvsig.fmap.dal.spi.DataStoreProvider;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.exception.NotYetImplemented;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.undo.RedoException;
import org.gvsig.tools.undo.UndoException;
import org.gvsig.tools.undo.command.Command;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultFeatureStore extends AbstractDisposable implements
    DataStoreInitializer, FeatureStoreProviderServices, FeatureStore, Observer {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultFeatureStore.class);

    private static final String PERSISTENCE_DEFINITION_NAME = "FeatureStore";

    private DataStoreParameters parameters = null;
    private FeatureSelection selection;
    private FeatureLocks locks;

    private DelegateWeakReferencingObservable delegateObservable =
        new DelegateWeakReferencingObservable(this);

    private FeatureCommandsStack commands;
    private FeatureTypeManager featureTypeManager;
    private FeatureManager featureManager;
    private SpatialManager spatialManager;

    private FeatureType defaultFeatureType = null;
    private List featureTypes = new ArrayList();

    private int mode = MODE_QUERY;
    private long versionOfUpdate = 0;
    private boolean hasStrongChanges = true;
    private boolean hasInserts = true;

    private DefaultDataManager dataManager = null;

    private FeatureStoreProvider provider = null;

    private DefaultFeatureIndexes indexes;

    private DefaultFeatureStoreTransforms transforms;

    DelegatedDynObject metadata;

    private Set metadataChildren;

    private Long featureCount = null;

    private long temporalOid = 0;

    private FeatureCacheProvider cache;

    /*
     * TODO:
     *
     * - Comprobar que solo se pueden a�adir reglas de validacion sobre un
     * EditableFeatureType. - Comprobar que solo se puede hacer un update con un
     * featureType al que se le han cambiado las reglas de validacion cuando
     * hasStrongChanges=false.
     */

    public DefaultFeatureStore() {

    }

    public void intializePhase1(DataManager dataManager,
        DataStoreParameters parameters) throws InitializeException {

        DynObjectManager dynManager = ToolsLocator.getDynObjectManager();

        this.metadata =
            (DelegatedDynObject) dynManager.createDynObject(
                METADATA_DEFINITION_NAME, MetadataManager.METADATA_NAMESPACE);

        this.dataManager = (DefaultDataManager) dataManager;

        this.parameters = parameters;
        this.transforms = new DefaultFeatureStoreTransforms(this);
        try {
            indexes = new DefaultFeatureIndexes(this);
        } catch (DataException e) {
            throw new InitializeException(e);
        }

    }

    public void intializePhase2(DataStoreProvider provider) {
        this.provider = (FeatureStoreProvider) provider;
        this.delegate(provider);
        this.metadataChildren = new HashSet();
        this.metadataChildren.add(provider);
    }

    public DataStoreParameters getParameters() {
        return parameters;
    }

    public int getMode() {
        return this.mode;
    }

    public DataManager getManager() {
        return this.dataManager;
    }

    public Iterator getChildren() {
        return this.provider.getChilds();
    }

    public FeatureStoreProvider getProvider() {
        return this.provider;
    }

    public FeatureManager getFeatureManager() {
        return this.featureManager;
    }

    public void setFeatureTypes(List types, FeatureType defaultType) {
        this.featureTypes = types;
        this.defaultFeatureType = defaultType;
    }

    public void open() throws OpenException {
        if (this.mode != MODE_QUERY) {
            // TODO: Se puede hacer un open estando en edicion ?
            try {
                throw new IllegalStateException();
            } catch(Exception ex) {
                LOG.warn("Opening a store in editing/append mode ("+this.getFullName()+").",ex);
            }
        }
        this.notifyChange(DataStoreNotification.BEFORE_OPEN);
        this.provider.open();
        this.notifyChange(DataStoreNotification.AFTER_OPEN);
    }

    public void refresh() throws OpenException, InitializeException {
        if (this.mode != MODE_QUERY) {
            throw new IllegalStateException();
        }
        this.notifyChange(FeatureStoreNotification.BEFORE_REFRESH);
        this.featureCount = null;
        this.provider.refresh();
        this.notifyChange(FeatureStoreNotification.AFTER_REFRESH);
    }

    public void close() throws CloseException {
        if (this.mode != MODE_QUERY) {
            // TODO: Se puede hacer un close estando en edicion ?
            try {
                throw new IllegalStateException();
            } catch(Exception ex) {
                LOG.warn("Clossing a store in editing/append mode ("+this.getFullName()+").",ex);
            }
        }
        this.notifyChange(DataStoreNotification.BEFORE_CLOSE);
        this.featureCount = null;
        this.provider.close();
        this.notifyChange(DataStoreNotification.AFTER_CLOSE);
    }

    protected void doDispose() throws BaseException {
        if (this.mode != MODE_QUERY) {
            // TODO: Se puede hacer un dispose estando en edicion ?
            try {
                throw new IllegalStateException();
            } catch(Exception ex) {
                LOG.warn("Dispossing a store in editing/append mode ("+this.getFullName()+").",ex);
            }
        }
        this.notifyChange(DataStoreNotification.BEFORE_DISPOSE);
        this.disposeIndexes();
        this.provider.dispose();
        if (this.selection != null) {
            this.selection.dispose();
            this.selection = null;
        }
        this.commands = null;
        this.featureCount = null;
        if (this.locks != null) {
            // this.locks.dispose();
            this.locks = null;
        }

        if (this.featureTypeManager != null) {
            this.featureTypeManager.dispose();
            this.featureTypeManager = null;
        }

        this.featureManager = null;
        this.spatialManager = null;

        this.parameters = null;
        this.notifyChange(DataStoreNotification.AFTER_DISPOSE);
        if (delegateObservable != null) {
            this.delegateObservable.deleteObservers();
            this.delegateObservable = null;
        }
    }

    public boolean allowWrite() {
        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        if( ! identityManager.getCurrentIdentity().isAuthorized(DataManager.WRITE_STORE_AUTHORIZATION,this.getParameters(), this.getName()) ) {
            return false;
        }
        return this.provider.allowWrite();
    }

    public boolean canWriteGeometry(int geometryType) throws DataException {
        return this.provider.canWriteGeometry(geometryType, 0);
    }

    public DataServerExplorer getExplorer() throws ReadException,
        ValidateDataParametersException {
        return this.provider.getExplorer();
    }

    /*
     * public Metadata getMetadata() throws MetadataNotFoundException {
     * // TODO:
     * // Si el provider devuelbe null habria que ver de construir aqui
     * // los metadatos basicos, como el Envelope y el SRS.
     *
     * // TODO: Estando en edicion el Envelope deberia de
     * // actualizarse usando el spatialManager
     * return this.provider.getMetadata();
     * }
     */

    public Envelope getEnvelope() throws DataException {
        if (this.mode == MODE_FULLEDIT) {
        	// Just in case another thread tries to write in the store
        	synchronized (this) {
        		return this.spatialManager.getEnvelope();
			}
        }
        if (hasDynValue(DataStore.METADATA_ENVELOPE)){
            return (Envelope)getDynValue(DataStore.METADATA_ENVELOPE);
        }
        return this.provider.getEnvelope();
    }

    /**
     * @deprecated use getDefaultFeatureType().getDefaultSRS()
     */
    public IProjection getSRSDefaultGeometry() throws DataException {
        return this.getDefaultFeatureType().getDefaultSRS();
    }

    public FeatureSelection createDefaultFeatureSelection()
        throws DataException {
        return new DefaultFeatureSelection(this);
    }

    public FeatureProvider createDefaultFeatureProvider(FeatureType type)
        throws DataException {
        if (type.hasOID()) {
            return new DefaultFeatureProvider(type,
                this.provider.createNewOID());
        }
        return new DefaultFeatureProvider(type);
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        /*if (this.mode != FeatureStore.MODE_QUERY) {
            throw new PersistenceException(new IllegalStateException(
                this.getName()));
        }*/
        state.set("dataStoreName", this.getName());
        state.set("parameters", this.parameters);
        state.set("selection", this.selection);
        state.set("transforms", this.transforms);
        // TODO locks persistence
        // state.set("locks", this.locks);
        // TODO indexes persistence
        // state.set("indexes", this.indexes);
        Map evaluatedAttr = new HashMap(1);
        Iterator iterType = featureTypes.iterator();
        Iterator iterAttr;
        FeatureType type;
        DefaultFeatureAttributeDescriptor attr;
        List attrs;
        while (iterType.hasNext()) {
            type = (FeatureType) iterType.next();
            attrs = new ArrayList();
            iterAttr = type.iterator();
            while (iterAttr.hasNext()) {
                attr = (DefaultFeatureAttributeDescriptor) iterAttr.next();
                if ((attr.getEvaluator() != null)
                    && (attr.getEvaluator() instanceof Persistent)) {
                    attrs.add(attr);
                }
            }
            if (!attrs.isEmpty()) {
                evaluatedAttr.put(type.getId(), attrs);
            }

        }

        if (evaluatedAttr.isEmpty()) {
            evaluatedAttr = null;
        }

        state.set("evaluatedAttributes", evaluatedAttr);
        state.set("defaultFeatureTypeId", defaultFeatureType.getId());

    }

    public void loadFromState(PersistentState state)
        throws PersistenceException {
        if (this.provider != null) {
            throw new PersistenceStoreAlreadyLoadedException(this.getName());
        }
        if (this.getManager() == null) {
            this.dataManager = (DefaultDataManager) DALLocator.getDataManager();
        }

        DataStoreParameters params =
            (DataStoreParameters) state.get("parameters");

        try {

            this.dataManager.intializeDataStore(this, params);
            this.selection = (FeatureSelection) state.get("selection");
            this.transforms =
                (DefaultFeatureStoreTransforms) state.get("transforms");
            Map evaluatedAttributes = (Map) state.get("evaluatedAttributes");
            if ((evaluatedAttributes != null) && !evaluatedAttributes.isEmpty()) {
                List attrs;
                Iterator iterEntries =
                    evaluatedAttributes.entrySet().iterator();
                Entry entry;
                while (iterEntries.hasNext()) {
                    entry = (Entry) iterEntries.next();
                    attrs = (List) entry.getValue();
                    if (attrs.isEmpty()) {
                        continue;
                    }
                    int fTypePos = -1;
                    DefaultFeatureType type = null;
                    for (int i = 0; i < featureTypes.size(); i++) {
                        type = (DefaultFeatureType) featureTypes.get(i);
                        if (type.getId().equals(entry.getKey())) {
                            fTypePos = i;
                            break;
                        }
                    }
                    if (fTypePos < 0) {
                        throw new PersistenceCantFindFeatureTypeException(
                            this.getName(), (String) entry.getKey());
                    }
                    DefaultEditableFeatureType eType =
                        (DefaultEditableFeatureType) type.getEditable();
                    Iterator iterAttr = attrs.iterator();
                    FeatureAttributeDescriptor attr;
                    while (iterAttr.hasNext()) {
                        attr = (FeatureAttributeDescriptor) iterAttr.next();
                        eType.addLike(attr);
                    }
                    featureTypes.set(fTypePos, eType.getNotEditableCopy());

                }

            }

            String defFTypeid = state.getString("defaultFeatureTypeId");
            FeatureType ftype = null;

            if (this.defaultFeatureType == null ||
                this.defaultFeatureType.getId() == null ||
                !this.defaultFeatureType.getId().equals(
                state.getString("defaultFeatureTypeId"))) {

                ftype = getFeatureType(defFTypeid);
                if (ftype == null) {
                	/*
                	 * Un error en el m�todo de PostgresQL getName(), hace que 
                	 * el nombre del featureType sea valor retornado por el getProviderName()
                	 * De momento se pone este parche para apa�arlo y poder mantener compatibilidad
                	 * con proyectos antiguos (2.1 y 2.2)
                	 */
                	ftype = getFeatureType(provider.getName());
                	if(ftype == null){
                		throw new PersistenceCantFindDefaultFeatureTypeException(
                				this.getName(), defFTypeid);
                	}
                }
                this.defaultFeatureType = ftype;
            }
            LOG.info("loadFromState() {} {}.", provider.getProviderName(), parameters.toString());

        } catch (InitializeException e) {
            throw new PersistenceException(e);
        } catch (DataException e) {
            throw new PersistenceException(e);
        }

    }

    public static void registerPersistenceDefinition() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if (manager.getDefinition(PERSISTENCE_DEFINITION_NAME) == null) {
            DynStruct definition =
                manager.addDefinition(DefaultFeatureStore.class,
                    PERSISTENCE_DEFINITION_NAME, PERSISTENCE_DEFINITION_NAME
                        + " Persistent definition", null, null);
            definition.addDynFieldString("dataStoreName").setMandatory(true)
                .setPersistent(true);

            definition.addDynFieldObject("parameters")
                .setClassOfValue(DynObject.class).setMandatory(true)
                .setPersistent(true);

            definition.addDynFieldObject("selection")
                .setClassOfValue(FeatureSelection.class).setMandatory(false)
                .setPersistent(true);

            definition.addDynFieldObject("transforms")
                .setClassOfValue(DefaultFeatureStoreTransforms.class)
                .setMandatory(true).setPersistent(true);

            definition.addDynFieldMap("evaluatedAttributes")
                .setClassOfItems(List.class) // List<DefaultFeatureAttributeDescriptor>
                .setMandatory(false).setPersistent(true);

            definition.addDynFieldString("defaultFeatureTypeId")
                .setMandatory(true).setPersistent(true);
        }
    }

    public static void registerMetadataDefinition() throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(METADATA_DEFINITION_NAME) == null) {
            DynStruct metadataDefinition =
                manager.addDefinition(METADATA_DEFINITION_NAME, null);
            metadataDefinition.extend(manager
                .getDefinition(DataStore.METADATA_DEFINITION_NAME));
        }
    }

    //
    // ====================================================================
    // Gestion de la seleccion
    //

    public void setSelection(DataSet selection) throws DataException {
        this.setSelection((FeatureSet) selection);
    }

    public DataSet createSelection() throws DataException {
        return createFeatureSelection();
    }

    public DataSet getSelection() throws DataException {
        return this.getFeatureSelection();
    }

    public void setSelection(FeatureSet selection) throws DataException {
        setSelection(selection, true);
    }

    /**
     * @see #setSelection(FeatureSet)
     * @param undoable
     *            if the action must be undoable
     */
    public void setSelection(FeatureSet selection, boolean undoable)
        throws DataException {
        if (selection == null) {
            if (undoable) {
                throw new SelectionNotAllowedException(getName());
            }

        } else {
            if (selection.equals(this.selection)) {
                return;
            }
            if (!selection.isFromStore(this)) {
                throw new SelectionNotAllowedException(getName());
            }
        }

        if (this.selection != null) {
            this.selection.deleteObserver(this);
        }
        if (selection == null) {
            if (this.selection != null) {
                this.selection.dispose();
            }
            this.selection = null;
            return;
        }
        if (selection instanceof FeatureSelection) {
            if (undoable && isEditing()) {
                commands.selectionSet(this, this.selection,
                    (FeatureSelection) selection);
            }
            if (this.selection != null) {
                this.selection.dispose();
            }
            this.selection = (FeatureSelection) selection;
        } else {
            if (undoable && isEditing()) {
                commands.startComplex("_selectionSet");
            }
            if (selection instanceof DefaultFeatureSelection) {
                DefaultFeatureSelection defSelection =
                    (DefaultFeatureSelection) selection;
                defSelection.deselectAll(undoable);
                defSelection.select(selection, undoable);
            } else {
                this.selection.deselectAll();
                this.selection.select(selection);
            }
            if (undoable && isEditing()) {
                commands.endComplex();
            }
        }
        this.selection.addObserver(this);

        this.notifyChange(DataStoreNotification.SELECTION_CHANGE);
    }

    public FeatureSelection createFeatureSelection() throws DataException {
        return this.provider.createFeatureSelection();
    }

    public FeatureSelection getFeatureSelection() throws DataException {
        if (selection == null) {
            this.selection = createFeatureSelection();
            this.selection.addObserver(this);
        }
        return selection;
    }

    //
    // ====================================================================
    // Gestion de notificaciones
    //

    public void notifyChange(String notification) {
        if (delegateObservable != null) {
            notifyChange(new DefaultFeatureStoreNotification(this, notification));
        }

    }

    public void notifyChange(String notification, FeatureProvider data) {
        try {
            notifyChange(notification, createFeature(data));
        } catch (DataException ex) {
            LOG.error("Error notifying about the notification: " + notification
                + ", with the data: " + data, ex);
        }
    }

    public void notifyChange(String notification, Feature feature) {
        notifyChange(new DefaultFeatureStoreNotification(this, notification,
            feature));
    }

    public void notifyChange(String notification, Command command) {
        notifyChange(new DefaultFeatureStoreNotification(this, notification,
            command));
    }

    public void notifyChange(String notification, EditableFeatureType type) {
        notifyChange(new DefaultFeatureStoreNotification(this, notification,
            type));
    }

    public void notifyChange(FeatureStoreNotification storeNotification) {
        delegateObservable.notifyObservers(storeNotification);
    }

    public void notifyChange(String notification, Resource resource) {
        notifyChange(new DefaultFeatureStoreNotification(this,
            DataStoreNotification.RESOURCE_CHANGED));
    }

    //
    // ====================================================================
    // Gestion de bloqueos
    //

    public boolean isLocksSupported() {
        return this.provider.isLocksSupported();
    }

    public FeatureLocks getLocks() throws DataException {
        if (!this.provider.isLocksSupported()) {
            LOG.warn("Locks not supported");
            return null;
        }
        if (locks == null) {
            this.locks = this.provider.createFeatureLocks();
        }
        return locks;
    }

    //
    // ====================================================================
    // Interface Observable
    //

    public void disableNotifications() {
        this.delegateObservable.disableNotifications();

    }

    public void enableNotifications() {
        this.delegateObservable.enableNotifications();
    }

    public void beginComplexNotification() {
        this.delegateObservable.beginComplexNotification();

    }

    public void endComplexNotification() {
        this.delegateObservable.endComplexNotification();

    }

    public void addObserver(Observer observer) {
        if (delegateObservable != null) {
            this.delegateObservable.addObserver(observer);
        }
    }

    public void deleteObserver(Observer observer) {
        if (delegateObservable != null) {
            this.delegateObservable.deleteObserver(observer);
        }
    }

    public void deleteObservers() {
        this.delegateObservable.deleteObservers();

    }

    //
    // ====================================================================
    // Interface Observer
    //
    // Usado para observar:
    // - su seleccion
    // - sus bloqueos
    // - sus recursos
    //

    public void update(Observable observable, Object notification) {
        if (observable instanceof FeatureSet) {
            if (observable == this.selection) {
                this.notifyChange(DataStoreNotification.SELECTION_CHANGE);
            } else
                if (observable == this.locks) {
                    this.notifyChange(FeatureStoreNotification.LOCKS_CHANGE);
                }

        } else
            if (observable instanceof FeatureStoreProvider) {
                if (observable == this.provider) {

                }

            }
    }

    //
    // ====================================================================
    // Edicion
    //

    private void newVersionOfUpdate() {
        this.versionOfUpdate++;
    }

    private long currentVersionOfUpdate() {
        return this.versionOfUpdate;
    }

    private void checkInEditingMode() throws NeedEditingModeException {
        if (mode != MODE_FULLEDIT) {
            throw new NeedEditingModeException(this.getName());
        }
    }

    private void checkNotInAppendMode() throws IllegalStateException {
        if (mode == MODE_APPEND) {
			throw new IllegalStateException("Error: store "
					+ this.getFullName() + " is in append mode");
        }
    }

    private void checkIsOwnFeature(Feature feature)
        throws IllegalFeatureException {
        if (((DefaultFeature) feature).getStore() != this) {
            throw new IllegalFeatureException(this.getName());
        }
        // FIXME: fixFeatureType no vale para el checkIsOwnFeature
        // fixFeatureType((DefaultFeatureType) feature.getType());
    }

    private void exitEditingMode() {
        if (commands != null) {
            commands.clear();
            commands = null;
        }

        if (featureTypeManager != null) {
            featureTypeManager.dispose();
            featureTypeManager = null;

        }

        // TODO implementar un dispose para estos dos
        featureManager = null;
        spatialManager = null;

        featureCount = null;

        mode = MODE_QUERY;
        hasStrongChanges = true; // Lo deja a true por si las moscas
        hasInserts = true;
    }

    synchronized public void edit() throws DataException {
        edit(MODE_FULLEDIT);
    }

    synchronized public void edit(int mode) throws DataException {
        LOG.debug("Starting editing in mode: {}", new Integer(mode));
        try {
            if (this.mode != MODE_QUERY) {
                throw new AlreadyEditingException(this.getName());
            }
            if (!this.provider.supportsAppendMode()) {
                mode = MODE_FULLEDIT;
            }
            switch (mode) {
            case MODE_QUERY:
                throw new IllegalStateException(this.getName());

            case MODE_FULLEDIT:
                if (!this.transforms.isEmpty()) {
                    throw new IllegalStateException(this.getName());
                }
                notifyChange(FeatureStoreNotification.BEFORE_STARTEDITING);
                invalidateIndexes();
                featureManager =
                    new FeatureManager(new MemoryExpansionAdapter());
                featureTypeManager =
                    new FeatureTypeManager(this, new MemoryExpansionAdapter());
                spatialManager =
                    new SpatialManager(this, provider.getEnvelope());

                commands =
                    new DefaultFeatureCommandsStack(this, featureManager,
                        spatialManager, featureTypeManager);
                this.mode = MODE_FULLEDIT;
                hasStrongChanges = false;
                hasInserts = false;
                notifyChange(FeatureStoreNotification.AFTER_STARTEDITING);
                break;
            case MODE_APPEND:
                if (!this.transforms.isEmpty()) {
                    throw new IllegalStateException(this.getName());
                }
                notifyChange(FeatureStoreNotification.BEFORE_STARTEDITING);
                invalidateIndexes();
                this.provider.beginAppend();
                this.mode = MODE_APPEND;
                hasInserts = false;
                notifyChange(FeatureStoreNotification.AFTER_STARTEDITING);
                break;
            }
        } catch (Exception e) {
            throw new StoreEditException(e, this.getName());
        }
    }

    private void invalidateIndexes() {
        setIndexesValidStatus(false);
    }

    private void setIndexesValidStatus(boolean valid) {
        FeatureIndexes indexes = getIndexes();
        LOG.debug("Setting the store indexes to valid status {}: {}", (valid
            ? Boolean.TRUE : Boolean.FALSE), indexes);
        for (Iterator iterator = indexes.iterator(); iterator.hasNext();) {
            FeatureIndex index = (FeatureIndex) iterator.next();
            if (index instanceof FeatureIndexProviderServices) {
                FeatureIndexProviderServices indexServices =
                    (FeatureIndexProviderServices) index;
                indexServices.setValid(valid);
            }
        }
    }

    private void updateIndexes() throws FeatureIndexException {
        FeatureIndexes indexes = getIndexes();
        LOG.debug("Refilling indexes: {}", indexes);
        for (Iterator iterator = indexes.iterator(); iterator.hasNext();) {
            FeatureIndex index = (FeatureIndex) iterator.next();
            if (index instanceof FeatureIndexProviderServices) {
                FeatureIndexProviderServices indexServices =
                    (FeatureIndexProviderServices) index;
                indexServices.fill(true, null);
            }
        }
    }

    private void waitForIndexes() {
        FeatureIndexes indexes = getIndexes();
        LOG.debug("Waiting for indexes to finish filling: {}", indexes);
        for (Iterator iterator = indexes.iterator(); iterator.hasNext();) {
            FeatureIndex index = (FeatureIndex) iterator.next();
            if (index instanceof FeatureIndexProviderServices) {
                FeatureIndexProviderServices indexServices =
                    (FeatureIndexProviderServices) index;
                indexServices.waitForIndex();
            }
        }
    }

    private void disposeIndexes() {
        FeatureIndexes indexes = getIndexes();
        LOG.debug("Disposing indexes: {}", indexes);
        for (Iterator iterator = indexes.iterator(); iterator.hasNext();) {
            FeatureIndex index = (FeatureIndex) iterator.next();
            if (index instanceof FeatureIndexProviderServices) {
                FeatureIndexProviderServices indexServices =
                    (FeatureIndexProviderServices) index;
                indexServices.dispose();
            }
        }
    }

    public boolean isEditing() {
        return mode == MODE_FULLEDIT;
    }

    public boolean isAppending() {
        return mode == MODE_APPEND;
    }

    synchronized public void update(EditableFeatureType type)
        throws DataException {
        try {
            checkInEditingMode();
            if (type == null) {
                throw new NullFeatureTypeException(getName());
            }
            // FIXME: Comprobar que es un featureType aceptable.
            notifyChange(FeatureStoreNotification.BEFORE_UPDATE_TYPE, type);
            newVersionOfUpdate();

            FeatureType oldt = type.getSource().getCopy();
            FeatureType newt = type.getCopy();
            commands.update(newt, oldt);

            if (((DefaultEditableFeatureType) type).hasStrongChanges()) {
                hasStrongChanges = true;
            }
            notifyChange(FeatureStoreNotification.AFTER_UPDATE_TYPE, type);
        } catch (Exception e) {
            throw new StoreUpdateFeatureTypeException(e, this.getName());
        }
    }

    public void delete(Feature feature) throws DataException {
        this.commands.delete(feature);
    }

    synchronized public void doDelete(Feature feature) throws DataException {
        try {
            checkInEditingMode();
            checkIsOwnFeature(feature);
            if (feature instanceof EditableFeature) {
                throw new StoreDeleteEditableFeatureException(getName());
            }
            notifyChange(FeatureStoreNotification.BEFORE_DELETE, feature);

            //Update the featureManager and the spatialManager
            featureManager.delete(feature.getReference());
            spatialManager.deleteFeature(feature);

            newVersionOfUpdate();
            hasStrongChanges = true;
            notifyChange(FeatureStoreNotification.AFTER_DELETE, feature);
        } catch (Exception e) {
            throw new StoreDeleteFeatureException(e, this.getName());
        }
    }

    private static EditableFeature lastChangedFeature = null;

    public synchronized void insert(EditableFeature feature)
        throws DataException {
        LOG.debug("In editing mode {}, insert feature: {}", new Integer(mode),
            feature);
        try {
            switch (mode) {
            case MODE_QUERY:
                throw new NeedEditingModeException(this.getName());

            case MODE_APPEND:
                checkIsOwnFeature(feature);
                if (feature.getSource() != null) {
                    throw new NoNewFeatureInsertException(this.getName());
                }
                this.featureCount = null;
                notifyChange(FeatureStoreNotification.BEFORE_INSERT, feature);
                feature.validate(Feature.UPDATE);
                provider.append(((DefaultEditableFeature) feature).getData());
                hasStrongChanges = true;
                hasInserts = true;
                notifyChange(FeatureStoreNotification.AFTER_INSERT, feature);
                break;

            case MODE_FULLEDIT:
                if (feature.getSource() != null) {
                    throw new NoNewFeatureInsertException(this.getName());
                }
                commands.insert(feature);
            }
        } catch (Exception e) {
            throw new StoreInsertFeatureException(e, this.getName());
        }
    }

    synchronized public void doInsert(EditableFeature feature)
        throws DataException {
        checkIsOwnFeature(feature);

        waitForIndexes();

        notifyChange(FeatureStoreNotification.BEFORE_INSERT, feature);
        newVersionOfUpdate();
        if ((lastChangedFeature == null)
            || (lastChangedFeature.getSource() != feature.getSource())) {
            lastChangedFeature = feature;
            feature.validate(Feature.UPDATE);
            lastChangedFeature = null;
        }
        //Update the featureManager and the spatialManager
        ((DefaultEditableFeature) feature).setInserted(true);
        DefaultFeature newFeature = (DefaultFeature) feature.getNotEditableCopy();


        featureManager.add(newFeature);
        spatialManager.insertFeature(newFeature);

        hasStrongChanges = true;
        hasInserts = true;
        notifyChange(FeatureStoreNotification.AFTER_INSERT, feature);
    }

    public void update(EditableFeature feature)
    throws DataException {
        if ((feature).getSource() == null) {
            insert(feature);
            return;
        }
        commands.update(feature, feature.getSource());
    }

    synchronized public void doUpdate(EditableFeature feature, Feature oldFeature)
        throws DataException {
        try {
            checkInEditingMode();
            checkIsOwnFeature(feature);
            notifyChange(FeatureStoreNotification.BEFORE_UPDATE, feature);
            newVersionOfUpdate();
            if ((lastChangedFeature == null)
                || (lastChangedFeature.getSource() != feature.getSource())) {
                lastChangedFeature = feature;
                feature.validate(Feature.UPDATE);
                lastChangedFeature = null;
            }

            //Update the featureManager and the spatialManager
            Feature newf = feature.getNotEditableCopy();
            featureManager.update(newf, oldFeature);
            spatialManager.updateFeature(newf, oldFeature);

            hasStrongChanges = true;
            notifyChange(FeatureStoreNotification.AFTER_UPDATE, feature);
        } catch (Exception e) {
            throw new StoreUpdateFeatureException(e, this.getName());
        }
    }

    synchronized public void redo() throws RedoException {
        Command redo = commands.getNextRedoCommand();
        try {
            checkInEditingMode();
        } catch (NeedEditingModeException ex) {
            throw new RedoException(redo, ex);
        }
        notifyChange(FeatureStoreNotification.BEFORE_REDO, redo);
        newVersionOfUpdate();
        commands.redo();
        hasStrongChanges = true;
        notifyChange(FeatureStoreNotification.AFTER_REDO, redo);
    }

    synchronized public void undo() throws UndoException {
        Command undo = commands.getNextUndoCommand();
        try {
            checkInEditingMode();
        } catch (NeedEditingModeException ex) {
            throw new UndoException(undo, ex);
        }
        notifyChange(FeatureStoreNotification.BEFORE_UNDO, undo);
        newVersionOfUpdate();
        commands.undo();
        hasStrongChanges = true;
        notifyChange(FeatureStoreNotification.AFTER_UNDO, undo);
    }

    public List getRedoInfos() {
        if (isEditing() && (commands != null)) {
            return commands.getRedoInfos();
        } else {
            return null;
        }
    }

    public List getUndoInfos() {
        if (isEditing() && (commands != null)) {
            return commands.getUndoInfos();
        } else {
            return null;
        }
    }

    public synchronized FeatureCommandsStack getCommandsStack()
        throws DataException {
        checkInEditingMode();
        return commands;
    }

    synchronized public void cancelEditing() throws DataException {
        spatialManager.cancelModifies();
        try {
            checkInEditingMode();

            boolean clearSelection = this.hasStrongChanges;
            if (this.selection instanceof FeatureReferenceSelection) {
                clearSelection = this.hasInserts;
            }
            notifyChange(FeatureStoreNotification.BEFORE_CANCELEDITING);
            exitEditingMode();
            if (clearSelection) {
                ((FeatureSelection) this.getSelection()).deselectAll();
            }
            updateIndexes();
            notifyChange(FeatureStoreNotification.AFTER_CANCELEDITING);
        } catch (Exception e) {
            throw new StoreCancelEditingException(e, this.getName());
        }
    }

    synchronized public void finishEditing() throws DataException {
        LOG.debug("finish editing of mode: {}", new Integer(mode));
        try {

            /*
             * Selection needs to be cleared when editing stops
             * to prevent conflicts with selection remaining from
             * editing mode.
             */
//            ((FeatureSelection) this.getSelection()).deselectAll();

            switch (mode) {
            case MODE_QUERY:
                throw new NeedEditingModeException(this.getName());

            case MODE_APPEND:
                ((FeatureSelection) this.getSelection()).deselectAll();
                notifyChange(FeatureStoreNotification.BEFORE_FINISHEDITING);
                provider.endAppend();
                exitEditingMode();
                updateIndexes();
                notifyChange(FeatureStoreNotification.AFTER_FINISHEDITING);
                break;

            case MODE_FULLEDIT:
                if (hasStrongChanges && !this.allowWrite()) {
                    throw new WriteNotAllowedException(getName());
                }

                if(featureManager.isSelectionCompromised()) {
                    ((FeatureSelection) this.getSelection()).deselectAll();
                };

                notifyChange(FeatureStoreNotification.BEFORE_FINISHEDITING);
                if (hasStrongChanges) {
                    validateFeatures(Feature.FINISH_EDITING);

                    /*
                     * This will throw a PerformEditingExceptionif the provider
                     * does not accept the changes (for example, an invalid field name)
                     */
                    provider.performChanges(featureManager.getDeleted(),
                        featureManager.getInserted(),
                        featureManager.getUpdated(),
                        featureTypeManager.getFeatureTypesChanged());
                }
                exitEditingMode();
                updateIndexes();
                notifyChange(FeatureStoreNotification.AFTER_FINISHEDITING);
                break;
            }
        } catch (PerformEditingException pee) {
            throw new WriteException(provider.getSourceId().toString(), pee);
        } catch (Exception e) {
            throw new FinishEditingException(e);
        }
    }

    /**
     * Save changes in the provider without leaving the edit mode.
     * Do not call observers to communicate a change of ediding mode.
     * The operation's history is eliminated to prevent inconsistencies
     * in the data.
     *
     * @throws DataException
     */
    synchronized public void commitChanges() throws DataException {
      LOG.debug("commitChanges of mode: {}", new Integer(mode));
      if( !canCommitChanges() ) {
    	  throw new WriteNotAllowedException(getName());
      }
      try {
        switch (mode) {
        case MODE_QUERY:
          throw new NeedEditingModeException(this.getName());

        case MODE_APPEND:
          this.provider.endAppend();
          exitEditingMode();
          invalidateIndexes();
          this.provider.beginAppend();
          hasInserts = false;
          break;

        case MODE_FULLEDIT:
          if (hasStrongChanges && !this.allowWrite()) {
            throw new WriteNotAllowedException(getName());
          }
          if (hasStrongChanges) {
            validateFeatures(Feature.FINISH_EDITING);
            provider.performChanges(featureManager.getDeleted(),
              featureManager.getInserted(),
              featureManager.getUpdated(),
              featureTypeManager.getFeatureTypesChanged());
          }
          invalidateIndexes();
          featureManager =
            new FeatureManager(new MemoryExpansionAdapter());
          featureTypeManager =
            new FeatureTypeManager(this, new MemoryExpansionAdapter());
          spatialManager =
            new SpatialManager(this, provider.getEnvelope());

          commands =
            new DefaultFeatureCommandsStack(this, featureManager,
              spatialManager, featureTypeManager);
          featureCount = null;
          hasStrongChanges = false;
          hasInserts = false;
          break;
        }
      } catch (Exception e) {
        throw new FinishEditingException(e);
      }
    }

    synchronized public boolean canCommitChanges() throws DataException {
        if ( !this.allowWrite()) {
        	return false;
        }
    	switch (mode) {
    	default:
        case MODE_QUERY:
        	return false;

        case MODE_APPEND:
        	return true;

        case MODE_FULLEDIT:
            List types = this.getFeatureTypes();
            for( int i=0; i<types.size(); i++ ) {
            	Object type = types.get(i);
            	if( type instanceof DefaultEditableFeatureType ) {
	            	if( ((DefaultEditableFeatureType)type).hasStrongChanges() ) {
	            		return false;
	            	}
            	}
            }
            return true;
    	}
    }

    public void beginEditingGroup(String description)
        throws NeedEditingModeException {
        checkInEditingMode();
        commands.startComplex(description);
    }

    public void endEditingGroup() throws NeedEditingModeException {
        checkInEditingMode();
        commands.endComplex();
    }

    public boolean isAppendModeSupported() {
        return this.provider.supportsAppendMode();
    }

    public void export(DataServerExplorer explorer, String provider,
        NewFeatureStoreParameters params) throws DataException {

        if (this.getFeatureTypes().size() != 1) {
            throw new NotYetImplemented(
                "export whith more than one type not yet implemented");
        }
        FeatureSelection featureSelection = (FeatureSelection) getSelection();
        FeatureStore target = null;
        FeatureSet features = null;
        DisposableIterator iterator = null;
        try {
            FeatureType type = this.getDefaultFeatureType();
            if ((params.getDefaultFeatureType() == null)
                || (params.getDefaultFeatureType().size() == 0)) {
                params.setDefaultFeatureType(type.getEditable());

            }
            explorer.add(provider, params, true);

            DataManager manager = DALLocator.getDataManager();
            target = (FeatureStore) manager.openStore(provider, params);
            FeatureType targetType = target.getDefaultFeatureType();

            target.edit(MODE_APPEND);
            FeatureAttributeDescriptor[] pk = type.getPrimaryKey();
            if (featureSelection.getSize() > 0) {
                features = this.getFeatureSelection();
            } else {
                if ((pk != null) && (pk.length > 0)) {
                    FeatureQuery query = createFeatureQuery();
                    for (int i = 0; i < pk.length; i++) {
                        query.getOrder().add(pk[i].getName(), true);
                    }
                    features = this.getFeatureSet(query);
                } else {
                    features = this.getFeatureSet();
                }
            }
            iterator = features.fastIterator();
            while (iterator.hasNext()) {
                DefaultFeature feature = (DefaultFeature) iterator.next();
                target.insert(target.createNewFeature(targetType, feature));
            }
            target.finishEditing();
            target.dispose();
        } catch (Exception e) {
            throw new DataExportException(e, params.toString());
        } finally {
            dispose(iterator);
            dispose(features);
            dispose(target);
        }
    }

    //
    // ====================================================================
    // Obtencion de datos
    // getDataCollection, getFeatureCollection
    //

    public DataSet getDataSet() throws DataException {
        checkNotInAppendMode();
        FeatureQuery query =
            new DefaultFeatureQuery(this.getDefaultFeatureType());
        return new DefaultFeatureSet(this, query);
    }

    public DataSet getDataSet(DataQuery dataQuery) throws DataException {
        checkNotInAppendMode();
        return new DefaultFeatureSet(this, (FeatureQuery) dataQuery);
    }

    public void getDataSet(Observer observer) throws DataException {
        checkNotInAppendMode();
        this.getFeatureSet(null, observer);
    }

    public void getDataSet(DataQuery dataQuery, Observer observer)
        throws DataException {
        checkNotInAppendMode();
        this.getFeatureSet((FeatureQuery) dataQuery, observer);
    }

    @Override
    public FeatureSet getFeatureSet() throws DataException {
        return this.getFeatureSet((FeatureQuery)null);
    }

    @Override
    public FeatureSet getFeatureSet(FeatureQuery featureQuery)
        throws DataException {
        checkNotInAppendMode();
        if( featureQuery==null ) {
            featureQuery = new DefaultFeatureQuery(this.getDefaultFeatureType());
        }
        return new DefaultFeatureSet(this, featureQuery);
    }

    @Override
    public List<Feature> getFeatures(FeatureQuery query, int pageSize)  {
        try {
            FeaturePagingHelper pager = this.dataManager.createFeaturePagingHelper(this, query, pageSize);
            return pager.asList();
        } catch (BaseException ex) {
            throw new RuntimeException("Can't create the list of features.", ex);
        }
    }
    
    public void accept(Visitor visitor) throws BaseException {
        FeatureSet set = getFeatureSet();
        try {
            set.accept(visitor);
        } finally {
            set.dispose();
        }
    }

    public void accept(Visitor visitor, DataQuery dataQuery)
        throws BaseException {
        FeatureSet set = getFeatureSet((FeatureQuery) dataQuery);
        try {
            set.accept(visitor);
        } finally {
            set.dispose();
        }
    }

    public FeatureType getFeatureType(FeatureQuery featureQuery)
        throws DataException {
        DefaultFeatureType fType =
            (DefaultFeatureType) this.getFeatureType(featureQuery
                .getFeatureTypeId());
        if( featureQuery.hasAttributeNames() || featureQuery.hasConstantsAttributeNames() ) {
            return fType.getSubtype(featureQuery.getAttributeNames(), featureQuery.getConstantsAttributeNames() );
        }
        return fType;
    }

    public void getFeatureSet(Observer observer) throws DataException {
        checkNotInAppendMode();
        this.getFeatureSet(null, observer);
    }

    public void getFeatureSet(FeatureQuery query, Observer observer)
        throws DataException {
        class LoadInBackGround implements Runnable {

            private FeatureStore store;
            private FeatureQuery query;
            private Observer observer;

            public LoadInBackGround(FeatureStore store, FeatureQuery query,
                Observer observer) {
                this.store = store;
                this.query = query;
                this.observer = observer;
            }

            void notify(FeatureStoreNotification theNotification) {
                observer.update(store, theNotification);
                return;
            }

            public void run() {
                FeatureSet set = null;
                try {
                    set = store.getFeatureSet(query);
                    notify(new DefaultFeatureStoreNotification(store,
                        FeatureStoreNotification.LOAD_FINISHED, set));
                } catch (Exception e) {
                    notify(new DefaultFeatureStoreNotification(store,
                        FeatureStoreNotification.LOAD_FINISHED, e));
                } finally {
                    dispose(set);
                }
            }
        }

        checkNotInAppendMode();
        if (query == null) {
            query = new DefaultFeatureQuery(this.getDefaultFeatureType());
        }
        LoadInBackGround task = new LoadInBackGround(this, query, observer);
        Thread thread = new Thread(task, "Load Feature Set in background");
        thread.start();
    }

    public Feature getFeatureByReference(FeatureReference reference)
        throws DataException {
        checkNotInAppendMode();
        DefaultFeatureReference ref = (DefaultFeatureReference) reference;
        FeatureType featureType;
        if (ref.getFeatureTypeId() == null) {
            featureType = this.getDefaultFeatureType();
        } else {
            featureType = this.getFeatureType(ref.getFeatureTypeId());
        }
        return this.getFeatureByReference(reference, featureType);
    }

    public Feature getFeatureByReference(FeatureReference reference,
        FeatureType featureType) throws DataException {
        checkNotInAppendMode();
        featureType = fixFeatureType((DefaultFeatureType) featureType);
        if (this.mode == MODE_FULLEDIT) {
            Feature f = featureManager.get(reference, this, featureType);
            if (f != null) {
                return f;
            }
        }

        FeatureType sourceFeatureType = featureType;
        if (!this.transforms.isEmpty()) {
            sourceFeatureType = this.transforms.getSourceFeatureTypeFrom(featureType);
        }
        // TODO comprobar que el id es de este store

        DefaultFeature feature =
            new DefaultFeature(this,
                this.provider.getFeatureProviderByReference(
                    (FeatureReferenceProviderServices) reference, sourceFeatureType));

        if (!this.transforms.isEmpty()) {
            return this.transforms.applyTransform(feature, featureType);
        }
        return feature;
    }

    //
    // ====================================================================
    // Gestion de features
    //

    private FeatureType fixFeatureType(DefaultFeatureType type)
        throws DataException {
        FeatureType original = this.getDefaultFeatureType();

        if ((type == null) || type.equals(original)) {
            return original;
        } else {
            if (!type.isSubtypeOf(original)) {
                Iterator iter = this.getFeatureTypes().iterator();
                FeatureType tmpType;
                boolean found = false;
                while (iter.hasNext()) {
                    tmpType = (FeatureType) iter.next();
                    if (type.equals(tmpType)) {
                        return type;

                    } else
                        if (type.isSubtypeOf(tmpType)) {
                            found = true;
                            original = tmpType;
                            break;
                        }

                }
                if (!found) {
                    throw new IllegalFeatureTypeException(getName());
                }
            }
        }

        // Checks that type has all fields of pk
        // else add the missing attributes at the end.
        if (!original.hasOID()) {
            // Gets original pk attributes
            DefaultEditableFeatureType edOriginal =
                (DefaultEditableFeatureType) original.getEditable();
            FeatureAttributeDescriptor orgAttr;
            Iterator edOriginalIter = edOriginal.iterator();
            while (edOriginalIter.hasNext()) {
                orgAttr = (FeatureAttributeDescriptor) edOriginalIter.next();
                if (!orgAttr.isPrimaryKey()) {
                    edOriginalIter.remove();
                }
            }

            // Checks if all pk attributes are in type
            Iterator typeIterator;
            edOriginalIter = edOriginal.iterator();
            FeatureAttributeDescriptor attr;
            while (edOriginalIter.hasNext()) {
                orgAttr = (FeatureAttributeDescriptor) edOriginalIter.next();
                typeIterator = type.iterator();
                while (typeIterator.hasNext()) {
                    attr = (FeatureAttributeDescriptor) typeIterator.next();
                    if (attr.getName().equals(orgAttr.getName())) {
                        edOriginalIter.remove();
                        break;
                    }
                }
            }

            // add missing pk attributes if any
            if (edOriginal.size() > 0) {
                boolean isEditable = type instanceof DefaultEditableFeatureType;
                DefaultEditableFeatureType edType =
                    (DefaultEditableFeatureType) original.getEditable();
                edType.clear();
                edType.addAll(type);
                edType.addAll(edOriginal);
                if (!isEditable) {
                    type = (DefaultFeatureType) edType.getNotEditableCopy();
                }
            }

        }

        return type;
    }

    public void validateFeatures(int mode) throws DataException {
        FeatureSet collection = null;
        DisposableIterator iter = null;
        try {
            checkNotInAppendMode();
            collection = this.getFeatureSet();
            iter = collection.fastIterator();
            long previousVersionOfUpdate = currentVersionOfUpdate();
            while (iter.hasNext()) {
                ((DefaultFeature) iter.next()).validate(mode);
                if (previousVersionOfUpdate != currentVersionOfUpdate()) {
                    throw new ConcurrentDataModificationException(getName());
                }
            }
        } catch (Exception e) {
            throw new ValidateFeaturesException(e, getName());
        } finally {
            dispose(iter);
            dispose(collection);
        }
    }

    public FeatureType getDefaultFeatureType() throws DataException {
        try {

            if (isEditing()) {
                FeatureType auxFeatureType =
                    featureTypeManager.getType(defaultFeatureType.getId());
                if (auxFeatureType != null) {
                    return avoidEditable(auxFeatureType);
                }
            }
            FeatureType type = this.transforms.getDefaultFeatureType();
            if (type != null) {
                return avoidEditable(type);
            }

            return avoidEditable(defaultFeatureType);

        } catch (Exception e) {
            throw new GetFeatureTypeException(e, getName());
        }
    }

    private FeatureType avoidEditable(FeatureType ft) {
        if (ft instanceof EditableFeatureType) {
            return ((EditableFeatureType) ft).getNotEditableCopy();
        } else {
            return ft;
        }
    }

    public FeatureType getFeatureType(String featureTypeId)
        throws DataException {
        if (featureTypeId == null) {
            return this.getDefaultFeatureType();
        }
        try {
            if (isEditing()) {
                FeatureType auxFeatureType =
                    featureTypeManager.getType(featureTypeId);
                if (auxFeatureType != null) {
                    return auxFeatureType;
                }
            }
            FeatureType type = this.transforms.getFeatureType(featureTypeId);
            if (type != null) {
                return type;
            }
            Iterator iter = this.featureTypes.iterator();
            while (iter.hasNext()) {
                type = (FeatureType) iter.next();
                if (type.getId().equals(featureTypeId)) {
                    return type;
                }
            }
            return null;
        } catch (Exception e) {
            throw new GetFeatureTypeException(e, getName());
        }
    }

    public FeatureType getProviderDefaultFeatureType() {
        return defaultFeatureType;
    }

    public List getFeatureTypes() throws DataException {
        try {
            List types;
            if (isEditing()) {
                types = new ArrayList();
                Iterator it = featureTypes.iterator();
                while (it.hasNext()) {
                    FeatureType type = (FeatureType) it.next();
                    FeatureType typeaux =
                        featureTypeManager.getType(type.getId());
                    if (typeaux != null) {
                        types.add(typeaux);
                    } else {
                        types.add(type);
                    }
                }
                it = featureTypeManager.newsIterator();
                while (it.hasNext()) {
                    FeatureType type = (FeatureType) it.next();
                    types.add(type);
                }
            } else {
                types = this.transforms.getFeatureTypes();
                if (types == null) {
                    types = featureTypes;
                }
            }
            return Collections.unmodifiableList(types);
        } catch (Exception e) {
            throw new GetFeatureTypeException(e, getName());
        }
    }

    public List getProviderFeatureTypes() throws DataException {
        return Collections.unmodifiableList(this.featureTypes);
    }

    public Feature createFeature(FeatureProvider data) throws DataException {
        DefaultFeature feature = new DefaultFeature(this, data);
        return feature;
    }

    public Feature createFeature(FeatureProvider data, FeatureType type)
        throws DataException {
        // FIXME: falta por implementar
        // Comprobar si es un subtipo del feature de data
        // y construir un feature usando el subtipo.
        // Probablemente requiera generar una copia del data.
        throw new NotYetImplemented();
    }

    public EditableFeature createNewFeature(FeatureType type,
        Feature defaultValues) throws DataException {
        try {
            FeatureProvider data = createNewFeatureProvider(type);
            DefaultEditableFeature feature =
                new DefaultEditableFeature(this, data);
            feature.initializeValues(defaultValues);
            data.setNew(true);

            return feature;
        } catch (Exception e) {
            throw new CreateFeatureException(e, getName());
        }
    }

    private FeatureProvider createNewFeatureProvider(FeatureType type)
        throws DataException {
        type = this.fixFeatureType((DefaultFeatureType) type);
        FeatureProvider data = this.provider.createFeatureProvider(type);
        data.setNew(true);
        if (type.hasOID() && (data.getOID() == null)) {
            data.setOID(this.provider.createNewOID());
        } else {
            data.setOID(this.getTemporalOID());
        }
        return data;

    }

    public EditableFeature createNewFeature(FeatureType type,
        boolean defaultValues) throws DataException {
        try {
            FeatureProvider data = createNewFeatureProvider(type);
            DefaultEditableFeature feature =
                new DefaultEditableFeature(this, data);
            if (defaultValues) {
                feature.initializeValues();
            }
            return feature;
        } catch (Exception e) {
            throw new CreateFeatureException(e, getName());
        }
    }

    public EditableFeature createNewFeature(boolean defaultValues)
        throws DataException {
        return this.createNewFeature(this.getDefaultFeatureType(),
            defaultValues);
    }

    public EditableFeature createNewFeature() throws DataException {
        return this.createNewFeature(this.getDefaultFeatureType(), true);
    }

    public EditableFeature createNewFeature(Feature defaultValues) throws DataException {
        FeatureType ft = this.getDefaultFeatureType();
        EditableFeature f = this.createNewFeature(ft, false);
        Iterator it = ft.iterator();
        while(it.hasNext()) {
            FeatureAttributeDescriptor desc = (FeatureAttributeDescriptor) it.next();
            try {
                f.set(desc.getName(), defaultValues.get(desc.getName()));
            } catch(Throwable th) {
                // Ignore
            }
        }
        return f;
    }

    public EditableFeatureType createFeatureType() {
        DefaultEditableFeatureType ftype = new DefaultEditableFeatureType();
        return ftype;
    }

    public EditableFeatureType createFeatureType(String id) {
        DefaultEditableFeatureType ftype = new DefaultEditableFeatureType(id);
        return ftype;
    }

    //
    // ====================================================================
    // Index related methods
    //

    public FeatureIndexes getIndexes() {
        return this.indexes;
    }

    public FeatureIndex createIndex(FeatureType featureType,
        String attributeName, String indexName) throws DataException {
        return createIndex(null, featureType, attributeName, indexName);
    }

    public FeatureIndex createIndex(String indexTypeName,
        FeatureType featureType, String attributeName, String indexName)
        throws DataException {

        return createIndex(indexTypeName, featureType, attributeName,
            indexName, false, null);
    }

    public FeatureIndex createIndex(FeatureType featureType,
        String attributeName, String indexName, Observer observer)
        throws DataException {
        return createIndex(null, featureType, attributeName, indexName,
            observer);
    }

    public FeatureIndex createIndex(String indexTypeName,
        FeatureType featureType, String attributeName, String indexName,
        final Observer observer) throws DataException {

        return createIndex(indexTypeName, featureType, attributeName,
            indexName, true, observer);
    }

    private FeatureIndex createIndex(String indexTypeName,
        FeatureType featureType, String attributeName, String indexName,
        boolean background, final Observer observer) throws DataException {

        checkNotInAppendMode();
        FeatureIndexProviderServices index = null;
        index =
            dataManager.createFeatureIndexProvider(indexTypeName, this,
                featureType, indexName,
                featureType.getAttributeDescriptor(attributeName));

        try {
            index.fill(background, observer);
        } catch (FeatureIndexException e) {
            throw new InitializeException(index.getName(), e);
        }

        ((DefaultFeatureIndexes) getIndexes()).addIndex(index);
        return index;
    }

    //
    // ====================================================================
    // Transforms related methods
    //

    public FeatureStoreTransforms getTransforms() {
        return this.transforms;
    }

    public FeatureQuery createFeatureQuery() {
        return new DefaultFeatureQuery();
    }

    public DataQuery createQuery() {
        return createFeatureQuery();
    }

    //
    // ====================================================================
    // UndoRedo related methods
    //

    public boolean canRedo() {
        return commands.canRedo();
    }

    public boolean canUndo() {
        return commands.canUndo();
    }

    public void redo(int num) throws RedoException {
        for (int i = 0; i < num; i++) {
            redo();
        }
    }

    public void undo(int num) throws UndoException {
        for (int i = 0; i < num; i++) {
            undo();
        }
    }

    //
    // ====================================================================
    // Metadata related methods
    //

    public Object getMetadataID() {
        return this.provider.getSourceId();
    }

    public void delegate(DynObject dynObject) {
        this.metadata.delegate(dynObject);
    }

    public DynClass getDynClass() {
        return this.metadata.getDynClass();
    }

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		if( this.transforms.hasDynValue(name) ) {
			return this.transforms.getDynValue(name);
		}
		if (this.metadata.hasDynValue(name)) {
			return this.metadata.getDynValue(name);
		}
		if (METADATA_PROVIDER.equalsIgnoreCase(name)) {
			return this.provider.getProviderName();
		} else if (METADATA_CONTAINERNAME.equalsIgnoreCase(name)) {
			return this.provider.getSourceId();
		} else if (METADATA_FEATURETYPE.equalsIgnoreCase(name)) {
			try {
				return this.getDefaultFeatureType();
			} catch (DataException e) {
				return null;
			}
		}
		return this.metadata.getDynValue(name);
	}

    public boolean hasDynValue(String name) {
		if( this.transforms.hasDynValue(name) ) {
			return true;
		}
        return this.metadata.hasDynValue(name);
    }

    public void implement(DynClass dynClass) {
        this.metadata.implement(dynClass);
    }

    public Object invokeDynMethod(String name, Object[] args)
        throws DynMethodException {
        return this.metadata.invokeDynMethod(this, name, args);
    }

    public Object invokeDynMethod(int code, Object[] args)
        throws DynMethodException {
        return this.metadata.invokeDynMethod(this, code, args);
    }

    public void setDynValue(String name, Object value)
        throws DynFieldNotFoundException {
		if( this.transforms.hasDynValue(name) ) {
			this.transforms.setDynValue(name, value);
			return;
		}
        this.metadata.setDynValue(name, value);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataChildren()
     */
    public Set getMetadataChildren() {
        return this.metadataChildren;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataName()
     */
    public String getMetadataName() {
        return this.provider.getProviderName();
    }

    public FeatureTypeManager getFeatureTypeManager() {
        return this.featureTypeManager;
    }

    public long getFeatureCount() throws DataException {
        if (featureCount == null) {
            featureCount = new Long(this.provider.getFeatureCount());
        }
        if (this.isEditing()) {
            if(this.isAppending()) {
                try{
                    throw new IllegalStateException();
                } catch(IllegalStateException e) {
                    LOG.info("Call DefaultFeatureStore.getFeatureCount editing in mode APPEND");
                    e.printStackTrace();
                }
                return -1;
            } else {
                return featureCount.longValue()
                    + this.featureManager.getDeltaSize();
            }
        }
        return featureCount.longValue();
    }

    private Long getTemporalOID() {
        return new Long(this.temporalOid++);
    }

    public FeatureType getProviderFeatureType(String featureTypeId) {
        if (featureTypeId == null) {
            return this.defaultFeatureType;
        }
        FeatureType type;
        Iterator iter = this.featureTypes.iterator();
        while (iter.hasNext()) {
            type = (FeatureType) iter.next();
            if (type.getId().equals(featureTypeId)) {
                return type;
            }
        }
        return null;
    }

    public FeatureProvider getFeatureProviderFromFeature(Feature feature) {
        return ((DefaultFeature) feature).getData();
    }

    public DataStore getStore() {
        return this;
    }

    public FeatureStore getFeatureStore() {
        return this;
    }

    public void createCache(String name, DynObject parameters)
        throws DataException {
        cache = dataManager.createFeatureCacheProvider(name, parameters);
        if (cache == null) {
            throw new CreateException("FeaureCacheProvider", null);
        }
        cache.apply(this, provider);
        provider = cache;

        featureCount = null;
    }

    public FeatureCache getCache() {
        return cache;
    }

    public void clear() {
        if (metadata != null) {
            metadata.clear();
        }
    }

    public String getName() {
        return this.provider.getName();
    }

    public String getFullName() {
        try {
            return this.provider.getFullName();
        } catch(Throwable th) {
            return null;
        }
    }

    public String getProviderName() {
        return this.provider.getProviderName();
    }

    public boolean isKnownEnvelope() {
        return this.provider.isKnownEnvelope();
    }

    public boolean hasRetrievedFeaturesLimit() {
        return this.provider.hasRetrievedFeaturesLimit();
    }

    public int getRetrievedFeaturesLimit() {
        return this.provider.getRetrievedFeaturesLimit();
    }

    public Interval getInterval() {
        return this.provider.getInterval();
    }

    public Collection getTimes() {
        return this.provider.getTimes();
    }

    public Collection getTimes(Interval interval) {
        return this.provider.getTimes(interval);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    public Object clone() throws CloneNotSupportedException {

        DataStoreParameters dsp = getParameters();

        DefaultFeatureStore cloned_store = null;

        try {
            cloned_store = (DefaultFeatureStore) DALLocator.getDataManager().
                openStore(this.getProviderName(), dsp);
            if (transforms != null) {
                cloned_store.transforms = (DefaultFeatureStoreTransforms) transforms.clone();
                cloned_store.transforms.setStoreForClone(cloned_store);
            }
        } catch (Exception e) {
            throw new CloneException(e);
        }
        return cloned_store;

    }

    public Feature getFeature(DynObject dynobject) {
        if (dynobject instanceof DynObjectFeatureFacade){
            Feature f = ((DynObjectFeatureFacade)dynobject).getFeature();
            return f;
        }
        return null;
    }

    public Iterator iterator() {
        try {
            return this.getFeatureSet().fastIterator();
        } catch (DataException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}