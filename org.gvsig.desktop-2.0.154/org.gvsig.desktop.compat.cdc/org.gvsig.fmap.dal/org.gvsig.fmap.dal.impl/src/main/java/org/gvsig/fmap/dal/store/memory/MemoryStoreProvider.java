/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.memory;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices;
import org.gvsig.fmap.dal.feature.spi.memory.AbstractMemoryStoreProvider;
import org.gvsig.fmap.dal.feature.spi.memory.MemoryResource;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 * 
 *         TODO: take into account the parameter "autoOrderAttributeName" to
 *         order the {@link Feature}s by default by that attribute, in ascending
 *         order.
 */
public class MemoryStoreProvider extends AbstractMemoryStoreProvider {

    private static final Logger logger = LoggerFactory
        .getLogger(MemoryStoreProvider.class);

    public static final String NAME = "Memory";
    public static final String DESCRIPTION = "Memory provider";

    public static final String METADATA_DEFINITION_NAME = "MemoryProvider";

    private long counterNewsOIDs = 0;
    private Envelope envelope = null;
    private Map oids = null;

    private Double sourceID = null;

    private ResourceProvider memoryResource;

    public static DynObject newMetadataContainer(String name) {
        return ToolsLocator.getDynObjectManager().createDynObject(
            MetadataLocator.getMetadataManager().getDefinition(name));
    }

    public MemoryStoreProvider(MemoryStoreParameters memoryStoreParameters,
        DataStoreProviderServices storeServices) throws InitializeException {
        super(memoryStoreParameters, storeServices,
            newMetadataContainer(METADATA_DEFINITION_NAME));
        sourceID = new Double(Math.random());
        data = new ArrayList();
        oids = new Hashtable();
        initialize();
        memoryResource =
            createResource(MemoryResource.NAME,
                new Object[] { memoryStoreParameters.getName() });
    }

    public FeatureProvider createFeatureProvider(FeatureType featureType)throws DataException  {
            this.open();
            return new DefaultFeatureProvider(featureType, this.createNewOID());
    }

    public String getName() {
        return "Memory_" + Integer.toHexString((int) (Math.random()*100000)).toUpperCase();
    }

    public String getFullName() {
        return "memory:" + Double.toHexString(this.sourceID.doubleValue());
    }

    private void initialize() {
        FeatureType featureType =
            getStoreServices().createFeatureType(getName()).getNotEditableCopy();
        List types = new ArrayList(1);
        types.add(featureType);
        getStoreServices().setFeatureTypes(types, featureType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#createNewOID()
     */
    public Object createNewOID() {
        return new Long(counterNewsOIDs++);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#getName()
     */
    public String getProviderName() {
        return NAME;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#getOIDType()
     */
    public int getOIDType() {
        return DataTypes.LONG;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.fmap.dal.spi.DataStoreProvider#getSourceId()
     */
    public Object getSourceId() {
        return sourceID;
    }

    public void open() throws OpenException {
    }

    public static void registerMetadataDefinition() throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(METADATA_DEFINITION_NAME) == null) {
            DynStruct metadataDefinition =
                manager.addDefinition(METADATA_DEFINITION_NAME, "Memory Store");
            metadataDefinition.extend(manager
                .getDefinition(DataStore.SPATIAL_METADATA_DEFINITION_NAME));
        }
    }

    public boolean allowWrite() {
        return true;
    }

    public void performChanges(Iterator deleteds, Iterator inserteds,
        Iterator updateds, Iterator originalFeatureTypesUpdated)
        throws PerformEditingException {
        FeatureSet set = null;
        DisposableIterator iter = null;
        try {
            FeatureType featureType =
                this.getStoreServices().getDefaultFeatureType();
            List types = new ArrayList(1);
            types.add(featureType);
            getStoreServices().setFeatureTypes(types, featureType);

            set = this.getFeatureStore().getFeatureSet();

            // Create a visitor to generate new data
            EditionVisitor visitor = new EditionVisitor(set.getSize());

            // collect new data
            set.accept(visitor);

            // update data objects
            data = visitor.getData();
            oids = visitor.getOids();
            envelope = visitor.getEnvelope();

        } catch (DataException e) {
            logger.error("Error adding features", e);
        } catch (BaseException e) {
            logger.error("Error performing edition", e);
        } finally {
            dispose(iter);
            dispose(set);
        }
    }

    public Envelope getEnvelope() throws DataException {
        return envelope;
    }

    protected FeatureProvider internalGetFeatureProviderByReference(
        FeatureReferenceProviderServices reference) throws DataException {
        return (FeatureProvider) oids.get(reference.getOID());
    }

    public ResourceProvider getResource() {
        return memoryResource;
    }

    public boolean supportsAppendMode() {
        return true;
    }

    public void append(FeatureProvider featureProvider) throws DataException {
        FeatureProvider newFeatureProvider = featureProvider.getCopy();
        if (data.isEmpty()) {
            try {
                envelope =
                    (Envelope) newFeatureProvider.getDefaultEnvelope().clone();
            } catch (CloneNotSupportedException e) {
                envelope = newFeatureProvider.getDefaultEnvelope();
            }
        } else {
            envelope.add(newFeatureProvider.getDefaultEnvelope());
        }

        newFeatureProvider.setNew(false);
        data.add(newFeatureProvider);
        oids.put(newFeatureProvider.getOID(), newFeatureProvider);
        logger.debug("Envelope after adding feature: ", envelope.toString());
    }

    public void beginAppend() throws DataException {
        // Nothing to do
    }

    public void endAppend() throws DataException {
        // Nothing to do
    }

    /**
     * Visitor to create new data objects from finish edition operation
     * 
     * @author jmvivo ( jmvivo at disid dot com)
     * 
     */
    private class EditionVisitor implements Visitor {

        private List data;
        private Map oids;
        private Envelope envelope;
        private FeatureStoreProviderServices services;

        /**
         * @param size
         *            to use to initialize data structures
         * @throws CreateEnvelopeException
         */
        public EditionVisitor(long size) {
            data = new ArrayList((int) size);
            oids = new Hashtable((int) size);
            envelope = null;
            services = MemoryStoreProvider.this.getStoreServices();
        }

        public void visit(Object obj) throws VisitCanceledException,
            BaseException {
            Feature feature = (Feature) obj;
            FeatureProvider featureProvider =
                services.getFeatureProviderFromFeature(feature);
            featureProvider.setNew(false);
            data.add(featureProvider);
            oids.put(featureProvider.getOID(), featureProvider);
            if (envelope == null) {
                try {
                    envelope = (Envelope) feature.getDefaultEnvelope().clone();
                } catch (CloneNotSupportedException e) {
                    envelope = feature.getDefaultEnvelope();
                }
            } else {
                envelope.add(feature.getDefaultEnvelope());
            }
            Envelope aux = feature.getDefaultEnvelope();
            if (aux != null) { //There are geometries
            	if (envelope == null) {
            		try {
            			envelope = (Envelope) aux.clone();
            		} catch (CloneNotSupportedException e) {
            			envelope = feature.getDefaultEnvelope();
            		}
            	} else {
            		envelope.add(feature.getDefaultEnvelope());
            	}
            }
        }

        public List getData() {
            return data;
        }

        public Map getOids() {
            return oids;
        }

        public Envelope getEnvelope() {
            return envelope;
        }
    }

}
