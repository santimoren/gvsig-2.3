/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.cresques.Messages;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexOperationException;
import org.gvsig.fmap.dal.feature.exception.InvalidFeatureIndexException;
import org.gvsig.fmap.dal.feature.spi.DefaultLongList;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProvider;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProviderServices;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.WeakReferencingObservable;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.gvsig.tools.task.AbstractMonitorableTask;
import org.gvsig.tools.task.CancellableTask;
import org.gvsig.tools.task.MonitorableTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default feature index provider services.
 * 
 * @author gvSIG team
 */
public class DefaultFeatureIndex extends AbstractDisposable implements
    FeatureIndexProviderServices,
    WeakReferencingObservable {

    private static final Logger LOG = LoggerFactory
        .getLogger(DefaultFeatureIndex.class);

    private final FeatureStoreProviderServices featureStore;
    private final FeatureType featureType;
    private final String attributeName;
    private final String indexName;
    private final int dataType;
    private final FeatureIndexProvider indexProvider;
    private List attributeNames;

    private Object featureOperationTaskLock = new Object();
    private FeatureIndexOperationTask featureIndexTask;

    private boolean valid = true;

    private DelegateWeakReferencingObservable observable =
        new DelegateWeakReferencingObservable(this);

    public DefaultFeatureIndex(FeatureStoreProviderServices featureStore,
        FeatureType featureType, FeatureIndexProvider indexProvider,
        String attributeName, String indexName) {

        if (featureStore == null) {
            throw new IllegalArgumentException("featureStore cannot be null.");
        }
        if (featureType == null) {
            throw new IllegalArgumentException("featureType cannot be null.");
        }
        if (attributeName == null) {
            throw new IllegalArgumentException("attributeName cannot be null.");
        }
        if (indexName == null) {
            throw new IllegalArgumentException("indexName cannot be null.");
        }

        // FIXME Esto debe ir al provider
        if (featureStore.getProvider().getOIDType() != DataTypes.INT
            && featureStore.getProvider().getOIDType() != DataTypes.LONG) {
            throw new IllegalArgumentException();
        }

        FeatureAttributeDescriptor attr =
            featureType.getAttributeDescriptor(attributeName);
        if (attr == null) {
            throw new IllegalArgumentException("Attribute " + attributeName
                + " not found in FeatureType " + featureType.toString());
        }

        this.featureStore = featureStore;
        this.featureType = featureType;
        this.attributeName = attributeName;
        this.indexName = indexName;
        this.dataType = attr.getType();
        this.indexProvider = indexProvider;

        attributeNames = new ArrayList();
        attributeNames.add(attributeName);
    }

    public final FeatureAttributeDescriptor getFeatureAttributeDescriptor() {
        return featureType.getAttributeDescriptor(attributeName);
    }

    public final FeatureStoreProviderServices getFeatureStoreProviderServices() {
        return featureStore;
    }

    public final FeatureType getFeatureType() {
        return featureType;
    }

    public final String getAttributeName() {
        return attributeName;
    }

    public final int getDataType() {
        return dataType;
    }

    /**
     * {@link MonitorableTask} and {@link CancellableTask} to perform long
     * operations on the index: filling and inserting or deleting a feature set.
     * 
     * @author gvSIG Team
     * @version $Id$
     */
    private static class FeatureIndexOperationTask extends
        AbstractMonitorableTask {

        private final DefaultFeatureIndex index;

        public static final int OP_FILL = 0;
        public static final int OP_INSERT_FSET = 1;
        public static final int OP_DELETE_FSET = 2;

        private static final String[] OP_NAMES = { //
            Messages.getText("filling_index"), // OP_FILL
                Messages.getText("updating_index"), // OP_INSERT_FSET
                Messages.getText("updating_index"), // OP_DELETE_FSET
            };

        private final int operation;

        private final FeatureSet data;

        private final Observer operationObserver;

        private final FeatureStore store;

        /**
         * Creates a new {@link FeatureIndexOperationTask}
         * 
         * @param index
         *            to operate on
         * @param store
         *            to index data from
         * @param operation
         *            to perform: {@link #OP_FILL}, {@link #OP_INSERT_FSET} or
         *            {@link #OP_DELETE_FSET}
         * @param data
         *            feature set to insert or delete in the insert or delete
         *            operations
         * @param operationObserver
         *            to be notified when the operation starts, finishes, is
         *            cancelled or has finished with errors
         */
        protected FeatureIndexOperationTask(DefaultFeatureIndex index,
            FeatureStore store, int operation, FeatureSet data,
            Observer operationObserver) {
            super(OP_NAMES[operation]);
            this.index = index;
            this.store = store;
            this.operation = operation;
            this.data = data;
            this.operationObserver = operationObserver;
            setDaemon(true);
            setPriority(MIN_PRIORITY);
        }

        public void run() {
            try {
                switch (operation) {
                case OP_FILL:
                    notify(FeatureStoreNotification.INDEX_FILLING_STARTED);
                    clearAndFill();
                    break;

                case OP_INSERT_FSET:
                    notify(FeatureStoreNotification.INDEX_FILLING_STARTED);
                    insert(data);
                    break;

                case OP_DELETE_FSET:
                    notify(FeatureStoreNotification.INDEX_FILLING_STARTED);
                    delete(data);
                    break;
                }
            } catch (Exception e) {
                Exception fioex =
                    new FeatureIndexOperationException(index,
                        OP_NAMES[operation], e);
                notify(FeatureStoreNotification.INDEX_FILLING_ERROR, fioex);
                throw new RuntimeException(fioex);
            } finally {
                index.removeTask(this);
            }
        }

        /**
         * Clears the index data and fills it again.
         */
        private void clearAndFill() throws DataException {
            FeatureSet set = null;
            try {
                synchronized (index) {
                    set = store.getFeatureSet();
                    if (isCancellationRequested()) {
                        cancel();
                        return;
                    }
                    index.clear();
                    if (isCancellationRequested()) {
                        cancel();
                        return;
                    }
                    insert(set);
                    index.setValid(true);
                }
            } catch (IllegalStateException e) {
            	// The feature store has entered in editing or 
            	// append mode again, cancel indexing.
            	cancel();
            } finally {
                DisposeUtils.dispose(set);
            }
        }

        private void insert(FeatureSet data) throws DataException {
            DisposableIterator it = null;
            long counter = 0;
            try {
                it = data.fastIterator();
                synchronized (index) {
                    taskStatus.setRangeOfValues(0, data.getSize());
                    taskStatus.add();
                    while (it.hasNext()) {
                        if (isCancellationRequested()) {
                            index.clear();
                            cancel();
                            return;
                        }
                        Feature feat = (Feature) it.next();
                        index.insert(feat);
                        taskStatus.setCurValue(counter++);
                    }
                    notify(FeatureStoreNotification.INDEX_FILLING_SUCCESS);
                }
                taskStatus.terminate();
            } catch (IllegalStateException e) {
            	// The feature store has entered in editing or 
            	// append mode again, cancel indexing.
            	taskStatus.cancel();
            } catch (RuntimeException e) {
                taskStatus.abort();
                throw e;
            } catch (DataException e) {
                taskStatus.abort();
                throw e;
            } finally {
                DisposeUtils.dispose(it);
                taskStatus.remove();
            }
        }

        private void delete(FeatureSet data) throws FeatureIndexException {
            DisposableIterator it = null;
            try {
                it = data.fastIterator();
                synchronized (index) {
                    while (it.hasNext()) {
                        if (isCancellationRequested()) {
                            cancel();
                            return;
                        }
                        Feature feat = (Feature) it.next();
                        index.delete(feat);
                    }
                    notify(FeatureStoreNotification.INDEX_FILLING_SUCCESS);
                }
            } catch (DataException e) {
                throw new FeatureIndexException(e);
            } finally {
                DisposeUtils.dispose(it);
            }
        }

        private void cancel() {
            notify(FeatureStoreNotification.INDEX_FILLING_CANCELLED);
            taskStatus.cancel();
        }

        public void notifyOperationObserver(DataStoreNotification notification) {
            if (this.operationObserver != null) {
                this.operationObserver.update(index, notification);
            }
        }

        private void notify(String notificationType) {
            DataStoreNotification notification =
                new DefaultFeatureStoreNotification(store, notificationType,
                    index);
            notifyOperationObserver(notification);
            index.notifyObservers(notification);
        }

        private void notify(String notificationType, Exception exception) {
            DataStoreNotification notification =
                new DefaultFeatureStoreNotification(store, notificationType,
                    exception);
            notifyOperationObserver(notification);
            index.notifyObservers(notification);
        }
    }

    private FeatureIndexOperationTask createIndexTask(int operation,
        FeatureSet data, Observer observer) {
        synchronized (featureOperationTaskLock) {
            if (featureIndexTask != null) {
                this.featureIndexTask.cancelRequest();
                removeTask(this.featureIndexTask);
            }
            FeatureIndexOperationTask fillingTask =
                new FeatureIndexOperationTask(this,
                    featureStore.getFeatureStore(), operation, data, observer);
            this.featureIndexTask = fillingTask;
            return fillingTask;
        }
    }

    private void removeTask(FeatureIndexOperationTask task) {
        synchronized (featureOperationTaskLock) {
            // Remove if it is not null and the same task
            if (this.featureIndexTask == task) {
                featureIndexTask = null;
            }
        }
    }

    public void fill() throws FeatureIndexException {
        fill(false, null);
    }

    public void fill(boolean background, Observer observer)
        throws FeatureIndexException {
        FeatureIndexOperationTask task =
            createIndexTask(FeatureIndexOperationTask.OP_FILL, null, observer);
        if (background) {
            task.start();
        } else {
            task.run();
        }
    }

    public final void insert(FeatureSet data) throws DataException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        FeatureIndexOperationTask task =
            createIndexTask(FeatureIndexOperationTask.OP_INSERT_FSET, data,
                null);
        task.run();
    }

    public synchronized final void insert(Feature feat) throws DataException {
        try {
        	FeatureIndexProvider prov = getIndexProvider();
        	Object value = feat.get(getAttributeName());
        	if(prov.allowNulls() || value != null ) {
        		prov.insert(value,
        				(FeatureReferenceProviderServices) feat.getReference());
        	}
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Could not add Feature: " + feat
                + " to index " + this
                + ". It does not contain a column with name "
                + getAttributeName());
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Could not add Feature: " + feat
                + " to index " + this + ". Attribute " + getAttributeName()
                + " data type is not valid.");
        }
    }

    public final void delete(FeatureSet data) throws FeatureIndexException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        FeatureIndexOperationTask task =
            createIndexTask(FeatureIndexOperationTask.OP_DELETE_FSET, data,
                null);
        task.run();
    }

    public synchronized final void delete(Feature feat) throws DataException {
        getIndexProvider().delete(feat.get(getAttributeName()),
            (FeatureReferenceProviderServices) feat.getReference());
    }

    private synchronized void clear() throws DataException {
        getIndexProvider().clear();
    }

    public synchronized FeatureSet getMatchFeatureSet(Object value)
        throws FeatureIndexException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        return new IndexFeatureSet(this, new DefaultLongList(
            indexProvider.match(value)));
    }

    public synchronized FeatureSet getRangeFeatureSet(Object value1,
        Object value2) throws FeatureIndexException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        return new IndexFeatureSet(this, new DefaultLongList(
            indexProvider.range(value1, value2)));
    }

    public synchronized FeatureSet getNearestFeatureSet(int count, Object value)
        throws FeatureIndexException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        return new IndexFeatureSet(this, new DefaultLongList(
            indexProvider.nearest(count, value)));
    }

    public synchronized FeatureSet getNearestFeatureSet(int count,
        Object value, Object tolerance) throws FeatureIndexException {
        if (!isValid()) {
            throw new InvalidFeatureIndexException();
        }
        return new IndexFeatureSet(this, new DefaultLongList(
            indexProvider.nearest(count, value, tolerance)));
    }

    public void initialize() throws InitializeException {
        indexProvider.setFeatureIndexProviderServices(this);
        indexProvider.initialize();
    }

    public List getAttributeNames() {
        return attributeNames;
    }

    public String getNewFileName(String prefix, String sufix) {
        int n = 0;
        File file = new File(prefix + getName(), sufix);
        while (file.exists()) {
            n++;
            file = new File(prefix + getName() + n, sufix);
        }
        return file.getAbsolutePath();
    }

    public String getFileName() {
        // TODO Auto-generated method stub
        return null;
    }

    public String getTemporaryFileName() {
        // TODO Auto-generated method stub
        return null;
    }


    public FeatureIndexProvider getFeatureIndexProvider() {
        return this.indexProvider;
    }

    public boolean isFilling() {
        synchronized (featureOperationTaskLock) {
            return featureIndexTask != null;
        }
    }

    public boolean isValid() {
        synchronized (featureOperationTaskLock) {
            return !isFilling() && valid;
        }
    }

    public synchronized void waitForIndex() {
        // Nothing to do, this is just used for anyone to block until the index
        // has finished being used by a FeatureIndexOperation.
        LOG.debug("Wait finished for index: {}", this);
    }

    public void setValid(boolean valid) {
        synchronized (featureOperationTaskLock) {
            this.valid = valid;
        }
    }

    protected void doDispose() throws BaseException {
        synchronized (featureOperationTaskLock) {
            setValid(false);
            if (this.featureIndexTask != null) {
                this.featureIndexTask.cancelRequest();
                this.featureIndexTask = null;
            }
        }
        // Wait for any task until it finishes running
        synchronized (this) {
            return;
        }
    }

    public String toString() {
        return "Feature index with name" + indexName
            + ", for the FeatureType: " + featureType + ", and the attribute: "
            + attributeName;
    }

    public void notifyObservers(Object notification) {
        observable.notifyObservers(notification);
    }

    public String getName() {
        return indexName;
    }

    private FeatureIndexProvider getIndexProvider() {
        return indexProvider;
    }

    public void addObserver(Observer observer) {
        observable.addObserver(observer);
    }

    public void deleteObserver(Observer observer) {
        observable.deleteObserver(observer);
    }

    public void deleteObservers() {
        observable.deleteObservers();
    }

}
