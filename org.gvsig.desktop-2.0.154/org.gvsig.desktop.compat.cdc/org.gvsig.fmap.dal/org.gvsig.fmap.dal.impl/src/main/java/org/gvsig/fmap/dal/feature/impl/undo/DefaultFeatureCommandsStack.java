/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.undo;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReferenceSelection;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.fmap.dal.feature.impl.FeatureManager;
import org.gvsig.fmap.dal.feature.impl.FeatureTypeManager;
import org.gvsig.fmap.dal.feature.impl.SpatialManager;
import org.gvsig.fmap.dal.feature.impl.undo.command.FTypeUpdateCommand;
import org.gvsig.fmap.dal.feature.impl.undo.command.FeatureDeleteCommand;
import org.gvsig.fmap.dal.feature.impl.undo.command.FeatureInsertCommand;
import org.gvsig.fmap.dal.feature.impl.undo.command.FeatureUpdateCommand;
import org.gvsig.fmap.dal.feature.impl.undo.command.SelectionCommandReverse;
import org.gvsig.fmap.dal.feature.impl.undo.command.SelectionCommandSelect;
import org.gvsig.fmap.dal.feature.impl.undo.command.SelectionCommandSelectAll;
import org.gvsig.fmap.dal.feature.impl.undo.command.SelectionCommandSet;
import org.gvsig.tools.undo.command.Command;
import org.gvsig.tools.undo.command.impl.DefaultUndoRedoCommandStack;

/**
 * Clase en memoria para registrar y gestionar los comandos que vamos
 * realizando. La forma en que ha sido implementada esta clase, en vez de una
 * �nica lista para albergar los comandos de deshacer(undos) y los de
 * rehacer(redos), se ha optado por dos pilas una para deshacer(undos) y otra
 * para rehacer(redos), de esta forma : Cuando se a�ade un nuevo comando, se
 * inserta este a la pila de deshacer(undos) y se borra de la de rehacer(redos).
 * Si se realiza un deshacer se desapila este comando de la pila deshacer(undos)
 * y se apila en la de rehacer(redos). Y de la misma forma cuando se realiza un
 * rehacer se desapila este comando de la pila de rehacer(redos) y pasa a la de
 * deshacer(undos).
 *
 * @author Vicente Caballero Navarro
 */
public class DefaultFeatureCommandsStack extends DefaultUndoRedoCommandStack
        implements FeatureCommandsStack {
    private FeatureManager expansionManager;
    private SpatialManager spatialManager;
    private FeatureTypeManager featureTypeManager;
    private DefaultFeatureStore featureStore;

    public DefaultFeatureCommandsStack(DefaultFeatureStore featureStore,
        FeatureManager expansionManager, SpatialManager spatialManager, 
        FeatureTypeManager featureTypeManager) {
        this.featureStore = featureStore;
        this.expansionManager = expansionManager;
        this.spatialManager = spatialManager;
        this.featureTypeManager = featureTypeManager;
    }

    public void clear() {
        super.clear();
        expansionManager.clear();
        featureTypeManager.clear();
        spatialManager.clear();
    }

    public void deselect(DefaultFeatureReferenceSelection selection,
            FeatureReference reference) {
        SelectionCommandSelect command = new SelectionCommandSelect(selection,
                reference, false, "_selectionDeselect");
        add(command);
    }

    public void deselectAll(DefaultFeatureReferenceSelection selection)
            throws DataException {
        if (isSameLastCommand("_selectionDeselectAll")){
        	return;
        }
    	SelectionCommandSelectAll command = new SelectionCommandSelectAll(
                selection, false, "_selectionDeselectAll");
        add(command);
    }
    
    private boolean isSameLastCommand(String description){
    	if (getUndoInfos().size() > 0){
	    	Command lastCommand = getNextUndoCommand();
	    	if (lastCommand.getDescription().equals(description)){
	    		return true;
	    	}
    	}
    	return false;
    }

    public void select(DefaultFeatureReferenceSelection selection,
            FeatureReference reference) {
        SelectionCommandSelect command = new SelectionCommandSelect(selection,
                reference, true, "_selectionSelect");
        add(command);
    }

    public void selectAll(DefaultFeatureReferenceSelection selection)
            throws DataException {
    	 if (isSameLastCommand("_selectionSelectAll")){
         	return;
         }
    	SelectionCommandSelectAll command = new SelectionCommandSelectAll(
                selection, true, "_selectionSelectAll");
        add(command);
    }

    public void selectionReverse(DefaultFeatureReferenceSelection selection) {
        SelectionCommandReverse command = new SelectionCommandReverse(
                selection, "_selectionReverse");
        add(command);
    }

    public void selectionSet(DefaultFeatureStore store,
            FeatureSelection oldSelection, FeatureSelection newSelection) {
        SelectionCommandSet command = new SelectionCommandSet(store,
                oldSelection, newSelection, "_selectionSet");
        add(command);
    }

    public void delete(Feature feature) throws DataException {
        FeatureDeleteCommand command = new FeatureDeleteCommand(featureStore,
                feature, "_featureDelete");
        add(command);
        command.execute();
    }

    public void insert(Feature feature) throws DataException {
        FeatureInsertCommand command = new FeatureInsertCommand(featureStore,
                feature, "_featureInsert");
        add(command);
        command.execute();
    }

    public void update(Feature feature, Feature oldFeature) throws DataException {
        FeatureUpdateCommand command = new FeatureUpdateCommand(featureStore,
               feature, oldFeature,
                "_featureUpdate");
        add(command);
        command.execute();
    }

    public void update(FeatureType featureType, FeatureType oldFeatureType) {
        FTypeUpdateCommand command = new FTypeUpdateCommand(
                featureTypeManager, featureType, oldFeatureType,
                "_typeUpdate");
        add(command);
        command.execute();
    }

}