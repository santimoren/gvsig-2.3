/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataEvaluatorRuntimeException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.IllegalValueException;
import org.gvsig.fmap.dal.feature.exception.SetReadOnlyAttributeException;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.DynObjectFeatureFacade;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.timesupport.Instant;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.lang.Cloneable;

public class DefaultFeature implements Feature, EvaluatorData, Cloneable {

	private static DataTypesManager dataTypesManager = null; 
	protected FeatureProvider data;
	protected FeatureReference reference;
	private WeakReference storeRef;
	
	private boolean inserted = false;

    /*
	 * Usar con mucha precaucion o mejor no usar. Lo precisa el
	 * DefaultFeatureSet en la ordenacion.
	 */
	public DefaultFeature(FeatureStore store) {
		this.storeRef = new WeakReference(store);
		this.reference = null;
	}

	public DefaultFeature(FeatureStore store, FeatureProvider data) {
		this.data = data;
		this.storeRef = new WeakReference(store);
		this.reference = null;
		this.inserted = !data.isNew();
	}

	DefaultFeature(DefaultFeature feature) {
		this.data = feature.data.getCopy();
		this.storeRef = feature.storeRef;
		this.reference = feature.reference;
		this.inserted = feature.isInserted();
	}

	public void setData(FeatureProvider data) {
		this.data = data;
		this.reference = null;
		this.inserted = true; 
	}

	public FeatureProvider getData() {
		return this.data;
	}

	protected DataTypesManager getDataTypesManager() {
		if( dataTypesManager==null ) {
			dataTypesManager = ToolsLocator.getDataTypesManager();
		}
		return dataTypesManager;
	}

    void set(FeatureAttributeDescriptor attribute, Object value) {
        int i = attribute.getIndex();

        if ( attribute.isReadOnly() ) {
            throw new SetReadOnlyAttributeException(attribute.getName(), this.getType());
        }
        FeatureAttributeEmulator emulator = attribute.getFeatureAttributeEmulator();
        if( emulator!= null ) {
            emulator.set((EditableFeature) this,value);
            return;
        }
        
        if ( value == null ) {
            if ( !attribute.allowNull() ) {
                if ( !attribute.isAutomatic() ) {
                    throw new IllegalValueException(attribute, value);
                }
            }
            this.data.set(i, null);
            return;

        }

        if ( attribute.getFeatureAttributeGetter() != null ) {
            value = attribute.getFeatureAttributeGetter().setter(value);
        }

        Class objectClass = attribute.getObjectClass();
        if( objectClass!=null ) {
            if ( objectClass.isInstance(value) ) {
                this.data.set(i, value);
                return;
            }

            if ( !objectClass.isInstance(value) ) {
                try {
                    value
                            = this.getDataTypesManager().coerce(attribute.getType(),
                                    value);
                } catch (CoercionException e) {
                    throw new IllegalArgumentException("Can't convert to "
                            + this.getDataTypesManager().getTypeName(
                                    attribute.getType()) + " from '"
                            + value.getClass().getName() + "' with value '"
                            + value.toString() + "'.");
                }
            }
        }
        this.data.set(i, value);
    }
	
    private Object get(int index,Class theClass, int type) {
        Object value = this.get(index);
        if( theClass.isInstance(value) ) {
            return value;
        }

        
        try {
            return this.getDataTypesManager().coerce(type, value);
        } catch (CoercionException e) {
            
            if (value == null) {
                return null;
            }
            throw new IllegalArgumentException(
                    "Can't convert to "+theClass.getName()+
                    " from '"+value.getClass().getName()+
                    "' with value '"+value.toString()+"'.");
        }
    }
    
//  private Object getNumberByType(Number value, int type) {
//      if (type==DataTypes.DOUBLE){
//          return new Double(value.doubleValue());
//      }else if (type==DataTypes.FLOAT){
//          return new Float(value.floatValue());
//      }else if (type==DataTypes.LONG){
//          return new Long(value.longValue());
//      }else if (type==DataTypes.INT){
//          return new Integer(value.intValue());
//      }else if (type==DataTypes.STRING){
//          return value.toString();
//      }
//      return value;
//  }

    public void initializeValues() {
        FeatureType type = this.getType();
        Iterator iterator = type.iterator();

        while (iterator.hasNext()) {
            FeatureAttributeDescriptor attribute = (FeatureAttributeDescriptor) iterator
            .next();
            if (attribute.isAutomatic() || attribute.isReadOnly()
                    || attribute.getEvaluator() != null) {
                continue;
            }
            if (attribute.getDefaultValue() == null && !attribute.allowNull()) {
                continue;
            }
            this.set(attribute, attribute.getDefaultValue());
        }
    }

    public void clear() {
        initializeValues();
    }

    public void initializeValues(Feature feature) {
        FeatureType myType=this.getType();
        FeatureType type =feature.getType();
        Iterator iterator = type.iterator();

        while (iterator.hasNext()) {
            FeatureAttributeDescriptor attribute = (FeatureAttributeDescriptor) iterator
            .next();
            FeatureAttributeDescriptor myAttribute=myType.getAttributeDescriptor(attribute.getName());
            if (myAttribute != null) {
                this.set(myAttribute, feature.get(attribute.getIndex()));
            }
        }
    }


    public FeatureStore getStore() {
        return (FeatureStore) this.storeRef.get();
    }

    public FeatureType getType() {
        return this.data.getType();
    }

    public EditableFeature getEditable() {
        return new DefaultEditableFeature(this);
    }

    public Feature getCopy() {
        return new DefaultFeature(this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return new DefaultFeature(this);
    }

    public FeatureReference getReference() {
        if (this.reference == null) {
            if (!isInserted()) {
                return null;
            }
            reference = new DefaultFeatureReference(this);
        }
        return this.reference;
    }

    class UnableToGetReferenceException extends BaseRuntimeException {

        /**
         * 
         */
        private static final long serialVersionUID = 1812805035204824163L;

        /**
         * @param exception
         */
        public UnableToGetReferenceException(BaseException exception) {
            super("Unable to get reference", "_UnableToGetReferenceException",
                serialVersionUID);
            this.initCause(exception);
            
        }
        
    }
    
    public void validate(int mode) throws DataException  {
        ((DefaultFeatureType) this.data.getType()).validateFeature(this, mode);
    }

    public List getSRSs() {
        // TODO Auto-generated method stub
        return null;
    }

    public Envelope getDefaultEnvelope() {
        Envelope envelope = this.data.getDefaultEnvelope();
        if( envelope == null ) {
            Geometry geom = this.getDefaultGeometry();
            if( geom!=null ) {
                envelope = geom.getEnvelope();
            }
        }
        return envelope;
    }

    public Geometry getDefaultGeometry() {
    	Geometry geom = this.data.getDefaultGeometry();
    	if( geom!=null ) {
    		return geom;
    	}
    	int i = this.data.getType().getDefaultGeometryAttributeIndex();
    	geom = (Geometry) this.get(i);
        return geom;
    }

    public IProjection getDefaultSRS() {
        return this.data.getType().getDefaultSRS();
    }

    public List getGeometries() {
        // TODO Auto-generated method stub
        return null;
    }

    public Object get(String name) {
        int index = this.data.getType().getIndex(name);
        if( index < 0 ) {
            throw new IllegalArgumentException("Attribute name '"+name+"' not found in the feature.");
        }
        return this.get(index);
    }


    public Object get(int index) {
        FeatureType type = this.data.getType();
        if( index <0 || index >= type.size() ) {
            throw new IllegalArgumentException("Attribute index '"+index+"' out of range (0 to "+this.data.getType().size()+".");
        }
        FeatureAttributeDescriptor attribute = type.getAttributeDescriptor(index);
        if (!this.data.getType().hasEvaluators()) {
            return get(attribute, this.data.get(index));		
        }		
        Evaluator eval = attribute.getEvaluator();
        if (eval == null) {
            return this.data.get(index);
        } else {
            Object value = this.data.get(index);
            if (value != null) { // FIXME: para comprobar si esta calculado usar
                // un array
                // especifico.
                return get(attribute, this.data.get(index));
            }
            try {
                value = eval.evaluate(this);
            } catch (EvaluatorException e) {
                throw new DataEvaluatorRuntimeException(e);
            }
            this.data.set(index, value);
            return  get(attribute, value);
        }		
    }

    private Object get(FeatureAttributeDescriptor featureAttributeDescriptor, Object value){
        FeatureAttributeEmulator emulator = featureAttributeDescriptor.getFeatureAttributeEmulator();
        if( emulator != null ) {
            return emulator.get(this);
        }
        FeatureAttributeGetter getter = featureAttributeDescriptor.getFeatureAttributeGetter();
        if( getter != null ) {
            return getter.getter(value);
        }
        return value;
    }

    public Object[] getArray(String name) {
        return this.getArray(this.data.getType().getIndex(name));
    }

    public Object[] getArray(int index) {
        return (Object[]) this.get(index);
    }

    public boolean getBoolean(String name) {
        return this.getBoolean(this.data.getType().getIndex(name));
    }

    public boolean getBoolean(int index) {
        Boolean value = ((Boolean) this.get(index,Boolean.class,DataTypes.BOOLEAN));
        if (value == null) {
            return false;
        }
        return value.booleanValue();
    }

    public byte getByte(String name) {
        return this.getByte(this.data.getType().getIndex(name));
    }

    public byte getByte(int index) {
        Byte value = ((Byte) this.get(index,Byte.class,DataTypes.BYTE));
        if (value == null) {
            return 0;
        }
        return value.byteValue();
    }

    public Date getDate(String name) {
        return this.getDate(this.data.getType().getIndex(name));
    }

    public Date getDate(int index) {
        Date value = ((Date) this.get(index,Date.class,DataTypes.DATE));

        return value;
    }

    public double getDouble(String name) {
        return this.getDouble(this.data.getType().getIndex(name));
    }

    public double getDouble(int index) {
        
        Double value = ((Double) this.get(index,Double.class,DataTypes.DOUBLE));
        if (value == null) {
            return 0;
        }
        return value.doubleValue();
    }

    public Feature getFeature(String name) {
        return this.getFeature(this.data.getType().getIndex(name));
    }

    public Feature getFeature(int index) {
        return (Feature) this.get(index);
    }

    public float getFloat(String name) {
        return this.getFloat(this.data.getType().getIndex(name));
    }

    public float getFloat(int index) {
        Float value = ((Float) this.get(index,Float.class,DataTypes.FLOAT));
        if (value == null) {
            return 0;
        }
        return value.floatValue();
    }

    public Geometry getGeometry(String name) {
        return this.getGeometry(this.data.getType().getIndex(name));
    }

    public Geometry getGeometry(int index) {
        return (Geometry) this.get(index,Geometry.class,DataTypes.GEOMETRY);
    }

    public int getInt(String name) {
        return this.getInt(this.data.getType().getIndex(name));
    }

    public int getInt(int index) {
        Integer value = ((Integer) this.get(index,Integer.class,DataTypes.INT));
        if (value == null) {
            return 0;
        }
        return ((Integer)value).intValue();
    }

    public long getLong(String name) {
        return this.getLong(this.data.getType().getIndex(name));
    }

    public long getLong(int index) {
        Long value = ((Long) this.get(index,Long.class,DataTypes.LONG));
        if (value == null) {
            return 0;
        }
        return value.longValue();
    }

    public String getString(String name) {
        return this.getString(this.data.getType().getIndex(name));
    }

    public String getString(int index) {
        return (String) this.get(index,String.class,DataTypes.STRING);
    }

    public Object getContextValue(String name) {
        name = name.toLowerCase();
        if (name.equals("store")) {
            return this.getStore();
        }

        if (name.equals("featuretype")) {
            return this.data.getType();
        }

        if (name.equals("feature")) {
            return this;
        }

        throw new IllegalArgumentException(name);
    }

    public Iterator getDataNames() {
        class DataNamesIterator implements Iterator {
            Iterator attributeIteraror;

            DataNamesIterator(DefaultFeature feature) {
                this.attributeIteraror = feature.getType().iterator();
            }

            public boolean hasNext() {
                return this.attributeIteraror.hasNext();
            }

            public Object next() {
                return ((FeatureAttributeDescriptor) this.attributeIteraror
                        .next()).getName();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

        }
        return new DataNamesIterator(this);
    }

    public Object getDataValue(String name) {
        name = name.toLowerCase();
        try {
            return get(name);
        } catch (IllegalArgumentException ex) {
            if( "defaultgeometry".equalsIgnoreCase(name )) {
                return this.getDefaultGeometry();
            }
            throw ex;
        }
    }

    public Iterator getDataValues() {
        class DataValuesIterator implements Iterator {
            DefaultFeature feature;
            int current = 0;

            DataValuesIterator(DefaultFeature feature) {
                this.feature = feature;
            }

            public boolean hasNext() {
                return current < feature.getType().size() - 1;
            }

            public Object next() {
                return feature.get(current++);
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

        }
        return new DataValuesIterator(this);
    }

    public boolean hasContextValue(String name) {
        name = name.toLowerCase();
        if (name.equals("store")) {
            return true;
        }

        if (name.equals("featuretype")) {
            return true;
        }

        if (name.equals("feature")) {
            return true;
        }
        return false;
    }

    public boolean hasDataValue(String name) {
        name = name.toLowerCase();
        return this.data.getType().getIndex(name) >= 0;
    }
    
    public Instant getInstant(int index) {
        return ((Instant) this.get(index,Date.class,DataTypes.INSTANT));
    }

    public Instant getInstant(String name) {
        return this.getInstant(this.data.getType().getIndex(name));
    }

    public Interval getInterval(int index) {
        return ((Interval) this.get(index,Date.class,DataTypes.INTERVAL));
    }

    public Interval getInterval(String name) {
        return this.getInterval(this.data.getType().getIndex(name));
    }

    @Override
    public DynObject getAsDynObject() {
        DynObjectFeatureFacade facade = new DynObjectFeatureFacade(this);
        return facade;
    }

    public String toString() {
       // StringBuffer buffer = new StringBuffer("Feature with values: ");
    	StringBuffer buffer = new StringBuffer("");
        FeatureAttributeDescriptor[] attributeDescriptors =
            getType().getAttributeDescriptors();
        for (int i = 0; i < attributeDescriptors.length; i++) {
            String name = attributeDescriptors[i].getName();
            //buffer.append(name).append("=").append(get(name));
            buffer.append(get(name));
            if (i < attributeDescriptors.length - 1) {
                buffer.append(", ");
            }
        }
        return buffer.toString();
    }
    
    


	/**
     * @return the inserted
     */
    public boolean isInserted() {
        return inserted;
    }

    
    /**
     * @param inserted the inserted to set
     */
    public void setInserted(boolean inserted) {
        this.inserted = inserted;
    }

    public EvaluatorData getEvaluatorData() {
        return this;
    }
}
