/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.text.MessageFormat;

import java.util.Iterator;
import java.util.zip.CRC32;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataListException;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.FeatureTypeIntegrityException;
import org.gvsig.fmap.dal.feature.exception.UnsupportedDataTypeException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.evaluator.Evaluator;

public class DefaultEditableFeatureType extends DefaultFeatureType implements
        EditableFeatureType {

    /**
     *
     */
    private static final long serialVersionUID = -713976880396024389L;

    private boolean hasStrongChanges;
    private DefaultFeatureType source;

    public DefaultEditableFeatureType() {
        super();
        this.hasStrongChanges = false;
        this.source = null;
    }

    public DefaultEditableFeatureType(String id) {
        super(id);
        this.hasStrongChanges = false;
        this.source = null;
    }

    protected DefaultEditableFeatureType(DefaultEditableFeatureType other) {
        super(other);
        this.source = (DefaultFeatureType) other.getSource();
    }

    protected DefaultEditableFeatureType(DefaultFeatureType other) {
        super(other);
        this.source = other;
    }

    protected void intitalizeAddAttibute(DefaultFeatureAttributeDescriptor attr) {
        super.add(new DefaultEditableFeatureAttributeDescriptor(attr));
    }

    public boolean hasStrongChanges() {
        if (hasStrongChanges) {
            return true;
        }
        Iterator iter = this.iterator();
        DefaultEditableFeatureAttributeDescriptor attr;
        while (iter.hasNext()) {
            attr = (DefaultEditableFeatureAttributeDescriptor) iter.next();
            if (attr.hasStrongChanges()) {
                return true;
            }
        }
        return false;
    }

    public FeatureType getCopy() {
        return new DefaultEditableFeatureType(this);
    }

    public EditableFeatureType getEditable() {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(DefaultFeatureType other) {
        Iterator iter = other.iterator();
        DefaultFeatureAttributeDescriptor attr;
        DefaultEditableFeatureAttributeDescriptor editableAttr;
        while (iter.hasNext()) {
            attr = (DefaultFeatureAttributeDescriptor) iter.next();
            if (attr instanceof DefaultEditableFeatureAttributeDescriptor) {
                editableAttr = new DefaultEditableFeatureAttributeDescriptor(
                        attr);
            } else {
                editableAttr = new DefaultEditableFeatureAttributeDescriptor(
                        attr);
            }
            super.add(editableAttr);
        }
        this.pk = null;
        this.fixAll();
        return true;
    }

    public EditableFeatureAttributeDescriptor addLike(
            FeatureAttributeDescriptor other) {
        DefaultEditableFeatureAttributeDescriptor editableAttr;
        if (other instanceof DefaultEditableFeatureAttributeDescriptor) {
            editableAttr = new DefaultEditableFeatureAttributeDescriptor(
                    (DefaultFeatureAttributeDescriptor) other);
        } else {
            editableAttr = new DefaultEditableFeatureAttributeDescriptor(
                    (DefaultFeatureAttributeDescriptor) other);
        }
        super.add(editableAttr);
        this.fixAll();
        return editableAttr;

    }

    public FeatureType getSource() {
        return source;
    }

    private long getCRC() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < this.size(); i++) {
            FeatureAttributeDescriptor x = this.getAttributeDescriptor(i);
            buffer.append(x.getName());
            buffer.append(x.getDataTypeName());
            buffer.append(x.getSize());
        }
        CRC32 crc = new CRC32();
        byte[] data = buffer.toString().getBytes();
        crc.update(data);
        return crc.getValue();
    }

    public FeatureType getNotEditableCopy() {
        this.fixAll();
        DefaultFeatureType copy = new DefaultFeatureType(this, false);
        Iterator iter = this.iterator();
        DefaultFeatureAttributeDescriptor attr;
        while (iter.hasNext()) {
            attr = (DefaultFeatureAttributeDescriptor) iter.next();
            copy.add(new DefaultFeatureAttributeDescriptor(attr));
        }
        return copy;
    }

    public EditableFeatureAttributeDescriptor add(String name, int type) {
        return this.add(name, type, true);
    }

    private EditableFeatureAttributeDescriptor add(String name, int type, boolean updateHasStrongChanges) {
        DefaultEditableFeatureAttributeDescriptor attr = new DefaultEditableFeatureAttributeDescriptor();
        Iterator iter = this.iterator();
        while (iter.hasNext()) {
            EditableFeatureAttributeDescriptor descriptor = (EditableFeatureAttributeDescriptor) iter.next();
            if (descriptor.getName().equalsIgnoreCase(name)) {
                throw new RuntimeException(
                        MessageFormat.format("Name descriptor {0} duplicated.", new String[]{name})
                );
            }

        }
        attr.setName(name);
        switch (type) {
            case DataTypes.BOOLEAN:
            case DataTypes.BYTE:
            case DataTypes.BYTEARRAY:
            case DataTypes.CHAR:
            case DataTypes.DATE:
            case DataTypes.DOUBLE:
            case DataTypes.FLOAT:
            case DataTypes.GEOMETRY:
            case DataTypes.INT:
            case DataTypes.LONG:
            case DataTypes.OBJECT:
            case DataTypes.STRING:
            case DataTypes.TIME:
            case DataTypes.TIMESTAMP:
                break;

            default:
                throw new UnsupportedDataTypeException(name, type);
        }
        attr.setDataType(type);
        attr.setIndex(this.size());
        super.add(attr);
        if (!hasStrongChanges && updateHasStrongChanges) {
            hasStrongChanges = true;
        }
        this.pk = null;
        return attr;
    }

    public EditableFeatureAttributeDescriptor add(String name, int type,
            int size) {
        return this.add(name, type, true).setSize(size);
    }

    public EditableFeatureAttributeDescriptor add(String name, int type,
            Evaluator evaluator) {
        if (evaluator == null) {
            throw new IllegalArgumentException();
        }
        return this.add(name, type, false).setEvaluator(evaluator);
    }

    public EditableFeatureAttributeDescriptor add(String name, int type,
            FeatureAttributeEmulator emulator) {
        if (emulator == null) {
            throw new IllegalArgumentException();
        }
        return this.add(name, type, false).setFeatureAttributeEmulator(emulator);
    }
    
    public EditableFeatureAttributeDescriptor add(String name, String type) {
        return this.add(name,ToolsLocator.getDataTypesManager().getType(type));
    }

    public EditableFeatureAttributeDescriptor add(String name, String type, int size) {
        return this.add(name,ToolsLocator.getDataTypesManager().getType(type), size);
    }
    
    public Object remove(String name) {
        DefaultFeatureAttributeDescriptor attr = (DefaultFeatureAttributeDescriptor) this
                .get(name);
        if (attr == null) {
            return null;
        }
        if (attr.getEvaluator() == null) {
            hasStrongChanges = true;
        }
        super.remove(attr);
        this.pk = null;
        this.fixAll();
        return attr;
    }

    protected void fixAll() {
        int i = 0;
        Iterator iter = super.iterator();
        DefaultFeatureAttributeDescriptor attr;

        while (iter.hasNext()) {
            attr = (DefaultFeatureAttributeDescriptor) iter
                    .next();
            attr.setIndex(i++);
            if (attr instanceof DefaultEditableFeatureAttributeDescriptor) {
                ((DefaultEditableFeatureAttributeDescriptor) attr).fixAll();
            }
            if (attr.getEvaluator() != null) {
                this.hasEvaluators = true;
            }
            if (attr.getFeatureAttributeEmulator() != null) {
                this.hasEmulators = true;
            }
            if (this.defaultGeometryAttributeName == null && attr.getType() == DataTypes.GEOMETRY) {
                this.defaultGeometryAttributeName = attr.getName();
            }
        }
        if (this.defaultGeometryAttributeName != null) {
            this.defaultGeometryAttributeIndex = this.getIndex(this.defaultGeometryAttributeName);
        }
        this.internalID = Long.toHexString(this.getCRC());
    }

    public void checkIntegrity() throws DataListException {
        Iterator iter = super.iterator();
        FeatureTypeIntegrityException ex = null;

        while (iter.hasNext()) {
            DefaultEditableFeatureAttributeDescriptor attr = (DefaultEditableFeatureAttributeDescriptor) iter
                    .next();
            try {
                attr.checkIntegrity();
            } catch (Exception e) {
                if (ex == null) {
                    ex = new FeatureTypeIntegrityException(this.getId());
                }
                ex.add(e);
            }
        }
        if (ex != null) {
            throw ex;
        }
    }

    public boolean remove(EditableFeatureAttributeDescriptor attribute) {
        if (attribute.getEvaluator() != null) {
            hasStrongChanges = true;
        }
        if (!super.remove(attribute)) {
            return false;
        }
        this.fixAll();
        return true;
    }

    public void setDefaultGeometryAttributeName(String name) {
        if (name == null || name.length() == 0) {
            this.defaultGeometryAttributeName = null;
            this.defaultGeometryAttributeIndex = -1;
            return;
        }
        DefaultFeatureAttributeDescriptor attr = (DefaultFeatureAttributeDescriptor) this
                .get(name);
        if (attr == null) {
            throw new IllegalArgumentException("Attribute '" + name
                    + "' not found.");
        }
        if (attr.getType() != DataTypes.GEOMETRY) {
            throw new IllegalArgumentException("Attribute '" + name
                    + "' is not a geometry.");
        }
        this.defaultGeometryAttributeName = name;
        this.defaultGeometryAttributeIndex = attr.getIndex();
    }

    public void setHasOID(boolean hasOID) {
        this.hasOID = hasOID;
    }

    protected Iterator getIterator(Iterator iter) {
        return new EditableDelegatedIterator(iter, this);
    }

    public EditableFeatureAttributeDescriptor getEditableAttributeDescriptor(String name) {
        return (EditableFeatureAttributeDescriptor) this.getAttributeDescriptor(name);
    }

    public EditableFeatureAttributeDescriptor getEditableAttributeDescriptor(int index) {
        return (EditableFeatureAttributeDescriptor) this.getAttributeDescriptor(index);
    }

    protected class EditableDelegatedIterator extends DelegatedIterator {

        private DefaultEditableFeatureType fType;

        public EditableDelegatedIterator(Iterator iter,
                DefaultEditableFeatureType fType) {
            super(iter);
            this.fType = fType;
        }

        public void remove() {
            this.iterator.remove();
            this.fType.fixAll();
        }

    }

    protected void setAllowAutomaticValues(boolean value) {
        this.allowAtomaticValues = value;
    }

    public void setDefaultTimeAttributeName(String name) {
        if (name == null || name.length() == 0) {
            this.defaultTimeAttributeIndex = -1;
            return;
        }
        DefaultFeatureAttributeDescriptor attr = (DefaultFeatureAttributeDescriptor) this
                .get(name);
        if (attr == null) {
            throw new IllegalArgumentException("Attribute '" + name
                    + "' not found.");
        }

        this.defaultTimeAttributeIndex = attr.getIndex();
    }
}
