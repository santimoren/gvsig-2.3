/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureIndexes;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.dispose.DisposableIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DOCUMENT ME!
 *
 * @author Vicente Caballero Navarro
 */
public class SpatialManager {
    private static final Logger LOG = LoggerFactory.getLogger(SpatialManager.class);
    
    protected boolean isFullExtentDirty = true;
    private FeatureStore featureStore;
    private FeatureIndex featureIndex = null;
    private Envelope originalEnvelope = null;
    private Envelope fullEnvelope = null;
    private ArrayList feaOperation=new ArrayList();
    private boolean noSpatialData = false;

    public SpatialManager(FeatureStore featureStore, Envelope originalEnvelope)
            throws DataException {
        this.featureStore=featureStore;
        FeatureIndexes findexes=featureStore.getIndexes();
        // Comprobamos si hay algun campo espacial a manejar

        FeatureType fType = this.featureStore.getDefaultFeatureType();
        // TODO Multy FType !!
        if (fType.getDefaultGeometryAttributeIndex() < 0) {
            noSpatialData = true;
            return;
        }
        FeatureAttributeDescriptor attr = fType.getAttributeDescriptor(fType
                .getDefaultGeometryAttributeIndex());
        this.originalEnvelope = originalEnvelope;
        if ((originalEnvelope != null) && (!originalEnvelope.isEmpty())) {
            this.fullEnvelope = originalEnvelope.getGeometry().getEnvelope();
        } else {
            FeatureAttributeDescriptor geoAttr = fType.getAttributeDescriptor(fType.getDefaultGeometryAttributeIndex());
            try {
                this.fullEnvelope = GeometryLocator.getGeometryManager()
                        .createEnvelope(geoAttr.getGeometrySubType());
            } catch (Exception e) {
                // FIXME Excpetion
                throw new RuntimeException(e);
            }
        }
        if (!fType.hasOID()) {
            return;
        }

        Iterator iterator = findexes.iterator();
        FeatureIndex index;
        while (iterator.hasNext()) {
            index = (FeatureIndex) iterator.next();
            if (index.getAttributeNames().size() == 1
                    && index.getAttributeNames().contains(attr.getName())) {
                featureIndex = index;
                break;
            }
        }

        if (featureIndex instanceof DefaultFeatureIndex) {
            ((DefaultFeatureIndex) featureIndex).setValid(true);
        }
    }



    /**
     * DOCUMENT ME!
     *
     * @param feature DOCUMENT ME!
     * @param oldFeature DOCUMENT ME!
     */
    public void updateFeature(Feature feature, Feature oldFeature) {
        if (noSpatialData) {
            return;
        }
            try {
            	if (featureIndex != null) {
            		featureIndex.delete(oldFeature);
            	}
                feaOperation.add(new FeatureOperation(((DefaultFeature) oldFeature)
                        .getReference(), FeatureOperation.DELETE));
            	if (featureIndex != null) {
            		featureIndex.insert(feature);
            	}
                feaOperation.add(new FeatureOperation(((DefaultFeature) feature)
                        .getReference(), FeatureOperation.INSERT));
            } catch (DataException e) {
                throw new RuntimeException("Exception updating feature: "
                    + feature, e);
            }
            // } else {
            // fullEnvelope.add(feature.getDefaultEnvelope());
        isFullExtentDirty = true;
    }

    /**
     * DOCUMENT ME!
     *
     * @param feature DOCUMENT ME!
     */
    public void insertFeature(Feature feature) {
        if (noSpatialData) {
            return;
        }
            try {
            	if (featureIndex != null) {
            		featureIndex.insert(feature);
            	}
                feaOperation.add(new FeatureOperation(
                    ((DefaultFeature) feature).getReference(),
                    FeatureOperation.INSERT));
            } catch (DataException e) {
                throw new RuntimeException("Exception inserting feature: "
                    + feature, e);
            }
            isFullExtentDirty = true;
//        } else if (!isFullExtentDirty) {
//            fullEnvelope.add(feature.getDefaultEnvelope());
//        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param feature DOCUMENT ME!
     */
    public void deleteFeature(Feature feature) {
        if (noSpatialData) {
            return;
        }
            try {
            	if (featureIndex != null) {
            		featureIndex.delete(feature);
            	}
                feaOperation.add(new FeatureOperation(((DefaultFeature) feature)
                        .getReference(), FeatureOperation.DELETE));
            } catch (DataException e) {
                throw new RuntimeException("Exception deleting feature: "
                    + feature, e);
            }
        isFullExtentDirty = true;
    }

    public void clear() {
    }

    public Envelope getEnvelope() throws DataException {
        if (noSpatialData) {
            return null;
        }
        if (!isFullExtentDirty){
            return this.fullEnvelope;
        }

        // FIXME in every changes when anyone ask for envelope it was regenerated.
        //       if we assume that the envelope may not be the minimum in edit mode
        //       this call must be very much faster


        FeatureAttributeDescriptor attr = featureStore.getDefaultFeatureType()
                .getAttributeDescriptor(
                        featureStore.getDefaultFeatureType()
                                .getDefaultGeometryAttributeIndex());
        Envelope fullEnvelope = null;

        FeatureSet set = null;
        DisposableIterator iterator = null;
        try {
            set = featureStore.getFeatureSet();
            iterator = set.fastIterator();
            //First while to initialize the feature envelope
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Envelope envelope = feature.getDefaultEnvelope();
                if (envelope != null){
                    fullEnvelope = (Envelope)envelope.clone();
                    break;
                }
            }           
            //Second while to add new evelopes tho the full envelope
            while (iterator.hasNext()) {
                Feature feature = (Feature) iterator.next();
                Envelope envelope = feature.getDefaultEnvelope();           
                if(envelope!=null){
                    fullEnvelope.add(envelope);
                }
            }
            //Creating an empty envelope by default
            if (fullEnvelope == null){
                fullEnvelope = GeometryLocator.getGeometryManager().createEnvelope(
                    attr.getGeometrySubType());
            }
        } catch (ConcurrentModificationException e) {
            throw e;
        } catch (ConcurrentDataModificationException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (iterator != null) {
                iterator.dispose();
            }
            if (set != null) {
                set.dispose();
            }
        }
        this.fullEnvelope = fullEnvelope;
        this.isFullExtentDirty = false;
        return fullEnvelope;
    }



    public void cancelModifies() {
        if (noSpatialData) {
            return;
        }
        if (featureIndex != null){
            for (int i = feaOperation.size()-1 ; i>=0 ; i--){
                try {
                    FeatureOperation fo = (FeatureOperation) feaOperation.get(i);
                    if (fo.getOperation() == FeatureOperation.INSERT){                     
                        featureIndex.delete(fo.getFeatureReference().getFeature());
                    }else if (fo.getOperation() == FeatureOperation.DELETE){
                        featureIndex.insert(fo.getFeatureReference().getFeature());
                    }
                } catch (DataException e) {
                    LOG.error("Error canceling the edition", e);
                }           
            }
        }
        if (originalEnvelope!=null){
            try {
                fullEnvelope = (Envelope) originalEnvelope.clone();
            } catch (CloneNotSupportedException e) {
                // Should never happen
                LOG.error("While cloning envelope", e);
            }
        } else {
            fullEnvelope = null;
        }
        isFullExtentDirty = false;
    }

    private class FeatureOperation{
        final static int INSERT=0;
        final static int DELETE=1;
        private FeatureReference ref;
        private int operation;
        public FeatureOperation(FeatureReference fe,int op){
            ref=fe;
            operation=op;
        }
        public FeatureReference getFeatureReference() {
            return ref;
        }
        public void setFeatureReference(FeatureReference ref) {
            this.ref = ref;
        }
        public int getOperation() {
            return operation;
        }
        public void setOperation(int operation) {
            this.operation = operation;
        }
    }

}
