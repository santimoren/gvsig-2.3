/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.impl.undo.command;

import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.impl.FeatureTypeManager;
import org.gvsig.tools.undo.command.impl.AbstractCommand;

/**
 * Add the methods and constructors to use FeatureType.
 *
 * @author Vicente Caballero Navarro
 */
public abstract class AbstractFTypeCommand extends AbstractCommand {
    protected FeatureType type;
    protected FeatureTypeManager expansionManager;

    protected AbstractFTypeCommand(FeatureTypeManager expansionManager,
    		FeatureType type,
    		String description) {
        super(description);
        this.type = type;
        this.expansionManager = expansionManager;
    }

    /**
     * Returns the FeatureType of this command.
     *
     * @return FeatureType.
     */
    public FeatureType getFeatureType() {
        return type;
    }
}
