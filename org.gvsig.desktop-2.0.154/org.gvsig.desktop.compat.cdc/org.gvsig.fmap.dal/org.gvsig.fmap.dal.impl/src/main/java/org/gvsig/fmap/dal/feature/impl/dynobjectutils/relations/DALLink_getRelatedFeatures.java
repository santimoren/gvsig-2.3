package org.gvsig.fmap.dal.feature.impl.dynobjectutils.relations;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dynobject.AbstractDynMethod;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynMethodNotSupportedException;

public class DALLink_getRelatedFeatures extends AbstractDynMethod {

    private static final String DAL_code = "DAL.code";
    private static final String DAL_foreingTable = "DAL.foreingTable";
    private static final String DAL_foreingCode = "DAL.foreingCode";
    private static final String DAL_pageSize = "DAL.pageSize";


    public DALLink_getRelatedFeatures() {
        super("DAL.getRelatedFeatures","Retrieve a list with al elements related to this object.");
    }

    public Object invoke(DynObject self, Object[] args) throws DynMethodException {
        if (!(self instanceof FacadeOfAFeature)) {
            return null;
        }

        DynField_v2 field = (DynField_v2) args[0];
        String foreignCodeName = (String) field.getTags().get(DAL_foreingCode);
        String foreignTableName = (String) field.getTags().get(DAL_foreingTable);
        String codeName = (String) field.getTags().get(DAL_code);
        int pageSize;
        try {
            pageSize = field.getTags().getInt(DAL_pageSize);
        } catch (CoercionException ex) {
            pageSize = 100;
        }
        if (pageSize < 100) {
            pageSize = 100;
        }

        Feature feature = ((FacadeOfAFeature) self).getFeature();
        FeaturePagingHelper featurePager = getRelatedFeatures(feature, codeName, foreignTableName, foreignCodeName, pageSize);

        return featurePager.asListOfDynObjects();
    }

    private FeaturePagingHelper getRelatedFeatures(Feature feature, String codeName, String foreignTableName, String foreignCodeName, int pageSize) throws ComputeRelatedFeaturesException {
        Object fkValue = "<unknow>";
        try {
            FeatureStore store = feature.getStore();

            DataManager manager = DALLocator.getDataManager();
            FeatureAttributeDescriptor attrdesc = feature.getType().getAttributeDescriptor(codeName);

            DataStoreParameters foreignStoreParmeters = store.getExplorer().get(foreignTableName);
            FeatureStore foreignStore = (FeatureStore) manager.openStore(store.getProviderName(), foreignStoreParmeters);
            FeatureQuery query = foreignStore.createFeatureQuery();
            fkValue = feature.get(codeName);
            if (attrdesc.getDataType().isNumeric()) {
                fkValue = fkValue.toString();
            } else {
                fkValue = "'" + fkValue.toString().replace("'", "''") + "'";
            }
            String where = foreignCodeName + " = " + fkValue;
            query.addFilter(manager.createExpresion(where));
            FeaturePagingHelper featurePager = manager.createFeaturePagingHelper(foreignStore, query, pageSize);
            return featurePager;
        } catch (Exception ex) {
            throw new ComputeRelatedFeaturesException(foreignTableName, foreignCodeName, fkValue.toString(), ex);
        }
    }

    private static class ComputeRelatedFeaturesException extends DynMethodException {

        private final static String MESSAGE_FORMAT = "Can't retrieve relation items from %(storename)s for attribute %(attrname)s with value %(attrvalue)s.";
        private final static String MESSAGE_KEY = "_OneToManyMethodException";
        private static final long serialVersionUID = -3248317756866564508L;

        public ComputeRelatedFeaturesException(String storename, String attrname, String attrvalue, Throwable cause) {
            super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
            setValue("storename", storename);
            setValue("attrname", attrname);
            setValue("attrvalue", attrvalue);
            this.initCause(cause);
        }
    }

}
