/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.tools.undo.command.Command;

public class DefaultFeatureStoreNotification implements
		FeatureStoreNotification {
	private Feature feature = null;
	private DataSet collectionResult = null;
	private boolean loadCollectionSucefully = false;
	private Exception exceptionLoading = null;
	private Command command = null;
	private EditableFeatureType featureType = null;
	private String type;
	private DataStore source;
    private FeatureIndex index;

	protected void init(DataStore source, String type) {
		this.source = source;
		this.type = type;
	}

	public DefaultFeatureStoreNotification(DataStore source, String type) {
		this.init(source, type);
	}

	public DefaultFeatureStoreNotification(DataStore source, String type, Feature feature) {
		this.init(source, type);
		this.feature = feature;
	}
	public DefaultFeatureStoreNotification(DataStore source, String type, Command command) {
		this.init(source, type);
		this.command = command;
	}

	public DefaultFeatureStoreNotification(DataStore source, String type, Exception exception) {
		this.init(source, type);
		this.loadCollectionSucefully = false;
		this.exceptionLoading = exception;
	}

	public DefaultFeatureStoreNotification(DataStore source, String type, DataSet collection) {
		this.init(source, type);
		this.loadCollectionSucefully = true;
		this.collectionResult = collection;
	}


	public DefaultFeatureStoreNotification(DataStore source, String type,
			EditableFeatureType featureType) {
		this.init(source, type);
		this.featureType = featureType;
	}

    public DefaultFeatureStoreNotification(DataStore source, String type,
        FeatureIndex index) {
        this.init(source, type);
        this.index = index;
    }

	public Feature getFeature() {
		return feature;
	}

	public EditableFeatureType getFeatureType() {
		return featureType;
	}

	public DataSet getCollectionResult() {
		return this.collectionResult;
	}

	public Exception getExceptionLoading() {
		return this.exceptionLoading;
	}

	public boolean loadSucefully() {
		return this.loadCollectionSucefully;
	}

	public Command getCommand() {
		return this.command;
	}

	public DataStore getSource() {
		return source;
	}

	public String getType() {
		return type;
	}

    public FeatureIndex getIndex() {
        return index;
    }
}
