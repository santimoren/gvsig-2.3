/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeEmulator;
import org.gvsig.fmap.dal.feature.FeatureRules;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynMethod;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;

public class DefaultFeatureType extends ArrayList<FeatureAttributeDescriptor> implements FeatureType,
		DynClass, org.gvsig.tools.lang.Cloneable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7988721447349282215L;

	private DefaultFeatureRules rules;
	protected boolean hasEvaluators;
	protected boolean hasEmulators;
	protected String defaultGeometryAttributeName;
	protected int defaultGeometryAttributeIndex;
	protected int defaultTimeAttributeIndex;
	private String id;
	protected boolean hasOID;
	protected boolean allowAtomaticValues;
	protected FeatureAttributeDescriptor[] pk = null;
	protected String internalID = null;

	private List srsList = null; 

	protected DefaultFeatureType(String id) {
		this.internalID = Integer.toHexString((int) (Math.random()*100000)).toUpperCase();
		this.id = id;
		this.rules = new DefaultFeatureRules();
		this.hasEvaluators = false;
		this.hasEmulators = false;
		this.defaultGeometryAttributeName = null;
		this.defaultGeometryAttributeIndex = -1;
		this.defaultTimeAttributeIndex = -1;
		this.allowAtomaticValues = false;
	}

	protected DefaultFeatureType() {
		this("default");
	}

	protected DefaultFeatureType(DefaultFeatureType other) {
		this("default");
		initialize(other, true);
	}

	protected DefaultFeatureType(DefaultFeatureType other,
			boolean copyAttributes) {
		this("default");
		initialize(other, copyAttributes);
	}
        
	protected void initialize(DefaultFeatureType other, boolean copyAttributes) {
		this.id = other.getId();
		if (copyAttributes) {
                    Iterator iter = other.iterator();
                    DefaultFeatureAttributeDescriptor attr;
                    while (iter.hasNext()) {
                            attr = (DefaultFeatureAttributeDescriptor) iter.next();
                            this.intitalizeAddAttibute(attr);
                    }
                    if( other.pk!=null ) {
                        this.pk = new FeatureAttributeDescriptor[other.pk.length];
                        for( int i=0; i<other.pk.length; i++ ) {
                            this.pk[i] = other.pk[i].getCopy();
                        }
                    }
		}
		this.defaultGeometryAttributeName = other.defaultGeometryAttributeName;
		this.hasEvaluators = other.hasEvaluators;
		this.hasEmulators = other.hasEmulators;
		this.rules = (DefaultFeatureRules) other.rules.getCopy();
		this.defaultGeometryAttributeIndex = other.defaultGeometryAttributeIndex;
		this.defaultTimeAttributeIndex = other.defaultTimeAttributeIndex;
		this.hasOID = other.hasOID;
		this.id = other.id; // XXX ???? copiar o no esto????
                this.internalID = other.internalID;
	}

	protected void intitalizeAddAttibute(DefaultFeatureAttributeDescriptor attr) {
		super.add(attr.getCopy());
	}

	public String getId() {
		return this.id;
	}

	public Object get(String name) {
		FeatureAttributeDescriptor attr;
		Iterator iter = this.iterator();
		while (iter.hasNext()) {
			attr = (FeatureAttributeDescriptor) iter.next();
			if (attr.getName().equalsIgnoreCase(name)) {
				return attr;
			}
		}
		return null;
	}

	public FeatureAttributeDescriptor getAttributeDescriptor(String name) {
		FeatureAttributeDescriptor attr;
		Iterator iter = this.iterator();
		while (iter.hasNext()) {
			attr = (FeatureAttributeDescriptor) iter.next();
			if (attr.getName().equalsIgnoreCase(name)) {
				return attr;
			}
		}
		return null;
	}

	public FeatureAttributeDescriptor getAttributeDescriptor(int index) {
		return (FeatureAttributeDescriptor) super.get(index);
	}

	public FeatureType getCopy() {
		return new DefaultFeatureType(this);
	}
        
        public Object clone() {
            return this.getCopy();
        }

	public int getDefaultGeometryAttributeIndex() {
		return this.defaultGeometryAttributeIndex;
	}

	public String getDefaultGeometryAttributeName() {
		return this.defaultGeometryAttributeName;
	}

	public EditableFeatureType getEditable() {
		return new DefaultEditableFeatureType(this);
	}

	public int getIndex(String name) {
		FeatureAttributeDescriptor attr;
		Iterator iter = this.iterator();
		while (iter.hasNext()) {
			attr = (FeatureAttributeDescriptor) iter.next();
			if (attr.getName().equalsIgnoreCase(name)) {
				return attr.getIndex();
			}
		}
		return -1;
	}

	public FeatureRules getRules() {
		return this.rules;
	}

	public boolean hasEvaluators() {
		return this.hasEvaluators;
	}

	public boolean hasEmulators() {
		return this.hasEmulators;
	}

	public List getSRSs() {
		if (this.srsList == null) {
			ArrayList tmp = new ArrayList();
			Iterator iter = iterator();
			Iterator tmpIter;
			boolean allreadyHave;
			IProjection tmpSRS;
			FeatureAttributeDescriptor attr;
			while (iter.hasNext()){
				attr = (FeatureAttributeDescriptor) iter.next();
				if (attr.getDataType().getType() == DataTypes.GEOMETRY
						&& attr.getSRS() != null) {
					allreadyHave = false;
					tmpIter = tmp.iterator();
					while (tmpIter.hasNext()) {
						tmpSRS = (IProjection) tmpIter.next();
						if (tmpSRS.getAbrev().equals(attr.getSRS().getAbrev())) {
							allreadyHave = true;
							break;
						}
					}
					if (!allreadyHave) {
						tmp.add(attr.getSRS());
					}
				}
			}
			this.srsList = Collections.unmodifiableList(tmp);
		}
		return this.srsList;
	}

	public IProjection getDefaultSRS() {
		if (this.getDefaultGeometryAttributeIndex() < 0) {
			return null;
		}
		return this.getAttributeDescriptor(
				this.getDefaultGeometryAttributeIndex()).getSRS();
	}

	public void validateFeature(Feature feature, int mode) throws DataException {
            DefaultFeatureRules rules = (DefaultFeatureRules) this.getRules();
            rules.validate(feature,mode);
	}

	public FeatureType getSubtype(String[] names) throws DataException {
                if( names==null || names.length <1) {
                    return (FeatureType) this.clone();
                }
		return new SubtypeFeatureType(this, names, null);
	}

	public FeatureType getSubtype(String[] names, String[] constantsNames) throws DataException {
                if( (names==null || names.length <1) && (constantsNames==null || constantsNames.length <1) ) {
                    return (FeatureType) this.clone();
                }
		return new SubtypeFeatureType(this, names,constantsNames);
	}

	public boolean isSubtypeOf(FeatureType featureType) {
		return false;
	}



	class SubtypeFeatureType extends DefaultFeatureType {
		/**
		 *
		 */
		private static final long serialVersionUID = 6913732960073922540L;
		WeakReference parent;

                SubtypeFeatureType(DefaultFeatureType parent, String[] names, String[] constantsNames)
                        throws DataException {
                    super(parent, false);
                    DefaultFeatureAttributeDescriptor attrcopy;
                    DefaultFeatureAttributeDescriptor attr;
                    Set attrnames = null;

                    // Copy attributes
                    if ( names != null && names.length > 0 ) {
                        attrnames = new HashSet();
                        attrnames.addAll(Arrays.asList(names));
                        if ( parent.hasEmulators ) {
                            for ( int i = 0; i < parent.size(); i++ ) {
                                attr = (DefaultFeatureAttributeDescriptor) parent.getAttributeDescriptor(i);
                                FeatureAttributeEmulator emulator = attr.getFeatureAttributeEmulator();
                                if ( emulator != null ) {
                                    String ss[] = emulator.getRequiredFieldNames();
                                    if ( ss != null ) {
                                        attrnames.addAll(Arrays.asList(ss));
                                    }
                                }
                            }
                        }
                        Iterator it = attrnames.iterator();
                        int i = 0;
                        while ( it.hasNext() ) {
                            String name = (String) it.next();
                            attr = (DefaultFeatureAttributeDescriptor) parent
                                    .getAttributeDescriptor(name);
                            if ( attr == null ) {
                                throw new SubtypeFeatureTypeNameException(name, parent
                                        .getId());
                            }
                            attrcopy = new DefaultFeatureAttributeDescriptor(attr);
                            this.add(attrcopy);
                            attrcopy.index = i++;
                        }

                    } else {
                        for ( int i = 0; i < parent.size(); i++ ) {
                            attr = (DefaultFeatureAttributeDescriptor) parent.getAttributeDescriptor(i);
                            attrcopy = new DefaultFeatureAttributeDescriptor(attr);
                            this.add(attrcopy);
                            attrcopy.index = i;
                        }
                    }

                    // Set the consttants attributes.
                    if ( constantsNames != null && constantsNames.length > 0 ) {
                        for ( int i = 0; i < constantsNames.length; i++ ) {
                            if ( attrnames != null && attrnames.contains(constantsNames[i]) ) {
                                continue;
                            }
                            attr = (DefaultFeatureAttributeDescriptor) this.getAttributeDescriptor(constantsNames[i]);
                            attr.setConstantValue(true);
                        }
                    }

                    // Add missing pk fiels if any
                    if ( !parent.hasOID() ) {
                        Iterator iter = parent.iterator();
                        while ( iter.hasNext() ) {
                            attr = (DefaultFeatureAttributeDescriptor) iter.next();
                            if ( attr.isPrimaryKey() && this.getIndex(attr.getName()) < 0 ) {
                                attrcopy = new DefaultFeatureAttributeDescriptor(attr);
                                this.add(attrcopy);
                                attrcopy.index = this.size() - 1;
                            }
                        }
                    }

                    this.defaultGeometryAttributeIndex = this
                            .getIndex(this.defaultGeometryAttributeName);
                    if ( this.defaultGeometryAttributeIndex < 0 ) {
                        this.defaultGeometryAttributeName = null;
                    }
                    this.parent = new WeakReference(parent);
                }

		public FeatureType getSubtype(String[] names) throws DataException {
			return new SubtypeFeatureType((DefaultFeatureType) this.parent
					.get(), names, null);
		}

		public boolean isSubtypeOf(FeatureType featureType) {
			if (featureType == null) {
				return false;
			}
			FeatureType parent = (FeatureType) this.parent.get();
			return featureType.equals(parent);
		}

		public EditableFeatureType getEditable() {
			throw new UnsupportedOperationException();
		}
	}

	public class SubtypeFeatureTypeNameException extends DataException {

		/**
		 *
		 */
		private static final long serialVersionUID = -4414242486723260101L;
		private final static String MESSAGE_FORMAT = "Attribute name '%(name)s' not found in type (%(type)s).";
		private final static String MESSAGE_KEY = "_SubtypeFeatureTypeNameException";

		public SubtypeFeatureTypeNameException(String name, String type) {
			super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
			setValue("name", name);
			setValue("type", type);
		}
	}

	public boolean hasOID() {
		return hasOID;
	}
	public String toString(){
		StringBuffer s = new StringBuffer();
		s.append(this.getId());
		s.append(":[");
		String attName;
		for (int i = 0; i < size(); i++) {
			attName =((FeatureAttributeDescriptor)get(i)).getName().toString();
			s.append(attName);
			if (i < size() - 1) {
				s.append(',');
			}
		}
		s.append(']');
		return s.toString();
	}

	public Iterator iterator() {
		return getIterator(super.iterator());
	}

	protected Iterator getIterator(Iterator iter) {
		return new DelegatedIterator(iter);
	}

	protected class DelegatedIterator implements Iterator {

		protected Iterator iterator;

		public DelegatedIterator(Iterator iter) {
			this.iterator = iter;
		}

		public boolean hasNext() {
			return iterator.hasNext();
		}

		public Object next() {
			return iterator.next();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	public boolean allowAutomaticValues() {
		return this.allowAtomaticValues;
	}

	public FeatureAttributeDescriptor[] getAttributeDescriptors() {
		return (FeatureAttributeDescriptor[]) super
				.toArray(new FeatureAttributeDescriptor[super.size()]);
	}

	public FeatureAttributeDescriptor[] getPrimaryKey() {
		if (pk == null) {
			List pkList = new ArrayList();
			Iterator iter = super.iterator();
			FeatureAttributeDescriptor attr;
			while (iter.hasNext()){
				attr = (FeatureAttributeDescriptor) iter.next();
				if (attr.isPrimaryKey()){
					pkList.add(attr);
				}
			}
			pk = (FeatureAttributeDescriptor[]) pkList
					.toArray(new FeatureAttributeDescriptor[pkList.size()]);
		}
		return pk;
	}

	public FeatureAttributeDescriptor getDefaultGeometryAttribute() {
		if (this.defaultGeometryAttributeIndex < 0) {
			return null;
		}
		return (FeatureAttributeDescriptor) super
				.get(this.defaultGeometryAttributeIndex);
	}



	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DefaultFeatureType)) {
			return false;
		}
		DefaultFeatureType otherType = (DefaultFeatureType) other;
		if (!this.id.equals(otherType.id)) {
			return false;
		}
		if (this.size() != otherType.size()) {
			return false;
		}
		FeatureAttributeDescriptor attr,attrOther;
		Iterator iter,iterOther;
		iter = this.iterator();
		iterOther = otherType.iterator();
		while (iter.hasNext()) {
			attr = (FeatureAttributeDescriptor) iter.next();
			attrOther = (FeatureAttributeDescriptor) iterOther.next();
			if (!attr.equals(attrOther)) {
				return false;
			}
		}

		if (defaultGeometryAttributeName != otherType.defaultGeometryAttributeName) {
			if (defaultGeometryAttributeName == null) {
				return false;
			}
			return defaultGeometryAttributeName
					.equals(otherType.defaultGeometryAttributeName);

		}
		return true;

	}

	/**
	 * Start of DynClass interface implementation
	 * READONLY
	 */

	public DynField addDynField(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField getDeclaredDynField(String name) {
		return (DynField) getAttributeDescriptor(name);
	}

	public DynField[] getDeclaredDynFields() {
		return (DynField[]) getAttributeDescriptors();
	}

	public String getDescription() {
		return null;
	}

	public DynField getDynField(String name) {
		return (DynField) getAttributeDescriptor(name);
	}

	public DynField[] getDynFields() {
		return (DynField[]) getAttributeDescriptors();
	}

	public String getName() {
		return this.id + "_" + internalID;
	}

	public void removeDynField(String name) {
		throw new UnsupportedOperationException();

	}

	public void addDynMethod(DynMethod dynMethod) {
		throw new UnsupportedOperationException();

	}

	public void extend(DynClass dynClass) {
		throw new UnsupportedOperationException();

	}

	public void extend(String dynClassName) {
		throw new UnsupportedOperationException();

	}

	public void extend(String namespace, String dynClassName) {
		throw new UnsupportedOperationException();

	}

	public DynMethod getDeclaredDynMethod(String name)
			throws DynMethodException {
		return null;
	}

	public DynMethod[] getDeclaredDynMethods() throws DynMethodException {
		return null;
	}

	public DynMethod getDynMethod(String name) throws DynMethodException {
		return null;
	}

	public DynMethod getDynMethod(int code) throws DynMethodException {
		return null;
	}

	public DynMethod[] getDynMethods() throws DynMethodException {
		return null;
	}

	public DynClass[] getSuperDynClasses() {
		return null;
	}

	public boolean isInstance(DynObject dynObject) {
		if (dynObject.getDynClass().getName() == getName()) {
			return true;
		}
		return false;
	}

	public DynObject newInstance() {

		throw new UnsupportedOperationException();
	}

	public void removeDynMethod(String name) {
		throw new UnsupportedOperationException();

	}

	public DynField addDynFieldChoice(String name, int type,
			Object defaultValue, DynObjectValueItem[] values,
			boolean mandatory, boolean persistent) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldRange(String name, int type,
			Object defaultValue, Object min, Object max, boolean mandatory,
			boolean persistent) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldSingle(String name, int type,
			Object defaultValue, boolean mandatory, boolean persistent) {
		throw new UnsupportedOperationException();
	}

	public void validate(DynObject object) throws DynObjectValidateException {
		//FIXME: not sure it's the correct code
		if (object instanceof Feature) {
			Feature fea = (Feature) object;
			if (fea.getType().equals(this)) {
				return;
			}
		}
		throw new DynObjectValidateException(this.id);
	}

	public DynField addDynFieldLong(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldChoice(String name, int type,
			Object defaultValue, DynObjectValueItem[] values) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldRange(String name, int type,
			Object defaultValue, Object min, Object max) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldSingle(String name, int type, Object defaultValue) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldString(String name) {
		throw new UnsupportedOperationException();
	}
	
	public DynField addDynFieldInt(String name) {
		throw new UnsupportedOperationException();
	}
	
	public DynField addDynFieldDouble(String name) {
		throw new UnsupportedOperationException();
	}
	
	public DynField addDynFieldFloat(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldBoolean(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldList(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldMap(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldObject(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldSet(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldArray(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldDate(String name) {
		throw new UnsupportedOperationException();
	}

	public void extend(DynStruct struct) {
		throw new UnsupportedOperationException();
	}

	public String getFullName() {
        // TODO: usar el DynClassName
		return this.id;
	}

	public String getNamespace() {
		return "DALFeature";
	}

	public DynStruct[] getSuperDynStructs() {
		return null;
	}

	public void setDescription(String description) {
		throw new UnsupportedOperationException();
	}

	public void setNamespace(String namespace) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldFile(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldFolder(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldURL(String name) {
		throw new UnsupportedOperationException();
	}

	public DynField addDynFieldURI(String name) {
		throw new UnsupportedOperationException();
	}

    public boolean isExtendable(DynStruct dynStruct) {
        return false;
    }

	public void extend(DynStruct[] structs) {
		// TODO Auto-generated method stub
		
	}

	public void remove(DynStruct superDynStruct) {
		// TODO Auto-generated method stub
		
	}

	public void removeAll(DynStruct[] superDynStruct) {
		// TODO Auto-generated method stub
		
	}

	public FeatureAttributeDescriptor getDefaultTimeAttribute() {
		if (this.defaultTimeAttributeIndex < 0) {
			return null;
		}
		return (FeatureAttributeDescriptor) super
				.get(this.defaultTimeAttributeIndex);
	}
}