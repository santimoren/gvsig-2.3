/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.index.AbstractFeatureIndexProvider;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;

import com.infomatiq.jsi.IntProcedure;
import com.infomatiq.jsi.Rectangle;
import com.infomatiq.jsi.rtree.RTree;

public class JSIRSpatialIndexProvider extends AbstractFeatureIndexProvider {

	public static final String NAME = "JSIRSpatialIndexProvider";
	
	private RTree rtree = null;
	
	class ListIntProcedure implements IntProcedure {

        List solution = new ArrayList();

        public boolean execute(int arg0) {
            solution.add(new Integer(arg0));
            return true;
        }

        public List getSolution() {
            return solution;
        }
    }
	
	public JSIRSpatialIndexProvider() {
		
	}
	
    public void initialize() {
    	try {
    		this.rtree = createRTree();
		} catch (Exception e) {
			throw new RuntimeException();
		}
    }
    
    private RTree createRTree() {
        RTree rtree = new RTree();
        Properties props = new Properties();
        // props.setProperty("MaxNodeEntries", "500");
        // props.setProperty("MinNodeEntries", "200");
        rtree.init(props);
        return rtree;
    }

    public void insert(Object value, FeatureReferenceProviderServices fref) {
        Envelope env = getEnvelope(value);

        if (env == null) {
            throw new IllegalArgumentException(
                "value is neither Geometry or Envelope");
        }

        Object oid = fref.getOID();
        if (!isCompatibleOID(oid)) {
            throw new IllegalArgumentException("OID type not compatible: "
                + oid.getClass().getName());
        }

        rtree.add(toJsiRect(env), ((Number) oid).intValue());
    }

    public void delete(Object value, FeatureReferenceProviderServices fref) {
        Envelope env = getEnvelope(value);

        if (env == null) {
            throw new IllegalArgumentException(
                "value is neither Geometry or Envelope");
        }

        Object oid = fref.getOID();
        if (!isCompatibleOID(oid)) {
            throw new IllegalArgumentException("OID type not compatible: "
                + oid.getClass().getName());
        }

        rtree.delete(toJsiRect(env), ((Number) oid).intValue());
    }

    public List match(Object value) throws FeatureIndexException {
        Envelope env = getEnvelope(value);

        if (env == null) {
            throw new IllegalArgumentException(
                "value is neither Geometry or Envelope");
        }
        ListIntProcedure solution = new ListIntProcedure();
        rtree.intersects(toJsiRect(env), solution);
        return new LongList(solution.getSolution());
    }

    public List match(Object min, Object max) {
        throw new UnsupportedOperationException(
            "Can't perform this kind of search.");
    }

    public List nearest(int count, Object value) {
        if (value == null) {
            throw new IllegalArgumentException("value is null");
        }

        if (value instanceof Point) {
            Point p = (Point) value;
            com.infomatiq.jsi.Point jsiPoint =
                new com.infomatiq.jsi.Point((float) p.getDirectPosition()
                    .getOrdinate(0), (float) p.getDirectPosition().getOrdinate(
                    1));
            return (List) rtree.nearest(jsiPoint, count);
        } else {
            Envelope env = getEnvelope(value);

            if (env == null) {
                throw new IllegalArgumentException(
                    "value is neither Geometry or Envelope");
            }
            return new LongList((List) rtree.nearest(toJsiRect(env), count));
        }
    }

    public boolean isMatchSupported() {
        return true;
    }

    public boolean isNearestSupported() {
        return true;
    }

    public boolean isNearestToleranceSupported() {
        return false;
    }

    public boolean isRangeSupported() {
        return false;
    }

    public List nearest(int count, Object value, Object tolerance)
        throws FeatureIndexException {
        throw new UnsupportedOperationException();
    }

    public List range(Object value1, Object value2)
        throws FeatureIndexException {
        throw new UnsupportedOperationException();
    }

    /**
     * Indicates whether the given OID's type is compatible
     * with this provider
     * 
     * @param oid
     * 
     * @return
     *         true if this index provider supports the given oid type
     */
    private boolean isCompatibleOID(Object oid) {
        if (!(oid instanceof Number)) {
            return false;
        }

        long num = ((Number) oid).longValue();

        if (num > Integer.MAX_VALUE || num < Integer.MIN_VALUE) {
            return false;
        }

        return true;
    }

    protected Envelope getEnvelope(Object value) {
        Envelope env = null;

        if (value instanceof Envelope) {
            env = (Envelope) value;
        } else
            if (value instanceof Geometry) {
                env = ((Geometry) value).getEnvelope();
            }
        return env;
    }
    
    protected Rectangle toJsiRect(Envelope env) {
        Point min = env.getLowerCorner();
        Point max = env.getUpperCorner();

        Rectangle jsiRect =
            new Rectangle((float) min.getX(), (float) min.getY(),
                (float) max.getX(), (float) max.getY());
        return jsiRect;
    }

    public void clear() throws DataException {
        rtree = createRTree();
    }
 }
