/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.raster.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataQuery;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStoreNotification;
import org.gvsig.fmap.dal.impl.DefaultDataManager;
import org.gvsig.fmap.dal.raster.CoverageSelection;
import org.gvsig.fmap.dal.raster.CoverageStore;
import org.gvsig.fmap.dal.raster.spi.CoverageStoreProvider;
import org.gvsig.fmap.dal.raster.spi.CoverageStoreProviderServices;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.spi.DataStoreInitializer;
import org.gvsig.fmap.dal.spi.DataStoreProvider;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.RelativeInterval;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.undo.command.Command;
import org.gvsig.tools.visitor.Visitor;

/**
 * @author jmvivo
 *
 */
public class DefaultCoverageStore extends AbstractDisposable implements
		CoverageStore, CoverageStoreProviderServices, DataStoreInitializer {

//	final static private Logger logger = LoggerFactory
//			.getLogger(DefaultFeatureStore.class);

	private DataStoreParameters parameters = null;
	private CoverageSelection selection;

//	private long versionOfUpdate = 0;

	private DefaultDataManager dataManager = null;

	private CoverageStoreProvider provider = null;

	private DelegatedDynObject metadata;

	private DelegateWeakReferencingObservable delegateObservable = new DelegateWeakReferencingObservable(
			this);

	public DefaultCoverageStore() {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.fmap.dal.impl.DataStoreImplementation#intializePhase1(org.gvsig
	 * .fmap.dal.impl.DefaultDataManager,
	 * org.gvsig.fmap.dal.DataStoreParameters)
	 */
	public void intializePhase1(DataManager dataManager,
			DataStoreParameters parameters) throws InitializeException {
		DynObjectManager dynManager = ToolsLocator.getDynObjectManager();

		this.metadata = (DelegatedDynObject) dynManager
				.createDynObject( 
						MetadataLocator.getMetadataManager().getDefinition(DataStore.SPATIAL_METADATA_DEFINITION_NAME)
				);
		this.dataManager = (DefaultDataManager)dataManager;
		this.parameters = parameters;

	}



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.fmap.dal.impl.DataStoreImplementation#intializePhase2(org.gvsig
	 * .fmap.dal.spi.DataStoreProvider)
	 */
	public void intializePhase2(DataStoreProvider provider)
			throws InitializeException {

		this.provider = (CoverageStoreProvider) provider;
		this.delegate(provider);

	}


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#createSelection()
	 */
	public DataSet createSelection() throws DataException {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getChildren()
	 */
	public Iterator getChildren() {
		return this.provider.getChilds();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getDataSet()
	 */
	public DataSet getDataSet() throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getDataSet(org.gvsig.fmap.dal.DataQuery)
	 */
	public DataSet getDataSet(DataQuery dataQuery) throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

	public void accept(Visitor visitor) throws BaseException {
		// TODO Auto-generated method stub
	}

	public void accept(Visitor visitor, DataQuery dataQuery)
			throws BaseException {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getDataSet(org.gvsig.tools.observer.Observer)
	 */
	public void getDataSet(Observer observer) throws DataException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getDataSet(org.gvsig.fmap.dal.DataQuery, org.gvsig.tools.observer.Observer)
	 */
	public void getDataSet(DataQuery dataQuery, Observer observer)
			throws DataException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getExplorer()
	 */
	public DataServerExplorer getExplorer() throws DataException,
			ValidateDataParametersException {
		return provider.getExplorer();
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getParameters()
	 */
	public DataStoreParameters getParameters() {
		return this.parameters;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#getSelection()
	 */
	public DataSet getSelection() throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#refresh()
	 */
	public void refresh() throws DataException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.DataStore#setSelection(org.gvsig.fmap.dal.DataSet)
	 */
	public void setSelection(DataSet selection) throws DataException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.ComplexObservable#beginComplexNotification()
	 */
	public void beginComplexNotification() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.ComplexObservable#disableNotifications()
	 */
	public void disableNotifications() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.ComplexObservable#enableNotifications()
	 */
	public void enableNotifications() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.ComplexObservable#endComplexNotification()
	 */
	public void endComplexNotification() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.Observable#addObserver(org.gvsig.tools.observer.Observer)
	 */
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.Observable#deleteObserver(org.gvsig.tools.observer.Observer)
	 */
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.observer.Observable#deleteObservers()
	 */
	public void deleteObservers() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#getState()
	 */
	public PersistentState getState() throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#loadState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void saveToState(PersistentState state) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.gvsig.tools.persistence.Persistent#setState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void loadFromState(PersistentState state) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	//
	// ====================================================================
	// Metadata related methods
	//

	public Object getMetadataID() {
		return this.provider.getSourceId();
	}

	public void delegate(DynObject dynObject) {
		this.metadata.delegate(dynObject);
	}

	public DynClass getDynClass() {
		return this.metadata.getDynClass();
	}

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		return this.metadata.getDynValue(name);
	}

	public boolean hasDynValue(String name) {
		return this.metadata.hasDynValue(name);
	}

	public void implement(DynClass dynClass) {
		this.metadata.implement(dynClass);
	}

	public Object invokeDynMethod(String name, Object[] args)
			throws DynMethodException {
		return this.metadata.invokeDynMethod(this, name, args);
	}

	public Object invokeDynMethod(int code, Object[] args)
			throws DynMethodException {
		return this.metadata.invokeDynMethod(this, code, args);
	}

	public void setDynValue(String name, Object value)
			throws DynFieldNotFoundException {
		this.metadata.setDynValue(name, value);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.metadata.Metadata#getMetadataChildren()
	 */
	public Set getMetadataChildren() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.metadata.Metadata#getMetadataName()
	 */
	public String getMetadataName() {
		return this.provider.getProviderName();
	}


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.raster.spi.CoverageStoreProviderServices#createDefaultCoverageSelection()
	 */
	public CoverageSelection createDefaultCoverageSelection()
			throws DataException {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.raster.spi.CoverageStoreProviderServices#getManager()
	 */
	public DataManager getManager() {
		return this.dataManager;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.dal.raster.spi.CoverageStoreProviderServices#getProvider()
	 */
	public CoverageStoreProvider getProvider() {
		return this.provider;
	}

	public void notifyChange(String notification) {
		delegateObservable
				.notifyObservers(new DefaultCoverageStoreNotification(
				this, notification));

	}


	public void notifyChange(String notification, Command command) {
		delegateObservable
				.notifyObservers(new DefaultCoverageStoreNotification(
				this, notification, command));
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices#notifyChange
	 * (java.lang.String, org.gvsig.fmap.dal.resource.Resource)
	 */
	public void notifyChange(String notification, Resource resource) {
		delegateObservable.notifyObservers(new DefaultFeatureStoreNotification(
				this, FeatureStoreNotification.RESOURCE_CHANGED));
	}

	public void open() throws OpenException {
		// TODO: Se puede hacer un open estando en edicion ?
		this.notifyChange(DataStoreNotification.BEFORE_OPEN);
		this.provider.open();
		this.notifyChange(DataStoreNotification.AFTER_OPEN);
	}

	public void close() throws CloseException {
		// TODO: Se puede hacer un close estando en edicion ?
		this.notifyChange(DataStoreNotification.BEFORE_CLOSE);
		this.provider.close();
		this.notifyChange(DataStoreNotification.AFTER_CLOSE);
	}

	protected void doDispose() throws BaseException {
		this.notifyChange(DataStoreNotification.BEFORE_DISPOSE);
		this.provider.dispose();
		if (this.selection != null) {
			this.selection.dispose();
			this.selection = null;
		}

		this.parameters = null;
		this.notifyChange(DataStoreNotification.AFTER_DISPOSE);
		this.delegateObservable.deleteObservers();
		this.delegateObservable = null;
	}

	public boolean allowWrite() {
		return this.provider.allowWrite();
	}

	public DataQuery createQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	public DataStore getStore() {
		return this;
	}

	public CoverageStore getCoverageStore() {
		return this;
	}

	public void clear() {
		if (metadata != null) {
			metadata.clear();
		}
	}

	public String getProviderName() {
		return this.provider.getProviderName();
	}
	
	public String getName() {
		return this.provider.getName();
	}

	public String getFullName() {
		return this.provider.getFullName();
	}

    public Interval getInterval() {
        // TODO Auto-generated method stub
        return null;
    }
    
    public Collection getTimes() {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection getTimes(Interval interval) {
        // TODO Auto-generated method stub
        return null;
    }   
}
