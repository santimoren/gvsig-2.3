/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.featureset;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.impl.DefaultFeature;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.exception.BaseException;

public class FastDefaultIterator extends DefaultIterator {

	DefaultFeature myFeature;

	public FastDefaultIterator(DefaultFeatureSet featureSet, long index)
			throws DataException {
		super(featureSet);
		this.initializeFeature();
		if (index > 0) {
			if (featureSet.provider.canIterateFromIndex()) {
				try {
					this.iterator = featureSet.provider.fastIterator(index);
				} catch (UnsupportedOperationException e) {
					this.iterator = featureSet.provider.fastIterator();
					skypto(index);
				}
			} else {
				this.iterator = featureSet.provider.fastIterator();
				skypto(index);
			}
		} else {
			this.iterator = featureSet.provider.fastIterator();
		}

	}

	protected DefaultFeature createFeature(FeatureProvider fData)
			throws DataException {
		fData.setNew(false);
		this.myFeature.setData(fData);

		if (this.fset.transform.isEmpty()) {
			return myFeature;
		} else {
			return (DefaultFeature) this.fset.transform.applyTransform(
					myFeature, fset
							.getDefaultFeatureType());
		}
	}

	protected void initializeFeature() {
		myFeature = new DefaultFeature(fset.store);
	}

	public void remove() {
		super.remove();
		this.initializeFeature();
	}

	protected void doDispose() throws BaseException {
		super.doDispose();
		myFeature = null;
	}

}