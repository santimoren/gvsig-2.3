/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.undo.command;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.tools.undo.RedoException;
import org.gvsig.tools.undo.command.impl.AbstractCommand;


/**
 * Add the methods and constructors to use Features.
 *
 * @author Vicente Caballero Navarro
 */
public abstract class AbstractFeatureCommand extends AbstractCommand {
    protected Feature feature;
    protected DefaultFeatureStore featureStore;
    
    protected AbstractFeatureCommand(DefaultFeatureStore featureStore,         
        Feature feature, String description) {
        super(description);
        this.featureStore = featureStore;
        this.feature = feature;
    }

    /**
     * Returns the Feature of this command.
     * 
     * @return Feature.
     */
    public Feature getFeature() {
        return feature;
    }


    public int getType() {
        return DELETE;
    }
    
    
    public void redo() throws RedoException {
        try {
            execute();
        } catch (DataException e) {
            throw new RedoException(this, e);
        }
    }

    public abstract void execute() throws DataException;      
}
