/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.AbstractFeatureStoreTransform;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.impl.expansionadapter.ExpansionAdapter;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider.FeatureTypeChanged;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class FeatureTypeManager {

    private ExpansionAdapter expansionAdapter;
    private ArrayList deleted = new ArrayList();// <FeatureID>
    private int deltaSize = 0;
    private HashMap added = new HashMap();
    private HashMap modifiedFromOriginal = new HashMap();
    private FeatureType originalType = null;
    private boolean first = true;
    private FeatureTypeManagerFeatureStoreTransforms transforms;
    private FeatureStore store;

    public FeatureTypeManager(FeatureStore store,
            ExpansionAdapter expansionAdapter) {
        this.expansionAdapter = expansionAdapter;
        this.store = store;
        this.transforms = new FeatureTypeManagerFeatureStoreTransforms();
        this.transforms.setFeatureStore(store);
    }

    public void dispose() {
        this.expansionAdapter.close();
        this.expansionAdapter = null;
        this.deleted.clear();
        this.deleted = null;
        this.transforms.clear();
    }

    public FeatureType getType(String id) throws DataException {
        Integer intNum = ((Integer) added.get(id));
        if (intNum == null) {
            intNum = ((Integer) modifiedFromOriginal.get(id));
            if (intNum == null) {
                return null;
            }
        }
        int num = intNum.intValue();

        if (num == -1) {
            /*
             * This happens for example when we are going back to the
             * original feature type which is not managed by
             * expansionAdapter
             */
            return null;
        }

        FeatureType type = (FeatureType) expansionAdapter.getObject(num);
        return type;
    }

    public int update(FeatureType type, FeatureType oldType) {
        // deleted.add(oldType.getId());
        if (first) {
            originalType = oldType;
            first = false;
        }
        int oldNum = -1;
        int num = expansionAdapter.addObject(type);
        String id = type.getId();

        if (added.containsKey(id)) {
            oldNum = ((Integer) added.get(id)).intValue();
            added.put(id, new Integer(num));
        } else {
            if (modifiedFromOriginal.get(id) != null) {
                oldNum = ((Integer) modifiedFromOriginal.get(id)).intValue();
            }
            modifiedFromOriginal.put(id, new Integer(num));
        }

        try {
            this.transforms.add(new UpdateFeatureTypeTransform(this.store,
                    oldType, type));
        } catch (DataException e) {
            throw new RuntimeException(); // FIXME (pero esto no deberia de
            // pasar nunca)
        }
        return oldNum;
    }

    private class UpdateFeatureTypeTransform extends AbstractFeatureStoreTransform {

        private FeatureType ftSource;

        private EditableFeatureType ftTarget_editable;
        private FeatureType ftTarget_non_editable;

        private WeakReference wkRefStore;
        private List ftypes = null;
        private List attrInSourceToUse;

        UpdateFeatureTypeTransform(FeatureStore featureStore,
                FeatureType ftSource, FeatureType ftTarget) {
            this.ftSource = ftSource;

            if (ftTarget instanceof EditableFeatureType) {

                ftTarget_editable = (EditableFeatureType) ftTarget;
                ftTarget_non_editable = ftTarget_editable.getNotEditableCopy();
            } else {
                ftTarget_non_editable = ftTarget;
            }

            this.wkRefStore = new WeakReference(featureStore);
            this.initializeAttributesToUse();
        }

        private void initializeAttributesToUse() {
            attrInSourceToUse = new ArrayList();

            Iterator iter = null;
            if (ftTarget_editable != null) {
                iter = ftTarget_editable.iterator();
            } else {
                iter = ftTarget_non_editable.iterator();
            }

            FeatureAttributeDescriptor tAttr, sAttr;
            EditableFeatureAttributeDescriptor ead = null;
            while (iter.hasNext()) {
                tAttr = (FeatureAttributeDescriptor) iter.next();
                sAttr = this.ftSource.getAttributeDescriptor(tAttr.getName());
                if (sAttr == null) {
                    if (tAttr instanceof EditableFeatureAttributeDescriptor) {
                        ead = (EditableFeatureAttributeDescriptor) tAttr;
                        if (ead.getOriginalName() != null) {
                            sAttr = this.ftSource.getAttributeDescriptor(ead.getOriginalName());
                            if (sAttr == null) {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                if (tAttr.getType() != sAttr.getType()) {
                    /*
                     * Ignore if type is not the same (user removed field
                     * and added another with same name but different type)
                     */
                    continue;
                }
                attrInSourceToUse.add(sAttr.getName());
            }
        }

        public void applyTransform(Feature source, EditableFeature target)
                throws DataException {

            Iterator iter = target.getType().iterator();
            FeatureAttributeDescriptor tAttr;
            FeatureAttributeDescriptor tAttr_edi;
            FeatureAttributeDescriptor srcAtt;

            /*
             * field name in source feature
             */
            String s_name;

            /*
             * field name in target feature (the same as in source
             * except if renamed)
             */
            String t_name;

            EditableFeatureAttributeDescriptor eatd = null;
            while (iter.hasNext()) {
                tAttr = (FeatureAttributeDescriptor) iter.next();

                if (ftTarget_editable != null) {
                    /*
                     * If target FT is editable, try to get original name
                     */
                    t_name = tAttr.getName();
                    s_name = t_name;
                    tAttr_edi = ftTarget_editable.getAttributeDescriptor(t_name);
                    if (tAttr_edi instanceof EditableFeatureAttributeDescriptor) {
                        eatd = (EditableFeatureAttributeDescriptor) tAttr_edi;
                        s_name = eatd.getOriginalName();
                    }

                    /*
                     * If not found, use normal name
                     */
                    if (s_name == null) {
                        s_name = tAttr.getName();
                    }
                } else {
                    /*
                     * If target FT is not editable, use normal name
                     */
                    t_name = tAttr.getName();
                    s_name = t_name;
                }

                srcAtt = source.getType().getAttributeDescriptor(s_name);

                if (srcAtt != null
                        && /*
                         * This prevents the case when user has removed field and
                         * added new field with same name and different type.
                         * In that case, value will be the default value (else below)
                         */ srcAtt.getType() == tAttr.getType()) {

                    target.set(t_name, source.get(s_name));

                } else {
                    target.set(t_name, tAttr.getDefaultValue());
                }
            }
        }

        public FeatureType getDefaultFeatureType() throws DataException {
            return this.ftTarget_non_editable;
        }

        public FeatureStore getFeatureStore() {
            return (FeatureStore) this.wkRefStore.get();
        }

        public List getFeatureTypes() throws DataException {
            if (this.ftypes == null) {
                this.ftypes = Arrays
                        .asList(new FeatureType[]{this.ftTarget_non_editable});
            }
            return this.ftypes;
        }

        public FeatureType getSourceFeatureTypeFrom(
                FeatureType targetFeatureType) {
            EditableFeatureType orgType = ftSource.getEditable();
            Iterator iter = orgType.iterator();
            FeatureAttributeDescriptor attr;
            EditableFeatureAttributeDescriptor efad = null;

            while (iter.hasNext()) {
                attr = (FeatureAttributeDescriptor) iter.next();
                if (!attrInSourceToUse.contains(attr.getName())) {
                    if (attr instanceof EditableFeatureAttributeDescriptor) {
                        efad = (EditableFeatureAttributeDescriptor) attr;
                        if (efad.getOriginalName() != null
                                && !attrInSourceToUse.contains(efad.getOriginalName())) {
                            iter.remove();
                        }
                    } else {
                        iter.remove();
                    }
                }
            }
            return orgType.getNotEditableCopy();
        }

        public void setFeatureStore(FeatureStore featureStore) {
            this.wkRefStore = new WeakReference(featureStore);
        }

        public boolean isTransformsOriginalValues() {
            return false;
        }

    }

    public void restore(String id) {
        deleted.remove(id);
        deltaSize++;
    }

    public void restore(String id, int num) {
        if (added.containsKey(id)) {
            added.put(id, new Integer(num));
        } else {
            modifiedFromOriginal.put(id, new Integer(num));
        }
    }

    public boolean isDeleted(FeatureType type) {
        return deleted.contains(type.getId());
    }

    public boolean isDeleted(String id) {
        return deleted.contains(id);
    }

    public void clear() {
        added.clear();
        modifiedFromOriginal.clear();
        expansionAdapter.close();
        deleted.clear();// <FeatureID>
        deltaSize = 0;
    }

    public boolean hasChanges() {
        return added.size() > 0 || modifiedFromOriginal.size() > 0
                || deleted.size() > 0;
    }

    public Iterator newsIterator() {
        return added.values().iterator();
    }

    public boolean hasNews() {
        return !added.isEmpty();
    }

    public long getDeltaSize() {
        return deltaSize;
    }

    public FeatureType getOriginalFeatureType() {
        return originalType;
    }

    public DefaultFeatureStoreTransforms getTransforms() {
        return this.transforms;
    }

    public class FeatureTypeManagerFeatureStoreTransforms extends
            DefaultFeatureStoreTransforms {

        private FeatureTypeManagerFeatureStoreTransforms() {

        }

        protected void checkEditingMode() {
        }

        protected void notifyChangeToStore() {
        }

        public PersistentState getState() throws PersistenceException {
            // FIXME
            throw new UnsupportedOperationException();
        }

        public void loadState(PersistentState state)
                throws PersistenceException {
            // FIXME
            throw new UnsupportedOperationException();
        }

        public void loadFromState(PersistentState state) throws PersistenceException {
            // FIXME
            throw new UnsupportedOperationException();
        }

        public FeatureStoreTransform add(FeatureStoreTransform transform)
                throws DataException {
            if (!(transform instanceof UpdateFeatureTypeTransform)) {
                // FIXME
                throw new IllegalArgumentException();
            }
            return super.add(transform);
        }

    }

    public class FeatureTypesChangedItem implements FeatureTypeChanged {

        private FeatureType source;
        private FeatureType target;

        public FeatureTypesChangedItem(FeatureType source, FeatureType target) {
            this.source = source;
            this.target = target;
        }

        public FeatureType getSource() {
            return source;
        }

        public FeatureType getTarget() {
            return target;
        }

    }

    public Iterator getFeatureTypesChanged() throws DataException {
        // FIXME this don't work for Store.fType.size() > 1
        List list = new ArrayList();
        if (modifiedFromOriginal.size() > 0) {
            FeatureType src = this.getOriginalFeatureType();
            list.add(new FeatureTypesChangedItem(src, this.store
                    .getFeatureType(src.getId())));
        }
        return list.iterator();
    }

}
