/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.dal.store.memory;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;


/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class MemoryStoreLibrary extends AbstractLibrary{

    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
    }

	public void doInitialize()  {
		
	}

	public void doPostInitialize()  {
		List exs = new ArrayList();
		
		MemoryStoreParameters.registerParametersDefinition();
		try {
			MemoryStoreProvider.registerMetadataDefinition();
		} catch (MetadataException e) {
			exs.add(e);
		}
		
        DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator
		.getDataManager();
        
		if (!dataman.getStoreProviders().contains(MemoryStoreProvider.NAME)) {
			dataman.registerStoreProvider(MemoryStoreProvider.NAME,
					MemoryStoreProvider.class, MemoryStoreParameters.class);
		}

		if( exs.size()>0 ) {
			throw new LibraryException(this.getClass(), exs);
		}
	}
}



