package org.gvsig.fmap.dal.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataServerExplorerPool;
import org.gvsig.fmap.dal.DataServerExplorerPoolEntry;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class DefaultDataServerExplorerPool implements DataServerExplorerPool {

    
    private static final String PERSISTENCE_DEFINITION_NAME = "DataServerExplorerPool";

    private List pool;

    public DefaultDataServerExplorerPool() {
        this.pool = new ArrayList();
    }

    public boolean contains(String name) {
        DataServerExplorerPoolEntry entry = this.get(name);
        return entry!=null;
    }
    
    public boolean contains(DataServerExplorerPoolEntry entry) {
        return contains(entry.getName());
    }
    
    public boolean isEmpty() {
        return this.pool.isEmpty();
    }
    
    public void add(String name, DataServerExplorerParameters explorer) {
        this.add(name, null, explorer);
    }

    public void add(String name, String description, DataServerExplorerParameters explorer) {
        DefaultDataServerExplorerPoolEntry newexplorer = new DefaultDataServerExplorerPoolEntry(name, description, explorer);
        DataServerExplorerPoolEntry existent = this.get(name);
        if( existent!=null ) {
            newexplorer.copyTo(existent);
        } else {
            this.pool.add(newexplorer);
        }
    }

    public void remove(DataServerExplorerPoolEntry entry) {
        this.remove(entry.getName());
    }

    public void remove(int explorerIndex) {
        this.pool.remove(explorerIndex);
    }

    public void remove(String name) {
        if( StringUtils.isBlank(name) ) {
            return;
        }
        for( int i=0; i<this.pool.size(); i++ ) {
            DataServerExplorerPoolEntry entry = (DataServerExplorerPoolEntry) this.pool.get(i);
            if( name.equalsIgnoreCase(entry.getName()) ) {
                this.remove(i);
            }
        }
    }
    
    public int size() {
        return this.pool.size();
    }

    public DataServerExplorerPoolEntry get(int index) {
        return (DataServerExplorerPoolEntry) this.pool.get(index);
    }

    public DataServerExplorerPoolEntry get(String name) {
        if( StringUtils.isBlank(name) ) {
            return null;
        }
        for( int i=0; i<this.pool.size(); i++ ) {
            DataServerExplorerPoolEntry entry = (DataServerExplorerPoolEntry) this.pool.get(i);
            if( name.equalsIgnoreCase(entry.getName()) ) {
                return entry;
            }
        }
        return null;
    }

    public Iterator iterator() {
        return this.pool.iterator();
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("pool", pool);
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        List l = state.getList("pool");
        this.pool = new ArrayList();
        this.pool.addAll(l);
    }

    public static void registerPersistenceDefinition() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        if ( manager.getDefinition(PERSISTENCE_DEFINITION_NAME) == null ) {
            DynStruct definition
                    = manager.addDefinition(DefaultDataServerExplorerPool.class,
                            PERSISTENCE_DEFINITION_NAME, PERSISTENCE_DEFINITION_NAME
                            + " Persistent definition", null, null);

            definition.addDynFieldList("pool")
                    .setClassOfItems(DefaultDataServerExplorerPoolEntry.class)
                    .setMandatory(true)
                    .setPersistent(true);
        }
    }

}
