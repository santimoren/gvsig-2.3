/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.featureset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeature;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.exception.BaseException;

public class OrderedIterator extends DefaultIterator {

	protected Feature lastFeature = null;

	OrderedIterator(DefaultFeatureSet featureSet, Iterator iterator, long index) {
		super(featureSet);
		// FIXME QUE PASA CON SIZE > Integer.MAX_VALUE ?????
		if (featureSet.orderedData == null) {
			List data = new ArrayList();
			Object item;
			while (iterator.hasNext()) {
				item = iterator.next();
				if (item instanceof DefaultFeature) {
					data.add(((DefaultFeature) item).getData());
				} else {
					data.add(item);
				}
			}
			Collections.sort(data, new FeatureProviderComparator(featureSet.store,
					featureSet.query.getOrder()));
			featureSet.orderedData = data;
		}
		if (iterator instanceof DisposableIterator) {
			((DisposableIterator) iterator).dispose();
		}
		iterator = null;

		if (index < Integer.MAX_VALUE) {
			this.iterator = featureSet.orderedData.listIterator((int) index);
		} else {
			this.iterator = featureSet.orderedData.iterator();
			this.skypto(index);
		}
	}

	public OrderedIterator(DefaultFeatureSet featureSet, long index) {
		super(featureSet);
		if (index < Integer.MAX_VALUE) {
			this.iterator = featureSet.orderedData.listIterator((int) index);
		} else {
			this.iterator = featureSet.orderedData.iterator();
			this.skypto(index);
		}
	}

	public void remove() {
		super.remove();
		this.iterator.remove();
	}

	protected DefaultFeature createFeature(FeatureProvider fData)
			throws DataException {
		fData.setNew(false);
		return new DefaultFeature(fset.store, fData);

	}

	protected void doDispose() throws BaseException {
		super.doDispose();
		lastFeature = null;
	}
}

