/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.DataRuntimeException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ReversedSelectionIteratorException;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.DynObjectSetFeatureSetFacade;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the FeatureSelection interface. Internally, only
 * FeatureReference values are stored.
 *
 * This implementation performs better if used with the selection related
 * methods: select, deselect and isSelected ones.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class DefaultFeatureSelection extends DefaultFeatureReferenceSelection
		implements FeatureSelection {

	private static final Logger LOG = LoggerFactory
			.getLogger(DefaultFeatureSelection.class);

	private Map featureTypeCounts = new HashMap(1);
	private Map<Feature,Iterator> featureIterators = new HashMap<>();

	/**
	 * Creates a DefaultFeatureSelection, with a FeatureStore.
	 *
	 * @param featureStore
	 *            the FeatureStore to load Features from
	 * @throws DataException
	 *             if there is an error while getting the total number of
	 *             Features of the Store.
	 * @see AbstractSetBasedDataSelection#DefaultSelection(int)
	 */
	public DefaultFeatureSelection(DefaultFeatureStore featureStore)
			throws DataException {
		super(featureStore);
	}

	/**
	 * Creates a new Selection with the total size of Features from which the
	 * selection will be performed.
	 *
	 * @param featureStore
	 *            the FeatureStore of the selected FeatureReferences
	 * @param helper
	 *            to get some information of the Store
	 * @throws DataException
	 *             if there is an error while getting the total number of
	 *             Features of the Store.
	 */
	public DefaultFeatureSelection(FeatureStore featureStore,
			FeatureSelectionHelper helper) throws DataException {
		super(featureStore, helper);
	}

	/**
	 * Constructor used by the persistence manager. Don't use directly. After to
	 * invoke this method, the persistence manager calls the the method
	 * {@link #loadFromState(PersistentState)} to set the values of the internal
	 * attributes that this class needs to work.
	 */
	public DefaultFeatureSelection() {
		super();
	}

	public boolean select(Feature feature) {
		return select(feature, true);
	}

	/**
	 * @see #select(Feature)
	 * @param undoable
	 *            if the action must be undoable
	 */
	public boolean select(Feature feature, boolean undoable) {
		// TODO: should we check if the feature is from the same FeatureStore??
		if (feature == null) {
			return false;
		}

		// LOGGER.debug("Selected feature: {}", feature);

		if (isReversed()) {
			removeFeatureTypeCount(feature.getType());
		} else {
			addFeatureTypeCount(feature.getType());
		}
		return select(feature.getReference(), undoable);
	}

	public boolean select(FeatureSet features) throws DataException {
		return select(features, true);
	}

	/**
	 * @see #select(FeatureSet)
	 * @param undoable
	 *            if the action must be undoable
	 */
	public boolean select(FeatureSet features, boolean undoable)
			throws DataException {
		boolean change = false;
		boolean inComplex = false;
		if (undoable && getFeatureStore().isEditing()
				&& !getCommands().inComplex()) {

			getCommands().startComplex("_selectionSelectFeatureSet");
			inComplex = getCommands().inComplex();
		}

		disableNotifications();
		DisposableIterator iter = null;
		try {
			for (iter = features.fastIterator(); iter.hasNext();) {
				change |= select((Feature) iter.next(), undoable);
			}
		} finally {
			dispose(iter);
		}
		enableNotifications();
		if (undoable && getFeatureStore().isEditing() && inComplex) {
			getCommands().endComplex();
		}
		if (change) {
			notifyObservers(DataStoreNotification.SELECTION_CHANGE);
		}
		return change;
	}

	public boolean deselect(Feature feature) {
		return deselect(feature, true);
	}

	/**
	 * @see #deselect(Feature)
	 * @param undoable
	 *            if the action must be undoable
	 */
	public boolean deselect(Feature feature, boolean undoable) {
		if (feature == null) {
			return false;
		}

		LOG.debug("Deselected feature: {}", feature);

		if (isReversed()) {
			addFeatureTypeCount(feature.getType());
		} else {
			removeFeatureTypeCount(feature.getType());
		}
		return deselect(feature.getReference(), undoable);
	}

	public boolean deselect(FeatureSet features) throws DataException {
		return deselect(features, true);
	}

	/**
	 * @see #deselect(FeatureSet)
	 * @param undoable
	 *            if the action must be undoable
	 */
	public boolean deselect(FeatureSet features, boolean undoable)
			throws DataException {
		boolean change = false;
		if (undoable && getFeatureStore().isEditing()) {
			getCommands().startComplex("_selectionDeselectFeatureSet");
		}
		disableNotifications();
		DisposableIterator iter = null;
		try {
			for (iter = features.fastIterator(); iter.hasNext();) {
				change |= deselect((Feature) iter.next(), undoable);
			}
		} finally {
			dispose(iter);
		}
		enableNotifications();
		if (undoable && getFeatureStore().isEditing()) {
			getCommands().endComplex();
		}
		if (change) {
			notifyObservers(DataStoreNotification.SELECTION_CHANGE);
		}
		return change;
	}

	public boolean isSelected(Feature feature) {
		if (feature == null) {
			return false;
		}

        // Use the selection data size as a small optimization for the most
        // common case, when nothing is selected and every feature is checked
        // while drawing or painting the table document.
        if (selectionData.isReversed()) {
            return selectionData.getSize() == 0
                || !selectionData.contains(feature.getReference());
        } else {
            return selectionData.getSize() > 0
                && selectionData.contains(feature.getReference());
        }
	}

	public FeatureType getDefaultFeatureType() {
		try {
			return getFeatureStore().getDefaultFeatureType();
		} catch (DataException ex) {
			LOG.error("Error getting the default feature type "
					+ "of the FeatureStore: " + getFeatureStore(), ex);
		}
		return null;
	}

	public List getFeatureTypes() {
		// Go through the map of FeatureTypes, and return only the ones that
		// have at least a Feature.
		List types = new ArrayList();
		for (java.util.Iterator iterator = featureTypeCounts.entrySet()
				.iterator(); iterator.hasNext();) {
			Map.Entry entry = (Entry) iterator.next();
			FeatureType type = (FeatureType) entry.getKey();
			Long count = (Long) entry.getValue();

			if (count.longValue() > 0) {
				types.add(type);
			}
		}

		return types;
	}

	public long getSize() throws DataException {
		return getSelectedCount();
	}

	public boolean isEmpty() throws DataException {
		return getSelectedCount() == 0;
	}

	/**
	 * Returns the list of selected values, or the deselected ones if the
	 * selection has been reversed.
	 */
	public DisposableIterator iterator() {
		return iterator(0);
	}

	/**
	 * Returns the list of selected values, or the deselected ones if the
	 * selection has been reversed.
	 *
	 * WARN: not very good performance implementation.
	 */
	public DisposableIterator iterator(long index) {
		return iterator(index, false);
	}

	/**
	 * Returns the list of selected values, or the deselected ones if the
	 * selection has been reversed.
	 *
	 * WARN: not really a fast implementation.
	 */
	public DisposableIterator fastIterator() {
		return fastIterator(0);
	}

	/**
	 * Returns the list of selected values, or the deselected ones if the
	 * selection has been reversed.
	 *
	 * WARN: not really a fast implementation.
	 */
	public DisposableIterator fastIterator(long index) {
		return iterator(index, true);
	}

	protected void clearFeatureReferences() {
		super.clearFeatureReferences();
		featureTypeCounts.clear();
	}

	/**
	 * Creates an iterator for the Selection.
	 */
	private DisposableIterator iterator(long index, boolean fastIterator) {
		if (isReversed()) {
			DisposableIterator iter = new ReversedFeatureIteratorFacade(
					getData(), getFeatureStore(), fastIterator);
			for (long l = 0; l < index && iter.hasNext(); l++) {
				iter.next();
			}
			return iter;

		} else {
			// TODO: maybe we could add a new referenceIterator(int index)
			// method that could be implemented in a more performant way

			java.util.Iterator iter = selectionData.getSelected().iterator();
			for (long l = 0; l < index && iter.hasNext(); l++) {
				iter.next();
			}
			return new FeatureIteratorFacade(iter, getFeatureStore());
		}
	}

	private Long removeFeatureTypeCount(FeatureType featureType) {
		Long count = (Long) featureTypeCounts.get(featureType);
		if (count == null) {
			count = new Long(-1);
		} else {
			count = new Long(count.longValue() - 1);
		}
		featureTypeCounts.put(featureType, count);
		return count;
	}

	private Long addFeatureTypeCount(FeatureType featureType) {
		Long count = (Long) featureTypeCounts.get(featureType);
		if (count == null) {
			count = new Long(1);
		} else {
			count = new Long(count.longValue() + 1);
		}
		featureTypeCounts.put(featureType, count);
		return count;
	}

	/**
	 * Facade over a Iterator of FeatureReferences, to return Features instead.
	 *
	 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
	 */
	private class FeatureIteratorFacade implements DisposableIterator {

		private final Logger LOGGER = LoggerFactory
				.getLogger(FeatureIteratorFacade.class);

		private java.util.Iterator refIterator;

		private FeatureStore featureStore;
		private Feature currentFeature = null;

		public FeatureIteratorFacade(java.util.Iterator iter,
				FeatureStore featureStore) {
			this.refIterator = iter;
			this.featureStore = featureStore;
		}

		public boolean hasNext() {
			return refIterator.hasNext();
		}

		public Object next() {
			FeatureReference ref = nextFeatureReference();
			try {
			    currentFeature = featureStore.getFeatureByReference(ref);
				return currentFeature;
			} catch (DataException ex) {
				LOGGER.error(
						"Error loading the Feature with FeatureReference: "
								+ ref, ex);
				return null;
			}
		}

		/**
		 * Returns the next FeatureReference.
		 *
		 * @return the next FeatureReference
		 */
		public FeatureReference nextFeatureReference() {
			return (FeatureReference) refIterator.next();
		}

		public void remove() {
		    try {
                featureStore.delete(currentFeature);
                refIterator.remove();
            } catch (DataException e) {
                throw new RemoveFromFeatureSelectionException(e);
            }
		}

        public class RemoveFromFeatureSelectionException extends DataRuntimeException {

            /**
             *
             */
            private static final long serialVersionUID = 2636692469445838928L;
            private final static String MESSAGE_FORMAT = "Can't remove feature from selection.";
            private final static String MESSAGE_KEY = "_RemoveFromFeatureSelectionException";

            public RemoveFromFeatureSelectionException(Throwable cause) {
                super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
                //setValue("store", store);
            }
        }

		public void dispose() {
			if (refIterator instanceof DisposableIterator) {
				((DisposableIterator) refIterator).dispose();
			}
			refIterator = null;
			featureStore = null;
		}
	}

	/**
	 * Facade over a Iterator of FeatureReferences, to return Features instead,
	 * when the Selection is reversed
	 *
	 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
	 */
	private class ReversedFeatureIteratorFacade implements DisposableIterator {

		private SelectionData selectionData;

		private DisposableIterator iterator;

        private Feature nextFeature = null;
        private Feature currentFeature = null;

		private FeatureSet featureSet;

		public ReversedFeatureIteratorFacade(SelectionData selectionData,
				FeatureStore featureStore, boolean fastIterator) {
			this.selectionData = selectionData;

			// Load a Set with all the store features
			try {
				featureSet = featureStore.getFeatureSet();
				//if (fastIterator) {
					iterator = featureSet.fastIterator();
//				} else {
//					iterator = featureSet.iterator();
//				}
			} catch (DataException ex) {
				throw new ReversedSelectionIteratorException(ex);
			}

			// Filter the features not selected and position in the next
			// selected feature
			positionInNextElement();
		}

		public boolean hasNext() {
			return nextFeature != null;
		}

		public Object next() {
            featureIterators.remove(currentFeature);
		    currentFeature = nextFeature.getCopy();
		    featureIterators.put(currentFeature, this);
			positionInNextElement();
			return currentFeature ;
		}

		public void remove() {
			try {
                featureSet.delete(currentFeature);
            } catch (DataException e) {
                throw new RemoveFromFeatureSelectionException(e);

            }
		}

    	public class RemoveFromFeatureSelectionException extends DataRuntimeException {

    	    /**
             *
             */
            private static final long serialVersionUID = 2636692469445838928L;
            private final static String MESSAGE_FORMAT = "Can't remove feature from reversed selection.";
    	    private final static String MESSAGE_KEY = "_RemoveFromFeatureSelectionException";

    	    public RemoveFromFeatureSelectionException(Throwable cause) {
    	        super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
    	        //setValue("store", store);
    	    }
    	}


		private void positionInNextElement() {
			nextFeature = null;
			while (iterator.hasNext()) {
				nextFeature = (Feature) iterator.next();
				if (selectionData.contains(nextFeature.getReference())) {
					nextFeature = null;
				} else {
					break;
				}
			}
		}

		public void dispose() {
			this.featureSet.dispose();
			this.iterator.dispose();
			this.selectionData = null;
			this.nextFeature = null;
		}
	}

	public void delete(Feature feature) throws DataException {
	    Iterator it = this.featureIterators.get(feature);
	    if( it!=null ) {
	        it.remove();
	        return;
	    }
	    feature.getStore().delete(feature);
	}

	public void insert(EditableFeature feature) throws DataException {
        feature.getStore().insert(feature);
    }

	public void update(EditableFeature feature) throws DataException {
        feature.getStore().update(feature);
    }

	/*
	 * (non-Javadoc)
	 *
	 * @seeorg.gvsig.fmap.dal.feature.impl.DefaultFeatureReferenceSelection#
	 * loadFromState(org.gvsig.tools.persistence.PersistentState)
	 */
	public void loadFromState(PersistentState state)
			throws PersistenceException {
		super.loadFromState(state);

	}

	public void accept(Visitor visitor) throws BaseException {
		accept(visitor, 0);
	}

	public final void accept(Visitor visitor, long firstValueIndex)
			throws BaseException {
		try {
			doAccept(visitor, firstValueIndex);
		} catch (VisitCanceledException ex) {
			// The visit has been cancelled by the visitor, so we finish here.
			LOG.debug(
					"The visit, beggining on position {}, has been cancelled "
							+ "by the visitor: {}", new Long(firstValueIndex),
					visitor);
		}
	}

	private void doAccept(Visitor visitor, long firstValueIndex)
			throws BaseException {
		DisposableIterator iterator = fastIterator(firstValueIndex);

		if (iterator != null) {
			try {
				while (iterator.hasNext()) {
					Feature feature = (Feature) iterator.next();
					visitor.visit(feature);
				}
			} finally {
				iterator.dispose();
			}
		}
	}

	protected void doDispose() throws BaseException {
		super.doDispose();
		featureTypeCounts.clear();
	}

	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		DynStruct definition = manager.addDefinition(
				DefaultFeatureSelection.class, "DefaultFeatureSelection",
				"DefaultFeatureSelection Persistent definition", null, null);

		definition.extend(manager.getDefinition(DefaultFeatureReferenceSelection.DYNCLASS_PERSISTENT_NAME));
		definition.addDynFieldMap("featureTypeCounts")
				.setClassOfItems(Long.class).setMandatory(false);

	}

	public Object clone() throws CloneNotSupportedException {
		DefaultFeatureSelection clone = (DefaultFeatureSelection) super.clone();
		clone.featureTypeCounts = new HashMap(featureTypeCounts);
		return clone;
	}

    public DynObjectSet getDynObjectSet() {
        return new DynObjectSetFeatureSetFacade(this, getFeatureStore());
    }

    public DynObjectSet getDynObjectSet(boolean fast) {
        return new DynObjectSetFeatureSetFacade(this, getFeatureStore(), fast);
    }

}
