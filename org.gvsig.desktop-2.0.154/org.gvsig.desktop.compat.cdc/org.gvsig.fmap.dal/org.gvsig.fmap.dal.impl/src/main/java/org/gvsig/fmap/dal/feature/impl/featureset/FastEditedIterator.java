/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.featureset;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.impl.DefaultEditableFeature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReference;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.exception.BaseException;

/**
 * Iterator implementation which shares the returned Feature object instance.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class FastEditedIterator extends EditedIterator {

    DefaultFeature myFeature;

    public FastEditedIterator(DefaultFeatureSet featureSet, long index)
        throws DataException {
        super(featureSet);
        this.initializeFeature();
        if (index > 0) {
        	
            this.iterator = featureSet.provider.fastIterator();
            setNewsFeatures(null);
            skypto(index);

            /*
            * This did not work because the provider
            * skips to the index without considering the
            * changes in editing mode. We need the
            * "skypto" method which knows about those
            * changes.
        	*
            if (featureSet.provider.canIterateFromIndex()) {
                try {
                    this.iterator = featureSet.provider.fastIterator(index);
                } catch (IllegalArgumentException e) {
                    this.iterator = featureSet.provider.fastIterator();
                    setNewsFeatures(null);
                    skypto(index);
                } catch (UnsupportedOperationException e) {
                    this.iterator = featureSet.provider.fastIterator();
                    setNewsFeatures(null);
                    skypto(index);
                }
            } else {
                this.iterator = featureSet.provider.fastIterator();
                setNewsFeatures(null);
                skypto(index);
            }
            */
            
        } else {
            this.iterator = featureSet.provider.fastIterator();
            setNewsFeatures(featureManager.getInserted());
        }

    }

    protected void initializeFeature() {
        myFeature = new DefaultFeature(fset.store);
    }

    protected DefaultFeature createFeature(FeatureProvider data) throws DataException {

        DefaultFeature f = null;
        try {
            data.setNew(isFeatureIsNew());
            FeatureReference ref =
                new DefaultFeatureReference(fset.store, data);
            f =
                (DefaultFeature) featureManager.get(ref, fset.store,
                    data.getType());
        } catch (DataException e) {
            RuntimeException ex = new RuntimeException();
            e.initCause(e);
            throw ex;
        }
        if (f == null) {
            this.myFeature.setData(data);
        } else {
            // TODO Sacamos una copia del data o no???
            this.myFeature.setData(f.getData().getCopy());
        }
        if (this.fset.transform.isEmpty()) {
            return myFeature;
        } else {
            
            if (f == null) {
                myFeature = (DefaultFeature)
                    this.fset.transform.applyTransform(myFeature,
                        fset.getDefaultFeatureType());
            } else {
                /*
                 * In this case "f != null" the data comes from the featureManager,
                 * so it is data inserted/updated by the user, so we must
                 * overwrite the data after applying the transformations
                 * because the transformations can be "Adding field"
                 * and are necessary in some cases:
                 */
                DefaultFeature saved_feat = (DefaultFeature) myFeature.getCopy();
                myFeature = (DefaultFeature)
                    this.fset.transform.applyTransform(myFeature,
                        fset.getDefaultFeatureType());
                myFeature = overwrite(myFeature, saved_feat);
            }
            return myFeature;
        }       

    }

    private DefaultFeature overwrite(
        DefaultFeature old_feat,
        DefaultFeature new_feat) {
        
        DefaultEditableFeature resp = (DefaultEditableFeature) old_feat.getEditable();
        
        FeatureType fty = new_feat.getData().getType();
        FeatureAttributeDescriptor[] atts = fty.getAttributeDescriptors();
        for (int i=0; i<atts.length; i++) {
            try {
                resp.set(atts[i].getName(), new_feat.get(atts[i].getName()));
            } catch (Exception exc) {
                // Field not found
            }
        }
        return (DefaultFeature) resp.getNotEditableCopy();
    }

    public void remove() {
        super.remove();
        this.initializeFeature();
    }

    protected void doDispose() throws BaseException {
        super.doDispose();
        myFeature = null;
    }

}
