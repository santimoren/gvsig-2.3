/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureQueryOrder;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.AndEvaluator;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.evaluator.EvaluatorFieldsInfo;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * Defines the properties of a collection of Features, as a result of a query
 * through a FeatureStore.
 * <p>
 * A FeatureQuery is always defined by a FeatureType, or by the list of
 * attribute names of the FeatureStore to return.
 * </p>
 * <p>
 * The filter allows to select Features whose properties have values with the
 * characteristics defined by the filter.
 * </p>
 * <p>
 * The order is used to set the order of the result FeatureCollection, based on
 * the values of the properties of the Features.
 * </p>
 * <p>
 * The scale parameter can be used by the FeatureStore as a hint about the
 * quality or resolution of the data needed to view or operate with the data
 * returned. As an example, the FeatureStore may use the scale to return only a
 * representative subset of the data, or maybe to return Features with less
 * detail, like a point or a line instead of a polygon.
 * </p>
 * <p>
 * If an implementation of FeatureStore is able to get other parameters to
 * customize the behavior of the getDataCollection methods, there is an option
 * to set more parameters through the setAttribute method.
 * </p>
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class DefaultFeatureQuery implements FeatureQuery {

    public static final String SCALE_PARAM_NAME = "Scale";

    private Map queryParameters = new HashMap();

    private String featureTypeId = null;

    private List attributeNames = new ArrayList();

    private List constantsAttributeNames = new ArrayList();

    private Evaluator filter;

    /**
     * This boolean value is used to know if the current filter
     * has been added using the {@link FeatureQuery#addFilter(Evaluator)}
     * method or the {@link FeatureQuery#setFilter(Evaluator)} method.
     */
//    private boolean isAddFilter = true;

    private FeatureQueryOrder order = new FeatureQueryOrder();

    private long limit;

    private long pageSize;

    /**
     * Creates a FeatureQuery which will load all available Features of a type.
     *
     * @param featureType
     *            the type of Features of the query
     */
    public DefaultFeatureQuery() {
        super();
    }

    /**
     * Creates a FeatureQuery which will load all available Features of a type.
     *
     * @param featureType
     *            the type of Features of the query
     */
    public DefaultFeatureQuery(FeatureType featureType) {
        super();
        this.setFeatureType(featureType);
    }

    /**
     * Creates a FeatureQuery with the type of features, a filter and the order
     * for the FeatureCollection.
     *
     * @param featureType
     *            the type of Features of the query
     * @param filter
     *            based on the properties of the Features
     * @param order
     *            for the result
     */
    public DefaultFeatureQuery(FeatureType featureType, Evaluator filter) {
        super();
        this.setFeatureType(featureType);
        this.filter = filter;
    }

    /**
     * Creates a FeatureQuery with the type of features, a filter, the order for
     * the FeatureCollection and the view scale.
     *
     * @param featureType
     *            the type of Features of the query
     * @param filter
     *            based on the properties of the Features
     * @param order
     *            for the result
     * @param scale
     *            to view the Features.
     */
    public DefaultFeatureQuery(FeatureType featureType, Evaluator filter,
        double scale) {
        this.setFeatureType(featureType);
        this.filter = filter;
        this.setScale(scale);
    }

    /**
     * Creates a FeatureQuery which will load a list of attribute names of all
     * available Features.
     *
     * @param attributeNames
     *            the list of attribute names to load
     */
    public DefaultFeatureQuery(String[] attributeNames) {
        super();
        setAttributeNames(attributeNames);
    }

    /**
     * Creates a FeatureQuery with the list of attribute names of feature, a
     * filter and the order for the FeatureCollection.
     *
     * @param attributeNames
     *            the list of attribute names to load
     * @param filter
     *            based on the properties of the Features
     * @param order
     *            for the result
     */
    public DefaultFeatureQuery(String[] attributeNames, Evaluator filter) {
        super();
        setAttributeNames(attributeNames);
        this.filter = filter;
    }

    /**
     * Creates a FeatureQuery with the list of attribute names of feature, a
     * filter, the order for the FeatureCollection and the view scale.
     *
     * @param attributeNames
     *            the list of attribute names to load
     * @param filter
     *            based on the properties of the Features
     * @param order
     *            for the result
     * @param scale
     *            to view the Features.
     */
    public DefaultFeatureQuery(String[] attributeNames, Evaluator filter,
        double scale) {
        setAttributeNames(attributeNames);
        this.filter = filter;
        this.setScale(scale);
    }

    public double getScale() {
        Double scale = (Double) this.getQueryParameter(SCALE_PARAM_NAME);
        if (scale == null) {
            return 0;
        }
        return scale.doubleValue();
    }

    public void setScale(double scale) {
        this.setQueryParameter(SCALE_PARAM_NAME, new Double(scale));
    }

    public Object getQueryParameter(String name) {
        return queryParameters.get(name);
    }

    public void setQueryParameter(String name, Object value) {
        queryParameters.put(name, value);
    }

    public void setFeatureType(FeatureType featureType) {
        this.featureTypeId = featureType.getId();
    }

    public String[] getAttributeNames() {
        return (String[])attributeNames.toArray(new String[attributeNames.size()]);
    }

    public void setAttributeNames(String[] attributeNames) {
        this.attributeNames.clear();
        if (attributeNames != null){
            for (int i=0 ; i<attributeNames.length ; i++){
                this.attributeNames.add(attributeNames[i]);
            }
        }
    }

    public void addAttributeName(String attributeName){
        //If the attribute exists finish the method
        for (int i=0 ; i<attributeNames.size() ; i++){
            if (attributeNames.get(i).equals(attributeName)){
                return;
            }
        }
        this.attributeNames.add(attributeName);
    }

    public boolean hasAttributeNames() {
        return !this.attributeNames.isEmpty();
    }

    public void clearAttributeNames() {
        this.attributeNames = new ArrayList();
    }

    public Evaluator getFilter() {
        return filter;
    }

    @Override
    public void setFilter(String filter) {
        try {
            this.setFilter(DALLocator.getDataManager().createExpresion(filter));
        } catch (Exception ex) {
            throw new RuntimeException("Can't create filter from '"+filter+"'",ex);
        }
    }

    public void setFilter(Evaluator filter) {
        this.filter = filter;
        addFilterAttributes(filter);
//        isAddFilter = false;
    }

    @Override
    public void addFilter(String filter) {
        try {
            this.addFilter(DALLocator.getDataManager().createExpresion(filter));
        } catch (Exception ex) {
            throw new RuntimeException("Can't create filter from '"+filter+"'",ex);
        }
    }

    @Override
    public void addFilter(Evaluator evaluator) {
        if( evaluator == null ) {
            return;
        }
//        if (isAddFilter){
            if (this.filter == null){
                this.filter = evaluator;
            }else{
                if (evaluator != null){
                    if (this.filter instanceof AndEvaluator){
                        ((AndEvaluator)this.filter).addEvaluator(evaluator);
                    }else{
                        this.filter = new AndEvaluator(this.filter);
                        ((AndEvaluator)this.filter).addEvaluator(evaluator);
                    }
                }
            }
//        }else{
//            this.filter = evaluator;
//        }
        addFilterAttributes(evaluator);
//        isAddFilter = true;
    }

    public void clearFilter() {
      this.filter = null;
    }

    private void addFilterAttributes(Evaluator evaluator){
        if (evaluator != null){
            EvaluatorFieldsInfo fieldsInfo = evaluator.getFieldsInfo();
            if (fieldsInfo == null){
                // FieldsInfo is not available in this evaluator
                return;
            }
            String[] fieldNames = fieldsInfo.getFieldNames();
            if (fieldNames== null){
                // fieldNames is not available in this evaluator
                return;
            }

            for (int i=0 ; i<fieldNames.length ; i++){
                addAttributeName(fieldNames[i]);
            }
        }
    }

    public FeatureQueryOrder getOrder() {
        return order;
    }

    public void setOrder(FeatureQueryOrder order) {
        this.order = order;
    }

    public boolean hasFilter() {
        return this.filter != null;
    }

    public boolean hasOrder() {
        return this.order != null && this.order.size() > 0;
    }

    public Object clone() throws CloneNotSupportedException {
        DefaultFeatureQuery clone = (DefaultFeatureQuery) super.clone();

        // Clone attribute names array
        if (attributeNames != null) {
            clone.attributeNames = new ArrayList();
            for (int i=0 ; i<attributeNames.size() ; i++){
                clone.attributeNames.add(attributeNames.get(i));
            }
        }

        // Clone order
        if (order != null) {
            clone.order = (FeatureQueryOrder) order.clone();
        }

        return clone;
    }

    public FeatureQuery getCopy() {
        try {
            return (FeatureQuery) clone();
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        // DefaultFeatureQuery aCopy = new DefaultFeatureQuery();
        //
        // aCopy.featureTypeId = this.featureTypeId;
        //
        // if (this.attributeNames != null) {
        // aCopy.attributeNames = (String[]) Arrays
        // .asList(this.attributeNames).toArray(new String[0]);
        // }
        //
        // aCopy.filter = this.filter;
        //
        // if (this.order != null) {
        // aCopy.order = this.order.getCopy();
        // }
        //
        // return aCopy;
    }

    public String getFeatureTypeId() {
        return featureTypeId;
    }

    public void setFeatureTypeId(String featureTypeId) {
        this.featureTypeId = featureTypeId;
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        // FIXME: falta por terminar de implementar
        state.set("queryParameters", this.queryParameters);
        state.set("featureTypeId", this.featureTypeId);
        state.set("attributeNames", this.attributeNames);
        // state.set("filter", this.filter);
        state.set("limit", this.limit);
        state.set("pageSize", this.pageSize);

    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        // FIXME: falta por terminar de implementar
        this.queryParameters = (Map) state.get("queryParameters");
        this.featureTypeId = state.getString("featureTypeId");
        this.attributeNames = state.getList("attributeNames");
        this.filter = (Evaluator) state.get("filter");
        this.limit = state.getLong("limit");
        this.pageSize = state.getLong("pageSize");

    }

    /**
     * Register the class on PersistenceManager
     *
     */
    public static void registerPersistent() {
        DynStruct definition =
            ToolsLocator.getPersistenceManager()
            .addDefinition(DefaultFeatureQuery.class,
                "DefaultFeatureQuery",
                "DefaultFeatureQuery Persistent definition",
                null,
                null);

        definition.addDynFieldMap("queryParameters")
        .setClassOfItems(Object.class)
        .setMandatory(true);

        definition.addDynFieldString("featureTypeId").setMandatory(false);

        definition.addDynFieldList("attributeNames")
        .setClassOfItems(String.class)
        .setMandatory(false);

        definition.addDynFieldObject("filter")
        .setClassOfValue(Evaluator.class)
        .setMandatory(false);

        definition.addDynFieldObject("order")
        .setClassOfValue(FeatureQueryOrder.class)
        .setMandatory(false);

        definition.addDynFieldLong("limit").setMandatory(false);

        definition.addDynFieldLong("pageSize").setMandatory(false);

    }

    public long getLimit() {
        return limit;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public String[] getConstantsAttributeNames() {
        return (String[])constantsAttributeNames.toArray(new String[constantsAttributeNames.size()]);
    }

    public void setConstantsAttributeNames(String[] constantsAttributeNames) {
        this.constantsAttributeNames.clear();
        if (constantsAttributeNames != null){
            for (int i=0 ; i<constantsAttributeNames.length ; i++){
                this.constantsAttributeNames.add(constantsAttributeNames[i]);
            }
        }
    }

    public void addConstantAttributeName(String attributeName) {
        //If the attribute exists finish the method
        for (int i=0 ; i<constantsAttributeNames.size() ; i++){
            if (constantsAttributeNames.get(i).equals(attributeName)){
                return;
            }
        }
        this.constantsAttributeNames.add(attributeName);
    }

    public boolean hasConstantsAttributeNames() {
        return !this.constantsAttributeNames.isEmpty();
    }

    public void clearConstantsAttributeNames() {
        this.constantsAttributeNames = new ArrayList();
    }

}
