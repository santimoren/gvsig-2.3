/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.AbstractFeatureStoreTransform;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureQuery;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReference;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReferenceSelection;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureSelection;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStoreTransforms;
import org.gvsig.fmap.dal.feature.impl.JSIRSpatialIndexProvider;
import org.gvsig.fmap.dal.feature.impl.MemorySpatialIndexProvider;
import org.gvsig.fmap.dal.feature.impl.attributegetter.DayToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.DayToRelativeInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.HourToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.HourToRelativeInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.MilliToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.MilliToRelativeInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.MinuteToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.MinuteToRelativeInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.MonthToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.SecondToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.SecondToRelativeInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.attributegetter.YearToAbsoluteInstantFeatureAttributeGetter;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.ComputeAvailableValuesFromTable;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.DumbFeatureTypeDefinitionsManager;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.relations.DALLink_getRelatedFeature;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.relations.DALLink_getRelatedFeatures;
import org.gvsig.fmap.dal.feature.spi.memory.MemoryResource;
import org.gvsig.fmap.dal.feature.spi.memory.MemoryResourceParameters;
import org.gvsig.fmap.dal.resource.impl.DefaultResourceManager;
import org.gvsig.fmap.dal.resource.spi.MultiResource;
import org.gvsig.fmap.dal.resource.spi.MultiResourceParameters;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.i18n.Messages;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.identitymanagement.SimpleIdentityManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;
import org.xmlpull.v1.XmlPullParserException;

/**
 * Initialize the implementation of DAL.
 * 
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class DALDefaultImplLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsImplementationOf(DALLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        DALLocator.registerDefaultDataManager(DefaultDataManager.class);
        DALLocator.registerResourceManager(DefaultResourceManager.class);
        DALLocator.registerDefaultFeatureTypeDefinitionsManager(DumbFeatureTypeDefinitionsManager.class);
    }

    protected void doPostInitialize() throws LibraryException {
        List exs = new ArrayList();

        Messages.addResourceFamily("org.gvsig.fmap.dal.i18n.text",
            DALDefaultImplLibrary.class.getClassLoader(),
            DALDefaultImplLibrary.class.getClass().getName());

        try {
            registerBaseNewProvidersParametersDefinition();
        } catch (XmlPullParserException e) {
            exs.add(e);
        } catch (IOException e) {
            exs.add(e);
        }

        SimpleIdentityManager identityManager = ToolsLocator.getIdentityManager();
        identityManager.registerAction(DataManager.CREATE_STORE_AUTHORIZATION);
        identityManager.registerAction(DataManager.READ_STORE_AUTHORIZATION);
        identityManager.registerAction(DataManager.WRITE_STORE_AUTHORIZATION);
        
        
        DefaultFeatureQuery.registerPersistent();
        DefaultFeatureReference.registerPersistent();
        DefaultFeatureStoreTransforms.registerPersistent();
        AbstractFeatureStoreTransform.registerPersistent();
        DefaultFeatureReferenceSelection.registerPersistent();
        DefaultFeatureSelection.registerPersistent();
        // DefaultCoverageStore.registerPersistent();

        DefaultFeatureStore.registerPersistenceDefinition();
        DefaultDataServerExplorerPool.registerPersistenceDefinition();
        DefaultDataServerExplorerPoolEntry.registerPersistenceDefinition();
        try {
            DefaultFeatureStore.registerMetadataDefinition();
        } catch (MetadataException e) {
            exs.add(e);
        }

        // DefaultFeatureAttributeDescriptor.registerPersistent();

        //Register the FeatureAttributeTransform
        DataManager dataManager = DALLocator.getDataManager();
        if (dataManager == null) {
            throw new ReferenceNotRegisteredException(
                    DALLocator.DATA_MANAGER_NAME, DALLocator.getInstance());
        }
        dataManager.registerFeatureAttributeGetter("milliToRelativeInstant", MilliToRelativeInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("milliToAbsoluteInstant", MilliToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("secondToRelativeInstant", SecondToRelativeInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("secondToAbsoluteInstant",SecondToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("minuteToRelativeInstant",MinuteToRelativeInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("minuteToAbsoluteInstant", MinuteToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("hourToRelativeInstant", HourToRelativeInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("hourToAbsoluteInstant", HourToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("dayToAbsoluteInstant", DayToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("dayToRelativeInstant", DayToRelativeInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("monthToAbsoluteInstant", MonthToAbsoluteInstantFeatureAttributeGetter.class);
        dataManager.registerFeatureAttributeGetter("yearToAbsoluteInstant", YearToAbsoluteInstantFeatureAttributeGetter.class);
        
        // Register a default spatial index based in the default memory spatial index of geom library
    	if (!dataManager.getFeatureIndexProviders().contains(MemorySpatialIndexProvider.NAME)) {
    		((DataManagerProviderServices)dataManager).registerFeatureIndexProvider(MemorySpatialIndexProvider.NAME, "Default Spatial index",
    				MemorySpatialIndexProvider.class, DataTypes.GEOMETRY);
    		dataManager.setDefaultFeatureIndexProviderName(DataTypes.GEOMETRY, MemorySpatialIndexProvider.NAME);
    	}
    	if (!dataManager.getFeatureIndexProviders().contains(JSIRSpatialIndexProvider.NAME)) {
    		((DataManagerProviderServices)dataManager).registerFeatureIndexProvider(JSIRSpatialIndexProvider.NAME, "JSIR Spatial index",
    				JSIRSpatialIndexProvider.class, DataTypes.GEOMETRY);
    	}
    	
        ResourceManagerProviderServices resourceManager =
                (ResourceManagerProviderServices) DALLocator.getResourceManager();
        if (resourceManager == null) {
            throw new ReferenceNotRegisteredException(
                DALLocator.RESOURCE_MANAGER_NAME, DALLocator.getInstance());
        }

        if (!resourceManager.getResourceProviders().contains(
            MultiResource.TYPE_NAME)) {
            resourceManager.register(MultiResource.TYPE_NAME,
                MultiResource.DESCRIPTION, MultiResource.class,
                MultiResourceParameters.class);
        }

        if (!resourceManager.getResourceProviders().contains(
            MemoryResource.NAME)) {
            resourceManager.register(MemoryResource.NAME,
                MemoryResource.DESCRIPTION, MemoryResource.class,
                MemoryResourceParameters.class);
        }

        DynObjectManager dynObjectmanager = ToolsLocator.getDynObjectManager();
        dynObjectmanager.registerDynMethod(new DALLink_getRelatedFeatures());
        dynObjectmanager.registerDynMethod(new DALLink_getRelatedFeature());
        dynObjectmanager.registerDynMethod(new ComputeAvailableValuesFromTable("getAvailableValuesFromTable", "Retrieve available values from the elements of a table."));
        
        if (exs.size() > 0) {
            throw new LibraryException(this.getClass(), exs);
        }
    }

    private void registerBaseNewProvidersParametersDefinition()
        throws XmlPullParserException, IOException {
        DynObjectManager manager = ToolsLocator.getDynObjectManager();
        if (manager
            .get(
                "DAL",
                DataStoreProviderServices.BASE_NEWPARAMETERS_PROVIDER_DEFINITION_NAME) == null) {
            Map definitions =
                manager
                    .importDynClassDefinitions(
                        DataStoreProviderServices.class
                            .getResourceAsStream("ProviderParametersDefinition.xml"),
                        DataStoreProviderServices.class.getClassLoader());
            manager
                .add((DynClass) definitions
                    .get(DataStoreProviderServices.BASE_NEWPARAMETERS_PROVIDER_DEFINITION_FULLNAME));
            manager
                .add((DynClass) definitions
                    .get(DataStoreProviderServices.BASE_PARAMETERS_PROVIDER_DEFINITION_FULLNAME));
        }
    }

}       
