/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.lang.ref.Reference;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureReferenceSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.impl.undo.FeatureCommandsStack;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.observer.impl.BaseWeakReferencingObservable;
import org.gvsig.tools.observer.impl.DelegateWeakReferencingObservable;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.visitor.Visitor;

/**
 * Default implementation of a FeatureReferenceSelection, based on the usage of
 * a java.util.Set to store individual selected or not selected
 * FeatureReferences, depending on the usage of the {@link #reverse()} method.
 *
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class DefaultFeatureReferenceSelection extends AbstractDisposable
		implements FeatureReferenceSelection {

	public static final String DYNCLASS_PERSISTENT_NAME =
			"DefaultFeatureReferenceSelection";

    protected SelectionData selectionData = new SelectionData();

    private FeatureStore featureStore;

    private FeatureSelectionHelper helper;

	private DelegateWeakReferencingObservable delegateObservable =
			new DelegateWeakReferencingObservable(this);

	/**
	 * Creates a new Selection with the total size of Features from which the
	 * selection will be performed.
	 *
	 * @param featureStore
	 *            the FeatureStore of the selected FeatureReferences
	 * @throws DataException
	 *             if there is an error while getting the total number of
	 *             Features of the Store.
	 */
    public DefaultFeatureReferenceSelection(DefaultFeatureStore featureStore)
            throws DataException {
        super();
        this.featureStore = featureStore;
        this.helper = new DefaultFeatureSelectionHelper(featureStore);
        selectionData.setTotalSize(featureStore.getFeatureCount());
    }

    /**
     * Creates a new Selection with the total size of Features from which the
     * selection will be performed.
     *
     * @param featureStore
     *            the FeatureStore of the selected FeatureReferences
     * @param helper
     *            to get some information of the Store
     * @throws DataException
     *             if there is an error while getting the total number of
     *             Features of the Store.
     */
    public DefaultFeatureReferenceSelection(FeatureStore featureStore,
            FeatureSelectionHelper helper)
            throws DataException {
        super();
        this.featureStore = featureStore;
        this.helper = helper;
        selectionData.setTotalSize(featureStore.getFeatureCount());
    }

	/**
	 * Constructor used by the persistence manager. Don't use directly. After to
	 * invoke this method, the persistence manager calls the the method
	 * {@link #loadFromState(PersistentState)} to set the values of the internal
	 * attributes that this class needs to work.
	 */
	public DefaultFeatureReferenceSelection() {
		super();
	}

    public boolean select(FeatureReference reference) {
        return select(reference, true);
    }

    /**
     * @see #select(FeatureReference)
     * @param undoable
     *            if the action must be undoable
     */
    public boolean select(FeatureReference reference, boolean undoable) {
        
        if (reference == null) {
            throw new IllegalArgumentException("reference");
        }
        
        if (isSelected(reference)) {
            return false;
        }

        if (undoable && getFeatureStore().isEditing()) {
            getCommands().select(this, reference);
        }
        boolean change = false;
        if (selectionData.isReversed()) {
            change = selectionData.remove(reference);
        } else {
            change = selectionData.add(reference);
        }

        if (change) {
            notifyObservers(DataStoreNotification.SELECTION_CHANGE);
        }

        return change;
    }

    public boolean deselect(FeatureReference reference) {
        return deselect(reference, true);
    }

    /**
     * @see #deselect(FeatureReference)
     * @param undoable
     *            if the action must be undoable
     */
    public boolean deselect(FeatureReference reference, boolean undoable) {
        if (!isSelected(reference)) {
            return false;
        }

        if (undoable && getFeatureStore().isEditing()) {
            getCommands().deselect(this, reference);
        }
        boolean change = false;
        if (selectionData.isReversed()) {
            change = selectionData.add(reference);
        } else {
            change = selectionData.remove(reference);
        }

        if (change) {
            notifyObservers(DataStoreNotification.SELECTION_CHANGE);
        }

        return change;
    }

    public void selectAll() throws DataException {
        selectAll(true);
    }

    /**
     * @see #selectAll()
     * @param undoable
     *            if the action must be undoable
     */
    public void selectAll(boolean undoable) throws DataException {
        if (undoable && getFeatureStore().isEditing()) {
            getCommands().startComplex("_selectionSelectAll");
            getCommands().selectAll(this);
        }
        if (!selectionData.isReversed()) {
            selectionData.setReversed(true);
        }
        clearFeatureReferences();
        if (undoable && getFeatureStore().isEditing()) {
            getCommands().endComplex();
        }
        notifyObservers(DataStoreNotification.SELECTION_CHANGE);
    }

    public void deselectAll() throws DataException {
        deselectAll(false);
    }

    /**
     * @see #deselectAll()
     * @param undoable
     *            if the action must be undoable
     */
    public void deselectAll(boolean undoable) throws DataException {
        if (undoable && getFeatureStore().isEditing()) {
            getCommands().startComplex("_selectionDeselectAll");
            getCommands().deselectAll(this);
        }
        if (selectionData.isReversed()) {
            selectionData.setReversed(false);
        }
        clearFeatureReferences();
        if (undoable && getFeatureStore().isEditing()) {
            getCommands().endComplex();
        }

        notifyObservers(DataStoreNotification.SELECTION_CHANGE);
    }

    public boolean isSelected(FeatureReference reference) {
        if (selectionData.isReversed()) {
            return !selectionData.contains(reference);
        } else {
            return selectionData.contains(reference);
        }
    }

    public void reverse() {
        reverse(true);
    }

    /**
     * @see #reverse()
     * @param undoable
     *            if the action must be undoable
     */
    public void reverse(boolean undoable) {
        if (undoable && getFeatureStore().isEditing()) {
            getCommands().selectionReverse(this);
        }
        selectionData.setReversed(!selectionData.isReversed());
        notifyObservers(DataStoreNotification.SELECTION_CHANGE);
    }

    public long getSelectedCount() {
        if (selectionData.isReversed()) {
                return selectionData.getTotalSize() - selectionData.getSize()
                        + helper.getFeatureStoreDeltaSize();
        } else {
            return selectionData.getSize();
        }
    }

    public Iterator referenceIterator() {
        return Collections.unmodifiableSet(selectionData.getSelected())
                .iterator();
    }

	protected void doDispose() throws BaseException {
		delegateObservable.deleteObservers();
		deselectAll(false);
    }

    public boolean isFromStore(DataStore store) {
        return featureStore.equals(store);
    }

    public void accept(Visitor visitor) throws BaseException {
        for (Iterator iter = selectionData.getSelected().iterator(); iter
                .hasNext();) {
            visitor.visit(iter.next());
        }
    }

    public void update(Observable observable,
			Object notification) {
        // If a Feature is deleted, remove it from the selection Set.
        if (notification instanceof FeatureStoreNotification) {
            FeatureStoreNotification storeNotif = (FeatureStoreNotification) notification;
            if (FeatureStoreNotification.AFTER_DELETE
                    .equalsIgnoreCase(storeNotif.getType())) {
                selectionData.remove(storeNotif.getFeature().getReference());
            }
        }
    }

    public SelectionData getData() {
        return selectionData;
    }

    public void setData(SelectionData selectionData) {
        this.selectionData = selectionData;
        notifyObservers(DataStoreNotification.SELECTION_CHANGE);
    }

    public String toString() {
        return getClass().getName() + ": " + getSelectedCount()
                + " features selected, reversed = "
                + selectionData.isReversed() + ", featureIds contained: "
                + selectionData.getSelected();
    }

    protected boolean isReversed() {
        return selectionData.isReversed();
    }

    /**
     * Removes all the stored FeatureRefence objects.
     */
    protected void clearFeatureReferences() {
        selectionData.clear();
    }

	/**
	 * Returns the FeatureStore of the selected FeatureReferences.
	 *
	 * @return the featureStore
	 */
    protected FeatureStore getFeatureStore() {
        return featureStore;
    }

	/**
	 * Returns the reference to the commands record.
	 *
	 * @return the reference to the commands record
	 */
    protected FeatureCommandsStack getCommands() {
        return helper.getFeatureStoreCommandsStack();
    }

	public static class SelectionData implements Cloneable {
        private Set selected = new HashSet();

        /**
         * Sets how the Set of selected values has to be dealt.
         * <p>
         * If selected is FALSE, then values into the Set are the selected ones,
         * anything else is not selected.
         * </p>
         * <p>
         * If selected is TRUE, then values into the Set are values not
         * selected, anything else is selected.
         * </p>
         */
        private boolean reversed = false;

        private long totalSize;

        /**
         * @return the selected
         */
        public Set getSelected() {
            return selected;
        }

        /**
         * @param selected
         *            the selected to set
         */
        public void setSelected(Set selected) {
            this.selected = selected;
        }

        /**
         * @return the reversed
         */
        public boolean isReversed() {
            return reversed;
        }

        /**
         * @param reversed
         *            the reversed to set
         */
        public void setReversed(boolean reversed) {
            this.reversed = reversed;
        }

        /**
         * @return the totalSize
         */
        public long getTotalSize() {
            return totalSize;
        }

        /**
         * @param totalSize
         *            the totalSize to set
         */
        public void setTotalSize(long totalSize) {
            this.totalSize = totalSize;
        }

        public boolean add(FeatureReference reference) {
            return selected.add(reference);
        }

        public boolean remove(FeatureReference reference) {
            return selected.remove(reference);
        }

        public void clear() {
            selected.clear();
        }

        public boolean contains(FeatureReference reference) {
            return selected.contains(reference);
        }

        public int getSize() {
            return selected.size();
        }

        public Object clone() throws CloneNotSupportedException {
			SelectionData clone = (SelectionData) super.clone();
			// reversed and totalSize already cloned by parent.
			// clone the selected Set
			clone.selected = new HashSet(selected);
            return clone;
        }
    }

    // *** Persistence ***

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("store", featureStore);
		state.set("reversed", selectionData.isReversed());
		state.set("totalSize", selectionData.getTotalSize());
		state.set("selected", selectionData.getSelected().iterator());
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		featureStore = (FeatureStore)state.get("store");
		helper = new DefaultFeatureSelectionHelper((DefaultFeatureStore)featureStore);
		selectionData.setReversed(state.getBoolean("reversed"));
		selectionData.setTotalSize(state.getLong("totalSize"));
		Iterator it = state.getIterator("selected");
		while (it.hasNext()) {
			DefaultFeatureReference ref = (DefaultFeatureReference) it.next();
			selectionData.add(ref);
		}
		
		/*
		 * If we do not do this, feature store will not listen
		 * to changes in selection after instantiating a
		 * persisted selection. For non-persisted instances,
		 * this line corresponds to the line found in method:
		 * getFeatureSelection() in DefaultFeatureStore.
		 * This is not dangerous because "addObserver" only adds
		 * if they were not already added, so future invocations
		 * with same instances will have no effect.
		 */
		this.addObserver((DefaultFeatureStore)featureStore);
	}

	public static void registerPersistent() {
		DynStruct definition = ToolsLocator.getPersistenceManager().addDefinition(
				DefaultFeatureReferenceSelection.class, 
				DYNCLASS_PERSISTENT_NAME, 
				"DefaultFeatureReferenceSelection Persistent definition",
				null, 
				null
			);

		definition.addDynFieldObject("store").setClassOfValue(FeatureStore.class).setMandatory(true);
		definition.addDynFieldBoolean("reversed").setMandatory(true);
		definition.addDynFieldLong("totalSize").setMandatory(true);
		definition.addDynFieldList("selected").setClassOfItems(DefaultFeatureReference.class).setMandatory(true);

	}

	public void addObserver(Observer observer) {
		delegateObservable.addObserver(observer);
	}

	public void addObserver(Reference ref) {
		delegateObservable.addObserver(ref);
	}

	public void addObservers(BaseWeakReferencingObservable observable) {
		delegateObservable.addObservers(observable);
	}

	public void beginComplexNotification() {
		delegateObservable.beginComplexNotification();
	}

	public int countObservers() {
		return delegateObservable.countObservers();
	}

	public void deleteObserver(Observer observer) {
		delegateObservable.deleteObserver(observer);
	}

	public void deleteObserver(Reference ref) {
		delegateObservable.deleteObserver(ref);
	}

	public void deleteObservers() {
		delegateObservable.deleteObservers();
	}

	public void disableNotifications() {
		delegateObservable.disableNotifications();
	}

	public void enableNotifications() {
		delegateObservable.enableNotifications();
	}

	public void endComplexNotification() {
		// We don't want to notify many times in a complex notification
		// scenario, so ignore notifications if in complex.
		// Only one notification will be sent when the complex notification
		// ends.
		delegateObservable
				.notifyObservers(DataStoreNotification.SELECTION_CHANGE);
		delegateObservable.endComplexNotification();
	}

	public boolean inComplex() {
		return delegateObservable.inComplex();
	}

	public boolean isEnabledNotifications() {
		return delegateObservable.isEnabledNotifications();
	}

	public void notifyObservers() {
		// We don't want to notify many times in a complex notification
		// scenario, so ignore notifications if in complex.
		// Only one notification will be sent when the complex notification
		// ends.
		if (!delegateObservable.inComplex()) {
			delegateObservable.notifyObservers();
		}
	}

	public void notifyObservers(Object arg) {
		if (!delegateObservable.inComplex()) {
			delegateObservable.notifyObservers(arg);
		}
	}

	public Object clone() throws CloneNotSupportedException {
		DefaultFeatureReferenceSelection clone = (DefaultFeatureReferenceSelection) super
				.clone();
		// Original observers aren't cloned
		clone.delegateObservable = new DelegateWeakReferencingObservable(clone);
		// Clone internal data
		clone.selectionData = (SelectionData) selectionData.clone();
		// featureStore and helper are already swallow cloned by our parent
		return clone;
	}
}
