package org.gvsig.fmap.dal.feature.impl.dynobjectutils.relations;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FacadeOfAFeature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.DisposeUtils;
import org.gvsig.tools.dynobject.AbstractDynMethod;
import org.gvsig.tools.dynobject.DynField_v2;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynMethodException;

public class DALLink_getRelatedFeature extends AbstractDynMethod {
    private static final String DAL_code = "DAL.code";
    private static final String DAL_foreingTable = "DAL.foreingTable";
    private static final String DAL_foreingCode = "DAL.foreingCode";
 

    public DALLink_getRelatedFeature() {
        super("DAL.getRelatedFeature","Retrieve the element of the other side of the relation.");
    }

    public Object invoke(DynObject self, Object[] args) throws DynMethodException {
        if (!(self instanceof FacadeOfAFeature)) {
            return null;
        }

        DynField_v2 field = (DynField_v2) args[0];
        String codeName = (String) field.getTags().get(DAL_code);
        String foreignTableName = (String) field.getTags().get(DAL_foreingTable);
        String foreignCodeName = (String) field.getTags().get(DAL_foreingCode);

        Feature feature = ((FacadeOfAFeature) self).getFeature();
        return getRelatedFeature(feature, codeName, foreignTableName, foreignCodeName);
    }

    private DynObject getRelatedFeature(Feature feature, String attrName, String otherStoreName, String otherAttrName) throws ComputeRelatedFeatureException {
        Object fkValue = "<unknow>";
        FeatureSet set = null;
        DisposableIterator it = null;
        try {
            FeatureStore store = feature.getStore();

            DataManager manager = DALLocator.getDataManager();
            FeatureAttributeDescriptor attrdesc = feature.getType().getAttributeDescriptor(attrName);

            DataStoreParameters foreignStoreParmeters = store.getExplorer().get(otherStoreName);
            FeatureStore foreignStore = (FeatureStore) manager.openStore(store.getProviderName(), foreignStoreParmeters);
            FeatureQuery query = foreignStore.createFeatureQuery();
            fkValue = feature.get(attrName);
            if (attrdesc.getDataType().isNumeric()) {
                fkValue = fkValue.toString();
            } else {
                fkValue = "'" + fkValue.toString().replace("'", "''") + "'";
            }
            String where = otherAttrName + " = " + fkValue;
            query.addFilter(manager.createExpresion(where));
            set = foreignStore.getFeatureSet(query);
            if( set.getSize()!= 1 ) {
                throw new ComputeRelatedFeatureException(otherStoreName, otherAttrName, fkValue.toString());
            }
            it = set.fastIterator();
            Feature otherFeature = (Feature) it.next();
            return otherFeature.getAsDynObject();
        } catch (Exception ex) {
            throw new ComputeRelatedFeatureException(otherStoreName, otherAttrName, fkValue.toString(), ex);
        } finally {
            DisposeUtils.disposeQuietly(it);
            DisposeUtils.disposeQuietly(set);
        }
    }

    private static class ComputeRelatedFeatureException extends DynMethodException {

        private final static String MESSAGE_FORMAT = "Can't retrieve relation items from %(storename)s for attribute %(attrname)s with value %(attrvalue)s.";
        private final static String MESSAGE_KEY = "_OneToManyMethodException";
        private static final long serialVersionUID = -3248317756866564508L;

        public ComputeRelatedFeatureException(String storename, String attrname, String attrvalue) {
            super(MESSAGE_FORMAT, MESSAGE_KEY, serialVersionUID);
            setValue("storename", storename);
            setValue("attrname", attrname);
            setValue("attrvalue", attrvalue);
        }

        public ComputeRelatedFeatureException(String storename, String attrname, String attrvalue, Throwable cause) {
            this(storename,attrname, attrvalue);
            this.initCause(cause);
        }
    }

}
