package org.gvsig.fmap.dal.feature.impl.dynobjectutils;

import java.io.File;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.FeatureTypeDefinitionsManager;
import org.gvsig.tools.dynobject.DynClass;


public class DumbFeatureTypeDefinitionsManager implements FeatureTypeDefinitionsManager {

    @Override
    public DynClass get(FeatureStore store, FeatureType featureType) {
        return featureType;
    }

    @Override
    public boolean contains(FeatureStore store, FeatureType featureType) {
        return false;
    }

    @Override
    public void add(FeatureStore store, FeatureType featureType, DynClass dynClass) {
        // Do nothing
    }

    @Override
    public void remove(FeatureStore store, FeatureType featureType) {
        // Do nothing
    }

    @Override
    public void addModel(File model) {
    }
    
}
