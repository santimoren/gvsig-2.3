/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class DefaultFeatureReference implements
		FeatureReferenceProviderServices, Persistent {

	private Object oid;
	private Integer myHashCode = null;
	private Object[] pk;
	private String[] pkNames;
	private WeakReference storeRef;
	private boolean isNewFeature;
	private String featureTypeId;
	 
	/**
     * Constructor used by the persistence manager. Don't use directly.
     * After to invoke this method, the persistence manager calls 
     * the the method {@link #loadFromState(PersistentState)} to set 
     * the values of the internal attributes that this class needs to work.
     */
	public DefaultFeatureReference() {
		super();		
	}
	
	public DefaultFeatureReference(DefaultFeature feature) {
		this(feature.getStore(), feature.getData());
	}

	public DefaultFeatureReference(FeatureStore store,
			FeatureProvider fdata) {
		this.isNewFeature = fdata.isNew();
		this.oid = null;
		this.pk = null;
		this.storeRef = new WeakReference(store);
		this.featureTypeId = fdata.getType().getId();

		if (fdata.getType().hasOID() || isNewFeature) {
			this.oid = fdata.getOID();
			if (this.oid == null) {
				// FIXME Exception
				throw new RuntimeException("Missing OID");
			}
		} else {
			this.calculatePK(fdata);
			if (this.pk == null) {
				// FIXME Exception
				throw new RuntimeException("Missing pk attributes");
			}
		}

	}

	/*
	 * Use only for Persistent.setState
	 */
	public DefaultFeatureReference(FeatureStore store) {
		this.isNewFeature = false;
		this.oid = null;
		this.pk = null;
		this.storeRef = new WeakReference(store);
	}

	public DefaultFeatureReference(FeatureStore store, Object oid) {
		// FIXME featureTypeId is needed !!!
		this.isNewFeature = false;
		this.oid = oid;
		this.pk = null;
		this.storeRef = new WeakReference(store);
	}

	private DefaultFeatureStore getStore() {
		return (DefaultFeatureStore) this.storeRef.get();
	}

	private void calculatePK(FeatureProvider fdata) {
		ArrayList keys = new ArrayList();
		ArrayList keyNames = new ArrayList();
		FeatureType type = fdata.getType();
		Iterator it = type.iterator();
		while (it.hasNext()) {
			FeatureAttributeDescriptor attr = (FeatureAttributeDescriptor) it
					.next();
			if (attr.isPrimaryKey()) {
				keys.add(fdata.get(attr.getIndex()));
				keyNames.add(attr.getName());
			}
		}
		if (keys.size() < 1) {
			pk = null;
			pkNames = null;
		} else {
			pk = keys.toArray();
            pkNames = (String[]) keyNames.toArray(new String[keyNames.size()]);
		}
	}

	public Feature getFeature() throws DataException {
		return this.getStore().getFeatureByReference(this);
	}

	public Feature getFeature(FeatureType featureType) throws DataException {
		return this.getStore().getFeatureByReference(this, featureType);
	}

	public Object getOID() {
		return this.oid;
	}

	public boolean isNewFeature() {
		return this.isNewFeature;
	}


	public boolean equals(Object obj) {
		if (!(obj instanceof DefaultFeatureReference)) {
			return false;
		}
		DefaultFeatureReference other = (DefaultFeatureReference) obj;

		FeatureStore otherStore = (FeatureStore) other.storeRef.get();
		FeatureStore myrStore = (FeatureStore) this.storeRef.get();
		if (otherStore == null || myrStore == null) {
			return false;
		}
		if (!myrStore.equals(otherStore)) {
			return false;
		}
		if (myHashCode != null && other.myHashCode != null) {
			return myHashCode.equals(other.myHashCode);
		}
		if (this.oid != null) {
			return this.oid.equals(other.oid);
		}
		if(pk != null) {
			if(other.pk == null) {
				return false;
			}
    		for (int i = 0; i < this.pk.length; i++) {
    			if (!this.pk[i].equals(other.pk[i])) {
    				return false;
    			}
    		}
		}
		return true;
	}

	public int hashCode() {
		if (this.oid != null) {
			return this.oid.hashCode();
		}
		if (myHashCode == null) {
			StringBuffer buff = new StringBuffer();

			for (int i = 0; i < this.pk.length; i++) {
				buff.append(this.pk[i].hashCode());
				buff.append("##");
			}
			myHashCode = new Integer(buff.toString().hashCode());
		}
		return myHashCode.intValue();
	}

	public String[] getKeyNames() {
		return pkNames;
	}

	public Object getKeyValue(String name) {
		for (int i = 0; i < pkNames.length; i++) {
			if (pkNames[i].equalsIgnoreCase(name)) {
				return pk[i];
			}
		}
		// FIXME exception????
		return null;
	}

	public String getFeatureTypeId() {
		return featureTypeId;
	}

	// *** Persistence ***

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.oid = state.get("oid");
		this.myHashCode = (Integer) state.get("myHashCode");
		this.storeRef = new WeakReference(state.get("store"));
		this.isNewFeature = state.getBoolean("isNewFeature");
		this.featureTypeId = state.getString("featureTypeId");
		List pkList = (List) state.get("pk");
		if (pkList != null) {
			List pkNamesList = (List) state.get("pkNames");
			if (pkNamesList == null || pkList.size() != pkNamesList.size()) {
				throw new PersistenceException("bad pkNames value");
			}
			this.pk = pkList.toArray();
            this.pkNames =
                (String[]) pkNamesList.toArray(new String[pkList.size()]);
		} else {
			this.pk = null;
			this.pkNames = null;
		}
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("oid", oid);
		state.set("myHashCode", myHashCode);
		state.set("isNewFeature", isNewFeature);
		state.set("store", (Persistent) storeRef.get());
		state.set("featureTypeId", featureTypeId);
		if (pk == null) {
			state.setNull("pk");
			state.setNull("pkNames");
		} else {
			state.set("pk", pk);
			state.set("pkNames", pkNames);
		}

	}

	public static void registerPersistent() {
		DynStruct definition = ToolsLocator.getPersistenceManager().addDefinition(
				DefaultFeatureReference.class, 
				"Reference", 
				"DefaultFeatureReference Persistent definition", 
				null, 
				null
			);

		definition.addDynFieldObject("oid")
			.setClassOfValue(Object.class)
			.setMandatory(false)
			.setPersistent(true);
			
		definition.addDynFieldBoolean("isNewFeature")
			.setMandatory(true)
			.setPersistent(true);
	
		definition.addDynFieldObject("store")
			.setClassOfValue(FeatureStore.class)
			.setMandatory(true)
			.setPersistent(true);

		definition.addDynFieldInt("myHashCode")
			.setMandatory(false)
			.setPersistent(true);
	
		definition.addDynFieldString("featureTypeId")
			.setMandatory(true)
			.setPersistent(true);

		definition.addDynFieldArray("pk")
			.setClassOfItems(Object.class)
			.setMandatory(false)
			.setPersistent(true);

        definition.addDynFieldArray("pkNames")
			.setClassOfItems(String.class)
			.setMandatory(false)
			.setPersistent(true);

	}

	public String toString() {
		return MessageFormat.format(
				"FeatureReference: oid = {0}, myHashCode = {1}, "
				+ "pks = {2}, pkNames = {3}, "
				+ "isNewFeature = {4}, featureTypeId = {5}", new Object[] {
				oid, myHashCode, (pk == null ? null : Arrays.asList(pk)),
				(pkNames == null ? null : Arrays.asList(pkNames)),
				new Boolean(isNewFeature),
				featureTypeId });
	}

}
