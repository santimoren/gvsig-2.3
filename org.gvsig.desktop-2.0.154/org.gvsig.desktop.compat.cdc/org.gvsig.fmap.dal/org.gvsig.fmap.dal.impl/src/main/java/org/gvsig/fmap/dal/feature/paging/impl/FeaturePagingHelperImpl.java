/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.paging.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.impl.dynobjectutils.DynObjectFeatureFacade;
import org.gvsig.fmap.dal.feature.paging.FacadeOfAFeaturePagingHelper;
import org.gvsig.fmap.dal.feature.paging.FeaturePagingHelper;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.dynobject.impl.DefaultDynObjectPagingHelper;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * Helper class to access the values of a FeatureCollection by position. Handles
 * pagination automatically to avoid filling the memory in case of big
 * collections.
 *
 * TODO: evaluate if its more convenient to read values in the background when
 * the returned value is near the end of the page, instead of loading a page on
 * demand.
 *
 * @author gvSIG Team
 */
public class FeaturePagingHelperImpl extends DefaultDynObjectPagingHelper
    implements FeaturePagingHelper {

    private static final Logger LOG = LoggerFactory
        .getLogger(FeaturePagingHelperImpl.class);

    private FeatureQuery query;

    private FeatureStore featureStore;

    /** If the selected Features must be returned as the first ones. **/
    private boolean selectionUp = false;

    private FeatureSet featSet = null;
    private FeatureSelection initialSelection = null;

    private Feature[] features = null;

    private boolean initialization_completed = false;

    private FeatureSelection selection = null;
    /**
     * Constructs a FeaturePagingHelperImpl from data of a FeatureStore.
     *
     * @param featureStore
     *            to extract data from
     * @throws DataException
     *             if there is an error initializing the helper
     */
    public FeaturePagingHelperImpl(FeatureStore featureStore)
        throws BaseException {
        this(featureStore, DEFAULT_PAGE_SIZE);
    }

    /**
     * Constructs a FeaturePagingHelperImpl from data of a FeatureStore.
     *
     * @param featureStore
     *            to extract data from
     * @param pageSize
     *            the number of elements per page data
     * @throws DataException
     *             if there is an error initializing the helper
     */
    public FeaturePagingHelperImpl(FeatureStore featureStore, int pageSize)
        throws BaseException {
        this(featureStore, null, pageSize);
    }

    /**
     * Constructs a FeaturePagingHelperImpl from data of a FeatureStore.
     *
     * @param featureStore
     *            to extract data from
     * @throws DataException
     *             if there is an error initializing the helper
     */
    public FeaturePagingHelperImpl(FeatureStore featureStore,
        FeatureQuery featureQuery) throws BaseException {
        this(featureStore, featureQuery, DEFAULT_PAGE_SIZE);
    }

    /**
     * Constructs a FeaturePagingHelperImpl from data of a FeatureStore.
     *
     * @param featureSet
     *            to extract data from
     * @param pageSize
     *            the number of elements per page data
     * @throws DataException
     *             if there is an error initializing the helper
     */
    public FeaturePagingHelperImpl(FeatureStore featureStore,
        FeatureQuery featureQuery, int pageSize) throws BaseException {
        super();
        FeatureQuery query = featureQuery;
        if (featureQuery == null) {
            query = featureStore.createFeatureQuery();
            query.setFeatureType(featureStore.getDefaultFeatureType());
        }

        this.featureStore = featureStore;
        this.query = query;
        this.query.setPageSize(pageSize);

        setDefaultCalculator(new Sizeable() {
            public long getSize() {
            	FeatureSet featureSet = getFeatureSet(false);
                try {
					return featureSet.getSize();
                } catch (BaseException e) {
                    LOG.error("Error getting the size of the FeatureSet: "
                        + featureSet, e);
                    return 0l;
                }
            }
        }, pageSize);


        if (LOG.isDebugEnabled()) {

            LOG.debug("FeaturePagingHelperImpl created with {} pages, "
                + "and a page size of {}", new Long(getCalculator()
                .getNumPages()), new Integer(pageSize));
        }
        this.initialization_completed = true;
    }

    /**
     * @return the selectionUp status
     */
    public boolean isSelectionUp() {
        return selectionUp;
    }
    
    public FeatureSelection getSelection() {
        if (selection == null) {
            try {
                return getFeatureStore().getFeatureSelection();
            } catch (Exception e) {
                LOG.warn("Error getting the selection", e);
            }
        }
        return selection;
    }
    
    public void setSelection(FeatureSelection selection) {
        this.selection = selection;
    }
    
    @Override
    public void setSelectionUp(boolean selectionUp) {
        this.selectionUp = selectionUp;
        try {
            FeatureSelection currentSelection = getSelection();
            if (selectionUp && !currentSelection.isEmpty()) {
                initialSelection =(FeatureSelection) currentSelection.clone();
                setCalculator(new OneSubsetOneSetPagingCalculator(
                    new FeatureSetSizeableDelegate(initialSelection),
                    new FeatureSetSizeableDelegate(getFeatureSet(false)),
                    getMaxPageSize()));
            } else {
                if (initialSelection != null) {
                    initialSelection.dispose();
                    initialSelection = null;
                }
                setDefaultCalculator(new FeatureSetSizeableDelegate(
                    getFeatureSet(false)), getMaxPageSize());
            }
        } catch (BaseException e) {
            LOG.error("Error setting the selection up setting to: "
                + selectionUp, e);
        } catch (CloneNotSupportedException e) {
            LOG.error("Error cloning the selection "
                + "while setting the selection up", e);
        }
    }

    public synchronized Feature getFeatureAt(long index) throws BaseException {
        // Check if we have currently loaded the viewed page data,
        // or we need to load a new one
    	int maxPageSize = getMaxPageSize();
    	long currentPage = getCurrentPage();
    	long currentPage2 = currentPage;
    	
    	
        long pageForIndex = (long) Math.floor(index / maxPageSize);

        if (pageForIndex != currentPage) {
            setCurrentPage(pageForIndex);
            currentPage2 = getCurrentPage();
        }

        long positionForIndex = index - (currentPage2 * maxPageSize);

        if (positionForIndex >= getCurrentPageFeatures().length) {
            throw new FeatureIndexException(
                new IndexOutOfBoundsException("positionForIndex too big: "
                    + positionForIndex));
        } else {
            Feature feature = getCurrentPageFeatures()[(int) positionForIndex];
            return feature;
        }

    }

    public Feature[] getCurrentPageFeatures() {
        if( this.features==null ) {
            try {
                this.loadCurrentPageData();
            } catch (BaseException ex) {
                // Do nothing
            }
            if( this.features == null ) {
                String msg = "Can't retrieve the features from current page.";
                LOG.warn(msg);
                throw new RuntimeException(msg);
            }
        }
        return features;
    }

    /**
     * Gets the feature set.
     * The boolean tells whether we must create the featureset
     * again (for example perhaps we need it after a feature
     * has been added/removed)
     */
    private FeatureSet getFeatureSet(boolean reset) {

        if (featSet == null || reset) {

            if (featSet != null) {
                try {
                    featSet.dispose();
                } catch (Exception ex) {
                    LOG.info("Error while disposing featset.", ex);
                }
            }

            try {
                FeatureStore featureStore = getFeatureStore();
                synchronized (featureStore) {
                    featSet = featureStore.getFeatureSet(getFeatureQuery());
                }
            } catch (DataException e) {
                throw new RuntimeException("Error getting a feature set with the query " + getFeatureQuery());
            }
        }
        return featSet;
    }

    public DynObjectSet getDynObjectSet() {
    	return getFeatureSet(false).getDynObjectSet();
    }

    public void reloadCurrentPage() throws BaseException {

        boolean sel_up = this.isSelectionUp();

        setSelectionUp(false);
        if (getCalculator().getCurrentPage() > -1) {
            loadCurrentPageData();
        }

        if (sel_up) {
            setSelectionUp(true);
        }
    }

    public void reload() throws BaseException {

        /*
         * Force re-creation of feature set
         */
        this.getFeatureSet(true);


        setDefaultCalculator(new Sizeable() {
            public long getSize() {
            	FeatureSet featureSet = getFeatureSet(false);
                try {
					return featureSet.getSize();
                } catch (BaseException e) {
                    LOG.error("Error getting the size of the FeatureSet: "
                        + featureSet, e);
                    return 0l;
                }
            }
        }, getCalculator().getMaxPageSize());
        reloadCurrentPage();
    }

    public FeatureStore getFeatureStore() {
        return featureStore;
    }

    public FeatureQuery getFeatureQuery() {
        return query;
    }

    /**
     * Loads all the Features of the current page.
     */
    protected synchronized void loadCurrentPageData() throws BaseException {
        if( !initialization_completed ) {
            return;
        }
        final int currentPageSize = getCalculator().getCurrentPageSize();
        final Feature[] values = new Feature[currentPageSize];

        long t1 = 0;
        if (LOG.isTraceEnabled()) {
            t1 = System.currentTimeMillis();
        }

        if (selectionUp) {
            loadCurrentPageDataWithSelectionUp(values);
        } else {
            loadCurrentPageDataNoSelection(values);
        }

        if (LOG.isTraceEnabled()) {
            long t2 = System.currentTimeMillis();
            LOG.trace("Time to load {} features: {} ms", new Integer(
                currentPageSize), new Long(t2 - t1));
        }

        this.features = values;
    }

    private void loadCurrentPageDataWithSelectionUp(final Feature[] values)
            throws BaseException {
        FeatureSelection selection = initialSelection;
        if (selection == null) {
            loadCurrentPageDataNoSelection(values);
        } else {
            FeatureSet set = getFeatureSet(false);
            try {
                OneSubsetOneSetPagingCalculator twoSetsCalculator = null;
                if (getCalculator() instanceof OneSubsetOneSetPagingCalculator) {
                    twoSetsCalculator
                            = (OneSubsetOneSetPagingCalculator) getCalculator();
                } else {
                    twoSetsCalculator
                            = new OneSubsetOneSetPagingCalculator(
                                    new FeatureSetSizeableDelegate(selection),
                                    new FeatureSetSizeableDelegate(set),
                                    getMaxPageSize(), getCalculator().getCurrentPage());
                    setCalculator(twoSetsCalculator);
                }

	        // First load values from the selection, if the current page has
                // elements from it
                if (twoSetsCalculator.hasCurrentPageAnyValuesInFirstSet()) {
                    loadDataFromFeatureSet(values, 0, selection,
                            twoSetsCalculator.getFirstSetInitialIndex(),
                            twoSetsCalculator.getFirstSetHowMany(), null);
                }
	        // Next, load values from the FeatureSet if the current page has values
                // from it
                if (twoSetsCalculator.hasCurrentPageAnyValuesInSecondSet()) {
                    loadDataFromFeatureSet(
                            values,
                            // The cast will work as that size will be <= maxpagesize,
                            // which is an int
                            (int) twoSetsCalculator.getFirstSetHowMany(), set,
                            twoSetsCalculator.getSecondSetInitialIndex(),
                            twoSetsCalculator.getSecondSetHowMany(), selection);
                }
            } finally {
                /*
                 * This is the feature set
                 * we dont want to lose it
                 */
                // set.dispose();
            }
        }
    }

    private void loadCurrentPageDataNoSelection(final Feature[] values)
        throws BaseException {

        long firstPosition = getCalculator().getInitialIndex();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Loading {} Features starting at position {}",
                new Integer(getCalculator().getCurrentPageSize()), new Long(
                    firstPosition));
        }

        FeatureSet featureSet = getFeatureSet(false);
        try {
        	loadDataFromFeatureSet(values, 0, featureSet, firstPosition,
        			getCalculator().getCurrentPageSize(), null);
        } catch(DataException ex) {
            throw ex;
            // } finally {
        	// featureSet.dispose();
        }

    }

    private void loadDataFromFeatureSet(final Feature[] values,
        final int valuesPosition, FeatureSet set, long initialIndex,
        final long howMany, final FeatureSelection selectedFeaturesToSkip)
        throws DataException {

        try {
            set.accept(new Visitor() {

                private int i = valuesPosition;

                public void visit(Object obj) throws VisitCanceledException,
                    BaseException {
                    if (i >= valuesPosition + howMany) {
                        throw new VisitCanceledException();
                    }
                    Feature current = (Feature) obj;
                    // Add the current Feature only if we don't skip selected
                    // features or the feature is not selected
                    if (selectedFeaturesToSkip == null
                        || !selectedFeaturesToSkip.isSelected(current)) {
                        try {
                            values[i] = current.getCopy();
                            i++;
                        } catch(Exception ex) {
                            // Aqui no deberia petar, pero...
                            // me he encontrado un caso que tenia una referencia a
                            // una feature seleccionada que ya no existia. No se como
                            // habia pasado, se habia quedado de antes guardada en el
                            // proyecto pero la feature ya no existia, y eso hacia que
                            // petase al intentar leer de disco la feature a partir
                            // de una referencia no valida.
                        }
                    }
                }
            }, initialIndex);
        } catch (BaseException e) {
            if (e instanceof DataException) {
                throw ((DataException) e);
            } else {
                LOG.error("Error loading the data starting at position {}",
                    new Long(initialIndex), e);
            }
        }
    }

    public void delete(Feature feature) throws BaseException {
        featureStore.delete(feature);
        /*
         * Force re-creation of feature set
         */
        this.getFeatureSet(true);

        reloadCurrentPage();
    }

    public void insert(EditableFeature feature) throws BaseException {
    	featureStore.insert(feature);
        /*
         * Force re-creation of feature set
         */
        this.getFeatureSet(true);

        reloadCurrentPage();
    }

    public void update(EditableFeature feature) throws BaseException {
    	featureStore.update(feature);
        /*
         * Force re-creation of feature set
         */
        this.getFeatureSet(true);

        reloadCurrentPage();
    }

    public FeatureType getFeatureType() {

        FeatureType ft = null;

        try {
            ft = featureStore.getDefaultFeatureType();
        } catch (DataException e) {
            LOG.error("Error while getting feature type: " +
                e.getMessage(), e);
        }
        return ft;

        /*
         *
        FeatureSet featureSet = getFeatureSet();
        try {
            return featureSet.getDefaultFeatureType();
        } finally {
            featureSet.dispose();
        }
        */


    }

    protected void doDispose() throws BaseException {
        initialSelection.dispose();
        if (featSet != null) {
            try {
                featSet.dispose();
            } catch (Exception ex) {
                LOG.info("Error while disposing featset.", ex);
            }
        }
    }

    public DynObject[] getCurrentPageDynObjects() {
        Feature[] features = getCurrentPageFeatures();
        DynObject[] dynobjects = new DynObject[features.length];
        for (int i = 0; i < dynobjects.length; i++) {
            dynobjects[i] = new DynObjectFeatureFacade(features[i]);
        }
        return dynobjects;
    }

    public DynObject getDynObjectAt(long index) throws BaseException {
        return new DynObjectFeatureFacade(getFeatureAt(index));
    }

    public List asList() {
        return new FeaturePagingHelperList();
    }

    public List asListOfDynObjects() {
        return new DynObjectPagingHelperList();
    }

    private class FeaturePagingHelperList extends PagingHelperList {
        public Object get(int i) {
            try {
                return getFeatureAt(i);
            } catch (BaseException ex) {
                throw  new RuntimeException(ex);
            }
        }
    }

    private class DynObjectPagingHelperList extends PagingHelperList {
        public Object get(int i) {
            try {
                return getDynObjectAt(i);
            } catch (BaseException ex) {
                throw  new RuntimeException(ex);
            }
        }

    }

    private abstract class PagingHelperList implements List,  FacadeOfAFeaturePagingHelper {

        @Override
        public FeaturePagingHelper getFeaturePagingHelper() {
            return FeaturePagingHelperImpl.this;
        }
        
        public int size() {
            try {
                return (int) getFeatureSet(false).getSize();
            } catch (DataException ex) {
                throw  new RuntimeException(ex);
            }
        }

        public boolean isEmpty() {
            try {
                return getFeatureSet(false).isEmpty();
            } catch (DataException ex) {
                throw  new RuntimeException(ex);
            } catch (ConcurrentDataModificationException ex) {
                LOG.warn(
                    "Error to asking about the emptiness of the store. Retrying reloading data.",
                    ex);
                try {
                    reload();
                } catch (BaseException e) {
                    LOG.warn("Error reloading data.", e);
                    throw new RuntimeException(e);
                }
                try {
                    return getFeatureSet(false).isEmpty();
                } catch (DataException e) {
                    LOG.warn(
                        "Error to asking about the emptiness of the store after reloading data.",
                        e);
                    throw new RuntimeException(e);
                }
            }
        }

        public Iterator iterator() {
            try {
                return getFeatureSet(false).fastIterator();
            } catch (DataException ex) {
                throw  new RuntimeException(ex);
            }
        }

        public boolean contains(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Object[] toArray() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Object[] toArray(Object[] ts) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean add(Object e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean containsAll(Collection clctn) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean addAll(Collection clctn) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean addAll(int i, Collection clctn) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean removeAll(Collection clctn) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean retainAll(Collection clctn) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public void clear() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Object set(int i, Object e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public void add(int i, Object e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public Object remove(int i) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public int indexOf(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public int lastIndexOf(Object o) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public ListIterator listIterator() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public ListIterator listIterator(int i) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public List subList(int i, int i1) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
}
