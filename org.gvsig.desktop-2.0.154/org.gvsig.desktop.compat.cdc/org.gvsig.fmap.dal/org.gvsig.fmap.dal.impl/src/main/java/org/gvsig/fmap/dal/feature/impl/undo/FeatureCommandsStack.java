/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.undo;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.*;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReferenceSelection;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.tools.undo.command.UndoRedoCommandStack;

public interface FeatureCommandsStack extends UndoRedoCommandStack {

    public void insert(Feature feature) throws DataException;

    public void update(Feature feature, Feature oldFeature) throws DataException;

    public void delete(Feature feature) throws DataException;

    public void update(FeatureType featureType, FeatureType oldFeatureType);

    public void select(DefaultFeatureReferenceSelection selection,
            FeatureReference reference);

    public void deselect(DefaultFeatureReferenceSelection selection,
            FeatureReference reference);

    public void selectAll(DefaultFeatureReferenceSelection selection)
            throws DataException;

    public void deselectAll(DefaultFeatureReferenceSelection selection)
            throws DataException;

    public void selectionReverse(DefaultFeatureReferenceSelection selection);

    public void selectionSet(DefaultFeatureStore store,
            FeatureSelection oldSelection, FeatureSelection newSelection);

}