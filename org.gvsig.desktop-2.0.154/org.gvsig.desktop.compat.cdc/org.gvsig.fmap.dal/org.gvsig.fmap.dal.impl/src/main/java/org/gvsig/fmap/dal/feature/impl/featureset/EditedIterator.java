/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.featureset;

import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.exception.ConcurrentDataModificationException;
import org.gvsig.fmap.dal.feature.impl.DefaultFeature;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureReference;
import org.gvsig.fmap.dal.feature.impl.DefaultFeatureStore;
import org.gvsig.fmap.dal.feature.impl.FeatureManager;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.exception.BaseException;

/**
 * Iterator for stores in edition mode.
 * 
 * @author gvSIG Team
 * @version $Id$
 * @deprecated use the {@link FastEditedIterator} instead
 */
public class EditedIterator extends FilteredIterator {

    private Iterator newsFeatures;
    private boolean featureIsNew;
    private DefaultFeatureStore store;
    protected FeatureManager featureManager;

    public EditedIterator(DefaultFeatureSet featureSet) {
        super(featureSet);
        this.store = featureSet.store;
        this.featureManager = this.store.getFeatureManager();
    }

    public EditedIterator(DefaultFeatureSet featureSet, long index)
        throws DataException {
        this(featureSet);
        this.newsFeatures = null;
        if (index > 0) {

            this.iterator = featureSet.provider.iterator();
            skypto(index);

            /*
             * This did not work because the provider
             * skips to the index without considering the
             * changes in editing mode. We need the
             * "skypto" method which knows about those
             * changes.
             * 
        	if (featureSet.provider.canIterateFromIndex()) {
                try {
                    this.iterator = featureSet.provider.iterator(index);
                } catch (IllegalArgumentException e) {
                    this.iterator = featureSet.provider.iterator();
                    skypto(index);
                } catch (UnsupportedOperationException e) {
                    this.iterator = featureSet.provider.iterator();
                    skypto(index);
                }
            } else {
                this.iterator = featureSet.provider.iterator();
                skypto(index);
            }
             */
            
        } else {
            this.iterator = featureSet.provider.iterator();
        }

    }
    
    public void remove() {
    	if(this.getIterator() == newsFeatures) {
    		newsFeatures.remove();
    	} else {
    		super.remove();
    	}
    }

    public boolean hasNext() {
        if (store.isEditing()) {
            return super.hasNext();
        } else {
            throw new ConcurrentDataModificationException(store.getFullName());
        }
    }

    public Object next() {
        if (store.isEditing()) {
            return super.next();
        } else {
            throw new ConcurrentDataModificationException(store.getFullName());
        }
    }

    protected void setNewsFeatures(Iterator newsFeatures) {
        this.newsFeatures = newsFeatures;
    }

    protected boolean isFeatureIsNew() {
        return featureIsNew;
    }

    protected Iterator getIterator() {
        if (this.featureIsNew) {
            return this.newsFeatures;
        } else
            if (this.iterator.hasNext()) {
                featureIsNew = false;
                return this.iterator;
            } else {
                featureIsNew = true;
                this.newsFeatures = this.featureManager.getInserted();
                return this.newsFeatures;
            }
    }

    protected DefaultFeature createFeature(FeatureProvider data) throws DataException {

        DefaultFeature f = null;
        data.setNew(featureIsNew);
        try {
            FeatureReference ref = new DefaultFeatureReference(store, data);
            f =
                (DefaultFeature) this.featureManager.get(ref, store,
                    data.getType());
        } catch (DataException e) {
            RuntimeException ex = new RuntimeException();
            e.initCause(e);
            throw ex;
        }
        if (f == null) {
            // La feature no se ha editado.
            f = new DefaultFeature(store, data);
        }
        if (this.fset.transform.isEmpty()) {
            return f;
        } else {
            return (DefaultFeature) this.fset.transform.applyTransform(
                    f, fset.getDefaultFeatureType());
        }     
    }

    protected boolean isDeletedOrHasToSkip(FeatureProvider data) {

        // XXX
        // si recorriendo los originales nos
        // encontramos uno nuevo, no lo devolvemos
        // porque se recorrera mas tarde.
        // Esto es una interaccion con los indices
        // ya que estos contienen todas las features ya
        // sea nuevas o no

        if (data.isNew() && !featureIsNew) {
            return true;
        }
        FeatureReference ref = new DefaultFeatureReference(store, data);
        return this.featureManager.isDeleted(ref);
    }

    protected void doDispose() throws BaseException {
        super.doDispose();
        if (newsFeatures instanceof DisposableIterator) {
            ((DisposableIterator) newsFeatures).dispose();
        }
        newsFeatures = null;
        this.featureManager = null;
        this.store = null;
    }

}
