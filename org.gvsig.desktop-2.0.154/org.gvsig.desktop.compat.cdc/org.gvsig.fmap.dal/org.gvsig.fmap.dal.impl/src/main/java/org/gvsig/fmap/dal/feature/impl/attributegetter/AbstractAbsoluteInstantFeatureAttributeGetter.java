/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl.attributegetter;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.timesupport.AbsoluteInstant;
import org.gvsig.timesupport.AbsoluteInstantTypeNotRegisteredException;
import org.gvsig.tools.dataTypes.DataType;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public abstract class AbstractAbsoluteInstantFeatureAttributeGetter extends AbstractObjectToTimefeatureAttributeGetter {
    protected int dateTimeFieldtype = -1;
    protected DataType dataType = null;
    
    public AbstractAbsoluteInstantFeatureAttributeGetter(
        int dateTimeFieldtype) {
        super();
        this.dateTimeFieldtype = dateTimeFieldtype;
        this.dataType = DATA_TYPES_MANAGER.get(DataTypes.INSTANT);
    }   

    public Object getter(Object sourceAttributeValue) {        
        try {
            if (sourceAttributeValue == null){
                return TIME_SUPPORT_MANAGER.createAbsoluteInstant(dateTimeFieldtype, 0);
            }
            return TIME_SUPPORT_MANAGER.createAbsoluteInstant(dateTimeFieldtype, ((Integer)sourceAttributeValue).intValue());
        } catch (AbsoluteInstantTypeNotRegisteredException e) {
           LOG.error("Impossible to apply the tranformation for this field", e);
        }
        return TIME_SUPPORT_MANAGER.createAbsoluteInstant();
    }

    public Object setter(Object targetAttributeValue) {
        return new Integer(((AbsoluteInstant)targetAttributeValue).getValue(0));        
    }
    
    public DataType getDataType() {        
        return dataType;
    }
 }
