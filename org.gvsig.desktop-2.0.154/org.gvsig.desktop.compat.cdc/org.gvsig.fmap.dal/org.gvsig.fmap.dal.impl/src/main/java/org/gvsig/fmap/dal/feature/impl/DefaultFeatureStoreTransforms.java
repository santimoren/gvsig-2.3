/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureStoreTransform;
import org.gvsig.fmap.dal.feature.FeatureStoreTransforms;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class DefaultFeatureStoreTransforms implements FeatureStoreTransforms,
		Persistent {

	private DefaultFeatureStore store;
	private List transforms;
	private Boolean isTramsformValues;
	private TransformTemporalList lastTransformStack;

	public DefaultFeatureStoreTransforms() {
		this.store = null;
		this.transforms = new ArrayList();
		this.isTramsformValues = Boolean.FALSE;
	}

	public DefaultFeatureStoreTransforms(DefaultFeatureStore store) {
		this.store = store;
		this.transforms = new ArrayList();
	}

	protected void checkEditingMode() {
		if (store == null || store.getMode() != FeatureStore.MODE_QUERY) {
			throw new IllegalStateException();
		}
	}

	protected void notifyChangeToStore() {
		this.store.notifyChange(FeatureStoreNotification.TRANSFORM_CHANGE);
	}

	public FeatureStoreTransform add(FeatureStoreTransform transform)
			throws DataException {
		checkEditingMode();
		if (transform.getDefaultFeatureType()!=null){
			if( ! transform.getFeatureTypes().contains(transform.getDefaultFeatureType())) {
				throw new IllegalArgumentException(); // FIXME: A�adir tipo especifico.
			}
			this.transforms.add(transform);
			transform.setSourceMetadata(this.store.metadata);
			this.notifyChangeToStore();
			if (this.isTramsformValues == null
					|| (!this.isTramsformValues.booleanValue())) {
				if (transform.isTransformsOriginalValues()) {
					this.isTramsformValues = Boolean.TRUE;
				}

			}
		}

		return transform;
	}


	public void clear() {
		checkEditingMode();
		this.transforms.clear();
		this.notifyChangeToStore();
		this.isTramsformValues = Boolean.FALSE;
	}

	public FeatureStoreTransform getTransform(int index) {
		return (FeatureStoreTransform) this.transforms.get(index);
	}

	public Iterator iterator() {
		return Collections.unmodifiableList(transforms).iterator();
	}

	public Object remove(int index) {
		checkEditingMode();
		Object trans = this.transforms.remove(index);
		this.notifyChangeToStore();
		this.isTramsformValues = null;
		return trans;
	}

	public boolean remove(FeatureStoreTransform transform) {
		checkEditingMode();
		boolean removed = this.transforms.remove(transform);
		if (removed) {
			this.notifyChangeToStore();

		}
		this.isTramsformValues = null;
		return removed;

	}

	public int size() {
		return this.transforms.size();
	}

	public boolean isEmpty() {
		return this.transforms.isEmpty();
	}

	private class TransformTemporalListElement {
		public FeatureStoreTransform transform = null;
		public FeatureType targetFeatureType = null;
	}

	private class TransformTemporalList extends ArrayList {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1677014259279944000L;
		private FeatureType targetFType;

		public boolean add(Object arg0) {
			if (this.isEmpty()) {
				targetFType = ((TransformTemporalListElement) arg0).targetFeatureType;
			}
			return super.add(arg0);
		}

	}

	protected TransformTemporalList getTransformTemporalList(
			FeatureType targetFeatureType) {
		if (this.lastTransformStack == null
				|| this.lastTransformStack.size() != this.transforms.size()
				|| !(this.lastTransformStack.targetFType
						.equals(targetFeatureType))) {
			TransformTemporalList result = new TransformTemporalList();
			TransformTemporalListElement item;
			FeatureType nextFType = targetFeatureType;

			for (int i = transforms.size() - 1; i > -1; i--) {
				item = new TransformTemporalListElement();
				item.transform = (FeatureStoreTransform) transforms.get(i);
				item.targetFeatureType = nextFType;
				nextFType = item.transform
						.getSourceFeatureTypeFrom(item.targetFeatureType);
				result.add(item);
			}
			this.lastTransformStack = result;
		}
		return this.lastTransformStack;
	}

	public Feature applyTransform(DefaultFeature source,
			FeatureType targetFeatureType)
			throws DataException {
		if (this.transforms.isEmpty()) {
			return source;
		}

		TransformTemporalList stack = this
				.getTransformTemporalList(targetFeatureType);
		TransformTemporalListElement item;
		FeatureProvider targetData;
		EditableFeature target;
		ListIterator iterator = stack.listIterator(stack.size());
		FeatureType tft = null;


		while (iterator.hasPrevious()) {
			item = (TransformTemporalListElement) iterator.previous();
			
			tft = item.targetFeatureType;
			if (tft instanceof EditableFeatureType) {
			    /*
			     * Must be non-editable to create 
			     * DefaultFeatureProvider
			     */
			    tft = ((EditableFeatureType) tft).getNotEditableCopy();
			}
			
			targetData = this.store.createDefaultFeatureProvider(tft);
			targetData.setOID(source.getData().getOID());
			targetData.setNew(false);
			target = (new DefaultEditableFeature(this.store, targetData))
					.getEditable();
			item.transform.applyTransform(source, target);
 			source = (DefaultFeature) target.getNotEditableCopy();
		}

		return source;

	}

	public FeatureType getSourceFeatureTypeFrom(FeatureType targetFeatureType) {
		FeatureType tmpFType = targetFeatureType;

		for (int i = transforms.size() - 1; i > -1; i--) {
			FeatureStoreTransform transform = (FeatureStoreTransform) transforms
					.get(i);
			tmpFType = transform.getSourceFeatureTypeFrom(tmpFType);
		}
		return tmpFType;
	}

	public FeatureType getDefaultFeatureType() throws DataException {
		if (this.transforms.isEmpty()) {
			return null;
		}
		FeatureStoreTransform transform = (FeatureStoreTransform) this.transforms
				.get(this.transforms.size() - 1);
		return transform.getDefaultFeatureType();
	}

	public List getFeatureTypes() throws DataException {
		if (this.transforms.isEmpty()) {
			return null;
		}
		FeatureStoreTransform transform = (FeatureStoreTransform) this.transforms
				.get(this.transforms.size() - 1);
		return transform.getFeatureTypes();
	}

	public FeatureStore getFeatureStore() {
		return this.store;
	}

	public void setFeatureStore(FeatureStore featureStore) {
		if (this.store != null) {
			throw new IllegalStateException();// FIXME: A�adir tipo especifico.
		}
		this.store = (DefaultFeatureStore) featureStore;
	}
	
	   
	public void setStoreForClone(FeatureStore featureStore) {
	        this.store = (DefaultFeatureStore) featureStore;
	}

	public boolean isTransformsOriginalValues() {
		if (this.isTramsformValues == null) {
			Iterator iter = this.transforms.iterator();
			FeatureStoreTransform transform;
			this.isTramsformValues = Boolean.FALSE;
			while (iter.hasNext()) {
				transform = (FeatureStoreTransform) iter.next();
				if (transform.isTransformsOriginalValues()){
					this.isTramsformValues = Boolean.TRUE;
					break;
				}
			}
		}
		return this.isTramsformValues.booleanValue();
	}

	public FeatureType getFeatureType(String featureTypeId)
			throws DataException {
		if (this.transforms.isEmpty()) {
			return null;
		}
		if (featureTypeId == null) {
			return this.getDefaultFeatureType();
		}
		Iterator iter = this.getFeatureTypes().iterator();
		FeatureType fType;
		while (iter.hasNext()) {
			fType = (FeatureType) iter.next();
			if (fType.getId().equals(featureTypeId)) {
				return fType;
			}
		}
		return null;
	}



	// *** Persistence ***

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("store", store);
		state.set("isTramsformValues", this.isTramsformValues);
		state.set("transforms", this.transforms);
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.isTramsformValues = (Boolean) state.get("isTramsformValues");
		this.transforms = (List) state.get("transforms");
		this.store = (DefaultFeatureStore) state.get("store");
	}

	public static void registerPersistent() {
		DynStruct definition = ToolsLocator.getPersistenceManager().addDefinition(
				DefaultFeatureStoreTransforms.class, 
				"DefaultFeatureStoreTransforms", 
				"DefaultFeatureStoreTransforms Persistent definition",
				null, 
				null
			);

		definition.addDynFieldBoolean("isTramsformValues").setMandatory(false);
		definition.addDynFieldList("transforms").setClassOfItems(FeatureStoreTransform.class).setMandatory(true);
		definition.addDynFieldObject("store").setClassOfValue(FeatureStore.class).setMandatory(true);

	}

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		if( ! this.transforms.isEmpty() ) {
			FeatureStoreTransform transform = (FeatureStoreTransform) transforms.get(this.transforms.size()-1);
			if( transform.hasDynValue(name) ) {
				return transform.getDynValue(name);
			}
		}
		throw new DynFieldNotFoundException(name, "transforms");
	}

	public void setDynValue(String name, Object value) throws DynFieldNotFoundException {
		if( ! this.transforms.isEmpty() ) {
			FeatureStoreTransform transform = (FeatureStoreTransform) transforms.get(this.transforms.size()-1);
			transform.setDynValue(name, value);
		}
	}

	public boolean hasDynValue(String name) {
		if( ! this.transforms.isEmpty() ) {
			FeatureStoreTransform transform = (FeatureStoreTransform) transforms.get(this.transforms.size()-1);
			if( transform.hasDynValue(name) ) {
				return true;
			}
		}
		return false;
	}
	
	public Object clone() throws CloneNotSupportedException {
	    
	    DefaultFeatureStoreTransforms cloned = (DefaultFeatureStoreTransforms) super.clone();
	    cloned.transforms = new ArrayList();
	    int n = this.transforms.size();
	    for (int i=0; i<n; i++) {
	        cloned.transforms.add(((FeatureStoreTransform) this.transforms.get(i)).clone());
	    }
	    cloned.lastTransformStack = null;
	    
	    return cloned;
	}

}
