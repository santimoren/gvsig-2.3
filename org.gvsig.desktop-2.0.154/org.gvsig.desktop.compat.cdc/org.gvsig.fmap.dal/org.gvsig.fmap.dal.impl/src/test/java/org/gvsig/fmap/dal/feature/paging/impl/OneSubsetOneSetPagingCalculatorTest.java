/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.paging.impl;

import junit.framework.TestCase;

import org.gvsig.tools.paging.PagingCalculator.Sizeable;

/**
 * Unit tests for the {@link OneSubsetOneSetPagingCalculator} class.
 * 
 * @author gvSIG team
 */
public class OneSubsetOneSetPagingCalculatorTest extends
 TestCase {

	private static final int MAX_PAGE_SIZE = 4;

	private TestSizeable sizeable1 = new TestSizeable(6);
	private TestSizeable sizeable2 = new TestSizeable(17);

	private OneSubsetOneSetPagingCalculator calculator;

	protected void setUp() throws Exception {
		super.setUp();
		calculator =
            new OneSubsetOneSetPagingCalculator(sizeable1, sizeable2,
                MAX_PAGE_SIZE);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getFirstSetSize()}
	 * .
	 */
	public void testGetFirstSetSize() {
		assertEquals(sizeable1.getSize(), calculator.getFirstSetSize());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getSecondSetSize()}
	 * .
	 */
	public void testGetSecondSetSize() {
		assertEquals(sizeable2.getSize() - sizeable1.getSize(), calculator.getSecondSetSize());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getFirstSetInitialIndex()}
	 * .
	 */
	public void testGetFirstSetInitialIndex() {
		calculator.setCurrentPage(0);
		assertEquals(0, calculator.getFirstSetInitialIndex());

		calculator.setCurrentPage(1);
		assertEquals(0 + MAX_PAGE_SIZE, calculator.getFirstSetInitialIndex());

		calculator.setCurrentPage(2);
		assertEquals(-1, calculator.getFirstSetInitialIndex());

		calculator.setCurrentPage(3);
		assertEquals(-1, calculator.getFirstSetInitialIndex());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertEquals(-1, calculator.getFirstSetInitialIndex());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertEquals(0, calculator.getFirstSetInitialIndex());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getSecondSetInitialIndex()}
	 * .
	 */
	public void testGetSecondSetInitialIndex() {
		calculator.setCurrentPage(0);
		assertEquals(-1, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(1);
		assertEquals(0, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(2);
		assertEquals(2, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(3);
		assertEquals(2 + MAX_PAGE_SIZE, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertEquals(2 + ((calculator.getNumPages() - 3) * MAX_PAGE_SIZE),
				calculator.getSecondSetInitialIndex());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertEquals(0, calculator.getSecondSetInitialIndex());

		// Set the two sets equal, so all elements in the first set are
		// contained in the second one
		sizeable1.setSize(15l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(4);

		calculator.setCurrentPage(0);
		assertEquals(-1, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(1);
		assertEquals(-1, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(2);
		assertEquals(-1, calculator.getSecondSetInitialIndex());

		calculator.setCurrentPage(3);
		assertEquals(-1, calculator.getSecondSetInitialIndex());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getFirstSetHowMany()}
	 * .
	 */
	public void testGetFirstSetHowMany() {
		calculator.setCurrentPage(0);
		assertEquals(MAX_PAGE_SIZE, calculator.getFirstSetHowMany());

		calculator.setCurrentPage(1);
		assertEquals(2, calculator.getFirstSetHowMany());

		calculator.setCurrentPage(2);
		assertEquals(0, calculator.getFirstSetHowMany());

		calculator.setCurrentPage(3);
		assertEquals(0, calculator.getFirstSetHowMany());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertEquals(0, calculator.getFirstSetHowMany());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertEquals(1, calculator.getFirstSetHowMany());

		// Change values so all data is available into the first set
		sizeable1.setSize(25l);
		sizeable2.setSize(25l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertEquals(20, calculator.getFirstSetHowMany());

		calculator.setCurrentPage(1);
		assertEquals(5, calculator.getFirstSetHowMany());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#getSecondSetHowMany()}
	 * .
	 */
	public void testGetSecondSetHowMany() {
		calculator.setCurrentPage(0);
		assertEquals(0l, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(1);
		assertEquals(2l, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(2);
		assertEquals(MAX_PAGE_SIZE, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(3);
		assertEquals(MAX_PAGE_SIZE, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertEquals(1l, calculator.getSecondSetHowMany());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertEquals(14l, calculator.getSecondSetHowMany());

		// Set the two sets equal, so all elements in the first set are
		// contained in the second one
		sizeable1.setSize(15l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(4);

		calculator.setCurrentPage(0);
		assertEquals(0l, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(1);
		assertEquals(0l, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(2);
		assertEquals(0l, calculator.getSecondSetHowMany());

		calculator.setCurrentPage(3);
		assertEquals(0l, calculator.getSecondSetHowMany());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#hasCurrentPageAllValuesInFirstSet()}
	 * .
	 */
	public void testHasCurrentPageAllValuesInFirstSet() {
		calculator.setCurrentPage(0);
		assertTrue(calculator.hasCurrentPageAllValuesInFirstSet());

		calculator.setCurrentPage(1);
		assertFalse(calculator.hasCurrentPageAllValuesInFirstSet());

		calculator.setCurrentPage(2);
		assertFalse(calculator.hasCurrentPageAllValuesInFirstSet());

		calculator.setCurrentPage(3);
		assertFalse(calculator.hasCurrentPageAllValuesInFirstSet());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertFalse(calculator.hasCurrentPageAllValuesInFirstSet());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertFalse(calculator.hasCurrentPageAllValuesInFirstSet());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#hasCurrentPageAnyValuesInFirstSet()}
	 * .
	 */
	public void testHasCurrentPageAnyValuesInFirstSet() {
		calculator.setCurrentPage(0);
		assertTrue(calculator.hasCurrentPageAnyValuesInFirstSet());

		calculator.setCurrentPage(1);
		assertTrue(calculator.hasCurrentPageAnyValuesInFirstSet());

		calculator.setCurrentPage(2);
		assertFalse(calculator.hasCurrentPageAnyValuesInFirstSet());

		calculator.setCurrentPage(3);
		assertFalse(calculator.hasCurrentPageAnyValuesInFirstSet());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertFalse(calculator.hasCurrentPageAnyValuesInFirstSet());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertTrue(calculator.hasCurrentPageAnyValuesInFirstSet());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.feature.paging.impl.OneSubsetOneSetPagingCalculator#hasCurrentPageAnyValuesInSecondSet()}
	 * .
	 */
	public void testHasCurrentPageAnyValuesInSecondSet() {
		calculator.setCurrentPage(0);
		assertFalse(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(1);
		assertTrue(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(2);
		assertTrue(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(3);
		assertTrue(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(calculator.getNumPages() - 1);
		assertTrue(calculator.hasCurrentPageAnyValuesInSecondSet());

		// Change values so all data fits into a single page
		sizeable1.setSize(1l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(20);

		calculator.setCurrentPage(0);
		assertTrue(calculator.hasCurrentPageAnyValuesInSecondSet());

		// Set the two sets equal, so all elements in the first set are
		// contained in the second one
		sizeable1.setSize(15l);
		sizeable2.setSize(15l);
		calculator.setMaxPageSize(4);

		calculator.setCurrentPage(0);
		assertFalse(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(1);
		assertFalse(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(2);
		assertFalse(calculator.hasCurrentPageAnyValuesInSecondSet());

		calculator.setCurrentPage(3);
		assertFalse(calculator.hasCurrentPageAnyValuesInSecondSet());
	}

    private class TestSizeable implements Sizeable {

		private long size;

		public TestSizeable(long size) {
			this.size = size;
		}

		public long getSize() {
			return size;
		}

		public void setSize(long size) {
			this.size = size;
		}
	}
}
