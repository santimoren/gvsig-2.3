/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import junit.framework.TestCase;

import org.easymock.MockControl;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceParameters;

public class TestAbstractResourcePerformance extends TestCase {
	private MockControl paramsControl;
	private ResourceParameters params;
	private AbstractResource resource;
	private MockControl paramsControl2;
	private ResourceParameters params2;
	private AbstractNonBlockingResource nonBlockingResource;

	protected void setUp() throws Exception {
		super.setUp();
		paramsControl = MockControl.createNiceControl(ResourceParameters.class);
		params = (ResourceParameters) paramsControl.getMock();
		resource = new TestResource(params);
		paramsControl2 =
				MockControl.createNiceControl(ResourceParameters.class);
		params2 = (ResourceParameters) paramsControl2.getMock();
		nonBlockingResource = new TestNonBlockingResource(params2);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#execute(java.lang.Runnable)}
	 * .
	 */
	public void testExecuteBlockingPerformance() throws Exception {
		executePerformance(new ResourceAction() {
			public Object run() throws Exception {
				return null;
			}
		}, resource);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#execute(java.lang.Runnable)}
	 * .
	 */
	public void testExecuteNonBlockingPerformance() throws Exception {
		executePerformance(new ResourceAction() {
			public Object run() throws Exception {
				return null;
			}
		}, nonBlockingResource);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#execute(java.lang.Runnable)}
	 * .
	 */
	public void executePerformance(ResourceAction action, Resource resource)
			throws Exception {

		final int executions = 100000;
		long time1 = System.currentTimeMillis();
		for (int i = 0; i < executions; i++) {
			resource.execute(action);
		}
		long time2 = System.currentTimeMillis();

		System.out.print(resource.getName());
		System.out.print(": total time: " + (time2 - time1) + " ms.");
		System.out.println(" - time per execution: "
				+ ((float) (time2 - time1) / (float) executions) + " ms.");
	}
}