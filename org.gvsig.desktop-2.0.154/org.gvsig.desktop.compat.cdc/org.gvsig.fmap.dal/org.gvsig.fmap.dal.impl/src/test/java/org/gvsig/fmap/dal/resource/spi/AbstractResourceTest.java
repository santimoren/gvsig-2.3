/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import junit.framework.TestCase;

import org.easymock.MockControl;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyOpenException;

/**
 * Unit tests for the class {@link AbstractResource}
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class AbstractResourceTest extends TestCase {

	private AbstractResource resource;
	private MockControl paramsControl;
	private ResourceParameters params;

	protected void setUp() throws Exception {
		super.setUp();
		paramsControl = MockControl.createNiceControl(ResourceParameters.class);
		params = (ResourceParameters) paramsControl.getMock();
		resource = new TestResource(params);
	}

	/**
	 * Test method for {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#getLastTimeOpen()}.
	 */
	public void testGetLastTimeOpen() throws ResourceNotifyOpenException {
		long time = resource.getLastTimeOpen();
		resource.notifyOpen();
		assertTrue("The resource last time open hasn't been updated",
				resource.getLastTimeOpen() > time);
	}

	/**
	 * Test method for {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#getLastTimeUsed()}.
	 */
	public void testGetLastTimeUsed() throws Exception {
		long time = resource.getLastTimeUsed();
		resource.execute(new ResourceAction() {
			public Object run() throws Exception {
				try {
					// Wait for 100 milliseconds
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// No problem
				}
				return null;
			}
		});
		assertTrue("The resource last time used hasn't been updated",
				resource.getLastTimeUsed() > time);
	}

	/**
	 * Test method for {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#openCount()}.
	 */
	public void testOpenCount() throws Exception {
		assertEquals(0, resource.openCount());
		resource.notifyOpen();
		assertEquals(1, resource.openCount());
		resource.notifyOpen();
		resource.notifyOpen();
		assertEquals(3, resource.openCount());
		resource.notifyClose();
		assertEquals(2, resource.openCount());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.dal.resource.spi.AbstractResource#execute(java.lang.Runnable)}
	 * .
	 */
	public void testExecute() throws Exception {
		final MutableBoolean testValue = new MutableBoolean();
		resource.execute(new ResourceAction() {
			public Object run() throws Exception {
				testValue.setValue(true);
				return null;
			}
		});
		assertTrue("Runnable execution not performed", testValue.isValue());
	}

	public class MutableBoolean {
		private boolean value = false;

		public void setValue(boolean value) {
			this.value = value;
		}

		public boolean isValue() {
			return value;
		}
	}
}
