/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi;

import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureType;

/**
 * Interface for feature references services for feature based data providers
 *
 *
 * @author jmvivo
 *
 */
public interface FeatureReferenceProviderServices extends FeatureReference {

	/**
	 * Return the OID (Object IDentifier) object that represents this reference
	 *
	 * <strong>Note:</strong> this can be <code>null</code> if provider does not
	 * support OID
	 *
	 * @return <code>Object</code> that identifies a feature
	 *
	 * @see FeatureStoreProvider#getOIDType()
	 * @see FeatureType#hasOID()
	 */
	public Object getOID();

	/**
	 * Return the attribute names that compound the Primary Key of represented
	 * feature
	 *
	 * @return attribute names
	 *
	 * @see #getKeyValue(String)
	 */
	public String[] getKeyNames();

	/**
	 * Return the value of an attribute that compound the Primary Key of
	 * represented feature
	 *
	 * @param name
	 * @return
	 *
	 * @see #getKeyNames()
	 */
	public Object getKeyValue(String name);

	/**
	 * Return the identifier of the {@link FeatureType} of the represented
	 * feature
	 *
	 * @return
	 */
	public String getFeatureTypeId();

}
