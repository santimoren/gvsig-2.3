/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.raster.spi;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.raster.CoverageSelection;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;


/**
 * @author jmvivo
 *
 */
public abstract class AbstractCoverageStoreProvider extends AbstractDisposable
		implements CoverageStoreProvider {
	protected CoverageStoreProviderServices store;
	protected DelegatedDynObject metadata;
	protected DataStoreParameters parameters;


	protected AbstractCoverageStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices, DynObject metadata) {
		init(params, storeServices, metadata);
	}

	protected AbstractCoverageStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices) {
		init(params, storeServices, null);
	}
	
	protected AbstractCoverageStoreProvider() {
	}
	
	protected void init(DataStoreParameters params,
			DataStoreProviderServices storeServices, DynObject metadata) {
		this.store = (CoverageStoreProviderServices) storeServices;
		this.metadata = (DelegatedDynObject) metadata;
		this.parameters = params;
	}
	
	/**
	 * Gets the DataStoreParameters
	 * @return DataStoreParameters
	 */
	public DataStoreParameters getDataStoreParameters() {
		return parameters;
	}

	/**
	 * Set metada container if this not set at construction time and only in one
	 * time. In other case an Exception will be throw
	 *
	 * @param metadata
	 */
	protected void setMetadata(DynObject metadata) {
		if (this.metadata != null) {
			// FIXME Exception
			throw new IllegalStateException();
		}
		this.metadata = (DelegatedDynObject) metadata;
	}

	protected ResourceProvider createResource(String type, Object[] params)
			throws InitializeException {
		ResourceManagerProviderServices manager = (ResourceManagerProviderServices) DALLocator
				.getResourceManager();
		ResourceProvider resource = manager.createAddResource(type, params);
		return resource;
	}

	public CoverageStoreProviderServices getStoreServices() {
		return this.store;
	}

	public String getClassName() {
		return this.getClass().getName();
	}

	public boolean allowWrite() {
		return false;
	}

	public CoverageSelection createCoverageSelection() throws DataException {
		return this.store.createDefaultCoverageSelection();
	}

	public void refresh() throws OpenException {
		// Do nothing by default
	}

	public void close() throws CloseException {
		// Do nothing by default
	}

	protected void doDispose() throws BaseException {
		this.metadata = null;
		this.store = null;
		this.parameters = null;
	}

	public Envelope getEnvelope() throws DataException {
		return null;
	}

	public abstract DataServerExplorer getExplorer() throws ReadException,
			ValidateDataParametersException;

	public void delegate(DynObject dynObject) {
		if (this.metadata == null) {
			return;
		}
		this.metadata.delegate(dynObject);
	}

	public DynClass getDynClass() {
		if (this.metadata == null) {
			return null;
		}
		return this.metadata.getDynClass();
	}

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.getDynValue(name);
	}

	public boolean hasDynValue(String name) {
		if (this.metadata == null) {
			return false;
		}
		// TODO this.open??
		return this.metadata.hasDynValue(name);
	}

	public void implement(DynClass dynClass) {
		if (this.metadata == null) {
			return;
		}
		this.metadata.implement(dynClass);

	}

	public Object invokeDynMethod(int code, Object[] args)
			throws DynMethodException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.invokeDynMethod(this, code, args);
	}

	public Object invokeDynMethod(String name, Object[] args)
			throws DynMethodException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.invokeDynMethod(this, name, args);
	}

	public void setDynValue(String name, Object value)
			throws DynFieldNotFoundException {
		if (this.metadata == null) {
			return;
		}
		// TODO this.open??
		this.metadata.setDynValue(name, value);
	}

	public void clear() {
		if (metadata != null) {
			metadata.clear();
		}
	}
}
