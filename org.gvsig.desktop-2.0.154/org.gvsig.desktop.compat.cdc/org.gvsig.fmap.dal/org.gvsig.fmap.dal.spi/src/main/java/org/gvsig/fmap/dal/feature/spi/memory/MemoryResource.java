/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi.memory;

import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.spi.AbstractNonBlockingResource;

/**
 * Resource based on memory. It may be used to represent some data loaded into
 * memory, so it can be named as the data or the source resource loaded.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MemoryResource extends AbstractNonBlockingResource {

	public static final String NAME = "memory";
	public static final String DESCRIPTION = "Memory based resource";

	/**
	 * Creates a new {@link MemoryResource}.
	 * 
	 * @param parameters
	 *            to create the resource
	 * @throws InitializeException
	 *             if there is an error creating the resource
	 */
	public MemoryResource(MemoryResourceParameters parameters)
			throws InitializeException {
		super(parameters);
	}

	public Object get() throws AccessResourceException {
		return getName();
	}

	public String getName() throws AccessResourceException {
		return NAME;
	}

	public boolean isThis(ResourceParameters parameters)
			throws ResourceException {
		return false;
	}

}
