/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.spi;

import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.cache.FeatureCacheProvider;
import org.gvsig.fmap.dal.feature.spi.cache.FeatureCacheProviderFactory;
import org.gvsig.fmap.dal.feature.spi.index.FeatureIndexProviderServices;
import org.gvsig.tools.dynobject.DynObject;


/**
 * Inteface of DataManager of Services for data providers
 *
 * @author jmvivo
 *
 */
public interface DataManagerProviderServices extends DataManager {

    /**
     * 
     * @param name
     * @param dataStoreProviderClass
     * @param parametersClass 
     * @deprecated Use registerStoreProviderFactory
     */
	public void registerStoreProvider(String name,
			Class dataStoreProviderClass,
			Class parametersClass);
	
	/**
	 * Registers a store factory. 
	 * @param name
         * @param storeFactoryClass
	 */
	public void registerStoreFactory(String name,
			Class storeFactoryClass);

	/**
	 * Register a new provider of data server explorer
	 *
	 * FIXME
	 *
         * @param name
	 * @param dataSourceClass
	 *            class of provider
	 * @param parametersClass
	 *            parameters class of provider
	 */
	public void registerExplorerProvider(String name,
			Class dataSourceClass, Class parametersClass);


    /**
     * Registers a new feature index provider.
     *
     * @param name
     * 			provider's name
     *
     * @param description
     * 			provider's description
     *
     * @param clazz
     * 			a custom FeatureIndexProvider implementation
     *
     * @param dataType
     * 			one of the constants in {@link DataTypes}. This means that this provider
     * 			can build indexes based on attributes of this type.
     */
	public void registerFeatureIndexProvider(String name, String description, Class clazz, int dataType);

	/**
	 * Returns a DataIndexProvider compatible with the attribute data type.
	 *
         * @param name
	 * @param store
	 *            associated FeatureStore
	 * @param type
	 *            associated FeatureType
         * @param indexName
	 * @param attr
	 *            associated FeatureAttributeDescriptor
	 * @return empty DataIndexProvider, initialized and ready to use
         * @throws org.gvsig.fmap.dal.exception.InitializeException
         * @throws org.gvsig.fmap.dal.exception.ProviderNotRegisteredException
	 */
	public FeatureIndexProviderServices createFeatureIndexProvider(String name, FeatureStore store, FeatureType type,
			String indexName,
			FeatureAttributeDescriptor attr) throws InitializeException,
			ProviderNotRegisteredException;

       /**
        * Registers a new cache provider.
        *
        *
        * @param providerFactory
        */
	 public void registerFeatureCacheProvider(FeatureCacheProviderFactory providerFactory);
		    
	 /**
	  * Creates a FeatureCacheProvider from a name and the parameters to configure 
	  * its data server explorer. The cache provider creates new stores using the
	  * {@link DataServerExplorerParameters}.
	  * @param name provider's name.	   	
	  * @param parameters parameters used to create a explorer.
	  * @return A cache provider
	  * @throws DataException
	  */
	 public FeatureCacheProvider createFeatureCacheProvider(String name, DynObject parameters) throws DataException;


	 /**
	  *
	  * Creates a new instance of the provider associated to the passed parameters.
	  * 
          * @param providerServices
	  * @param parameters
	  * @return
	  * @throws InitializeException
	  * @throws ProviderNotRegisteredException
	  */
	 public DataStoreProvider createProvider(DataStoreProviderServices providerServices, DataStoreParameters parameters) throws InitializeException, ProviderNotRegisteredException;	 

	 public void registerStoreProviderFactory(DataStoreProviderFactory factory);

         /**
          * Este metodo es temporal hasta que se integre el nuevo raster en el 
          * core de gvSIG.
          * 
          * @param rasterStoreClass 
          */
         public void registerDefaultRasterStore(Class rasterStoreClass);
}
