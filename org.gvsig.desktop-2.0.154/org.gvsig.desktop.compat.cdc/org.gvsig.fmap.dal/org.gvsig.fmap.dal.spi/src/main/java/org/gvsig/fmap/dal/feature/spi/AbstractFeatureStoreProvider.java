/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.cresques.cts.ICRSFactory;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.FeatureLocks;
import org.gvsig.fmap.dal.feature.FeatureReference;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.timesupport.Interval;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;

/**
 * Abstract implementation of {@link FeatureStoreProvider}
 *
 */
public abstract class AbstractFeatureStoreProvider extends AbstractDisposable
		implements FeatureStoreProvider {

	private FeatureStoreProviderServices store;
	private DelegatedDynObject metadata;
	private DataStoreParameters parameters;

    private static final Logger logger = LoggerFactory.getLogger(AbstractFeatureStoreProvider.class);


	/**
	 * Default Constructor.
	 *
	 * @param params
	 * @param storeServices
	 * @param metadata
	 */
	protected AbstractFeatureStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices, DynObject metadata) {
		this.store = (FeatureStoreProviderServices) storeServices;
		this.metadata = (DelegatedDynObject) metadata;
		this.parameters = params;
	}

	/**
	 * Constructor when cannot create metada in constrution time. <br>
	 * <br>
	 * <strong>Note: </strong> Don't use it if not is necesary. Set metada
	 * <strong>as soon as posible</strong> by
	 * {@link AbstractFeatureStoreProvider#setMetadata(DynObject)}
	 *
	 * @param params
	 * @param storeServices
	 */
	protected AbstractFeatureStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices) {
		this.store = (FeatureStoreProviderServices) storeServices;
		this.metadata = null;
		this.parameters = params;
	}

	public final FeatureProvider getFeatureProviderByReference(
			final FeatureReferenceProviderServices reference)
			throws DataException {
		this.open();
		FeatureProvider featureProvider = (FeatureProvider) getResource()
				.execute(new ResourceAction() {
					public Object run() throws Exception {
						return internalGetFeatureProviderByReference(reference);
					}
                                        public String toString() {
                                            return "getFeatureByReference";
                                        }

				});

		if (featureProvider == null) {
			throw new FeatureProviderNotFoundException(reference);
		}

		return featureProvider;
	}

	public final FeatureProvider getFeatureProviderByReference(
			final FeatureReferenceProviderServices reference,
			final FeatureType featureType) throws DataException {
		this.open();
		FeatureProvider featureProvider = (FeatureProvider) getResource()
				.execute(new ResourceAction() {
			public Object run() throws Exception {
				return internalGetFeatureProviderByReference(reference,
						featureType);
			}
		});

		if (featureProvider == null) {
			throw new FeatureProviderNotFoundException(reference);
		}

		return featureProvider;
	}

	/**
	 * Returns a {@link FeatureProvider} by reference, using the default
	 * {@link FeatureType}. This method may be rewritten by the child classes as
	 * an implementation of the
	 * {@link #getFeatureProviderByReference(FeatureReferenceProviderServices)}
	 * method.
	 *
	 * @param reference
	 *            the reference to the {@link FeatureProvider}
	 * @return the {@link FeatureProvider} being referenced
	 * @throws DataException
	 *             if there is an error loading the {@link FeatureProvider}
	 */
	protected FeatureProvider internalGetFeatureProviderByReference(
			FeatureReferenceProviderServices reference) throws DataException {
		return internalGetFeatureProviderByReference(reference,
				getStoreServices().getDefaultFeatureType());
	}

	/**
	 * Set metada container if this not set at construction time and only in one
	 * time. In other case an Exception will be throw
	 *
	 * @param metadata
	 */
	protected void setMetadata(DynObject metadata) {
		if (this.metadata != null) {
			// FIXME Exception
			throw new IllegalStateException();
		}
		this.metadata = (DelegatedDynObject) metadata;
	}

	/**
	 * @return the parameters
	 */
	public DataStoreParameters getParameters() {
		return parameters;
	}

	/**
	 * Create or get a resource of <code>type</code> for <code>params</code> in
	 * {@link ResourceManager}
	 *
	 * @param type
	 * @param params
	 * @return
	 * @throws InitializeException
	 */
	protected ResourceProvider createResource(String type, Object[] params)
			throws InitializeException {
		ResourceManagerProviderServices manager = (ResourceManagerProviderServices) DALLocator
				.getResourceManager();
		ResourceProvider resource = manager.createAddResource(type, params);
		return resource;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#getStoreServices()
	 */
	public FeatureStoreProviderServices getStoreServices() {
		return this.store;
	}

	public FeatureStore getFeatureStore() {
		if(this.store == null){
			return null;
		}
		return this.store.getFeatureStore();
	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#allowWrite()
	 */
	public boolean allowWrite() {
		return false;
	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#performChanges(Iterator,
	 *      Iterator, Iterator, Iterator)
	 */

	public void performChanges(Iterator deleteds, Iterator inserteds,
			Iterator updateds, Iterator featureTypesChanged)
			throws DataException {
		// FIXME exception
		throw new UnsupportedOperationException();

	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#isLocksSupported()
	 */
	public boolean isLocksSupported() {
		return false;
	}

	/**
	 * Default Factory of {@link FeatureProvider}. Create a new default
	 * {@link FeatureProvider} instance.<br>
	 *
	 * Override this if you need an special implemtation of
	 * {@link FeatureProvider}.
	 *
	 * @return
	 * @throws DataException
	 *
	 * @see {@link org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#createFeatureProvider(FeatureType)}
	 */

	public FeatureProvider createFeatureProvider(FeatureType type)
			throws DataException {
		return this.store.createDefaultFeatureProvider(type);
	}

	/**
	 * unsupported by default (return <code>null</code>), override this
	 * otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#createFeatureLocks()
	 */
	public FeatureLocks createFeatureLocks() throws DataException {
		return null;
	}

	/**
	 * Default Factory of {@link FeatureSelection}. Create a new default
	 * {@link FeatureSelection} instance.<br>
	 *
	 * Override this if you need an special implemtation of
	 * {@link FeatureSelection}.
	 *
	 * @return
	 * @throws DataException
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#createFeatureSelection()
	 */
	public FeatureSelection createFeatureSelection() throws DataException {
		return this.store.createDefaultFeatureSelection();
	}

	/**
	 * do nothing by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#refresh()
	 */
	public void refresh() throws OpenException {
		// Do nothing by default
	}

	/**
	 * do nothing by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#close()
	 */
	public void close() throws CloseException {
		// Do nothing by default
	}

	protected void doDispose() throws BaseException {
		this.metadata = null;
		this.store = null;
	}

	/**
	 * unsupported geometry by default (return <code>null</code>), override this
	 * otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#getEnvelope()
	 */
	public Envelope getEnvelope() throws DataException {
		return null;
	}

	/**
	 * unsupported geometry write by default (return <code>false</code>),
	 * override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#canWriteGeometry(int,
	 *      int)
	 */
	public boolean canWriteGeometry(int geometryType, int geometrySubType)
			throws DataException {
		return false;
	}

	// --- Metadata methods ---

	public void delegate(DynObject dynObject) {
		if (this.metadata == null) {
			return;
		}
		this.metadata.delegate(dynObject);
	}

	public DynClass getDynClass() {
		if (this.metadata == null) {
			return null;
		}
		return this.metadata.getDynClass();
	}

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.getDynValue(name);
	}

	public boolean hasDynValue(String name) {
		if (this.metadata == null) {
			return false;
		}
		// TODO this.open??
		return this.metadata.hasDynValue(name);
	}

	public void implement(DynClass dynClass) {
		if (this.metadata == null) {
			return;
		}
		this.metadata.implement(dynClass);

	}

	public Object invokeDynMethod(int code, Object[] args)
			throws DynMethodException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.invokeDynMethod(this, code, args);
	}

	public Object invokeDynMethod(String name, Object[] args)
			throws DynMethodException {
		if (this.metadata == null) {
			return null;
		}
		// TODO this.open??
		return this.metadata.invokeDynMethod(this, name, args);
	}

	public void setDynValue(String name, Object value)
			throws DynFieldNotFoundException {
		if (this.metadata == null) {
			return;
		}
		// TODO this.open??
		this.metadata.setDynValue(name, value);
	}

	// --- end Metadata methods ---

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#allowAutomaticValues()
	 */
	public boolean allowAutomaticValues() {
		return false;

	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#append(org.gvsig.
	 *      fmap.dal.feature.spi.FeatureProvider)
	 */
	public void append(FeatureProvider featureProvider) throws DataException {
		// FIXME exception
		throw new UnsupportedOperationException();
	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#beginAppend()
	 */
	public void beginAppend() throws DataException {
		// FIXME exception
		throw new UnsupportedOperationException();
	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#endAppend()
	 */
	public void endAppend() throws DataException {
		// FIXME exception
		throw new UnsupportedOperationException();
	}

	public void abortAppend() throws DataException {
		// FIXME exception
		throw new UnsupportedOperationException();
	}

	/**
	 * unsupported by default, override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#supportsAppendMode()
	 */
	public boolean supportsAppendMode() {
		return false;
	}

	/**
	 * unsupported by default (return null), override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.spi.DataStoreProvider#getChilds()
	 */
	public Iterator getChilds() {
		return null;
	}

	/**
	 * unsupported by default (return null), override this otherwise
	 *
	 * @see org.gvsig.fmap.dal.spi.DataStoreProvider#getExplorer()
	 */
	public DataServerExplorer getExplorer() throws ReadException,
			ValidateDataParametersException {
		return null;
	}

	public void clear() {
		if (metadata != null) {
			metadata.clear();
		}
	}

	/**
	 * Returns a {@link FeatureProvider} by reference, using the provided
	 * {@link FeatureType}. This is the child classes implementation of the
	 * {@link #getFeatureProviderByReference(FeatureReferenceProviderServices)}
	 * method.
	 *
	 * @param reference
	 *            the reference to the {@link FeatureProvider}
	 * @param featureType
	 *            the type of feature to load
	 * @return the {@link FeatureProvider} being referenced
	 * @throws DataException
	 *             if there is an error loading the {@link FeatureProvider}
	 */
	protected abstract FeatureProvider internalGetFeatureProviderByReference(
			FeatureReferenceProviderServices reference, FeatureType featureType)
			throws DataException;

	public static class FeatureProviderNotFoundException extends DataException {

		private static final long serialVersionUID = 5161749797695723151L;

		public FeatureProviderNotFoundException(FeatureReference reference) {
			super("Cannot retreive FeatureProvider for reference %(reference)",
					"_FeatureProviderNotFoundException", serialVersionUID);
			setValue("reference", reference.toString());
		}
	}

	public boolean isKnownEnvelope(){
	    return true;
	}

    public boolean hasRetrievedFeaturesLimit(){
        return false;
    }

    public int getRetrievedFeaturesLimit(){
        return -1;
    }

    public Interval getInterval() {
        return null;
    }

    public Collection getTimes() {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection getTimes(Interval interval) {
        // TODO Auto-generated method stub
        return null;
    }

    protected void savePrjFile(File dataFile, IProjection proj){
        File file = new File(FilenameUtils.removeExtension(dataFile.getAbsolutePath())+".prj");
        try {
            String export = proj.export(ICRSFactory.FORMAT_WKT_ESRI);
            if(export!=null){
                FileUtils.writeStringToFile(file, export);
            }
        } catch (Exception e) {
            logger.info("Can't write prj file '" + file.getAbsolutePath() + "'.");
        }
    }
}
