/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.spi;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.exception.CopyParametersException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynObjectEncoder;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * @author jmvivo
 *
 */
public abstract class AbstractDataParameters implements DataParameters {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDataParameters.class);

    @Override
    public Object getDynValue(String name) {
        return getDelegatedDynObject().getDynValue(name);
    }

    @Override
    public String toString() {
        DynObjectEncoder encoder = ToolsLocator.getDynObjectManager().createSimpleDynObjectEncoder();
        return encoder.encode(this);
    }

    @Override
    public void setDynValue(String name, Object value) {
        DelegatedDynObject delegated = getDelegatedDynObject();
        if (delegated.getDynClass().getDynField(name) != null) {
            delegated.setDynValue(name, value);
        } else {
            try {
                throw new IllegalArgumentException(name);
            } catch (IllegalArgumentException ex) {
                logger.warn("Attribute '" + name + "' is not defined in "
                        + delegated.getDynClass().getFullName() + " definition", ex);
            }
        }
    }

    @Override
    public void clear() {
        DynObjectManager manager = ToolsLocator.getDynObjectManager();
        manager.clear(this);
    }

    protected void copyValuesTo(AbstractDataParameters target) {
        DynObjectManager manager = ToolsLocator.getDynObjectManager();
        manager.copy(this, target);
    }

    @Override
    public DataParameters getCopy() {
        // TODO Delegar en el DynObject cuando tenga este servicio
        AbstractDataParameters copy;
        try {
            copy = (AbstractDataParameters) this.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CopyParametersException("data parameters", e);
        }
        this.copyValuesTo(copy);
        return copy;
    }

    @Override
    public void saveToState(PersistentState state) throws PersistenceException {
        DynField[] fields = getDelegatedDynObject().getDynClass().getDynFields();

        for (DynField field : fields) {
            if (field.isPersistent()) {
                String name = field.getName();
                Object value = this.getDynValue(name);
                state.set(name, value);
            }
        }
    }

    @Override
    public void loadFromState(PersistentState state) throws PersistenceException {
        @SuppressWarnings("rawtypes")
        Iterator it = state.getNames();
        while (it.hasNext()) {
            String name = (String) it.next();
            try {
                Object value = state.get(name);
                this.setDynValue(name, value);
            } catch (Throwable t) {
                logger.warn("Can't load '" + name + "' property", t);
            }
        }
    }

    @Override
    public void delegate(DynObject dynObject) {
        getDelegatedDynObject().delegate(dynObject);

    }

    @Override
    public DynClass getDynClass() {
        return getDelegatedDynObject().getDynClass();
    }

    @Override
    public boolean hasDynValue(String name) {
        return getDelegatedDynObject().hasDynValue(name);
    }

    @Override
    public void implement(DynClass dynClass) {
        getDelegatedDynObject().implement(dynClass);
    }

    @Override
    public Object invokeDynMethod(String name, Object[] args)
            throws DynMethodException {
        return getDelegatedDynObject().invokeDynMethod(this, name, args);
    }

    @Override
    public Object invokeDynMethod(int code, Object[] args)
            throws DynMethodException {
        return getDelegatedDynObject().invokeDynMethod(this, code, args);
    }

    @Override
    public void validate() throws ValidateDataParametersException {
        try {
            this.getDynClass().validate(this);
        } catch (DynObjectValidateException e) {
            throw new ValidateDataParametersException(e);
        }
    }

    /**
     * Returns an instance of the {@link DynObject} to delegate to.
     *
     * @return the delegate {@link DynObject}
     */
    protected abstract DelegatedDynObject getDelegatedDynObject();

}
