/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi;

import java.util.List;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.spi.AbstractDataStoreProviderFactory;

public abstract class AbstractFeatureStoreProviderFactory extends
		AbstractDataStoreProviderFactory implements FeatureStoreProviderFactory {


	public AbstractFeatureStoreProviderFactory(String name, String description) {
		super(name, description);
	}

	public int allowMultipleGeometryTypes() {
		return UNKNOWN;
	}

	public int allowEditableFeatureType() {
		return UNKNOWN;
	}

    public List getSupportedDataTypes() {
        // null means all supported
        return null;
    }

    public List getSupportedGeometryTypesSubtypes() {
        // null means all supported
        return null;
    }

    public boolean allowsMandatoryAttributes() {
        return true;
    }

    public boolean allowsPrimaryKeyAttributes() {
        return true;
    }

    public int useLocalIndexesCanImprovePerformance() {
	return UNKNOWN;
    }

    /**
     * @return  dummy feature type. Must be overridden by subclasses
     *
     */
    public FeatureType createDefaultFeatureType() {
        DataManager dm = DALLocator.getDataManager();
        EditableFeatureType eft = dm.createFeatureType();

        if (allowEditableFeatureType() == DataStoreProviderFactory.YES) {
            return eft;
        } else {
            return eft.getNotEditableCopy();
        }
    }

    @Override
    public int getMaxAttributeNameSize() {
        return -1;
    }
}
