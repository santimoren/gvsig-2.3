/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi.index;

import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureIndex;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.FeatureIndexException;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProviderServices;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;

/**
 * SPI for all index implementations.
 * 
 * This observable object provides notificationswhen the index has finished
 * filling with data and is available to be used. The observer will
 * receive then a {@link FeatureStoreNotification#INDEX_FILLING_SUCCESS}
 * notification, with the index object if it has finished
 * successfully, or a {@link FeatureStoreNotification#INDEX_FILLING_ERROR}
 * notification with the exception object if there has been
 * any error in the process.
 * 
 * @see FeatureStoreNotification#INDEX_FILLING_SUCCESS
 * @see FeatureStoreNotification#INDEX_FILLING_ERROR
 * 
 * @author gvSIG team
 */
public interface FeatureIndexProviderServices extends FeatureIndex, Observable,
    Disposable {

    /** Initializes this provider */
    public void initialize() throws InitializeException;

    /** Column to which belongs this index */
    public FeatureAttributeDescriptor getFeatureAttributeDescriptor();

    /** FeatureType to which belongs this index */
    public FeatureType getFeatureType();

    /** FeatureStore to which belongs this index */
    public FeatureStoreProviderServices getFeatureStoreProviderServices();

    /**
     * Returns the absolute path (directory + filename) where this index is or
     * will be stored
     */
    public String getFileName();

    /**
     * Returns a temporary absolute path (directory + filename) according to the
     * system environment
     */
    public String getTemporaryFileName();

    /**
     * Calculates and returns a new filename for an index, using the given
     * prefix and suffix
     */
    public String getNewFileName(String prefix, String sufix);

    /**
     * Fills this index with the store's data. This operation will not return
     * until the index has filled with all the store's data.
     * 
     * @throws FeatureIndexException
     *             if there is an error while filling the index
     */
    public void fill() throws FeatureIndexException;

    /**
     * Fills this index with the store's data.
     * 
     * @param background
     *            if the filling must be performed in background
     * @param observer
     *            optional observer to be notified when the
     *            fill index operation finishes
     * 
     * @throws FeatureIndexException
     *             if there is an error while filling the index
     * 
     * @see FeatureStoreNotification#INDEX_FILLING_STARTED
     * @see FeatureStoreNotification#INDEX_FILLING_SUCCESS
     * @see FeatureStoreNotification#INDEX_FILLING_CANCELLED
     * @see FeatureStoreNotification#INDEX_FILLING_ERROR
     */
    public void fill(boolean background, Observer observer)
        throws FeatureIndexException;

    /**
     * Sets the index as valid or invalid, so it may be used or not.
     * 
     * @param valid
     *            status to set the index to
     */
    public void setValid(boolean valid);

    /**
     * If the index is in the process of being filled by a background task,
     * synchronize this method or add some mechanism for external callers to
     * block on it.
     */
    public void waitForIndex();
}
