package org.gvsig.fmap.dal.spi;

import java.io.File;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.exception.BaseException;

public abstract class AbstractDataServerExplorer extends AbstractDisposable implements DataServerExplorer {

    private DataServerExplorerParameters parameters;
    private final DataServerExplorerProviderServices providerServices;

    protected AbstractDataServerExplorer(DataServerExplorerParameters parameters, DataServerExplorerProviderServices providerServices) {
        this.parameters = parameters;
        this.providerServices = providerServices;
    }

    public DataServerExplorerProviderServices getServerExplorerProviderServices() {
        return this.providerServices;
    }
    
    @Override
    public DataServerExplorerParameters getParameters() {
        return this.parameters;
    }

    @Override
    protected void doDispose() throws BaseException {
        this.parameters = null;
    }

    @Override
    public DataStoreParameters get(String name) throws DataException {
        return null;
    }
    
    public File getResourcePath(DataStore dataStore, String resourceName) throws DataException {
        return null;
    }

    
}
