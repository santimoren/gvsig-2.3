/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi.memory;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.IteratorIndexTooBigException;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureProviderIterator;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureSetProvider;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProvider;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureSetProvider;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.exception.BaseException;

/**
 * Implementation of {@link FeatureSetProvider} used in
 * {@link AbstractMemoryStoreProvider}
 *
 */
public class MemoryFeatureSet extends AbstractFeatureSetProvider {

	protected List data;

	public MemoryFeatureSet(AbstractFeatureStoreProvider store,
			FeatureQuery query, FeatureType featureType, List data) {
		super(store, query,
		/*
		 * Comprobar si los attributos solicitados son distintos a los
		 * originales.
		 */
		featureType.equals(store.getStoreServices()
				.getProviderFeatureType(featureType.getId())) ? null
				: featureType);
		this.data = data;
		/*
		 * Nota: Como no sabe filtrar ni ordenar ignora el query.
		 */
	}

	public boolean canFilter() {
		return false;
	}

	public boolean canOrder() {
		return false;
	}

	public boolean canIterateFromIndex() {
		return true;
	}

	public long getSize() throws DataException {
		return data.size();
	}

	public boolean isEmpty() throws DataException {
		return data.isEmpty();
	}

	protected void doDispose() throws BaseException {
		//data only has to be deleted when the store.dispose method is called
		//data.clear();
	}

	protected AbstractFeatureProviderIterator createFastIterator(long index)
			throws DataException {
		return createIterator(index);
	}

	protected AbstractFeatureProviderIterator createIterator(long index)
			throws DataException {
		if (index > 0 && index >= data.size()) {
			throw new IteratorIndexTooBigException(index, data.size());
		}
		Iterator iter =
				index <= 0 ? data.iterator() : data.listIterator((int) index);
		if (getFeatureType() == null) {
			return new DelegatedDisposableIterator(getStore(), iter);
		} else {
			return new DelegatedDisposableWrappedIterator(getStore(), iter,
					getFeatureType());
		}
	}

	protected class DelegatedDisposableIterator extends
			AbstractFeatureProviderIterator {
		private Iterator delegated;

		public DelegatedDisposableIterator(AbstractFeatureStoreProvider store,
				Iterator it) {
			super(store);
			this.delegated = it;
		}

		protected void internalDispose() {
			this.delegated = null;
		}

		protected boolean internalHasNext() {
			return delegated.hasNext();
		}

		protected Object internalNext() {
			return delegated.next();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		protected void doDispose() throws BaseException {
			if (delegated instanceof Disposable) {
				((Disposable) delegated).dispose();
			}
		}
	}

	protected class DelegatedDisposableWrappedIterator extends
			DelegatedDisposableIterator {
		private FeatureType fType;

		public DelegatedDisposableWrappedIterator(
				AbstractFeatureStoreProvider store, Iterator it,
				FeatureType featureType) {
			super(store, it);
			this.fType = featureType;
		}

		protected Object internalNext() {
			DefaultFeatureProvider feature =
					(DefaultFeatureProvider) super.internalNext();
			if (fType == null) {
				return feature;
			} else {
				return new MemoryFeatureProviderAttributeMapper(feature,
						this.fType);
			}

		}
	}
}
