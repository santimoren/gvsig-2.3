/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
 
package org.gvsig.fmap.dal.feature.spi.cache;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.service.spi.ProviderFactory;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public interface FeatureCacheProviderFactory extends ProviderFactory{

	  /**
	   * Returns the name of the providers created by this factory.
	   * 
	   * @return the provider name
	   */
	  String getName();

	  /**
	   * Creates a new CacheProvider.
	   * 
	   * @param parameters
	   *            for the CacheProvider
	   * @return the new CacheProvider
	   * @throws DataException
	   *             if the parameters are not valid or there is an error creating
	   *             the CacheProvider
	   */
	  public FeatureCacheProvider createCacheProvider(DynObject parameters) throws DataException;

	  /**
	   * Creates the parameters for the {@link Provider} created by this factory.
	   * 
	   * @return the provider parameters
	   */
	  DynObject createParameters();

	  /**
	   * Initialices the factory.
	   */
	  void initialize();

	
}

