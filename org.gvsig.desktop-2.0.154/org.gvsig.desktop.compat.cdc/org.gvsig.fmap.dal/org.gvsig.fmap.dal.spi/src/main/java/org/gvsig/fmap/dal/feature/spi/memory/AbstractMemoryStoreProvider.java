/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi.memory;

import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProvider;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.FeatureSetProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.exception.BaseException;

/**
 * Abstract implementation for {@link FeatureStoreProvider} for provider that
 * are loaded statically in memory
 * 
 */
public abstract class AbstractMemoryStoreProvider extends
		AbstractFeatureStoreProvider {

	protected List data;

	protected AbstractMemoryStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices, DynObject metadata) {
		super(params, storeServices, metadata);
	}

	protected AbstractMemoryStoreProvider(DataStoreParameters params,
			DataStoreProviderServices storeServices) {
		super(params, storeServices);
	}

	public void performChanges(Iterator deleteds, Iterator inserteds, Iterator updateds, Iterator originalFeatureTypesUpdated) throws PerformEditingException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Load a {@link FeatureProvider} into memory store.<br>
	 * Use this when loading data.
	 * 
	 * @param data
	 */
	public void addFeatureProvider(FeatureProvider data) {
		data.setOID(this.createNewOID());
		this.data.add(data);
	}

	/**
	 * Return the current size of the memory store.
	 * 
	 * @return
	 * @throws DataException
	 */
	public long getDataSize() throws DataException {
		this.open();
		return this.data.size();
	}

	protected FeatureProvider internalGetFeatureProviderByReference(
			FeatureReferenceProviderServices reference) throws DataException {
		int oid = ((Long) reference.getOID()).intValue();
		return (FeatureProvider) this.data.get(oid);
	}

	protected FeatureProvider internalGetFeatureProviderByReference(
			FeatureReferenceProviderServices reference, FeatureType featureType)
			throws DataException {
        return new MemoryFeatureProviderAttributeMapper(
            (DefaultFeatureProvider) internalGetFeatureProviderByReference(reference),
            featureType);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#createSet(org.gvsig.fmap.dal.feature.FeatureQuery, org.gvsig.fmap.dal.feature.FeatureType)
	 */
	public FeatureSetProvider createSet(FeatureQuery query, FeatureType featureType)
			throws DataException {
		this.open();
		return new MemoryFeatureSet(this, query, featureType, this.data);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProvider#createFeatureProvider(org.gvsig.fmap.dal.feature.FeatureType)
	 */
	public FeatureProvider createFeatureProvider(FeatureType featureType)throws DataException  {
		this.open();
		return new DefaultFeatureProvider(featureType);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureStoreProvider#getFeatureCount()
	 */
	public long getFeatureCount() throws DataException {
		return data.size();
	}

	protected AbstractMemoryStoreProvider getMemoryProvider() {
		return this;
	}

	protected void doDispose() throws BaseException {
		super.doDispose();
		data = null;
	}
}
