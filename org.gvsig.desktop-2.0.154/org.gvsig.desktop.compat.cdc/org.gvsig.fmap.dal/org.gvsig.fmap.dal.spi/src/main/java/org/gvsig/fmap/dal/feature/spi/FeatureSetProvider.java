/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dispose.DisposableIterator;

/**
 * Interface for set of feature based data providers
 *
 *
 * @author jmvivo
 *
 */
public interface FeatureSetProvider extends Disposable {

	boolean canFilter();

	boolean canOrder();

	boolean canIterateFromIndex();

	long getSize() throws DataException;

	boolean isEmpty() throws DataException;

    /**
     * Returns an iterator over the elements in this set, in the order
     * (if any) defined when the collection was obtained.
     * 
     * <p>
     * Fast in this case means that each of the elements returned may be a
     * reused or pooled object instance, so don't use it to be stored in any
     * way.
     * </p>
     * <p>
     * If you need to store one of the {@link FeatureProvider} of the iterator,
     * use the {@link FeatureProvider#getCopy()} to create a clone of the
     * object.
     * </p>
     * 
     * @return an iterator of the elements in this collection (in proper
     *         sequence).
     * 
     * @throws DataException
     *             if there is an error getting the iterator
     * 
     * @deprecated use {@link #fastIterator()} instead
     */
	DisposableIterator iterator() throws DataException;

    /**
     * Returns an iterator over the elements in this set, in the order
     * (if any) defined when the collection was obtained.
     * 
     * <p>
     * Fast in this case means that each of the elements returned may be a
     * reused or pooled object instance, so don't use it to be stored in any
     * way.
     * </p>
     * <p>
     * If you need to store one of the {@link FeatureProvider} of the iterator,
     * use the {@link FeatureProvider#getCopy()} to create a clone of the
     * object.
     * </p>
     * 
     * @param index
     *            index of first element to be returned from the iterator (by a
     *            call to the <tt>next</tt> method).
     * @return an iterator of the elements in this collection (in proper
     *         sequence).
     * 
     * @throws DataException
     *             if there is an error getting the iterator
     * 
     * @deprecated use {@link #fastIterator()} instead
     */
	DisposableIterator iterator(long index) throws DataException;

    /**
     * Returns an iterator over the elements in this set, in the order
     * (if any) defined when the collection was obtained.
     * 
     * @see #fastIterator()
     * @see #fastIterator(long)
     * 
     * @return an iterator of the elements in this collection (in proper
     *         sequence).
     * 
     * @throws DataException
     *             if there is an error getting the iterator
     */
    DisposableIterator fastIterator() throws DataException;

    /**
     * Returns an iterator over the elements in this set, in the order
     * (if any) defined when the collection was obtained.
     * 
     * @see #fastIterator()
     * @see #fastIterator(long)
     * 
     * @param index
     *            index of first element to be returned from the iterator (by a
     *            call to the <tt>next</tt> method).
     * @return an iterator of the elements in this collection (in proper
     *         sequence).
     * 
     * @throws DataException
     *             if there is an error getting the iterator
     */
	DisposableIterator fastIterator(long index) throws DataException;

}
