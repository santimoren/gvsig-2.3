/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.PrepareResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyChangesException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyCloseException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyDisposeException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyOpenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource implementation which is able to show an unique interface to a group
 * of Resources.
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiResource extends AbstractResource {

	public static final String TYPE_NAME = "multi";

	public static final String DESCRIPTION = "Group of multiple resources";

	private static final Logger LOG =
			LoggerFactory.getLogger(MultiResource.class);

	private Map resources = new HashMap<>();

	// Initially, the first one
	private int defaultResourcePos = 0;

	//Blocks adding methods
	private boolean isFrozen=false;

	/**
	 * Creates a new {@link MultiResource} from the given parameters.
	 *
	 * @param parameters
	 *            to create the resource from
	 * @throws InitializeException
	 *             if there is an error creating the resource
	 */
	public MultiResource(MultiResourceParameters parameters)
			throws InitializeException {
		super(parameters);
	}

	private List getResources() {
		return new ArrayList<>(resources.values());
	}

	/**
	 * Adds a new {@link ResourceProvider} to the list of Resources managed by
	 * this group.
	 *
	 * @param parameters
	 *            to create the resource from
	 * @param defaultResource
	 *            if the added resource is to be used as the multi resource
	 *            default one
	 * @throws InitializeException
	 *             if there is an error adding the resource
	 */
	public void addResource(ResourceParameters parameters,
			boolean defaultResource) throws InitializeException {
	    String name=(String)parameters.getDynValue("name");
	    commonAddResource(name, parameters,defaultResource);
	}

	/**
	 * Adds a new {@link ResourceProvider} to the list of Resources managed by
	 * this group.
	 *
	 * @param name
	 *            of the resource to add
	 * @param params
	 *            parameters to create the resource with
	 * @param defaultResource
	 *            if the added resource is to be used as the multi resource
	 *            default one
	 * @throws InitializeException
	 *             if there is an error adding the resource
	 */
	public void addResource(String name, Object[] params,
			boolean defaultResource) throws InitializeException {
	    commonAddResource(name, params,defaultResource);
	}

	private void commonAddResource(String name, Object params,
        boolean defaultResource) throws InitializeException {
	        String multiresourceName=null;
	        try {
	            multiresourceName=this.getName();
	        } catch (AccessResourceException e1) {
	            LOG.warn("Imposible to get MultiResource name");
	        }
	        Resource res = (Resource)(this.resources.get(name));
	        if( res!=null ) {
	            return;
	        }
	        if (isFrozen){
	            LOG.warn("Added resource "+name+" to a frozen multiresource "+multiresourceName);
	        }
	        ResourceProvider resourceProvider =null;
	        if (params instanceof ResourceParameters){
	            resourceProvider =  ((ResourceManagerProviderServices) DALLocator.
	                getResourceManager()).createAddResource((ResourceParameters)params);
	        }else if (params instanceof Object[]){
	            resourceProvider =  ((ResourceManagerProviderServices) DALLocator.
                    getResourceManager()).createAddResource(name,(Object[])params);
	        }else{
	            LOG.warn("Resource could not be created "+name);
	        }
            try {
                resources.put(resourceProvider.getName(),resourceProvider);
            } catch (AccessResourceException e) {
                LOG.warn("Resource not added "+name+ " to "+multiresourceName);
            }
	        if (defaultResource) {
	            defaultResourcePos = resources.size() - 1;
	        }
	}




	public boolean isThis(ResourceParameters parameters)
			throws ResourceException {
		if (parameters instanceof MultiResourceParameters) {
			MultiResourceParameters multiParams =
					(MultiResourceParameters) parameters;
			return multiParams.getName() != null
					&& multiParams.getName().equals(
							getMultiResourceParameters().getName());
		}

		return false;
	}

	public void notifyChanges() throws ResourceNotifyChangesException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).notifyChanges();
		}
		super.notifyChanges();
	}

	public void notifyClose() throws ResourceNotifyCloseException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).notifyClose();
		}
		super.notifyClose();
	}

	public void notifyDispose() throws ResourceNotifyDisposeException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).notifyDispose();
		}
		super.notifyDispose();
	}

	public void notifyOpen() throws ResourceNotifyOpenException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).notifyOpen();
		}
		super.notifyOpen();
	}

	public void prepare() throws PrepareResourceException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).prepare();
		}
		super.prepare();
	}

	public void closeRequest() throws ResourceException {
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			((ResourceProvider) getResources().get(i)).closeRequest();
		}
		super.closeRequest();
	}

	public Object get() throws AccessResourceException {
		ResourceProvider defaultResource = getDefaultResource();
		if (defaultResource == null) {
			return null;
		} else {
			return defaultResource.get();
		}
	}

	public Object getData() {
		ResourceProvider defaultResource = getDefaultResource();
		if (defaultResource == null) {
			return null;
		} else {
			return defaultResource.getData();
		}
	}

	private MultiResourceParameters getMultiResourceParameters() {
		return (MultiResourceParameters) getParameters();
	}

	public String getName() throws AccessResourceException {
	    String name ="MultiResource "+ getMultiResourceParameters().getName();
	    return name;
	    /*
		StringBuffer buffer =
				new StringBuffer().append("MultiResource ").append(
						getMultiResourceParameters().getName()).append(":: ");
		List resources = getResources();
		for (int i = 0; i < resources.size(); i++) {
			buffer.append(((ResourceProvider) resources.get(i)).getName());
			if (defaultResourcePos == i) {
				buffer.append(" (default)");
			}
			if (i < resources.size() - 1) {
				buffer.append(", ");
			}
		}
		return buffer.toString();
		*/
	}

	public String toString() {
		try {
			return getName();
		} catch (AccessResourceException e) {
			LOG.error("Error getting the resource name", e);
		}
		return super.toString();
	}

	private ResourceProvider getDefaultResource() {
		if (getResources().size() > 0) {
			return (ResourceProvider) getResources().get(defaultResourcePos);
		} else {
			return null;
		}
	}

	public void setData(Object data) {
		ResourceProvider defaultResource = getDefaultResource();
		if (defaultResource != null) {
			defaultResource.setData(data);
		}
	}

	/**
	 * Blocks the adding methods. It is called when all their resources components are added.
	 */
	public void frozen(){
	    this.isFrozen=true;
	}

    /**
     * Locks the multiresource components to avoid another concurrent modification
     */
   @Override
   public Object execute(ResourceAction action)
       throws ResourceExecuteException {
       Object value = null;
       synchronized (lock) {
           Iterator it=resources.values().iterator();
           while (it.hasNext()){
               AbstractResource resource=(AbstractResource)it.next();
               resource.multiResourcelock=this.lock;
           }
           executeBegins();
           try {
               value = performExecution(action);
           } catch (Exception e) {
               throw new ResourceExecuteException(this, e);
           } finally {
               executeEnds();
           }
       }
       return value;
   }

   public  void addMultiResourceConsumer(ResourceConsumer consumer) {
       super.addConsumer(consumer);
       Iterator it=resources.values().iterator();
       while (it.hasNext()){
           AbstractResource resource=(AbstractResource)it.next();
           resource.addConsumer(consumer);
       }
   }

}
