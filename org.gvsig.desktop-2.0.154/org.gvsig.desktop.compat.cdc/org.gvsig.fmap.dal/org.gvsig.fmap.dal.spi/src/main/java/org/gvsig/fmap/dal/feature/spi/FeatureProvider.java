/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi;

import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;


/**
 * Inteface for feature of feature based data provider
 *
 * @author jmvivo
 *
 */
public interface FeatureProvider {

        /**
         * Indicates whether the attributo must be assigned by the provider.
         * Return true if can be assigned, otherwise return false.
         * 
         * @param i
         *      index of the attribute 
         * @return True if the attribute i can't be set
         */
        public boolean isReadOnly(int i);
        
	public void set(int i, Object value);

	public Object get(int i);

	public void set(String name, Object value);

	public Object get(String name);

	public void setOID(Object oid);

	public Object getOID();

	public FeatureType getType();

	public FeatureProvider getCopy();

	public Envelope getDefaultEnvelope();

	public Geometry getDefaultGeometry();

	public void setDefaultEnvelope(Envelope extent);

	public void setDefaultGeometry(Geometry geom);

	public boolean isNull(int i);

	public boolean isNull(String name);

	public boolean isNew();

	public void setNew(boolean isNew);

}
