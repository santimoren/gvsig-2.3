/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.spi;

import java.util.Collection;
import java.util.Iterator;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.timesupport.Interval;
import org.gvsig.timesupport.Time;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dynobject.DynObject;

/**
 * Base interface for all data providers
 * 
 * @author jmvivo
 *
 */
public interface DataStoreProvider extends org.gvsig.fmap.dal.DataStoreProvider, DynObject, Disposable {

	/**
	 * Retruns an {@link java.util.Iterator} of SubStores from this store. it
	 * this hasn't SubStores returns <code>null</code>.
	 *
	 * @return SubStores iterator
	 */
	public abstract Iterator getChilds();

	/**
	 * Create a {@link DataServerExplorer} from the same source that this store.
	 *
	 * @return ServerExplorer
	 * @throws ReadException
	 * @throws ValidateDataParametersException
	 */
	public abstract DataServerExplorer getExplorer() throws ReadException,
			ValidateDataParametersException;

	/**
	 * Open store. You must call it before do anything whith store.<br>
	 * This method can be called repeatly.
	 *
	 * @throws OpenException
	 */
	public abstract void open() throws OpenException;

	/**
	 * Request to close de source
	 *
	 * @throws CloseException
	 */
	public abstract void close() throws CloseException;

	/**
	 * Returns the {@link Resource} from where the data is being loaded.
	 * 
	 * @return the data {@link Resource}
	 */
	ResourceProvider getResource();

	/**
	 * Force to reload information of Store
	 *
	 * @throws OpenException
	 * @throws InitializeException
	 */
	public abstract void refresh() throws OpenException, InitializeException;

	/**
	 * Returns the unique identifier of the Store
	 *
	 * FIXME add examples
	 *
	 * @return
	 */
	public abstract Object getSourceId();

	/**
	 * Return the name of the provider.
	 * Examples: dbf, shp, jdbc, postgresql, wfs, gml
	 * 
	 * @return
	 */
	public String getProviderName();

	/**
	 * Returns the name associated to the provider.
	 * This name is provided for informational purposes only.
	 * Explamples:
	 * 
	 * In a dbf the filename without the path
	 * 
	 * In a DDBB table the name of the table
	 * 
	 * In a WFS layer the name of the layer.
	 *
	 * @return String containing this store's name.
	 */
	public String getName();
	
	/**
	 * Returns a more descriptive name for the provider that getName.
	 * This name is provided for informational purposes only.
	 * Explamples:
	 * 
	 * In a file based store may return the full name of the filename, path and filename.
	 * 
	 * In a data base based store may return "server:dbname:tablename"
	 * 
	 * In a WFS layer based store may return "server:layername"
	 * 
	 * @return String Containing the full name of the store
	 */
	public String getFullName();
	
	/**
     * Gets the {@link Interval} of the store, that means the temporal
     * interval where the store has valid data.
     * @return
     *         a time interval or null if there is not time support
     */
    public Interval getInterval();
    
    /**
     * Gets all the possible values of time for which the store has data.  
     * @return
     *         a collection of {@link Time} objects.
     */
    public Collection getTimes();
    
    /**
     * Gets all the possible values of time for which the store has data
     * and intersects with an interval.
     * @param interval
     *         the interval of time
     * @return
     *         a collection of {@link Time} objects.
     */
    public Collection getTimes(Interval interval);
	
}