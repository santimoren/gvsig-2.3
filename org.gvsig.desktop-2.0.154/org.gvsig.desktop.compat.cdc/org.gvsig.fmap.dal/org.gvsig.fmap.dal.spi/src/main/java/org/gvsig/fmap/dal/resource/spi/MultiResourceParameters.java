/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import org.gvsig.fmap.dal.feature.spi.memory.MemoryResource;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectEncoder;
import org.gvsig.tools.dynobject.DynObjectManager;

/**
 * Parameters for the creation of a multi resource.
 *
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class MultiResourceParameters extends AbstractResourceParameters {

    public static final String DYNCLASS_NAME = "MultiResourceParameters";

    private static final String FIELD_NAME = "name";

    private DelegatedDynObject delegatedDynObject;

    /**
     * Creates a new {@link MultiResourceParameters}.
     */
    public MultiResourceParameters() {
        this.delegatedDynObject
                = (DelegatedDynObject) ToolsLocator.getDynObjectManager()
                .createDynObject(registerDynClass());
    }

    /**
     * Creates a new {@link MultiResourceParameters}.
     *
     * @param name for the parameters. Will be used as the name of the
     * {@link MemoryResource} created with this parametres.
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public MultiResourceParameters(String name) {
        this();
        setName(name);
    }

    @Override
    protected DelegatedDynObject getDelegatedDynObject() {
        return delegatedDynObject;
    }

    @Override
    public String getTypeName() {
        return MultiResource.TYPE_NAME;
    }

    public String getName() {
        return (String) getDelegatedDynObject().getDynValue(FIELD_NAME);
    }

    public void setName(String name) {
        getDelegatedDynObject().setDynValue(FIELD_NAME, name);
    }

    @Override
    public String getResurceID() {
        DynObjectEncoder encoder = ToolsLocator.getDynObjectManager().createSimpleDynObjectEncoder();
        return encoder.encodePair("name",this.getName());
    }

    /**
     * Registers the {@link DynClass} of this parameters attributes.
     *
     * @return the dyn class of this parameters
     */
    private static synchronized DynClass registerDynClass() {
        DynObjectManager dynman = ToolsLocator.getDynObjectManager();
        DynClass dynClass = dynman.get(DYNCLASS_NAME);
        if (dynClass == null) {
            dynClass = dynman.add(DYNCLASS_NAME);
            dynClass.addDynFieldString(FIELD_NAME).setDescription(
                    "The name of the multi resource");
        }
        return dynClass;
    }
}
