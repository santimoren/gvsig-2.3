
package org.gvsig.fmap.dal.spi;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.tools.locator.LocatorException;


public class DALSPILocator {
    
    public static DataManagerProviderServices getDataManagerProviderServices() throws LocatorException {
        return (DataManagerProviderServices) DALLocator.getDataManager();
    }
    
    public static ResourceManagerProviderServices getResourceManagerProviderServices() throws LocatorException {
        return (ResourceManagerProviderServices) DALLocator.getResourceManager();
    }
}
