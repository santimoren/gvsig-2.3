/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.spi;

import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;

/**
 * Abstract base implementation for data iterators.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractDataProviderIterator extends AbstractDisposable
		implements DisposableIterator {

	private final DataStoreProvider dataStoreProvider;

	private final ResourceAction nextResourceAction = new ResourceAction() {
		public Object run() throws Exception {
			dataStoreProvider.open();
			return internalNext();
		}
	};

	private final ResourceAction hasNextResourceAction = new ResourceAction() {
		public Object run() throws Exception {
			boolean value = internalHasNext();
			return value ? Boolean.TRUE : Boolean.FALSE;
		}
	};

	/**
	 * Creates a new iterator instance.
	 * 
	 * @param storeProvider
	 *            to load the {@link FeatureProvider}s from
	 */
	public AbstractDataProviderIterator(DataStoreProvider dataStoreProvider) {
		this.dataStoreProvider = dataStoreProvider;
	}

    public Object next() {
		return getResource().execute(nextResourceAction);
	}

    public boolean hasNext() {
		Object hasNext = getResource().execute(hasNextResourceAction);
		return ((Boolean) hasNext).booleanValue();
	}

	/**
	 * Returns the {@link Resource} from where the data is going to be loaded.
	 * 
	 * @return the {@link Resource} from where the data is going to be loaded
	 */
	protected final Resource getResource() {
		return getDataStoreProvider().getResource();
	}

	/**
	 * Returns the {@link DataStoreProvider} to load the Iterator data.
	 * 
	 * @return the {@link DataStoreProvider}
	 */
	protected DataStoreProvider getDataStoreProvider() {
		return dataStoreProvider;
	}

	/**
	 * Returns the next iterator element. It is the child classes implementation
	 * for the {@link #next()} method.
	 * 
	 * @return the next element
	 */
	protected abstract Object internalNext();

	/**
	 * Returns if there are more elements to get from the iterator. It is the
	 * child classes implementation for the {@link #hasNext()} method.
	 * 
	 * @return if there are more elements
	 */
	protected abstract boolean internalHasNext();
}
