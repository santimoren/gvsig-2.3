/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi;

import java.util.Iterator;
import java.util.List;

/**
 * Default implementation for {@link LongList}<br>
 * <br>
 *
 * <strong>Note:</strong> This class is based on {@link java.util.List}
 */
public class DefaultLongList implements LongList {

	private List list = null;

	public DefaultLongList(List list) {
		this.list = list;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.LongList#getSize()
	 */
	public long getSize() {
		return list.size();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.LongList#isEmpty()
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.LongList#iterator()
	 */
	public Iterator iterator() {
		return list.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.LongList#iterator(long)
	 */
	public Iterator iterator(long index) {
		if (index > Integer.MAX_VALUE || index < Integer.MIN_VALUE) {
			throw new IndexOutOfBoundsException();
		}
		return list.listIterator((int) index);
	}
}

