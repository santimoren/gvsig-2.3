/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.ResourceParameters;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;

/**
 * <p>
 * Abstract Implementation for Resource that allows the concurrent access.
 * </p>
 * 
 * @author jmvivo
 * 
 */
public abstract class AbstractNonBlockingResource extends AbstractResource {

	/**
	 * @param parameters
	 * @throws InitializeException
	 */
	public AbstractNonBlockingResource(ResourceParameters parameters)
			throws InitializeException {
		super(parameters);
	}

	public Object execute(ResourceAction action)
			throws ResourceExecuteException {
		Object value = null;
		synchronized (lock) {
			executeBegins();
		}
		try {
			value = performExecution(action);
		} catch (Exception e) {
			throw new ResourceExecuteException(this, e);
		} finally {
			synchronized (lock) {
				executeEnds();
			}
		}
		return value;
	}
}
