/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.spi;

import java.util.List;

import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.resource.Resource;
import org.gvsig.fmap.dal.resource.ResourceManager;
import org.gvsig.fmap.dal.resource.ResourceParameters;

public interface ResourceManagerProviderServices extends ResourceManager {

	public boolean register(String type, String description, Class handler,
			Class params);

	public DataParameters createParameters(String type)
			throws InitializeException;

	public ResourceProvider createAddResource(ResourceParameters params)
			throws InitializeException;

	public ResourceProvider createResource(ResourceParameters params)
			throws InitializeException;

	public ResourceProvider createAddResource(String type, Object[] params)
			throws InitializeException;

	public ResourceProvider createResource(String type, Object[] params)
			throws InitializeException;

	public void remove(Resource resource) throws DataException;

	public void remove(String name) throws DataException;
	
	public List getResourceProviders();

}
