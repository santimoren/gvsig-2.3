/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi;

import java.util.Iterator;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;

/**
 * Base implementation for {@link FeatureSetProvider}s, adding some utility
 * methods.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public abstract class AbstractFeatureSetProvider extends AbstractDisposable
		implements FeatureSetProvider {

	private final AbstractFeatureStoreProvider store;
	private final FeatureQuery query;
	private final FeatureType featureType;

	/**
	 * Creates a new {@link FeatureSetProvider}.
	 * 
	 * @param store
	 *            the underlying {@link FeatureStoreProvider} to get the data
	 *            from
	 * @param query
	 *            used to create the {@link FeatureSetProvider}
	 * @param featureType
	 *            the type of feature to get
	 */
	public AbstractFeatureSetProvider(AbstractFeatureStoreProvider store,
			FeatureQuery query, FeatureType featureType) {
		this.store = store;
		this.query = query;
		this.featureType = featureType;
	}

	/**
	 * Return the {@link AbstractFeatureStoreProvider}.
	 * 
	 * @return the store
	 */
	protected AbstractFeatureStoreProvider getStore() {
		return store;
	}

	/**
	 * Returns the {@link FeatureQuery} used to create this set.
	 * 
	 * @return the query
	 */
	protected FeatureQuery getQuery() {
		return query;
	}

	/**
	 * Returns the type of features to load.
	 * 
	 * @return the featureType
	 */
	protected FeatureType getFeatureType() {
		return featureType;
	}

	public final DisposableIterator fastIterator() throws DataException {
		return fastIterator(0);
	}

	public final DisposableIterator fastIterator(long index)
			throws DataException {
		return createFastIterator(index);
	}

	public final DisposableIterator iterator() throws DataException {
		return iterator(0);
	}

	public final DisposableIterator iterator(long index) throws DataException {
		return createIterator(index);
	}

	/**
	 * Creates a new {@link Iterator}, begginning at the specified data index.
	 * 
	 * @param index
	 *            the first element position to be returned by the
	 *            {@link Iterator}
	 * @return a new {@link Iterator}
	 * @throws DataException
	 *             if there is an error creating the {@link Iterator}
	 */
	protected abstract AbstractFeatureProviderIterator createIterator(long index)
			throws DataException;

	/**
	 * Creates a new fast {@link Iterator}, begginning at the specified data
	 * index. By fast this means the object instances of data (
	 * {@link FeatureProvider}) may be reused between the
	 * {@link Iterator#next()} method invocations.
	 * 
	 * @param index
	 *            the first element position to be returned by the
	 *            {@link Iterator}
	 * @return a new {@link Iterator}
	 * @throws DataException
	 *             if there is an error creating the {@link Iterator}
	 */
	protected abstract AbstractFeatureProviderIterator createFastIterator(
			long index) throws DataException;
}
