/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.feature.spi.memory;

import java.util.Iterator;

import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;

/**
 * Envuelve un FeatureProvider en memoria para que muestre un FeautureType
 * distinto del orginal sin modificarlo
 *
 *
 * Wrap a FeatureProvider stored in memory to display a FeautureType other than
 * the original one without change it
 *
 * @author jmvivo
 *
 */
public class MemoryFeatureProviderAttributeMapper implements FeatureProvider {
	private DefaultFeatureProvider original;
	private FeatureType myFeatureType;

	/**
	 * Default constructor.
	 * 
	 * @param original
	 *            featureProvider
	 * @param featureType
	 *            to expose
	 */
	public MemoryFeatureProviderAttributeMapper(DefaultFeatureProvider original,
			FeatureType featureType) {
		this.original = original;
		this.myFeatureType = featureType;

	}

        public boolean isReadOnly(int i) {
            FeatureAttributeDescriptor attribute = myFeatureType.getAttributeDescriptor(i);
            if (attribute.getEvaluator() == null) {
                    return true;
            }
            return false;
        }
        
        /*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#get(int)
	 */
	public Object get(int i) {
		return original.get(myFeatureType.getAttributeDescriptor(i).getName());
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#get(java.lang.String)
	 */
	public Object get(String name) {
		return original.get(name);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getCopy()
	 */
	public FeatureProvider getCopy() {
		DefaultFeatureProvider data = new DefaultFeatureProvider(myFeatureType);
		data.setOID(this.original.getOID());
		Iterator iter = myFeatureType.iterator();
		FeatureAttributeDescriptor attr;
		while (iter.hasNext()) {
			attr = (FeatureAttributeDescriptor) iter.next();
			data.set(attr.getIndex(), this.original.get(attr.getName()));
		}
		data.setNew(this.original.isNew());
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getDefaultEnvelope()
	 */
	public Envelope getDefaultEnvelope() {
		return original.getDefaultEnvelope();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getDefaultGeometry()
	 */
	public Geometry getDefaultGeometry() {
		return original.getDefaultGeometry();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getOID()
	 */
	public Object getOID() {
		return original.getOID();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getType()
	 */
	public FeatureType getType() {
		return myFeatureType;
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNew()
	 */
	public boolean isNew() {
		return original.isNew();
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNull(int)
	 */
	public boolean isNull(int i) {
		return original.isNull(myFeatureType.getAttributeDescriptor(i)
				.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNull(java.lang.String)
	 */
	public boolean isNull(String name) {
		return original.isNull(name);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#set(int, java.lang.Object)
	 */
	public void set(int i, Object value) {
		original.set(myFeatureType.getAttributeDescriptor(i).getName(),
				value);


	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#set(java.lang.String, java.lang.Object)
	 */
	public void set(String name, Object value) {
		original.set(name, value);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#setDefaultEnvelope(org.gvsig.fmap.geom.primitive.Envelope)
	 */
	public void setDefaultEnvelope(Envelope extent) {
		original.setDefaultEnvelope(extent);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#setDefaultGeometry(org.gvsig.fmap.geom.Geometry)
	 */
	public void setDefaultGeometry(Geometry geom) {
		original.setDefaultGeometry(geom);

	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#setNew(boolean)
	 */
	public void setNew(boolean isNew) {
		original.setNew(isNew);
	}

	/*
	 * (non-Javadoc)
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#setOID(java.lang.Object)
	 */
	public void setOID(Object oid) {
		original.setOID(oid);
	}

}
