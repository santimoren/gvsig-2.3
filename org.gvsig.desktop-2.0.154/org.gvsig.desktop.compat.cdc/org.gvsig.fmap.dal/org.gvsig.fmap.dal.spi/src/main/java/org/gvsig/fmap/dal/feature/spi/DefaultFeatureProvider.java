/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.feature.spi;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.SetReadOnlyAttributeException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;

/**
 * Default implementation for {@link FeatureProvider}
 *
 */
public class DefaultFeatureProvider implements FeatureProvider {

	protected FeatureType featureType;
	protected boolean[] nulls;
	protected Object[] values;
	protected Geometry defaultGeometry;
	protected Envelope envelope;
	private Object oid;
	private boolean isNew=false;

	public DefaultFeatureProvider(FeatureType type) {
		if (type instanceof EditableFeatureType) {
			throw new IllegalArgumentException("type can't by editable.");
		}
		this.featureType = type;
		this.values = new Object[featureType.size()];
		this.nulls = new boolean[featureType.size()];

		this.envelope = null;
		this.defaultGeometry = null;
		this.oid = null;
	}

	public DefaultFeatureProvider(FeatureType type, Object oid) {
		this(type);
		this.oid = oid;
	}

        public boolean isReadOnly(int i) {
            FeatureAttributeDescriptor attribute = featureType.getAttributeDescriptor(i);
            if (attribute.getEvaluator() != null) {
                    return true;
            }
            return false;
        }
        
	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#set(int,
	 * java.lang.Object)
	 */
	public void set(int i, Object value) {
                FeatureAttributeDescriptor attribute = featureType.getAttributeDescriptor(i);
                if( this.isReadOnly(i) ) {
			return;
		}
		if (featureType.getDefaultGeometryAttributeIndex() == i) {
			defaultGeometry = (Geometry) value;
			envelope = null;
		}
		if (value == null) {
			nulls[i] = true;
			values[i] = attribute.getDefaultValue();
		} else {
			values[i] = value;
			nulls[i] = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#set(java.lang.String,
	 * java.lang.Object)
	 */
	public void set(String name, Object value) {
		set(featureType.getIndex(name), value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#get(int)
	 */
	public Object get(int i) {
		return values[i];
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#get(java.lang.String)
	 */
	public Object get(String name) {
		FeatureAttributeDescriptor featureAttributeDescriptor = featureType.getAttributeDescriptor(name);	
		return values[featureAttributeDescriptor.getIndex()];
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getType()
	 */
	public FeatureType getType() {
		return featureType;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getCopy()
	 */
	public FeatureProvider getCopy() {
		DefaultFeatureProvider data = new DefaultFeatureProvider(
				this.getType());
		return getCopy(data);
	}

	/**
	 * Copy values from current instance to <code>data</code>
	 */
	protected FeatureProvider getCopy(DefaultFeatureProvider data) {
		data.oid = this.oid;
		System.arraycopy(this.values, 0, data.values, 0, this.values.length);
		data.defaultGeometry = this.defaultGeometry;
		data.envelope = this.envelope;
		data.isNew=this.isNew;
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getDefaultEnvelope()
	 */
	public Envelope getDefaultEnvelope() {
		if (envelope == null && getDefaultGeometry() != null) {
			envelope = getDefaultGeometry().getEnvelope();
		}
		return envelope;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getDefaultGeometry()
	 */
	public Geometry getDefaultGeometry() {
            return this.defaultGeometry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureProvider#setDefaultEnvelope(org
	 * .gvsig.fmap.geom.primitive.Envelope)
	 */
	public void setDefaultEnvelope(Envelope envelope) {
		this.envelope = envelope;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureProvider#setDefaultGeometry(org
	 * .gvsig.fmap.geom.Geometry)
	 */
	public void setDefaultGeometry(Geometry geom) {
		int i = featureType.getDefaultGeometryAttributeIndex();
                this.set(i, geom);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNull(int)
	 */
	public boolean isNull(int i) {
		return nulls[i];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNull(java.lang.String)
	 */
	public boolean isNull(String name) {
		int i = featureType.getIndex(name);
		return isNull(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#getOID()
	 */
	public Object getOID() {
		return this.oid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gvsig.fmap.dal.feature.spi.FeatureProvider#setOID(java.lang.Object)
	 */
	public void setOID(Object oid) {
		this.oid = oid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#isNew()
	 */
	public boolean isNew(){
		return isNew;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.gvsig.fmap.dal.feature.spi.FeatureProvider#setNew(boolean)
	 */
	public void setNew(boolean isNew){
		this.isNew=isNew;
	}
}
