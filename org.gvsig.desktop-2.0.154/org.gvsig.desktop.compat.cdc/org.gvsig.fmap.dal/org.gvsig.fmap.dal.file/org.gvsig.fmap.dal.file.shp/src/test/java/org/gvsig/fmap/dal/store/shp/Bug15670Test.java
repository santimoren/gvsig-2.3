/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp;

import junit.framework.TestCase;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * Code to test bug gvsig-desktop#15670. This is based on the code provided by
 * the bug reporter (see ticket), thanks to him!!
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Bug15670Test extends TestCase {

    public void testBug15670() throws Exception {
        new DefaultLibrariesInitializer().fullInitialize();
        DataManager manager = DALLocator.getDataManager();
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        NewFeatureStoreParameters destParams =
                (NewFeatureStoreParameters) manager.createNewStoreParameters(
                    "FilesystemExplorer", "Shape");
        EditableFeatureType type = destParams.getDefaultFeatureType();
        GeometryType geometryType =
            geometryManager.getGeometryType(Geometry.TYPES.POINT,
                Geometry.SUBTYPES.GEOM2D);
        type.add("GEOMETRY", org.gvsig.fmap.geom.DataTypes.GEOMETRY)
            .setGeometryType(geometryType);
        type.setDefaultGeometryAttributeName("GEOMETRY");

        destParams.setDynValue("shpfile", "/tmp/mySHP.shp");
        destParams.setDynValue("dbffile", "/tmp/mySHP.dbf");
        destParams.setDynValue("shxfile", "/tmp/mySHP.shx");
        destParams.setDynValue("crs", "EPSG:23030");
        destParams.setDefaultFeatureType(type);

        manager.newStore("FilesystemExplorer", "Shape", destParams, true);
        FeatureStore store =
            (FeatureStore) manager.openStore("Shape", destParams);

        store.edit();
        EditableFeature feature = store.createNewFeature().getEditable();
        final Point geom =
            geometryManager.createPoint(10.0, 10.0, Geometry.SUBTYPES.GEOM2D);
        feature.setGeometry("GEOMETRY", geom);
        store.insert(feature);
        store.finishEditing();

        store.accept(new Visitor() {

            public void visit(Object obj) throws VisitCanceledException,
                BaseException {
                Feature feature = (Feature) obj;
                assertEquals(geom, feature.getGeometry("GEOMETRY"));
            }
        });
        store.dispose();
    }
}
