/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp;

import java.io.File;

import junit.framework.TestCase;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.library.impl.DefaultLibrariesInitializer;

/**
 * Code to test bug gvsig-desktop#15597. This is based on the code provided by
 * the bug reporter (see ticket), thanks to him!!
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class Bug15597Test extends TestCase {
    private static final String TEMP_PATH = System.getProperty("java.io.tmpdir") + File.separator;
    
    public void testBug15642() throws Exception {
        new DefaultLibrariesInitializer().fullInitialize();
        DataManager manager = DALLocator.getDataManager();
        GeometryManager geometryManager = GeometryLocator.getGeometryManager();

        //Create a store
        NewFeatureStoreParameters destParams =
            (NewFeatureStoreParameters) manager.createNewStoreParameters(
                "FilesystemExplorer", "Shape");

        //Create a feature type with two fields: "geom" and "field1"
        EditableFeatureType type = destParams.getDefaultFeatureType();
        GeometryType geometryType =
            geometryManager.getGeometryType(Geometry.TYPES.POINT,
                Geometry.SUBTYPES.GEOM2D);
        type.add("geom", org.gvsig.fmap.geom.DataTypes.GEOMETRY)
            .setGeometryType(geometryType);
        type.setDefaultGeometryAttributeName("geom");
        type.add("field1", DataTypes.STRING).setSize(2);
       
        destParams.setDynValue("shpfile", TEMP_PATH + "mySHP.shp");      
        destParams.setDynValue("crs", "EPSG:23030");
        destParams.setDefaultFeatureType(type);

        manager.newStore("FilesystemExplorer", "Shape", destParams, true);
        FeatureStore store =
            (FeatureStore) manager.openStore("Shape", destParams);

                
        //Edit the store and add a new Feature.
        store.edit();
             
        EditableFeature feature1 = store.createNewFeature().getEditable();
        feature1.setGeometry(type.getDefaultGeometryAttributeIndex(), 
            geometryManager.createPoint(0, 0, SUBTYPES.GEOM2D));
        feature1.set("field1", "Hi");                     
      
        store.insert(feature1);
        
        //Finish the edition
        store.finishEditing();

        //Edit the feature type and add a new field: "field2"
        store.edit();
       
        EditableFeatureType type2 = 
            store.getDefaultFeatureType().getEditable();        
        type2.add("field2", DataTypes.STRING).setSize(10);
        store.update(type2);    
        
        assertEquals(store.getDefaultFeatureType().getAttributeDescriptors().length, 3);
        
        //Add a second feature
        EditableFeature feature2 = store.createNewFeature().getEditable();
        feature2.setGeometry(type2.getDefaultGeometryAttributeIndex(),
            geometryManager.createPoint(1, 1, SUBTYPES.GEOM2D));
        feature2.set("field1", "Hi");    
        feature2.set("field2", "Max");   
       
        store.insert(feature2);
        
        //The edition is not finished. Check if all the features have the two fields
        FeatureSet featureSet = store.getFeatureSet();
        DisposableIterator it = featureSet.fastIterator();
        
        it.hasNext();
        Feature feature = (Feature)it.next();   
        assertNotNull(feature.getDefaultGeometry());
        assertEquals("Hi", feature.get("field1"));
        assertNull(feature.get("field2"));
        
        it.hasNext();
        feature = (Feature)it.next();   
        assertNotNull(feature.getDefaultGeometry());
        assertEquals("Hi", feature.get("field1"));
        assertEquals("Max", feature.get("field2"));        
        
        it.dispose();
        store.finishEditing();
        
        //Edit the feature type and remove the field: "field1"
        store.edit();
        
        EditableFeatureType type3 = 
            store.getDefaultFeatureType().getEditable();        
        type3.remove("field1");
        store.update(type3);    
        
        assertEquals(store.getDefaultFeatureType().getAttributeDescriptors().length, 2);
                
        //Finish the edition
        store.finishEditing();
        
        //Check if all the features have one field
        featureSet = store.getFeatureSet();
        it = featureSet.fastIterator();        
        
        it.hasNext();
        feature = (Feature)it.next();   
        assertNotNull(feature.getDefaultGeometry());       
        assertEquals("", feature.get("field2"));
        
        it.hasNext();
        feature = (Feature)it.next();   
        assertNotNull(feature.getDefaultGeometry());
        assertEquals("Max", feature.get("field2"));     
        
        it.dispose();
        store.dispose();
    }
}
