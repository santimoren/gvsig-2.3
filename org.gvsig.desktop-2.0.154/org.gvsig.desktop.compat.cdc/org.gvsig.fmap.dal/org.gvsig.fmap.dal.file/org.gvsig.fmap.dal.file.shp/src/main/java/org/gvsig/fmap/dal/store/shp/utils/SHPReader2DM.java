/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteOrder;

import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.store.shp.SHPStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.utils.bigfile.BigByteBuffer2;


/**
 * @author fdiaz
 *
 */
public class SHPReader2DM extends AbstractSHPReader {

    /**
     *
     */
    public SHPReader2DM(SHPStoreParameters params) {
        super(params);
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPReader#readPoint(org.gvsig.utils.bigfile.BigByteBuffer2)
     */
    public Geometry readPoint(BigByteBuffer2 bb) throws CreateGeometryException {
        // bytes 1 to 4 are the type and have already been read.
        // bytes 4 to 12 are the X coordinate
        // bytes 13 to 20 are the X coordinate
        GeometryManager gManager = GeometryLocator.getGeometryManager();
        bb.order(ByteOrder.LITTLE_ENDIAN);
        double x = bb.getDouble();
        double y = bb.getDouble();
        double m = bb.getDouble();
        Point p = (Point) gManager.create(Geometry.TYPES.POINT, Geometry.SUBTYPES.GEOM2DM);
        p.setX(x);
        p.setY(y);
        p.setCoordinateAt(p.getDimension()-1, m);
        return p;
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPReader#readPoLyline(org.gvsig.utils.bigfile.BigByteBuffer2)
     */
    public Geometry readPoLyline(BigByteBuffer2 bb) throws CreateGeometryException, ReadException {

        GeometryManager gManager = GeometryLocator.getGeometryManager();

        Point p = null;
        int numParts;
        int numPoints;
        int i;
        int j;

        bb.position(bb.position() + 32);
        numParts = bb.getInt();
        numPoints = bb.getInt();

        int[] tempParts = new int[numParts];

        MultiLine multiLine = null;
        Line line = null;
        if (numParts > 1) {
            multiLine = (MultiLine) gManager.create(Geometry.TYPES.MULTILINE, Geometry.SUBTYPES.GEOM2DM);
        }

        for (i = 0; i < numParts; i++) {
            tempParts[i] = bb.getInt();
        }

        j = 0;

        for (i = 0; i < numPoints; i++) {
            p = (Point) gManager.create(Geometry.TYPES.POINT, Geometry.SUBTYPES.GEOM2DM);
            fillXY(p, bb);

            if (i == tempParts[j]) {
                if(multiLine!=null && line != null){
                    multiLine.addCurve(line);
                }
                line = (Line) gManager.create(Geometry.TYPES.LINE, Geometry.SUBTYPES.GEOM2DM);
                if (j < (numParts - 1)) {
                    j++;
                }
            }
            line.addVertex(p);
        }
        checkNumVerticesOfLine(line);

        if (multiLine != null) {
            multiLine.addCurve(line);
            fillM(multiLine, bb);
            return multiLine;
        } else {
            fillM(line, bb);
            return line;
        }
    }


    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPReader#readPoLygon(org.gvsig.utils.bigfile.BigByteBuffer2)
     */
    public Geometry readPoLygon(BigByteBuffer2 bb) throws CreateGeometryException, ReadException, GeometryOperationNotSupportedException, GeometryOperationException {

        GeometryManager gManager = GeometryLocator.getGeometryManager();

        Point p = null;
        int numParts;
        int numPoints;
        int i;

        bb.position(bb.position() + 32);
        numParts = bb.getInt();
        numPoints = bb.getInt();

        int[] tempParts = new int[numParts];

        for (i = 0; i < numParts; i++) {
            tempParts[i] = bb.getInt();
        }

        MultiPolygon multipolygon = null;
        Polygon polygon = null;
        Ring ring = null;

        int pointsCounter = 0;
        for(int part=0; part<numParts; part++){
            ring = (Ring) gManager.create(Geometry.TYPES.RING, Geometry.SUBTYPES.GEOM2DM);
            int lastPoint = numPoints;
            if(part<numParts-1){
                lastPoint = tempParts[part+1];
            }
            while(pointsCounter<lastPoint){
                p = (Point) gManager.create(Geometry.TYPES.POINT, Geometry.SUBTYPES.GEOM2DM);
                fillXY(p, bb);
                ring.addVertex(p);
                pointsCounter++;
            }
            ring.closePrimitive();
            checkNumVerticesOfRing(ring);
            if (ring.isCCW() && polygon!=null) {
                // Los anillos interiores deben ser CCW pero si encontramos un
                // anillo interior que no est� envuelto en un pol�gono
                // consideramos que es un pol�gono en s� y nos aseguramos de
                // darle la vuelta (en el else)
                //FIXME: Comprobar que este es el comportamiento deseado
                polygon.addInteriorRing(ring);
            } else {
                if(polygon!=null){
                    if(multipolygon==null){
                        multipolygon = (MultiPolygon) gManager.create(Geometry.TYPES.MULTIPOLYGON, Geometry.SUBTYPES.GEOM2DM);
                    }
                    multipolygon.addPrimitive(polygon);
                }
                polygon = (Polygon) gManager.create(Geometry.TYPES.POLYGON, Geometry.SUBTYPES.GEOM2DM);
                polygon.ensureCapacity(ring.getNumVertices());
                if(ring.isCCW()){
                    //To ensure CW orientation for polygons
                    for (int v = ring.getNumVertices()-1; v >=0; v--) {
                        polygon.addVertex(ring.getVertex(v));
                    }
                } else {
                    for (int v = 0; v < ring.getNumVertices(); v++) {
                        polygon.addVertex(ring.getVertex(v));
                    }
                }
            }
        }

        if (multipolygon != null) {
            multipolygon.addPrimitive(polygon);
            fillM(multipolygon, bb);
            return multipolygon;
        } else {
            fillM(polygon, bb);
            return polygon;
        }
    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPReader#readMultiPoint(org.gvsig.utils.bigfile.BigByteBuffer2)
     */
    public Geometry readMultiPoint(BigByteBuffer2 bb) throws CreateGeometryException, ReadException {

        GeometryManager gManager = GeometryLocator.getGeometryManager();

        int numPoints;
        bb.position(bb.position() + 32);
        numPoints = bb.getInt();

        MultiPoint multipoint = gManager.createMultiPoint(Geometry.SUBTYPES.GEOM2DM);

        for (int i = 0; i < numPoints; i++) {
            Point point = (Point) gManager.create(Geometry.TYPES.POINT, Geometry.SUBTYPES.GEOM2DM);
            point.setX(bb.getDouble());
            point.setY(bb.getDouble());
            multipoint.addPoint(point);
        }

        fillM(multipoint, bb);
        return multipoint;
    }

}
