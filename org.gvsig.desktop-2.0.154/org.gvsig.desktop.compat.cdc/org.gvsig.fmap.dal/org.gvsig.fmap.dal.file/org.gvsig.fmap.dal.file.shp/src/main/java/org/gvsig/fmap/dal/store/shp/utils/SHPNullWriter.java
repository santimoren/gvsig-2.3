/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;

import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;

public class SHPNullWriter implements SHPShapeWriter {

	public SHPNullWriter() {
	}

	public int getShapeType() {
	    return SHP.NULL;
	}

	public void write(ByteBuffer buffer, Geometry geometry) {
	}

	public int getLength() {
		return 4;
	}

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPShapeWriter#write(java.nio.ByteBuffer)
     */
    public void write(ByteBuffer buffer) throws WriteException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPShapeWriter#initialize(org.gvsig.fmap.geom.Geometry)
     */
    public void initialize(Geometry geometry) throws GeometryException {
    }

}
