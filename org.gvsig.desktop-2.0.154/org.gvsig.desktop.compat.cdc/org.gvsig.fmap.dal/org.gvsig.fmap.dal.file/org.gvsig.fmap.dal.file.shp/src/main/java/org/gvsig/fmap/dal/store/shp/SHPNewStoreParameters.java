/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.shp;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.store.dbf.DBFNewStoreParameters;
import org.gvsig.fmap.geom.Geometry;

public class SHPNewStoreParameters extends SHPStoreParameters implements
    NewFeatureStoreParameters {

    public static final String PARAMETERS_DEFINITION_NAME =
        "SHPNewStoreParameters";
    public static final String FEATURETYPE_PARAMTER_NAME = "FeatureType";
    protected static final String GEOMETRYTYPE_PARAMETER_NAME = "geometryType";

    public SHPNewStoreParameters() {
        super(PARAMETERS_DEFINITION_NAME);
    }

    public byte getCodePage() {
        return ((Byte) this
            .getDynValue(DBFNewStoreParameters.CODEPAGE_PARAMTER_NAME))
            .byteValue();
    }

    public void setCodePage(byte value) {
        setDynValue(DBFNewStoreParameters.CODEPAGE_PARAMTER_NAME, new Byte(
            value));
    }

    public int getGeometryType() {
        Integer type = (Integer) this.getDynValue(GEOMETRYTYPE_PARAMETER_NAME);
        if (type == null) {
            return Geometry.TYPES.NULL;
        }
        return type.intValue();
    }

    public void setGeometryType(int type) {
        this.setDynValue(GEOMETRYTYPE_PARAMETER_NAME, new Integer(type));
    }

    public EditableFeatureType getDefaultFeatureType() {

        FeatureType featureType = (FeatureType) this.getDynValue(FEATURETYPE_PARAMTER_NAME);
        if(featureType instanceof EditableFeatureType){
            return (EditableFeatureType)featureType;
        }
        return (EditableFeatureType) featureType.getEditable();
    }

    public void setDefaultFeatureType(FeatureType featureType) {
        this.setDynValue(FEATURETYPE_PARAMTER_NAME, featureType);
    }

    @Override
    public void fixParameters() {
        super.fixParameters();
        EditableFeatureType featureType = getDefaultFeatureType();
        if (featureType != null) {
            FeatureAttributeDescriptor geometryAttribute =
            	featureType.getAttributeDescriptor(featureType.getDefaultGeometryAttributeName());
            if (geometryAttribute != null) {
                if (this.getDynValue(GEOMETRYTYPE_PARAMETER_NAME) == null) {
                    //Set the geometryType parameter
                    int geometryType =
                        featureType.getAttributeDescriptor(
                            geometryAttribute.getName())
                            .getGeometryType();
                    setGeometryType(geometryType);
                }

                //If the CRS parameter has not been fixed, but there is
                //a geometric attribute with the CRS, this attribute
                //can be used.
                if (getCRS() == null) {
                    setCRS(geometryAttribute.getSRS());
                }
            }
        }
    }
}
