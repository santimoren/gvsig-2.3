/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.io.File;
import java.io.FileFilter;

public class MyFileFilter implements FileFilter {
	private String shpPath;
	public MyFileFilter(String shpPath){
		this.shpPath=shpPath;
	}
	public boolean accept(File pathname) {
		String strend=pathname.getAbsolutePath();
		strend=strend.substring(0,strend.length()-3);
		String strshp=shpPath.substring(0, shpPath.length() - 3);
		if (strend.equals(strshp)) {
			return true;
		}
		return false;
	}

}