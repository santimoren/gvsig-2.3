/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.cresques.cts.ICRSFactory;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyChangesException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyCloseException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyOpenException;
import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.resource.spi.MultiResource;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.dal.store.dbf.DBFStoreParameters;
import org.gvsig.fmap.dal.store.dbf.DBFStoreProvider;
import org.gvsig.fmap.dal.store.shp.utils.ISHPFile;
import org.gvsig.fmap.dal.store.shp.utils.SHP;
import org.gvsig.fmap.dal.store.shp.utils.SHPFile2;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.exception.BaseException;

/**
 *
 */
public class SHPStoreProvider extends DBFStoreProvider {

    private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
    private static final Logger logger = LoggerFactory.getLogger(SHPStoreProvider.class);
    public static String NAME = "Shape";
    public static String DESCRIPTION = "Shape file";
    private ISHPFile shpFile;

    private MultiResource resource;

    protected static final String GEOMETRY_ATTIBUTE_NAME = "GEOMETRY";

    public static final String METADATA_DEFINITION_NAME = NAME;

    private SHPFeatureWriter writer = null;

    private boolean loTengoEnUso;

    /**
     * @param params
     * @param storeServices
     * @throws InitializeException
     */
    public SHPStoreProvider(SHPStoreParameters params, DataStoreProviderServices storeServices)
        throws InitializeException {
        super(params, storeServices, FileHelper.newMetadataContainer(METADATA_DEFINITION_NAME));
    }

    protected void init(DBFStoreParameters params, DataStoreProviderServices storeServices) throws InitializeException {

        this.shpFile = new SHPFile2((SHPStoreParameters) params);
        super.init(params, storeServices);
        this.shpFile.setUseNullGeometry(this.getShpParameters().getUseNullGeometry());
    }

    public Object getDynValue(String name) throws DynFieldNotFoundException {
        if (DataStore.METADATA_CRS.equalsIgnoreCase(name)) {

            return this.getShpParameters().getCRS();

        } else if (DataStore.METADATA_ENVELOPE.equalsIgnoreCase(name)) {
            try {
                return this.shpFile.getFullExtent();
            } catch (ReadException e) {
                return null;
            }
        }
        return super.getDynValue(name);
    }

    protected void initResource(DBFStoreParameters params, DataStoreProviderServices storeServices)
        throws InitializeException {

        SHPStoreParameters shpParams = (SHPStoreParameters) params;
        resource = (MultiResource) createResource(MultiResource.TYPE_NAME, new Object[] { shpParams.getSHPFileName() });

        resource.addResource(FileResource.NAME, new Object[] { shpParams.getSHPFileName() }, true);
        resource.addResource(FileResource.NAME, new Object[] { shpParams.getSHXFileName() }, true);
        resource.addResource(FileResource.NAME, new Object[] { shpParams.getDBFFileName() }, true);

        resource.frozen();
        resource.addMultiResourceConsumer(this);
        super.initResource(resource, storeServices);
    }

    ;

    public ResourceProvider getResource() {
        return resource;
    }

    /**
     *
     * @throws ResourceNotifyChangesException
     */
    protected void resourcesNotifyChanges() throws ResourceNotifyChangesException {
        getResource().notifyChanges();
        // TODO .prj

    }

    /**
     * @throws ResourceNotifyCloseException
     *
     */
    protected void resourcesNotifyClose() throws ResourceNotifyCloseException {
        getResource().notifyClose();
        // TODO .prj

    }

    @Override
    protected void doDispose() throws BaseException {
        super.doDispose();
        getResource().removeConsumer(this);
        this.writer = null;
        this.shpFile = null;
    }

    protected void disposeResource() {
        getResource().removeConsumer(this);
    }

    /**
     * @throws ResourceNotifyOpenException
     *
     */
    protected void resourcesOpen() throws ResourceNotifyOpenException {
        getResource().notifyOpen();
    }

    protected static EditableFeatureAttributeDescriptor addGeometryColumn(EditableFeatureType fType) {

        EditableFeatureAttributeDescriptor attrTmp = null;
        EditableFeatureAttributeDescriptor attr = null;
        Iterator<?> iter = fType.iterator();
        while (iter.hasNext()) {
            attrTmp = (EditableFeatureAttributeDescriptor) iter.next();
            if (attrTmp.getType() == DataTypes.GEOMETRY) {
                if (attr != null) {
                    // Two geom fields not allowed
                    fType.remove(attrTmp.getName());
                } else {
                    attr = attrTmp;
                }
            }
        }

        if (attr == null) {
            String geofield = createGeometryFieldName(fType);
            attr = fType.add(geofield, DataTypes.GEOMETRY);
            attr.setDefaultValue(null);
        }

        attr.setObjectClass(Geometry.class);
        fType.setDefaultGeometryAttributeName(attr.getName());
        return attr;

    }

    private static String createGeometryFieldName(FeatureType ft) {

        if (ft.getAttributeDescriptor(GEOMETRY_ATTIBUTE_NAME) == null) {
            return GEOMETRY_ATTIBUTE_NAME;
        }

        int i = 0;
        String candidate = GEOMETRY_ATTIBUTE_NAME + i;
        while (ft.getAttributeDescriptor(candidate) != null) {
            i++;
            candidate = GEOMETRY_ATTIBUTE_NAME + i;
        }
        return candidate;
    }

    protected static FeatureType removeGeometryColumn(EditableFeatureType fType) {
        Iterator<?> iter = fType.iterator();
        FeatureAttributeDescriptor attr;
        while (iter.hasNext()) {
            attr = (FeatureAttributeDescriptor) iter.next();
            if (attr.getType() == DataTypes.GEOMETRY) {
                iter.remove();
            }
        }
        fType.setDefaultGeometryAttributeName(null);
        return fType.getNotEditableCopy();
    }

    protected EditableFeatureType getTheFeatureType() throws InitializeException, OpenException {
        final EditableFeatureType fType = super.getTheFeatureType();
        this.open();
        try {
            getResource().execute(new ResourceAction() {

                public Object run() throws Exception {
                    EditableFeatureAttributeDescriptor attr = addGeometryColumn(fType);

                    attr.setGeometryType(geomManager.getGeometryType(shpFile.getGeometryType(),
                        shpFile.getGeometrySubType()));

                    IProjection srs = getShpParameters().getCRS();
                    attr.setSRS(srs);

                    return null;
                }
            });
            return fType;
        } catch (ResourceExecuteException e) {
            throw new InitializeException(e);
        }
    }

    protected SHPStoreParameters getShpParameters() {
        return (SHPStoreParameters) getParameters();
    }

    public String getProviderName() {
        return NAME;
    }

    public boolean allowWrite() {
        return this.shpFile.isEditable() &&
            super.allowWrite() &&
            !this.getShpParameters().getLoadCorruptGeometriesAsNull() &&
            !this.getShpParameters().getAllowInconsistenciesInGeometryType() &&
            !this.getShpParameters().getFixLinearRings();
    }

    /**
     *
     * @param index
     * @param featureType
     * @return
     * @throws ReadException
     */
    protected FeatureProvider getFeatureProviderByIndex(long index, FeatureType featureType) throws DataException {
        this.open();
        try {

            FeatureProvider featureProvider = super.getFeatureProviderByIndex(index, featureType);
            featureProvider.setDefaultEnvelope(this.shpFile.getBoundingBox(index));
            return featureProvider;
        } catch (DataException e) {
            throw e;
        } catch (CreateEnvelopeException e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(e);
        } catch (CreateGeometryException e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(e);
        }

    }

    protected void initFeatureProviderByIndex(FeatureProvider featureProvider, long index, FeatureType featureType)
        throws DataException {
        try {
            super.initFeatureProviderByIndex(featureProvider, index, featureType);
            featureProvider.setDefaultEnvelope(this.shpFile.getBoundingBox(index));
        } catch (CreateEnvelopeException e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(e);
        } catch (CreateGeometryException e) {
            throw new org.gvsig.fmap.dal.feature.exception.CreateGeometryException(e);
        }
    }

    /**
     *
     * @param featureProvider
     * @throws DataException
     */
    @Override
    protected void loadFeatureProviderByIndex(FeatureProvider featureProvider) throws DataException {

        FeatureType featureType = featureProvider.getType();
        long index = ((Long) featureProvider.getOID());
        boolean hasGeometry = false;
        int i = featureType.getDefaultGeometryAttributeIndex();
        if (i >= 0) {
            if (!featureProvider.isReadOnly(i)) {
                Geometry geom;
                try {
                    geom = this.shpFile.getGeometry(index);
                } catch (Exception e) {
                    if( this.getShpParameters().getLoadCorruptGeometriesAsNull() ) {
                        geom = null;
                    } else {
                        throw new ReadGeometryException(getName(), featureProvider.getOID(), e);
                    }
                }
                featureProvider.set(i, geom);
            }
            hasGeometry = true;
        }
        if (hasDBFAttributes(featureType, hasGeometry)) {
            super.loadFeatureProviderByIndex(featureProvider);
        }

    }

    public class ReadGeometryException extends ReadException {

        private final static String MESSAGE_FORMAT = "There was errors loading a geometry from '%(store)'.\nCheck 'Load corrupt geometries as null' in the shape's properties of the add layer dialog to skip corrupt geometries. The layer will become read only.";
        private final static String MESSAGE_KEY = "_ReadGeometryException";
        private static final long serialVersionUID = 2626155328734197112L;

        public ReadGeometryException(String store, Object oid, Throwable cause) {
            super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
            setValue("store", store);
            setValue("storeParameters",getParameters());
        }
    }

    private boolean hasDBFAttributes(FeatureType featureType, boolean hasGeometry) {
        FeatureAttributeDescriptor[] attributes = featureType.getAttributeDescriptors();
        // If there aren't any attributes, nor has any DBF attributes
        if (attributes == null || attributes.length == 0) {
            return false;
        }
        // If there is only one attribute and it is the geometry one
        if (attributes.length == 1 && hasGeometry) {
            return false;
        }
        // In any other case
        return true;
    }

    protected void loadValue(FeatureProvider featureProvider, int rowIndex, FeatureAttributeDescriptor descriptor)
        throws ReadException {
        if (descriptor.getType() == DataTypes.GEOMETRY) {
            return;
        } else {
            super.loadValue(featureProvider, rowIndex, descriptor);
        }
    }

    public FeatureProvider createFeatureProvider(FeatureType type) throws DataException {
        FeatureProvider data = new SHPFeatureProvider(this, type);
        return data;
    }

    protected void openFile() throws IOException, DataException {
        super.openFile();
        this.shpFile.open();

    }

    protected void closeFile() throws CloseException {
        super.closeFile();
        if (!this.shpFile.isOpen()) {
            return;
        }
        this.shpFile.close();
    }

    public boolean canWriteGeometry(final int geometryType, int geometrySubType) throws DataException {
        this.open();
        return ((Boolean) getResource().execute(new ResourceAction() {

            public Object run() throws Exception {
                boolean value = shpFile.canWriteGeometry(geometryType);
                return value ? Boolean.TRUE : Boolean.FALSE;
            }
        })).booleanValue();
    }

    @SuppressWarnings("rawtypes")
    public void performChanges(Iterator deleteds, Iterator inserteds, Iterator updateds,
        Iterator originalFeatureTypesUpdated) throws PerformEditingException {

        /*
         * This will throw an exception if there are new fields
         * with names too long
         */
        checkNewFieldsNameSize(originalFeatureTypesUpdated);

        final FeatureType fType;
        try {
            fType = this.getStoreServices().getDefaultFeatureType();
        } catch (DataException e) {
            throw new PerformEditingException(this.getProviderName(), e);
        }
        // TODO Comprobar el campo de geometria

        final EditableFeatureType dbfFtype = fType.getEditable();

        removeGeometryColumn(dbfFtype);

        try {
            // TODO repasar el concepto de enUso de un recurso.
            loTengoEnUso = true;
            resourceCloseRequest();

            getResource().execute(new ResourceAction() {

                public Object run() throws Exception {
                    FeatureSet set = null;
                    DisposableIterator iter = null;
                    try {
                        set = getFeatureStore().getFeatureSet();
                        writer = new SHPFeatureWriter(getProviderName());

                        SHPStoreParameters shpParams = getShpParameters();
                        SHPStoreParameters tmpParams = (SHPStoreParameters) shpParams.getCopy();

                        File tmp_base = File.createTempFile("tmp_" + System.currentTimeMillis(), null);
                        String str_base = tmp_base.getCanonicalPath();

                        tmpParams.setDBFFile(str_base + ".dbf");
                        tmpParams.setSHPFile(str_base + ".shp");
                        tmpParams.setSHXFile(str_base + ".shx");

                        writer.begin(tmpParams, fType, dbfFtype, set.getSize());

                        iter = set.fastIterator();
                        while (iter.hasNext()) {
                            Feature feature = (Feature) iter.next();
                            writer.append(feature);
                        }

                        writer.end();
                        loTengoEnUso = false;
                        close();


                        if (!shpParams.getDBFFile().delete()) {
                            logger.debug("Can't delete dbf file '" + shpParams.getDBFFile() + "'.");
                            throw new IOException("Can't delete dbf '"
                                + FilenameUtils.getBaseName(shpParams.getDBFFileName())
                                + "' file to replace with the new dbf.\nThe new dbf is in temporary file '" + str_base
                                + "'");
                        }
                        if (!shpParams.getSHPFile().delete()) {
                            logger.debug("Can't delete dbf file '" + shpParams.getSHPFile() + "'.");
                            throw new IOException("Can't delete shp '"
                                + FilenameUtils.getBaseName(shpParams.getSHPFileName())
                                + "' file to replace with the new shp.\nThe new shp is in temporary file '" + str_base
                                + "'");
                        }
                        if (!shpParams.getSHXFile().delete()) {
                            logger.debug("Can't delete dbf file '" + shpParams.getSHXFile() + "'.");
                            throw new IOException("Can't delete shx '"
                                + FilenameUtils.getBaseName(shpParams.getSHXFileName())
                                + "' file to replace with the new shx.\nThe new shx is in temporary file '" + str_base
                                + "'");
                        }

                        File prjFile = SHP.getPrjFile(shpParams.getSHPFile());
                        if (prjFile.exists()) {
                            if (!prjFile.delete()) {
                                logger.debug("Can't delete prj file '" + prjFile + "'.");
                                throw new IOException("Can't delete shx '"
                                    + FilenameUtils.getBaseName(prjFile.getPath())
                                    + "' file to replace with the new shx.\nThe new shx is in temporary file '"
                                    + str_base + "'");
                            }
                        }
                        FileUtils.moveFile(tmpParams.getDBFFile(), shpParams.getDBFFile());
                        FileUtils.moveFile(tmpParams.getSHPFile(), shpParams.getSHPFile());
                        FileUtils.moveFile(tmpParams.getSHXFile(), shpParams.getSHXFile());

                        savePrjFile(shpParams.getFile(), tmpParams.getCRS());

                        resourcesNotifyChanges();
                        initFeatureType();
                        return null;
                    } finally {
                        loTengoEnUso = false;
                        dispose(set);
                        dispose(iter);
                    }
                }
            });

        } catch (Exception e) {
            throw new PerformEditingException(this.getProviderName(), e);
        }

    }

    protected void resourceCloseRequest() throws ResourceException {
        getResource().closeRequest();
    }

    public Envelope getEnvelope() throws DataException {
        this.open();
        return (Envelope) this.getDynValue("Envelope");
    }

    public void append(final FeatureProvider featureProvider) throws DataException {
        getResource().execute(new ResourceAction() {

            public Object run() throws Exception {
                writer.append(getStoreServices().createFeature(featureProvider));
                return null;
            }
        });
    }

    public void beginAppend() throws DataException {
        getResource().execute(new ResourceAction() {

            public Object run() throws Exception {
                FeatureStore store = getFeatureStore();
                FeatureType fType = store.getDefaultFeatureType();

                // TODO Comprobar el campo de geometria
                EditableFeatureType dbfFtype = fType.getEditable();

                removeGeometryColumn(dbfFtype);
                FeatureSet set = store.getFeatureSet();

                writer = new SHPFeatureWriter(getProviderName());

                writer.begin(getShpParameters(), fType, dbfFtype, set.getSize());
                return null;
            }
        });
    }

    public void endAppend() throws DataException {
        getResource().execute(new ResourceAction() {

            public Object run() throws Exception {
                writer.end();
                close();

                SHPStoreParameters shpParameters = SHPStoreProvider.this.getShpParameters();

                savePrjFile(shpParameters.getFile(), shpParameters.getCRS());

                resourcesNotifyChanges();
                return null;
            }
        });
    }

    public Object getSourceId() {
        return this.getShpParameters().getFile();
    }


}
