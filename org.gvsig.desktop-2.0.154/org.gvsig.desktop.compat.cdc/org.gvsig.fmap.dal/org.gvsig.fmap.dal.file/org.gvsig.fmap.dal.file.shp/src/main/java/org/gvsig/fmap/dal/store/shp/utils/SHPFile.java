/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.store.shp.SHPStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.PointGeometryType;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.utils.bigfile.BigByteBuffer2;

/**
 * @author jmvivo
 *
 */
public class SHPFile implements ISHPFile {

    private static final Logger logger = LoggerFactory.getLogger(SHPFile.class);
    private Envelope extent;
    private int type;
    private int subType;
    private String srsParameters;

    private FileInputStream fin;
    private FileChannel channel;
    private BigByteBuffer2 bb;
    private FileInputStream finShx;
    private FileChannel channelShx;
    private BigByteBuffer2 bbShx;

    private SHPStoreParameters params;

    private int[] supportedGeometryTypes;
    private final GeometryManager gManager = GeometryLocator
        .getGeometryManager();

    private GeometryType gtypeNull;
    private PointGeometryType gtypePoint2D;
    private GeometryType gtypeCurve2D;
    private GeometryType gtypeSurface2D;
    private GeometryType gtypeMultiPoint2D;

    private boolean useNullGeometry = false;

    public SHPFile(SHPStoreParameters params) {
        this.params = params;
        try {
            gtypeNull = gManager.getGeometryType(TYPES.NULL, SUBTYPES.GEOM2D);
            gtypePoint2D =
                (PointGeometryType) gManager.getGeometryType(TYPES.POINT,
                    SUBTYPES.GEOM2D);
            gtypeCurve2D =
                gManager.getGeometryType(TYPES.CURVE, SUBTYPES.GEOM2D);
            gtypeSurface2D =
                gManager.getGeometryType(TYPES.SURFACE, SUBTYPES.GEOM2D);
            gtypeMultiPoint2D =
                gManager.getGeometryType(TYPES.MULTIPOINT, SUBTYPES.GEOM2D);

        } catch (GeometryTypeNotSupportedException e) {
            throw new RuntimeException(
                "Unable to get the 2D geometry types to use", e);
        } catch (GeometryTypeNotValidException e) {
            throw new RuntimeException(
                "Unable to get the 2D geometry types to use", e);
        }
    }

    public void setUseNullGeometry(boolean useNullGeometry) {
        this.useNullGeometry = useNullGeometry;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.dal.Resource#doClose()
     */
    public void close() throws CloseException {
        CloseException ret = null;

        // FIXME: Arreglar esto para que se acumulen los errores
        try {
            channel.close();
            channelShx.close();
        } catch (IOException e) {
            ret = new CloseException("SHPFile.close", e);
        } finally {
            try {
                fin.close();
                finShx.close();
            } catch (IOException e1) {
                ret = new CloseException("SHPFile.close", e1);
            }
        }

        if (ret != null) {
            throw ret;
        }
        bb = null;
        bbShx = null;
        fin = null;
        finShx = null;
        channel = null;
        channelShx = null;
        srsParameters = null;
    }

    public boolean isOpen() {
        return this.fin != null;
    }

    public synchronized void open() throws DataException {
        try {
            fin = new FileInputStream(this.params.getSHPFile());
        } catch (java.io.FileNotFoundException e) {
            throw new FileNotFoundException(this.params.getSHPFileName());
        }

        // Open the file and then get a channel from the stream
        channel = fin.getChannel();

        // long size = channel.size();

        // Get the file's size and then map it into memory
        // bb = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        try {
            bb = new BigByteBuffer2(channel, FileChannel.MapMode.READ_ONLY);
        } catch (IOException e) {
            throw new ReadException(this.params.getSHPFileName(), e);

        }
        try {
            finShx = new FileInputStream(this.params.getSHXFile());
        } catch (java.io.FileNotFoundException e) {
            throw new FileNotFoundException(this.params.getSHXFileName());
        }

        // Open the file and then get a channel from the stream
        channelShx = finShx.getChannel();

        // long sizeShx = channelShx.size();

        // Get the file's size and then map it into memory
        // bb = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        // bbShx = channelShx.map(FileChannel.MapMode.READ_ONLY, 0, sizeShx);
        try {
            bbShx =
                new BigByteBuffer2(channelShx, FileChannel.MapMode.READ_ONLY);
        } catch (IOException e) {
            throw new ReadException(this.params.getSHXFileName(), e);

        }
        bbShx.order(ByteOrder.BIG_ENDIAN);

        // create a new header.
        ShapeFileHeader2 myHeader = new ShapeFileHeader2();

        bb.position(0);

        // read the header
        myHeader.readHeader(bb);

        double[] min = new double[2];
        min[0] = myHeader.myXmin;
        min[1] = myHeader.myYmin;
        double[] max = new double[2];
        max[0] = myHeader.myXmax;
        max[1] = myHeader.myYmax;

        try {
            extent =
                gManager.createEnvelope(min[0], min[1], max[0], max[1],
                    SUBTYPES.GEOM2D);
        } catch (CreateEnvelopeException e1) {
            logger.error("Error creating the envelope", e1);
        }
        // extent = new Rectangle2D.Double(myHeader.myXmin, myHeader.myYmin,
        // myHeader.myXmax - myHeader.myXmin,
        // myHeader.myYmax - myHeader.myYmin);

        type = myHeader.myShapeType;
        subType = getGeometrySubType();

        this.initSupportedGeometryTypes();

        double x = myHeader.myXmin;
        double y = myHeader.myYmin;
        double w = myHeader.myXmax - myHeader.myXmin;
        double h = myHeader.myYmax - myHeader.myYmin;

        if (w == 0) {
            x -= 0.1;
            w = 0.2;
        }

        if (h == 0) {
            y -= 0.1;
            h = 0.2;
        }

        // TODO: SRS
        File prjFile = SHP.getPrjFile(this.params.getSHPFile());
        if (prjFile.exists()) {
            BufferedReader input = null;
            try {
                input = new BufferedReader(new FileReader(prjFile));
            } catch (java.io.FileNotFoundException e) {
                throw new FileNotFoundException(prjFile.getAbsolutePath());
            }

            try {
                this.srsParameters = input.readLine();
            } catch (IOException e) {
                throw new ReadException("SHPFile.open prj", e);
            } finally {
                try {
                    input.close();
                } catch (IOException e) {
                    // TODO ???
                }
            }

        } else {
            this.srsParameters = null;
        }
    }

    public Envelope getFullExtent() throws ReadException {
        return this.extent;
    }

    public boolean isEditable() {
        return this.params.getDBFFile().canWrite()
            && this.params.getSHPFile().canWrite()
            && this.params.getSHXFile().canWrite();
    }

    public int getGeometryType() throws ReadException {
        int auxType = 0;

        switch (type) {
        case (SHP.POINT2D):
        case (SHP.POINT3D):
            auxType = auxType | Geometry.TYPES.POINT;

            break;

        case (SHP.POLYLINE2D):
        case (SHP.POLYLINE3D):
            auxType = auxType | Geometry.TYPES.MULTICURVE;

            break;

        case (SHP.POLYGON2D):
        case (SHP.POLYGON3D):
            auxType = auxType | Geometry.TYPES.MULTISURFACE;

            break;
        case (SHP.MULTIPOINT2D):
        case (SHP.MULTIPOINT3D):
            auxType = auxType | Geometry.TYPES.MULTIPOINT;

            break;
        }

        return auxType;
    }

    public int getGeometrySubType() throws ReadException {
        switch (type) {
        case (SHP.POINT2D):
        case (SHP.POLYLINE2D):
        case (SHP.POLYGON2D):
        case (SHP.MULTIPOINT2D):
            return SUBTYPES.GEOM2D;
        case (SHP.POINT3D):
        case (SHP.POLYLINE3D):
        case (SHP.POLYGON3D):
        case (SHP.MULTIPOINT3D):
            return SUBTYPES.GEOM3D;
        case (SHP.POINTM):
        case (SHP.POLYLINEM):
        case (SHP.POLYGONM):
        case (SHP.MULTIPOINTM):
            return SUBTYPES.GEOM2DM;
        }

        return SUBTYPES.UNKNOWN;
    }

    private Geometry getNullGeometry() throws CreateGeometryException {
        if( this.useNullGeometry ) {
            return gtypeNull.create();
        }
        return null;
    }

    /**
     * Gets the geometry with the index provided.
     * Set to synchronized to prevent concurrent threads issue (?)
     *
     * @param position
     * @return
     * @throws ReadException
     * @throws CreateGeometryException
     */
    public synchronized Geometry getGeometry(long position)
        throws ReadException, CreateGeometryException {

        int shapeType;
        bb.position(getPositionForRecord(position));
        bb.order(ByteOrder.LITTLE_ENDIAN);
        shapeType = bb.getInt();

        // el shape tal con tema tal y n�mro tal es null
        if (shapeType == SHP.NULL) {
            return getNullGeometry();
        }

        /*
         * Inconsistency: this particular shape is not
         * of the expected type. This can be because the SHP file
         * is corrupt and it can cause "out of memory error"
         * because the code will possibly try to instantiate a
         * huge (absurd) array, so it's safer to return a null geometry
         */
        if (shapeType != type) {
            return getNullGeometry();
        }

        // retrieve that shape.
        // tempRecord.setShape(readShape(tempShapeType, tempContentLength, in));
        switch (type) {
        case (SHP.POINT2D):
        case (SHP.POINT3D):
        case (SHP.POINTM):
            Point point = readPoint(bb);
            fillPoint(point);
            return point;

        case (SHP.POLYLINE2D):
            Curve curve = (Curve) gtypeCurve2D.create();
            fillCurve(curve);
            if( curve.getNumVertices()==0 ) {
                return getNullGeometry();
            }
            return curve;

        case (SHP.POLYGON2D):
            Surface surface = (Surface) gtypeSurface2D.create();
            fillSurface(surface);
            if( surface.getNumVertices()==0 ) {
                return getNullGeometry();
            }
            return surface;

        case (SHP.POLYLINE3D):
            Curve curve3D =
                (Curve) gManager.create(TYPES.CURVE, SUBTYPES.GEOM3D);
            fillCurve(curve3D);
            fillZ(curve3D);
            if( curve3D.getNumVertices()==0 ) {
                return getNullGeometry();
            }
            return curve3D;

        case (SHP.POLYGON3D):
            Surface surface3D =
                (Surface) gManager.create(TYPES.SURFACE, SUBTYPES.GEOM3D);
            fillSurface(surface3D);
            fillZ(surface3D);
            if( surface3D.getNumVertices()==0 ) {
                return getNullGeometry();
            }
            return surface3D;

        case (SHP.MULTIPOINT2D):

            Point p = null;
            int numPoints;
            int i;
            int j;
            bb.position(bb.position() + 32);
            numPoints = bb.getInt();

            Point[] points = new Point[numPoints];

            for (i = 0; i < numPoints; i++) {
                points[i] =
                    (Point) gManager.create(TYPES.POINT, SUBTYPES.GEOM2D);
                points[i].setX(bb.getDouble());
                points[i].setY(bb.getDouble());
            }

            MultiPoint multipoint = (MultiPoint) gtypeMultiPoint2D.create();
            // MultiPoint multipoint =
            // (MultiPoint)GeometryLocator.getGeometryManager().create(TYPES.MULTIPOINT,
            // SUBTYPES.GEOM2D);
            for (int k = 0; k < points.length; k++) {
                multipoint.addPoint(points[k]);
            }

            if( multipoint.getPrimitivesNumber()==0 ) {
                return getNullGeometry();
            }
            return multipoint;

        case (SHP.MULTIPOINT3D):
            bb.position(bb.position() + 32);
            numPoints = bb.getInt();

            double[] temX = new double[numPoints];
            double[] temY = new double[numPoints];
            double[] temZ = new double[numPoints];

            for (i = 0; i < numPoints; i++) {
                temX[i] = bb.getDouble();
                temY[i] = bb.getDouble();
                // temZ[i] = bb.getDouble();
            }

            for (i = 0; i < numPoints; i++) {
                temZ[i] = bb.getDouble();
            }

            MultiPoint multipoint3D =
                (MultiPoint) gManager.create(TYPES.MULTIPOINT, SUBTYPES.GEOM3D);
            for (int k = 0; k < temX.length; k++) {
                Point pointAux =
                    (Point) gManager.create(TYPES.POINT, SUBTYPES.GEOM3D);
                pointAux.setX(temX[k]);
                pointAux.setY(temY[k]);
                pointAux.setCoordinateAt(2, temZ[k]);
                multipoint3D.addPoint(pointAux);
            }
            if( multipoint3D.getPrimitivesNumber()==0 ) {
                return getNullGeometry();
            }
            return multipoint3D;
        }

        return null;
    }

    private void fillPoint(Point point) throws ReadException {
        if (subType == Geometry.SUBTYPES.GEOM3D) {
            point.setCoordinateAt(Geometry.DIMENSIONS.Z, bb.getDouble());
        } else
            if (subType == Geometry.SUBTYPES.GEOM2DM) {
                point.setCoordinateAt(2, bb.getDouble());
            }
    }

    private void fillCurve(Curve curve)
        throws CreateGeometryException, ReadException {
        Point p = null;
        int numParts;
        int numPoints;
        int i;
        int j;

        bb.position(bb.position() + 32);
        numParts = bb.getInt();
        numPoints = bb.getInt();

        int[] tempParts = new int[numParts];

        for (i = 0; i < numParts; i++) {
            tempParts[i] = bb.getInt();
        }

        j = 0;

        for (i = 0; i < numPoints; i++) {
            p = readPoint(bb);

            if (i == tempParts[j]) {
                curve.addMoveToVertex(p);

                if (j < (numParts - 1)) {
                    j++;
                }
            } else {
                curve.addVertex(p);
            }
        }
    }

    private void fillSurface(Surface surface)
        throws CreateGeometryException, ReadException {
        Point p = null;
        int numParts;
        int numPoints;
        int i;
        int partIndex;

        bb.position(bb.position() + 32);
        numParts = bb.getInt();
        numPoints = bb.getInt();

        int[] tempParts = new int[numParts];

        for (i = 0; i < numParts; i++) {
            tempParts[i] = bb.getInt();
        }

        partIndex = 0;

        for (i = 0; i < numPoints; i++) {
            p = readPoint(bb);

            if (i == tempParts[partIndex]) {
                surface.addMoveToVertex(p);
                if (partIndex < (numParts - 1)) {
                    partIndex++;
                }
            } else {
                if ((i == tempParts[partIndex] - 1) || (i == numPoints - 1)) {
                    surface.closePrimitive();
                } else {
                    surface.addVertex(p);
                }
            }
        }
    }

    private void fillZ(OrientablePrimitive orientablePrimitive)
        throws CreateGeometryException, ReadException {
        double[] boxZ = new double[2];
        boxZ[0] = bb.getDouble();
        boxZ[1] = bb.getDouble();

        for (int i = 0; i < orientablePrimitive.getNumVertices(); i++) {
            orientablePrimitive.setCoordinateAt(i, 2, bb.getDouble());
        }
    }

    private long getPositionForRecord(long numRec) {
        // shx file has a 100 bytes header, then, records
        // 8 bytes length, one for each entity.
        // first 4 bytes are the offset
        // next 4 bytes are length

        int posIndex = 100 + ((int) numRec * 8);
        // bbShx.position(posIndex);
        long pos = 8 + 2 * bbShx.getInt(posIndex);

        return pos;
    }

    /**
     * Reads the Point from the shape file.
     *
     * @param in
     *            ByteBuffer.
     *
     * @return Point2D.
     * @throws ReadException
     * @throws CreateGeometryException
     */
    private Point readPoint(BigByteBuffer2 in)
        throws CreateGeometryException, ReadException {
        // bytes 1 to 4 are the type and have already been read.
        // bytes 4 to 12 are the X coordinate
        in.order(ByteOrder.LITTLE_ENDIAN);

        return Geometry.SUBTYPES.GEOM2D == subType ? gtypePoint2D.createPoint(
            in.getDouble(), in.getDouble()) : gManager.createPoint(
            in.getDouble(),
            in.getDouble(), subType);
    }

    /**
     * Lee un rect�ngulo del fichero.
     *
     * @param in
     *            ByteBuffer.
     *
     * @return Rect�ngulo.
     * @throws CreateEnvelopeException
     *
     * @throws IOException
     */
    private Envelope readRectangle(BigByteBuffer2 in)
        throws CreateEnvelopeException {
        in.order(ByteOrder.LITTLE_ENDIAN);
        double x = in.getDouble();
        double y = in.getDouble();

        double x2 = in.getDouble();

        if (x2 - x == 0) {
            x2 += 0.2;
            x -= 0.1;
        }

        double y2 = in.getDouble();

        if (y2 - y == 0) {
            y2 += 0.2;
            y -= 0.1;
        }
        Envelope tempEnvelope =
            gManager.createEnvelope(x, y, x2, y2, SUBTYPES.GEOM2D);
        return tempEnvelope;
    }

    /**
     * Gets the geometry bbox with the index provided.
     * Set to synchronized to prevent concurrent threads issue (?)
     *
     * @param featureIndex
     * @return
     * @throws ReadException
     * @throws CreateEnvelopeException
     * @throws CreateGeometryException
     */
    public synchronized Envelope getBoundingBox(long featureIndex)
        throws ReadException, CreateEnvelopeException, CreateGeometryException {
        Point p = null;
        Envelope BoundingBox = null;
        try {
            bb.position(getPositionForRecord(featureIndex));
        } catch (Exception e) {
            throw new ReadException("getBondingBox (" + featureIndex + ")", e);
            // logger.error(" Shapefile is corrupted. Drawing aborted. ="+e+
            // "  "+"index = "+index);
        }
        bb.order(ByteOrder.LITTLE_ENDIAN);

        int tipoShape = bb.getInt();

        // AZABALA: si tipoShape viene con valores erroneos deja de funcionar
        // el metodo getShape(i)
        // if (tipoShape != SHP.NULL) {
        // type = tipoShape;

        // }

        // retrieve that shape.
        // tempRecord.setShape(readShape(tempShapeType, tempContentLength, in));
        switch (tipoShape) {
        case (SHP.POINT2D):
        case (SHP.POINT3D):
            p = readPoint(bb);
            BoundingBox =
                gManager.createEnvelope(p.getX() - 0.1, p.getY() - 0.1,
                    p.getX() + 0.2, p.getY() + 0.2, SUBTYPES.GEOM2D);
            // new Rectangle2D.Double(p.getX() - 0.1,
            // p.getY() - 0.1, 0.2, 0.2);

            break;

        case (SHP.POLYLINE2D):
        case (SHP.POLYGON2D):
        case (SHP.MULTIPOINT2D):
        case (SHP.POLYLINE3D):
        case (SHP.POLYGON3D):
        case (SHP.MULTIPOINT3D):

            // BoundingBox
            BoundingBox = readRectangle(bb);

            break;
        }

        return BoundingBox;
    }

    /**
     * @return
     */
    public String getSRSParameters() {
        return this.srsParameters;
    }

    private void initSupportedGeometryTypes() throws ReadException {
        switch (this.getGeometryType()) {
        case Geometry.TYPES.POINT:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.POINT, Geometry.TYPES.NULL };
            break;
        case Geometry.TYPES.MULTIPOINT:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.MULTIPOINT, Geometry.TYPES.NULL };
            break;
        case Geometry.TYPES.MULTICURVE:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.CURVE, Geometry.TYPES.ELLIPSE,
                    Geometry.TYPES.ARC, Geometry.TYPES.CIRCLE,
                    Geometry.TYPES.SURFACE, Geometry.TYPES.NULL,
                    Geometry.TYPES.MULTICURVE };
            break;
        case Geometry.TYPES.MULTISURFACE:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.ELLIPSE, Geometry.TYPES.CIRCLE,
                    Geometry.TYPES.SURFACE, Geometry.TYPES.NULL,
                    Geometry.TYPES.MULTISURFACE };
            break;

        default:
            supportedGeometryTypes = new int[] {};
        }
    }

    public boolean canWriteGeometry(int gvSIGgeometryType) {
        for (int i = 0; i < supportedGeometryTypes.length; i++) {
            if (gvSIGgeometryType == supportedGeometryTypes[i]) {
                return true;
            }
        }
        return false;
    }
}
