/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;

import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;

/**
 * Interfaz para escribir todos los tipos de formato shape.
 *
 * @author fdiaz
 */
public interface SHPShapeWriter {
	/**
	 * Devuelve el tipo de shape de que se trata.
	 *
	 * @return Tipo de shape.
	 */
	public int getShapeType();

	/**
	 * Escribe en el buffer la geometr�a que se pasa como par�metro.
	 *
	 * @param buffer Buffer donde escribir.
	 * @throws WriteException
	 */
	public void write(ByteBuffer buffer) throws WriteException;

	/**
	 * Devuelve el tama�o de la geometr�a.
	 *
	 * @param geometry Geometr�a a medir.
	 *
	 * @return Tama�o de la geometr�a.
	 */
	public int getLength();

    /**
     * Inicializa el writer con la geometr�a.
     *
     * @param geometry la Geometr�a.
     * @throws GeometryException
     * @throws GeometryOperationException
     * @throws GeometryOperationNotSupportedException
     *
     */
	public void initialize(Geometry geometry) throws GeometryException, GeometryOperationNotSupportedException, GeometryOperationException;
}
