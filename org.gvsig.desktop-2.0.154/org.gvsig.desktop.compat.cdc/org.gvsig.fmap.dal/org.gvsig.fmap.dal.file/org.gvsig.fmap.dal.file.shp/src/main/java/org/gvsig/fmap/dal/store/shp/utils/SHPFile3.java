/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.store.shp.SHPStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.OrientablePrimitive;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.PointGeometryType;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.utils.bigfile.BigByteBuffer2;

/**
 * @author jmvivo
 *
 */
public class SHPFile3 implements ISHPFile {

    private static final Logger logger = LoggerFactory.getLogger(SHPFile3.class);
    private Envelope extent;
    private int type;
    private int subType;
    private String srsParameters;

    private FileInputStream fin;
    private FileChannel channel;
    private BigByteBuffer2 bb;
    private FileInputStream finShx;
    private FileChannel channelShx;
    private BigByteBuffer2 bbShx;

    private SHPStoreParameters params;

    private int[] supportedGeometryTypes;
    private final GeometryManager gManager = GeometryLocator
            .getGeometryManager();

    private GeometryType gtypeNull;
    private PointGeometryType gtypePoint2D;
    private GeometryType gtypeCurve2D;
    private GeometryType gtypeSurface2D;
    private GeometryType gtypeMultiPoint2D;

    private boolean useNullGeometry = false;

    private boolean allowInconsistenciesInGeometryTypeWarningShow = false;
    
    public SHPFile3(SHPStoreParameters params) {
        this.params = params;
        try {
            gtypeNull = gManager.getGeometryType(TYPES.NULL, SUBTYPES.GEOM2D);
            gtypePoint2D
                    = (PointGeometryType) gManager.getGeometryType(TYPES.POINT,
                            SUBTYPES.GEOM2D);
            gtypeCurve2D
                    = gManager.getGeometryType(TYPES.CURVE, SUBTYPES.GEOM2D);
            gtypeSurface2D
                    = gManager.getGeometryType(TYPES.SURFACE, SUBTYPES.GEOM2D);
            gtypeMultiPoint2D
                    = gManager.getGeometryType(TYPES.MULTIPOINT, SUBTYPES.GEOM2D);

        } catch (GeometryTypeNotSupportedException e) {
            throw new RuntimeException(
                    "Unable to get the 2D geometry types to use", e);
        } catch (GeometryTypeNotValidException e) {
            throw new RuntimeException(
                    "Unable to get the 2D geometry types to use", e);
        }
    }

    public void setUseNullGeometry(boolean useNullGeometry) {
        this.useNullGeometry = useNullGeometry;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.gvsig.fmap.dal.Resource#doClose()
     */
    public void close() throws CloseException {
        CloseException ret = null;

        logger.debug("Closing shp/shx file '"+this.params.getSHPFileName()+"'");
        
        // FIXME: Arreglar esto para que se acumulen los errores
        try {
            channel.close();
            channelShx.close();
        } catch (IOException e) {
            ret = new CloseException("SHPFile.close", e);
        } finally {
            try {
                fin.close();
                finShx.close();
            } catch (IOException e1) {
                ret = new CloseException("SHPFile.close", e1);
            }
        }

        if (ret != null) {
            throw ret;
        }
        bb = null;
        bbShx = null;
        fin = null;
        finShx = null;
        channel = null;
        channelShx = null;
        srsParameters = null;
    }

    public boolean isOpen() {
        return this.fin != null;
    }

    public synchronized void open() throws DataException {
        try {
            fin = new FileInputStream(this.params.getSHPFile());
        } catch (java.io.FileNotFoundException e) {
            throw new FileNotFoundException(this.params.getSHPFileName());
        }

        // Open the file and then get a channel from the stream
        channel = fin.getChannel();

        // long size = channel.size();
        // Get the file's size and then map it into memory
        // bb = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        try {
            bb = new BigByteBuffer2(channel, FileChannel.MapMode.READ_ONLY);
        } catch (IOException e) {
            throw new ReadException(this.params.getSHPFileName(), e);

        }
        try {
            finShx = new FileInputStream(this.params.getSHXFile());
        } catch (java.io.FileNotFoundException e) {
            throw new FileNotFoundException(this.params.getSHXFileName());
        }

        // Open the file and then get a channel from the stream
        channelShx = finShx.getChannel();

        // long sizeShx = channelShx.size();
        // Get the file's size and then map it into memory
        // bb = channel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        // bbShx = channelShx.map(FileChannel.MapMode.READ_ONLY, 0, sizeShx);
        try {
            bbShx
                    = new BigByteBuffer2(channelShx, FileChannel.MapMode.READ_ONLY);
        } catch (IOException e) {
            throw new ReadException(this.params.getSHXFileName(), e);

        }
        bbShx.order(ByteOrder.BIG_ENDIAN);

        // create a new header.
        ShapeFileHeader2 myHeader = new ShapeFileHeader2();

        bb.position(0);

        // read the header
        myHeader.readHeader(bb);

        double[] min = new double[2];
        min[0] = myHeader.myXmin;
        min[1] = myHeader.myYmin;
        double[] max = new double[2];
        max[0] = myHeader.myXmax;
        max[1] = myHeader.myYmax;

        try {
            extent
                    = gManager.createEnvelope(min[0], min[1], max[0], max[1],
                            SUBTYPES.GEOM2D);
        } catch (CreateEnvelopeException e1) {
            logger.warn("Error creating the envelope", e1);
        }
        // extent = new Rectangle2D.Double(myHeader.myXmin, myHeader.myYmin,
        // myHeader.myXmax - myHeader.myXmin,
        // myHeader.myYmax - myHeader.myYmin);

        type = myHeader.myShapeType;
        subType = getGeometrySubType();

        this.initSupportedGeometryTypes();

        double x = myHeader.myXmin;
        double y = myHeader.myYmin;
        double w = myHeader.myXmax - myHeader.myXmin;
        double h = myHeader.myYmax - myHeader.myYmin;

        if (w == 0) {
            x -= 0.1;
            w = 0.2;
        }

        if (h == 0) {
            y -= 0.1;
            h = 0.2;
        }

        // TODO: SRS
        File prjFile = SHP.getPrjFile(this.params.getSHPFile());
        if (prjFile.exists()) {
            BufferedReader input = null;
            try {
                input = new BufferedReader(new FileReader(prjFile));
            } catch (java.io.FileNotFoundException e) {
                throw new FileNotFoundException(prjFile.getAbsolutePath());
            }

            try {
                this.srsParameters = input.readLine();
            } catch (IOException e) {
                throw new ReadException("SHPFile.open prj", e);
            } finally {
                try {
                    input.close();
                } catch (IOException e) {
                    // TODO ???
                }
            }

        } else {
            this.srsParameters = null;
        }
    }

    public Envelope getFullExtent() throws ReadException {
        return this.extent;
    }

    public boolean isEditable() {
        return this.params.getDBFFile().canWrite()
                && this.params.getSHPFile().canWrite()
                && this.params.getSHXFile().canWrite();
    }

    public int getGeometryType() throws ReadException {
        int auxType = 0;

        switch (type) {
            case (SHP.POINT2D):
            case (SHP.POINT3D):
                auxType = auxType | Geometry.TYPES.POINT;

                break;

            case (SHP.POLYLINE2D):
            case (SHP.POLYLINE3D):
                auxType = auxType | Geometry.TYPES.MULTICURVE;

                break;

            case (SHP.POLYGON2D):
            case (SHP.POLYGON3D):
                auxType = auxType | Geometry.TYPES.MULTISURFACE;

                break;
            case (SHP.MULTIPOINT2D):
            case (SHP.MULTIPOINT3D):
                auxType = auxType | Geometry.TYPES.MULTIPOINT;

                break;
        }

        return auxType;
    }

    public int getGeometrySubType() throws ReadException {
        switch (type) {
            case (SHP.POINT2D):
            case (SHP.POLYLINE2D):
            case (SHP.POLYGON2D):
            case (SHP.MULTIPOINT2D):
                return SUBTYPES.GEOM2D;
            case (SHP.POINT3D):
            case (SHP.POLYLINE3D):
            case (SHP.POLYGON3D):
            case (SHP.MULTIPOINT3D):
                return SUBTYPES.GEOM3D;
            case (SHP.POINTM):
            case (SHP.POLYLINEM):
            case (SHP.POLYGONM):
            case (SHP.MULTIPOINTM):
                return SUBTYPES.GEOM2DM;
        }

        return SUBTYPES.UNKNOWN;
    }

    public Geometry getNullGeometry() throws CreateGeometryException {
        if (this.useNullGeometry) {
            return gtypeNull.create();
        }
        return null;
    }

    /**
     * Gets the geometry with the index provided. Set to synchronized to prevent
     * concurrent threads issue (?)
     *
     * @param position
     * @return
     * @throws ReadException
     * @throws CreateGeometryException
     */
    public synchronized Geometry getGeometry(long position)
            throws ReadException, CreateGeometryException {

        int shapeType;
        bb.position(getPositionForRecord(position));
        bb.order(ByteOrder.LITTLE_ENDIAN);
        shapeType = bb.getInt();

        if (shapeType == SHP.NULL) {
            return getNullGeometry();
        }

        /*
         * Inconsistency: this particular shape is not
         * of the expected type. This can be because the SHP file
         * is corrupt and it can cause "out of memory error"
         * because the code will possibly try to instantiate a
         * huge (absurd) array, so it's safer to return a null geometry
         */
        if (shapeType != type) {
            if( !allowInconsistenciesInGeometryTypeWarningShow ) {
                logger.warn("Geometry type of Shape ("+type+") does not match the geometry found ("+shapeType+") in the shape '"+this.params.getSHPFileName()+".");
                allowInconsistenciesInGeometryTypeWarningShow = true;
            }
            if( ! this.params.getAllowInconsistenciesInGeometryType() ) {
                return getNullGeometry();
            }
        }

        Geometry geometry;
        Point point;
        switch (type) {
            case (SHP.POINT2D):
                point = readPoint(bb);
                return point;

            case (SHP.POINT3D):
                point = readPoint(bb);
                point.setCoordinateAt(
                        Geometry.DIMENSIONS.Z,
                        bb.getDouble()
                );
                return point;

            case (SHP.POINTM):
                point = readPoint(bb);
                point.setCoordinateAt(2, bb.getDouble());
                return point;

            case (SHP.POLYLINE2D):
                geometry = readGeometry(
                        Geometry.TYPES.CURVE,
                        Geometry.TYPES.MULTICURVE,
                        Geometry.SUBTYPES.GEOM2D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;

            case (SHP.POLYGON2D):
                geometry = readGeometry(
                        Geometry.TYPES.SURFACE,
                        Geometry.TYPES.MULTISURFACE,
                        Geometry.SUBTYPES.GEOM2D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;

            case (SHP.POLYLINE3D):
                geometry = readGeometry(
                        Geometry.TYPES.CURVE,
                        Geometry.TYPES.MULTICURVE,
                        Geometry.SUBTYPES.GEOM3D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;

            case (SHP.POLYGON3D):
                geometry = readGeometry(
                        Geometry.TYPES.SURFACE,
                        Geometry.TYPES.MULTISURFACE,
                        Geometry.SUBTYPES.GEOM3D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;

            case (SHP.MULTIPOINT2D):
                geometry = readMultiPoint(SUBTYPES.GEOM2D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;

            case (SHP.MULTIPOINT3D):
                geometry = readMultiPoint(SUBTYPES.GEOM3D);
                if (geometry == null) {
                    return getNullGeometry();
                }
                return geometry;
        }

        return null;
    }

    private Geometry readMultiPoint(int subtype) throws CreateGeometryException, ReadException {
        Point point;

        bb.position(bb.position() + 32);
        int numPoints = bb.getInt();
        
        if( numPoints < 1 ) {
            return null;
        }

//        Point[] points = new Point[numPoints];

        MultiPrimitive multi = (MultiPrimitive) gManager.create(Geometry.TYPES.MULTIPOINT, subtype);
        multi.ensureCapacity(numPoints);

        for (int i = 0; i < numPoints; i++) {
            point = (Point) gManager.create(TYPES.POINT, subtype);
            point.setX(bb.getDouble());
            point.setY(bb.getDouble());
            multi.addPrimitive(point);
        }

        if (subtype == Geometry.SUBTYPES.GEOM3D) {
            for (int i = 0; i < numPoints; i++) {
                double z = bb.getDouble();
                point = (Point) multi.getPrimitiveAt(i);
                point.setCoordinateAt(Geometry.DIMENSIONS.Z, z);
            }
        }
        if (multi.getPrimitivesNumber() == 0) {
            return null;
        }
        return multi;
    }

    private Geometry readGeometry(int type, int typemulti, int subtype)
            throws CreateGeometryException, ReadException {

        bb.position(bb.position() + 32);

        int numberOfMultis = bb.getInt();
        int numberOfPoints = bb.getInt();
        
        if( numberOfPoints<1  ) {
            return null;
        }
        if( numberOfMultis<1 ) {
            numberOfMultis=0;
        }

        int[] indexOfPointsOfMulti = new int[numberOfMultis+1];
        for (int i = 0; i < numberOfMultis; i++) {
            indexOfPointsOfMulti[i] = bb.getInt();
        }
        indexOfPointsOfMulti[numberOfMultis] = -1;
                
        MultiPrimitive multi = null;
        if (numberOfMultis > 1) {
            multi = (MultiPrimitive) gManager.create(typemulti, subtype);
        }
        OrientablePrimitive primitive = null;

        int nextSurface = 0;

        for (int i = 0; i < numberOfPoints; i++) {
            Point p = readPoint(subtype, bb);
            if (i == indexOfPointsOfMulti[nextSurface]) {
                // Cambiamos de poligono
                if( primitive != null ) {
                    if( multi != null ) {
                        dumpAddMultiInfo(multi,primitive);
                        primitive.closePrimitive();
                        multi.addPrimitive(primitive);
                    }
                }
                primitive = (OrientablePrimitive) gManager.create(type, subtype);
                primitive.addVertex(p);
                nextSurface++;
            } else {
                primitive.addVertex(p);
            }
        }
        if (multi != null) {
            dumpAddMultiInfo(multi,primitive);
            primitive.closePrimitive();
            multi.addPrimitive(primitive);
            if (subtype == Geometry.SUBTYPES.GEOM3D) {
                fillZ(multi);
            }
            return multi;
        }
        if (primitive==null || primitive.getNumVertices() < 1) {
            return null;
        }
        if (subtype == Geometry.SUBTYPES.GEOM3D) {
            fillZ(primitive);
        }
        return primitive;
    }

    private void dumpAddMultiInfo(MultiPrimitive multi, OrientablePrimitive primitive) {
        String last = "???";
        String first = "???";
        try {
            first = primitive.getVertex(0).convertToWKT();
        } catch (Exception ex) {

        }
        try {
            last = primitive.getVertex(primitive.getNumVertices()-1).convertToWKT();            
        } catch (Exception ex) {

        }
        logger.debug("multi["+multi.getPrimitivesNumber()+"], vertex:"+primitive.getNumVertices()+", first:"+first+", last"+last+".");
    }
    
    private void fillZ(Geometry geometry)
            throws CreateGeometryException, ReadException {
        double[] boxZ = new double[2];
        boxZ[0] = bb.getDouble();
        boxZ[1] = bb.getDouble();

        if (geometry == null) {
            return;
        }
        if (geometry instanceof Aggregate) {
            Aggregate multi = (Aggregate) geometry;
            for (int i = 0; i < multi.getPrimitivesNumber(); i++) {
                OrientablePrimitive primitive = (OrientablePrimitive) geometry;
                for (int p = 0; p < primitive.getNumVertices(); p++) {
                    primitive.setCoordinateAt(p, 2, bb.getDouble());
                }
            }
        } else if (geometry instanceof OrientablePrimitive) {
            OrientablePrimitive primitive = (OrientablePrimitive) geometry;
            for (int p = 0; p < primitive.getNumVertices(); p++) {
                primitive.setCoordinateAt(p, 2, bb.getDouble());
            }
        } else {
            logger.warn("Geoemtry type '" + geometry.getClass().getName() + "'unexpected ");
        }
    }

    private long getPositionForRecord(long numRec) {
        // shx file has a 100 bytes header, then, records
        // 8 bytes length, one for each entity.
        // first 4 bytes are the offset
        // next 4 bytes are length

        int posIndex = 100 + ((int) numRec * 8);
        // bbShx.position(posIndex);
        long pos = 8 + 2 * bbShx.getInt(posIndex);

        return pos;
    }

    /**
     * Reads the Point from the shape file.
     *
     * @param in ByteBuffer.
     *
     * @return Point2D.
     * @throws ReadException
     * @throws CreateGeometryException
     */
    private Point readPoint(BigByteBuffer2 in)
            throws CreateGeometryException, ReadException {
        return readPoint(subType, in);
    }

    private Point readPoint(int subtype, BigByteBuffer2 in)
            throws CreateGeometryException, ReadException {
        // bytes 1 to 4 are the type and have already been read.
        // bytes 4 to 12 are the X coordinate
        in.order(ByteOrder.LITTLE_ENDIAN);
        double x = in.getDouble();
        double y = in.getDouble();
        Point p = gManager.createPoint(x, y, subtype);
        return p;
    }

    /**
     * Lee un rectángulo del fichero.
     *
     * @param in ByteBuffer.
     *
     * @return Rectángulo.
     * @throws CreateEnvelopeException
     *
     * @throws IOException
     */
    private Envelope readRectangle(BigByteBuffer2 in)
            throws CreateEnvelopeException {
        in.order(ByteOrder.LITTLE_ENDIAN);
        double x = in.getDouble();
        double y = in.getDouble();

        double x2 = in.getDouble();

        if (x2 - x == 0) {
            x2 += 0.2;
            x -= 0.1;
        }

        double y2 = in.getDouble();

        if (y2 - y == 0) {
            y2 += 0.2;
            y -= 0.1;
        }
        Envelope tempEnvelope
                = gManager.createEnvelope(x, y, x2, y2, SUBTYPES.GEOM2D);
        return tempEnvelope;
    }

    /**
     * Gets the geometry bbox with the index provided. Set to synchronized to
     * prevent concurrent threads issue (?)
     *
     * @param featureIndex
     * @return
     * @throws ReadException
     * @throws CreateEnvelopeException
     * @throws CreateGeometryException
     */
    public synchronized Envelope getBoundingBox(long featureIndex)
            throws ReadException, CreateEnvelopeException, CreateGeometryException {
        Point p;
        Envelope BoundingBox = null;
        try {
            bb.position(getPositionForRecord(featureIndex));
        } catch (Exception e) {
            throw new ReadException("getBondingBox (" + featureIndex + ")", e);
            // logger.error(" Shapefile is corrupted. Drawing aborted. ="+e+
            // "  "+"index = "+index);
        }
        bb.order(ByteOrder.LITTLE_ENDIAN);

        int tipoShape = bb.getInt();

        // AZABALA: si tipoShape viene con valores erroneos deja de funcionar
        // el metodo getShape(i)
        // if (tipoShape != SHP.NULL) {
        // type = tipoShape;
        // }
        // retrieve that shape.
        // tempRecord.setShape(readShape(tempShapeType, tempContentLength, in));
        switch (tipoShape) {
            case (SHP.POINT2D):
            case (SHP.POINT3D):
                p = readPoint(bb);
                BoundingBox
                        = gManager.createEnvelope(p.getX() - 0.1, p.getY() - 0.1,
                                p.getX() + 0.2, p.getY() + 0.2, SUBTYPES.GEOM2D);
                // new Rectangle2D.Double(p.getX() - 0.1,
                // p.getY() - 0.1, 0.2, 0.2);

                break;

            case (SHP.POLYLINE2D):
            case (SHP.POLYGON2D):
            case (SHP.MULTIPOINT2D):
            case (SHP.POLYLINE3D):
            case (SHP.POLYGON3D):
            case (SHP.MULTIPOINT3D):

                // BoundingBox
                BoundingBox = readRectangle(bb);

                break;
        }

        return BoundingBox;
    }

    /**
     * @return
     */
    public String getSRSParameters() {
        return this.srsParameters;
    }

    private void initSupportedGeometryTypes() throws ReadException {
        switch (this.getGeometryType()) {
            case Geometry.TYPES.POINT:
                supportedGeometryTypes
                        = new int[]{Geometry.TYPES.POINT, Geometry.TYPES.NULL};
                break;
            case Geometry.TYPES.MULTIPOINT:
                supportedGeometryTypes
                        = new int[]{Geometry.TYPES.MULTIPOINT, Geometry.TYPES.NULL};
                break;
            case Geometry.TYPES.MULTICURVE:
                supportedGeometryTypes
                        = new int[]{Geometry.TYPES.CURVE, Geometry.TYPES.ELLIPSE,
                            Geometry.TYPES.ARC, Geometry.TYPES.CIRCLE,
                            Geometry.TYPES.SURFACE, Geometry.TYPES.NULL,
                            Geometry.TYPES.MULTICURVE};
                break;
            case Geometry.TYPES.MULTISURFACE:
                supportedGeometryTypes
                        = new int[]{Geometry.TYPES.ELLIPSE, Geometry.TYPES.CIRCLE,
                            Geometry.TYPES.SURFACE, Geometry.TYPES.NULL,
                            Geometry.TYPES.MULTISURFACE};
                break;

            default:
                supportedGeometryTypes = new int[]{};
        }
    }

    public boolean canWriteGeometry(int gvSIGgeometryType) {
        for (int i = 0; i < supportedGeometryTypes.length; i++) {
            if (gvSIGgeometryType == supportedGeometryTypes[i]) {
                return true;
            }
        }
        return false;
    }
}
