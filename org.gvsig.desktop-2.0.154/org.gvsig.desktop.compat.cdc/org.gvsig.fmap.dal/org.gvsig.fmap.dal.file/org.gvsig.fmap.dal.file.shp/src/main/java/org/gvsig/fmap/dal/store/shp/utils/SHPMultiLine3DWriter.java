/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiCurve;
import org.gvsig.fmap.geom.aggregate.MultiLine;
import org.gvsig.fmap.geom.primitive.Curve;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;


/**
 *
 * @author fdiaz
 */
public class SHPMultiLine3DWriter implements SHPShapeWriter {
    private Geometry geometry;
	protected int m_type;
	protected int[] parts;
	protected Point[] points;
	private GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(SHPMultiLine3DWriter.class);

	/**
	 * Crea un nuevo SHPMultiLine.
	 */
	public SHPMultiLine3DWriter() {
		m_type = SHP.POLYLINE3D;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getShapeType()
	 */
	public int getShapeType() {
		return m_type;
	}

	/**
	 * @throws WriteException
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#write(ByteBuffer, IGeometry)
	 */
	public void write(ByteBuffer buffer) throws WriteException {
		Envelope env = geometry.getEnvelope();

		buffer.putDouble(env.getMinimum(0));
		buffer.putDouble(env.getMinimum(1));
		buffer.putDouble(env.getMaximum(0));
		buffer.putDouble(env.getMaximum(1));

		try {
            initialize(geometry);
        } catch (GeometryException e) {
            throw new WriteException("SHPMultiLine3D write", e);
        }

		double minM=Double.POSITIVE_INFINITY;
        double maxM=Double.NEGATIVE_INFINITY;
        double minZ=Double.POSITIVE_INFINITY;
        double maxZ=Double.NEGATIVE_INFINITY;
		int numParts = parts.length;
		int npoints = points.length;
		buffer.putInt(numParts);
		buffer.putInt(npoints);

		for (int i = 0; i < numParts; i++) {
			buffer.putInt(parts[i]);
		}

		for (int t = 0; t < npoints; t++) {
			Point point = points[t];
            buffer.putDouble(point.getX());
			buffer.putDouble(point.getY());
            double z = point.getCoordinateAt(Geometry.DIMENSIONS.Z);
            if(z<minZ){
                minZ = z;
            }
            if(z>maxZ){
                maxZ = z;
            }
            if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
                double m = point.getCoordinateAt(point.getDimension()-1);
                if (m < minM) {
                    minM = m;
                }
                if (m > maxM) {
                    maxM = m;
                }
            }
		}
        buffer.putDouble(minZ);
        buffer.putDouble(maxZ);
        for (int t = 0; t < npoints; t++) {
            Point point = points[t];
            buffer.putDouble(point.getCoordinateAt(Geometry.DIMENSIONS.Z));
        }
        if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
            buffer.putDouble(minM);
            buffer.putDouble(maxM);
            for (int t = 0; t < npoints; t++) {
                Point point = points[t];
                buffer.putDouble(point.getCoordinateAt(point.getDimension()-1));
            }
        }
	}

	/**
	 * @throws GeometryException
     *
     */
    public void initialize(Geometry g) throws GeometryException {

        ArrayList<Point> arrayPoints = new ArrayList<Point>();
        ArrayList<Integer> arrayParts = new ArrayList<Integer>();

        geometry = g;

        if(geometry instanceof Line){
            Line line = (Line)g;
            arrayParts.add(0);
            for (int i = 0; i < line.getNumVertices(); i++) {
                arrayPoints.add(line.getVertex(i));
            }
        } else {

            MultiLine multiLine = null;
            if (geometry instanceof MultiLine) {
                multiLine = (MultiLine) g;
            } else if (g instanceof MultiCurve) {
                multiLine = geomManager.createMultiLine(g.getGeometryType().getSubType());
                MultiCurve multiCurve = (MultiCurve) geometry;
                for (int i = 0; i < multiCurve.getPrimitivesNumber(); i++) {
                    Curve curve = (Curve) multiCurve.getPrimitiveAt(i);
                    if (curve instanceof Line) {
                        multiLine.addPrimitive(curve);
                    } else {
                        MultiLine lines = curve.toLines();
                        for (int j = 0; j < lines.getPrimitivesNumber(); j++) {
                            multiLine.addPrimitive(lines.getPrimitiveAt(j));
                        }
                    }
                }
            } else {
                multiLine = g.toLines();
            }

            int index = 0;
            arrayParts.add(index);
            for (int i = 0; i < multiLine.getPrimitivesNumber(); i++) {
                Line line = (Line) multiLine.getPrimitiveAt(i);
                for (int j = 0; j < line.getNumVertices(); j++) {
                    arrayPoints.add(line.getVertex(j));
                }
                if (i < multiLine.getPrimitivesNumber() - 1) {
                    index += line.getNumVertices();
                    arrayParts.add(index);
                }
            }
        }
        points = arrayPoints.toArray(new Point[0]);
        parts = new int[arrayParts.size()];
        for (int i = 0; i < parts.length; i++) {
            parts[i] = arrayParts.get(i);
        }
    }

    /**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getLength(int)
	 */
	public int getLength() {
		int numlines;
		int numpoints;
		int length;

		numlines = parts.length;
		numpoints = points.length;
		// 44 = Shape Type + Box + NumParts + NumPoints
		// (4 * numlines) = Parts
		// (numpoints * 16) = Points
		// 16 = ZMin + ZMax
		// (numpoints * 8) = Zarray
        length = 44 + (4 * numlines) + (numpoints * 16) + 16 + (numpoints * 8);
        if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
            // 16 = MMin + MMax
            // (numpoints * 8) = Marray
            length += 16 + (numpoints * 8);
        }

		return length;
	}
}
