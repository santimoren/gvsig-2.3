/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.awt.geom.Rectangle2D;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Elemento shape de tipo Pol�gono.
 *
 * @author Vicente Caballero Navarro
 */
public class SHPPolygon extends SHPMultiLine {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	private static final Logger logger = LoggerFactory.getLogger(SHPPolygon.class);

	/**
	 * Crea un nuevo SHPPolygon.
	 */
	public SHPPolygon() {
		m_type = SHP.POLYGON2D;
	}

	/**
	 * Crea un nuevo SHPPolygon.
	 *
	 * @param type Tipo de shape.
	 *
	 * @throws ShapefileException
	 */
	public SHPPolygon(int type) {
		if ((type != SHP.POLYGON2D) &&
				(type != SHP.POLYGONM) &&
				(type != SHP.POLYGON3D)) {
//			throw new ShapefileException("No es de tipo 5, 15, o 25");
		}

		m_type = type;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getShapeType()
	 */
	public int getShapeType() {
		return m_type;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#read(MappedByteBuffer, int)
	 */
	public synchronized Geometry read(MappedByteBuffer buffer, int type) {
		double minX = buffer.getDouble();
		double minY = buffer.getDouble();
		double maxX = buffer.getDouble();
		double maxY = buffer.getDouble();
		Rectangle2D rec = new Rectangle2D.Double(minX, minY, maxX - minX,
				maxY - maxY);
		int numParts = buffer.getInt();
		int numPoints = buffer.getInt();

		int[] partOffsets = new int[numParts];

		for (int i = 0; i < numParts; i++) {
			partOffsets[i] = buffer.getInt();
		}

		Point[] points = readPoints(buffer, numPoints);

		/* if (m_type == FConstant.SHAPE_TYPE_POLYGONZ) {
		   //z
		   buffer.position(buffer.position() + (2 * 8));
		   for (int t = 0; t < numPoints; t++) {
		       points[t].z = buffer.getDouble();
		   }
		   }
		 */
		int offset = 0;
		int start;
		int finish;
		int length;

		for (int part = 0; part < numParts; part++) {
			start = partOffsets[part];

			if (part == (numParts - 1)) {
				finish = numPoints;
			} else {
				finish = partOffsets[part + 1];
			}

			length = finish - start;

			Point[] pointsPart = new Point[length];

			for (int i = 0; i < length; i++) {
				pointsPart[i] = points[offset++];
			}
		}

		try {
			return geomManager.createSurface(getGeneralPathX(points, partOffsets), SUBTYPES.GEOM2D);
		} catch (CreateGeometryException e) {
			logger.error("Error creating a surface", e);
			return null;
		}
	}

	/**
	 * Lee los puntos del buffer.
	 *
	 * @param buffer
	 * @param numPoints N�mero de puntos.
	 *
	 * @return Vector de Puntos.
	 */
	private synchronized Point[] readPoints(final MappedByteBuffer buffer,
		final int numPoints) {
		Point[] points = new Point[numPoints];

		for (int t = 0; t < numPoints; t++) {
			try {
				points[t] = geomManager.createPoint(buffer.getDouble(), buffer.getDouble(), SUBTYPES.GEOM2D);
			} catch (CreateGeometryException e) {
				logger.error("Error creating a point", e);
			}
		}

		return points;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#write(ByteBuffer, IGeometry)
	 */
	public synchronized void write(ByteBuffer buffer, Geometry geometry) {
		Envelope env = geometry.getEnvelope();

		buffer.putDouble(env.getMinimum(0));
		buffer.putDouble(env.getMinimum(1));
		buffer.putDouble(env.getMaximum(0));
		buffer.putDouble(env.getMaximum(1));
		
		int nparts = parts.length;
		int npoints = points.length;

		buffer.putInt(nparts);
		buffer.putInt(npoints);

		for (int t = 0; t < nparts; t++) {
			buffer.putInt(parts[t]);
		}

		for (int t = 0; t < points.length; t++) {
			buffer.putDouble(points[t].getX());
			buffer.putDouble(points[t].getY());
		}

		if (m_type == SHP.POLYGON3D) {
			double[] zExtreame = SHP.getZMinMax(zs);
			if (Double.isNaN(zExtreame[0])) {
				buffer.putDouble(0.0);
				buffer.putDouble(0.0);
			} else {
				buffer.putDouble(zExtreame[0]);
				buffer.putDouble(zExtreame[1]);
			}
			for (int t = 0; t < npoints; t++) {
				double z = zs[t];
				if (Double.isNaN(z)) {
					buffer.putDouble(0.0);
				} else {
					buffer.putDouble(z);
				}
			}
		}

		if ((m_type == SHP.POLYGONM)) {
			buffer.putDouble(-10E40);
			buffer.putDouble(-10E40);

			for (int t = 0; t < npoints; t++) {
				buffer.putDouble(-10E40);
			}
		}
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getLength(com.iver.cit.gvsig.core.BasicShape.FGeometry)
	 */
	public synchronized int getLength(Geometry fgeometry) {
		int npoints = points.length;

		int length;

		if (m_type == SHP.POLYGON3D || m_type == SHP.POLYGONM) {
			length = 44 + (4 * parts.length) + (16 * npoints) + (8 * npoints) + 16;
		} else if (m_type == SHP.POLYGON2D) {
			length = 44 + (4 * parts.length) + (16 * npoints);
		} else {
			throw new IllegalStateException("Expected ShapeType of Polygon, got " + m_type);
		}

		return length;
	}
}
