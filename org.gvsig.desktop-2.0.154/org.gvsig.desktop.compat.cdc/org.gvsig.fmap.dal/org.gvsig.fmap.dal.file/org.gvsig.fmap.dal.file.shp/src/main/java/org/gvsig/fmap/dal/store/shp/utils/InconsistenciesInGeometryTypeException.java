
package org.gvsig.fmap.dal.store.shp.utils;

import org.gvsig.fmap.dal.exception.ReadException;


public class InconsistenciesInGeometryTypeException extends ReadException {

    private final static String MESSAGE_FORMAT = "Inconsistencies in geometry type '%(store)'.";
    private final static String MESSAGE_KEY = "InconsistenciesInGeometryTypeException";
    private static final long serialVersionUID = -1098020049568796722L;

    public InconsistenciesInGeometryTypeException(String store, Throwable cause) {
        super(MESSAGE_FORMAT, cause, MESSAGE_KEY, (int)serialVersionUID);
        setValue("store", store);
    }

}
