/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.utils.bigfile.BigByteBuffer2;


/**
 * @author fdiaz
 *
 */
public interface SHPReader {

    public Geometry readPoint(BigByteBuffer2 bb) throws CreateGeometryException;

    public Geometry readPoLyline(BigByteBuffer2 bb) throws CreateGeometryException, ReadException;

    public Geometry readPoLygon(BigByteBuffer2 bb) throws CreateGeometryException, GeometryOperationNotSupportedException, GeometryOperationException, ReadException;

    public Geometry readMultiPoint(BigByteBuffer2 bb) throws CreateGeometryException, ReadException;

}
