/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.aggregate.MultiPolygon;
import org.gvsig.fmap.geom.aggregate.MultiSurface;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.fmap.geom.primitive.Surface;
import org.gvsig.tools.exception.BaseException;

/**
 *
 *
 */
public class SHPPolygon3DWriter implements SHPShapeWriter {
    private Geometry geometry;
    private int m_type;
    private int[] parts;
    private Point[] points;

    private static final Logger logger = LoggerFactory.getLogger(SHPPolygon3DWriter.class);


	/**
	 * Crea un nuevo SHPPolygon.
	 */
	public SHPPolygon3DWriter() {
		m_type = SHP.POLYGON3D;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getShapeType()
	 */
	public int getShapeType() {
		return m_type;
	}

	/**
	 * @throws WriteException
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#write(ByteBuffer, IGeometry)
	 */
    public synchronized void write(ByteBuffer buffer) throws WriteException {
        Envelope env = geometry.getEnvelope();

        buffer.putDouble(env.getMinimum(0));
        buffer.putDouble(env.getMinimum(1));
        buffer.putDouble(env.getMaximum(0));
        buffer.putDouble(env.getMaximum(1));

        try {
            initialize(geometry);
        } catch (BaseException e) {
            throw new WriteException("SHPPolygon2DWriter", e);
        }

        double minM = Double.POSITIVE_INFINITY;
        double maxM = Double.NEGATIVE_INFINITY;
        double minZ = Double.POSITIVE_INFINITY;
        double maxZ = Double.NEGATIVE_INFINITY;
        int numParts = parts.length;
        int npoints = points.length;

        buffer.putInt(numParts);
        buffer.putInt(npoints);

        for (int i = 0; i < numParts; i++) {
            buffer.putInt(parts[i]);
        }

        for (int t = 0; t < npoints; t++) {
            Point point = points[t];
            buffer.putDouble(point.getX());
            buffer.putDouble(point.getY());
            double z = point.getCoordinateAt(Geometry.DIMENSIONS.Z);
            if (z < minZ) {
                minZ = z;
            }
            if (z > maxZ) {
                maxZ = z;
            }
            if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
                double m = point.getCoordinateAt(point.getDimension()-1);
                if (m < minM) {
                    minM = m;
                }
                if (m > maxM) {
                    maxM = m;
                }
            }
        }
        buffer.putDouble(minZ);
        buffer.putDouble(maxZ);
        for (int t = 0; t < npoints; t++) {
            Point point = points[t];
            buffer.putDouble(points[t].getCoordinateAt(Geometry.DIMENSIONS.Z));
        }

        if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
            buffer.putDouble(minM);
            buffer.putDouble(maxM);
            for (int t = 0; t < npoints; t++) {
                Point point = points[t];
                buffer.putDouble(points[t].getCoordinateAt(point.getDimension()-1));
            }
        }
    }

	   /**
     * @throws GeometryException
	 * @throws GeometryOperationException
	 * @throws GeometryOperationNotSupportedException
     *
     */
    public void initialize(Geometry g) throws GeometryException, GeometryOperationNotSupportedException, GeometryOperationException {

        geometry = g;
        GeometryManager geomManager = GeometryLocator.getGeometryManager();

        ArrayList<Point> arrayPoints = new ArrayList<Point>();
        ArrayList<Integer> arrayParts = new ArrayList<Integer>();

        if(geometry instanceof Polygon){
            Polygon polygon = (Polygon)geometry;
            polygon.ensureOrientation(false);
            int index = 0;
            arrayParts.add(index);
            for (int i = 0; i < polygon.getNumVertices(); i++) {
                arrayPoints.add(polygon.getVertex(i));
            }
            if(polygon.getNumInteriorRings()!=0){
                index += polygon.getNumVertices();
                arrayParts.add(index);
            }
            for (int r=0; r<polygon.getNumInteriorRings(); r++){
                Ring ring = polygon.getInteriorRing(r);
                ring.ensureOrientation(true);
                for (int i = 0; i < ring.getNumVertices(); i++) {
                    arrayPoints.add(ring.getVertex(i));
                }
                if(r<polygon.getNumInteriorRings()-1){
                    index += ring.getNumVertices();
                    arrayParts.add(index);
                }
            }
        } else {

            MultiPolygon multiPolygon = null;
            if (geometry instanceof MultiPolygon) {
                multiPolygon = (MultiPolygon) geometry;
            } else if (geometry instanceof MultiSurface) {
                multiPolygon = geomManager.createMultiPolygon(geometry.getGeometryType().getSubType());
                MultiSurface multiSurface = (MultiSurface) geometry;
                for (int i = 0; i < multiSurface.getPrimitivesNumber(); i++) {
                    Surface surface = (Surface) multiSurface.getPrimitiveAt(i);
                    if (surface instanceof Polygon) {
                        Polygon polygon = (Polygon)surface;
                        polygon.ensureOrientation(false);
                        multiPolygon.addPrimitive(surface);
                    } else {
                        MultiPolygon polygons = surface.toPolygons();
                        for (int j = 0; j < polygons.getPrimitivesNumber(); j++) {
                            Primitive polygon = polygons.getPrimitiveAt(j);
                            polygon.ensureOrientation(false);
                            multiPolygon.addPrimitive(polygon);
                        }
                    }
                }
            } else {
                multiPolygon = geometry.toPolygons();
            }

            arrayParts.add(0);
            int index = 0;
            for (int i = 0; i < multiPolygon.getPrimitivesNumber(); i++) {
                Polygon polygon = (Polygon) multiPolygon.getPrimitiveAt(i);

                polygon.ensureOrientation(false);
                for (int j = 0; j < polygon.getNumVertices(); j++) {
                    arrayPoints.add(polygon.getVertex(j));
                }
                if(polygon.getNumInteriorRings()!=0 || i<multiPolygon.getPrimitivesNumber()-1){
                    index += polygon.getNumVertices();
                    arrayParts.add(index);
                }
                for (int r=0; r<polygon.getNumInteriorRings(); r++){
                    Ring ring = polygon.getInteriorRing(r);
                    ring.ensureOrientation(true);
                    for (int j = 0; j < ring.getNumVertices(); j++) {
                        arrayPoints.add(ring.getVertex(j));
                    }
                    if((i<multiPolygon.getPrimitivesNumber()-1) || (r<polygon.getNumInteriorRings()-1)){
                        index += ring.getNumVertices();
                        arrayParts.add(index);
                    }
                }
            }
        }
        points = arrayPoints.toArray(new Point[0]);
        parts = new int[arrayParts.size()];
        for (int i = 0; i < parts.length; i++) {
            parts[i] = arrayParts.get(i);
        }
    }


	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getLength(com.iver.cit.gvsig.core.BasicShape.FGeometry)
	 */
	public synchronized int getLength() {
        int numlines;
        int numpoints;
        int length;

        numlines = parts.length;
        numpoints = points.length;
        // 44 = Shape Type + Box + NumParts + NumPoints
        // (4 * numlines) = Parts
        // (numpoints * 16) = Points
        // 16 = ZMin + ZMax
        // (numpoints * 8) = Zarray
        length = 44 + (4 * numlines) + (numpoints * 16) + 16 + (numpoints * 8);
        if (geometry.getGeometryType().getSubType() == Geometry.SUBTYPES.GEOM3DM) {
            // 16 = MMin + MMax
            // (numpoints * 8) = Marray
            length += 16 + (numpoints * 8);
        }

        return length;
    }
}
