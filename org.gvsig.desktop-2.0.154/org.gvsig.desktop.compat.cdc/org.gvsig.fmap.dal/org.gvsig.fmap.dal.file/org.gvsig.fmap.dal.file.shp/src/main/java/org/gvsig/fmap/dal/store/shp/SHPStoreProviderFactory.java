/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.DataStoreProvider;
import org.gvsig.fmap.dal.DataStoreProviderFactory;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProviderFactory;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.dal.store.dbf.utils.DbaseFile;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dynobject.DynObject;

public class SHPStoreProviderFactory extends AbstractFeatureStoreProviderFactory implements FeatureStoreProviderFactory{

    private static final Logger logger = LoggerFactory.getLogger(SHPStoreProviderFactory.class);

    public static final String DEFAULT_GEOMETRY_FIELD_NAME = "GEOMETRY";
    public static final int DEFAULT_GEOMETRY_TYPE = Geometry.TYPES.SURFACE;
    public static final int DEFAULT_GEOMETRY_SUBTYPE = Geometry.SUBTYPES.GEOM2D;

	protected SHPStoreProviderFactory(String name, String description) {
		super(name, description);
	}

	public DataStoreProvider createProvider(DataParameters parameters,
			DataStoreProviderServices providerServices)
			throws InitializeException {
		return new SHPStoreProvider((SHPStoreParameters) parameters, providerServices);
	}

	public DynObject createParameters() {
		return new SHPStoreParameters();
	}

	public int allowCreate() {
		return YES;
	}

	public int allowWrite() {
		return YES;
	}

	public int allowRead() {
		return YES;
	}

	public int hasRasterSupport() {
		return NO;
	}

	public int hasTabularSupport() {
		return YES;
	}

	public int hasVectorialSupport() {
		return YES;
	}

	public int allowMultipleGeometryTypes() {
		return NO;
	}


    public int allowEditableFeatureType() {
        return YES;
    }

    public int useLocalIndexesCanImprovePerformance() {
        return YES;
    }

    public List getSupportedDataTypes() {

        DataTypesManager manager = ToolsLocator.getDataTypesManager();

        List resp = new ArrayList<Integer>();
        resp.add(manager.get(DataTypes.STRING));
        resp.add(manager.get(DataTypes.INT));
        resp.add(manager.get(DataTypes.FLOAT));
        resp.add(manager.get(DataTypes.DATE));
        resp.add(manager.get(DataTypes.GEOMETRY));
        return resp;
    }

    public List getSupportedGeometryTypesSubtypes() {
        // null means all supported
        int[] item = null;
        List resp = new ArrayList<int[]>();
        // ====================================== 2d
        item = new int[2];
        item[0] = Geometry.TYPES.POINT;
        item[1] = Geometry.SUBTYPES.GEOM2D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.CURVE;
        item[1] = Geometry.SUBTYPES.GEOM2D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.SURFACE;
        item[1] = Geometry.SUBTYPES.GEOM2D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.MULTIPOINT;
        item[1] = Geometry.SUBTYPES.GEOM2D;
        resp.add(item);
        // ====================================== 3d
        item = new int[2];
        item[0] = Geometry.TYPES.POINT;
        item[1] = Geometry.SUBTYPES.GEOM3D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.CURVE;
        item[1] = Geometry.SUBTYPES.GEOM3D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.SURFACE;
        item[1] = Geometry.SUBTYPES.GEOM3D;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.MULTIPOINT;
        item[1] = Geometry.SUBTYPES.GEOM3D;
        resp.add(item);
        // ====================================== 2dm
        item = new int[2];
        item[0] = Geometry.TYPES.POINT;
        item[1] = Geometry.SUBTYPES.GEOM2DM;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.CURVE;
        item[1] = Geometry.SUBTYPES.GEOM2DM;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.SURFACE;
        item[1] = Geometry.SUBTYPES.GEOM2DM;
        resp.add(item);

        item = new int[2];
        item[0] = Geometry.TYPES.MULTIPOINT;
        item[1] = Geometry.SUBTYPES.GEOM2DM;
        resp.add(item);
        // ======================================

        return resp;
    }

    public boolean allowsMandatoryAttributes() {
        return false;
    }

    public boolean allowsPrimaryKeyAttributes() {
        return false;
    }

    /**
     * @return  dummy feature type. Must be overridden by subclasses
     *
     */
    public FeatureType createDefaultFeatureType() {
        DataManager dm = DALLocator.getDataManager();
        EditableFeatureType eft = dm.createFeatureType();

        EditableFeatureAttributeDescriptor efatd =
            eft.add(DEFAULT_GEOMETRY_FIELD_NAME, DataTypes.GEOMETRY);

        GeometryType gt = null;
        try {
            gt = GeometryLocator.getGeometryManager().getGeometryType(
                DEFAULT_GEOMETRY_TYPE,
                DEFAULT_GEOMETRY_SUBTYPE);
            efatd.setGeometryType(gt);
        } catch (Exception ex) {
            logger.info("Error while setting geom type: " + ex.getMessage());
            logger.info("Trying deprecated way...");
            efatd.setGeometryType(DEFAULT_GEOMETRY_TYPE);
            efatd.setGeometrySubType(DEFAULT_GEOMETRY_SUBTYPE);
        }
        eft.setDefaultGeometryAttributeName(DEFAULT_GEOMETRY_FIELD_NAME);
        return eft.getNotEditableCopy();
    }

	@Override
	public int getMaxAttributeNameSize() {
	    return DbaseFile.MAX_FIELD_NAME_LENGTH;
	}
}
