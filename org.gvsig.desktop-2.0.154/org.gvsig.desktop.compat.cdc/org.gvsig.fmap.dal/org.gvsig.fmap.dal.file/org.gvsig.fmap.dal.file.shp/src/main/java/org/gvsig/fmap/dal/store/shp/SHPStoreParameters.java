/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.shp;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.cresques.cts.ICRSFactory;
import org.cresques.cts.IProjection;

import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.store.dbf.DBFStoreParameters;
import org.gvsig.fmap.dal.store.shp.utils.SHP;

public class SHPStoreParameters extends DBFStoreParameters {

    public static final String PARAMETERS_DEFINITION_NAME = "SHPStoreParameters";

    private static final String SHXFILE_PARAMTER_NAME = "shxfile";
    private static final String SHPFILE_PARAMTER_NAME = "shpfile";
    private static final String CRS_PARAMTER_NAME = "CRS";
    private static final String USE_NULLGEOMETRY_PARAMTER_NAME = "useNullGeometry";
    private static final String ALLOW_INCONSISTENCIES_IN_GEOMETRY_TYPE = "allowInconsistenciesInGeometryType";
    private static final String LOAD_CORRUPT_GEOMETRIES_AS_NULL = "loadCorruptGeometriesAsNull";
    private static final String FIX_LINEARRINGS = "fixLinearRings";

	public SHPStoreParameters() {
		this(PARAMETERS_DEFINITION_NAME);
	}

	@Override
	public void validate() throws ValidateDataParametersException {
		fixParameters();
		super.validate();
	}

	public void fixParameters() {
		File file = this.getSHPFile();
		if( file!=null ) {
			if (this.getDBFFile() == null){
				this.setDBFFile(SHP.getDbfFile(file));
			}
			if (this.getSHXFile() == null) {
				this.setSHXFile(SHP.getShxFile(file));
			}
		}
	}

	public SHPStoreParameters(String parametersDefinitionName) {
		super(parametersDefinitionName, SHPStoreProvider.NAME);
	}

	public boolean isValid() {
		return super.isValid() && (this.getSHPFileName() != null);
	}

	public File getFile() {
		return this.getSHPFile();
	}

	public void setFile(File file) {
		this.setSHPFile(file);
	}
	public void setFile(String fileName) {
		this.setSHPFile(fileName);
	}


	public String getSHPFileName() {
		if( this.getSHPFile()==null ) {
			return null;
		}
		return this.getSHPFile().getAbsolutePath();
	}

	public File getSHPFile() {
		return (File) this.getDynValue(SHPFILE_PARAMTER_NAME);
	}

	public void setSHPFile(File file) {
		this.setDynValue(SHPFILE_PARAMTER_NAME, file);
		if (this.getDBFFile() == null){
			this.setDBFFile(SHP.getDbfFile(file));
		}
		if (this.getSHXFile() == null) {
			this.setSHXFile(SHP.getShxFile(file));
		}
        if (getCRS() == null){
            String wktEsri = loadPrj(file);
            if (wktEsri != null) {
                IProjection proj = CRSFactory.getCRSFactory().get(ICRSFactory.FORMAT_WKT_ESRI, wktEsri);
                setCRS(proj);
            }
        }

	}

	public void setSHPFile(String fileName) {
		this.setDynValue(SHPFILE_PARAMTER_NAME, fileName);
		File file = (File) this.getDynValue(SHPFILE_PARAMTER_NAME);
		if (this.getDBFFile() == null){
			this.setDBFFile(SHP.getDbfFile(file));
		}
		if (this.getSHXFile() == null) {
			this.setSHXFile(SHP.getShxFile(file));
		}
        if (getCRS() == null){
            String wktEsri = loadPrj(file);
            if (wktEsri != null) {
                IProjection proj = CRSFactory.getCRSFactory().get(ICRSFactory.FORMAT_WKT_ESRI, wktEsri);
                setCRS(proj);
            }
        }
	}

	private String loadPrj(File shpFile){
        File prjFile = SHP.getPrjFile(shpFile);
        if (prjFile.exists()) {
            try {
                return FileUtils.readFileToString(prjFile);
            } catch (IOException e) {
                return null;
            }
        }
        return null;
	}

	public String getSHXFileName() {
		if( this.getSHXFile()==null ) {
			return null;
		}
		return this.getSHXFile().getPath();
	}

	public File getSHXFile() {
		return (File) this.getDynValue(SHXFILE_PARAMTER_NAME);
	}
	public void setSHXFile(File file) {
		this.setDynValue(SHXFILE_PARAMTER_NAME, file);
	}

	public void setSHXFile(String fileName) {
		this.setDynValue(SHXFILE_PARAMTER_NAME, fileName);
	}

	public void setCRS(IProjection srs) {
		setDynValue(CRS_PARAMTER_NAME, srs);
	}

	public void setCRS(String srs) {
		setDynValue(CRS_PARAMTER_NAME, srs);
	}

	public IProjection getCRS() {
		return (IProjection) getDynValue(CRS_PARAMTER_NAME);
	}

        public boolean getUseNullGeometry() {
		return BooleanUtils.isTrue((Boolean) getDynValue(USE_NULLGEOMETRY_PARAMTER_NAME));
	}

        public boolean getAllowInconsistenciesInGeometryType() {
		return BooleanUtils.isTrue((Boolean) getDynValue(ALLOW_INCONSISTENCIES_IN_GEOMETRY_TYPE));
        }
        
        public boolean getLoadCorruptGeometriesAsNull() {
		return BooleanUtils.isTrue((Boolean) getDynValue(LOAD_CORRUPT_GEOMETRIES_AS_NULL));
        }
        
        public boolean getFixLinearRings() {
		return BooleanUtils.isTrue((Boolean) getDynValue(FIX_LINEARRINGS));
        }
}
