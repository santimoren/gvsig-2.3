/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.io.File;



/**
 * Clase con las constantes que representan los diferentes tipos de Shape y
 * m�todos est�ticos relativos a los shapes.
 *
 * @author Vicente Caballero Navarro
 */
public class SHP {

    public static final int NULL = 0;
    public static final int POINT2D = 1;
    public static final int POLYLINE2D = 3;
    public static final int POLYGON2D = 5;
    public static final int MULTIPOINT2D = 8;
    public static final int POINT3D = 11;
    public static final int POLYLINE3D = 13;
    public static final int POLYGON3D = 15;
    public static final int MULTIPOINT3D = 18;
	public final static int POINTM = 21;
	public final static int POLYLINEM = 23;
	public final static int POLYGONM = 25;
	public final static int MULTIPOINTM = 28;
    /**
     * Crea a partir del tipo de geometr�a un shape writer del tipo m�s adecuado.
     *
     * @param type Tipo de geometr�a.
     *
     * @return ShapeWriter m�s adecuado.
     *
     * @throws ShapefileException Se lanza cuando es causada por la creaci�n del shape.
     */
    public static SHPShapeWriter create(int type) {
        SHPShapeWriter writer;

        switch (type) {
        case NULL:
            writer = new SHPNullWriter();
            break;

        case POINT2D:
        case POINT3D:
        case POINTM:
            writer = new SHPPointWriter(type);
            break;

        case POLYLINE2D:
            writer = new SHPMultiLine2DWriter();
            break;
        case POLYLINE3D:
            writer = new SHPMultiLine3DWriter();
            break;
        case POLYLINEM:
            writer = new SHPMultiLine2DMWriter();
            break;

        case POLYGON2D:
            writer = new SHPPolygon2DWriter();
            break;
        case POLYGON3D:
            writer = new SHPPolygon3DWriter();
            break;
        case POLYGONM:
            writer = new SHPPolygon2DMWriter();
            break;

        case MULTIPOINT2D:
        case MULTIPOINT3D:
        case MULTIPOINTM:
            writer = new SHPMultiPointWriter(type);
            break;

        default:
            writer = null;
        }

        return writer;
    }

    public static String getTypeName(int type) {

        switch (type) {
        case NULL:
            return "NULL";
        case POINT2D:
            return "POINT2D";
        case POINT3D:
            return "POINT3D";
        case POINTM:
            return "POINTM";
        case POLYLINE2D:
            return "POLYLINE2D";
        case POLYLINE3D:
            return "POLYLINE3D";
        case POLYLINEM:
            return "POLYLINEM";
        case POLYGON2D:
            return "POLYGON2D";
        case POLYGON3D:
            return "POLYGON3D";
        case POLYGONM:
            return "POLYGONM";
        case MULTIPOINT2D:
            return "MULTIPOINT2D";
        case MULTIPOINT3D:
            return "MULTIPOINT3D";
        case MULTIPOINTM:
            return "MULTIPOINTM";
        default:
            return Integer.toString(type);
        }
    }

    /**
     * Devuelve un array con dos doubles, el primero representa el m�nimo valor
     * y el segundo el m�ximo de entre los valores que se pasan como par�metro
     * en forma de array.
     *
     * @param zs Valores a comprobar.
     *
     * @return Array de doubles con el valor m�nimo y el valor m�ximo.
     */
    public static double[] getZMinMax(double[] zs) {
        if (zs == null) {
            return null;
        }

        double min = Double.MAX_VALUE;
        double max = Double.NEGATIVE_INFINITY;

        for (int i = 0; i < zs.length; i++) {
            if (zs[i] > max) {
                max = zs[i];
            }

            if (zs[i] < min) {
                min = zs[i];
            }
        }

        return new double[] { min, max };
    }
    public static File getDbfFile(File shpFile){
    	String str = shpFile.getAbsolutePath();
		File directory=shpFile.getParentFile();
		File[] files=new File[0];
		if (directory!=null){
			MyFileFilter myFileFilter = new MyFileFilter(str);
			files=directory.listFiles(myFileFilter);
		}
		String[] ends=new String[] {"dbf","DBF","Dbf","dBf","DBf","dbF","DbF","dBF"};
		File dbfFile=findEnd(str,files,ends);
		return dbfFile;
    }

    public static File getShpFile(File dbfFile){
    	String str = dbfFile.getAbsolutePath();
		File directory=dbfFile.getParentFile();
		File[] files=new File[0];
		if (directory!=null){
			MyFileFilter myFileFilter = new MyFileFilter(str);
			files=directory.listFiles(myFileFilter);
		}
		String[] ends=new String[] {"shp","SHP","Shp","sHp","SHp","shP","ShP","sHP"};
		File shpFile=findEnd(str,files,ends);
		return shpFile;
    }

    public static File getShxFile(File shpFile){
    	String str = shpFile.getAbsolutePath();
		File directory=shpFile.getParentFile();
		File[] files=new File[0];
		if (directory!=null){
			MyFileFilter myFileFilter = new MyFileFilter(str);
			files=directory.listFiles(myFileFilter);
		}
		String[] ends=new String[] {"shx","SHX","Shx","sHx","SHx","shX","ShX","sHX"};
		File shxFile=findEnd(str,files,ends);
		return shxFile;
    }

    public static File getPrjFile(File shpFile) {
		String str = shpFile.getAbsolutePath();
		File directory = shpFile.getParentFile();
		File[] files = new File[0];
		if (directory != null) {
			MyFileFilter myFileFilter = new MyFileFilter(str);
			files = directory.listFiles(myFileFilter);
		}
		String[] ends = new String[] { "prj", "PRJ", "Prj", "pRj", "PRj",
				"prJ", "PrJ", "pRJ" };
		File prjFile = findEnd(str, files, ends);
		return prjFile;
	}

    private static File findEnd(String str,File[] files, String[] ends) {
    	for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[0])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[1])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[2])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[3])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[4])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[5])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[6])) {
				return dbfFile;
			}
		}
		for (int i=0;i<files.length;i++) {
			File dbfFile=files[i];
			if (dbfFile.getAbsolutePath().endsWith(ends[7])) {
				return dbfFile;
			}
		}
		return new File(removeExtension(str)+"."+ends[0]);
    }

    private static String removeExtension(String fname){
    	int i = fname.lastIndexOf(".");
    	if (i<0){
    		return fname;
    	}
    	return fname.substring(0,i);
    }

}
