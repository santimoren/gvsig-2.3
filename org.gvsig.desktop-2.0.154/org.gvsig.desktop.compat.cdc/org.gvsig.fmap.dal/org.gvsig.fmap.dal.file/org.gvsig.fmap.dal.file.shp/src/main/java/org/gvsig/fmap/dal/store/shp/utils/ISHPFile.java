/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gvsig.fmap.dal.store.shp.utils;

import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Envelope;

/**
 *
 * @author jjdelcerro
 */
public interface ISHPFile {

    boolean canWriteGeometry(int gvSIGgeometryType);

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.fmap.dal.Resource#doClose()
     */
    void close() throws CloseException;

    /**
     * Gets the geometry bbox with the index provided.
     * Set to synchronized to prevent concurrent threads issue (?)
     *
     * @param featureIndex
     * @return
     * @throws ReadException
     * @throws CreateEnvelopeException
     * @throws CreateGeometryException
     */
    Envelope getBoundingBox(long featureIndex) throws ReadException, CreateEnvelopeException, CreateGeometryException;

    Envelope getFullExtent() throws ReadException;

    /**
     * Gets the geometry with the index provided.
     * Set to synchronized to prevent concurrent threads issue (?)
     *
     * @param position
     * @return
     * @throws ReadException
     * @throws CreateGeometryException
     */
    Geometry getGeometry(long position) throws ReadException, CreateGeometryException;

    int getGeometrySubType() throws ReadException;

    int getGeometryType() throws ReadException;

    /**
     * @return
     */
    String getSRSParameters();

    boolean isEditable();

    boolean isOpen();

    void open() throws DataException;

    void setUseNullGeometry(boolean useNullGeometry);
    
}
