/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryException;
import org.gvsig.fmap.geom.aggregate.MultiPoint;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.primitive.Point;

/**
 * Elemento shape de tipo multipunto.
 *
 */
public class SHPMultiPointWriter implements SHPShapeWriter {
	private static final Logger logger = LoggerFactory.getLogger(SHPMultiPointWriter.class);
	private int m_type;
	private MultiPoint multipoint;
	private int numpoints;

	/**
	 * Crea un nuevo SHPMultiPoint.
	 *
	 * @param type Tipo de multipunto.
	 *
	 * @throws ShapefileException
	 */
	public SHPMultiPointWriter(int type) {
		m_type = type;
	}

	/**
	 * Devuelve el tipo de multipoint en concreto.
	 *
	 * @return Tipo de multipoint.
	 */
	public int getShapeType() {
		return m_type;
	}

	/**
	 * @Deprecated Use SHPReader
	 */
	@Deprecated
	public Geometry read(MappedByteBuffer buffer, int type) {
		return null;
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#write(ByteBuffer, IGeometry)
	 */
	public void write(ByteBuffer buffer) {
        Envelope env = multipoint.getEnvelope();

        buffer.putDouble(env.getMinimum(0));
        buffer.putDouble(env.getMinimum(1));
        buffer.putDouble(env.getMaximum(0));
        buffer.putDouble(env.getMaximum(1));

        buffer.putInt(numpoints);

        for (int i = 0; i < numpoints; i++) {
            Point point = (Point) multipoint.getPrimitiveAt(i);
            buffer.putDouble(point.getX());
            buffer.putDouble(point.getY());
        }

        if (m_type == SHP.MULTIPOINT3D) {
            putExtraCoordinateValuesInBuffer(buffer, Geometry.DIMENSIONS.Z);
            putExtraCoordinateValuesInBuffer(buffer, multipoint.getDimension()-1);
        }

        if (m_type == SHP.MULTIPOINTM) {
            putExtraCoordinateValuesInBuffer(buffer, multipoint.getDimension()-1);
        }
	}

    /**
     * @param buffer
     * @param coordinate
     */
    private void putExtraCoordinateValuesInBuffer(ByteBuffer buffer, int coordinate) {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        List<Double> values = new ArrayList<Double>();
        for (int i = 0; i < numpoints; i++) {
            Point point = (Point) multipoint.getPrimitiveAt(i);
            double value = point.getCoordinateAt(coordinate);
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
            values.add(value);
        }
        buffer.putDouble(min);
        buffer.putDouble(max);
        for (Iterator iterator = values.iterator(); iterator.hasNext();) {
            Double value = (Double) iterator.next();
            buffer.putDouble(value);
        }
    }

    /**
	 * @see com.iver.cit.gvsig.fmap.shp.SHPShape#getLength(com.iver.cit.gvsig.core.BasicShape.FGeometry)
	 */
	public int getLength() {
		int length;

		if (m_type == SHP.MULTIPOINT2D) {
			// two doubles per coord (16 * numgeoms) + 40 for header
			length = (numpoints * 16) + 40;
		} else if (m_type == SHP.MULTIPOINTM) {
			// add the additional MMin, MMax for 16, then 8 per measure
			length = (numpoints * 16) + 40 + 16 + (8 * numpoints);
		} else if (m_type == SHP.MULTIPOINT3D) {
			// add the additional ZMin,ZMax, plus 8 per Z
		    // add the additional MMin, MMax for 16, then 8 per measure
			length = (numpoints * 16) + 40 + 16 + (8 * numpoints) + 16 + (8 * numpoints);
		} else {
			throw new IllegalStateException("Expected ShapeType of Multipoint, got " +
				m_type);
		}

		return length;
	}

    /* (non-Javadoc)
     * @see org.gvsig.fmap.dal.store.shp.utils.SHPShapeWriter#initialize(org.gvsig.fmap.geom.Geometry)
     */
    public void initialize(Geometry geometry) throws GeometryException {
        multipoint = (MultiPoint) geometry;
        numpoints = multipoint.getPrimitivesNumber();
    }
}
