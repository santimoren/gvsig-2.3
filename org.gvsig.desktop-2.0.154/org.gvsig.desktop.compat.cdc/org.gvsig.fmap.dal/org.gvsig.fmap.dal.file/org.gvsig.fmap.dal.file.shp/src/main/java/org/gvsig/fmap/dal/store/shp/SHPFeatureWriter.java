/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.shp;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.store.dbf.DBFFeatureWriter;
import org.gvsig.fmap.dal.store.dbf.DBFStoreParameters;
import org.gvsig.fmap.dal.store.shp.utils.SHPFileWrite;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;

public class SHPFeatureWriter extends DBFFeatureWriter {

    private static final GeometryManager geomManager = GeometryLocator
        .getGeometryManager();
    private static final Logger logger = LoggerFactory
        .getLogger(GeometryManager.class);

    private SHPFileWrite shpWrite;
    private Envelope envelope = null;
    private int[] supportedGeometryTypes;
    private int fileSize;
    private FeatureType shpFeatureType;

    protected SHPFeatureWriter(String name) {
        super(name);
    }

    public void begin(DBFStoreParameters dbfParameters,
        FeatureType featureType, FeatureType dbfFeatureType, long numRows)
        throws DataException {
        SHPStoreParameters shpParameters = (SHPStoreParameters) dbfParameters;
        File shpFile = shpParameters.getSHPFile();
        File shxFile = shpParameters.getSHXFile();

        FileChannel shpChannel = null;
        FileChannel shxChannel = null;

        try {
            shpChannel =
                (FileChannel) getWriteChannel(shpFile.getAbsolutePath());
            shxChannel =
                (FileChannel) getWriteChannel(shxFile.getAbsolutePath());
        } catch (IOException e) {
            throw new WriteException(this.name, e);
        }

        shpWrite = new SHPFileWrite(shpChannel, shxChannel);
        int shapeType = getShapeTypeAndSetSupportedGeometries(featureType);
        try {
            shpWrite.writeHeaders(
                geomManager.createEnvelope(0, 0, 0, 0, featureType.getDefaultGeometryAttribute().getGeomType().getSubType()),
                shapeType, 0, 0);
        } catch (CreateEnvelopeException e) {
            logger.error("Error creating the envelope", e);
        }

        this.shpFeatureType = featureType;
        super.begin(dbfParameters, dbfFeatureType, numRows);

    }

    private int getShapeTypeAndSetSupportedGeometries(FeatureType featureType) {

        FeatureAttributeDescriptor geometryAttr =
            featureType.getAttributeDescriptor(featureType
                .getDefaultGeometryAttributeIndex());
        int gvSIG_geometryType;
        int gvSIG_geometrySubType;
        try{
            GeometryType geomType = geometryAttr.getGeomType();
            gvSIG_geometryType = geomType.getType();
            gvSIG_geometrySubType = geomType.getSubType();
        } catch(Exception e){
            gvSIG_geometryType = geometryAttr.getGeometryType();
            gvSIG_geometrySubType = geometryAttr.getGeometrySubType();
        }
        this.setSupportedGeometryTypes(gvSIG_geometryType);
        int shapeType = 0;
        shapeType =
            shpWrite.getShapeType(gvSIG_geometryType, gvSIG_geometrySubType);
        return shapeType;
    }

    public void dispose() {
        super.dispose();
        this.envelope = null;
        this.shpWrite = null;
    }

    public void end() throws DataException {
        if (envelope == null) {
            try {
                envelope =
                    geomManager.createEnvelope(0, 0, 0, 0, SUBTYPES.GEOM2D);
            } catch (CreateEnvelopeException e) {
                logger.error("Error creating the envelope", e);
            }
        }
        int shapeType = getShapeTypeAndSetSupportedGeometries(shpFeatureType);
        shpWrite.writeHeaders(envelope, shapeType, super.getRowCount(),
            fileSize);
        super.end();
        shpWrite.close();
    }

    public void append(Feature feature) throws DataException {

        Geometry theGeom = feature.getDefaultGeometry();
        if (theGeom != null) {
            if (!canWriteGeometry(theGeom.getType())) {
                throw new WriteException(this.name, // FIXME Excepcion correcta
                    new RuntimeException("UnsupportedGeometryType: "
                        + theGeom.getGeometryType().getName()));
            }
            super.append(feature);
            fileSize = shpWrite.writeIGeometry(theGeom);
            Envelope envelope = theGeom.getEnvelope();
            if (envelope != null) {
                if (this.envelope != null) {
                    this.envelope.add(envelope);
                } else {
                    this.envelope = envelope;
                }
            }
        } else {
            super.append(feature);
            fileSize = shpWrite.writeIGeometry(theGeom);
        }
    }

    private void setSupportedGeometryTypes(int gvSIG_geometryType) {
        switch (gvSIG_geometryType) {
        case Geometry.TYPES.POINT:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.POINT, Geometry.TYPES.NULL };
            break;
        case Geometry.TYPES.MULTIPOINT:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.MULTIPOINT, Geometry.TYPES.NULL };
            break;
        case Geometry.TYPES.MULTICURVE:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.CURVE, Geometry.TYPES.LINE, Geometry.TYPES.ELLIPSE,
                    Geometry.TYPES.ARC, Geometry.TYPES.CIRCLE,
                    Geometry.TYPES.ELLIPSE, Geometry.TYPES.MULTICURVE, Geometry.TYPES.MULTILINE,
                    Geometry.TYPES.SPLINE, Geometry.TYPES.NULL };
            break;
        case Geometry.TYPES.MULTISURFACE:
            supportedGeometryTypes =
                new int[] { Geometry.TYPES.SURFACE, Geometry.TYPES.POLYGON, Geometry.TYPES.CIRCLE,
                    Geometry.TYPES.ELLIPSE, Geometry.TYPES.MULTISURFACE, Geometry.TYPES.MULTIPOLYGON,
                    Geometry.TYPES.SPLINE, Geometry.TYPES.NULL };  // ��� SPLINE ???
            break;

        default:
            supportedGeometryTypes = new int[] {};
        }
    }

    public boolean canWriteGeometry(int gvSIGgeometryType) {
        for (int i = 0; i < supportedGeometryTypes.length; i++) {
            if (gvSIGgeometryType == supportedGeometryTypes[i]) {
                return true;
            }
        }
        return false;
    }

    public void begin(DBFStoreParameters storeParameters,
        FeatureType featureType, long numRows) throws DataException {
        throw new UnsupportedOperationException();
    }

}
