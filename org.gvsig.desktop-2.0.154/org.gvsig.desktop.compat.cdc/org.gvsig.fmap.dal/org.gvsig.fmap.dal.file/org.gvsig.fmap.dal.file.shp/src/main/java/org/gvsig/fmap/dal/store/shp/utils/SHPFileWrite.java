/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.exception.WriteException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.exception.BaseException;

public class SHPFileWrite {

    private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
    private static final Logger logger = LoggerFactory.getLogger(SHPFileWrite.class);
    private SHPShapeWriter m_shape = null;
    private ByteBuffer m_bb = null;
    private ByteBuffer m_indexBuffer = null;
    private int m_pos = 0;
    private int m_offset;
    private int m_type = SHP.NULL;
    private int m_cnt;
    private FileChannel shpChannel;
    private FileChannel shxChannel;
    private int inconsistenciesInGeometryTypeCounter = 0;

    public SHPFileWrite(FileChannel shpChannel, FileChannel shxChannel) {
        this.shpChannel = shpChannel;
        this.shxChannel = shxChannel;
    }

    /**
     * Make sure our buffer is of size.
     *
     */
    private void checkShapeBuffer(int size) {
        if ( m_bb.capacity() < size ) {
            m_bb = ByteBuffer.allocateDirect(size);
        }
    }

    /**
     * Drain internal buffers into underlying channels.
     *
     * @throws WriteException
     *
     */
    private void drain() throws WriteException {
        m_bb.flip();
        m_indexBuffer.flip();
        try {
            while ( m_bb.remaining() > 0 ) {
                shpChannel.write(m_bb);
            }

            while ( m_indexBuffer.remaining() > 0 ) {
                shxChannel.write(m_indexBuffer);
            }
        } catch (IOException e) {
            throw new WriteException("SHP File Write Drain", e);
        }
        m_bb.flip().limit(m_bb.capacity());
        m_indexBuffer.flip().limit(m_indexBuffer.capacity());
    }

    private void allocateBuffers() {
        m_bb = ByteBuffer.allocateDirect(16 * 1024);
        m_indexBuffer = ByteBuffer.allocateDirect(100);
    }

    /**
     * Close the underlying Channels.
     */
    public void close() throws WriteException {
        try {
            shpChannel.close();
            shxChannel.close();
        } catch (IOException e) {
            throw new WriteException("SHP File Write Close", e);
        }
        shpChannel = null;
        shxChannel = null;
        m_shape = null;

        if ( m_indexBuffer instanceof ByteBuffer ) {
            if ( m_indexBuffer != null ) {
                ///NIOUtilities.clean(m_indexBuffer);
            }
        }

        if ( m_indexBuffer instanceof ByteBuffer ) {
            if ( m_indexBuffer != null ) {
                ///NIOUtilities.clean(m_bb);
            }
        }

        m_indexBuffer = null;
        m_bb = null;
    }

    private void writeHeaders(Geometry[] geometries, int type)
            throws WriteException {
        this.m_type = type;
        this.inconsistenciesInGeometryTypeCounter = 0;

        int fileLength = 100;
        Envelope envelope = null;
        try {
            envelope = geomManager.createEnvelope(SUBTYPES.GEOM2D);
        } catch (CreateEnvelopeException e) {
            logger.error("Error creating the envelope", e);
        }

        for ( int i = geometries.length - 1; i >= 0; i-- ) {
            Geometry fgeometry = geometries[i];
            try {
                m_shape.initialize(fgeometry);
            } catch (BaseException e) {
                throw new WriteException("SHPFileWrite write headers", e);
            }
            int size = m_shape.getLength() + 8;
            fileLength += size;
            envelope.add(fgeometry.getEnvelope());
        }

        writeHeaders(envelope, type, geometries.length, fileLength);

    }

    /**
     * Writes shape header (100 bytes)
     */
    public void writeHeaders(Envelope bounds, int type,
            int numberOfGeometries,
            int fileLength) throws WriteException {

        this.m_type = type;
        this.inconsistenciesInGeometryTypeCounter = 0;

        if ( m_bb == null ) {
            allocateBuffers();
        }
        // Posicionamos al principio.
        m_bb.position(0);
        m_indexBuffer.position(0);

        ShapeFileHeader2 header = new ShapeFileHeader2();

        header.write(m_bb, type, numberOfGeometries, fileLength / 2,
                bounds
                .getMinimum(0), bounds.getMinimum(1), bounds.getMaximum(0),
                bounds.getMaximum(1), 0, 0, 0, 0);

        header.write(m_indexBuffer, type, numberOfGeometries,
                50 + (4 * numberOfGeometries), bounds.getMinimum(0), bounds.getMinimum(1),
                bounds.getMaximum(0), bounds.getMaximum(1), 0, 0, 0, 0);

        m_offset = 50;
        m_cnt = 0;

        try {
            shpChannel.position(0);
            shxChannel.position(0);
        } catch (IOException e) {
            throw new WriteException("SHP File Write Headers", e);
        }
        drain();
    }

    public int writeIGeometry(Geometry g) throws WriteException {
        if ( g == null ) {
            m_shape = SHP.create(0);
        } else {
            int shapeType = getShapeType(g.getType(), g.getGeometryType().getSubType());
            m_shape = SHP.create(shapeType);
        }
        return writeGeometry(g);
    }

    /**
     * Writes a single Geometry.
     *
     * @param g
     * @return the position of buffer (after the last geometry, it will allow
     * you to write the file size in the header.
     */
    public synchronized int writeGeometry(Geometry g)
            throws WriteException {
        if ( m_shape.getShapeType() != this.m_type ) {
            if ( this.inconsistenciesInGeometryTypeCounter < 10 ) {
                logger.warn("Saving a geometry of type '" + SHP.getTypeName(m_shape.getShapeType()) + "' in a shape of type '" + SHP.getTypeName(this.m_type) + "'.");
            } else if ( this.inconsistenciesInGeometryTypeCounter < 11 ) {
                logger.warn("Too many warnings. Saving a geometry of type '" + SHP.getTypeName(m_shape.getShapeType()) + "' in a shape of type '" + SHP.getTypeName(this.m_type) + "'.");
            }
            this.inconsistenciesInGeometryTypeCounter++;
        }

        if ( m_bb == null ) {
            allocateBuffers();
            m_offset = 50;
            m_cnt = 0;

            try {
                shpChannel.position(0);
                shxChannel.position(0);
            } catch (IOException e) {
                throw new WriteException("SHP File Write", e);
            }
        }

        m_pos = m_bb.position();
        try {
            m_shape.initialize(g);
        } catch (BaseException e) {
            throw new WriteException("SHPFileWrite write geometry", e);
        }
        int length = m_shape.getLength();

        // must allocate enough for shape + header (2 ints)
        checkShapeBuffer(length + 8);

        length /= 2;

        m_bb.order(ByteOrder.BIG_ENDIAN);
        m_bb.putInt(++m_cnt);
        m_bb.putInt(length);
        m_bb.order(ByteOrder.LITTLE_ENDIAN);
        m_bb.putInt(m_shape.getShapeType());
        m_shape.write(m_bb);

        m_pos = m_bb.position();

        // write to the shx
        m_indexBuffer.putInt(m_offset);
        m_indexBuffer.putInt(length);
        m_offset += (length + 4);
        drain();

        return m_pos; // Devolvemos hasta donde hemos escrito
    }

    /**
     * Returns a shapeType compatible with shapeFile constants from a gvSIG's
     * IGeometry type
     *
     * @param geometryType
     * @return a shapeType compatible with shapeFile constants from a gvSIG's
     * IGeometry type
     */
    public int getShapeType(int geometryType, int geometrySubType) {
        if ( geometrySubType == Geometry.SUBTYPES.GEOM3D ||  geometrySubType == Geometry.SUBTYPES.GEOM3DM  ) {
            switch (geometryType) {
            case Geometry.TYPES.NULL:
                return SHP.NULL;

            case Geometry.TYPES.POINT:
                return SHP.POINT3D;

            case Geometry.TYPES.CURVE:
            case Geometry.TYPES.LINE:
            case Geometry.TYPES.MULTICURVE:
            case Geometry.TYPES.MULTILINE:
            case Geometry.TYPES.ARC:
            case Geometry.TYPES.SPLINE:
            case Geometry.TYPES.CIRCUMFERENCE:
            case Geometry.TYPES.PERIELLIPSE:
                return SHP.POLYLINE3D;

            case Geometry.TYPES.SURFACE:
            case Geometry.TYPES.POLYGON:
            case Geometry.TYPES.MULTISURFACE:
            case Geometry.TYPES.MULTIPOLYGON:
            case Geometry.TYPES.CIRCLE:
            case Geometry.TYPES.ELLIPSE:
            case Geometry.TYPES.ELLIPTICARC:
            case Geometry.TYPES.FILLEDSPLINE:
                return SHP.POLYGON3D;

            case Geometry.TYPES.MULTIPOINT:
                return SHP.MULTIPOINT3D; //TODO falta aclarar cosas aqu�.
            }

        } else if ( geometrySubType == Geometry.SUBTYPES.GEOM2DM  ) {
            switch (geometryType) {
            case Geometry.TYPES.NULL:
                return SHP.NULL;

            case Geometry.TYPES.POINT:
                return SHP.POINTM;

            case Geometry.TYPES.CURVE:
            case Geometry.TYPES.LINE:
            case Geometry.TYPES.MULTICURVE:
            case Geometry.TYPES.MULTILINE:
            case Geometry.TYPES.ARC:
            case Geometry.TYPES.SPLINE:
            case Geometry.TYPES.CIRCUMFERENCE:
            case Geometry.TYPES.PERIELLIPSE:
                return SHP.POLYLINEM;

            case Geometry.TYPES.SURFACE:
            case Geometry.TYPES.POLYGON:
            case Geometry.TYPES.MULTISURFACE:
            case Geometry.TYPES.MULTIPOLYGON:
            case Geometry.TYPES.CIRCLE:
            case Geometry.TYPES.ELLIPSE:
            case Geometry.TYPES.ELLIPTICARC:
            case Geometry.TYPES.FILLEDSPLINE:
                return SHP.POLYGONM;

            case Geometry.TYPES.MULTIPOINT:
                return SHP.MULTIPOINTM; //TODO falta aclarar cosas aqu�.
            }

        } else {
            switch (geometryType) {
            case Geometry.TYPES.POINT:
                return SHP.POINT2D;

            case Geometry.TYPES.CURVE:
            case Geometry.TYPES.LINE:
            case Geometry.TYPES.MULTICURVE:
            case Geometry.TYPES.MULTILINE:
            case Geometry.TYPES.ARC:
            case Geometry.TYPES.SPLINE:
            case Geometry.TYPES.CIRCUMFERENCE:
            case Geometry.TYPES.PERIELLIPSE:
                return SHP.POLYLINE2D;

            case Geometry.TYPES.SURFACE:
            case Geometry.TYPES.POLYGON:
            case Geometry.TYPES.MULTISURFACE:
            case Geometry.TYPES.MULTIPOLYGON:
            case Geometry.TYPES.CIRCLE:
            case Geometry.TYPES.ELLIPSE:
            case Geometry.TYPES.ELLIPTICARC:
            case Geometry.TYPES.FILLEDSPLINE:
                return SHP.POLYGON2D;

            case Geometry.TYPES.MULTIPOINT:
                return SHP.MULTIPOINT2D; //TODO falta aclarar cosas aqu�.
            }
        }
        return SHP.NULL;
    }
}
