/* gvSIG. Desktop Geographic Information System.
 *
 * Copyright � 2007-2015 gvSIG Association
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.shp.utils;

import java.nio.ByteOrder;

import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ReadRuntimeException;
import org.gvsig.fmap.dal.store.shp.SHPStoreParameters;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.aggregate.MultiPrimitive;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.Line;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.geom.primitive.Polygon;
import org.gvsig.fmap.geom.primitive.Primitive;
import org.gvsig.fmap.geom.primitive.Ring;
import org.gvsig.utils.bigfile.BigByteBuffer2;


/**
 * @author fdiaz
 *
 */
public abstract class AbstractSHPReader implements SHPReader {
    private final SHPStoreParameters params;

    /**
     *
     * @param params
     */
    public AbstractSHPReader(SHPStoreParameters params) {
        this.params = params;
    }

    protected class InvalidNumberOfPointsInLinearRingException extends ReadRuntimeException {

        private final static String MESSAGE_FORMAT = "Invalid number of points in LinearRing (found %(NumPoints) - must be 0 or >= 4).\nCheck 'Fix LinearRings and LineStrings' in the shape's properties of the add layer dialog to try fix it.";
        private final static String MESSAGE_KEY = "_InvalidNumberOfPointsInLinearRingException";
        private static final long serialVersionUID = -8265770463632826027L;

        public InvalidNumberOfPointsInLinearRingException(int numPoints) {
            super(MESSAGE_FORMAT, null, MESSAGE_KEY, serialVersionUID);
            setValue("NumPoints", numPoints);
            this.getMessage();
        }
    }

    protected class InvalidNumberOfPointsInLineStringException extends ReadRuntimeException {

        /**
         *
         */
        private static final long serialVersionUID = -6458824187605578665L;
        private final static String MESSAGE_FORMAT = "Invalid number of points in LineString (found %(NumPoints) - must be 0 or >= 2).\nCheck 'Fix LinearRings and LineStrings' in the shape's properties of the add layer dialog to try fix it.";
        private final static String MESSAGE_KEY = "_InvalidNumberOfPointsInLineStringException";

        public InvalidNumberOfPointsInLineStringException(int numPoints) {
            super(MESSAGE_FORMAT, null, MESSAGE_KEY, serialVersionUID);
            setValue("NumPoints", numPoints);
            this.getMessage();
        }
    }
    public void checkNumVerticesOfRing(Ring ring ) throws InvalidNumberOfPointsInLinearRingException {
            if( ring.getNumVertices()<4 ) {
                if( this.fixLinearRings() ) {
                    Point p = ring.getVertex(ring.getNumVertices()-1);
                    while( ring.getNumVertices()<4 ) {
                        p = (Point) p.cloneGeometry();
                        ring.addVertex(p);
                    }
                } else {
                    throw new InvalidNumberOfPointsInLinearRingException(ring.getNumVertices());
                }
            }
    }

    public void checkNumVerticesOfLine(Line line) throws InvalidNumberOfPointsInLinearRingException {
        if (line.getNumVertices() > 0 && line.getNumVertices() < 2) {
            if (this.fixLinearRings()) {
                Point p = line.getVertex(line.getNumVertices() - 1);
                while (line.getNumVertices() < 2) {
                    p = (Point) p.cloneGeometry();
                    line.addVertex(p);
                }
            } else {
                throw new InvalidNumberOfPointsInLineStringException(line.getNumVertices());
            }
        }
    }

    public boolean fixLinearRings() {
        return params.getFixLinearRings();
    }

    /**
     * @param p
     * @param bb
     */
    protected void fillXY(Point p, BigByteBuffer2 bb) {
        bb.order(ByteOrder.LITTLE_ENDIAN);
        double x = bb.getDouble();
        double y = bb.getDouble();
        p.setX(x);
        p.setY(y);
    }

    protected void fillM(Geometry geometry, BigByteBuffer2 bb) throws CreateGeometryException, ReadException {
        double[] boxM = new double[2];
        boxM[0] = bb.getDouble();
        boxM[1] = bb.getDouble();
        if (geometry instanceof MultiPrimitive) {
            for (int primitivesNumber = 0; primitivesNumber < ((MultiPrimitive) geometry).getPrimitivesNumber(); primitivesNumber++) {
                Primitive primitive = ((MultiPrimitive) geometry).getPrimitiveAt(primitivesNumber);
                if (primitive instanceof Point) {
                    Point point = (Point) primitive;
                    point.setCoordinateAt(point.getDimension()-1, bb.getDouble());
                }
                if (primitive instanceof Line) {
                    Line line = (Line) primitive;
                    for (int i = 0; i < line.getNumVertices(); i++) {
                        line.setCoordinateAt(i, line.getDimension()-1, bb.getDouble());
                    }
                }
                if (primitive instanceof Polygon) {
                    Polygon polygon = (Polygon) primitive;
                    for (int i = 0; i < polygon.getNumVertices(); i++) {
                        polygon.setCoordinateAt(i, polygon.getDimension()-1, bb.getDouble());
                    }
                    int rings = polygon.getNumInteriorRings();
                    for (int i = 0; i < rings; i++) {
                        Ring ring = polygon.getInteriorRing(i);
                        for (int j = 0; j < ring.getNumVertices(); j++) {
                            ring.setCoordinateAt(j, ring.getDimension()-1, bb.getDouble());
                        }
                    }
                }
            }
        } else if (geometry instanceof Line) {
            Line line = (Line) geometry;
            for (int i = 0; i < line.getNumVertices(); i++) {
                line.setCoordinateAt(i, line.getDimension()-1, bb.getDouble());
            }
        } else if (geometry instanceof Polygon) {
            Polygon polygon = (Polygon) geometry;
            for (int i = 0; i < polygon.getNumVertices(); i++) {
                polygon.setCoordinateAt(i, polygon.getDimension()-1, bb.getDouble());
            }
            int rings = polygon.getNumInteriorRings();
            for (int i = 0; i < rings; i++) {
                Ring ring = polygon.getInteriorRing(i);
                for (int j = 0; j < ring.getNumVertices(); j++) {
                    ring.setCoordinateAt(j, ring.getDimension()-1, bb.getDouble());
                }
            }
        }
    }

    protected void fillZ(Geometry geometry, BigByteBuffer2 bb) throws CreateGeometryException, ReadException {
        double[] boxM = new double[2];
        boxM[0] = bb.getDouble();
        boxM[1] = bb.getDouble();
        if (geometry instanceof MultiPrimitive) {
            for (int primitivesNumber = 0; primitivesNumber < ((MultiPrimitive) geometry).getPrimitivesNumber(); primitivesNumber++) {
                Primitive primitive = ((MultiPrimitive) geometry).getPrimitiveAt(primitivesNumber);
                if (primitive instanceof Point) {
                    Point point = (Point) primitive;
                    point.setCoordinateAt(Geometry.DIMENSIONS.Z, bb.getDouble());
                }
                if (primitive instanceof Line) {
                    Line line = (Line) primitive;
                    for (int i = 0; i < line.getNumVertices(); i++) {
                        line.setCoordinateAt(i, Geometry.DIMENSIONS.Z, bb.getDouble());
                    }
                }
                if (primitive instanceof Polygon) {
                    Polygon polygon = (Polygon) primitive;
                    for (int i = 0; i < polygon.getNumVertices(); i++) {
                        polygon.setCoordinateAt(i, Geometry.DIMENSIONS.Z, bb.getDouble());
                    }
                    int rings = polygon.getNumInteriorRings();
                    for (int i = 0; i < rings; i++) {
                        Ring ring = polygon.getInteriorRing(i);
                        for (int j = 0; j < ring.getNumVertices(); j++) {
                            ring.setCoordinateAt(j, Geometry.DIMENSIONS.Z, bb.getDouble());
                        }
                    }
                }
            }
        } else if (geometry instanceof Line) {
            Line line = (Line) geometry;
            for (int i = 0; i < line.getNumVertices(); i++) {
                line.setCoordinateAt(i, Geometry.DIMENSIONS.Z, bb.getDouble());
            }
        } else if (geometry instanceof Polygon) {
            Polygon polygon = (Polygon) geometry;
            for (int i = 0; i < polygon.getNumVertices(); i++) {
                polygon.setCoordinateAt(i, Geometry.DIMENSIONS.Z, bb.getDouble());
            }
            int rings = polygon.getNumInteriorRings();
            for (int i = 0; i < rings; i++) {
                Ring ring = polygon.getInteriorRing(i);
                for (int j = 0; j < ring.getNumVertices(); j++) {
                    ring.setCoordinateAt(j, Geometry.DIMENSIONS.Z, bb.getDouble());
                }
            }
        }
    }


}
