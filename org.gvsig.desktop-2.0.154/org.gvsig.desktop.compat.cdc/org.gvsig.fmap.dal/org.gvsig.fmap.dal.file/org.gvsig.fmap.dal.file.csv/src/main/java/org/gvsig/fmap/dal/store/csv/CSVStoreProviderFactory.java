/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.csv;

import org.gvsig.fmap.dal.DataParameters;
import org.gvsig.fmap.dal.DataStoreProvider;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.feature.FeatureStoreProviderFactory;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProviderFactory;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DynObject;

public class CSVStoreProviderFactory extends AbstractFeatureStoreProviderFactory implements FeatureStoreProviderFactory{

	protected CSVStoreProviderFactory(String name, String description) {
		super(name, description);
	}

	public DataStoreProvider createProvider(DataParameters parameters,
			DataStoreProviderServices providerServices)
			throws InitializeException {
		return new CSVStoreProvider((CSVStoreParameters) parameters, providerServices);
	}

	public DynObject createParameters() {
		return new CSVStoreParameters();
	}
	
	public int allowCreate() {
		return NO;
	}
	
	public int allowWrite() {
		return NO;
	}

	public int allowRead() {
		return YES;
	}
	
	public int hasRasterSupport() {
		return NO;
	}
	
	public int hasTabularSupport() {
		return YES;
	}
	
	public int hasVectorialSupport() {
		return YES;
	}

	public int allowMultipleGeometryTypes() {
		return YES;
	}
	
	public int allowEditableFeatureType() {
		return NO;
	}

}
