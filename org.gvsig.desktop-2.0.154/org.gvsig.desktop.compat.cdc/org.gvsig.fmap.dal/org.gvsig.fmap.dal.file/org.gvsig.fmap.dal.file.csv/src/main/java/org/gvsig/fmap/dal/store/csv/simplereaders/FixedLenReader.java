
package org.gvsig.fmap.dal.store.csv.simplereaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.store.csv.CSVStoreParameters;


public class FixedLenReader implements SimpleReader {

    BufferedReader reader = null;
    CSVStoreParameters.FieldDefinition fieldsDefinition[] = null;
    List fields = null;
    private final int fieldCount;
//    private final CSVStoreParameters parameters;
    private final String commentStartMarker;
    private int currentLine =1;

    public FixedLenReader(FileReader reader, CSVStoreParameters parameters) {
//        this.parameters = parameters;
        this.reader = new BufferedReader(reader);
        this.fieldsDefinition = CSVStoreParameters.getFieldsDefinition(parameters);
        this.fieldCount = this.fieldsDefinition.length;
        this.fields = new ArrayList(this.fieldCount);
        for( int i=0; i<this.fieldCount; i++ ) {
            this.fields.add(null);
        }
        this.commentStartMarker = CSVStoreParameters.getCommentStartMarker(parameters);
    }

    public String[] getHeader() throws IOException {
        String[] header = new String[this.fieldCount];
        for( int i=0; i<this.fieldCount; i++ ) {
            header[i] = Character.toString((char) (i+'A')).toUpperCase();
        }
        return header;
    }

    @Override
    public int getColumnsCount() throws IOException {
        return this.fieldCount;
    }

    public List<String> read() throws IOException {
        String line = this.reader.readLine();
        this.currentLine++;
        while( !StringUtils.isEmpty(line) && !StringUtils.isEmpty(this.commentStartMarker) && line.startsWith(this.commentStartMarker) ) {
            line = this.reader.readLine();
            this.currentLine++;
        }
        return this.parse(line);
    }

    public List<String> parse(String line) throws IOException {
        if( line == null ) {
            return null;
        }
        for ( int i = 0; i < this.fieldCount; i++ ) {
            CSVStoreParameters.FieldDefinition fieldDefinition = this.fieldsDefinition[i];
            String value = null;
            try {
                if ( fieldDefinition.getToEndOfLine() ) {
                    value = line.substring(fieldDefinition.getStart());
                } else {
                    value = line.substring(
                            fieldDefinition.getStart(),
                            fieldDefinition.getEnd()
                    );
                }
            } catch(Exception ex) {
                // Ignore errors
                // Probablemente por que las lineas no tienen tantos caracteres como
                // se han indicado en la definicion.
            }
            this.fields.set(i, value);
        }
        return this.fields;
    }

    public void close() throws IOException {
        this.reader.close();
        this.reader = null;
    }

    public List<String> skip(int lines) throws IOException {
        String line = null;
        for ( int i = 0; i < lines; i++ ) {
            line = this.reader.readLine();
            this.currentLine++;
            while( line!=null && this.commentStartMarker!=null && line.startsWith(this.commentStartMarker) ) {
                line = this.reader.readLine();
                this.currentLine++;
            }
        }
        return this.parse(line);
    }

    public int getLine() {
        return this.currentLine;
    }
}
