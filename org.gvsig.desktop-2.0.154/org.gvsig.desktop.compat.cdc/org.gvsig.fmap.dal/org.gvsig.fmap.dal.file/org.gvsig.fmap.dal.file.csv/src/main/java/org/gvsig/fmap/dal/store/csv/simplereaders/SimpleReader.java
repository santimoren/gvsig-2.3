package org.gvsig.fmap.dal.store.csv.simplereaders;

import java.io.IOException;
import java.util.List;
import java.io.Closeable;

public interface SimpleReader extends Closeable {

    public String[] getHeader() throws IOException;

    public int getColumnsCount() throws IOException ;
            
    public List<String> read() throws IOException;

    @Override
    public void close() throws IOException;

    public List<String> skip(int lines) throws IOException;
    
    public int getLine();
}
