package org.gvsig.fmap.dal.store.csv.simplereaders;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.gvsig.fmap.dal.store.csv.CSVStoreParameters;
import org.gvsig.tools.dynobject.DynObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.comment.CommentStartsWith;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.quote.QuoteMode;

public class CSVReader implements SimpleReader {

    //
    // http://supercsv.sourceforge.net/examples_reading.html
    // http://supercsv.sourceforge.net/apidocs/index.html
    //
    private static final Logger logger = LoggerFactory.getLogger(CSVReader.class);

    private CsvListReader reader;
    private final CSVStoreParameters parameters;
    private List<String>  nextLine;
    private int columns;

    public CSVReader(FileReader in, CSVStoreParameters parameters) {
        this(parameters);
        this.reader = new CsvListReader(in, getCSVPreferences());
    }    
    
    public CSVReader(CSVStoreParameters parameters) {
        this.reader = null;
        this.parameters = parameters;
        this.reader = null;
        this.nextLine = null;
        this.columns = 0;
    }

    public CSVStoreParameters getParameters() {
        return this.parameters;
    }

    @Override
    public String[] getHeader() throws IOException {
        return this.reader.getHeader(true);
    }
    
    @Override
    public int getColumnsCount() throws IOException {
        if( this.columns <= 0 ) {
            this.columns = reader.length();
            if( this.columns <= 0 ) {
                this.nextLine = this.reader.read();
                this.columns = reader.length();
            }
        }
        return this.columns;
    }

    @Override
    public List<String> read() throws IOException {
        List<String> line;
        if( this.nextLine != null ) {
            line = this.nextLine;
            this.nextLine = null;
        } else {
            line = this.reader.read();
        }
        return line;
    }

    @Override
    public void close() throws IOException {
        this.reader.close();
    }

    @Override
    public List<String> skip(int lines) throws IOException {
        if( lines <= 0 ) {
            return null;
        }
        if( this.nextLine != null ) {
            this.nextLine = null;
            lines--;
        }
        List<String> row = null;
        for ( int i = 0; i < lines; i++ ) {
            row = reader.read();
        }
        return row;
    }

    public CsvPreference getCSVPreferences() {
        try {
            String s;
            char quoteChar;
            int delimiterChar;
            String endOfLineSymbols;

            DynObject params = this.getParameters();

            CsvPreference.Builder builder;

            CsvPreference defaultPreference = CSVStoreParameters
                    .getPredefinedCSVPreferences(params);
            if ( defaultPreference == null ) {
                defaultPreference = CsvPreference.STANDARD_PREFERENCE;
            }

            endOfLineSymbols = CSVStoreParameters.getRecordSeparator(params);
            if ( StringUtils.isBlank(endOfLineSymbols) ) {
                endOfLineSymbols = defaultPreference.getEndOfLineSymbols();
            }
            s = CSVStoreParameters.getQuoteCharacter(params);
            if ( StringUtils.isBlank(s) ) {
                quoteChar = (char) defaultPreference.getQuoteChar();
            } else {
                quoteChar = s.charAt(0);
            }
            s = CSVStoreParameters.getDelimiter(params);
            if ( StringUtils.isBlank(s) ) {
                delimiterChar = defaultPreference.getDelimiterChar();
            } else {
                delimiterChar = s.charAt(0);
            }

            builder = new CsvPreference.Builder(quoteChar, delimiterChar,
                    endOfLineSymbols);

            s = CSVStoreParameters.getCommentStartMarker(params);
            if ( !StringUtils.isBlank(s) ) {
                CommentStartsWith cs = new CommentStartsWith(s);
                builder.skipComments(cs);
            }

            builder.surroundingSpacesNeedQuotes(CSVStoreParameters
                    .getSurroundingSpacesNeedQuotes(params));
            QuoteMode quoteMode = CSVStoreParameters.getQuoteMode(params);
            if ( quoteMode != null ) {
                builder.useQuoteMode(quoteMode);
            }
            return builder.build();
        } catch (Exception e) {
            logger.warn("Can't make preferences for CSV '" + getFullFileName()
                    + "'.", e);
            return null;
        }
    }
    
    private String getFullFileName() {
        // Usar solo para mostrar mensajes en el logger.
        String s;
        try {
            s = getParameters().getFile().getAbsolutePath();
        } catch (Exception e2) {
            s = "(unknow)";
        }
        return s;        
    }

    @Override
    public int getLine() {
        if( this.reader==null ) {
            return 0;
        }
        return this.reader.getLineNumber();
    }

}
