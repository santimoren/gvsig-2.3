/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.csv;

import java.io.File;
import java.util.Locale;
import org.apache.commons.lang3.BooleanUtils;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.OpenDataStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.quote.AlwaysQuoteMode;
import org.supercsv.quote.NormalQuoteMode;
import org.supercsv.quote.QuoteMode;

public class CSVStoreParameters extends AbstractDataParameters implements
        OpenDataStoreParameters, FilesystemStoreParameters {

    private static final Logger logger = LoggerFactory.getLogger(CSVStoreParameters.class);

    public static final String PARAMETERS_DEFINITION_NAME = "CSVStoreParameters";

    private static final String FILE = "file";
    private static final String IGNOREERRORS = "ignoreErrors";
    private static final String PROFILE = "profile";
    private static final String QUOTEPOLICY = "quotePolicy";
    private static final String QUOTECHAR = "quoteCharacter";
    private static final String RECORDSEPARATOR = "recordSeparator";
    private static final String DELIMITER = "delimiter";
    private static final String COMMENTSTARTMARKER = "commentStartMarker";
    private static final String AUTOMATICTYPESDETECTION = "automaticTypesDetection";

    private static final String ESCAPECHARACTER = "escapeCharacter";
    private static final String FIRST_LINE_HEADER = "firstLineHeader";
    private static final String HEADER = "header";
    private static final String SURROUNDINGSPACESNEEDQUOTES = "surroundingSpacesNeedQuotes";

    //private static final String IGNOREEMPTYLINES = "ignoreEmptyLines";
    private static final String CRS = "CRS";
    private static final String FIELDTYPES = "fieldtypes";
//    private static final String NULLTO = "nullTo";
    private static final String CHARSET = "charset"; // Default "UTF-8"
    private static final String LOCALE = "locale";
    private static final String POINT_COLUMN_NAME = "pointColumnName";
    private static final String LIMIT = "limit";

    private DelegatedDynObject parameters;

    public CSVStoreParameters() {
        this(PARAMETERS_DEFINITION_NAME);
    }

    protected CSVStoreParameters(String parametersDefinitionName) {
        this(parametersDefinitionName, CSVStoreProvider.NAME);
    }

    public CSVStoreParameters(String parametersDefinitionName, String name) {
        super();
        this.parameters = (DelegatedDynObject) FileHelper.newParameters(parametersDefinitionName);
        this.setDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME, name);
    }

    public String getDataStoreName() {
        return (String) this.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
    }

    public String getDescription() {
        return this.getDynClass().getDescription();
    }

    protected DelegatedDynObject getDelegatedDynObject() {
        return parameters;
    }

    public boolean isValid() {
        if ( getFileName(this) == null ) {
            return false;
        }
        return true;
    }

    public File getFile() {
        return (File) this.getDynValue(FILE);
    }

    public void setFile(File file) {
        this.setDynValue(FILE, file);
    }

    public static CsvPreference getPredefinedCSVPreferences(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(PROFILE);
        if ( "NONE".equalsIgnoreCase(s) ) {
            return null;
        }
        if ( "STANDARD_PREFERENCE".equalsIgnoreCase(s) ) {
            return CsvPreference.STANDARD_PREFERENCE;
        }
        if ( "EXCEL_PREFERENCE".equalsIgnoreCase(s) ) {
            return CsvPreference.EXCEL_PREFERENCE;
        }
        if ( "EXCEL_NORTH_EUROPE_PREFERENCE".equalsIgnoreCase(s) ) {
            return CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE;
        }
        if ( "TAB_PREFERENCE".equalsIgnoreCase(s) ) {
            return CsvPreference.TAB_PREFERENCE;
        }
        return null;
    }

    public static QuoteMode getQuoteMode(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(QUOTEPOLICY);
        if ( "AlwaysQuoteMode".equalsIgnoreCase(s) ) {
            return new AlwaysQuoteMode();
        }
        if ( "NormalQuoteMode".equalsIgnoreCase(s) ) {
            return new NormalQuoteMode();
        }
        return null;
    }

    static IProjection getCRS(DynObject dynobj) {
        return (IProjection) dynobj.getDynValue(CRS);
    }

    static String getFileName(DynObject dynobj) {
        File f = (File) dynobj.getDynValue(FILE);
        if ( f == null ) {
            return null;
        }
        return f.getPath();
    }

    static File getFile(DynObject dynobj) {
        File f = (File) dynobj.getDynValue(FILE);
        return f;
    }

    public static String getRecordSeparator(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(RECORDSEPARATOR);
        return StringEscapeUtils.unescapeJava(s);
    }

    static Locale getLocale(DynObject dynobj) {
        try {
            String s = (String) dynobj.getDynValue(LOCALE);
            if ( s.trim().length() == 0 ) {
                return null;
            }
            if ( "DEFAULT".equalsIgnoreCase(s.trim()) ) {
                return Locale.getDefault();
            }
            Locale locale = null;
            // locale = Locale.forLanguageTag(s); // Since java 1.7
            String[] ss = s.split("-");
            switch (ss.length) {
            case 1:
                locale = new Locale(ss[0]);
                break;
            case 2:
                locale = new Locale(ss[0], ss[1]);
                break;
            case 3:
            default:
                locale = new Locale(ss[0], ss[1], ss[2]);
                break;
            }
            return locale;
        } catch (Exception ex) {
            logger.warn("Can't get locale from CSV parameters.", ex);
            return null;
        }
    }

    public static String getCommentStartMarker(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(COMMENTSTARTMARKER);
        return StringEscapeUtils.unescapeJava(s);
    }
    
    public static String getPointColumnName(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(POINT_COLUMN_NAME);
        return s;
    }

    public static String getQuoteCharacter(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(QUOTECHAR);
        s = StringEscapeUtils.unescapeJava(s);
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.substring(0, 1);
    }

    public static String getDelimiter(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(DELIMITER);
        s = StringEscapeUtils.unescapeJava(s);
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.substring(0, 1);
    }

    static String getHeader(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(HEADER);
        s = StringEscapeUtils.unescapeJava(s);
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s;
    }

    static String[] getHeaders(DynObject dynobj) {
        String s = getHeader(dynobj);
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        String sep = getDelimiter(dynobj);
        if ( sep == null ) {
            sep = getDelimiter(s);
            if ( sep == null ) {
                // Chungo
                return null;
            }
        }
        String[] ss = s.split("[" + sep + "]");
        return ss;
    }

    static String getDelimiter(String line) {
        if( StringUtils.isBlank(line) ) {
            return null;
        }
        String sep = null;
        // Cuidado con los ":", los he puesto al final a proposito
        // ya que podian estar en la cadena para separar el size
        // de cada tipo.
        String seps = ",;-|@#/+$%&!:";
        for ( int i = 0; i < seps.length(); i++ ) {
            sep = seps.substring(i, 1);
            if ( line.contains(seps.substring(i, 1)) ) {
                break;
            }
            sep = null;
        }
        return sep;
    }

    static String getCharset(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(CHARSET);
        return StringEscapeUtils.unescapeJava(s);
    }

    static String[] getPointDimensionNames(DynObject dynobj) {
        String s = (String) dynobj.getDynValue("point");
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.split(",");
    }

    public static boolean getSurroundingSpacesNeedQuotes(DynObject dynobj) {
        Boolean b = (Boolean) dynobj.getDynValue(SURROUNDINGSPACESNEEDQUOTES);
        return BooleanUtils.isTrue(b);
    }

    static boolean getAutomaticTypesDetection(DynObject dynobj) {
        Boolean b = (Boolean) dynobj.getDynValue(AUTOMATICTYPESDETECTION);
        return BooleanUtils.isTrue(b);
    }

    static boolean getIgnoreErrors(DynObject dynobj) {
        Boolean b = (Boolean) dynobj.getDynValue(IGNOREERRORS);
        return BooleanUtils.isTrue(b);
    }

    static boolean isFirstLineHeader(DynObject dynobj) {
        Boolean b = (Boolean) dynobj.getDynValue(FIRST_LINE_HEADER);
        return BooleanUtils.isTrue(b);
    }

//    static int[] getFieldTypes(DynObject dynobj) {
//        String s = (String) dynobj.getDynValue(FIELDTYPES);
//        if ( StringUtils.isBlank(s) ) {
//            return null;
//        }
//        String sep = getDelimiter(s);
//        if ( sep == null ) {
//            return null;
//        }
//        DataTypesManager dataTypeManager = ToolsLocator.getDataTypesManager();
//        String fieldTypeNames[] = s.split("[" + sep + "]");
//        int fieldTypes[] = new int[fieldTypeNames.length];
//        for ( int i = 0; i < fieldTypeNames.length; i++ ) {
//            s = fieldTypeNames[i].trim();
//            if ( s.contains(":") ) {
//                s = s.split(":")[0];
//            }
//            fieldTypes[i] = dataTypeManager.getType(s);
//        }
//        return fieldTypes;
//    }
//
//    static int[] getFieldSizes(DynObject dynobj) {
//        String s = (String) dynobj.getDynValue(FIELDTYPES);
//        if ( StringUtils.isBlank(s) ) {
//            return null;
//        }
//        String sep = getDelimiter(s);
//        if ( sep == null ) {
//            return null;
//        }
//        DataTypesManager dataTypeManager = ToolsLocator.getDataTypesManager();
//        String fieldTypeNames[] = s.split("[" + sep + "]");
//        int fieldSizes[] = new int[fieldTypeNames.length];
//        for ( int i = 0; i < fieldTypeNames.length; i++ ) {
//            String fieldtypeDef = fieldTypeNames[i].trim();
//            if ( fieldtypeDef.contains(":") ) {
//                try {
//                    String[] parts = fieldtypeDef.split(":");
//                    int fieldType = dataTypeManager.getType(parts[0]);
//                    if( fieldType == DataTypes.GEOMETRY ) {
//                        fieldSizes[i] = 1;
//                    } else {
//                        s = parts[1];
//                        fieldSizes[i] = Integer.parseInt(s);
//                    }
//                } catch (Exception ex) {
//                    logger.warn("Can't get size of field " + i + " (" + fieldtypeDef + ").", ex);
//                }
//            } else {
//                fieldSizes[i] = 0;
//            }
//        }
//        return fieldSizes;
//    }

    static String getRawFieldTypes(DynObject dynobj) {
        String s = (String) dynobj.getDynValue(FIELDTYPES);
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.trim();
    }

    static int getSkipLines(DynObject dynobj) {
        Integer n = (Integer) dynobj.getDynValue("skipLines");
        if ( n == null ) {
            return 0;
        }
        return n;
    }

    static int getLimit(DynObject dynobj) {
        Integer n = (Integer) dynobj.getDynValue(LIMIT);
        if ( n == null ) {
            return -1;
        }
        return n;
    }

    static String getRawFieldsDefinition(DynObject dynobj) {
        String s = (String) dynobj.getDynValue("fieldsDefinition");
        if ( StringUtils.isBlank(s) ) {
            return null;
        }
        return s.trim();
    }

    public static class FieldDefinition {

        private int start;
        private int end;

        public FieldDefinition(String def) {
            def = def.trim();
            String[] ss = def.split(":");
            this.start = Integer.parseInt(ss[0]);
            if ( ss.length < 2 ) {
                this.end = -1;
            } else {
                this.end = Integer.parseInt(ss[1]);
            }
        }

        public int getStart() {
            return this.start;
        }

        public int getEnd() {
            return this.end;
        }

        public boolean getToEndOfLine() {
            return this.end == -1;
        }
    }

    public static FieldDefinition[] getFieldsDefinition(DynObject dynobj) {
        String definition = getRawFieldsDefinition(dynobj);
        if ( definition == null ) {
            return null;
        }
        int i=0;
        try {
            String[] defs = StringUtils.split(definition);
            FieldDefinition[] fieldsDefinition = new FieldDefinition[defs.length];
            for ( i = 0; i < defs.length; i++ ) {
                fieldsDefinition[i] = new FieldDefinition(defs[i]);
            }
            return fieldsDefinition;
        } catch (Exception ex) {
            throw  new IllegalArgumentException("Can't recognize the format field definition '"+definition+"' ("+i+").");
        }
    }

}
