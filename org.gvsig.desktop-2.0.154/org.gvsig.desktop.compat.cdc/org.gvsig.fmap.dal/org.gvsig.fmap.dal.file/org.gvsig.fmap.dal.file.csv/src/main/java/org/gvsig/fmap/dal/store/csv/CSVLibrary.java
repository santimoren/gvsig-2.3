/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.csv;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.dal.DALFileLibrary;
import org.gvsig.fmap.dal.DALFileLocator;
import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class CSVLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
        require(DALFileLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		List<Throwable> exs = new ArrayList<Throwable>();

		FileHelper.registerParametersDefinition(
				CSVStoreParameters.PARAMETERS_DEFINITION_NAME,
				CSVStoreParameters.class, "CSVParameters.xml");
		try {
			FileHelper.registerMetadataDefinition(
					CSVStoreProvider.METADATA_DEFINITION_NAME,
					CSVStoreProvider.class, "CSVMetadata.xml");
		} catch (MetadataException e) {
			exs.add(e);
		}

		DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator
				.getDataManager();

		try {
			if (!dataman.getStoreProviders().contains(CSVStoreProvider.NAME)) {
				dataman.registerStoreProviderFactory(new CSVStoreProviderFactory(CSVStoreProvider.NAME, CSVStoreProvider.DESCRIPTION));

			}
		} catch (RuntimeException e) {
			exs.add(e);
		}

		try {
			DALFileLocator.getFilesystemServerExplorerManager()
					.registerProvider(CSVStoreProvider.NAME,
							CSVStoreProvider.DESCRIPTION,
							CSVFilesystemServerProvider.class);
		} catch (RuntimeException e) {
			exs.add(e);
		}

		if (exs.size() > 0) {
			throw new LibraryException(this.getClass(), exs);
		}
	}
}
