/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.csv;

import java.io.File;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.CreateException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.RemoveException;
import org.gvsig.fmap.dal.resource.spi.ResourceConsumer;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.impl.AbstractFilesystemServerExplorerProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProviderServices;

public class CSVFilesystemServerProvider extends AbstractFilesystemServerExplorerProvider 
	implements FilesystemServerExplorerProvider, ResourceConsumer {

	private FilesystemServerExplorerProviderServices serverExplorer;

	public String getDataStoreProviderName() {
		return CSVStoreProvider.NAME;
	}

	public int getMode() {
		return DataServerExplorer.MODE_FEATURE | DataServerExplorer.MODE_GEOMETRY;
	}

	public boolean accept(File pathname) {
		return (pathname.getName().toLowerCase().endsWith(".csv"));
	}

	public String getDescription() {
		return CSVStoreProvider.DESCRIPTION;
	}

	public DataStoreParameters getParameters(File file) throws DataException {
		DataManager manager = DALLocator.getDataManager();
		CSVStoreParameters params = (CSVStoreParameters) manager
				.createStoreParameters(this
				.getDataStoreProviderName());
		params.setFile(file);
		return params;
	}

	public boolean canCreate() {
		return false;
	}

	public boolean canCreate(NewDataStoreParameters parameters) {
			throw new UnsupportedOperationException(); 
	}

	public void create(NewDataStoreParameters parameters, boolean overwrite)
			throws CreateException {
		throw new UnsupportedOperationException(); 
	}

	public NewDataStoreParameters getCreateParameters() throws DataException {
		throw new UnsupportedOperationException(); 
	}

	public void initialize(
			FilesystemServerExplorerProviderServices serverExplorer) {
		this.serverExplorer = serverExplorer;
	}

	public void remove(DataStoreParameters parameters) throws RemoveException {
		File file = ((CSVStoreParameters) parameters).getFile();
		if (!file.exists()) {
			throw new RemoveException(this.getDataStoreProviderName(),
					new FileNotFoundException(file));
		}
		if (!file.delete()) {
			// FIXME throws ???
		}

	}

	public boolean closeResourceRequested(ResourceProvider resource) {
		// while it is using a resource anyone can't close it
		return false;
	}

	public void resourceChanged(ResourceProvider resource) {
		//Do nothing

	}


}
