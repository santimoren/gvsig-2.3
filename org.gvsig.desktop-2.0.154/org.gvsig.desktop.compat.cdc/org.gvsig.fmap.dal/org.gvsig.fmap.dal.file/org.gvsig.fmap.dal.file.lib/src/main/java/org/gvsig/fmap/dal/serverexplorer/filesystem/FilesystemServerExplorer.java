/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.serverexplorer.filesystem;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;

public interface FilesystemServerExplorer extends DataServerExplorer {

	public static final String NAME = "FilesystemExplorer";

	public void setCurrentPath(File path) throws FileNotFoundException;

	public File getCurrentPath();

	public File getRoot();
	
	public List getProviderNameList(File file);

	public String getProviderName(File file);
	
	public NewDataStoreParameters getAddParameters(File file)
			throws DataException;

	public DataStoreParameters createStoreParameters(File file)
			throws DataException;
	public DataStoreParameters createStoreParameters(File file, String providerName) 
			throws DataException;

	public Iterator getFilters();

	public Iterator getFilters(int mode);
	
	public FilesystemFileFilter getFilter(int mode, String description);
	
	public FilesystemFileFilter getGenericFilter();
}
