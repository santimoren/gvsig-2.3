/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.filesystem.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FilenameUtils;

import org.gvsig.fmap.dal.DALFileLocator;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.exception.RemoveException;
import org.gvsig.fmap.dal.exception.ServerExplorerAddException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemFileFilter;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProviderServices;
import org.gvsig.fmap.dal.spi.AbstractDataServerExplorer;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataServerExplorerProviderServices;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;

public class DefaultFilesystemServerExplorer extends AbstractDataServerExplorer
        implements FilesystemServerExplorerProviderServices,
        FilesystemFileFilter {

    private File root;
    private File current; // Current path
    private List serverProviders;

    public DefaultFilesystemServerExplorer(
            FilesystemServerExplorerParameters parameters,
            DataServerExplorerProviderServices services)
            throws InitializeException {
        super(parameters, services);
        if (this.getFSParameters().getRoot() != null) {
            this.root = new File(this.getFSParameters().getRoot());
        }
        if (this.getFSParameters().getInitialpath() != null) {
            this.current = new File(this.getFSParameters().getInitialpath());
        }
        if (this.root == null && this.current == null) {
			// throw new InitializeException(this.getName(),
            // new IllegalArgumentException());
        } else if (this.current == null) {
            this.current = new File(this.getFSParameters().getRoot());
        }
    }

    private FilesystemServerExplorerParameters getFSParameters() {
        return (FilesystemServerExplorerParameters) this.getParameters();
    }

    @Override
    protected void doDispose() throws BaseException {
        this.root = null;
        super.doDispose();
    }

    @Override
    public List list(int mode) throws DataException {
        if (this.current == null) {
            throw new IllegalStateException();
        }
        if (!this.current.exists()) {
            throw new org.gvsig.fmap.dal.exception.FileNotFoundException(this.current);
        }

        if (!this.current.isDirectory()) {
            throw new IllegalArgumentException(this.getProviderName()
                    + ": Path not a directory '" + this.current + "'");
        }

        List files = new ArrayList(); // return files
        List providers = this.getProviders(mode);
        String allfiles[] = this.current.list();

        for (int i = 0; i < allfiles.length; i++) {
            File file = new File(this.root, allfiles[i]);
            Iterator providersit = providers.iterator();
            while (providersit.hasNext()) {
                FilesystemServerExplorerProvider provider = (FilesystemServerExplorerProvider) providersit
                        .next();
                if (provider.accept(file)) {
                    DataStoreParameters dsp = this.createStoreParameters(file);
                    if (dsp != null) {
                        files.add(dsp);
                    }
                    break;
                }
            }
        }
        return files;
    }

    public List list() throws DataException {
        if (this.current == null) {
            throw new IllegalStateException(); // FIXME
        }
        if (!this.current.exists()) {
            // TODO crear excepcion de Data??
            new org.gvsig.fmap.dal.exception.FileNotFoundException(this.current);
        }

        if (!this.current.isDirectory()) {
            new IllegalArgumentException(this.getProviderName()
                    + ": Path not a directory '" + this.current + "'");
        }

        String files[] = this.current.list();
        int i;
        File theFile;
        ArrayList list = new ArrayList();
        DataStoreParameters dsp = null;

        for (i = 0; i < files.length; i++) {
            theFile = new File(this.root, files[i]);
            dsp = this.createStoreParameters(theFile);
            if (dsp != null) {
                list.add(dsp);
            }
        }
        return list;
    }

    public void setCurrentPath(File path) throws FileNotFoundException {
        // FIXME Comprobar si es un directorio existente
        if (!path.exists()) {
            throw new FileNotFoundException(path);
        }
        if (!path.isDirectory()) {
            throw new IllegalArgumentException(path.getPath()
                    + " is not a directory");
        }
        if (!isFromRoot(path)) {
            throw new IllegalArgumentException(path.getPath()
                    + " is not from root");

        }

        this.current = path;
    }

    public File getCurrentPath() {
        return this.current;
    }

    public File getRoot() {
        return this.root;
    }

    public void remove(DataStoreParameters dsp) throws RemoveException {
        String providerName = dsp.getDataStoreName();
        try {
            this.checkIsMine(dsp);
            FilesystemServerExplorerProvider provider = this
                    .getProvider(providerName);

            provider.remove(dsp);
        } catch (DataException e) {
            throw new RemoveException(this.getProviderName(), e);
        }
    }

    public boolean add(String providerName, NewDataStoreParameters ndsp,
            boolean overwrite) throws DataException {

        try {
            this.checkIsMine(ndsp);
            FilesystemServerExplorerProvider provider = this
                    .getProvider(providerName);

            ndsp.validate();
            provider.create(ndsp, overwrite);
            return true;
        } catch (DataException e) {
            throw new ServerExplorerAddException(this.getProviderName(), e);
        } catch (ValidateDataParametersException e) {
            throw new ServerExplorerAddException(this.getProviderName(), e);
        }
    }

    public boolean canAdd() {
        return this.root.canWrite();
    }

    public String getProviderName() {
        return FilesystemServerExplorer.NAME;
    }

    public NewDataStoreParameters getAddParameters(String storeName)
            throws DataException {
        FilesystemServerExplorerProvider provider = this.getProvider(storeName);
        if (provider == null) {
            throw new IllegalArgumentException(
                    "Not registered in this explorer"); // FIXME
        }

        NewDataStoreParameters nParams = provider.getCreateParameters();
        // nParams.setAttribute("path", this.getCurrentPath().getPath());
        return nParams;
    }

    public boolean canAdd(String storeName) throws DataException {
        if (storeName == null) {
            return false;// CanAdd with genericFilter
        }
        FilesystemServerExplorerProvider provider = this.getProvider(storeName);
        if (provider == null) {
            throw new IllegalArgumentException(
                    "Not registered in this explorer"); // FIXME
        }

        return provider.canCreate();

    }

	// ==========================================
    private FilesystemServerExplorerProvider getProvider(String storeName)
            throws InitializeException, ProviderNotRegisteredException {
        Iterator providers = getProviders(FilesystemServerExplorer.MODE_ALL).iterator();
        FilesystemServerExplorerProvider provider;
        while (providers.hasNext()) {
            provider = (FilesystemServerExplorerProvider) providers.next();
            if (provider.getDataStoreProviderName().equals(storeName)) {
                return provider;
            }
        }
        return null;
    }

    private DataManagerProviderServices getManager() {
        return (DataManagerProviderServices) DALLocator.getDataManager();
    }

    public DataStoreParameters createStoreParameters(File file, String providerName) throws DataException {

        return this.getParametersFor(file, providerName, true);
    }

    public DataStoreParameters createStoreParameters(File file) throws DataException {

        return this.getParametersFor(file, null, true);
    }

    public DataStoreParameters getParametersFor(File file, String providerName, boolean checksExist) throws DataException {

        if (checksExist) {
            if (!file.exists()) {
                return null;
            }
            if (!file.isFile()) {
                return null;
            }
            if (!file.canRead()) {
                return null;
            }
            if (file.isHidden()) { // XXX ???
                return null;
            }
        }
        if (providerName == null) {
            providerName = this.getProviderName(file);
        }
        if (providerName != null) {
            DataStoreParameters params = this.getManager()
                    .createStoreParameters(providerName);
            ((FilesystemStoreParameters) params).setFile(file);
            return params;

        }
        return null;
    }

    public List getProviderNameList(File file) {
        Iterator filters = getFilters();
        List list = new ArrayList();
        while (filters.hasNext()) {
            FilesystemFileFilter filter = (FilesystemFileFilter) filters.next();
            if (filter.accept(file)) {
                list.add(filter.getDataStoreProviderName());
            }
        }
        return list;
    }

    public String getProviderName(File file) {
        Iterator filters = getFilters();
        while (filters.hasNext()) {
            FilesystemFileFilter filter = (FilesystemFileFilter) filters.next();
            if (filter.accept(file)) {
                return filter.getDataStoreProviderName();
            }
        }
        return null;
    }

    public List getDataStoreProviderNames() {
        Set names = new HashSet();

        Iterator filters = getFilters();
        while (filters.hasNext()) {
            FilesystemFileFilter filter = (FilesystemFileFilter) filters.next();
            names.add(filter.getDataStoreProviderName());
        }
        return new ArrayList(names);
    }

    private void checkIsMine(DataStoreParameters dsp)
            throws IllegalArgumentException, DataException {
        // FIXME Exception ???
        if (!(dsp instanceof FilesystemStoreParameters)) {
            new IllegalArgumentException(
                    "not instance of FilesystemStoreParameters");
        }
        Iterator filters = getFilters();
        File file = ((FilesystemStoreParameters) dsp).getFile();
        if (!this.isFromRoot(file)) {
            throw new IllegalArgumentException("worng explorer");
        }
        FilesystemFileFilter filter;
        while (filters.hasNext()) {
            filter = (FilesystemFileFilter) filters.next();
            if (dsp.getDataStoreName()
                    .equals(filter.getDataStoreProviderName())) {
                return;
            }
        }
        throw new IllegalArgumentException("worng explorer");
    }

    private boolean isFromRoot(File file) {
        if (this.root == null) {
            return true;
        }
        return file.getAbsolutePath().startsWith(this.root.getAbsolutePath());
    }

    public List getProviders() {
        if (this.serverProviders == null) {
            Iterator iter = DALFileLocator.getFilesystemServerExplorerManager()
                    .getRegisteredProviders();
            this.serverProviders = new ArrayList();
            Extension ext;
            FilesystemServerExplorerProvider provider;
            while (iter.hasNext()) {
                ext = (Extension) iter.next();
                try {
                    provider = (FilesystemServerExplorerProvider) ext.create();
                } catch (Exception e) {
                    throw new RuntimeException(e);// FIXME !!!
                }
                provider.initialize(this);
                this.serverProviders.add(provider);
            }
        }
        return this.serverProviders;
    }

    public List getProviders(int mode) {
        Iterator iter = DALFileLocator.getFilesystemServerExplorerManager()
                .getRegisteredProviders();
        List providers = new ArrayList();
        Extension ext;
        FilesystemServerExplorerProvider provider;
        while (iter.hasNext()) {
            ext = (Extension) iter.next();
            try {
                provider = (FilesystemServerExplorerProvider) ext.create();
            } catch (Exception e) {
                throw new RuntimeException(e);// FIXME !!!
            }
            if (provider.isMode(mode)) {
                provider.initialize(this);
                providers.add(provider);
            }
        }
        return providers;
    }

    public Iterator getFilters(int mode) {
        return this.getProviders(mode).iterator();
    }

    public Iterator getFilters() {
        return this.getProviders().iterator();
    }

    public FilesystemFileFilter getFilter(int mode, final String description) {

        final List filters = new ArrayList();
        Iterator it = this.getFilters(mode);
        while (it.hasNext()) {
            filters.add(it.next());
        }
        FilesystemFileFilter filter = new FilesystemFileFilter() {

            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                for (int i = 0; i < filters.size(); i++) {
                    if (((FilesystemFileFilter) filters.get(i)).accept(f)) {
                        return true;
                    }
                }
                return false;
            }

            public String getDescription() {
                return description;
            }

            public String getDataStoreProviderName() {
                return null;
            }
        };
        return filter;
    }

    public FilesystemFileFilter getGenericFilter() {
        // FIXME: Este metodo, getGenericFilter, no tengo claro que deba existir (jjdc)
        return this;
    }

    public String getDataStoreProviderName() {
        return null;
    }

    public String getDescription() {
        return "All supporteds";
    }

    public boolean accept(File pathname) {
        try {
            return this.createStoreParameters(pathname) != null;
        } catch (DataException e) {
            throw new RuntimeException(e); // FIXME excpetion??
        }
    }

    public DataStore open(File file) throws DataException,
            ValidateDataParametersException {
        DataManager manager = DALLocator.getDataManager();
        String providerName = this.getProviderName(file);

        DataStoreParameters params = this.getAddParameters(file);
        return manager.openStore(providerName, params);
    }

    public NewDataStoreParameters getAddParameters(File file)
            throws DataException {
        DataStoreParameters params = this.getParametersFor(file, null, false);
        NewDataStoreParameters newParams = this.getAddParameters(params
                .getDataStoreName());
        ((FilesystemStoreParameters) newParams)
                .setFile(((FilesystemStoreParameters) params).getFile());
        return newParams;
    }

    @Override
    public File getResourcePath(DataStore dataStore, String resourceName) throws DataException {
        FilesystemServerExplorerProvider provider
                = this.getProvider(dataStore.getProviderName());
        if (provider == null) {
            return null;
        }
        String rootPathName = provider.getResourceRootPathName(dataStore);
        if (rootPathName == null) {
            return null;
        }
        File f = new File(FilenameUtils.getFullPathNoEndSeparator(rootPathName),resourceName);
        if( f.exists() ) {
            return f;
        }
        return new File(rootPathName + "." + resourceName);
    }

    @Override
    public DataStoreParameters get(String name) throws DataException {
        File theFile = new File(this.current,name);
        DataStoreParameters dsp = this.createStoreParameters(theFile);
        return dsp;
    }
}
