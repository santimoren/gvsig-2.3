/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.filesystem;

import org.gvsig.fmap.dal.DataServerExplorerParameters;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynObjectManager;

public class FilesystemServerExplorerParameters extends AbstractDataParameters
		implements DataServerExplorerParameters {

    public static final String DYNCLASS_NAME = "FilesystemServerExplorerParameters";

    private static final String FIELD_ROOT = "root";

	private DelegatedDynObject delegatedDynObject;

	public FilesystemServerExplorerParameters() {
		this.delegatedDynObject = (DelegatedDynObject) ToolsLocator
				.getDynObjectManager()
				.createDynObject(this.registerDynClass());
	}

	private DynClass registerDynClass() {
	   	DynObjectManager dynman = ToolsLocator.getDynObjectManager();
    	DynClass dynClass = dynman.get(DYNCLASS_NAME);
    	DynField field;
    	if (dynClass == null) {
    		dynClass = dynman.add(DYNCLASS_NAME);

    		field = dynClass.addDynField(FIELD_ROOT);
    		field.setType(DataTypes.STRING);
    		field.setDescription("Root directory path of the explorer");
    		field.setTheTypeOfAvailableValues(DynField.ANY);


    		field = dynClass.addDynField("initialpath");
			field.setType(DataTypes.STRING);
			field.setDescription("Initial path of the explorer");
			field.setTheTypeOfAvailableValues(DynField.ANY);

    	}
    	return dynClass;
	}

	public void setRoot(String path) {
		this.setDynValue(FIELD_ROOT, path);
	}

	public String getRoot() {
		return (String) this.getDynValue(FIELD_ROOT);
	}

	public void setInitialpath(String path) {
		this.setDynValue("initialpath", path);
	}

	public String getInitialpath() {
		return (String) this.getDynValue("initialpath");
	}

	public String getExplorerName() {
		return FilesystemServerExplorer.NAME;
	}

	protected DelegatedDynObject getDelegatedDynObject() {
		return delegatedDynObject;
	}
}