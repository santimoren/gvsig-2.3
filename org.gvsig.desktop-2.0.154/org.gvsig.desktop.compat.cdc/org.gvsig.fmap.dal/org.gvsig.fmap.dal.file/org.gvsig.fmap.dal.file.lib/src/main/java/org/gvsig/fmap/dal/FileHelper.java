/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal;

import java.io.InputStream;

import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileHelper {

	private static final Logger LOG = LoggerFactory.getLogger(FileHelper.class);

	private FileHelper() {
		// Do nothing
	}
	
	public static DynStruct registerParametersDefinition(String name, Class theClass, String filename) {
		DynStruct definition = null;
		
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		definition = manager.getDefinition(name);
		if ( definition == null) {
			InputStream stream = theClass.getResourceAsStream(filename);
			if( stream == null ) {
				throw new DefinitionNotFoundException(theClass,filename);
			}
			definition = manager.addDefinition(
					theClass,
					name, 
					stream,
					theClass.getClassLoader(), 
					null, 
					null
			);
		}
		return definition;
	}
	
	public static DynStruct registerMetadataDefinition(String name, Class theClass, String filename) throws MetadataException {
		DynStruct definition = null;

		MetadataManager manager = MetadataLocator.getMetadataManager();
		definition = manager.getDefinition(name);
		if ( definition == null) {
			InputStream stream = theClass.getResourceAsStream(filename);
			if( stream == null ) {
				throw new DefinitionNotFoundException(theClass,filename);
			}
			definition = manager.addDefinition(
					name, 
					theClass.getResourceAsStream(filename),
					theClass.getClassLoader()
			);
		}
		return definition;
	}

	public static DynObject newParameters(String name) {
    	return ToolsLocator.getDynObjectManager()
			.createDynObject(
				ToolsLocator.getPersistenceManager().getDefinition(name)
			);
	}
	
	public static DynObject newMetadataContainer(String name) {
		try {
			MetadataManager manager = MetadataLocator.getMetadataManager();
			DynStruct definition = manager.getDefinition(name);
	    	return ToolsLocator.getDynObjectManager().createDynObject(definition);
		} catch(NullPointerException e) {
			throw new CantCreateMetadataException(name,e);
		}
	}

	private static class DefinitionNotFoundException extends BaseRuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 7598155353307119897L;

		public DefinitionNotFoundException(Class theClass, String filename ) {
			super(
				"Can't open parameters definition '%(filename)' from class %(classname).",
				"_ResourceForParametersDefinitionNotFoundException",
				serialVersionUID
			);
			this.setValue("filename", filename);
			this.setValue("classname", theClass.getClass().getName());
		}
	}
	
	private static class CantCreateMetadataException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -8373306339537106075L;

		CantCreateMetadataException(String name, Throwable cause) {
			super("Can't retrieve metadata definition for '"+name+"'.", cause);			
		}
	}
}
