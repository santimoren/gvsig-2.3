/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.resource.file;

import java.io.File;

import org.gvsig.fmap.dal.resource.spi.AbstractResourceParameters;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.DataTypes;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectEncoder;
import org.gvsig.tools.persistence.PersistenceManager;

public class FileResourceParameters extends AbstractResourceParameters {

    public static final String DYNCLASS_NAME = "FileResourceParameters";

    private static final String DYNFIELDNAME_FILE = "file";

    private DelegatedDynObject delegatedDynObject;

    public FileResourceParameters() {
        this.delegatedDynObject = (DelegatedDynObject) ToolsLocator
                .getDynObjectManager()
                .createDynObject(registerDynClass());
    }

    public FileResourceParameters(Object[] params) {
        this();
        // XXX puede que sobre
        setDynValue(DYNFIELDNAME_FILE, params[0]);
    }

    public FileResourceParameters(String name) {
        this();
        setDynValue(DYNFIELDNAME_FILE, name);
    }

    public String getFileName() {
        if (this.getFile() == null) {
            return null;
        }
        return this.getFile().getPath();
    }

    public FileResourceParameters setFileName(String fileName) {
        this.setDynValue(DYNFIELDNAME_FILE, fileName);
        return this;
    }

    public File getFile() {
        return (File) this.getDynValue(DYNFIELDNAME_FILE);
    }

    public FileResourceParameters setFile(File file) {
        this.setDynValue(DYNFIELDNAME_FILE, file);
        return this;
    }

    public FileResourceParameters setFile(String fileName) {
        this.setDynValue(DYNFIELDNAME_FILE, fileName);
        return this;
    }

    @Override
    public String getResurceID() {
        DynObjectEncoder encoder = ToolsLocator.getDynObjectManager().createSimpleDynObjectEncoder();
        return encoder.encodePair("file",this.getFile().getAbsolutePath());
    }
    
    

    @Override
    public String getTypeName() {
        return FileResource.NAME;
    }

    @Override
    protected DelegatedDynObject getDelegatedDynObject() {
        return delegatedDynObject;
    }

    /*
     * Added "static synchronized" because sometimes there is a
     * "DuplicateDynClassException" when user selects more than one file (SHP,
     * for example) to be added to the view. "definition" is null but then there
     * is an exception because in the meantime another thread has registered it
     * (I'm not sure this is the cause, but I cannot imagine another reason)
     */
    private static synchronized DynClass registerDynClass() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynClass definition = (DynClass) manager.getDefinition(DYNCLASS_NAME);
        if (definition == null) {
            definition = (DynClass) manager.addDefinition(
                    FileResourceParameters.class,
                    DYNCLASS_NAME,
                    DYNCLASS_NAME + " persistence definition",
                    null,
                    null
            );

            definition.addDynField(DYNFIELDNAME_FILE).setType(DataTypes.FILE)
                    .setDescription("The file");
        }
        return definition;
    }
}
