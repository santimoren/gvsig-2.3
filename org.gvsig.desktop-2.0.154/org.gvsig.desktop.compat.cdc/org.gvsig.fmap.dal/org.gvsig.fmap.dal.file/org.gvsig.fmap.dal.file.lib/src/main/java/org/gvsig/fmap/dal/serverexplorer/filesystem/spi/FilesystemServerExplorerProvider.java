/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.serverexplorer.filesystem.spi;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.CreateException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.RemoveException;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemFileFilter;

/**
 * @author jmvivo
 *
 */
public interface FilesystemServerExplorerProvider extends FilesystemFileFilter {

	public void initialize(FilesystemServerExplorerProviderServices serverExplorer);

	public boolean canCreate();

	public void create(NewDataStoreParameters parameters, boolean overwrite)
			throws CreateException;

	public NewDataStoreParameters getCreateParameters() throws DataException;

	public void remove(DataStoreParameters parameters) throws RemoveException;

	public boolean canCreate(NewDataStoreParameters parameters);
	
	public String getResourceRootPathName(DataStore dataStore);
	
	public int getMode();

	/**
	 * Return true if any mode in the parameter is supported.
	 * 
	 * @param mode
	 * @return
	 */
	public boolean isMode(int mode);
}
