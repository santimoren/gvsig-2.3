/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2010 {Prodevelop}   {Task}
 */

package org.gvsig.fmap.dal.serverexplorer.filesystem.impl;

import java.io.File;
import org.apache.commons.io.FilenameUtils;

import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProvider;

/**
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera Llodr&aacute;</a>
 */
public abstract class AbstractFilesystemServerExplorerProvider implements FilesystemServerExplorerProvider {

        @Override
        public String getResourceRootPathName(DataStore dataStore) {
            DataStoreParameters parameters = dataStore.getParameters();
            if (parameters instanceof FilesystemStoreParameters) {
                File f = ((FilesystemStoreParameters) parameters).getFile();
                if (f != null) {
                    return FilenameUtils.removeExtension(f.getAbsolutePath());
                }
            }
            if (dataStore.getParameters().hasDynValue(FileResource.NAME)) {
                Object obj = (dataStore.getParameters().getDynValue(FileResource.NAME));
                if (obj != null) {
                    return removeFileExtension(new File(obj.toString()));
                }
            }
            return null;
        }

	protected String removeFileExtension(File file){
		int whereDot = file.getName().lastIndexOf(".");	
		if ((0 < whereDot) && (whereDot <= file.getName().length() - 2)) {
			whereDot = file.getAbsolutePath().lastIndexOf(".");
			return file.getAbsolutePath().substring(0, whereDot);
		}	
		return file.getAbsolutePath();
	}
	
        @Override
	public int getMode() {
		return DataServerExplorer.MODE_ALL;
	}
	
        @Override
	public boolean isMode(int mode) {
		if( mode == DataServerExplorer.MODE_ALL ) {
			return true;
		}
		return (this.getMode() & mode) != 0;
	}

}

