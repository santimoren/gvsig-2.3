/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */


package org.gvsig.fmap.dal;

import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.resource.file.FileResourceParameters;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.fmap.dal.serverexplorer.filesystem.impl.DefaultFilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.impl.DefaultFilesystemServerExplorerManager;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;


public class DALFileLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
    }

	protected void doInitialize() throws LibraryException {
	}

	protected void doPostInitialize() throws LibraryException {
    	DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator.getDataManager();

    	ResourceManagerProviderServices resman = (ResourceManagerProviderServices) DALLocator.getResourceManager();

    	if (!resman.getResourceProviders().contains(FileResource.NAME)) {
			resman.register(FileResource.NAME, FileResource.DESCRIPTION,
					FileResource.class, FileResourceParameters.class);
		}

    	if (!dataman.getExplorerProviders().contains(FilesystemServerExplorer.NAME)) {
			dataman.registerExplorerProvider(FilesystemServerExplorer.NAME,
					DefaultFilesystemServerExplorer.class,
					FilesystemServerExplorerParameters.class);
			DALFileLocator
					.registerFilesystemSeverExplorerManager(DefaultFilesystemServerExplorerManager.class);
    	}
	}
}
