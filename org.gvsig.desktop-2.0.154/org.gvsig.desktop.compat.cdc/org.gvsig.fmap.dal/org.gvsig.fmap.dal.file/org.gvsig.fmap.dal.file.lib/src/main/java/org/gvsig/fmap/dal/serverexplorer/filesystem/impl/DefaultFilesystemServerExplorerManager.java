/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.serverexplorer.filesystem.impl;

import java.util.Iterator;

import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerManager;
import org.gvsig.tools.ToolsLocator;

public class DefaultFilesystemServerExplorerManager implements
		FilesystemServerExplorerManager {

	public Iterator getRegisteredProviders() {
		return ToolsLocator.getExtensionPointManager().get(FILE_FILTER_EPSNAME)
				.iterator();
	}

	public void registerProvider(String name, String description,
			Class filesystemServerProvicerClass) {
		ToolsLocator.getExtensionPointManager().add(FILE_FILTER_EPSNAME,
				FILE_FILTER_EPSDESCRIPTION).append(
				name, description,
				filesystemServerProvicerClass);
	}

}
