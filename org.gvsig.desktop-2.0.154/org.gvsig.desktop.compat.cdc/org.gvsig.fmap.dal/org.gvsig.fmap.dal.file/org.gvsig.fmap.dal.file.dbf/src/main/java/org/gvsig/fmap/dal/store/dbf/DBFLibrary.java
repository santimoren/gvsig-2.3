/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.dbf;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gvsig.fmap.dal.DALFileLibrary;
import org.gvsig.fmap.dal.DALFileLocator;
import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.resource.file.FileResourceParameters;
import org.gvsig.fmap.dal.resource.spi.ResourceManagerProviderServices;
import org.gvsig.fmap.dal.spi.DataManagerProviderServices;
import org.gvsig.fmap.dal.store.dbf.utils.DbaseFile;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObjectValueItem;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class DBFLibrary extends AbstractLibrary {

    @Override
    public void doRegistration() {
        registerAsServiceOf(DALLibrary.class);
        require(DALFileLibrary.class);
    }

	@Override
	protected void doInitialize() throws LibraryException {
	}

	@Override
	protected void doPostInitialize() throws LibraryException {
		List<Throwable> exs = new ArrayList<Throwable>();

		FileHelper.registerParametersDefinition(
				DBFStoreParameters.PARAMETERS_DEFINITION_NAME,
				DBFStoreParameters.class,
				"DBFParameters.xml"
		);
		updateEncodingDefinition();
		FileHelper.registerParametersDefinition(
				DBFNewStoreParameters.PARAMETERS_DEFINITION_NAME,
				DBFNewStoreParameters.class,
				"DBFParameters.xml"
		);
		try {
			FileHelper.registerMetadataDefinition(
					DBFStoreProvider.METADATA_DEFINITION_NAME,
					DBFStoreProvider.class,
					"DBFMetadata.xml"
			);
		} catch (MetadataException e) {
			exs.add(e);
		}

        DataManagerProviderServices dataman = (DataManagerProviderServices) DALLocator
				.getDataManager();

		try {
			if (!dataman.getStoreProviders().contains(DBFStoreProvider.NAME)) {
			    dataman.registerStoreProviderFactory(new DBFStoreProviderFactory(DBFStoreProvider.NAME, DBFStoreProvider.DESCRIPTION));
			}
		} catch (RuntimeException e) {
			exs.add(e);
		}

		try {
			DALFileLocator.getFilesystemServerExplorerManager()
					.registerProvider(DBFStoreProvider.NAME,
							DBFStoreProvider.DESCRIPTION,
							DBFFilesystemServerProvider.class);
		} catch (RuntimeException e) {
			exs.add(e);
		}

		if( exs.size()>0 ) {
			throw new LibraryException(this.getClass(), exs);
		}
	}

	private static void updateEncodingDefinition() {
		DynStruct parametersDefinition = ToolsLocator.getPersistenceManager().getDefinition(DBFStoreParameters.PARAMETERS_DEFINITION_NAME);

		DynObjectValueItem[] values = parametersDefinition.getDynField("encoding").getAvailableValues();

		Set<DynObjectValueItem> charsetSet = new LinkedHashSet<DynObjectValueItem>(160);

		charsetSet.addAll( Arrays.asList(values) );
		Map<String,Charset> charsets = Charset.availableCharsets();
		Iterator<String> iter = charsets.keySet().iterator();
		while (iter.hasNext()){
			String value = (String) iter.next();
			String label= value;
			charsetSet.add(new DynObjectValueItem(value, label));
		}

		parametersDefinition.getDynField("Encoding")
			.setAvailableValues(
					(DynObjectValueItem[]) charsetSet.toArray(new DynObjectValueItem[charsets.size()])
			);


	}
}
