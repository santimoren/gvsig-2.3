/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.dbf;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreNotification;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.exception.CloseException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.OpenException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.UnsupportedVersionException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.AttributeNameException;
import org.gvsig.fmap.dal.feature.exception.AttributeNameTooLongException;
import org.gvsig.fmap.dal.feature.exception.PerformEditingException;
import org.gvsig.fmap.dal.feature.exception.UnknownDataTypeException;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureStoreProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureReferenceProviderServices;
import org.gvsig.fmap.dal.feature.spi.FeatureSetProvider;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.exception.AccessResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceException;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyChangesException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyCloseException;
import org.gvsig.fmap.dal.resource.exception.ResourceNotifyOpenException;
import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.resource.spi.ResourceConsumer;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorer;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemServerExplorerParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.fmap.dal.store.dbf.utils.DbaseFile;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.exception.BaseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBFStoreProvider extends AbstractFeatureStoreProvider implements
        ResourceConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(DBFStoreProvider.class);

    public static final int MAX_FIELD_NAME_LENGTH = DbaseFile.MAX_FIELD_NAME_LENGTH;

    public static String NAME = "DBF";
    public static String DESCRIPTION = "DBF file";
    private static final Locale ukLocale = new Locale("en", "UK");

    public static final String METADATA_DEFINITION_NAME = NAME;
    private static final String METADATA_ENCODING = "Encoding";
    private static final String METADATA_CODEPAGE = "CodePage";

    private DbaseFile dbfFile = null;
    private ResourceProvider dbfResource;
    private long counterNewsOIDs = -1;
    private DBFFeatureWriter writer;

    private static long lastLogTime = 0;

    private boolean loTengoEnUso;

    private boolean allowDuplicatedFieldNames;

    protected static void registerMetadataDefinition() throws MetadataException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        if (manager.getDefinition(METADATA_DEFINITION_NAME) == null) {
            manager.addDefinition(
                    METADATA_DEFINITION_NAME,
                    DBFStoreParameters.class.getResourceAsStream("DBFStoreMetadata.xml"),
                    DBFStoreParameters.class.getClassLoader()
            );
        }
    }

    public DBFStoreProvider(DBFStoreParameters params,
            DataStoreProviderServices storeServices)
            throws InitializeException {
        super(
                params,
                storeServices,
                FileHelper.newMetadataContainer(METADATA_DEFINITION_NAME)
        );
        this.init(params, storeServices);
    }

    protected DBFStoreProvider(DBFStoreParameters params,
            DataStoreProviderServices storeServices, DynObject metadata)
            throws InitializeException {
        super(params, storeServices, metadata);
        this.init(params, storeServices);
    }

    protected void init(DBFStoreParameters params,
            DataStoreProviderServices storeServices) throws InitializeException {
        if (params == null) {
            throw new InitializeException(new NullPointerException("params is null"));
        }
        File theFile = getDBFParameters().getDBFFile();
        if (theFile == null) {
            throw new InitializeException(new NullPointerException("dbf file is null"));
        }
        initResource(params, storeServices);

        Charset charset = params.getEncoding();
        allowDuplicatedFieldNames = params.allowDuplicatedFieldNames();
        this.dbfFile = new DbaseFile(theFile, charset, allowDuplicatedFieldNames);

        writer = new DBFFeatureWriter(this.getProviderName());

        this.initFeatureType();

    }

    public Object getDynValue(String name) throws DynFieldNotFoundException {
        try {
            this.open();
        } catch (OpenException e) {
            throw new RuntimeException(e);
        }

        if (METADATA_ENCODING.equalsIgnoreCase(name)) {
            return this.dbfFile.getOriginalCharset();
        } else if (METADATA_CODEPAGE.equalsIgnoreCase(name)) {
            return new Byte(this.dbfFile.getCodePage());
        }
        return super.getDynValue(name);
    }

    protected void initResource(DBFStoreParameters params,
            DataStoreProviderServices storeServices) throws InitializeException {

        File theFile = getDBFParameters().getDBFFile();
        dbfResource
                = this.createResource(FileResource.NAME,
                        new Object[]{theFile.getAbsolutePath()});
        dbfResource.addConsumer(this);
    }

    protected void initResource(ResourceProvider resource,
        DataStoreProviderServices storeServices){
        dbfResource = resource;
        dbfResource.addConsumer(this);
    }

    public String getProviderName() {
        return NAME;
    }

    protected DBFStoreParameters getDBFParameters() {
        return (DBFStoreParameters) super.getParameters();
    }

    public DataServerExplorer getExplorer() throws ReadException {
        DataManager manager = DALLocator.getDataManager();
        FilesystemServerExplorerParameters params;
        try {
            params = (FilesystemServerExplorerParameters) manager
                    .createServerExplorerParameters(FilesystemServerExplorer.NAME);
            params.setRoot(this.getDBFParameters().getDBFFile().getParent());
            return manager.openServerExplorer(FilesystemServerExplorer.NAME, params);
        } catch (DataException e) {
            throw new ReadException(this.getName(), e);
        } catch (ValidateDataParametersException e) {
            // TODO Auto-generated catch block
            throw new ReadException(this.getName(), e);
        }
    }

    protected FeatureProvider internalGetFeatureProviderByReference(
            FeatureReferenceProviderServices reference, FeatureType featureType)
            throws DataException {
        return this.getFeatureProviderByIndex(
                ((Long) reference.getOID()).longValue(), featureType);
    }

    public void performChanges(Iterator deleteds, Iterator inserteds,
            Iterator updateds, Iterator originalFeatureTypesUpdated)
            throws PerformEditingException {

        /*
         * This will throw an exception if there are new fields
         * with names too long
         */
        checkNewFieldsNameSize(originalFeatureTypesUpdated);

        try {
            // TODO repasar el concepto de enUso de un recurso.
            loTengoEnUso = true;
            final FeatureStore store
                    = this.getStoreServices().getFeatureStore();
            resourceCloseRequest();
            getResource().execute(new ResourceAction() {

                public Object run() throws Exception {
                    FeatureSet set = null;
                    DisposableIterator iter = null;
                    try {
                        set = store.getFeatureSet();
                        DBFStoreParameters tmpParams
                                = (DBFStoreParameters) getDBFParameters().getCopy();

                        File tmp_file = File.createTempFile(
                                "tmp_" + System.currentTimeMillis(), ".dbf");
                        tmp_file.deleteOnExit();

                        tmpParams.setDBFFile(tmp_file);

                        writer.begin(tmpParams, store.getDefaultFeatureType(),
                                set.getSize());

                        iter = set.fastIterator();
                        while (iter.hasNext()) {
                            Feature feature = (Feature) iter.next();
                            writer.append(feature);
                        }

                        writer.end();
                        loTengoEnUso = false;
                        try {
                            close();
                        } catch (CloseException e1) {
                            throw new PerformEditingException(getProviderName(), e1);
                        }
                        getDBFParameters().getDBFFile().delete();

                        File target_file = getDBFParameters().getDBFFile();
                        if (target_file.exists()) {
                            target_file.delete();
                        }
                        tmp_file.renameTo(target_file);

                        if (tmp_file.exists() && !target_file.exists()) {
                            // Renaming failed, let's simply copy.
                            // We assume we cannot delete it, but we
                            // used deleteOnExit and it's
                            // temporary, so no problem
                            LOG.info("Warning: copying tmp file instead of renaming: "
                                    + target_file.getName());
                            FileUtils.copyFile(tmp_file, target_file);
                        }

                        resourcesNotifyChanges();
                        initFeatureType();
                    } finally {
                        loTengoEnUso = false;
                        if (set != null) {
                            set.dispose();
                        }
                        if (iter != null) {
                            iter.dispose();
                        }
                    }
                    return null;
                }
            });
        } catch (ResourceExecuteException | ResourceException e) {
            throw new PerformEditingException(this.getProviderName(), e);
        }

        this.counterNewsOIDs = -1;
    }

    protected void checkNewFieldsNameSize(Iterator ft_upd) throws PerformEditingException {

        String long_fields = getNewFieldsWithNameTooLong(ft_upd);
        if (long_fields != null) {
            AttributeNameException ane = new AttributeNameTooLongException(
                    long_fields,
                    getProviderName(),
                    MAX_FIELD_NAME_LENGTH);
            throw new PerformEditingException(getProviderName(), ane);
        }
    }

    /**
     * Returns null or a string which is a comma-separated list
     *
     * @param ft_updated
     * @return
     */
    protected String getNewFieldsWithNameTooLong(Iterator ft_updated) {

        String resp = "";
        FeatureTypeChanged item = null;
        FeatureType ft = null;
        FeatureAttributeDescriptor[] atts = null;
        while (ft_updated.hasNext()) {
            item = (FeatureTypeChanged) ft_updated.next();
            ft = item.getTarget();
            atts = ft.getAttributeDescriptors();
            for (int i = 0; i < atts.length; i++) {
                if (atts[i].getName().length() > MAX_FIELD_NAME_LENGTH) {
                    if (resp.length() == 0) {
                        resp = atts[i].getName();
                    } else {
                        resp = resp + ", " + atts[i].getName();
                    }
                }
            }
        }

        if (resp.length() == 0) {
            return null;
        } else {
            return "(" + resp + ")";
        }
    }
    /*
     * ==================================================
     */

    public FeatureProvider createFeatureProvider(FeatureType type) throws DataException {
        return new DBFFeatureProvider(this, type);
    }


    /*
     * ===================================================
     */
    protected void initFeatureType() throws InitializeException {
        try {
            FeatureType defaultType
                    = this.getTheFeatureType().getNotEditableCopy();
            List types = new ArrayList(1);
            types.add(defaultType);
            this.getStoreServices().setFeatureTypes(types, defaultType);
        } catch (OpenException e) {
            throw new InitializeException(getResource().toString(), e);
        }
    }

    public class DuplicatedFieldNameException extends ReadException {

        /**
         *
         */
        private static final long serialVersionUID = -1671651605329756160L;
        private final static String MESSAGE_FORMAT = "Duplicated field name '%(fieldName)'.\nCheck 'Allow duplicated field names' in layer properties in add layer dialog. The layer will become read only.";
        private final static String MESSAGE_KEY = "_DuplicatedFieldNameException";

        public DuplicatedFieldNameException(String fieldName) {
            super(MESSAGE_FORMAT, null, MESSAGE_KEY, serialVersionUID);
            setValue("fieldName", fieldName);
        }
    }

//
//    private String getUniqueFieldName(String fieldName, EditableFeatureType fType) {
//
//        int index = 1;
//        String tempFieldName = fieldName;
//        while(fType.get(tempFieldName)!=null && index<1000){
//            index++;
//            String sufix = String.valueOf(index);
//            tempFieldName = tempFieldName.substring(0, DbaseFile.MAX_FIELD_NAME_LENGTH-sufix.length())+sufix;
//        }
//        if(index>=1000){
//            throw new RuntimeException("Can't fix duplicated name for field '"+fieldName+"'.");
//        }
//        return tempFieldName;
//    }

    protected EditableFeatureType getTheFeatureType()
            throws InitializeException, OpenException {
        try {
            this.open();
        } catch (DataException e) {
            throw new InitializeException(this.getProviderName(), e);
        }
        return (EditableFeatureType) getResource().execute(
                new ResourceAction() {

                    public Object run() throws Exception {
                        int fieldCount = -1;
                        fieldCount = dbfFile.getFieldCount();

                        EditableFeatureType fType = getStoreServices().createFeatureType(getName());

                        fType.setHasOID(true);
                        int precision;
                        for (int i = 0; i < fieldCount; i++) {
                            char fieldType = dbfFile.getFieldType(i);
                            EditableFeatureAttributeDescriptor attr;

                            String fieldName = dbfFile.getFieldName(i);
                            if(fType.get(fieldName)!=null){
                                throw new DuplicatedFieldNameException(fieldName);
                            }

                            if (fieldType == 'L') {
                                attr = fType.add(fieldName, DataTypes.BOOLEAN);
                                attr.setDefaultValue(new Boolean(false));
                                attr.setAllowNull(false);

                            } else if ((fieldType == 'F') || (fieldType == 'N')) {
                                precision = dbfFile.getFieldDecimalLength(i);
                                if (precision > 0) {
                                    attr = fType.add(fieldName,
                                            DataTypes.DOUBLE,
                                            dbfFile.getFieldLength(i));
                                    attr.setPrecision(precision);
                                    attr.setDefaultValue(new Double(0));

                                } else {
                                    int length = dbfFile.getFieldLength(i);
                                    int type = DataTypes.INT;
                                    if (length > 9) {
                                        type = DataTypes.LONG;
                                    }
                                    attr = fType.add(fieldName,
                                            type,
                                            length);
                                    attr.setDefaultValue(new Integer(0));
                                }
                                attr.setAllowNull(false);

                            } else if (fieldType == 'C' || getDBFParameters().handleDatesAsStrings()) {
                                attr = fType.add(fieldName, DataTypes.STRING);
                                attr.setSize(dbfFile.getFieldLength(i));
                                attr.setDefaultValue("");
                                attr.setAllowNull(false);

                            } else if (fieldType == 'D') {
                                attr = fType.add(fieldName, DataTypes.DATE);
                                attr.setDefaultValue(new Date(0)); // def value 1-1-1970
                                attr.setAllowNull(true);
                                String sfmt = getDBFParameters().getDateFormat();
                                if (!StringUtils.isBlank(sfmt)) {
                                    try {
                                        SimpleDateFormat datefmt = new SimpleDateFormat(sfmt, getDBFParameters().getLocale());
                                        attr.setDateFormat(datefmt);
                                    } catch (Exception ex) {
                                        LOG.warn("Invalid date format ("+sfmt+") specified in DBF parameters.",ex);
                                    }
                                }
                            } else {
                                throw new InitializeException(getProviderName(),
                                        new UnknownDataTypeException(
                                            fieldName, ""
                                                + fieldType, getProviderName()));
                            }
                        }

                        // FeatureRules rules = fType.getRules();
                        // rules.add(new DBFRowValidator());
                        return fType;
                    }

                }
        );
    }

    protected void loadValue(FeatureProvider featureProvider, int rowIndex,
            FeatureAttributeDescriptor descriptor) throws ReadException {

        if (descriptor.getEvaluator() != null) {
            // Nothing to do
            return;
        }

        int dbfIndex = this.dbfFile.getFieldIndex(descriptor.getName());

        if (dbfIndex < 0) {
            // Someone asked to load a field
            // which does not exist in the DBF file. This can happen
            // in editing process when a field has been added
            // in the current editing session, so we simply do nothing.
            // The expansion manager is expected to manage those new fields
            // and their values.
            long curr_time = System.currentTimeMillis();
            // This ensures not more than one log every 2 seconds
            if (curr_time - lastLogTime > 2000) {
                LOG.info("Warning: The requested field does not exist in the DBF file. Assumed it's a new field in editing mode.");
                lastLogTime = curr_time;
            }
            return;
        }

        String value = null;
        try {
            value = this.dbfFile.getStringFieldValue(rowIndex, dbfIndex);
        } catch (DataException e) {
            throw new ReadException(this.getName(), e);
        }
        value = value.trim();
        int fieldType = descriptor.getType();
        switch (fieldType) {
            case DataTypes.STRING:
                featureProvider.set(descriptor.getIndex(), value);
                break;

            case DataTypes.DOUBLE:
                try {
                    featureProvider.set(descriptor.getIndex(), new Double(value));
                } catch (NumberFormatException e) {
                    featureProvider.set(descriptor.getIndex(), null);
                }
                break;

            case DataTypes.INT:
                try {
                    featureProvider.set(descriptor.getIndex(), new Integer(value));
                } catch (NumberFormatException e) {
                    featureProvider.set(descriptor.getIndex(), null);
                }
                break;

            case DataTypes.FLOAT:
                try {
                    featureProvider.set(descriptor.getIndex(), new Float(value));
                } catch (NumberFormatException e) {
                    featureProvider.set(descriptor.getIndex(), null);
                }
                break;

            case DataTypes.LONG:
                try {
                    featureProvider.set(descriptor.getIndex(), new Long(value));
                } catch (NumberFormatException e) {
                    featureProvider.set(descriptor.getIndex(), null);
                }
                break;

            case DataTypes.BOOLEAN:
                if (value.equalsIgnoreCase("T")) {
                    featureProvider.set(descriptor.getIndex(), Boolean.TRUE);
                } else {
                    featureProvider.set(descriptor.getIndex(), Boolean.FALSE);

                }
                break;

            case DataTypes.BYTE:
                try {
                    featureProvider.set(descriptor.getIndex(), new Byte(value));
                } catch (NumberFormatException e) {
                    featureProvider.set(descriptor.getIndex(), null);
                }
                break;

            case DataTypes.DATE:
                if (value.equals("")) {
                    value = null;
                    return;
                }
                Date dat = null;
                DateFormat df = new SimpleDateFormat("yyyyMMdd");
                try {
                    dat = df.parse(value);
                } catch (ParseException e) {
                    throw new InvalidDateException(df.toString(), value, this.getProviderName(), e);
                }
                featureProvider.set(descriptor.getIndex(), dat);
                break;

            default:
                featureProvider
                        .set(descriptor.getIndex(), descriptor.getDefaultValue());
                break;
        }
    }

    private static class InvalidDateException extends ReadException {
        public InvalidDateException(String dateFormat, String value, String store, Throwable cause) {
            super(
                    "Can't parse date value '%(value)' with format '%(dateFormat)' in dbf '%(store)'.",
                    cause,
                    "Cant_parse_date_value_XvalueX_with_format_XdateFormatX_in_dbf_XstoreX",
                    0
            );
            setValue("dateFormat",dateFormat);
            setValue("value", value);
            setValue("store",store);
        }
    }

    /**
     * *
     * NOT supported in Alter Mode
     *
     * @param index
     * @return
     * @throws ReadException
     */
    protected FeatureProvider getFeatureProviderByIndex(long index) throws DataException {
        return this
                .getFeatureProviderByIndex(index, this.getStoreServices()
                        .getDefaultFeatureType());
    }

    public long getFeatureCount() throws ReadException, OpenException,
            ResourceNotifyChangesException {
        this.open();
        return ((Long) getResource().execute(new ResourceAction() {
            public Object run() throws Exception {
                return Long.valueOf(dbfFile.getRecordCount());
            }
        })).longValue();
    }

    public FeatureSetProvider createSet(FeatureQuery query, FeatureType featureType)
            throws DataException {
        return new DBFSetProvider(this, query, featureType);
    }

    public boolean canCreate() {
        return true;
    }

    public boolean canWriteGeometry(int geometryType, int geometrySubType) throws DataException {
        return false;
    }

    public void open() throws OpenException {
        if (this.dbfFile.isOpen()) {
            return;
        }
        try {
            getResource().execute(new ResourceAction() {
                public Object run() throws Exception {
                    openFile();
                    resourcesOpen();
                    return null;
                }
            });

        } catch (ResourceExecuteException e) {
            throw new OpenException(this.getProviderName(), e);
        }
    }

    protected void openFile() throws FileNotFoundException,
            UnsupportedVersionException, IOException, DataException {
        this.dbfFile.open();
    }

    public void close() throws CloseException {
        if( loTengoEnUso ) {
            return;
        }
        if (dbfFile == null || !this.dbfFile.isOpen()) {
            return;
        }
        super.close();

        //Cerrar recurso
        try {
            getResource().execute(new ResourceAction() {
                public Object run() throws Exception {
                    closeFile();
                    resourcesNotifyClose();
                    return null;
                }
            });
        } catch (ResourceExecuteException  e) {
            throw new CloseException(this.getProviderName(), e);
        }
    }

    protected void closeFile() throws CloseException {
        this.dbfFile.close();
    }

    @Override
    protected void doDispose() throws BaseException {
        this.close();
        dbfFile = null;
        disposeResource();
        super.doDispose();
    }

    protected void disposeResource() {
        this.dbfResource.removeConsumer(this);
        dbfResource = null;
    }

    public boolean closeResourceRequested(ResourceProvider resource) {
        try {
            this.close();
        } catch (CloseException e) {
            return false;
        }
        return true;
    }

    public boolean allowWrite() {
        if(allowDuplicatedFieldNames){
            return false;
        }
        return this.dbfFile.isWritable();
    }

    public void refresh() throws OpenException {
        try {
            this.close();
        } catch (CloseException e) {
            throw new OpenException(this.getProviderName(), e);
        }
        this.open();
        try {
            this.initFeatureType();
        } catch (InitializeException e) {
            throw new OpenException(this.getProviderName(), e);
        }
    }

    /**
     *
     * @param index
     * @param featureType
     * @return
     * @throws ReadException
     */
    protected FeatureProvider getFeatureProviderByIndex(long index,
            FeatureType featureType) throws DataException {
        FeatureProvider featureProvider = this.createFeatureProvider(featureType);
        featureProvider.setOID(new Long(index));
        return featureProvider;
    }

    protected void initFeatureProviderByIndex(FeatureProvider featureProvider,
            long index, FeatureType featureType) throws DataException {
        featureProvider.setOID(new Long(index));
    }

    /**
     *
     * @param featureProvider
     * @throws DataException
     */
    protected void loadFeatureProviderByIndex(FeatureProvider featureProvider)
            throws DataException {

        long index = ((Long) featureProvider.getOID()).longValue();
        int rec_count = this.dbfFile.getRecordCount();

        if (index >= rec_count) {

            ReadException rex = new ReadException(this.getName(),
                    new ArrayIndexOutOfBoundsException(
                            "Index of requested feature ("
                            + index + ") is >= record count (" + rec_count + ")"));

            LOG.info("Error while loading feature. ", rex);
            throw rex;
        }

        Iterator iterator = featureProvider.getType().iterator();
        while (iterator.hasNext()) {
            FeatureAttributeDescriptor descriptor
                    = (FeatureAttributeDescriptor) iterator.next();
            this.loadValue(featureProvider, (int) index, descriptor);
        }
    }

    public int getOIDType() {
        return DataTypes.LONG;
    }

    public Object createNewOID() {
        if (this.counterNewsOIDs < 0) {
            try {
                this.counterNewsOIDs = this.getFeatureCount();
            } catch (DataException e) {
                e.printStackTrace();
            }

        } else {
            this.counterNewsOIDs++;
        }
        return new Long(counterNewsOIDs);
    }

    public boolean supportsAppendMode() {
        return true;
    }

    public void append(final FeatureProvider featureProvider)
            throws DataException {
        getResource().execute(new ResourceAction() {
            public Object run() throws Exception {
                writer.append(getStoreServices().createFeature(featureProvider));
                return null;
            }
        });
    }

    public void beginAppend() throws DataException {
        this.close();
        getResource().execute(new ResourceAction() {
            public Object run() throws Exception {
                writer.begin(getDBFParameters(),
                        getStoreServices().getDefaultFeatureType(),
                        getStoreServices().getFeatureStore().getFeatureCount());
                return null;
            }
        });
    }

    public void endAppend() throws DataException {
        getResource().execute(new ResourceAction() {
            public Object run() throws Exception {
                writer.end();
                resourcesNotifyChanges();
                counterNewsOIDs = -1;
                return null;
            }
        });
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.gvsig.fmap.dal.resource.spi.ResourceConsumer#resourceChanged(org.
     * gvsig.fmap.dal.resource.spi.ResourceProvider)
     */
    public void resourceChanged(ResourceProvider resource) {
        if (this.getStoreServices()!=null){
            this.getStoreServices().notifyChange(
                    DataStoreNotification.RESOURCE_CHANGED,
                    resource);
        }
    }

    /**
     *
     * @throws ResourceNotifyChangesException
     */
    protected void resourcesNotifyChanges()
            throws ResourceNotifyChangesException {
        this.dbfResource.notifyChanges();
    }

    /**
     * @throws ResourceNotifyCloseException
     *
     */
    protected void resourcesNotifyClose() throws ResourceNotifyCloseException {
        this.dbfResource.notifyClose();
    }

    /**
     * @throws ResourceNotifyOpenException
     *
     */
    protected void resourcesOpen() throws ResourceNotifyOpenException {
        this.dbfResource.notifyOpen();
    }

    public Object getSourceId() {
        return this.getDBFParameters().getFile();
    }

    public String getName() {
        String name = this.getDBFParameters().getFile().getName();
        int n = name.lastIndexOf(".");
        if (n < 1) {
            return name;
        }
        return name.substring(0, n);
    }

    public String getFullName() {
        return this.getDBFParameters().getFile().getAbsolutePath();
    }

    protected void resourceCloseRequest() throws ResourceException {
        this.dbfResource.closeRequest();
    }

    public ResourceProvider getResource() {
        return dbfResource;
    }
}
