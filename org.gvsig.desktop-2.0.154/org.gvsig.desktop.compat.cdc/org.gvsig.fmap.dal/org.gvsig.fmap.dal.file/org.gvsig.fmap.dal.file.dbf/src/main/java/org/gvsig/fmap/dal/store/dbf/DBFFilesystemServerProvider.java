/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.dbf;

import java.io.File;
import java.io.IOException;

import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataServerExplorer;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.NewDataStoreParameters;
import org.gvsig.fmap.dal.exception.CreateException;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.FileNotFoundException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.RemoveException;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;
import org.gvsig.fmap.dal.resource.ResourceAction;
import org.gvsig.fmap.dal.resource.exception.ResourceExecuteException;
import org.gvsig.fmap.dal.resource.file.FileResource;
import org.gvsig.fmap.dal.resource.spi.ResourceConsumer;
import org.gvsig.fmap.dal.resource.spi.ResourceProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.AbsolutePathRequiredException;
import org.gvsig.fmap.dal.serverexplorer.filesystem.impl.AbstractFilesystemServerExplorerProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProvider;
import org.gvsig.fmap.dal.serverexplorer.filesystem.spi.FilesystemServerExplorerProviderServices;
import org.gvsig.tools.dataTypes.DataTypes;

public class DBFFilesystemServerProvider extends AbstractFilesystemServerExplorerProvider 
	implements FilesystemServerExplorerProvider, ResourceConsumer {

	protected FilesystemServerExplorerProviderServices serverExplorer;

	public String getDataStoreProviderName() {
		return DBFStoreProvider.NAME;
	}

	public int getMode() {
		return DataServerExplorer.MODE_FEATURE;
	}

	public boolean accept(File pathname) {
		return (pathname.getName().toLowerCase().endsWith(".dbf"));
	}

	public String getDescription() {
		return DBFStoreProvider.DESCRIPTION;
	}

	public DataStoreParameters getParameters(File file) throws DataException {
		DataManager manager = DALLocator.getDataManager();
		DBFStoreParameters params = (DBFStoreParameters) manager
				.createStoreParameters(this.getDataStoreProviderName());
		params.setDBFFile(file.getPath());
		return params;
	}

	public boolean canCreate() {
		return true;
	}

	public boolean canCreate(NewDataStoreParameters parameters) {
		DBFNewStoreParameters params = (DBFNewStoreParameters) parameters;
		if (params.getDBFFile().getParentFile().canWrite()) {
			return false;
		}
		if (params.getDBFFile().exists()) {
			if (!params.getDBFFile().canWrite()) {
				return false;
			}
		}
		if (params.getDefaultFeatureType() == null) {
			return false;
		}
		return true;
	}

	public void create(NewDataStoreParameters parameters, boolean overwrite)
			throws CreateException {

		final DBFNewStoreParameters params = (DBFNewStoreParameters) parameters;
		File file = params.getDBFFile();
		
		if (!file.isAbsolute()) {
			throw new AbsolutePathRequiredException(file.getPath());
		}


		if (file.exists()) {
			if (overwrite) {
//				if (!file.delete()) {
//					throw new CreateException(this.getDataStoreProviderName(),
//							new IOException(
//							"cannot delete file"));
//				}
			} else {
				throw new CreateException(this.getDataStoreProviderName(),
						new IOException(
						"file already exist"));
			}
		}

		final FileResource resource;
		try {

			resource = (FileResource) this.serverExplorer
					.getServerExplorerProviderServices().createResource(
							FileResource.NAME,
					new Object[] { file.getAbsolutePath() });
		} catch (InitializeException e1) {
			throw new CreateException(params.getDBFFileName(), e1);
		}
		resource.addConsumer(this);

		try {
			resource.execute(new ResourceAction() {
				public Object run() throws Exception {
					DBFFeatureWriter writer =
							new DBFFeatureWriter(getDataStoreProviderName());

					writer.begin(
                                                params, 
                                                fixFeatureType(params.getDefaultFeatureType()), 
                                                0
                                        );
					writer.end();
					writer.dispose();
					resource.notifyChanges();
					return null;
				}
			});
		} catch (ResourceExecuteException e) {
			throw new CreateException(this.getDataStoreProviderName(), e);
		} finally {
			resource.removeConsumer(this);
		}


	}


	protected NewDataStoreParameters createInstanceNewDataStoreParameters() {
		return new DBFNewStoreParameters();
	}


	public NewDataStoreParameters getCreateParameters() {
		NewFeatureStoreParameters params = (NewFeatureStoreParameters) this
				.createInstanceNewDataStoreParameters();
		params.setDefaultFeatureType(this.serverExplorer
				.getServerExplorerProviderServices().createNewFeatureType());
		return params;
	}

	public void initialize(FilesystemServerExplorerProviderServices serverExplorer) {
		this.serverExplorer = serverExplorer;

	}

	public void remove(DataStoreParameters parameters) throws RemoveException {
		DBFStoreParameters params = (DBFStoreParameters) parameters;
		File file = params.getDBFFile();
		if (!file.exists()) {
			throw new RemoveException(this.getDataStoreProviderName(),
					new FileNotFoundException(params.getDBFFile()));
		}
		if (!file.delete()) {
			throw new RemoveException(this.getDataStoreProviderName(),
					new IOException()); // FIXME Exception
		}
	}

	public boolean closeResourceRequested(ResourceProvider resource) {
		// while it is using a resource anyone can't close it
		return !(this.equals(resource));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.gvsig.fmap.dal.resource.spi.ResourceConsumer#resourceChanged(org.
	 * gvsig.fmap.dal.resource.spi.ResourceProvider)
	 */
	public void resourceChanged(ResourceProvider resource) {
		// Do nothing
	}

        protected EditableFeatureType fixFeatureType(FeatureType featureType) {
            EditableFeatureType newFeatureType;
            
            if( featureType instanceof EditableFeatureType ) {
                newFeatureType = (EditableFeatureType) featureType.getCopy();
            } else {
                newFeatureType = featureType.getEditable();
            }
            
            for( int i=0; i<newFeatureType.size(); i++ ) {
                EditableFeatureAttributeDescriptor attr = (EditableFeatureAttributeDescriptor) newFeatureType.getAttributeDescriptor(i);
                String s; 
                switch(attr.getType()) {
                case DataTypes.INT:
                    s = String.valueOf(Integer.MAX_VALUE);
                    attr.setSize(18);
                    attr.setPrecision(0);
                    break;
                case DataTypes.LONG:
                    s = String.valueOf(Long.MAX_VALUE);
                    attr.setSize(18);
                    attr.setPrecision(0);
                    break;
                case DataTypes.FLOAT:
                    attr.setSize(18);
                    attr.setPrecision(6);
                    break;
                case DataTypes.DOUBLE:
                    attr.setSize(18);
                    attr.setPrecision(6);
                    break;
                }
            }
            return newFeatureType;
        }
        
}
