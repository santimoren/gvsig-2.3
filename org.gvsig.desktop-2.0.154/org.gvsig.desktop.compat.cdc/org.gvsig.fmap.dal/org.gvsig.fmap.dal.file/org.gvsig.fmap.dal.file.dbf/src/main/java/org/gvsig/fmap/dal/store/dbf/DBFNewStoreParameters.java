/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.dbf;

import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.NewFeatureStoreParameters;

public class DBFNewStoreParameters extends DBFStoreParameters implements
		NewFeatureStoreParameters {

    public static final String PARAMETERS_DEFINITION_NAME = "DBFNewStoreParameters";

    public static final String FEATURETYPE_PARAMTER_NAME = "FeatureType";
    
    public static final String CODEPAGE_PARAMTER_NAME = "codePage";
    
    public DBFNewStoreParameters() {
    	super(PARAMETERS_DEFINITION_NAME);
    }

	public byte getCodePage(){
		return ((Byte) this.getDynValue(CODEPAGE_PARAMTER_NAME)).byteValue();
	}

	public void setCodePage(byte value){
		setDynValue(CODEPAGE_PARAMTER_NAME, new Byte(value));
	}

	public EditableFeatureType getDefaultFeatureType() {
		return (EditableFeatureType) this.getDynValue(FEATURETYPE_PARAMTER_NAME);
	}

	public void setDefaultFeatureType(FeatureType featureType) {
		this.setDynValue(FEATURETYPE_PARAMTER_NAME, featureType);
	}

}
