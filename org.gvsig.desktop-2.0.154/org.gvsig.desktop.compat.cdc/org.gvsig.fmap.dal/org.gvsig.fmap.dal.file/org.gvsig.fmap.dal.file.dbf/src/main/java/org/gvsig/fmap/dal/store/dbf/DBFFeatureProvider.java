/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.dbf;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadRuntimeException;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.DefaultFeatureProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.primitive.Envelope;

public class DBFFeatureProvider extends DefaultFeatureProvider {
	protected DBFStoreProvider store;
	protected boolean loading;
	protected boolean loaded;

	public DBFFeatureProvider(DBFStoreProvider store, FeatureType type) {
		super(type);
		this.store = store;
		loading = false;
		loaded = false;
	}

	protected void load() {
		if (loading || loaded || this.isNew()) {
			return;
		}
		loading = true;
		try {
			this.store.loadFeatureProviderByIndex(this);
		} catch (DataException e) {
			throw new ReadRuntimeException(getNameForMessages(), this.getOID(), e);
		} finally {
			loading = false;
			loaded = true;
		}
	}
        
        protected String getNameForMessages() {
            // Solo con proposito de mostrar en mensajes de error.
            try {
                return this.store.getName();
            } catch(Exception ex) {
                return "unknown";
            }
        }

	public void set(int i, Object value) {
		this.load();
		super.set(i, value);
	}

	public void set(String name, Object value) {
		this.load();
		super.set(featureType.getIndex(name), value);
	}

	public Object get(int i) {
		this.load();
		return super.get(i);
	}

	public Object get(String name) {
		this.load();
		return super.get(name);
	}

	public Geometry getDefaultGeometry() {
		return null;
	}

	public Envelope getDefaultEnvelope() {
		return null;
	}

	public void setOID(Object oid) {
		this.loaded = false;
		super.setOID(oid);
	}

	public FeatureProvider getCopy() {
		this.load();
		return super.getCopy();
	}




}
