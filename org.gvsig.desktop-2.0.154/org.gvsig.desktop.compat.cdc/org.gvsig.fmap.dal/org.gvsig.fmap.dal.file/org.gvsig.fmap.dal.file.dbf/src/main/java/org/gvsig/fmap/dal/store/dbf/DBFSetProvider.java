/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.dbf;

import java.util.NoSuchElementException;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadRuntimeException;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureProviderIterator;
import org.gvsig.fmap.dal.feature.spi.AbstractFeatureSetProvider;
import org.gvsig.fmap.dal.feature.spi.FeatureProvider;
import org.gvsig.tools.exception.BaseException;

/**
 * @author jjdelcerro
 *
 */
public class DBFSetProvider extends AbstractFeatureSetProvider {

	public DBFSetProvider(DBFStoreProvider store, FeatureQuery query,
			FeatureType featureType)
			throws DataException {
		super(store, query, featureType);
	}

	public boolean canFilter() {
		return false;
	}

	public boolean canIterateFromIndex() {
		return true;
	}

	public boolean canOrder() {
		return false;
	}

	public long getSize() throws DataException {
		return getStore().getFeatureCount();
	}

	public boolean isEmpty() throws DataException {
		return getSize() == 0;
	}

	@Override
	protected void doDispose() throws BaseException {
	}

	protected AbstractFeatureProviderIterator createIterator(long index)
			throws DataException {
		return new DBFIterator((DBFStoreProvider) getStore(), getFeatureType(),
				index);
	}

	protected AbstractFeatureProviderIterator createFastIterator(long index)
			throws DataException {
		return new FastDBFIterator((DBFStoreProvider) getStore(),
				getFeatureType(), index);
	}

	protected class DBFIterator extends AbstractFeatureProviderIterator {
		protected long index;
		protected FeatureType type;
		protected long count;

		public DBFIterator(DBFStoreProvider store, FeatureType type,
				long startOn) throws DataException {
			super(store);
			this.index = startOn;
			this.type = type;
			this.count = store.getFeatureCount();
		}

		protected boolean internalHasNext() {
			return this.count > index;
		}

		protected Object internalNext() {
			if (index >= this.count) {
				throw new NoSuchElementException();
			}
			try {

				FeatureProvider ret =
						getDBFStoreProvider().getFeatureProviderByIndex(index,
								this.type);
				index++;
				return ret;
			} catch (DataException e) {
				throw new ReadRuntimeException(getNameForMessages(), index,e);
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		protected void internalDispose() {
			type = null;
		}

		protected DBFStoreProvider getDBFStoreProvider() {
			return (DBFStoreProvider) getFeatureStoreProvider();
		}
                
                protected String getNameForMessages() {
                    // Solo con proposito de mostrar en mensajes de error.
                    try {
                        return getFeatureStoreProvider().getName();
                    } catch(Exception ex) {
                        return "unknown";
                    }
                }

		@Override
		protected void doDispose() throws BaseException {
			// Nothing to do
		}
	}

	protected class FastDBFIterator extends DBFIterator {
		protected FeatureProvider data;

		public FastDBFIterator(DBFStoreProvider store, FeatureType type,
				long startOn) throws DataException {
			super(store, type, startOn);
			this.data = getFeatureStoreProvider().createFeatureProvider(type);
		}

		protected Object internalNext() {
			if (index >= this.count) {
				throw new NoSuchElementException();
			}

			try {
				getDBFStoreProvider().initFeatureProviderByIndex(this.data,
						index, type);
			} catch (DataException e) {
				throw new ReadRuntimeException(getNameForMessages(), index, e);
			}
			index++;
			return this.data;
		}

		protected void internalDispose() {
			super.internalDispose();
			data = null;
		}
	}
}
