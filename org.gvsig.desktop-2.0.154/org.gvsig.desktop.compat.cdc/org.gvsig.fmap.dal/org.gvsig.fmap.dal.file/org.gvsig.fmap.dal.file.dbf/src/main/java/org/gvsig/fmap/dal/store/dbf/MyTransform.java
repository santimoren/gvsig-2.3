/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.dal.store.dbf;

import java.util.ArrayList;
import java.util.List;

import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.AbstractFeatureStoreTransform;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class MyTransform extends AbstractFeatureStoreTransform {

    private String geomName;
	private String xname;
	private String yname;

    private GeometryManager geomManager;

	/**
	 * A default constructor with out parameters
	 */
	public MyTransform() {
		this.geomManager = GeometryLocator.getGeometryManager();

	}

	/**
	 * Initializes the transform by assigning the source store and the names of
	 * the necessary attributes.
	 *
	 * @param store
	 *            source store.
	 *
	 * @param geomName
	 *            name of the geometry attribute in the default feature type
	 *            from the source store.
	 *
	 * @param xname
	 *            name of the attribute containing the X coordinates
	 *
	 * @param yname
	 *            name of the attribute containing the Y coordinates
	 *
	 * @throws DataException
	 */
	public MyTransform initialize(FeatureStore store, String geomName,
			String xname, String yname) throws DataException {

		// Initialize some data
		setFeatureStore(store);
		this.geomName = geomName;
		this.xname = xname;
		this.yname = yname;

		// obtain the feature type, add the new attribute
		// and keep a reference to the
		// resulting feature type
		EditableFeatureType type = getFeatureStore().getDefaultFeatureType()
				.getEditable();
		type.add(geomName, DataTypes.GEOMETRY);
		List list = new ArrayList(1);
		list.add(type.getNotEditableCopy());
		setFeatureTypes(list, (FeatureType) list.get(0));
		return this;
	}

	/**
	 * Applies this transform to a target editable feature, using data from the
	 * source feature.
	 */
	public void applyTransform(Feature source, EditableFeature target)
			throws DataException {

		// copy source feature data over target feature
		target.copyFrom(source);

		// calculate and assign new attribute's value
		Geometry geom;
		try {
			geom = geomManager.createPoint(source.getDouble(xname), source
					.getDouble(yname), Geometry.SUBTYPES.GEOM2D);
		} catch (CreateGeometryException e) {
			throw new XYTransformException(e);
		}
		target.setGeometry(this.geomName, geom);
	}

        public class XYTransformException extends DataException {
            private final static String MESSAGE_FORMAT = "Problems apply XYTransform, can't create point.";
            private final static String MESSAGE_KEY = "_XYTransformException";
            private static final long serialVersionUID = -3556249688758572523L;

            public XYTransformException(Throwable cause) {
                    super(MESSAGE_FORMAT, cause, MESSAGE_KEY, serialVersionUID);
            }            
        }

    public void saveToState(PersistentState state) throws PersistenceException {
		state.set("xname", xname);
		state.set("yname", yname);
		state.set("geomName", geomName);
	}

    public void loadFromState(PersistentState state)
			throws PersistenceException {
		this.xname = state.getString("xname");
		this.yname = state.getString("yname");
		this.geomName = state.getString("geomName");
	}

    /**
	 * Returns the FeatureType to use to get data from original store
	 */
	public FeatureType getSourceFeatureTypeFrom(FeatureType targetFeatureType) {
		EditableFeatureType ed = targetFeatureType.getEditable();
		ed.remove(this.geomName);
		return ed.getNotEditableCopy();
	}

	/**
	 * Informs that original values of store don't will be modified
	 */
	public boolean isTransformsOriginalValues() {
		return false;
	}

}
