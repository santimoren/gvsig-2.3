/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.dal.store.dbf;

import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import org.gvsig.fmap.dal.FileHelper;
import org.gvsig.fmap.dal.OpenDataStoreParameters;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.serverexplorer.filesystem.FilesystemStoreParameters;
import org.gvsig.fmap.dal.spi.AbstractDataParameters;
import org.gvsig.fmap.dal.spi.DataStoreProviderServices;
import org.gvsig.tools.dynobject.DelegatedDynObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DBFStoreParameters extends AbstractDataParameters implements
		OpenDataStoreParameters, FilesystemStoreParameters {

    private static final Logger logger = LoggerFactory.getLogger(DBFStoreParameters.class);

    public static final String PARAMETERS_DEFINITION_NAME = "DBFStoreParameters";

	public static final String DBFFILE_PARAMTER_NAME = "dbfFile";
	public static final String ENCODING_PARAMTER_NAME = "encoding";
	public static final String HANDLE_DATES_AS_STRINGS = "handleDatesAsStrings";
	public static final String DATE_FORMAT = "dateFormat";
	public static final String LOCALE = "locale";
    public static final String ALLOW_DUPLICATED_FIELD_NAMES = "allowDuplicatedFieldNames";

	private DelegatedDynObject parameters;

	public DBFStoreParameters() {
		this(PARAMETERS_DEFINITION_NAME);
	}

	protected DBFStoreParameters(String parametersDefinitionName) {
		this(parametersDefinitionName, DBFStoreProvider.NAME);
	}

	public DBFStoreParameters(String parametersDefinitionName, String name) {
		super();
		this.parameters = (DelegatedDynObject) FileHelper.newParameters(parametersDefinitionName);
		this.setDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME, name);
	}

	public String getDataStoreName() {
		return (String) this.getDynValue(DataStoreProviderServices.PROVIDER_PARAMTER_NAME);
	}

	public String getDescription() {
		return this.getDynClass().getDescription();
	}

	public boolean isValid() {
		return (this.getDBFFileName() != null);
	}

	public File getFile() {
		return (File) this.getDynValue(DBFFILE_PARAMTER_NAME);
	}

	public void setFile(File file) {
		this.setDynValue(DBFFILE_PARAMTER_NAME, file);
	}

	public void setFile(String fileName) {
		this.setDynValue(DBFFILE_PARAMTER_NAME, fileName);
	}

	public String getDBFFileName() {
		if( this.getDBFFile() == null ) {
			return null;
		}
		return this.getDBFFile().getAbsolutePath();
	}

	public File getDBFFile() {
		return (File) this.getDynValue(DBFFILE_PARAMTER_NAME);
	}

	public void setDBFFile(File file) {
		this.setDynValue(DBFFILE_PARAMTER_NAME, file);
	}

	public void setDBFFile(String fileName) {
		this.setDynValue(DBFFILE_PARAMTER_NAME, fileName);
	}

	public String getEncodingName() {
		String s = (String) getDynValue(ENCODING_PARAMTER_NAME);
                if( StringUtils.isBlank(s) ) {
                        return null;
                }
                if( "DEFAULT".equalsIgnoreCase(s.trim()) ) {
                    return null;
                }
                return s.trim();
        }

	public Charset getEncoding() {
		String name = getEncodingName();
                if( StringUtils.isBlank(name) ) {
                        return null;
                }
		return Charset.forName(name);
	}

	public void setEncoding(String encoding) {
		this.setEncoding(Charset.forName(encoding));
    }

    public boolean handleDatesAsStrings() {
        Boolean x = (Boolean) getDynValue(HANDLE_DATES_AS_STRINGS);
        return BooleanUtils.isTrue(x);
    }

    public boolean allowDuplicatedFieldNames() {
        Boolean x = (Boolean) getDynValue(ALLOW_DUPLICATED_FIELD_NAMES);
        return BooleanUtils.isTrue(x);
    }

    public void setEncoding(Charset charset) {
        this.setDynValue(ENCODING_PARAMTER_NAME, charset.name());
	}

	protected DelegatedDynObject getDelegatedDynObject() {
		return parameters;
	}

        public Locale getLocale() {
            try {
                    String s = (String) this.getDynValue(LOCALE);
                    if( s.trim().length()==0 ) {
                            return null;
                    }
                    if( "DEFAULT".equalsIgnoreCase(s.trim()) ) {
                        return Locale.getDefault();
                    }
                    Locale locale = null;
                    // locale = Locale.forLanguageTag(s); // Since java 1.7
                    String[] ss = s.split("-");
                    switch(ss.length) {
                    case 1:
                       locale = new Locale(ss[0]);
                        break;
                    case 2:
                       locale = new Locale(ss[0],ss[1]);
                       break;
                    case 3:
                    default:
                       locale = new Locale(ss[0],ss[1],ss[2]);
                       break;
                    }
                    return locale;
            } catch( Exception ex) {
                    logger.warn("Can't get locale from DBF parameters.", ex);
                    return Locale.getDefault();
            }
        }

        public String getDateFormat() {
		return (String) getDynValue(DATE_FORMAT);
	}

        public void validate() throws ValidateDataParametersException {
            super.validate();
            String sfmt = getDateFormat();
            if( !StringUtils.isBlank(sfmt) ) {
                try {
                    SimpleDateFormat datefmt = new SimpleDateFormat(sfmt, getLocale());
                } catch(Exception ex) {
                    throw new InvalidDateFormatException(sfmt,ex);
                }
            }
        }

        private static class InvalidDateFormatException extends ValidateDataParametersException {

            public InvalidDateFormatException(String sfmt, Throwable cause) {
                super(
                        "Date format specified '%(dataformat)' is not valid.",
                        cause,
                        "Date_format_specified_XdataformatX_is_not_valid",
                        0
                );
                setValue("dataformat",sfmt);

            }
        }

}
