/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.i18n;

import org.gvsig.i18n.tools.impl.DefaultI18Manager;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

/**
 * This library is in charge of providing a richer translation manager than the
 * one used by default.
 * 
 * 
 * @author christian
 * 
 */
public class MessagesLibrary extends AbstractLibrary {

	public void doRegistration() {
		super.doRegistration();
		this.registerAsServiceOf(ToolsLibrary.class);
		require(ToolsLocator.class);
	}

	protected void doInitialize() throws LibraryException {
		ToolsLocator.registerI18nManager(DefaultI18Manager.class);
	}

	protected void doPostInitialize() throws LibraryException {
        // Nothing to do
	}

}
