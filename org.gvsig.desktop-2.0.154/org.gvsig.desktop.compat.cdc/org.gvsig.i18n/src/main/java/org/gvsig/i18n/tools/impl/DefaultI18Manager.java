/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.i18n.tools.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gvsig.i18n.Messages;
import org.gvsig.tools.i18n.I18nManager;

/**
 * This manager is in charge of providing with a richer text translation than
 * the one used by default.
 * 
 * 
 * @author gvSIG Team
 * 
 */
public class DefaultI18Manager implements I18nManager {

	public String getTranslation(String message) {
		return Messages.getText(message);
	}

        public String getTranslation(String message, String[] args) {
		return Messages.getText(message,args);
        }

	public void addResourceFamily(String family, ClassLoader loader,
			String callerName) {
		if (!Messages.hasLocales()) {
			Messages.addLocale(Locale.getDefault());
		}
		Messages.addResourceFamily(family, loader, callerName);
	}

        public void addResourceFamily(String family, File folder) {
            if (!Messages.hasLocales()) {
                Messages.addLocale(Locale.getDefault());
            }
            try {
                Messages.addResourceFamily(family, folder);
            } catch (MalformedURLException ex) {
                throw new RuntimeException(ex);
            }
        }
}
