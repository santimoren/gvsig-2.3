/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.i18n.utils;

import java.util.HashMap;

/**
 * Convenience class to manage the attributes of the project tag from the config.xml
 * file.
 * 
 * 
 * @author cesar
 *
 */
public class Project {

	public String dir;
	public String basename;
	
	/**
	 * The directory which stores the property files of the project.
	 */
	public String propertyDir;
	
	/**
	 * Source of the keys: whether they are loaded from the property files of
	 * the project or they are searched inside the sources.
	 * <ul><li>sourceKeys="sources": The keys are searched inside the Java source
	 * files and the config.xml files from the extensions.</li>
	 * <li>sourceKeys="properties": The keys are loaded from the property files
	 * of each project.</li></ul> 
	 */
	public String sourceKeys;
	
	/**
	 * Stores the associated dictionaries. Each value of the
	 * HashMap is a Properties object, containing the translations for each
	 * language.
	 * 
	 * <p>Example:</p>
	 * Properties dictionary = (Properties)dictionaries.get("es");
	 * 
	 */
	public HashMap dictionaries;
	
	/**
	 * Stores the subdirectories containing sources. When searching for keys in
	 * the source files, only these subdirectories will be searched.
	 */
	public String[] srcDirs;
	
}
