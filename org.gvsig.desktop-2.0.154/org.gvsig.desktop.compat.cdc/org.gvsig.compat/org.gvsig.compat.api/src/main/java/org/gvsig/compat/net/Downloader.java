/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.net;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.net.UnknownHostException;



/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public interface Downloader {
    /**
     * Downloads an URL into a temporary file that is removed the next time the
     * tempFileManager class is called, which means the next time gvSIG is launched.
     *
     * @param url
     * @param name
     * @param cancel
     * @return
     * @throws IOException
     * @throws ConnectException
     * @throws UnknownHostException
     */
    public File downloadFile(URL url, String name, ICancellable cancel) throws IOException,ConnectException, UnknownHostException;
    

    /**
     * Downloads a URL using the HTTP Post protocol
     * @param url
     * The server URL
     * @param data
     * The data to send in the request
     * @param name
     * A common name for all the retrieved files
     * @param cancel
     * Used to cancel the downloads
     * @return
     * The retrieved file
     * @throws IOException
     * @throws ConnectException
     * @throws UnknownHostException
     */
    public File downloadFile(URL url, String data, String name, ICancellable cancel) throws IOException,ConnectException, UnknownHostException;

    public File downloadFile(URL url, String data, String name, ICancellable cancel, int maxbytes) throws IOException,ConnectException, UnknownHostException;

    public void removeURL(URL url);
    
    public void removeURL(Object url);
    
    /**
     * Cleans every temporal file previously downloaded.
     */
    public void cleanUpTempFiles();
       

}
