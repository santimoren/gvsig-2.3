/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.lang;

/**
 * String Utilities used for Java SE-ME compatibility.
 * 
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public interface StringUtils {
    
    /**
     * Compatible implementation of the String.split(regexp) method.
     * @see String#split(String)
     */
    String[] split(String input, String regex);
    
    /**
     * Compatible implementation of the String.split(regexp, limit) method.
     * @see String#split(String, int)
     */
    String[] split(String input, String regex, int limit);
    
    /**
     * Compatible implementation of the String.replaceAll(regexp, replacement)
     * method.
     * 
     * @see String#replaceAll(String, String)
     */
    String replaceAll(String input, String regex,
            String replacement);
    
    /**
     * Compatible implementation of the String.replaceFirst(regexp, replacement)
     * method.
     * 
     * @see String#replaceFirst(String, String)
     */
    String replaceFirst(String input, String regex,
            String replacement);

	/**
	 * Compares two Strings, maybe taking into account the current locale.
	 * 
	 * @param source
	 *            the source text
	 * @param target
	 *            the target text to compare to
	 * @param localized
	 *            if the comparison must be made with the current locale (ex: if
	 *            making a difference between accented characters or not).
	 * @return Returns an integer value. Value is less than zero if source is
	 *         less than target, value is zero if source and target are equal,
	 *         value is greater than zero if source is greater than target.
	 */
	int compare(String source, String target, boolean localized);
}
