/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat;

import org.gvsig.compat.lang.GraphicsUtils;
import org.gvsig.compat.lang.MathUtils;
import org.gvsig.compat.lang.StringUtils;
import org.gvsig.compat.net.Downloader;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Initialization of the libCompat library.
 * 
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class CompatLibrary extends AbstractLibrary {

    public void doRegistration() {
        registerAsAPI(CompatLibrary.class);
    }

	protected void doInitialize() throws LibraryException {
		// Nothing to do
	}

	protected void doPostInitialize() throws LibraryException {
		// Validate there is any implementation registered.
		StringUtils stringUtils = CompatLocator.getStringUtils();
		
		if (stringUtils == null) {
			throw new ReferenceNotRegisteredException(
					CompatLocator.STRINGUTILS_NAME, CompatLocator.getInstance());
		}
		
		// Validate there is any implementation registered.
		MathUtils mathUtils = CompatLocator.getMathUtils();
		
		if (mathUtils == null) {
			throw new ReferenceNotRegisteredException(
					CompatLocator.MATHUTILS_NAME, CompatLocator.getInstance());
		}
		
		// Validate there is any implementation registered.
		GraphicsUtils gUtils = CompatLocator.getGraphicsUtils();
		
		if (gUtils == null) {
			throw new ReferenceNotRegisteredException(
					CompatLocator.GRAPHICSUTILS_NAME, CompatLocator.getInstance());
		}
		
		  // Validate there is any implementation registered for the Downloader.
        Downloader downloader = CompatLocator.getDownloader();
        
        if (downloader == null) {
            throw new ReferenceNotRegisteredException(
                    CompatLocator.DOWNLOADER_NAME, CompatLocator.getInstance());
        }
	}

}
