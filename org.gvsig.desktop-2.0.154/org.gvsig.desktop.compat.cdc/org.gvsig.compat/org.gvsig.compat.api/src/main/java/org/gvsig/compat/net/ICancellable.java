/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.net;

/**
 * <p>When a task is accessing to remote data, takes an indeterminate time, and occasionally gets locked. That's
 * the reason a task should support to be cancelable.</p>
 * <p><code>ICancellable</code> interface is designed for getting information about the cancellation of a
 * task of downloading remote information.</p>
 */
public interface ICancellable {
	/**
	 * <p>Returns <code>true</code> if a download or a group of downloads tasks has been canceled.</p>
	 * 
	 * @return <code>true</code> if a download or a group of downloads tasks has been canceled, otherwise <code>false</code>
	 */
	public boolean isCanceled();
	
	/**
	 * <p>Used to cancel only a group of downloads tasks with the same identifier.</p>
	 * 
	 * @return the identifier
	 */
	public Object getID();
}
