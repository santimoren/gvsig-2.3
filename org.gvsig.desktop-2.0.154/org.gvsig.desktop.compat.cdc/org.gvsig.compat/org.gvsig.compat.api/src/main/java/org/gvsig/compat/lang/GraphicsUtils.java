/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.lang;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import org.gvsig.compat.print.PrintAttributes;

/**
 * Multi-platform graphic utils interface.
 * This allows platforms with poor java.awt.Graphics2D implementations to
 * share most of the application source code.
 *   
 * @author Juan Lucas Dominguez Rubio jldominguez at prodevelop.es
 *
 */
public interface GraphicsUtils {
	
	/**
	 * Create a buffered Image of the given size and type.
	 * In this context, buffered image means editable image (admits setRGB etc)
	 *  
	 * @param w width in pixels of the requested image
	 * @param h height in pixels of the requested image
	 * @param type image type (refers to bands, etc. see {@link Image}
	 * @return a buffered (editable) image of the desired size and type
	 */
	public BufferedImage createBufferedImage(int w, int h, int type);
	
	/**
	 * Produces a copy of a buffered image with the same graphic content (pixel color)
	 * @param img image to be copied
	 * @return new instance of a BufferedImage (editable) with same size, type and graphic content.
	 */
	public BufferedImage copyBufferedImage(BufferedImage img);
	
	/**
	 * Returns an istance of Font which is the same as the one provided except for the size.
	 * 
	 * @param srcfont provided font to be used as model
	 * @param newheight height in pixels of the new fonr 
	 * @return the new font, same properties as the provided font, but different size
	 */
	public Font deriveFont(Font srcfont, float newheight);
	
	/**
	 * Returns device screen DPI (dots per inch)
	 * 
	 * @return number of pixels per inch in the current screen
	 */
	public int getScreenDPI();

	/**
	 * Adds a translation operation to the Graphics2D object's transformation matrix.
	 *   
	 * @param g Object whose transformation matrix has to be modified
	 * @param x horizontal offset of the translation 
	 * @param y vertical offset of the translation
	 */
	public void translate(Graphics2D g, double x, double y);
	
	/**
	 * Creates an object that implements the PrintAttributes interface with default/trivial values.
	 * 
	 * @return an object that implements the PrintAttributes interface with default/trivial values.
	 */
	public PrintAttributes createPrintAttributes();
	
	/**
	 * Sets the rendering hints to the Graphics2D object in a way that is suitable for drawing.
	 *  
	 * @param g the Graphics2D object whose rendering hints have to be set for drawing
	 */
	public void setRenderingHintsForDrawing(Graphics2D g);
	
	/**
	 * Sets the rendering hints to the Graphics2D object in a way that is suitable for printing.
	 *  
	 * @param g the Graphics2D object whose rendering hints have to be set for printing
	 */
	public void setRenderingHintsForPrinting(Graphics2D g);
}
