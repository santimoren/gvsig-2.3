/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.print;

/**
 * Multi-platform interface to store print attributes.
 * Currently ionly supports the printing quality attribute
 * 
 * @author Juan Lucas Dominguez Rubio jldominguez at prodevelop.es
 *
 */
public interface PrintAttributes {
	
	/**
	 * Constant to indicate a poor printing quality (useful for drafts, etc) 
	 */
    public static final int PRINT_QUALITY_DRAFT = 0;
	/**
	 * Constant to indicate a normal printing quality 
	 */
	public static final int PRINT_QUALITY_NORMAL = 1;
	/**
	 * Constant to indicate a high printing quality 
	 */
	public static final int PRINT_QUALITY_HIGH = 2;
	
	/**
	 * DPI standard values associated with indices
	 */
    public static final int[] PRINT_QUALITY_DPI = { 72, 300, 600 };
	
	/**
	 * Gets the print quality value for this object.
	 * @return the print quality value for this object.
	 */
	public int getPrintQuality();
	
	/**
	 * Sets the print quality value for this object.
	 * @param pq the new value for the print quality value
	 */
	public void setPrintQuality(int pq);

}
