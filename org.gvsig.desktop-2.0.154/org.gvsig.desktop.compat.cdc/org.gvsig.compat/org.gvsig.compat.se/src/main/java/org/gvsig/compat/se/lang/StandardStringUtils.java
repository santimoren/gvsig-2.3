/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.se.lang;

import java.text.Collator;

import org.gvsig.compat.lang.StringUtils;

/**
 * String utilities implementation for Java Standard Edition.
 * 
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 */
public class StandardStringUtils implements StringUtils {

	public String[] split(String input, String regex) {
		return input == null ? null : input.split(regex);
	}

	public String[] split(String input, String regex, int limit) {
		return input == null ? null : input.split(regex, limit);
	}

	public String replaceAll(String input, String regex, String replacement) {
		return input == null ? null : input.replaceAll(regex, replacement);
	}

	public String replaceFirst(String input, String regex, String replacement) {
		return input == null ? null : input.replaceFirst(regex, replacement);
	}

	public int compare(String source, String target, boolean localized) {
		if (localized) {
			Collator collator = Collator.getInstance();
			collator.setStrength(Collator.PRIMARY);
			return collator.compare(source, target);
		} else {
			return source.compareTo(target);
		}
	}

}
