package org.gvsig.compat.se.net.downloader.se;

import org.gvsig.compat.net.ICancellable;
import org.gvsig.compat.se.net.downloader.Downloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class SEMonitor implements Runnable {

    ICancellable c;
    private volatile boolean finish = false;
    Downloader downloader;
    private static Logger LOG = LoggerFactory.getLogger(SEMonitor.class);

    public SEMonitor(Downloader downloader, ICancellable cancel) {
        downloader.setCanceled(cancel.getID(), false);
        this.c = cancel;
        this.downloader = downloader;
    }

    public void run() {
        while (!c.isCanceled() && !getFinish()) {
            try {
                Thread.sleep(downloader.getLatency());
            } catch (InterruptedException e) {
                LOG.error("Error", e);
            }
        }

        /*  WARNING!! This works because only one download is being processed at once.
         *  You could prefer to start several transfers simultaneously. If so, you
         *  should consideer using a non-static variable such is Utilities.canceled to
         *  control when and which transfer in particular has been canceled.
         *
         *  The feature of transfer several files is at the moment under study. We are
         *  planning to add an intelligent system that will give you a lot of services
         *  and ease-of-use. So, we encourage you to wait for it instead of write your
         *  own code.
         */
        if (c.isCanceled()) {
            downloader.setCanceled(c.getID(), true);
        }
    }

    public synchronized void setFinish(boolean value) {
        finish = value;
    }

    public synchronized boolean getFinish() {
        return finish;
    }
}
