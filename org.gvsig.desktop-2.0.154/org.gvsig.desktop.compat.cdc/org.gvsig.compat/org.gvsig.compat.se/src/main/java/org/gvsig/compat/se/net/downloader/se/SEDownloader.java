/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.se.net.downloader.se;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.net.ICancellable;
import org.gvsig.compat.se.net.downloader.Downloader;

/**
 * Clase con m�todos de utilidad en el protocolo WMS
 *
 */
public class SEDownloader implements Downloader {

//    String characters;
    private boolean canceled;
    private final long latency = 500;
    private static int count = 0;
    private static Logger LOG = LoggerFactory.getLogger(SEDownloader.class);

    /**
     * Used to cancel a group of files
     * <b>key</b>: Group id, <b>value</b>: Boolean (true if the group has to be
     * canceled. Otherwise it is false)
     */
    private Hashtable canceledGroup = new Hashtable();
    /**
     * <b>key</b>: URL, <b>value</b>: path to the downloaded file.
     */
    private Hashtable cachedURItoFile;
    private Exception downloadException;
    final String tempDirectoryPath = System.getProperty("java.io.tmpdir") + "/tmp-andami";

    public SEDownloader() {
        super();
//        characters = "";
//        for (int j = 32; j <= 127; j++) {
//            characters += (char) j;
//        }
//        characters += "�������������������������������������������ǡ�����\n\r\f\t��";
    }

    /**
     * Return the content of a file that has been created from a URL using the
     * HTTP GET protocol
     *
     * @param url The URL
     * @return File containing this URL's content or null if no file was found.
     */
    private File getPreviousDownloadedURL(URL url) {
        return getPreviousDownloaded(url);
    }

    /**
     * Return the content of a file that has been created from a URL using the
     * HTTP POST protocol
     *
     * @param url The URL
     * @param data The data to send on the query
     * @return File containing this URL's content or null if no file was found.
     */
    private File getPreviousDownloadedURL(URL url, String data) {
        if (data == null) {
            return getPreviousDownloaded(url);
        }
        return getPreviousDownloaded(url.toString() + data);
    }

    /**
     * Returns the content of a URL as a file from the file system.<br>
     * <p>
     * If the URL has been already downloaded in this session and notified to
     * the system using the static <b>Utilities.addDownloadedURL(URL)</b>
     * method, it can be restored faster from the file system avoiding to
     * download it again.
     * </p>
     *
     * @param theurl
     * @return File containing this URL's content or null if no file was found.
     */
    private File getPreviousDownloaded(Object theurl) {

        File f = null;
        Object thekey = null;
        try {
            if (theurl instanceof URL) {
                thekey = ((URL) theurl).toURI();
            } else {
                thekey = theurl;
            }
        } catch (Exception e) {
            LOG.info("Warning: did not check url: " + theurl, e);
            return null;
        }

        if (cachedURItoFile != null && cachedURItoFile.containsKey(thekey)) {
            String filePath = (String) cachedURItoFile.get(thekey);
            f = new File(filePath);
            if (!f.exists()) {
                return null;
            }
        }
        return f;
    }

    /**
     * Adds an URL to the table of downloaded files for further uses. If the URL
     * already exists in the table its filePath value is updated to the new one
     * and the old file itself is removed from the file system.
     *
     * @param url
     * @param filePath
     */
    public void addDownloadedURL(URL url, String filePath) {
        if (cachedURItoFile == null) {
            cachedURItoFile = new Hashtable();
        }

        URI theuri = null;
        try {
            theuri = url.toURI();
        } catch (Exception e) {
            LOG.info("Warning: did not cache bad url: " + url, e);
            return;
        }

        String fileName = (String) cachedURItoFile.put(theuri, filePath);
        //JMV: No se puede eliminar el anterior porque puede que alguien lo
        // este usando
		/*
         if (fileName!=null){
         File f = new File(fileName);
         if (f.exists())
         f.delete();
         }
         */
    }

    /**
     * Downloads an URL into a temporary file that is removed the next time the
     * tempFileManager class is called, which means the next time gvSIG is
     * launched.
     *
     * @param url
     * @param name
     * @return
     * @throws IOException
     * @throws ConnectException
     * @throws UnknownHostException
     */
    public synchronized File downloadFile(URL url, String name, ICancellable cancel) throws IOException, ConnectException, UnknownHostException {
        File f = null;

        if ((f = getPreviousDownloadedURL(url)) == null) {
            File tempDirectory = new File(tempDirectoryPath);
            if (!tempDirectory.exists()) {
                tempDirectory.mkdir();
            }

            f = new File(calculateFileName(name));

            if (cancel == null) {
                cancel = new ICancellable() {
                    public boolean isCanceled() {
                        return false;
                    }

                    public Object getID() {
                        return SEDownloader.class.getName();
                    }
                };
            }

            SEMonitor monitorObj = new SEMonitor(this, cancel);
            Thread downloader = new Thread(createDownloaderTask(this, url, null, f, cancel.getID().getClass(),-1));
            Thread monitor = new Thread(monitorObj);

            monitor.start();
            downloader.start();
            while (!getCanceled(cancel.getID()) && downloader.isAlive()) {
                try {
                    Thread.sleep(latency);
                } catch (InterruptedException e) {
                    LOG.error("Error", e);
                }
            }

            try {
                monitorObj.setFinish(true);
                monitor.join();
                downloader.join();
            } catch (InterruptedException e1) {
                LOG.warn(e1.getMessage());
            }
            downloader = null;
            monitor = null;

            if (getCanceled(cancel.getID())) {
                return null;
            }
            downloader = null;
            monitor = null;
            if (this.downloadException != null) {
                Exception e = this.downloadException;
                if (e instanceof FileNotFoundException) {
                    throw (IOException) e;
                } else if (e instanceof IOException) {
                    throw (IOException) e;
                } else if (e instanceof ConnectException) {
                    throw (ConnectException) e;
                } else if (e instanceof UnknownHostException) {
                    throw (UnknownHostException) e;
                }
            }
        } else {
            LOG.info(url.toString() + " cached at '" + f.getAbsolutePath() + "'");
        }

        return f;
    }

    private String calculateFileName(String name) {
        count++;
        int index = name.lastIndexOf(".");
        if (index > 0) {
            return tempDirectoryPath + "/" + name.substring(0, index) + System.currentTimeMillis() + count
                    + name.substring(index, name.length());
        }
        return tempDirectoryPath + "/" + name + System.currentTimeMillis() + count;
    }

    /**
     * Downloads a URL using the HTTP Post protocol
     *
     * @param url The server URL
     * @param data The data to send in the request
     * @param name A common name for all the retrieved files
     * @param cancel Used to cancel the downloads
     * @return The retrieved file
     * @throws IOException
     * @throws ConnectException
     * @throws UnknownHostException
     */
    public synchronized File downloadFile(URL url, String data, String name, ICancellable cancel) throws IOException, ConnectException, UnknownHostException {
        return downloadFile(url, data, name, cancel, -1);
    }
        
    public synchronized File downloadFile(URL url, String data, String name, ICancellable cancel,int maxbytes) throws IOException, ConnectException, UnknownHostException {
        File f = null;

        if ((f = getPreviousDownloadedURL(url, data)) == null) {
            File tempDirectory = new File(tempDirectoryPath);
            if (!tempDirectory.exists()) {
                tempDirectory.mkdir();
            }

            f = new File(calculateFileName(name));

            if (cancel == null) {
                cancel = new ICancellable() {
                    public boolean isCanceled() {
                        return false;
                    }

                    public Object getID() {
                        return SEDownloader.class.getName();
                    }
                };
            }
            SEMonitor monitorObj = new SEMonitor(this, cancel);
            Thread downloader = new Thread(
                    createDownloaderTask(this, url, data, f, cancel.getID(),maxbytes)
            );
            
            Thread monitor = new Thread(monitorObj);
            monitor.start();
            downloader.start();
            while (!getCanceled(cancel.getID()) && downloader.isAlive()) {
                try {
                    Thread.sleep(latency);
                } catch (InterruptedException e) {
                    LOG.error("Error", e);
                }
            }

            try {
                monitorObj.setFinish(true);
                monitor.join();
                downloader.join();
            } catch (InterruptedException e1) {
                LOG.warn(e1.getMessage());
            }
            downloader = null;
            monitor = null;

            if (getCanceled(cancel.getID())) {
                return null;
            }
            if (this.downloadException != null) {
                Exception e = this.downloadException;
                if (e instanceof FileNotFoundException) {
                    throw (IOException) e;
                } else if (e instanceof IOException) {
                    throw (IOException) e;
                } else if (e instanceof ConnectException) {
                    throw (ConnectException) e;
                } else if (e instanceof UnknownHostException) {
                    throw (UnknownHostException) e;
                }
            }
        } else {
            LOG.info(url.toString() + " cached at '" + f.getAbsolutePath() + "'");
        }

        return f;
    }

    /**
     * Try if a group of downloads has been canceled
     *
     * @param groupId Group id
     * @return If the group has been canceled
     */
    public boolean getCanceled(Object groupId) {
        Object obj = canceledGroup.get(groupId);
        if (obj != null) {
            return ((Boolean) obj).booleanValue();
        }
        return false;
    }

    /**
     * Cancel a group of downloads
     *
     * @param groupId Group id
     * @param isCanceled if the group has to be canceled
     */
    public void setCanceled(Object groupId, boolean isCanceled) {
        if (groupId == null) {
            groupId = SEDownloader.class.getName();
        }
        canceledGroup.put(groupId, new Boolean(isCanceled));
    }

    /**
     * Cleans every temporal file previously downloaded.
     */
    public void cleanUpTempFiles() {
        try {
            File tempDirectory = new File(tempDirectoryPath);

            File[] files = tempDirectory.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    // s�lo por si en un futuro se necesitan crear directorios temporales
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    files[i].delete();
                }
            }
            tempDirectory.delete();
        } catch (Exception e) {
        }

    }

    /**
     * Recursive directory delete.
     *
     * @param f
     */
    private void deleteDirectory(File f) {
        File[] files = f.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                deleteDirectory(files[i]);
            }
            files[i].delete();
        }

    }

    /**
     * Remove an URL from the system cache. The file will remain in the file
     * system for further eventual uses.
     *
     * @param url
     */
    public void removeURL(URL url) {

        URI theuri = null;

        try {
            theuri = url.toURI();
        } catch (Exception e) {
            LOG.info("Warning: did not remove bad url: " + url, e);
            return;
        }

        if (cachedURItoFile != null && cachedURItoFile.containsKey(theuri)) {
            cachedURItoFile.remove(theuri);
        }
    }

    /**
     * Remove an URL from the system cache. The file will remain in the file
     * system for further eventual uses.
     *
     * @param url
     */
    public void removeURL(Object url) {
        if (cachedURItoFile != null && cachedURItoFile.containsKey(url)) {
            cachedURItoFile.remove(url);
        }
    }

    public void setDownloadException(Exception exception) {
        this.downloadException = exception;
    }

    public long getLatency() {
        return this.latency;
    }

    protected Runnable createDownloaderTask(Downloader downloader, URL url, String data, File target, Object groupID,int maxbytes){
        SEDownloaderTask t = new SEDownloaderTask(downloader, url, data, target, groupID);
        t.setMaxbytes(maxbytes);
        return t;
                
    }
}
