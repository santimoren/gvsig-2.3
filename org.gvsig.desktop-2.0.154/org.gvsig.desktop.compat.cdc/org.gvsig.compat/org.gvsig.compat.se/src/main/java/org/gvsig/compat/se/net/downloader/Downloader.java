package org.gvsig.compat.se.net.downloader;

import java.net.URL;

public interface Downloader extends org.gvsig.compat.net.Downloader {

    public void setDownloadException(Exception exception);

    public boolean getCanceled(Object groupId);
    
    void addDownloadedURL(URL url, String filePath);
    
    long getLatency();
    
    void setCanceled(Object groupId, boolean isCanceled);

}
