package org.gvsig.compat.se.net.downloader.se;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.prefs.Preferences;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.gvsig.compat.se.net.downloader.Downloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class SEDownloaderTask implements Runnable {

    private URL url = null;
    private File dstFile = null;
    private Object groupID = null;
    private String data = null;
    private Downloader downloader = null;
    private static Logger LOG = LoggerFactory.getLogger(SEDownloaderTask.class);
    private int maxbytes = -1;

    public SEDownloaderTask(Downloader downloader, URL url, String data, File dstFile, Object groupID) {
        this.url = url;
        this.data = data;
        this.dstFile = dstFile;
        this.groupID = groupID;
        this.downloader = downloader;
        downloader.setDownloadException(null);
    }

    public void setMaxbytes(int maxbytes) {
        this.maxbytes = maxbytes;
    }

    public int getMaxbytes() {
        return maxbytes;
    }

    
    public void run() {
        LOG.info("downloading '" + url.toString() + "' to: " + dstFile.getAbsolutePath());
        if (data != null) {
            LOG.info("using POST, request = " + data);
        }
        // getting timeout from preferences in milliseconds.
        Preferences prefs = Preferences.userRoot().node("gvsig.downloader");
        // by default 1 minute (60000 milliseconds.
        int timeout = prefs.getInt("timeout", 60000);

        DataOutputStream dos;
        try {
            DataInputStream is;
            OutputStreamWriter os = null;
            HttpURLConnection connection = null;
            //If the used protocol is HTTPS
            if (url.getProtocol().equals("https")) {
                disableHttsValidation();
            }
            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (gvSIG) like Gecko");
            connection.setConnectTimeout(timeout);
            //If it uses a HTTP POST
            if (data != null) {
                connection.setRequestProperty("SOAPAction", "post");
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
                os = new OutputStreamWriter(connection.getOutputStream());
                os.write(data);
                os.flush();
            }
            is = new DataInputStream(connection.getInputStream());

            dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dstFile)));
            byte[] buffer = new byte[1024 * 4];

            long readed = 0;
            for (int i = is.read(buffer); !downloader.getCanceled(groupID) && i > 0; i = is.read(buffer)) {
                dos.write(buffer, 0, i);
                readed += i;
                if( maxbytes>0 && readed>maxbytes ) {
                    break;
                }

            }
            if (os != null) {
                os.close();
            }
            dos.close();
            is.close();
            is = null;
            dos = null;
            if (downloader.getCanceled(groupID)) {
                LOG.info("[RemoteServices] '" + url + "' CANCELED.");
                dstFile.delete();
                dstFile = null;
            } else {
                downloader.addDownloadedURL(url, dstFile.getAbsolutePath());
            }
        } catch (Exception e) {
            LOG.info("Error downloading", e);
            downloader.setDownloadException(e);
        }
    }

    /**
     * This method disables the Https certificate validation.
     *
     * @throws KeyManagementException
     * @throws NoSuchAlgorithmException
     */
    private void disableHttsValidation() throws KeyManagementException, NoSuchAlgorithmException {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }
}
