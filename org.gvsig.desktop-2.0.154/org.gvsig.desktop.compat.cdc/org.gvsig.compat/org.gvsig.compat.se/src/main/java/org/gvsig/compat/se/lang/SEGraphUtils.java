/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.compat.se.lang;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.prefs.Preferences;

import org.gvsig.compat.lang.GraphicsUtils;
import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.compat.se.print.SePrintAttributes;

/**
 * JSE (Desktop Java) implementation of {@link GraphicsUtils}
 * 
 * @author Juan Lucas Dominguez Rubio jldominguez at prodevelop.es
 * 
 * @see GraphicsUtils
 *
 */
public class SEGraphUtils implements GraphicsUtils{

	public BufferedImage copyBufferedImage(BufferedImage img) {
		
		BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		WritableRaster w = image.getRaster();
		img.copyData(w);
		return image;
	}

	public BufferedImage createBufferedImage(int w, int h, int type) {
		BufferedImage res = new BufferedImage(
				w,
				h,
				type);
		return res;
	}

	public PrintAttributes createPrintAttributes() {
		return new SePrintAttributes();
	}

	public Font deriveFont(Font srcfont, float newheight) {
		return srcfont.deriveFont(newheight);
	}

	/**
	 * Tries to use DPI value from gvSIG preferences. If this does not exist, gets
	 * value from Java Toolkit. 
	 */
	public int getScreenDPI() {
		
		Preferences prefsResolution = Preferences.userRoot().node("gvsig.configuration.screen");
		Toolkit kit = Toolkit.getDefaultToolkit();
		int dpi = prefsResolution.getInt("dpi", kit.getScreenResolution());
		return dpi;
	}

	public void translate(Graphics2D g, double x, double y) {
		g.translate(x, y);
		
	}

	public void setRenderingHintsForDrawing(Graphics2D g) {
		
		RenderingHints renderHints = new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		renderHints.put(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		renderHints.put(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setRenderingHints(renderHints);
		
	}

	public void setRenderingHintsForPrinting(Graphics2D g) {
		
		RenderingHints renderHints = new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		renderHints.put(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHints(renderHints);
		
	}

}
