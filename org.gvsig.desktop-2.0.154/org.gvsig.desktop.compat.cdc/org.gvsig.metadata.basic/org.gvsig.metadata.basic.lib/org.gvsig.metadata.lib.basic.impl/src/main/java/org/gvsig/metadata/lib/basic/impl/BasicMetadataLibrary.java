/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.lib.basic.impl;

import java.util.Locale;

import org.gvsig.metadata.MetadataLibrary;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;

public class BasicMetadataLibrary extends AbstractLibrary {

	public void doRegistration() {
		super.doRegistration();
		require(ToolsLibrary.class);
		registerAsImplementationOf(MetadataLibrary.class);
	}

	protected void doInitialize() throws LibraryException {
        MetadataLocator.registerMetadataManager(BasicMetadataManager.class);
        I18nManager i18nManager = ToolsLocator.getI18nManager();

        i18nManager.addResourceFamily("org.gvsig.metadata.basic.impl.i18n.text",
				BasicMetadataLibrary.class.getClassLoader(),
				BasicMetadataLibrary.class.getClass().getName());
	}

	protected void doPostInitialize() throws LibraryException {
		MetadataManager metadataManager = MetadataLocator.getMetadataManager();
		if (!(metadataManager instanceof MetadataManager)) {
			throw new LibraryException(BasicMetadataManager.class, null);
		}
	}
	
}
