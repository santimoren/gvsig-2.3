/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.lib.basic.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParserException;

import org.gvsig.metadata.Metadata;
import org.gvsig.metadata.MetadataContainer;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.AddMetadataDefinitionException;
import org.gvsig.metadata.exceptions.DuplicateMetadataDefinitionException;
import org.gvsig.metadata.exceptions.InvalidMetadataNamespaceException;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObjectManager;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.impl.DefaultDynClassName;
import org.gvsig.tools.persistence.impl.exception.CantFindDefinitionInStreamException;


/**
 * Abstract metadata manager used in both metadata basic and full modules.
 */
public abstract class AbstractMetadataManager implements MetadataManager {

	private final Logger LOG = LoggerFactory
			.getLogger(AbstractMetadataManager.class);

	private Map metadataItems;

	protected DynObjectManager dynManager;

	protected File metadataHomeFile = null;

	private boolean doSavingValidation;

	private boolean doExportingValidation;

	public AbstractMetadataManager() {
		dynManager = ToolsLocator.getDynObjectManager();
	}

	protected void initDefinitions() {
		this.metadataItems = new HashMap();
		Iterator definitionsIter = ToolsLocator.getDynObjectManager().iterator(
				MetadataManager.METADATA_NAMESPACE);
		DynStruct definition;
		while (definitionsIter.hasNext()) {
			definition = (DynStruct) definitionsIter.next();
			if (!this.getMetadataItems().containsKey(definition)) {
				Metadata metadata = createMetadata(definition);
				this.getMetadataItems().put(definition, metadata);
			}
		}
	}

	public DynStruct addDefinition(String name, String description)
			throws MetadataException {

		DynClass definition = dynManager.createDynClass(METADATA_NAMESPACE,
				name, description);

		addDefinition(definition);
		return definition;
	}

	public DynStruct addDefinition(String metadataDefinitionName,
			InputStream stream, ClassLoader loader) throws MetadataException {
		Map x;
		try {
			x = dynManager.importDynClassDefinitions(stream, loader,
					METADATA_NAMESPACE);
		} catch (XmlPullParserException e) {
			throw new AddMetadataDefinitionException(e, metadataDefinitionName);
		} catch (IOException e) {
			throw new AddMetadataDefinitionException(e, metadataDefinitionName);
		}
		DynClass definition = (DynClass) x.get(metadataDefinitionName);
		if (definition == null) {
			throw new CantFindDefinitionInStreamException(
					metadataDefinitionName, getKeys(x));
		}
		addDefinition(definition);
		return definition;
	}

	private String getKeys(Map theMap) {
		return new ArrayList(theMap.keySet()).toString();
	}

	public DynStruct getDefinition(Metadata metadata) throws MetadataException {
		return dynManager.get(METADATA_NAMESPACE, metadata.getMetadataName());
	}

	public DynStruct getDefinition(String metadataName) {
		if (metadataName == null)
			return null;
		if (metadataName.contains(":")) {
			int index = metadataName.lastIndexOf(":");
			return dynManager.get(metadataName.substring(0, index),
					metadataName.substring(index + 1, metadataName.length()));
		}
		return dynManager.get(METADATA_NAMESPACE, metadataName);
	}

	public void addDefinition(DynClass definition) throws MetadataException {
		if (definition == null) {
			return;
		}
		if ((definition.getNamespace() == null)
				|| (definition.getNamespace().equals(""))) {
			definition.setNamespace(METADATA_NAMESPACE);
		}

		if (!METADATA_NAMESPACE.equalsIgnoreCase(definition.getNamespace())) {
			throw new InvalidMetadataNamespaceException(definition.getName(),
					definition.getNamespace());
		}
		if (dynManager.get(definition.getNamespace(), definition.getName()) != null) {
			throw new DuplicateMetadataDefinitionException(definition.getName());
		}
		dynManager.add(definition);

		LOG.trace("Registered metadata definition {}.",
				definition.getFullName());

	}

	public void removeDefinition(DynStruct dynStruct) {
		if (dynStruct == null)
			return;
		this.metadataItems.remove(dynStruct);
		dynManager.remove(dynStruct);
	}

	public MetadataContainer createMetadataContainer(String name) {
		DynClass dynClass = dynManager.get(METADATA_NAMESPACE, name);
		if (dynClass == null) {
			throw new IllegalArgumentException(
					"Can't locate class '"
							+ new DefaultDynClassName(METADATA_NAMESPACE, name)
									.getFullName() + "'.");
		}

		return createMetadataContainer(dynClass);
		// return new BasicMetadataContainer(dynClass);
	}

	protected abstract MetadataContainer createMetadataContainer(
			DynClass dynClass);

	public void storeMetadata(Metadata metadata) throws MetadataException {
		// In SimpleMetadataManager do nothing
	}

	public void loadMetadata(Metadata metadata) throws MetadataException {
		// In SimpleMetadataManager do nothing
	}

	private Map getMetadataItems() {
		if (this.metadataItems == null) {
			initDefinitions();
		}
		return this.metadataItems;
	}

	public Metadata getMetadata(DynStruct definition)
			throws InvalidMetadataNamespaceException {
		if (this.getMetadataItems().containsKey(definition)) {
			return (Metadata) this.getMetadataItems().get(definition);
		}

		if (definition == null) {
			return null;
		}

		if ((definition.getNamespace() == null)
				|| (!definition.getNamespace().equals(METADATA_NAMESPACE))) {
			throw new InvalidMetadataNamespaceException(definition);
		}

		Metadata metadata = createMetadata(definition);

		this.getMetadataItems().put(definition, metadata);

		LOG.trace("Registered metadata definition {}.",
				definition.getFullName());

		return metadata;
	}

	protected abstract Metadata createMetadata(DynStruct definition);

	public File getMetadataHomeRepository() {
		if (this.metadataHomeFile == null) {
			String path = System.getProperty("user.home");
			path += File.separator + ".org.gvsig.metadata";
			this.metadataHomeFile = new File(path);
		}
		if (!this.metadataHomeFile.exists()) {
			this.metadataHomeFile.mkdirs();
		}
		return this.metadataHomeFile;
	}

	public File getRepositoryFile(String relativePath) {
		if ((relativePath == null) || (relativePath.equals("")))
			return this.metadataHomeFile;
		return new File(getMetadataHomeRepository().toString() + File.separator
				+ relativePath);
	}

	public void storeDefinitions() {
		Iterator it = ToolsLocator.getDynObjectManager().iterator(
				METADATA_NAMESPACE);
	}

	public Iterator getDefinitions() {
		// initDefinitions();
		return getMetadataItems().values().iterator();
	}

	public void setMetadataHomeRepository(File metadataHomeFile) {
		if (metadataHomeFile == null) {
			return;
		}

		this.metadataHomeFile = metadataHomeFile;
		if (!this.metadataHomeFile.exists())
			this.metadataHomeFile.mkdirs();
	}

	public Metadata getMetadata(String metadataID)
			throws InvalidMetadataNamespaceException {
		return this.getMetadata(getDefinition(metadataID));
	}

	public void setValidationBeforeSaving(boolean doValidation) {
		this.doSavingValidation = doValidation;
	}

	public boolean getValidationBeforeSaving() {
		return this.doSavingValidation;
	}

	public void setValidationBeforeExporting(boolean doValidation) {
		this.doExportingValidation = doValidation;
	}

	public boolean getValidationBeforeExporting() {
		return this.doExportingValidation;
	}
}
