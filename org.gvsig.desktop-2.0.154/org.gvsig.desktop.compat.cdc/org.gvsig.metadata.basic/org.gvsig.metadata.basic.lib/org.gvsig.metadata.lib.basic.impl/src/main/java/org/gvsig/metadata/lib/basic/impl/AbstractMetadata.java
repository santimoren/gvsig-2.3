/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.lib.basic.impl;

import java.util.HashSet;
import java.util.Set;

import org.gvsig.metadata.Metadata;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.impl.DefaultDynObject;

/**
 * Abstract metadata used in both metadata basic and full modules.
 */
public abstract class AbstractMetadata extends DefaultDynObject implements
		Metadata, Comparable {

	public AbstractMetadata(DynStruct dynClass) {
		super(dynClass);
	}

	public Set getMetadataChildren() throws MetadataException {
		Set children = new HashSet();
		DynStruct[] items = this.getDynClass().getSuperDynStructs();
		if (items == null)
			return children;

		Object item;
		for (int i = 0; i < items.length; i++) {
			item = items[i];
			if (item instanceof Metadata) {
				children.add(item);
			}
		}
		return children;
	}

	public Object getMetadataID() throws MetadataException {
		return this.getDynClass().getFullName();
	}

	public String getMetadataName() throws MetadataException {
		return this.getDynClass().getName();
	}


	public int compareTo(Object object) {
		if (object == null) {
			return 0;
		}
		if (object instanceof Metadata) {
			return this
					.getDynClass()
					.getFullName()
					.toLowerCase()
					.compareTo(
							((Metadata) object).getDynClass().getFullName()
									.toLowerCase());
		}
		if (object instanceof DynStruct) {
			return this
					.getDynClass()
					.getFullName()
					.toLowerCase()
					.compareTo(((DynStruct) object).getFullName().toLowerCase());
		}
		return 0;
	}

	public String toString() {
		String name = this.getDynClass().getName();
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	public String toDynObjectString() {
		return super.toString();
	}
}
