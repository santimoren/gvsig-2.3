/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata;

import java.io.File;
import java.io.InputStream;
import java.util.Iterator;

import org.gvsig.metadata.exceptions.InvalidMetadataNamespaceException;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynStruct;

/**
 * Manages the load and storage of Metadata objects.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:cordin@disid.com">C�sar Ordi�ana</a>
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * 
 * @version $Id$
 * 
 */
public interface MetadataManager {

	public final static String METADATA_NAMESPACE = "metadata";

	/**
	 * Adds a new definition in the metadata manager.
	 * 
	 * The new definition is defined in the namespace by default for DynClasses
	 * used for metadata.
	 * 
	 * @param name
	 *            the definition Name
	 * @param description
	 *            the definition Description
	 * @return the created DynStruct definition
	 * 
	 * @throws MetadataException
	 */
	public DynStruct addDefinition(String name, String description)
			throws MetadataException;

	/**
	 * Adds a new definition in the metadata manager.
	 * 
	 * The new definition is defined in the namespace by default for DynClasses
	 * used for metadata.
	 * 
	 * @param dynClass
	 *            the new definition
	 * 
	 * @return the DynStruct definition
	 * 
	 * @throws MetadataException
	 */
	public void addDefinition(DynClass definition) throws MetadataException;

	/**
	 * Adds a new definition in the metadata manager.
	 * 
	 * The new definition is defined in the namespace by default for DynClasses
	 * used for metadata.
	 * 
	 * @param name
	 *            the definition Name
	 * @param stream
	 *            the input stream to be parsed.
	 * @param loader
	 *            the Class loader that can parse this input stream
	 * @return the created DynStruct definition
	 * 
	 * @throws MetadataException
	 */
	public DynStruct addDefinition(String name, InputStream stream,
			ClassLoader loader) throws MetadataException;

	/**
	 * <p>
	 * If a Metadata definition ( {@link DynStruct} ) with the given definition
	 * name has been previously registered in this manager, then this method
	 * returns that definition. Otherwise, it returns null.
	 * </p>
	 * 
	 * @param definitionName
	 *            the name whose corresponding attribute definition is to be
	 *            retrieved.
	 * 
	 * @return The attribute definition corresponding to the provided metadata
	 *         class, or null otherwise.
	 */
	public DynStruct getDefinition(String definitionName);

	/**
	 * <p>
	 * Retrieves all the current definitions in the form of an Iterator
	 * interface.
	 * </p>
	 * 
	 * @return The Metadata iterator containing the current metadata definitions
	 */
	public Iterator getDefinitions();

	/**
	 * <p>
	 * If a Metadata definition ( {@link DynStruct} ) with a given metadata that
	 * has been previously registered in this manager, then this method returns
	 * that definition. Otherwise, it returns null.
	 * </p>
	 * 
	 * @param metadata
	 *            the given metadata.
	 * 
	 * @return The attribute definition corresponding to the provided metadata
	 *         class, or null otherwise.
	 */
	public DynStruct getDefinition(Metadata metadata) throws MetadataException;

	/**
	 * 
	 * If the current dynStruct has been previously registered, it is removed
	 * from both the dynObjectManager and the metadataManager lists. Otherwise,
	 * does nothing.
	 * 
	 * @param dynStruct
	 *            the dynStruct to be removed.
	 */
	public void removeDefinition(DynStruct dynStruct);

	/**
	 * Creates a default {@link Metadata} instance based on a given
	 * {@link DynStruct}, if it has not been previously registered, and adds
	 * this Metadata to the system.
	 * 
	 * @param dynStruct
	 *            the given dynStruct object.
	 * @return the resultant Metadata object.
	 * @throws InvalidMetadataNamespaceException
	 */
	public Metadata getMetadata(DynStruct dynStruct)
			throws InvalidMetadataNamespaceException;

	/**
	 * This function populates the given Metadata object with the current values
	 * that the system contains. If there is any invalid access to an undeclared
	 * field, for example, a MetadataException will be thrown.
	 * 
	 * @param metadata
	 *            the metadata that needs to be populated.
	 * 
	 * @throws MetadataException
	 *             Exception caused by any access violation.
	 */
	public void loadMetadata(Metadata metadata) throws MetadataException;

	/**
	 * This function stores the given Metadata object to an output resource, be
	 * it a database or a file containing its information.
	 * 
	 * @param metadata
	 *            the metadata that needs to be stored.
	 * 
	 * @throws MetadataException
	 *             Exception caused by any access violation.
	 */
	public void storeMetadata(Metadata metadata) throws MetadataException;

	/**
	 * This is a utility interface to implement classes that implement the
	 * interface Metadata.
	 * 
	 * Use the createMetadataContainer in the MetadataManager to create a
	 * container for delegate the implementation of Metadata methods in it.
	 * 
	 * @param name
	 *            the DynStruct name.
	 * 
	 * @return the metadata container.
	 */
	public MetadataContainer createMetadataContainer(String name);

	/**
	 * Returns the main Metadata extension root repository.
	 * 
	 * @return the File object containing the home repository folder
	 * 
	 */
	public File getMetadataHomeRepository();

	/**
	 * Sets the main Metadata root repository for this manager.
	 * 
	 * @param metadataHomeRepository
	 *            the main Repository folder.
	 */
	public void setMetadataHomeRepository(File metadataHomeRepository);

	/**
	 * Retrieves a metadata object based on its metadata ID. This metadata
	 * object is basically a dynObject if metadata namespace set to "metadata".
	 * 
	 * @param metadataID
	 *            the metadata ID of the metadata object.
	 * @return
	 * @throws InvalidMetadataNamespaceException
	 *             if the metadataID was not previously registered as Metadata.
	 */
	public Metadata getMetadata(String metadataID)
			throws InvalidMetadataNamespaceException;

	/**
	 * Added options properties setter for validating before the saving
	 * function. If it is set to false, the validation will not be performed.
	 * @param doValidation
	 *		if the validation must be performed or not before saving metadata objects.
	 */
	public void setValidationBeforeSaving(boolean doValidation);

	/**
	 * Added options properties getter for validating before the saving
	 * function. If it is set to false, the validation will not be performed.
	 * @return
	 *            if the validation must be performed or not before saving metadata objects.
	 */
	public boolean getValidationBeforeSaving();

	/**
	 * Added options properties setter for validating before the exporting
	 * function. If it is set to false, the validation will not be performed.
	 * @param doValidation
	 *		if the validation must be performed or not before saving metadata objects.
	 */
	public void setValidationBeforeExporting(boolean doValidation);

	/**
	 * Added options properties getter for validating before the exporting
	 * function. If it is set to false, the validation will not be performed.
	 * @return
	 *            if the validation must be performed or not before saving metadata objects.
	 */
	public boolean getValidationBeforeExporting();
}
