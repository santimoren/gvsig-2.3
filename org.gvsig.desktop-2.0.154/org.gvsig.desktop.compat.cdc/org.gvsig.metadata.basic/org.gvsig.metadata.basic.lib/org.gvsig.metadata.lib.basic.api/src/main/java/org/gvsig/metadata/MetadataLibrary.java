/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata;

import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;

/**
 * Library that initalizes the Metadata module.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public class MetadataLibrary extends AbstractLibrary {

	public void doRegistration() {
		super.doRegistration();
		registerAsAPI(MetadataLibrary.class);
		require(ToolsLibrary.class);
	}

	protected void doInitialize() throws LibraryException {
	}

	protected void doPostInitialize() throws LibraryException {
		// Validate if there is any implementation registered.
		MetadataManager manager = MetadataLocator.getMetadataManager();

		if (manager == null) {
			throw new ReferenceNotRegisteredException(
					MetadataLocator.METADATA_MANAGER_NAME,
					MetadataLocator.getInstance());
		}
	}

}
