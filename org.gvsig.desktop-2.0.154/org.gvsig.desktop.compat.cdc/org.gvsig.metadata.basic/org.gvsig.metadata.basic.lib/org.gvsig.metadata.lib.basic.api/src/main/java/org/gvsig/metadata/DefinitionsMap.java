/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynField;
import org.gvsig.tools.dynobject.DynStruct;

/**
 * 
 * This is the intermediary of the ImportExport module, which enables to have a
 * disposable iterator of a list of DynStructs. It also permits to obtain
 * another disposable iterator of the related DynFields based on a given
 * DynStruct. It extends from a Map<String, DynStruct>.
 * 
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public interface DefinitionsMap extends Map {


	/**
	 * Removes a definition from the list
	 * 
	 * @param definition
	 *            the definition to be removed.
	 */
	public void remove(DynStruct definition);

	/**
	 * Adds a definition to the list
	 * 
	 * @param definition
	 *            the definition to be added.
	 */
	public void add(DynStruct newDefinition);

	public void addAll(Map map);

	/**
	 * Determines if this definition is already included.
	 * 
	 * @param definition
	 *            the current definition to be checked.
	 * @return true if the definition is contained, false otherwise.
	 */
	public boolean hasDefinition(DynStruct definition);

	/**
	 * Saves the current Metadata elements of the list.
	 * 
	 */
	public void save();

	/**
	 * Retrieves the size of the list of elements.
	 */
	public int size();

	/**
	 * Sets the current user definitions linked to the current hierarchy
	 * definitions.
	 * 
	 * @param userDefs
	 *            the user Definitions
	 * @param hierarchyDefs
	 *            the hierarchy Definitions
	 */
	public void setHierarchyDefinitions(Map userDefs, Map hierarchyDefs);

	/**
	 * Returns a list containing definitions ( {@link DynStruct} ), which are
	 * disposable.
	 * 
	 * @return the disposable DynStructs iterator.
	 */
	public Iterator iterator();

	/**
	 * Returns a disposable list iteration containing {@link DynField}s based on
	 * a given definition ( {@link DynStruct} ).
	 * 
	 * @param definition
	 *            the DynStruct definition from which to obtain the associated
	 *            DynFields.
	 * @return the disposable DynFields iterator.
	 */
	public DisposableIterator dynFieldsIterator(DynStruct definition);

	/**
	 * Returns a list containing children definitions ({@link DynStruct}s) based
	 * on a given definition.
	 * 
	 * @param definition
	 *            the DynStruct definition from which to obtain the associated
	 *            children.
	 * @return the disposable DynFields iterator.
	 */
	public List getChildren(DynStruct definition);

	/**
	 * Returns a disposable list iteration containing the current elements of
	 * the list.
	 * 
	 * @return the disposable DynFields iterator.
	 */
	public DisposableIterator disposableIterator();

	/**
	 * Returns a list iteration containing the current parent element
	 * definitions of a given childDefinition.
	 * 
	 * @param childDefinition
	 * @return the parent definitions' iterator.
	 */
	public Iterator parentsIterator(DynStruct childDefinition);

	/**
	 * Returns a list iteration containing the current children element
	 * definitions of a given parentDefinition.
	 * 
	 * @return the parent definitions' iterator.
	 */
	public Iterator childrenIterator(DynStruct parentDefinition);

}
