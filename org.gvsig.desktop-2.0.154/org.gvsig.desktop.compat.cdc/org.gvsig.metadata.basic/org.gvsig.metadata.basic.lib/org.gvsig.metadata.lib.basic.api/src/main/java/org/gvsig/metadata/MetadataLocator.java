/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.LocatorException;

/**
 * 
 * Locator that provides the Metadata manager instance.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public class MetadataLocator extends BaseLocator {

	public static final String METADATA_MANAGER_NAME = "org.gvsig.metadata.manager";

	private static final String METADATA_MANAGER_DESCRIPTION = "Implementation for MetadataManager";

	/**
	 * Unique instance.
	 */
	private static final MetadataLocator instance = new MetadataLocator();

	/**
	 * Return the singleton instance.
	 * 
	 * @return the singleton instance
	 */
	public static MetadataLocator getInstance() {
		return instance;
	}

	/**
	 * Return a reference to MetadataManager.
	 * 
	 * @return a reference to MetadataManager
	 * @throws org.gvsig.tools.locator.LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see org.gvsig.tools.locator.Locator#get(String)
	 */
	public static MetadataManager getMetadataManager() throws LocatorException {
		return (MetadataManager) getInstance().get(METADATA_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the MetadataManager interface.
	 * 
	 * @param clazz
	 *            implementing the MetadataManager interface
	 */
	public static void registerMetadataManager(Class clazz) {
		getInstance().register(METADATA_MANAGER_NAME,
				METADATA_MANAGER_DESCRIPTION, clazz);
	}

}
