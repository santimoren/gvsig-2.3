/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.impl;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import org.gvsig.metadata.Metadata;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.metadata.swing.basic.api.AbstractSimpleMetadataPanel;
import org.gvsig.metadata.swing.basic.api.JMetadataPanel;
import org.gvsig.metadata.swing.basic.api.MetadataSwingManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynform.DynFormLocator;
import org.gvsig.tools.dynform.DynFormManager;
import org.gvsig.tools.dynform.JDynForm;
import org.gvsig.tools.dynobject.exception.DynObjectValidateException;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.service.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicJMetadataPanel extends JMetadataPanel {

    private static final Logger logger = LoggerFactory.getLogger(BasicJMetadataPanel.class);

    private static final long serialVersionUID = 6514865354393558940L;
    private JLabel lblName;
    private MetadataManager manager;
    private JDynForm dynform;

    private I18nManager i18nManager;


    public BasicJMetadataPanel(MetadataSwingManager uiManager,
            Metadata metadata, boolean editable) {
        super(uiManager, metadata, editable);
    }

    protected void initManagers() {
        manager = MetadataLocator.getMetadataManager();
        i18nManager = ToolsLocator.getI18nManager();
    }

    protected void initData() {
        String metadataName = "";
        if (!this.hasMetadata()) {
            return;
        }
        try {
            metadataName = getMetadata().getMetadataName();
            this.lblName.setText(i18nManager.getTranslation("_{0}_metadata", new String[] {i18nManager.getTranslation(metadataName)}));
        } catch (MetadataException e) {
            logger.warn("Can't init panel data", e);
        }
    }

    protected void initUI() {
        this.setLayout(new BorderLayout());
        this.lblName = new JLabel();

        this.add(this.lblName, BorderLayout.NORTH);
    }

    public void saveMetadata() throws DynObjectValidateException {
        try {
            this.dynform.getValues(this.getMetadata());
            this.getMetadata().getDynClass().validate(this.getMetadata());
            manager.storeMetadata(this.getMetadata());
        } catch (MetadataException e) {
            logger.warn("Can't save metadata", e);
        }
    }

    protected AbstractSimpleMetadataPanel createMetadataComponent(Metadata metadata, boolean refresh) {
        if (metadata == null) {
            metadata = getMetadata();
        }
        try {
            DynFormManager dynformmgr = DynFormLocator.getDynFormManager();
            dynform = dynformmgr.createJDynForm(metadata);
            this.add(dynform.asJComponent());
            this.updateUI();
        } catch (ServiceException e) {
            logger.warn("Can't create metadata form component", e);
        }
        return this;
    }
}
