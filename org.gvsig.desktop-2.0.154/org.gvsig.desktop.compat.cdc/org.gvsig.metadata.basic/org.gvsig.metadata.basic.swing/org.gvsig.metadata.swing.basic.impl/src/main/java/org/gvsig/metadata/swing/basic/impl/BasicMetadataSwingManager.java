/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.impl;

import org.gvsig.metadata.Metadata;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.metadata.swing.basic.api.JMetadataPanel;
import org.gvsig.metadata.swing.basic.api.MetadataSwingManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.usability.UsabilitySwingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * 04/04/2010
 *
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 */
public class BasicMetadataSwingManager implements MetadataSwingManager {

    private static final Logger logger = LoggerFactory.getLogger(BasicMetadataSwingManager.class);

    private I18nManager i18nManager;

    public BasicMetadataSwingManager() {
        i18nManager = ToolsLocator.getI18nManager();
    }

    public String translate(String message) {
        if (message != null) {
            return i18nManager.getTranslation(message);
        }
        return "";
    }

    public MetadataManager getManager() {
        return MetadataLocator.getMetadataManager();
    }

    public JMetadataPanel createJMetadataPanel(Metadata metadata, boolean editable) {
        try {
            this.getManager().loadMetadata(metadata);

        } catch (MetadataException e) {
            logger.warn("Can't load metadata", e);
        }
        BasicJMetadataPanel panel = new BasicJMetadataPanel(this, metadata,editable);
        panel.setMetadata(metadata);
        return panel;
    }

    public JMetadataPanel createJMetadataPanel(Metadata metadata) {
        return this.createJMetadataPanel(metadata, true);
    }

    public UsabilitySwingManager getUIBuilder() {
        return ToolsSwingLocator.getUsabilitySwingManager();
    }
}
