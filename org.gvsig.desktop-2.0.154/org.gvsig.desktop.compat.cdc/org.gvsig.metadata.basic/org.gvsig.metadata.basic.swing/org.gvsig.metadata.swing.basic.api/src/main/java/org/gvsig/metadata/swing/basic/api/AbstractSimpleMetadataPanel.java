/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.api;

import javax.swing.JPanel;

import org.gvsig.metadata.Metadata;

public abstract class AbstractSimpleMetadataPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 8275085021104037254L;
    private AbstractSimpleMetadataPanel metadataComponent;
    private Metadata metadata;
    private boolean editable;

    protected MetadataSwingManager uiManager;

    public AbstractSimpleMetadataPanel(MetadataSwingManager uiManager,
            Metadata metadata, boolean editable) {
        super();
        this.uiManager = uiManager;
        this.editable = editable;
        this.metadata = metadata;
        init();
    }

    private void init() {
        initManagers();
        initUI();
        setMetadata(metadata);
    }

    protected boolean isEditable() {
        return this.editable;
    }

    protected void setEditable(boolean isEditable) {
        this.editable = isEditable;
    }

    protected abstract void initManagers();

    protected abstract void initData();

    protected abstract void initUI();

    protected abstract AbstractSimpleMetadataPanel createMetadataComponent(Metadata metadata, boolean doRefresh);

    public void setMetadataComponent(AbstractSimpleMetadataPanel metadataComponent) {
        this.metadataComponent = metadataComponent;
    }

    public AbstractSimpleMetadataPanel getMetadataComponent() {
        return metadataComponent;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
        initData();
        this.setMetadataComponent(createMetadataComponent(getMetadata(), true));
        this.updateUI();
    }

    public Metadata getMetadata() {
        return metadata;
    }

    protected boolean hasMetadata() {
        return (this.metadata != null);
    }

}
