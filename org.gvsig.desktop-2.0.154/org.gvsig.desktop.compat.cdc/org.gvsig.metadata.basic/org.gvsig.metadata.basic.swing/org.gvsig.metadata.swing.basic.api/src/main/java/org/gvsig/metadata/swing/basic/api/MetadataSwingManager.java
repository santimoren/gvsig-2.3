/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.api;

import org.gvsig.metadata.Metadata;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.swing.api.usability.UsabilitySwingManager;

/**
 * 
 * A basic metadata swing manager to provide a metadata GUI editor.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public interface MetadataSwingManager {

	/**
	 * 
	 * Given a text, it looks for its translation. If none is found, then the
	 * original text is shown.
	 * 
	 * @param text
	 *            to be translated.
	 * @return the translation text result.
	 */
	public String translate(String message);

	/**
	 * Gets the main manager that allows interacting with metadata objects.
	 * 
	 * @return the metadata Manager
	 */
	public MetadataManager getManager();

	/**
	 * Creates the JPanel in which users can edit and visualize metadata
	 * objects. Editable by default.
	 * 
	 * @param metadata
	 *            the metadata to be displayed.
	 * @return the metadata editor Panel
	 * @throws MetadataException
	 */
	public JMetadataPanel createJMetadataPanel(Metadata metadata);

	/**
	 * Creates the JPanel in which users can edit and visualize metadata
	 * objects.
	 * 
	 * @param metadata
	 *            the metadata to be displayed.
	 * @param editable
	 *            if the metadata panel should be editable or not.
	 * @return the metadata editor Panel
	 * @throws MetadataException
	 */
	public JMetadataPanel createJMetadataPanel(Metadata metadata,
			boolean editable);

	/**
	 * Gets the ToolsLocator usability swing manager to create UI components
	 * based on gvSIG standards.
	 * 
	 * @return
	 */
	public UsabilitySwingManager getUIBuilder();

}
