/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.api;

import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;
import org.gvsig.tools.swing.api.ToolsSwingLibrary;

/**
 * 
 * Initialization of the basic Metadata API Library.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public class MetadataSwingLibrary extends AbstractLibrary {

	public void doRegistration() {
		super.doRegistration();
		registerAsAPI(MetadataSwingLibrary.class);
        require(ToolsSwingLibrary.class);
    }
    
	/**
	 * Performs all the initializations of the library, only related to himself:
	 * register implementation classes through the Locator, start services, etc.
	 * 
	 * @throws LibraryException
	 *             if there is an error while performing the initialization of
	 *             the library
	 */
	protected void doInitialize() throws LibraryException {

	}

	/**
	 * Performs all the initializations or validations related to the library
	 * dependencies, as getting references to objects through other libraries
	 * Locators.
	 * 
	 * @throws LibraryException
	 *             if there is an error while loading an implementation of the
	 *             library
	 */
	protected void doPostInitialize() throws LibraryException {
	    // Validate there is any implementation registered.
        MetadataManager manager = MetadataLocator.getMetadataManager();
        
        if (manager == null) {
            throw new ReferenceNotRegisteredException(
                    MetadataLocator.METADATA_MANAGER_NAME, MetadataLocator.getInstance());
        }
		/*
		 * Validates if there is at least an implementation of the
		 * MetadataManager registered.
		 */
		MetadataSwingManager swingManager = MetadataSwingLocator.getMetadataSwingManager();

		if (swingManager == null) {
			throw new ReferenceNotRegisteredException(
					MetadataSwingLocator.METADATA_MANAGER_NAME,
					MetadataSwingLocator.getInstance());
		}
	}
}
