/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.metadata.swing.basic.api;

import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;

/**
 * Locator for the Metadata Library main object instances.
 * 
 * Replaces the MetadataManager by its own services, by adding as many constants
 * and methods as needed, taking the MetadataManager as a guide.
 * 
 * @author gvSIG Team
 * @author <a href="mailto:reinhold@uji.es">cmartin</a>
 * @version $Id$
 * 
 */
public class MetadataSwingLocator extends BaseLocator {

	/**
	 * Unique instance.
	 */
	private static final MetadataSwingLocator instance = new MetadataSwingLocator();

	private static final String METADATA_MANAGER_DESCRIPTION = "Metadata API Library Manager";

	public static final String METADATA_MANAGER_NAME = "org.gvsig.metadata.swing.basic.api.manager";

	/**
	 * Returns the Metadata singleton instance.
	 * 
	 * @return the Metadata singleton instance
	 */
	public static MetadataSwingLocator getInstance() {
		return instance;
	}

	/**
	 * Returns a reference to the MetadataManager.
	 * 
	 * @return a reference to MetadataManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static MetadataSwingManager getMetadataSwingManager()
			throws LocatorException {
		return (MetadataSwingManager) getInstance().get(METADATA_MANAGER_NAME);
	}

	public static void registerDefaultMetadataSwingManager(Class clazz) {
		getInstance().registerDefault(METADATA_MANAGER_NAME,
				METADATA_MANAGER_DESCRIPTION, clazz);
	}

	/**
	 * Registers the Class implementing the MetadataManager interface.
	 * 
	 * @param clazz
	 *            implementing the MetadataManager interface
	 */
	public static void registerMetadataSwingManager(Class clazz) {
		getInstance().register(METADATA_MANAGER_NAME,
				METADATA_MANAGER_DESCRIPTION, clazz);
	}
}
