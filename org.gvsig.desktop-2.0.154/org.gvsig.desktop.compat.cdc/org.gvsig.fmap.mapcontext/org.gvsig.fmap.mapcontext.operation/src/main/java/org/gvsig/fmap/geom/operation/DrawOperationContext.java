/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import java.awt.Graphics2D;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.task.Cancellable;


public class DrawOperationContext extends GeometryOperationContext{
	private ISymbol symbol=null;
	private Graphics2D graphics=null;
	private Cancellable cancellable;
	private double dpi;
	private double scale;
	private boolean hasDPI=false;
	private ViewPort viewPort;
	private Feature feature;
	
	public ViewPort getViewPort() {
		return viewPort;
	}
	public void setViewPort(ViewPort viewPort) {
		this.viewPort = viewPort;
	}
	public Graphics2D getGraphics() {
		return graphics;
	}
	public void setGraphics(Graphics2D graphics) {
		this.graphics = graphics;
	}
	public ISymbol getSymbol() {
		return symbol;
	}
	public void setSymbol(ISymbol symbol) {
		this.symbol = symbol;
	}
	public void setCancellable(Cancellable cancel) {
		this.cancellable=cancel;

	}
	public Cancellable getCancellable() {
		return cancellable;
	}
	public void setDPI(double dpi) {
		this.hasDPI=true;
		this.dpi=dpi;
	}
	public double getDPI(){
		return dpi;
	}
	public boolean hasDPI() {
		return hasDPI;
	}
	/**
	 * @param scale the scale to set
	 */
	public void setScale(double scale) {
		this.scale = scale;
	}
	/**
	 * @return the scale
	 */
	public double getScale() {
		return scale;
	}
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
