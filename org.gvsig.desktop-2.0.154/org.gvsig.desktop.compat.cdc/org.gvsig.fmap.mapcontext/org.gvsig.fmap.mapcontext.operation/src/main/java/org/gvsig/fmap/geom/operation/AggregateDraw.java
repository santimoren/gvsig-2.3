/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.aggregate.Aggregate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vicente Caballero Navarro
 *
 */
public class AggregateDraw extends GeometryOperation {
	
	public static final String NAME = "draw";
	public static int CODE = Integer.MIN_VALUE;

	final static private Logger logger = LoggerFactory
	.getLogger(AggregateDraw.class);

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.GeometryOperation#getOperationIndex()
	 */
	public int getOperationIndex() {
		return CODE;
	}

	/* (non-Javadoc)
	 * @see org.gvsig.fmap.geom.operation.GeometryOperation#invoke(org.gvsig.fmap.geom.Geometry, org.gvsig.fmap.geom.operation.GeometryOperationContext)
	 */
	public Object invoke(Geometry geom, GeometryOperationContext ctx)
	throws GeometryOperationException {

		Aggregate aggregate = (Aggregate) geom;
		for (int i=0;i<aggregate.getPrimitivesNumber();i++){
			Geometry prim = aggregate.getPrimitiveAt(i);
			try {
				prim.invokeOperation(CODE, ctx);
			} catch (GeometryOperationNotSupportedException e) {
				throw new GeometryOperationException(e);
			}
		}
		return null;
	}

	public static void register() {
		GeometryManager geoMan = GeometryLocator.getGeometryManager();
		
		CODE = geoMan.getGeometryOperationCode(NAME);

		geoMan.registerGeometryOperation(NAME, new AggregateDraw(),
				TYPES.AGGREGATE);
		geoMan.registerGeometryOperation(NAME, new AggregateDraw(),
				TYPES.MULTIPOINT);
		geoMan.registerGeometryOperation(NAME, new AggregateDraw(),
				TYPES.MULTICURVE);
		geoMan.registerGeometryOperation(NAME, new AggregateDraw(),
				TYPES.MULTISURFACE);
		geoMan.registerGeometryOperation(NAME, new AggregateDraw(),
				TYPES.MULTISOLID);

	}

}
