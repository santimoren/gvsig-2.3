/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.geom.operation;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.geom.primitive.GeneralPathX;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.task.Cancellable;

/**
 * @author gvSIG Team
 *
 */
public class DrawInts extends GeometryOperation {
	private static final GeometryManager geomManager = GeometryLocator.getGeometryManager();
	public static final String NAME = "drawInts";
	public static int CODE = Integer.MIN_VALUE;

	final static private Logger logger = LoggerFactory.getLogger(DrawInts.class);

	public Object invoke(Geometry geom, GeometryOperationContext ctx) throws GeometryOperationException{
		DrawOperationContext doc=(DrawOperationContext)ctx;
		ViewPort viewPort = doc.getViewPort();
		ISymbol symbol = doc.getSymbol();
		Graphics2D g=doc.getGraphics();
		Cancellable cancel=doc.getCancellable();
		//		 make the symbol to resize itself with the current rendering context
		try {
			if (doc.hasDPI()){
				double previousSize = ((CartographicSupport)symbol).
				toCartographicSize(viewPort, doc.getDPI(), geom);
				// draw it as normally
				Geometry decimatedShape = transformToInts(geom, viewPort.getAffineTransform());

				//the AffineTransform has to be null because the transformToInts method
				//reprojects the geometry
				symbol.draw(g, null, decimatedShape, doc.getFeature(), cancel);

				// restore previous size
				((CartographicSupport)symbol).setCartographicSize(previousSize, geom);
			}else{
				Geometry decimatedShape = transformToInts(geom, viewPort.getAffineTransform());
				symbol.draw(g, viewPort.getAffineTransform(), decimatedShape,
						doc.getFeature(), cancel);
			}
		} catch (CreateGeometryException e) {
			e.printStackTrace();
			throw new GeometryOperationException(e);
		}

		return null;
	}

	public int getOperationIndex() {
		return CODE;
	}

	private Geometry transformToInts(Geometry gp, AffineTransform at) throws CreateGeometryException {
		GeneralPathX newGp = new GeneralPathX();
		double[] theData = new double[6];
		double[] aux = new double[6];

		// newGp.reset();
		PathIterator theIterator;
		int theType;
		int numParts = 0;

		java.awt.geom.Point2D ptDst = new java.awt.geom.Point2D.Double();
		java.awt.geom.Point2D ptSrc = new java.awt.geom.Point2D.Double();
		boolean bFirst = true;
		int xInt, yInt, antX = -1, antY = -1;


		theIterator = gp.getPathIterator(null); //, flatness);
		int numSegmentsAdded = 0;
		while (!theIterator.isDone()) {
			theType = theIterator.currentSegment(theData);

			switch (theType) {
			case PathIterator.SEG_MOVETO:
				numParts++;
				ptSrc.setLocation(theData[0], theData[1]);
				at.transform(ptSrc, ptDst);
				antX = (int) ptDst.getX();
				antY = (int) ptDst.getY();
				newGp.moveTo(antX, antY);
				numSegmentsAdded++;
				bFirst = true;
				break;

			case PathIterator.SEG_LINETO:
				ptSrc.setLocation(theData[0], theData[1]);
				at.transform(ptSrc, ptDst);
				xInt = (int) ptDst.getX();
				yInt = (int) ptDst.getY();
				if ((bFirst) || ((xInt != antX) || (yInt != antY)))
				{
					newGp.lineTo(xInt, yInt);
					antX = xInt;
					antY = yInt;
					bFirst = false;
					numSegmentsAdded++;
				}
				break;

			case PathIterator.SEG_QUADTO:
				at.transform(theData,0,aux,0,2);
				newGp.quadTo(aux[0], aux[1], aux[2], aux[3]);
				numSegmentsAdded++;
				break;

			case PathIterator.SEG_CUBICTO:
				at.transform(theData,0,aux,0,3);
				newGp.curveTo(aux[0], aux[1], aux[2], aux[3], aux[4], aux[5]);
				numSegmentsAdded++;
				break;

			case PathIterator.SEG_CLOSE:
				if (numSegmentsAdded < 3) {
					newGp.lineTo(antX, antY);
				}
				newGp.closePath();

				break;
			} //end switch

			theIterator.next();
		} //end while loop

		Geometry geom = null;
		GeometryType geometryType = gp.getGeometryType();

		if(geometryType.isTypeOf(Geometry.TYPES.POINT)){
		    geom = geomManager.createPoint(ptDst.getX(), ptDst.getY(), SUBTYPES.GEOM2D);
		} else if(geometryType.isTypeOf(Geometry.TYPES.MULTIPOINT)){
            geom = geomManager.createMultiPoint(newGp, SUBTYPES.GEOM2D);
		} else if(geometryType.isTypeOf(Geometry.TYPES.CURVE)){
		    geom = geomManager.createCurve(newGp, SUBTYPES.GEOM2D);
        } else if(geometryType.isTypeOf(Geometry.TYPES.MULTICURVE)){
            geom = geomManager.createMultiCurve(newGp, SUBTYPES.GEOM2D);
		} else if(geometryType.isTypeOf(Geometry.TYPES.SURFACE)){
		    geom = geomManager.createSurface(newGp, SUBTYPES.GEOM2D);
        } else if(geometryType.isTypeOf(Geometry.TYPES.MULTISURFACE)){
            geom = geomManager.createMultiSurface(newGp, SUBTYPES.GEOM2D);
		}
		return geom;
	}

	/**
	 *
	 */
	public static void register() {

		GeometryManager gm = GeometryLocator.getGeometryManager();
		CODE = gm.getGeometryOperationCode(NAME);
		geomManager.registerGeometryOperation(NAME, new DrawInts());
	}


}
