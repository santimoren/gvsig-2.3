/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial.impl;

import java.util.Iterator;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ValidateDataParametersException;
import org.gvsig.fmap.dal.feature.EditableFeature;
import org.gvsig.fmap.dal.feature.EditableFeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.EditableFeatureType;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.exceptions.LegendLayerException;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialUniqueValueLegend;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default {@link GraphicLayer} implementation.
 *
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class DefaultGraphicLayer extends FLyrVect implements GraphicLayer {

    private static final String DEFAULT = "default";
    protected static final Logger logger = LoggerFactory.getLogger(DefaultGraphicLayer.class);
    private static DataManager dataManager = DALLocator.getDataManager();
    private IVectorialUniqueValueLegend legend = null;

    private FeatureStore store = null;
    private long ids = 0;

    private int symbolId = -1;

    private int featureIdIndex;
    private int groupIdIndex;
    private int geomIndex;
    private int idsymIndex;
    private int labelIndex;
    private int tagIndex;
    private int priorityIndex;

    public void initialize(IProjection projection) throws ValidateDataParametersException, DataException, LoadLayerException {
        GeometryManager geomManager = GeometryLocator.getGeometryManager();
        store = dataManager.createMemoryStore(FEATURE_ATTR_PRIORITY);
        store.edit();

        EditableFeatureType editableFeatureType = store.getDefaultFeatureType().getEditable();

        editableFeatureType.add(GraphicLayer.FEATURE_ATTR_GROUPID, DataTypes.STRING);

        EditableFeatureAttributeDescriptor geometryDescriptor = editableFeatureType.add(GraphicLayer.FEATURE_ATTR_GEOMETRY, DataTypes.GEOMETRY);
        try {
            geometryDescriptor.setGeometryType(geomManager.getGeometryType(TYPES.GEOMETRY, SUBTYPES.GEOM2D));
        } catch (Exception e) {
            logger.info("Can't create/assign geomery type to feature type of GraphicLayer.", e);
        }
        geometryDescriptor.setSRS(projection);
        editableFeatureType.setDefaultGeometryAttributeName(GraphicLayer.FEATURE_ATTR_GEOMETRY);

        editableFeatureType.add(GraphicLayer.FEATURE_ATTR_IDSYMBOL, DataTypes.INT);

        editableFeatureType.add(GraphicLayer.FEATURE_ATTR_LABEL, DataTypes.STRING);

        EditableFeatureAttributeDescriptor featureIdDescriptor
                = editableFeatureType.add(GraphicLayer.FEATURE_ATTR_FEATUREID, DataTypes.LONG);

        editableFeatureType.add(GraphicLayer.FEATURE_ATTR_TAG, DataTypes.OBJECT);
        editableFeatureType.add(GraphicLayer.FEATURE_ATTR_PRIORITY, DataTypes.INT);

        featureIdDescriptor.setIsPrimaryKey(true);
        editableFeatureType.setHasOID(true);

        store.update(editableFeatureType);

        store.finishEditing();

        FeatureType ftype = store.getDefaultFeatureType();
        featureIdIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_FEATUREID);
        groupIdIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_GROUPID);
        geomIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_GEOMETRY);
        idsymIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_IDSYMBOL);
        labelIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_LABEL);
        tagIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_TAG);
        priorityIndex = ftype.getIndex(GraphicLayer.FEATURE_ATTR_PRIORITY);

        this.bindToDataStore(store);

        setName("Graphic Layer");
    }

    public void addGraphic(String groupId, Geometry geom, int idsym) {
        addGraphic(groupId, geom, idsym, null, null, DEFAULT_PRIORITY);
    }

    public void addGraphic(Geometry geom, int idsym) {
        addGraphic(DEFAULT, geom, idsym, null, null, DEFAULT_PRIORITY);
    }

    public void addGraphic(Geometry geom, int idsym, String label) {
        addGraphic(DEFAULT, geom, idsym, label, null, DEFAULT_PRIORITY);
    }

    public void addGraphic(String groupId, Geometry geom, int idsym, String label) {
        addGraphic(groupId, geom, idsym, label, null, DEFAULT_PRIORITY);
    }

    public void addGraphic(String groupId, Geometry geom, int idsym, String label,
            Object tag, int priority) {

        try {
            store.beginComplexNotification();
            // Just in case another thread is going to read from the store
            synchronized (store) {
                if (store.isEditing() || store.isAppending() ) {
                    insertGeometry(groupId, geom, idsym, label, tag, priority);
                } else {
                    store.edit(FeatureStore.MODE_APPEND);
                    try {
                        insertGeometry(groupId, geom, idsym, label, tag, priority);
                        store.finishEditing();
                    } catch(Throwable th) {
                        try {
                            store.cancelEditing();
                        } catch(Throwable th2) {
                            // Do nothing
                        }
                        throw th;
                    }
                }
            }
        } catch (Throwable th) {
            logger.warn("Error adding a geometry to the graphic layer", th);
        } finally {
            try {
                store.endComplexNotification();
            } catch(Exception ex) {
                // Do nothing
            }
        }
    }

    private void insertGeometry(String groupId, Geometry geom, int idsym,
            String label, Object tag, int priority) throws DataException {
        EditableFeature feature = store.createNewFeature().getEditable();
        feature.setString(groupIdIndex, groupId);
        feature.setGeometry(geomIndex, geom);
        feature.setInt(idsymIndex, idsym);
        feature.setString(labelIndex, label);
        feature.setLong(featureIdIndex, ids);
        feature.set(tagIndex, tag);
        feature.setInt(priorityIndex, priority);

        ids++;

        store.insert(feature);
    }

    public void addGraphics(String groupId, Iterator geoms, int idsym) {
        addGraphics(groupId, geoms, idsym, null, null, DEFAULT_PRIORITY);
    }

    public void addGraphics(String groupId, Iterator geoms, int idsym,
            String label) {
        addGraphics(groupId, geoms, idsym, label, null, DEFAULT_PRIORITY);
    }

    public void addGraphics(String groupId, Iterator geoms, int idsym,
            String label, Object tag, int priority) {
        try {
            // Just in case another thread is going to read from the store
            synchronized (store) {
                if (store.isEditing() || store.isAppending() ) {
                    store.disableNotifications();
                    for (; geoms.hasNext();) {
                        Geometry geom = (Geometry) geoms.next();
                        insertGeometry(groupId, geom, idsym, label, tag, priority);
                    }
                    store.enableNotifications();
                } else {
                    store.edit(FeatureStore.MODE_APPEND);
                    try {
                        store.disableNotifications();
                        for (; geoms.hasNext();) {
                            Geometry geom = (Geometry) geoms.next();
                            insertGeometry(groupId, geom, idsym, label, tag, priority);
                        }
                        store.enableNotifications();
                        store.finishEditing();
                    } catch(Throwable th) {
                        try {
                            store.cancelEditing();
                        } catch(Throwable th2) {
                            // Do nothing
                        }
                        throw th;
                        
                    }
                }
            }
        } catch (Throwable th) {
            logger.warn("Error adding a geometry to the graphic layer", th);
        }
    }

    public int addSymbol(ISymbol newSymbol) {
        symbolId++;
        legend.addSymbol(new Integer(symbolId), newSymbol);
        return symbolId;
    }

    public ISymbol getSymbol(int symbolPos) {
        return legend.getSymbolByValue(new Integer(symbolPos));
    }

    public int getSymbolId(ISymbol symbol) {
        Object key = legend.getSymbolKey(symbol);
        return key == null ? -1 : ((Number) key).intValue();
    }

    public void clearAllGraphics() {
        DisposableIterator iterator = null;
        FeatureSet featureSet = null;
        try {
            if (!store.isEditing()) {
                store.edit();
                featureSet = store.getFeatureSet();
                for (iterator = featureSet.fastIterator(); iterator.hasNext();) {
                    Feature feature = (Feature) iterator.next();
                    featureSet.delete(feature);
                }
                store.finishEditing();
            } else {
                featureSet = store.getFeatureSet();
                for (iterator = featureSet.fastIterator(); iterator.hasNext();) {
                    Feature feature = (Feature) iterator.next();
                    featureSet.delete(feature);
                }
            }
        } catch (DataException e) {
            logger.warn("Error clearing all the geometry of the graphic layer", e);
        } finally {
            if (featureSet != null) {
                featureSet.dispose();
            }
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    public int clearAllSymbols() {
        legend.clear();
        symbolId = -1;
        return symbolId;
    }

    public void removeGraphics(String groupId) {
        DisposableIterator iterator = null;
        FeatureSet featureSet = null;
        try {
            store.beginComplexNotification();
            if (!store.isEditing()) {
                store.edit();
                featureSet = store.getFeatureSet();
                store.beginEditingGroup(groupId);
                for (iterator = featureSet.fastIterator(); iterator.hasNext();) {
                    Feature feature = (Feature) iterator.next();
                    if (feature.get(FEATURE_ATTR_GROUPID).equals(groupId)) {
                        featureSet.delete(feature);
                    }
                }
                store.endEditingGroup();
                store.finishEditing();
            } else {
                featureSet = store.getFeatureSet();
                store.beginEditingGroup(groupId);
                for (iterator = featureSet.fastIterator(); iterator.hasNext();) {
                    Feature feature = (Feature) iterator.next();
                    if (feature.get(FEATURE_ATTR_GROUPID).equals(groupId)) {
                        featureSet.delete(feature);
                    }
                }
                store.endEditingGroup();
            }
            store.endComplexNotification();
        } catch (DataException e) {
            logger.warn("Error clearing all the geometry of the graphic layer", e);
        } finally {
            if (featureSet != null) {
                featureSet.dispose();
            }
            if (iterator != null) {
                iterator.dispose();
            }
        }
    }

    protected void doDispose() throws BaseException {
        super.doDispose();
        if (store != null) {
            store.dispose();
            store = null;
        }
    }

    public void setLegend(IVectorLegend legend) throws LegendLayerException {
        if (legend instanceof IVectorialUniqueValueLegend) {
            super.setLegend(legend);
            this.legend = (IVectorialUniqueValueLegend) legend;
            this.legend.setClassifyingFieldNames(new String[]{FEATURE_ATTR_IDSYMBOL});
        } else {
            if (this.legend != null) { // Skip the first assignment, is always bad.
                logger.warn("The allocation of legend type '" + legend.getClass().getName() + "' is ignored. Required a legend of type IVectorialUniqueValueLegend.");
            }
        }
    }

    public Envelope getFullEnvelope() throws ReadException {
        Envelope rAux;
        try {
            rAux = getFeatureStore().getEnvelope();
        } catch (BaseException e) {
            throw new ReadException(getName(), e);
        }

        if (rAux == null) {
            return null;
        }

        ICoordTrans ct = getCoordTrans();
        if (ct != null) {
            rAux = rAux.convert(ct);
        }
        return rAux;
    }
}
