/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.mapcontext.rendering.legend.styling;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.persistence.Persistent;


/**
 * Interface that defines provides support to a ILabelingStrategy to supply
 * the set of <b>LabelClass</b>(es) that handle the label's style, content, etc..
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface ILabelingMethod extends Persistent {

	/**
	 * Returns all the labels defined by this Labeling Method
	 * @return all the labels in this labeling method;
	 */
	public ILabelClass[] getLabelClasses();

	/**
	 * Returns the LabelClass contained by this ILabelingMethod whose name matches
	 * with the string passed in <b>labelName</b>.
	 * @param labelName
	 * @return
	 */
	public ILabelClass getLabelClassByName(String labelName);

	/**
	 * Adds a LabelClass to the set of LabelClass contained by this ILabelingMethod.
	 * @param lbl
	 */
	public void addLabelClass(ILabelClass lbl);

	/**
	 * Removes the LabelClass from the set of LabelClasses contained by this
	 * ILabelingMethod.
	 * @param lbl, the LabelClass to be removed
	 */
	public void deleteLabelClass(ILabelClass lbl);

	/**
	 * Returns <b>true</b> if this ILabelingMethod allos the definition of more
	 * LabelClasses than just the default LabelClass.
	 * @return boolean
	 */
	public boolean allowsMultipleClass();

	/**
	 * Changes the mane of a given LabelClass
	 * @param lbl, the LabelClass
	 * @param newName, the new name
	 */
	public void renameLabelClass(ILabelClass lbl, String newName);

	/**
	 * 
	 * 
	 * @param layer
	 * @param lc
	 * @param viewPort
	 * @param usedFields
	 * @return
	 * @throws DataException
	 */
	public FeatureSet getFeatureIteratorByLabelClass(
			FLyrVect layer,
			ILabelClass lc,
			ViewPort viewPort,
			String[] usedFields) throws DataException;

	public boolean definesPriorities();

	public void setDefinesPriorities(boolean flag);

	public void clearAllClasses();

	public ILabelingMethod cloneMethod();
}
