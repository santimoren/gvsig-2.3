/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;


import java.awt.Color;

import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;


/**
 * Interface for the legend with unique values.
 *
 */
public interface IVectorialUniqueValueLegend extends IClassifiedVectorLegend {
	
	public static final String LEGEND_NAME = "VectorialUniqueValue";
	
    /**
     * Establishes the symbol for the value which is the argument of the function.
     *
     * @param id index.
     * @param symbol symbol.
     */
    void setValueSymbolByID(int id, ISymbol symbol);


    /**
     * Returns the symbols starting from an ID. It looks in m_ArrayKeys for the
     * element ID and with this key obtains the FSymbol.

     * @param key ID.
     *
     * @return symbol associated with the ID
     */

    // public FSymbol getSymbolByID(int ID);

    /**
     * Returns the symbols starting from its key.
     *
     * @param key ID.
     *
     * @return symbol associated with the key.
     * @throws MapContextException 
     */
    public ISymbol getSymbolByValue(Object key);

	/**
	 * Returns the key related to a registered {@link ISymbol} or null if it is
	 * not registered.
	 * 
	 * @param symbol
	 *            to look for
	 * @return the related key
	 */
	Object getSymbolKey(ISymbol symbol);

	ZSort getZSort();

	void setZSort(ZSort zSort);

	Color[] getColorScheme();

	void setColorScheme(Color[] colors);

}
