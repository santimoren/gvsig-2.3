/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Map;

import org.cresques.cts.ICoordTrans;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.task.Cancellable;



/**
 * Interface of a vectorial legend.
 *
 * @author Vicente Caballero Navarro
 */
public interface IVectorLegend extends ILegend, DrawingObservable {
	/**
	 * Inserts the DataSource.
	 *
	 * @param ds DataSource.
	 * @throws DataException TODO
	 * @throws FieldNotFoundException when the field is not found.
	 */
//	void setFeatureStore(FeatureStore fs)
//		throws DataException;

	/**
	 * Returns the symbol to be used to represent the feature in the i-th
	 * record in the DataSource
	 * @param i, the record index
	 *
	 * @return ISymbol.
	 */
//	ISymbol getSymbol(int i) throws ReadException;
	/**
     * Returns a symbol starting from an IFeature.
	 *
	 * TAKE CARE!! When we are using a feature iterator as a database
	 * the only field that will be filled is the fieldID.
	 * The rest of fields will be null to reduce the time of creation
	 *
	 * @param feat IFeature.
	 *
	 * @return S�mbolo.
	 * @throws MapContextException 
	 */
    ISymbol getSymbolByFeature(Feature feat) throws MapContextException;

	/**
	 * Returns the type of the shape.
	 *
	 * @return Returns the type of the shapes that the legend is ready to use.
	 *
	 */
	int getShapeType();

	/**
	 * Defines the type of the shape.
	 *
	 * @param shapeType type of the shape.
	 */
	void setShapeType(int shapeType);

	/**
	 * Establishes the default symbol of a legend. In a SingleSymbolLegend the symbol
	 * is established by calling this method.
	 *
	 * @param s default symbol.
	 */
	void setDefaultSymbol(ISymbol s);

	/**
     * Returns true or false depending on if the rest of values are used.
	 *
	 * @return  True if the rest of values are used.
	 */
    public boolean isUseDefaultSymbol();

    void useDefaultSymbol(boolean b);

//   jjdc los eliminamos, y debe quedar dependiente de la implementacion y n de api
//   public ZSort getZSort();
//
//	public void setZSort(ZSort zSort);
//
	public boolean isSuitableForShapeType(int shapeType);

	/**
	 * Draws the {@link FeatureStore} data using this legend simbology.
	 * 
	 * @param image
	 *            the base image to draw over
	 * @param graphics2D
	 *            the {@link Graphics2D} to draw to
	 * @param viewPort
	 *            the ViewPort to use
	 * @param cancel
	 *            the {@link Cancellable} delegate object
	 * @param scale
	 *            the scale of the view
	 * @param queryParameters
	 *            the query parameters
	 * @param coordTrans
	 *            the transformation coordinates to use for reprojection
	 * @param featureStore
	 * 			  {@link FeatureStore} to load the data to draw from
	 * @throws LegendException
	 */
	void draw(BufferedImage image, Graphics2D graphics2D, ViewPort viewPort,
			Cancellable cancel, double scale, Map queryParameters,
			ICoordTrans coordTrans, FeatureStore featureStore)
			throws LegendException;
	
	 /**
     * Draws the {@link FeatureStore} data using this legend simbology.
     * 
     * @param image
     *            the base image to draw over
     * @param graphics2D
     *            the {@link Graphics2D} to draw to
     * @param viewPort
     *            the ViewPort to use
     * @param cancel
     *            the {@link Cancellable} delegate object
     * @param scale
     *            the scale of the view
     * @param queryParameters
     *            the query parameters
     * @param coordTrans
     *            the transformation coordinates to use for reprojection
     * @param featureStore
     *            {@link FeatureStore} to load the data to draw from
     * @param featureQuery 
     *            the query used to filter the features.
     * @throws LegendException
     */
    void draw(BufferedImage image, Graphics2D graphics2D, ViewPort viewPort,
            Cancellable cancel, double scale, Map queryParameters,
            ICoordTrans coordTrans, FeatureStore featureStore, FeatureQuery featureQuery)
            throws LegendException;

	void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
			double scale, Map queryParameters, ICoordTrans coordTrans,
			FeatureStore featureStore, PrintAttributes properties) throws LegendException;
	
	void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, FeatureQuery featureQuery, PrintAttributes properties) throws LegendException;

}
