/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols.impl;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolPreferences;

/**
 * Default {@link SymbolPreferences} implementation based on object attributes.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class DefaultSymbolPreferences implements SymbolPreferences {

	private String symbolFileExtension = ".gvssym";

	private Color defaultSymbolColor;

	private Color defaultSymbolFillColor;

	private boolean defaultSymbolFillColorAleatory;

	private Font defaultSymbolFont;

	private String symbolLibraryPath;

	/**
	 * The unit that will be used when creating new symbols with Cartographic
	 * support.
	 */
	private int defaultCartographicSupportMeasureUnit = -1;

	/**
	 * The reference system that will be used when creating new symbols with
	 * Cartographic support.
	 */
	private int defaultCartographicSupportReferenceSystem =
			CartographicSupport.WORLD;

	public DefaultSymbolPreferences() {
		resetDefaultSymbolColor();
		resetDefaultSymbolFillColor();
		resetDefaultSymbolFillColorAleatory();
		resetSymbolLibraryPath();
		resetDefaultSymbolFont();
	}

	public String getSymbolFileExtension() {
		return symbolFileExtension;
	}

	public void setSymbolFileExtension(String symbolFileExtension) {
		this.symbolFileExtension = symbolFileExtension;
	}
	public String getSymbolLibraryPath() {
		return symbolLibraryPath;
	}

	public void setSymbolLibraryPath(String symbolLibraryPath) {
		this.symbolLibraryPath = symbolLibraryPath;
	}

	public void resetSymbolLibraryPath() {
	    
		symbolLibraryPath =
				System.getProperty("user.home") + File.separator + "gvSIG"
				+ File.separator + "plugins" + File.separator +
				"org.gvsig.app.mainplugin" + File.separator + "Symbols";
	}

	public Color getDefaultSymbolColor() {
		return defaultSymbolColor;
	}

	public void setDefaultSymbolColor(Color defaultSymbolColor) {
		this.defaultSymbolColor = defaultSymbolColor;
	}

	public void resetDefaultSymbolColor() {
		defaultSymbolColor = Color.DARK_GRAY;
	}

	public Color getDefaultSymbolFillColor() {
		return defaultSymbolFillColor;
	}

	public void setDefaultSymbolFillColor(Color defaultSymbolFillColor) {
		this.defaultSymbolFillColor = defaultSymbolFillColor;
	}

	public void resetDefaultSymbolFillColor() {
		defaultSymbolFillColor = new Color(60, 235, 235);
	}

	public boolean isDefaultSymbolFillColorAleatory() {
		return defaultSymbolFillColorAleatory;
	}

	public void setDefaultSymbolFillColorAleatory(
			boolean defaultSymbolFillColorAleatory) {
		this.defaultSymbolFillColorAleatory = defaultSymbolFillColorAleatory;
	}

	public void resetDefaultSymbolFillColorAleatory() {
		defaultSymbolFillColorAleatory = false;
	}

	public Font getDefaultSymbolFont() {
		return defaultSymbolFont;
	}

	public void setDefaultSymbolFont(Font defaultSymbolFont) {
		this.defaultSymbolFont = defaultSymbolFont;
	}

	public void resetDefaultSymbolFont() {
		defaultSymbolFont = new Font("SansSerif", Font.PLAIN, 14);
	}

	public int getDefaultCartographicSupportMeasureUnit() {
		return defaultCartographicSupportMeasureUnit;
	}

	public void setDefaultCartographicSupportMeasureUnit(
			int defaultCartographicSupportMeasureUnit) {
		this.defaultCartographicSupportMeasureUnit =
				defaultCartographicSupportMeasureUnit;
	}

	public int getDefaultCartographicSupportReferenceSystem() {
		return defaultCartographicSupportReferenceSystem;
	}

	public void setDefaultCartographicSupportReferenceSystem(
			int defaultCartographicSupportReferenceSystem) {
		this.defaultCartographicSupportReferenceSystem =
				defaultCartographicSupportReferenceSystem;
	}
}
