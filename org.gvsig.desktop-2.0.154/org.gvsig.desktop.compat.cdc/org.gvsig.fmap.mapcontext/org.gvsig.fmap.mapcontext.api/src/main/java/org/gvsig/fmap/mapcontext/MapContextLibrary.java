/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.CompatLibrary;
import org.gvsig.fmap.dal.DALLibrary;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.impl.DefaultMapContextDrawer;
import org.gvsig.fmap.mapcontext.layers.ExtendedPropertiesHelper;
import org.gvsig.fmap.mapcontext.layers.FLayerStatus;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.FLyrDefault;
import org.gvsig.fmap.mapcontext.layers.LayerFactory;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.layers.vectorial.ReprojectDefaultGeometry;
import org.gvsig.fmap.mapcontext.tools.persistence.ColorPersistenceFactory;
import org.gvsig.fmap.mapcontext.tools.persistence.DimensionPersistenceFactory;
import org.gvsig.fmap.mapcontext.tools.persistence.FontPersistenceFactory;
import org.gvsig.fmap.mapcontext.tools.persistence.Point2DPersistenceFactory;
import org.gvsig.fmap.mapcontext.tools.persistence.Rectangle2DPersistenceFactory;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;
import org.gvsig.tools.locator.ReferenceNotRegisteredException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.util.Caller;
import org.gvsig.tools.util.impl.DefaultCaller;

/**
 * Library for the MapContext library API.
 * @author jmvivo
 */
public class MapContextLibrary extends AbstractLibrary {
	final static private Logger LOG = LoggerFactory.getLogger(FLyrDefault.class);

    public void doRegistration() {
        registerAsAPI(MapContextLibrary.class);
        require(DALLibrary.class);
        require(CompatLibrary.class);
    }

	protected void doInitialize() throws LibraryException {
		LayerFactory.getInstance().registerLayerToUseForStore(
				FeatureStore.class, FLyrVect.class);
	}

	protected void doPostInitialize() throws LibraryException {
            Caller caller = new DefaultCaller();

            MapContextManager manager = MapContextLocator.getMapContextManager();

            if (manager == null) {
                    throw new ReferenceNotRegisteredException(
                                    MapContextLocator.MAPCONTEXT_MANAGER_NAME,
                                    MapContextLocator.getInstance());
            }
            
            caller.add( new FLyrDefault.RegisterMetadata() );
            caller.add( new DefaultMapContextDrawer.RegisterMapContextDrawer() );
            
            caller.add( new ViewPort.RegisterPersistence() );
            caller.add( new ExtendedPropertiesHelper.RegisterPersistence() );
            caller.add( new MapContext.RegisterPersistence() );
            caller.add( new ExtentHistory.RegisterPersistence() );
            caller.add( new ReprojectDefaultGeometry.RegisterPersistence() );
            caller.add( new FLayerStatus.RegisterPersistence() );
            caller.add( new FLyrDefault.RegisterPersistence() );
            caller.add( new FLyrVect.RegisterPersistence() );
            caller.add( new FLayers.RegisterPersistence() );

            caller.add( new ColorPersistenceFactory.RegisterPersistence() );
            caller.add( new Point2DPersistenceFactory.RegisterPersistence() );
            caller.add( new FontPersistenceFactory.RegisterPersistence() );
            caller.add( new Rectangle2DPersistenceFactory.RegisterPersistence() );
            caller.add( new DimensionPersistenceFactory.RegisterPersistence() );

            if( !caller.call() ) {
                    throw new LibraryException(MapContextLibrary.class, caller.getExceptions());
            }

	}

}
