/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import java.awt.geom.Point2D;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.layers.AbstractLinkProperties;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;


/**
 * This class extends AbstractLinkProperties and implements the method to get an array of URI
 * using a point2D and a tolerance. This class extends AstractLinkProperties to add HyperLink
 * to Vectorial Layer(FLyrVect)
 * 
 * @deprecated this functionality has extracted of the core form gvSIG 2.0 
 */

public class FLyrVectLinkProperties extends AbstractLinkProperties {

    /**
     * Default Constructor. Costructs a LinkProperties with the necessary information
     *
     */
    public FLyrVectLinkProperties(){
        setType(0);
        setField(null);
        setExt(null);
    }

    /**
     * Constructor. Constructs a LinkProperties with the information that receives
     * @param tipo
     * @param fieldName
     * @param extension
     */
    public FLyrVectLinkProperties(int tipo, String fieldName, String extension){
        setType(tipo);
        setField(fieldName);
        setExt(extension);
    }

    /**
     * Creates an array of URI. With the point and the tolerance makes a query to the layer
     * and gets the geometries that contains the point with a certain error (tolerance).
     * For each one of this geometries creates one URI and adds it to the array
     * @param layer
     * @param point
     * @param tolerance
     * @return Array of URI
     * @throws ReadException
     */
    public URI[] getLink(FLayer layer, Point2D point, double tolerance) throws ReadException{
    	//Sacado de la clase LinkListener
		FLyrVect lyrVect = (FLyrVect) layer;
		FeatureStore fStore = null;
		FeatureSet fSet=null;
		DisposableIterator iter = null;

    	try {
    		fStore = lyrVect.getFeatureStore();
    		//URI uri[]=null;

    		//Construimos el BitSet (V�ctor con componentes BOOLEAN) con la consulta que
    		try {
    			fSet = lyrVect.queryByPoint(point, tolerance, fStore.getDefaultFeatureType());//fStore.getFeatureSet(featureQuery);
    		} catch (ReadException e) {
    			return null;
    		} catch (DataException e) {
    			return null;
			}

    		//Si el bitset creado no est� vac�o creamos el vector de URLS correspondientes
    		//a la consulta que hemos hecho.

    		ArrayList tmpUris=new ArrayList();
    		String auxext="";
    		int idField = fStore.getDefaultFeatureType().getIndex(this.getField());




    		//Consigo el identificador del campo pasandole como par�metro el
    		//nombre del campo del �nlace
    		if (idField == -1){
        		throw new ReadException(lyrVect.getName()+": '"+this.getField()+"' not found",new Exception());
    		}
    		//Recorremos el BitSet siguiendo el ejmplo de la clase que se
    		//proporciona en la API
    		iter = fSet.fastIterator();
    		while (iter.hasNext()){
    			//TODO
    			//Sacado de la clase LinkPanel, decidir como pintar la URL le
    			//corresponde a LinkPanel, aqu� solo creamos el vector de URL�s
    			if (!super.getExt().equals("")){
    				auxext="."+this.getExt();
    			}
    			//Creamos el fichero con el nombre del campo y la extensi�n.
    			Feature feature = (Feature) iter.next();
    			String auxField=feature.getString(idField);
    			URI tmpUri = null;
    			if(auxField.startsWith("http:/")){
    				tmpUri= new URI(auxField);
    			}else{
    				File file =new File(feature.getString(idField)+auxext);
    				tmpUri = file.toURI();
    			}
    			tmpUris.add(tmpUri);
    			System.out.println(tmpUri.toString());


    		}

    		if (tmpUris.size()==0){
    			return null;
    		}

    		// Creo el vector de URL�s con la misma longitud que el bitset

    		return (URI[]) tmpUris.toArray(new URI[0]);

    	} catch (ReadException e) {
    		throw new ReadException(lyrVect.getName(),e);
    	} catch (URISyntaxException e) {
    		throw new ReadException(lyrVect.getName(),e);
		} catch (DataException e) {
			throw new ReadException(lyrVect.getName(),e);
		}finally{
			if (iter != null) {
				iter.dispose();
			}
			if (fSet != null) {
				fSet.dispose();
			}
		}

    }

	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		DynStruct definition = manager.addDefinition(
				FLyrVectLinkProperties.class,
				"FLyrVectLinkProperties",
				"FLyrVectLinkProperties Persistence definition",
				null, 
				null
		);
		definition.addDynFieldInt("typeLink").setMandatory(true);
		definition.addDynFieldString("fieldName").setMandatory(true);
		definition.addDynFieldString("extName").setMandatory(true);
	}


}
