/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.geom.Rectangle2D;
import org.gvsig.tools.ToolsLocator;

import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Factory to manage the persistence of {@link Rectangle2D} objects.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 * @author 2010- <a href="jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a> -
 *         gvSIG team
 */
public class Rectangle2DPersistenceFactory extends
		AbstractSinglePersistenceFactory {

	private static final String DYNCLASS_NAME = "AwtRectangle2D";
	private static final String DYNCLASS_DESCRIPTION = "Awt Rectangle2D";

	public static final String FIELD_X = "x";
	public static final String FIELD_Y = "y";
	public static final String FIELD_WIDTH = "width";
	public static final String FIELD_HEIGHT = "height";

	/**
	 * Creates a new {@link Rectangle2DPersistenceFactory} with the
	 * {@link DynStruct} definition for the {@link Rectangle2D} objects.
	 */
	public Rectangle2DPersistenceFactory() {
		super(Rectangle2D.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null,
				null);

		DynStruct definition = this.getDefinition();

		definition.addDynFieldDouble(FIELD_X).setMandatory(true);
		definition.addDynFieldDouble(FIELD_Y).setMandatory(true);
		definition.addDynFieldDouble(FIELD_WIDTH).setMandatory(true);
		definition.addDynFieldDouble(FIELD_HEIGHT).setMandatory(true);
	}

	public Object createFromState(PersistentState state)
			throws PersistenceException {

		double x = state.getDouble(FIELD_X);
		double y = state.getDouble(FIELD_Y);
		double width = state.getDouble(FIELD_WIDTH);
		double height = state.getDouble(FIELD_HEIGHT);

		return new Rectangle2D.Double(x, y, width, height);
	}

	public void saveToState(PersistentState state, Object obj)
			throws PersistenceException {
		Rectangle2D rec = (Rectangle2D) obj;

		state.set(FIELD_X, rec.getX());
		state.set(FIELD_Y, rec.getY());
		state.set(FIELD_WIDTH, rec.getWidth());
		state.set(FIELD_HEIGHT, rec.getHeight());
	}
        
        public static class RegisterPersistence implements Callable {

            public Object call() throws Exception {
                PersistenceManager persistenceManager
                        = ToolsLocator.getPersistenceManager();
                persistenceManager.registerFactory(new Rectangle2DPersistenceFactory());
                return Boolean.TRUE;
            }
        }          
}