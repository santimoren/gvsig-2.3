/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend.events;

import org.gvsig.fmap.mapcontext.events.FMapEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;

/**
 * <p>The class <code>LegendClearEvent</code> stores all necessary information of an event 
 * produced when a legend is cleared.</p>
 * 
 * @see FMapEvent
 * @author Vicente Caballero Navarro
 */
public class LegendClearEvent extends ClassificationLegendEvent {
	private ISymbol[] oldSymbols;
	/**
	 * Constructor method
	 * @param oldSymbols
	 */
	public LegendClearEvent(ISymbol[] oldSymbols) {
		this.oldSymbols = oldSymbols;
	}
	/**
	 * Obtains the old symbols of the legend
	 * @return
	 */
	public ISymbol[] getOldSymbols() {
		return oldSymbols;
	}
	/**
	 * Returns the type of the event
	 */
	public int getEventType() {
		return LegendEvent.LEGEND_CLEARED;
	}
}