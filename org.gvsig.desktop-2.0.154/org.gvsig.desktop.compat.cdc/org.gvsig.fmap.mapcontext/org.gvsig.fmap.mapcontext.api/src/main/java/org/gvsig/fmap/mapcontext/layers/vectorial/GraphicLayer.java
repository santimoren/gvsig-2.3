/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import java.util.Iterator;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;

/**
 * Utility layer to be able to draw graphics in a {@link MapContext}. Those
 * graphics won't be stored and will be rendered on top of all the other
 * MapContext's layers.
 * 
 * @author gvSIG team
 * @version $Id$
 */
public interface GraphicLayer extends VectorLayer {

	public static final int DEFAULT_PRIORITY = 100;

	public static final String FEATURE_ATTR_FEATUREID = "featureId";
	public static final String FEATURE_ATTR_GROUPID = "groupId";
	public static final String FEATURE_ATTR_GEOMETRY = "geom";
	public static final String FEATURE_ATTR_IDSYMBOL = "idsym";
	public static final String FEATURE_ATTR_LABEL = "label";
	public static final String FEATURE_ATTR_TAG = "tag";
	public static final String FEATURE_ATTR_PRIORITY = "priority";

	/**
	 * Adds a new {@link ISymbol} to the layer to be used to render the
	 * {@link Geometry} objects. The symbol will be added to the legend of the
	 * layer.
	 * 
	 * @param newSymbol
	 *            the {@link ISymbol} to add
	 * @return an identifier of the symbol to be used when adding geometries
	 */
	public int addSymbol(ISymbol newSymbol);

	/**
	 * Returns the {@link ISymbol} of the layer with the given identifier.
	 * 
	 * @param symbolId
	 *            the symbol identifier
	 * @return the symbol with the given id or null if there is not any symbol
	 *         registered with that id
	 */
	public ISymbol getSymbol(int symbolId);

	/**
	 * Returns the identifier of the symbol into the layer.
	 * 
	 * @param symbol
	 *            the symbol to look for
	 * @return the symbol identifier or -1 if it is not registered into the
	 *         layer
	 */
	public int getSymbolId(ISymbol symbol);

	/**
	 * @deprecated @see
	 *             {@link #addGraphic(String, Geometry, int, String, Object, int)

	 */
	public void addGraphic(Geometry geom, int idsym);

	/**
	 * @deprecated @see
	 *             {@link #addGraphic(String, Geometry, int, String, Object, int)

	 */
	public void addGraphic(Geometry geom, int idsym, String label);

    /**
     * Adds a new {@link Geometry} to be rendered into the layer.
     * 
     * @param groupId
     *            group identifier. Allows to identify a group of
     *            {@link Geometry} added to the layer
     * @param geom
     *            the {@link Geometry} to add
     * @param idsym
     *            the {@link ISymbol} identifier to apply to render the
     *            {@link Geometry}
     */
    public void addGraphic(String groupId, Geometry geom, int idsym);

	/**
	 * Adds a new {@link Geometry} to be rendered into the layer.
	 * 
	 * @param groupId
	 *            group identifier. Allows to identify a group of
	 *            {@link Geometry} added to the layer
	 * @param geom
	 *            the {@link Geometry} to add
	 * @param idsym
	 *            the {@link ISymbol} identifier to apply to render the
	 *            {@link Geometry}
	 * @param label
	 *            the text to show if the {@link ISymbol} is a
	 *            {@link ITextSymbol}. Set null otherwise.
	 */
	public void addGraphic(String groupId, Geometry geom, int idsym,
			String label);

	/**
	 * Adds a new {@link Geometry} to be rendered into the layer.
	 * 
	 * @param groupId
	 *            group identifier. Allows to identify a group of
	 *            {@link Geometry} added to the layer
	 * @param geom
	 *            the {@link Geometry} to add
	 * @param idsym
	 *            the {@link ISymbol} identifier to apply to render the
	 *            {@link Geometry}
	 * @param label
	 *            the text to show if the {@link ISymbol} is a
	 *            {@link ITextSymbol}. Set null otherwise.
	 * @param tag
	 *            an object to classify, identify or add related information to
	 *            the {@link Geometry}
	 * @param priority
	 *            to apply while rendering the {@link Geometry}. Values with
	 *            lower priority will be rendered first.
	 */
	public void addGraphic(String groupId, Geometry geom, int idsym,
			String label, Object tag, int priority);

    /**
     * Adds a new {@link Geometry} to be rendered into the layer.
     * 
     * @param groupId
     *            group identifier. Allows to identify a group of
     *            {@link Geometry} added to the layer
     * @param geoms
     *            the {@link Geometry}s to add
     * @param idsym
     *            the {@link ISymbol} identifier to apply to render the
     *            {@link Geometry}
     */
    public void addGraphics(String groupId, Iterator geoms, int idsym);

    /**
     * Adds a new {@link Geometry} to be rendered into the layer.
     * 
     * @param groupId
     *            group identifier. Allows to identify a group of
     *            {@link Geometry} added to the layer
     * @param geoms
     *            the {@link Geometry}s to add
     * @param idsym
     *            the {@link ISymbol} identifier to apply to render the
     *            {@link Geometry}
     * @param label
     *            the text to show if the {@link ISymbol} is a
     *            {@link ITextSymbol}. Set null otherwise.
     */
    public void addGraphics(String groupId, Iterator geoms, int idsym,
        String label);

    /**
     * Adds a new {@link Geometry} to be rendered into the layer.
     * 
     * @param groupId
     *            group identifier. Allows to identify a group of
     *            {@link Geometry} added to the layer
     * @param geoms
     *            the {@link Geometry}s to add
     * @param idsym
     *            the {@link ISymbol} identifier to apply to render the
     *            {@link Geometry}
     * @param label
     *            the text to show if the {@link ISymbol} is a
     *            {@link ITextSymbol}. Set null otherwise.
     * @param tag
     *            an object to classify, identify or add related information to
     *            the {@link Geometry}
     * @param priority
     *            to apply while rendering the {@link Geometry}. Values with
     *            lower priority will be rendered first.
     */
    public void addGraphics(String groupId, Iterator geoms, int idsym,
        String label, Object tag, int priority);

	/**
	 * Removes all previously registered {@link Geometry} objects from the layer
	 * with the given groupId.
	 * 
	 * @param groupId
	 *            of the geometries to remove
	 */
	public void removeGraphics(String groupId);

	/**
	 * Removes all registered {@link Geometry} objects.
	 */
	public void clearAllGraphics();

	/**
	 * Removes all registered {@link ISymbol} objects.
	 */
	public int clearAllSymbols();

}
