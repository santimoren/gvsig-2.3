/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.events.listeners;

import org.gvsig.fmap.mapcontext.events.ColorEvent;
import org.gvsig.fmap.mapcontext.events.ExtentEvent;
import org.gvsig.fmap.mapcontext.events.ProjectionEvent;


/**
 * <p>Defines the interface for an object that listens to changes in a view port.</p>
 */
public interface ViewPortListener {
	/**
 	 * <p>Called when the <i>extent</i> of the view port has changed.</p>
	 *
	 * @param e an extend event object
	 */
	void extentChanged(ExtentEvent e);

	/**
	 * <p>Called when the background color of the view port has changed.</p>
	 * 
	 * @param e a color event object
	 */
	void backColorChanged(ColorEvent e);

	/**
	 * <p>Called when the projection of the view port has changed.</p>
	 *
	 * @param e a projection event object
	 */
	void projectionChanged(ProjectionEvent e);
}

