/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.fmap.mapcontext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.extensionpoint.ExtensionPoint;
import org.gvsig.tools.extensionpoint.ExtensionPoint.Extension;
import org.gvsig.tools.extensionpoint.ExtensionPointManager;
import org.gvsig.tools.locator.BaseLocator;
import org.gvsig.tools.locator.Locator;
import org.gvsig.tools.locator.LocatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Locator for the MapContext library.
 * 
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class MapContextLocator extends BaseLocator {

	private static Logger logger = LoggerFactory.getLogger(MapContextLocator.class);
	
	public static final String MAPCONTEXT_MANAGER_NAME = "mapcontextlocator.manager";
	private static final String MAPCONTEXT_MANAGER_DESCRIPTION = "MapContext Library manager";

	public static final String SYMBOL_MANAGER_NAME = "symbol.manager";
	private static final String SYMBOL_MANAGER_DESCRIPTION = "Symbols manager";

	public static final String DEFAULT_LAYER_ORDER_MANAGER_NAME = "default.layer.order.manager";
	private static final String DEFAULT_LAYER_ORDER_MANAGER_DESCRIPTION = "Default layer order manager";
	public static final String LAYER_ORDER_MANAGER_NAME =
			"layer.order.manager.name";
	public static final String LAYER_ORDER_MANAGER_EXT_POINT =
			"layer.order.manager.extension.point";

	private static final MapContextLocator instance = new MapContextLocator();

	/**
	 * Private constructor, we don't want it to be able to be instantiated
	 * outside this class.
	 */
	private MapContextLocator() {
		// Nothing to do
	}

	public static MapContextLocator getInstance() {
		return instance;
	}

	/**
	 * Return a reference to MapContextManager.
	 * 
	 * @return a reference to MapContextManager
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static MapContextManager getMapContextManager()
			throws LocatorException {
		return (MapContextManager) getInstance().get(MAPCONTEXT_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the MapContextManager interface.
	 * 
	 * @param clazz
	 *            implementing the MapContextManager interface
	 */
	public static void registerMapContextManager(Class clazz) {
		getInstance().register(MAPCONTEXT_MANAGER_NAME,
				MAPCONTEXT_MANAGER_DESCRIPTION, clazz);
	}

	/**
	 * Return a reference to the {@link SymbolManager}.
	 * 
	 * @return a reference to the SymbolManager.
	 * @throws LocatorException
	 *             if there is no access to the class or the class cannot be
	 *             instantiated
	 * @see Locator#get(String)
	 */
	public static SymbolManager getSymbolManager() throws LocatorException {
		return (SymbolManager) getInstance().get(SYMBOL_MANAGER_NAME);
	}

	/**
	 * Registers the Class implementing the SymbolManager interface.
	 * 
	 * @param clazz
	 *            implementing the SymbolManager interface
	 */
	public static void registerSymbolManager(Class clazz) {
		getInstance().register(SYMBOL_MANAGER_NAME, SYMBOL_MANAGER_DESCRIPTION,
				clazz);
	}
	
	/* *************************************
	 * Layer order management
	 * *************************************
	 */
	
	/**
	 * Register the class as the default order manager.
	 * This will only work if there is not a default order manager already.
	 * Also registers the class in the list of available order managers.
	 * 
	 * @param lom_class
	 * @throws LocatorException
	 */
	public static void registerDefaultOrderManager(Class lom_class)
			throws LocatorException {
		
		if (LayerOrderManager.class.isAssignableFrom(lom_class)) {
			getInstance().registerDefault(
					DEFAULT_LAYER_ORDER_MANAGER_NAME,
					DEFAULT_LAYER_ORDER_MANAGER_DESCRIPTION,
					lom_class);
			
			// Also register as available order manager
			registerOrderManager(lom_class);
			
		} else {
			// not valid class passed:
			throw new LocatorException(
					"Class '" +
			(lom_class == null ? "NULL" : lom_class.getClass().getName()) +
			"' does not implement LayerOrderManager.",
					"", -1);
		}
	}

	/**
	 * Adds the class to the list of available order managers
	 * 
	 * @param lom_class
	 */
	public static void registerOrderManager(Class lom_class) {
		
		if (LayerOrderManager.class.isAssignableFrom(lom_class)) {
			
			ExtensionPointManager epMan = ToolsLocator.getExtensionPointManager();
			ExtensionPoint ep = epMan.add(LAYER_ORDER_MANAGER_EXT_POINT, "");
			ep.append(LAYER_ORDER_MANAGER_NAME + "." + lom_class.getName(),
					"", lom_class);
		} else {
			// not valid class passed:
			throw new LocatorException(
					"Class '"
			+ (lom_class == null ? "NULL" : lom_class.getClass().getName()) 
			+ "' does not implement LayerOrderManager.",
					"", -1);
		}
	}
	
	/**
	 * Returns the default order manager or null if none was
	 * registered as default.
	 * 
	 * @return
	 * @throws LocatorException
	 */
	public static LayerOrderManager getDefaultOrderManager()
			throws LocatorException {
		
		LayerOrderManager resp = null;
		try {
			resp = (LayerOrderManager) getInstance().get(
					DEFAULT_LAYER_ORDER_MANAGER_NAME);
		} catch (Exception ex) {
			logger.info("Cant get default layer order manager", ex);
		}
		return resp;
	}
	

	/**
	 * Gets list of available order managers.
	 * 
	 * @return
	 */
	public static List getOrderManagers() {
		
		ExtensionPointManager epMan = ToolsLocator.getExtensionPointManager();
		ExtensionPoint ep = epMan.get(LAYER_ORDER_MANAGER_EXT_POINT);

		List managers = new ArrayList();
		Iterator iter = ep.iterator();

	        
		while (iter.hasNext()) {
			Extension extension = (Extension) iter.next();
			LayerOrderManager lom = null;
			try {
				lom = (LayerOrderManager) extension.create();
			} catch (Exception e) {
				logger.warn("Cant create LayerOrderManager " + extension.getName(), e);
				continue;
			}
			managers.add(lom);
		}
		return managers;
	}
	
}