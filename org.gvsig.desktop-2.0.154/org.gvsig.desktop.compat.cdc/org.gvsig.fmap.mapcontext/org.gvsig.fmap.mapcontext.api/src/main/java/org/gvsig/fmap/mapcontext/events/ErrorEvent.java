/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.events;


/**
 * <p><code>ErrorEvent</code> stores all necessary information of an error produced on a layer.</p>
 * 
 * @see FMapEvent
 * 
 * 
 * @author jaume
 */
/* 
 * jjdc
 *  
 * FIXME: No esta claro que no se use.
 *               Si se deja deprecated habria que documentar por que hay que sustituirla. 
 * @deprecated As of release 1.0.2, don't used
 */
public class ErrorEvent extends FMapEvent {
	/**
	 * <p>Extra information about the error, like which layer was produced.</p>
	 * 
	 * @see #getMessage()
	 */
	private String message;
    
    /**
     * <p>Exception associated to the error produced.</p>
     * 
     * @see #getException()
     */
	private Exception exception;

    /**
     * <p>Constructs an <code>ErrorEvent</code> with the specified, detailed message as extra information, and the exception thrown.</p>
     * 
     * @param message detailed error information
     * @param e the exception thrown when the error was produced
     */
	public ErrorEvent(String message, Exception e){
		this.message = message;
		this.exception = e;
	}

    /**
     * <p>Gets the <code>Exception</code> associated to the error produced.</p>
     * 
     * @return the exception thrown when the error was produced
     */
	public Exception getException() {
		return exception;
	}

    /**
     * <p>Gets detailed message with extra information.</p>
     * 
     * @return the detail message with extra information
     */
	public String getMessage() {
		return message;
	}

}
