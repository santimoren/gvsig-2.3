/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.events;

/**
 * <p>All events produced on a layer must be of a particular type.</p>
 *
 * <p><code>FMapEvent</code> defines the least information that can have an event
 *  produced on a layer, its <i>type</i>.</p>
 */
public class FMapEvent {
	/**
	 * Kind of event type.
	 */
	private int eventType;
	/**
	 * <p>Gets the type of this event.</p>
	 * 
	 * @return the type of this event
	 */
	public int getEventType() {
		return eventType;
	}
	/**
	 * <p>Sets the type of the event.</p>
	 * 
	 * @param eventType the number that identifies this event's type
	 */
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
}

