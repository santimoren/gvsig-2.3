/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataManager;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ProviderNotRegisteredException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;

/**
 * Crea un adaptador del driver que se le pasa como par�metro en los m�todos
 * createLayer. Si hay memoria suficiente se crea un FLyrMemory que pasa todas
 * las features del driver a memoria
 */
public class LayerFactory {
//	final static private Logger logger = LoggerFactory.getLogger(LayerFactory.class);

	private static LayerFactory instance = null;



	public static LayerFactory getInstance() {
		if (instance == null) {
			instance = new LayerFactory();
		}
		return instance;
	}

	/**
	 * Guarda registro de que clase de capa debe usar para un determinado Store
	 *
	 * como clave usamos el nombre de registro del dataStore
	 */
	private Map layersToUseForStore = new HashMap();

	/**
	 * Registra que clase tiene que usar para un {@link DataStore} determinado. <br>
	 * Por defecto, si el
	 *
	 *
	 * @param dataStoreName
	 *            Nombre de registro del {@link DataStore} dentro del
	 *            {@link DataManager}
	 * @param layerClassToUse
	 *            clase que implementa {@link SingleLayer}
	 * @return
	 */

	public boolean registerLayerToUseForStore(String dataStoreName,
			Class layerClassToUse) {

		DataManager dm = DALLocator.getDataManager();
		DataStoreParameters dsparams;
		try {
			dsparams = dm.createStoreParameters(dataStoreName);
		} catch (InitializeException e) {
			e.printStackTrace();
			return false;
		} catch (ProviderNotRegisteredException e) {
			e.printStackTrace();
			return false;
		}
		if (!layerClassToUse.isAssignableFrom(SingleLayer.class)) {
			return false;
		}
		this.layersToUseForStore.put(dsparams.getDataStoreName(),
				layerClassToUse);

		return true;
	}

	public boolean registerLayerToUseForStore(Class storeClass,
			Class layerClassToUse) {

//		DataManager dm = DALLocator.getDataManager();
		if (!DataStore.class.isAssignableFrom(storeClass)) {
			return false;
		}

		if (!SingleLayer.class.isAssignableFrom(layerClassToUse)
				|| !FLayer.class.isAssignableFrom(layerClassToUse)) {
			return false;
		}
		this.layersToUseForStore.put(storeClass,
				layerClassToUse);

		return true;
	}

	private Class getLayerClassFor(DataStore store) {
		Class result = (Class) this.layersToUseForStore.get(store.getName());
		if (result == null) {
			Iterator iter = this.layersToUseForStore.entrySet().iterator();
			Map.Entry entry;
			Class key;
			while (iter.hasNext()) {
				entry = (Entry) iter.next();
				if (entry.getKey() instanceof Class) {
					key = (Class) entry.getKey();
					if (key.isAssignableFrom(store.getClass())) {
						result = (Class) entry.getValue();
						break;
					}
				}
			}
		}
		return result;

	}



	/**
	 * @deprecated to be removed in gvSIG 2.1
	 * @see {@link MapContextManager}.
	 */
	public FLayer createLayer(String layerName,
			DataStoreParameters storeParameters) throws LoadLayerException {
		// Se obtiene el driver que lee
		try{
			DataManager dataManager=DALLocator.getDataManager();
			DataStore dataStore=dataManager.createStore(storeParameters);
			return createLayer(layerName, dataStore);
		}catch (Exception e) {
			throw new LoadLayerException(layerName,e);
		}
	}

	/**
	 * @deprecated to be removed in gvSIG 2.1
	 * @see {@link MapContextManager}.
	 */
	public FLayer createLayer(String layerName, DataStore dataStore) throws LoadLayerException{
		try{	
			Class layerClass = this.getLayerClassFor(dataStore);
			if (layerClass == null) {
				throw new CantRetrieveLayerByStoreException(layerName,dataStore.getName());
			}
			FLayer layer;
			try {
				layer = (FLayer) layerClass.newInstance();
			} catch (InstantiationException e) {
				throw new LoadLayerException(layerName, e);
			} catch (IllegalAccessException e) {
				throw new LoadLayerException(layerName, e);
			}

			layer.setName(layerName);
			((SingleLayer) layer).setDataStore(dataStore);
			IProjection proj = (IProjection)dataStore.getDynValue(FeatureStore.METADATA_CRS);
			if (proj != null) {
				layer.setProjection(proj);
			}
			layer.load();
			return layer;
		} catch (Exception e) {
			throw new LoadLayerException(layerName,e);
		}
	}
	
	private class CantRetrieveLayerByStoreException extends LoadLayerException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1442450896900126712L;

		CantRetrieveLayerByStoreException(String layerName, String storeName) {
			super(
				"Can't retrieve the class leyer of %(layer) to use for store %(store).",
				null,
				"_Cant_retrieve_the_class_leyer_of_XlayerX_to_use_for_store_XstoreX",
				serialVersionUID
			);
			setValue("layer", layerName);
			setValue("store", storeName);
		}
	}

}
