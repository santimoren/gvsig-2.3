/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;

import java.awt.Color;
import java.util.Map;

import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;


/**
 * Interface that allows the methods to classify the legend through intervals.
 *
 */
public interface IVectorialIntervalLegend extends IClassifiedVectorLegend {
	
	public static final String LEGEND_NAME = "VectorialInterval";

    public void setDefaultSymbol(ISymbol s);

    public IInterval getInterval(Object v) ;
    public int getIntervalType();

	/**
	 *
	 * Returns the symbol starting from an interval
	 *
	 * @param key interval.
	 *
	 * @return symbol.
	 */
    public ISymbol getSymbolByInterval(IInterval key);


    /**
     * Inserts the type of the classification of the intervals.
	 *
	 * @param tipoClasificacion type of the classification.
	 */
    public void setIntervalType(int tipoClasificacion);

    /**
	 * Inserts the initial color
	 * @param startColor initial color.
	 */
	public void setStartColor(Color startColor);

    /**
	 * Inserts the final color.
	 * @param endColor final color.
	 */
	public void setEndColor(Color endColor);

    /**
	 * Returns the initial color.
	 * @return  Color initial color.
	 */
	public Color getStartColor();

    /**
	 * Returns the final color
	 * @return Color  final color.
	 */
	public Color getEndColor();

	public IInterval[] calculateIntervals(FeatureStore featureStore,
			String selectedItem, int intervalCount, int shapeType)
			throws DataException;

        public void setIntervals(IInterval[] intervals);

        public Map<IInterval,ISymbol> createSymbols(IInterval[] intervals);

}

