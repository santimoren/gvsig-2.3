/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;


/**
 * <p><code>LayerDrawingListener</code> defines listeners
 *  to catch and handle drawing events from layers.</p>
 */
public interface LayerDrawingListener {
    /**
 	 * <p>Called before a layer of type {@link FLayer FLayer} or {@link FLayers FLayers} is drawn.</p>
 	 * 
     * @param e a layer event object
     * 
     * @throws CancelationException if cancels the operation, this exception will have the message
     *  that user will see.
     * 
     * @see LayerDrawEvent
     */
    void beforeLayerDraw(LayerDrawEvent e) throws CancelationException;
    
    /**
     * <p>Process to execute after a layer had been drawn.</p>
     * 
     * @param e a layer event object
     * 
     * @throws CancelationException if cancels the operation, this exception will have the message
     *  that user will see.
     * 
     * @see LayerDrawEvent
     */
    void afterLayerDraw(LayerDrawEvent e) throws CancelationException;
    
    /**
     * <p>Process to execute before a {@link GraphicLayer GraphicLayer} had been drawn.</p>
     *
     * @param e a layer event object
     * 
     * @throws CancelationException if cancels the operation, this exception will have the message
     *  that user will see.
     * 
     * @see LayerDrawEvent
     */
    void beforeGraphicLayerDraw(LayerDrawEvent e) throws CancelationException;

    /**
     * <p>Process to execute after a {@link GraphicLayer GraphicLayer} had been drawn.</p>
     * 
     * @param e a layer event object
     * 
     * @throws CancelationException if cancels the operation, this exception will have the message
     *  that user will see.
     * 
     * @see LayerDrawEvent
     */
    void afterLayerGraphicDraw(LayerDrawEvent e) throws CancelationException;
}
