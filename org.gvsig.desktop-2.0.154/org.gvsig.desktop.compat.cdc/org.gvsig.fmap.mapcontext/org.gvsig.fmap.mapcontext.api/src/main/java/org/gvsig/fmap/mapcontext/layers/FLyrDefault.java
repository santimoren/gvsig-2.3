/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.DataTypes;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.exceptions.ReloadLayerException;
import org.gvsig.fmap.mapcontext.exceptions.StartEditionLayerException;
import org.gvsig.fmap.mapcontext.impl.DefaultMapContextManager;
import org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendChangedEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.listeners.LegendListener;
import org.gvsig.metadata.MetadataContainer;
import org.gvsig.metadata.MetadataLocator;
import org.gvsig.metadata.MetadataManager;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.impl.AbstractDisposable;
import org.gvsig.tools.dynobject.DynClass;
import org.gvsig.tools.dynobject.DynObject;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.exception.DynFieldNotFoundException;
import org.gvsig.tools.dynobject.exception.DynMethodException;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;
import org.slf4j.LoggerFactory;



/**
 * <p>Implementation of the common characteristics of all layers: visibility, activation, name, ...</p>
 *
 * <p>Represents the definition of a basic layer, implementing {@link FLayer FLayer}, and new functionality:
 * <ul>
 *  <li>Supports transparency.
 *  <li>Notification of evens produced using this layer.
 *  <li>Can have internal virtual layers.
 *  <li>Can have a text layer.
 *  <li>Supports an strategy for visit its geometries.
 *  <li>Can have an image in the <i>TOC (table of contents)</i> associated to the state of this layer.
 * </ul>
 * </p>
 *
 * <p>Each graphical layer will inherit from this class and adapt to its particular logic and model according
 *  its nature.</p>
 *
 * @see FLayer
 * @see FLayerStatus
 */
public abstract class FLyrDefault extends AbstractDisposable implements FLayer, FLayerHidesArea,
		LayerListener {
	/**
	 * Useful for debug the problems during the implementation.
	 */
	final static private org.slf4j.Logger logger = LoggerFactory.getLogger(FLyrDefault.class);

	private LayerChangeSupport layerChangeSupport = new LayerChangeSupport();

        /**
         * Flag to set the layer as a temporary layer.
         */
        private boolean temporary;

	/**
	 * Path to the upper layer which this layer belongs.
	 *
	 * @see #getParentLayer()
	 * @see #setParentLayer(FLayers)
	 */
	private FLayers parentLayer = null;

	/**
	 * Transparency level of this layer in the range 0-255. By default 255.
	 * 0   --> Transparent
	 * 255 --> Opaque
	 *
	 * @see #getTransparency()
	 * @see #setTransparency(int)
	 */
	private int transparency = 255;

	/**
	 * Coordinate transformation.
	 *
	 * @see #getCoordTrans()
	 * @see #setCoordTrans(ICoordTrans)
	 */
	private ICoordTrans ct;

	/**
	 * Minimum scale, >= 0 or -1 if not defined. By default -1.
	 *
	 * @see #getMinScale()
	 * @see #setMinScale(double)
	 */
	private double minScale = -1; // -1 indica que no se usa

	/**
	 * Maximum scale, >= 0 or -1 if not defined. By default -1.
	 *
	 * @see #getMaxScale()
	 * @see #setMaxScale(double)
	 */
	private double maxScale = -1;
	//	private boolean isInTOC = true;

	/**
	 * Array list with all listeners registered to this layer.
	 *
	 * @see #getLayerListeners()
	 * @see #removeLayerListener(LayerListener)
	 * @see #callEditionChanged(LayerEvent)
	 */
	protected Set<LayerListener> layerListeners = new HashSet<>();

	//by default, all is active, visible and avalaible
	/**
	 * Status of this layer.
	 *
	 * @see #getFLayerStatus()
	 * @see #setFLayerStatus(FLayerStatus)
	 * @see #isActive()
	 * @see #setActive(boolean)
	 * @see #isVisible()
	 * @see #setVisible(boolean)
	 * @see #visibleRequired()
	 * @see #isEditing()
	 * @see #setEditing(boolean)
	 * @see #isInTOC()
	 * @see #isCachingDrawnLayers()
	 * @see #setCachingDrawnLayers(boolean)
	 * @see #isDirty()
	 * @see #setDirty(boolean)
	 * @see #isAvailable()
	 * @see #setAvailable(boolean)
	 * @see #isOk()
	 * @see #isWritable()
	 * @see #getNumErrors()
	 * @see #getError(int)
	 * @see #getErrors()
	 * @see #addError(BaseException)
	 */
	private FLayerStatus status = new FLayerStatus();
	/**
	 * Image drawn shown in the TOC according the status of this layer.
	 *
	 * @see #getTocStatusImage()
	 * @see #setTocStatusImage(Image)
	 */
	private Image tocStatusImage;

	protected MetadataContainer metadataContainer;

	/**
	 * Draw version of the context. It's used for know when de componend has
	 * changed any visualization property
	 *
	 *  @see getDrawVersion
	 *  @see updateDrawVersion
	 */
	private long drawVersion= 0L;


        private ExtendedPropertiesHelper properties = new ExtendedPropertiesHelper();

	public FLyrDefault(MetadataContainer metadataContainer) {
		this.metadataContainer = metadataContainer;
	}

	public FLyrDefault() {
		this(MetadataLocator
			.getMetadataManager()
				.createMetadataContainer(FLayer.METADATA_DEFINITION_NAME)
		);
	}


	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getProperty(java.lang.Object)
	 */
	public Object getProperty(Object key) {
		return properties.getProperty(key);
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setProperty(java.lang.Object, java.lang.Object)
	 */
	public void setProperty(Object key, Object val) {
		properties.setProperty(key, val);
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getExtendedProperties()
	 */
	public Map getExtendedProperties() {
		return properties.getExtendedProperties();
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setActive(boolean)
	 */
	public void setActive(boolean selected) {
		status.active = selected;
		callActivationChanged(LayerEvent.createActivationChangedEvent(this,
		"active"));
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isActive()
	 */
	public boolean isActive() {
		return status.active;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.metadataContainer.setDynValue(METADATA_NAME, name);
		callNameChanged(LayerEvent.createNameChangedEvent(this, "name"));
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getName()
	 */
	public String getName() {
            String name = "(unknow)";
            try {
                name = (String) this.metadataContainer.getDynValue(METADATA_NAME);
            } catch( Throwable th) {
                logger.warn("Can't retrive the layer name.");
            }
            return name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#load()
	 */
    public void load() throws LoadLayerException {
        MetadataManager manager = MetadataLocator.getMetadataManager();
        try {
            manager.loadMetadata(this);
        } catch (MetadataException e) {
            throw new LoadLayerException("Can't load metadata.", e);
        }
        DefaultMapContextManager mcmanager = (DefaultMapContextManager) MapContextLocator.getMapContextManager();
        mcmanager.notifyLoadLayer(this);
    }

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setVisible(boolean)
	 */
	public void setVisible(boolean visibility) {
		if (status.visible != visibility){
			status.visible = visibility;
			this.updateDrawVersion();

			//			if (this.getMapContext() != null){
			//				this.getMapContext().clearAllCachingImageDrawnLayers();
			//			}
			callVisibilityChanged(LayerEvent.createVisibilityChangedEvent(this,
			"visible"));
		}
	}


	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isVisible()
	 */
	public boolean isVisible() {
		return status.visible && status.available;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getParentLayer()
	 */
	public FLayers getParentLayer() {
		return parentLayer;
	}


	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setParentLayer(com.iver.cit.gvsig.fmap.layers.FLayers)
	 */
	public void setParentLayer(FLayers root) {
		if (this.parentLayer != root){
			this.parentLayer = root;
			this.updateDrawVersion();
		}
	}

	/**
	 * <p>Inserts the projection to this layer.</p>
	 *
	 * @param proj information about the new projection
	 *
	 * @see #isReprojectable()
	 * @see #reProject(MapControl)
	 */
	public void setProjection(IProjection proj) {
		IProjection curProj = this.getProjection();
		if (curProj == proj) {
			return;
		}
		if (curProj != null && curProj.equals(proj)){
			return;
		}
		this.updateDrawVersion();
		this.metadataContainer.setDynValue(METADATA_CRS, proj);
		// Comprobar que la proyecci�n es la misma que la de FMap
		// Si no lo es, es una capa que est� reproyectada al vuelo
		if ((proj != null) && (getMapContext() != null)) {
			if (proj != getMapContext().getProjection()) {
				ICoordTrans ct = proj.getCT(getMapContext().getProjection());
				setCoordTrans(ct);
				logger.debug("Cambio proyecci�n: FMap con "
						+ getMapContext().getProjection().getAbrev() + " y capa "
						+ getName() + " con " + proj.getAbrev());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.cresques.geo.Projected#getProjection()
	 */
	public IProjection getProjection() {
		return (IProjection) this.metadataContainer.getDynValue(METADATA_CRS);
	}

	/**
	 * <p>Changes the projection of this layer.</p>
	 * <p>This method will be overloaded in each kind of layer, according its specific nature.</p>
	 *
	 * @param mapC <code>MapControl</code> instance that will reproject this layer
	 *
	 * @return <code>true<code> if the layer has been created calling {@link FLayers#addLayer(FLayer) FLayers#addLayer}. But returns <code>false</code>
	 *  if the load control logic of this layer is in the reprojection method
	 *
	 * @see #isReprojectable()
	 * @see #setProjection(IProjection)
	 */
	public void reProject(ICoordTrans arg0) {
	}

	/**
	 * Returns the transparency level of this layer, in the range 0-255 .
	 *
	 * @return the transparency level
	 *
	 * @see #setTransparency(int)
	 */
	public int getTransparency() {
		return transparency;
	}

	/**
	 * Inserts the transparency level for this layer, the range allowed is 0-255 .
	 *
	 * @param trans the transparency level
	 *
	 * @see #getTransparency()
	 */
	public void setTransparency(int trans) {
		if (this.transparency != trans){
			transparency = trans;
			this.updateDrawVersion();
		}
	}


	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getMapContext()
	 */
	public MapContext getMapContext() {
		if (getParentLayer() != null) {
			return getParentLayer().getMapContext();
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#addLayerListener(com.iver.cit.gvsig.fmap.layers.LayerListener)
	 */
	public boolean addLayerListener(LayerListener o) {
            if( o == null ) {
                return true;
            }
            return layerListeners.add(o);
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getLayerListeners()
	 */
	public LayerListener[] getLayerListeners() {
		return (LayerListener[])layerListeners.toArray(new LayerListener[0]);
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#removeLayerListener(com.iver.cit.gvsig.fmap.layers.LayerListener)
	 */
	public boolean removeLayerListener(LayerListener o) {
		return layerListeners.remove(o);
	}
	/**
	 *
	 */
	private void callDrawValueChanged(LayerEvent e) {
           for (LayerListener listener : this.layerListeners) {
               try {
                   listener.drawValueChanged(e);
               } catch (Exception ex) {
                   logger.warn("Error calling listener '"+listener.toString()+"'.", ex);
               }
           }
	}
	/**
	 * Called by the method {@linkplain #setName(String)}. Notifies all listeners associated to this layer,
	 *  that its name has changed.
	 *
	 * @param e a layer event with the name of the property that has changed
	 *
	 * @see #setName(String)
	 */
	private void callNameChanged(LayerEvent e) {
           for (LayerListener listener : this.layerListeners) {
               try {
                   listener.nameChanged(e);
               } catch (Exception ex) {
                   logger.warn("Error calling listener '"+listener.toString()+"'.", ex);
               }
           }
	}

	/**
	 * Called by the method {@linkplain #setVisible(boolean)}. Notifies all listeners associated to this layer,
	 *  that its visibility has changed.
	 *
	 * @param e a layer event with the name of the property that has changed
	 *
	 * @see #setVisible(boolean)
	 */
	private void callVisibilityChanged(LayerEvent e) {
           for (LayerListener listener : this.layerListeners) {
               try {
                   listener.visibilityChanged(e);
               } catch (Exception ex) {
                   logger.warn("Error calling listener '"+listener.toString()+"'.", ex);
               }
           }
	}

	/**
	 * Called by the method {@linkplain #setActive(boolean)}. Notifies all listeners associated to this layer,
	 *  that its active state has changed.
	 *
	 * @param e a layer event with the name of the property that has changed
	 *
	 * @see #setActive(boolean)
         */
       private void callActivationChanged(LayerEvent e) {
           for (LayerListener listener : this.layerListeners) {
               try {
                   listener.activationChanged(e);
               } catch (Exception ex) {
                   logger.warn("Error calling listener '"+listener.toString()+"'.", ex);
               }
           }
       }

	/**
	 * Returns the virtual layers associated to this layer.
	 *
	 * @return a node with the layers
	 *
	 * @see #setVirtualLayers(FLayers)
	 */
	//	public FLayers getVirtualLayers() {
	//		return virtualLayers;
	//	}

	/**
	 * Inserts virtual layers to this layer.
	 *
	 * @param virtualLayers a node with the layers
	 *
	 * @see #getVirtualLayers()
	 */
	//	public void setVirtualLayers(FLayers virtualLayers) {
	//		this.virtualLayers = virtualLayers;
	//	}

	/**
	 * Sets transformation coordinates for this layer.
	 *
	 * @param ct an object that implements the <code>ICoordTrans</code> interface, and with the transformation coordinates
	 *
	 * @see #getCoordTrans()
	 */
	public void setCoordTrans(ICoordTrans ct) {
		if (this.ct == ct){
			return;
		}
		if (this.ct != null && this.ct.equals(ct)){
			return;
		}
		this.ct = ct;
		this.updateDrawVersion();
	}

	/**
	 * Returns the transformation coordinates of this layer.
	 *
	 * @return an object that implements the <code>ICoordTrans</code> interface, and with the transformation coordinates
	 *
	 * @see #setCoordTrans(ICoordTrans)
	 */
	public ICoordTrans getCoordTrans() {
		return ct;
	}

	/**
	 * <p>Method called by {@link FLayers FLayers} to notify this layer that is going to be added.
	 *  This previous notification is useful for the layers that need do something before being added. For
	 *  example, the raster needs reopen a file that could have been closed recently.</p>
	 */
	public void wakeUp() throws LoadLayerException {
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getMinScale()
	 */
	public double getMinScale() {
		return minScale;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getMaxScale()
	 */
	public double getMaxScale() {
		return maxScale;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setMinScale(double)
	 */
	public void setMinScale(double minScale) {
		if (this.minScale != minScale){
			this.minScale = minScale;
			this.updateDrawVersion();
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setMaxScale(double)
	 */
	public void setMaxScale(double maxScale) {
		if (this.maxScale != maxScale){
			this.maxScale = maxScale;
			this.updateDrawVersion();
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isWithinScale(double)
	 */
	public boolean isWithinScale(double scale) {

		boolean bVisible = true;
		if (getMinScale() != -1) {
			if (scale < getMinScale()){
				bVisible = false;
			}
		}
		if (getMaxScale() != -1) {
			if (scale > getMaxScale()) {
				bVisible = false;
			}
		}

		return bVisible;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setEditing(boolean)
	 */
	public void setEditing(boolean b) throws StartEditionLayerException {
		status.editing = b;
	}
	/**
	 * Called by some version of the method {@linkplain #setEditing(boolean)} overwritten. Notifies
	 *  all listeners associated to this layer, that its edition state has changed.
	 *
	 * @param e a layer event with the name of the property that has changed
	 *
	 * @see #setEditing(boolean)
	 */
	protected void callEditionChanged(LayerEvent e) {
           for (LayerListener listener : this.layerListeners) {
               try {
                   listener.editionChanged(e);
               } catch (Exception ex) {
                   logger.warn("Error calling listener '"+listener.toString()+"'.", ex);
               }
           }
        }

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isEditing()
	 */
	public boolean isEditing() {
		return status.editing;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getTocImageIcon()
	 */
	public String getTocImageIcon() {
		return "layer-icon";
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isInTOC()
	 */
	public boolean isInTOC() {
		return status.inTOC;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setInTOC(boolean)
	 */
	public void setInTOC(boolean b) {
		status.inTOC=b;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isAvailable()
	 */
	public boolean isAvailable() {
		return status.available;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setAvailable(boolean)
	 */
	public void setAvailable(boolean available) {
		if (status.available != available){
			status.available = available;
			this.updateDrawVersion();
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#reload()
	 */
	public void reload() throws ReloadLayerException {
	    if(this.ct!=null){
	        IProjection srcProj = this.ct.getPOrig();
	        if(!this.getProjection().equals(srcProj)){
	            this.ct = this.getProjection().getCT(this.ct.getPDest());
	        }
	    }
		this.setAvailable(true);
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getFLayerStatus()
	 */
	public FLayerStatus getFLayerStatus(){
		return status.cloneStatus();
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#setFLayerStatus(com.iver.cit.gvsig.fmap.layers.FLayerStatus)
	 */
	public void setFLayerStatus(FLayerStatus status){
		if (!this.status.equals(status)){
			this.status = status;
			this.updateDrawVersion();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isOk()
	 */

	public boolean isOk(){
		return status.isOk();
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getNumErrors()
	 */
	public int getNumErrors(){
		return status.getNumErrors();
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getError(int)
	 */
	public BaseException getError(int i){
		return status.getError(i);
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getErrors()
	 */
	public List getErrors(){
		return status.getErrors();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#addError(BaseException)
	 */
        @Override
	public void addError(BaseException exception){
		status.addLayerError(exception);
	}

        @Override
        public void setError(Exception ex) {
            this.status.setLayerError(ex);
        }

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#visibleRequired()
	 */
	public boolean visibleRequired() {
		return status.visible;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getInfoString()
	 */
	public String getInfoString() {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#isWritable()
	 */
	public boolean isWritable() {
		return status.writable;
	}

	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#cloneLayer()
	 */
	public FLayer cloneLayer() throws Exception {
		return this;
	}
	/*
	 * (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.layers.FLayer#getTocStatusImage()
	 */
	public Image getTocStatusImage() {
            return tocStatusImage;
	}

	/**
	 * Inserts the image icon that will be shown in the TOC next to this layer, according its status.
	 *
	 * @param tocStatusImage the image
	 *
	 * @see #getTocStatusImage()
	 */
	public void setTocStatusImage(Image tocStatusImage) {
		this.tocStatusImage = tocStatusImage;
		logger.debug("setTocStatusImage " + tocStatusImage + " sobre capa " + this.getName());
	}

	public ComposedLayer newComposedLayer() {
		return null;
	}

	public boolean allowLinks()
	{
		return false;
	}

	public AbstractLinkProperties getLinkProperties()
	{
		return null;
	}

	public URI[] getLink(Point2D point, double tolerance) throws ReadException{
		return null;
	}

	public void addLegendListener(LegendListener listener) {
            if( listener == null ) {
                return ;
            }
            layerChangeSupport.addLayerListener(listener);
	}

	/**
     * @param e
	 * @see LayerChangeSupport#callLegendChanged(LegendChangedEvent)
	 */
	protected void callLegendChanged(LegendChangedEvent e) {
		layerChangeSupport.callLegendChanged(e);
		if(parentLayer != null) {
			parentLayer.callLegendChanged(e);
		}
	}

	/**
	 * @see LayerChangeSupport#removeLayerListener(LegendListener)
	 */
	public void removeLegendListener(LegendListener listener) {
		layerChangeSupport.removeLayerListener(listener);
	}
	public String getClassName() {
		return this.getClass().getName();
	}

	public void delegate(DynObject dynObject) {
		this.metadataContainer.delegate(dynObject);
	}

	public DynClass getDynClass() {
		return this.metadataContainer.getDynClass();
	}

	public Object getDynValue(String name) throws DynFieldNotFoundException {
		return this.metadataContainer.getDynValue(name);
	}

	public boolean hasDynValue(String name) {
		return this.metadataContainer.hasDynValue(name);
	}

	public void implement(DynClass dynClass) {
		this.metadataContainer.implement(dynClass);
	}

	public Object invokeDynMethod(int code, Object[] args)
	throws DynMethodException {
		return this.metadataContainer.invokeDynMethod(this, code, args);
	}

	public Object invokeDynMethod(String name, Object[] args)
	throws DynMethodException {
		return this.metadataContainer.invokeDynMethod(this, name, args);
	}

	public void setDynValue(String name, Object value)
	throws DynFieldNotFoundException {
		this.metadataContainer.setDynValue(name, value);
	}

	public long getDrawVersion() {
		return this.drawVersion;
	}

	protected void updateDrawVersion(){
		this.drawVersion++;
		this.callDrawValueChanged(LayerEvent.createDrawValuesChangedEvent(this, ""));
		if (this.parentLayer != null){
			this.parentLayer.updateDrawVersion();
		}
	}

	public boolean hasChangedForDrawing(long value){
		return this.drawVersion > value;
	}

	public void activationChanged(LayerEvent e) {
	}

	public void drawValueChanged(LayerEvent e) {
		this.updateDrawVersion();
	}

	public void editionChanged(LayerEvent e) {

	}

	public void nameChanged(LayerEvent e) {

	}

	public void visibilityChanged(LayerEvent e) {

	}

	// ========================================================

	public void saveToState(PersistentState state) throws PersistenceException {
            try {
		state.set("parentLayer", (Persistent)parentLayer);
		state.set("status",status);
		state.set("minScale", minScale);
		state.set("maxScale", maxScale);
		state.set("transparency",transparency);
		state.set("coordTrans",ct);
		state.set("name", getName());
		state.set("crs", getProjection());
		state.set("properties",properties.getExtendedProperties());
            } catch(PersistenceException ex) {
                logger.warn("Can't save to persistent state the layer '"+this.getName()+"'.");
                throw ex;
            } catch(RuntimeException ex) {
                logger.warn("Can't save to persistent state the layer '"+this.getName()+"'.");
                throw ex;
            }
	}

	public void loadFromState(PersistentState state) throws PersistenceException {
            try {
		this.setDynValue(METADATA_NAME, state.getString("name"));
		this.setDynValue(METADATA_CRS, state.get("crs"));

		this.parentLayer = (FLayers) state.get("parentLayer");
		this.status = (FLayerStatus) state.get("status");
		this.minScale = state.getDouble("minScale");
		this.maxScale = state.getDouble("maxScale");
		this.transparency = state.getInt("transparency");
		this.ct = (ICoordTrans) state.get("coordTrans");

                this.properties.setExtendedProperties((Map)state.get("properties"));
            } catch(PersistenceException ex) {
                logger.warn("Can't load from persietent state the layer '"+this.getName()+"'.");
                throw ex;
            } catch(RuntimeException ex) {
                logger.warn("Can't load from persietent state the layer '"+this.getName()+"'.");
                throw ex;
            }

	}
    public static class RegisterPersistence implements Callable {

        public Object call() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		DynStruct definition = manager.addDefinition(
				FLyrDefault.class,
				"FLyrDefault",
				"FLyrDefault Persistence definition",
				null,
				null
		);
		definition.addDynFieldString("name").setMandatory(false);
		definition.addDynFieldInt("transparency").setMandatory(true);
		definition.addDynFieldDouble("minScale").setMandatory(true);
		definition.addDynFieldDouble("maxScale").setMandatory(true);
		definition.addDynFieldObject("crs").setClassOfValue(IProjection.class).setMandatory(false);
		definition.addDynFieldObject("parentLayer").setClassOfValue(FLayers.class).setMandatory(false);
		definition.addDynFieldObject("coordTrans").setClassOfValue(ICoordTrans.class).setMandatory(false);
		definition.addDynFieldObject("status").setClassOfValue(FLayerStatus.class).setMandatory(true);
                definition.addDynFieldMap("properties").setClassOfItems(Object.class)
				.setMandatory(true);

            return Boolean.TRUE;
        }
    }


//	/**
//	 * Splits string into an array of strings
//	 * @param input input string
//	 * @param sep separator string
//	 * @return an array of strings
//	 */
//	public static String[] splitString(String input, String sep) {
//		return Pattern.compile(sep).split(input, 0);
//	}

	public void clear() {
		if (metadataContainer != null) {
			metadataContainer.clear();
		}
	}

    public String getMetadataName() throws MetadataException {
        return FLayer.METADATA_DEFINITION_NAME;
    }

        public static class RegisterMetadata implements Callable {

            public Object call() {
                MetadataManager metadataManager = MetadataLocator.getMetadataManager();

                DynStruct metadataDefinition = metadataManager.getDefinition(FLayer.METADATA_DEFINITION_NAME);
                if ( metadataDefinition == null ) {
                    try {
                        metadataDefinition = metadataManager.addDefinition(
                                FLayer.METADATA_DEFINITION_NAME,
                                FLayer.METADATA_DEFINITION_DESCRIPTION);
                        metadataDefinition.addDynField(FLayer.METADATA_NAME)
                                .setMandatory(true);

                        IProjection ipr
                                = MapContextLocator.getMapContextManager().getDefaultCRS();

                        metadataDefinition.addDynFieldObject(FLayer.METADATA_CRS)
                                .setType(DataTypes.CRS).setMandatory(true)
                                .setDefaultFieldValue(ipr);
                    } catch (MetadataException e) {
                        logger.warn("Can't create metadata definition for 'Layer'", e);
                    }
                }
                return Boolean.TRUE;
            }
        }

	public String toString() {
		return this.getName();
	}

	public boolean hidesThisArea(Envelope area) {
		return false;
	}

        public boolean isTemporary() {
            return this.temporary;
        }

        public void setTemporary(boolean temporary) {
            this.temporary = temporary;
        }
}