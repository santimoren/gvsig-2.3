/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.exceptions;


/**
 * @author Vicente Caballero Navarro
 */
public class LegendLayerException extends LoadLayerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4033030636689365030L;

    protected LegendLayerException(String message, Throwable cause, String key,
            long code) {
        super(message, cause, key, code);
    }
    
	public LegendLayerException(String layer,Throwable cause) {
		super(
				"Can�t load the legend for layer %(layer).",
				cause,
				"Cant_load_the_legend_for_layer_XlayerX",
				serialVersionUID
		);
		setValue("layer", layer);
	}
	
	public LegendLayerException(String layer) {
		this(layer,null);
	}
	
}
