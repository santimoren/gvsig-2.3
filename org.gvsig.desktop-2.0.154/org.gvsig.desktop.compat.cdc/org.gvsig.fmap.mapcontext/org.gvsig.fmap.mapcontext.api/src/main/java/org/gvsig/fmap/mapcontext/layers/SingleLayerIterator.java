/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.ArrayList;


/**
 * @author fjp
 *
 * Class to iterate for all layers in FLayers, but assuring not FLayers is returned.
 * The layer you receive in next method is a "leaf" layer, not grouped. 
 */
public class SingleLayerIterator {

	ArrayList singleLayers  =new ArrayList();
	int i = 0;
	int subIndex = 0;	
	public SingleLayerIterator(FLayers layers)
	{
		addSubLayer(layers);
		
	}

	public boolean hasNext() {
		return (i < singleLayers.size());
	}

	public FLayer next() {
		FLayer aux = (FLayer) singleLayers.get(i); 
		i++;
		return aux;
	}
	
	private void addSubLayer(FLayer lyr) {
		FLayers layers;
		if (lyr instanceof FLayers)
		{
			layers = (FLayers)lyr;
			for (int i=0; i < layers.getLayersCount(); i++)
			{
				addSubLayer(layers.getLayer(i));
			}			
		}
		else
		{
			singleLayers.add(lyr);
		}		
	}
	
	public void rewind()
	{
		i=0;
	}

}


