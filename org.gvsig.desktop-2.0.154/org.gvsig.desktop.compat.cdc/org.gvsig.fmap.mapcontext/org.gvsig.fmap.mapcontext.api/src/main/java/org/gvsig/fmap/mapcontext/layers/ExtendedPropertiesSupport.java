/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.Map;

public interface ExtendedPropertiesSupport {

    /**
     * <p>
     * Returns a reference to an object (property) associated to this layer.</p>
     *
     * <p>
     * For example, you can attach a network definition to key "network" and
     * check
     * if a layer has a network loaded using
     * <i>getAssociatedObject("network")</i> and
     * that it's not null.</p>
     *
     * @param key the key associated to the property
     *
     * @return <code>null</code> if key is not found
     *
     * @see #getExtendedProperties()
     * @see #setProperty(Object, Object)
     */
    public Object getProperty(Object key);

    /**
     * Inserts an object as a property to this layer.
     *
     * @param key the key associated to the property
     * @param obj the property
     *
     * @see #getProperty(Object)
     * @see #getExtendedProperties()
     */
    public void setProperty(Object key, Object obj);

    /**
     * Returns a hash map with all new properties associated to this layer.
     *
     * @return hash table with the added properties
     *
     * @see #getProperty(Object)
     * @see #setProperty(Object, Object)
     */
    public Map getExtendedProperties();
}
