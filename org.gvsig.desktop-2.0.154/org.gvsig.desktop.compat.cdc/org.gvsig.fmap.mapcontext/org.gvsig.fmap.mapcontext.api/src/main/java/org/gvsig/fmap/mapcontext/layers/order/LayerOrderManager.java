/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.order;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.tools.persistence.Persistent;

/**
 * 
 * This manager will decide the position where layers ({@link FLayer})
 * are added to a layer collection ({@link FLayers})
 * 
 * @author gvSIG Team
 *
 */
public interface LayerOrderManager extends Persistent, Cloneable {
	
	/**
	 * <p>Gets the proposed position for the newLayer in the target layer
	 * collection. The OrderManager will study the arrangement of the
	 * target layer collection and will decide the right position for
	 * the new layer, according to its own criterion.</p>
	 * 
	 * @param target The target collection to which <code>newLayer</code>
	 * will be added
	 * @param newLayer The layer to be inserted in the layer collection
	 * 
	 * @return The proposed position for the new layer
	 */
	public int getPosition(FLayers target, FLayer newLayer);

	/**
	 * <p>Gets the name. The name should identify the
	 * policy followed by this OrderManager to sort the layers.</p>
	 * 
	 * @return The name of the OrderManager
	 */
	public String getName();

	/**
	 * <p>Gets the description. The description should be enough to get
	 * an idea of the policy and features of this OrderManager.</p>
	 * 
	 * @return The description of the OrderManager
	 */
	public String getDescription();

	/**
	 * <p>Gets the code used to register this OrderManager.</p>
	 * 
	 * @return The code used to register this orderManager 
	 */
	public String getCode();
	
	public Object clone() throws CloneNotSupportedException;

}
