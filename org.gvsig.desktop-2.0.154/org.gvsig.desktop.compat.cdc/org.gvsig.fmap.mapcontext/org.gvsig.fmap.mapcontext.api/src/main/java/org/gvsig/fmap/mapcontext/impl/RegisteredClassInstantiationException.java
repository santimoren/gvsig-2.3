/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {gvSIG}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.impl;

import org.gvsig.fmap.mapcontext.MapContextRuntimeException;

/**
 * Exception thrown when a class instance could not be created.
 * 
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class RegisteredClassInstantiationException extends
		MapContextRuntimeException {

	private static final long serialVersionUID = -2797661681885864660L;

	private static final String KEY = "registered_class_instantiation_exception";

	private static final String MESSAGE = "Unable to create an object instance"
			+ "of the type %(typeToImplement) from the class %(registeredClass)"
			+ ", registered with the name %(registeredName)";

	public RegisteredClassInstantiationException(Class typeToImplement,
			Class registeredClass, String registeredName, Throwable cause) {
		super(MESSAGE, cause, KEY, serialVersionUID);
		setValue("typeToImplement", typeToImplement);
		setValue("registeredClass", registeredClass);
		setValue("registeredName", registeredName);
	}
}