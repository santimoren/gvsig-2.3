/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend.styling;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.exception.CreateGeometryException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.fmap.mapcontext.rendering.symbols.ITextSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.fmap.mapcontext.rendering.symbols.styles.ILabelStyle;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.lang.Cloneable;

public interface ILabelClass extends Cloneable, Persistent, CartographicSupport{

	/**
	 * Returns true if the label will be showed in the map
	 *
	 * @return isVisible boolean
	 */
	boolean isVisible();

	/**
	 * Returns true if the label will be showed in the map
	 *
	 * @return isVisible boolean
	 */
	boolean isVisible(double scale);

	/**
	 * Sets the visibility of the label in the map.
	 *
	 * @param isVisible boolean
	 */
	void setVisible(boolean isVisible);

	/**
	 * Returns the text symbol that is being used for the text(the font,
	 * size,style,aligment)
	 *
	 * @return label ITextSymbol
	 */
	ITextSymbol getTextSymbol();

	/**
	 * Stablishes the text symbol that is going to be used for the text(the
	 * font,size,style,aligment)
	 *
	 * @param textSymbol ITextSymbol
	 */
	void setTextSymbol(ITextSymbol textSymbol);

	/**
	 * Stablishes the style for the label.
	 *
	 * @param labelStyle ILabelStyle
	 */
	void setLabelStyle(ILabelStyle labelStyle);

	/**
	 * Returns the style of the label
	 *
	 */
	ILabelStyle getLabelStyle();

	/**
	 * Returns the name of the label
	 *
	 */
	String getName();

	/**
	 * Stablishes the name of the label
	 * @param name
	 */
	void setName(String name);

	String toString();

	/**
	 * Sets the text for the label
	 *
	 * @param texts String[]
	 */
	void setTexts(String[] texts);

	/**
	 * Return the text for the label
	 *
	 * @param texts String[]
	 */
	String[] getTexts();

	/**
	 * <p>
	 * LabelLocationMetrics, contains the anchor point, rotation, and some
	 * other geometric calculations computed by the PlacementManager.
	 * </p>
	 *
	 * <p>
	 * The shp argument is passed as an accessory for subclasses of this
	 * class in case they need futher geometric calculations
	 * </p>
	 * @param graphics, graphics to use to paint the label.
	 * @param llm, concrete settings of the placement of this layer
	 * @param shp, the Shape over whose the label is painted
	 */
	void draw(Graphics2D graphics, ILabelLocationMetrics llm, Geometry geom);

	/**
	 * Useful to render a Label with size inside little rectangles.
	 *
	 * @param graphics Graphics2D
	 * @param bounds Rectangle
	 * @throws SymbolDrawingException
	 */
	void drawInsideRectangle(Graphics2D graphics, Rectangle bounds)
			throws SymbolDrawingException;

	int getPriority();

	void setPriority(int priority);

	Geometry getShape(ILabelLocationMetrics llm) throws CreateGeometryException;

	String getClassName();

	double getCartographicSize(ViewPort viewPort, double dpi, Geometry geom);

	int getReferenceSystem();

	int getUnit();

	void setCartographicSize(double cartographicSize, Geometry geom);

	void setReferenceSystem(int referenceSystem);

	void setUnit(int unitIndex);

	double toCartographicSize(ViewPort viewPort, double dpi, Geometry geom);

	Rectangle getBounds();

	String getSQLQuery();

	void setSQLQuery(String sqlQuery);

	/*
	 * Extended API to add latest improvements from gvSIG 1.X (sept 2013)
	 */
	
	/**
	 * Returns the expression that defines the text which will be showed in
	 * the label
	 *
	 * @return labelExpressions String[]
	 */
	public 	String[] getLabelExpressions();
	
	/**
	 * Sets the expression that defines the text which will be showed in
	 * the label
	 *
	 * @return labelExpressions String[]
	 */
	public void setLabelExpressions(String[] lbl_exps);

	public void setUseSqlQuery(boolean use_sql);
	
	public boolean isUseSqlQuery();
	
	/**
	 * Returns the expression that defines the text which will be showed in
	 * the label ready to be used for the label parser
	 *
	 * @return labelExpression String
	 */
	public 	String getStringLabelExpression();



}