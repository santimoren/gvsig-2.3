/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.events;

import org.cresques.cts.IProjection;

/**
 * <p>Event produced when the projection of the view port has changed.</p>
 */
public class ProjectionEvent extends FMapEvent {
	/**
	 * <p>The new projection.</p>
	 */
	private IProjection newProyection;

	/**
	 * <p>Identifies this object as an event related with the projection.</p>
	 */
	private static final int PROJECTION_EVENT = 0;

	/**
	 * <p>Creates a new projection event.</p>
	 * 
	 * @param proj the new projection
	 * 
	 * @return a new projection event
	 */
	public static ProjectionEvent createProjectionEvent(IProjection proj){
		return new ProjectionEvent(proj, PROJECTION_EVENT);
	}

	/**
	 * <p>Creates a new projection event of the specified type.</p>
	 *
	 * @param proj the new projection
	 * @param eventType identifier of this kind of event
	 */
	private ProjectionEvent(IProjection proj, int eventType) {
		setEventType(eventType);
		newProyection = proj;
	}

	/**
	 * <p>Gets the new projection.</p>
	 *
	 * @return the new projection
	 */
	public IProjection getNewProjection() {
		return newProyection;
	}
}
