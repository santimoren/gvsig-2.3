/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.operations;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.LayerDrawEvent;
import org.gvsig.tools.task.Cancellable;

/**
 * Abstract class for the classes that make able
 * a single draw for a layers group
 *
 * @see  org.gvsig.fmap.mapcontext.layers.FLayer#newComposedLayer()
 */
public abstract class ComposedLayer {

	ArrayList pendingLayersEvents= new ArrayList();
	MapContext mapContext = null;

	public ComposedLayer(){

	}

	public void setMapContext(MapContext mapContext) {
		this.mapContext = mapContext;
	}

	/**
	 * Checks if the layer can be added to this group
	 *
	 * @param layer
	 * @return layer can be added or not
	 */
	public abstract boolean canAdd(FLayer layer);

	/**
	 * Adds the layer to the draw group.
	 * You have to implements the doAdd(FLayer) method.
	 *
	 *
	 * @see org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer#canAdd(FLayer)
	 *
	 * @param layer
	 * @throws Exception
	 */
	public final void add(FLayer layer) throws Exception {
		if (this.mapContext == null){
			this.mapContext =layer.getMapContext();
		}
        doAdd(layer);
    	pendingLayersEvents.add(layer);
	}

	/**
	 * Draws all the group in one pass
	 * You have to implements the doDraw method.
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.FLayer#draw(BufferedImage, Graphics2D, ViewPort, Cancellable, double)
	 */
    public final void draw(BufferedImage image, Graphics2D g, ViewPort viewPort,
			Cancellable cancel,double scale) throws ReadException {
    	fireLayerDrawingEvents(g,viewPort, LayerDrawEvent.LAYER_BEFORE_DRAW);
    	doDraw(image, g, viewPort, cancel, scale);
    	fireLayerDrawingEvents(g,viewPort, LayerDrawEvent.LAYER_AFTER_DRAW);
		pendingLayersEvents.clear();
    }

	/**
	 * Prints all the group in one pass
	 * You have to implements the doDraw method.
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.FLayer#draw(BufferedImage, Graphics2D, ViewPort, Cancellable, double)
	 */
    public final void print(Graphics2D g, ViewPort viewPort,
			Cancellable cancel,double scale,PrintAttributes properties) throws ReadException {
    	doPrint(g, viewPort, cancel, scale,properties);
		pendingLayersEvents.clear();
    }

	/**
	 * Fires the event LayerDrawEvent.LAYER_AFTER_DRAW for every
	 * layers of the group.
	 *
	 *
	 *  @see org.gvsig.fmap.mapcontext.layers.LayerDrawEvent
	 *
	 */
	private void fireLayerDrawingEvents(Graphics2D g, ViewPort viewPort, int eventType) {
		Iterator iter = pendingLayersEvents.iterator();
		LayerDrawEvent afterEvent;
		FLayer layer = null ;
		while (iter.hasNext()) {
			layer =(FLayer)iter.next();
			afterEvent = new LayerDrawEvent(layer, g, viewPort, eventType);
			// System.out.println("+++ evento " + afterEvent.getLayer().getName());
			mapContext.fireLayerDrawingEvent(afterEvent);
		}
	}


	/**
	 * Adds the layer to the draw group.
	 * Specific implementation.
	 *
	 * @see org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer#add(FLayer)
	 *
	 * @see org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer#canAdd(FLayer)
	 *
	 * @param layer
	 * @throws Exception
	 */
	protected abstract void doAdd(FLayer layer) throws Exception ;

	/**
	 * Draws all the group in one pass.
	 * Specific implementation.
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer#draw(BufferedImage, Graphics2D, ViewPort, Cancellable, double)
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.FLayer#draw(BufferedImage, Graphics2D, ViewPort, Cancellable, double)
	 */
	protected abstract void doDraw(BufferedImage image, Graphics2D g, ViewPort viewPort,
			Cancellable cancel,double scale) throws ReadException ;

	/**
	 * Prints all the group in one pass.
	 * Specific implementation.
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer#print(Graphics2D, ViewPort, Cancellable, double)
	 *
	 * @see  org.gvsig.fmap.mapcontext.layers.FLayer#print(Graphics2D, ViewPort, Cancellable, double)
	 */
	protected abstract void doPrint(Graphics2D g, ViewPort viewPort,
			Cancellable cancel,double scale,PrintAttributes properties) throws ReadException ;

}
