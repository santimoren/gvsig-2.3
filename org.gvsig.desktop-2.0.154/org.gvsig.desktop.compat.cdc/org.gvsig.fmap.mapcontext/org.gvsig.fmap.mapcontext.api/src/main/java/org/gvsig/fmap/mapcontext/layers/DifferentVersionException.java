/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

/**
 * Excepci�n que se lanzara cunado exista un problema de versi�n.
 *
 * @author Vicente Caballero Navarro
 */
public class DifferentVersionException extends Exception {
	/**
	 * Crea una nueva DifferentVersionException
	 */
	public DifferentVersionException() {
		super();
	}

	/**
	 * Crea una nueva DifferentVersionException
	 *
	 * @param arg0
	 */
	public DifferentVersionException(String arg0) {
		super(arg0);
	}

	/**
	 * Crea una nueva DifferentVersionException
	 *
	 * @param arg0
	 * @param arg1
	 */
	public DifferentVersionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Crea una nueva DifferentVersionException
	 *
	 * @param arg0
	 */
	public DifferentVersionException(Throwable arg0) {
		super(arg0);
	}
}
