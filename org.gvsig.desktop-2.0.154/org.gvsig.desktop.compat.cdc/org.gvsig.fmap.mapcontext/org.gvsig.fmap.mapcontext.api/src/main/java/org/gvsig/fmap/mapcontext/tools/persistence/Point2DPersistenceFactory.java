/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.geom.Point2D;
import org.gvsig.tools.ToolsLocator;

import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Factory to manage the persistence of {@link Point2D} objects.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 * @author 2010- <a href="jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a> -
 *         gvSIG team
 */
public class Point2DPersistenceFactory extends AbstractSinglePersistenceFactory {

	static final String FIELD_X = "x";
	static final String FIELD_Y = "y";

	private static final String DYNCLASS_NAME = "AwtPoint2D";
	private static final String DYNCLASS_DESCRIPTION = "Awt Point2D";

	/**
	 * Creates a new {@link Point2DPersistenceFactory} with the
	 * {@link DynStruct} definition for the {@link Point2D} objects.
	 */
	public Point2DPersistenceFactory() {
		super(Point2D.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null, null);

		DynStruct definition = this.getDefinition();

		definition.addDynFieldDouble(FIELD_X).setMandatory(true);
		definition.addDynFieldDouble(FIELD_Y).setMandatory(true);
	}

	public Object createFromState(PersistentState state)
			throws PersistenceException {
		double x = state.getDouble(FIELD_X);
		double y = state.getDouble(FIELD_Y);

		return new Point2D.Double(x, y);
	}

	public void saveToState(PersistentState state, Object obj)
			throws PersistenceException {
		Point2D point = (Point2D) obj;
		state.set(FIELD_X, point.getX());
		state.set(FIELD_Y, point.getY());
	}
        
        public static class RegisterPersistence implements Callable {

            public Object call() throws Exception {
                PersistenceManager persistenceManager
                        = ToolsLocator.getPersistenceManager();
                persistenceManager.registerFactory(new Point2DPersistenceFactory());
                return Boolean.TRUE;
            }
        }
}