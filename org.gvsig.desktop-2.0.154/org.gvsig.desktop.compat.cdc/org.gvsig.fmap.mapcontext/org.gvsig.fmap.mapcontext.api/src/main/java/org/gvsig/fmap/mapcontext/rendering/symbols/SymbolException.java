/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols;

import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.tools.exception.BaseException;

/**
 * Parent exception of symbol related exceptions.
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public abstract class SymbolException extends MapContextException {

	private static final long serialVersionUID = -5432842435290441524L;

	/**
	 * @see BaseException#BaseException(String, String, long)
	 */
	public SymbolException(String message, String key, long code) {
		super(message, key, code);
	}

	/**
	 * @see BaseException#BaseException(String, Throwable, String, long)
	 */
	public SymbolException(String message, Throwable cause, String key,
			long code) {
		super(message, cause, key, code);
	}
}
