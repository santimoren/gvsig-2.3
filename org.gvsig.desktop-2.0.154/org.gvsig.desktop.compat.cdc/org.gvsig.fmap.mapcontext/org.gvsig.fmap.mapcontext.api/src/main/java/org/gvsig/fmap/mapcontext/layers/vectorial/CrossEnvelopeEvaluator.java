/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.exception.DataEvaluatorRuntimeException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.evaluator.AbstractEvaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;

public class CrossEnvelopeEvaluator extends AbstractEvaluator {

	private Envelope envelope;
	private String geomName;
	private Envelope envelopeTrans;
	private boolean isDefault;
	private String srs;
	private String envelopeWKT;

	public CrossEnvelopeEvaluator(Envelope envelope,
			IProjection envelopeProjection, FeatureType featureType,
			String geomName) {
		FeatureAttributeDescriptor fad = (FeatureAttributeDescriptor) featureType
				.get(geomName);

		this.isDefault = featureType.getDefaultGeometryAttributeName().equals(
				geomName);
		this.envelope = envelope;
		// this.srs = envelopeProjection.getAbrev();
		// this.projection=CRSFactory.getCRS(fad.getSRS());
		// this.envelopeProjection=envelopeProjection;
//		ICoordTrans ct = null;
//		if (!this.srs.equals(fad.getSRS())) { // FIXME comparaci�n
//			ct = envelopeProjection.getCT(fad.getSRS());
//		}
//		if (ct != null) {
//			this.envelopeTrans = envelope.convert(ct);
//		} else {
			this.envelopeTrans = envelope;
//		}
		this.geomName = geomName;
		this.srs = envelopeProjection.getAbrev();

		this.getFieldsInfo().addMatchFieldValue(geomName, envelopeTrans);

	}

	public Object evaluate(EvaluatorData data) throws EvaluatorException {
		if (isDefault) {
			Feature feature = (Feature) data.getContextValue("feature");
			Envelope featureEnvelope = feature.getDefaultEnvelope();
                        if( featureEnvelope == null ) {
                            return Boolean.FALSE;
                        }
			Boolean r = new Boolean(envelopeTrans.intersects(featureEnvelope));
			return r;

		} else {
			Geometry geom = (Geometry) data.getDataValue(geomName);
                        if( geom == null ) {
                            return Boolean.FALSE;
                        }
			return new Boolean(envelopeTrans.intersects(geom.getEnvelope()));

		}
	}

	public String getName() {
		return "contains in envelope";
	}

	public String getSQL() {
		if (envelopeWKT == null) {
			try {
				envelopeWKT = envelope.getGeometry().convertToWKT();
			} catch (GeometryOperationNotSupportedException e) {
				throw new DataEvaluatorRuntimeException(e);
			} catch (GeometryOperationException e) {
				throw new DataEvaluatorRuntimeException(e);
			}
		}

		return " ST_intersects(ST_GeomFromText('" + envelopeWKT + "', '" + srs
				+ "'), ST_envelope(" + geomName + ")) ";
	}

}
