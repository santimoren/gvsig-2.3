/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import org.gvsig.tools.ToolsLocator;

import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Factory to manage the persistence of {@link Rectangle2D} objects.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 * @author 2010- <a href="jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a> -
 *         gvSIG team
 */
public class DimensionPersistenceFactory extends
		AbstractSinglePersistenceFactory {

	private static final String DYNCLASS_NAME = "AwtDimension";
	private static final String DYNCLASS_DESCRIPTION = "Dimension in AWT";

	public static final String FIELD_HEIGHT = "height";
	public static final String FIELD_WIDTH = "width";

	/**
	 * Creates a new {@link DimensionPersistenceFactory} with the
	 * {@link DynStruct} definition for the {@link Dimension} objects.
	 */
	public DimensionPersistenceFactory() {
		super(Dimension.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null,
				null);

		DynStruct definition = this.getDefinition();

		definition.addDynFieldInt(FIELD_WIDTH).setMandatory(true);
		definition.addDynFieldInt(FIELD_HEIGHT).setMandatory(true);
	}

	public Object createFromState(PersistentState state)
			throws PersistenceException {
		Dimension dimension = new Dimension();
		dimension.width = state.getInt(FIELD_WIDTH);
		dimension.height = state.getInt(FIELD_HEIGHT); 
		return dimension;
	}

	public void saveToState(PersistentState state, Object obj)
			throws PersistenceException {
		Dimension dim = (Dimension) obj;

		state.set(FIELD_HEIGHT, dim.height);
		state.set(FIELD_WIDTH, dim.width);
	}
        
        public static class RegisterPersistence implements Callable {

            public Object call() throws Exception {
                PersistenceManager persistenceManager
                        = ToolsLocator.getPersistenceManager();
                persistenceManager.registerFactory(new DimensionPersistenceFactory());
                return Boolean.TRUE;
            }
        }         
}