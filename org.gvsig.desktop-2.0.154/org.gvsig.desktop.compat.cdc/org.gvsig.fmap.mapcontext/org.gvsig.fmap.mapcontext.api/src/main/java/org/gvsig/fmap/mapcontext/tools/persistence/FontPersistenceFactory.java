/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.Font;
import org.gvsig.tools.ToolsLocator;

import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Factory to manage the persistence of {@link Font} objects.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 * @author 2010- <a href="jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a> -
 *         gvSIG team
 */
public class FontPersistenceFactory extends AbstractSinglePersistenceFactory {

	static final String FIELD_NAME = "name";
	static final String FIELD_STYLE = "style";
	static final String FIELD_SIZE = "size";

	private static final String DYNCLASS_NAME = "AwtFont";
	private static final String DYNCLASS_DESCRIPTION = "Awt Font";

	/**
	 * Creates a new {@link FontPersistenceFactory} with the {@link DynStruct}
	 * definition for the {@link Font} objects.
	 */
	public FontPersistenceFactory() {
		super(Font.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null, null);

		DynStruct definition = this.getDefinition();

		definition.addDynFieldString(FIELD_NAME).setMandatory(true);
		definition.addDynFieldInt(FIELD_STYLE).setMandatory(true);
		definition.addDynFieldInt(FIELD_SIZE).setMandatory(true);
	}

	public Object createFromState(PersistentState state)
			throws PersistenceException {
		String name = state.getString(FIELD_NAME);
		int style = state.getInt(FIELD_STYLE);
		int size = state.getInt(FIELD_SIZE);

		return new Font(name, style, size);
	}

	public void saveToState(PersistentState state, Object obj)
			throws PersistenceException {
		Font font = (Font) obj;
		state.set(FIELD_NAME, font.getName());
		state.set(FIELD_STYLE, font.getStyle());
		state.set(FIELD_SIZE, font.getSize());
	}
        
        public static class RegisterPersistence implements Callable {

            public Object call() throws Exception {
                PersistenceManager persistenceManager
                        = ToolsLocator.getPersistenceManager();
                persistenceManager.registerFactory(new FontPersistenceFactory());
                return Boolean.TRUE;
            }
        }        
}