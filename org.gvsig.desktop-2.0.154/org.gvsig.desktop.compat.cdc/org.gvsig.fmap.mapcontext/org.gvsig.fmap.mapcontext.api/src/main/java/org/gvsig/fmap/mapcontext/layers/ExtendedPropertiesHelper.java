/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

public class ExtendedPropertiesHelper implements ExtendedPropertiesSupport, Persistent {

    private static final Logger logger = LoggerFactory.getLogger(ExtendedPropertiesHelper.class);

    /*
    Add this to class that implements ExtendedPropertiesSupport

        private ExtendedPropertiesHelper propertiesHelper = new ExtendedPropertiesHelper();

    This methods

        public Object getProperty(Object key) {
            return this.propertiesHelper.getProperty(key);
        }

        public void setProperty(Object key, Object obj) {
            this.propertiesHelper.setProperty(key, obj);
        }

        public Map getExtendedProperties() {
            return this.propertiesHelper.getExtendedProperties();
        }

    To the persistence defnition

        definition.addDynFieldObject("propertiesHelper").setClassOfValue(ExtendedPropertiesHelper.class)
                        .setMandatory(false);

    To the loadFromState

        this.propertiesHelper = (ExtendedPropertiesHelper) state.get("propertiesHelper");

    And to the saveToState

        state.set("propertiesHelper",propertiesHelper);

    */
    private Map properties = new HashMap();

    public Object getProperty(Object key) {
        return properties.get(key);
    }

    public void setProperty(Object key, Object val) {
        properties.put(key, val);
    }

    public Map getExtendedProperties() {
        return properties;
    }

    public void setExtendedProperties(Map properties) {
        this.properties = new HashMap();
        this.properties.putAll(properties);
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("properties", properties);
    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        this.properties = new HashMap();

        Map props = (Map) state.get("properties");
        Iterator it = props.keySet().iterator();
        while(it.hasNext()){
            Object key = it.next();
            if (key == null){
                continue;
            }
            try {
                Object value = props.get(key);
                this.properties.put(key, value);
            } catch (Exception e) {
                logger.warn("Can't read property "+key.toString()+" from persistence.",e);
            }
        }
    }

    public static class RegisterPersistence implements Callable {

        public Object call() {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            DynStruct definition = manager.addDefinition(
                    ExtendedPropertiesHelper.class,
                    "ExtendedPropertiesHelper",
                    "ExtendedPropertiesHelper Persistence definition",
                    null,
                    null
            );
            definition.addDynFieldMap("properties").setClassOfItems(Object.class)
                    .setMandatory(true);

            return Boolean.TRUE;
        }
    }

}
