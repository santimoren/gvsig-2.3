/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols.styles;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;


/**
 * Defines the style that a Label symbol can contain which typically define
 * a background of the label as an Image, the texts that the label holds
 * and their position within the label in rendering time.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public interface ILabelStyle extends IStyle {
	/**
	 * @return int, the amount of fields this style allows
	 */
	public int getFieldCount();

	/**
	 * Sets the texts that will appear in the label.
	 * @param texts
	 */
	public void setTextFields(String[] texts);


	/**
	 * Returns an array of rectangles defining the text boxes where text is
	 * placed.
	 * @return
	 */
	public Rectangle2D[] getTextBounds();

	public Dimension getSize();

	/**
	 * Returns the position of the point labeled by this label in percent units relative
	 * to the label bounds. It determines the offset to be applied to the label to be
	 * placed and allows the user to use custom styles.
	 */
	public Point2D getMarkerPoint();

	/**
	 * Sets the position of the point labeled by this in percent units relative to the
	 * label bounds
	 * @param p
	 * @throws IllegalArgumentException if the point coordinates are >0.0 or <1.0
	 */
	public void setMarkerPoint(Point2D p) throws IllegalArgumentException;

	/**
	 * Sets a TextFieldArea using its index. With this method the user can
	 * modify the size of the rectangle for the TextFieldArea
	 * @param index
	 * @param rect
	 */
	public void setTextFieldArea(int index, Rectangle2D rect);
	
	/**
	 * Adds a new TextFieldArea with an specific size which is defined as a rectangle.
	 * @param index,int
	 * @param rect,Rectangle2D
	 */
	public void addTextFieldArea(Rectangle2D rect);
	
	/**
	 * Delete the TextFieldArea specified by its index.
	 * @param index,int
	 */
	public void deleteTextFieldArea(int index);
	
	/**
	 * Sets the size for a laber and stablishes an unit scale factor to not change
	 * too much its content
	 * @param width
	 * @param height
	 */
	public void setSize(double width, double height);
	
	
	public IBackgroundFileStyle getBackgroundFileStyle() ;

	public void setBackgroundFileStyle(IBackgroundFileStyle background);
}
