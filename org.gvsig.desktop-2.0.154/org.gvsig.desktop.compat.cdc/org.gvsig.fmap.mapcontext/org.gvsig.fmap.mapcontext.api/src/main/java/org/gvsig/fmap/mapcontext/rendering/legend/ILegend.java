/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;

import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.Persistent;

/**
 * Information about the legend to be represented in the interface.
 *
 */
public interface ILegend extends Persistent, Cloneable {
	/**
	 * Obtains the default symbol of the legend.
	 *
	 * @return default symbol.
	 */
	ISymbol getDefaultSymbol();

	/**
	 * Clones the legend.
	 *
	 * @return Cloned legend.
	 *
	 * @throws XMLException
	 */
	ILegend cloneLegend();
	/**
	 * Adds a new listener to the legend.
	 *
	 * @param listener to be added
	 */
	void addLegendListener(LegendContentsChangedListener listener);
	/**
	 * Removes a listener from the legend.
	 *
	 * @param listener to be removed
	 */
	public void removeLegendListener(LegendContentsChangedListener listener);
	/**
	 * Executed when the default symbol of a legend is changed.
	 *
	 * @param event
	 */
	public void fireDefaultSymbolChangedEvent(SymbolLegendEvent event);
	/**
	 * Obtains the listeners of a legend.
	 *
	 * @return LegendListener[] array composed by the listeners of a legend.
	 */
	public LegendContentsChangedListener[] getListeners();
}

