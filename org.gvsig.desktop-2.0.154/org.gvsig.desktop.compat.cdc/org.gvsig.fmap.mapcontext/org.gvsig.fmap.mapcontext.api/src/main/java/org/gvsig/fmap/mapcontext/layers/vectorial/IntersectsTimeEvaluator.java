/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.timesupport.Time;
import org.gvsig.tools.evaluator.AbstractEvaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;


/**
 * @author gvSIG Team
 * @version $Id$
 *
 */
public class IntersectsTimeEvaluator extends AbstractEvaluator {
    private Time time;
    private String timeAttributeName;
   
    public IntersectsTimeEvaluator(Time time, String timeAttributeName) {
        super();  
        this.time = time;
        this.timeAttributeName = timeAttributeName;
        this.getFieldsInfo().addMatchFieldValue(timeAttributeName, time);
    }

    public Object evaluate(EvaluatorData data) throws EvaluatorException {
        Feature feature = ((Feature) data.getContextValue("feature"));
        Time featureTime = (Time)feature.get(timeAttributeName);
        if( featureTime==null ) {
            return Boolean.FALSE;
        }
        return new Boolean(time.intersects(featureTime));       
    }

    public String getName() {       
        return "intersets with time";
    }

    public String getSQL() {
        // TODO add the time filter!!!
        return super.getSQL();
    }
}
