
package org.gvsig.fmap.mapcontext.rendering.legend.styling;

public interface ILabelClassFactory {

    public String getName();

    public String getID();
    
    public ILabelClass create();

    public String toString();
    
}
