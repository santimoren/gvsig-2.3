/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.operations;

import org.gvsig.fmap.mapcontext.exceptions.LegendLayerException;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;

/**
 * <p>All vector layers that can be classifiable and can have associated a legend, must implement
 *  <code>ClassifiableVectorial</code>.</p>
 *
 * @see Classifiable
 */
public interface ClassifiableVectorial extends Classifiable{
	/**
	 * <p>Sets the layer's legend as a vector legend.</p>
	 *
	 * @param r the legend with vector data
	 *
	 * @throws LegendLayerException if fails setting the legend to this layer.
	 */
	void setLegend(IVectorLegend r) throws LegendLayerException;
}
