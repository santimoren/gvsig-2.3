/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.tools.coerce;

import java.awt.Color;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dataTypes.CoercionException;
import org.gvsig.tools.dataTypes.DataTypesManager;
import org.gvsig.tools.dataTypes.DataTypesManager.Coercion;

/**
 * Convert a Color to String.
 * 
 *  
 * @author gvSIG Team
 * @version $Id$
 * 
 */
public class CoerceStringToColor implements Coercion {

	public CoerceStringToColor() {
		// Do nothing
	}
	
	public Object coerce(Object value) throws CoercionException {
		try {
			if( value == null ) {
				return value;
			}
			if( value instanceof String ) {
				SimpleAttributesSerializer properties = new SimpleAttributesSerializer("Color");
				properties.load((String) value);
				int r = Integer.parseInt((String)(properties.get("red")));
				int g = Integer.parseInt((String)(properties.get("green")));
				int b = Integer.parseInt((String)(properties.get("blue")));
				int a = Integer.parseInt((String)(properties.get("alpha")));
				return new Color(r,g,b,a);
			}
		} catch (Exception e) {
			throw new CoercionException(e);
		}
		throw new CoercionException();
	}
	public static void selfRegister() {
        DataTypesManager dataTypesManager = ToolsLocator.getDataTypesManager();
		int id = dataTypesManager.getType("String");
        dataTypesManager.addCoercion(id, new CoerceStringToColor());
	}
}

