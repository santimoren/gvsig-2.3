/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.tools.coerce;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class SimpleAttributesSerializer {
	Map values = null;
	String name = null;
	
	public SimpleAttributesSerializer(String name) {
		this.name = name;
		this.values = new HashMap();
	}
	
	public void put(String name, Object value) {
		this.values.put(name, value);
	}
	
	public Object get(String name) {
		return this.get(name);
	}
	
	public String toString() {
		StringBuffer buffer = null;
		Iterator it = this.values.entrySet().iterator();
		while( it.hasNext() ) {
			Entry entry = (Entry)(it.next());
			if( buffer==null ) {
				buffer = new StringBuffer();
				buffer.append(this.name).append(":");
			} else {
				buffer.append(";");
			}
			buffer.append(entry.getKey())
				.append("=")
				.append(entry.getValue())
				.append(";");
		}
		return buffer.toString();
	}
	
	public void load(String buffer) {
		if( ! buffer.startsWith(this.name+":") ) {
			return;
		}
		
		String[] pairs = buffer.substring(this.name.length()) .split(";");
		for( int i=0; i<pairs.length; i++ ) {
			String[] pair = pairs[i].split("=");
			if( pair.length==2 ) {
				this.values.put(pair[0].trim(), pair[1].trim());
			}
		}
	}
}
