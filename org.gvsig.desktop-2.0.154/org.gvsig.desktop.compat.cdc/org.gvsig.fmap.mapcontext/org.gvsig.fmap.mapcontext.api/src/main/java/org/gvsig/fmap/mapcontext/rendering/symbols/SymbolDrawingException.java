/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols;

import org.gvsig.fmap.mapcontext.Messages;

/**
 * Exception thrown when a symbol cannot be drawn. The type of the exception
 * will determine the cause.
 * <ol>
 * 	<li><b>Unsupported set of settings</b>: getType() == SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS</li>
 * 	<li><b>Shape type mismatch</b>: getType() == SymbolDrawingException.SHAPETYPE_MISMATCH</li>
 * </ol>
 * 
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class SymbolDrawingException extends Exception {
	private static final long serialVersionUID = 4059410328246182074L;
	public static final int UNSUPPORTED_SET_OF_SETTINGS = 0;
	public static final int SHAPETYPE_MISMATCH = 1;
	public static final String STR_UNSUPPORTED_SET_OF_SETTINGS = Messages.getString("unsupported_set_of_settings");
	
	private int type;
	
	public int getType() {
		return type;
	}
	/**
	 * Creates a new instance of this exception.
	 * <ol>
	 * 	<li><b>Unsupported set of settings</b>: getType() == SymbolDrawingException.UNSUPPORTED_SET_OF_SETTINGS</li>
	 * 	<li><b>Shape type mismatch</b>: getType() == SymbolDrawingException.SHAPETYPE_MISMATCH</li>
	 * </ol>
	 */ 
	public SymbolDrawingException(int type) {
		this.type = type;
	}
}
