/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.operations;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;

/**
 * DOCUMENT ME!
 *
 */
public interface SingleLayer extends FLayer {
	/**
	 * M�todo que devuelve el DataStore del que saca los datos la capa.
	 *
	 * @return DataStore de la capa.
	 */
	public DataStore getDataStore();

	/**
	 * Fija el DataStore asociado a la capa. 
	 * Este metodo no deberia ser usado desde el API, es un metodo interno que solo
	 * deberia ser usado por la factoria de capas.
	 *
	 * @param dataStore a usar en la capa.
	 *
	 * @throws LoadLayerException
	 * @throws {@link UnsupportedOperationException}
	 *
	 * @deprecated no esta permitido cambiar el datastore de ua capa una vez creada esta.
	 */
	public void setDataStore(DataStore dataStore) throws LoadLayerException;

    /**
     * By default null
     * @return
     */
//    public Strategy getStrategy();
//    public void setStrategy(Strategy s);
}
