/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.Color;
import org.gvsig.tools.ToolsLocator;

import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.AbstractSinglePersistenceFactory;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;

/**
 * Factory to manage the persistence of {@link Color} objects.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 * @author 2010- <a href="jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a> -
 *         gvSIG team
 */
public class ColorPersistenceFactory extends AbstractSinglePersistenceFactory {

	static final String FIELD_RED = "red";
	static final String FIELD_GREEN = "green";
	static final String FIELD_BLUE = "blue";
	static final String FIELD_ALPHA = "alpha";

	private static final String DYNCLASS_NAME = "AwtColor";
	private static final String DYNCLASS_DESCRIPTION = "Awt Color";

	/**
	 * Creates a new {@link ColorPersistenceFactory} with the {@link DynStruct}
	 * definition for the {@link Color} objects.
	 */
	public ColorPersistenceFactory() {
		super(Color.class, DYNCLASS_NAME, DYNCLASS_DESCRIPTION, null, null);

		DynStruct definition = this.getDefinition();

		definition.addDynFieldInt(FIELD_RED).setMandatory(true);
		definition.addDynFieldInt(FIELD_GREEN).setMandatory(true);
		definition.addDynFieldInt(FIELD_BLUE).setMandatory(true);
		definition.addDynFieldInt(FIELD_ALPHA).setMandatory(true);
	}

	public Object createFromState(PersistentState state)
			throws PersistenceException {
		int red = state.getInt(FIELD_RED);
		int green = state.getInt(FIELD_GREEN);
		int blue = state.getInt(FIELD_BLUE);
		int alpha = state.getInt(FIELD_ALPHA);

		return new Color(red, green, blue, alpha);
	}

	public void saveToState(PersistentState state, Object obj)
			throws PersistenceException {
		Color color = (Color) obj;
		state.set(FIELD_RED, color.getRed());
		state.set(FIELD_GREEN, color.getGreen());
		state.set(FIELD_BLUE, color.getBlue());
		state.set(FIELD_ALPHA, color.getAlpha());
	}

        public static class RegisterPersistence implements Callable {

            public Object call() throws Exception {
                PersistenceManager persistenceManager
                        = ToolsLocator.getPersistenceManager();
                persistenceManager.registerFactory(new ColorPersistenceFactory());
                return Boolean.TRUE;
            }
        }

}