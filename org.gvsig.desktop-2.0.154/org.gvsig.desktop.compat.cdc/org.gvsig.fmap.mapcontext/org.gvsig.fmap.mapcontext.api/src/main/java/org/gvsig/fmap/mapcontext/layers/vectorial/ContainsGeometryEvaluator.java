/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.gvsig.fmap.dal.exception.DataEvaluatorRuntimeException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.tools.evaluator.AbstractEvaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;
/**
 *
 * @author Vicente Caballero Navarro
 *
 */
public class ContainsGeometryEvaluator extends AbstractEvaluator {

	private String geomName;
	private Geometry geometry;
	private Geometry geometryTrans;
	private String srs;
	private boolean isDefault;
	private String geometryWKT;

	public ContainsGeometryEvaluator(Geometry geometry,
			IProjection projection, FeatureType featureType,
			String geomName) {
		FeatureAttributeDescriptor fad = (FeatureAttributeDescriptor) featureType
				.get(geomName);
		this.isDefault = featureType.getDefaultGeometryAttributeName().equals(
				geomName);
		this.geometry = geometry;
		this.geometryTrans = geometry.cloneGeometry();
		this.srs = projection.getAbrev();
		
		IProjection fad_proj = fad.getSRS();
		ICoordTrans ct = null;
		if (fad_proj != null && !fad_proj.equals(projection)) {
		    ct = projection.getCT(fad_proj);
		}

		if (ct != null) {
			geometryTrans.reProject(ct);
		}
		this.geomName = geomName;

		this.getFieldsInfo().addMatchFieldValue(geomName, geometryTrans);

	}

	public Object evaluate(EvaluatorData data) throws EvaluatorException {
		try {
                    Geometry geom = null;
                    if ( isDefault ) {
                        Feature feature = (Feature) data.getContextValue("feature");
                        geom = feature.getDefaultGeometry();

                    } else {
                        geom = (Geometry) data.getDataValue(geomName);
                    }
                    if ( geom == null ) {
                        return Boolean.FALSE;
                    }
                    return new Boolean(geometryTrans.contains(geom));

		} catch (Exception e) {
			throw new EvaluatorException(e);
		}
	}

	public String getName() {
		return "contains with geometry";
	}

	public String getSQL() {
		if (geometryWKT == null) {
			try {
				geometryWKT = geometry.convertToWKT();
			} catch (GeometryOperationNotSupportedException e) {
				throw new DataEvaluatorRuntimeException(e);
			} catch (GeometryOperationException e) {
				throw new DataEvaluatorRuntimeException(e);
			}
		}

		return " ST_contains(ST_GeomFromText('" + geometryWKT + "', " + "'"
				+ srs + "'" + "), " + geomName + ") ";
	}

}
