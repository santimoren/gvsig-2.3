/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.mapcontext.rendering.symbols;

import java.awt.Color;
import java.awt.Font;

/**
 * Preferences for symbol configuration and default values.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public interface SymbolPreferences {

	/**
	 * Returns the file extension to use for persisted symbols.
	 * 
	 * @return the symbol file extension
	 */
	String getSymbolFileExtension();

	/**
	 * Sets the file extension to use for persisted symbols.
	 * 
	 * @param extension
	 *            the file extension to use for persisted symbols
	 */
	void setSymbolFileExtension(String extension);

	/**
	 * Returns the default color to use for symbols.
	 * 
	 * @return the symbol default color
	 */
	Color getDefaultSymbolColor();

	/**
	 * Sets the default color to use for symbols.
	 * 
	 * @param defaultSymbolColor
	 *            the symbol default color
	 */
	void setDefaultSymbolColor(Color defaultSymbolColor);

	/**
	 * Sets the default color for symbols to the original value.
	 */
	void resetDefaultSymbolColor();

	/**
	 * Returns the default fill color to use for symbols.
	 * 
	 * @return the default fill color
	 */
	Color getDefaultSymbolFillColor();

	/**
	 * Sets the default fill color to use for symbols.
	 * 
	 * @param defaultSymbolFillColor
	 *            the default fill color
	 */
	void setDefaultSymbolFillColor(Color defaultSymbolFillColor);

	/**
	 * Sets the default fill color for symbols to the original value.
	 */
	void resetDefaultSymbolFillColor();

	/**
	 * Returns if the default symbol fill color has been generated in a aleatory
	 * way.
	 * 
	 * @return if the default symbol fill color is aleatory
	 */
	boolean isDefaultSymbolFillColorAleatory();

	/**
	 * Sets if the default symbol fill color has been generated in a aleatory
	 * way.
	 * 
	 * @param defaultSymbolFillColorAleatory
	 *            if the default symbol fill color is aleatory
	 */
	void setDefaultSymbolFillColorAleatory(
			boolean defaultSymbolFillColorAleatory);

	/**
	 * Sets if the default symbol fill color is aleatory to the default value.
	 */
	void resetDefaultSymbolFillColorAleatory();

	/**
	 * Returns the default font to use for symbols.
	 * 
	 * @return the default symbol font
	 */
	Font getDefaultSymbolFont();

	/**
	 * Sets the default font to use for symbols.
	 * 
	 * @param defaultSymbolFont
	 *            the default symbol font
	 */
	void setDefaultSymbolFont(Font defaultSymbolFont);

	/**
	 * Sets the default font to use for symbols to the original value.
	 */
	void resetDefaultSymbolFont();

	/**
	 * Returns the path to the symbol library, where the persisted symbols can
	 * be found.
	 * 
	 * @return the path to the symbol library
	 */
	String getSymbolLibraryPath();

	/**
	 * Sets the path to the symbol library, where the persisted symbols can be
	 * found.
	 * 
	 * @param symbolLibraryPath
	 *            the path to the symbol library
	 */
	void setSymbolLibraryPath(String symbolLibraryPath);

	/**
	 * Sets the path to the symbol library to the original value.
	 */
	void resetSymbolLibraryPath();

	/**
	 * Returns the unit that will be used when creating new symbols with
	 * Cartographic support.
	 * 
	 * @return the default cartographic support measure unit
	 * @see CartographicSupport#getUnit()
	 */
	int getDefaultCartographicSupportMeasureUnit();

	/**
	 * Sets the unit that will be used when creating new symbols with
	 * Cartographic support.
	 * 
	 * @param defaultCartographicSupportMeasureUnit
	 *            the default cartographic support measure unit
	 * @see CartographicSupport#setUnit()
	 */
	void setDefaultCartographicSupportMeasureUnit(
			int defaultCartographicSupportMeasureUnit);

	/**
	 * Returns the reference system that will be used when creating new symbols
	 * with Cartographic support.
	 * 
	 * @return the cartographic reference system
	 * @see CartographicSupport#PAPER
	 * @see CartographicSupport#WORLD
	 */
	int getDefaultCartographicSupportReferenceSystem();

	/**
	 * Sets the reference system that will be used when creating new symbols
	 * with Cartographic support.
	 * 
	 * @param defaultCartographicSupportReferenceSystem
	 *            the cartographic reference system
	 * @see CartographicSupport#PAPER
	 * @see CartographicSupport#WORLD
	 */
	void setDefaultCartographicSupportReferenceSystem(
			int defaultCartographicSupportReferenceSystem);
}
