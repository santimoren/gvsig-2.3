/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend.styling;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.tools.task.Cancellable;

/**
 * This interface enables support for labeling an object such as a layer, but also others, if any. It gives support for detecting if labeling is being applied (<code>isLabeled()</code> and <code>setIsLabeled()</code>), to manage the labeling strategy (<code>getLabelingStrategy()</code> and <code>setLabelingStrategy</code>). The labeling is performed by the <code>drawLabels(..)</code> method.
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface ILabelable {
	/**
	 * Returns <b>true</b> if labels are drawn, or <b>false</b> otherwise.
	 * @return <b>boolean</b> telling if labels are drawn
	 */
	public boolean isLabeled();

	/**
	 * Enables or disables the label drawing.
	 * @param isLabeled, if true then labels will be drawn
	 */
	public void setIsLabeled(boolean isLabeled);

	/**
	 * Returns the current labeling strategy
	 * @return ILabelingStrategy
	 * @see ILabelingStrategy
	 */
	public ILabelingStrategy getLabelingStrategy();

	/**
	 * Sets the new labeling strategy. Changes on the results will take effect next time the drawLabels(...) method is invoked.
	 * @param  strategy
	 * @uml.property  name="labelingStrategy"
	 */
	public void setLabelingStrategy(ILabelingStrategy strategy);

    /**
     * Causes the labels to be drawn. The policy of process is determined by the
     * LabelingStrategy previously set.
     * 
     * @param image
     * @param g
     * @param viewPort
     * @param cancel
     * @param scale
     * @param dpi
     * @throws ReadException
     */
	public void drawLabels(BufferedImage image, Graphics2D g, ViewPort viewPort, Cancellable cancel, double scale, double dpi) throws ReadException;
	public void printLabels(Graphics2D g, ViewPort viewPort,
    		Cancellable cancel, double scale, PrintAttributes properties) throws ReadException;
}
