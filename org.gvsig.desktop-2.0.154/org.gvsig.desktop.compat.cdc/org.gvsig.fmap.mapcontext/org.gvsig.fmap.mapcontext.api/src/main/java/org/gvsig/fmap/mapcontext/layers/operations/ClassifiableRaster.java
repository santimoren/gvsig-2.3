/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.operations;

import org.gvsig.fmap.mapcontext.rendering.legend.IRasterLegend;


/**
 * <p>Interface that all raster layers that can be classifiable and can have associated a legend,
 *  must implement.</p>
 *
 * @see Classifiable
 */
public interface ClassifiableRaster extends Classifiable {
	/**
	 * <p>Sets the layer's legend as a raster legend.</p>
	 *
	 * @param r the legend with raster data
	 *
	 * @throws DriverException if fails the driver used in this method.
	 */
	void setLegend(IRasterLegend r);

	/**
	 * <p>Returns the type of this shape.</p>
	 * <p>All geometry types are defined in <code>Geometry.TYPES</code>, and their shape equivalent in <code>FConstant</code>.</p>
	 *
	 * @return the type of this shape
	 *
	 * @see Geometry
	 * @see Constant
	 */
	int getShapeType();
}
