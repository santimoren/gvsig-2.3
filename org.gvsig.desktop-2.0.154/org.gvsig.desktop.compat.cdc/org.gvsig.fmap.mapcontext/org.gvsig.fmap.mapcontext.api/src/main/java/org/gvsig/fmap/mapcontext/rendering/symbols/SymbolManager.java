/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.rendering.symbols;

import java.awt.Color;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.MapContextRuntimeException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.task.CancellableTask;
import org.gvsig.tools.visitor.Visitor;

/**
 * Symbols management: creation, registration, etc.
 * 
 * @author gvSIG team
 */
public interface SymbolManager {

    public static final String LEGEND_FILE_EXTENSION = ".gvsleg";
    public static final String LABELINGSTRATEGY_FILE_EXTENSION = ".gvslab";
    
	/**
	 * Loads the symbols persisted into a folder. It loads the symbols through
	 * the use of the current {@link PersistenceManager}, loading all files
	 * found into the folder.
	 * 
	 * @param folder
	 *            to load the persisted symbols from
	 * @return the list of loaded symbols
	 * @throws SymbolException
	 *             if there is an error loading the symbols
	 */
	ISymbol[] loadSymbols(File folder) throws SymbolException;

	/**
	 * Loads the symbols persisted into a folder. It loads the symbols through
	 * the use of the current {@link PersistenceManager}, loading the files
	 * found into the folder which apply to the provided filter.
	 * 
	 * @param folder
	 *            to load the persisted symbols from
	 * @param filter
	 *            to apply to know which files to load
	 * @return the list of loaded symbols
	 * @throws SymbolException
	 *             if there is an error loading the symbols
	 */
	ISymbol[] loadSymbols(File folder, FileFilter filter)
			throws SymbolException;
	
	public CancellableTask loadSymbols(File folder, FileFilter filter, Visitor visitor);
	/**
	 * Persists a {@link ISymbol} into the given folder, with the given file
	 * name.
	 * 
	 * @param symbol
	 *            to persist
	 * @param fileName
	 *            of the file to create
	 * @param folder
	 *            where to create the file
	 * @throws SymbolException
	 *             if there is an error persisting the symbol, or the file to
	 *             create already exists
	 */
	void saveSymbol(ISymbol symbol, String fileName, File folder)
			throws SymbolException;

	/**
	 * Persists a {@link ISymbol} into the given folder, with the given file
	 * name.
	 * 
	 * @param symbol
	 *            to persist
	 * @param fileName
	 *            of the file to create
	 * @param folder
	 *            where to create the file
	 * @param overwrite
	 *            what to do if the file to create already exists
	 * @throws SymbolException
	 *             if there is an error persisting the symbol, or the file to
	 *             create already exists and overwrite is false
	 */
	void saveSymbol(ISymbol symbol, String fileName, File folder,
			boolean overwrite) throws SymbolException;

	/**
	 * Returns the current {@link SymbolPreferences}.
	 * 
	 * @return the current {@link SymbolPreferences}
	 */
	SymbolPreferences getSymbolPreferences();

	/**
	 * Creates a new {@link ISymbol}.
	 * 
	 * @param symbolName
	 *            the name of the symbol to create
	 * @return a new {@link ISymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	ISymbol createSymbol(String symbolName) throws MapContextRuntimeException;

	/**
	 * Creates a new {@link ISymbol} which can be used to render the given
	 * {@link Geometry} type.
	 * 
	 * @param geomType
	 *            the {@link Geometry} type to render
	 * @return a new {@link ISymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	ISymbol createSymbol(int geomType) throws MapContextRuntimeException;

	/**
	 * Creates a new {@link ISymbol} with the given {@link Color}.
	 * 
	 * @param symbolName
	 *            the name of the symbol to create
	 * @param color
	 *            the color for the symbol
	 * @return a new {@link ISymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	ISymbol createSymbol(String symbolName, Color color)
			throws MapContextRuntimeException;

	/**
	 * Creates a new {@link ISymbol} which can be used to render the given
	 * {@link Geometry} type, with the given {@link Color}.
	 * 
	 * @param geomType
	 *            the {@link Geometry} type to render
	 * @param color
	 *            the color for the symbol
	 * @return a new {@link ISymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	ISymbol createSymbol(int geomType, Color color)
			throws MapContextRuntimeException;

	/**
	 * Creates a new {@link IMultiLayerSymbol}.
	 * 
	 * @param symbolName
	 *            the name of the symbol to create
	 * @return a new {@link IMultiLayerSymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	IMultiLayerSymbol createMultiLayerSymbol(String symbolName)
			throws MapContextRuntimeException;

	/**
	 * Creates a new {@link IMultiLayerSymbol} which can be used to render the
	 * given {@link Geometry} type.
	 * 
	 * @param geomType
	 *            the {@link Geometry} type to render
	 * @return a new {@link IMultiLayerSymbol}
	 * @throws MapContextRuntimeException
	 *             if there is an error creating the symbol
	 */
	IMultiLayerSymbol createMultiLayerSymbol(int geomType)
			throws MapContextRuntimeException;

	/**
	 * Registers a {@link ISymbol} implementation class with a name. This way
	 * the symbol class is not related to any geometry type, so it can be
	 * created only through the {@link #createSymbol(String)} method.
	 * 
	 * @param symbolName
	 *            the symbol name
	 * @param symbolClass
	 *            the symbol implementation class
	 * @throws MapContextRuntimeException
	 *             if the provided class does not implement the ISymbol
	 *             interface
	 */
	void registerSymbol(String symbolName, Class symbolClass)
			throws MapContextRuntimeException;

	/**
	 * Registers a {@link ISymbol} implementation class with a name and a list
	 * of geometry types which the symbol is able to render.
	 * 
	 * @param symbolName
	 *            the symbol name
	 * @param geomTypes
	 *            the list of geometry types the symbol is able to render
	 * @param symbolClass
	 *            the symbol implementation class
	 * @throws MapContextRuntimeException
	 *             if the provided class does not implement the ISymbol
	 *             interface
	 */
	void registerSymbol(String symbolName, int[] geomTypes, Class symbolClass)
			throws MapContextException;

	/**
	 * Registers a {@link IMultiLayerSymbol} implementation class with a name.
	 * This way the symbol class is not related to any geometry type, so it can
	 * be created only through the {@link #createMultiLayerSymbol(String)}
	 * method.
	 * 
	 * @param symbolName
	 *            the symbol name
	 * @param symbolClass
	 *            the symbol implementation class
	 * @throws MapContextRuntimeException
	 *             if the provided class does not implement the
	 *             {@link IMultiLayerSymbol} interface
	 */
	void registerMultiLayerSymbol(String symbolName, Class symbolClass)
			throws MapContextRuntimeException;

	/**
	 * Registers a {@link IMultiLayerSymbol} implementation class with a name
	 * and a list of geometry types which the symbol is able to render.
	 * 
	 * @param symbolName
	 *            the symbol name
	 * @param geomTypes
	 *            the list of geometry types the symbol is able to render
	 * @param symbolClass
	 *            the symbol implementation class
	 * @throws MapContextRuntimeException
	 *             if the provided class does not implement the
	 *             {@link IMultiLayerSymbol} interface
	 */
	void registerMultiLayerSymbol(String symbolName, int[] geomTypes,
			Class symbolClass) throws MapContextRuntimeException;

	/**
	 * Returns an instance of {@link IWarningSymbol}.
	 * 
	 * TODO: revisar el uso de esto, a ver si es necesario o se puede ocultar
	 * dentro de la implementación.
	 * 
	 * @param message
	 * @param symbolDesc
	 * @param symbolDrawExceptionType
	 * @return the warning symbol
	 * @throws MapContextRuntimeException
	 */
	IWarningSymbol getWarningSymbol(String message, String symbolDesc,
			int symbolDrawExceptionType) throws MapContextRuntimeException;
	
	void setSymbolPreferences(SymbolPreferences symbolPreferences);
        
        public List getSymbolLibraryNames();
        
        public ISymbol getSymbol(String libraryName, String symbolID) throws SymbolException ;

}