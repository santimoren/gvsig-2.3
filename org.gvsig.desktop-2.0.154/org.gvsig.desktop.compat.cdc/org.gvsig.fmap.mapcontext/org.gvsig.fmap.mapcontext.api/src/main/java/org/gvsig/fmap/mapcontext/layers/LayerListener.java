/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;



/**
 * <p><code>LayerListener</code> defines the interface for an object that listens
 *  to changes in a layer.</p>
 */
public interface LayerListener {
	/**
	 * <p>Called when the visibility of a layer has changed.</p>
	 * 
	 * @param e a visibility changed layer event object
	 */
	public void visibilityChanged(LayerEvent e);

	/**
	 * <p>Called when the activation of a layer has changed.</p>
	 * 
	 * @param e an activation changed layer event object
	 */
	public void activationChanged(LayerEvent e);

	/**
	 * <p>Called when the name of a layer has changed.</p>
	 * 
	 * @param e a name changed layer event object
	 */
	public void nameChanged(LayerEvent e);

	/**
	 * <p>Called when the edition of a layer has changed.</p>
	 * 
	 * @param e an edition changed layer event object
	 */
	public void editionChanged(LayerEvent e);

	/**
	 * <p>
	 * Called when a draw value of a layer has changed.
	 * </p>
	 * 
	 * @param e
	 *            an edition changed layer event object
	 */
	public void drawValueChanged(LayerEvent e);
}
