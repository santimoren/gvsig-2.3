/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

/**
 * 
 * @deprecated
 *
 */
public class MappingAnnotation implements Persistent {

	private int columnText = -1;
	private int columnRotate = -1;
	private int columnColor = -1;
	private int columnHeight = -1;
	private int columnTypeFont = -1;
	private int columnStyleFont = -1;

	public MappingAnnotation() {
		
	}
	
	public int getColumnColor() {
		return columnColor;
	}

	public void setColumnColor(int columnColor) {
		this.columnColor = columnColor;
	}

	public int getColumnHeight() {
		return columnHeight;
	}

	public void setColumnHeight(int columnHeight) {
		this.columnHeight = columnHeight;
	}

	public int getColumnRotate() {
		return columnRotate;
	}

	public void setColumnRotate(int columnRotate) {
		this.columnRotate = columnRotate;
	}

	public int getColumnStyleFont() {
		return columnStyleFont;
	}

	public void setColumnStyleFont(int columnStyleFont) {
		this.columnStyleFont = columnStyleFont;
	}

	public int getColumnText() {
		return columnText;
	}

	public void setColumnText(int columnText) {
		this.columnText = columnText;
	}

	public int getColumnTypeFont() {
		return columnTypeFont;
	}

	public void setColumnTypeFont(int columnTypeFont) {
		this.columnTypeFont = columnTypeFont;
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {

		columnText = state.getInt("columnText");
		columnRotate = state.getInt("columnRotate");
		columnColor = state.getInt("columnColor");
		columnHeight = state.getInt("columnHeight");
		columnTypeFont = state.getInt("columnTypeFont");
		columnStyleFont = state.getInt("columnStyleFont");
	}

	public void saveToState(PersistentState state) throws PersistenceException {

		state.set("columnText", columnText);
		state.set("columnRotate", columnRotate);
		state.set("columnColor", columnColor);
		state.set("columnHeight", columnHeight);
		state.set("columnTypeFont", columnTypeFont);
		state.set("columnStyleFont", columnStyleFont);

	}
	
	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		DynStruct definition = manager.addDefinition(
				MappingAnnotation.class,
				"MappingAnnotation",
				"MappingAnnotation Persistence definition",
				null, 
				null
		);
		definition.addDynFieldInt("columnText").setMandatory(true);
		definition.addDynFieldInt("columnRotate").setMandatory(true);
		definition.addDynFieldInt("columnColor").setMandatory(true);
		definition.addDynFieldInt("columnHeight").setMandatory(true);
		definition.addDynFieldInt("columnTypeFont").setMandatory(true);
		definition.addDynFieldInt("columnStyleFont").setMandatory(true);
	}


}
