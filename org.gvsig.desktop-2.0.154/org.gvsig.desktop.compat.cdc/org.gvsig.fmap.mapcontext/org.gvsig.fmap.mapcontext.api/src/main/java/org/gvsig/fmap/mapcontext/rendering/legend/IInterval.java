/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend;

import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.Persistent;

public interface IInterval extends Persistent, Cloneable {

	/**
	 * Returns if the given value is included into the interval.
	 * 
	 * @param v
	 *            value to check. Must be instance of {@link Number}
	 * 
	 * @return if the given value is included into the interval
	 */
	boolean isInInterval(Object v);

	/**
	 * Returns the interval minimum included value.
	 * 
	 * @return the minimum value
	 */
	double getMin();

	/**
	 * Returns the interval maximum included value.
	 * 
	 * @return the maximum value
	 */
	double getMax();

}