/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextRuntimeException;
import org.gvsig.fmap.mapcontext.Messages;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.operations.ComposedLayer;
import org.gvsig.fmap.mapcontext.layers.operations.InfoByPoint;
import org.gvsig.fmap.mapcontext.layers.operations.LayerCollection;
import org.gvsig.fmap.mapcontext.layers.operations.LayerNotFoundInCollectionException;
import org.gvsig.fmap.mapcontext.layers.operations.LayersVisitable;
import org.gvsig.fmap.mapcontext.layers.operations.LayersVisitor;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelable;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dispose.Disposable;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.dynobject.impl.MultiDynObjectSet;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;
import org.gvsig.tools.visitor.VisitCanceledException;
import org.gvsig.tools.visitor.Visitor;

/**
 * <p>
 * Represents a generic collection of layers, that can be represented as a node
 * in a tree of nodes of layers.</p>
 *
 * <p>
 * Adapts the basic functionality implemented for a layer in the abstract class
 * <code>FLyrDefault</code>, to a collection of layers, implementing, as well,
 * specific methods for this kind of object, defined in the interfaces
 * <code>VectorialData</code>, <code>LayerCollection</code>, and
 * <code>InfoByPoint</code>.</p>
 *
 * @see FLyrDefault
 */
public class FLayers extends FLyrDefault implements LayerCollection,
        InfoByPoint, List<FLayer> {

    /**
     * List with all listeners registered for this kind of node.
     */
    protected ArrayList layerCollectionListeners = null;

    /**
     * A synchronized list with the layers.
     */
    protected List<FLayer> layers = null;

    protected MapContext fmap;

    /**
     * Useful for debug the problems during the implementation.
     */
    private static final Logger logger = LoggerFactory.getLogger(FLayers.class);

    public FLayers() {
        super();
        layerCollectionListeners = new ArrayList();
        layers = Collections.synchronizedList(new ArrayList());
    }
    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#addLayerCollectionListener(com.iver.cit.gvsig.fmap.layers.LayerCollectionListener)
     */

    @Override
    public void addLayerCollectionListener(LayerCollectionListener listener) {
        if (!layerCollectionListeners.contains(listener)) {
            layerCollectionListeners.add(listener);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#setAllVisibles(boolean)
     */
    @Override
    public void setAllVisibles(boolean visible) {
        FLayer lyr;

        for (int i = 0; i < layers.size(); i++) {
            lyr = ((FLayer) layers.get(i));
            lyr.setVisible(visible);

            if (lyr instanceof LayerCollection) {
                ((LayerCollection) lyr).setAllVisibles(visible);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#removeLayerCollectionListener(com.iver.cit.gvsig.fmap.layers.LayerCollectionListener)
     */
    @Override
    public void removeLayerCollectionListener(LayerCollectionListener listener) {
        layerCollectionListeners.remove(listener);
    }

    /**
     * Adds a layer on an specified position in this node.
     *
     * @param pos position in the inner list where the layer will be added
     * @param layer a layer
     */
    private void doAddLayer(int pos, FLayer layer) {
        layers.add(pos, layer);
        ToolsLocator.getDisposableManager().bind(layer);
        layer.setParentLayer(this);
        IProjection layerProj = layer.getProjection();
        if (layerProj != null && fmap != null) {
            IProjection mapContextProj = fmap.getProjection();
            // TODO REVISAR ESTO !!!!
            // Esta condici�n puede que no fuese exacta para todos los casos
            if (!layerProj.getAbrev().equals(mapContextProj.getAbrev())) {
                ICoordTrans ct = layerProj.getCT(mapContextProj);
                layer.setCoordTrans(ct);
            } else {
                layer.setCoordTrans(null);
            }
        }
        this.updateDrawVersion();
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#addLayer(com.iver.cit.gvsig.fmap.layers.FLayer)
     */
    @Override
    public void addLayer(FLayer layer) {

        MapContext mco = this.getMapContext();

        if (mco != null) {
            /*
             * Get order manager from map context
             */
            int position = mco.getOrderManager().getPosition(this, layer);
            addLayer(position, layer);
        } else {
            /*
             * This happens when FLayers object is not in a
             * map context, so no order manager is available.
             */
            addLayer(layers.size(), layer);
        }
    }

    /**
     * Adds a layer in an specified position in this node.
     *
     * @param pos
     * @param layer a layer
     */
    public void addLayer(int pos, FLayer layer) {
        try {
            //Notificamos a la capa que va a ser a�adida
            if (layer instanceof FLyrDefault) {
                ((FLyrDefault) layer).wakeUp();
            }

            if (layer instanceof FLayers) {
                FLayers layers = (FLayers) layer;
                if( fmap != null ) {
                    fmap.addAsCollectionListener(layers);
                }
            }
            callLayerAdding(LayerCollectionEvent.createLayerAddingEvent(layer));

            doAddLayer(pos, layer);

            callLayerAdded(LayerCollectionEvent.createLayerAddedEvent(layer));
        } catch (CancelationException e) {
            logger.warn(e.getMessage());
        } catch (LoadLayerException e) {
            layer.setAvailable(false);
            layer.addError(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#moveTo(int, int)
     */
    @Override
    public void moveTo(int from, int to) throws CancelationException {
        int newfrom = layers.size() - from - 1;
        int newto = layers.size() - to - 1;
        if (newfrom < 0 || newfrom >= layers.size() || newto < 0 || newto >= layers.size()) {
            return;
        }
        FLayer aux = (FLayer) layers.get(newfrom);
        callLayerMoving(LayerPositionEvent.createLayerMovingEvent(aux, newfrom, newto));
        layers.remove(newfrom);
        layers.add(newto, aux);
        this.updateDrawVersion();
        callLayerMoved(LayerPositionEvent.createLayerMovedEvent(aux, newfrom, newto));
    }

    /**
     * Removes an inner layer.
     *
     * @param lyr a layer
     */
    private void doRemoveLayer(FLayer lyr) {
        layers.remove(lyr);
        lyr.dispose();
        this.updateDrawVersion();
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#removeLayer(com.iver.cit.gvsig.fmap.layers.FLayer)
     */
    @Override
    public void removeLayer(FLayer lyr) throws CancelationException {
        callLayerRemoving(LayerCollectionEvent.createLayerRemovingEvent(lyr));
        doRemoveLayer(lyr);
        callLayerRemoved(LayerCollectionEvent.createLayerRemovedEvent(lyr));
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#removeLayer(int)
     */
    @Override
    public void removeLayer(int idLayer) {
        FLayer lyr = (FLayer) layers.get(idLayer);
        callLayerRemoving(LayerCollectionEvent.createLayerRemovingEvent(lyr));
        this.doRemoveLayer(lyr);
//		layers.remove(idLayer);
//		this.updateDrawVersion();
        callLayerRemoved(LayerCollectionEvent.createLayerRemovedEvent(lyr));
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#removeLayer(java.lang.String)
     */
    @Override
    public void removeLayer(String layerName) {
        FLayer lyr;

        for (int i = 0; i < layers.size(); i++) {
            lyr = ((FLayer) layers.get(i));

            if (lyr.getName().compareToIgnoreCase(layerName) == 0) {
                removeLayer(i);

                break;
            }
        }
    }

    /**
     * Replace a layer identified by its name, by another.
     *
     * @param layerName the name of the layer to be replaced
     * @param layer the new layer
     * @throws org.gvsig.fmap.mapcontext.exceptions.LoadLayerException
     * @deprecated use {@link FLayers#replaceLayer(FLayer, FLayer)}
     */
    public void replaceLayer(String layerName, FLayer layer) throws LoadLayerException {
        replaceLayer(getLayer(layerName), layer);
    }

    /**
     * Replace a layer by another layer. It search recursively by all the
     * ILayerCollection nodes
     *
     * @param layer the layer to be replaced
     * @param newLayer the new layer
     * @throws org.gvsig.fmap.mapcontext.exceptions.LoadLayerException
     */
    public void replaceLayer(FLayer layer, FLayer newLayer) throws LoadLayerException {
        replaceLayer(this, layer, newLayer);
    }

    /**
     * Replace a layer by other layer. It search recursively by all the
     * ILayerCollection nodes
     *
     * @param parentLayer the parent layer
     * @param layer the layer to be replaced
     * @param newLayer the new layer
     * @throws LoadLayerException
     */
    private void replaceLayer(FLayers parentLayer, FLayer layer, FLayer newLayer) throws LoadLayerException {
        FLayer lyr;
        for (int i = 0; i < parentLayer.getLayersCount(); i++) {
            lyr = ((FLayer) parentLayer.getLayer(i));
            if (lyr.equals(layer)) {
                parentLayer.removeLayer(i);
                parentLayer.addLayer(i, newLayer);
                break;
            }
            if (lyr instanceof LayerCollection) {
                replaceLayer((FLayers) lyr, layer, newLayer);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#getVisibles()
     */
    @Override
    public FLayer[] getVisibles() {
        ArrayList array = new ArrayList();
        LayersIterator iter = new LayersIterator(this) {
            @Override
            public boolean evaluate(FLayer layer) {
                return layer.isVisible();
            }

        };

        while (iter.hasNext()) {
            array.add(iter.nextLayer());
        }

        return (FLayer[]) array.toArray(new FLayer[0]);
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#getLayer(int)
     */
    @Override
    public FLayer getLayer(int index) {
        return (FLayer) layers.get(index);
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#getLayer(java.lang.String)
     */
    @Override
    public FLayer getLayer(String layerName) {
        for (int i = 0; i < layers.size(); i++) {
            FLayer lyr = ((FLayer) layers.get(i));
            if (lyr.getName().compareToIgnoreCase(layerName) == 0) {
                return lyr;
            }
            if (lyr instanceof FLayers) {
                List layerList = toPlainList(lyr);
                for (int j = 0; j < layerList.size(); j++) {
                    FLayer lyr2 = ((FLayer) layerList.get(j));
                    if (lyr2.getName().compareToIgnoreCase(layerName) == 0) {
                        return lyr2;
                    }
                }
            }
        }
        return null;
    }

    private List toPlainList(FLayer layer) {
        return toPlainList(layer, new ArrayList());
    }

    private List toPlainList(FLayer layer, List result) {
        if (layer instanceof FLayers) {
            FLayers layerGroup = (FLayers) layer;
            for (int i = 0; i < layerGroup.getLayersCount(); i++) {
                toPlainList(layerGroup.getLayer(i), result);
            }
        } else {
            result.add(layer);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#getLayersCount()
     */
    @Override
    public int getLayersCount() {
        return layers.size();
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLayer#draw(java.awt.image.BufferedImage, java.awt.Graphics2D, com.iver.cit.gvsig.fmap.ViewPort, com.iver.utiles.swing.threads.Cancellable, double)
     */
    @Override
    public void draw(BufferedImage image, Graphics2D g, ViewPort viewPort,
            Cancellable cancel, double scale) throws ReadException {
        // FIXME Arreglar este error
        throw new RuntimeException("Esto no deberia de llamarse");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.iver.cit.gvsig.fmap.layers.FLayer#print(java.awt.Graphics2D,
     * com.iver.cit.gvsig.fmap.ViewPort,
     * com.iver.utiles.swing.threads.Cancellable, double,
     * javax.print.attribute.PrintAttributes)
     */
    @Override
    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
            double scale, PrintAttributes properties)
            throws ReadException {
        throw new RuntimeException("No deberia pasar por aqui");
    }

    public void print_old(Graphics2D g, ViewPort viewPort, Cancellable cancel,
            double scale, PrintAttributes properties)
            throws ReadException {
        this.print_old(g, viewPort, cancel, scale, properties, null);
    }

    /**
     * <p>
     * Checks all layers (each one as a sub-node of this node <i>collection of
     * layers</i>) of this collection and draws their requested properties. If a
     * node is a group of layers (<code>ComposedLayer</code>), executes it's
     * drawn.</p>
     *
     * <p>
     * All nodes which could group with the composed layer <code>group</code>,
     * will be drawn together. And once the <code>
     * group</code> is drawn, will be set to <code>null</code> if hasn't a
     * parent layer.</p>
     *
     * <p>
     * The particular implementation depends on the kind of each layer and
     * composed layer. And this process can be cancelled at any time by the
     * shared object <code>cancel</code>.</p>
     *
     * <p>
     * According the print quality, labels will be printed in different
     * resolution:
     * <ul>
     * <li><b>PrintQuality.DRAFT</b>: 72 dpi (dots per inch).</li>
     * <li><b>PrintQuality.NORMAL</b>: 300 dpi (dots per inch).</li>
     * <li><b>PrintQuality.HIGH</b>: 600 dpi (dots per inch).</li>
     * </ul>
     * </p>
     *
     * @param g for rendering 2-dimensional shapes, text and images on the
     * Java(tm) platform
     * @param viewPort the information for drawing the layers
     * @param cancel shared object that determines if this layer can continue
     * being drawn
     * @param scale the scale of the view. Must be between
     * {@linkplain FLayer#getMinScale()} and {@linkplain FLayer#getMaxScale()}.
     * @param properties properties that will be print
     * @param group a composed layer pending to paint; if this parameter is
     * <code>null</code>, the composed layer
     *
     * @return <code>null</code> if the layers in <code>group</code> had been
     * drawn or were <code>null</code>; otherwise, the <code>group</code>
     * @throws org.gvsig.fmap.dal.exception.ReadException
     *
     * @see FLayer#print(Graphics2D, ViewPort, Cancellable, double,
     * PrintAttributes)
     */
    public ComposedLayer print_old(Graphics2D g, ViewPort viewPort, Cancellable cancel, double scale, PrintAttributes properties, ComposedLayer group)
            throws ReadException {
        double dpi = 72;

        int resolution = properties.getPrintQuality();
        if (resolution == PrintAttributes.PRINT_QUALITY_NORMAL) {
            dpi = 300;
        } else if (resolution == PrintAttributes.PRINT_QUALITY_HIGH) {
            dpi = 600;
        } else if (resolution == PrintAttributes.PRINT_QUALITY_DRAFT) {
            dpi = 72;
        }

        // TODO: A la hora de imprimir, isWithinScale falla, porque est�
        // calculando la escala en pantalla, no para el layout.
        // Revisar esto.
        // TODO: We have to check when we have to call the drawLabels method when exists a ComposedLayer group.
        for (int i = 0; i < layers.size(); i++) {
            FLayer lyr = (FLayer) layers.get(i);
            if (!lyr.isVisible() || !lyr.isWithinScale(scale)) {
                continue;
            }

            try {

                // Checks for draw group (ComposedLayer)
                if (group != null) {
                    if (lyr instanceof FLayers) {
                        group = ((FLayers) lyr).print_old(g, viewPort, cancel, scale, properties, group);
                    } else {
                        // If layer can be added to the group, does it
                        if (lyr instanceof ILabelable
                                && ((ILabelable) lyr).isLabeled()
                                && ((ILabelable) lyr).getLabelingStrategy() != null
                                && ((ILabelable) lyr).getLabelingStrategy().shouldDrawLabels(scale)) {
                            group.add(lyr);
                        } else {
                            // draw the 'pending to draw' layer group
                            group.print(g, viewPort, cancel, scale, properties);

                            // gets a new group instance
                            if (lyr instanceof ILabelable
                                    && ((ILabelable) lyr).isLabeled()
                                    && ((ILabelable) lyr).getLabelingStrategy() != null
                                    && ((ILabelable) lyr).getLabelingStrategy().shouldDrawLabels(scale)) {
                                group = lyr.newComposedLayer();
                            } else {
                                group = null;
                            }
                            // if layer hasn't group, draws it inmediately
                            if (group == null) {
                                if (lyr instanceof FLayers) {
                                    group = ((FLayers) lyr).print_old(g, viewPort, cancel, scale, properties, group);
                                } else {
                                    lyr.print(g, viewPort, cancel, scale, properties);
                                    if (lyr instanceof ILabelable
                                            && ((ILabelable) lyr).isLabeled()
                                            && ((ILabelable) lyr).getLabelingStrategy() != null
                                            && ((ILabelable) lyr).getLabelingStrategy().shouldDrawLabels(scale)) {
                                        ILabelable lLayer = (ILabelable) lyr;
                                        lLayer.drawLabels(null, g, viewPort, cancel, scale, dpi);
                                    }
                                }
                            } else {
                                // add the layer to the group
                                group.setMapContext(fmap);
                                group.add(lyr);

                            }

                        }
                    }
                } else {
                    // gets a new group instance
                    group = lyr.newComposedLayer();
                    // if layer hasn't group, draws it inmediately
                    if (group == null) {
                        if (lyr instanceof FLayers) {
                            group = ((FLayers) lyr).print_old(g, viewPort, cancel, scale, properties, group);
                        } else {
                            lyr.print(g, viewPort, cancel, scale, properties);
                            if (lyr instanceof ILabelable && ((ILabelable) lyr).isLabeled()) {
                                ILabelable lLayer = (ILabelable) lyr;

                                lLayer.drawLabels(null, g, viewPort, cancel, scale, dpi);
                            }
                        }
                    } else {
                        // add the layer to the group
                        group.setMapContext(fmap);
                        group.add(lyr);

                    }
                }

            } catch (Exception e) {
                String mesg = Messages.getString("error_printing_layer") + " " + lyr.getName() + ": " + e.getMessage();
                if( fmap!=null ) {
                    fmap.addLayerError(mesg);
                }
                logger.error(mesg, e);
            }

        }

        if (group != null && this.getParentLayer() == null) {
            //si tenemos un grupo pendiente de pintar, pintamos
            group.print(g, viewPort, cancel, scale, properties);
            group = null;

        }
        return group;
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLayer#getFullExtent()
     */
    @Override
    public Envelope getFullEnvelope() {
        Envelope rAux = null;
        boolean first = true;

        for (Iterator iter = layers.iterator(); iter.hasNext();) {
            FLayer capa = (FLayer) iter.next();
            try {
                if (first) {
                    rAux = (Envelope) capa.getFullEnvelope().clone();
                    first = false;
                } else {
                    rAux.add(capa.getFullEnvelope());
                }
            } catch (Exception e) {
                logger.warn("Can't calculate the envelope of the layer group '"+this.getName()+"'.",e);
            }
        }

        return rAux;
    }

    /**
     * Notifies all listeners associated to this collection of layers, that
     * another layer is going to be added or replaced in the internal list of
     * layers.
     *
     * @param event a layer collection event with the new layer
     */
    protected void callLayerAdding(LayerCollectionEvent event)
            throws CancelationException {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerAdding(event);
        }
    }

    /**
     * Notifies all listeners associated to this collection of layers, that a
     * layer is going to be removed from the internal list of layers.
     *
     * @param event a layer collection event with the layer being removed
     *
     * @throws CancelationException any exception produced during the
     * cancellation of the driver.
     */
    protected void callLayerRemoving(LayerCollectionEvent event)
            throws CancelationException {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerRemoving(event);
        }
    }

    /**
     * Notifies all listeners associated to this collection of layers, that a
     * layer is going to be moved in the internal list of layers.
     *
     * @param event a layer collection event with the layer being moved, and the
     * initial and final positions
     *
     * @throws CancelationException any exception produced during the
     * cancellation of the driver.
     */
    protected void callLayerMoving(LayerPositionEvent event)
            throws CancelationException {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerMoving(event);
        }
    }

    /**
     * Notifies all listeners associated to this collection of layers, that
     * another layer has been added or replaced in the internal list of layers.
     *
     * @param event a layer collection event with the new layer
     */
    protected void callLayerAdded(LayerCollectionEvent event) {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerAdded(event);
        }
    }

    /**
     * Notifies all listeners associated to this collection of layers, that
     * another layer has been removed from the internal list of layers.
     *
     * @param event a layer collection event with the layer removed
     */
    protected void callLayerRemoved(LayerCollectionEvent event) {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerRemoved(event);
        }
    }

    /**
     * Notifies all listeners associated to this collection of layers, that
     * another layer has been moved in the internal list of layers.
     *
     * @param event a layer collection event with the layer moved, and the initial
     * and final positions
     */
    protected void callLayerMoved(LayerPositionEvent event) {
        ArrayList aux = (ArrayList) layerCollectionListeners.clone();
        for (Iterator iter = aux.iterator(); iter.hasNext();) {
            ((LayerCollectionListener) iter.next()).layerMoved(event);
        }
    }

    @Override
    public void saveToState(PersistentState state) throws PersistenceException {

        super.saveToState(state);

        state.set("mapContext", fmap);

        List layersToSave = new ArrayList();
        Iterator iter = this.layers.iterator();
        while (iter.hasNext()) {
            FLayer layer = (FLayer) iter.next();
            if (!layer.isTemporary()) {
                layersToSave.add(layer);
            }
        }
        state.set("layers", layersToSave);
    }

    @Override
    public void loadFromState(PersistentState state) throws PersistenceException {

        super.loadFromState(state);

        setMapContext((MapContext) state.get("mapContext"));
        Iterator iter = state.getIterator("layers");
        while (iter.hasNext()) {
            FLayer item = (FLayer) iter.next();
            layers.add(item);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#getMapContext()
     */
    @Override
    public MapContext getMapContext() {
        return fmap;
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#setAllActives(boolean)
     */
    @Override
    public void setAllActives(boolean active) {
        FLayer lyr;

        for (int i = 0; i < layers.size(); i++) {
            lyr = ((FLayer) layers.get(i));
            lyr.setActive(active);

            if (lyr instanceof LayerCollection) {
                ((LayerCollection) lyr).setAllActives(active);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.layerOperations.LayerCollection#getActives()
     */
    @Override
    public FLayer[] getActives() {
        List ret = new ArrayList();
        LayersIterator it = new LayersIterator(this) {

            @Override
            public boolean evaluate(FLayer layer) {
                return layer.isActive();
            }

        };

        while (it.hasNext()) {
            ret.add(it.next());
        }
        return (FLayer[]) ret.toArray(new FLayer[0]);
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#getMinScale()
     */
    @Override
    public double getMinScale() {
        return -1; // La visibilidad o no la controla cada capa
        // dentro de una colecci�n
    }
    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#getMaxScale()
     */

    @Override
    public double getMaxScale() {
        return -1;
    }
    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#setMinScale(double)
     */

    @Override
    public void setMinScale(double minScale) {
        for (Iterator iter = layers.iterator(); iter.hasNext();) {
            FLayer lyr = (FLayer) iter.next();
            lyr.setMinScale(minScale);
        }
    }
    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#setMaxScale(double)
     */

    @Override
    public void setMaxScale(double maxScale) {
        for (Iterator iter = layers.iterator(); iter.hasNext();) {
            FLayer lyr = (FLayer) iter.next();
            lyr.setMinScale(maxScale);
        }
    }
    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#setActive(boolean)
     */

    @Override
    public void setActive(boolean b) {
        super.setActive(b);
        for (int i = 0; i < layers.size(); i++) {
            ((FLayer) layers.get(i)).setActive(b);
        }
    }
    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#addLayerListener(com.iver.cit.gvsig.fmap.layers.LayerListener)
     */

    @Override
    public boolean addLayerListener(LayerListener o) {
        for (int i = 0; i < layers.size(); i++) {
            ((FLayer) layers.get(i)).addLayerListener(o);
        }
        return true;
    }

    @Override
    public DynObjectSet getInfo(Point p, double tolerance,
            Cancellable cancel) throws LoadLayerException, DataException {
        return getInfo(this.getMapContext().getViewPort().convertToMapPoint(p), tolerance);
    }

    @Override
    public DynObjectSet getInfo(Point p, double tolerance, Cancellable cancel,
            boolean fast) throws LoadLayerException, DataException {
        return getInfo(this.getMapContext().getViewPort().convertToMapPoint(p), tolerance);
    }

    @Override
    public DynObjectSet getInfo(org.gvsig.fmap.geom.primitive.Point p,
            double tolerance) throws LoadLayerException, DataException {
        int i;
        FLayer layer;
        List res = new ArrayList();
        for (i = 0; i < this.layers.size(); i++) {
            layer = (FLayer) layers.get(i);
            if (layer instanceof InfoByPoint) {
                InfoByPoint queryable_layer = (InfoByPoint) layer;
                res.add(queryable_layer.getInfo(p, tolerance));
            }
        }
        DynObjectSet[] innerSets
                = (DynObjectSet[]) res.toArray(new DynObjectSet[res.size()]);
        return new MultiDynObjectSet(innerSets);
    }

    /*
     * (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.layers.FLyrDefault#getTocImageIcon()
     */
    @Override
    public String getTocImageIcon() {
        return "layer-icon-group";
    }

    /**
     * <p>
     * Sets the <code>MapContext</code> that contains this layer node.</p>
     *
     * @param mapContext the <code>MapContext</code> that contains this layer
     * node
     */
    public void setMapContext(MapContext mapContext) {
        this.fmap = mapContext;
    }

    @Override
    public void accept(Visitor visitor) throws BaseException {
        for (int i = 0; i < this.getLayersCount(); i++) {
            FLayer layer = this.getLayer(i);
            try {
                if (layer instanceof LayersVisitable) {
                    ((LayersVisitable) layer).accept(visitor);
                } else {
                    visitor.visit(layer);
                }
            } catch (VisitCanceledException ex) {
                break;
            }
        }
    }

    @Override
    public void accept(LayersVisitor visitor) throws BaseException {
        for (int i = 0; i < this.getLayersCount(); i++) {
            FLayer layer = this.getLayer(i);
            if (layer instanceof LayersVisitable) {
                ((LayersVisitable) layer).accept(visitor);
            } else {
                visitor.visit(layer);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataID()
     */
    @Override
    public Object getMetadataID() throws MetadataException {
        StringBuilder strb = new StringBuilder();
        strb.append("Layers(");
        strb.append(this.getName());
        strb.append("):{");
        Iterator iter = this.layers.iterator();
        while (iter.hasNext()) {
            strb.append(((FLayer) iter.next()).getMetadataID());
            strb.append(",");
        }
        strb.append("}");
        return strb.toString();

    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataChildren()
     */
    @Override
    public Set getMetadataChildren() {
        Set ret = new TreeSet();
        Iterator iter = this.layers.iterator();
        while (iter.hasNext()) {
            ret.add(iter.next());
        }
        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataName()
     */
    @Override
    public String getMetadataName() throws MetadataException {
        StringBuilder strb = new StringBuilder();
        strb.append("Layer Group '");
        strb.append(this.getName());
        strb.append("': {");
        Iterator iter = this.layers.iterator();
        while (iter.hasNext()) {
            strb.append(((FLayer) iter.next()).getMetadataName());
            strb.append(",");
        }
        strb.append("}");
        return strb.toString();
    }

    @Override
    public void beginDraw(Graphics2D g, ViewPort viewPort) {
        if( fmap == null ) {
            return;
        }
        LayerDrawEvent beforeEvent = new LayerDrawEvent(this, g, viewPort, LayerDrawEvent.LAYER_BEFORE_DRAW);
        fmap.fireLayerDrawingEvent(beforeEvent);
    }

    @Override
    public void endDraw(Graphics2D g, ViewPort viewPort) {
        if( fmap == null ) {
            return;
        }
        LayerDrawEvent afterEvent = new LayerDrawEvent(this, g, viewPort, LayerDrawEvent.LAYER_AFTER_DRAW);
        fmap.fireLayerDrawingEvent(afterEvent);
    }

    public static class RegisterPersistence implements Callable {

        @Override
        public Object call() {

            PersistenceManager manager = ToolsLocator.getPersistenceManager();
            DynStruct definition = manager.addDefinition(
                    FLayers.class,
                    "FLayers",
                    "FLayers Persistence definition",
                    null,
                    null
            );
            definition.extend(PersistenceManager.PERSISTENCE_NAMESPACE, "FLyrDefault");

            definition.addDynFieldObject("mapContext").setClassOfValue(MapContext.class).setMandatory(true);
            definition.addDynFieldList("layers").setClassOfItems(FLayer.class).setMandatory(true);

            return Boolean.TRUE;
        }
    }

    @Override
    protected void doDispose() throws BaseException {
        if (layers != null) {
            for (int i = 0; i < layers.size(); i++) {
                dispose((Disposable) layers.get(i));
            }
        }
    }

    @Override
    public void move(FLayer layer, LayerCollection group, int where, FLayer adjoiningLayer) throws LayerNotFoundInCollectionException {

        callLayerRemoving(LayerCollectionEvent.createLayerRemovingEvent(layer));
        group.addLayer(layer, where, adjoiningLayer);
        removeLayer(layer);
        this.updateDrawVersion();
        callLayerRemoved(LayerCollectionEvent.createLayerRemovedEvent(layer));

    }

    public void join(FLayer layer, LayerCollection group) {
        try {
            layers.remove(layer);
            group.addLayer(layer, END, null);
            this.updateDrawVersion();
        } catch (LayerNotFoundInCollectionException e) {
            throw new MapContextRuntimeException(e);
        }
    }

    @Override
    public void move(FLayer layer, LayerCollection group) {
        try {
            move(layer, group, END, null);
        } catch (LayerNotFoundInCollectionException e) {
            throw new MapContextRuntimeException(e);
        }
    }

    @Override
    public void addLayer(FLayer layer, int where, FLayer adjoiningLayer)
            throws LayerNotFoundInCollectionException {

        switch (where) {
            case BEGIN:
                addLayer(0, layer);
                break;
            case BEFORE:
                if (adjoiningLayer != null) {
                    if (this.layers.contains(adjoiningLayer)) {
                        for (int i = 0; i < this.getLayersCount(); i++) {
                            if (adjoiningLayer == this.getLayer(i)) {
                                addLayer(i, layer);
                                break;
                            }
                        }
                    } else {
                        throw new LayerNotFoundInCollectionException(adjoiningLayer, this);
                    }
                } else {
                    addLayer(0, layer);
                }
                break;
            case AFTER:
                if (adjoiningLayer != null) {
                    if (this.layers.contains(adjoiningLayer)) {
                        for (int i = 0; i < this.getLayersCount(); i++) {
                            if (adjoiningLayer == this.getLayer(i)) {
                                addLayer(i + 1, layer);
                                break;
                            }
                        }
                    } else {
                        throw new LayerNotFoundInCollectionException(adjoiningLayer, this);
                    }
                } else {
                    this.addLayer(layer);
                }
                break;
            default: // By default add layer an the end of the collection
                this.addLayer(layer);
                break;
        }

    }

    public FLayer getFirstActiveLayer() {
        LayersIterator it = new LayersIterator(this) {
            @Override
            public boolean evaluate(FLayer layer) {
                return layer.isActive();
            }
        };
        if( it.hasNext() ) {
            return (FLayer) it.next();
        }
        return null;
    }

    public FLyrVect getFirstActiveVectorLayer() {
        LayersIterator it = new LayersIterator(this) {
            @Override
            public boolean evaluate(FLayer layer) {
                return layer.isActive() && layer instanceof FLyrVect;
            }
        };
        if( it.hasNext() ) {
            return (FLyrVect) it.next();
        }
        return null;
    }
    
    @Override
    public Iterator iterator() {
        return this.layers.iterator();
    }

    public Iterator deepiterator() {
        List layers = toPlainList(this);
        return layers.iterator();
    }
    
    @Override
    public int size() {
        return this.layers.size();
    }
    
    @Override
    public FLayer get(int index) {
        return this.layers.get(index);
    }
    
    @Override
    public boolean isEmpty() {
        return this.layers.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.layers.contains(o);
    }

    @Override
    public Object[] toArray() {
        return this.layers.toArray();
    }

    @Override
    public Object[] toArray(Object[] ts) {
        return this.layers.toArray(ts);
    }

    @Override
    public boolean add(FLayer e) {
        this.addLayer(e);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        this.removeLayer((FLayer) o);
        return true;
    }

    @Override
    public boolean containsAll(Collection clctn) {
        return this.layers.containsAll(clctn);
    }

    @Override
    public void add(int i, FLayer e) {
        this.addLayer(i, (FLayer) e);
    }

    @Override
    public FLayer remove(int i) {
        FLayer o = this.getLayer(i);
        this.removeLayer(i);
        return o;
    }

    @Override
    public int indexOf(Object o) {
        return this.layers.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return this.layers.lastIndexOf(o);
    }

    @Override
    public ListIterator listIterator() {
        return this.layers.listIterator();
    }

    @Override
    public ListIterator listIterator(int i) {
        return this.layers.listIterator(i);
    }

    @Override
    public List subList(int i, int i1) {
        return this.layers.subList(i, i1);
    }
    
    @Override
    public boolean addAll(Collection clctn) {
        Iterator it = clctn.iterator();
        while( it.hasNext() ) {
            this.add((FLayer) it.next());
        }
        return true;
    }

    @Override
    public boolean addAll(int i, Collection clctn) {
        Iterator it = clctn.iterator();
        while( it.hasNext() ) {
            this.add(i, (FLayer) it.next());
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection clctn) {
        Iterator it = clctn.iterator();
        while( it.hasNext() ) {
            this.remove((FLayer) it.next());
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection clctn) {
        Iterator it = this.layers.iterator();
        while( it.hasNext() ) {
            Object o = it.next();
            if( !clctn.contains(o) ) {
                this.remove((FLayer) o);
            }
        }
        return true;
    }

    @Override
    public FLayer set(int i, FLayer e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
