/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend.styling;

import org.gvsig.tools.persistence.Persistent;



/**
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface IPlacementConstraints extends Persistent, Cloneable {
	public static int DefaultDuplicateLabelsMode = IPlacementConstraints.ONE_LABEL_PER_FEATURE_PART;

	// constants regarding label duplication
	public static final int REMOVE_DUPLICATE_LABELS             =       2;
	public static final int ONE_LABEL_PER_FEATURE               =       3;
	public static final int ONE_LABEL_PER_FEATURE_PART          =       4;

	// constants regarding point settings
	public static final int OFFSET_HORIZONTALY_AROUND_THE_POINT =       5;
	public static final int ON_TOP_OF_THE_POINT                 =       6;
	public static final int AT_SPECIFIED_ANGLE                  =       7;
	public static final int AT_ANGLE_SPECIFIED_BY_A_FIELD       =       8;

	// constants regarding polygon settings (also apply for lines)
	public static final int HORIZONTAL                          =       9;
	public static final int PARALLEL                            =      10;

	// constants regarding line settings
	public static final int FOLLOWING_LINE                      =      11;
	public static final int PERPENDICULAR                 	    =      12;

	// constants regarding the location along the line
	public static final int AT_THE_END_OF_THE_LINE              =      13;
	public static final int AT_THE_MIDDLE_OF_THE_LINE           =      14;
	public static final int AT_THE_BEGINING_OF_THE_LINE         =      15;
	public static final int AT_BEST_OF_LINE 					=	   16;

	public abstract void setPlacementMode(int mode);

	// regarding label position along the line
	public abstract boolean isBelowTheLine();
	public abstract void setBelowTheLine(boolean b);
	public abstract boolean isAboveTheLine();
	public abstract void setAboveTheLine(boolean b);
	public abstract boolean isOnTheLine();
	public abstract void setOnTheLine(boolean b);
	public abstract void setLocationAlongTheLine(int location);


	// regarding the orientation system
	public abstract boolean isPageOriented();
	public abstract void setPageOriented(boolean b);

	// regarding the label duplication
	public abstract void setDuplicateLabelsMode(int mode);
	public abstract int getDuplicateLabelsMode();


	public abstract boolean isParallel();

	public abstract boolean isFollowingLine();

	public abstract boolean isPerpendicular();

	public abstract boolean isHorizontal();

	public abstract boolean isAtTheBeginingOfLine();

	public abstract boolean isInTheMiddleOfLine();

	public abstract boolean isAtTheEndOfLine();

	public boolean isAtBestOfLine();

	public abstract boolean isOnTopOfThePoint();

	public abstract boolean isAroundThePoint();

	public boolean isFitInsidePolygon();

	public void setFitInsidePolygon(boolean b);


}
