/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.strategies;

import org.cresques.cts.ICoordTrans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.gvsig.fmap.dal.DataSet;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureSelection;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.operations.LayersVisitor;
import org.gvsig.fmap.mapcontext.layers.operations.SingleLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.tools.dispose.DisposableIterator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.visitor.NotSupportedOperationException;

/**
 * This visitor accumulates the envelope of the
 * selected geometries
 */
public class SelectedEnvelopeVisitor implements LayersVisitor {
    
    private static final Logger logger =
        LoggerFactory.getLogger(SelectedEnvelopeVisitor.class);
    
	private Envelope rectangle = null;
	public Envelope getSelectioEnvelope() {
	    return rectangle;
	}

	public String getProcessDescription() {
		return "Defining rectangle to zoom from selected geometries";
	}

	public void visit(Object obj) throws BaseException {
		if (obj instanceof FLayer) {
			this.visit((FLayer) obj);
		} else {
		    throw new NotSupportedOperationException(this, obj);
		}
	}

	public void visit(FLayer layer) throws BaseException {
	    
		if (!(layer instanceof SingleLayer)) {
			return;
		} else if (!layer.isActive()) {
			return;
        } else if (!(layer instanceof FLyrVect)) {
            return;
        }
		
		FLyrVect lyr_vect = (FLyrVect) layer;
		BaseException be = null;
		
		DataSet selection = ((SingleLayer) layer).getDataStore()
				.getSelection();
		
		if (selection != null && (selection instanceof FeatureSelection)) {
		    
		    long tot_count = lyr_vect.getFeatureStore().getFeatureCount();
		    FeatureSelection fsel = (FeatureSelection) selection;
		    long sel_count = fsel.getSize();
		    if (tot_count == sel_count) {
		        try {
		            /*
		             * If all selected, use layer envelope
		             */
		            rectangle = accum(rectangle, lyr_vect.getFullEnvelope(), null);
		        } catch (CloneNotSupportedException ex) {
                    /*
                     * This should not happen because
                     * envelopes support clone 
                     */
		            logger.debug("Error while adding envelope: " + ex.getMessage(), ex);
		        }
		    } else {
		        
		        /*
		         * We'll use this reprojection if not null
		         * to convert feature envelopes
		         */
		        ICoordTrans ct = lyr_vect.getCoordTrans();
		        
		        DisposableIterator iter = fsel.fastIterator();
		        Feature feat = null;
		        while (iter.hasNext()) {
		            feat = (Feature) iter.next();
		            try {
	                    rectangle = accum(
	                        rectangle, feat.getDefaultEnvelope(), ct);
		            } catch (CloneNotSupportedException ex) {
	                    /*
	                     * This should not happen because
	                     * envelopes support clone 
	                     */
	                    logger.debug("Error while adding envelope: " + ex.getMessage(), ex);
	                    break;
		            }
		        }
		        iter.dispose();
		    }
		}
	}

	/**
	 * Adds add_env envelope to ini_env after reprojecting add_env
	 * if cotr not null
	 * 
	 * @param ini_env
	 * @param add_env
	 * @param cotr
	 * @return
	 */
    private Envelope accum(
        Envelope ini_env,
        Envelope add_env,
        ICoordTrans cotr) throws CloneNotSupportedException {
        
        if (add_env == null) {
            /*
             * Nothing to add
             */
            return ini_env;
        } else {
            
            if (cotr == null) {
                
                /*
                 * No reprojection needed
                 */
                if (ini_env == null) {
                    return add_env;
                } else {
                    Envelope resp = (Envelope) ini_env.clone(); 
                    resp.add(add_env);
                    return resp;
                }
                
            } else {
                
                /*
                 * Reproject before adding
                 */
                Envelope add_rep = (Envelope) add_env.convert(cotr).clone();
                if (ini_env == null) {
                    /*
                     * Initial was null
                     */
                    return add_rep;
                } else {
                    Envelope resp = (Envelope) ini_env.clone(); 
                    resp.add(add_rep);
                    return resp;
                }
            }
        }
    }


}
