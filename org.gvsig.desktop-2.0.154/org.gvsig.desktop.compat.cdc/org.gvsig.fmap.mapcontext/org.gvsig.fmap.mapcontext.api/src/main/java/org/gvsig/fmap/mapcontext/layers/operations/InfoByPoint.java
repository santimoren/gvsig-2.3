/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.operations;


import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.task.Cancellable;


/**
 * <p>Interface that must be implemented by layers which support the operation "<i>information by point</i>".</p>
 *
 * <p>That operation allows get meta-information from a point and an area around, of a layer.</p>
 */
public interface InfoByPoint {

    /**
     * <p>
     * Executes a consultation about information of a point on the layer.
     * </p>
     * 
     * <p>
     * There is an area around the point where will got the information.
     * </p>
     * 
     * @param p
     *            point where is the consultation
     * @param tolerance
     *            permissible margin around the coordinates of the point where
     *            the method will got the information. Each
     *            singular implementation of this method would use it in a
     *            different way. The coordinates also depend on the
     *            implementation.
     * @param cancel
     *            shared object that determines if this layer can continue being
     *            drawn
     * 
     * @return a DynObjectSet. If the InfoByPoint Object is a vector layer it
     *         should return a FeatureSet.
     * @throws DataException
     *             TODO
     * @throws LoadLayerException
     *             any exception produced using the driver.
     *             
     * @deprecated use instead {@link #getInfo(Point, double)}
     */
    public DynObjectSet getInfo(java.awt.Point p, double tolerance, Cancellable cancel)
        throws LoadLayerException, DataException;

    /**
     * <p>
     * Executes a consultation about information of a point on the layer.
     * </p>
     * 
     * <p>
     * There is an area around the point where will got the information.
     * </p>
     * 
     * @param p
     *            point where is the consultation
     * @param tolerance
     *            permissible margin around the coordinates of the point where
     *            the method will got the information. Each
     *            singular implementation of this method would use it in a
     *            different way. The coordinates also depend on the
     *            implementation.
     * @param cancel
     *            shared object that determines if this layer can continue being
     *            drawn
     * 
     * @param fast
     *            if true try to reuse objects as much as possible to make the
     *            object iteration faster. If true, DynObjects got through the
     *            returned set must not be stored unless cloned.
     * 
     * @return a DynObjectSet. If the InfoByPoint Object is a vector layer it
     *         should return a FeatureSet.
     * @throws DataException
     *             TODO
     * @throws LoadLayerException
     *             any exception produced using the driver.
     *             
     * @deprecated use instead {@link #getInfo(Point, double)}
     */
    public DynObjectSet getInfo(java.awt.Point p, double tolerance, Cancellable cancel,
        boolean fast) throws LoadLayerException, DataException;

    /**
     * <p>
     * Executes a consultation about information of a point on the layer.
     * </p>
     * 
     * <p>
     * There is an area around the point where will got the information.
     * </p>
     * 
     * @param p
     *            point in map coordinates where is the consultation
     * @param tolerance
     *            permissible margin around the coordinates of the point where
     *            the method will got the information. Each
     *            singular implementation of this method would use it in a
     *            different way. The unit are in map coordinates.
     */            
    public DynObjectSet getInfo(Point p, double tolerance) throws LoadLayerException, DataException;
}
