/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.exception.DataEvaluatorRuntimeException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.operation.GeometryOperationException;
import org.gvsig.fmap.geom.operation.GeometryOperationNotSupportedException;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.tools.evaluator.AbstractEvaluator;
import org.gvsig.tools.evaluator.EvaluatorData;
import org.gvsig.tools.evaluator.EvaluatorException;

public class IntersectsEnvelopeEvaluator extends AbstractEvaluator {

	private Envelope envelope;
	private String geomName;
	private String envelopeWKT = null;
	private Envelope envelopeTrans;
	private boolean isDefault;
	private String srs;
	String defaultGeometryAttributeName;

	public IntersectsEnvelopeEvaluator(Envelope envelope,
			IProjection envelopeProjection, FeatureType featureType,
			String geomName) {
		FeatureAttributeDescriptor fad = (FeatureAttributeDescriptor) featureType
				.get(geomName);

		defaultGeometryAttributeName = featureType.getDefaultGeometryAttributeName();
        this.isDefault = defaultGeometryAttributeName.equals(
				geomName);
		this.envelope = envelope;
		// this.srs = envelopeProjection.getAbrev();
		// this.projection=CRSFactory.getCRS(fad.getSRS());
		// this.envelopeProjection=envelopeProjection;
//		ICoordTrans ct = null;
//		if (!this.srs.equals(fad.getSRS())) { // FIXME comparación
//			ct = envelopeProjection.getCT(fad.getSRS());
//		}
//		if (ct != null) {
//			this.envelopeTrans = envelope.convert(ct);
//		} else {
			this.envelopeTrans = envelope;
//		}
		this.geomName = geomName;

		this.srs = envelopeProjection.getAbrev();

		this.getFieldsInfo().addMatchFieldValue(geomName, envelopeTrans);

	}

    public Object evaluate(EvaluatorData data) throws EvaluatorException {
        Envelope featureEnvelope;
        Geometry geom = null;
        if (isDefault) {
            geom = (Geometry) data.getDataValue(defaultGeometryAttributeName);
            featureEnvelope = ((Feature) data.getContextValue("feature")).getDefaultEnvelope();
        } else {
            geom = (Geometry) data.getDataValue(geomName);
            if (geom == null) {
                return Boolean.FALSE;
            }
            featureEnvelope = geom.getEnvelope();
        }
        if (envelopeTrans.intersects(featureEnvelope)) {
            return new Boolean(envelopeTrans.intersects(geom));
        }
        return Boolean.FALSE;
    }

	public String getName() {
		return "intersects envelope";
	}

	public String getSQL() {

		if (envelopeWKT == null) {
			try {
				envelopeWKT = (String) envelope.getGeometry().convertToWKT();
			} catch (GeometryOperationNotSupportedException e) {
				throw new DataEvaluatorRuntimeException(e);
			} catch (GeometryOperationException e) {
				throw new DataEvaluatorRuntimeException(e);
			}
		}

		return " ST_intersects(ST_GeomFromText('" + envelopeWKT + "', '" + srs
				+ "'), ST_envelope(" + geomName + ")) ";
	}

}
