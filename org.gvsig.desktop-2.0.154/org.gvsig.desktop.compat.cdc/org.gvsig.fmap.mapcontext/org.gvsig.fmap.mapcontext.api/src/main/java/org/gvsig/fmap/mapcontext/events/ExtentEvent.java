/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.events;


import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.timesupport.Time;



/**
 * <p>Event produced when the adjusted extent or the time
 * of the view port have changed.</p>
 * <p>
 * TODO: This class has to be renamed to ViewPortEvent.
 * </p>
 *
 * @author Vicente Caballero Navarro
 */
public class ExtentEvent extends FMapEvent {
	/**
	 * <p>Reference to the new adjusted extent.</p>
	 */
	private Envelope newExtent;
	
	/**
	 * <p>Reference to the new time.</p>
	 */
	private Time newTime;

	/**
	 * <p>Identifier of this kind of event.</p>
	 */
	private static final int EXTENT_EVENT = 0;
	private static final int TIME_EVENT = 1;

	/**
	 * <p>Returns a new extent event.</p>
	 *
	 * @param c the new adjusted extent
	 *
	 * @return a new extent event
	 * @deprecated
	 *         use the constructor 
	 */
	public static ExtentEvent createExtentEvent(Envelope r){
		return new ExtentEvent(r, EXTENT_EVENT);
	}

	/**
	 * <p>Creates a new extent event.</p>
	 *
	 * @param c the new adjusted extent
	 * @param eventType identifier of this kind of event
	 */
	private ExtentEvent(Envelope r, int eventType) {
		setEventType(eventType);
		newExtent = r;
	}
	
	/**
     * <p>Creates a new extent event.</p>
     *
     * @param c the new adjusted extent
     */
    public ExtentEvent(Envelope envelope) {
        this(envelope, EXTENT_EVENT);       
    }
    
    /**
     * <p>Creates a new time event.</p>
     * @param time
     *          the new time
     */
    public ExtentEvent(Time time) {
        setEventType(TIME_EVENT);
        newTime = time;
    }

	/**
	 * <p>Gets the new adjusted event.</p>
	 *
	 * @return the new adjusted extent
	 */
	public Envelope getNewExtent() {
		return newExtent;
	}
	
	/**
     * <p>Gets the new time event.</p>
     *
     * @return the new time extent
     */
    public Time getNewTime() {
        return newTime;
    }
}


