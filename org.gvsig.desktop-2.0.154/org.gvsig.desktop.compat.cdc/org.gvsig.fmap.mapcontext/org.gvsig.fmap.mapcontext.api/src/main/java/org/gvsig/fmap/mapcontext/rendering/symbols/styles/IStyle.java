/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbols.styles;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.tools.lang.Cloneable;
import org.gvsig.tools.persistence.Persistent;

/**
 * Used by Objects that have to define an style for them.This interface has
 * methods to stablish a description and get it,to specify if an object
 * that implements this interface is suitable for a kind of symbol and other
 * methods to draw symbols.
 * @author   jaume dominguez faus - jaume.dominguez@iver.es
 */
public interface IStyle extends Persistent, Cloneable {
	/**
	 * Defines the description of a symbol.It will be specified with an
	 * string.
	 * @param desc,String
	 */
	public abstract void setDescription(String desc);
	/**
	 * Returns the description of a symbol
	 * @return Description of a symbol
	 */
	public abstract String getDescription();
	/**
	 * Useful to render the symbol inside the TOC, or inside little
	 * rectangles. For example, think about rendering a Label with size
	 * in meters => You will need to specify a size in pixels.
	 * Of course. You may also preffer to render a prepared image, etc.
	 * @param g Graphics2D
	 * @param r Rectangle
	 */
	public abstract void drawInsideRectangle(Graphics2D g, Rectangle r) throws SymbolDrawingException;

	/**
	 * True if this symbol is ok for the style or class.
	 * @param symbol ISymbol
	 */

	public abstract boolean isSuitableFor(ISymbol symbol);

	/**
	 * Used to show an outline of the style to graphically show its properties.
	 * @param g, the Graphics2D where to draw
	 * @param r, the bounds of the style inside such Graphics2D
	 */
	public abstract void drawOutline(Graphics2D g, Rectangle r) throws SymbolDrawingException;
}
