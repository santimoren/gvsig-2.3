/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers;

/**
 * Lanzada cuando se intenta realizar una operaci�n generalmente sobre una
 * clase abstracta o interfaz y la implementaci�n de dicha clase abstracta o
 * interfaz no soporta dicha  operaci�n. Ver la descripci�n del m�todo que la
 * origin�
 */
public class OperationNotSupportedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1057162354181561395L;

	/**
	 * Crea una nueva OperationNotSupportedException.
	 */
	public OperationNotSupportedException() {
		super();
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param message
	 */
	public OperationNotSupportedException(String message) {
		super(message);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param message
	 * @param cause
	 */
	public OperationNotSupportedException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param cause
	 */
	public OperationNotSupportedException(Throwable cause) {
		super(cause);
	}
}
