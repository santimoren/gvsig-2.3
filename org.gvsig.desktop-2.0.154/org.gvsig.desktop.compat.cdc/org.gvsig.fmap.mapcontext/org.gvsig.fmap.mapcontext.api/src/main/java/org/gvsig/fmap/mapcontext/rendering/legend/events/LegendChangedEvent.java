/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.legend.events;

import java.util.Arrays;
import java.util.List;
import org.gvsig.fmap.mapcontext.events.FMapEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;



/**
 * <p>Event or set of events produced when changes a legend.</p>
 * 
 */
public class LegendChangedEvent extends LegendEvent {
	/**
	 * <p>Identifies this event as a changed of a legend.</p>
 	 */
	public static final int LEGEND_CHANGED = 0;

	/**
	 * <p>Previous vector legend.</p>
	 */
	private final ILegend oldLegend;

	/**
	 * <p>New vector legend.</p>
	 */
	private final ILegend newLegend;

	/**
	 * <p>Events that constitute this one.</p>
	 */
	private List<LegendChangedEvent> events;

        private FMapEvent sourceEvent;

        /**
	 * <p>Creates a new legend change event.</p>
	 * 
	 * @param oldLegend previous vector legend
	 * @param newLegend new vector legend
	 * 
	 * @return a new legend change event
	 */
	public static LegendChangedEvent createLegendChangedEvent(ILegend oldLegend,
			ILegend newLegend){
		return new LegendChangedEvent(oldLegend, newLegend, LEGEND_CHANGED);
	}
	
	public static LegendChangedEvent createLegendChangedEvent(ILegend legend, FMapEvent event){
            LegendChangedEvent e = new LegendChangedEvent(legend, legend, LEGEND_CHANGED);
            e.setSourceEvent(event);
            return e;
	}
	
	/**
	 * <p>Creates a new legend change event.</p>
	 *
	 * @param oldLegend previous vector legend
	 * @param newLegend new vector legend
	 */
	private LegendChangedEvent(ILegend oldLegend,
			ILegend newLegend, int eventType) {
		this.oldLegend = oldLegend;
		this.newLegend = newLegend;
		setEventType(eventType);
	}

	/**
	 * <p>Gets the previous vector legend.</p>
	 *
	 * @return the previous vector legend
	 */
	public ILegend getOldLegend() {
		return oldLegend;
	}

	/**
	 * <p>Gets the new vector legend.</p>
	 *
	 * @return the new vector legend
	 */
	public ILegend getNewLegend() {
		return newLegend;
	}

	/**
	 * <p>Gets the events that constitute this one.</p>
	 *
	 * @return an array with the events that constitute this one
	 */
	public LegendChangedEvent[] getEvents() {
		return events.toArray(new LegendChangedEvent[this.events.size()]);
	}

	/**
	 * <p>Sets the events that constitute this one.</p>
	 *
	 * @param events an array with the events that constitute this one
	 */
	public void setEvents(LegendChangedEvent[] events) {
            this.events.addAll(Arrays.asList(events));
	}
        
        public LegendChangedEvent addEvent(LegendChangedEvent event) {
            this.events.add(event);
            return this;
        }        
        
        public void setSourceEvent(FMapEvent event) {
            this.sourceEvent = event;
        }
        
        public FMapEvent getSourceEvent() {
            return this.sourceEvent;
        }
}