/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext;

import java.awt.geom.Rectangle2D;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.Persistent;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.util.Callable;


/**
 * <p>
 * <code>ExtentHistory</code> is designed for managing a history of extents.
 * </p>
 * <p>
 * Note: An <i>extent</i> is a rectangular area, with information of its
 * top-left 2D corner.
 * </p>
 *
 * @author Vicente Caballero Navarro
 */
public class ExtentHistory implements Persistent {

  private static final String FIELD_CURRENT_EXTENT = "current";
  private static final String FIELD_NUM_PREVIOUS = "num";
  private static final String FIELD_NUM_NEXT = "numnext";
  private static final String FIELD_NUM_RECORDS = "numrec";
  private static final String FIELD_PREVIOUS_EXTENTS = "extents";
  private static final String FIELD_NEXT_EXTENTS = "extentsNext";
  
  /**
   * <p>
   * Maximum number of extents that can store.
   * </p>
   */
  private int NUMREC;

  /**
   * <p>
   * Array with the previous extents.
   * </p>
   *
   * @see #hasPrevious()
   * @see #put(Rectangle2D)
   * @see #getPrev()
   * @see #popPrev()
   */
  private Rectangle2D[] extentsPrev;

  /**
   * <p>
   * Array with the next extents.
   * </p>
   *
   * @see #hasNext()
   * @see #putNext(Rectangle2D)
   * @see #getNext()
   * @see #popNext()
   */
  private Rectangle2D[] extentsNext;

  /**
   * <p>
   * The current extent of the viewport.
   * </p>
   */
  private Rectangle2D currentExtent;

  /**
   * <p>
   * Number of previous extents stored.
   * </p>
   *
   * @see #hasPrevious()
   * @see #put(Rectangle2D)
   * @see #getPrev()
   * @see #popPrev()
   */
  private int numPrev = 0;

  /**
   * <p>
   * Number of next extents stored.
   * </p>
   *
   * @see #hasNext()
   * @see #putNext(Rectangle2D)
   * @see #getNext()
   * @see #popNext()
   */
  private int numNext = 0;

  /**
   * <p>
   * Creates a new instance of <code>ExtentsHistory</code> with an history of 10
   * extents.
   * </p>
   */
  public ExtentHistory() {
    this(10);
  }

  /**
   * <p>
   * Creates a new instance of <code>ExtentsHistory</code> with an history of
   * <code>numEntries</code> extents.
   * </p>
   *
   * @param numEntries the maximum number of extents that will store the
   *          instance
   */
  public ExtentHistory(int numEntries) {
    NUMREC = numEntries;
    extentsPrev = new Rectangle2D[NUMREC];
    extentsNext = new Rectangle2D[NUMREC];
  }

  /**
   * <p>
   * Appends the specified extent at the end of the array of previous zooms.
   * </p>
   *
   * @param ext the new extent
   */
  public void put(Rectangle2D ext) {
    
    // Si al cargar de la persistencia currentExtent es null ViewPort nos dar� su extent
    if(currentExtent == null) {
      this.currentExtent = ext;
      return;
    }
    
    if ( !ext.equals(getPrev()) && !ext.equals(getNext()) ) {
      clearNumNext();
    }

    pushPrevious(currentExtent);

    this.currentExtent = ext;
  }

  /**
   * <p>
   * Returns <code>true</code> if there are previous extents registered.
   * </p>
   *
   * @return <code>true</code> if there are previous extents registered;
   *         <code>false</code> otherwise
   */
  public boolean hasPrevious() {
    return numPrev > 0;
  }

  /**
   * <p>
   * Returns <code>true</code> if there are next extents registered.
   * </p>
   *
   * @return <code>true</code> if there are next extents registered;
   *         <code>false</code> otherwise
   */
  public boolean hasNext() {
    return numNext > 0;
  }

  /**
   * <p>
   * Returns the last previous extent from the history.
   * </p>
   *
   * @return the last previous extent from the history
   */
  public Rectangle2D getPrev() {
    if (numPrev <= 0) {
      return null;
    }
    Rectangle2D ext = extentsPrev[numPrev - 1];

    return ext;
  }

  /**
   * <p>
   * Returns the last next extent from the history.
   * </p>
   *
   * @return the last next extent from the history
   */
  public Rectangle2D getNext() {
    if (numNext <= 0) {
      return null;
    }

    Rectangle2D ext = extentsNext[numNext - 1];
    return ext;
  }

  /**
   * <p>
   * Extracts (removing) the last previous extent from the history.
   * </p>
   *
   * @return last previous extent from the history
   */
  private Rectangle2D popPrev() {
    if (numPrev <= 0) {
      return null;
    }

    Rectangle2D ext = extentsPrev[--numPrev];
    return ext;
  }

  /**
   * <p>
   * Extracts (removing) the last next extent from the history.
   * </p>
   *
   * @return last next extent from the history
   */
  private Rectangle2D popNext() {
    if (numNext <= 0) {
      return null;
    }

    Rectangle2D ext = extentsNext[--numNext];
    return ext;
  }

  /**
   * <p>
   * Sets to zero the number of previous extents from the history.
   * </p>
   */
  private void clearNumPrev() {
    numPrev = 0;
  }

  /**
   * <p>
   * Sets to zero the number of next extents from the history.
   * </p>
   */
  private void clearNumNext() {
    numNext = 0;
  }

  /**
   * <p>
   * Adds the current extent to the next extent history and sets the last
   * previous extent as the new current extent.
   * </p>
   *
   * @return the last previous extent as the new current extent
   */
  public Rectangle2D setPreviousExtent() {
    
    if (currentExtent != null) {
      pushNext(currentExtent);
    }
    currentExtent = popPrev();
    return currentExtent;
  }
  
  /**
   * <p>
   * Adds the specified extent to the next extent history.
   * If the array of next extents is complete, loses the
   * first extent of the array and adds the new one at the end.
   * </p>
   *  
   */
  private void pushNext(Rectangle2D ext) {
    if (numNext < (NUMREC)) {
      extentsNext[numNext] = ext;
      numNext = numNext + 1;
    }
    else {
      for (int i = 0; i < (NUMREC - 1); i++) {
        extentsNext[i] = extentsNext[i + 1];
      }
      extentsNext[numNext - 1] = ext;
    }
  }

  /**
   * <p>
   * Adds the current extent to the previous extent history and sets the last
   * next extent as the new current extent.
   * </p>
   * 
   * @return the last next extent as the new current extent
   */
  public Rectangle2D setNextExtent() {

    if (currentExtent != null) {
      pushPrevious(currentExtent);
      
    }
    currentExtent = popNext();
    return currentExtent;
  }

  /**
   * <p>
   * Adds the specified extent to the previous extent history.
   * If the array of previous extents is complete, loses the
   * first extent of the array and adds the new one at the end.
   * </p>
   *  
   */
  private void pushPrevious(Rectangle2D ext) {
    if (numPrev < (NUMREC)) {
      extentsPrev[numPrev] = ext;
      numPrev++;
    } else {
      for (int i = 0; i < (NUMREC - 1); i++) {
        extentsPrev[i] = extentsPrev[i + 1];
      }
      extentsPrev[numPrev - 1] = ext;
    }
  }
  
  /**
   * <p>
   * Returns the current extent.
   * </p>
   *  
   * @return the current extent
   */
  public Rectangle2D getCurrent() {
    return currentExtent;
  }

  public void loadFromState(PersistentState state) throws PersistenceException {

    if (state.hasValue("num")) {
      numPrev = state.getInt("num");
    }
    else {
      clearNumPrev();
    }

    if (state.hasValue("numnext")) {
      numNext = state.getInt("numnext");
    }
    else {
      clearNumNext();
    }

    if (state.hasValue("numrec")) {
      NUMREC = state.getInt("numrec");
    }
    else {
      NUMREC = 10;
    }

    if (state.hasValue("extents")) {
      extentsPrev = (Rectangle2D[]) state
          .getArray("extents", Rectangle2D.class);
    }
    else {
      extentsPrev = new Rectangle2D[NUMREC];
      clearNumPrev();
    }

    if (state.hasValue("extentsNext")) {
      extentsNext = (Rectangle2D[]) state.getArray("extentsNext",
          Rectangle2D.class);
    }
    else {
      extentsNext = new Rectangle2D[NUMREC];
      clearNumNext();
    }
    
    if (state.hasValue("current")) {
      currentExtent = (Rectangle2D) state.get("current");
    }
    else {
      currentExtent = null;
    }
  }

  /**
   * <p>
   * Returns information of this object. All information is stored as
   * properties:<br>
   * </p>
   * <p>
   * <b>Properties:</b>
   * <ul>
   * <li><i>className</i>: name of this class.
   * <li><i>num</i>: number of extents registered.
   * <li><i>numrec</i>: maximum number of extents that can register.
   * <li><i>extents</i>: .
   * </ul>
   * </p>
   */
  public void saveToState(PersistentState state) throws PersistenceException {

    state.set(FIELD_NUM_PREVIOUS, numPrev);
    state.set(FIELD_NUM_NEXT, numNext);
    state.set(FIELD_NUM_RECORDS, NUMREC);
    state.set(FIELD_PREVIOUS_EXTENTS, extentsPrev);
    state.set(FIELD_NEXT_EXTENTS, extentsNext);
    state.set(FIELD_CURRENT_EXTENT, currentExtent);
  }

  public static class RegisterPersistence implements Callable {

    public Object call() {
      PersistenceManager manager = ToolsLocator.getPersistenceManager();
      DynStruct definition = manager.addDefinition(ExtentHistory.class,
          "ExtentHistory", "ExtentHistory Persistence definition", null, null);
      definition.addDynFieldInt(FIELD_NUM_PREVIOUS).setMandatory(true);
      definition.addDynFieldInt(FIELD_NUM_NEXT).setMandatory(true);
      definition.addDynFieldInt(FIELD_NUM_RECORDS).setMandatory(true);
      definition.addDynFieldArray(FIELD_PREVIOUS_EXTENTS).setClassOfItems(Rectangle2D.class)
          .setMandatory(true);
      definition.addDynFieldArray(FIELD_NEXT_EXTENTS)
          .setClassOfItems(Rectangle2D.class).setMandatory(true);
      definition.addDynFieldObject(FIELD_CURRENT_EXTENT)
      .setClassOfValue(Rectangle2D.class).setMandatory(false);

      return Boolean.TRUE;
    }
  }
}