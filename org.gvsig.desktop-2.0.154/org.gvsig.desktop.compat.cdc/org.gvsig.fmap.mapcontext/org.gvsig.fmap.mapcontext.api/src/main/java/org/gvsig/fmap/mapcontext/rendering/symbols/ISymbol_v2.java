package org.gvsig.fmap.mapcontext.rendering.symbols;

public interface ISymbol_v2 extends ISymbol {

    /**
     * Set the id ob the symbol (the basename of file).
     * This attribute is not persistent.
     *
     * @param id
     */
    public void setID(String id);

    /**
     * Get the id ob the symbol (the basename of file).
     * This attribute is not persistent.
     *
     * @param id
     */
    public String getID();
}
