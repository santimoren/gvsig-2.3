/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.vectorial;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.slf4j.LoggerFactory;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.DALLocator;
import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.exception.InitializeException;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.exception.ReadRuntimeException;
import org.gvsig.fmap.dal.feature.FeatureAttributeDescriptor;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.dal.feature.FeatureStoreNotification;
import org.gvsig.fmap.dal.feature.FeatureType;
import org.gvsig.fmap.dal.feature.exception.CreateGeometryException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.SUBTYPES;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.GeometryLocator;
import org.gvsig.fmap.geom.GeometryManager;
import org.gvsig.fmap.geom.exception.CreateEnvelopeException;
import org.gvsig.fmap.geom.primitive.Circle;
import org.gvsig.fmap.geom.primitive.Envelope;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.geom.type.GeometryTypeNotSupportedException;
import org.gvsig.fmap.geom.type.GeometryTypeNotValidException;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.MapContextManager;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.LegendLayerException;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.exceptions.ReloadLayerException;
import org.gvsig.fmap.mapcontext.exceptions.StartEditionLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLyrDefault;
import org.gvsig.fmap.mapcontext.layers.LayerEvent;
import org.gvsig.fmap.mapcontext.layers.SpatialCache;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.LegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.events.FeatureDrawnNotification;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendChangedEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendClearEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.symbols.CartographicSupport;
import org.gvsig.metadata.exceptions.MetadataException;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynObjectSet;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.exception.BaseException;
import org.gvsig.tools.exception.BaseRuntimeException;
import org.gvsig.tools.locator.LocatorException;
import org.gvsig.tools.observer.Observable;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;
import org.gvsig.tools.util.Callable;

/**
 * Capa b�sica Vectorial.
 *
 */
public class FLyrVect extends FLyrDefault implements VectorLayer,
        LegendContentsChangedListener, Observer {

    final static private org.slf4j.Logger logger
            = LoggerFactory.getLogger(FLyrVect.class);
    private final GeometryManager geomManager
            = GeometryLocator.getGeometryManager();

    /**
     * Leyenda de la capa vectorial
     */
    private IVectorLegend legend;
    private int typeShape = -1;
    private FeatureStore featureStore = null;
    private SpatialCache spatialCache = new SpatialCache();

    /**
     * An implementation of gvSIG spatial index
     */
    // protected ISpatialIndex spatialIndex = null;
    private IVectorLegend loadLegend = null;

    private boolean isLabeled;
    protected ILabelingStrategy strategy;
//	private ReprojectDefaultGeometry reprojectTransform;
    private FeatureQuery baseQuery = null;

    public FLyrVect() {
        super();
    }

    public String getTocImageIcon() {
        if (this.isAvailable()) {
            return MapContextLocator.getMapContextManager().getIconLayer(this.getDataStore());
        } else {
            /*
             * data store can be be null,
             * for example, a layer not loaded from persistence
             */
            return "layer-icon-unavailable";
        }

    }

    /**
     * Devuelve el VectorialAdapater de la capa.
     *
     * @return VectorialAdapter.
     */
    public DataStore getDataStore() {
//        if (!this.isAvailable()) {
//            return null;
//        }
        return featureStore;
    }

    /**
     * Asigna el data-store a la capa. Esta operacion no se deneria poder hacer
     * desde fuera de la clase.
     *
     * @param dataStore
     * @throws LoadLayerException
     * @deprecated use {@link #bindToDataStore(DataStore)}
     */
    public void setDataStore(DataStore dataStore) throws LoadLayerException {
        bindToDataStore(dataStore);
    }

    /**
     * Enlaza la capa con el DataStore indicado.
     *
     * @param dataStore
     * @throws LoadLayerException
     */
    protected void bindToDataStore(DataStore dataStore) throws LoadLayerException {
        if (this.featureStore != null && this.featureStore != dataStore) {
            this.featureStore.deleteObserver(this);
        }

        featureStore = (FeatureStore) dataStore;

        MapContextManager mapContextManager
                = MapContextLocator.getMapContextManager();

        //Set the legend
        IVectorLegend legend
                = (IVectorLegend) mapContextManager.getLegend(dataStore);

        if (legend == null) {
            throw new LegendLayerException(this.getName());
        }

        this.setLegend(legend);

        //Set the labeling strategy
        ILabelingStrategy labeler
                = (ILabelingStrategy) mapContextManager.getLabelingStrategy(dataStore);

        if (labeler != null) {
            labeler.setLayer(this);
            this.setLabelingStrategy(labeler);
            this.setIsLabeled(true); // TODO: ac� no s'hauria de detectar si t�
            // etiquetes?????
        }

        this.delegate(dataStore);

        dataStore.addObserver(this);

        ToolsLocator.getDisposableManager().bind(dataStore);
    }

    public Envelope getFullEnvelope() throws ReadException {
        Envelope rAux;
        try {
            rAux = getFeatureStore().getEnvelope();
        } catch (BaseException e) {
            throw new ReadException(getName(), e);
        }

        // Esto es para cuando se crea una capa nueva con el fullExtent de ancho
        // y alto 0.
        if (rAux == null || rAux.isEmpty() || rAux.getMaximum(0) - rAux.getMinimum(0) == 0
                && rAux.getMaximum(1) - rAux.getMinimum(1) == 0) {
            try {
                rAux
                        = geomManager.createEnvelope(0, 0, 90, 90, SUBTYPES.GEOM2D);
            } catch (CreateEnvelopeException e) {
                logger.error("Error creating the envelope", e);
                e.printStackTrace();
            }
        }
        // Si existe reproyecci�n, reproyectar el extent
        ICoordTrans ct = getCoordTrans();
        if (ct != null) {
            boolean originalEnvelopeIsEmpty = rAux.isEmpty();
            rAux = rAux.convert(ct);
            if(!originalEnvelopeIsEmpty && rAux.isEmpty()){
                try {
                    this.setAvailable(false);
                    throw new EnvelopeCantBeInitializedException();
                } catch(EnvelopeCantBeInitializedException e){
                    this.addError(e);
                }
            }
        }
        return rAux;

    }

    public void setBaseQuery(FeatureQuery baseQuery) {
        this.baseQuery = baseQuery;
    }

    @Override
    public FeatureQuery getBaseQuery() {
        return this.baseQuery;
    }

    public void addBaseFilter(Evaluator filter) {
        if( this.baseQuery == null ) {
            this.baseQuery = this.getFeatureStore().createFeatureQuery();
        }
        this.baseQuery.addFilter(filter);
    }

    public void addBaseFilter(String filter) {
        try {
            this.addBaseFilter( DALLocator.getDataManager().createExpresion(filter));
        } catch (InitializeException ex) {
            throw new RuntimeException("Can't create filter with '"+filter+"'", ex);
        }
    }

    /**
     * Draws using IFeatureIterator. This method will replace the old draw(...)
     * one.
     *
     * @autor jaume dominguez faus - jaume.dominguez@iver.es
     * @param image
     * @param g
     * @param viewPort
     * @param cancel
     * @param scale
     * @throws ReadDriverException
     */
    public void draw(BufferedImage image,
            Graphics2D g,
            ViewPort viewPort,
            Cancellable cancel,
            double scale) throws ReadException {

        if (legend == null) {
            return;
        }

        if (!this.isWithinScale(scale)) {
            return;
        }
        if (cancel.isCanceled()) {
            return;
        }

        if (spatialCache.isEnabled()) {
            spatialCache.clearAll();
            legend.addDrawingObserver(this);
        }

        FeatureQuery featureQuery = createFeatureQuery();

        try {
            FeatureAttributeDescriptor featureAttributeDescriptor
                    = getFeatureStore().getDefaultFeatureType().getDefaultTimeAttribute();

            if ((viewPort.getTime() != null) && (featureAttributeDescriptor != null)) {
                IntersectsTimeEvaluator intersectsTimeEvaluator
                        = new IntersectsTimeEvaluator(viewPort.getTime(), featureAttributeDescriptor.getName());
                featureQuery.addFilter(intersectsTimeEvaluator);
            }
        } catch (DataException e1) {
            logger.error("Impossible to get the temporal filter", e1);
        }

        try {

            long tini = System.currentTimeMillis();

            legend.draw(image,
                    g,
                    viewPort,
                    cancel,
                    scale,
                    null,
                    getCoordTrans(),
                    getFeatureStore(),
                    featureQuery);

            logger.debug("Layer " + this.getName() + " drawn in "
                    + (System.currentTimeMillis() - tini) + " milliseconds.");

        } catch (LegendException e) {
            this.setAvailable(false);
            this.setError(e);
            throw new ReadException(getName(), e);
        } finally {
            if (spatialCache.isEnabled()) {
                legend.deleteDrawingObserver(this);
            }
        }
    }

    public void print(Graphics2D g,
            ViewPort viewPort,
            Cancellable cancel,
            double scale,
            PrintAttributes properties) throws ReadException {
        if (!this.isWithinScale(scale)) {
            return;
        }
        if (cancel.isCanceled()) {
            return;
        }
        FeatureQuery featureQuery = createFeatureQuery();

        try {
            legend.print(g,
                    viewPort,
                    cancel,
                    scale,
                    null,
                    getCoordTrans(),
                    getFeatureStore(),
                    featureQuery,
                    properties);

        } catch (LegendException e) {
            this.setVisible(false);
            this.setActive(false);
            throw new ReadException(getName(), e);
        }
    }

    public void setLegend(IVectorLegend legend) throws LegendLayerException {
        if (this.legend == legend) {
            return;
        }
        if (this.legend != null && this.legend.equals(legend)) {
            return;
        }
        IVectorLegend oldLegend = this.legend;
        this.legend = legend;
        if (oldLegend != null) {
            oldLegend.removeLegendListener(this);
            oldLegend.deleteDrawingObserver(this);
        }
        if (legend != null) {
            this.legend.addDrawingObserver(this);
            this.legend.addLegendListener(this);
        }
        LegendChangedEvent e = LegendChangedEvent.createLegendChangedEvent(oldLegend, this.legend);
        e.setLayer(this);
        updateDrawVersion();
        callLegendChanged(e);
    }

    /**
     * Devuelve la Leyenda de la capa.
     *
     * @return Leyenda.
     */
    public ILegend getLegend() {
        return legend;
    }

    public int getShapeType() throws ReadException {
        if (typeShape == -1) {
            FeatureType featureType = null;
            try {
                if (getDataStore() != null) {
                    featureType
                            = (((FeatureStore) getDataStore()).getDefaultFeatureType());
                }
            } catch (DataException e) {
                throw new ReadException(getName(), e);
            }
            if (featureType != null) {
                int indexGeom = featureType.getDefaultGeometryAttributeIndex();
                typeShape
                        = featureType.getAttributeDescriptor(indexGeom).getGeometryType();
            }
        }
        return typeShape;
    }

    /**
     * Returns the layer's geometry type
     *
     * @return the geometry type
     *
     * @throws ReadException if there is an error getting the geometry type
     */
    public GeometryType getGeometryType() throws ReadException {
        FeatureType featureType = null;
        try {
            if (getDataStore() != null) {
                featureType
                        = (((FeatureStore) getDataStore()).getDefaultFeatureType());
            }
        } catch (DataException e) {
            throw new ReadException(getName(), e);
        }
        return featureType == null ? null : featureType
                .getDefaultGeometryAttribute().getGeomType();
    }

    public void saveToState(PersistentState state) throws PersistenceException {

        FeatureStore featureStore = null;

        if (!this.isAvailable()) {
            logger.info("The '" + this.getName() + "' layer is not available, it will persist not.");
            return;
        }

        try {
            super.saveToState(state);

            if (getLegend() != null) {
                state.set("legend", getLegend());
            }

            featureStore = getFeatureStore();

            if (featureStore != null) {
                state.set("featureStore", featureStore);
            }

            state.set("isLabeled", isLabeled);

            if (strategy != null) {
                state.set("labelingStrategy", strategy);
            }

            if (getLinkProperties() != null) {
                state.set("linkProperties", getLinkProperties());
            }

            state.set("typeShape", typeShape);
        } catch (PersistenceException ex) {
            logger.warn("Can't persist to state the layer '" + this.getName() + "'.", ex);
            throw ex;
        } catch (RuntimeException ex) {
            logger.warn("Can't persist to state the layer '" + this.getName() + "'.", ex);
            throw ex;
        }

    }

    public void loadFromState(PersistentState state) throws PersistenceException {

        DataStore store = null;
        IVectorLegend vectorLegend = null;
        ILabelingStrategy labelingStrategy = null;
        Boolean isLabeled = Boolean.FALSE;

        try {
            super.loadFromState(state);
            store = (DataStore) state.get("featureStore");

            try {
                this.bindToDataStore(store);
            } catch (LoadLayerException e) {
                throw new PersistenceException("Can't bind layer '" + this.getName() + "' to store '" + store.getFullName() + "'.", e);
            }

            vectorLegend = (IVectorLegend) state.get("legend");

            try {
                this.setLegend(vectorLegend);
            } catch (LegendLayerException e) {
                throw new PersistenceException("Can't set vector legend to the layer.", e);
            }

            try {
                isLabeled = (Boolean) state.get("isLabeled");
                if (isLabeled.booleanValue()) {
                    labelingStrategy = (ILabelingStrategy) state.get("labelingStrategy");
                }
            } catch (Exception ex) {
                throw new PersistenceException("Can't load labeling strategi from persistent state.",
                        ex);
            }

            if (isLabeled.booleanValue()) {
                this.setIsLabeled(true);
                this.setLabelingStrategy(labelingStrategy);
            } else {
                this.setIsLabeled(false);
                this.setLabelingStrategy(null);
            }

            typeShape = state.getInt("typeShape");

        } catch (Throwable e) {
            String storeName = (store == null) ? "unknow" : store.getFullName();
            logger.warn("can't load layer '" + this.getName() + "' (store=" + storeName + ") from persisted state.", e);
            this.setAvailable(false);
            return;
        }

    }

    /**
     * Sobreimplementaci�n del m�todo toString para que las bases de datos
     * identifiquen la capa.
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        /*
         * Se usa internamente para que la parte de datos identifique de forma
         * un�voca las tablas
         */
        String ret = super.toString();

        return ret ; //"layer" + ret.substring(ret.indexOf('@') + 1);
    }

    public boolean isEditing() {
        FeatureStore fs = getFeatureStore();
        if (fs == null) {
            /*
             * This happens when layer is not available, for example,
             * it was not possible to load from persistence
             */
            return false;
        } else {
            return fs.isEditing();
        }
    }

    public void setEditing(boolean b) throws StartEditionLayerException {

        try {
            throw new RuntimeException();
        } catch (Throwable th) {
            logger.info("This method is deprecated. ", th);
        }

        if (b == super.isEditing()) {
            return;
        }

        super.setEditing(b);
        FeatureStore fs = getFeatureStore();
        if (b) {
            try {
                fs.edit();
            } catch (DataException e) {
                throw new StartEditionLayerException(getName(), e);
            }
        }
        setSpatialCacheEnabled(b);
        callEditionChanged(LayerEvent.createEditionChangedEvent(this, "edition"));
    }

    /**
     * @deprecated Use {@link #getSpatialCache()}
     */
    public void clearSpatialCache() {
        spatialCache.clearAll();
    }

    /**
     * @deprecated Use {@link #getSpatialCache()}
     */
    public boolean isSpatialCacheEnabled() {
        return spatialCache.isEnabled();
    }

    /**
     * @deprecated Use {@link #getSpatialCache()}
     */
    public void setSpatialCacheEnabled(boolean spatialCacheEnabled) {
        spatialCache.setEnabled(spatialCacheEnabled);
    }

    public SpatialCache getSpatialCache() {
        return spatialCache;
    }

    /**
     * Siempre es un numero mayor de 1000
     *
     * @param maxFeatures
     */
    public void setMaxFeaturesInEditionCache(int maxFeatures) {
        if (maxFeatures > spatialCache.getMaxFeatures()) {
            spatialCache.setMaxFeatures(maxFeatures);
        }

    }

    /**
     * This method returns a boolean that is used by the FPopMenu to make
     * visible the properties menu or not. It is visible by default, and if a
     * later don't have to show this menu only has to override this method.
     *
     * @return If the properties menu is visible (or not)
     */
    public boolean isPropertiesMenuVisible() {
        return true;
    }

    public void reload() throws ReloadLayerException {
        super.reload();
        try {
            getFeatureStore().refresh();
        } catch (Exception e) {
            throw new ReloadLayerException(getName(), e);
        }
    }

    protected void setLoadSelection(Object xml) {
        // this.loadSelection = xml;
    }

    protected void setLoadLegend(IVectorLegend legend) {
        this.loadLegend = legend;
    }

    protected void putLoadSelection() {
        // if (this.loadSelection == null) return;
        // try {
        // this.getRecordset().getSelectionSupport().setXMLEntity(this.loadSelection);
        // } catch (ReadDriverException e) {
        // throw new XMLException(e);
        // }
        // this.loadSelection = null;

    }

    protected void putLoadLegend() throws LegendLayerException {
        if (this.loadLegend == null) {
            return;
        }
        this.setLegend(this.loadLegend);
        this.loadLegend = null;
    }

    protected void cleanLoadOptions() {
        this.loadLegend = null;
    }

    public boolean isWritable() {
        return getFeatureStore().allowWrite();
    }

    public FLayer cloneLayer() throws Exception {
        FLyrVect clonedLayer = new FLyrVect();
        clonedLayer.bindToDataStore(getDataStore());
        // if (isJoined()) {
        // clonedLayer.setIsJoined(true);
        // }
        clonedLayer.setVisible(isVisible());
        // clonedLayer.setISpatialIndex(getISpatialIndex());
        clonedLayer.setName(getName());
        clonedLayer.setCoordTrans(getCoordTrans());

        clonedLayer.setLegend((IVectorLegend) getLegend().cloneLegend());

        clonedLayer.setIsLabeled(isLabeled());
        ILabelingStrategy labelingStrategy = getLabelingStrategy();
        if (labelingStrategy != null) {
            clonedLayer.setLabelingStrategy(labelingStrategy);
        }

        return clonedLayer;
    }

    protected boolean isOnePoint(AffineTransform graphicsTransform,
            ViewPort viewPort,
            double dpi,
            CartographicSupport csSym,
            Geometry geom,
            int[] xyCoords) {
        return isOnePoint(graphicsTransform, viewPort, geom, xyCoords)
                && csSym.getCartographicSize(viewPort, dpi, geom) <= 1;
    }

    private boolean isOnePoint(AffineTransform graphicsTransform,
            ViewPort viewPort,
            Geometry geom,
            int[] xyCoords) {
        boolean onePoint = false;
        int type = geom.getType();
        if (type == Geometry.TYPES.NULL) {
            return false;
        }
        if (type != Geometry.TYPES.POINT && type != Geometry.TYPES.MULTIPOINT) {

            Envelope geomBounds = geom.getEnvelope();

            // ICoordTrans ct = getCoordTrans();
            // Se supone que la geometria ya esta reproyectada
            // if (ct!=null) {
            // // geomBounds = ct.getInverted().convert(geomBounds);
            // geomBounds = geomBounds.convert(ct);
            // }
            double dist1Pixel = viewPort.getDist1pixel();

            onePoint
                    = (geomBounds.getLength(0) <= dist1Pixel && geomBounds.getLength(1) <= dist1Pixel);

            if (onePoint) {
                // avoid out of range exceptions
                org.gvsig.fmap.geom.primitive.Point p;
                try {
                    p
                            = geomManager.createPoint(geomBounds.getMinimum(0),
                                    geomBounds.getMinimum(1),
                                    SUBTYPES.GEOM2D);
                    p.transform(viewPort.getAffineTransform());
                    p.transform(graphicsTransform);
                    xyCoords[0] = (int) p.getX();
                    xyCoords[1] = (int) p.getY();
                } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
                    logger.error("Error creating a point", e);
                }

            }

        }
        return onePoint;
    }

    public boolean isLabeled() {
        return isLabeled;
    }

    public void setIsLabeled(boolean isLabeled) {
        this.isLabeled = isLabeled;
    }

    public ILabelingStrategy getLabelingStrategy() {
        return strategy;
    }

    public void setLabelingStrategy(ILabelingStrategy strategy) {
        this.strategy = strategy;
        if (strategy == null) {
            return;
        }
        strategy.setLayer(this);
        updateDrawVersion();
    }

    public void drawLabels(BufferedImage image,
            Graphics2D g,
            ViewPort viewPort,
            Cancellable cancel,
            double scale,
            double dpi) throws ReadException {
        if (strategy != null && isWithinScale(scale)) {
            strategy.draw(image, g, scale, viewPort, cancel, dpi);
        }
    }

    public void printLabels(Graphics2D g,
            ViewPort viewPort,
            Cancellable cancel,
            double scale,
            PrintAttributes properties) throws ReadException {
        if (strategy != null) {
            strategy.print(g, scale, viewPort, cancel, properties);
        }
    }

    /**
     * Return true, because a Vectorial Layer supports HyperLink
     *
     * @deprecated the hiperlink functionaliti is out the layer now
     */
    public boolean allowLinks() {
        return false;
    }

    public void load() throws LoadLayerException {
        super.load();
    }

    public FeatureStore getFeatureStore() {
        return (FeatureStore) getDataStore();
    }

    public FeatureQuery createFeatureQuery() {
        if( this.baseQuery==null ) {
            return this.getFeatureStore().createFeatureQuery();
        }
        try {
            return (FeatureQuery) baseQuery.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * @deprecated use instead
     * {@link #queryByPoint(org.gvsig.fmap.geom.primitive.Point, double, FeatureType)}
     */
    public FeatureSet queryByPoint(Point2D mapPoint,
            double tol,
            FeatureType featureType) throws DataException {
        logger.warn("Deprecated use of queryByPoint.");
        GeometryManager manager = GeometryLocator.getGeometryManager();
        org.gvsig.fmap.geom.primitive.Point center;
        try {
            center
                    = (org.gvsig.fmap.geom.primitive.Point) manager.create(TYPES.POINT,
                            SUBTYPES.GEOM2D);
            center.setX(mapPoint.getX());
            center.setY(mapPoint.getY());
            Circle circle
                    = (Circle) manager.create(TYPES.CIRCLE, SUBTYPES.GEOM2D);
            circle.setPoints(center, tol);
            return queryByGeometry(circle, featureType);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            throw new CreateGeometryException(TYPES.CIRCLE, SUBTYPES.GEOM2D, e);
        }
    }

    public FeatureSet queryByPoint(org.gvsig.fmap.geom.primitive.Point point,
            double tol,
            FeatureType featureType) throws DataException {
        GeometryManager manager = GeometryLocator.getGeometryManager();
        try {
            Circle circle
                    = (Circle) manager.create(TYPES.CIRCLE, SUBTYPES.GEOM2D);
            circle.setPoints(point, tol);
            return queryByGeometry(circle, featureType);
        } catch (org.gvsig.fmap.geom.exception.CreateGeometryException e) {
            throw new CreateGeometryException(TYPES.CIRCLE, SUBTYPES.GEOM2D, e);
        }
    }

    /**
     * Input geom must be in the CRS of the view.
     *
     * @param geom
     * @param featureType
     * @return
     * @throws DataException
     */
    public FeatureSet queryByGeometry(Geometry geom, FeatureType featureType) throws DataException {
        FeatureQuery featureQuery = createFeatureQuery();
        String geomName
                = featureStore.getDefaultFeatureType()
                .getDefaultGeometryAttributeName();
        featureQuery.setFeatureType(featureType);

        Geometry query_geo = this.transformToSourceCRS(geom, true);
        IProjection query_proj = getMapContext().getProjection();
        if (this.getCoordTrans() != null) {
            query_proj = this.getCoordTrans().getPOrig();
        }

        IntersectsGeometryEvaluator iee
                = new IntersectsGeometryEvaluator(
                        query_geo,
                        query_proj,
                        featureStore.getDefaultFeatureType(),
                        geomName);
        featureQuery.setFilter(iee);
        featureQuery.setAttributeNames(null);
        return getFeatureStore().getFeatureSet(featureQuery);

    }

    /**
     * It return the {@link FeatureSet} that intersects with the envelope.
     *
     * @param envelope envelope that defines the area for the query.
     * @param featureType only the features with this feature type are used in
     * the query.
     * @return the set of features that intersect with the envelope.
     * @throws DataException
     */
    public FeatureSet queryByEnvelope(Envelope envelope, FeatureType featureType) throws DataException {
        return queryByEnvelope(envelope, featureType, null);
    }

    /**
     * It return the {@link FeatureSet} that intersects with the envelope.
     *
     * @param envelope envelope that defines the area for the query in viewport
     * CRS
     * @param featureType only the features with this feature type are used in
     * the query.
     * @param names the feature attributes that have to be checked.
     * @return the set of features that intersect with the envelope.
     * @throws DataException
     */
    public FeatureSet queryByEnvelope(Envelope envelope,
            FeatureType featureType,
            String[] names) throws DataException {
        FeatureQuery featureQuery = createFeatureQuery();
        if (names == null) {
            featureQuery.setFeatureType(featureType);
        } else {
            featureQuery.setAttributeNames(names);
            featureQuery.setFeatureTypeId(featureType.getId());
        }
        String geomName = featureStore.getDefaultFeatureType()
                .getDefaultGeometryAttributeName();

        Envelope query_env = fromViewPortCRSToSourceCRS(this, envelope);
        IProjection query_proj = getMapContext().getProjection();
        if (this.getCoordTrans() != null) {
            query_proj = this.getCoordTrans().getPOrig();
        }

        IntersectsGeometryEvaluator iee
                = new IntersectsGeometryEvaluator(
                        query_env.getGeometry(), query_proj,
                        featureStore.getDefaultFeatureType(),
                        geomName);
        featureQuery.setFilter(iee);
        return getFeatureStore().getFeatureSet(featureQuery);

    }

    public DynObjectSet getInfo(Point p, double tolerance, Cancellable cancel) throws LoadLayerException,
            DataException {

        return getInfo(p, tolerance, cancel, true);
    }

    public DynObjectSet getInfo(Point p,
            double tolerance,
            Cancellable cancel,
            boolean fast) throws LoadLayerException, DataException {
        Point2D infop = new Point2D.Double(p.x, p.y);
        Point2D pReal = this.getMapContext().getViewPort().toMapPoint(infop);
        return queryByPoint(pReal,
                tolerance,
                getFeatureStore().getDefaultFeatureType()).getDynObjectSet(fast);
    }

    public DynObjectSet getInfo(org.gvsig.fmap.geom.primitive.Point p,
            double tolerance) throws LoadLayerException, DataException {
        return queryByPoint(p, tolerance, getFeatureStore().getDefaultFeatureType()).getDynObjectSet(false);
    }

    @Override
    public void legendCleared(LegendClearEvent event) {
        this.updateDrawVersion();
        LegendChangedEvent e = LegendChangedEvent.createLegendChangedEvent(legend,event);
        this.callLegendChanged(e);
    }

    @Override
    public boolean symbolChanged(SymbolLegendEvent e) {
        this.updateDrawVersion();
        LegendChangedEvent ev = LegendChangedEvent.createLegendChangedEvent(legend, e);
        this.callLegendChanged(ev);
        return true;
    }

    public void update(Observable observable, Object notification) {
        if (observable.equals(this.featureStore)) {
            if (notification instanceof FeatureStoreNotification) {
                FeatureStoreNotification event
                        = (FeatureStoreNotification) notification;
                if (event.getType() == FeatureStoreNotification.AFTER_DELETE
                        || event.getType() == FeatureStoreNotification.AFTER_UNDO
                        || event.getType() == FeatureStoreNotification.AFTER_REDO
                        || event.getType() == FeatureStoreNotification.AFTER_REFRESH
                        || event.getType() == FeatureStoreNotification.AFTER_UPDATE
                        || event.getType() == FeatureStoreNotification.AFTER_UPDATE_TYPE
                        || event.getType() == FeatureStoreNotification.SELECTION_CHANGE
                        || event.getType() == FeatureStoreNotification.AFTER_INSERT) {
                    this.updateDrawVersion();

                } else if (event.getType() == FeatureStoreNotification.AFTER_CANCELEDITING) {

                    setSpatialCacheEnabled(false);
                    callEditionChanged(LayerEvent.createEditionChangedEvent(this, "edition"));
                    this.updateDrawVersion();

                } else if (event.getType() == FeatureStoreNotification.AFTER_STARTEDITING) {

                    setSpatialCacheEnabled(true);
                    callEditionChanged(LayerEvent.createEditionChangedEvent(this, "edition"));

                } else if (event.getType() == FeatureStoreNotification.TRANSFORM_CHANGE) {
                    //If a transform has to be applied, try to reload the layer.
                    try {
                        reload();
                    } catch (ReloadLayerException e) {
                        logger.info("While reloading layer.", e);
                        this.setAvailable(false);
                    }
                } else if (event.getType() == FeatureStoreNotification.RESOURCE_CHANGED) {
                    this.setAvailable(false);
                } else if (event.getType() == FeatureStoreNotification.AFTER_FINISHEDITING) {
                    this.setAvailable(true);
                    setSpatialCacheEnabled(false);
                    callEditionChanged(LayerEvent.createEditionChangedEvent(this, "edition"));
                    this.updateDrawVersion();
                }
            }
        } else if (notification instanceof FeatureDrawnNotification
                && (isEditing() || isLayerToSnap())) {
            // This code is needed in editing mode
            // for all layers involved in snapping
            // (including the layer being edited)
            Geometry geometry
                    = ((FeatureDrawnNotification) notification).getDrawnGeometry();
            spatialCache.insert(geometry.getEnvelope(), geometry);
        }
    }

    private boolean isLayerToSnap() {

        if (this.getMapContext() == null) {
            /*
             * This happens with the graphics layer because it has no parent
             */
            return false;
        } else {
            return this.getMapContext().getLayersToSnap().contains(this);
        }

        /*
         Iterator itersnap = this.getMapContext().getLayersToSnap().iterator();
         Object item = null;
         while (itersnap.hasNext()) {
         item = itersnap.next();
         if (item == this) {
         return true;
         }
         }
         return false;
         */
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataChildren()
     */
    public Set getMetadataChildren() {
        Set ret = new TreeSet();
        ret.add(this.featureStore);
        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.gvsig.metadata.Metadata#getMetadataID()
     */
    public Object getMetadataID() throws MetadataException {
        return "Layer(" + this.getName() + "):"
                + this.featureStore.getMetadataID();
    }

    public GeometryType getTypeVectorLayer() throws DataException,
            LocatorException,
            GeometryTypeNotSupportedException,
            GeometryTypeNotValidException {
        // FIXME Esto deberia de pedirse a FType!!!!
        FeatureStore fs = this.getFeatureStore();
        FeatureType fType = fs.getDefaultFeatureType();
        FeatureAttributeDescriptor attr
                = fType.getAttributeDescriptor(fType.getDefaultGeometryAttributeIndex());
        GeometryType geomType
                = GeometryLocator.getGeometryManager()
                .getGeometryType(attr.getGeometryType(),
                        attr.getGeometrySubType());
        return geomType;
    }

    public static class RegisterPersistence implements Callable {

        public Object call() {
            PersistenceManager manager = ToolsLocator.getPersistenceManager();

            DynStruct definition
                    = manager.addDefinition(FLyrVect.class,
                            "FLyrVect",
                            "FLyrVect Persistence definition",
                            null,
                            null);
            definition.extend(PersistenceManager.PERSISTENCE_NAMESPACE,
                    "FLyrDefault");

            definition.addDynFieldObject("legend")
                    .setClassOfValue(IVectorLegend.class)
                    .setMandatory(true);
            definition.addDynFieldObject("featureStore")
                    .setClassOfValue(FeatureStore.class)
                    .setMandatory(true);
            definition.addDynFieldBoolean("isLabeled").setMandatory(true);
            definition.addDynFieldInt("typeShape").setMandatory(true);
            definition.addDynFieldObject("labelingStrategy")
                    .setClassOfValue(ILabelingStrategy.class)
                    .setMandatory(false);

            return Boolean.TRUE;
        }
    }

    protected void doDispose() throws BaseException {
        dispose(featureStore);
        spatialCache.clearAll();
    }

    /**
     * Returns envelope in layer's data source CRS from envelope provided in
     * viewport CRS
     *
     * @param lyr
     * @param env
     * @return
     */
    public static Envelope fromViewPortCRSToSourceCRS(FLayer lyr, Envelope env) {

        if (lyr == null || env == null) {
            return null;
        }

        ICoordTrans ct = lyr.getCoordTrans();
        if (ct == null) {
            return env;
        } else {
            return env.convert(ct.getInverted());
        }
    }

    public Geometry transformToSourceCRS(Geometry geom, boolean clone) {
        return fromViewPortCRSToSourceCRS(this, geom, clone);
    }

    /**
     * Returns geometry in layer's data source CRS from geometry provided in
     * viewport CRS
     *
     * @param lyr
     * @param geo
     * @param clone
     * @return
     * @deprecated use the transformToSourceCRS method of layer.
     */
    public static Geometry fromViewPortCRSToSourceCRS(
            FLayer lyr,
            Geometry geo,
            boolean clone) {

        if (lyr == null || geo == null) {
            return null;
        }
        ICoordTrans ct = lyr.getCoordTrans();
        Geometry resp = geo;
        if (clone) {
            resp = resp.cloneGeometry();
        }
        if (ct != null) {
            resp.reProject(ct.getInverted());
        }
        return resp;
    }

    public Iterator iterator() {
        return this.getFeatureStore().iterator();
    }

    protected class EnvelopeCantBeInitializedException extends BaseException {

        /**
         *
         */
        private static final long serialVersionUID = 4572797479347381552L;
        private final static String MESSAGE_FORMAT = "The envelope can't be initialized, maybe the layer has a wrong projection. Change the projection in layer properties of the add layer dialog to try fix it.";
        private final static String MESSAGE_KEY = "_EnvelopeCantBeInitializedException";

        public EnvelopeCantBeInitializedException() {
            super(MESSAGE_FORMAT, null, MESSAGE_KEY, serialVersionUID);
        }
    }

}
