/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {Create Manager to register MapContextDrawer implementation}
 */
package org.gvsig.fmap.mapcontext;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.List;

import org.cresques.cts.IProjection;

import org.gvsig.fmap.dal.DataStore;
import org.gvsig.fmap.dal.DataStoreParameters;
import org.gvsig.fmap.mapcontext.exceptions.LoadLayerException;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.vectorial.GraphicLayer;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendReader;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendWriter;
import org.gvsig.fmap.mapcontext.rendering.legend.styling.ILabelingStrategy;
import org.gvsig.fmap.mapcontext.rendering.symbols.IMultiLayerSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.IWarningSymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolManager;
import org.gvsig.tools.observer.Observable;

/**
 * Manager of the MapContext library.
 * 
 * Holds the default implementation of the {@link MapContextDrawer}.
 * 
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 * @author <a href="mailto:jjdelcerro@gvsig.org">Joaquin Jose del Cerro</a>
 */
public interface MapContextManager extends Observable {

        public static final String GET_DEFAULT_CRS = "MAPCONTEXTMANAGER_GET_DEFAULT_CRS";
        public static final String CREATE_LEGEND = "MAPCONTEXTMANAGER_CREATE_LEGEND";
        public static final String CREATE_MAPCONTEXT_DRAWER = "MAPCONTEXTMANAGER_CREATE_MAPCONTEXT_DRAWER";
        public static final String SET_MAPCONTEXT_DRAWER = "MAPCONTEXTMANAGER_SET_MAPCONTEXT_DRAWER";
        public static final String CREATE_GRAPHICS_LAYER = "MAPCONTEXTMANAGER_CREATE_GRAPHICS_LAYER";
        public static final String REGISTER_LEGEND = "MAPCONTEXTMANAGER_REGISTER_LEGEND";
        public static final String REGISTER_LEGEND_READER = "MAPCONTEXTMANAGER_REGISTER_LEGEND_READER";
        public static final String CREATE_LEGEND_READER = "MAPCONTEXTMANAGER_CREATE_LEGEND_READER";
        public static final String REGISTER_LEGEND_WRITER = "MAPCONTEXTMANAGER_REGISTER_LEGEND_WRITER";
        public static final String CREATE_SYMBOL = "MAPCONTEXTMANAGER_CREATE_SYMBOL";
        public static final String LOAD_SYMBOLS = "MAPCONTEXTMANAGER_LOAD_SYMBOLS";
        public static final String REGISTER_MULTILAYER_SYMBOL = "MAPCONTEXTMANAGER_REGISTER_MULTILAYER_SYMBOL";
        public static final String REGISTER_SYMBOL = "MAPCONTEXTMANAGER_REGISTER_SYMBOL";
        public static final String CREATE_LAYER = "MAPCONTEXTMANAGER_CREATE_LAYER";
        public static final String LOAD_LAYER = "MAPCONTEXTMANAGER_LOAD_LAYER";
        public static final String REGISTER_ICON_LAYER = "MAPCONTEXTMANAGER_REGISTER_ICON_LAYER";
        public static final String CREATE_MAPCONTEXT = "MAPCONTEXTMANAGER_CREATE_MAPCONTEXT";
        public static final String LOAD_MAPCONTEXT = "MAPCONTEXTMANAGER_LOAD_MAPCONTEXT";


	public MapContext createMapContext();
	
	/**
	 * Create a new layer from the data parameters passed as parameter.
	 * 
	 * @param layerName name used in for the new layer. 
	 * @param parameters used for create the {@link DataStore} of the new layer
	 * 
	 * @return the new FLayer
	 * 
	 * @throws LoadLayerException
	 */
	public FLayer createLayer(String layerName,
			DataStoreParameters parameters) throws LoadLayerException;

	/**
	 * Create a layer from a {@link DataStore}.
	 * 
	 * @param layerName name used in for the new layer. 
	 * @param store used for the new layer
	 * 
	 * @return the new FLayer
	 * 
	 * @throws LoadLayerException
	 */
	public FLayer createLayer(String layerName, DataStore store)
			throws LoadLayerException;

	/**
	 * Create a layer to be used as the {@link GraphicLayer}.
	 * 
	 * @param projection used in the layer.
	 * 
	 * @return the new {@link GraphicLayer}.
	 */
	public GraphicLayer createGraphicsLayer(IProjection projection);

	/**
	 * Returns the current {@link SymbolManager}.
	 * 
	 * @return the {@link SymbolManager}
	 */
	SymbolManager getSymbolManager();

	/**
	 * Sets the class to use as the default implementation for the
	 * {@link MapContextDrawer}.
	 * 
	 * @param drawerClazz
	 *            the {@link MapContextDrawer} class to use
	 * @throws MapContextException
	 *             if there is any error setting the class
	 */
	public void setDefaultMapContextDrawer(Class drawerClazz)
			throws MapContextException;

	public void validateMapContextDrawer(Class drawerClazz) throws MapContextException;

	/**
	 * Creates a new instance of the default {@link MapContextDrawer}
	 * implementation.
	 * 
	 * @return the new {@link MapContextDrawer} instance
	 * @throws MapContextException
	 *             if there is an error creating the new object instance
	 */
	public MapContextDrawer createDefaultMapContextDrawerInstance()
			throws MapContextException;

	/**
	 * Creates a new instance of the provided {@link MapContextDrawer}
	 * implementation.
	 * 
	 * @param drawerClazz
	 *            the {@link MapContextDrawer} implementation class
	 * @return the new {@link MapContextDrawer} instance
	 * @throws MapContextException
	 *             if there is an error creating the new object instance
	 */
	public MapContextDrawer createMapContextDrawerInstance(Class drawerClazz)
			throws MapContextException;

	
	
	void registerLegend(String legendName, Class legendClass)
			throws MapContextRuntimeException;

	ILegend createLegend(String legendName) throws MapContextRuntimeException;

	void setDefaultVectorLegend(String legendName);

	IVectorLegend createDefaultVectorLegend(int shapeType)
			throws MapContextRuntimeException;

    // ================================================================
	// = Legend reading/writing (GVSLEG, SLD, etc)
	
	/**
	 * Registers legend writer.
     * Format is a MIME type string. Examples:
	 * 
	 * "application/zip; subtype=gvsleg",
     * "text/xml; subtype=sld/1.0.0",
     * "text/xml; subtype=sld/1.1.0",
	 * 
	 * @param legendClass Legend class
	 * @param format File type in mime format.
	 * @param writerClass Class object of the writer
	 * @throws MapContextRuntimeException
	 */
	void registerLegendWriter(Class legendClass, String format,
			Class writerClass) throws MapContextRuntimeException;

	/**
	 * 
     * Registers legend reader.
     * Format is a MIME type string. Examples:
     * 
     * "application/zip; subtype=gvsleg",
     * "text/xml; subtype=sld/1.0.0",
     * "text/xml; subtype=sld/1.1.0",
	 * 
	 * @param format
	 * @param readerClass
	 * @throws MapContextRuntimeException
	 */
	void registerLegendReader(String format, Class readerClass)
			throws MapContextRuntimeException;

	/**
	 * Creates a legend writer for the specified legend class
	 * 
	 */
	ILegendWriter createLegendWriter(Class legendClass, String format)
			throws MapContextException;

	/**
	 * Creates a legend reader for the given format
	 * ("sld", "gvsleg", etc are extracted from the MIME long string)
	 */
	ILegendReader createLegendReader(String format)
			throws MapContextRuntimeException;
	
	/**
	 * 
     * Format is a MIME type string. Examples:
     * 
     * "application/zip; subtype=gvsleg",
     * "text/xml; subtype=sld/1.0.0",
     * "text/xml; subtype=sld/1.1.0",
	 * 
	 * @return A list of Strings with the available formats for reading
	 * legends
	 */
	List getLegendReadingFormats();

	/**
	 * 
     * Format is a MIME type string. Examples:
     * 
     * "application/zip; subtype=gvsleg",
     * "text/xml; subtype=sld/1.0.0",
     * "text/xml; subtype=sld/1.1.0",
	 * 
	 * @return A list of Strings with the available formats for writing
	 * legends
	 */
	List getLegendWritingFormats();

	
    // ================================================================

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	int getDefaultCartographicSupportMeasureUnit();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultCartographicSupportMeasureUnit(
			int defaultCartographicSupportMeasureUnit);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	int getDefaultCartographicSupportReferenceSystem();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultCartographicSupportReferenceSystem(
			int defaultCartographicSupportReferenceSystem);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	Color getDefaultSymbolColor();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultSymbolColor(Color defaultSymbolColor);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void resetDefaultSymbolColor();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	Color getDefaultSymbolFillColor();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultSymbolFillColor(Color defaultSymbolFillColor);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void resetDefaultSymbolFillColor();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	boolean isDefaultSymbolFillColorAleatory();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultSymbolFillColorAleatory(
			boolean defaultSymbolFillColorAleatory);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void resetDefaultSymbolFillColorAleatory();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	Font getDefaultSymbolFont();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setDefaultSymbolFont(Font defaultSymbolFont);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void resetDefaultSymbolFont();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	String getSymbolLibraryPath();

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void setSymbolLibraryPath(String symbolLibraryPath);

	/**
	 * @deprecated to be removed in gvSIG 2.0
	 * @see {@link SymbolPreferences}.
	 */
	void resetSymbolLibraryPath();
	
	
	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	ISymbol createSymbol(String symbolName) throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	ISymbol createSymbol(int shapeType) throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	ISymbol createSymbol(String symbolName, Color color)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	ISymbol createSymbol(int shapeType, Color color)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	IMultiLayerSymbol createMultiLayerSymbol(String symbolName)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	IMultiLayerSymbol createMultiLayerSymbol(int shapeType)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	void registerSymbol(String symbolName, Class symbolClass)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	void registerSymbol(String symbolName, int[] shapeTypes, Class symbolClass)
			throws MapContextException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	void registerMultiLayerSymbol(String symbolName, Class symbolClass)
			throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	void registerMultiLayerSymbol(String symbolName, int[] shapeTypes,
			Class symbolClass) throws MapContextRuntimeException;

	/**
	 * @deprecated to be removed in gvSIG 2.0 @see {@link SymbolManager}
	 */
	IWarningSymbol getWarningSymbol(String message, String symbolDesc,
			int symbolDrawExceptionType) throws MapContextRuntimeException;
	
	/**
	 * It returns the legend associated with a {@link DataStore}.
	 * If the legend doesn't exist it returns <code>null</code>.
	 * @param dataStore
	 * the store that could have a legend.
	 * @return
	 * the legend or <code>null</code>.
	 */
	ILegend getLegend(DataStore dataStore);	
	
	/**
     * It returns the labeling strategy associated with a {@link DataStore}.
     * If the labeling strategy doesn't exist it returns <code>null</code>.
     * @param dataStore
     * the store that could have a labeling strategy.
     * @return
     * the labeling strategy or <code>null</code>.
     */
	ILabelingStrategy getLabelingStrategy(DataStore dataStore);


	void registerIconLayer(String store, String iconName);
	
	String getIconLayer(DataStore store);
	
    // TODO:
    // DynObjectModel getFeatureTypeUIModel(DataStore store,
    // FeatureType featureType);
	
	/**
	 * Returns the default CRS.
	 * This is NOT taken from the app
	 * preferences because this is a library. It is a
	 * "hard-coded" default CRS, used as a last resort.
	 * @return the default CRS
	 */
	IProjection getDefaultCRS();
        
        public File getColorTableLibraryFolder();
        
        public void setColorTableLibraryFolder(File colorTableLibraryFolder);
        
}