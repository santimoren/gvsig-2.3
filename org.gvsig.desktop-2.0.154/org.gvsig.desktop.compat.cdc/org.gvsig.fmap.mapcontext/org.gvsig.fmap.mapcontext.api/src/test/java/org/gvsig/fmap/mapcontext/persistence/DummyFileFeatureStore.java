/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us at info AT
 * gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.persistence;

import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.fmap.dal.feature.DummyFetureStore;

public class DummyFileFeatureStore extends DummyFetureStore implements FeatureStore {

    private String name = "[empty]";

    public DummyFileFeatureStore() {

    }

    public DummyFileFeatureStore(String id) {
        name = "[FILE FEATURE STORE - " + id + "]";
    }

    public String getName() {
        return name;
    }

    public void loadFromState(PersistentState state)
            throws PersistenceException {
        name = state.getString("name");
    }

    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("name", name);
    }

    public static void registerPersistent() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynStruct definition = manager.addDefinition(
                DummyFileFeatureStore.class,
                "DummyFileFeatureStore",
                "DummyFileFeatureStore Persistence definition",
                null,
                null
        );
        definition.addDynFieldString("name")
                .setMandatory(true);
    }

}
