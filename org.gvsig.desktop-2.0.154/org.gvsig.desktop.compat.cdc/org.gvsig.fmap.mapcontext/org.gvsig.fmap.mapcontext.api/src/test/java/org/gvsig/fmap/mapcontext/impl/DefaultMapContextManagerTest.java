/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {DiSiD Technologies}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.impl;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.cresques.cts.ICoordTrans;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.crs.CRSFactory;
import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.mapcontext.MapContext;
import org.gvsig.fmap.mapcontext.MapContextDrawer;
import org.gvsig.fmap.mapcontext.MapContextException;
import org.gvsig.fmap.mapcontext.MapContextRuntimeException;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.exceptions.IncompatibleLegendReadException;
import org.gvsig.fmap.mapcontext.exceptions.ReadLegendException;
import org.gvsig.fmap.mapcontext.exceptions.WriteLegendException;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.ISingleSymbolLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorialIntervalLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.LegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendReader;
import org.gvsig.fmap.mapcontext.rendering.legend.driver.ILegendWriter;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;

/**
 * @author <a href="mailto:cordinyana@gvsig.org">C�sar Ordi�ana</a>
 */
public class DefaultMapContextManagerTest extends
AbstractLibraryAutoInitTestCase {

    private DefaultMapContextManager manager;

    protected void doSetUp() throws Exception {
        manager = new DefaultMapContextManager();

        manager.registerLegend("DummyLegend", DummyVectorLegend.class);
        manager.setDefaultVectorLegend("DummyLegend");
        manager.registerLegendReader("Dummy", DummyLegendReader.class);
        // manager.registerLegendWriter("DummyLegend".getClass(), "Dummy", DummyLegendWriter.class);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for
     * {@link org.gvsig.fmap.mapcontext.impl.DefaultMapContextManager#createDefaultMapContextDrawerInstance()}
     * .
     */
    public void testGetDefaultMapContextDrawerInstance() throws Exception {

        manager.setDefaultMapContextDrawer(DummyMapContextDrawer.class);

        MapContextDrawer drawer = manager
        .createDefaultMapContextDrawerInstance();

        assertNotNull("The created MapContextDrawer instance can't be null",
            drawer);
        assertTrue(
            "The created MapContextDrawer is not instance of the registered class"
            + DummyMapContextDrawer.class,
            drawer instanceof DummyMapContextDrawer);
    }

    /**
     * Test method for
     * {@link org.gvsig.fmap.mapcontext.impl.DefaultMapContextManager#setDefaultMapContextDrawer(java.lang.Class)}
     * .
     */
    public void testSetDefaultMapContextDrawer() throws Exception {

        // First, try to register an invalid class
        try {
            manager.setDefaultMapContextDrawer(Object.class);
            fail("Error, a class that does not implement the MapContextDrawer "
                + "interface has been accepted");
        } catch (MapContextRuntimeException e) {
            // OK
        }

        // Next, try to register a valid class
        manager.setDefaultMapContextDrawer(DummyMapContextDrawer.class);
    }

    public void testCreateGraphicsLayer() throws Exception {
        assertNotNull(manager.createGraphicsLayer(CRSFactory.getCRS("EPSG:23030")));
    }

    public void testCreateDefaultVectorLegend() throws Exception {

        IVectorLegend legend = manager.createDefaultVectorLegend(1);
        assertNotNull(legend);
        assertTrue(legend instanceof DummyVectorLegend);
    }

    public void testRegisterCreateLegend() throws Exception {

        manager.registerLegend("DummyLegend", DummyVectorLegend.class);

        ILegend legend = manager.createLegend("DummyLegend");
        assertNotNull(legend);
        assertTrue(legend instanceof DummyVectorLegend);

        assertNull(manager.createLegend("NONE"));
    }

    public void testRegisterCreateLegendReader() throws Exception {

        manager.registerLegendReader("Dummy", DummyLegendReader.class);

        ILegendReader legendReader = manager.createLegendReader("Dummy");
        assertNotNull(legendReader);
        assertTrue(legendReader instanceof DummyLegendReader);

        assertNull(manager.createLegendReader("NONE"));
    }

    public void testRegisterCreateLegendWriter() throws Exception {

        manager.registerLegend("DummyLegend", DummyVectorLegend.class);

        manager.registerLegendWriter(ISingleSymbolLegend.class, "Dummy",
            DummyLegendWriter.class);

        // Test the registered writer is created
        ILegendWriter legendWriter = manager.createLegendWriter(
            ISingleSymbolLegend.class, "Dummy");
        assertNotNull(legendWriter);
        assertTrue(legendWriter instanceof DummyLegendWriter);

        // Test non registered cases
        assertNull(manager.createLegendWriter(IVectorialIntervalLegend.class, "Dummy"));
        assertNull(manager.createLegendWriter(IVectorialIntervalLegend.class, "NONE"));
        assertNull(manager.createLegendWriter(IVectorialIntervalLegend.class, "NONE"));
    }

    public static class DummyMapContextDrawer implements MapContextDrawer {

        public void dispose() {
        }

        public void draw(FLayers root, BufferedImage image, Graphics2D g,
            Cancellable cancel, double scale) throws ReadException {
        }

        public void print(FLayers root, Graphics2D g, Cancellable cancel,
            double scale, PrintAttributes properties) throws ReadException {
        }

        public void setMapContext(MapContext mapContext) {
        }

        public void setViewPort(ViewPort viewPort) {
        }

    }

    public static class DummyLegendReader implements ILegendReader {

        public ILegend read(File inFile, int geometryType)
            throws ReadLegendException, IncompatibleLegendReadException,
            IOException {
            // TODO Auto-generated method stub
            return null;
        }

    }

    public static class DummyLegendWriter implements ILegendWriter {

        public void write(ILegend legend, File outFile, String format)
            throws WriteLegendException, IOException {
            // TODO Auto-generated method stub
            
        }

    }

    public static class DummyVectorLegend implements IVectorLegend {

        public void draw(BufferedImage image, Graphics2D graphics2d,
            ViewPort viewPort, Cancellable cancel, double scale,
            Map queryParameters, ICoordTrans coordTrans,
            FeatureStore featureStore, FeatureQuery featureQuery) throws LegendException {
            // Empty method
        }

        public void draw(BufferedImage image, Graphics2D graphics2d,
            ViewPort viewPort, Cancellable cancel, double scale,
            Map queryParameters, ICoordTrans coordTrans,
            FeatureStore featureStore) throws LegendException {
            // Empty method
        }

        public int getShapeType() {
            // Empty method
            return 0;
        }

        public ISymbol getSymbolByFeature(Feature feat)
        throws MapContextException {
            // Empty method
            return null;
        }

        public boolean isSuitableForShapeType(int shapeType) {
            // Empty method
            return false;
        }

        public boolean isUseDefaultSymbol() {
            // Empty method
            return false;
        }

        public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
            double scale, Map queryParameters, ICoordTrans coordTrans,
            FeatureStore featureStore, PrintAttributes properties)
        throws LegendException {
            // Empty method
        }

        public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
            double scale, Map queryParameters, ICoordTrans coordTrans,
            FeatureStore featureStore, FeatureQuery featureQuery, PrintAttributes properties)
        throws LegendException {
            // Empty method
        }

        public void setDefaultSymbol(ISymbol s) {
            // Empty method
        }

        public void setShapeType(int shapeType) {
            // Empty method
        }

        public void useDefaultSymbol(boolean b) {
            // Empty method
        }

        public void addLegendListener(LegendContentsChangedListener listener) {
            // Empty method
        }

        public ILegend cloneLegend() {
            // Empty method
            return null;
        }

        public Object clone() throws CloneNotSupportedException {
            // TODO Auto-generated method stub
            return super.clone();
        }

        public void fireDefaultSymbolChangedEvent(SymbolLegendEvent event) {
            // Empty method
        }

        public ISymbol getDefaultSymbol() {
            // Empty method
            return null;
        }

        public LegendContentsChangedListener[] getListeners() {
            // Empty method
            return null;
        }

        public void removeLegendListener(LegendContentsChangedListener listener) {
            // Empty method
        }

        public void loadFromState(PersistentState state)
        throws PersistenceException {
            // Empty method
        }

        public void saveToState(PersistentState state)
        throws PersistenceException {
            // Empty method
        }

        public void addDrawingObserver(Observer observer) {
            // Empty method
        }

        public void deleteDrawingObserver(Observer observer) {
            // Empty method
        }

        public void deleteDrawingObservers() {
            // Empty method
        }
    }
}