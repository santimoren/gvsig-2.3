/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.Font;

import org.easymock.MockControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;

/**
 * Unit tests for the class {@link FontPersistenceFactory}.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class FontPersistenceFactoryTest extends AbstractLibraryAutoInitTestCase {

	private FontPersistenceFactory factory;
	private MockControl stateControl;
	private PersistentState state;
	private Font font;

	protected void doSetUp() throws Exception {
		PersistenceManager persistenceManager = ToolsLocator
				.getPersistenceManager();
		factory = (FontPersistenceFactory) persistenceManager.getFactories()
				.get(Font.class);
		stateControl = MockControl.createNiceControl(PersistentState.class);
		state = (PersistentState) stateControl.getMock();
		font = new Font("Arial", Font.BOLD, 18);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.FontPersistenceFactory#createFromState(org.gvsig.tools.persistence.PersistentState, java.lang.Class)}
	 * .
	 */
	public void testCreateFromState() throws Exception {
		stateControl.expectAndReturn(state
				.getString(FontPersistenceFactory.FIELD_NAME), font.getName());
		stateControl.expectAndReturn(state
				.getInt(FontPersistenceFactory.FIELD_STYLE), font.getStyle());
		stateControl.expectAndReturn(state
				.getInt(FontPersistenceFactory.FIELD_SIZE), font.getSize());
		stateControl.replay();

		Font newFont = (Font) factory.createFromState(state);
		assertTrue(newFont.equals(font));

		stateControl.verify();
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.FontPersistenceFactory#saveToState(org.gvsig.tools.persistence.PersistentState, java.lang.Object)}
	 * .
	 */
	public void testSaveToState() throws Exception {
		state.set(FontPersistenceFactory.FIELD_NAME, font.getName());
		state.set(FontPersistenceFactory.FIELD_STYLE, font.getStyle());
		state.set(FontPersistenceFactory.FIELD_SIZE, font.getSize());
		stateControl.replay();

		factory.saveToState(state, font);

		stateControl.verify();
	}

}