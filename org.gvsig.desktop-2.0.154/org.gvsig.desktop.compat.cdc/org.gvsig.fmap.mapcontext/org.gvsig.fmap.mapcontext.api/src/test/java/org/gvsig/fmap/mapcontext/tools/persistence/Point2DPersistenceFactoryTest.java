/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.geom.Point2D;

import org.easymock.MockControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;

/**
 * Unit tests for the class {@link Point2DPersistenceFactory}.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class Point2DPersistenceFactoryTest extends
		AbstractLibraryAutoInitTestCase {

	private Point2DPersistenceFactory factory;
	private MockControl stateControl;
	private PersistentState state;
	private Point2D point;

	protected void doSetUp() throws Exception {
		PersistenceManager persistenceManager = ToolsLocator
				.getPersistenceManager();
		factory = (Point2DPersistenceFactory) persistenceManager.getFactories()
				.get(Point2D.class);
		stateControl = MockControl.createNiceControl(PersistentState.class);
		state = (PersistentState) stateControl.getMock();
		double x = Math.random() * 1000d;
		double y = Math.random() * 1000d;
		point = new Point2D.Double(x, y);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.Point2DPersistenceFactory#createFromState(PersistentState, Class)}
	 * .
	 */
	public void testCreateFromState() throws Exception {
		stateControl.expectAndReturn(state
				.getDouble(Point2DPersistenceFactory.FIELD_X), point.getX());
		stateControl.expectAndReturn(state
				.getDouble(Point2DPersistenceFactory.FIELD_Y), point.getY());
		stateControl.replay();

		Point2D newPoint = (Point2D) factory.createFromState(state);
		assertTrue(newPoint.equals(point));

		stateControl.verify();
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.Point2DPersistenceFactory#saveToState(org.gvsig.tools.persistence.PersistentState, java.lang.Object)}
	 * .
	 */
	public void testSaveToState() throws Exception {
		state.set(Point2DPersistenceFactory.FIELD_X, point.getX());
		state.set(Point2DPersistenceFactory.FIELD_Y, point.getY());
		stateControl.replay();

		factory.saveToState(state, point);

		stateControl.verify();
	}
}