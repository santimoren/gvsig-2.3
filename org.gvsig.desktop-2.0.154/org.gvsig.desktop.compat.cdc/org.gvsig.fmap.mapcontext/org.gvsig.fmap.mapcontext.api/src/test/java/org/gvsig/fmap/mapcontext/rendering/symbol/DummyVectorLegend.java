/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbol;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Map;

import org.cresques.cts.ICoordTrans;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.exception.DataException;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.dal.feature.FeatureQuery;
import org.gvsig.fmap.dal.feature.FeatureSet;
import org.gvsig.fmap.dal.feature.FeatureStore;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.legend.ILegend;
import org.gvsig.fmap.mapcontext.rendering.legend.IVectorLegend;
import org.gvsig.fmap.mapcontext.rendering.legend.LegendException;
import org.gvsig.fmap.mapcontext.rendering.legend.events.LegendContentsChangedListener;
import org.gvsig.fmap.mapcontext.rendering.legend.events.SymbolLegendEvent;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.evaluator.Evaluator;
import org.gvsig.tools.observer.Observer;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;

public class DummyVectorLegend implements IVectorLegend {

    private long shpType = -1;

    private ISymbol sym = new DummySymbol(Color.RED, TYPES.SOLID);

    public DummyVectorLegend() {
        shpType = System.currentTimeMillis() % 1000000;
    }

    public DummyVectorLegend(int type) {
        shpType = type; // System.currentTimeMillis() % 1000000;
    }

    public void draw(BufferedImage image, Graphics2D graphics2D,
        ViewPort viewPort, Cancellable cancel, double scale,
        Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, FeatureQuery featureQuery) throws LegendException {

    }
    public void draw(BufferedImage image, Graphics2D graphics2D,
        ViewPort viewPort, Cancellable cancel, double scale,
        Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore) throws LegendException {

        try {
            FeatureSet fs = featureStore.getFeatureSet();
            Iterator iter = fs.iterator();
            Feature feat = null;
            ISymbol symb = null;
            while (iter.hasNext()) {
                feat = (Feature) iter.next();
                symb = this.getSymbolByFeature(feat);
                symb.draw(graphics2D, viewPort.getAffineTransform(), feat.getDefaultGeometry(), feat, null);
            }
        } catch (DataException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // TODO Auto-generated method stub

    }

    public int getShapeType() {
        // TODO Auto-generated method stub
        return (int) shpType;
    }

    public ISymbol getSymbolByFeature(Feature feat) {
        return sym;
    }

    public boolean isSuitableForShapeType(int shapeType) {
        return sym.getSymbolType() == shapeType;
    }

    public boolean isUseDefaultSymbol() {
        return true;
    }

    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Object object, ICoordTrans coordTrans,
        FeatureStore featureStore, Evaluator evaluator, PrintAttributes properties)
    throws LegendException {
        // TODO Auto-generated method stub

    }

    public void setDefaultSymbol(ISymbol s) throws IllegalArgumentException {
        sym = s;
    }

    public void setShapeType(int shapeType) {
        shpType = shapeType;
    }

    public void useDefaultSymbol(boolean b) {
    }

    public void addLegendListener(LegendContentsChangedListener listener) {
        // TODO Auto-generated method stub

    }

    public ILegend cloneLegend() {
        // TODO Auto-generated method stub
        return null;
    }

    public void fireDefaultSymbolChangedEvent(SymbolLegendEvent event) {
        // TODO Auto-generated method stub

    }

    public ISymbol getDefaultSymbol() {
        return sym;
    }

    public LegendContentsChangedListener[] getListeners() {
        // TODO Auto-generated method stub
        return null;
    }

    public void removeLegendListener(LegendContentsChangedListener listener) {
        // TODO Auto-generated method stub

    }

    public String getClassName() {
        return this.getClass().getName();
    }




    public void addDrawingObserver(Observer observer) {
        // TODO Auto-generated method stub

    }

    public void deleteDrawingObserver(Observer observer) {
        // TODO Auto-generated method stub

    }

    public void deleteDrawingObservers() {
        // TODO Auto-generated method stub

    }

    public void loadFromState(PersistentState state) throws PersistenceException {
        shpType = state.getLong("shpType");
    }


    public void saveToState(PersistentState state) throws PersistenceException {
        state.set("shpType", shpType);
    }

    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, FeatureQuery featureQuery, PrintAttributes properties)
    throws LegendException {
        // TODO Auto-generated method stub

    }

    public void print(Graphics2D g, ViewPort viewPort, Cancellable cancel,
        double scale, Map queryParameters, ICoordTrans coordTrans,
        FeatureStore featureStore, PrintAttributes properties)
    throws LegendException {
        // TODO Auto-generated method stub

    }

    public static void registerPersistent() {
        PersistenceManager manager = ToolsLocator.getPersistenceManager();
        DynStruct definition = manager.addDefinition(
            DummyVectorLegend.class,
            "DummyVectorLegend",
            "DummyVectorLegend Persistence definition",
            null, 
            null
        );
        definition.addDynFieldLong("shpType")
        .setMandatory(true);
    }


    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
