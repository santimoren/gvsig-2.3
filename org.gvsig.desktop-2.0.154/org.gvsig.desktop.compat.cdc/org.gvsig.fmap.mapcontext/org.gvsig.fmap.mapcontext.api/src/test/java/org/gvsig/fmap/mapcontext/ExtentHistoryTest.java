/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import junit.framework.TestCase;

/**
 * Unit tests for the class {@link ExtentHistory}.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class ExtentHistoryTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.ExtentHistory#put(java.awt.geom.Rectangle2D)}
	 * .
	 */
	public void testPut() {
		ExtentHistory history = new ExtentHistory();

		Rectangle2D rectangle = new Rectangle2D.Double(1, 1, 2, 2);
		Rectangle2D rectangle2 = new Rectangle2D.Double(2, 2, 3, 3);
		Rectangle2D rectangle3 = new Rectangle2D.Double(3, 3, 4, 4);
		history.put(rectangle3);
		history.put(rectangle2);
		history.put(rectangle);

		assertEquals(rectangle2, history.getPrev());

		history = new ExtentHistory(2);

		history.put(rectangle);
		history.put(rectangle2);
		history.put(rectangle3);

		assertEquals(rectangle2, history.getPrev());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.ExtentHistory#hasPrevious()}.
	 */
	public void testHasPrevious() {
		ExtentHistory history = new ExtentHistory();

		assertFalse(history.hasPrevious());

		Rectangle2D rectangle = new Rectangle2D.Double(1, 1, 2, 2);
		history.put(rectangle);
		assertFalse(history.hasPrevious());
 
		Rectangle2D rectangle2 = new Rectangle2D.Double(2, 2, 3, 3);
    history.put(rectangle2);
		assertTrue(history.hasPrevious());

		history.setPreviousExtent();
		assertFalse(history.hasPrevious());
	}

	 /**
   * Test method for
   * {@link org.gvsig.fmap.mapcontext.ExtentHistory#hasNext()}.
   */
  public void testHasNext() {
    ExtentHistory history = new ExtentHistory();

    assertFalse(history.hasNext());

    Rectangle2D rectangle = new Rectangle2D.Double(1, 1, 2, 2);
    history.put(rectangle);

    history.setPreviousExtent();
    assertTrue(history.hasNext());
  }

	/**
	 * Test method for {@link org.gvsig.fmap.mapcontext.ExtentHistory#getPrev()}.
	 */
	public void testGet() {
		ExtentHistory history = new ExtentHistory();

		assertNull("The ExtentHistory has got an element without adding one",
				history.getPrev());

		Rectangle2D rectangle = new Rectangle2D.Double(1, 1, 2, 2);
		Rectangle2D rectangle2 = new Rectangle2D.Double(2, 2, 3, 3);

		history.put(rectangle);
		assertEquals(rectangle, history.getCurrent());

		history.put(rectangle2);
		assertEquals(rectangle2, history.getCurrent());
		assertEquals(rectangle, history.getPrev());

		assertEquals(rectangle, history.setPreviousExtent());

		assertNull("The ExtentHistory hasn't got a previous element", history.setPreviousExtent());
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.ExtentHistory#removePrev()}.
	 */
	public void testRemovePrev() {
		ExtentHistory history = new ExtentHistory();

		assertNull(
				"The ExtentHistory allows setting an element without adding "
						+ "at least one", history.setPreviousExtent());

		Rectangle2D rectangle = new Rectangle2D.Double(1, 1, 2, 2);
		Rectangle2D rectangle2 = new Rectangle2D.Double(2, 2, 3, 3);

		history.put(rectangle);
		history.put(rectangle2);

		assertEquals(rectangle, history.setPreviousExtent());

		assertNull("The ExtentHistory allows to set the last element, "
				+ "but all have been previously removed", history.setPreviousExtent());
	}
}