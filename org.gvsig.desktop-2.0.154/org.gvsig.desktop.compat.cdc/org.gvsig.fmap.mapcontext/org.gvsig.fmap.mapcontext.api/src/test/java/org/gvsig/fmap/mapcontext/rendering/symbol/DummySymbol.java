/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.rendering.symbol;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;

import org.gvsig.compat.print.PrintAttributes;
import org.gvsig.fmap.dal.feature.Feature;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.primitive.Point;
import org.gvsig.fmap.mapcontext.ViewPort;
import org.gvsig.fmap.mapcontext.rendering.symbols.ISymbol;
import org.gvsig.fmap.mapcontext.rendering.symbols.SymbolDrawingException;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.gvsig.tools.task.Cancellable;

public class DummySymbol implements ISymbol {

	private Color color = Color.GREEN;
	private int type = TYPES.SOLID;
	
	public DummySymbol(Color c, int geomtype) {
		color = c;
		type = geomtype;
	}
	
	public Object clone() {
		return null;
	}
	
	public void draw(Graphics2D g, AffineTransform affineTransform,
			Geometry geom, Feature f, Cancellable cancel) {
		
		g.setColor(color);
		
		if (geom instanceof Point) {
			Point p = (Point) geom;
			p.transform(affineTransform);
			g.drawRect((int) p.getX(), (int) p.getY(), 3, 3);
		} else {
			PathIterator piter = geom.getPathIterator(affineTransform); 
			drawPathIterator(g, piter);
		}
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r,
			PrintAttributes properties) throws SymbolDrawingException {
		// TODO Auto-generated method stub

	}

	public Color getColor() {
		return color;
	}

	public String getDescription() {
		return "a dummy symbol";
	}

	public int getOnePointRgb() {
		return color.getRGB();
	}

	public void getPixExtentPlus(Geometry geom, float[] distances,
			ViewPort viewPort, int dpi) {
		// TODO Auto-generated method stub

	}

	public ISymbol getSymbolForSelection() {
		return new DummySymbol(Color.YELLOW, type);
	}

	public int getSymbolType() {
		return type;
	}

	public boolean isOneDotOrPixel(Geometry geom,
			double[] positionOfDotOrPixel, ViewPort viewPort, int dpi) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isShapeVisible() {
		return true;
	}

	public boolean isSuitableFor(Geometry geom) {
		return type == geom.getType();
	}

	public void setColor(Color c) {
		color = c;
	}

	public void setDescription(String desc) {
	}

	public void loadFromState(PersistentState state)
			throws PersistenceException {
	}

	public void saveToState(PersistentState state) throws PersistenceException {
	}

	public void print(Graphics2D g, AffineTransform at, Geometry shape,
			PrintAttributes properties) {
	}
	
	
	
	/**
	 * Draws the general path on the graphics object with the given affine transformation
	 * @param g the graphics object
	 * @param gp the general path
	 */
	public static void drawPathIterator(Graphics2D g, PathIterator pit) {
		
		ArrayList x = new ArrayList();
		ArrayList y = new ArrayList();
		double[] current = new double[6];
		
		while (!pit.isDone()) {
			pit.currentSegment(current);
			x.add(new Integer((int) current[0]));
			y.add(new Integer((int) current[1]));
			pit.next();
		}
		
		int[] gx = integerArrayListToIntArray(x);
		int[] gy = integerArrayListToIntArray(y);

		g.drawPolyline(gx, gy, gx.length);
	}
	
	
	/**
	 * Converts array list of Integer objects into array of int
	 * @param l
	 * @return array of int
	 */
	public static int[] integerArrayListToIntArray(ArrayList l) {

		int size = l.size();
		int[] resp = new int[size];
		for (int i=0; i<size; i++) {
			resp[i] = ((Integer) l.get(i)).intValue();
		}
		return resp;
	}

}
