/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.geom.Rectangle2D;

import org.easymock.MockControl;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;

/**
 * Unit tests for the class {@link Rectangle2DPersistenceFactory}.
 * 
 * @author 2009- <a href="cordinyana@gvsig.org">C�sar Ordi�ana</a> - gvSIG team
 */
public class Rectangle2DPersistenceFactoryTest extends
		AbstractLibraryAutoInitTestCase {

	private Rectangle2DPersistenceFactory factory;
	private MockControl stateControl;
	private PersistentState state;
	private Rectangle2D rectangle;

	protected void doSetUp() throws Exception {
		PersistenceManager persistenceManager = ToolsLocator
				.getPersistenceManager();
		factory = (Rectangle2DPersistenceFactory) persistenceManager
				.getFactories().get(Rectangle2D.class);
		stateControl = MockControl.createNiceControl(PersistentState.class);
		state = (PersistentState) stateControl.getMock();
		double x = Math.random() * 1000d;
		double y = Math.random() * 1000d;
		rectangle = new Rectangle2D.Double(x, y, 320, 200);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.Rectangle2DPersistenceFactory#createFromState(PersistentState, Class)}
	 * .
	 */
	public void testCreateFromState() throws Exception {
		stateControl.expectAndReturn(state
				.getDouble(Rectangle2DPersistenceFactory.FIELD_X), rectangle
				.getX());
		stateControl.expectAndReturn(state
				.getDouble(Rectangle2DPersistenceFactory.FIELD_Y), rectangle
				.getY());
		stateControl.expectAndReturn(state
				.getDouble(Rectangle2DPersistenceFactory.FIELD_WIDTH),
				rectangle.getWidth());
		stateControl.expectAndReturn(state
				.getDouble(Rectangle2DPersistenceFactory.FIELD_HEIGHT),
				rectangle.getHeight());
		stateControl.replay();

		Rectangle2D newRectangle = (Rectangle2D) factory.createFromState(state);

		System.out.println("Orig x = " + rectangle.getX() + " - New x = "
				+ newRectangle.getX());
		assertEquals(rectangle.getX(), newRectangle.getX(), 0.0d);
		System.out.println("Orig y = " + rectangle.getY() + " - New y = "
				+ newRectangle.getY());
		assertEquals(rectangle.getY(), newRectangle.getY(), 0.0d);
		System.out.println("Orig width = " + rectangle.getWidth()
				+ " - New width = " + newRectangle.getWidth());
		assertEquals(rectangle.getWidth(), newRectangle.getWidth(), 0.0d);
		System.out.println("Orig height = " + rectangle.getHeight()
				+ " - New height = " + newRectangle.getHeight());
		assertEquals(rectangle.getHeight(), newRectangle.getHeight(), 0.0d);

		assertTrue(newRectangle.equals(rectangle));

		stateControl.verify();
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.Rectangle2DPersistenceFactory#saveToState(org.gvsig.tools.persistence.PersistentState, java.lang.Object)}
	 * .
	 */
	public void testSaveToState() throws Exception {
		state.set(Rectangle2DPersistenceFactory.FIELD_X, rectangle.getX());
		state.set(Rectangle2DPersistenceFactory.FIELD_Y, rectangle.getY());
		state.set(Rectangle2DPersistenceFactory.FIELD_WIDTH, rectangle
				.getWidth());
		state.set(Rectangle2DPersistenceFactory.FIELD_HEIGHT, rectangle
				.getHeight());
		stateControl.replay();

		factory.saveToState(state, rectangle);

		stateControl.verify();
	}
}