/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
/*
 * AUTHORS (In addition to CIT):
 * 2009 {}  {{Task}}
 */
package org.gvsig.fmap.mapcontext.tools.persistence;

import java.awt.Dimension;

import org.easymock.MockControl;

import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.junit.AbstractLibraryAutoInitTestCase;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;

/**
 * Unit tests for the class {@link DimensionPersistenceFactory}.
 * 
 * @author gvSIG team
 */
public class DimensionPersistenceFactoryTest extends
		AbstractLibraryAutoInitTestCase {

	private DimensionPersistenceFactory factory;
	private MockControl stateControl;
	private PersistentState state;
	private Dimension dimension;

	protected void doSetUp() throws Exception {
		PersistenceManager persistenceManager = ToolsLocator
				.getPersistenceManager();
		factory = (DimensionPersistenceFactory) persistenceManager
				.getFactories().get(Dimension.class);
		stateControl = MockControl.createNiceControl(PersistentState.class);
		state = (PersistentState) stateControl.getMock();
		dimension = new Dimension(320, 200);
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.DimensionPersistenceFactory#createFromState(org.gvsig.tools.persistence.PersistentState, java.lang.Class)}
	 * .
	 */
	public void testCreateFromState() throws Exception {
		stateControl.expectAndReturn(state
				.getInt(DimensionPersistenceFactory.FIELD_WIDTH), dimension.width);			
		stateControl.expectAndReturn(state
				.getInt(DimensionPersistenceFactory.FIELD_HEIGHT), dimension.height);				
		stateControl.replay();

		Dimension newDimension = (Dimension) factory.createFromState(state);
		assertTrue(newDimension.equals(dimension));

		stateControl.verify();
	}

	/**
	 * Test method for
	 * {@link org.gvsig.fmap.mapcontext.tools.persistence.DimensionPersistenceFactory#saveToState(org.gvsig.tools.persistence.PersistentState, java.lang.Object)}
	 * .
	 */
	public void testSaveToState() throws Exception {
		state.set(DimensionPersistenceFactory.FIELD_WIDTH, dimension.width);
		state.set(DimensionPersistenceFactory.FIELD_HEIGHT, dimension.height);
				
		stateControl.replay();

		factory.saveToState(state, dimension);

		stateControl.verify();
	}

}