/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.order.impl;

import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;

public class TrivialLayerOrderManager implements LayerOrderManager {

	public static final String TRIVIAL_LAYER_ORDER_MANAGER_PERSISTENCE_NAME =
			"TRIVIAL_LAYER_ORDER_MANAGER_PERSISTENCE_NAME";

	public TrivialLayerOrderManager() {
		
	}
	
	public int getPosition(FLayers target, FLayer newLayer) {
		/*
		 * Always on top
		 */
		return target.getLayersCount();
	}

	public String getName() {
		return Messages.getText("_Default_order");
	}

	public String getDescription() {
		return Messages.getText("_Layers_are_placed_always_on_top");
	}

	public String getCode() {
		return this.getClass().getName();
	}
	
	public void loadFromState(PersistentState state) throws PersistenceException {
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("name", this.getName());
	}
	
	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if (manager.getDefinition(TRIVIAL_LAYER_ORDER_MANAGER_PERSISTENCE_NAME) == null) {
			DynStruct def = manager.addDefinition(
					TrivialLayerOrderManager.class,
					TRIVIAL_LAYER_ORDER_MANAGER_PERSISTENCE_NAME,
					TRIVIAL_LAYER_ORDER_MANAGER_PERSISTENCE_NAME +" Persistence definition",
					null, 
					null);
			def.addDynFieldString("name").setMandatory(true);
		}			
	}
	
	public Object clone() throws CloneNotSupportedException {
		return new TrivialLayerOrderManager();
	}

}
