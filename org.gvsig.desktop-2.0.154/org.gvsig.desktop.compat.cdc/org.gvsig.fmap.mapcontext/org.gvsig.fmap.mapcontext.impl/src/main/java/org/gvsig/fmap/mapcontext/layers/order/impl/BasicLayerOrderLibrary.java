/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */

package org.gvsig.fmap.mapcontext.layers.order.impl;

import org.gvsig.fmap.mapcontext.MapContextLibrary;
import org.gvsig.fmap.mapcontext.MapContextLocator;
import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.library.AbstractLibrary;
import org.gvsig.tools.library.LibraryException;


/**
 * Registers the default implementation for {@link LayerOrderManager}
 * 
 * @author <a href="mailto:jpiera@gvsig.org">Jorge Piera</a>
 */
public class BasicLayerOrderLibrary extends AbstractLibrary {

    public void doRegistration() {
        require(MapContextLibrary.class);
    }

    protected void doInitialize() throws LibraryException {
        Messages.addResourceFamily(
        		"org.gvsig.fmap.mapcontext.impl.i18n.text",
        		BasicLayerOrderLibrary.class.getClassLoader(),
        		BasicLayerOrderLibrary.class.getClass().getName());
        
    	TrivialLayerOrderManager.registerPersistent();
    	RasterPolLinePointOrderManager.registerPersistent();

        MapContextLocator.registerDefaultOrderManager(
    			TrivialLayerOrderManager.class);
    	MapContextLocator.registerOrderManager(
    			RasterPolLinePointOrderManager.class);
    }

    protected void doPostInitialize() throws LibraryException {  	

    }
}
