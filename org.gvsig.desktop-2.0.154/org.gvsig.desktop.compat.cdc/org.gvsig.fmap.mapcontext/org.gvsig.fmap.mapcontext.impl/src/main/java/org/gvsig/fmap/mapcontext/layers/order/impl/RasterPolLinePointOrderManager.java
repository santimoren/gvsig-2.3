/**
 * gvSIG. Desktop Geographic Information System.
 *
 * Copyright (C) 2007-2013 gvSIG Association.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 * For any additional information, do not hesitate to contact us
 * at info AT gvsig.com, or visit our website www.gvsig.com.
 */
package org.gvsig.fmap.mapcontext.layers.order.impl;

import java.util.Map;

import org.gvsig.fmap.dal.exception.ReadException;
import org.gvsig.fmap.geom.Geometry;
import org.gvsig.fmap.geom.Geometry.TYPES;
import org.gvsig.fmap.geom.type.GeometryType;
import org.gvsig.fmap.mapcontext.layers.FLayer;
import org.gvsig.fmap.mapcontext.layers.FLayers;
import org.gvsig.fmap.mapcontext.layers.order.LayerOrderManager;
import org.gvsig.fmap.mapcontext.layers.vectorial.FLyrVect;
import org.gvsig.i18n.Messages;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.dynobject.DynStruct;
import org.gvsig.tools.persistence.PersistenceManager;
import org.gvsig.tools.persistence.PersistentState;
import org.gvsig.tools.persistence.exception.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RasterPolLinePointOrderManager implements LayerOrderManager {

	public static final String RASTER_POLYGON_LINE_POINT_LAYER_ORDER_MANAGER_PERSISTENCE_NAME =
			"RASTER_POLYGON_LINE_POINT_LAYER_ORDER_MANAGER_PERSISTENCE_NAME";
	
	private static Logger logger =
		LoggerFactory.getLogger(RasterPolLinePointOrderManager.class);
	
	public RasterPolLinePointOrderManager() {
		
	}
	
	public int getPosition(FLayers target, FLayer newLayer) {
		
		int new_weight = 3;
		new_weight = getLayerWeight(newLayer);
		
		int len = target.getLayersCount();
		int item_w = 0;
		// from top to bottom,
		// look for a layer at
		// least as "heavy" as this one
		for (int i=(len-1); i>=0; i--) {
			item_w = getLayerWeight(target.getLayer(i));
			if (item_w >= new_weight) {
				return (i+1);
			}
		}
		// layer "falls" to bottom
		return 0;
	}

	public String getDescription() {
		return Messages.getText("_Raster_at_bottom_then_polygons_lines_points");
	}

	public String getName() {
		return Messages.getText("_Raster_Polygon_Line_Point_order_manager");
	}

	
	public String getCode() {
		return this.getClass().getName();
	}

	/**
	 * 
	 * @param lyr
	 * @return weight: point=0, line=1, polygon=2, other=3, raster=4
	 */
	private int getLayerWeight(FLayer lyr) {
		
		if (lyr.getClass().getName().toLowerCase().indexOf("raster") != -1) {
			return 4;
		} else {
			if (lyr instanceof FLyrVect) {
				FLyrVect lyrv = (FLyrVect) lyr;
				int type2d = Geometry.TYPES.SURFACE;
				try {
					type2d = simplifyType(lyrv.getGeometryType());
				} catch (ReadException e) {
					logger.error("While getting geo type.", e);
				}
				switch (type2d) {
				case TYPES.SURFACE:
					return 2;
				case TYPES.CURVE:
					return 1;
				case TYPES.POINT:
					return 0;
				default:
					// should not reach this
					return 3;
				}
			} else {
				// other:
				return 3;
			}
		}
		
	}

	/**
	 * 
	 */
	private int simplifyType(GeometryType gt) {

		if (gt.getType() == TYPES.POINT || gt.getType() == TYPES.MULTIPOINT) {
			return TYPES.POINT;
		} else {
			if (gt.isTypeOf(TYPES.CURVE) || gt.getType() == TYPES.MULTICURVE) {
				return TYPES.CURVE;
			} else {
				if (gt.isTypeOf(TYPES.SURFACE) || gt.getType() == TYPES.MULTISURFACE) {
					return TYPES.SURFACE;
				} else {
					// default
					return TYPES.SURFACE;
				}
			}
		}
	}

	public void loadFromState(PersistentState state) throws PersistenceException {
	}

	public void saveToState(PersistentState state) throws PersistenceException {
		state.set("name", this.getName());
	}
	
	public static void registerPersistent() {
		PersistenceManager manager = ToolsLocator.getPersistenceManager();
		if (manager.getDefinition(RASTER_POLYGON_LINE_POINT_LAYER_ORDER_MANAGER_PERSISTENCE_NAME) == null) {
			DynStruct def = manager.addDefinition(
					RasterPolLinePointOrderManager.class,
					RASTER_POLYGON_LINE_POINT_LAYER_ORDER_MANAGER_PERSISTENCE_NAME,
					RASTER_POLYGON_LINE_POINT_LAYER_ORDER_MANAGER_PERSISTENCE_NAME +" Persistence definition",
					null, 
					null);
			def.addDynFieldString("name").setMandatory(true);
		}			
	}
	
	public Object clone() throws CloneNotSupportedException {
		return new RasterPolLinePointOrderManager();
	}
	


}
